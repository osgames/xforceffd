unit Helper;

{
  Inno Setup
  Copyright (C) 1997-2005 Jordan Russell
  Portions by Martijn Laan
  For conditions of distribution and use, see LICENSE.TXT.

  Interface to 64-bit helper

  NOTE: These functions are NOT thread-safe. Do not call them from multiple
  threads simultaneously.

  $jrsoftware: issrc/Projects/Helper.pas,v 1.4 2005/04/13 20:38:52 jr Exp $
}

interface

uses
  Windows, SysUtils, Struct;

//function GetHelperResourceName: String;
function HelperGrantPermission(const AObjectType: DWORD;
  const AObjectName: PAnsiChar; const AEntries: TGrantPermissionEntry;
  const AEntryCount: Integer; const AInheritance: DWORD): DWORD;
procedure HelperRegisterServer(const AUnregister: Boolean;
  const Filename: String; const AFailCriticalErrors: Boolean);
procedure HelperRegisterTypeLibrary(const AUnregister: Boolean;
  Filename: String);
procedure SetHelperExeFilename(const Filename: String);
procedure StopHelper(const DelayAfterStopping: Boolean);

implementation

{x$DEFINE HELPERDEBUG}

uses
  Int64Em, CmnFunc2, PathFunc, InstFunc, Logging, Msgs, MsgIDs;

const
  HELPER_VERSION = 103;

const
  REQUEST_PING = 1;
  REQUEST_GRANT_PERMISSION = 2;
  REQUEST_REGISTER_SERVER = 3;
  REQUEST_REGISTER_TYPE_LIBRARY = 4;

type
  TRequestGrantPermissionData = record
    ObjectType: DWORD;
    EntryCount: DWORD;
    Inheritance: DWORD;
    ObjectName: array[0..4095] of AnsiChar;
    Entries: array[0..MaxGrantPermissionEntries-1] of TGrantPermissionEntry;
  end;
  TRequestRegisterServerData = record
    Unregister: BOOL;
    FailCriticalErrors: BOOL;
    Filename: array[0..4095] of AnsiChar;
    Directory: array[0..4095] of AnsiChar;
  end;
  TRequestRegisterTypeLibraryData = record
    Unregister: BOOL;
    Filename: array[0..4095] of WideChar;
  end;

  TRequestData = record
    SequenceNumber: DWORD;
    Command: DWORD;
    DataSize: DWORD;
    case Integer of
      0: (Data: array[0..65535] of Byte);
      1: (GrantPermissionData: TRequestGrantPermissionData);
      2: (RegisterServerData: TRequestRegisterServerData);
      3: (RegisterTypeLibraryData: TRequestRegisterTypeLibraryData);
  end;

  TResponseData = record
    SequenceNumber: DWORD;
    StatusCode: DWORD;
    ErrorCode: DWORD;
    DataSize: DWORD;
    Data: array[0..65535] of Byte;
  end;

var
  HelperExeFilename: String;
  HelperRunning, HelperNeedsRestarting: Boolean;
  HelperProcessHandle, HelperPipe: THandle;
  HelperCommandSequenceNumber: DWORD;
  HelperPipeNameSequence: LongWord;

  Request: TRequestData;
  Response: TResponseData;

(*function GetHelperResourceName: String;
begin
  {$R HelperEXEs.res}
  case ProcessorArchitecture of
    paX64: Result := 'HELPER_EXE_AMD64';
    paIA64: Result := 'HELPER_EXE_IA64';
  else
    Result := '';
  end;
end;}*)

procedure SetHelperExeFilename(const Filename: String);
begin
  HelperExeFilename := Filename;
end;

procedure StartHelper;
const
  FILE_FLAG_FIRST_PIPE_INSTANCE = $00080000;
  InheritableSecurity: TSecurityAttributes = (
    nLength: SizeOf(InheritableSecurity); lpSecurityDescriptor: nil;
    bInheritHandle: True);
var
  PerformanceCount: Integer64;
  PipeName: String;
  Pipe, RemotePipe: THandle;
  Mode: DWORD;
  StartupInfo: TStartupInfo;
  ProcessInfo: TProcessInformation;
begin
  Log('Starting 64-bit helper process.');

  { We don't *have* to check IsWin64 here; the helper *should* run fine without
    the new APIs added in 2003 SP1. But let's be consistent and disable 64-bit
    functionality across the board when the user is running 64-bit Windows and
    IsWin64=False. }
  if HelperExeFilename = '' then
    InternalError('64-bit helper EXE wasn''t extracted');

  repeat
    { Generate a very unique pipe name }
    Inc(HelperPipeNameSequence);
    HelperCommandSequenceNumber := GetTickCount;
    if not QueryPerformanceCounter(TLargeInteger(PerformanceCount)) then
      GetSystemTimeAsFileTime(TFileTime(PerformanceCount));
    PipeName := Format('\\.\pipe\InnoSetup64BitHelper-%.8x-%.8x-%.8x-%.8x%.8x',
      [GetCurrentProcessId, HelperPipeNameSequence, HelperCommandSequenceNumber,
       PerformanceCount.Hi, PerformanceCount.Lo]);

    { Create the pipe }
    Pipe := CreateNamedPipe(PChar(PipeName),
      PIPE_ACCESS_DUPLEX or FILE_FLAG_FIRST_PIPE_INSTANCE,
      PIPE_TYPE_MESSAGE or PIPE_READMODE_MESSAGE or PIPE_WAIT,
      1, 8192, 8192, 0, nil);
    if Pipe <> INVALID_HANDLE_VALUE then
      Break;
    { Loop if there's a name clash (ERROR_PIPE_BUSY), otherwise raise error }
    if GetLastError <> ERROR_PIPE_BUSY then
      Win32ErrorMsg('CreateNamedPipe');
  until False;

  try
    { Create an inheritable handle to the pipe for the helper to use }
    RemotePipe := CreateFile(PChar(PipeName), GENERIC_READ or GENERIC_WRITE,
      0, @InheritableSecurity, OPEN_EXISTING, 0, 0);
    if RemotePipe = INVALID_HANDLE_VALUE then
      Win32ErrorMsg('CreateFile');
    try
      Mode := PIPE_READMODE_MESSAGE or PIPE_WAIT;
      if not SetNamedPipeHandleState(RemotePipe, Mode, nil, nil) then
        Win32ErrorMsg('SetNamedPipeHandleState');

      FillChar(StartupInfo, SizeOf(StartupInfo), 0);
      StartupInfo.cb := SizeOf(StartupInfo);
      if not CreateProcess(PChar(HelperExeFilename),
         PChar(Format('helper %d 0x%x', [HELPER_VERSION, RemotePipe])), nil,
         nil, True, CREATE_DEFAULT_ERROR_MODE or CREATE_NO_WINDOW, nil,
         PChar(GetSystemDir), StartupInfo, ProcessInfo) then
        Win32ErrorMsg('CreateProcess');

      HelperRunning := True;
      HelperNeedsRestarting := False;
      HelperProcessHandle := ProcessInfo.hProcess;
      HelperPipe := Pipe;
      Pipe := 0;  { ensure the 'except' section can't close it now } 
      CloseHandle(ProcessInfo.hThread);
      LogFmt('Helper process PID: %u', [ProcessInfo.dwProcessId]);
    finally
      { We don't need a handle to RemotePipe after creating the process }
      CloseHandle(RemotePipe);
    end;
  except
    if Pipe <> 0 then
      CloseHandle(Pipe);
    raise;
  end;
end;

procedure StopHelper(const DelayAfterStopping: Boolean);
{ Stops the helper process if it's running }
var
  ExitCode: DWORD;
begin
  if not HelperRunning then
    Exit;

  { Before attempting to stop anything, set HelperNeedsRestarting to ensure
    CallHelper can never access a partially-stopped helper }
  HelperNeedsRestarting := True;

  Log('Stopping 64-bit helper process.');
  { Closing our handle to the pipe will cause the helper's blocking ReadFile
    call to return False, and the process to exit }
  CloseHandle(HelperPipe);
  HelperPipe := 0;
  while WaitForSingleObject(HelperProcessHandle, 10000) = WAIT_TIMEOUT do begin
    { It should never have to resort to terminating the process, but if the
      process for some unknown reason didn't exit in response to our closing
      the pipe, it should be safe to kill it since it most likely isn't doing
      anything other than waiting for a request. }
    Log('Helper isn''t responding; killing it.');
    TerminateProcess(HelperProcessHandle, 1);
  end;
  if GetExitCodeProcess(HelperProcessHandle, ExitCode) then begin
    if ExitCode = 0 then
      Log('Helper process exited.')
    else
      LogFmt('Helper process exited with failure code: 0x%x', [ExitCode]);
  end
  else
    Log('Helper process exited, but failed to get exit code.');
  CloseHandle(HelperProcessHandle);
  HelperProcessHandle := 0;
  HelperRunning := False;

  { Give it extra time to fully terminate to ensure that the EXE isn't still
    locked on SMP systems when we try to delete it in DeinitSetup.
    (Note: I'm not 100% certain this is needed; I don't have an SMP AMD64
    system to test on. It didn't seem to be necessary on IA64, but I suspect
    that may be because it doesn't execute x86 code in true SMP fashion.)
    This also limits the rate at which new helper processes can be spawned,
    which is probably a good thing. }
  if DelayAfterStopping then
    Sleep(250);
end;

procedure InternalCallHelper(const ACommand, ADataSize: DWORD);
var
  RequestSize, BytesRead: DWORD;
begin
  Inc(HelperCommandSequenceNumber);
  { On entry, only Request.Data needs to be filled }
  Request.SequenceNumber := HelperCommandSequenceNumber;
  Request.Command := ACommand;
  Request.DataSize := ADataSize;
  RequestSize := Cardinal(@TRequestData(nil^).Data) + ADataSize;
  try
    {$IFDEF HELPERDEBUG}
    LogFmt('Helper: Sending request (size: %u): Seq=%u, Command=%u, DataSize=%u',
      [RequestSize, Request.SequenceNumber, Request.Command, Request.DataSize]);
    {$ENDIF}
    if not TransactNamedPipe(HelperPipe, @Request, RequestSize, @Response,
       SizeOf(Response), BytesRead, nil) then
      Win32ErrorMsg('TransactNamedPipe');
    {$IFDEF HELPERDEBUG}
    LogFmt('Helper: Got response (size: %u): Seq=%u, StatusCode=%u, ErrorCode=%u, DataSize=%u',
      [BytesRead, Response.SequenceNumber, Response.StatusCode,
       Response.ErrorCode, Response.DataSize]);
    {$ENDIF}
    if (Cardinal(BytesRead) < Cardinal(@TResponseData(nil^).Data)) or
       (Response.DataSize <> Cardinal(BytesRead) - Cardinal(@TResponseData(nil^).Data)) then
      InternalError('CallHelper: Response message has wrong size');
    if Response.SequenceNumber <> Request.SequenceNumber then
      InternalError('CallHelper: Wrong sequence number');
    if Response.StatusCode = 0 then
      InternalError('CallHelper: Command did not execute');
  except
    { If an exception occurred, then the helper may have crashed or is in some
      weird state. Attempt to stop it now, and also set HelperNeedsRestarting
      to ensure it's restarted on the next call in case our stop attempt here
      fails for some reason. }
    HelperNeedsRestarting := True;
    Log('Exception while communicating with helper:' + SNewLine + GetExceptMessage);
    StopHelper(True);
    raise;
  end;
end;

procedure CallHelper(const ACommand, ADataSize: DWORD);
begin
  { Start/restart helper if needed }
  if not HelperRunning or HelperNeedsRestarting then begin
    StopHelper(True);
    StartHelper;
  end
  else begin
    { It is running -- or so we think. Before sending the specified request,
      send a ping request to verify that it's still alive. It may have somehow
      died since we last talked to it (unlikely, though). }
    try
      InternalCallHelper(REQUEST_PING, 0);
    except
      { Don't propogate any exception; just log it and restart the helper }
      Log('Ping failed; helper seems to have died.');
      StopHelper(True);
      StartHelper;
    end;
  end;

  InternalCallHelper(ACommand, ADataSize);
end;

{ High-level interface functions }

function HelperGrantPermission(const AObjectType: DWORD;
  const AObjectName: PAnsiChar; const AEntries: TGrantPermissionEntry;
  const AEntryCount: Integer; const AInheritance: DWORD): DWORD;
begin
  if (AEntryCount < 1) or
     (AEntryCount > SizeOf(Request.GrantPermissionData.Entries) div SizeOf(Request.GrantPermissionData.Entries[0])) then
    InternalError('HelperGrantPermission: Invalid entry count');

  Request.GrantPermissionData.ObjectType := AObjectType;
  Request.GrantPermissionData.EntryCount := AEntryCount;
  Request.GrantPermissionData.Inheritance := AInheritance;
  StrLCopy(Request.GrantPermissionData.ObjectName, AObjectName,
    SizeOf(Request.GrantPermissionData.ObjectName)-1);
  Move(AEntries, Request.GrantPermissionData.Entries,
    AEntryCount * SizeOf(Request.GrantPermissionData.Entries[0]));

  CallHelper(REQUEST_GRANT_PERMISSION, SizeOf(Request.GrantPermissionData));

  Result := Response.ErrorCode;
end;

procedure HelperRegisterServer(const AUnregister: Boolean;
  const Filename: String; const AFailCriticalErrors: Boolean);
{ Registers or unregisters the specified DLL/OCX inside the helper. Raises an
  exception on failure.
  NOTE: Filename is not expanded before passing it to the helper. }
const
  ProcNames: array[Boolean] of String = ('DllRegisterServer',
    'DllUnregisterServer');
var
  SaveCursor: HCURSOR;
begin
  Request.RegisterServerData.Unregister := AUnregister;
  Request.RegisterServerData.FailCriticalErrors := AFailCriticalErrors;
  StrPLCopy(Request.RegisterServerData.Filename, Filename,
    SizeOf(Request.RegisterServerData.Filename)-1);
  StrPLCopy(Request.RegisterServerData.Directory, PathExtractDir(Filename),
    SizeOf(Request.RegisterServerData.Directory)-1);

  SaveCursor := SetCursor(LoadCursor(0, IDC_WAIT));
  try
    { Stop the helper before and after the call to be 100% sure the state of the
      helper is clean prior to and after registering. Can't trust foreign code. }
    StopHelper(False);
    CallHelper(REQUEST_REGISTER_SERVER, SizeOf(Request.RegisterServerData));
    StopHelper(False);
  finally
    SetCursor(SaveCursor);
  end;

  case Response.StatusCode of
    1: begin
         { The LoadLibrary call failed }
         SetLastError(Response.ErrorCode);
         Win32ErrorMsg('LoadLibrary');
       end;
    2: begin
         { The GetProcAddress call failed }
         SetLastError(Response.ErrorCode);
         Win32ErrorMsg('GetProcAddress');
       end;
    3: begin
         { The call to Dll(Un)RegisterServer was made; possibly succeeded }
         if FAILED(Response.ErrorCode) then
           RaiseOleError(ProcNames[AUnregister], Response.ErrorCode);
       end;
  else
    InternalError('HelperRegisterServer: StatusCode invalid');
  end;
end;

procedure HelperRegisterTypeLibrary(const AUnregister: Boolean;
  Filename: String);
{ Registers or unregisters the specified type library inside the helper.
  Raises an exception on failure. }
var
  Len: Integer;
begin
  Filename := PathExpand(Filename);

  Request.RegisterTypeLibraryData.Unregister := AUnregister;
  Len := MultiByteToWideChar(CP_ACP, 0, PChar(Filename), Length(Filename),
    Request.RegisterTypeLibraryData.Filename,
    (SizeOf(Request.RegisterTypeLibraryData.Filename) div SizeOf(Request.RegisterTypeLibraryData.Filename[0])) - 1);
  Request.RegisterTypeLibraryData.Filename[Len] := #0;

  { Stop the helper before and after the call to be 100% sure the state of the
    helper is clean prior to and after registering. Can't trust foreign code. }
  StopHelper(False);
  CallHelper(REQUEST_REGISTER_TYPE_LIBRARY, SizeOf(Request.RegisterTypeLibraryData));
  StopHelper(False);

  case Response.StatusCode of
    1: begin
         { The LoadTypeLib call failed }
         RaiseOleError('LoadTypeLib', Response.ErrorCode);
       end;  
    2: begin
         { The call to RegisterTypeLib was made; possibly succeeded }
         if (Response.ErrorCode <> S_OK) or AUnregister then
           RaiseOleError('RegisterTypeLib', Response.ErrorCode);
       end;
    3: begin
         { The ITypeLib::GetLibAttr call failed }
         RaiseOleError('ITypeLib::GetLibAttr', Response.ErrorCode);
       end;
    4: begin
         { The call to UnRegisterTypeLib was made; possibly succeeded }
         if (Response.ErrorCode <> S_OK) or not AUnregister then
           RaiseOleError('UnRegisterTypeLib', Response.ErrorCode);
       end;
  else
    InternalError('HelperRegisterTypeLibrary: StatusCode invalid');
  end;
end;

end.

