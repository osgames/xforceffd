object UpdateForm: TUpdateForm
  Left = 247
  Top = 125
  BorderStyle = bsDialog
  Caption = 'Update'
  ClientHeight = 279
  ClientWidth = 388
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 8
    Top = 8
    Width = 119
    Height = 13
    Caption = 'Aktuelle X-Force-Version:'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object Versionlabel: TLabel
    Left = 136
    Top = 8
    Width = 3
    Height = 13
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object NewVersion: TLabel
    Left = 88
    Top = 38
    Width = 3
    Height = 13
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object Button1: TButton
    Left = 8
    Top = 32
    Width = 75
    Height = 25
    Caption = 'Update laden'
    TabOrder = 0
    OnClick = Button1Click
  end
  object InfoMemo: TMemo
    Left = 8
    Top = 64
    Width = 369
    Height = 169
    Lines.Strings = (
      'Kein Update geladen')
    ReadOnly = True
    ScrollBars = ssVertical
    TabOrder = 1
  end
  object FileProgress: TProgressBar
    Left = 8
    Top = 240
    Width = 369
    Height = 17
    Step = 1
    TabOrder = 2
  end
  object DoPatch: TButton
    Left = 8
    Top = 240
    Width = 121
    Height = 25
    Caption = 'Update durchf'#252'hren'
    TabOrder = 3
    Visible = False
    OnClick = DoPatchClick
  end
  object OpenUpdate: TOpenDialog
    DefaultExt = 'kdp'
    Filter = 'XForce - Update (*.kdp)|*.kdp'
    Options = [ofHideReadOnly, ofNoChangeDir, ofPathMustExist, ofFileMustExist]
    Left = 256
    Top = 8
  end
end
