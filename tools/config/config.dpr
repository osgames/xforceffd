program config;

uses
  Forms,
  Registry,
  Windows,
  Dialogs,
  SysUtils,
  KD4Utils,
  frmMain in 'frmMain.pas' {MainForm},
  frmlanguage in 'frmlanguage.pas' {LanguageForm},
  frmUpdate in 'frmUpdate.pas' {UpdateForm},
  frmOnline in 'frmOnline.pas' {OnlineUpdateForm},
  frmProtokoll in 'frmProtokoll.pas' {Protokoll};

{$R *.RES}

var
  Reg      : TRegistry;
  Dir      : String;
  FileName : String;
  TempFile : PChar;
begin
  if (ParamCount>0) and (ParamStr(1)='/copy') then
  begin
    // Wartet, bis die Datei sich ins X-Force Verzeichnis kopieren kann
    Reg:=OpenXForceRegistry('',false);
    if Reg<>nil then
    begin
      Dir:=IncludeTrailingBackSlash(Reg.ReadString('InstallDir'));
      Reg.Free;

      FileName:=Dir+'config.exe';
      if not FileExists(FileName) then
        exit;

      if UpperCase(FileName)=UpperCase(Application.ExeName) then
        exit;

      MessageDlg('Zum Update muss das Konfigurationsprogramm neu gestartet werden.',mtInformation,[mbOK],0);
      while not CopyFile(PChar(Application.ExeName),PChar(FileName),false) do
      begin
        if GetLastError=32 then
          Sleep(1000)
        else
          exit;
      end;
      Application.BringToFront;
      MessageDlg('Update des Konfigurationsprogramm erfolgreich abgeschlossen. Konfigurationsprogramm kann jetzt neu gestartet werden.',mtInformation,[mbOK],0);
    end;
    exit;
  end;
  // Tempor�re Datei l�schen
  TempFile:=AllocMem(200);
  GetEnvironmentVariable('TMP',PChar(TempFile),200);
  TempFile:=strCat(TempFile,PChar('\config.exe'));

  DeleteFile(TempFile);

  Reg:=OpenXForceRegistry('updates');
  if Reg<>nil then
  begin
    Reg.WriteString('CONFIG',ConfigVersion);
    Reg.Free;
  end;

  Application.Initialize;
  Application.CreateForm(TMainForm, MainForm);
  Application.CreateForm(TLanguageForm, LanguageForm);
  Application.CreateForm(TUpdateForm, UpdateForm);
  Application.CreateForm(TOnlineUpdateForm, OnlineUpdateForm);
  Application.CreateForm(TProtokoll, Protokoll);
  Application.Run;
end.
