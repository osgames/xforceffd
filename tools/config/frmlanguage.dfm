object LanguageForm: TLanguageForm
  Left = 754
  Top = 230
  BorderIcons = [biSystemMenu, biMaximize]
  BorderStyle = bsDialog
  Caption = 'Choose Language'
  ClientHeight = 264
  ClientWidth = 243
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 8
    Top = 8
    Width = 98
    Height = 13
    Caption = 'Installed Languages:'
  end
  object ListBox1: TListBox
    Left = 8
    Top = 32
    Width = 225
    Height = 193
    Style = lbOwnerDrawFixed
    ItemHeight = 18
    TabOrder = 0
    OnClick = ListBox1Click
    OnDblClick = ListBox1DblClick
    OnDrawItem = ListBox1DrawItem
  end
  object BitBtn1: TBitBtn
    Left = 8
    Top = 232
    Width = 89
    Height = 25
    Enabled = False
    TabOrder = 1
    OnClick = BitBtn1Click
    Kind = bkOK
  end
  object BitBtn2: TBitBtn
    Left = 144
    Top = 232
    Width = 89
    Height = 25
    Caption = 'Cancel'
    TabOrder = 2
    OnClick = BitBtn2Click
    Kind = bkCancel
  end
end
