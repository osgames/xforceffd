unit frmUpdate;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Registry, FileCtrl, ShellApi, Undo, FileClass, ComCtrls;

type
  TInstallProgress = procedure(FileNr,FileCount: Integer) of object;

  TUpdateForm = class(TForm)
    Label1: TLabel;
    Versionlabel: TLabel;
    OpenUpdate: TOpenDialog;
    Button1: TButton;
    NewVersion: TLabel;
    InfoMemo: TMemo;
    FileProgress: TProgressBar;
    DoPatch: TButton;
    procedure FormShow(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure DoPatchClick(Sender: TObject);
  private
    fActions   : TList;
    fAutoUpdate: Boolean;
    Version    : String;
    fFile      : String;
    Dir        : String;
    FOnInstallProgress: TInstallProgress;

    procedure InstallLanguage(GameSet: String; Language: String; Data: TStringList);
    { Private-Deklarationen }
  public
    Protokoll  : TStringList;
    function OpenPatch(FileName: String): boolean;
    function PerformPatch(FileName: String): Boolean;

    property OnInstallProgress: TInstallProgress read FOnInstallProgress write fOnInstallProgress;
    { Public-Deklarationen }
  end;

var
  UpdateForm: TUpdateForm;

  SCaptionHistorie  : String;
  MUpdateComplete   : String;
  CInformation      : String;
  MErrorOccured     : String;
  EInvUpdateFile    : String;
  EWrongVersion     : String;
  SUpdateTo         : String;
  IconSize          : Integer = 16;

  UpdateFehlerFrei  : Boolean;

implementation

{$R *.DFM}

uses
  frmMain, PatchUnit, ArchivFile, ExtRecord, record_utils, string_utils,
  gameset_api, XForce_Types, ProjektRecords, Koding, Defines;

procedure TUpdateForm.FormShow(Sender: TObject);
begin
  Versionlabel.Caption:=Version;
  Dir:=IncludeTrailingBackslash(Directory);

  Protokoll.Clear;
end;

procedure TUpdateForm.Button1Click(Sender: TObject);
begin
  if OpenUpdate.Execute then
  begin
    OpenPatch(OpenUpdate.FileName);
  end;
end;

procedure TUpdateForm.FormCreate(Sender: TObject);
begin
  fActions:=TList.Create;
  Protokoll:=TStringList.Create;
end;

procedure TUpdateForm.FormDestroy(Sender: TObject);
begin
  while fActions.Count>0 do
  begin
    TPatchAction(fActions[0]).Free;
    fActions.Delete(0);
  end;
  fActions.Free;
end;

procedure TUpdateForm.DoPatchClick(Sender: TObject);
var
  Action   : TPatchAction;
  Dummy    : Integer;
  Archiv   : TArchivFile;
  SeArchiv : TArchivFile;
  TrArchiv : TArchivFile;
  PatchFile: TStringList;
  Data     : TStringList;

  Logging  : TUninstallLog;
  UnFile   : TFile;
begin
  UnFile:=TFile.Create(Directory+'unins000.dat',fdOpenExisting,faReadWrite,fsReadWrite);
  Logging:=TSetupUninstallLog.Create;
  Logging.Load(UnFile,Directory+'unins000.dat');
  DoPatch.Visible:=false;
  FileProgress.Visible:=true;
  Archiv:=TArchivFile.Create;
  Archiv.OpenArchiv(fFile);
  SeArchiv:=TArchivFile.Create;
  try
    for Dummy:=0 to fActions.Count-1 do
    begin
      Action:=TPatchAction(fActions[Dummy]);
      case Action.Action of
        atReplace :
        begin
          Archiv.OpenRessource(Action.Param1);
          SeArchiv.OpenArchiv(Action.Param2);
          SeArchiv.ReplaceRessource(Archiv.Stream,Action.Param3);
        end;
        atFile    :
        begin
          Archiv.OpenRessource(Action.Param1);

          if not FileExists(Action.Param2) then
          begin
            Logging.Add(utDeleteFile,[Action.Param2],0);
          end;

          Archiv.Stream.SaveToFile(Action.Param2);
        end;
        atDeleteRessource :
        begin
          try
            SeArchiv.OpenArchiv(Action.Param2);
            SeArchiv.DeleteRessource(Action.Param1);
          except
          end;
        end;
        atDeleteFile      : DeleteFile(Action.Param1);
        atMoveRessource   :
        begin
          SeArchiv.OpenArchiv(Action.Param2);
          TrArchiv:=TArchivFile.Create;
          TrArchiv.OpenArchiv(Action.Param3);
          SeArchiv.OpenRessource(Action.Param1);
          TrArchiv.ReplaceRessource(SeArchiv.Stream,Action.Param1);
          SeArchiv.DeleteRessource(Action.Param1);
          TrArchiv.Free;
        end;
        atRenameRessource :
        begin
          SeArchiv.OpenArchiv(Action.Param2);
          SeArchiv.RenameRessource(Action.Param1,Action.Param3);
        end;
        atCreateDir       :
        begin
          if not DirectoryExists(Action.Param1) then
          begin
            Logging.Add(utDeleteDirOrFiles,[Action.Param1],0);
          end;

          ForceDirectories(Action.Param1);
        end;
        atStartProgramm   :
        begin
          ShellExecute(0,'open',PChar(Action.Param1),PChar(Action.Param2),PChar(ExtractFilePath(Action.Param2)),SW_HIDE);
        end;
        atApplyGameSetLanguage :
        begin
          Data:=TStringList.Create;
          Archiv.OpenRessource(Action.Param1);
          Data.LoadFromStream(Archiv.Stream);
          InstallLanguage(Action.Param3,Action.Param2,Data);
          Data.Free;
        end;
      end;
      if Assigned(fOnInstallProgress) then
        fOnInstallProgress(Dummy+1,FileProgress.Max);
    end;
    PatchFile:=TStringList.Create;
    try
      PatchFile.LoadFromFile(Dir+'patch.txt');
    except
      PatchFile.Add(StringOfChar('-',78));
      PatchFile.Add(SCaptionHistorie);
      PatchFile.Add(StringOfChar('-',78));
    end;

    PatchFile.Insert(3,' '+Version);
    PatchFile.Insert(4,StringOfChar('-',78));
    PatchFile.Insert(5,'');
    PatchFile.Insert(6,'');
    PatchFile.Insert(7,StringOfChar('-',78));
    for Dummy:=InfoMemo.Lines.Count-1 downto 0 do
      PatchFile.Insert(6,'  '+InfoMemo.Lines[Dummy]);
    PatchFile.SaveToFile(Dir+'patch.txt');
    PatchFile.Free;

    if fAutoUpdate then
    begin
      Protokoll.Add(Format('%s',[Version]));
      Protokoll.Add(StringOfChar('-',78));

      for Dummy:=0 to InfoMemo.Lines.Count-1 do
        Protokoll.Add(InfoMemo.Lines[Dummy]);
      Protokoll.Add('');
    end
    else
      Application.MessageBox(PChar(Format(MUpdateComplete,[Version])),PChar(CInformation),MB_ICONINFORMATION);

    UpdateFehlerFrei:=true;
  except
    on E: Exception do
    begin
      Application.MessageBox(PChar(MErrorOccured+#13#10+E.Message),PChar(EError),MB_ICONERROR);
      UpdateFehlerFrei:=false;
    end;
  end;
  SeArchiv.Free;
  Archiv.Free;
  FileProgress.Visible:=false;

  UnFile.Free;

  Logging.Save(Directory+'unins000.dat',false,false);

  Logging.Free;
  Close;
end;

function TUpdateForm.OpenPatch(FileName: String):boolean;
var
  Archiv    : TArchivFile;
  Head      : TPatchHeader;
  Count     : Integer;
  Dummy     : Integer;
  Temp      : TPatchAction;
  Strings   : TStringList;
  Text      : String;
  LastEmpty : Boolean;
begin
  Archiv:=TArchivFile.Create;
  try
    result:=true;
    Archiv.OpenArchiv(FileName);
    try
      try
        Archiv.OpenRessource('Header');
      except
        Archiv.OpenRessource('Head');
      end;
      Archiv.Stream.Read(Head,SizeOf(Head));
      if Head.Signatur<>$4CE018AF then
        raise Exception.Create('');
    except
      raise Exception.Create(EInvUpdateFile);
    end;

    NewVersion.Caption:=Head.InfoText;
    try
      Archiv.OpenRessource('Info');
      Strings:=TStringList.Create;
      Strings.LoadFromStream(Archiv.Stream);
      InfoMemo.Lines.Clear;
      Text:='';
      for Dummy:=0 to Strings.Count-1 do
      begin
        Text:=Text+WrapText(Strings[Dummy],65)+#13#10;
      end;
      Strings.Text:=Text;
      LastEmpty:=true;
      for Dummy:=0 to Strings.Count-1 do
      begin
        if length(Strings[Dummy])>0 then
        begin
          if (Copy(Strings[Dummy],1,2)='- ') then
            LastEmpty:=false
          else
          begin
            if not LastEmpty then
              Strings[Dummy]:='  '+Strings[Dummy];
          end;
        end
        else
          LastEmpty:=true;
      end;
      InfoMemo.Lines:=Strings;
      Strings.Free;
    except
      InfoMemo.Lines.Clear;
    end;
    while fActions.Count>0 do
    begin
      TPatchAction(fActions[0]).Free;
      fActions.Delete(0);
    end;
    Archiv.OpenRessource('Actions');
    Archiv.Stream.Read(Count,SizeOf(Count));
    for Dummy:=0 to Count-1 do
    begin
      Temp:=TPatchAction.Create;
      Temp.InstallDir:=Directory;
      Temp.LoadFromStream(Archiv.Stream);
      fActions.Add(Temp);
    end;
    FileProgress.max:=fActions.Count;
    FileProgress.Position:=0;
    DoPatch.Visible:=true;
    Archiv.Free;
    Version:=Head.InfoText;
    fFile:=FileName;
    InfoMemo.SelStart:=0;
    InfoMemo.SelLength:=0;
  except
    on E:Exception do
    begin
      Archiv.Free;
      Application.MessageBox(PChar(MErrorOccured+#13#10+E.Message),PChar(EError),MB_ICONERROR);
      result:=false;
      exit;
    end;
  end;
end;

function TUpdateForm.PerformPatch(FileName: String): Boolean;
begin
  fAutoUpdate:=true;
  Dir:=Directory;
  OpenPatch(FileName);
  DoPatchClick(nil);
  fAutoUpdate:=false;
  
  result:=UpdateFehlerFrei;
end;

procedure TUpdateForm.InstallLanguage(GameSet, Language: String;
  Data: TStringList);
var
  Archiv    : TArchivFile;
  Projects  : TRecordArray;
  Aliens    : TRecordArray;
  UFOS      : TRecordArray;
  Languages : TStringArray;
  Stream    : TMemoryStream;
  Dummy     : Integer;
  Lang      : String;

  procedure ImportToArray(ID: Cardinal;var Arr: TRecordArray;Prop: String);
  var
    Dummy: Integer;
    Text : String;
    NName: String;
    NDesc: String;
  begin
     if Prop='Info' then
    begin
      NName:=Copy(Data[0],3,length(Data[0]));
      Data.Delete(0);
    end;
    Data.Delete(0);
    NDesc:='';
    while (length(Data[0])=0) or (Copy(Data[0],1,59)<>'-----------------------------------------------------------') do
    begin
      if NDesc<>'' then
        NDesc:=NDesc+#13#10;
      NDesc:=NDesc+Data[0];
      Data.Delete(0);
    end;

    for Dummy:=0 to high(Arr) do
    begin
      if Arr[Dummy].GetCardinal('ID')=ID then
      begin
        if Prop='Info' then
        begin
          Text:=Arr[Dummy].GetString('Name');
          Text:=string_utils_SetLanguageString(lang,Text,NName);
          Arr[Dummy].SetString('Name',Text);
        end;

        Text:=Arr[Dummy].GetString(Prop);
        Text:=string_utils_SetLanguageString(lang,Text,NDesc);
        Arr[Dummy].SetString(Prop,Text);
        exit;
      end;
    end;
  end;
  
  procedure ImportObject;
  var
    ID  : Cardinal;
    Typ : Char;
    Prop: String;
    expl: TStringArray;
  begin
    expl:=string_utils_explode('/',copy(Data[0],2,length(Data[0])-2));
    Assert(length(Expl)>=2,'Invalid Gameset-Language File');
    Assert(length(expl[0])>0);
    Typ:=expl[0][1];
    ID:=StrToInt64(Expl[1]);
    if length(expl)>2 then
      Prop:=expl[2]
    else
      Prop:='Info';

    Data.Delete(0);

    case Typ of
      'P' : ImportToArray(ID,Projects,Prop);
      'U' : ImportToArray(ID,UFOS,Prop);
      'A' : ImportToArray(ID,Aliens,Prop);
    end;
  end;

  procedure SaveRessource(Name: String; Version: Cardinal; Arr: TRecordArray;Projects: Boolean = false);
  var
    Stream  : TMemoryStream;
    StreamC : TMemoryStream;
    Header  : TEntryHeader;
    Dummy   : Integer;
  begin
    Stream:=TMemoryStream.Create;
    StreamC:=TmemoryStream.Create;

    Header.Version:=Version;
    Header.Entrys:=length(Arr);

    Stream.Write(Header,SizeOf(Header));

    if Projects then
    begin
      for Dummy:=0 to high(Arr) do
        SaveProjectRecord(Arr[Dummy],Stream);
    end
    else
    begin
      for Dummy:=0 to high(Arr) do
        Arr[Dummy].SaveToStream(Stream);
    end;

    Encode(Stream,StreamC);

    Archiv.ReplaceRessource(StreamC,Name,true);

    Stream.Free;
    StreamC.Free;
  end;

begin
  Lang:=lowercase(Language);
  try
    Archiv:=TArchivFile.Create;
    Archiv.OpenArchiv(GameSet);
    gameset_api_LoadProjects(Archiv,Projects);
    gameset_api_LoadAliens(Archiv,Aliens);
    gameset_api_LoadUFOs(Archiv,UFOS);
    gameset_api_LoadLanguages(Archiv,Languages);

    // Text importieren
    while Data.Count>0 do
    begin
      if (length(trim(Data[0]))>0) and (trim(Data[0])[1]='[') then
        ImportObject;

      Data.Delete(0);
    end;

    string_utils_Add(Languages,Lang);

    Stream:=TMemoryStream.Create;

    // Sprachen Speichern
    Dummy:=Length(Languages);
    Stream.Write(Dummy,SizeOf(Dummy));
    for Dummy:=0 to high(Languages) do
    begin
      WriteString(Stream,Languages[Dummy]);
    end;
    Archiv.ReplaceRessource(Stream,'Languages');
    Stream.Free;

    // Daten abspeichern
    SaveRessource(RNForschdat,ForschVersion,Projects,true);
    SaveRessource(RNUFOs,UFOMVersion,UFOs);
    SaveRessource(RNAlien,AlienEditVersion,Aliens);
  finally
    Archiv.Free;
  end;
end;

end.
