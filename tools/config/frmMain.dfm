object MainForm: TMainForm
  Left = 252
  Top = 368
  BorderIcons = [biSystemMenu, biMinimize]
  BorderStyle = bsSingle
  Caption = 'X-Force Konfiguration'
  ClientHeight = 280
  ClientWidth = 284
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  PixelsPerInch = 96
  TextHeight = 13
  object LanguageLabel: TLabel
    Left = 128
    Top = 60
    Width = 74
    Height = 13
    Caption = 'LanguageLabel'
  end
  object BitModeLabel: TLabel
    Left = 128
    Top = 92
    Width = 3
    Height = 13
  end
  object Label1: TLabel
    Left = 8
    Top = 12
    Width = 51
    Height = 13
    Caption = 'Version: '
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Versionlabel: TLabel
    Left = 64
    Top = 12
    Width = 3
    Height = 13
  end
  object languageButton: TButton
    Left = 8
    Top = 53
    Width = 105
    Height = 25
    Caption = 'Sprache'
    TabOrder = 0
    OnClick = languageButtonClick
  end
  object Updatebutton: TButton
    Left = 128
    Top = 215
    Width = 105
    Height = 25
    Caption = 'Update'
    TabOrder = 1
    Visible = False
    OnClick = UpdatebuttonClick
  end
  object OnlineButton: TButton
    Left = 8
    Top = 85
    Width = 105
    Height = 25
    Caption = 'Online-Update'
    TabOrder = 2
    OnClick = OnlineButtonClick
  end
  object BitmapModeBox: TGroupBox
    Left = 8
    Top = 197
    Width = 265
    Height = 73
    Caption = 'Farbtiefe:'
    TabOrder = 3
    object Bitmode16: TRadioButton
      Left = 8
      Top = 24
      Width = 113
      Height = 13
      Caption = 'Bitmode16'
      TabOrder = 0
      OnClick = BitmapModeChange
    end
    object Bitmode32: TRadioButton
      Left = 8
      Top = 45
      Width = 113
      Height = 17
      Caption = 'Bitmode32'
      TabOrder = 1
      OnClick = BitmapModeChange
    end
  end
  object ModusBox: TGroupBox
    Left = 8
    Top = 117
    Width = 265
    Height = 73
    Caption = 'ModusBox'
    TabOrder = 4
    object FullscreenMode: TRadioButton
      Left = 8
      Top = 19
      Width = 113
      Height = 17
      Caption = 'FullscreenMode'
      TabOrder = 0
      OnClick = ChangeModeClick
    end
    object WindowMode: TRadioButton
      Left = 8
      Top = 42
      Width = 113
      Height = 17
      Caption = 'WindowMode'
      TabOrder = 1
      OnClick = ChangeModeClick
    end
  end
  object DirectoryEdit: TEdit
    Left = 8
    Top = 32
    Width = 265
    Height = 16
    BorderStyle = bsNone
    Color = clBtnFace
    ReadOnly = True
    TabOrder = 5
    Text = 'DirectoryEdit'
  end
  object XPManifest: TXPManifest
    Left = 248
    Top = 8
  end
end
