unit frmOnline;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, CheckLst, WinINet, ComCtrls,thaXML, KD4Utils, ExtCtrls, FileCtrl,
  Registry, ImgList;

const
 // UpdateFile = 'http://fuji/download/updates/updates.xml';
  UpdateFile                = 'http://www.xforce-online.de/download/updates/updates.xml';

  ConnectToInternet         = false;
  OfflineInfo               : String = '';
  BeginConnection           : String = '';
  CouldNotConnectToInternet : String = '';
  ConnectionApproved        : String = '';
  GetFileInfos              : String = '';
  Download                  : String = '';
  DownloadReady             : String = '';

  CInstalled                : String = '';
  CAvaible                  : String = '';

type
  TUpdateInfo = record
    id           : Integer;
    Test         : Boolean;
    Important    : Boolean;
    Icon         : Integer;
    Title        : String;
    Size         : Integer;
    Date         : String;
    Description  : String;
    URL          : String;
    references   : String;
    reference    : Integer;
    package      : String;
    Version      : String;
    NeedVersions : String;
    action       : String;
    HomePage     : String;
    Update       : Boolean;
    Installed    : Boolean;
  end;

  TOnlineUpdateForm = class(TForm)
    availUpdateLabel: TLabel;
    UpdateList: TCheckListBox;
    SearchUpdates: TButton;
    XMLParser: TXMLParser;
    Panel1: TPanel;
    descriptionLabel: TLabel;
    InstallUpdates: TButton;
    StatusBar1: TStatusBar;
    DownloadPanel: TPanel;
    DownloadProgressBar: TProgressBar;
    SizeLabel: TLabel;
    recvLabel: TLabel;
    FileSizeLabel: TLabel;
    ReceivedLabel: TLabel;
    DownloadSize: TLabel;
    CompleteSize: TLabel;
    InstallPanel: TPanel;
    InstallLabel: TLabel;
    InstallBar: TProgressBar;
    SaveDownloads: TCheckBox;
    Panel2: TPanel;
    OfflineCheckBox: TCheckBox;
    Button1: TButton;
    IconList: TImageList;
    HomepageButton: TButton;
    bpsLabel: TLabel;
    BytesPerSecondLabel: TLabel;
    remainLabel: TLabel;
    RemainingLabel: TLabel;
    procedure SearchUpdatesClick(Sender: TObject);
    procedure UpdateListDrawItem(Control: TWinControl; Index: Integer;
      Rect: TRect; State: TOwnerDrawState);
    procedure UpdateListClick(Sender: TObject);
    procedure InstallUpdatesClick(Sender: TObject);
    procedure UpdateListClickCheck(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure TestversionClick(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure HomepageButtonClick(Sender: TObject);
    procedure OfflineCheckBoxClick(Sender: TObject);
  private
    fError : TBitmap;
    procedure DownloadProgress(Status: String; AktBytesRead,
      TotallyBytes, BytesPerSecond: Integer);

    procedure ParseAllDownloads(Tag: TXMLTag);
    procedure FillUpdateList;

    function DownloadFile(Index: Integer): Boolean;

    procedure RefreshSize;

    procedure ProgressUpdate(FileNr,FileCount: Integer);

    function FindListIndexOfUpdate(UpdateIndex: Integer): Integer;
    { Private-Deklarationen }
  public
    fUpdates: Array of TUpdateInfo;
    { Public-Deklarationen }
  end;

var
  OnlineUpdateForm: TOnlineUpdateForm;
  DownloadError   : String;
  RestartConfig   : String;

implementation

uses frmMain, frmUpdate, frmProtokoll, string_utils, file_utils, ShellAPI;

type
  TDownloadCallBack = procedure(Status: String; AktBytesRead: Integer;TotallyBytes: Integer; BytesPerSecond: Integer) of Object;

function DownloadFileFromURL(URL: String; Stream: TStream; CallBack: TDownloadCallBack): boolean;
var
  Connection: Pointer;
  URLHandle : Pointer;
  Buffer    : Array[0..8191] of Char;
  Size      : Cardinal;
  FileSize  : Integer;
  Res       : Cardinal;
  Readed    : Cardinal;
  TotalRead : Cardinal;
  StartTime : Cardinal;

  procedure CallCallBack(Status: String);
  var
    BytesPerSecond: Integer;
    TimeConsumed  : Integer;
  begin
    if Assigned(CallBack) then
    begin
      TimeConsumed:=GetTickCount-StartTime;
      BytesPerSecond:=round(TotalRead/(TimeConsumed/1000));
      CallBack(Status,TotalRead,FileSize,BytesPerSecond);
    end;
  end;

begin
  TotalRead:=0;
  FileSize:=0;
  result:=false;
  CallCallBack(beginConnection);
  if (ConnectToInternet) and (not InternetAutodial(INTERNET_AUTODIAL_FORCE_ONLINE,Application.Handle)) then
  begin
    CallCallBack(CouldNotConnectToInternet);
    exit;
  end;

  Connection:=InternetOpen(PChar(Application.Exename),INTERNET_OPEN_TYPE_PRECONFIG,nil,nil,0);
  if Connection=nil then
  begin
//    InternetErrorDlg(Application.Handle,nil,)
    CallCallBack(CouldNotConnectToInternet);
    exit;
  end;

  CallCallBack(Format(ConnectionApproved,[URL]));
  URLHandle:=InternetOpenUrl(Connection,PChar(URL),nil,0,0,INTERNET_FLAG_RELOAD);

  Size:=8192;
  Res:=0;
  HttpQueryInfo(URLHandle,HTTP_QUERY_STATUS_CODE,Addr(Buffer[0]),Size,Res);
  if Buffer<>'200' {HTTP_OK 200} then
  begin
    CallCallBack('HTTP-Error-Code:'+Buffer);
    if (Buffer='401') or (Buffer='407') then
    begin
      while InternetErrorDlg(Application.Handle,URLHandle,ERROR_INTERNET_INCORRECT_PASSWORD,0,Pointer(Res))=ERROR_INTERNET_FORCE_RETRY do
      begin
        URLHandle:=InternetOpenUrl(Connection,PChar(URL),nil,0,0,INTERNET_FLAG_RELOAD)
      end;
    end
    else
    begin
      InternetCloseHandle(Connection);
      exit;
    end;
  end;

  Size:=8192;
  CallCallBack(GetFileInfos);
  if not HttpQueryInfo(URLHandle,HTTP_QUERY_CONTENT_LENGTH,Addr(Buffer[0]),Size,Res) then
  begin
    InternetCloseHandle(Connection);
    exit;
  end;

  OnlineUpdateForm.DownloadPanel.Visible:=true;

  FileSize:=StrToInt(Buffer);
  TotalRead:=0;
  Stream.Size:=FileSize;

  StartTime:=GetTickCount;
  repeat
    InternetReadFile(URLHandle,Addr(Buffer[0]),8192,Readed);
    Stream.Write(Buffer,Readed);
    inc(TotalRead,Readed);
    CallCallBack(Download);
    Application.ProcessMessages;
  until Readed=0;

  if TotalRead<>FileSize then
  begin
    CallCallBack('Fehler beim Herunterladen der Datei (Serververbindung unterbrochen?)');
    Stream.Size:=0;             
    InternetCloseHandle(Connection);
    exit;
  end;

  CallCallBack(DownloadReady);
  InternetCloseHandle(Connection);
//  InternetAutodialHangup(0);
  result:=true;

  OnlineUpdateForm.DownloadPanel.Visible:=false;
end;

{$R *.DFM}

procedure TOnlineUpdateForm.SearchUpdatesClick(Sender: TObject);
var
  Stream      : TMemoryStream;
  StringList  : TStringList;
  root        : TXMLTag;
  Tag         : TXMLTag;
begin
  if not DirectoryExists(Directory+'downloads') then
    CreateDir(Directory+'downloads');

  Stream:=TMemoryStream.Create;
  if OfflineCheckBox.Checked then
  begin
    Stream.LoadFromFile(Directory+'downloads\updates.xml');
  end
  else
  begin
    if not DownloadFileFromURL(UpdateFile,Stream,DownloadProgress) then
    begin
      Application.MessageBox(PChar(DownloadError),PChar(EError),MB_ICONERROR);
      exit;
    end;

    Stream.SaveToFile(Directory+'downloads\updates.xml');
  end;

  StringList:=TStringList.Create;
  Stream.Position:=0;
  StringList.LoadFromStream(Stream);

  root:=TXMLTag.create(nil,'root');
  if not XMLParser.ParseXMLText(StringList.Text,root) then
  begin
    Application.MessageBox('Ung�ltige Updateinformationen',PChar(EError),MB_ICONERROR);
    exit;
  end;
  Tag:=root.Items.Tags['files'];
  if Tag=nil then
  begin
    Application.MessageBox('Ung�ltige Updateinformationen',PChar(EError),MB_ICONERROR);
  end
  else
  begin
    ParseAllDownloads(Tag);
    SearchUpdates.Enabled:=false;
  end;

  root.Free;
  Stream.Free;
  StringList.Free;
end;

procedure TOnlineUpdateForm.DownloadProgress(Status: String; AktBytesRead, TotallyBytes, BytesPerSecond: Integer);
begin
  Statusbar1.SimpleText:=Status;
  DownloadProgressBar.Max:=TotallyBytes;
  DownloadProgressBar.Position:=AktBytesRead;

  FileSizeLabel.Caption:=IntToKB(TotallyBytes);
  ReceivedLabel.Caption:=IntToKB(AktBytesRead);

  BytesPerSecondLabel.Caption:=IntToKB(BytesPerSecond)+'/s';

  if BytesPerSecond>0 then
    RemainingLabel.Caption:=FormatDateTime('hh:mm:ss',((TotallyBytes-AktBytesRead)/BytesPerSecond)/(60*60*24))
  else
    RemainingLabel.Caption:='';
end;

procedure TOnlineUpdateForm.ParseAllDownloads(Tag: TXMLTag);
var
  Dummy: Integer;
  Reg  : TRegistry;

  procedure parseDownload(Tag: TXMLTag);
  var
    updateInfo: TXMLTag;
    Dummy     : Integer;
  begin
    SetLength(fUpdates,length(fUpdates)+1);
    with fUpdates[high(fUpdates)] do
    begin
      ID:=StrToInt(Tag.Attributes.Attributes['id']);
//      Enabled:=Tag.Attributes.Attributes['canchoose']<>'false';
      Test:=Tag.Attributes.Attributes['testversion']='true';
      Important:=Tag.Attributes.Attributes['important']='true';
      if Tag.Attributes.Exists('icon') then
        Icon:=StrToInt(Tag.Attributes.Attributes['icon'])
      else
        Icon:=-1;

      Title:=Tag.Items.Tags['title'].Value;
      Size:=StrToInt(Tag.Items.Tags['size'].Value);
      Date:=Tag.Items.Tags['date'].Value;
      Description:=Tag.Items.Tags['description'].Value;
      Package:=Tag.Items.Tags['package'].Value;
      action:=Tag.Items.Tags['action'].Value;

      reference:=-1;

      if Tag.Items.Tags['references']<>nil then
        references:=Tag.Items.Tags['references'].Value;

      if Tag.Items.Tags['homepage']<>nil then
        homepage:=Tag.Items.Tags['homepage'].Value;

      UpdateInfo:=Tag.Items.Tags['updateinfo'];
      URL:=UpdateInfo.Items.Tags['url'].Value;
      Version:=UpdateInfo.Items.Tags['version'].Value;

      Update:=Reg.ValueExists(Package);
      if Update then
        Installed:=Reg.ReadString(Package)=Version;

      for Dummy:=0 to UpdateInfo.Items.Count-1 do
      begin
        if UpdateInfo.Items[Dummy] is TXMLTag then
        begin
          if TXMLTag(UpdateInfo.Items[Dummy]).Caption='needversion' then
          begin
            if NeedVersions<>'' then
              needVersions:=NeedVersions+',';

            if TXMLTag(UpdateInfo.Items[Dummy]).Attributes.Attributes['optional']='true' then
              NeedVersions:=NeedVersions+'"-'+TXMLTag(UpdateInfo.Items[Dummy]).Attributes.Attributes['package']
            else
              NeedVersions:=NeedVersions+'"'+TXMLTag(UpdateInfo.Items[Dummy]).Attributes.Attributes['package'];
            NeedVersions:=NeedVersions+':'+TXMLTag(UpdateInfo.Items[Dummy]).Value+'"';
          end;
	end;
      end;
    end;
  end;

begin
  Reg:=OpenXForceRegistry('Updates');
  SetLength(fUpdates,0);
  UpdateList.Items.Clear;
  for Dummy:=0 to Tag.Items.Count-1 do
  begin
    if Tag.Items[Dummy] is TXMLTag then
    begin
      if TXMLTag(Tag.Items[Dummy]).Caption='file' then
        parseDownload(TXMLTag(Tag.Items[Dummy]));
    end;
  end;
  FillUpdateList;
  Reg.Free;
end;

procedure TOnlineUpdateForm.UpdateListDrawItem(Control: TWinControl;
  Index: Integer; Rect: TRect; State: TOwnerDrawState);
var
  Text: String;
  Item : Integer;
begin
  Item:=Index;
  Index:=StrToIntDef(UpdateList.Items[Index],-1);
  with UpdateList.Canvas do
  begin
    FillRect(Rect);

    if Index=-1 then
    begin
      Font.Color:=clWindowText;
      Font.Style:=[fsBold];
      TextOut(Rect.Left+2,Rect.Top+4,UpdateList.Items[Item]);
      exit;
    end;
    
    if OfflineCheckBox.Checked then
      if not FileExists(Directory+'downloads\'+IntToStr(fUpdates[Index].ID)) then
        Font.Color:=clGrayText;

    if fUpdates[Index].Icon<>-1 then
      IconList.Draw(UpdateList.Canvas,Rect.Left+20,Rect.Top+2,fUpdates[Index].Icon);

    if fUpdates[Index].Update then
      IconList.Draw(UpdateList.Canvas,Rect.Left+2,Rect.Top+2,5);

    if fUpdates[Index].Important then
    begin
      FillRect(Classes.Rect(Rect.Left+2,Rect.Top+2,Rect.Left+18,Rect.Top+18));
      Draw(Rect.Left+2,Rect.Top+2,fError);
    end;

    if fUpdates[Index].Installed then
      FillRect(Classes.Rect(Rect.Left+2,Rect.Top+2,Rect.Left+18,Rect.Top+18));
      
    TextOut(Rect.Left+40,Rect.Top+4,fUpdates[Index].Title);
    TextOut(Rect.Right-180,Rect.Top+4,fUpdates[Index].Date);
    Font.Style:=[fsBold];
    Text:=IntToKB(fUpdates[Index].Size);
    TextOut(Rect.Right-3-TextWidth(Text),Rect.Top+4,Text);
    Font.Style:=[];
  end;
end;

procedure TOnlineUpdateForm.UpdateListClick(Sender: TObject);
var
  Index: Integer;
begin
  if UpdateList.ItemIndex<>-1 then
  begin
    Index:=StrToIntDef(UpdateList.Items[UpdateList.ItemIndex],-1);

    if Index<>-1 then
    begin
      descriptionLabel.Caption:=fUpdates[Index].Description;
      HomePageButton.Enabled:=fUpdates[Index].HomePage<>'';
    end
    else
    begin
      descriptionLabel.Caption:='';
      HomePageButton.Enabled:=false;
    end;
  end;
end;

procedure TOnlineUpdateForm.InstallUpdatesClick(Sender: TObject);
var
  Dummy   : Integer;
  Index   : Integer;
begin
  InstallUpdates.Enabled:=false;
  SaveDownloads.Enabled:=false;
  UpdateList.Enabled:=false;
  OfflineCheckBox.Enabled:=false;

  for Dummy:=0 to UpdateList.Items.Count-1 do
  begin
    if UpdateList.Checked[Dummy] then
    begin
      Index:=StrToInt(UpdateList.Items[Dummy]);
      if not DownloadFile(Index) then
        exit;
    end;
  end;
  Protokoll.Memo1.Lines:=UpdateForm.Protokoll;
  UpdateForm.Protokoll.Clear;
  Protokoll.ShowModal;
  FillUpdateList;

  UpdateList.Enabled:=true;
  SaveDownloads.Enabled:=true;
  HomepageButton.Enabled:=false;
end;

function TOnlineUpdateForm.DownloadFile(Index: Integer): boolean;
var
  Stream   : TMemoryStream;
  Reg      : TRegistry;
  SavedFile: String;
begin
  result:=true;

  SavedFile:=Directory+'downloads\'+IntToStr(fUpdates[Index].ID);

  // Pr�fen, ob datei heruntergeladen wird (wenn die Datei nicht exisitert oder die gr��e sich unterscheidet)
  if (not (FileExists(SavedFile)) or (file_utils_GetFileSize(SavedFile)<>fUpdates[Index].Size)) then
  begin
    if OfflineCheckBox.Checked then
    begin
      Application.MessageBox(PChar('Installationspaket f�r Update '+fUpdates[Index].Title+' offline nicht verf�gbar'),PChar(EError),MB_ICONERROR);
      exit;
    end;

    Stream:=TMemoryStream.Create;
    if DownloadFileFromURL(fUpdates[Index].URL,Stream,DownloadProgress) then
    begin
      Stream.SaveToFile(SavedFile);
    end
    else
    begin
      Stream.Free;
      result:=false;
      Application.MessageBox('Download des Installationspaketes fehlgeschlagen',PChar(EError),MB_ICONERROR);
      exit;
    end;
    Stream.Free;
  end;

  if fUpdates[Index].action='update' then
  begin
    InstallBar.Position:=0;
    InstallPanel.Visible:=true;
    InstallPanel.Refresh;
    UpdateForm.OnInstallProgress:=ProgressUpdate;
    if UpdateForm.PerformPatch(SavedFile) then
    begin
      Reg:=OpenXForceRegistry('updates');
      Reg.WriteString(fUpdates[Index].package,fUpdates[Index].Version);
      Reg.Free;
    end
    else
    begin
      Application.MessageBox(PChar(Format('Das Update %s ist fehlgeschlagen. Es sollten alle laufenden Anwendungen beendet werden.',[fUpdates[Index].Title])),PChar(EError),MB_ICONERROR);
    end;
    UpdateForm.OnInstallProgress:=nil;
    InstallPanel.Visible:=false;
    fUpdates[Index].Installed:=true;
  end;

  if not SaveDownloads.Checked then
    DeleteFile(SavedFile);
end;

procedure TOnlineUpdateForm.UpdateListClickCheck(Sender: TObject);
var
  Index  : Integer;

  function IsUpdateSelected: boolean;
  var
    Dummy: Integer;
  begin
    result:=false;
    for Dummy:=0 to UpdateList.Items.Count-1 do
    begin
      if UpdateList.Checked[Dummy] then
      begin
        result:=true;
        exit;
      end;
    end;
  end;

  procedure CheckReferences(Index: Integer; Checked: Boolean);
  begin
    if fUpdates[Index].reference<>-1 then
    begin
      UpdateList.Checked[fUpdates[Index].reference]:=Checked;
      CheckReferences(StrToInt(UpdateList.Items[fUpdates[Index].reference]),Checked);
    end;
  end;

begin
  if UpdateList.ItemIndex=-1 then
    exit;

  Index:=StrToIntDef(UpdateList.Items[UpdateList.ItemIndex],-1);
  if Index=-1 then
    exit;
    
  CheckReferences(Index,UpdateList.Checked[UpdateList.ItemIndex]);

  RefreshSize;

  InstallUpdates.Enabled:=IsUpdateSelected;
end;

procedure TOnlineUpdateForm.FillUpdateList;
var
  Dummy   : Integer;
  Dummy2  : Integer;
  List    : TStringList;
  OK      : Boolean;
  Text    : String;
  Group   : String;
  Reg     : TRegistry;
  Ver     : String;
  ThisVer : String;
  Versions: TStringArray;
  refs    : TStringArray;
  Index   : Integer;

  function CheckVersions(Versions: TStringArray; Version: String): Boolean;
  var
    Dummy: Integer;
  begin
    for Dummy:=0 to high(Versions) do
    begin
      if Matchstrings(Version,Versions[Dummy]) then
      begin
        result:=true;
        exit;
      end;
    end;
    result:=false;
  end;
begin
  UpdateList.Items.Clear;
  List:=TStringList.Create;
  Reg:=OpenXForceRegistry('updates');

  UpdateList.Items.Add(CAvaible);
  UpdateList.Header[0]:=true;

  for Dummy:=0 to high(fUpdates) do
  begin
    List.Commatext:=fUpdates[Dummy].NeedVersions;

    // Pr�fen, ob diese Version bereits installiert ist
    if Reg.ValueExists(fUpdates[Dummy].package) then
    begin
      if Reg.ReadString(fUpdates[Dummy].package)=fUpdates[Dummy].Version then
        continue;
    end;

    OK:=true;

    // Pr�fen, ob die ben�tigte Versionen installiert ist
    Group:='';
    for Dummy2:=0 to List.Count-1 do
    begin
      Text:=List[Dummy2];
      if Pos(':',Text)>0 then
      begin
        Group:=Copy(Text,1,Pos(':',Text)-1);
        ThisVer:=Copy(Text,Pos(':',Text)+1,10000);
        if not OK then
          break;
        OK:=false;
      end
      else
        ThisVer:=Text;

      if Group[1]='-' then
      begin
        Group:=Copy(Group,2,length(Group));
        if not Reg.ValueExists(Group) then
        begin
          OK:=true;
          break;
        end;
      end;

      if Reg.ValueExists(Group) then
        Ver:=Reg.ReadString(Group)
      else
        Ver:='';

      Versions:=string_utils_explode(',',ThisVer);
      if CheckVersions(Versions,Ver) then
//    if string_utils_inArray(Ver,Versions) then
        OK:=true;
    end;

    if not OK then
    begin
      // Die ben�tigte Version ist nicht installiert => Pr�fen ob die Referenzen
      // installiert werden k�nnen
      if fUpdates[Dummy].references<>'' then
      begin
        refs:=string_utils_explode(' or ',fUpdates[Dummy].references);
        for Dummy2:=0 to high(refs) do
        begin
          Index:=FindListIndexOfUpdate(StrToInt(refs[Dummy2]));
          if Index<>-1 then
          begin
            fUpdates[Dummy].reference:=Index;
            UpdateList.ItemEnabled[Index]:=false;
            OK:=true;
            break;
          end;
        end;
      end;
    end;

    if OK then
      UpdateList.Items.Add(IntToStr(Dummy));
//      UpdateList.ItemEnabled[UpdateList.Items.Count-1]:=fUpdates[Dummy].Enabled;
  end;

  UpdateList.Items.Add(CInstalled);
  UpdateList.Header[UpdateList.Items.Count-1]:=true;

  for Dummy:=0 to high(fUpdates) do
  begin
    if fUpdates[Dummy].Installed then
    begin
      UpdateList.Items.Add(IntToStr(Dummy));
      UpdateList.ItemEnabled[UpdateList.Items.Count-1]:=false;
    end;
  end;
  Reg.Free;
  List.Free;
end;

procedure TOnlineUpdateForm.RefreshSize;
var
  Dummy: Integer;
  Size : Integer;
  Index: Integer;
begin
  Size:=0;
  for Dummy:=0 to UpdateList.Items.Count-1 do
  begin
    if UpdateList.Checked[Dummy] then
    begin
      Index:=StrToInt(UpdateList.Items[Dummy]);
      inc(Size,fUpdates[Index].Size);
    end;
  end;
  CompleteSize.Caption:=IntToKB(Size);
end;

procedure TOnlineUpdateForm.FormShow(Sender: TObject);
begin
  RefreshSize;
  OfflineCheckBox.Enabled:=FileExists(Directory+'downloads\updates.xml');
end;

procedure TOnlineUpdateForm.ProgressUpdate(FileNr,FileCount: Integer);
begin
  InstallBar.Max:=FileCount;
  InstallBar.Position:=FileNr;
  InstallBar.Repaint;
end;

procedure TOnlineUpdateForm.FormCreate(Sender: TObject);

  function CreateIcon(MessageType: TMsgDlgType): TBitmap;
  var
    Icon: TIcon;
    Temp: TBitmap;
  begin
    Icon:=TIcon.Create;
    case MessageType of
      mtError       : Icon.Handle:=LoadIcon(0,IDI_ERROR);
      mtWarning     : Icon.Handle:=LoadIcon(0,IDI_WARNING);
      mtInformation : Icon.Handle:=LoadIcon(0,IDI_INFORMATION);
    end;
    Temp:=TBitmap.Create;
    Temp.Width:=Icon.Width;
    Temp.Height:=Icon.Height;
    Temp.Canvas.Brush.Color:=clWhite;
    Temp.Canvas.FillRect(Rect(0,0,Temp.Width,Temp.Height));
    Temp.Canvas.Draw(0,0,Icon);
    Icon.Free;
    result:=TBitmap.Create;
    result.Width:=Iconsize;
    result.Height:=Iconsize;
    result.Canvas.StretchDraw(Rect(0,0,Iconsize,Iconsize),Temp);
    result.TransparentColor:=result.Canvas.Pixels[0,0];
    result.Transparent:=true;
    Temp.Free;
  end;

begin
  fError:=CreateIcon(mtWarning);
end;

procedure TOnlineUpdateForm.TestversionClick(Sender: TObject);
begin
  FillUpdateList;
end;

procedure TOnlineUpdateForm.Button1Click(Sender: TObject);
begin
  ShowMessage(OfflineInfo);
end;

function TOnlineUpdateForm.FindListIndexOfUpdate(
  UpdateIndex: Integer): Integer;
var
  Dummy : Integer;
  Index : Integer;
begin
  for Dummy:=0 to UpdateList.Items.Count-1 do
  begin
    Index:=StrToIntDef(UpdateList.Items[Dummy],-1);
    if (Index<>-1) and (fUpdates[Index].id=UpdateIndex) then
    begin
      result:=Dummy;
      exit;
    end;
  end;
  result:=-1;
end;

procedure TOnlineUpdateForm.HomepageButtonClick(Sender: TObject);
var
  Index: Integer;
begin
  Index:=StrToInt(UpdateList.Items[UpdateList.ItemIndex]);
  ShellExecute(Application.Handle,'open',PChar(fUpdates[Index].HomePage),nil,nil,SW_SHOWNORMAL);
end;

procedure TOnlineUpdateForm.OfflineCheckBoxClick(Sender: TObject);
begin
  UpdateList.Refresh;
end;

end.
