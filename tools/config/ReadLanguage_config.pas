unit readlanguage_config;

interface

uses Classes, SysUtils, Forms, Windows, Dialogs;

function ReadLanguageFile(language: String): boolean;

implementation

uses
  frmMain, frmUpdate, frmOnline;

var
  StringList: TStringList;
  MissingStrings : String;

function GetValue(Name: String): String;
begin
  if StringList.IndexOfName(Name)=-1 then
  begin
    MissingStrings:=MissingStrings+Name+#13#10;
    result:='Missing ('+Name+')';
  end
  else
    result:=StringList.Values[Name];
end;

function ReadLanguageFile(language: String): boolean;
var
  Dummy: Integer;
  Filename : String;
begin
  MissingStrings:='';
  StringList:=TStringList.Create;
  FileName:=IncludeTrailingBackslash(ExtractFilePath(Application.ExeName))+'language\config\'+language+'.dat';
  if not FileExists(FileName) then
  begin
    result:=false;
    exit;
  end
  else
  begin
    StringList.LoadFromFile(FileName);
    result:=true;
  end;

  {$I 'readlanguage.inc'}

  if MissingStrings<>'' then
  begin
    MessageBeep(MB_ICONERROR);
    MessageDlg(Format('Some Strings couldn''t find in language file: '#13#10#13#10'%s',[MissingStrings]),mtError,[mbOK],0);
  end;

  StringList.Free;
end;

end.

