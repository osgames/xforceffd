unit frmlanguage;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, Registry;

type
  TLanguageForm = class(TForm)
    Label1: TLabel;
    ListBox1: TListBox;
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    procedure FormCreate(Sender: TObject);
    procedure BitBtn2Click(Sender: TObject);
    procedure ListBox1DrawItem(Control: TWinControl; Index: Integer;
      Rect: TRect; State: TOwnerDrawState);
    procedure ListBox1Click(Sender: TObject);
    procedure ListBox1DblClick(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    fAktLanguage: String;
    { Private-Deklarationen }
  public
    { Public-Deklarationen }
  end;

var
  LanguageForm: TLanguageForm;

implementation

uses
  KD4Utils;

{$R *.DFM}

procedure TLanguageForm.FormCreate(Sender: TObject);
var
  Directory : String;
  Data      : TSearchRec;
  List      : TStringList;
  Text      : String;
begin
  Directory:=ExtractFilePath(Application.ExeName);
  // Sprachdateien einlesen
  if FindFirst(IncludeTrailingBackslash(Directory)+'language\config\*.dat',faAnyFile,Data)<>0 then
  begin
    MessageDlg('No language Files found',mtWarning,[mbOK],0);
    exit;
  end;
  List:=TStringList.Create;
  repeat
    List.LoadFromFile(IncludeTrailingBackslash(Directory)+'language\config\'+data.Name);
    Text:=List.Values['Language']+' ('+Data.Name+')';
    ListBox1.Items.Add(Text);
    if fAktLanguage=Data.Name then
      ListBox1.ItemIndex:=ListBox1.Items.Count-1;
  until FindNext(Data)<>0;
  List.Free;
  FindClose(Data);
end;

procedure TLanguageForm.BitBtn2Click(Sender: TObject);
begin
  Close;
end;

procedure TLanguageForm.ListBox1DrawItem(Control: TWinControl; Index: Integer;
  Rect: TRect; State: TOwnerDrawState);
begin
  ListBox1.Canvas.FillRect(Rect);
  if Pos(fAktLanguage,ListBOx1.Items[Index])<>0 then
    ListBox1.Canvas.Font.Style:=[fsBold]
  else
    ListBox1.Canvas.Font.Style:=[];

  ListBox1.Canvas.TextOut(Rect.Left+2,Rect.Top+2,ListBox1.Items[Index]);
end;

procedure TLanguageForm.ListBox1Click(Sender: TObject);
begin
  BitBtn1.Enabled:=ListBox1.ItemIndex<>-1;
end;

procedure TLanguageForm.ListBox1DblClick(Sender: TObject);
begin
  BitBtn1.Click;
end;

procedure TLanguageForm.BitBtn1Click(Sender: TObject);
var
  language : String;
  Reg      : TRegistry;
begin
  Reg:=OpenXForceRegistry;

  language:=ListBox1.Items[ListBox1.ItemIndex];
  Reg.WriteString('Language',Copy(language,pos('(',language)+1,length(language)-pos('(',language)-5));
  ModalResult:=mrOK;

  Reg.Free;
end;

procedure TLanguageForm.FormShow(Sender: TObject);
var
  Reg: TRegistry;
begin
  Reg:=OpenXForceRegistry('',false);

  if Reg<>nil then
  begin
    fAktLanguage:=Reg.ReadString('Language');
    Reg.Free;
  end;
end;

end.
