unit Logging;

{
  Inno Setup
  Copyright (C) 1997-2004 Jordan Russell
  Portions by Martijn Laan
  For conditions of distribution and use, see LICENSE.TXT.

  Logging functions

  $jrsoftware: issrc/Projects/Logging.pas,v 1.7 2004/12/15 09:23:12 jr Exp $
}

interface

procedure Log(const S: String);
procedure LogFmt(const S: String; const Args: array of const);
procedure StartLogging(const Prefix: String);
procedure StartLoggingWithFixedFilename(const Filename: String);

const
  SYesNo: array[Boolean] of String = ('No', 'Yes');

implementation

uses
  Windows, SysUtils, CmnFunc2, FileClass;

var
  LogFile: TFile;

procedure StartLogging(const Prefix: String);
var
  Dir, DateStr, Filename: String;
  I: Cardinal;
  ST: TSystemTime;
  F: TFile;
begin
  if Assigned(LogFile) then
    Exit;  { logging was already started }
  Dir := GetTempDir;
  GetLocalTime(ST);
  DateStr := Format('%.4u-%.2u-%.2u', [ST.wYear, ST.wMonth, ST.wDay]);
  I := 1;
  while True do begin
    Filename := Dir + Format('%s Log %s #%.3u.txt', [Prefix, DateStr, I]);
    if not FileOrDirExists(Filename) then begin
      F := nil;
      try
        F := TFile.Create(Filename, fdCreateNew, faWrite, fsRead);
      except
        on E: EFileError do begin
          { Don't propogate ERROR_FILE_EXISTS errors; just try again.
            (Yes, we already checked if the file existed first, but this helps
            to make it race-proof.) }
          if E.ErrorCode <> ERROR_FILE_EXISTS then
            raise;
        end;
      end;
      if Assigned(F) then begin
        LogFile := F;
        Break;
      end;
    end;
    Inc(I);
  end;
  Log('Log opened.');
end;

procedure StartLoggingWithFixedFilename(const Filename: String);
begin
  if Assigned(LogFile) then
    Exit;  { logging was already started }
  LogFile := TFile.Create(Filename, fdCreateAlways, faWrite, fsRead);
  Log('Log opened.');
end;

procedure Log(const S: String);

  procedure WriteStr(const S: String);
  begin
    LogFile.WriteBuffer(Pointer(S)^, Length(S));
  end;

var
  ST: TSystemTime;
  LineStart, I: Integer;
begin
  if Assigned(LogFile) then begin
    GetLocalTime(ST);
    try
      WriteStr(Format('%.4u-%.2u-%.2u %.2u:%.2u:%.2u   ',
        [ST.wYear, ST.wMonth, ST.wDay, ST.wHour, ST.wMinute, ST.wSecond]));
      LineStart := 1;
      { Lines except for last line }
      for I := 1 to Length(S) do begin
        if S[I] = #10 then begin
          LogFile.WriteBuffer(S[LineStart], I - LineStart + 1);
          LineStart := I + 1;
          { Indent }
          WriteStr('                      ');
        end;
      end;
      { Last line }
      if LineStart <= Length(S) then
        LogFile.WriteBuffer(S[LineStart], Length(S) - LineStart + 1);
      WriteStr(#13#10);
    except
      { Failed to write? Close the file and don't log anything further. }
      try
        FreeAndNil(LogFile);
      except
      end;
    end;
  end;
//  if Debugging then
//    DebugNotifyLogMessage(S);
end;

procedure LogFmt(const S: String; const Args: array of const);
begin
//  if Assigned(LogFile) or Debugging then
//    Log(Format(S, Args));
end;

initialization
finalization
  if Assigned(LogFile) then begin
    Log('Log closed.');
    FreeAndNil(LogFile);
  end;
end.
