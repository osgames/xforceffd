unit frmMain;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Registry, Defines, readlanguage_config, KD4Utils, XPMan;

type
  TMainForm = class(TForm)
    languageButton: TButton;
    LanguageLabel: TLabel;
    BitModeLabel: TLabel;
    Label1: TLabel;
    Versionlabel: TLabel;
    Updatebutton: TButton;
    OnlineButton: TButton;
    BitmapModeBox: TGroupBox;
    Bitmode16: TRadioButton;
    Bitmode32: TRadioButton;
    ModusBox: TGroupBox;
    FullscreenMode: TRadioButton;
    WindowMode: TRadioButton;
    DirectoryEdit: TEdit;
    XPManifest: TXPManifest;
    procedure FormActivate(Sender: TObject);
    procedure languageButtonClick(Sender: TObject);
    procedure BitmapModeChange(Sender: TObject);
    procedure UpdatebuttonClick(Sender: TObject);
    procedure OnlineButtonClick(Sender: TObject);
    procedure ChangeModeClick(Sender: TObject);
  private
    procedure ReadBitMode;
    procedure ReadVersion;
    { Private-Deklarationen }
  public
    { Public-Deklarationen }
  end;

var
  MainForm: TMainForm;
  Version   : Integer;
  Language  : String;
  Directory : String;
  EError    : String;

  EWrongResolution : String;
  EWrongBitDepth   : String;

const
  ConfigVersion = '112';

implementation

uses frmlanguage, frmUpdate, frmOnline;

{$R *.DFM}

procedure TMainForm.FormActivate(Sender: TObject);
var
  Reg: TRegistry;
begin
  Reg:=OpenXForceRegistry('',false);
  if Reg=nil then
  begin
    ShowMessage('X-Force ist nicht installiert');
    Application.Terminate;
    exit;
  end;

  Language:=Reg.ReadString('Language');
  readLanguageFile(Language);
  Directory:=IncludeTrailingBackslash(Reg.ReadString('InstallDir'));
  Caption:=Caption+' V'+Copy(ConfigVersion,1,1)+'.'+Copy(ConfigVersion,2,2);

  ReadBitMode;
  ReadVersion;
  LanguageLabel.Caption:=Language;

  DirectoryEdit.Text:=Directory;

  Reg.Free;
end;

procedure TMainForm.languageButtonClick(Sender: TObject);
var
  Reg: TRegistry;
begin
  if LanguageForm.ShowModal=mrOK then
  begin
    Reg:=OpenXForceRegistry;
    Language:=Reg.ReadString('Language');
    readLanguageFile(Language);
    LanguageLabel.Caption:=Language;
    ReadBitMode;

    Reg.Free;
  end;
end;

procedure TMainForm.ReadBitMode;
var
  Mode24BIt : boolean;
  Window    : Boolean;
  Reg       : TRegistry;
begin
  Reg:=OpenXForceRegistry('Settings');
  Mode24Bit:=false;
  if Reg.ValueExists('TrueColor') then
    Mode24Bit:=Reg.ReadBool('TrueColor');

  if Mode24Bit then
    Bitmode32.Checked:=true
  else
    Bitmode16.Checked:=true;

  Window:=false;
  if Reg.ValueExists('WindowedMode') then
    Window:=Reg.ReadBool('WindowedMode');

  if Window then
    WindowMode.Checked:=true
  else
    FullscreenMode.Checked:=true;

  Reg.Free;
end;

procedure TMainForm.BitmapModeChange(Sender: TObject);
var
  Mode24BIt : boolean;
  Reg       : TRegistry;
begin
  Reg:=OpenXForceRegistry('Settings');

  Mode24Bit:=Bitmode32.Checked;
  Reg.WriteBool('TrueColor',Mode24Bit);

  Reg.Free;
end;

procedure TMainForm.ReadVersion;
var
  Reg: TRegistry;
begin
  Reg:=OpenXForceRegistry();

  Version:=Reg.ReadInteger('Version');

  Reg.OpenKey('Updates',false);
  Versionlabel.Caption:='V0.'+IntToStr(Version)+' ['+Reg.ReadString('XFORCE-MAIN')+']';

  Reg.Free;
end;

procedure TMainForm.UpdatebuttonClick(Sender: TObject);
begin
  UpdateForm.ShowModal;
  ReadVersion;
end;

procedure TMainForm.OnlineButtonClick(Sender: TObject);
begin
  OnlineUpdateForm.ShowModal;
end;

procedure TMainForm.ChangeModeClick(Sender: TObject);
var
  Window : boolean;
  Bitmode: Integer;
  Reg    : TRegistry;
begin
  Reg:=OpenXForceRegistry('Settings');

  if WindowMode.Checked then
  begin
    if (Screen.Width<=ScreenWidth) or (Screen.Height<=ScreenHeight)  then
    begin
      MessageDlg(EWrongResolution,mtError,[mbOK],0);
      FullscreenMode.Checked:=true;
    end;

    BitMode:=GetDeviceCaps(GetDC(GetActiveWindow),BITSPIXEL);
    if (BitMode<>16) and (BitMode<>32) then
    begin
      MessageDlg(EWrongBitDepth,mtError,[mbOK],0);
      FullscreenMode.Checked:=true;
    end;
  end;
  Window:=WindowMode.Checked;
  Reg.WriteBool('WindowedMode',Window);

  Bitmode16.Enabled:=not Window;
  Bitmode32.Enabled:=not Window;

  Reg.Free;
end;

end.
