unit frmChooseSkript;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Buttons;

type
  TChooseSkript = class(TForm)
    ListBox1: TListBox;
    Label1: TLabel;
    OKButton: TBitBtn;
    BitBtn2: TBitBtn;
    procedure ListBox1DrawItem(Control: TWinControl; Index: Integer;
      Rect: TRect; State: TOwnerDrawState);
    procedure ListBox1DblClick(Sender: TObject);
  private
    fArchivName : String;
    procedure SetArchivName(const Value: String);
    function GetSkript: String;
    { Private-Deklarationen }
  public
    { Public-Deklarationen }
    property ArchivName: String read fArchivName write SetArchivName;
    property Skript    : String read GetSkript;
  end;

var
  ChooseSkript: TChooseSkript;

implementation

{$R *.dfm}

uses
  ArchivFile, ExtRecord, record_utils, gameset_api, XForce_Types, string_utils;

procedure TChooseSkript.SetArchivName(const Value: String);
var
  Archiv   : TArchivFile;
  Head     : TSetHeader;
  Skripts  : TRecordArray;
  Dummy    : Integer;
begin
  Archiv:=TArchivFile.Create;
  Archiv.OpenArchiv(Value,true);
  try
    gameset_api_ReadHeader(Archiv,Head);
  except
    Archiv.Free;
    raise;
  end;

  gameset_api_LoadSkripts(Archiv,Skripts);

  ListBox1.Items.Clear;
  for Dummy:=0 to high(Skripts) do
  begin
    ListBox1.Items.Add(IntToHex(Skripts[Dummy].GetCardinal('ID'),8)+' ('+string_utils_GetLanguageString('deutsch',Skripts[Dummy].GetString('Name'))+')');
  end;

  record_utils_ClearArray(Skripts);

  OKButton.Enabled:=ListBox1.Items.Count>0;

  Archiv.Free;
end;

procedure TChooseSkript.ListBox1DrawItem(Control: TWinControl;
  Index: Integer; Rect: TRect; State: TOwnerDrawState);
var
  Item  : String;
  Parts : TStringArray;
begin
  Item:=ListBox1.Items[Index];
  Parts:=string_utils_explode('(',Item);
  with ListBox1.Canvas do
  begin
    FillRect(Rect);
    TextOut(Rect.Left+2,Rect.Top+2,Copy(Parts[1],1,length(Parts[1])-1));
  end;
end;

procedure TChooseSkript.ListBox1DblClick(Sender: TObject);
begin
  OKButton.Click;
end;

function TChooseSkript.GetSkript: String;
begin
  result:='';
  if ListBox1.ItemIndex<>-1 then
    result:=ListBox1.Items[ListBox1.ItemIndex];
end;

end.
