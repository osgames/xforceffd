program MEdit;

uses
  SysUtils,
  Forms,
  Windows,
  frmMain in 'frmMain.pas' {EditWindow},
  frmChild in 'frmChild.pas' {EditChild},
  frmOptions in 'frmOptions.pas' {OptionsWindow},
  frmEvents in 'frmEvents.pas' {EventWindow},
  frmSearch in 'frmSearch.pas' {SearchForm},
  frmAbout in 'frmAbout.pas' {AboutBox},
  frmNew in 'frmNew.pas' {NewSkriptForm},
  frmMap in 'frmMap.pas' {ShowMapForm},
  register_scripttypes in '..\..\game\source\units\register_scripttypes.pas',
  script_api in '..\..\game\source\api\script_api.pas',
  script_utils in '..\..\game\source\utils\script_utils.pas',
  script_propertyhelpers in '..\..\game\source\Units\script_propertyhelpers.pas',
  frmChooseSkript in 'frmChooseSkript.pas' {ChooseSkript},
  AutoCompleteHelper in 'AutoCompleteHelper.pas',
  api_utils in '..\..\game\source\utils\api_utils.pas';

{$R *.RES}

begin
  Application.Initialize;
  SetCurrentDirectory(PChar(ExtractFilePath(Application.ExeName)));
  Application.Title := 'Missions-Editor';
  Application.HelpFile := '';
  Application.CreateForm(TEditWindow, EditWindow);
  Application.CreateForm(TEventWindow, EventWindow);
  Application.CreateForm(TSearchForm, SearchForm);
  Application.CreateForm(TAboutBox, AboutBox);
  Application.CreateForm(TNewSkriptForm, NewSkriptForm);
  Application.CreateForm(TShowMapForm, ShowMapForm);
  Application.CreateForm(TChooseSkript, ChooseSkript);
  Application.Run;
end.
