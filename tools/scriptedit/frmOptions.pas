unit frmOptions;

{$I SynEdit.inc}

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, ComCtrls, Mask,  ArchivFile, Koding, XForce_types,
  SynEdit, ExtCtrls, ShlObj, JvCombobox, JvColorCombo, JvSpin, JvMaskEdit,
  JvExMask, JvExStdCtrls, JvToolEdit;

type
  TOptionsWindow = class(TForm)                                                
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    TabSheet4: TTabSheet;
    FontFrame: TGroupBox;
    FontNameLabel: TLabel;
    Sizelabel: TLabel;
    SizeComboBox: TComboBox;
    Label2: TLabel;
    Label3: TLabel;
    AttributeList: TListBox;
    PreviewHighlight: TSynEdit;
    TextDefault: TCheckBox;
    BackDefault: TCheckBox;
    StyleBox: TGroupBox;
    Bold: TCheckBox;
    Italic: TCheckBox;
    Underline: TCheckBox;
    StrikeOut: TCheckBox;
    Options: TGroupBox;
    AutoSave: TCheckBox;
    Label4: TLabel;
    GroupBox1: TGroupBox;
    Label1: TLabel;
    GamesetName: TLabel;
    AutoViewEvent: TCheckBox;
    SamplePanel: TPanel;
    GroupBox2: TGroupBox;
    LineNumbers: TCheckBox;
    RightEdge: TCheckBox;
    AutoIndent: TCheckBox;
    Gutter: TCheckBox;
    Panel1: TPanel;
    GroupUndo: TCheckBox;
    OKButton: TButton;
    CancelButton: TButton;
    DirectoryEdit: TEdit;
    BrowseButton: TButton;
    ColorFrame: TGroupBox;
    Label5: TLabel;
    TextSelector: TColorBox;
    BackgroundSelector: TColorBox;
    Label6: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    SelectedTextSelector: TColorBox;
    SelectedBackgroundSelector: TColorBox;
    MatchingBracketSelector: TColorBox;
    FontNameBox: TJvFontComboBox;
    RightColor: TColorBox;
    RightCol: TJvSpinEdit;
    Label10: TLabel;
    TextColor: TColorBox;
    BackColor: TColorBox;
    NewGameBox: TRadioButton;
    SaveGameBox: TRadioButton;
    GameSetEdit: TJvFilenameEdit;
    SaveGameEdit: TJvFilenameEdit;
    AvaibleSaveGame: TListBox;
    procedure GameSetEditChange(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FontNameBoxChange(Sender: TObject);
    procedure SizeComboBoxChange(Sender: TObject);
    procedure AttributeListClick(Sender: TObject);
    procedure ChangeColor(Sender: TObject);
    procedure OKButtonClick(Sender: TObject);
    procedure DirectoryEditButtonClick(Sender: TObject);
    procedure ChangeHighLight(Sender: TObject);
    procedure TextDefaultClick(Sender: TObject);
    procedure BackDefaultClick(Sender: TObject);
    procedure PreviewHighlightStatusChange(Sender: TObject;
      Changes: TSynStatusChanges);
    procedure GutterClick(Sender: TObject);
    procedure LineNumbersClick(Sender: TObject);
    procedure RightEdgeClick(Sender: TObject);
    procedure NewGameBoxClick(Sender: TObject);
    procedure SaveGameEditChange(Sender: TObject);
  private
    ChangeSyntax: boolean;
    { Private-Deklarationen }
  public
    { Public-Deklarationen }
  end;

var
  OptionsWindow: TOptionsWindow;

implementation

uses frmMain, SynEditHighlighter, savegame_api;
{$R *.DFM}

function GetNameOfSet(FileName: String): String;
var
  Archiv  : TArchivFile;
  m       : TMemoryStream;
  Version : Integer;
begin
  if not FileExists(FileName) then
    exit;
    
  m:=nil;
  Archiv:=TArchivFile.Create;
  try
    Archiv.OpenArchiv(FileName,true);
    Archiv.OpenRessource('Header');
    m:=TMemoryStream.Create;
    Decode(Archiv.Stream,m);
    m.Read(Version,SizeOf(Version));
    if (Version=HeadVersion) then
    begin
      result:=ReadString(m);
    end
    else
    begin
      raise EInvalidVersion.Create('');
    end;
  finally
    if m<>nil then m.Free;
    Archiv.Free;
  end;
end;

procedure TOptionsWindow.GameSetEditChange(Sender: TObject);
var
  Tem: String;
begin
  try
    GamesetName.Caption:=GetNameOfSet(GameSetEdit.Text);
  except
    GameSetName.Caption:='';
    Tem:=GameSetEdit.Text;
    GameSetEdit.Text:='';
    raise Exception.Create('Datei '+Tem+' ist kein g�ltiger X-Force Spielsatz');
  end;
end;

procedure TOptionsWindow.FormShow(Sender: TObject);
begin
  PageControl1.ActivePage:=TabSheet1;
  AutoSave.Checked:=frmMain.AutoSave;
  AutoViewEvent.Checked:=frmMain.AutoViewEvent;
  FontNameBox.FontName:=frmMain.DefaultFont;
  SamplePanel.Font.Name:=frmMain.DefaultFont;
  SizeComboBox.Text:=IntToStr(frmMain.DefaultSize);
  SamplePanel.Font.Size:=frmMain.DefaultSize;
  DirectoryEdit.Text:=frmMain.DefaultDir;
  TextSelector.Selected:=frmMain.FontColor;
  BackgroundSelector.Selected:=frmMain.BackColor;
  SelectedTextSelector.Selected:=frmMain.SelFontColor;
  SelectedBackgroundSelector.Selected:=frmMain.SelBackColor;
  OptionsWindow.MatchingBracketSelector.Selected:=MatchingKlam;
  AttributeList.ItemIndex:=0;
  AttributeListClick(Self);
  ChangeColor(Self);
  Gutter.Checked:=frmMain.GutterVisible;
  LineNumbers.Enabled:=Gutter.Checked;
  LineNumbers.Checked:=frmMain.LineNumbers;
  RightEdge.Checked:=frmMain.RightEdge>0;
  RightCol.Enabled:=RightEdge.Checked;
  RightCol.Value:=frmMain.RightEdge;
  RightColor.Selected:=frmMain.RightColor;
  GroupUndo.Checked:=frmMain.GroupUndo;
  AutoIndent.Checked:=frmMain.AutoIndent;

  NewGameBox.Checked:=not TestSaveGame;
  SaveGameBox.Checked:=TestSaveGame;

  SaveGameEdit.FileName:=SaveGameFile;
  SaveGameEditChange(SaveGameEdit);

  AvaibleSaveGame.ItemIndex:=AvaibleSaveGame.Items.IndexOf(SaveGameName);

  try
    GamesetName.Caption:=GetNameOfSet(frmMain.GameSet);
    GameSetEdit.Text:=frmMain.GameSet;
  except
    GameSetName.Caption:='';
    GameSetEdit.Text:='';
  end;
end;

procedure TOptionsWindow.FormCreate(Sender: TObject);
var
  Dummy: Integer;
begin
  PreviewHighlight.HighLighter:=EditWindow.PascalSyntax;

  for dummy:=8 to 14 do
    SizeComboBox.Items.Add(Inttostr(dummy));

  SizeComboBox.ItemIndex:=0;
  ChangeSyntax:=false;
  for dummy:=0 to EditWindow.PascalSyntax.AttrCount-1 do
    AttributeList.Items.Add(EditWindow.PascalSyntax.Attribute[Dummy].Name);
end;

procedure TOptionsWindow.FontNameBoxChange(Sender: TObject);
begin
  PreviewHighlight.Font.Name:=FontNameBox.FontName;
  SamplePanel.Font.Name:=FontNameBox.FontName;
end;

procedure TOptionsWindow.SizeComboBoxChange(Sender: TObject);
begin
  PreviewHighlight.Font.Size:=StrToInt(SizeComboBox.Text);
  SamplePanel.Font.Size:=StrToInt(SizeComboBox.Text);
end;

procedure TOptionsWindow.AttributeListClick(Sender: TObject);
var
  ForeCol,BackCol: TColor;
  Style: TFontStyles;
begin
  ChangeSyntax:=true;

  BackCol:=EditWindow.PascalSyntax.Attribute[AttributeList.ItemIndex].Background;
  ForeCol:=EditWindow.PascalSyntax.Attribute[AttributeList.ItemIndex].Foreground;

  Style:=EditWindow.PascalSyntax.Attribute[AttributeList.ItemIndex].Style;

  Bold.Checked:=(fsBold in Style);
  Italic.Checked:=(fsItalic in Style);
  Underline.Checked:=(fsUnderline in Style);
  StrikeOut.Checked:=(fsStrikeOut in Style);

  TextColor.Selected:=ForeCol;
  TextColor.Enabled:=not (ForeCol=clNone);
  TextDefault.Checked:=(ForeCol=clNone);
  if TextDefault.Checked then
    TextColor.Selected:=TextSelector.Selected;

  BackColor.Selected:=BackCol;
  BackColor.Enabled:=not (BackCol=clNone);
  BackDefault.Checked:=(BackCol=clNone);
  if BackDefault.Checked then
    BackColor.Selected:=BackgroundSelector.Selected;

  ChangeSyntax:=False;
end;

procedure TOptionsWindow.ChangeColor(Sender: TObject);
begin
  PreviewHighlight.Font.Color:=TextSelector.Selected;
  PreviewHighlight.Color:=BackgroundSelector.Selected;
  PreviewHighlight.SelectedColor.Foreground:=SelectedTextSelector.Selected;
  PreviewHighlight.SelectedColor.Background:=SelectedBackgroundSelector.Selected;
end;

procedure TOptionsWindow.OKButtonClick(Sender: TObject);
var
  Size: Integer;
begin
  try
    Size:=StrToInt(SizeComboBox.Text);
    if (Size<8) or (Size>72) then
    begin
      SizeComboBox.SetFocus;
      raise Exception.Create('Bitte einen Wert zwischen 8 und 72 eingeben.');
    end;
    if SaveGameBox.Checked and (AvaibleSaveGame.ItemIndex=-1) then
      raise Exception.Create('Bitte w�hle einen Spielstand aus');
  except
    ModalResult:=mrNone;
    raise;
  end;
end;

procedure TOptionsWindow.DirectoryEditButtonClick(Sender: TObject);
var
  BrowseInfo : TBrowseInfo;
  Dir        : PItemIDList;
  Path       : array[0..MAX_PATH] of char;
begin
  FillChar(BrowseInfo,SizeOf(BrowseInfo),#0);
  BrowseInfo.hwndOwner:=Handle;
//  BrowseInfo.pszDisplayName:=PChar(DirectoryEdit.Text);
  BrowseInfo.lpszTitle:=PChar('Verzeichnis w�hlen ...');
  BrowseInfo.ulFlags:=BIF_RETURNONLYFSDIRS;
  BrowseInfo.lpfn:=nil;
  BrowseInfo.lParam:=0;
  Dir:=SHBrowseForFolder(BrowseInfo);
  if Dir=nil then exit;
  SHGetPathFromIDList(Dir,Path);
  DirectoryEdit.Text:=Path;
end;

procedure TOptionsWindow.ChangeHighLight(Sender: TObject);
var
  NewStyle: TFontStyles;
  ForeCol,BackCol: TColor;
begin
  if ChangeSyntax then exit;
  NewStyle:=[];
  if Bold.Checked then Include(NewStyle,fsBold);
  if Italic.Checked then Include(NewStyle,fsItalic);
  if UnderLine.Checked then Include(NewStyle,fsUnderLine);
  if StrikeOut.Checked then Include(NewStyle,fsStrikeOut);
  if TextDefault.Checked then
    ForeCol:=clNone
  else
    ForeCol:=TextColor.Selected;
  if BackDefault.Checked then BackCol:=clNone else BackCol:=BackColor.Selected;
  with EditWindow.PascalSyntax.Attribute[AttributeList.ItemIndex] do
  begin
    Foreground:=ForeCol;
    Background:=BackCol;
    Style:=NewStyle;
  end;
end;

procedure TOptionsWindow.TextDefaultClick(Sender: TObject);
begin
  TextColor.Enabled:=not (TextDefault.Checked);
  if not (TextColor.Enabled) then
    TextColor.Selected:=TextSelector.Selected;
  ChangeHighLight(Self);
end;

procedure TOptionsWindow.BackDefaultClick(Sender: TObject);
begin
  BackColor.Enabled:=not (BackDefault.Checked);
  if not (BackColor.Enabled) then
    BackColor.Selected:=BackgroundSelector.Selected;
  ChangeHighLight(Self);
end;

procedure TOptionsWindow.PreviewHighlightStatusChange(Sender: TObject;
  Changes: TSynStatusChanges);
var
  Attri : TSynHighlighterAttributes;
  Token : String;
begin
  if Changes * [scCaretX, scCaretY] <> [] then
  begin
    if PreviewHighlight.GetHighlighterAttriAtRowCol(PreviewHighlight.CaretXY, Token,Attri) then
    begin
      AttributeList.ItemIndex:=AttributeList.Items.IndexOf(Attri.Name);
      AttributeListClick(Self);
    end;
  end;
end;

procedure TOptionsWindow.GutterClick(Sender: TObject);
begin
  PreviewHighlight.Gutter.Visible:=Gutter.Checked;
  LineNumbers.Enabled:=Gutter.Checked;
  if (not LineNumbers.Enabled) then LineNumbers.Checked:=false;
end;

procedure TOptionsWindow.LineNumbersClick(Sender: TObject);
begin
  PreviewHighlight.Gutter.ShowLineNumbers:=LineNumbers.Checked;
end;

procedure TOptionsWindow.RightEdgeClick(Sender: TObject);
begin
  RightCol.Enabled:=RightEdge.Checked;
  if not RightEdge.Checked then
    RightCol.Value:=0
  else
    RightCol.Value:=80;
end;

procedure TOptionsWindow.NewGameBoxClick(Sender: TObject);
begin
  GameSetEdit.Enabled:=NewGameBox.Checked;
  SaveGameEdit.Enabled:=not NewGameBox.Checked;
  AvaibleSaveGame.Enabled:=not NewGameBox.Checked;
end;

procedure TOptionsWindow.SaveGameEditChange(Sender: TObject);
var
  Dummy  : Integer;
  Archiv : TArchivFile;
  Games  : TGameStatusArray;
begin
  AvaibleSaveGame.Items.Clear;
  
  if not FileExists(SaveGameEdit.FileName) then
    exit;

  Archiv:=TArchivFile.Create;
  try
    Archiv.OpenArchiv(SaveGameEdit.FileName);
    savegame_api_ReadSaveGames(Archiv,Games);
    AvaibleSaveGame.Items.BeginUpdate;
    for Dummy:=0 to high(Games) do
    begin
      AvaibleSaveGame.Items.Add(Games[Dummy].Name);
    end;
    AvaibleSaveGame.Items.EndUpdate;
  finally
    Archiv.Free;
  end;
end;

end.
