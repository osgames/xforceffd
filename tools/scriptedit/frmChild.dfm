object EditChild: TEditChild
  Left = 550
  Top = 315
  VertScrollBar.Visible = False
  AutoScroll = False
  ClientHeight = 388
  ClientWidth = 450
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  FormStyle = fsMDIChild
  Icon.Data = {
    0000010001001010100001000400280100001600000028000000100000002000
    0000010004000000000080000000000000000000000010000000100000000000
    0000000080000080000000808000800000008000800080800000C0C0C0008080
    80000000FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF000000
    000000000000087777777777700008F7F7F7F7F7700008FF7F44444F700008F7
    F7F7F7F7700008FF4444444F700008F7F7F7F7F7700008FF7F44444F700008F7
    F7F7F7F7700008FF7F44444F700008F7F7F7F7F7700008FF44444F00000008F7
    F7F7F77F800008FFFFFFFF780000088888888880000000000000000000008003
    0000800300008003000080030000800300008003000080030000800300008003
    000080030000800300008003000080070000800F0000801F0000FFFF0000}
  OldCreateOrder = False
  Position = poDefault
  Visible = True
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  OnDeactivate = FormDeactivate
  PixelsPerInch = 96
  TextHeight = 13
  object Splitter: TSplitter
    Left = 0
    Top = 288
    Width = 450
    Height = 3
    Cursor = crVSplit
    Align = alBottom
    ResizeStyle = rsUpdate
    Visible = False
  end
  object EditFeld: TSynEdit
    Left = 0
    Top = 0
    Width = 450
    Height = 288
    Hint = '|Editorfeld f'#252'r das Skript'
    Align = alClient
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Courier New'
    Font.Style = []
    ParentShowHint = False
    PopupMenu = EditWindow.EditPopup
    ShowHint = True
    TabOrder = 0
    OnClick = EditFeldClick
    OnKeyPress = EditFeldKeyPress
    OnKeyUp = EditFeldKeyUp
    OnMouseDown = EditFeldMouseDown
    OnMouseMove = EditFeldMouseMove
    BookMarkOptions.BookmarkImages = EditWindow.BookmarkList
    BookMarkOptions.LeftMargin = 0
    Gutter.AutoSize = True
    Gutter.DigitCount = 2
    Gutter.Font.Charset = DEFAULT_CHARSET
    Gutter.Font.Color = clWindowText
    Gutter.Font.Height = -11
    Gutter.Font.Name = 'Courier New'
    Gutter.Font.Style = []
    Gutter.LeftOffset = 20
    Gutter.Width = 32
    Highlighter = EditWindow.PascalSyntax
    Lines.Strings = (
      '')
    MaxUndo = 2048
    Options = [eoAutoIndent, eoDragDropEditing, eoGroupUndo, eoRightMouseMovesCursor, eoScrollHintFollows, eoScrollPastEol, eoShowScrollHint, eoSmartTabDelete, eoSmartTabs, eoTabsToSpaces, eoTrimTrailingSpaces]
    TabWidth = 2
    WantTabs = True
    OnChange = EditFeldChange
    OnProcessCommand = EditFeldProcessCommand
    OnReplaceText = EditFeldReplaceText
    OnSpecialLineColors = EditFeldSpecialLineColors
    OnStatusChange = EditFeldStatusChange
    OnPaintTransient = EditFeldPaintTransient
    RemovedKeystrokes = <
      item
        Command = ecDeleteLastChar
        ShortCut = 8200
      end
      item
        Command = ecLineBreak
        ShortCut = 8205
      end
      item
        Command = ecContextHelp
        ShortCut = 112
      end
      item
        Command = ecMatchBracket
        ShortCut = 24642
      end>
    AddedKeystrokes = <>
  end
  object MessageList: TListBox
    Left = 0
    Top = 291
    Width = 450
    Height = 97
    Style = lbOwnerDrawFixed
    Align = alBottom
    ItemHeight = 19
    TabOrder = 1
    Visible = False
    OnDblClick = MessageListDblClick
    OnDrawItem = MessageListDrawItem
  end
  object KD4SaveGame: TKD4SaveGame
    Punkte = 0
    LongMessages = []
    MonthBilanz = False
    SaveDirectory = 'save\'
    Left = 168
    Top = 88
  end
  object SynEditParamShow: TSynCompletionProposal
    DefaultType = ctParams
    Options = [scoLimitToMatchedText, scoUseBuiltInTimer, scoEndCharCompletion, scoCompleteWithTab, scoCompleteWithEnter]
    ClBackground = clInfoBk
    EndOfTokenChr = '()[]. '
    TriggerChars = '(,'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clBtnText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = [fsBold]
    Columns = <>
    OnExecute = SynEditParamShowExecute
    ShortCut = 24608
    Editor = EditFeld
    TimerInterval = 100
    Left = 168
    Top = 40
  end
  object SynEditAutoComplete: TSynCompletionProposal
    Options = [scoLimitToMatchedText, scoUseInsertList, scoUsePrettyText, scoUseBuiltInTimer, scoEndCharCompletion, scoConsiderWordBreakChars, scoCompleteWithTab, scoCompleteWithEnter]
    NbLinesInWindow = 15
    Width = 500
    EndOfTokenChr = '()[].,;@-+*/ <>'
    TriggerChars = '.('
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clBtnText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = [fsBold]
    Columns = <
      item
        BiggestWord = 'CONSTRUCTOR'
      end
      item
        BiggestWord = 'CONSTRUCTOR'
      end>
    ItemHeight = 18
    Images = EditWindow.ImageList
    OnExecute = SynEditAutoCompleteExecute
    ShortCut = 16416
    Editor = EditFeld
    TimerInterval = 250
    OnCodeCompletion = SynEditAutoCompleteCodeCompletion
    Left = 200
    Top = 40
  end
  object CodeExplorerTimer: TTimer
    OnTimer = CodeExplorerTimerTimer
    Left = 136
    Top = 40
  end
end
