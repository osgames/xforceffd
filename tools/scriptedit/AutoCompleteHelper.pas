unit AutoCompleteHelper;

interface

uses SynEditTypes;

function TruncateSourceCode(Source: String; Pos: TBufferCoord): String;

implementation

uses SysUtils, Classes, uPSUtils, Math;

function TruncateSourceCode(Source: String; Pos: TBufferCoord): String;
var
  Parser      : TPSPascalParser;
  Text        : String;

  procPos     : Integer;
  procBegin   : Integer;

  List        : TStringList;

  Block       : Integer;

{  List        : TStringList;
  Line        : String;
  X           : Integer;
  Y           : Integer;
  SaveY       : Integer;
  ProcSaveY   : Integer;
  CutLine     : Integer;
  OwnProc     : Boolean;
  InProc      : Integer;
  Block       : Integer;
  BeginProc   : Boolean;
  InBlock     : Integer;
  WasVar      : Boolean;
  ProcName    : String;}

  procedure SetToWhiteSpaces(wsBegin, wsEnd: Integer);
  begin
    while (wsBegin<wsEnd) and (wsBegin<=length(Text)) do
    begin
      if (wsBegin>0) and (not (Text[wsBegin] in [#13,#10])) then
        Text[wsBegin]:=' ';

      inc(wsBegin);
    end;
  end;
begin
  Parser:=TPSPascalParser.Create;
  Parser.EnableComments:=true;

  Text:=Source;
  Parser.SetText(Source);

  while not (Parser.CurrTokenID=CSTI_EOF) do
  begin
    case Parser.CurrTokenID of
      CSTII_function, CSTII_procedure:
      begin
        procPos:=Parser.CurrTokenPos;
        procBegin:=-1;
        Block:=0;
      end;
      CSTII_Forward:
      begin
        Parser.Next;
        if Parser.CurrTokenID=CSTI_SemiColon then
        begin
          SetToWhiteSpaces(procPos,Parser.CurrTokenPos+2);
          procPos:=-1;
        end;
      end;
      CSTII_begin:
      begin
        if procBegin=-1 then
          procBegin:=Parser.CurrTokenPos+length('begin')+1
        else
          inc(Block);
      end;
      CSTII_try: inc(Block);
      CSTII_end:
      begin
        if Block=0 then
        begin
          SetToWhiteSpaces(procBegin,Parser.CurrTokenPos-1);
          procBegin:=-1;
          procPos:=-1;

          if Parser.Row>=Pos.Line then
          begin
            SetToWhiteSpaces(Parser.CurrTokenPos+length('end')+1,length(Text)+1);
            break;
          end;
        end
        else
          dec(Block);
      end;
      CSTIINT_Comment:
      begin
        SetToWhiteSpaces(Parser.CurrTokenPos+1,Parser.CurrTokenPos+length(Parser.OriginalToken)+1);
      end;
    end;

    Parser.Next;
  end;

  Parser.Free;
  result:=Text;

{  if Pos.Line<List.Count then
  begin
    Line:=List[Pos.Line];
    X:=Pos.Char;
    if X>=length(Line) then
      X:=length(Line);
    while (X>0) and (Line[X] in TSynValidStringChars) do
      dec(X);

    BeginProc:=false;
    Line:=Copy(List[Pos.Line],1,X);
    List[Pos.Line]:=Line;
  end;

  Y:=min(List.Count-1,Pos.Line-1);

  CutLine:=0;
  Block:=0;
  OwnProc:=true;
  Parser:=TPSPascalParser.Create;
  while (Y>0) do
  begin
    if Y>=List.Count then
      Y:=List.Count-1;

    Parser.SetText(List[Y]);
    if Parser.CurrTokenID in [CSTII_procedure,CSTII_function] then
    begin
      // Procedure aufr�umen
      InBlock:=0;
      ProcSaveY:=Y;
      WasVar:=false;
      inc(Y);
      while (Y<List.Count) do
      begin
        Parser.SetText(List[Y]);
        if (Parser.CurrTokenID = CSTII_var) and (not OwnProc) then
          WasVar:=true;
        if Parser.CurrTokenID = CSTII_begin then
        begin
          WasVar:=true;
          if InBlock>0 then
            List[Y]:=StringOfChar(' ',length(List[Y]));

          inc(Y);
          inc(InBlock);
          continue;
        end;

        if Parser.CurrTokenID = CSTII_end then
        begin
          dec(InBlock);
          if InBlock=0 then
            break;
        end;

        if WasVar then
          List[Y]:=StringOfChar(' ',length(List[Y]));

        inc(Y);
      end;
      Y:=ProcSaveY;
      dec(Y);
      OwnProc:=false;
      continue;
    end;

    if (Parser.CurrTokenID = CSTII_begin) then
    begin
      inc(Block);
      if (not OwnProc) then
      begin
        dec(CutLine);
        BeginProc:=true;
      end;
    end;

    if CutLine>0 then
      List[Y]:=StringOfChar(' ',length(List[Y]));

    if (Parser.CurrTokenID = CSTII_end) then
    begin
      dec(Block);
      if (not OwnProc) then
        inc(CutLine);
    end;

    dec(Y);
  end;

  // forwards der letzten Funktion entfernen (Verursacht Probleme in der Reihenfolge der Funktionen,
  // wodurch Parameter und lokale Variablen nicht korrekt ermittelt werden.

    // Funktionsnamen der letzten Funktion ermitteln

  Y:=List.Count-1;

  while (Y>=0) do
  begin
    Parser.SetText(List[Y]);
    dec(Y);

    if Parser.CurrTokenID=CSTII_begin then
      break;
  end;

  if Parser.CurrTokenID=CSTII_begin then
  begin
    ProcName:='';
    while (Y>=0) do
    begin
      Parser.SetText(List[Y]);
      if (Parser.CurrTokenID=CSTII_procedure) or (Parser.CurrTokenID=CSTII_function) then
      begin
        Parser.Next;
        if Parser.CurrTokenID=CSTI_Identifier then
        begin
          ProcName:=Parser.OriginalToken;
          SaveY:=Y;
        end;
        break;
      end;
      dec(Y);
    end;

    if ProcName<>'' then
    begin
      Y:=0;
      while Y<List.Count do
      begin
        Parser.SetText(List[Y]);
        if (Parser.CurrTokenID=CSTII_procedure) or (Parser.CurrTokenID=CSTII_function) then
        begin
          Parser.Next;
          if Parser.CurrTokenID=CSTI_Identifier then
          begin
            if FastUpperCase(Parser.OriginalToken)=FastUpperCase(ProcName) then
            begin
              if SaveY<>Y then
                List[Y]:=StringOfChar(' ',length(List[Y]));

              break;
            end;
          end;
        end;
        inc(Y);
      end;
    end;
  end;}

end;

end.
