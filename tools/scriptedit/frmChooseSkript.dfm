object ChooseSkript: TChooseSkript
  Left = 189
  Top = 199
  BorderStyle = bsDialog
  Caption = 'Skript w'#228'hlen'
  ClientHeight = 336
  ClientWidth = 258
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 8
    Top = 8
    Width = 94
    Height = 13
    Caption = 'Skripte im Spielsatz:'
  end
  object ListBox1: TListBox
    Left = 8
    Top = 24
    Width = 241
    Height = 273
    Style = lbOwnerDrawFixed
    ItemHeight = 16
    MultiSelect = True
    TabOrder = 0
    OnDblClick = ListBox1DblClick
    OnDrawItem = ListBox1DrawItem
  end
  object OKButton: TBitBtn
    Left = 64
    Top = 304
    Width = 89
    Height = 25
    TabOrder = 1
    Kind = bkOK
  end
  object BitBtn2: TBitBtn
    Left = 160
    Top = 304
    Width = 89
    Height = 25
    TabOrder = 2
    Kind = bkCancel
  end
end
