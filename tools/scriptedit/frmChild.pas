unit frmChild;

{$I SynEdit.inc}

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, TraceFile, SynEdit, XForce_types, math, SynEditKeyCmds,
  SynEditMiscClasses, SynEditSearch, uPSUtils, uPSCompiler, KD4SaveGame,
  ArchivFile, Defines, uPSRunTime, uPSDebugger, MissionList, script_Utils,
  SynEditRegexSearch, MapGenerator, SynCompletionProposal, Menus, IDHashMap,
  TB2Dock, TB2ToolWindow, TBX, TBXDkPanels, ComCtrls, JvTabBar;

type

  TSkriptType    = (stMission,stMap);

  TSetBoolean = set of Boolean;

  TEditChild = class(TForm)
    EditFeld: TSynEdit;
    MessageList: TListBox;
    Splitter: TSplitter;
    KD4SaveGame: TKD4SaveGame;
    SynEditParamShow: TSynCompletionProposal;
    SynEditAutoComplete: TSynCompletionProposal;
    CodeExplorerTimer: TTimer;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure EditFeldChange(Sender: TObject);
    procedure EditFeldSpecialLineColors(Sender: TObject; Line: Integer;
      var Special: Boolean; var FG, BG: TColor);
    procedure FormCreate(Sender: TObject);
    procedure MessageListDrawItem(Control: TWinControl; Index: Integer;
      Rect: TRect; State: TOwnerDrawState);
    procedure EditFeldStatusChange(Sender: TObject;
      Changes: TSynStatusChanges);
    procedure EditFeldMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure FormActivate(Sender: TObject);
    procedure EditFeldReplaceText(Sender: TObject; const ASearch,
      AReplace: String; Line, Column: Integer;
      var Action: TSynReplaceAction);
    procedure MessageListDblClick(Sender: TObject);
    procedure ScriptEngineOnLine(Sender: TObject; const FileName: String;
      Position, Row, Col: Cardinal);
    procedure FormDestroy(Sender: TObject);
    procedure SynEditParamShowExecute(Kind: SynCompletionType;
      Sender: TObject; var CurrentInput: String; var x, y: Integer;
      var CanExecute: Boolean);
    procedure SynEditAutoCompleteExecute(Kind: SynCompletionType;
      Sender: TObject; var CurrentInput: String; var x, y: Integer;
      var CanExecute: Boolean);
    procedure EditFeldKeyPress(Sender: TObject; var Key: Char);
    procedure EditFeldPaintTransient(Sender: TObject; Canvas: TCanvas;
      TransientType: TTransientType);
    procedure EditFeldProcessCommand(Sender: TObject;
      var Command: TSynEditorCommand; var AChar: Char; Data: Pointer);
    procedure SynEditAutoCompleteCodeCompletion(Sender: TObject;
      var Value: String; Shift: TShiftState; Index: Integer;
      EndToken: Char);
    procedure EditFeldClick(Sender: TObject);
    procedure EditFeldMouseMove(Sender: TObject; Shift: TShiftState; X,
      Y: Integer);
    procedure EditFeldKeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure CodeExplorerTimerTimer(Sender: TObject);
    procedure FormDeactivate(Sender: TObject);
  private
    fFile         : String;
    fErrorLine    : Integer;
    fMarkLine     : Integer;
    fLastPos      : TPoint;
    fTestMode     : boolean;
    fPosMark      : TSynEditMark;
    fMaxFuncWi    : Integer;
    fMarkedError  : Integer;

    fExtension    : String;
    fSkriptType   : TSkriptType;

    fCheckMapGen  : TMapGenerator;

    fMission      : TMission;
    fMapGenerator : TMapGenerator;

    fLineInfo     : Array of Boolean;

    fObjectList   : TParamInfoArray;
    fTypeInfos    : TIDHashMap;

    fmemoryPool   : Pointer;

    fBarItem      : TJvTabBarItem;

    // Auf true Setzen wenn beim der Verarbeitung der n�chsten Quellzeile in den
    // Pause-Modus gewechselt werden soll
    fPauseNextLine: Boolean;
    fAutoShowEvent: Boolean;

    fDefines      : TStringList;

    procedure SetFileName(const Value: String);

    procedure ChangeTestMode(Active: boolean);

    procedure AddMessage(Mess: String);
    procedure MarkErrorLine(Index: Integer);

    function GetImage(Line: Integer): Integer;

    function GetLineMark(Line: Integer): TSynEditMark;
    procedure SetSkriptType(const Value: TSkriptType);
    procedure MissionDestroy(Mission: TObject);

    function GetRunning: Boolean;

    procedure RunTimeException(Sender: TPSExec; ExError: TPSError; const ExParam: string; ExObject: TObject; ProcNo, Position: Cardinal);

    procedure GetMessage(Sender: TObject);

    function Compiler    : TPSPascalCompiler;
    procedure FillAutoComplete(var List: TParamInfoArray; Types: TInfoTypes; BuildIn: TSetBoolean; FromFather: Cardinal = 0; Typ: String = '');

    procedure CompileScript(Skript: String; CallBack: TPSOnNotify);

    procedure RebuildLokalObjektList(CursorPos: Boolean = true);
    procedure BuildLokalObjektList(Comp: TPSPascalCompiler);

    procedure RebuildCodeExplorer;
    procedure BuildCodeExplorer(Comp: TPSPascalCompiler);
    { Private-Deklarationen }

    function LookUpList(LookUp: String; var ParamInfo: TParamInfoRecord): Boolean;overload;
    function LookUpList(LookUp: Cardinal; var ParamInfo: TParamInfoRecord): Boolean;overload;

    function FindParameter(LocLine: String; X: Integer; out Func: TParamInfoRecord; out ParamCount: Integer): Boolean;
    function GetLookUpString(Line: String; EndPos: Integer): String;
    function GetLookUpObjectName(Line: String): String;

    function GetLineOfFirstProcedure: Integer;

    function FindEnumName(TypName: String; Value: Integer): String;
    function FindRecordFieldNr(RecName: String; FieldName: String): Integer;
    function FindRecordFieldName(RecName: String; FieldNo: Integer): String;
    function FindSubTypeHash(TypName: String): Cardinal;

    function VariantToString(Variant: TPSVariantIFC): String;

    procedure ClearMemoryPool;

    //** Codefolding: http://www.delphipraxis.net/topic61765.html&postdays=0&postorder=asc&highlight=codefolding&start=0
    //procedure SetCodefoldingRegions;

  public
    function Save(AutoSave: Boolean): boolean;
    procedure SaveAs(FileName: String);

    procedure ShowError(Error: String);
    procedure ShowErrors;

    procedure ResetError;
    function StartTestModus: Boolean;
    procedure StopTestModus;
    function StartOwnerFunction(Name: String): Boolean;
    procedure SetBreakPoint(LineNr: Integer);
    procedure Pause;

    procedure SearchDeklaration;

    procedure AktuVariablen;

    procedure HighlightLine(Line: Integer);

    procedure RunTest;
    procedure TestEnde;
    function DoNextStep(StepInto: Boolean): boolean;
    function CheckSkript(FromSaving: Boolean = false): boolean;

    function GetWatchValue(WatchString: String): String;

    // Erweiterte Bearbeitungsfunktionen
    procedure CreateNewProcedure(Declaration: String);

    procedure ShowMap;

    procedure CallStartEvent(SingleStep: Boolean);
    function WaitForEvent: Boolean;

    function SkriptEngine: TPSDebugExec;

    property FileName      : String read fFile write SetFileName;

    property DebugMode     : boolean read fTestMode;
    property Running       : Boolean read GetRunning;
    property Mission       : TMission read fMission;

    property Extension     : String read fExtension;
    property SkriptType    : TSkriptType read fSkriptType write SetSkriptType;

    property BarItem       : TJvTabBarItem read fBarItem write fBarItem;

    property WantShowEvent : Boolean read fAutoShowEvent write fAutoShowEvent;
    { Public-Deklarationen }
  end;

var
  EditChild: TEditChild;

implementation

uses
  frmMain, SynEditHighlighter, frmMap, savegame_api, game_api, string_utils,
  gameset_api, ExtRecord, record_utils, SynEditTypes, AutoCompleteHelper,
  register_scripttypes, KD4Utils, JvInspector, api_utils;
{$R *.DFM}

var
  Child: TEditChild;


function AutoCompleteCompilerBeforeCleanUp(Sender: TPSPascalCompiler): Boolean;
begin
  TEditChild(Sender.ID).BuildLokalObjektList(Sender);
  result:=true;
end;

function CodeExplorerBeforeCleanUp(Sender: TPSPascalCompiler): Boolean;
begin
  TEditChild(Sender.ID).BuildCodeExplorer(Sender);
  result:=true;
end;

function PositionToLine(Skript: String; Position: Integer): Integer;
var
  Dummy: Integer;
  Line : Integer;
begin
  Line:=1;
  for Dummy:=1 to Position do
  begin
    if Skript[Dummy]=#10 then
      inc(Line);
  end;
  result:=Line-1;
end;

function ScriptEngineWriteLine(Sender: TPSPascalCompiler; Position: Cardinal): Boolean;
begin
  Child.fLineInfo[PositionToLine(Child.EditFeld.Lines.Text,Position)]:=true;
  result:=true;
end;

procedure ScriptEngineLineInfo(Sender: TPSDebugExec; const FileName: String; Position, Row, Col: Cardinal);
begin
  TEditChild(Sender.ID).ScriptEngineOnLine(Sender,FileName,Position,Row,Col);
end;

procedure ScriptEngineIdle(Sender: TPSDebugExec);
begin
  Application.HandleMessage;
  if Application.Terminated then
    (TObject(Sender.id) as TEditChild).StopTestModus;
end;

procedure MissionRunTimException(Sender: TPSExec; ExError: TPSError; const ExParam: string; ExObject: TObject; ProcNo, Position: Cardinal);
begin
  (TObject(Sender.ID) as TEditChild).RunTimeException(Sender,ExError,ExParam,ExObject,ProcNo,Position);
end;

procedure TEditChild.CompileScript(Skript: String; CallBack: TPSOnNotify);
var
  Comp: TPSPascalCompiler;
begin
  if SkriptType=stMission then
  begin
    Comp:=TPSPascalCompiler.Create;
    Comp.AllowNoBegin:=true;
    Comp.BooleanShortCircuit:=true;
    Comp.OnUses:=RegisterCompiler;

    Comp.ID:=Self;
    Comp.OnBeforeCleanup:=CallBack;

    Comp.Compile(Skript);

    Comp.Free;
  end
  else
  begin
    with TMapGenerator.Create do
    begin
      Compiler.ID:=Self;
      Compiler.OnBeforeCleanup:=CallBack;
      Compile(Skript);
      Free;
    end;
  end;
end;

function TEditChild.GetLookUpString(Line: String; EndPos: Integer): String;
const
  TSynValidIdentifierChars = TSynValidStringChars + ['.',' ','[',']','(',')'];
var
  TmpX       : Integer;
  ParenCount : Integer;
  WasSpace   : Boolean;
  CanSpace   : Boolean;
begin
  //we have a valid open paren, lets see what the word before it is
  if Line[EndPos] in TSynValidStringChars then
  begin
    while (EndPos < length(Line)) and (Line[EndPos] in TSynValidStringChars) do
      inc(EndPos);

    dec(EndPos);
  end;
  
  TmpX := EndPos;

  ParenCount:=0;
  WasSpace:=false;
  CanSpace:=true;
  while (TmpX > 0) and ((Line[TmpX] in TSynValidIdentifierChars) or (ParenCount>0)) do
  begin
    case Line[TmpX] of
      ')',']': inc(ParenCount);
      '(','[':
        begin
          if ParenCount=0 then
            break;
          dec(ParenCount);
        end;
      '.' :
        begin
          CanSpace:=true;
          WasSpace:=false;
        end;
      ' ' :
        begin
          if not CanSpace then
            WasSpace:=true;
        end;
      else
      begin
        if WasSpace then
          break;
        WasSpace:=false;
        CanSpace:=false;
      end;
    end;

    dec(TmpX);
  end;

  if ParenCount = 0 then
    result := Copy(Line, TmpX+1, EndPos - TmpX)
  else
    result := '';
end;

function TEditChild.GetLookUpObjectName(Line: String): String;
begin
  if Pos('(',Line)<>0 then
    result:=Copy(Line,1,Pos('(',Line)-1)
  else
    result:=Line;
end;

function TEditChild.FindParameter(LocLine: String; X: Integer; out Func: TParamInfoRecord; out ParamCount: Integer): Boolean;
var
  TmpX : Integer;
  ParenCounter : Integer;
  LookUp : String;
begin
  //go back from the cursor and find the first open paren
  TmpX := X;
  if TmpX > length(locLine) then
    TmpX := length(locLine)
  else
    dec(TmpX);

  result := False;
  ParamCount := 0;
  while (TmpX > 0) and not(result) do
  begin
    if LocLine[TmpX] = ',' then
    begin
      inc(ParamCount);
      dec(TmpX);
    end
    else if LocLine[TmpX] = ')' then
    begin
      //We found a close, go till it's opening paren
      ParenCounter := 1;
      dec(TmpX);
      while (TmpX > 0) and (ParenCounter > 0) do
      begin
        if LocLine[TmpX] = ')' then
          inc(ParenCounter)
        else if LocLine[TmpX] = '(' then
          dec(ParenCounter);

        dec(TmpX);
      end;
    end
    else if locLine[TmpX] = '(' then
    begin
      //we have a valid open paren, lets see what the word before it is
      LookUp := GetLookUpString(locLine,tmpX-1);
      if LookUp<>'' then
      begin
        result := LookupList(Lookup,Func);
        if not (Func.Typ in [itProcedure,itFunction,itType]) then
        begin
          result:=false;
        end;
        if not(result) then
          dec(TmpX);
      end
      else
        dec(TmpX);
    end
    else
      dec(TmpX)
  end;
end;

procedure TEditChild.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  ModalResult:=mrNone;
  Action:=caNone;
  if EditFeld.Modified then
  begin
    BringToFront;
    EditFeld.SetFocus;
    case MessageDlg('�nderungen in '+FileName+' speichern?',mtConfirmation,mbYesNoCancel,0) of
      mrYes    :
      begin
        Save(false);
        if EditFeld.Modified then
          exit;
      end;
      mrNo     : Action:=caFree;
      mrCancel : exit;
    end;
  end;
  StopTestModus;
  Action:=caFree;
  ModalResult:=mrOK;
  if fBarItem<>nil then
  begin
    fBarItem.Data:=nil;
    fBarItem.Free;
    fBarItem:=nil;
  end;
end;

procedure TEditChild.EditFeldChange(Sender: TObject);
var
  ErLine: Integer;
begin
  if fErrorLine<>-1 then
  begin
    ErLine:=fErrorLine;
    fErrorLine:=-1;
    fMarkedError:=-1;
    EditFeld.InvalidateLine(ErLine);
    MessageList.Refresh;
  end;
  if EditFeld.Modified then
  begin
    EditWindow.StatusBar.Panels[1].Text:='Ver�ndert';
    if Caption[length(Caption)]<>'*' then
      Caption:=Caption+' *';
  end
  else
  begin
    EditWindow.StatusBar.Panels[1].Text:='';
    if Caption[length(Caption)]='*' then
    begin
      Caption:=Copy(Caption,1,length(Caption)-2);
    end;
  end;
end;

function TEditChild.Save(AutoSave: Boolean): boolean;
var
  Stream   : TMemoryStream;
  Parts    : TStringArray;
  Archiv   : TArchivFile;
  head     : TSetHeader;
  Skripts  : TRecordArray;
  Skript   : TExtRecord;
  Header   : TEntryHeader;
  Dummy    : Integer;
begin
  if not EditFeld.Modified then
  begin
    result:=true;
    exit;
  end;
  result:=false;
  if fFile='' then
  begin
    if SkriptType=stMap then
      EditWindow.SaveDialog.FilterIndex:=2
    else
      EditWindow.SaveDialog.FilterIndex:=1;

    if EditWindow.SaveDialog.Execute then
      FileName:=EditWindow.SaveDialog.FileName
    else
      exit;

  end;

  if (not AutoSave) and (not CheckSkript(true)) then
  begin
    if Application.MessageBox(PChar('Skript enth�lt Fehler. Dennoch speichern?'),PChar('Fehler'),MB_YESNO or MB_ICONWARNING)=ID_NO then
      exit;
  end;

  Parts:=string_utils_explode('|',fFile);
  if length(Parts)=1 then
    EditFeld.Lines.SaveToFile(fFile)
  else
  begin
    Archiv:=TArchivFile.Create;
    Archiv.OpenArchiv(Parts[0]);
    try
      gameset_api_ReadHeader(Archiv,Head);
      gameset_api_LoadSkripts(Archiv,Skripts);
      Skript:=record_utils_FindID(Skripts,StrToInt64('$'+Copy(Parts[1],1,8)));
      Assert(Skript<>nil);
      Skript.SetString('Skript',Child.EditFeld.Lines.Text);

      // Skripte speichern
      Stream:=TMemoryStream.Create;

      Header.Version:=SkriptVersion;
      Header.Entrys:=length(Skripts);
      Stream.Write(Header,SizeOf(Header));
      for Dummy:=0 to high(Skripts) do
        Skripts[Dummy].SaveToStream(Stream);

      Archiv.ReplaceRessource(Stream,RNSkripte,true);
      Stream.Free;

      record_utils_ClearArray(Skripts);
    except
      Archiv.Free;
      exit;
    end;
    Archiv.Free;
  end;

  EditFeld.Modified:=false;
  EditWindow.AddToOpenList(fFile);
  EditWindow.StateChange;

  if Caption[length(Caption)]='*' then
    Caption:=Copy(Caption,1,length(Caption)-2);

  result:=true;
end;

procedure TEditChild.SaveAs(FileName: String);
var
  OldFile: String;
begin
  OldFile:=fFile;
  fFile:=FileName;
  EditWindow.AddToOpenList(fFile);
  Save(false);
  if EditFeld.Modified then
    fFile:=OldFile;
  Self.FileName:=fFile;
end;

procedure TEditChild.ResetError;
begin
  MessageList.Items.Clear;
  MessageList.Visible:=false;
  Splitter.Visible:=false;
  fMarkedError:=-1;
end;

procedure TEditChild.HighlightLine(Line: Integer);
var
  OldLine: Integer;
begin
  if OldLine=fMarkLine then
    exit;

  OldLine:=fMarkLine;

  fMarkLine:=Line;
  if fMarkLine<>-1 then
    EditFeld.InvalidateLine(fMarkLine);

  if OldLine<>-1 then
    EditFeld.InvalidateLine(OldLine);
end;

procedure TEditChild.MarkErrorLine(Index: Integer);
var
  Temp      : String;
  NewLine   : Integer;
  ErrorPos  : Integer;
  Msg       : String;
begin
  try
    Msg:=MessageList.Items[Index];
    Temp:=Copy(Msg,Pos('(',Msg),length(Msg));
    Temp:=Copy(Temp,2,Pos(':',Temp)-2);
    NewLine:=StrToInt(Temp);
    Temp:=Msg;
    Temp:=Copy(Temp,Pos(':',Temp)+1,Pos(')',Temp)-Pos(':',Temp)-1);
    ErrorPos:=StrToInt(Temp);
    EditFeld.CaretX:=ErrorPos;
    EditFeld.CaretY:=NewLine;
    EditFeld.EnsureCursorPosVisibleEx(true);
    EditFeld.SetFocus;
    EditFeld.InvalidateLine(fErrorLine);
    EditFeld.InvalidateLine(NewLine);
    fMarkedError:=Index;
    MessageList.Refresh;
    fErrorLine:=NewLine;
  except
    fErrorLine:=-1;
  end;
end;

procedure TEditChild.EditFeldSpecialLineColors(Sender: TObject;
  Line: Integer; var Special: Boolean; var FG, BG: TColor);
var
  BreakPoint  : Boolean;
  Valid       : boolean;
  Mark        : TSynEditMark;
begin
  Special:=false;
  if Line=fErrorLine then
  begin
    Special:=true;
    FG:=clYellow;
    BG:=clMaroon;
  end
  else if Line=fMarkLine then
  begin
    Special:=true;
    FG:=clYellow;
    BG:=clMaroon;
  end
  else if DebugMode and (Line=fPosMark.Line) and (not Running) then
  begin
    Special:=true;
    FG:=clWhite;
    BG:=clBlue;
  end
  else
  begin
    BreakPoint:=false;
    Valid:=false;
    Mark:=GetLineMark(Line);
    if Mark<>nil then
    begin
      BreakPoint:=Mark.ImageIndex<>13;
      Valid:=(Mark.ImageIndex=11) or (Mark.ImageIndex=10);
    end;
    if BreakPoint then
    begin
      Special:=true;
      if Valid then
      begin
        FG:=clWhite;
        BG:=clRed;
      end
      else
      begin
        FG:=clRed;
        BG:=clMaroon;
      end;
    end;
  end;
end;

procedure TEditChild.SetFileName(const Value: String);
begin
  fFile := Value;
  Caption:=Value;
  if fBarItem<>nil then
    fBarItem.Caption:=ExtractFileName(Value);
end;

procedure TEditChild.ChangeTestMode(Active: boolean);
var
  Dummy  : Integer;
  Mark   : TSynEditMark;
begin
  if fTestMode=Active then exit;

  if Active then
  begin
    // Pr�fen, ob bereits ein Test l�uft
    for Dummy:=0 to EditWindow.MDIChildCount-1 do
    begin
      if (EditWindow.MDIChildren[Dummy]<>Self) and TEditChild(EditWindow.MDIChildren[Dummy]).DebugMode then
      begin
        raise Exception.Create('In einem anderen Fenster ist der Testmodus bereits aktiv. Der laufende Test muss beendet werden, um das Skript im Testmodus zu starten.');
        exit;
      end;
    end;
    fMaxFuncWi:=0;
    if EditFeld.Modified then
      Caption:=StringReplace(Caption,' *',' [Test] *',[])
    else
      Caption:=Caption+' [Test]';

    for Dummy:=0 to EditFeld.Lines.Count-1 do
    begin
      if fLineInfo[Dummy-1] then
      begin
        Mark:=GetLineMark(Dummy);
        if Mark=nil then
        begin
          Mark := TSynEditMark.Create(EditFeld);
          with mark do
          begin
            Line := Dummy;
            ImageIndex := 13;
            Visible := true;
            InternalImage := false;
          end;
          EditFeld.Marks.Place(Mark);
        end;
      end;
    end;
  end
  else
  begin
    if SkriptEngine=nil then
      exit;
      
    SkriptEngine.Stop;
    
    Caption:=StringReplace(Caption,' [Test]','',[]);
    fPosMark.Line:=-1;
    for Dummy:=EditFeld.Marks.Count-1 downto 0 do
    begin
      if EditFeld.Marks[Dummy].ImageIndex=13 then
      begin
        EditFeld.Marks[Dummy].Free;
        EditFeld.Marks.Delete(Dummy);
      end;
    end;
    if fMission<>nil then
    begin
      fMission.MissionAbort;
      fMission:=nil;
    end;

    EditWindow.WatchInspector.Clear;
  end;
  fTestMode:=Active;
  EditFeld.ReadOnly:=Active;
  for Dummy:=0 to EditFeld.Marks.Count-1 do
  begin
    if (not EditFeld.Marks[Dummy].IsBookmark) and ((EditFeld.Marks[Dummy].ImageIndex>=10) and (EditFeld.Marks[Dummy].ImageIndex<=12)) then
      EditFeld.Marks[Dummy].ImageIndex:=GetImage(EditFeld.Marks[Dummy].Line);
  end;
  EditFeld.Invalidate;
  MessageList.Visible:=Active;
  Splitter.Visible:=Active;
end;

function TEditChild.StartTestModus: Boolean;
var
  Data  : TNewGameRec;
  Parts : TStringArray;
  Archiv: TArchivFile;
begin
  RebuildLokalObjektList;

  fErrorLine:=-1;
  EditFeld.Invalidate;
  result:=false;
  if not CheckSkript then
    exit;

  result:=true;

  if SkriptType=stMission then
  begin
    savegame_api_Init(KD4SaveGame);

    if (Pos('|',Self.FileName)<>0) or (not TestSaveGame) then
    begin
      with Data do
      begin
        FileName:='TestPlayer';
        Name:='ScriptTest';
        Kapital:=1000000;
        StartAlphatron:=50000;
        Forsch:=100;
        Prod:=100;
        Soldat:=10;
        Forscher:=10;
        Techniker:=10;

        if Pos('|',Self.FileName)<>0 then
        begin
          // Skript aus einem Spielsatz ist ge�ffnet => Spielsatz wird zum
          // testen genutzt
          Parts:=string_utils_explode('|',Self.FileName);
          GameFile.FileName:=Parts[0];
        end
        else
        begin
          GameFile.FileName:=GameSet;
        end;

        Basis.X:=210;
        Basis.Y:=120;
        Language:='german';
      end;
      KD4SaveGame.NewGame(Data);
    end
    else
    begin
      if not FileExists(SaveGameFile) then
      begin
        Application.MessageBox('Es wurde ein ung�ltiger Spielstand zum Test angegeben. Bitte pr�fe die Einstellungen.','Fehler',MB_ICONERROR);
        result:=false;
        exit;
      end;

      try
        Archiv:=TArchivFile.Create;
        Archiv.OpenArchiv(SaveGameFile,true);

        if not Archiv.ExistRessource(SaveGameName) then
        begin
          Application.MessageBox('Es wurde ein ung�ltiger Spielstand zum Test angegeben. Bitte pr�fe die Einstellungen.','Fehler',MB_ICONERROR);
          result:=false;
          exit;
        end;
        try
          Archiv.OpenRessource(SaveGameName);
          KD4SaveGame.LoadGame(SaveGameFile,Archiv.Stream);
        except
          Application.MessageBox('Es wurde ein ung�ltiger Spielstand zum Test angegeben. Bitte pr�fe die Einstellungen.','Fehler',MB_ICONERROR);
          result:=false;
          exit;
        end;
      finally
        Archiv.Free;
      end;
    end;

    fMission:=KD4SaveGame.MissionList.CreateMission(EditFeld.Lines.Text,GetShortHint(fFile),fDefines);

    fMission.NotifyList.RegisterEvent(EVENT_MISSIONDESTROY,MissionDestroy);

  end
  else
  begin
    fMapGenerator.Compiler.OnWriteLine:=ScriptEngineWriteLine;
    fMapGenerator.Compile(EditFeld.Lines.Text);
  end;

  SkriptEngine.Id:=Self;
  SkriptEngine.DebugEnabled:=true;
  SkriptEngine.OnSourceLine:=ScriptEngineLineInfo;
  SkriptEngine.OnIdleCall:=ScriptEngineIdle;
  SkriptEngine.OnException:=MissionRunTimException;

  try
    ChangeTestMode(true);
  except
    on E: Exception do
    begin
      fTestMode:=true;
      ChangeTestMode(false);

      Application.MessageBox(PChar(E.Message),'Fehler',MB_ICONERROR);

      result:=false;
    end;
  end;
end;

procedure TEditChild.StopTestModus;
begin
  ChangeTestMode(false);

//  if fMission<>nil then
//    fMission.MissionAbort;
end;

function TEditChild.GetImage(Line: Integer): Integer;
begin
  if (DebugMode) then
  begin
    if fLineInfo[Line-1] then
      result:=11
    else
      result:=12;
  end
  else
    result:=10;
end;

function TEditChild.StartOwnerFunction(Name: String): Boolean;
begin
  Result:=SkriptEngine.RunProcPN([],Name);
end;

function TEditChild.DoNextStep(StepInto: Boolean): boolean;
var
  Error  : String;
begin
  result:=false;
  
  if not fTestMode then
    exit;

  result:=true;

  Assert(SkriptEngine<>nil);
  if (fMission=nil) or (not fMission.WaitForEvent) then
  begin
    if StepInto then
      SkriptEngine.StepInto
    else
      SkriptEngine.StepOver;

{    if SkriptEngine.ExceptionCode<>ErNoError then
    begin
      StopTestModus;
      Error:=script_utils_GetErrorString(SkriptEngine);
      ShowError(Error);
      result:=true;
      Application.MessageBox(PChar('Es ist ein Laufzeitfehler aufgetreten:'#13#10#13#10+Error),'Laufzeitfehler', MB_OK or MB_ICONERROR);
      exit;
    end;}
  end
  else
  begin
    fPauseNextLine:=true;
    EditWindow.ShowEvents(Self);
  end;
end;

procedure TEditChild.FormCreate(Sender: TObject);
var
  Archiv: TArchivFile;
begin
  fPosMark:=TSynEditMark.Create(EditFeld);
  fPosMark.ImageIndex:=14;
  fPosMark.BookmarkNumber:=11;
  fPosMark.InternalImage:=false;
  fPosMark.Line:=-1;
  fPosMark.Visible:=true;
  EditFeld.Marks.Place(fPosMark);

  fMemoryPool:=GetMemory(20000);
  ZeroMemory(fMemoryPool,20000);

  KD4SaveGame.MissionList.Compiler.OnWriteLine:=ScriptEngineWriteLine;

  // Daten des Savegames Laden
  Archiv:=TArchivFile.Create;
  Archiv.OpenArchiv(XForceDir+FGameDataFile,true);

  // Personennamen laden
  Archiv.OpenRessource(RNNameDat);
  KD4SaveGame.PersonList.LoadNames(Archiv.Stream);

  // L�nder laden
  Archiv.OpenRessource(RNOrganisations);
  KD4SaveGame.Organisations.LoadLandInfo(Archiv.Stream);

  // St�dte laden
  Archiv.OpenRessource(RNTownData);
  KD4SaveGame.Organisations.LoadTownInfo(Archiv.Stream);

  Archiv.Free;

  fAutoShowEvent:=false;

  savegame_api_init(KD4Savegame);
  savegame_api_RegisterMessageHandler(GetMessage);

  SkriptType:=stMission;

  fTypeInfos:=TIDHashMap.Create;

  fMapGenerator:=TMapGenerator.Create;

  fDefines:=TStringList.Create;
  fDefines.Add('DEBUG');
  fDefines.Add('SKRIPTEDIT');

  //** Codefolding: http://www.delphipraxis.net/topic61765.html&postdays=0&postorder=asc&highlight=codefolding&start=0
  //SetCodeFoldingRegions;
end;

procedure TEditChild.AddMessage(Mess: String);
var
  Funk: String;
begin
  if Mess[1]='[' then
  begin
    Funk:=copy(Mess,1,Pos(']',Mess));
    MessageList.Canvas.Font.Style:=[fsBold];
    if MessageList.Canvas.TextWidth(Funk)>fMaxFuncWi then fMaxFuncWi:=MessageList.Canvas.TextWidth(Funk);
    MessageList.Canvas.Font.Style:=[];
  end;
  MessageList.Items.Add(Mess);
  MessageList.ItemIndex:=MessageList.Items.Count-1;
end;

procedure TEditChild.MessageListDrawItem(Control: TWinControl;
  Index: Integer; Rect: TRect; State: TOwnerDrawState);
var
  Funk  : String;
  PosK  : Integer;
begin
  with MessageList do
  begin
    Canvas.FillRect(Rect);
    if Items[Index][1]='[' then
    begin
      PosK:=Pos(']',Items[Index]);
      Funk:=copy(Items[Index],1,PosK);
      Canvas.Font.Style:=[fsBold];
      Canvas.TextOut(Rect.Left+20,Rect.Top+2,Funk);
      if Index=fMarkedError then
      begin
        EditWindow.ImageList.Draw(Canvas,Rect.Left,Rect.Top+1,26);
      end;
      Canvas.Font.Style:=[];
      Canvas.TextOut(Rect.Left+(fMaxFuncWi+20)+10,Rect.Top+2,copy(Items[Index],PosK+2,200));
    end
    else
    begin
      Canvas.Font.Color:=clRed;
      Canvas.Font.Style:=[fsBold];
      Canvas.TextOut(Rect.Left+2,Rect.Top+2,Items[Index]);
      Canvas.Font.Style:=[];
      Canvas.Font.Color:=clBlack;
    end;
  end;
end;

procedure TEditChild.SetBreakPoint(LineNr: Integer);
var
  mark  : TSynEditMark;
begin
  Mark:=GetLineMark(LineNr);
  if Mark=nil then
  begin
    Mark := TSynEditMark.Create(EditFeld);
    with mark do
    begin
      Line := LineNr;
      Char := EditFeld.CaretX;
      Visible:=true;
      InternalImage:=false;
    end;
    EditFeld.Marks.Place(Mark);
  end;
  if (Mark.ImageIndex = 10) or (Mark.ImageIndex = 11) or (Mark.ImageIndex = 12) then
  begin
    if (fLineInfo[LineNr-1]) and (fTestMode) then
      Mark.ImageIndex:=13
    else
    begin
      EditFeld.Marks.Delete(EditFeld.Marks.IndexOf(Mark));
      Mark.Free;
    end;
  end
  else
  begin
    mark.ImageIndex:=GetImage(LineNr);
  end;
  EditFeld.InvalidateLine(LineNr);
end;

function TEditChild.CheckSkript(FromSaving: Boolean): boolean;
begin
  ReBuildCodeExplorer;

  ResetError;
  if (frmMain.AutoSave) and (not FromSaving) then
    Save(true);

  Child:=Self;

  SetLength(fLineInfo,EditFeld.Lines.Count);
  FillChar(fLineInfo[0],EditFeld.Lines.Count,#0);

  if SkriptType=stMission then
    result:=KD4SaveGame.MissionList.CompileScript(EditFeld.Lines.Text,GetShortHint(fFile),fDefines)
  else
  begin
    Assert(fCheckMapGen<>nil);
    result:=fCheckMapGen.Compile(EditFeld.Lines.Text);
  end;

  if not result then
    MessageBeep(MB_ICONERROR);

//  FillList;
  ShowErrors;
end;

procedure TEditChild.EditFeldStatusChange(Sender: TObject;
  Changes: TSynStatusChanges);
var
  Attri : TSynHighlighterAttributes;
  Token : String;
begin
  if Changes * [scAll,scCaretX, scCaretY] <> [] then
  begin
    EditWindow.StatusBar.Panels[0].Text:=Format('%.3d : %.3d',[EditFeld.CaretY,EditFeld.CaretX]);
    if EditFeld.GetHighlighterAttriAtRowCol(EditFeld.CaretXY, Token,Attri) then
    begin
      EditWindow.StatusBar.Panels[2].Text:=Attri.Name;
    end
    else
      EditWindow.StatusBar.Panels[2].Text:='';
  end;
  if fBarItem<>nil then
    fBarItem.Modified:=EditFeld.Modified;
end;

procedure TEditChild.EditFeldMouseDown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
var
  Line: Integer;
begin
  if Button<>mbLeft then
  begin
    if Button=mbRight then
    begin
      BringToFront;
    end;
  end;
  if X<EditFeld.Gutter.RealGutterWidth(EditFeld.CharWidth) then
  begin
    Line:=EditFeld.PixelsToRowColumn(X,Y).Row;
    if Line<=EditFeld.Lines.Count then
      SetBreakPoint(Line);
  end;
end;

function TEditChild.GetLineMark(Line: Integer): TSynEditMark;
var
  Mark : TSynEditMark;
  i    : Integer;
begin
  result:=nil;
  with EditFeld do
  begin
    for i:=Marks.Count - 1 downto 0 do
    begin
      Mark:=Marks.Items[i];
      if (not Mark.IsBookmark) and (Mark.Line = Line) then
      begin
        if (Mark.ImageIndex>=10) and (Mark.ImageIndex<=13) then
        begin
          result:=Mark;
          break;
        end;
      end;
    end;
  end;
end;

procedure TEditChild.FormActivate(Sender: TObject);
var
  Attri : TSynHighlighterAttributes;
  Token : String;
begin
  EditWindow.StatusBar.Panels[0].Text:=Format('%.3d : %.3d',[EditFeld.CaretY,EditFeld.CaretX]);
  if EditFeld.GetHighlighterAttriAtRowCol(EditFeld.CaretXY, Token,Attri) then
  begin
    EditWindow.StatusBar.Panels[2].Text:=Attri.Name;
  end
  else
    EditWindow.StatusBar.Panels[2].Text:='';

  if EditFeld.Modified then
    EditWindow.StatusBar.Panels[1].Text:='Ver�ndert'
  else
    EditWindow.StatusBar.Panels[1].Text:='';

  AktuVariablen;

  CodeExplorerTimer.Enabled:=false;
  CodeExplorerTimer.Enabled:=true;

  api_reinit(KD4SaveGame);

  EditWindow.ObjectBrowserMission.Visible:=SkriptType=stMission;
  EditWindow.ObjectBrowserMap.Visible:=SkriptType=stMap;

  if fBarItem<>nil then
    fBarItem.Selected:=true;
end;

procedure TEditChild.ShowError(Error: String);
begin
  AddMessage(Error);
  MessageList.Visible:=true;
  Splitter.Visible:=true;
end;

procedure TEditChild.ShowErrors;
var
  Dummy: Integer;
begin
  MessageList.Items.Clear;
  Assert(Compiler<>nil);
  with Compiler do
  begin
    for Dummy:=0 to MsgCount-1 do
      ShowError(Msg[Dummy].MessageToString);
  end;

  for Dummy:=0 to MessageList.Items.Count-1 do
  begin
    if FastUpperCase(Copy(messageList.Items[Dummy],2,5))='ERROR' then
    begin
      MarkErrorLine(Dummy);
      break;
    end;
  end;
end;

procedure TEditChild.RunTest;
begin
  if DebugMode then
  begin
    if not WaitForEvent then
    begin
      fPosMark.Line:=-1;
      EditFeld.Invalidate;
      SkriptEngine.Run;
    end
    else
    begin
      fPauseNextLine:=false;
      EditWindow.ShowEvents(Self);
    end;
  end
  else
  begin
    if StartTestModus then
    begin
      CallStartEvent(false);
      fPauseNextLine:=false;
    end;
  end;
end;

procedure TEditChild.TestEnde;
begin
  StopTestModus;
end;

procedure TEditChild.EditFeldReplaceText(Sender: TObject; const ASearch,
  AReplace: String; Line, Column: Integer; var Action: TSynReplaceAction);
var
  Result: Integer;
begin
  MessageBeep(MB_ICONQUESTION);
  Result:=MessageDlg('Dieses Vorkommen von '''+ASearch+''' ersetzen?',mtConfirmation,[mbYes,mbNo,mbCancel,mbAll],0);
  case Result of
    mrYes    : Action:=raReplace;
    mrNo     : Action:=raSkip;
    mrAll    : Action:=raReplaceAll;
    mrCancel : Action:=raCancel;
  end;
end;

procedure TEditChild.MessageListDblClick(Sender: TObject);
begin
  MarkErrorLine(MessageList.ItemIndex);
end;

procedure TEditChild.SetSkriptType(const Value: TSkriptType);
begin
  fSkriptType:=Value;
  case Value of
    stMission  :
    begin
      fExtension:='xms';
    end;
    stMap      :
    begin
      fExtension:='xma';
      if fCheckMapGen=nil then
        fCheckMapGen:=TMapGenerator.Create;
    end;
  end;

  EditWindow.ObjectBrowserMission.Visible:=SkriptType=stMission;
  EditWindow.ObjectBrowserMap.Visible:=SkriptType=stMap;
end;

procedure TEditChild.CallStartEvent(SingleStep: Boolean);
begin
  fPauseNextLine:=SingleStep;
  
  if SkriptType=stMission then
  begin
    Assert(fMission<>nil);

    fMission.Start;
  end
  else
  begin
    Assert(fMapGenerator<>nil);
    fMapGenerator.Generate(10);
    if fTestMode then
    begin
      ShowMap;
      StopTestModus;
    end;
  end;

  fPosMark.Line:=-1;
  EditFeld.Invalidate;
end;

procedure TEditChild.ShowMap;
begin
  ShowMapForm.ShowMap(fMapGenerator);
  ShowMapForm.ShowModal;
end;

procedure TEditChild.ScriptEngineOnLine(Sender: TObject;
  const FileName: String; Position, Row, Col: Cardinal);
var
  Mark: TSynEditMark;
begin
  if not DebugMode then
    exit;

  fAutoShowEvent:=true;

  Row:=PositionToLine(EditFeld.Text,Position)+1;
  Mark:=GetLineMark(Row);
  if Mark=nil then
    exit;
    
  if (Mark.ImageIndex<>13) or fPauseNextLine then
  begin
    SkriptEngine.Pause;
    fPauseNextLine:=false;
  end;

  Application.ProcessMessages;

  fLastPos:=Point(1,Row);

  if SkriptEngine=nil then
    exit;

  if (SkriptEngine.DebugMode in [dmPaused]) then
  begin
    fPosMark.Line:=Row;
    if fPosMark.Line<>-1 then
      EditFeld.CaretY:=fPosMark.Line;

    EditFeld.CaretX := 1;
    EditFeld.EnsureCursorPosVisible;

    EditFeld.Invalidate;

    AktuVariablen;
  end;

  if SkriptEngine.DebugMode in [dmRun] then
    EditWindow.ActionSkriptPauseUpdate(EditWindow.ActionSkriptPause);
end;

function TEditChild.GetRunning: Boolean;
begin
  result:=DebugMode and (SkriptEngine<>nil) and (SkriptEngine.DebugMode=dmRun);
end;

procedure TEditChild.AktuVariablen;
var
  Dummy : Integer;
  Name  : String;
  pv    : PIFVariant;
  Adress: Integer;
  Parent: TJvCustomInspectorItem;
  SelInd: Integer;
  SavedSettingsData: Array of record
    Expanded    : Boolean;
  end;


  procedure AddItem(Parent: TJvCustomInspectorItem; Name, Text: String);
  var
    NewItem : TJvCustomInspectorItem;
  begin
    PString(Adress)^:=Text;

    if Parent=nil then
    begin
      NewItem := TJvInspectorVarData.New(EditWindow.WatchInspector.Root,Name,typeInfo(String),PString(Adress));
    end
    else
    begin
      NewItem := TJvInspectorVarData.New(Parent,Name,typeInfo(String),PString(Adress));
    end;

    NewItem.ReadOnly:=true;

    inc(Adress,sizeOf(String));
  end;

  function CreateSubKategorie(Parent: TJvCustomInspectorItem; Name: String): TJvCustomInspectorItem;
  begin
    if Parent = nil then
      Parent:=EditWindow.WatchInspector.Root;

    result:=TJvInspectorCustomCategoryItem.Create(Parent, nil);
    result.SortKind:=iskNone;
    result.DisplayName:=Name;
    result.Expanded:=true;
  end;

  procedure VariantToStr(Variant: TPSVariantIFC; Parent: TJvCustomInspectorItem; Name: String);
  var
    Entrys    : Integer;
    Dummy     : Integer;
    Text      : String;
    NewVar    : TPSVariantIFC;
    OffS      : Integer;
    typname   : String;
    Hash      : Cardinal;
    OldParent : TJvCustomInspectorItem;
  begin
     if Variant.aType=nil then
       exit;
       
     case Variant.aType.BaseType of
      btArray:
      begin
        Entrys:=PSDynArrayGetLength(Pointer(Variant.dta^),Variant.aType);
        Parent:=CreateSubKategorie(Parent,Name);
        for Dummy:=0 to Entrys-1 do
        begin
          VariantToStr(PSGetArrayField(Variant,Dummy),Parent,Format('[%d] ',[Dummy]));
        end;
      end;
      btStaticArray:
      begin
        Entrys:=TPSTypeRec_StaticArray(Variant.aType).Size;
        Parent:=CreateSubKategorie(Parent,Name+' = Array');
        NewVar:=Variant;
        for Dummy:=0 to Entrys-1 do
        begin
          Offs := TPSTypeRec_StaticArray(Variant.aType).ArrayType.RealSize * Cardinal(Dummy);
          NewVar.aType := TPSTypeRec_StaticArray(Variant.aType).ArrayType;
          NewVar.Dta := Pointer(IPointer(Variant.dta) + Offs);
          VariantToStr(NewVar,Parent,Format('[%d] ',[Dummy]));
        end;
      end;
      btRecord:
      begin
        Entrys:=TPSTypeRec_Record(Variant.aType).FieldTypes.Count;
        Parent:=CreateSubKategorie(Parent,Name+' = '+Variant.aType.ExportName);
        typname:=LowerCase(Variant.aType.ExportName);
        for Dummy:=0 to Entrys-1 do
        begin
          VariantToStr(PSGetRecField(Variant,Dummy),Parent,FindRecordFieldName(typname,Dummy));
        end;
      end;
      btSet:
      begin
        Hash:=FindSubTypeHash(Variant.aType.ExportName);
        Parent:=CreateSubKategorie(Parent,Name);
        for Dummy:=0 to high(fObjectList) do
        begin
          if (fObjectList[Dummy].Typ=itConstant) and (fObjectList[Dummy].ReturnTyp=Hash) then
          begin
            if script_utils_IsMemberOfSet(fObjectList[Dummy].Nr,Variant.dta) then
              AddItem(Parent,fObjectList[Dummy].OrgName,'True')
            else
              AddItem(Parent,fObjectList[Dummy].OrgName,'False')
          end;
        end;
      end;
      btClass:
      begin
        if TObject(Variant.Dta^) = nil then
        begin
          Text := 'nil';

          AddItem(Parent,Name,Text);
        end
        else
        begin
          try
            OldParent:=Parent;
            Parent:=nil;
            Parent:=CreateSubKategorie(OldParent,Name+' = '+TObject(Variant.Dta^).ClassName);
            Hash:=HashString(Variant.aType.ExportName);
            for Dummy:=0 to high(fObjectList) do
            begin
              if (fObjectList[Dummy].Typ=itField) and (fObjectList[Dummy].Father=Hash) then
              begin
                try
                  Text:=PSVariantToString(Variant,fObjectList[Dummy].OrgName);
                except
                  on E: Exception do
                    Text:=E.Message;
                end;
                AddItem(Parent,fObjectList[Dummy].OrgName,Text);
              end;
            end;
          except
            on E: Exception do
            begin
              Parent.Free;
              AddItem(OldParent,Name,E.Message);
            end;
          end;
        end;
      end;
      else
      begin
        Text:='';
        if (Variant.aType.BaseType in [btU8, btS8, btU16, btS16, btU32, btS32]) and (Variant.aType.ExportName<>'') then
          Text:=FindEnumName(Variant.aType.ExportName,PSGetUInt(Variant.Dta,Variant.aType));

        if Text='' then
        begin
          try
            Text:=PSVariantToString(Variant, '');
          except
            on E: Exception do
              Text:=E.Message;
          end;
        end;

        AddItem(Parent,Name,Text)
      end;
    end;
  end;

  function CountAbsItems(Root: TJvCustomInspectorItem): Integer;
  var
    Dummy: Integer;
  begin
    result:=1;
    for Dummy:=0 to Root.Count-1 do
    begin
      result:=result+CountAbsItems(Root.Items[Dummy]);
    end;
  end;

  function SaveSettings(Root: TJvCustomInspectorItem; Index: Integer): Integer;
  var
    Dummy: Integer;
  begin
    for Dummy:=0 to Root.Count-1 do
    begin
      Index:=SaveSettings(Root.Items[Dummy],Index);
    end;

    SavedSettingsData[Index].Expanded:=Root.Expanded;
    result:=Index+1;
  end;

  function RestoreSettings(Root: TJvCustomInspectorItem; Index: Integer): Integer;
  var
    Dummy: Integer;
  begin
    for Dummy:=0 to Root.Count-1 do
    begin
      Index:=RestoreSettings(Root.Items[Dummy],Index);
    end;

    Root.Expanded:=SavedSettingsData[Index].Expanded;
    result:=Index+1;
  end;

begin
  if not EditWindow.WatchWindow.Visible then
    exit;

  if (SkriptEngine=nil) or (not fTestMode) then
  begin
    EditWindow.WatchInspector.Clear;
    exit;
  end;

  // Einstellungen (Aufgeklappt/Zugeklappt speichern) der
  SetLength(SavedSettingsData,CountAbsItems(EditWindow.WatchInspector.Root));
  SaveSettings(EditWindow.WatchInspector.Root,0);
  SelInd:=EditWindow.WatchInspector.SelectedIndex;

  EditWindow.WatchInspector.BeginUpdate;
  EditWindow.WatchInspector.Clear;

  ClearMemoryPool;
  ZeroMemory(fmemoryPool,20000);
  Adress:=Integer(fMemoryPool);

  Parent:=CreateSubKategorie(nil,'Global');
  for Dummy:=0 to SkriptEngine.GlobalVarNames.Count-1 do
  begin
    Name:=SkriptEngine.GlobalVarNames[Dummy];
    pv:=SkriptEngine.GetGlobalVar(Dummy);
    VariantToStr(NewTPSVariantIFC(pv, False),Parent, Name);
  end;

  Parent:=CreateSubKategorie(nil,'Lokal');
  if SkriptEngine.CurrentProcVars<>nil then
  begin
    for Dummy:=0 to SkriptEngine.CurrentProcVars.Count-1 do
    begin
      Name:=SkriptEngine.CurrentProcVars[Dummy];
      pv:=SkriptEngine.GetProcVar(Dummy);
      VariantToStr(NewTPSVariantIFC(pv, False),Parent, Name);
    end;
  end;

  Parent:=CreateSubKategorie(nil,'Prozedurparamater');
  if SkriptEngine.CurrentProcParams<>nil then
  begin
    for Dummy:=0 to SkriptEngine.CurrentProcParams.Count-1 do
    begin
      Name:=SkriptEngine.CurrentProcParams[Dummy];
      pv:=SkriptEngine.GetProcParam(Dummy);
      VariantToStr(NewTPSVariantIFC(pv, False),Parent, Name);
    end;
  end;

  // Einstellungen zur�ckspielen sofern identisch.
  if CountAbsItems(EditWindow.WatchInspector.Root)=length(SavedSettingsData) then
    RestoreSettings(EditWindow.WatchInspector.Root,0);

  EditWindow.WatchInspector.EndUpdate;

  EditWindow.WatchInspector.SelectedIndex:=SelInd;
end;

function TEditChild.WaitForEvent: Boolean;
begin
  result:=fTestMode and (fMission<>nil) and (fMission.WaitForEvent);
end;

procedure TEditChild.MissionDestroy(Mission: TObject);
begin
  if fMission=Mission then
  begin
    fMission:=nil;
    StopTestModus;
  end;
end;

procedure TEditChild.RunTimeException(Sender: TPSExec; ExError: TPSError; const ExParam: string; ExObject: TObject; ProcNo, Position: Cardinal);
var
  mess: String;
begin
  if (ExObject is Exception) then
    mess:=Exception(ExObject).Message
  else
    mess:=PSErrorToString(ExError,ExParam);

  fErrorLine:=fLastPos.Y;

  mess:=Format('Laufzeitfehler aufgetreten an Position (%d:%d):'#13#10'%s',[fLastPos.Y,fLastPos.X,Mess]);

  EditFeld.CaretY:=fLastPos.Y;
  EditFeld.EnsureCursorPosVisibleEx(true);
  EditFeld.Invalidate;
  ShowMessage(mess);
  StopTestModus;
end;

procedure TEditChild.GetMessage(Sender: TObject);
begin
  game_api_MessageBox(savegame_api_GetLastMessage.Text,'Nachricht');
end;

function TEditChild.SkriptEngine: TPSDebugExec;
begin
  if fMission<>nil then
    result:=fMission.ScriptEngine
  else if SkriptType=stMap then
  begin
    result:=fMapGenerator.SkriptEngine
  end
  else
    result:=nil;
end;

function TEditChild.Compiler: TPSPascalCompiler;
begin
  result:=nil;
  if SkriptType=stMission then
    result:=KD4SaveGame.MissionList.Compiler
  else if SkriptType=stMap then
    result:=fCheckMapGen.Compiler
  else
    Assert(false);
end;

procedure TEditChild.FormDestroy(Sender: TObject);
begin
  if fMapGenerator<>nil then
    fMapGenerator.Free;

  if fCheckMapGen<>nil then
    fCheckMapGen.Free;

  fDefines.Free;
  
  fTypeInfos.Free;
  ClearMemoryPool;
  FreeMemory(fMemoryPool);
end;

procedure TEditChild.SynEditParamShowExecute(Kind: SynCompletionType;
  Sender: TObject; var CurrentInput: String; var x, y: Integer;
  var CanExecute: Boolean);
var
  ParamIndex     : Integer;
  Info           : TParamInfoRecord;
begin
  RebuildLokalObjektList;

  CanExecute :=  FindParameter(EditFeld.LineText,EditFeld.CaretX,Info,ParamIndex);

  TSynCompletionProposal(Sender).ItemList.Clear;

  if CanExecute then
  begin
    TSynCompletionProposal(Sender).Form.CurrentIndex := ParamIndex;
    if Info.Params = '' then
      Info.Params := '"* Keine Parameter *"';

    TSynCompletionProposal(Sender).ItemList.Add(Info.Params);
  end;
end;

procedure TEditChild.FillAutoComplete(var List: TParamInfoArray; Types: TInfoTypes; BuildIn: TSetBoolean; FromFather: Cardinal; Typ: String);
var
  Dummy   : Integer;
  HashT   : Cardinal;
  Father  : TParamInfoRecord;

  function CompareTypes(Typ1: Cardinal; Typ2: Cardinal): Boolean;
  var
    Type1, Type2: Integer;
    Info : TParamInfoRecord;
  begin
    if (Typ1=0) or (Typ2=0) then
    begin
      result:=false;
      exit;
    end;

    if Typ1=Typ2 then
    begin
      result:=true;
      exit;
    end ;

    if not fTypeInfos.FindKey(Typ1,Type1) then
    begin
      result:=false;
      exit;
    end;
    if not fTypeInfos.FindKey(Typ2,Type2) then
    begin
      result:=false;
      exit;
    end;

    result:=script_utils_BaseTypeCompatible(Type1,Type2);

    if result then
    begin
      // Pr�fen, ob Records und Aufz�hlungen kompatibel sind
      if (Type1=btEnum) or (Type1=btRecord) then
      begin
        result:=Typ1=Typ2;
        exit;
      end;
    end;

    if not result then
    begin
      // Klassenkompatibilit�t pr�fen
      if LookUpList(Typ2,Info) then
      begin
        while Info.SubType<>0 do
        begin
          Assert(LookUpList(Info.SubType,Info));
          if Info.ReturnTyp = Typ1 then
          begin
            result:=true;
            exit;
          end;
        end;
      end;
    end;
  end;

  function HasFieldReturnTyp(ReturnTyp: Cardinal; FatherTyp: Cardinal): Boolean;
  var
    Dummy: Integer;
  begin
    result:=false;
    if (FatherTyp=0) or (ReturnTyp=0) then
      exit;

    for Dummy:=0 to high(List) do
    begin
      if List[Dummy].Typ=itConstructor then
        continue;

      if (List[Dummy].Father=FatherTyp) then
      begin
        if (CompareTypes(ReturnTyp,List[Dummy].ReturnTyp)) then
        begin
          result:=true;
          exit;
        end;
        if List[Dummy].HasFields then
        begin
          if HasFieldReturnTyp(ReturnTyp,List[Dummy].Name) then
          begin
            result:=true;
            exit;
          end;
        end;
      end;
      if List[Dummy].Name=FatherTyp then
      begin
        if List[Dummy].SubType<>0 then
        begin
          if HasFieldReturnTyp(ReturnTyp,List[Dummy].SubType) then
          begin
            result:=true;
            exit;
          end;
        end;
      end;
    end;
  end;

  procedure AddAutoComplete(ParamInfo: TParamInfoRecord; FromFather: Cardinal; Sub: String = '');
  var
    Dummy    : Integer;
    Text     : String;
    Subs     : Boolean;
    Insert   : String;
  begin
    if ((Sub <> '') or (ParamInfo.Typ in Types)) and
       (ParamInfo.BuildIn in BuildIn) and
       (ParamInfo.OrgName<>'_MainProc_') and
       (ParamInfo.Father=FromFather) and
       (         (HashT=0) or
                 (CompareTypes(HashT,ParamInfo.ReturnTyp)) or
                 (HashT=ParamInfo.ReturnTyp) or
                 ParamInfo.HasFields
       ) then
    begin
      Text:='';
      Subs:=false;

      if ParamInfo.HasFields and (HashT<>0) and (not CompareTypes(HashT,ParamInfo.ReturnTyp)) then
      begin
        if HasFieldReturnTyp(HashT,ParamInfo.ReturnTyp) then
        begin
          Text:='\column{}\style{+B}'+ParamInfo.OrgName+' ...\style{-B}';
          Insert:=ParamInfo.OrgName;
          Subs:=true;
        end
        else
          exit;
      end
      else
      begin
        if Sub<>'' then
        begin
          Text:='\column{}\image{32}\style{+B}'+ParamInfo.OrgName+'\style{-B}'+ParamInfo.OrgParams;
          Insert:=Sub+'.'+ParamInfo.OrgName
        end
        else
        begin
          Text:='\column{}\style{+B}'+ParamInfo.OrgName+'\style{-B}'+ParamInfo.OrgParams;
          Insert:=ParamInfo.OrgName;
        end;
      end;

      case ParamInfo.Typ of
        itProcedure     : Text:='\image{17}\color{clTeal}procedure\color{clWindowText}'+Text;
        itFunction      : Text:='\image{18}\color{clblue}function\color{clWindowText}'+Text;
        itType          : Text:='\image{30}\color{clOlive}type\color{clWindowText}'+Text;
        itVar           : Text:='\image{16}\color{clMaroon}var\color{clWindowText}'+Text;
        itConstant      : Text:='\image{31}\color{clGreen}const\color{clWindowText}'+Text;
        itField         : Text:='\image{16}\color{clMaroon}field\color{clWindowText}'+Text;
        itConstructor   : Text:='\image{18}\color{clblue}constructor\color{clWindowText}'+Text;
        else
          Assert(false);
      end;

      SynEditAutoComplete.AddItem(Text,Insert);

      if Subs and (Sub = '') and (not (ParamInfo.Typ in [itProcedure,itFunction,itConstructor])) then
      begin
        for Dummy:=0 to high(List) do
          AddAutoComplete(List[Dummy],ParamInfo.ReturnTyp,ParamInfo.OrgName);
      end;
    end;
  end;

begin
  SynEditAutoComplete.ClearList;

  if LookUpList(FromFather,Father) then
  begin
    if Father.SubType<>0 then
    begin
      for Dummy:=0 to high(List) do
      begin
        if List[Dummy].Name=Father.SubType then
        begin
          FillAutoComplete(List,Types,BuildIn,List[Dummy].Name,Typ);
          break;
        end;
      end;
    end;
  end;
  HashT:=HashString(Typ);

  for Dummy:=0 to high(List) do
    AddAutoComplete(List[Dummy],FromFather);

//  if (SynEditAutoComplete.InsertList.Count=0) and (Hasht<>0) then
//    FillAutoComplete(List,Types,FromFather);
end;

procedure TEditChild.SynEditAutoCompleteExecute(Kind: SynCompletionType;
  Sender: TObject; var CurrentInput: String; var x, y: Integer;
  var CanExecute: Boolean);
const
  WithReturnValue : TInfoTypes = [itFunction,itVar,itConstant,itType];
var
  Parser      : TPSPascalParser;
  Token       : TPSPasToken;
  Prev        : TPSPasToken;
  PrevEnd     : Integer;
  Types       : TInfoTypes;
  tmpX        : Integer;
  Father      : Cardinal;
  Line        : String;
  Info        : TParamInfoRecord;
  ParamCount  : Integer;
  Parts       : TStringArray;
  Typ         : String;
  Obj         : String;
  BuildIn     : TSetBoolean;

  Attri       : TSynHighlighterAttributes;
begin
  if EditFeld.GetHighlighterAttriAtRowCol(EditFeld.CaretXY, Obj, Attri) then
  begin
    if (Attri=EditWindow.PascalSyntax.CommentAttri) then
    begin
      CanExecute:=false;
      exit;
    end;
  end;

  RebuildLokalObjektList;

  Line:=EditFeld.LineText;

  Parser:=TPSPascalParser.Create;
  Parser.SetText(Line);

  Types:=[itProcedure,itFunction,itVar,itType];
  Father:=0;
  Typ:='';
  BuildIn:=[true,false];

  CanExecute:=false;

  Prev:=CSTI_EOF;
  Token:=CSTI_EOF;
  PrevEnd:=-1;

  while (Parser.CurrTokenID<>CSTI_EOF) and (Parser.CurrTokenPos<(EditFeld.CaretX-1)) do
  begin
    Prev:=Token;
    PrevEnd:=Parser.CurrTokenPos+length(Parser.OriginalToken);
    Token:=Parser.CurrTokenID;
    // Tritt ein := oder ( auf, so wird ein Wert mit einem R�ckgabewert erwartet
    if (Token=CSTI_Assignment) and (Prev=CSTI_Identifier) then
    begin
      Types:=WithReturnValue;
      if LookUpList(Copy(EditFeld.LineText,1,Parser.CurrTokenPos),Info) then
        Typ:=Copy(Info.OrgParams,3,length(Info.OrgParams));
    end
    else if (Token in [CSTII_do, CSTII_then, CSTII_repeat, CSTII_else, CSTII_begin, CSTI_Semicolon]) then
    begin
      Types:=[itProcedure,itFunction,itVar,itType];
      Father:=0;
      Typ:='';
      BuildIn:=[true,false];
    end
    else if (Token in [CSTI_Equal,CSTI_NotEqual,CSTI_Greater,CSTI_GreaterEqual,CSTI_Less,CSTI_LessEqual]) then
    begin
      Types:=WithReturnValue;
      Obj:=GetLookUpString(EditFeld.LineText,Parser.CurrTokenPos);
      if LookUpList(Obj,Info) then
        Typ:=Copy(Info.OrgParams,3,length(Info.OrgParams));
    end
    else if (Token=CSTI_OpenRound) then
    begin
      Types:=WithReturnValue;
      Typ:='';
    end
    else if (Token=CSTII_for) then
    begin
      Types:=[itVar];
      Typ:='LongInt';
    end
    else if (Token in [CSTII_not,CSTII_and,CSTII_or,CSTII_if,CSTII_while,CSTII_until]) then
    begin
      Types:=WithReturnValue;
      Typ:='Boolean';
    end
    else if (Token=CSTII_to) or (Token=CSTII_downto) then
    begin
      Types:=WithReturnValue;
      Typ:='LongInt';
    end
    else if (Token=CSTII_is) or (Token=CSTII_as) then
    begin
      Types:=[itType];
      Typ:='TObject';
    end;

    Parser.Next;
  end;

  Parser.Free;

  if Token in [CSTI_String,CSTI_Real] then
    exit;

  if (PrevEnd<(EditFeld.CaretX-1)) then
    Prev:=Token;

  if Prev=CSTI_Colon then
    Types:=[itType]
  else if Prev=CSTI_AddressOf then
  begin
    Types:=[itProcedure,itFunction];
    Typ:='';
    BuildIn:=[false];
  end
  else if Prev=CSTI_Period then
  begin
    tmpX:= EditFeld.CaretX-1;
    if tmpX>length(Line) then
      tmpX:=length(Line);

    while (tmpX>0) and (Line[tmpX]<>'.') do
      dec(tmpX);

    dec(tmpX);

    Obj:=GetLookUpString(Line,tmpX);
//    Obj:=GetLookUpObjectName(Obj);

    if LookUpList(LowerCase(Obj),Info) then
    begin
      Father:=Info.ReturnTyp;
      if (Info.Typ=itType) and (LowerCase(Trim(Obj))=LowerCase(Info.OrgName)) then
      begin
        Types:=[itConstructor];
        Typ:='';
      end
      else
        Types:=[itField,itProcedure,itFunction];
    end;
  end
  else if Prev=CSTI_OpenBlock then
  begin
    Types:=[itProcedure,itFunction,itVar,itConstant];
    Father:=0;
    BuildIn:=[true,false];
    Typ:='LongInt';
  end;

  if (Prev<>CSTI_AddressOf) and FindParameter(EditFeld.LineText,EditFeld.CaretX,Info,ParamCount) then
  begin
    Parts:=string_utils_explode(',',Info.Params);
    if ParamCount<=high(Parts) then
    begin
      if Pos(':',Parts[ParamCount])>0 then
      begin
        Typ:=Copy(Parts[ParamCount],Pos(':',Parts[ParamCount])+2,length(Parts[ParamCount]));
        if Typ[length(Typ)-1]=';' then
          Typ:=Copy(Typ,1,length(Typ)-2)
        else
          Typ:=Copy(Typ,1,length(Typ)-1);
      end
      else
        Typ:='';

      Exclude(Types,itProcedure);
    end;
  end;

  CanExecute:=true;
  FillAutoComplete(fObjectList,Types,BuildIn,Father,Typ);
end;

procedure TEditChild.RebuildLokalObjektList(CursorPos: Boolean);
var
  Comp  : TPSPascalCompiler;
  Skript: String;
  Pos   : TBufferCoord;
begin
  Pos:=EditFeld.CaretXY;
  if not CursorPos then
  begin
    if (fPosMark<>nil) and (fPosMark.Line<>-1) then
      Pos.Line:=fPosMark.Line
    else
      Pos.Line:=EditFeld.Lines.Count+1;
  end;

  Skript:=script_utils_Preprocess(EditFeld.Text,Language);
  Skript:=TruncateSourceCode(Skript,Pos);

  SetLength(fObjectList,0);
  fTypeInfos.ClearList;

  SynEditAutoComplete.CancelCompletion;
  
  CompileScript(Skript,AutoCompleteCompilerBeforeCleanUp);
end;

procedure TEditChild.BuildLokalObjektList(Comp: TPSPascalCompiler);
var
  Dummy  : Integer;
  VDummy : Integer;
  Info   : TParamInfoRecord;
  Typ    : TPSType;
  Proc   : TPSRegProc;
  ProcInt: TPSInternalProcedure;
  con    : TPSConstant;
  Father : Cardinal;

  procedure ClearInfoRec;
  begin
    Info.Name:=0;
    Info.OrgName:='';
    Info.Params:='';
    Info.OrgParams:='';
    Info.Father:=0;
    Info.ReturnTyp:=0;
    Info.HasFields:=false;
    Info.SubType:=0;
    Info.Value:='';
    Info.BuildIn:=false;
    Info.DeclarePos.Y:=-1;
  end;

  procedure AddTypeInfo(Hash: Cardinal; BaseType: Integer);
  begin
    fTypeInfos.InsertID(Hash,BaseType);
  end;

  procedure AddInfo(var Info: TParamInfoRecord);
  begin
    if (length(Info.OrgName)<1) or (Info.OrgName[1]='!') or ((Info.OrgName[1] in [' ','_']) and (Info.BuildIn)) then
    begin
      ClearInfoRec;
      exit;
    end;

    Info.Name:=HashString(Info.OrgName);
    SetLength(fObjectList,Length(fObjectList)+1);
    fObjectList[high(fObjectList)]:=Info;
    ClearInfoRec;
  end;

  function TypHasField(Typ: TPSType): Boolean;
  begin
    result:=(Typ is TPSClassType) or (Typ is TPSRecordType);
  end;

  function GetTypeName(Typ: TPSType): String;
  begin
    if Typ is TPSArrayType then
      result:=GetTypeName(TPSArrayType(Typ).ArrayTypeNo)
    else
      result:=script_utils_GetTypeName(Typ);
  end;
  
  procedure AddTypedObject(Name: String; Typ: TPSType; InfoType: TInfoType = itVar);
  begin
    Info.OrgName:=Name;

    Info.OrgParams:=': '+script_utils_GetTypeName(Typ);
    Info.ReturnTyp:=HashString(GetTypeName(Typ));
    Info.HasFields:=TypHasField(Typ);
    Info.typ:=InfoType;
    AddInfo(Info);
  end;

  procedure AddVar(Obj: TObject; InfoType: TInfoType = itVar);
  var
    Typ: TPSType;
  begin
    if Obj.ClassType=TPSVar then
    begin
      Info.DeclarePos.X:=TPSVar(Obj).DeclareCol;
      Info.DeclarePos.Y:=TPSVar(Obj).DeclareRow;

      AddTypedObject(TPSVar(Obj).OrgName,TPSVar(Obj).aType);
    end
    else if Obj.ClassType=TPSProcVar then
    begin
      Info.DeclarePos.X:=TPSProcVar(Obj).DeclareCol;
      Info.DeclarePos.Y:=TPSProcVar(Obj).DeclareRow;

      AddTypedObject(TPSProcVar(Obj).OrgName,TPSProcVar(Obj).aType);
    end
    else if Obj.ClassType=TPSParameterDecl then
    begin
      Info.DeclarePos.X:=TPSParameterDecl(Obj).DeclareCol;
      Info.DeclarePos.Y:=TPSParameterDecl(Obj).DeclareRow;

      AddTypedObject(TPSVar(Obj).OrgName,TPSVar(Obj).aType);
    end
    else
      Assert(false,Obj.ClassName);
  end;

  procedure SetParams(var Info: TParamInfoRecord; Parameters: TPSParametersDecl);
  var
    Params: String;
  begin
    Info.OrgParams:=script_utils_GetParams(Parameters);
    Params:=script_utils_GetParams(Parameters,'"');

    Params:=Copy(Params,Pos('(',Params)+1,length(Params));
    Params:=Copy(Params,1,Pos(')',Params)-1);
    Params:=StringReplace(Params,'";',';",',[rfReplaceAll]);

    Info.Params:=Params;
  end;

  procedure AddProcedure(Proc: TObject);
  var
    Parameters: TPSParametersDecl;
  begin
    if Proc.ClassType=TPSRegProc then
    begin
      Info.OrgName:=TPSRegProc(Proc).OrgName;
      Parameters:=TPSRegProc(Proc).Decl;
      Info.BuildIn:=true;
    end
    else if Proc.ClassType=TPSInternalProcedure then
    begin
      Info.OrgName:=TPSInternalProcedure(Proc).OriginalName;
      if Info.OrgName='Main Proc' then
        Info.OrgName:='_MainProc_';

      Parameters:=TPSInternalProcedure(Proc).Decl;
      Info.BuildIn:=false;
      Info.DeclarePos.X:=TPSInternalProcedure(Proc).DeclareCol;
      Info.DeclarePos.Y:=TPSInternalProcedure(Proc).DeclareRow;
    end
    else if Proc.ClassType=TPSExternalProcedure then
      exit
    else
      Assert(false,Proc.ClassName);

    SetParams(Info,Parameters);

    if Parameters.Result=nil then
      info.Typ:=itProcedure
    else
    begin
      info.Typ:=itFunction;
      info.ReturnTyp:=HashString(GetTypeName(Parameters.Result));
      Info.HasFields:=TypHasField(Parameters.result);
    end;

    AddInfo(Info);
  end;

begin
  ClearInfoRec;

  // Parameter der letzten Funktion (Es wird davon ausgegangen, dass der Cursor
  // in der letzten Funktion steht und somit nur diese Paramter sichtbar sind)
  ProcInt:=nil;
  for Dummy:=Comp.GetProcCount-1 downto 0 do
  begin
    if (Comp.GetProc(Dummy) is TPSInternalProcedure) then
    begin
      ProcInt:=TPSInternalProcedure(Comp.GetProc(Dummy));
      break;
    end;
  end;
  if ProcInt<>nil then
  begin
    if ProcInt.Decl<>nil then
    begin
      for Dummy:=0 to ProcInt.Decl.ParamCount-1 do
        AddVar(ProcInt.Decl.Params[Dummy]);

      if ProcInt.Decl.Result<>nil then
        AddTypedObject('result',ProcInt.Decl.Result);
    end;
    
    for VDummy:=0 to ProcInt.ProcVars.Count-1 do
    begin
      AddVar(ProcInt.ProcVars[VDummy]);
    end;
  end;

  // Globale Variablen
  for Dummy:=0 to Comp.GetVarCount-1 do
    AddVar(Comp.GetVar(Dummy));

  // Konstanten
  for Dummy:=0 to Comp.GetConstCount-1 do
  begin
    con:=TPSConstant(Comp.GetConst(Dummy));

    Info.OrgName:=con.OrgName;

    Info.OrgParams:=': '+script_utils_GetTypeName(con.FValue.FType);
    Info.ReturnTyp:=HashString(GetTypeName(con.FValue.FType));
    Info.HasFields:=TypHasField(con.FValue.FType);
    Info.Nr:=Con.Value.ts32;
    Info.Typ:=itConstant;
    Info.DeclarePos.X:=con.FDeclareCol;
    Info.DeclarePos.Y:=con.FDeclareRow;
    Info.Value:=script_utils_GetStringValue(con);
    AddInfo(Info);
  end;

  // Eigene Funktionen
  // Bei 1 beginnen (0 = main_proc)
  for Dummy:=0 to Comp.GetProcCount-1 do
  begin
    ProcInt:=TPSInternalProcedure(Comp.GetProc(Dummy));
    if ProcInt is TPSInternalProcedure then
      AddProcedure(ProcInt);
  end;

  // registrierte Funktionen
  for Dummy:=0 to Comp.GetRegProcCount-1 do
  begin
    Proc:=Comp.GetRegProc(Dummy);
    if Proc is TPSRegProc then
      AddProcedure(Proc);
  end;

  // Typen �bernehmen
  for Dummy:=0 to Comp.GetTypeCount-1 do
  begin
    Typ:=Comp.GetType(Dummy);

    Info.OrgName:=Typ.OriginalName;

    Info.ReturnTyp:=HashString(Typ.OriginalName);
    Info.Params:='"CastValue"';
    Info.Typ:=itType;

    Info.DeclarePos.X:=Typ.DeclareCol;
    Info.DeclarePos.Y:=Typ.DeclareRow;

    Info.OrgParams:=': '+Info.OrgName;

    if Typ.OriginalName<>'' then
      AddTypeInfo(Info.ReturnTyp,Typ.BaseType);

    if Typ is TPSSetType then
      Info.SubType:=HashString(TPSSetType(Typ).SetType.OriginalName)
    else if Typ is TPSArrayType then
      Info.SubType:=HashString(TPSArrayType(Typ).ArrayTypeNo.OriginalName)
    else if Typ is TPSClassType then
    begin
      if TPSClassType(Typ).Cl.ClassInheritsFrom<>nil then
        Info.SubType:=HashString(TPSClassType(Typ).Cl.ClassInheritsFrom.aType.OriginalName);
    end;

    AddInfo(Info);

    Father:=HashString(Typ.OriginalName);

    if Typ is TPSRecordType then
    begin
      for VDummy:=0 to TPSRecordType(Typ).RecValCount-1 do
      begin
        ClearInfoRec;
        with TPSRecordType(Typ).RecVal(VDummy) do
        begin
          Info.OrgName:=FieldOrgName;

          Info.OrgParams:=': '+script_utils_GetTypeName(aType);
          Info.Typ:=itField;
          Info.Father:=Father;
          Info.HasFields:=TypHasField(aType);

          Info.ReturnTyp:=HashString(GetTypeName(aType));
          Info.Nr:=VDummy;
          AddInfo(Info);
        end;
      end;
    end;

    if Typ is TPSClassType then
    begin
      for VDummy:=0 to TPSClassType(Typ).Cl.Count-1 do
      begin
        ClearInfoRec;
        with TPSClassType(Typ).Cl.Items[VDummy] do
        begin
          Info.OrgName:=OrgName;
          Info.Father:=Father;

          SetParams(Info,Decl);

          if Decl.Result<>nil then
          begin
            Info.ReturnTyp:=HashString(GetTypeName(Decl.Result));
            Info.HasFields:=TypHasField(Decl.Result);
          end;

          if ClassType=TPSDelphiClassItemProperty then
          begin
            Info.Typ:=itField;
            Info.OrgParams:=StringReplace(Info.OrgParams,'(','[',[]);
            Info.OrgParams:=StringReplace(Info.OrgParams,')',']',[]);
          end
          else if ClassType=TPSDelphiClassItemConstructor then
            Info.Typ:=itConstructor
          else
          begin
            if Decl.Result=nil then
              Info.Typ:=itProcedure
            else
              Info.Typ:=itFunction;
          end;
          Info.BuildIn:=true;
          AddInfo(Info);
        end;
      end;
    end;
  end;
end;

procedure TEditChild.RebuildCodeExplorer;
var
  Skript      : string;
  BufferCoord : TBufferCoord;
begin
  Skript:=EditFeld.Text;
  Skript:=script_utils_Preprocess(EditFeld.Text,Language+';DEBUG;SKRIPTEDIT');
  CompileScript(Skript,CodeExplorerBeforeCleanUp);
end;

procedure TEditChild.BuildCodeExplorer(Comp: TPSPascalCompiler);
var
  Explorer  : TTreeView;
  Dummy     : Integer;

  ProcInt   : TPSInternalProcedure;
  PSVar     : TPSVar;
  PSType    : TPSType;
  PSCon     : TPSConstant;

  Node      : TTreeNode;
  Image     : Integer;

  function NewNode(Parent: TTreeNode; Name: String; Image: Integer; JumpTo: Integer): TTreeNode;
  var
    Dummy: Integer;
  begin
    result:=nil;
    if Parent=nil then
    begin
      for Dummy:=0 to Explorer.Items.Count-1 do
      begin
        if Explorer.Items[Dummy].Text=Name then
          result:=Explorer.Items[Dummy]
      end;

      if result=nil then
        result:=Explorer.Items.Add(nil,Name);
    end
    else
    begin
      for Dummy:=0 to Node.Count-1 do
      begin
        if Node.Item[Dummy].Text=Name then
          result:=Node.Item[Dummy];
      end;

      if result=nil then
        result:=Explorer.Items.AddChild(Parent,Name);
    end;

    result.ImageIndex:=Image;
    result.SelectedIndex:=Image;
    result.Data:=Pointer(JumpTo);
    if Parent<>nil then
      Parent.Expand(false);
  end;

  procedure SetDeleteFlag(Node: TTreeNode);
  var
    Dummy: Integer;
  begin
    if Node=nil then
    begin
      for Dummy:=0 to Explorer.Items.Count-1 do
        SetDeleteFlag(Explorer.Items[Dummy]);
    end
    else
    begin
      Node.Data:=Pointer(-1);
      for Dummy:=0 to Node.Count-1 do
        SetDeleteFlag(Node.Item[Dummy]);
    end;
  end;

  procedure DeleteNode(Node: TTreeNode);
  var
    Dummy: Integer;
  begin
    if Node=nil then
    begin
      for Dummy:=Explorer.Items.Count-1 downto 0 do
        DeleteNode(Explorer.Items[Dummy]);
    end
    else
    begin
      for Dummy:=Node.Count-1 downto 0 do
        DeleteNode(Node.Item[Dummy]);

      if (Node.Count=0) and (Integer(Node.Data)<0) then
        Node.Free
    end;
  end;
begin
  Explorer:=EditWindow.CodeExplorer;
  Explorer.Items.BeginUpdate;

  SetDeleteFlag(nil);

  Node:=NewNode(nil,'Prozeduren / Funktionen',15,-4);
  // Eigene Funktionen
  // Bei 1 beginnen (0 = main_proc)
  for Dummy:=1 to Comp.GetProcCount-1 do
  begin
    if Comp.GetProc(Dummy) is TPSInternalProcedure then
    begin
      ProcInt:=TPSInternalProcedure(Comp.GetProc(Dummy));
      if TPSInternalProcedure(ProcInt).decl.Result=nil then
        Image:=17
      else
        Image:=18;

      NewNode(Node,TPSInternalProcedure(ProcInt).OriginalName,Image,TPSInternalProcedure(ProcInt).DeclareRow);
    end;
  end;

  Node:=NewNode(nil,'Variablen',15,-3);

  // Globale Variablen
  for Dummy:=0 to Comp.GetVarCount-1 do
  begin
    PSVar:=Comp.GetVar(Dummy);
    if PSVar.DeclarePos<>0 then
       NewNode(Node,PSVar.OrgName,16,PSVar.DeclareRow);
  end;

  Node:=NewNode(nil,'Konstanten',15,-2);

  // Konstanten
  for Dummy:=0 to Comp.GetConstCount-1 do
  begin
    PSCon:=TPSConstant(Comp.GetConst(Dummy));

    if PSCon.DeclareRow>1 then
      NewNode(Node,PSCon.OrgName,31,PSCon.DeclareRow);
  end;

  Node:=NewNode(nil,'Typen',15,-1);

  // Typen �bernehmen
  for Dummy:=0 to Comp.GetTypeCount-1 do
  begin
    PSType:=Comp.GetType(Dummy);
    if (PSType.DeclarePos<>$FFFFFFFF) and (PSType.DeclarePos<>0) and (PSType.OriginalName<>'') then
      NewNode(Node,PSType.OriginalName,30,PSType.DeclareRow);
  end;

  DeleteNode(nil);

  Explorer.CustomSort(nil,0);

  Explorer.Items.EndUpdate;
end;

function TEditChild.LookUpList(LookUp: String; var ParamInfo: TParamInfoRecord): Boolean;
var
  Dummy: Integer;
  Hash : Cardinal;
  Parts: TStringArray;

  FindString : String;
  Parent     : Cardinal;

  function FindEntry(LookUp: String; Parent: Cardinal; var ParamInfo: TParamInfoRecord): Boolean;
  var
    Dummy: Integer;
    Hash : Cardinal;
  begin
    Hash:=HashString(LookUp);
    result:=false;
    for Dummy:=0 to high(fObjectList) do
    begin
      if (fObjectList[Dummy].Name=Hash) and (fObjectList[Dummy].Father=Parent) then
      begin
        result:=true;
        ParamInfo:=fObjectList[Dummy];
        exit;
      end
    end;

    // Keinen passenden Eintrag gefunden. Vorfahren pr�fen
    if LookUpList(Parent,ParamInfo) then
    begin
      if ParamInfo.SubType<>0 then
      begin
        result:=FindEntry(LookUp,ParamInfo.SubType,ParamInfo);
      end;
    end;
  end;

  function RemoveParams(Text: String): String;
  var
    Dummy    : Integer;
    Pos      : Integer;
    Brackets : Integer;
    InString : Boolean;
  begin
    Brackets:=0;
    InString:=false;
    Pos:=1;
    SetLength(result,length(Text));
    for Dummy:=1 to length(Text) do
    begin
      case Text[Dummy] of
        '(','[': inc(Brackets);
        ')',']': dec(Brackets);
        '''': InString:=not InString;
        ' ': continue;
        else
          if (Brackets=0) and (not InString) then
          begin
            result[Pos]:=Text[Dummy];
            inc(Pos);
          end;
      end;
    end;
    SetLength(result,Pos-1);
  end;

begin
  LookUp:=RemoveParams(LookUp);

{  if Pos('.',LookUp)=0 then
  begin
    // Einfacher Bezeichner wird gesucht
    Hash:=HashString(LookUp);
    result:=false;
    for Dummy:=0 to high(fObjectList) do
    begin
      if (fObjectList[Dummy].Name=Hash) and (fObjectList[Dummy].Father=0) then
      begin
        result:=true;
        ParamInfo:=fObjectList[Dummy];
        exit;
      end
    end;
  end
  else
  begin          }

    // Verkn�pfter bezeichner wird gesucht
    Parts:=string_utils_explode('.',LookUp);
    result:=false;
    Parent:=0;
    for Dummy:=0 to high(Parts) do
    begin
    if not FindEntry(Parts[Dummy],Parent,ParamInfo) then
        exit;

      Parent:=ParamInfo.ReturnTyp;
    end;
    result:=true;
//  end;
end;

function TEditChild.LookUpList(LookUp: Cardinal; var ParamInfo: TParamInfoRecord): Boolean;
var
  Dummy: Integer;
begin
  result:=false;
  for Dummy:=0 to high(fObjectList) do
  begin
    if (fObjectList[Dummy].Name=LookUp) and (fObjectList[Dummy].Father=0) then
    begin
      result:=true;
      ParamInfo:=fObjectList[Dummy];
      exit;
    end
  end;
end;

procedure TEditChild.EditFeldKeyPress(Sender: TObject; var Key: Char);
begin
  if (Key=' ') and (GetKeyState(VK_CONTROL)<0) then
    Key:=#0;
end;

procedure TEditChild.ClearMemoryPool;
var
  Adress: PInteger;
begin
  Adress:=PInteger(fMemoryPool);
  while Adress^<>0 do
  begin
    PString(Adress)^:='';
    inc(Adress);
  end;
end;

function TEditChild.GetWatchValue(WatchString: String): String;
var
  Value: PIFVariant;
  Info : TParamInfoRecord;
  Vari : TPSVariantIFC;
  x    : Integer;
  Val  : String;
  Ende : Integer;
  Nr   : Integer;

  function GetVariantForName(VarName: String): PIFVariant;
  var
    i: Longint;
    s1: string;
  begin
    s1 := FastUppercase(VarName);
    result := nil;
    if SkriptEngine.CurrentProcVars<>nil then
    begin
      for i := 0 to SkriptEngine.CurrentProcVars.Count -1 do
      begin
        if FastUppercase(SkriptEngine.CurrentProcVars[i]) =  s1 then
        begin
          result := SkriptEngine.GetProcVar(i);
          break;
        end;
      end;
    end;
    if result = nil then
    begin
      if SkriptEngine.CurrentProcParams<>nil then
      begin
        for i := 0 to SkriptEngine.CurrentProcParams.Count -1 do
        begin
          if FastUppercase(SkriptEngine.CurrentProcParams[i]) =  s1 then
          begin
            result := SkriptEngine.GetProcParam(i);
            break;
          end;
        end;
      end;
    end;
    if result = nil then
    begin
      for i := 0 to SkriptEngine.GlobalVarNames.Count -1 do
      begin
        if FastUpperCase(SkriptEngine.GlobalVarNames[i]) =  s1 then
        begin
          result := SkriptEngine.GetGlobalVar(i);
          break;
        end;
      end;
    end;
  end;

begin
  RebuildLokalObjektList(false);
  
  Value:=nil;
  result:='';
  if not DebugMode then
    exit;

  if Pos('(',WatchString)<>0 then
    exit;
    
  if (Pos('.',WatchString)=0) and (Pos('[',WatchString)=0) then
  begin
    // Einfacher Bezeichner
    Value:=GetVariantForName(WatchString);

    if Value=nil then
    begin
      // Nach einer Konstanten suche
      if LookUpList(WatchString,Info) then
      begin
        if Info.Typ=itConstant then
          result:= Info.Value;
      end;
      exit;
    end;

    Vari:=NewTPSVariantIFC(Value, False);
  end
  else
  begin
    Val:='';
    x:=1;
    Ende:=length(WatchString);
    repeat
      if WatchString[x] in ['.','[',']'] then
      begin
        Value:=GetVariantForName(Val);
        Val:='';
        inc(x);
      end;
      Val:=Val+WatchString[x];
      inc(x);
    until (x>Ende);

    if Value=nil then
      exit;

    Vari:=NewTPSVariantIFC(Value, False);
    if Vari.aType.BaseType=btRecord then
    begin
      Nr:=FindRecordFieldNr(Vari.aType.ExportName,Val);
      if Nr=-1 then
        exit
      else
      begin
        Vari:=PSGetRecField(Vari,Nr);
      end;
    end
    else if Vari.aType.BaseType=btClass then
    begin
      if TObject(Vari.dta^) = nil then
      begin
        result:='nicht verf�gbarer Wert';
        exit;
      end
      else
      begin
        result:=PSVariantToString(Vari,Val);
        exit;
      end;
    end
    else
      exit;
  end;
    
  result:=VariantToString(Vari);
end;

function TEditChild.VariantToString(Variant: TPSVariantIFC): String;
var
  Entrys    : Integer;
  Dummy     : Integer;
  Text      : String;
  NewVar    : TPSVariantIFC;
  OffS      : Integer;
  typname   : String;
  Hash      : Cardinal;
begin
  result:='';

  case Variant.aType.BaseType of
    btArray:
    begin
      Entrys:=PSDynArrayGetLength(Pointer(Variant.dta^),Variant.aType);
      for Dummy:=0 to Entrys-1 do
        result:=result+Format(', [%d]: ',[Dummy])+VariantToString(PSGetArrayField(Variant,Dummy));

      result:='('+Copy(result,3,length(result))+')';
    end;
    btStaticArray:
    begin
      Entrys:=TPSTypeRec_StaticArray(Variant.aType).Size;
      NewVar:=Variant;
      for Dummy:=0 to Entrys-1 do
      begin
        Offs := TPSTypeRec_StaticArray(Variant.aType).ArrayType.RealSize * Cardinal(Dummy);
        NewVar.aType := TPSTypeRec_StaticArray(Variant.aType).ArrayType;
        NewVar.Dta := Pointer(IPointer(Variant.dta) + Offs);

        result:=result+Format(', [%d]: ',[Dummy])+VariantToString(PSGetArrayField(Variant,Dummy));
      end;
      result:='('+Copy(result,3,length(result))+')';
    end;
    btRecord:
    begin
      Entrys:=TPSTypeRec_Record(Variant.aType).FieldTypes.Count;
      typname:=LowerCase(Variant.aType.ExportName);
      for Dummy:=0 to Entrys-1 do
      begin
        result:=result+Format(', %s: ',[FindRecordFieldName(typname,Dummy)])+VariantToString(PSGetRecField(Variant,Dummy));
      end;
      result:='('+Copy(result,3,length(result))+')';
    end;
    btSet:
    begin
      Hash:=FindSubTypeHash(Variant.aType.ExportName);
      for Dummy:=0 to high(fObjectList) do
      begin
        if (fObjectList[Dummy].Typ=itConstant) and (fObjectList[Dummy].ReturnTyp=Hash) then
        begin
          if script_utils_IsMemberOfSet(fObjectList[Dummy].Nr,Variant.dta) then
            result:=result+', '+fObjectList[Dummy].OrgName;
        end;
      end;
      result:='['+Copy(result,3,length(result))+']';
    end;
    btClass:
    begin
      if TObject(Variant.Dta^) = nil then
        result := 'nil'
      else
      begin
        Hash:=HashString(Variant.aType.ExportName);
        for Dummy:=0 to high(fObjectList) do
        begin
          if (fObjectList[Dummy].Typ=itField) and (fObjectList[Dummy].Father=Hash) then
          begin
            result:=result+Format(', %s: ',[fObjectList[Dummy].OrgName])+PSVariantToString(Variant,fObjectList[Dummy].OrgName);
          end;
        end;

        result:='('+Copy(result,3,length(result))+')';
      end;
    end;
    else
    begin
      if (Variant.aType.BaseType in [btU8, btS8, btU16, btS16, btU32, btS32]) and (Variant.aType.ExportName<>'') then
        result:=FindEnumName(Variant.aType.ExportName,PSGetUInt(Variant.Dta,Variant.aType));

      if result='' then
      begin
        try
          result:=PSVariantToString(Variant, '');
        except
          on E: Exception do
            result:=E.Message;
        end;
      end;
    end;
  end;
end;

function TEditChild.FindRecordFieldName(RecName: String; FieldNo: Integer): String;
var
  Dummy: Integer;
  Hash : Cardinal;
begin
  Hash:=HashString(RecName);
  for Dummy:=high(fObjectList) downto 0 do
  begin
    if (fObjectList[Dummy].Father=Hash) and (fObjectList[Dummy].Nr=FieldNo) then
    begin
      result:=fObjectList[Dummy].OrgName;
      exit;
    end
  end;
  result:=Format('[%d]',[FieldNo]);
end;

function TEditChild.FindRecordFieldNr(RecName: String; FieldName: String): Integer;
var
  Dummy: Integer;
  Hash : Cardinal;
  HashN: Cardinal;
begin
  Hash:=HashString(RecName);
  HashN:=HashString(FieldName);
  for Dummy:=high(fObjectList) downto 0 do
  begin
    if (fObjectList[Dummy].Father=Hash) and (fObjectList[Dummy].Name=HashN) then
    begin
      result:=fObjectList[Dummy].Nr;
      exit;
    end
  end;
  result:=-1;
end;

function TEditChild.FindEnumName(TypName: String; Value: Integer): String;
var
  Dummy: Integer;
  Hash : Cardinal;
begin
  Hash:=HashString(TypName);
  for Dummy:=high(fObjectList) downto 0 do
  begin
    if (fObjectList[Dummy].ReturnTyp=Hash) and (fObjectList[Dummy].Nr=Value) and (fObjectList[Dummy].Typ=itConstant) then
    begin
      result:=fObjectList[Dummy].OrgName;
      exit;
    end;
  end;
end;

function TEditChild.FindSubTypeHash(TypName: String): Cardinal;
var
  Dummy: Integer;
  Hash : Cardinal;
begin
  result:=0;
  Hash:=HashString(TypName);
  for Dummy:=high(fObjectList) downto 0 do
  begin
    if (fObjectList[Dummy].Name=Hash) then
    begin
      result:=fObjectList[Dummy].SubType;
      exit;
    end;
  end;
end;

//** Codefolding: http://www.delphipraxis.net/topic61765.html&postdays=0&postorder=asc&highlight=codefolding&start=0
(*procedure TEditChild.SetCodefoldingRegions;
begin
  EditFeld.CodeFolding.FoldRegions.Add(rtKeyWord,false,false,true,'begin','end');

  EditFeld.CodeFolding.FoldRegions.SkipRegions.Add('//','','',itSingleLineComment);
  EditFeld.CodeFolding.FoldRegions.SkipRegions.Add('{','}','',itMultiLineComment);
end;*)

procedure TEditChild.EditFeldPaintTransient(Sender: TObject;
  Canvas: TCanvas; TransientType: TTransientType);
const AllBrackets = ['{','[','(','<','}',']',')','>'];
var Editor: TSynEdit;
    OpenChars: array[0..2] of Char;
    CloseChars: array[0..2] of Char;

  function CharToPixels(P: TBufferCoord): TPoint;
  begin
    Result:=Editor.RowColumnToPixels(Editor.BufferToDisplayPos(P));
  end;

var P: TBufferCoord;
    Pix: TPoint;
    D     : TDisplayCoord;
    S: String;
    I: Integer;
    Attri: TSynHighlighterAttributes;
    start: Integer;
    TmpCharA, TmpCharB: Char;
begin
 if TSynEdit(Sender).SelAvail then exit;
  Editor := TSynEdit(Sender);
//if you had a highlighter that used a markup language, like html or xml, then you would want to highlight
//the greater and less than signs as well as illustrated below

//  if (Editor.Highlighter = shHTML) or (Editor.Highlighter = shXML) then
//    inc(ArrayLength);

  for i := 0 to 2 do
    case i of
      0: begin OpenChars[i] := '('; CloseChars[i] := ')'; end;
      1: begin OpenChars[i] := '{'; CloseChars[i] := '}'; end;
      2: begin OpenChars[i] := '['; CloseChars[i] := ']'; end;
      3: begin OpenChars[i] := '<'; CloseChars[i] := '>'; end;
    end;

  P := Editor.CaretXY;
  D := Editor.DisplayXY;

  Start := Editor.SelStart;

  if (Start > 0) and (Start <= length(Editor.Text)) then
    TmpCharA := Editor.Text[Start]
  else TmpCharA := #0;

  if (Start < length(Editor.Text)) then
    TmpCharB := Editor.Text[Start + 1]
  else TmpCharB := #0;

  if not(TmpCharA in AllBrackets) and not(TmpCharB in AllBrackets) then exit;
  S := TmpCharB;
  if not(TmpCharB in AllBrackets) then
  begin
    P.Char := P.Char - 1;
    S := TmpCharA;
  end;
  Editor.GetHighlighterAttriAtRowCol(P, S, Attri);

  if (Editor.Highlighter.SymbolAttribute = Attri) then
  begin
    for i := low(OpenChars) to High(OpenChars) do
    begin
      if (S = OpenChars[i]) or (S = CloseChars[i]) then
      begin
        Pix := CharToPixels(P);

        Editor.Canvas.Font.Color:=Attri.Foreground;
        Editor.Canvas.Brush.Style:=bsClear;

        if (TransientType = ttAfter) then
          Editor.Canvas.Pen.Color:=clRed
        else
        begin
          if Editor.Highlighter.SymbolAttribute.Background = clNone then
          begin
            Editor.Canvas.Pen.Color:=Editor.Color;
          end
          else
            Editor.Canvas.Pen.Color:=Editor.Highlighter.SymbolAttribute.Background;
        end;

        if (TransientType <> ttAfter) then
          Editor.Canvas.Rectangle(Bounds(Pix.X, Pix.Y+Editor.LineHeight-2,Editor.CharWidth,2));

        Editor.Canvas.TextOut(Pix.X, Pix.Y, S);

        if (TransientType = ttAfter) then
          Editor.Canvas.Rectangle(Bounds(Pix.X, Pix.Y+Editor.LineHeight-2,Editor.CharWidth,2));

        P := Editor.GetMatchingBracketEx(P);

        if (P.Char > 0) and (P.Line > 0) then
        begin
          Pix := CharToPixels(P);
          if Pix.X > Editor.Gutter.Width then
          begin
            if (TransientType <> ttAfter) then
              Editor.Canvas.Rectangle(Bounds(Pix.X, Pix.Y+Editor.LineHeight-2,Editor.CharWidth,2));

            if S = OpenChars[i] then
              Editor.Canvas.TextOut(Pix.X, Pix.Y, CloseChars[i])
            else
              Editor.Canvas.TextOut(Pix.X, Pix.Y, OpenChars[i]);

            if (TransientType = ttAfter) then
              Editor.Canvas.Rectangle(Bounds(Pix.X, Pix.Y+Editor.LineHeight-2,Editor.CharWidth,2));

          end;
        end;
      end; //if
    end;//for i :=
    Editor.Canvas.Brush.Style := bsSolid;
  end;
end;

procedure TEditChild.EditFeldProcessCommand(Sender: TObject;
  var Command: TSynEditorCommand; var AChar: Char; Data: Pointer);
const
  InProcessing: Boolean = false;
  AutoDouble  : Array[1..4] of Char = ('(','[','{','''');
  ReplacedBy  : Array[1..4] of Char = (')',']','}','''');
var
  PrevLine: Integer;
  Prev    : String;
  Ch,Ch1  : Char;
  Dummy   : Integer;
  Indent  : Integer;
  AddEnd  : Boolean;

  procedure InsertText(Text: String; MoveBack: Boolean = true);
  var
    Dummy: Integer;
  begin
    EditFeld.BeginUndoBlock;
    for Dummy:=1 to length(Text) do
    begin
      EditFeld.ExecuteCommand(ecChar,Text[Dummy],nil);
    end;

    if MoveBack then
    begin
      for Dummy:=1 to length(Text) do
        EditFeld.ExecuteCommand(ecLeft,#0,nil);
    end;

    EditFeld.EndUndoBlock;
  end;

  function GetChar(Line: String; Idx: Integer): Char;
  begin
    if (Idx>0) and (Length(Line)>=Idx) then
      result:=Line[Idx]
    else
      result:=#0;
  end;

begin
  if EditFeld.ReadOnly then
    exit;
    
  if InProcessing then
    exit;

  InProcessing:=true;

  case Command of
    ecChar:
    begin
      if AChar=' ' then
      begin
        if EditFeld.WordAtCursor='if' then
        begin
          EditFeld.ExecuteCommand(ecChar,' ',nil);
          EditFeld.ExecuteCommand(ecChar,'(',nil);
          InsertText(') then');

          Command:=ecNone;
        end
        else if EditFeld.WordAtCursor='while' then
        begin
          EditFeld.ExecuteCommand(ecChar,' ',nil);
          EditFeld.ExecuteCommand(ecChar,'(',nil);
          InsertText(') do');

          Command:=ecNone;
        end;
      end;

      Prev:=EditFeld.Lines[EditFeld.CaretY-1];
      Ch:=GetChar(Prev,EditFeld.CaretX);

      for Dummy:=low(AutoDouble) to High(AutoDouble) do
      begin
        if AutoDouble[Dummy]=AChar then
        begin
          if Ch=AChar then
          begin
            EditFeld.ExecuteCommand(ecRight,#0,nil);
            Command:=ecNone;
            break;
          end;
          InsertText(ReplacedBy[Dummy]);
          InProcessing:=false;
          break;
        end;

        if ReplacedBy[Dummy]=AChar then
        begin
          if Ch=AChar then
          begin
            EditFeld.ExecuteCommand(ecRight,#0,nil);
            Command:=ecNone;
            break;
          end;
        end;
      end;
    end;
    ecLineBreak:
    begin
      PrevLine:=EditFeld.CaretY;
      Prev:=lowercase(trim(Copy(EditFeld.Lines[PrevLine-1],1,EditFeld.CaretX)));
      if Pos(' ',Prev)<>0 then
        Prev:=Copy(Prev,1,Pos(' ',Prev)-1);

      if Prev='begin' then
      begin
        // Pr�fen, ob die n�chste Zeile genauso einger�ckt ist wie diese
        Indent:=Pos('b',lowercase(EditFeld.Lines[EditFeld.CaretY-1]));
        AddEnd:=true;
        Prev:='';
        while (Trim(Prev)='') and (EditFeld.Lines.Count>PrevLine) do
        begin
          Prev:=EditFeld.Lines[PrevLine];
          inc(PrevLine);
        end;
        if Trim(Prev)<>'' then
        begin
          for Dummy:=1 to length(Prev) do
          begin
            if Prev[Dummy]<>' ' then
            begin
              if Dummy>Indent then
                AddEnd:=false
              else if (Dummy=Indent) and (Copy(Trim(Prev),1,3)='end') then
                AddEnd:=false;

              break;
            end;
          end;
        end;

        if AddEnd then
        begin
          EditFeld.BeginUndoBlock;
          EditFeld.ExecuteCommand(ecLineBreak,#0,nil);
          EditFeld.ExecuteCommand(ecLineBreak,#0,nil);
          InsertText('end;',false);
          EditFeld.ExecuteCommand(ecUp,#0,nil);
          EditFeld.ExecuteCommand(ecLeft,#0,nil);
          EditFeld.ExecuteCommand(ecLeft,#0,nil);
          EditFeld.EndUndoBlock;

          Command:=ecNone;
        end
        else
        begin
          EditFeld.ExecuteCommand(ecLineBreak,#0,nil);
          EditFeld.ExecuteCommand(ecRight,#0,nil);
          EditFeld.ExecuteCommand(ecRight,#0,nil);
          Command:=ecNone;
        end;

      end
      else if (Prev='var') or (Prev='const') or (Prev='type') and AutoIndent then
      begin
        EditFeld.ExecuteCommand(ecLineBreak,#0,nil);
        EditFeld.ExecuteCommand(ecRight,#0,nil);
        EditFeld.ExecuteCommand(ecRight,#0,nil);
        Command:=ecNone;
      end
      else if (Prev='repeat') then
      begin
        EditFeld.BeginUndoBlock;
        EditFeld.ExecuteCommand(ecLineBreak,#0,nil);
        EditFeld.ExecuteCommand(ecLineBreak,#0,nil);
        InsertText('until',false);
        EditFeld.ExecuteCommand(ecUp,#0,nil);
        EditFeld.ExecuteCommand(ecLeft,#0,nil);
        EditFeld.ExecuteCommand(ecLeft,#0,nil);
        EditFeld.ExecuteCommand(ecLeft,#0,nil);
        EditFeld.EndUndoBlock;

        Command:=ecNone;
      end;
      // Deaktiviert. Ist zu unsch�n
{      else if (Prev='function') or (Prev='procedure') then
      begin
        AddEnd:=true;
        EditFeld.BeginUndoBlock;
        EditFeld.ExecuteCommand(ecLineBreak,#0,nil);
        InsertText('begin',false);
        EditFeld.ExecuteCommand(ecLineBreak,#0,nil);
        EditFeld.ExecuteCommand(ecLineBreak,#0,nil);
        InsertText('end;',false);
        EditFeld.ExecuteCommand(ecUp,#0,nil);
        EditFeld.ExecuteCommand(ecLeft,#0,nil);
        EditFeld.ExecuteCommand(ecLeft,#0,nil);
        EditFeld.EndUndoBlock;
        Command:=ecNone;
      end;}
    end;
    ecDeleteChar,ecDeleteLastChar:
    begin
      Prev:=EditFeld.Lines[EditFeld.CaretY-1];

      if Command=ecDeleteChar then
      begin
        Ch:=GetChar(Prev,EditFeld.CaretX+1);
        Ch1:=GetChar(Prev,EditFeld.CaretX);
      end
      else
      begin
        Ch:=GetChar(Prev,EditFeld.CaretX);
        Ch1:=GetChar(Prev,EditFeld.CaretX-1);
      end;

      for Dummy:=low(AutoDouble) to High(AutoDouble) do
      begin
        if (AutoDouble[Dummy]=Ch1) and (ReplacedBy[Dummy]=Ch) then
        begin
          EditFeld.ExecuteCommand(ecDeleteChar,#0,nil);
          break;
        end;
      end;
    end;
  end;

  case Command of
    ecLeft..ecGotoXY,ecSelLeft..ecCopy,ecGotoMarker0..ecGotoMarker9,ecDeleteLastChar..ecString:
      HighlightLine(-1);
  end;
  
  case Command of
    ecDeleteLastChar..ecPaste:
    begin
      CodeExplorerTimer.Enabled:=false;
      CodeExplorerTimer.Enabled:=true;
    end;
  end;

  InProcessing:=false;
end;

procedure TEditChild.SynEditAutoCompleteCodeCompletion(Sender: TObject;
  var Value: String; Shift: TShiftState; Index: Integer; EndToken: Char);
begin
  if EndToken in ['''','@'] then
    Value:=EditFeld.SelText;
end;

procedure TEditChild.SearchDeklaration;
var
  Info  : TParamInfoRecord;
  Attri : TSynHighlighterAttributes;
  Token : String;
begin
  if not EditFeld.GetHighlighterAttriAtRowCol(EditFeld.CaretXY, Token,Attri) then
    exit;

  if Attri.Name<>'Identifier' then
    exit;

  RebuildLokalObjektList;

  Token:=GetLookUpString(EditFeld.Lines[EditFeld.CaretY-1],EditFeld.CaretX);
  if LookUpList(Token,Info) then
  begin
    if Info.DeclarePos.Y>0 then
    begin
      EditFeld.CaretX:=Info.DeclarePos.X;
      EditFeld.CaretY:=Info.DeclarePos.Y;
      EditFeld.SelLength:=length(EditFeld.WordAtCursor);
      EditFeld.EnsureCursorPosVisible;
    end;
  end;
end;

procedure TEditChild.EditFeldClick(Sender: TObject);
begin
  if GetKeyState(VK_CONTROL)<0 then
    SearchDeklaration;
end;

procedure TEditChild.EditFeldMouseMove(Sender: TObject; Shift: TShiftState;
  X, Y: Integer);
var
  Attri : TSynHighlighterAttributes;
  Token : String;
  Coord : TBufferCoord;
begin
  if ssCtrl in Shift then
  begin
    if EditFeld.GetPositionOfMouse(Coord) then
    begin
      if EditFeld.GetHighlighterAttriAtRowCol(Coord, Token,Attri) then
      begin
        if Attri=EditWindow.PascalSyntax.IdentifierAttri then
        begin
          EditFeld.Cursor:=crHandPoint;
          exit;
        end;
      end;
    end;
    EditFeld.Cursor:=crIBeam;
  end
  else
    EditFeld.Cursor:=crIBeam;
end;

procedure TEditChild.EditFeldKeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key=VK_CONTROL then
    EditFeld.Cursor:=crIBeam;
end;

procedure TEditChild.Pause;
begin
  fPauseNextLine:=true;
end;

function TEditChild.GetLineOfFirstProcedure: Integer;
var
  Dummy: Integer;
begin
  RebuildLokalObjektList(false);

  result:=EditFeld.Lines.Count+1;

  for Dummy:=0 to high(fObjectList) do
  begin
    if (fObjectList[Dummy].Typ in [itProcedure,itFunction]) and (not fObjectList[Dummy].BuildIn) then
    begin
      result:=min(result,fObjectList[Dummy].DeclarePos.Y);
    end;
  end;
end;

procedure TEditChild.CreateNewProcedure(Declaration: String);
var
  Line: Integer;

  procedure InsertText(Text: String);
  var
    Dummy: Integer;
  begin
    for Dummy:=1 to length(Text) do
      EditFeld.ExecuteCommand(ecChar,Text[Dummy],nil);
  end;

  procedure NewLine(Count: Integer = 1);
  begin
    while Count>0 do
    begin
      EditFeld.ExecuteCommand(ecLineBreak,#0,nil);
      dec(Count);
    end;
  end;

begin
  if Declaration='' then
    exit;

  if not CheckSkript then
  begin
    Application.MessageBox('Vervollst�ndigung kann nicht aktiviert werden, da der Quellcode Fehler enth�lt','Fehler',MB_ICONERROR);
    exit;
  end;
  Line:=GetLineOfFirstProcedure;

  EditFeld.BeginUndoBlock;
  EditFeld.BeginUpdate;
  EditFeld.CaretX:=0;
  EditFeld.CaretY:=Line;

  InsertText(Declaration);
  NewLine;
  InsertText('begin');
  NewLine(2);
  InsertText('end;');
  NewLine(2);

  EditFeld.CaretY:=Line;
  if Declaration[1]='p' then
    EditFeld.CaretX:=11
  else
    EditFeld.CaretX:=10;

  EditFeld.EndUpdate;
  EditFeld.EndUndoBlock;
end;

procedure TEditChild.CodeExplorerTimerTimer(Sender: TObject);
begin
  CodeExplorerTimer.Enabled:=false;
  RebuildCodeExplorer;
end;

procedure TEditChild.FormDeactivate(Sender: TObject);
begin
  CodeExplorerTimer.Enabled:=false;
end;

end.


