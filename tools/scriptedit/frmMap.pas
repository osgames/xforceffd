unit frmMap;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Grids, frmChild, MapGenerator;

type
  TShowMapForm = class(TForm)
    MapGrid: TStringGrid;
  private
    { Private-Deklarationen }
  public
    procedure ShowMap(const Map: TMapGenerator);
    { Public-Deklarationen }
  end;

var
  ShowMapForm: TShowMapForm;

implementation

{$R *.DFM}

{ TShowMapForm }

procedure TShowMapForm.ShowMap(const Map: TMapGenerator);
var
  Dummy: Integer;
  X,Y : Integer;
begin
  MapGrid.ColCount:=Map.MapWidth+1;
  MapGrid.RowCount:=Map.MapHeight+1;

  for Dummy:=1 to MapGrid.ColCount do
  begin
    MapGrid.Cells[Dummy,0]:=IntToStr(Dummy);
  end;

  for Dummy:=1 to MapGrid.RowCount do
  begin
    MapGrid.Cells[0,Dummy]:=IntToStr(Dummy);
  end;

  for X:=0 to Map.MapWidth-1 do
  begin
    for Y:=0 to Map.MapHeight-1 do
    begin
      MapGrid.Cells[X+1,Y+1]:=Map.GetRoomAtPos(X,Y);
    end;
  end;
end;

end.
