object EventWindow: TEventWindow
  Left = 227
  Top = 130
  BorderStyle = bsDialog
  Caption = 'Ereigniss simulieren'
  ClientHeight = 304
  ClientWidth = 487
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnDestroy = FormDestroy
  OnShow = FormShow
  DesignSize = (
    487
    304)
  PixelsPerInch = 96
  TextHeight = 13
  object OKButton: TBitBtn
    Left = 295
    Top = 274
    Width = 89
    Height = 25
    Anchors = [akRight, akBottom]
    TabOrder = 0
    Kind = bkOK
  end
  object CancelButton: TBitBtn
    Left = 391
    Top = 274
    Width = 89
    Height = 25
    Anchors = [akRight, akBottom]
    TabOrder = 1
    Kind = bkCancel
  end
  object GroupBox1: TGroupBox
    Left = 8
    Top = 8
    Width = 473
    Height = 73
    Caption = 'Ereignisstyp'
    TabOrder = 2
    object UFOName: TLabel
      Left = 144
      Top = 48
      Width = 207
      Height = 17
      AutoSize = False
      Layout = tlCenter
    end
    object EventRadio: TRadioButton
      Left = 8
      Top = 16
      Width = 129
      Height = 17
      Caption = 'Registrierte Ereignisse'
      Checked = True
      TabOrder = 0
      TabStop = True
      OnClick = EventRadioClick
    end
    object ObjektRadio: TRadioButton
      Left = 8
      Top = 40
      Width = 129
      Height = 17
      Caption = 'Objekt-Ereignisse'
      TabOrder = 1
      OnClick = ObjektRadioClick
    end
    object ObjectList: TComboBox
      Left = 144
      Top = 36
      Width = 321
      Height = 24
      Style = csOwnerDrawFixed
      ItemHeight = 18
      TabOrder = 2
      OnChange = ObjectListChange
      OnDrawItem = ObjectListDrawItem
    end
  end
  object Panel2: TGroupBox
    Left = 8
    Top = 84
    Width = 473
    Height = 184
    Color = clBtnFace
    ParentColor = False
    TabOrder = 3
    object ListCaption: TLabel
      Left = 8
      Top = 10
      Width = 72
      Height = 13
      Caption = 'keine Auswahl:'
      Enabled = False
    end
    object EventList: TListBox
      Left = 8
      Top = 30
      Width = 457
      Height = 145
      Style = lbOwnerDrawFixed
      ItemHeight = 17
      TabOrder = 0
      OnClick = EventListClick
      OnDblClick = EventListDblClick
    end
  end
  object AbschussButton: TBitBtn
    Left = 7
    Top = 274
    Width = 89
    Height = 25
    Anchors = [akRight, akBottom]
    Caption = 'Abschuss'
    Enabled = False
    TabOrder = 4
    Visible = False
    OnClick = AbschussButtonClick
    NumGlyphs = 2
  end
end
