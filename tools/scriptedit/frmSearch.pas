unit frmSearch;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls,SynEdit, SynEditTypes;

type
  TSearchForm = class(TForm)
    Label1: TLabel;
    FindText: TEdit;
    ReplaceText: TEdit;
    Label2: TLabel;
    GroupBox1: TGroupBox;
    CaseSensitiv: TCheckBox;
    WholeWord: TCheckBox;
    Confirmation: TCheckBox;
    GroupBox2: TGroupBox;
    Up: TRadioButton;
    Down: TRadioButton;
    GroupBox3: TGroupBox;
    WholeText: TRadioButton;
    SelectedOnly: TRadioButton;
    Beginn: TGroupBox;
    FromCursor: TRadioButton;
    BeginnFile: TRadioButton;
    SearchButton: TButton;
    Button1: TButton;
    Replace: TButton;
    ReplaceAll: TButton;
    RegEx: TCheckBox;
    procedure Button1Click(Sender: TObject);
    procedure SearchButtonClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FindTextKeyPress(Sender: TObject; var Key: Char);
    procedure ReplaceClick(Sender: TObject);
    procedure ReplaceAllClick(Sender: TObject);
  private
    { Private-Deklarationen }
  public
    { Public-Deklarationen }
  end;

var
  SearchForm: TSearchForm;
  LastText  : String;
  LastRepl  : String;
  LastOpt   : TSynSearchOptions;

implementation

uses frmMain;

{$R *.DFM}

procedure TSearchForm.Button1Click(Sender: TObject);
begin
  Close;
end;

procedure TSearchForm.SearchButtonClick(Sender: TObject);
var
  AktOpt: TSynSearchOptions;
begin
  if FindText.Text='' then
  begin
    Application.MessageBox('Der Suchtext darf nicht leer bleiben.','Fehler',MB_OK or MB_ICONERROR);
    FindText.SetFocus;
    exit;
  end;

  if RegEx.Checked then
    EditWindow.EditFeld.SearchEngine:=EditWindow.SynEditRegexSearch
  else
    EditWindow.EditFeld.SearchEngine:=EditWindow.SynEditSearch;

  LastText:=FindText.Text;
  LastOpt:=[];
  if Down.Checked then
    LastOpt:=[ssoBackwards];
  if CaseSensitiv.Checked then
    Include(LastOpt,ssoMatchCase);
  if WholeWord.Checked then
    Include(LastOpt,ssoWholeWord);

  AktOpt:=LastOpt;

  if BeginnFile.Checked then
    Include(AktOpt,ssoEntireScope);
  if EditWindow.EditFeld.SearchReplace(LastText,'',AktOpt) = 0 then
    Application.MessageBox(PChar('Suchbegriff '''+FindText.Text+''' nicht gefunden'),PChar('Hinweis'),MB_OK or MB_ICONINFORMATION);
end;

procedure TSearchForm.FormActivate(Sender: TObject);
begin
  SearchButton.Enabled:=EditWindow.EditFeld<>nil;
  Replace.Enabled:=EditWindow.EditFeld<>nil;
  ReplaceAll.Enabled:=EditWindow.EditFeld<>nil;
end;

procedure TSearchForm.FindTextKeyPress(Sender: TObject; var Key: Char);
begin
  if Key=#13 then
  begin
    if SearchButton.Enabled then
    begin
      SearchButton.Click;
      Key:=#0;
    end;
  end;
end;

procedure TSearchForm.ReplaceClick(Sender: TObject);
var
  AktOpt : TSynSearchOptions;
begin
  if FindText.Text='' then
  begin
    Application.MessageBox('Der Suchtext darf nicht leer bleiben.','Fehler',MB_OK or MB_ICONERROR);
    FindText.SetFocus;
    exit;
  end;

  if RegEx.Checked then
    EditWindow.EditFeld.SearchEngine:=EditWindow.SynEditRegexSearch
  else
    EditWindow.EditFeld.SearchEngine:=EditWindow.SynEditSearch;

  LastRepl:=ReplaceText.Text;
  LastText:=FindText.Text;
  LastOpt:=[ssoReplace];
  if Confirmation.Checked then
    Include(LastOpt,ssoPrompt);
  if Down.Checked then
    Include(LastOpt,ssoBackwards);
  if CaseSensitiv.Checked then
    Include(LastOpt,ssoMatchCase);
  if WholeWord.Checked then
    Include(LastOpt,ssoWholeWord);

  AktOpt:=LastOpt;

  if BeginnFile.Checked then
    Include(AktOpt,ssoEntireScope);
  if EditWindow.EditFeld.SearchReplace(LastText,LastRepl,AktOpt) = 0 then
    Application.MessageBox(PChar('Suchbegriff '''+FindText.Text+''' nicht gefunden'),PChar('Hinweis'),MB_OK or MB_ICONINFORMATION);
end;

procedure TSearchForm.ReplaceAllClick(Sender: TObject);
var
  AktOpt : TSynSearchOptions;
begin
  if FindText.Text='' then
  begin
    Application.MessageBox('Der Suchtext darf nicht leer bleiben.','Fehler',MB_OK or MB_ICONERROR);
    FindText.SetFocus;
    exit;
  end;

  if RegEx.Checked then
    EditWindow.EditFeld.SearchEngine:=EditWindow.SynEditRegexSearch
  else
    EditWindow.EditFeld.SearchEngine:=EditWindow.SynEditSearch;

  LastText:='';
  AktOpt:=[ssoReplaceAll];
  if Down.Checked then
    Include(AktOpt,ssoBackwards);
  if CaseSensitiv.Checked then
    Include(AktOpt,ssoMatchCase);
  if WholeWord.Checked then
    Include(AktOpt,ssoWholeWord);

  Include(AktOpt,ssoEntireScope);
  if EditWindow.EditFeld.SearchReplace(FindText.Text,ReplaceText.Text,AktOpt) = 0 then
    Application.MessageBox(PChar('Suchbegriff '''+FindText.Text+''' nicht gefunden'),PChar('Hinweis'),MB_OK or MB_ICONINFORMATION);
end;

end.
