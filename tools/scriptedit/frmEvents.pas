unit frmEvents;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, ExtCtrls, frmChild, MissionList, ExtRecord;

type
  TEventWindow = class(TForm)
    OKButton: TBitBtn;
    CancelButton: TBitBtn;
    EventRadio: TRadioButton;
    UFOName: TLabel;
    GroupBox1: TGroupBox;
    Panel2: TGroupBox;
    ListCaption: TLabel;
    EventList: TListBox;
    ObjectList: TComboBox;
    ObjektRadio: TRadioButton;
    AbschussButton: TBitBtn;
    procedure EventRadioClick(Sender: TObject);
    procedure HourEndRadioClick(Sender: TObject);
    procedure DrawEventItem(Control: TWinControl; Index: Integer;
      Rect: TRect; State: TOwnerDrawState);
    procedure FormShow(Sender: TObject);
    procedure EventListDblClick(Sender: TObject);
    procedure ObjectListDrawItem(Control: TWinControl; Index: Integer;
      Rect: TRect; State: TOwnerDrawState);
    procedure ObjektRadioClick(Sender: TObject);
    procedure ObjectListChange(Sender: TObject);
    procedure EventListClick(Sender: TObject);
    procedure ObjectEventDrawItem(Control: TWinControl; Index: Integer;
      Rect: TRect; State: TOwnerDrawState);
    procedure AbschussButtonClick(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
   private
    fMission   : TMission;
    fPointer : Array of TObject;
    procedure ResetLabels;
    procedure FillObjektList;
    procedure AktuObjectEvents;
    procedure Clear;
    { Private-Deklarationen }
  public
    property Mission: TMission read fMission write fMission;
    procedure ShowEvents;
    { Public-Deklarationen }
  end;

var
  EventWindow: TEventWindow;

implementation

uses frmMain, ufo_api, ConvertRecords, UFOList,
  einsatz_api, EinsatzListe, raumschiff_api, RaumschiffList;

var
  CallEvent : String;
{$R *.DFM}

procedure TEventWindow.Clear;
var
  Dummy: Integer;
begin
  for Dummy:=0 to ObjectList.Items.Count-1 do
  begin
    ObjectList.Items.Objects[Dummy].Free;
  end;
  ObjectList.Items.Clear;
end;

procedure TEventWindow.EventRadioClick(Sender: TObject);
var
  Dummy: Integer;
begin
  ListCaption.Caption:='Registrierte Ereignisse:';
  ListCaption.Enabled:=true;
  EventList.Items.Clear;
  for Dummy:=0 to fMission.EventCount-1 do
  begin
    if fMission.GetEvent(Dummy).Active then
      EventList.Items.Add(IntToStr(Dummy));
  end;
  EventList.OnDrawItem:=DrawEventItem;
  ResetLabels;
  if EventList.Items.Count=0 then
    OKButton.Enabled:=false
  else
  begin
    EventList.ItemIndex:=0;
    OKButton.Enabled:=true;
  end;
  AbschussButton.Enabled:=false;
end;

procedure TEventWindow.HourEndRadioClick(Sender: TObject);
begin
  ListCaption.Caption:='keine Auswahl:';
  ListCaption.Enabled:=false;
  EventList.Items.Clear;
  EventList.OnDrawItem:=nil;
  OKButton.Enabled:=true;
  AbschussButton.Enabled:=false;
  ResetLabels;
end;

procedure TEventWindow.ResetLabels;
begin
  UFOName.Caption:='';
//  EventName.Caption:='';
  ObjectList.Enabled:=ObjektRadio.Checked;
end;

procedure TEventWindow.FormShow(Sender: TObject);
begin
  FillObjektList;
//  if HourEndRadio.Checked then
//    HourEndRadioClick(Self)
  if EventRadio.Checked then
    EventRadioClick(Self)
  else if ObjektRadio.Checked then
    ObjektRadioClick(Self);

  ResetLabels;
end;

procedure TEventWindow.DrawEventItem(Control: TWinControl; Index: Integer;
  Rect: TRect; State: TOwnerDrawState);
var
  UIndex: Integer;
begin
  UIndex:=StrToInt(EventList.Items[Index]);
  with EventList.Canvas do
  begin
    FillRect(Rect);
    TextOut(Rect.Left+2,Rect.Top+2,fMission.GetEvent(UIndex).CalledScript);
    TextOut(Rect.Left+120,Rect.Top+2,Format('nach %d Minuten',[fMission.GetEvent(UIndex).Interval]));
    if fMission.GetEvent(UIndex).Repeated then
      TextOut(Rect.Left+250,Rect.Top+2,'wiederholend');
  end;
end;

procedure TEventWindow.EventListDblClick(Sender: TObject);
begin
  if OKButton.Enabled then
    OKButton.Click;
end;

procedure TEventWindow.FillObjektList;
var
  Dummy: Integer;
  Temp : TExtRecord;
  Index: Integer;
begin
  SetLength(fPointer,0);
  Clear;
  with Mission do
  begin
    // Maximale Objekte errechnen
    SetLength(fPointer,RegisteredMissionUFOs+RegisteredMissionEinsaetze+RegisteredMissionSchiffs);
    Index:=0;

    // UFOs zur Liste hinzufügen
    for Dummy:=0 to RegisteredMissionUFOs-1 do
    begin
      if not GetRegisteredMissionUFO(Dummy).Delete then
      begin
        Temp:=MissionUFOToRecord(GetRegisteredMissionUFO(Dummy));
        fPointer[Index]:=ufo_api_GetUFO(GetRegisteredMissionUFO(Dummy).UFOID);
        ObjectList.Items.AddObject(TUFO(fPointer[Index]).Name,Temp);
        inc(Index);
      end;
    end;

    // Einsätze zur Liste hinzufügen
    for Dummy:=0 to RegisteredMissionEinsaetze-1 do
    begin
      if einsatz_api_GetEinsatz(GetRegisteredMissionEinsatz(Dummy).ID)<>nil then
      begin
        Temp:=MissionEinsatzToRecord(GetRegisteredMissionEinsatz(Dummy));
        fPointer[Index]:=einsatz_api_GetEinsatz(GetRegisteredMissionEinsatz(Dummy).ID);
        ObjectList.Items.AddObject(TEinsatz(fPointer[Index]).Name,Temp);
        inc(Index);
      end;
    end;

    // Raumschiff zur Liste hinzufügen
    for Dummy:=0 to RegisteredMissionSchiffs-1 do
    begin
      if raumschiff_api_GetRaumschiff(GetRegisteredMissionSchiff(Dummy).SchiffID)<>nil then
      begin
        Temp:=MissionSchiffToRecord(GetRegisteredMissionSchiff(Dummy));
        fPointer[Index]:=raumschiff_api_GetRaumschiff(GetRegisteredMissionSchiff(Dummy).SchiffID);
        ObjectList.Items.AddObject(TRaumschiff(fPointer[Index]).Name,Temp);
        inc(Index);
      end;
    end;

    SetLength(fPointer,Index);
  end;
  ObjectList.ItemIndex:=0;
  ObjektRadio.Enabled:=Index>0;
  if (Index=0) and (ObjektRadio.Checked) then
    EventRadio.Checked:=true;
end;

procedure TEventWindow.ObjectListDrawItem(Control: TWinControl;
  Index: Integer; Rect: TRect; State: TOwnerDrawState);
var
  Text: String;
begin
  Text:=TExtRecord(ObjectList.Items.Objects[Index]).RecordDefinition.RecordName;
  Text:=StringReplace(Text,'Mission','',[rfIgnoreCase]);

  with ObjectList.Canvas do
  begin
    FillRect(Rect);
    Font.Style:=[fsBold];
    TextOut(Rect.Left+2,Rect.Top+3,Text);
    Font.Style:=[];
    TextOut(Rect.Left+80,Rect.Top+3,ObjectList.Items[Index]);
  end;
end;

procedure TEventWindow.ObjektRadioClick(Sender: TObject);
begin
  ResetLabels;
  EventList.OnDrawItem:=ObjectEventDrawItem;
  ListCaption.Enabled:=true;
  AktuObjectEvents;
end;

procedure TEventWindow.AktuObjectEvents;
var
  Objekt     : TExtRecord;
  Dummy      : Integer;
  Text       : String;
begin
  try
    ListCaption.Caption:='Ereignisse zu '+ObjectList.Items[ObjectList.ItemIndex];
    EventList.Items.Clear;
    Objekt:=TExtRecord(ObjectList.Items.Objects[ObjectList.ItemIndex]);

    AbschussButton.Enabled:=false;
    for Dummy:=0 to Objekt.ValueCount-1 do
    begin
      with Objekt.Values[Dummy] do
      begin
        if (ValueType=ervtString) and (lowercase(Copy(ValueName,1,2))='on') then
        begin
          Text:=ValueName;
          if LowerCase(Name)='ondestroy' then
            AbschussButton.Enabled:=true;

          Text:=Text+'|'+Objekt.GetString(ValueName);

          EventList.Items.Add(Text);
        end;
      end;
    end;
    EventList.ItemIndex:=0;
    EventListClick(nil);
  finally
  end
end;

procedure TEventWindow.ObjectListChange(Sender: TObject);
begin
  AktuObjectEvents;
end;

procedure TEventWindow.EventListClick(Sender: TObject);
begin
  if EventList.ItemIndex<>-1 then
  begin
{    if ObjektRadio.Checked then
      OKButton.Enabled:=GetLongHint(EventList.Items[EventList.ItemIndex])<>''
    else}
      OKButton.Enabled:=true;
  end
  else
    OKButton.Enabled:=false;
end;

procedure TEventWindow.ObjectEventDrawItem(Control: TWinControl;
  Index: Integer; Rect: TRect; State: TOwnerDrawState);
var
  Text    : String;
  Enabled : Boolean;
begin
  Text:=EventList.Items[Index];
  Enabled:=true;
  with EventList.Canvas do
  begin
    if (Pos('|',Text)=0) or (GetLongHint(Text)='') then
    begin
      Enabled:=false;
      if odSelected in State then
      begin
        Brush.Color:=clBtnShadow;
        Font.Color:=clWhite;
      end
      else
        Font.Color:=clBtnShadow;
    end;
    FillRect(Rect);
    if Enabled then
    begin
      TextOut(Rect.Left+2,Rect.Top+2,GetShortHint(Text));
      TextOut(Rect.Left+200,Rect.Top+2,GetLongHint(Text));
    end
    else
    begin
      TextOut(Rect.Left+2,Rect.Top+2,GetShortHint(Text));
      TextOut(Rect.Left+200,Rect.Top+2,'nicht zugewiesen');
    end;
  end;
end;

procedure TEventWindow.ShowEvents;
var
  Index  : Integer;
  Event  : String;
  Obj    : TObject;
begin
  CallEvent:='';
  if ShowModal=mrOk then
  begin
    if EventRadio.Checked then
    begin
      Index:=StrToInt(EventList.Items[EventList.ItemIndex]);
      fMission.GetEvent(Index).DoInterval(fMission.GetEvent(Index).Interval);
//      result:=fChild.fEvents[Index].Name;
//      fChild.fEvents[Index].Aktiv:=false;
    end
    else
    begin
      Assert((ObjectList.ItemIndex<>-1) and (EventList.ItemIndex<>-1));

      Event:=lowercase(GetShortHint(EventList.Items[EventList.ItemIndex]));
      Obj:=fPointer[ObjectList.ItemIndex];
      Assert(Obj<>nil);

      if Obj is TUFO then
      begin
        // Verarbeitung UFO-Objekt-Events
        if Event='onshootdown' then
          TUFO(Obj).Abschuss
        else if Event='onescape' then
          TUFO(Obj).Escape
        else if Event='ondiscovered' then
          TUFO(Obj).Visible:=true
        else
          assert(false);
      end
      else if Obj is TEinsatz then
      begin
        // Verarbeitung Einsatzevents
        if Event='ontimeup' then
          TEinsatz(Obj).TimeUp
        else if Event='onwin' then
          TEinsatz(Obj).Win
        else
          Assert(false);
      end
      else if Obj is TRaumschiff then
      begin
        if Event='ondestroy' then
          TRaumschiff(Obj).VernichteRaumschiff
        else if Event='onreached' then
          TRaumschiff(Obj).ReachedDestination
        else
          assert(false);
      end;
    end;
  end;
end;

procedure TEventWindow.AbschussButtonClick(Sender: TObject);
var
  Dummy: Integer;
begin
  for Dummy:=0 to EventList.Items.Count-1 do
  begin
    if (GetShortHint(EventList.Items[Dummy])='OnDestroy') and (Pos('|',EventList.Items[Dummy])>0) then
      CallEvent:=GetLongHint(EventList.Items[Dummy]);
  end;
  PBoolean(Integer(fPointer[ObjectList.ItemIndex])+4)^:=false;
  Close;
end;

procedure TEventWindow.FormDestroy(Sender: TObject);
begin
  Clear;
end;

end.
