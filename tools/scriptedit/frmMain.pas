unit frmMain;

{$I SynEdit.inc}

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, ToolWin, ComCtrls, ActnList, ImgList, Defines, Menus,
  AppEvnts, math, TB2Item, TB2Dock, TB2Toolbar, Registry, TraceFile,
  frmChild, ArchivFile,Loader,XForce_types, SynEditHighlighter, SynEdit,
  TB2ToolWindow, Buttons, SynEditOptionsDialog, ExtRecord, ProjektRecords,
  ObjektRecords, SynHighlighterPas, UPSCompiler, readlanguage, SynEditTypes,
  SynEditRegexSearch, SynEditMiscClasses, uPSRuntime, SynEditSearch, StdActns,
  script_utils, JvExControls, JvComponent, JvInspector, TBX, TBXDkPanels,
  XPMan, TBXToolPals, TBXExtItems, TBXMDI, SynEditKeyCmds, JvExComCtrls,
  JvComCtrls, JvComponentBase, JvTabBar;

type

  TPopupType = (ptInsertProcedure,ptInsertProcedureCall);

  PPopupInfo = ^TPopupInfo;
  TPopupInfo = record
    Typ      : TPopupType;
    Info     : String;
    Next     : PPopupInfo;
  end;

  TEditWindow = class(TForm)
    ImageList: TImageList;
    ActionList: TActionList;
    ActionFileOpen: TAction;
    OpenDialog: TOpenDialog;
    ActionFileSave: TAction;
    ActionFileNew: TAction;
    SaveDialog: TSaveDialog;
    StatusBar: TStatusBar;
    ApplicationEvents: TApplicationEvents;
    ActionSkriptCheck: TAction;
    BookmarkList: TImageList;
    ActionEditCut: TEditCut;
    ActionEditCopy: TEditCopy;
    ActionEditPaste: TEditPaste;
    ActionEditUndo: TEditUndo;
    ActionEditRedo: TEditUndo;
    EditPopup: TPopupMenu;
    Rckgngig2: TMenuItem;
    Wiederholen2: TMenuItem;
    N2: TMenuItem;
    Ausschneiden2: TMenuItem;
    Kopieren2: TMenuItem;
    Einfgen2: TMenuItem;
    TBDockTop: TTBXDock;
    TBDockBottom: TTBXDock;
    TBToolbar1: TTBXToolbar;
    TBItem1: TTBXItem;
    TBItem3: TTBXItem;
    TBSeparatorItem1: TTBXSeparatorItem;
    TBItem4: TTBXItem;
    TBItem5: TTBXItem;
    TBItem6: TTBXItem;
    TBItem7: TTBXItem;
    TBItem8: TTBXItem;
    TBItem9: TTBXItem;
    ActionFileSaveAs: TAction;
    N3: TMenuItem;
    SetMarkes: TMenuItem;
    Positionsmarke91: TMenuItem;
    Positionsmarke81: TMenuItem;
    Positionsmarke71: TMenuItem;
    Positionsmarke61: TMenuItem;
    Positionsmarke51: TMenuItem;
    Positionsmarke41: TMenuItem;
    Positionsmarke31: TMenuItem;
    Positionsmarke21: TMenuItem;
    Positionsmarke11: TMenuItem;
    Positionsmarke01: TMenuItem;
    GoMarkes: TMenuItem;
    Positionsmarke92: TMenuItem;
    Positionsmarke82: TMenuItem;
    Positionsmarke72: TMenuItem;
    Positionsmarke62: TMenuItem;
    Positionsmarke52: TMenuItem;
    Positionsmarke42: TMenuItem;
    Positionsmarke32: TMenuItem;
    Positionsmarke22: TMenuItem;
    Positionsmarke12: TMenuItem;
    Positionsmarke02: TMenuItem;
    ActionSkriptStart: TAction;
    TBItem10: TTBXItem;
    ActionHelpKontext: TAction;
    OpenItem: TTBXSubmenuItem;
    WindowCascade: TWindowCascade;
    WindowTileVer: TWindowTileHorizontal;
    WindowTileHor: TWindowTileVertical;
    WindowMinimizeAll: TWindowMinimizeAll;
    WindowArrange: TWindowArrange;
    TBSeparatorItem4: TTBXSeparatorItem;
    ActionSkriptStop: TAction;
    TBItem13: TTBXItem;
    ActionSkriptStepInto: TAction;
    TBItem14: TTBXItem;
    TBSeparatorItem5: TTBXSeparatorItem;
    ActionSkriptBreakPoint: TAction;
    TBItem15: TTBXItem;
    N8: TMenuItem;
    ActionViewMessageList: TAction;
    Nachrichtenliste1: TMenuItem;
    ActionDebugAddValueEditor: TAction;
    ActionDebugAddValueList: TAction;
    ActionDebugDeleteEditor: TAction;
    ActionDebugEditValue: TAction;
    TBSeparatorItem3: TTBXSeparatorItem;
    ActionEditFind: TAction;
    ActionEditFindNext: TAction;
    ActionIntelliSense: TAction;
    ActionFileClose: TAction;
    ActionFileAllClose: TAction;
    ActionViewObjectBrowser: TAction;
    ActionViewWatchList: TAction;
    ActionParamInfo: TAction;
    PascalSyntax: TSynPasSyn;
    SynEditSearch: TSynEditSearch;
    SynEditRegexSearch: TSynEditRegexSearch;
    ActionSkriptStepOver: TAction;
    TBItem2: TTBXItem;
    JvInspectorBorlandPainter1: TJvInspectorBorlandPainter;
    TBSeparatorItem6: TTBXSeparatorItem;
    TBItem11: TTBXItem;
    TBItem12: TTBXItem;
    TBXMultiDockRight: TTBXMultiDock;
    WatchWindow: TTBXDockablePanel;
    WatchInspector: TJvInspector;
    TBXMultiDockLeft: TTBXMultiDock;
    BrowserWindow: TTBXDockablePanel;
    ObjectBrowserMap: TTreeView;
    ObjectBrowserMission: TTreeView;
    TBXSeparatorItem1: TTBXSeparatorItem;
    N5: TMenuItem;
    CodeSense1: TMenuItem;
    ParamSense1: TMenuItem;
    N10: TMenuItem;
    Ausdrucklschen2: TMenuItem;
    XPManifest1: TXPManifest;
    MenuDatei: TTBXSubmenuItem;
    MenuFileNew: TTBXItem;
    MenuFileOpen: TTBXItem;
    MenuFileSave: TTBXItem;
    MenuSaveAs: TTBXItem;
    MenuSep1: TTBXSeparatorItem;
    Schliessen1: TTBXItem;
    AlleSchliessen1: TTBXItem;
    N12: TTBXSeparatorItem;
    Optionen1: TTBXItem;
    N6: TTBXSeparatorItem;
    MenuExit: TTBXItem;
    Bearbeiten1: TTBXSubmenuItem;
    Rckgngig1: TTBXItem;
    Wiederholen1: TTBXItem;
    N1: TTBXSeparatorItem;
    Ausschneiden1: TTBXItem;
    Kopieren1: TTBXItem;
    Einfgen1: TTBXItem;
    N4: TTBXSeparatorItem;
    Suchen1: TTBXItem;
    Ausdruckhinzufgen2: TTBXItem;
    Ansicht1: TTBXSubmenuItem;
    ActionViewObjectBrowser1: TTBXItem;
    berwachteAusdrcke2: TTBXItem;
    N9: TTBXSeparatorItem;
    Nachrichtenliste2: TTBXItem;
    Skript1: TTBXSubmenuItem;
    berprfen1: TTBXItem;
    N7: TTBXSeparatorItem;
    Haltepunktsetzen1: TTBXItem;
    N13: TTBXSeparatorItem;
    Start1: TTBXItem;
    Einzelschritt1: TTBXItem;
    GesamteRoutine1: TTBXItem;
    N15: TTBXSeparatorItem;
    Stop1: TTBXItem;
    Window: TTBXSubmenuItem;
    berlappend1: TTBXItem;
    bereinander1: TTBXItem;
    Nebeneinander1: TTBXItem;
    Alleminimieren1: TTBXItem;
    Symboleanordnen1: TTBXItem;
    Hilfe1: TTBXSubmenuItem;
    Inhalt1: TTBXItem;
    N14: TTBXSeparatorItem;
    About: TTBXItem;
    MenuBar: TTBXToolbar;
    ReOpenMenu: TTBXSubmenuItem;
    CodeExplorerWindow: TTBXDockablePanel;
    ActionViewCodeExplorer: TAction;
    TBXItem1: TTBXItem;
    TBItem16: TTBXItem;
    CodeExplorer: TTreeView;
    TBXMDIWindowItem1: TTBXMDIWindowItem;
    TBSeparatorItem7: TTBXSeparatorItem;
    ActionHelpIndex: TAction;
    TBItem17: TTBXItem;
    N16: TMenuItem;
    ActionSkriptGotoDeclare: TAction;
    Deklarationsuchen1: TMenuItem;
    ActionSkriptPause: TAction;
    TBSeparatorItem8: TTBXSeparatorItem;
    TBXItem2: TTBXItem;
    TBItem18: TTBXItem;
    TBXMDIHandler1: TTBXMDIHandler;
    ObjektBrowserPopup: TPopupMenu;
    ActionFileSaveAll: TAction;
    TBItem19: TTBXItem;
    TBXItem3: TTBXItem;
    ActionSynEditIndent: TAction;
    ActionSynEditUnIndent: TAction;
    TBXItem4: TTBXItem;
    TBXItem5: TTBXItem;
    TBXSeparatorItem2: TTBXSeparatorItem;
    JvOpenFileBar: TJvTabBar;
    JvModernTabBarPainter1: TJvModernTabBarPainter;
    procedure ActionFileOpenExecute(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure ActionFileSaveExecute(Sender: TObject);
    procedure ActionFileNewExecute(Sender: TObject);
    procedure MenuExitClick(Sender: TObject);
    procedure ActionSkriptCheckExecute(Sender: TObject);
    procedure ActionEditUndoExecute(Sender: TObject);
    procedure ActionEditUndoUpdate(Sender: TObject);
    procedure ActionEditRedoUpdate(Sender: TObject);
    procedure ActionEditRedoExecute(Sender: TObject);
    procedure ActionEditCutExecute(Sender: TObject);
    procedure ActionEditCopyExecute(Sender: TObject);
    procedure ActionEditPasteExecute(Sender: TObject);
    procedure ActionEditCutUpdate(Sender: TObject);
    procedure ActionEditPasteUpdate(Sender: TObject);
    procedure ActionFileSaveAsExecute(Sender: TObject);
    procedure SetMarkClick(Sender: TObject);
    procedure GotoMarkClick(Sender: TObject);
    procedure EditPopupPopup(Sender: TObject);
    procedure ActionHelpKontextExecute(Sender: TObject);
    procedure ActionFileSaveUpdate(Sender: TObject);
    procedure ActionSkriptCheckUpdate(Sender: TObject);
    procedure IsWindowOpen(Sender: TObject);
    procedure Optionen1Click(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure IsInDebugMode(Sender: TObject);
    procedure ActionSkriptBreakPointExecute(Sender: TObject);
    procedure ActionSkriptStartExecute(Sender: TObject);
    procedure ActionSkriptStepIntoExecute(Sender: TObject);
    procedure ActionViewMessageListUpdate(Sender: TObject);
    procedure ActionViewMessageListExecute(Sender: TObject);
    procedure ActionViewWatchListUpdate(Sender: TObject);
    procedure ActionViewWatchListExecute(Sender: TObject);
    procedure ActionDebugAddValueEditorExecute(Sender: TObject);
    procedure ActionDebugAddValueListUpdate(Sender: TObject);
    procedure ApplicationEventsHint(Sender: TObject);
    procedure WatchListDblClick(Sender: TObject);
    procedure ActionSkriptStopExecute(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure ActionEditFindExecute(Sender: TObject);
    procedure FindDialogFind(Sender: TObject);
    procedure ActionEditFindNextExecute(Sender: TObject);
    procedure ActionEditFindNextUpdate(Sender: TObject);
//    procedure ActionIntelliSenseExecute(Sender: TObject);
    procedure ActionFileCloseExecute(Sender: TObject);
    procedure ActionFileAllCloseExecute(Sender: TObject);
    procedure SpeedButton1Click(Sender: TObject);
    procedure ActionViewObjectBrowserUpdate(Sender: TObject);
    procedure ActionViewObjectBrowserExecute(Sender: TObject);
    procedure AboutClick(Sender: TObject);
    procedure ActionListUpdate(Action: TBasicAction; var Handled: Boolean);
    procedure ActionSkriptStepOverExecute(Sender: TObject);
    procedure SaveDialogShow(Sender: TObject);
    procedure CodeSense1Click(Sender: TObject);
    procedure ParamSense1Click(Sender: TObject);
    procedure ActionHelpKontextUpdate(Sender: TObject);
    procedure ApplicationEventsShowHint(var HintStr: String;
      var CanShow: Boolean; var HintInfo: THintInfo);
    procedure ActionViewCodeExplorerUpdate(Sender: TObject);
    procedure ActionViewCodeExplorerExecute(Sender: TObject);
    procedure CodeExplorerClick(Sender: TObject);
    procedure CodeExplorerCompare(Sender: TObject; Node1, Node2: TTreeNode;
      Data: Integer; var Compare: Integer);
    procedure ActionHelpIndexExecute(Sender: TObject);
    procedure ActionSkriptGotoDeclareUpdate(Sender: TObject);
    procedure ActionSkriptGotoDeclareExecute(Sender: TObject);
    procedure ActionSkriptPauseUpdate(Sender: TObject);
    procedure ActionSkriptPauseExecute(Sender: TObject);
    procedure ObjectBrowserDoPopup(Sender: TObject;
      Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
    procedure ObjektBrowserPopupClickItem(Sender: TObject);
    procedure ObjectBrowserDblClick(Sender: TObject);
    procedure ActionFileSaveAllUpdate(Sender: TObject);
    procedure ActionFileSaveAllExecute(Sender: TObject);
    procedure ExecuteCommand(Sender: TObject);
    procedure JvOpenFileBarTabSelected(Sender: TObject;
      Item: TJvTabBarItem);
    procedure JvOpenFileBarTabClosed(Sender: TObject; Item: TJvTabBarItem);
  private
    fFiles     : TStringList;
    procedure OpenFile(FileName: String);
    procedure AktuOpenList;
    procedure FileClick(Sender: TObject);
    procedure SetOptions(EditFeld: TSynEdit);
    function AddEditChild: TEditChild;
    function GetEditFeld: TSynEdit;
    function GetEditChild: TEditChild;
    procedure InitializeObjectList;

    procedure PerformPopupAction(Info: PPopupInfo);

    { Private-Deklarationen }
  public
    ValueWidth       : Integer;

    procedure BuildInfoList(Browser: TTreeView; Compiler: TPSPascalCompiler);
    procedure StateChange;
    procedure AddToOpenList(FileName: String);
    function ShowEvents(Child: TEditChild): boolean;

    function CheckGameSetFileName(var FileName: String): boolean;
    property EditFeld : TSynEdit read GetEditFeld;

    property ActiveChild: TEditChild read GetEditChild;
    { Public-Deklarationen }
  end;

  {.$DEFINE MAPSCRIPTS}
var
  EditWindow    : TEditWindow;

  { Einstellungen }
  AutoSave      : Boolean = false;
  GameSet       : String  = 'data\default.pak';
  AutoViewEvent : boolean = true;
  DefaultFont   : String  = 'FixedSys';
  DefaultSize   : Integer = 8;
  DefaultDir    : String  = '';
  FontColor     : TColor  = clWindowText;
  BackColor     : TColor  = clWindow;
  SelFontColor  : TColor  = clWindow;
  SelBackColor  : TColor  = clHighlight;
  GutterVisible : boolean = true;
  LineNumbers   : boolean = false;
  RightEdge     : Integer = 80;
  RightColor    : TColor  = clSilver;
  AutoIndent    : boolean = true;
  GroupUndo     : boolean = false;
  MatchingKlam  : TColor  = clBlue;

  TestSaveGame  : Boolean = false;
  SaveGameFile  : String = '';
  SaveGameName  : String = '';

  XForceDir     : String  = '';

implementation

uses
  frmOptions, frmEvents, frmSearch, frmAbout, frmNew,
  register_scripttypes, UFOList, MapGenerator, string_utils, gameset_api,
  record_utils, frmChooseSkript, savegame_api, uPSUtils, ShellAPI,
  DateUtils;

const
  HelpURL   = 'http://doc.xforce-online.de/index.php?pagename=';
  SearchURL = HelpURL+'Main%%2FSearchWiki&text=XSkriptReferenz%%2F%s';
{$R *.DFM}

procedure RegisterPascalFoldings(Highlighter: TSynCustomHighlighter);
begin
//	Highlighter.FoldRegions.Add(rtKeyword, false, false, true, 'procedure', 'end', nil);
/// Highlighter.FoldRegions.Add(rtKeyword, false, false, true, 'function', 'end', nil);
end;

function ScriptEngineBuildOutput(Sender: TPSPascalCompiler): Boolean;
begin
  EditWindow.BuildInfoList(TTreeView(Sender.ID),Sender);
  result:=true;
end;

procedure TEditWindow.ActionFileOpenExecute(Sender: TObject);
var
  Dummy: Integer;
begin
  if OpenDialog.Execute then
  begin
    for Dummy:=0 to OpenDialog.Files.Count-1 do
      OpenFile(OpenDialog.Files[Dummy]);
  end;
end;

procedure TEditWindow.FormCreate(Sender: TObject);
var
  Dummy       : Integer;
  Reg         : TRegistry;
  ColorsSaved : boolean;
  Archiv      : TArchivFile;
  MRUFile     : String;
  Parts       : TStringArray;
  Strokes     : TSynEditKeyStrokes;

  procedure SetCommand(Action: TAction; Command: TSynEditorCommand);
  var
    Index: Integer;
  begin
    Index:=Strokes.FindCommand(Command);
    Action.ShortCut:=Strokes.Items[Index].ShortCut;
    Action.Tag:=Integer(Command);
  end;
begin
  TBRegLoadPositions(Self,HKEY_CURRENT_USER,'Software\Rich Entertainment\MEdit\ToolBars');

  XForceDir:=IncludeTrailingBackslash(ExtractFilePath(Application.ExeName));

  fFiles:=TStringList.Create;
  Reg:=TRegistry.Create;

  if Reg.OpenKey('Software\Rich Entertainment\MEdit\MRU',false) then
  begin
    for Dummy:=0 to 9 do
    begin
      if Reg.ValueExists(IntToStr(Dummy)) then
      begin
        MRUFile:=Reg.ReadString(intToStr(Dummy));
        Parts:=string_utils_explode('|',MRUFile);
        if (FileExists(Parts[0])) then
          fFiles.Add(MRUFile);
      end;
    end;
  end;
  
  // Einstellungen laden
  Reg.CloseKey;
  ColorsSaved:=false;
  if Reg.OpenKey('Software\Rich Entertainment\MEdit',false) then
  begin
    if Reg.ValueExists('AutoSave') then
      AutoSave:=Reg.ReadBool('AutoSave');
    if Reg.ValueExists('GameSet') then
      GameSet:=Reg.ReadString('GameSet');
    if Reg.ValueExists('AutoViewEvent') then
      AutoViewEvent:=Reg.ReadBool('AutoViewEvent');
    if Reg.ValueExists('FontSize') then
      DefaultSize:=Reg.ReadInteger('FontSize');
    if Reg.ValueExists('FontColor') then
      FontColor:=Reg.ReadInteger('FontColor');
    if Reg.ValueExists('BackColor') then
      BackColor:=Reg.ReadInteger('BackColor');
    if Reg.ValueExists('SelFontColor') then
      SelFontColor:=Reg.ReadInteger('SelFontColor');
    if Reg.ValueExists('SelBackColor') then
      SelBackColor:=Reg.ReadInteger('SelBackColor');
    if Reg.ValueExists('GutterVisible') then
      GutterVisible:=Reg.ReadBool('GutterVisible');
    if Reg.ValueExists('LineNumbers') then
      LineNumbers:=Reg.ReadBool('LineNumbers');
    if Reg.ValueExists('RightEdge') then
      RightEdge:=Reg.ReadInteger('RightEdge');
    if Reg.ValueExists('RightColor') then
      RightColor:=Reg.ReadInteger('RightColor');
    if Reg.ValueExists('GroupUndo') then
      GroupUndo:=Reg.ReadBool('GroupUndo');
    if Reg.ValueExists('AutoIndent') then
      AutoIndent:=Reg.ReadBool('AutoIndent');
    if Reg.ValueExists('MatchingBracketColor') then
      MatchingKlam:=Reg.ReadInteger('MatchingBracketColor');
    if Reg.ValueExists('DefaultDir') then
      DefaultDir:=Reg.ReadString('DefaultDir');
    if Reg.ValueExists('FontName') then
      DefaultFont:=Reg.ReadString('FontName');

    if Reg.ValueExists('TestSaveGame') then
      TestSaveGame:=Reg.ReadBool('TestSaveGame');

    if Reg.ValueExists('SaveGameFile') then
      SaveGameFile:=Reg.ReadString('SaveGameFile');

    if Reg.ValueExists('SaveGameName') then
      SaveGameName:=Reg.ReadString('SaveGameName');

    if DefaultFont='' then DefaultFont:='FixedSys';
    if PascalSyntax.LoadFromRegistry(HKEY_CURRENT_USER,'Software\Rich Entertainment\MEdit\HighLighter') then
      ColorsSaved:=true;
  end;
  if not ColorsSaved then
    PascalSyntax.SaveToRegistry(HKEY_CURRENT_USER,'Software\Rich Entertainment\MEdit\HighLighter');
  SaveDialog.InitialDir:=DefaultDir;
  OpenDialog.InitialDir:=DefaultDir;
  Reg.Free;
  Randomize;
  for Dummy:=0 to 9 do
  begin
    SetMarkes.Items[dummy].Hint:=Format('Setzt oder l�scht die Positionsmarke %d',[Dummy]);
    GoMarkes.Items[Dummy].Hint:=Format('Springt zur Positionsmarke %d',[Dummy]);
  end;
  AktuOpenList;

  CodeSense1.ShortCut:=ShortCut(VK_SPACE,[ssCtrl]);
  ParamSense1.ShortCut:=ShortCut(VK_SPACE,[ssCtrl,ssShift]);
//  WatchWindow.Floating:=true;
//  WatchWindow.SetBounds(20,20,300,400);

  ReadLanguageFile('deutsch');

  // Erdmaske laden
  { Erd-Maske f�r UFOListe }
  Archiv:=TArchivFile.Create;
  Archiv.OpenArchiv(XForceDir+'data\xforce.pak');

  Archiv.OpenRessource(RNEarthMask);
  UFOList.gEarthMask.LoadFromStream(Archiv.Stream);

  Archiv.Free;

  InitializeObjectList;

  savegame_api_SetMissionEditorMode;

  RegisterPascalFoldings(PascalSyntax);

  // Tastaturbelegung wieder zuweisen
  Strokes:=TSynEditKeyStrokes.Create(nil);
  Strokes.ResetDefaults;

  SetCommand(ActionSynEditIndent,ecBlockIndent);
  SetCommand(ActionSynEditUnIndent,ecBlockUnIndent);

  Strokes.Free;
end;

procedure TEditWindow.FormDestroy(Sender: TObject);
var
  Reg   : TRegistry;
  Dummy : Integer;
  parts : TStringArray;
begin
  Reg:=TRegistry.Create;
  Reg.OpenKey('Software\Rich Entertainment\MEdit\MRU',true);
  for Dummy:=0 to 9 do
  begin
    if (Dummy<fFiles.Count) then
    begin
      Parts:=string_utils_explode('|',fFiles[Dummy]);
      if (FileExists(Parts[0])) then
        Reg.WriteString(IntToStr(Dummy),fFiles[Dummy])
      else
        Reg.DeleteValue(IntToStr(Dummy));
    end;
  end;
  Reg.CloseKey;
  Reg.OpenKey('Software\Rich Entertainment\MEdit',true);
  Reg.WriteString('GameSet',GameSet);
  Reg.WriteBool('AutoSave',AutoSave);
  Reg.WriteBool('AutoViewEvent',AutoViewEvent);
  Reg.WriteString('FontName',DefaultFont);
  Reg.WriteInteger('FontSize',DefaultSize);
  Reg.WriteString('DefaultDir',DefaultDir);
  Reg.WriteInteger('FontColor',FontColor);
  Reg.WriteInteger('BackColor',BackColor);
  Reg.WriteInteger('SelFontColor',SelFontColor);
  Reg.WriteInteger('SelBackColor',SelBackColor);
  Reg.WriteBool('LineNumbers',LineNumbers);
  Reg.WriteBool('GutterVisible',GutterVisible);
  Reg.WriteInteger('RightEdge',RightEdge);
  Reg.WriteInteger('RightColor',RightColor);
  Reg.WriteBool('GroupUndo',GroupUndo);
  Reg.WriteBool('AutoIndent',AutoIndent);
  Reg.WriteInteger('MatchingBracketColor',MatchingKlam);
  Reg.WriteBool('TestSaveGame',TestSaveGame);
  Reg.WriteString('SaveGameFile',SaveGameFile);
  Reg.WriteString('SaveGameName',SaveGameName);
  Reg.Free;
  fFiles.Free;

  TBRegSavePositions(Self,HKEY_CURRENT_USER,'Software\Rich Entertainment\MEdit\ToolBars');
end;

procedure TEditWindow.ActionFileSaveExecute(Sender: TObject);
begin
  ActiveChild.Save(false);
end;

procedure TEditWindow.OpenFile(FileName: String);
var
  Dummy  : Integer;
  Child  : TEditChild;
  Ext    : String;
  Parts  : TStringArray;
  Archiv : TArchivFile;
  Head   : TSetHeader;
  Skripts: TRecordArray;
  Skript : TExtRecord;
begin
  if not CheckGameSetFileName(FileName) then
    exit;

  Parts:=string_utils_explode('|',FileName);
  Ext:=LowerCase(ExtractFileExt(Parts[0]));

  Child:=AddEditChild;

  if Ext='.xma' then
    Child.SkriptType:=stMap;

  Parts:=string_utils_explode('|',FileName);
  if length(Parts)=1 then
    Child.EditFeld.Lines.LoadFromFile(FileName)
  else
  begin
    Archiv:=TArchivFile.Create;
    Archiv.OpenArchiv(Parts[0]);
    try
      gameset_api_ReadHeader(Archiv,Head);
      gameset_api_LoadSkripts(Archiv,Skripts);
      Skript:=record_utils_FindID(Skripts,StrToInt64('$'+Copy(Parts[1],1,8)));
      Assert(Skript<>nil);
      Child.EditFeld.Lines.Text:=Skript.GetString('Skript');
      record_utils_ClearArray(Skripts);
    except
      Archiv.Free;
      Child.Free;
      exit;
    end;
    Archiv.Free;
  end;

  Child.FileName:=FileName;
  for Dummy:=0 to 9 do
    EditFeld.ClearBookMark(Dummy);

  TEditChild(Child).CheckSkript;
  
  ActionFileSave.Enabled:=false;
  EditFeld.Modified:=false;

  AddToOpenList(FileName);
  AktuOpenList;
  StateChange;
end;

procedure TEditWindow.ActionFileNewExecute(Sender: TObject);
var
  Child: TEditChild;
begin
  {$IFDEF MAPSCRIPTS}
  if NewSkriptForm.ShowModal=mrOk then
  begin
  {$ENDIF}
    Child:=AddEditChild;
    if NewSkriptForm.mapSkript.Checked then
    begin
      Child.SkriptType:=stMap;
      with Child.EditFeld.Lines do
      begin
        Clear;
        Add('program YourMapScript;');
        Add('');
        Add('procedure GenerateMap;');
        Add('begin');
        Add('');
        Add('end;');
        Add('');
        Add('begin');
        Add('  // Hier darf kein weiterer Code folgen');
        Add('end.');
      end;
    end
    else
    begin
      with Child.EditFeld.Lines do
      begin
        Clear;
        Add('program YourName;');
        Add('');
        Add('procedure StartMission;');
        Add('begin');
        Add('');
        Add('end;');
        Add('');
        Add('begin');
        Add('  // Hier darf kein weiterer Code folgen');
        Add('  // Missionsname festlegen');
        Add('  MissionName := ''YourName'';');
        Add('');
        Add('  // Missionstype festlegen ');
        Add('  //   mzUFOs = alle mit register_ufo registrierten UFOs m�ssen zerst�rt werden');
        Add('  //   mzUser = m�ssen muss mit Mission_Win oder Mission_loose abgeschlossen werden');
        Add('  MissionType := mzUFO;');
        Add('end.');
      end;
    end;
    Child.CheckSkript;
    EditFeld.Modified:=false;
    StateChange;
  {$IFDEF MAPSCRIPTS}
  end;
  {$ENDIF}
end;

procedure TEditWindow.BuildInfoList(Browser: TTreeView; Compiler: TPSPascalCompiler);
var
  Main   : TTreeNode;

  procedure AddPopupOption(Node: TTreeNode; PopupTyp: TPopupType; InfoString: String);
  var
    Info    : PPopupInfo;
    Travers : PPopupInfo;
  begin
    New(Info);
    ZeroMemory(Info,sizeOf(TPopupInfo));
    Info.Typ:=PopupTyp;
    Info.Info:=InfoString;
    Info.Next:=nil;

    if Node.Data=nil then
      Node.Data:=Info
    else
    begin
      Travers:=Node.Data;
      while Travers.Next<>nil do
        Travers:=Travers.Next;

      Travers.Next:=Info;
    end;
  end;


  function NewNode(Parent: TTreeNode; Name: String; Image: Integer): TTreeNode;
  begin
    if Parent=nil then
      result:=Browser.Items.Add(nil,Name)
    else
      result:=Browser.Items.AddChild(Parent,Name);
    result.ImageIndex:=Image;
    result.SelectedIndex:=Image;
  end;

  function GetObjektNode(List: TStringList; ParentNode: TTreeNode; Name: String): TTreeNode;
  var
    Ind : Integer;
  begin
    Ind:=List.IndexOf(Name);
    if Ind=-1 then
    begin
      Result:=NewNode(ParentNode,Name,15);
      List.AddObject(Name,result);
    end
    else
      result:=TTreeNode(List.Objects[Ind]);
  end;

  procedure AddFunctions(Node: TTreeNode);
  var
    Dummy  : Integer;
    Image  : Integer;
    Text   : String;
    Func   : TPSRegProc;
    List   : TStringList;
    KatNode: TTreeNode;
    NNode  : TTreeNode;
  begin
    List:=TStringList.Create;
    for Dummy:=0 to Compiler.GetRegProcCount-1 do
    begin
      Func:=Compiler.GetRegProc(Dummy);
      if Func is TPSRegProc then
      begin
        if (length(Func.Name)>0) and (Func.Name[1]<>'!') then
        begin
          if pos('_',Func.OrgName)<>0 then
          begin
            KatNode:=GetObjektNode(List,Node,Copy(Func.OrgName,1,Pos('_',Func.OrgName)-1));
          end
          else
            KatNode:=GetObjektNode(List,Node,'Standardfunktionen');

          Text:=Func.OrgName;
          Text:=Format('%s%s',[Text,script_utils_GetParams(Func.Decl)]);

          if Func.Decl.Result=nil then
            Image:=17
          else
            Image:=18;

          NNode:=NewNode(KatNode,Text,Image);

          AddPopupOption(NNode,ptInsertProcedureCall,Text);
        end;
      end
      else
        Assert(false,'Unbekannte Klasse: '+Func.ClassName);
    end;
    List.Free;
  end;

  procedure AddTypes(Node: TTreeNode);
  var
    List   : TStringList;
    Dummy  : Integer;
    Kat    : String;
    Typ    : TPSType;
    Temp   : TTreeNode;

    procedure PopulateUnnamedType(Node: TTreeNode; Typ: TPSType);forward;

    procedure PopulateType(Node: TTreeNode; Typ: TPSType);
    var
      Dummy : Integer;
      Cons  : TPSConstant;
      Save : TTreeNode;
    begin
      if Typ.ClassType=TPSEnumType then
      begin
        for Dummy:=0 to Compiler.GetConstCount-1 do
        begin
          Cons:=TPSConstant(Compiler.GetConst(Dummy));
          if Cons.Value.FType=Typ then
            newNode(Node,Cons.OrgName+' = '+IntToStr(Cons.Value.ts32),31);
        end;
      end
      else if Typ.ClassType=TPSRecordType then
      begin
        for Dummy:=0 to TPSRecordType(Typ).RecValCount-1 do
        begin
          Save:=NewNode(Node,TPSRecordType(Typ).RecVal(Dummy).FieldOrgName+': '+script_utils_GetTypeName(TPSRecordType(Typ).RecVal(Dummy).aType),16);
          PopulateUnnamedType(Save,TPSRecordType(Typ).RecVal(Dummy).aType);
        end;
      end;
    end;

    procedure BuildClassType(Node: TTreeNode; Typ: TPSClassType);
    var
      Clas  : TTreeNode;
      Dummy : Integer;
      Image : Integer;
      Str   : String;
      Kat   : String;
      List  : TStringList;
    begin
      List:=TStringList.Create;
      Str:=script_utils_GetTypeName(Typ);
      if Typ.Cl.ClassInheritsFrom<>nil then
        Str:=Str+': class('+script_utils_GetTypeName(Typ.Cl.ClassInheritsFrom.aType)+')';
      Clas:=NewNode(Node,Str,15);
      for Dummy:=0 to Typ.Cl.Count-1 do
      begin
        with Typ.Cl.Items[Dummy] do
        begin
          if Typ.Cl.Items[Dummy] is TPSDelphiClassItemProperty then
          begin
            Image:=16;
            Kat:='Eigenschaften';
          end
          else
          begin
            Kat:='Methoden';
            if Decl.Result=nil then
              Image:=17
            else
              Image:=18;
          end;

          Str:=OrgName;
          Str:=Str+script_utils_GetParams(Decl);

          NewNode(GetObjektNode(List,Clas,Kat),Str,Image);
        end;
      end;
      List.Free;
    end;

    procedure PopulateUnnamedType(Node: TTreeNode; Typ: TPSType);
    begin
      if (Typ.OriginalName='') or (Typ.OriginalName[1]='!') then
      begin
        if Typ.ClassType = TPSRecordType then
        begin
          Node.ImageIndex:=15;
          Node.SelectedIndex:=15;
          
          PopulateType(Node,Typ);
        end
        else if Typ.ClassType = TPSArrayType then
          PopulateUnnamedType(Node,TPSArrayType(Typ).ArrayTypeNo);
      end;
    end;

    procedure BuildProcedureType(Node: TTreeNode; Typ: TPSProceduralType);
    var
      Decl  : String;
      Text  : String;
      NNode : TTreeNode;
    begin
      Text:=script_utils_GetTypeName(Typ)+' = ';

      if Typ.ProcDef.Result=nil then
        Decl:='procedure '
      else
        Decl:='function ';

      Decl:=Decl+script_utils_GetParams(Typ.ProcDef);
      Text:=Text+Decl;
      NNode:=NewNode(Node,Text,16);

      AddPopupOption(NNode,ptInsertProcedure,Decl+';');
    end;

    procedure BuildType(Typ: TPsType);
    begin
      if Typ.ClassType=TPSType then
        NewNode(GetObjektNode(List,Node,'Standardtypen'),script_utils_GetTypeName(Typ),16)
      else if (Typ.ClassType=TPSEnumType) or (Typ.ClassType=TPSRecordType) then
      begin
        if (Typ.ClassType=TPSEnumType) then
          Kat:='Aufz�hlungen'
        else if (Typ.ClassType=TPSRecordType) then
          Kat:='Records';

        Temp:=NewNode(GetObjektNode(List,Node,Kat),script_utils_GetTypeName(Typ),15);
        PopulateType(Temp,Typ)
      end
      else if Typ.ClassType=TPSTypeLink then
        BuildType(TPSTypeLink(Typ).LinkTypeNo)
      else if Typ.ClassType=TPSArrayType then
        NewNode(GetObjektNode(List,Node,'Arraytypen'),script_utils_GetTypeName(Typ)+' = Array of '+script_utils_GetTypeName(TPSArrayType(Typ).ArrayTypeNo),16)
      else if Typ.ClassType=TPSSetType then
        NewNode(GetObjektNode(List,Node,'Sets'),script_utils_GetTypeName(Typ)+' = set of '+script_utils_GetTypeName(TPSSetType(Typ).SetType),16)
      else if (Typ.ClassType=TPSClassType) then
        BuildClassType(GetObjektNode(List,Node,'Objekte'),TPSClassType(Typ))
      else if (Typ.ClassType=TPSVariantType) then
        exit
      else if (Typ.ClassType=TPSProceduralType) then
        BuildProcedureType(GetObjektNode(List,Node,'Procedurtypen'),TPSProceduralType(Typ))
      else
        NewNode(GetObjektNode(List,Node,'Unbekannt'),script_utils_GetTypeName(Typ)+' '+Typ.ClassName,16);
    end;

  begin
    List:=TStringList.Create;
    for Dummy:=0 to Compiler.GetTypeCount-1 do
    begin
      Typ:=Compiler.GetType(Dummy);
      if (Typ.OriginalName='') or (Typ.OriginalName[1]='!') then
        continue;

      BuildType(Typ);
    end;
    List.Free;
  end;

  procedure AddConstants(Node: TTreeNode);
  var
    Dummy   : Integer;
    PSConst : TPSConstant;
    List    : TStringList;
    Text    : String;
  begin
    List:=TStringList.Create;
    for Dummy:=0 to Compiler.GetConstCount-1 do
    begin
      PSConst:=Compiler.GetConst(Dummy);
      if PSConst.FValue.FType.BaseType<>btEnum then
      begin
        Text:=PSConst.FOrgName;
        NewNode(GetObjektNode(List,Node,PSConst.fValue.FType.OriginalName),Text,31);
      end;
    end;
    List.Free;
  end;

begin
  Browser.Items.Clear;
  Main:=NewNode(nil,'Funktionen / Prozeduren',15);
  AddFunctions(Main);
  Main.AlphaSort(true);

  Main:=NewNode(nil,'Typen',15);
  AddTypes(Main);
  Main.AlphaSort(true);

  Main:=NewNode(nil,'Konstanten',15);
  AddConstants(Main);
  Main.AlphaSort(true);

  Browser.Refresh;
end;

procedure TEditWindow.MenuExitClick(Sender: TObject);
begin
  Close;
end;

procedure TEditWindow.StateChange;
begin
//  ActiveChild.EditFeld.OnSelectionChange(Self);
  EditFeld.OnChange(Self);
end;

procedure TEditWindow.ActionSkriptCheckExecute(Sender: TObject);
begin
  ActiveChild.CheckSkript;
end;

procedure TEditWindow.ActionEditUndoExecute(Sender: TObject);
begin
  EditFeld.Undo;
end;

procedure TEditWindow.ActionEditUndoUpdate(Sender: TObject);
begin
  ActionEditUndo.Enabled:=(EditFeld<>nil) and EditFeld.CanUndo;
end;

procedure TEditWindow.ActionEditRedoUpdate(Sender: TObject);
begin
  ActionEditRedo.Enabled:=(EditFeld<>nil) and EditFeld.CanRedo;
end;

procedure TEditWindow.ActionEditRedoExecute(Sender: TObject);
begin
  EditFeld.Redo;
end;

procedure TEditWindow.ActionEditCutExecute(Sender: TObject);
begin
  EditFeld.CutToClipboard;
end;

procedure TEditWindow.ActionEditCopyExecute(Sender: TObject);
begin
  EditFeld.CopyToClipboard;
end;

procedure TEditWindow.ActionEditPasteExecute(Sender: TObject);
begin
  EditFeld.PasteFromClipboard;
end;

procedure TEditWindow.ActionEditCutUpdate(Sender: TObject);
begin
  (Sender as TAction).Enabled:=(EditFeld<>nil) and EditFeld.SelAvail;
end;

procedure TEditWindow.ActionEditPasteUpdate(Sender: TObject);
begin
  (Sender as TAction).Enabled:=(EditFeld<>nil) and EditFeld.CanPaste
end;

procedure TEditWindow.ActionFileSaveAsExecute(Sender: TObject);
begin
  if not SaveDialog.Execute then
    exit;
  ActiveChild.EditFeld.Modified:=true;
  ActiveChild.SaveAs(SaveDialog.FileName);
end;

procedure TEditWindow.SetMarkClick(Sender: TObject);
var
  X,Y: integer;
begin
  with (Sender as TMenuItem) do
  begin
    if EditFeld.IsBookmark(Tag) then
    begin
      EditFeld.GetBookMark(Tag,X,Y);
      if Y=EditFeld.CaretY then
      begin
        EditFeld.ClearBookMark(Tag);
        ImageIndex:=-1;
        exit;
      end;
    end;
    EditFeld.SetBookMark(Tag,EditFeld.CaretX,EditFeld.CaretY);
    ImageIndex:=Tag;
  end;
end;

procedure TEditWindow.GotoMarkClick(Sender: TObject);
begin
  EditFeld.GotoBookMark((Sender as TMenuItem).Tag);
end;

procedure TEditWindow.EditPopupPopup(Sender: TObject);
var
  Dummy: Integer;
begin
  for Dummy:=0 to 9 do
  begin
    if EditFeld.IsBookmark(Dummy) then
    begin
      SetMarkes.Items[dummy].ImageIndex:=dummy;
      GoMarkes.Items[Dummy].ImageIndex:=dummy;
      GoMarkes.Items[Dummy].Enabled:=true;
    end
    else
    begin
      GoMarkes.Items[Dummy].ImageIndex:=-1;
      SetMarkes.Items[dummy].ImageIndex:=-1;
      GoMarkes.Items[Dummy].Enabled:=false;
    end;
  end;
end;

procedure TEditWindow.ActionSkriptStartExecute(Sender: TObject);
begin
  ActiveChild.RunTest;
end;

procedure TEditWindow.ActionHelpKontextExecute(Sender: TObject);
var
  Page : String;
begin
  if TAction(Sender).Enabled then
  begin
    Page:=Format(SearchURL,[EditFeld.WordAtCursor]);
    ShellExecute(Self.Handle,'open',PChar(Page),'','',SW_SHOWNORMAL);
  end;
end;

procedure TEditWindow.AktuOpenList;
var
  Button: TTBXItem;
  Menu  : TTBXItem;
  Dummy : Integer;
begin
  while OpenItem.Count>0 do
    OpenItem.Delete(0);

  while ReopenMenu.Count>0 do
    ReopenMenu.Delete(0);

  for Dummy:=0 to fFiles.Count-1 do
  begin
    Button:=TTBXItem.Create(OpenItem);
    Button.Caption:=Format('&%.1d %s',[Dummy,fFiles[Dummy]]);
    Button.OnClick:=FileClick;
    OpenItem.Add(Button);

    Menu:=TTBXItem.Create(Self);
    Menu.Caption:=Format('&%.1d %s',[Dummy,fFiles[Dummy]]);
    Menu.OnClick:=FileClick;
    ReopenMenu.Add(Menu);
  end;
  ReopenMenu.Enabled:=ReopenMenu.Count>0;
  OpenItem.DropdownCombo:=OpenItem.Count>0;
end;

procedure TEditWindow.AddToOpenList(FileName: String);
var
  Dummy: Integer;
begin
  for Dummy:=0 to fFiles.Count-1 do
    if StrIComp(PChar(FileName),PChar(fFiles[Dummy]))=0 then exit;

  fFiles.Insert(0,FileName);
  while (fFiles.Count>10) do
    fFiles.delete(10);
  AktuOpenList;
end;

procedure TEditWindow.FileClick(Sender: TObject);
var
  FileName: String;
begin
  if Sender is TTBXItem then
    FileName:=Copy((Sender as TTBXItem).Caption,4,MAX_PATH)
  else
    FileName:=Copy((Sender as TMenuItem).Caption,4,MAX_PATH);

  OpenFile(FileName);
end;

function TEditWindow.AddEditChild: TEditChild;
var
  Item: TJvTabBarItem;
begin
  result:=TEditChild.Create(Self);
  SetOptions(result.EditFeld);

  Item:=JvOpenFileBar.AddTab('Unbenannt');
  Item.Selected:=true;
  Item.Data:=result;

  result.BarItem:=Item;
  
  result.Caption:='Unbenannt';

//  RefreshMDIMenu;
end;

function TEditWindow.GetEditFeld: TSynEdit;
begin
  if ActiveMDIChild=nil then
    result:=nil
  else
    result:=ActiveChild.EditFeld;
end;

procedure TEditWindow.ActionFileSaveUpdate(Sender: TObject);
begin
  ActionFileSave.Enabled:=(EditFeld<>nil) and EditFeld.Modified;
end;

procedure TEditWindow.ActionSkriptCheckUpdate(Sender: TObject);
begin
  ActionSkriptCheck.Enabled:=(EditFeld<>nil) and (not ActiveChild.DebugMode);
end;

procedure TEditWindow.IsWindowOpen(Sender: TObject);
begin
  (Sender as TAction).Enabled:=(EditFeld<>nil);
end;

procedure TEditWindow.Optionen1Click(Sender: TObject);
var
  Dummy: Integer;
begin
  if OptionsWindow=nil then
    Application.CreateForm(TOptionsWindow,OptionsWindow);
    
  if OptionsWindow.ShowModal=mrOK then
  begin
    AutoSave:=OptionsWindow.AutoSave.Checked;
    GameSet:=OptionsWindow.GameSetEdit.Text;
//    SaveGame:=OptionsWindow.
    AutoViewEvent:=OptionsWindow.AutoViewEvent.Checked;
    DefaultFont:=OptionsWindow.FontNameBox.FontName;
    DefaultSize:=StrToInt(OptionsWindow.SizeComboBox.Text);
    DefaultDir:=OptionsWindow.DirectoryEdit.Text;
    FontColor:=OptionsWindow.TextSelector.Selected;
    BackColor:=OptionsWindow.BackgroundSelector.Selected;
    SelFontColor:=OptionsWindow.SelectedTextSelector.Selected;
    SelBackColor:=OptionsWindow.SelectedBackgroundSelector.Selected;
    GutterVisible:=OptionsWindow.Gutter.Checked;
    LineNumbers:=OptionsWindow.LineNumbers.Checked;
    RightEdge:=Round(OptionsWindow.RightCol.Value);
    MatchingKlam:=OptionsWindow.MatchingBracketSelector.Selected;
    RightColor:=OptionsWindow.RightColor.Selected;
    AutoIndent:=OptionsWindow.AutoIndent.Checked;
    GroupUndo:=OptionsWindow.GroupUndo.Checked;
    SaveDialog.InitialDir:=DefaultDir;
    OpenDialog.InitialDir:=DefaultDir;

    TestSaveGame:=OptionsWindow.SaveGameBox.Checked;
    SaveGameFile:=OptionsWindow.SaveGameEdit.FileName;
    if OptionsWindow.AvaibleSaveGame.ItemIndex<>-1 then
      SaveGameName:=OptionsWindow.AvaibleSaveGame.Items[OptionsWindow.AvaibleSaveGame.ItemIndex];

    for Dummy:=0 to MDIChildCount-1 do
    begin
      with TEditChild(MDIChildren[Dummy]) do
      begin
        SetOptions(EditFeld);
      end;
    end;
    PascalSyntax.SaveToRegistry(HKEY_CURRENT_USER,'Software\Rich Entertainment\MEdit\HighLighter');
  end
  else
    PascalSyntax.LoadFromRegistry(HKEY_CURRENT_USER,'Software\Rich Entertainment\MEdit\HighLighter');
end;

procedure TEditWindow.FormCloseQuery(Sender: TObject;
  var CanClose: Boolean);
var
  Action  : TCloseAction;
  Dummy   : Integer;
begin
  CanClose:=false;
  ActionSkriptStop.Execute;
  for Dummy:=MDIChildCount-1 downto 0 do
  begin
    Action:=caNone;
    TEditChild(MDIChildren[Dummy]).FormClose(Self,Action);
    if Action=caNone then exit;
  end;
  CanClose:=true;
end;

procedure TEditWindow.IsInDebugMode(Sender: TObject);
begin
  ActionSkriptStop.Enabled:=(EditFeld<>nil) and ActiveChild.DebugMode;
end;

procedure TEditWindow.ActionSkriptStepIntoExecute(Sender: TObject);
begin
  if ActiveChild.DebugMode then
  begin
    ActiveChild.DoNextStep(true)
  end
  else
  begin
    if ActiveChild.StartTestModus then
      ActiveChild.CallStartEvent(true);
  end;
end;

procedure TEditWindow.ActionSkriptBreakPointExecute(Sender: TObject);
begin
  ActiveChild.SetBreakPoint(EditFeld.CaretY);
 end;

function TEditWindow.ShowEvents(Child: TEditChild): boolean;
begin
  Child.BringToFront;
  result:=false;
  Child.WantShowEvent:=false;
  if (Child.DebugMode) and (Child.WaitForEvent) then
  begin
    if (Child.Mission<>nil) and (Child.Mission.MissionEnd) then
    begin
      Child.StopTestModus;
      exit;
    end;
    // Debugmodus wird beendet, sobald mission abgeschlossen ist
    EventWindow.Mission:=Child.Mission;
    EventWindow.ShowEvents;
  end;
end;

function TEditWindow.GetEditChild: TEditChild;
begin
  result:=TEditChild(ActiveMDIChild);
end;

procedure TEditWindow.ActionViewMessageListUpdate(Sender: TObject);
begin
  ActionViewMessageList.Enabled:=(ActiveChild<>nil);
  ActionViewMessageList.Checked:=(ActiveChild<>nil) and (ActiveChild.MessageList.Visible);
end;                                                     

procedure TEditWindow.ActionViewMessageListExecute(Sender: TObject);
begin
  ActiveChild.MessageList.Visible:=not ActiveChild.MessageList.Visible;
  ActiveChild.Splitter.Visible:=not ActiveChild.Splitter.Visible;
end;

procedure TEditWindow.SetOptions(EditFeld: TSynEdit);
var
  Opt  : TSynEditorOptions;
begin
  with EditFeld do
  begin
    Font.Name:=DefaultFont;
    Font.Size:=DefaultSize;
    Font.Color:=FontColor;
    Color:=BackColor;

    //** Codefolding: http://www.delphipraxis.net/topic61765.html&postdays=0&postorder=asc&highlight=codefolding&start=0
    //CodeFolding.FolderBarColor:=BackColor;

    SelectedColor.ForeGround:=SelFontColor;
    SelectedColor.BackGround:=SelBackColor;
    Gutter.Visible:=GutterVisible;
    Gutter.ShowLineNumbers:=LineNumbers;
    Gutter.Font.Name:=frmMain.DefaultFont;
    Gutter.Font.Size:=frmMain.DefaultSize;
    Gutter.Font.Color:=frmMain.FontColor;
    RightEdge:=frmMain.RightEdge;
    RightEdgeColor:=frmMain.RightColor;
    Opt:=Options;
    if GroupUndo then Include(Opt,eoGroupUndo) else Exclude(Opt,eoGroupUndo);
    if AutoIndent then Include(Opt,eoAutoIndent) else Exclude(Opt,eoAutoIndent);
    Options:=Opt;
  end;
end;

procedure TEditWindow.ActionViewWatchListUpdate(Sender: TObject);
begin
  ActionViewWatchList.Checked:=WatchWindow.Visible;
end;

procedure TEditWindow.ActionViewWatchListExecute(Sender: TObject);
begin
  if WatchWindow.Visible then
    WatchWindow.Hide
  else
    WatchWindow.Show;
end;

procedure TEditWindow.ActionDebugAddValueEditorExecute(Sender: TObject);
//var
//  Command : String;
begin
{  if EditFeld.SelAvail then
    ActiveChild.AddAusdruck(EditFeld.SelText)
  else
  begin
    if InputQuery('Ausdruck hinzuf�gen','Ausdruck:',Command) then
      ActiveChild.AddAusdruck(Command);
  end;}
end;

procedure TEditWindow.ActionDebugAddValueListUpdate(Sender: TObject);
begin
  (Sender as TAction).Enabled:=(ActiveMDIChild<>nil);
end;

procedure TEditWindow.ApplicationEventsHint(Sender: TObject);
begin
  StatusBar.Panels[3].Text:=Application.Hint;
end;

procedure TEditWindow.WatchListDblClick(Sender: TObject);
begin
  ActionDebugEditValue.Execute;
end;

procedure TEditWindow.ActionSkriptStopExecute(Sender: TObject);
begin
  ActiveChild.StopTestModus;
end;

procedure TEditWindow.FormShow(Sender: TObject);
var
  Dummy: Integer;
begin
  for Dummy:=1 to ParamCount do
    OpenFile(ParamStr(Dummy));
end;

procedure TEditWindow.ActionEditFindExecute(Sender: TObject);
begin
  SearchForm.FindText.Text:=EditFeld.SelText;
  SearchForm.Show;
end;

procedure TEditWindow.FindDialogFind(Sender: TObject);
begin
//(ssoMatchCase, ssoWholeWord, ssoBackwards,
//  ssoEntireScope, ssoSelectedOnly, ssoReplace, ssoReplaceAll, ssoPrompt);
{if EditFeld.SearchReplace(FindDialog.FindText,'',[]) = 0 then
  begin
    MessageBeep(MB_ICONINFORMATION);
    MessageDlg('Suchbegriff '''+FindDialog.FindText+''' nicht gefunden',mtInformation,[mbOK],0);
  end;}
end;

procedure TEditWindow.ActionEditFindNextExecute(Sender: TObject);
begin
  if EditFeld.SearchReplace(LastText,'',LastOpt) = 0 then
    Application.MessageBox(PChar('Suchbegriff '''+LastText+''' nicht gefunden'),PChar('Hinweis'),MB_OK or MB_ICONINFORMATION);
end;

procedure TEditWindow.ActionEditFindNextUpdate(Sender: TObject);
begin
  (Sender as TAction).Enabled:=(EditFeld<>nil) and (LastText<>'');
end;

{procedure TEditWindow.ActionIntelliSenseExecute(Sender: TObject);
begin
  IntelliSense.Skript:=ActiveChild.Skript;
  IntelliSense.CurrentEditor:=EditFeld;
  IntelliSense.ShowIntelliSense;
end;}

procedure TEditWindow.ActionFileCloseExecute(Sender: TObject);
begin
  ActiveChild.Close;
end;

procedure TEditWindow.ActionFileAllCloseExecute(Sender: TObject);
var
  Child   : TEditChild;
begin
  while (MDIChildCount>0) do
  begin
    Child:=TEditChild(MDIChildren[0]);

    Child.Close;
    
    if Child.ModalResult=mrNone then
      exit;

    Child.Free;
  end;
end;

procedure TEditWindow.SpeedButton1Click(Sender: TObject);
begin
  BrowserWindow.Visible:=false;
end;

procedure TEditWindow.ActionViewObjectBrowserUpdate(Sender: TObject);
begin
  ActionViewObjectBrowser.Checked:=BrowserWindow.Visible;
end;

procedure TEditWindow.ActionViewObjectBrowserExecute(Sender: TObject);
begin
  BrowserWindow.Visible:=not BrowserWindow.Visible;
end;

procedure TEditWindow.AboutClick(Sender: TObject);
begin
   AboutBox.ShowModal;
end;

procedure TEditWindow.ActionListUpdate(Action: TBasicAction;
  var Handled: Boolean);
begin
  if AutoViewEvent and (ActiveChild<>nil) and (ActiveChild.WantShowEvent) then
  begin
    if ActiveChild.WaitForEvent then
    begin
      ShowEvents(ActiveChild);
    end;
  end;
  JvOpenFileBar.Visible:=JvOpenFileBar.Tabs.Count>0;
end;

procedure TEditWindow.ActionSkriptStepOverExecute(Sender: TObject);
begin
  if ActiveChild.DebugMode then
  begin
    ActiveChild.DoNextStep(false)
  end
  else
  begin
    if ActiveChild.StartTestModus then
      ActiveChild.CallStartEvent(true);
  end;
end;

procedure TEditWindow.InitializeObjectList;
var
  Comp: TPSPascalCompiler;
  Map : TMapGenerator;
begin
  Comp:=TPSPascalCompiler.Create;
  Comp.AllowNoBegin:=true;
  Comp.BooleanShortCircuit:=true;
  Comp.OnUses:=RegisterCompiler;
  Comp.ID:=ObjectBrowserMission;

  Comp.OnBeforeOutput:=ScriptEngineBuildOutput;

  Comp.Compile('program test; end.');

  Comp.Free;

  Map:=TMapGenerator.Create;
  Map.Compiler.ID:=ObjectBrowserMap;
  Map.Compiler.OnBeforeOutput:=ScriptEngineBuildOutput;
  Map.Compile('program test; end.');
  Map.Free;
end;

procedure TEditWindow.SaveDialogShow(Sender: TObject);
begin
{  Assert(ActiveChild<>nil);
  if ActiveChild.SkriptType=stMission then
  begin
    SaveDialog.DefaultExt:='xms';
    SaveDialog.Filter:='Missions-Skript (*.xms)|*.xms|Alle Dateien (*.*)|*.*';
  end
  else
  begin
    SaveDialog.DefaultExt:='xma';
    SaveDialog.Filter:='Karten-Skript (*.xma)|*.xms|Alle Dateien (*.*)|*.*';
  end; }
end;

function TEditWindow.CheckGameSetFileName(var FileName: String): boolean;
var
  Parts : TStringArray;
  Ext   : String;
  Dummy : Integer;
begin
  result:=true;
  Parts:=string_utils_explode('|',FileName);

  if length(Parts)=1 then
  begin
    Ext:=LowerCase(ExtractFileExt(Parts[0]));
    if Ext='.pak' then
    begin
      ChooseSkript.ArchivName:=Parts[0];
      if ChooseSkript.ShowModal=mrOK then
      begin
        for Dummy:=0 to ChooseSkript.ListBox1.Items.Count-1 do
        begin
          if ChooseSkript.ListBox1.Selected[Dummy] then
            OpenFile(Parts[0]+'|'+ChooseSkript.ListBox1.Items[Dummy]);
        end;
      end;
      result:=false;
    end;
  end;
end;

procedure TEditWindow.CodeSense1Click(Sender: TObject);
begin
  ActiveChild.SynEditAutoComplete.ActivateCompletion;
end;

procedure TEditWindow.ParamSense1Click(Sender: TObject);
begin
  ActiveChild.SynEditParamShow.ActivateCompletion;
end;

procedure TEditWindow.ActionHelpKontextUpdate(Sender: TObject);
begin
  if EditFeld<>nil then
  begin
    ActionHelpKontext.Enabled:=true;
    ActionHelpKontext.Caption:=Format('Hilfe zu ''%s''',[EditFeld.WordAtCursor]);
  end
  else
    ActionHelpKontext.Enabled:=false;
end;

procedure TEditWindow.ApplicationEventsShowHint(var HintStr: String;
  var CanShow: Boolean; var HintInfo: THintInfo);
var
  EditChild: TEditChild;
  Feld     : TSynEdit;
  Coord    : TBufferCoord;

  WordStart: TBufferCoord;
  WordEnd  : TDisplayCoord;

  Word     : String;
begin
  if HintInfo.HintControl is TSynEdit then
  begin
    EditChild:=TEditChild(HintInfo.HintControl.Parent);
    if EditChild.DebugMode then
    begin
      Feld:=TSynEdit(HintInfo.HintControl);
      Feld.GetPositionOfMouse(Coord);
      Word:=Feld.GetWordAtRowCol(Coord);

      if Word<>'' then
      begin
        WordStart:=Feld.WordStartEx(Coord);

        HintInfo.CursorRect.TopLeft:=Feld.RowColumnToPixels(Feld.BufferToDisplayPos(WordStart));

        if (Feld.Text[Feld.RowColToCharIndex(WordStart)]='.') then
        begin
          dec(WordStart.Char);
          WordStart:=Feld.WordStartEx(WordStart);
          Word:=Feld.GetWordAtRowCol(WordStart)+'.'+Word;
        end;

        WordEnd:=Feld.BufferToDisplayPos(Feld.WordEndEx(Coord));

        HintStr:=EditChild.GetWatchValue(Word);
        if HintStr<>'' then
        begin
          HintStr:=Word+' = '+HintStr;

          HintInfo.CursorRect.BottomRight:=Feld.RowColumnToPixels(WordEnd);
          HintInfo.CursorRect.Bottom:=HintInfo.CursorRect.Top+Feld.LineHeight;

          if HintInfo.CursorPos.X>=HintInfo.CursorRect.Right then
            HintStr:='';
        end;

        HintInfo.HideTimeout:=-1;
      end;
    end;

    if HintStr='' then
      CanShow:=false;
  end;
end;

procedure TEditWindow.ActionViewCodeExplorerUpdate(Sender: TObject);
begin
  ActionViewCodeExplorer.Checked:=CodeExplorerWindow.Visible;
end;

procedure TEditWindow.ActionViewCodeExplorerExecute(Sender: TObject);
begin
  CodeExplorerWindow.Visible:=not CodeExplorerWindow.Visible;
end;

procedure TEditWindow.CodeExplorerClick(Sender: TObject);
begin
  if (CodeExplorer.Selected<>nil) and (EditFeld<>nil) then
  begin
    if Integer(CodeExplorer.Selected.Data)>=0 then
    begin
      EditFeld.CaretX:=1;
      EditFeld.CaretY:=Integer(CodeExplorer.Selected.Data);
      EditFeld.EnsureCursorPosVisibleEx(true);
      ActiveChild.HighlightLine(Integer(CodeExplorer.Selected.Data));
      Windows.SetFocus(ActiveChild.Handle);
      EditFeld.SetFocus;
    end;
  end;
end;

procedure TEditWindow.CodeExplorerCompare(Sender: TObject; Node1,
  Node2: TTreeNode; Data: Integer; var Compare: Integer);
begin
  Compare:=Integer(Node1.Data)-Integer(Node2.Data);
end;

procedure TEditWindow.ActionHelpIndexExecute(Sender: TObject);
var
  Page : String;
begin
  Page:=HelpURL+'MEdit.Inhalt';
  ShellExecute(Self.Handle,'open',PChar(Page),'','',SW_SHOWNORMAL);
end;

procedure TEditWindow.ActionSkriptGotoDeclareUpdate(Sender: TObject);
var
  Attri : TSynHighlighterAttributes;
  Token : String;
begin
  if (EditFeld<>nil) and (EditFeld.GetHighlighterAttriAtRowCol(EditFeld.CaretXY, Token,Attri)) then
    ActionSkriptGotoDeclare.Enabled:=Attri.Name='Identifier'
  else
    ActionSkriptGotoDeclare.Enabled:=false;
end;

procedure TEditWindow.ActionSkriptGotoDeclareExecute(Sender: TObject);
begin
  Assert(ActiveChild<>nil);
  ActiveChild.SearchDeklaration;
end;

procedure TEditWindow.ActionSkriptPauseUpdate(Sender: TObject);
begin
  ActionSkriptPause.Enabled:=(ActiveChild<>nil) and (ActiveChild.Running);
end;

procedure TEditWindow.ActionSkriptPauseExecute(Sender: TObject);
begin
  ActiveChild.Pause;
end;

procedure TEditWindow.ObjectBrowserDoPopup(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
var
  Node  : TTreeNode;
  Data  : PPopupInfo;
  Item  : TMenuItem;
  First : Boolean;
begin
  if Button<>mbRight then
    exit;

  (Sender as TTreeView).Selected:=(Sender as TTreeView).GetNodeAt(X,Y);
  Node:=(Sender as TTreeView).Selected;

  if (Node=nil) or (Node.Data=nil) then
    exit;

  ObjektBrowserPopup.Items.Clear;

  Data:=Node.Data;

  First:=true;

  while Data<>nil do
  begin
    Item:=TMenuItem.Create(Self);

    case Data.Typ of
      ptInsertProcedure: Item.Caption:='procedure/function erstellen';
      ptInsertProcedureCall : Item.Caption:='Aufruf einf�gen';
    end;

    Item.Tag:=Integer(Data);
    Item.OnClick:=ObjektBrowserPopupClickItem;
    Item.Enabled:=(EditFeld<>nil) and (not EditFeld.ReadOnly);

    if First then
    begin
      Item.Default:=true;
      First:=false;
    end;

    ObjektBrowserPopup.Items.Add(Item);
    Data:=Data.Next;
  end;

  ObjektBrowserPopup.Popup(Mouse.CursorPos.X,Mouse.CursorPos.Y);
end;

procedure TEditWindow.PerformPopupAction(Info: PPopupInfo);
var
  Text: String;

  function CreateProcedureCall(Text: String): String;
  var
    Dummy  : Integer;
    Params : Integer;
  begin
    result:=Text;
    // Parameter ersetzen
    if Pos('(',result)<>0 then
    begin
      Params:=0;
      Dummy:=Pos('(',result)+1;

      if result[Dummy]<>')' then
        inc(Params);

      while result[Dummy]<>')' do
      begin
        if result[Dummy] in [',',';'] then
          inc(Params);

        inc(Dummy);
      end;

      result:=copy(result,1,Pos('(',result)-1);
      result:=result+'(';
      while Params>1 do
      begin
        result:=result+', ';
        dec(Params);
      end;
      result:=result+')';
    end;

    // R�ckgabewert wegschneiden
    if Pos(':',result)<>0 then
      result:=copy(result,1,Pos(':',result)-1);
  end;

begin
  if Info=nil then
    exit;

  case Info.Typ of
    ptInsertProcedure     : ActiveChild.CreateNewProcedure(Info.Info);
    ptInsertProcedureCall :
    begin
      Text:=CreateProcedureCall(Info.Info);
      EditFeld.SelText:=Text;
      if Pos('(',Text)<>0 then
        EditFeld.CaretX:=EditFeld.CaretX-(length(Text)-Pos('(',Text));
    end;
  end;

  Windows.SetFocus(ActiveChild.Handle);
  EditFeld.SetFocus;
end;

procedure TEditWindow.ObjektBrowserPopupClickItem(Sender: TObject);
var
  Data: PPopupInfo;
begin
  Data:=PPopupInfo((Sender as TMenuItem).Tag);

  PerformPopupAction(Data);
end;

procedure TEditWindow.ObjectBrowserDblClick(Sender: TObject);
begin
  if ((Sender as TTreeView).Selected<>nil) and (ActiveChild<>nil) then
  begin
    PerformPopupAction(PPopupInfo((Sender as TTreeView).Selected.Data));
  end;
end;

procedure TEditWindow.ActionFileSaveAllUpdate(Sender: TObject);
var
  Dummy: Integer;
begin
  ActionFileSaveAll.Enabled:=false;
  for Dummy:=0 to MDIChildCount-1 do
  begin
    if TEditChild(MDIChildren[Dummy]).EditFeld.Modified then
      ActionFileSaveAll.Enabled:=true;
  end;
end;

procedure TEditWindow.ActionFileSaveAllExecute(Sender: TObject);
var
  Dummy: Integer;
  List : TList;
begin
  List:=TList.Create;
  for Dummy:=0 to MDIChildCount-1 do
    List.Add(MDIChildren[Dummy]);

  for Dummy:=0 to List.Count-1 do
    TEditChild(List[Dummy]).Save(true);

  List.Free;
end;

procedure TEditWindow.ExecuteCommand(Sender: TObject);
begin
  EditFeld.ExecuteCommand(TSynEditorCommand(TAction(Sender).Tag),#0,nil);
end;

procedure TEditWindow.JvOpenFileBarTabSelected(Sender: TObject;
  Item: TJvTabBarItem);
begin
  if (Item=nil) or (Item.Data=nil) then
    exit;
    
  TEditChild(Item.Data).BringToFront;
  Windows.SetFocus(TEditChild(Item.Data).Handle);
end;

procedure TEditWindow.JvOpenFileBarTabClosed(Sender: TObject;
  Item: TJvTabBarItem);
begin
  TEditChild(Item.Data).Close;
end;

end.

