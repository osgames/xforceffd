object OptionsWindow: TOptionsWindow
  Left = 290
  Top = 163
  BorderStyle = bsDialog
  Caption = 'Einstellungen'
  ClientHeight = 493
  ClientWidth = 440
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnCreate = FormCreate
  OnShow = FormShow
  DesignSize = (
    440
    493)
  PixelsPerInch = 96
  TextHeight = 13
  object PageControl1: TPageControl
    Left = 8
    Top = 8
    Width = 425
    Height = 452
    ActivePage = TabSheet1
    Anchors = [akLeft, akTop, akRight, akBottom]
    HotTrack = True
    MultiLine = True
    TabOrder = 0
    object TabSheet1: TTabSheet
      Caption = 'Allgemein'
      object Options: TGroupBox
        Left = 8
        Top = 8
        Width = 401
        Height = 89
        Caption = 'Optionen'
        Color = clBtnFace
        ParentColor = False
        TabOrder = 0
        object Label4: TLabel
          Left = 8
          Top = 40
          Width = 99
          Height = 13
          Caption = 'Standardverzeichnis:'
        end
        object AutoSave: TCheckBox
          Left = 8
          Top = 16
          Width = 145
          Height = 17
          Caption = 'Automatisches Speichern'
          TabOrder = 0
        end
        object DirectoryEdit: TEdit
          Left = 8
          Top = 56
          Width = 281
          Height = 21
          ReadOnly = True
          TabOrder = 1
        end
        object BrowseButton: TButton
          Left = 296
          Top = 56
          Width = 91
          Height = 22
          Caption = '&Durchsuchen ...'
          TabOrder = 2
          OnClick = DirectoryEditButtonClick
        end
      end
      object GroupBox1: TGroupBox
        Left = 8
        Top = 104
        Width = 401
        Height = 313
        Caption = 'Testmodus'
        Color = clBtnFace
        ParentColor = False
        TabOrder = 1
        object Label1: TLabel
          Left = 8
          Top = 40
          Width = 45
          Height = 13
          Caption = 'Spielsatz:'
        end
        object GamesetName: TLabel
          Left = 64
          Top = 40
          Width = 3
          Height = 13
        end
        object AutoViewEvent: TCheckBox
          Left = 8
          Top = 280
          Width = 257
          Height = 17
          Caption = 'Automatisches Einblenden des Ereigniss-Fensters'
          TabOrder = 0
        end
        object NewGameBox: TRadioButton
          Left = 8
          Top = 18
          Width = 377
          Height = 17
          Caption = 'Test mit neuem Spiel'
          TabOrder = 1
          OnClick = NewGameBoxClick
        end
        object SaveGameBox: TRadioButton
          Left = 8
          Top = 88
          Width = 377
          Height = 17
          Caption = 'Test mit vorhandenem Spielstand'
          TabOrder = 2
          OnClick = NewGameBoxClick
        end
        object GameSetEdit: TJvFilenameEdit
          Left = 8
          Top = 57
          Width = 377
          Height = 21
          ClipboardCommands = []
          DefaultExt = 'pak'
          AutoCompleteOptions = [acoAutoSuggest, acoFilterPrefixes]
          AutoCompleteFileOptions = [acfFileSystem, acfFileSysDirs]
          Filter = 'Gameset (*.pak)|*.pak|Alle Dateien (*.*)|*.*'
          InitialDir = '(Directory)'
          DialogOptions = [ofNoChangeDir, ofFileMustExist]
          ButtonFlat = True
          Enabled = False
          ButtonWidth = 20
          TabOrder = 3
          Text = 'GameSetEdit'
          OnChange = GameSetEditChange
        end
        object SaveGameEdit: TJvFilenameEdit
          Left = 8
          Top = 111
          Width = 377
          Height = 21
          ClipboardCommands = []
          DefaultExt = 'sav'
          AutoCompleteOptions = [acoAutoSuggest, acoFilterPrefixes]
          AutoCompleteFileOptions = [acfFileSystem, acfFileSysDirs]
          Filter = 'Spielstand (*.sav)|*.sav|Alle Dateien (*.*)|*.*'
          DialogOptions = [ofNoChangeDir, ofFileMustExist]
          ButtonFlat = True
          Enabled = False
          TabOrder = 4
          OnChange = SaveGameEditChange
        end
        object AvaibleSaveGame: TListBox
          Left = 8
          Top = 134
          Width = 377
          Height = 129
          Enabled = False
          ItemHeight = 13
          TabOrder = 5
        end
      end
    end
    object TabSheet4: TTabSheet
      Caption = 'Editor'
      ImageIndex = 3
      object FontFrame: TGroupBox
        Left = 8
        Top = 256
        Width = 401
        Height = 65
        Caption = 'Schriftart'
        TabOrder = 0
        object FontNameLabel: TLabel
          Left = 7
          Top = 20
          Width = 45
          Height = 13
          Caption = 'Schriftart:'
        end
        object Sizelabel: TLabel
          Left = 231
          Top = 21
          Width = 60
          Height = 13
          Caption = 'Schriftgr'#246#223'e:'
        end
        object SizeComboBox: TComboBox
          Left = 303
          Top = 17
          Width = 73
          Height = 21
          ItemHeight = 0
          TabOrder = 1
          OnChange = SizeComboBoxChange
        end
        object SamplePanel: TPanel
          Left = 2
          Top = 46
          Width = 397
          Height = 17
          Align = alBottom
          BevelOuter = bvNone
          Caption = 'AaBbXxYy'
          TabOrder = 0
        end
        object FontNameBox: TJvFontComboBox
          Left = 56
          Top = 16
          Width = 153
          Height = 22
          DroppedDownWidth = 153
          MaxMRUCount = 0
          ItemIndex = -1
          Options = [foFixedPitchOnly]
          Sorted = False
          TabOrder = 2
          OnChange = FontNameBoxChange
        end
      end
      object GroupBox2: TGroupBox
        Left = 8
        Top = 8
        Width = 401
        Height = 105
        Caption = 'Editor-Optionen'
        TabOrder = 1
        object Panel1: TPanel
          Left = 232
          Top = 24
          Width = 153
          Height = 73
          BevelInner = bvRaised
          BevelOuter = bvLowered
          TabOrder = 4
          object Label10: TLabel
            Left = 8
            Top = 16
            Width = 40
            Height = 13
            Caption = 'Position:'
          end
          object RightColor: TColorBox
            Left = 7
            Top = 38
            Width = 138
            Height = 22
            Style = [cbStandardColors, cbSystemColors, cbIncludeDefault, cbCustomColor, cbPrettyNames]
            ItemHeight = 16
            TabOrder = 0
          end
          object RightCol: TJvSpinEdit
            Left = 95
            Top = 14
            Width = 50
            Height = 21
            BeepOnError = False
            MaxValue = 1023.000000000000000000
            MinValue = 1.000000000000000000
            Value = 1023.000000000000000000
            TabOrder = 1
            ClipboardCommands = []
          end
        end
        object LineNumbers: TCheckBox
          Left = 8
          Top = 33
          Width = 161
          Height = 17
          Caption = 'Zeilennummern'
          TabOrder = 0
          OnClick = LineNumbersClick
        end
        object RightEdge: TCheckBox
          Left = 240
          Top = 18
          Width = 81
          Height = 17
          Caption = 'rechte Leiste'
          TabOrder = 1
          OnClick = RightEdgeClick
        end
        object AutoIndent: TCheckBox
          Left = 8
          Top = 50
          Width = 161
          Height = 17
          Caption = 'automatischer Zeileneinzug'
          TabOrder = 2
        end
        object Gutter: TCheckBox
          Left = 8
          Top = 16
          Width = 97
          Height = 17
          Caption = 'Linkes Gutter'
          TabOrder = 3
          OnClick = GutterClick
        end
        object GroupUndo: TCheckBox
          Left = 8
          Top = 67
          Width = 161
          Height = 17
          Caption = 'Gruppe r'#252'ckg'#228'ngig'
          TabOrder = 5
        end
      end
      object ColorFrame: TGroupBox
        Left = 8
        Top = 120
        Width = 401
        Height = 121
        Caption = 'Farben'
        TabOrder = 2
        object Label5: TLabel
          Left = 8
          Top = 101
          Width = 70
          Height = 13
          Caption = 'Klammernpaar:'
          Visible = False
        end
        object Label6: TLabel
          Left = 80
          Top = 16
          Width = 45
          Height = 13
          Caption = 'Textfarbe'
        end
        object Label7: TLabel
          Left = 240
          Top = 16
          Width = 79
          Height = 13
          Caption = 'Hintergrundfarbe'
        end
        object Label8: TLabel
          Left = 8
          Top = 37
          Width = 67
          Height = 13
          Caption = 'normaler Text:'
        end
        object Label9: TLabel
          Left = 8
          Top = 61
          Width = 43
          Height = 13
          Caption = 'Auswahl:'
        end
        object TextSelector: TColorBox
          Left = 82
          Top = 32
          Width = 153
          Height = 22
          Style = [cbStandardColors, cbSystemColors, cbIncludeDefault, cbCustomColor, cbPrettyNames]
          ItemHeight = 16
          TabOrder = 0
        end
        object BackgroundSelector: TColorBox
          Left = 240
          Top = 32
          Width = 153
          Height = 22
          Style = [cbStandardColors, cbSystemColors, cbIncludeDefault, cbCustomColor, cbPrettyNames]
          ItemHeight = 16
          TabOrder = 1
        end
        object SelectedTextSelector: TColorBox
          Left = 82
          Top = 56
          Width = 153
          Height = 22
          Style = [cbStandardColors, cbSystemColors, cbIncludeDefault, cbCustomColor, cbPrettyNames]
          ItemHeight = 16
          TabOrder = 2
        end
        object SelectedBackgroundSelector: TColorBox
          Left = 240
          Top = 56
          Width = 153
          Height = 22
          Style = [cbStandardColors, cbSystemColors, cbIncludeDefault, cbCustomColor, cbPrettyNames]
          ItemHeight = 16
          TabOrder = 3
        end
        object MatchingBracketSelector: TColorBox
          Left = 82
          Top = 96
          Width = 153
          Height = 22
          Style = [cbStandardColors, cbSystemColors, cbIncludeDefault, cbCustomColor, cbPrettyNames]
          ItemHeight = 16
          TabOrder = 4
          Visible = False
        end
      end
    end
    object TabSheet2: TTabSheet
      Caption = 'Syntax-Highlight'
      ImageIndex = 1
      DesignSize = (
        417
        424)
      object Label2: TLabel
        Left = 128
        Top = 8
        Width = 24
        Height = 13
        Caption = 'Text:'
      end
      object Label3: TLabel
        Left = 272
        Top = 8
        Width = 58
        Height = 13
        Caption = 'Hintergrund:'
      end
      object AttributeList: TListBox
        Left = 8
        Top = 8
        Width = 113
        Height = 153
        ItemHeight = 13
        TabOrder = 0
        OnClick = AttributeListClick
      end
      object PreviewHighlight: TSynEdit
        Left = 8
        Top = 168
        Width = 401
        Height = 248
        Cursor = crArrow
        Anchors = [akLeft, akTop, akBottom]
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Courier New'
        Font.Style = []
        TabOrder = 1
        Gutter.Font.Charset = DEFAULT_CHARSET
        Gutter.Font.Color = clWindowText
        Gutter.Font.Height = -11
        Gutter.Font.Name = 'Terminal'
        Gutter.Font.Style = []
        HideSelection = True
        InsertCaret = ctBlock
        Lines.Strings = (
          '{ Syntaxhervorhebung }'
          ''
          '{$DEFINE TEST}'
          ''
          'program Test;'
          ''
          'function StartMission: Boolean;'
          'var'
          '  Number, I, X: Integer;'
          'begin'
          '  Number:=123 + $FF + Trunc(1.2);'
          '  Caption:='#39'The number is '#39'#13#10+IntToStr(Number);'
          'end;')
        MaxUndo = 10
        Options = [eoAutoIndent, eoDragDropEditing, eoNoCaret, eoNoSelection, eoShowScrollHint, eoSmartTabs, eoTabsToSpaces, eoTrimTrailingSpaces]
        ReadOnly = True
        SelectionMode = smLine
        OnStatusChange = PreviewHighlightStatusChange
        RemovedKeystrokes = <
          item
            Command = ecDeleteLastChar
            ShortCut = 8200
          end
          item
            Command = ecLineBreak
            ShortCut = 8205
          end
          item
            Command = ecTab
            ShortCut = 9
          end
          item
            Command = ecShiftTab
            ShortCut = 8201
          end
          item
            Command = ecContextHelp
            ShortCut = 112
          end
          item
            Command = ecMatchBracket
            ShortCut = 24642
          end>
        AddedKeystrokes = <>
      end
      object TextDefault: TCheckBox
        Left = 128
        Top = 66
        Width = 73
        Height = 17
        Caption = 'Standard'
        TabOrder = 4
        OnClick = TextDefaultClick
      end
      object BackDefault: TCheckBox
        Left = 272
        Top = 66
        Width = 73
        Height = 17
        Caption = 'Standard'
        TabOrder = 2
        OnClick = BackDefaultClick
      end
      object StyleBox: TGroupBox
        Left = 128
        Top = 88
        Width = 281
        Height = 73
        Caption = 'Schrift-Stil'
        TabOrder = 3
        object Bold: TCheckBox
          Left = 9
          Top = 45
          Width = 83
          Height = 17
          Caption = 'Fett'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          TabOrder = 0
          OnClick = ChangeHighLight
        end
        object Italic: TCheckBox
          Left = 121
          Top = 47
          Width = 83
          Height = 14
          Caption = 'Kursiv'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          TabOrder = 1
          OnClick = ChangeHighLight
        end
        object Underline: TCheckBox
          Left = 9
          Top = 21
          Width = 83
          Height = 17
          Caption = 'Unterstrichen'
          TabOrder = 2
          OnClick = ChangeHighLight
        end
        object StrikeOut: TCheckBox
          Left = 121
          Top = 21
          Width = 101
          Height = 17
          Caption = 'Durchgestrichen'
          TabOrder = 3
          OnClick = ChangeHighLight
        end
      end
      object TextColor: TColorBox
        Left = 128
        Top = 32
        Width = 137
        Height = 22
        Style = [cbStandardColors, cbSystemColors, cbIncludeDefault, cbCustomColor, cbPrettyNames]
        ItemHeight = 16
        TabOrder = 5
        OnChange = ChangeHighLight
      end
      object BackColor: TColorBox
        Left = 272
        Top = 32
        Width = 137
        Height = 22
        Style = [cbStandardColors, cbSystemColors, cbIncludeDefault, cbCustomColor, cbPrettyNames]
        ItemHeight = 16
        TabOrder = 6
        OnChange = ChangeHighLight
      end
    end
  end
  object OKButton: TButton
    Left = 249
    Top = 463
    Width = 88
    Height = 25
    Caption = '&OK'
    Default = True
    ModalResult = 1
    TabOrder = 1
    OnClick = OKButtonClick
  end
  object CancelButton: TButton
    Left = 345
    Top = 463
    Width = 88
    Height = 25
    Cancel = True
    Caption = '&Abbrechen'
    ModalResult = 2
    TabOrder = 2
  end
end
