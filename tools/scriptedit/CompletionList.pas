unit CompletionList;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, SynEdit, SynEditKbdHandler, TraceFile, MissionSkriptEngine,
  Menus, TypInfo, math;

type
  TIntelliTyp  = (itMember,itConst,itVar,itEvent,itProcedure,itFunction,itObject);

  PIntelliInfo = ^TIntelliInfo;
  TIntelliInfo = record
    Index      : Integer;
    Name       : String;
    ShowString : String;
    MemberOf   : TRecordDiscriptor;
    InEvent    : TBasisObject;
    Typ        : TIntelliTyp;
    IsRecord   : Boolean;
  end;

  TIntelliSort  = (isName,isTyp,isDeclare);

  TIntelliFilter = record
    Activ      : Boolean;
    Typs       : set of TIntelliTyp;
    MembersOf  : TRecordDiscriptor;
    InEvent    : TBasisObject;
    CanRecord  : (crIgnore,crRecordsOnly,crNoRecords);
  end;

  TIntelliSense = class(TForm)
    ListBox1: TListBox;
    PopupMenu1: TPopupMenu;
    SortName: TMenuItem;
    SortDeclare: TMenuItem;
    SortTyp: TMenuItem;
    procedure FormActivate(Sender: TObject);
    procedure ListBox1Exit(Sender: TObject);
    procedure ListBox1KeyPress(Sender: TObject; var Key: Char);
    procedure ListBox1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure ListBox1DblClick(Sender: TObject);
    procedure ListBox1DrawItem(Control: TWinControl; Index: Integer;
      Rect: TRect; State: TOwnerDrawState);
    procedure OnChangeSort(Sender: TObject);
  private
    OldShowCaret    : Boolean;
    fEditor         : TSynEdit;
    fSkript         : TSkript;
    fEngine         : TMissionSkriptEngine;
    fAllList        : TStringList;
    fSort           : TIntelliSort;
    fFilter         : TIntelliFilter;
    fStart          : Integer;
    fEnd            : Integer;
    fLines          : Integer;
    procedure SetCurrentEditor(const Value: TSynEdit);
    procedure ApplySelection;
    procedure AktuList;
    procedure SetEngine(const Value: TMissionSkriptEngine);
    procedure Clear;
    procedure Sort(Sort: TIntelliSort);
    procedure BuildFilter;
    procedure BuildPossibleList;
    procedure SetPosition;
    procedure SetSkript(const Value: TSkript);
    { Private-Deklarationen }
  public
    constructor Create(AOwner: TComponent); override;
    destructor Destroy;override;
    function RemoveEditor(Editor: TCustomSynEdit): boolean;
    procedure ShowIntelliSense;
    procedure EndIntelliSense;

    property CurrentEditor: TSynEdit read fEditor write SetCurrentEditor;
    property Skript       : TSkript read fSkript write SetSkript;
    property Engine       : TMissionSkriptEngine read fEngine;
    { Public-Deklarationen }
  end;

const
  TypName : Array[TIntelliTyp] of String = ('member',
                                            'const',
                                            'var',
                                            'event',
                                            'procedure',
                                            'function',
                                            'object');
var
  IntelliSense: TIntelliSense;

implementation

uses frmMain;

{$R *.DFM}

{ TForm1 }

constructor TIntelliSense.Create(AOwner: TComponent);
begin
  inherited;
  fAllList:=TStringList.Create; 
  fSort:=isName;
  SortTyp.Tag:=Integer(isTyp);
  SortName.Tag:=Integer(isName);
  SortDeclare.Tag:=Integer(isDeclare);
end;

destructor TIntelliSense.Destroy;
begin
  fAllList.Free;
  inherited;
end;

function TIntelliSense.RemoveEditor(Editor: TCustomSynEdit): boolean;
begin
  Result := Assigned(Editor);
end;

procedure TIntelliSense.SetCurrentEditor(const Value: TSynEdit);
begin
  if (fEditor <> nil) then
  begin
    RemoveEditor(fEditor);
    fEditor := nil;
  end;
  fEditor := Value;
  if (fEditor <> nil) then
  begin
    with fEditor do
    begin
//      AddKeyDownHandler(fKeyDownProc);
//      AddKeyPressHandler(fKeyPressProc);
    end;
    fEditor.FreeNotification(Self);
  end;
end;

procedure TIntelliSense.FormActivate(Sender: TObject);
begin
  Visible := True;
  TCustomSynEdit (CurrentEditor).AddFocusControl(self);
end;

procedure TIntelliSense.ShowIntelliSense;
begin
  if (CurrentEditor <> nil) and (CurrentEditor.ReadOnly=false) then
  begin
    // Filter aufbauen
    FillChar(fFilter,SizeOf(fFilter),#0);
    try
      fSkript.ParseOnlyDeklarations(CurrentEditor.Lines);
    except
    end;
    SetEngine(Engine);
    try
      BuildFilter;
    except
      on E: Exception do
      begin
        MessageBeep(MB_ICONERROR);
        MessageDlg('Code-Vervollst�ndigung kann nicht aufgerufen werden, da das Skript Fehler enth�lt.',mtError,[mbOK],0);
        exit;
      end;
    end;
    if not fFilter.Activ then exit;
    if CurrentEditor.GetWordAtRowCol(CurrentEditor.CaretXY)=EmptyStr then
    begin
      fStart:=CurrentEditor.CaretX;
      fEnd:=CurrentEditor.CaretX;
    end
    else
    begin
      fStart:=CurrentEditor.WordStart.X;
      fEnd:=CurrentEditor.WordEnd.x;
    end;
    fLines:=CurrentEditor.Lines.Count;
    
    BuildPossibleList;
    AktuList;

    OldShowCaret := TCustomSynEdit(CurrentEditor).AlwaysShowCaret;
    TCustomSynEdit(CurrentEditor).AlwaysShowCaret := True;
    TCustomSynEdit(CurrentEditor).UpdateCaret;

    Show;
  end;
end;

procedure TIntelliSense.ListBox1Exit(Sender: TObject);
begin
  EndIntelliSense;
end;

procedure TIntelliSense.ListBox1KeyPress(Sender: TObject; var Key: Char);
begin
  if Key=#27 then exit; { Escape }
  if not (Key in ['A'..'Z','a'..'z','0'..'9','�','�','�','�','�','�',#8]) then
    ApplySelection;
  CurrentEditor.Perform(WM_CHAR,Integer(Key),0);
  if CurrentEditor.GetWordAtRowCol(CurrentEditor.CaretXY)=EmptyStr then
    fEnd:=high(Word)
  else
    fEnd:=CurrentEditor.WordEnd.x;
  AktuList;
  Key:=#0;
end;

procedure TIntelliSense.ListBox1KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if (Key=VK_ESCAPE) then
  begin
    EndIntelliSense;
    exit;
  end
  else if (Key=VK_UP) or (Key=VK_DOWN) or (Key=VK_PRIOR) or (KEY=VK_NEXT) or
          (Key=VK_HOME) or (Key=VK_END) then
  begin
    exit
  end
  else if (Key=VK_RETURN) then
  begin
    ApplySelection;
    exit;
  end;

  CurrentEditor.Perform(WM_KEYDOWN,Key,0);
  if (Key=VK_LEFT) or (KEY=VK_RIGHT) or (KEY=VK_DELETE) then
    AktuList;
  if not Visible then exit;
  Key:=0;
end;

procedure TIntelliSense.ApplySelection;
begin
  if not Visible then exit;
  if ListBox1.ItemIndex<>-1 then
  begin
    CurrentEditor.BeginUpdate;
    if CurrentEditor.CaretX-1<=length(CurrentEditor.Lines[CurrentEditor.CaretY-1]) then
    begin
      CurrentEditor.SetSelWord;
      if CurrentEditor.SelText<>CurrentEditor.GetWordAtRowCol(CurrentEditor.CaretXY) then
      begin
        CurrentEditor.BeginUndoBlock;
        CurrentEditor.CaretX:=CurrentEditor.CaretX-1;
        CurrentEditor.SetSelWord;
        CurrentEditor.EndUndoBlock;
      end;
    end;
    CurrentEditor.SelText:=fAllList[Integer(ListBox1.Items.Objects[ListBox1.ItemIndex])];
    CurrentEditor.EndUpdate;
  end;
  EndIntelliSense;
end;

procedure TIntelliSense.ListBox1DblClick(Sender: TObject);
begin
  ApplySelection;
end;

procedure TIntelliSense.SetEngine(const Value: TMissionSkriptEngine);
var
  Dummy : Integer;
  PInfo : PIntelliInfo;
  Obj   : TBasisObject;
  Index : Integer;
  List  : TList;

  procedure AddRecord(Rec: TRecordDiscriptor);
  var
    Dummy: Integer;
  begin
    for Dummy:=0 to Rec.MemberCount-1 do
    begin
      GetMem(PInfo,SizeOf(TIntelliInfo));
      FillChar(PInfo^,SizeOf(TIntelliInfo),#0);
      with Rec.Members[Dummy] do
      begin
        fAllList.AddObject(Name,TObject(PInfo));
        PInfo.Name:=Name;
        PInfo.Index:=Index;
        PInfo.MemberOf:=Rec;
        if Typ=mtMember then
        begin
          PInfo.Typ:=itMember;
          if ValueType=vtRecord then
            PInfo.ShowString:=PInfo.Name+'|: '+Discriptor.Name
          else
            PInfo.ShowString:=PInfo.Name+'|: '+NameOfType(ValueType);
        end
        else
        begin
          if Func.Return=vtNone then
          begin
            PInfo.ShowString:=PInfo.Name+'|';
            PInfo.Typ:=itProcedure;
          end
          else
          begin
            PInfo.ShowString:=Func.Deklaration;
            PInfo.Typ:=itFunction;
          end;
        end;
      end;
      inc(Index);
    end;
  end;

  procedure AddVars(Func: TOwnerFunction);
  var
    Dummy: Integer;
  begin
    for Dummy:=0 to Func.VarCount-1 do
    begin
      GetMem(PInfo,SizeOf(TIntelliInfo));
      FillChar(PInfo^,SizeOf(TIntelliInfo),#0);
      PInfo.Index:=Index;
      inc(Index);
      with Func.Variables[Dummy] do
      begin
        fAllList.AddObject(Name,TObject(PInfo));
        PInfo.ShowString:=Name+'|';
        PInfo.Typ:=itVar;
        PInfo.InEvent:=Func;
        if ValueType=vtRecord then
        begin
          PInfo.ShowString:=PInfo.ShowString+': '+AsRecord.RecordTyp.Name;
          PInfo.IsRecord:=true;
        end;
      end;
    end;
  end;

begin
  Clear;
  Index:=0;
  fEngine := Value;
  fAllList.Clear;
  if fEngine<>nil then
  begin
    List:=fEngine.Objekts;
    Dummy:=0;
    while Dummy<>-1 do
    begin
      if Dummy>=List.Count then
      begin
        if List=fEngine.Objekts then
        begin
          List:=fSkript.Objekts;
          if List.Count=0 then
            break;
          Dummy:=0;
        end
        else
          break;
      end;
      Obj:=TBasisObject(List[Dummy]);

      if Obj.IsPublic then
      begin
        GetMem(PInfo,SizeOf(TIntelliInfo));
        FillChar(PInfo^,SizeOf(TIntelliInfo),#0);
        PInfo.Index:=Index;
        inc(Index);
        fAllList.AddObject(Obj.Name,TObject(PInfo));
        with Obj do
        begin
          PInfo.Name:=Name;
          PInfo.ShowString:=Name+'|';
          case ObjektType of
            otFunction :
            begin
              if TFunction(Obj).Return=vtNone then
                PInfo.Typ:=itProcedure
              else
                PInfo.Typ:=itFunction;
              PInfo.ShowString:=TFunction(Obj).Deklaration;
            end;
            otOwnerFunction :
            begin
              PInfo.Typ:=itEvent;
              AddVars(TOwnerFunction(Obj));
            end;
            otVariable      :
            begin
              if TVariable(Obj).ReadOnly then
                PInfo.Typ:=itConst
              else
                PInfo.Typ:=itVar;
              if TVariable(Obj).ValueType=vtRecord then
              begin
                PInfo.ShowString:=Name+'|: '+TVariable(Obj).AsRecord.RecordTyp.Name;
                PInfo.IsRecord:=true;
              end;
            end;
            otRecord        : PInfo.Typ:=itObject;
          end;
        end;
      end;
      if Obj.ObjektType=otRecord then
        AddRecord(TRecordDiscriptor(fEngine.Objekts[Dummy]));
      inc(Dummy);
    end;
  end;
  Sort(fSort);
end;

procedure TIntelliSense.AktuList;
var
  Text       : String;
  Dummy      : Integer;
  Len        : Integer;
  Index      : Integer;
begin
  if (fLines<>CurrentEditor.Lines.Count) or (fStart>CurrentEditor.CaretX) or (fEnd<CurrentEditor.CaretX) then
  begin
    EndIntelliSense;
    exit;
  end;
  Text:=CurrentEditor.GetWordAtRowCol(CurrentEditor.CaretXY);
  Len:=Length(Text);
  Index:=-1;
  for Dummy:=0 to ListBox1.Items.Count-1 do
  begin
    if CompareText(Copy(ListBox1.Items[Dummy],1,Len),Text)=0 then
    begin
      Index:=Dummy;
      break;
    end;
  end;
  if (Text=EmptyStr) then
    Index:=0
  else if Index>ListBox1.Items.Count-1 then
    Index:=ListBox1.Items.Count-1;
  ListBox1.ItemIndex:=Index;
  SetPosition;
end;


procedure TIntelliSense.ListBox1DrawItem(Control: TWinControl;
  Index: Integer; Rect: TRect; State: TOwnerDrawState);
var
  PInfo : PIntelliInfo;
  typ   : String;
  First : String;
  TxtWid: Integer;
begin
  with ListBox1.Canvas do
  begin
    FillRect(Rect);
    Font.Style:=[fsBold];
    PInfo:=PIntelliInfo(fAllList.Objects[Integer(ListBox1.Items.Objects[Index])]);
    First:=GetShortHint(PInfo.ShowString);
    TextOut(Rect.Left+60,Rect.Top+2,First);
    TxtWid:=TextWidth(First);
    Font.Style:=[];
    TextOut(Rect.Left+65+TxtWid,Rect.Top+2,GetLongHint(PInfo.ShowString));
    Typ:=TypName[PInfo.Typ];
    TextOut(Rect.Left+2,Rect.Top+2,typ);
  end;
end;

procedure TIntelliSense.Clear;
var
  Dummy: Integer;
begin
  for Dummy:=0 to fAllList.Count-1 do
    FreeMem(Pointer(fAllList.Objects[Dummy]));
  fAllList.Clear;
end;

procedure TIntelliSense.OnChangeSort(Sender: TObject);
begin
  (Sender as TMenuItem).Checked:=true;
  Sort(TIntelliSort((Sender as TMenuItem).Tag));
  BuildPossibleList;
  AktuList;
end;

procedure TIntelliSense.Sort(Sort: TIntelliSort);
var
  I, J, P: Integer;

  function Compare(Index1,Index2: Integer): Integer;
  begin
    result:=0;
    case Sort of
      isName    : result:=CompareText(fAllList[Index1],fAllList[Index2]);
      isTyp     : result:=Integer(PIntelliInfo(fAllList.Objects[Index1]).Typ)-Integer(PIntelliInfo(fAllList.Objects[Index2]).Typ);
      isDeclare : result:=Integer(PIntelliInfo(fAllList.Objects[Index1]).Index)-Integer(PIntelliInfo(fAllList.Objects[Index2]).Index);
    end;
  end;

  procedure QuickSort(L,H: Integer);
  begin
    if L>=H then exit;
    repeat
      I := L;
      J := H;
      P := (L + H) shr 1;
      repeat
        while Compare(I, P) < 0 do Inc(I);
        while Compare(J, P) > 0 do Dec(J);
        if I <= J then
        begin
          fAllList.Exchange(I, J);
          if P = I then
            P := J
          else if P = J then
            P := I;
          Inc(I);
          Dec(J);
        end;
      until I > J;
      if L < J then QuickSort(L, J);
      L := I;
    until I >= H;
  end;
begin
  QuickSort(0,fAllList.Count-1);
  fSort:=Sort;
end;

procedure TIntelliSense.BuildFilter;
var
  LineTokens : Array of TToken;
  Parser     : TLineParser;
  tmpParser  : TLineParser;
  Index      : Integer;
  WasPoint   : Boolean;
  Obj        : TBasisObject;
  Ausdruck   : String;
  Klammern   : Integer;
  Ende       : Boolean;
  Val        : TValue;
  LastToken  : TTokenType;
  Location   : record
                 Func  : TOwnerFunction;
                 InVar : boolean;
               end;

  procedure InVarBereich;
  var
    Line     : Integer;
    WasBegin : Boolean;
  begin
    Line:=CurrentEditor.CaretY-2;
    WasBegin:=false;
    while Line>=0 do
    begin
      with TLineParser.Create(CurrentEditor.Lines[Line],Engine,nil) do
      begin
        Restart;
        NextToken;
        if TokenType=ttBei then
        begin
          NextToken;
          Obj:=fSkript.FindObjectFromName(TokenText);
          if Obj is TOwnerFunction then
            Location.Func:=TOwnerFunction(Obj);
          Free;
          exit;
        end
        else if TokenType=ttBlockBegin then
          WasBegin:=true
        else if TokenType=ttVar then
        begin
          if not WasBegin then
            Location.InVar:=true;
        end;
        Free;
      end;
      dec(Line);
    end;
  end;

  function GetLastToken: TTokenType;
  var
    LastToken: TTokenType;
    EndPos   : Integer;
    Index    : Integer;
  begin
    Index:=0;
    LastToken:=ttNone;
    with Parser do
    begin
      Restart;
      while true do
      begin
        NextToken;
        EndPos:=ParsingPos;
        if TokenType in [ttBezeichner,ttFalsch,ttWahr,ttZahl] then
          inc(EndPos,Length(TokenText));
        if (TokenType=ttLineEnd) or (EndPos>=CurrentEditor.CaretX) then
        begin
          result:=LastToken;
          exit;
        end;
        SetLength(LineTokens,Index+1);
        LineTokens[Index].Typ:=TokenType;
        LineTokens[Index].XPos:=ParsingPos;
        LineTokens[Index].Text:=TokenText;
        inc(Index);
        LastToken:=TokenType;
      end;
    end;
  end;

begin
  if Copy(Trim(CurrentEditor.Lines[CurrentEditor.CaretY-1]),1,2)='//' then
  begin
    fFilter.Activ:=false;
    exit;
  end;
  Location.Func:=nil;
  Location.InVar:=false;
  fFilter.Activ:=true;
  fFilter.Typs:=[low(TIntelliTyp)..high(TIntelliTyp)];
  fFilter.CanRecord:=crIgnore;
  SetLength(LineTokens,0);
  InVarBereich;
  Parser:=TLineParser.Create(CurrentEditor.Lines[CurrentEditor.CaretY-1],Engine,nil);
  LastToken:=GetLastToken;
  if Location.InVar then
  begin
    case LastToken of
      ttEqual       : fFilter.Typs:=[itConst];
      ttDoublePoint : fFilter.Typs:=[itObject];
      else
        fFilter.Activ:=false;
    end;
  end
  else
  begin
    fFilter.InEvent:=Location.Func;
    case LastToken of
      ttNone   :  fFilter.Typs:=[itVar,itProcedure,itFunction];
      ttPoint  :
      begin
        fFilter.Activ:=false;
    //    fFilter.Typs:=[itMember];
        if Length(LineTokens)=1 then exit;

        // Ausdruck vor dem Punkt ermitteln
        Index:=high(LineTokens)-1;
        Ende:=false;
        Klammern:=0;
        Ausdruck:='';
        WasPoint:=true;
        while not Ende do
        begin
          if LineTokens[Index].Typ=ttKlammerZu then
            inc(Klammern)
          else if Klammern>0 then
          begin
            if LineTokens[Index].Typ=ttKlammerAuf then
              dec(Klammern)
          end
          else if LineTokens[Index].Typ=ttBezeichner then
          begin
            if WasPoint then
              WasPoint:=false
            else
              Ende:=true;
          end
          else if LineTokens[Index].Typ=ttPoint then
          begin
            if WasPoint then
              Ende:=true
            else
              WasPoint:=true
          end
          else
            Ende:=true;

          if (not Ende) then
          begin
            Ausdruck:=LineTokens[Index].Text+Ausdruck;
            dec(Index);
            if (Index<0) then
              Ende:=true;
          end;
        end;
        if Ausdruck='' then
          exit;

        tmpParser:=TLineParser.Create(Ausdruck,Engine,nil);
        tmpParser.SetFunction(Location.Func);
        tmpParser.NextToken;
        tmpParser.CreateAusdruck(Val);
        tmpParser.Free;
        if Val.ValueTyp<>vtRecord then exit;
        fFilter.MembersOf:=Val.RecordTyp;
        fFilter.Activ:=true;
        Val.Free;
      end;
      ttAdd,ttSubtract,ttMultiply,ttDivide,ttEqual,ttGreaterEqual,ttwenn,ttGreater,ttSmaller,ttSmallerEqual,ttGreaterSmaller,
      ttUnd,ttNicht,ttOder,ttTo,ttDownTo,ttWhile,
      ttAssign,ttKlammerAuf,ttKomma   :  fFilter.Typs:=[itVar,itFunction,itConst];
      ttFor :
      begin
        fFilter.Typs:=[itVar];
        fFilter.CanRecord:=crNoRecords;
      end;
      else
        fFilter.Activ:=False;
    end;
  end;
end;

procedure TIntelliSense.EndIntelliSense;
begin
  Hide;
  RemoveEditor(CurrentEditor);
  TCustomSynEdit(CurrentEditor).AlwaysShowCaret := OldShowCaret;
end;

procedure TIntelliSense.SetPosition;
var
  Pt: TPoint;
begin
  Pt:=CurrentEditor.ClientToScreen(Point(CurrentEditor.CaretXPix,CurrentEditor.CaretYPix+CurrentEditor.LineHeight));
  if Pt.Y+Height>Screen.Height then
    Top:=Pt.Y-Height-CurrentEditor.LineHeight
  else
    Top:=Pt.y;

  if Pt.X+Width>Screen.Width then
    Left:=Pt.X-Width
  else
    Left:=Pt.X;
end;

procedure TIntelliSense.BuildPossibleList;
var
  Dummy      : Integer;
  PInfo      : PIntelliInfo;
  Size       : Integer;
  tmpSize    : Integer;
begin
  ListBox1.Items.BeginUpdate;
  ListBox1.Items.Clear;
  Size:=0;
  for Dummy:=0 to fAllList.Count-1 do
  begin
    PInfo:=PIntelliInfo(fAllList.Objects[Dummy]);

    if not (PInfo.Typ in fFilter.Typs) then
      continue;

    if PInfo.MemberOf<>fFilter.MembersOf then
      continue;

    if (PInfo.InEvent<>nil) and (fFilter.InEvent<>PInfo.InEvent) then
      continue;

    if fFilter.CanRecord<>crIgnore then
    begin
      if ((fFilter.CanRecord=crRecordsOnly) and (not PInfo.IsRecord)) or
         ((fFilter.CanRecord=crNoRecords) and (PInfo.IsRecord)) then
        continue;
    end;
    ListBox1.Items.AddObject(fAllList[Dummy],Pointer(Dummy));
    ListBox1.Canvas.Font.Style:=[fsBold];
    tmpSize:=ListBox1.Canvas.TextWidth(GetShortHint(PInfo.ShowString));
    ListBox1.Canvas.Font.Style:=[];
    tmpSize:=tmpSize+ListBox1.Canvas.TextWidth(GetLongHint(PInfo.ShowString));
    if tmpSize>Size then
      Size:=tmpSize;
  end;
  inc(Size,76);
  if ListBox1.Items.Count>6 then
    Inc(Size,20);
  Width:=Size;
  Height:=min(ListBox1.Items.Count,6)*ListBox1.ItemHeight+4;
  ListBox1.Items.EndUpdate;
end;

procedure TIntelliSense.SetSkript(const Value: TSkript);
begin
  fSkript := Value;
  fEngine:=fSkript.Engine;
  SetEngine(fEngine);
end;

end.
