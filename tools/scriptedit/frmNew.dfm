object NewSkriptForm: TNewSkriptForm
  Left = 315
  Top = 204
  BorderStyle = bsDialog
  Caption = 'neues Skript'
  ClientHeight = 121
  ClientWidth = 200
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 13
  object GroupBox1: TGroupBox
    Left = 8
    Top = 8
    Width = 185
    Height = 73
    Caption = 'Skripttyp'
    TabOrder = 0
    object missionSkript: TRadioButton
      Left = 8
      Top = 20
      Width = 169
      Height = 17
      Caption = 'Missionsskript (*.xms)'
      Checked = True
      TabOrder = 0
      TabStop = True
    end
    object mapSkript: TRadioButton
      Left = 8
      Top = 44
      Width = 169
      Height = 17
      Caption = 'Kartenskript (*.xma)'
      TabOrder = 1
    end
  end
  object BitBtn1: TBitBtn
    Left = 8
    Top = 88
    Width = 89
    Height = 25
    TabOrder = 1
    Kind = bkOK
  end
  object BitBtn2: TBitBtn
    Left = 104
    Top = 88
    Width = 89
    Height = 25
    TabOrder = 2
    Kind = bkCancel
  end
end
