object IntelliSense: TIntelliSense
  Left = 337
  Top = 283
  BorderStyle = bsNone
  ClientHeight = 123
  ClientWidth = 451
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  FormStyle = fsStayOnTop
  OldCreateOrder = False
  OnDeactivate = ListBox1Exit
  PixelsPerInch = 96
  TextHeight = 13
  object ListBox1: TListBox
    Left = 0
    Top = 0
    Width = 451
    Height = 123
    Align = alClient
    ItemHeight = 17
    Items.Strings = (
      'Item1'
      'Item2'
      'Item3'
      'Item3'
      'Item4'
      'Item5'
      'Item6')
    PopupMenu = PopupMenu1
    Style = lbOwnerDrawFixed
    TabOrder = 0
    OnDblClick = ListBox1DblClick
    OnDrawItem = ListBox1DrawItem
    OnExit = ListBox1Exit
    OnKeyDown = ListBox1KeyDown
    OnKeyPress = ListBox1KeyPress
  end
  object PopupMenu1: TPopupMenu
    Left = 136
    Top = 32
    object SortName: TMenuItem
      Caption = 'nach &Namen sortieren'
      Checked = True
      GroupIndex = 1
      RadioItem = True
      ShortCut = 16462
      OnClick = OnChangeSort
    end
    object SortDeclare: TMenuItem
      Tag = 1
      Caption = 'nach &Deklaration sortieren'
      GroupIndex = 1
      RadioItem = True
      ShortCut = 16452
      OnClick = OnChangeSort
    end
    object SortTyp: TMenuItem
      Tag = 2
      Caption = 'nach &Typ sortieren'
      GroupIndex = 1
      RadioItem = True
      ShortCut = 16468
      OnClick = OnChangeSort
    end
  end
end
