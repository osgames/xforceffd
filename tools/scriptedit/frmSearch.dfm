object SearchForm: TSearchForm
  Left = 1280
  Top = 481
  BorderStyle = bsDialog
  Caption = 'Suchen und Ersetzen'
  ClientHeight = 225
  ClientWidth = 445
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  FormStyle = fsStayOnTop
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 8
    Top = 13
    Width = 67
    Height = 13
    Caption = '&Suchen nach:'
    FocusControl = FindText
  end
  object Label2: TLabel
    Left = 8
    Top = 37
    Width = 74
    Height = 13
    Caption = '&Ersetzen durch:'
    FocusControl = ReplaceText
  end
  object FindText: TEdit
    Left = 88
    Top = 9
    Width = 241
    Height = 21
    TabOrder = 0
    OnKeyPress = FindTextKeyPress
  end
  object ReplaceText: TEdit
    Left = 88
    Top = 33
    Width = 241
    Height = 21
    TabOrder = 1
  end
  object GroupBox1: TGroupBox
    Left = 8
    Top = 64
    Width = 161
    Height = 89
    Caption = 'Optionen'
    TabOrder = 2
    object CaseSensitiv: TCheckBox
      Left = 8
      Top = 16
      Width = 135
      Height = 17
      Caption = '&Gro'#223'-/Kleinschreibung'
      TabOrder = 0
    end
    object WholeWord: TCheckBox
      Left = 8
      Top = 32
      Width = 135
      Height = 17
      Caption = 'Nur ganze &W'#246'rter'
      TabOrder = 1
    end
    object Confirmation: TCheckBox
      Left = 8
      Top = 48
      Width = 135
      Height = 17
      Caption = 'mit &Best'#228'tigung'
      Checked = True
      State = cbChecked
      TabOrder = 2
    end
    object RegEx: TCheckBox
      Left = 8
      Top = 64
      Width = 135
      Height = 17
      Caption = 'Re&gul'#228're Ausdr'#252'cke'
      TabOrder = 3
    end
  end
  object GroupBox2: TGroupBox
    Left = 176
    Top = 64
    Width = 153
    Height = 89
    Caption = 'Richtung'
    TabOrder = 3
    object Up: TRadioButton
      Left = 8
      Top = 16
      Width = 113
      Height = 17
      Caption = '&Vorw'#228'rts'
      Checked = True
      TabOrder = 0
      TabStop = True
    end
    object Down: TRadioButton
      Left = 8
      Top = 32
      Width = 113
      Height = 17
      Caption = '&R'#252'ckw'#228'rts'
      TabOrder = 1
    end
  end
  object GroupBox3: TGroupBox
    Left = 8
    Top = 160
    Width = 161
    Height = 57
    Caption = 'Bereich'
    TabOrder = 4
    object WholeText: TRadioButton
      Left = 8
      Top = 16
      Width = 113
      Height = 17
      Caption = 'G&esamte Datei'
      Checked = True
      TabOrder = 0
      TabStop = True
    end
    object SelectedOnly: TRadioButton
      Left = 8
      Top = 33
      Width = 113
      Height = 17
      Caption = '&Markierter Bereich'
      TabOrder = 1
    end
  end
  object Beginn: TGroupBox
    Left = 176
    Top = 160
    Width = 153
    Height = 57
    Caption = 'Bereich'
    TabOrder = 5
    object FromCursor: TRadioButton
      Left = 8
      Top = 16
      Width = 113
      Height = 17
      Caption = 'Ab &Cursor'
      Checked = True
      TabOrder = 0
      TabStop = True
    end
    object BeginnFile: TRadioButton
      Left = 8
      Top = 32
      Width = 113
      Height = 17
      Caption = '&Dateianfang'
      TabOrder = 1
    end
  end
  object SearchButton: TButton
    Left = 344
    Top = 8
    Width = 89
    Height = 25
    Caption = 'S&uchen'
    TabOrder = 6
    OnClick = SearchButtonClick
  end
  object Button1: TButton
    Left = 344
    Top = 192
    Width = 89
    Height = 25
    Cancel = True
    Caption = 'Schliessen'
    TabOrder = 7
    OnClick = Button1Click
  end
  object Replace: TButton
    Left = 344
    Top = 40
    Width = 89
    Height = 25
    Caption = 'Erset&zen'
    TabOrder = 8
    OnClick = ReplaceClick
  end
  object ReplaceAll: TButton
    Left = 344
    Top = 72
    Width = 89
    Height = 25
    Caption = 'Alle Ersetzen'
    TabOrder = 9
    OnClick = ReplaceAllClick
  end
end
