unit frmHelp;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ComCtrls, StdCtrls, ExtCtrls, OleCtrls, SHDocVw, ToolWin,FileCtrl, Menus,
  ActnList, ImgList;

type
  THelpForm = class(TForm)
    ToolBar1: TToolBar;
    WebBrowser1: TWebBrowser;
    Panel1: TPanel;
    Label1: TLabel;
    ProgressBar1: TProgressBar;
    PopupMenu1: TPopupMenu;
    Schliessen1: TMenuItem;
    ToolButton1: TToolButton;
    ToolButton2: TToolButton;
    ImageList1: TImageList;
    ActionList1: TActionList;
    ActionBack: TAction;
    ActionForward: TAction;
    ListBox1: TListBox;
    Splitter1: TSplitter;
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure Schliessen1Click(Sender: TObject);
    procedure WebBrowser1CommandStateChange(Sender: TObject;
      Command: Integer; Enable: WordBool);
    procedure ActionBackExecute(Sender: TObject);
    procedure ActionForwardExecute(Sender: TObject);
    procedure ListBox1DblClick(Sender: TObject);
  private
    fIndexList: TStringList;
    procedure BuildHelpFile(Time: TDateTime);
    { Private-Deklarationen }
  public
    procedure TestBuildHelpFile;
    { Public-Deklarationen }
  end;

var
  HelpForm: THelpForm;

implementation

{$R *.DFM}

procedure THelpForm.BuildHelpFile(Time: TDateTime);
var
  List        : TStringList;
  fFileList   : TStringList;
  Dummy       : Integer;
  Lines       : Integer;
  Count       : Integer;
  Text        : String;
  InStyleSheet: Boolean;
  InTopic     : Boolean;
  LinkPos     : Integer;
  EndPos      : Integer;
  Temp        : String;

  procedure StepBy(Lines: Integer);
  begin
    inc(Dummy,Lines);
    ProgressBar1.StepBy(Lines);
    ProgressBar1.Refresh;
  end;

  function FindTopicNumber(Topic: String): String;
  var
    Dummy: Integer;
  begin
    for Dummy:=1 to fIndexList.Count-1 do
    begin
      if LowerCase(Topic)=LowerCase(Copy(fIndexList[Dummy],6,100)) then
      begin
        result:=Copy(fIndexList[Dummy],1,4);
        exit;
      end;
    end;
  end;
begin
  Panel1.Visible:=true;
  Panel1.Refresh;
  ProgressBar1.Position:=0;
  List:=TStringList.Create;
  List.LoadFromFile('help.dat');
  ProgressBar1.Max:=List.Count*2;
  fIndexList.Clear;
  fIndexList.Add(DateTimeToStr(Time));
  ForceDirectories(ExtractFilePath(Application.ExeName)+'help');
  Lines:=List.Count;
  Dummy:=0;
  Count:=0;
  while (Dummy<Lines) do
  begin
    if StrLComp(PCHar(List[Dummy]),'$Topic ',7)=0 then
    begin
      fIndexList.Add(Format('%.4d ',[Count])+Copy(List[Dummy],8,100));
      StepBy(1);
      inc(Count);
    end;
    StepBy(1);
  end;
  fFileList:=TStringList.Create;
  InStyleSheet:=false;
  InTopic:=false;
  Count:=0;
  Dummy:=0;
  while (Dummy<Lines) do
  begin
    if InStyleSheet then
    begin
      if List[Dummy]='---' then
      begin
        fFileList.SaveToFile('help/help.css');
        InStyleSheet:=false;
      end
      else
      begin
        fFileList.Add(List[Dummy]);
      end;
    end;
    if InTopic then
    begin
      if List[Dummy]='---' then
      begin
        fFileList.Add('</BODY></HTML>');
        fFileList.SaveToFile('help/'+Format('Topic%.4d.html',[Count]));
        InTopic:=false;
        inc(Count);
      end
      else
      begin
        Text:=List[Dummy];
        if Text<>'<LINE>' then
          Text:=Text+'<BR>';
        Text:=StringReplace(Text,'<PROC>','<SPAN CLASS="Code">',[rfReplaceAll,rfIgnoreCase]);
        Text:=StringReplace(Text,'</PROC>','</SPAN>',[rfReplaceAll,rfIgnoreCase]);
        Text:=StringReplace(Text,'<PAR>','<SPAN CLASS="Parameter">',[rfReplaceAll,rfIgnoreCase]);
        Text:=StringReplace(Text,'</PAR>','</SPAN>',[rfReplaceAll,rfIgnoreCase]);
        Text:=StringReplace(Text,'<TAB>','&nbsp;&nbsp;&nbsp;',[rfReplaceAll,rfIgnoreCase]);
        Text:=StringReplace(Text,'<LINE>','<HR noshade size="1">',[rfReplaceAll,rfIgnoreCase]);
        Text:=StringReplace(Text,'</LINK>','</A>',[rfReplaceAll,rfIgnoreCase]);
        Text:=StringReplace(Text,'<CODE>','<SPAN CLASS="Code">',[rfReplaceAll,rfIgnoreCase]);
        Text:=StringReplace(Text,'</CODE>','</SPAN>',[rfReplaceAll,rfIgnoreCase]);
        repeat
          LinkPos:=Pos('<LINK ',Text);
          if LinkPos<>0 then
          begin
            Temp:=Copy(Text,LinkPos,100);
            EndPos:=Pos('>',Temp);
            Temp:=Copy(Text,LinkPos+6,EndPos-7);
            Delete(Text,LinkPos,EndPos);
            Insert('<A HREF="Topic'+FindTopicNumber(Temp)+'.html">',Text,LinkPos);
          end;
        until LinkPos=0;
        fFileList.Add(Text);
      end;
    end;
    if List[Dummy]='$StyleSheet' then
    begin
      InStyleSheet:=true;
      fFileList.Clear;
      StepBy(1);
      if List[Dummy]<>'---' then
      begin
        raise Exception.Create('Ung�ltige Datei');
      end;
    end
    else if StrLComp(PCHar(List[Dummy]),'$Topic',6)=0 then
    begin
      InTopic:=true;
      fFileList.Clear;
      fFileList.Add('<HTML><HEAD><LINK REL="stylesheet" TYPE="text/css" HREF="Help.CSS">');
      fFileList.Add('</HEAD><BODY>');
      fFileList.Add('<TABLE WIDTH=100%" BGCOLOR="#FFFFC0" CELLSPACING="0" CELLPADDING="0"><TR><TD CLASS="Header">'+Copy(fIndexList[Count+1],6,100));
      fFileList.Add('<BR></TD></TR></TABLE>');
      StepBy(3);
      if List[Dummy]<>'---' then
      begin
        raise Exception.Create('Ung�ltige Datei');
      end;
    end;
    StepBy(1);
  end;
  fFileList.Free;
  fIndexList.SaveToFile('help\index.dat');
  List.Free;
  Panel1.Visible:=false;
end;

procedure THelpForm.FormCreate(Sender: TObject);
begin
  HelpForm.Hide;
  fIndexList:=TStringList.Create;
end;

procedure THelpForm.FormDestroy(Sender: TObject);
begin
  fIndexList.Free;
end;

procedure THelpForm.TestBuildHelpFile;
var
  FindData: TSearchRec;
  Time    : TDateTime;
  TimeNew : TDateTime;
  Dummy   : Integer;
begin
  FindFirst('Help.dat',0,FindData);
  TimeNew:=FileDateToDateTime(FindData.Time);
  FindClose(FindData);
  // Pr�fen, ob neue Hilfedatei vorliegt
  if FindFirst('help\index.dat',0,FindData)=0 then
  begin
    fIndexList.LoadFromFile('help\index.dat');
    Time:=StrToDateTime(fIndexList[0]);
    FindClose(FindData);
    if TimeNew>Time then
      BuildHelpFile(TimeNew);
  end
  else
    BuildHelpFile(TimeNew);
  FindClose(FindData);
  WebBrowser1.Navigate(ExtractFilePath(Application.ExeName)+'help\topic0000.html');
  fIndexList.Delete(0);
  ListBox1.Items.Clear;
  ListBox1.Items.AddStrings(fIndexList);
end;

procedure THelpForm.Schliessen1Click(Sender: TObject);
begin
  Close;
end;

procedure THelpForm.WebBrowser1CommandStateChange(Sender: TObject;
  Command: Integer; Enable: WordBool);
begin
  case Command of
    CSC_NAVIGATEBACK: ActionBack.Enabled := Enable;
    CSC_NAVIGATEFORWARD: ActionForward.Enabled := Enable;
  end;
end;

procedure THelpForm.ActionBackExecute(Sender: TObject);
begin
  WebBrowser1.GoBack;
end;

procedure THelpForm.ActionForwardExecute(Sender: TObject);
begin
  WebBrowser1.GoForward;
end;

procedure THelpForm.ListBox1DblClick(Sender: TObject);
begin
  if ListBox1.ItemIndex<>-1 then
  begin
    WebBrowser1.Navigate(ExtractFilePath(Application.ExeName)+'help\topic'+Format('%.4d.html',[ListBox1.ItemIndex]));
  end;
end;

end.
