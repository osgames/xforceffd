object Form1: TForm1
  Left = 256
  Top = 276
  BorderStyle = bsDialog
  Caption = 'Language File Merger'
  ClientHeight = 132
  ClientWidth = 572
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 8
    Top = 16
    Width = 136
    Height = 13
    Caption = 'Eingabedatei (neue Version):'
  end
  object Label2: TLabel
    Left = 8
    Top = 40
    Width = 129
    Height = 13
    Caption = 'Eingabedatei (alte Version):'
  end
  object Label3: TLabel
    Left = 8
    Top = 64
    Width = 56
    Height = 13
    Caption = 'Mergedatei:'
  end
  object InputEdit1: TJvFilenameEdit
    Left = 161
    Top = 16
    Width = 401
    Height = 21
    ClipboardCommands = []
    Filter = 'Sprachdateien (*.dat)|*.dat|Alle Dateien (*.*)|*.*'
    InitialDir = 'c:\xforce\language'
    TabOrder = 0
    Text = 'C:\Programme\easyphp\www\local\gameset\development\deutsch.dat'
  end
  object InputEdit2: TJvFilenameEdit
    Left = 161
    Top = 40
    Width = 401
    Height = 21
    ClipboardCommands = []
    Filter = 'Sprachdateien (*.dat)|*.dat|Alle Dateien (*.*)|*.*'
    InitialDir = 'c:\xforce\language'
    DialogOptions = [ofHideReadOnly, ofPathMustExist, ofFileMustExist]
    TabOrder = 1
    Text = 'C:\Programme\easyphp\www\local\gameset\latest\deutsch.dat'
  end
  object MergeEdit1: TJvFilenameEdit
    Left = 161
    Top = 64
    Width = 401
    Height = 21
    ClipboardCommands = []
    Filter = 'Sprachdateien (*.dat)|*.dat|Alle Dateien (*.*)|*.*'
    InitialDir = 'C:\Programme\easyphp\www\local\game\latest'
    TabOrder = 2
    Text = 'C:\Programme\easyphp\www\local\gameset\development\english.dat'
  end
  object Button1: TButton
    Left = 8
    Top = 96
    Width = 153
    Height = 25
    Caption = 'Sprach-Dateien mischen'
    TabOrder = 3
    OnClick = Button1Click
  end
  object Button2: TButton
    Left = 168
    Top = 96
    Width = 153
    Height = 25
    Caption = 'Spielsatz-Dateien mischen'
    TabOrder = 4
    OnClick = Button2Click
  end
end
