unit frmMain;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Mask, JvExMask, JvToolEdit, string_utils;

type
  TForm1 = class(TForm)
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    InputEdit1: TJvFilenameEdit;
    InputEdit2: TJvFilenameEdit;
    MergeEdit1: TJvFilenameEdit;
    Button1: TButton;
    Button2: TButton;
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
  private
    { Private-Deklarationen }
  public
    { Public-Deklarationen }
  end;

var
  Form1: TForm1;

implementation

{$R *.DFM}

function LoadList(FileName: String): TStringList;
begin
  result:=TStringList.Create;
  result.LoadFromFile(FileName);
end;

procedure TForm1.Button1Click(Sender: TObject);
var
  Input1, Input2, Merge: TStringList;

  Dummy    : Integer;
  Name     : String;
  LastName : String;
  Beginn   : Cardinal;

  procedure AddAtEndOfList(List: TStringList; Text: String);
  var
    Name: String;
    Index: Integer;
  begin
    Name:=Copy(Text,1,Pos('=',Text)-1);
    if List.IndexOfName(Name)<>-1 then
    begin
      Index:=List.IndexOfName(Name);
      List[Index]:=Name+'=(new) '+Copy(Text,Pos('=',Text)+1,10000)+'(old) '+Copy(List[Index],Pos('=',Text)+1,10000);
    end
    else
    begin
      Text:=Name+'=(new) '+Copy(Text,Pos('=',Text)+1,10000);
      if List.IndexOfName(LastName)<>-1 then
        List.Insert(List.IndexOfName(LastName)+1,Text)
      else
        List.Add(Text);
    end;
  end;

begin
  Input1:=LoadList(InputEdit1.FileName);
  Input2:=LoadList(InputEdit2.FileName);
  Merge:=LoadList(MergeEdit1.FileName);

  Beginn:=GetTickCount;

  for Dummy:=0 to Input1.Count-1 do
  begin
    Name:=Input1.Names[Dummy];
    if Name<>'' then
    begin
      if Input2.IndexOfName(Name)=-1 then
      begin
        // Neuer Text zum �bersetzen
        if Merge.IndexOfName(Name)=-1 then
        begin
          AddAtEndOfList(Merge,Input1[Dummy]);
        end;
      end
      else if Input1.Values[Name]<>Input2.Values[Name] then
      begin
        // Text wurde ge�ndert
        AddAtEndOfList(Merge,Input1[Dummy]);
      end;
      LastName:=Name;
    end;
  end;

  Merge.SaveToFile(MergeEdit1.FileName);
  Input1.Free;
  Input2.Free;
  Merge.Free;

  ShowMessage('Fertig: '+IntToStr(GetTickCount-Beginn));
end;

procedure TForm1.Button2Click(Sender: TObject);
var
  Input1, Input2, Merge: TStringList;
  Beginn   : Cardinal;
  Name     : String;
  Dummy    : Integer;
  Dummy2   : Integer;

  Typ1,Field1,Name1,Text1 : String;
  ID1      : Cardinal;

  Typ2,Field2,Name2,Text2 : String;
  ID2      : Cardinal;

  procedure GetInfosOfTextBlock(List: TStringList; var StartIndex: Integer; out Typ: String; out ID: Cardinal; out Field: String; out Name: String; out Text: String);
  var
    Line: String;
    Arr : TStringArray;
  begin
    Line:=Copy(List[StartIndex],2,length(List[StartIndex])-2);
    Arr:=string_utils_explode('/',Line);
    Typ:=Arr[0];
    ID:=StrToInt64(Arr[1]);
    if length(Arr)>2 then
      Field:=Arr[2]
    else
      Field:='Info';

    inc(StartIndex);
    Line:=List[StartIndex];
    Name:='';

    if (length(Line)>0) and (Line[1]='$') then
    begin
      Name:=Copy(Line,3,length(Line));
      inc(StartIndex);
    end;

    inc(StartIndex);
    Text:='';
    while List[StartIndex]<>'-----------------------------------------------------------' do
    begin
      Text:=Text+List[StartIndex]+#13#10;
      inc(StartIndex);
    end;
    Delete(Text,length(Text)-1,2);
  end;

  function GetIndexOfObject(List: TStringList; Typ: String; ID: Cardinal; Field: String): Integer;
  var
    Dummy  : Integer;
    Line   : String;
    Arr    : TStringArray;
    CTyp   : String;
    CID    : Cardinal;
    CField : String;
  begin
    result:=-1;
    for Dummy:=0 to List.Count-1 do
    begin
      Line:=List[Dummy];
      if (length(Line)>0) and (Line[1]='[') then
      begin
        Arr:=string_utils_explode('/',Copy(Line,2,length(Line)-2));
        CTyp:=Arr[0];
        CID:=StrToInt64(Arr[1]);
        if length(Arr)>2 then
          CField:=Arr[2]
        else
          CField:='Info';

        if (CTyp=Typ) and (CID=ID) and (CField=Field) then
        begin
          result:=Dummy;
          exit;
        end;
      end;
    end;
  end;

  procedure Merging(Typ: String; ID: Cardinal; Field: String; Name1,Name2: String; Text1,Text2: String);
  var
    Index: Integer;
    IndexSave : Integer;
    Arr  : TStringArray;
    OldTyp, OldField, OldName, OldText: String;
    OldID : Cardinal;
    Line  : String;
  begin
    Index:=GetIndexOfObject(Merge,Typ,ID,Field);
    if Index=-1 then
    begin
      if (Name1='') and (Text1='') then
        exit;
      // Neuen Text hinzuf�gen
      Merge.Add(Format('[%s/%s/%s]',[Typ,IntToStr(ID),Field]));
      if Field='Info' then
        Merge.Add('$ (new) '+Name1);

      Merge.Add('-----------------------------------------------------------');
      Text1:='(new) '+StringReplace(Text1,#13#10,'|',[rfReplaceAll]);
      Arr:=string_utils_explode('|',Text1);
      string_utils_AddToTStrings(Arr,Merge);
      Merge.Add('-----------------------------------------------------------');
      Merge.Add('');
    end
    else
    begin
      IndexSave:=Index;
      GetInfosOfTextBlock(Merge,Index,OldTyp,OldID,OldField,OldName,OldText);

      inc(IndexSave);

      Line:=Merge[IndexSave];
      if (length(Line)>0) and (Line[1]='$') then
      begin
        if Name1<>Name2 then
          Merge[IndexSave]:='$ (new) '+Name1+' (old) '+OldName;
        inc(IndexSave);
      end;
      inc(IndexSave);

      if Text1<>Text2 then
      begin
        while Merge[IndexSave]<>'-----------------------------------------------------------' do
          Merge.delete(IndexSave);

        Text1:='(new) '+StringReplace(Text1,#13#10,'|',[rfReplaceAll]) + #13#10 + '(old) '+StringReplace(OldText,#13#10,'|',[rfReplaceAll]);
        Arr:=string_utils_explode('|',Text1);
        Index:=0;
        for Index:=high(Arr) downto 0 do
        begin
          Merge.Insert(IndexSave,Arr[Index]);
        end;
      end;
    end;
  end;

begin
  Input1:=LoadList(InputEdit1.FileName);
  Input2:=LoadList(InputEdit2.FileName);
  Merge:=LoadList(MergeEdit1.FileName);

  Beginn:=GetTickCount;

  Dummy:=0;
  while (Dummy<Input1.Count) do
  begin
    Name:=Input1[Dummy];
    if (length(Name)>0) and (Name[1]='[') then
    begin
      GetInfosOfTextBlock(Input1,Dummy,Typ1,ID1,Field1,Name1,Text1);

      Dummy2:=GetIndexOfObject(Input2,Typ1,ID1,Field1);
      if Dummy2<>-1 then
      begin
        GetInfosOfTextBlock(Input2,Dummy2,Typ2,ID2,Field2,Name2,Text2);
      end
      else
      begin
        ID2:=0;
        Name2:='';
        Text2:='';
      end;

      if (ID1<>ID2) or (Name1<>Name2) or (Text1<>Text2) then
      begin
        Merging(Typ1,ID1,Field1,Name1,Name2,Text1,Text2);
      end;
    end;
    inc(Dummy);
  end;

  Merge.SaveToFile(MergeEdit1.FileName);

  Input1.Free;
  Input2.Free;
  Merge.Free;

  ShowMessage('Fertig: '+IntToStr(GetTickCount-Beginn));
end;

end.
