Das Module Tools enth�lt die Sourcecodes f�r die Tools und Zusatzprogramme zu X-Force.

Um alle Tools kompilieren zu k�nnen werden folgende Komponenten ben�tigt.

Toolbar 2000 (http://www.jrsoftware.org/tb2k.php)
JVCL: JEDI Visual Component Library (http://prdownloads.sourceforge.net/jvcl/JCL%2BJVCL210FullInstall.zip?download)
Pascal Script (http://www.remobjects.com/download.asp?id={23BEE1E9-0933-4C1A-BF69-C98EAEF5296F}&nodownloadinfo=now)

WICHTIG: Das Modul tools muss im gleichen Verzeichnis ausgecheckt sein wie das Modul game.

Das Package \tools\components\xforcetoolscomps_delphix muss installiert werden.

Um den Missionseditor (scriptedit) zu kompilieren muss mindestens Delphi 6 benutzt 
werden. Mit Delphi 5 kann der Missionseditor leider nicht kompiliert werden.

Bitte pr�ft auch immer das Ausgabeverzeichnis unter den Projektoptionen.