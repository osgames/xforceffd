unit CompMain;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, ComCtrls, ExtCtrls, XANBitmap, Mask, JvExMask, JvSpin, Registry;

type
  TForm1 = class(TForm)
    ProgressBar1: TProgressBar;
    Label1: TLabel;
    StartButton: TButton;
    OpenXANFile: TOpenDialog;
    ScrollBox1: TScrollBox;
    Image1: TImage;
    SaveButton: TButton;
    JvSpinEdit1: TJvSpinEdit;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Edit1: TEdit;
    Button1: TButton;
    OpenDialog1: TOpenDialog;
    procedure FormShow(Sender: TObject);
    procedure StartButtonClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SaveButtonClick(Sender: TObject);
    procedure JvSpinEdit1Change(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
  private
    fAfterCompileEnd: Boolean;
    fFileName       : String;
    procedure GenerateFinalBitmap(maxCols: Integer);
    { Private-Deklarationen }
  public
    procedure Compile(FileName: String);
    { Public-Deklarationen }
  end;

var
  Form1: TForm1;

implementation

uses
  inifiles, shellapi, KD4Utils, XForce_Types;

var
  ImageOrder  : Array of Integer;
  ImageInfos  : Array of TXANImageInfo;
  Images      : Array of TBitmap;

{$R *.DFM}

function CompareImages(Index1,Index2: Integer;Typ: TFunctionType): Integer;
var
  tmp: Integer;
begin
  case Typ of
    ftCompare: result:=Images[ImageOrder[Index1]].Height-Images[ImageOrder[Index2]].Height;
    ftExchange:
    begin
      tmp:=ImageOrder[Index1];
      ImageOrder[Index1]:=ImageOrder[Index2];
      ImageOrder[Index2]:=tmp;
    end;
  end;
end;

procedure TForm1.GenerateFinalBitmap(maxCols: Integer);
var
  Dummy: Integer;
  Image: Integer;
  Cols : Integer;
  XPos : Integer;
  YPos : Integer;
  result  : TBitmap;
begin
  result:=TBitmap.Create;
  Cols:=0;

  XPos:=0;
  YPos:=0;
  Cols:=maxCols;
  for Dummy:=0 to high(ImageOrder) do
  begin
    Image:=ImageOrder[Dummy];
    dec(Cols);
    if (Cols<=-1) and (result.Width<Images[Image].Width+XPos) then
    begin
      XPos:=0;
      YPos:=result.Height;
    end;

    if result.Width<Images[Image].Width+XPos then
      result.Width:=Images[Image].Width+XPos;

    if result.Height<Images[Image].Height+YPos then
      result.Height:=Images[Image].Height+YPos;

    result.Canvas.Draw(XPos,YPos,Images[Image]);

    ImageInfos[Image].Rect:=Rect(XPos,YPos,XPos+Images[Image].Width,YPos+Images[Image].Height);

    inc(XPos,Images[Image].Width);

  end;
  Image1.Picture.Bitmap:=result;

  ScrollBox1.HorzScrollBar.Position:=Image1.Width;
  ScrollBox1.VertScrollBar.Position:=Image1.Height;

  jvSpinEdit1.Value:=maxCols;
  jvSpinEdit1.Enabled:=true;
  Label3.Caption:=Format('%d x %d = %.0n',[Image1.Width,Image1.Height,Image1.Width*Image1.Height/1]);
  result.Free;
end;

procedure TForm1.Compile(FileName: String);
var
  Ini    : TIniFile;
  Layers : Integer;
  Lists  : Array of TStringList;
  Dummy  : Integer;
  Bitmap : TBitmap;

  function GenerateBitmap(Index: Integer): TBitmap;
  var
    Dummy : Integer;
    tmp   : TBitmap;
  begin
    tmp:=nil;
    result:=nil;
    for Dummy:=0 to Layers-1 do
    begin
      if result=nil then
      begin
        result:=TBitmap.Create;
        result.LoadFromFile(Lists[Dummy].Values['File'+IntToStr(Index+1)]);
      end
      else
      begin
        if tmp=nil then
          tmp:=TBitmap.Create;

        tmp.LoadFromFile(Lists[Dummy].Values['File'+IntToStr(Index+1)]);
        Assert((tmp.Width=result.Width) and (tmp.Height=result.Height),'Alle Bilder eines Layers m�ssen die gleiche gr��e haben');

        tmp.TransparentColor:=clBlack;
        tmp.Transparent:=true;

        Result.Canvas.Draw(0,0,tmp);
      end;
    end;
    FreeAndNil(tmp);
  end;

  procedure OptimizeImage(Bitmap: TBitmap; var OffSetX,OffsetY: Integer);
  var
    tmp: TBitmap;
    X: Integer;
    Y: Integer;

    BestXBeginn: Integer;
    BestXEnd   : Integer;

    BestYBeginn: Integer;
    BestYEnd   : Integer;
    SrcRect    : TRect;

    function CheckRow(X: Integer): Boolean;
    var
      Y: Integer;
    begin
      result:=false;
      for Y:=0 to tmp.Height-1 do
      begin
        if tmp.Canvas.Pixels[X,Y]<>clBlack then
        begin
          result:=true;
          exit;
        end;
      end;
    end;

    function CheckCol(Y: Integer): Boolean;
    var
      X: Integer;
    begin
      result:=false;
      for X:=0 to tmp.Width-1 do
      begin
        if tmp.Canvas.Pixels[X,Y]<>clBlack then
        begin
          result:=true;
          exit;
        end;
      end;
    end;

  begin
    tmp:=TBitmap.Create;
    tmp.Width:=Bitmap.Width;
    tmp.Height:=Bitmap.Height;
    tmp.Canvas.Draw(0,0,Bitmap);

    BestXBeginn:=-1;

    X:=0;
    while (BestXBeginn=-1) and (X<tmp.Width) do
    begin
      if CheckRow(X) then
        BestXBeginn:=X;

      inc(X);
    end;

    // Nichts im Bild drin
    if BestXBeginn=-1 then
    begin
      Bitmap.Width:=0;
      Bitmap.Height:=0;
    end
    else
    begin
      X:=tmp.Width-1;
      BestXEnd:=-1;

      while (BestXEnd=-1) do
      begin
        if CheckRow(X) then
          BestXEnd:=X;

        dec(X);
      end;

      Y:=0;
      BestYBeginn:=-1;
      while (BestYBeginn=-1) do
      begin
        if CheckCol(Y) then
          BestYBeginn:=Y;

        inc(Y);
      end;

      Y:=tmp.Height-1;
      BestYEnd:=-1;

      while (BestYEnd=-1) do
      begin
        if CheckCol(Y) then
          BestYEnd:=Y;

        dec(Y);
      end;

      Bitmap.Width:=BestXEnd-BestXBeginn;
      Bitmap.Height:=BestYEnd-BestYBeginn;

      OffSetX:=-BestXBeginn;
      OffSetY:=-BestYBeginn;

      SrcRect:=Rect(BestXBeginn,BestYBeginn,BestXEnd,BestYEnd);

      Bitmap.Canvas.CopyRect(Rect(0,0,Bitmap.Width,Bitmap.Height),tmp.Canvas,SrcRect);
    end;
    tmp.Free;
  end;

begin
  StartButton.Enabled:=false;
  Ini:=TIniFile.Create(FileName);

  fFileName:=FileName;
  Layers:=Ini.ReadInteger('Settings','Layers',-1);

  Assert(Layers<>-1,'Layers-Eigenschaft nicht gefunden');

  SetLength(Lists,Layers);

  for Dummy:=0 to high(Lists) do
  begin
    Lists[Dummy]:=TStringList.Create;
    Ini.ReadSectionValues('Layer'+IntToStr(Dummy+1),Lists[Dummy]);
  end;

  SetLength(Images,Lists[0].Count);
  SetLength(ImageInfos,Lists[0].Count);

  ProgressBar1.Max:=length(Images);
  ProgressBar1.Position:=0;

  for Dummy:=0 to high(Images) do
  begin
    Images[Dummy]:=GenerateBitmap(Dummy);
    ProgressBar1.StepIt;
    ProgressBar1.Refresh;
    OptimizeImage(Images[Dummy],ImageInfos[Dummy].OffSet.X,ImageInfos[Dummy].OffSet.Y);
  end;

  SetLength(ImageOrder,length(ImageInfos));

  for Dummy:=0 to high(ImageOrder) do
    ImageOrder[Dummy]:=Dummy;

  QuickSort(0,high(ImageOrder),CompareImages);

  GenerateFinalBitmap(Ini.ReadInteger('Settings','Cols',10));

  // Listen freigeben
  for Dummy:=0 to high(Lists) do
    Lists[Dummy].Free;

  SaveButton.Enabled:=true;

  Ini.Free;
end;

procedure TForm1.FormShow(Sender: TObject);
begin
  if ParamCount>0 then
  begin
    fAfterCompileEnd:=true;
    Compile(ParamStr(1));
  end;
end;

procedure TForm1.StartButtonClick(Sender: TObject);
begin
  if OpenXANFile.Execute then
    Compile(OpenXANFile.FileName);
end;

procedure TForm1.FormCreate(Sender: TObject);
var
  Reg: TRegistry;
begin
  fAfterCompileEnd:=false;

  Reg:=TRegistry.Create;
  if Reg.OpenKey('Software\Rich Entertainment\XANTools',false) then
  begin
    try
      Edit1.Text:=Reg.ReadString('IrfanView');
    except
    end;
  end;
  Reg.Free;
end;

procedure TForm1.SaveButtonClick(Sender: TObject);
var
  Dir    : Array[0..200] of Char;
  tmpFile: String;
  Bitmap : TBitmap;
  Dummy  : Integer;
  
  procedure CorrectPalette(Bitmap: TBitmap);
  var
    PalEntrys: record
      case Integer of
        0: (
          Pal: TLogPalette);
        1: (
          palVersion: Word;
          palNumEntries: Word;
          palPalEntry: Array[0..255] of TPaletteEntry);
    end;
    Dummy: Integer;
  begin
    Assert(Bitmap.PixelFormat = pf8bit);
    Bitmap.IgnorePalette:=false;
    if Windows.GetPaletteEntries(Bitmap.Palette,0,255,PalEntrys.palPalEntry)=0 then
      RaiseLastWin32Error;

    for Dummy:=0 to 255 do
    begin
      if (PalEntrys.palPalEntry[Dummy].peRed=4) and (PalEntrys.palPalEntry[Dummy].peGreen=2) and (PalEntrys.palPalEntry[Dummy].peBlue=4) then
      begin
        PalEntrys.palPalEntry[Dummy].peRed:=0;
        PalEntrys.palPalEntry[Dummy].peGreen:=0;
        PalEntrys.palPalEntry[Dummy].peBlue:=0;
      end
      else if (PalEntrys.palPalEntry[Dummy].peRed=4) and (PalEntrys.palPalEntry[Dummy].peGreen=2) and (PalEntrys.palPalEntry[Dummy].peBlue>10) then
      begin
        PalEntrys.palPalEntry[Dummy].peRed:=0;
        PalEntrys.palPalEntry[Dummy].peGreen:=0;
      end;
    end;
    palEntrys.palVersion:=$300;
    palEntrys.palNumEntries:=255;
    Bitmap.Palette:=CreatePalette(PalEntrys.pal);
  end;

  procedure SaveXANFile;
  var
    fs     : TFileStream;
    Buffer : Cardinal;
  begin
    fs:=TFileStream.Create(ChangeFileExt(ExtractFileName(fFileName),'.xan'),fmCreate);
    // Header schreiben
    Buffer:=XANHeader;
    fs.Write(Buffer,SizeOf(Buffer));

    // Anzahl der Frames speichern
    Buffer:=length(ImageInfos);
    fs.Write(Buffer,SizeOf(Buffer));

    // Bildinformationen speichern
    fs.Write(ImageInfos[0],SizeOf(ImageInfos[0])*length(ImageInfos));

    Bitmap.SaveToStream(fs);

    fs.Free;
  end;
begin
  Bitmap:=Image1.Picture.Bitmap;
  GetTempPath(200,Dir);
  tmpFile:=IncludeTrailingBackslash(Dir)+'temp.bmp';
  Bitmap.SaveToFile(tmpFile);

  // Farbtiefe mit IrfanView reduzieren
  Assert(ShellExecute(Handle,'open',PCHar(Edit1.Text),PChar(tmpFile+' /bpp=8 /convert=temp.bmp'),nil,SW_MINIMIZE)>32, 'IrfanView konnte nicht gestartet werden');

  while Bitmap.PixelFormat<>pf8bit do
  begin
    try
      Bitmap.IgnorePalette:=false;
      Bitmap.LoadFromFile(tmpFile);
    except
    end;
    sleep(100);
  end;

  // Palette anpassen
  CorrectPalette(Bitmap);

  // Speichert alles in das XANFile
  SaveXANFile;

  for Dummy:=0 to high(Images) do
    Images[Dummy].Free;

  if fAfterCompileEnd then
    Close
  else
  begin
    StartButton.Enabled:=true;
    SaveButton.Enabled:=false;
  end;
end;

procedure TForm1.JvSpinEdit1Change(Sender: TObject);
begin
  GenerateFinalBitmap(round(JvSpinEdit1.Value));
end;

procedure TForm1.Button1Click(Sender: TObject);
begin
  OpenDialog1.InitialDir:=ExtractFilePath(Edit1.Text);
  if OpenDialog1.Execute then
    Edit1.Text:=OpenDialog1.FileName;
end;

procedure TForm1.FormDestroy(Sender: TObject);
var
  Reg: TRegistry;
begin
  fAfterCompileEnd:=false;

  Reg:=TRegistry.Create;
  Assert(Reg.OpenKey('Software\Rich Entertainment\XANTools',true));
  Reg.WriteString('IrfanView',Edit1.Text);
  Reg.Free;
end;

end.
