unit ViewMain;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  XANBitmap, StdCtrls, Mask, JvExMask, JvSpin, DXDraws, DXClass, ExtCtrls;

type
  TForm1 = class(TForm)
    Button1: TButton;
    FrameEdit: TJvSpinEdit;
    OpenXANFile: TOpenDialog;
    DXTimer1: TDXTimer;
    XANView: TDXDraw;
    Button2: TButton;
    AniTimer: TTimer;
    Label1: TLabel;
    Label2: TLabel;
    IntervalEdit: TJvSpinEdit;
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure DXTimer1Timer(Sender: TObject; LagCount: Integer);
    procedure Button2Click(Sender: TObject);
    procedure AniTimerTimer(Sender: TObject);
    procedure IntervalEditChange(Sender: TObject);
  private
    { Private-Deklarationen }
  public
    fXANBitmap: TXANBitmap;
    { Public-Deklarationen }
  end;

var
  Form1: TForm1;

implementation

{$R *.DFM}

procedure TForm1.FormCreate(Sender: TObject);
begin
  fXANBitmap:=TXANBitmap.Create(XANView);
end;

procedure TForm1.FormDestroy(Sender: TObject);
begin
  fXANBitmap.Free;
end;

procedure TForm1.Button1Click(Sender: TObject);
begin
  if OpenXANFile.Execute then
  begin
    fXANBitmap.LoadFromFile(OpenXANFile.FileName);
    FrameEdit.MaxValue:=fXANBitmap.Frames-1;
    FrameEdit.Enabled:=true;
  end;
end;

procedure TForm1.DXTimer1Timer(Sender: TObject; LagCount: Integer);
begin
  XANView.Surface.Fill(0);
  fXANBitmap.DrawImage(XANView.Surface,0,0,round(FrameEdit.Value));
  XANView.Flip;
end;

procedure TForm1.Button2Click(Sender: TObject);
begin
  AniTimer.Enabled:=not AniTimer.Enabled;
end;

procedure TForm1.AniTimerTimer(Sender: TObject);
var
  Value: Integer;
begin
  if fXANBitmap.Frames=0 then
    exit;
  Value:=round(FrameEdit.Value);
  Value:=(Value+1) mod fXANBitmap.Frames;
  FrameEdit.Value:=Value
end;

procedure TForm1.IntervalEditChange(Sender: TObject);
begin
  AniTimer.Interval:=round(IntervalEdit.Value);
end;

end.
