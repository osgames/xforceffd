unit frmMain;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, clipbrd;

type
  TForm1 = class(TForm)
    Label1: TLabel;
    Edit1: TEdit;
    Button1: TButton;
    Edit2: TEdit;
    Label2: TLabel;
    Edit3: TEdit;
    StringText: TLabel;
    Editdeutschdat: TButton;
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure Edit1Change(Sender: TObject);
    procedure EditdeutschdatClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    List: TStringList;
    FConfigurationSettings: TStringList;
    function ExpandEnvironmentStringsFunc(Value: string): string;
  public
  end;

var
  Form1: TForm1;

implementation

uses
  shellapi, IniFiles;

{$R *.DFM}

function GetMarkIndex(List: TStringList): Integer;
var
  Dummy: Integer;
begin
  result := -1;
  for Dummy := List.Count - 1 downto 0 do
    if Copy(List[Dummy], 1, 7) = '{MARKE:' then
    begin
      result := Dummy;
      exit;
    end;
end;

procedure AddToList(List: TStringList; Text: string);
var
  Ind: Integer;
begin
  Ind := GetMarkIndex(List);
  Assert(Ind <> -1);
  List.Insert(Ind, Text);
end;

procedure AddToFile(FileName: string; Text: string);
var
  list: TStringList;
begin
  List := TStringList.Create;
  List.LoadFromFile(FileName);
  AddToList(List, Text);
  List.SaveToFile(FileName);
  List.Free;
end;

function FoundText(List: TStringList; Text: string): Integer;
var
  Dummy: Integer;
begin
  result := -1;
  for Dummy := 0 to List.Count - 1 do
  begin
    if Copy(List[Dummy], Length(List.Names[Dummy]) + 2, 10000) = Text then
    begin
      result := Dummy;
      exit;
    end;
  end;
end;

procedure TForm1.Button1Click(Sender: TObject);
var
  Ind1, Ind2: Integer;
  Vor1, Vor2: string;
  Last: Integer;
  New: string;
begin
  Ind1 := FoundText(List, Edit1.Text);
  if Ind1 <> -1 then
  begin
    Edit2.Text := List.Names[Ind1];
    Clipboard.AsText := List.Names[Ind1];

    exit;
  end;
  Ind1 := GetMarkIndex(List);
  Assert(Ind1 <> -1);

  Vor2 := FormatDateTime('yymmdd', Now);
  Vor1 := FConfigurationSettings.Values['Prefix'] + Vor2;
  Ind2 := Ind1 - 1;
  while (Copy(List[Ind2], 3, 6) = Vor2) and (Copy(List[Ind2], 1, 8) <> Vor1) do dec(Ind2);
  if (Copy(List[Ind2], 1, 8) = Vor1) then
    Last := StrToInt(Copy(List[Ind2], 9, 4)) + 1
  else
    Last := 1;
  New := Format('%s%.4d', [Vor1, Last]);
  Edit2.Text := New;

  AddToList(List, New + '=' + Edit1.Text);

  List.SaveToFile(FConfigurationSettings.Values['LanguageFile_DEVEL']);
  List.SaveToFile(FConfigurationSettings.Values['LanguageFile_BIN']);

  AddToFile(FConfigurationSettings.Values['ReadLanguageFile'], Format('  %s:=GetValue(''%:1s'');', [New]));
  AddToFile(FConfigurationSettings.Values['StringConstFile'], Format('  %s    : string;', [New]));

  Clipboard.AsText := New;
end;

procedure TForm1.Button2Click(Sender: TObject);
begin
  StringText.Caption := List.Values[Edit3.Text];
end;

procedure TForm1.FormCreate(Sender: TObject);
var
  i: Integer;
begin
  FConfigurationSettings := TStringList.Create;
  with TIniFile.Create(ExtractFilePath(ParamStr(0)) + 'NewStrings.ini') do try
    ReadSectionValues('Configuration', FConfigurationSettings);
  finally
    Free;
  end;

  if FConfigurationSettings.Count = 0 then
    with TIniFile.Create(ExtractFilePath(ParamStr(0)) + 'NewStrings.default') do try
      ReadSectionValues('Configuration', FConfigurationSettings);
    finally
      Free;
    end;

  for i := 0 to FConfigurationSettings.Count - 1 do begin
    FConfigurationSettings[i] := ExpandEnvironmentStringsFunc(FConfigurationSettings[i]);
  end;

  List := TStringList.Create;
  List.LoadFromFile(FConfigurationSettings.Values['LanguageFile_DEVEL']);
  FConfigurationSettings.Values['Prefix'] := Copy(FConfigurationSettings.Values['Prefix'], 1, 2);
end;

procedure TForm1.Edit1Change(Sender: TObject);
var
  Index: Integer;
begin
  Index := FoundText(List, Edit1.Text);

  if Index <> -1 then
    Edit2.Text := List.Names[Index]
  else
    Edit2.Text := '';
end;

procedure TForm1.EditdeutschdatClick(Sender: TObject);
begin
  ShellExecute(0, 'open', pchar(FConfigurationSettings.Values['Editor']), pchar(FConfigurationSettings.Values['LanguageFile_DEVEL']), nil, SW_NORMAL);
end;

procedure TForm1.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  FConfigurationSettings.Free;
end;

function TForm1.ExpandEnvironmentStringsFunc(Value: string): string;
var
  Dest: array[0..4096] of char;
begin
  ExpandEnvironmentStrings(pchar(Value), Dest, Sizeof(Dest));
  Result := Dest;
end;

end.

