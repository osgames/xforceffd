object Form1: TForm1
  Left = 198
  Top = 153
  BorderStyle = bsDialog
  Caption = 'Neuen String anlegen:'
  ClientHeight = 147
  ClientWidth = 562
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnClose = FormClose
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 8
    Top = 10
    Width = 24
    Height = 13
    Caption = 'Text:'
  end
  object Label2: TLabel
    Left = 8
    Top = 80
    Width = 30
    Height = 13
    Caption = 'String:'
  end
  object StringText: TLabel
    Left = 8
    Top = 112
    Width = 48
    Height = 13
    Caption = 'StringText'
  end
  object Edit1: TEdit
    Left = 48
    Top = 6
    Width = 505
    Height = 21
    TabOrder = 0
    OnChange = Edit1Change
  end
  object Button1: TButton
    Left = 48
    Top = 32
    Width = 97
    Height = 25
    Caption = 'String erstellen'
    TabOrder = 1
    OnClick = Button1Click
  end
  object Edit2: TEdit
    Left = 432
    Top = 32
    Width = 121
    Height = 21
    ReadOnly = True
    TabOrder = 2
    Text = 'Edit2'
  end
  object Edit3: TEdit
    Left = 48
    Top = 75
    Width = 121
    Height = 21
    TabOrder = 3
    OnChange = Button2Click
  end
  object Editdeutschdat: TButton
    Left = 152
    Top = 32
    Width = 97
    Height = 25
    Caption = 'bearbeiten'
    TabOrder = 4
    OnClick = EditdeutschdatClick
  end
end
