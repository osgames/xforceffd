unit frmSettings;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, TileGroup, xforce_types, Defines;

type
  TTileSettings = class(TForm)
    BitBtn1: TBitBtn;
    PaintBox1: TPaintBox;
    Label1: TLabel;
    Label2: TLabel;
    PaintBox2: TPaintBox;
    Label3: TLabel;
    PaintBox3: TPaintBox;
    Label4: TLabel;
    PaintBox4: TPaintBox;
    Button1: TButton;
    Button2: TButton;
    Button3: TButton;
    BitBtn2: TBitBtn;
    Label5: TLabel;
    PaintBox5: TPaintBox;
    Button4: TButton;
    Button5: TButton;
    OpenBitmapDialog: TOpenDialog;
    Image1: TImage;
    Image2: TImage;
    Image3: TImage;
    Image4: TImage;
    GroupBox1: TGroupBox;
    BegehbarCheckBox: TCheckBox;
    procedure PaintBox1Paint(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure PaintBox2Paint(Sender: TObject);
    procedure PaintBox3Paint(Sender: TObject);
    procedure PaintBox4Paint(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure Button3Click(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure Button4Click(Sender: TObject);
    procedure PaintBox5Paint(Sender: TObject);
    procedure Button5Click(Sender: TObject);
    procedure BegehbarCheckBoxClick(Sender: TObject);
  private
    fTileInformation: TTileInformation;
    { Private-Deklarationen }
  public
    { Public-Deklarationen }
  end;

var
  TileSettings: TTileSettings;

implementation

uses frmMain, frmChooseTile;

{$R *.DFM}

procedure TTileSettings.PaintBox1Paint(Sender: TObject);
begin
  MainForm.TileGroup1.DrawFloorCanvas(PaintBox1.Canvas,0,0,MainForm.TileListBox.ItemIndex);
end;

procedure TTileSettings.FormShow(Sender: TObject);
begin
  fTileInformation:=MainForm.TileGroup1.TileInformation(MainForm.TileListBox.ItemIndex)^;

  BegehbarCheckBox.Checked:=fTileInformation.Begehbar;
end;

procedure TTileSettings.PaintBox2Paint(Sender: TObject);
begin
  MainForm.TileGroup1.DrawFloorCanvas(PaintBox2.Canvas,0,0,fTileInformation.LeftRotate);
end;

procedure TTileSettings.PaintBox3Paint(Sender: TObject);
begin
  MainForm.TileGroup1.DrawFloorCanvas(PaintBox3.Canvas,0,0,fTileInformation.HMirror);
end;

procedure TTileSettings.PaintBox4Paint(Sender: TObject);
begin
  MainForm.TileGroup1.DrawFloorCanvas(PaintBox4.Canvas,0,0,fTileInformation.RightRotate);
end;

procedure TTileSettings.Button1Click(Sender: TObject);
begin
  if ChooseTile.ShowModal=mrOK then
  begin
    fTileInformation.LeftRotate:=ChooseTile.ListBox1.ItemIndex;
    PaintBox2.Refresh;
  end;
end;

procedure TTileSettings.Button2Click(Sender: TObject);
begin
  if ChooseTile.ShowModal=mrOK then
  begin
    fTileInformation.HMirror:=ChooseTile.ListBox1.ItemIndex;
    PaintBox3.Refresh;
  end;
end;

procedure TTileSettings.Button3Click(Sender: TObject);
begin
  if ChooseTile.ShowModal=mrOK then
  begin
    fTileInformation.RightRotate:=ChooseTile.ListBox1.ItemIndex;
    PaintBox4.Refresh;
  end;
end;

procedure TTileSettings.BitBtn1Click(Sender: TObject);
var
  Index: Integer;
begin
  Index:=MainForm.TileListBox.ItemIndex;
  MainForm.TileGroup1.TileInformation(Index)^:=fTileInformation;
  with fTileInformation do
  begin
    if LeftRotate<>Index then
      MainForm.TileGroup1.TileInformation(LeftRotate).RightRotate:=Index;

    if HMirror<>Index then
      MainForm.TileGroup1.TileInformation(HMirror).HMirror:=Index;

    if VMirror<>Index then
      MainForm.TileGroup1.TileInformation(VMirror).VMirror:=Index;

    if RightRotate<>Index then
      MainForm.TileGroup1.TileInformation(RightRotate).LeftRotate:=Index;
  end;
  MainFOrm.TileGroup1.Modify:=true;
end;

procedure TTileSettings.Button4Click(Sender: TObject);
begin
  if ChooseTile.ShowModal=mrOK then
  begin
    fTileInformation.VMirror:=ChooseTile.ListBox1.ItemIndex;
    PaintBox5.Refresh;
  end;
end;

procedure TTileSettings.PaintBox5Paint(Sender: TObject);
begin
  MainForm.TileGroup1.DrawFloorCanvas(PaintBox5.Canvas,0,0,fTileInformation.VMirror);
end;

procedure TTileSettings.Button5Click(Sender: TObject);
var
  Bitmap: TBitmap;
begin
  if OpenBitmapDialog.Execute then
  begin
    Bitmap:=TBitmap.Create;
    Bitmap.LoadFromFile(OpenBitmapDialog.FileName);

    MainForm.MaskTile(Bitmap);
    MainForm.TileGroup1.ReplaceFloor(Bitmap,MainForm.TileListBox.ItemIndex);

    MainForm.Refresh;
    Refresh;
    Bitmap.Free;
  end;

end;

procedure TTileSettings.BegehbarCheckBoxClick(Sender: TObject);
begin
  fTileInformation.Begehbar:=BegehbarCheckBox.Checked
end;

end.
