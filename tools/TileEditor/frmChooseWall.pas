unit frmChooseWall;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, ExtCtrls, Defines;

type
  TChooseWall = class(TForm)
    WallListBox: TListBox;
    Panel1: TPanel;
    OKButton: TBitBtn;
    BitBtn2: TBitBtn;
    NoImageButton: TBitBtn;
    procedure WallListBoxDrawItem(Control: TWinControl; Index: Integer;
      Rect: TRect; State: TOwnerDrawState);
    procedure FormShow(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure NoImageButtonClick(Sender: TObject);
    procedure WallListBoxDblClick(Sender: TObject);
  private
    { Private-Deklarationen }
  public
    { Public-Deklarationen }
  end;

var
  ChooseWall: TChooseWall;

implementation

uses frmMain;

{$R *.DFM}

procedure TChooseWall.WallListBoxDrawItem(Control: TWinControl;
  Index: Integer; Rect: TRect; State: TOwnerDrawState);
begin
  WallListBox.Canvas.FillRect(Rect);
  MainForm.TileGroup1.DrawWallLeftCanvas(WallListBox.Canvas,Rect.Left+4,Rect.Top+4,Index);
end;

procedure TChooseWall.FormShow(Sender: TObject);
var
  Dummy: Integer;
begin
  WallListBox.ItemHeight:=WallHeight+8;
  WallListBox.Items.Clear;
  for Dummy:=0 to MainForm.TileGroup1.WallLeftCount-1 do
  begin
    WallListBox.Items.Add('');
  end;
end;

procedure TChooseWall.FormResize(Sender: TObject);
begin
  WallListBox.Columns:=WallListBox.Width div (WallWidth+8);
end;

procedure TChooseWall.NoImageButtonClick(Sender: TObject);
begin
  WallListBox.itemIndex:=-1;
end;

procedure TChooseWall.WallListBoxDblClick(Sender: TObject);
begin
  OKButton.Click;
end;

end.
