unit frmMain;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  TileGroup, ActnList, ComCtrls, ImgList, ToolWin, StdCtrls, ExtCtrls;

type
  TMainForm = class(TForm)
    ToolBar1: TToolBar;
    ImageList1: TImageList;
    ToolButton1: TToolButton;
    ActionList1: TActionList;
    ActionLoad: TAction;
    TileGroup1: TTileGroup;
    OpenTileSetDialog: TOpenDialog;
    PageControl1: TPageControl;
    TileSheet: TTabSheet;
    WallLeftSheet: TTabSheet;
    ObjectSheet: TTabSheet;
    TileListBox: TListBox;
    ActionRefresh: TAction;
    WallListBox: TListBox;
    OpenBitmapDialog: TOpenDialog;
    ActionAddTile: TAction;
    ToolButton2: TToolButton;
    ActionSave: TAction;
    ToolButton3: TToolButton;
    SaveDialog1: TSaveDialog;
    MaskFloorBitmap: TImage;
    MaskWallLeftBitmap: TImage;
    ToolButton4: TToolButton;
    MaskWallRightBitmap: TImage;
    ToolButton5: TToolButton;
    procedure ActionLoadExecute(Sender: TObject);
    procedure ActionRefreshExecute(Sender: TObject);
    procedure TileListBoxDrawItem(Control: TWinControl; Index: Integer;
      Rect: TRect; State: TOwnerDrawState);
    procedure TileSheetResize(Sender: TObject);
    procedure WallListBoxDrawItem(Control: TWinControl; Index: Integer;
      Rect: TRect; State: TOwnerDrawState);
    procedure ActionAddTileExecute(Sender: TObject);
    procedure TileListBoxDblClick(Sender: TObject);
    procedure ActionSaveExecute(Sender: TObject);
    procedure WallListBoxDblClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure ToolButton4Click(Sender: TObject);
  private
    { Private-Deklarationen }
  public
    procedure MaskTile(var Bitmap: TBitmap);
    procedure MaskWallLeft(Bitmap: TBitmap);
    procedure MaskWallRight(Bitmap: TBitmap);

    function GetWallHeight(Bitmap: TBitmap): Integer;

    procedure GenerateTileSimple(var Bitmap: TBitmap);
    procedure GenerateTileAccurate(var Bitmap: TBitmap);
    { Public-Deklarationen }
  end;

var
  MainForm: TMainForm;

implementation

uses frmSettings, frmWallSettings, Defines;

{$R *.DFM}

procedure TMainForm.ActionLoadExecute(Sender: TObject);
var
  Bitmap: TBitmap;
  Dummy : Integer;
begin
  if OpenTileSetDialog.Execute then
  begin
    TileGroup1.LoadFromFile(OpenTileSetDialog.Filename);
    ActionRefresh.Execute;
  end;
end;

procedure TMainForm.ActionRefreshExecute(Sender: TObject);
var
  Dummy: Integer;
begin
  Caption:='Tileset - Editor ['+TileGroup1.FileName+']';
  TileListBox.Items.Clear;
  For Dummy:=0 to TileGroup1.FloorCount-1 do
  begin
    TileListBox.Items.Add('');
  end;
  WallListBox.Items.Clear;
  For Dummy:=0 to TileGroup1.WallLeftCount-1 do
    WallListBox.Items.Add('');
end;

procedure TMainForm.TileListBoxDrawItem(Control: TWinControl; Index: Integer;
  Rect: TRect; State: TOwnerDrawState);
begin
  TileListBox.Canvas.FillRect(Rect);
  TileGroup1.DrawFloorCanvas(TileListBox.Canvas,Rect.Left+4,Rect.Top+4,Index);
end;

procedure TMainForm.TileSheetResize(Sender: TObject);
begin
  TileListBox.Columns:=TileListBox.Width div (TileWidth+8);
//  SendMessage(TileListBox.Handle, LB_SETCOLUMNWIDTH, (TileWidth+8), 0);
  WallListBox.Columns:=WallListBox.Width div ((WallWidth*2)+8);
  SendMessage(WallListBox.Handle, LB_SETCOLUMNWIDTH, (WallWidth*2)+12, 0);
end;

procedure TMainForm.WallListBoxDrawItem(Control: TWinControl; Index: Integer;
  Rect: TRect; State: TOwnerDrawState);
begin
  WallListBox.Canvas.FillRect(Rect);
  TileGroup1.DrawWallLeftCanvas(WallListBox.Canvas,Rect.Left+4,Rect.Top+4,Index);
  TileGroup1.DrawWallRightCanvas(WallListBox.Canvas,Rect.Left+WallWidth+8,Rect.Top+4,Index);
end;

procedure TMainForm.ActionAddTileExecute(Sender: TObject);
var
  Dummy    : Integer;
  Bitmap   : TBitmap;
  FileName : String;
  Ext      : String;
  RFileName: String;
begin
  if OpenBitmapDialog.Execute then
  begin
    Bitmap:=TBitmap.Create;
    for Dummy:=0 to OpenBitmapDialog.Files.Count-1 do
    begin
      Bitmap.LoadFromFile(OpenBitmapDialog.Files[Dummy]);
      if PageControl1.ActivePage=TileSheet then
      begin
        MaskTile(Bitmap);
        TileGroup1.AddFloor(Bitmap);
      end
      else if PageControl1.ActivePage=WallLeftSheet then
      begin
        MaskWallLeft(Bitmap);

        TileGroup1.AddWall(Bitmap);

        FileName:=ChangeFileExt(OpenBitmapDialog.Files[Dummy],'');
        Ext:=ExtractFileExt(OpenBitmapDialog.Files[Dummy]);
        if (FileName[length(FileName)-1]='_') and (lowerCase(FileName[length(FileName)])='l')then
        begin
          RFileName:=Copy(FileName,1,length(FileName)-2)+'_r'+Ext;
          if FileExists(RFileName) then
          begin
            Bitmap.LoadFromFile(RFileName);

            MaskWallRight(Bitmap);
            TileGroup1.ReplaceWallRight(Bitmap,TileGroup1.WallLeftCount-1);
          end;
        end;
      end;
    end;
    ActionRefresh.Execute;
  end;
end;

procedure TMainForm.TileListBoxDblClick(Sender: TObject);
begin
  if TileListBox.ItemIndex<>-1 then
    TileSettings.ShowModal;
end;

procedure TMainForm.ActionSaveExecute(Sender: TObject);
var
  Bitmap: TBitmap;
  Dummy : Integer;
begin
  if TileGroup1.Filename='' then
  begin
    if not SaveDialog1.Execute then
    begin
      exit;
    end;
    TileGroup1.Filename:=SaveDialog1.FileName;
    Caption:='Tileset - Editor ['+TileGroup1.FileName+']';
  end;

  Bitmap:=TBitmap.Create;

  Bitmap.Width:=WallWidth;
  Bitmap.Height:=Wallheight;
  for Dummy:=0 to TileGroup1.WallLeftCount-1 do
  begin
    TileGroup1.DrawWallLeftCanvas(Bitmap.Canvas,0,0,Dummy);
    TileGroup1.WallInformation(Dummy).AbsWallHeight:=GetWallHeight(Bitmap);
  end;

  Bitmap.Free;

  TileGroup1.SaveToFile;
end;

procedure TMainForm.WallListBoxDblClick(Sender: TObject);
begin
  if TListBox(Sender).ItemIndex<>-1 then
  begin
    WallSettings.EditIndex:=TListBox(Sender).ItemIndex;
    WallSettings.ShowModal;
  end;
end;

procedure TMainForm.FormCreate(Sender: TObject);
begin
  TileListBox.ItemHeight:=TileHeight+8;
  WallListBox.ItemHeight:=WallHeight+8;
end;

procedure TMainForm.MaskTile(var Bitmap: TBitmap);
begin
  if (Bitmap.Width=32) and (Bitmap.Height=32) then
    GenerateTileAccurate(Bitmap);

  Bitmap.Width:=TileWidth;
  Bitmap.Height:=TileHeight;
  Bitmap.Canvas.Brush.Style:=bsClear;
  Bitmap.Canvas.BrushCopy(Rect(0,0,TileWidth,TileHeight),MaskFloorBitmap.Picture.Bitmap,Rect(0,0,TileWidth,TileHeight),clWhite);
end;

procedure TMainForm.ToolButton4Click(Sender: TObject);
begin
  TileGroup1.New;
  ActionRefresh.Execute;
end;

procedure TMainForm.MaskWallRight(Bitmap: TBitmap);
var
  X,Y    : Integer;
  Ready  : Boolean;
begin
{  // Vom World-Editor werden fehlerhafte Grafiken erstellen. Diese werden hier
  // korrigiert
  X:=1;
  while (X<=7) do
  begin
    Y:=0;
    Ready:=false;
    while (Y<WallHeight) and (not Ready) do
    begin
      if Bitmap.Canvas.Pixels[X,Y]<>clFuchsia then
      begin
        if (Bitmap.Canvas.Pixels[X-1,Y]=clFuchsia) and (Bitmap.Canvas.Pixels[X+1,Y]<>clFuchsia) then
          Bitmap.Canvas.Pixels[X,Y]:=clFuchsia;
        Ready:=true;
      end;
      inc(Y);
    end;
    inc(X,2);
  end;}
  Bitmap.Width:=WallWidth;
  Bitmap.Height:=WallHeight;
  Bitmap.Canvas.Brush.Style:=bsClear;
  Bitmap.Canvas.BrushCopy(Rect(0,0,WallWidth,WallHeight),MaskWallRightBitmap.Picture.Bitmap,Rect(0,0,WallWidth,WallHeight),clWhite);
end;

procedure TMainForm.MaskWallLeft(Bitmap: TBitmap);
var
  X,Y    : Integer;
  Ready  : Boolean;
begin
  Bitmap.Width:=WallWidth;
  Bitmap.Height:=WallHeight;
  Bitmap.Canvas.Brush.Style:=bsClear;
  Bitmap.Canvas.BrushCopy(Rect(0,0,WallWidth,WallHeight),MaskWallLeftBitmap.Picture.Bitmap,Rect(0,0,WallWidth,WallHeight),clWhite);
end;

procedure TMainForm.GenerateTileSimple(var Bitmap: TBitmap);
var
  TempBitmap: TBitmap;
  X,Y       : Integer;
  NewX,NewY : Integer;
begin
  TempBitmap:=Bitmap;
  Bitmap:=TBitmap.Create;
  Bitmap.Width:=TileWidth;
  Bitmap.Height:=TileHeight;

  Y:=0;
  while (Y<32) do
  begin
    NewX:=(HalfTileWidth-1)+Y;
    NewY:=Y div 2;
    for X:=0 to 31 do
    begin
      Bitmap.Canvas.Pixels[NewX,NewY]:=TempBitmap.Canvas.Pixels[X,Y];
      dec(NewX);
      if (X mod 2)=1 then
        inc(NewY);
    end;
    inc(Y,2);
  end;

  Y:=1;
  while (Y<32) do
  begin
    NewX:=(HalfTileWidth)+Y;
    NewY:=Y div 2;
    for X:=0 to 31 do
    begin
      Bitmap.Canvas.Pixels[NewX,NewY]:=TempBitmap.Canvas.Pixels[X,Y];
      dec(NewX);
      if (X mod 2)=1 then
        inc(NewY);
    end;
    inc(Y,2);
  end;
end;

procedure TMainForm.GenerateTileAccurate(var Bitmap: TBitmap);
var
  TempBitmap: TBitmap;
  Temp2Bitmap: TBitmap;
  X,Y       : Integer;
  Counter   : Integer;
  Switcher  : Boolean;
begin
  TempBitmap:=Bitmap;
  Bitmap:=TBitmap.Create;
  Bitmap.Width:=TileWidth;
  Bitmap.Height:=TileHeight;

  Temp2Bitmap:=TBitmap.Create;
  Temp2Bitmap.Width:=TileWidth;
  Temp2Bitmap.Height:=TileHeight;

  Switcher:=false;
  for Y:=0 to 31 do
  begin
    for X:=0 to 31 do
    begin
      if not (Switcher) or (X<>0) then
        Temp2Bitmap.Canvas.Pixels[X+Y,Y]:=TempBitmap.Canvas.Pixels[X,Y]
      else
        Temp2Bitmap.Canvas.Pixels[Y+32,Y]:=TempBitmap.Canvas.Pixels[X,Y];
    end;
    Switcher:=not Switcher;
  end;


  Counter:=-15;
  Switcher:=false;
  for X:=0 to TileWidth-1 do
  begin
    for Y:=0 to TileHeight-1 do
    begin
      Bitmap.Canvas.Pixels[X,Y-Counter]:=Temp2Bitmap.Canvas.Pixels[X,Y]
    end;

    if Switcher then
      inc(Counter);

    Switcher:=not Switcher;
  end;

  Temp2Bitmap.Free;
  TempBitmap.Free;
end;

function TMainForm.GetWallHeight(Bitmap: TBitmap): Integer;
var
  X: Integer;
  Y: Integer;
begin
  for Y:=0 to WallHeight do
  begin
    for X:=0 to WallWidth-Wall3DEffekt do
    begin
      if Bitmap.Canvas.Pixels[X,Y]<>clFuchsia then
      begin
        result:=(WallHeight-Y)-(Wall3DEffekt div 2)-(X div 2);
        exit;
      end;
    end;
  end;
end;

end.
