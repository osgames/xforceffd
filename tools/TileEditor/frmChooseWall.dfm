object ChooseWall: TChooseWall
  Left = 337
  Top = 209
  Width = 558
  Height = 417
  BorderIcons = [biSystemMenu]
  Caption = 'Mauer ausw'#228'hlen'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poMainFormCenter
  OnResize = FormResize
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object WallListBox: TListBox
    Left = 0
    Top = 0
    Width = 550
    Height = 352
    Style = lbOwnerDrawFixed
    Align = alClient
    BorderStyle = bsNone
    Color = clBtnFace
    Columns = 18
    ItemHeight = 60
    TabOrder = 0
    OnDblClick = WallListBoxDblClick
    OnDrawItem = WallListBoxDrawItem
  end
  object Panel1: TPanel
    Left = 0
    Top = 352
    Width = 550
    Height = 36
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 1
    object OKButton: TBitBtn
      Left = 360
      Top = 8
      Width = 89
      Height = 25
      TabOrder = 0
      Kind = bkOK
    end
    object BitBtn2: TBitBtn
      Left = 456
      Top = 8
      Width = 91
      Height = 25
      TabOrder = 1
      Kind = bkCancel
    end
    object NoImageButton: TBitBtn
      Left = 3
      Top = 8
      Width = 89
      Height = 25
      Caption = 'keine Auswahl'
      Default = True
      ModalResult = 1
      TabOrder = 2
      Visible = False
      OnClick = NoImageButtonClick
      NumGlyphs = 2
    end
  end
end
