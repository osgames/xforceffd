program TileEdit;

uses
  MemCheck,
  Forms,
  frmMain in 'frmMain.pas' {MainForm},
  frmChooseTile in 'frmChooseTile.pas' {ChooseTile},
  frmWallSettings in 'frmWallSettings.pas' {WallSettings},
  frmChooseWall in 'frmChooseWall.pas' {ChooseWall},
  frmSettings in 'frmSettings.pas' {TileSettings};

{$R *.RES}

begin
  Application.Initialize;
  Application.CreateForm(TMainForm, MainForm);
  Application.CreateForm(TChooseTile, ChooseTile);
  Application.CreateForm(TWallSettings, WallSettings);
  Application.CreateForm(TChooseWall, ChooseWall);
  Application.CreateForm(TTileSettings, TileSettings);
  Application.Run;
end.
