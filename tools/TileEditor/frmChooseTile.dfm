object ChooseTile: TChooseTile
  Left = 320
  Top = 422
  Width = 549
  Height = 419
  BorderIcons = [biSystemMenu]
  Caption = 'Tile ausw'#228'hlen'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poMainFormCenter
  OnResize = FormResize
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object ListBox1: TListBox
    Left = 0
    Top = 0
    Width = 541
    Height = 354
    Style = lbOwnerDrawFixed
    Align = alClient
    BorderStyle = bsNone
    Color = clBtnFace
    Columns = 10
    ItemHeight = 35
    TabOrder = 0
    OnDblClick = ListBox1DblClick
    OnDrawItem = ListBox1DrawItem
  end
  object Panel1: TPanel
    Left = 0
    Top = 354
    Width = 541
    Height = 36
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 1
    DesignSize = (
      541
      36)
    object BitBtn1: TBitBtn
      Left = 344
      Top = 8
      Width = 89
      Height = 25
      Anchors = [akTop, akRight]
      TabOrder = 0
      Kind = bkOK
    end
    object BitBtn2: TBitBtn
      Left = 440
      Top = 8
      Width = 91
      Height = 25
      Anchors = [akTop, akRight]
      TabOrder = 1
      Kind = bkCancel
    end
  end
end
