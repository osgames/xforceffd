object TileSettings: TTileSettings
  Left = 375
  Top = 200
  BorderStyle = bsDialog
  Caption = 'Eigenschaften Untergr'#252'nde'
  ClientHeight = 254
  ClientWidth = 437
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poMainFormCenter
  OnShow = FormShow
  DesignSize = (
    437
    254)
  PixelsPerInch = 96
  TextHeight = 13
  object PaintBox1: TPaintBox
    Left = 144
    Top = 8
    Width = 64
    Height = 31
    OnPaint = PaintBox1Paint
  end
  object Label1: TLabel
    Left = 8
    Top = 16
    Width = 36
    Height = 13
    Caption = 'Normal:'
  end
  object Label2: TLabel
    Left = 8
    Top = 48
    Width = 106
    Height = 13
    Caption = 'mit dem Uhrzeigersinn:'
  end
  object PaintBox2: TPaintBox
    Left = 144
    Top = 48
    Width = 64
    Height = 31
    OnPaint = PaintBox2Paint
  end
  object Label3: TLabel
    Left = 8
    Top = 88
    Width = 101
    Height = 13
    Caption = 'Horizontal gespiegelt:'
  end
  object PaintBox3: TPaintBox
    Left = 144
    Top = 88
    Width = 64
    Height = 31
    OnPaint = PaintBox3Paint
  end
  object Label4: TLabel
    Left = 8
    Top = 168
    Width = 121
    Height = 13
    Caption = 'gegen den Uhrzeigersinn:'
  end
  object PaintBox4: TPaintBox
    Left = 144
    Top = 168
    Width = 64
    Height = 31
    OnPaint = PaintBox4Paint
  end
  object Label5: TLabel
    Left = 8
    Top = 128
    Width = 89
    Height = 13
    Caption = 'Vertikal gespiegelt:'
  end
  object PaintBox5: TPaintBox
    Left = 144
    Top = 128
    Width = 64
    Height = 31
    OnPaint = PaintBox5Paint
  end
  object Image1: TImage
    Left = 8
    Top = 64
    Width = 16
    Height = 16
    AutoSize = True
    Picture.Data = {
      07544269746D617036040000424D360400000000000036000000280000001000
      0000100000000100200000000000000400000000000000000000000000000000
      0000FF00FF00FF00FF00FF00FF00FF00FF00FF00FF0080000000800000008000
      000080000000800000008000000080000000FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF008000000080000000800000008000
      00008000000080000000800000008000000080000000FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00800000008000000080000000FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00800000008000000080000000FF00FF00FF00
      FF00FF00FF00FF00FF00800000008000000080000000FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00800000008000000080000000FF00
      FF00FF00FF00FF00FF008000000080000000FF00FF00FF00FF00FF00FF00FF00
      FF000080000000800000FF00FF00FF00FF00FF00FF008000000080000000FF00
      FF00FF00FF00FF00FF008000000080000000FF00FF00FF00FF00FF00FF000080
      0000008000000080000000800000FF00FF00FF00FF008000000080000000FF00
      FF00FF00FF00FF00FF008000000080000000FF00FF00FF00FF00FF00FF000080
      0000008000000080000000800000FF00FF00FF00FF008000000080000000FF00
      FF00FF00FF00FF00FF008000000080000000FF00FF00FF00FF00FF00FF00FF00
      FF000080000000800000FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF008000000080000000FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF008000000080000000FF00
      FF00FF00FF00FF00FF00800000008000000080000000FF00FF00FF00FF008000
      0000FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00800000008000000080000000FF00FF008000
      000080000000FF00FF00FF00FF00FF00FF008000000080000000FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF008000000080000000800000008000
      00008000000080000000FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF0080000000800000008000
      00008000000080000000FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF008000
      000080000000FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF008000
      0000FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00}
    Transparent = True
  end
  object Image2: TImage
    Left = 8
    Top = 104
    Width = 16
    Height = 16
    AutoSize = True
    Picture.Data = {
      07544269746D617036040000424D360400000000000036000000280000001000
      0000100000000100200000000000000400000000000000000000000000000000
      0000FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF0080000000FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF000000800000008000000080000000800000008000FF00
      FF00FF00FF00FF00FF00FF00FF0080000000FF00000080000000FF00FF00FF00
      FF00FF00FF00FF00FF000000800000008000FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF0080000000FF00000080000000FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF0000008000FF00FF0000008000FF00FF00FF00FF00FF00
      FF00FF00FF0080000000FF00000080000000FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF0000008000FF00FF00FF00FF0000008000FF00FF00FF00
      FF0080000000FF00000080000000FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF0000008000FF00FF00FF00FF00FF00FF00000080008000
      0000FF00000080000000FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00800000000000
      800080000000FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF0080000000FF0000008000
      000000008000FF00FF00FF00FF00FF00FF0000008000FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF0080000000FF00000080000000FF00
      FF00FF00FF0000008000FF00FF00FF00FF0000008000FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF0080000000FF00000080000000FF00FF00FF00
      FF00FF00FF00FF00FF0000008000FF00FF0000008000FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF0080000000FF00000080000000FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF000000800000008000FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF0080000000FF00FF00FF00FF00FF00FF00FF00
      FF000000800000008000000080000000800000008000FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00}
    Transparent = True
  end
  object Image3: TImage
    Left = 8
    Top = 144
    Width = 16
    Height = 16
    AutoSize = True
    Picture.Data = {
      07544269746D617036040000424D360400000000000036000000280000001000
      0000100000000100200000000000000400000000000000000000000000000000
      0000FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF0080000000FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF0080000000FF00000080000000FF00FF00FF00FF00FF00
      FF00FF00FF000000800000008000000080000000800000008000FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF0080000000FF00000080000000FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF000000800000008000FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF0080000000FF00000080000000FF00
      FF00FF00FF00FF00FF00FF00FF0000008000FF00FF0000008000FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF0080000000FF0000008000
      0000FF00FF00FF00FF0000008000FF00FF00FF00FF0000008000FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF0080000000FF00
      00008000000000008000FF00FF00FF00FF00FF00FF0000008000FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF008000
      00000000800080000000FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF0000008000FF00FF00FF00FF00FF00FF000000
      800080000000FF00000080000000FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF0000008000FF00FF00FF00FF0000008000FF00
      FF00FF00FF0080000000FF00000080000000FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF0000008000FF00FF0000008000FF00FF00FF00
      FF00FF00FF00FF00FF0080000000FF00000080000000FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF000000800000008000FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF0080000000FF00000080000000FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00000080000000800000008000000080000000
      8000FF00FF00FF00FF00FF00FF00FF00FF0080000000FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00}
    Transparent = True
  end
  object Image4: TImage
    Left = 8
    Top = 184
    Width = 16
    Height = 16
    AutoSize = True
    Picture.Data = {
      07544269746D617036040000424D360400000000000036000000280000001000
      0000100000000100200000000000000400000000000000000000000000000000
      0000FF00FF00FF00FF00FF00FF00FF00FF008000000080000000800000008000
      0000800000008000000080000000FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00800000008000000080000000800000008000
      000080000000800000008000000080000000FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00800000008000000080000000FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00800000008000000080000000FF00FF00FF00FF00FF00
      FF00FF00FF00800000008000000080000000FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00800000008000000080000000FF00FF00FF00
      FF00FF00FF008000000080000000FF00FF00FF00FF00FF00FF00008000000080
      0000FF00FF00FF00FF00FF00FF00FF00FF008000000080000000FF00FF00FF00
      FF00FF00FF008000000080000000FF00FF00FF00FF0000800000008000000080
      000000800000FF00FF00FF00FF00FF00FF008000000080000000FF00FF00FF00
      FF00FF00FF008000000080000000FF00FF00FF00FF0000800000008000000080
      000000800000FF00FF00FF00FF00FF00FF008000000080000000FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00008000000080
      0000FF00FF00FF00FF00FF00FF00FF00FF008000000080000000FF00FF00FF00
      FF00FF00FF008000000080000000FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF008000000080000000FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF0080000000FF00FF00FF00FF00800000008000000080000000FF00FF00FF00
      FF00FF00FF00FF00FF008000000080000000FF00FF00FF00FF00FF00FF008000
      000080000000FF00FF00800000008000000080000000FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00800000008000
      000080000000800000008000000080000000FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00800000008000
      0000800000008000000080000000FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF008000
      000080000000FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF0080000000FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00}
    Transparent = True
  end
  object BitBtn1: TBitBtn
    Left = 228
    Top = 208
    Width = 91
    Height = 25
    Anchors = [akTop, akRight]
    TabOrder = 0
    OnClick = BitBtn1Click
    Kind = bkOK
  end
  object Button1: TButton
    Left = 212
    Top = 50
    Width = 25
    Height = 25
    Anchors = [akTop, akRight]
    Caption = '...'
    TabOrder = 1
    OnClick = Button1Click
  end
  object Button2: TButton
    Left = 212
    Top = 90
    Width = 25
    Height = 25
    Anchors = [akTop, akRight]
    Caption = '...'
    TabOrder = 2
    OnClick = Button2Click
  end
  object Button3: TButton
    Left = 212
    Top = 170
    Width = 25
    Height = 25
    Anchors = [akTop, akRight]
    Caption = '...'
    TabOrder = 3
    OnClick = Button3Click
  end
  object BitBtn2: TBitBtn
    Left = 339
    Top = 208
    Width = 91
    Height = 25
    Anchors = [akTop, akRight]
    TabOrder = 4
    Kind = bkCancel
  end
  object Button4: TButton
    Left = 212
    Top = 130
    Width = 25
    Height = 25
    Anchors = [akTop, akRight]
    Caption = '...'
    TabOrder = 5
    OnClick = Button4Click
  end
  object Button5: TButton
    Left = 212
    Top = 10
    Width = 25
    Height = 25
    Hint = 'Neues Bild laden'
    Anchors = [akTop, akRight]
    Caption = '...'
    TabOrder = 6
    OnClick = Button5Click
  end
  object GroupBox1: TGroupBox
    Left = 248
    Top = 8
    Width = 177
    Height = 193
    Caption = 'Einstellungen'
    TabOrder = 7
    object BegehbarCheckBox: TCheckBox
      Left = 8
      Top = 16
      Width = 97
      Height = 17
      Caption = 'Begehbar'
      TabOrder = 0
      OnClick = BegehbarCheckBoxClick
    end
  end
  object OpenBitmapDialog: TOpenDialog
    DefaultExt = 'bmp'
    Filter = 'Bitmaps (*.bmp)|*.bmp'
    Options = [ofHideReadOnly, ofPathMustExist, ofFileMustExist, ofEnableSizing]
    Title = 'Tile ersetzen'
    Left = 24
    Top = 8
  end
end
