unit frmTileGroupAdd;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, TileGroup, types, Defines;

type
  TfrmTileGroup = class(TForm)
    BitBtn1: TBitBtn;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    RotateRightButton: TButton;
    MirrorHoriButton: TButton;
    RotateLeftButton: TButton;
    BitBtn2: TBitBtn;
    Label5: TLabel;
    MirrorVertButton: TButton;
    NormalButton: TButton;
    OpenBitmapDialog: TOpenDialog;
    NormalImage: TImage;
    RotateRightImage: TImage;
    MirrorHoriImage: TImage;
    MirriorVertImage: TImage;
    RotateLeftImage: TImage;
    procedure BitBtn1Click(Sender: TObject);
    procedure ChooseImageClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private-Deklarationen }
  public
    { Public-Deklarationen }
  end;

var
  frmTileGroup: TfrmTileGroup;

implementation

uses frmMain, frmChooseTile;

{$R *.DFM}

procedure TfrmTileGroup.BitBtn1Click(Sender: TObject);
var
  Index: Integer;
begin
  with MainForm.TileGroup1 do
  begin
    Index:=FloorCount;
//    AddFloor(NormalImage.Picture.Bitmap);
    AddFloor(RotateRightImage.Picture.Bitmap);
    AddFloor(MirrorHoriImage.Picture.Bitmap);
    AddFloor(MirriorVertImage.Picture.Bitmap);
    AddFloor(RotateLeftImage.Picture.Bitmap);

    // Normal
{    with TileInformation(Index)^ do
    begin
      LeftRotate:=Index+1;
      HMirror:=Index+2;
      VMirror:=Index+3;
      RightRotate:=Index+4;
    end;
}
    // Rechts rotiert
    with TileInformation(Index)^ do
    begin
      LeftRotate:=Index;
      HMirror:=Index+4;
      VMirror:=Index+4;
      RightRotate:=Index+1;
    end;

    // Hori gespiegelt
    with TileInformation(Index+2)^ do
    begin
      LeftRotate:=Index+1;
      HMirror:=Index+2;
      VMirror:=Index+3;
      RightRotate:=Index+4;
    end;

    // Verti gespiegelt
    with TileInformation(Index+3)^ do
    begin
      LeftRotate:=Index+1;
      HMirror:=Index+2;
      VMirror:=Index+3;
      RightRotate:=Index+4;
    end;

    // Links rotiert
    with TileInformation(Index+4)^ do
    begin
      LeftRotate:=Index+1;
      HMirror:=Index+2;
      VMirror:=Index+3;
      RightRotate:=Index+4;
    end;
  end;
  MainForm.ActionRefresh.Execute;
end;

procedure TfrmTileGroup.ChooseImageClick(Sender: TObject);
var
  Bitmap: TBitmap;
begin
  if OpenBitmapDialog.Execute then
  begin
    Bitmap:=TBitmap.Create;
    Bitmap.LoadFromFile(OpenBitmapDialog.FileName);

    Bitmap.Width:=TileWidth;
    Bitmap.Height:=TileHeight;

    Bitmap.Canvas.Brush.Style:=bsClear;
    Bitmap.Canvas.BrushCopy(Rect(0,0,TileWidth,TileHeight),MainForm.MaskFloor.Picture.Bitmap,Rect(0,0,TileWidth,TileHeight),clWhite);

    TImage((Sender as TButton).Tag).Picture.Bitmap:=Bitmap;

    MainForm.Refresh;
    Refresh;
    Bitmap.Free;
  end;

end;

procedure TfrmTileGroup.FormCreate(Sender: TObject);
begin
  NormalButton.Tag:=Integer(NormalImage);
  RotateRightButton.Tag:=Integer(RotateRightImage);
  MirrorHoriButton.Tag:=Integer(MirrorHoriImage);
  MirrorVertButton.Tag:=Integer(MirriorVertImage);
  RotateLeftButton.Tag:=Integer(RotateLeftImage);
end;

end.
