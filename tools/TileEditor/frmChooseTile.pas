unit frmChooseTile;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, ExtCtrls, Defines;

type
  TChooseTile = class(TForm)
    ListBox1: TListBox;
    Panel1: TPanel;
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    procedure ListBox1DrawItem(Control: TWinControl; Index: Integer;
      Rect: TRect; State: TOwnerDrawState);
    procedure FormShow(Sender: TObject);
    procedure ListBox1DblClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
  private
    { Private-Deklarationen }
  public
    { Public-Deklarationen }
  end;

var
  ChooseTile: TChooseTile;

implementation

uses frmMain;

{$R *.DFM}

procedure TChooseTile.ListBox1DrawItem(Control: TWinControl; Index: Integer;
  Rect: TRect; State: TOwnerDrawState);
begin
  ListBox1.Canvas.FillRect(Rect);
  MainForm.TileGroup1.DrawFloorCanvas(ListBox1.Canvas,Rect.Left+3,Rect.Top+3,Index);
end;

procedure TChooseTile.FormShow(Sender: TObject);
var
  Dummy: Integer;
begin
  ListBox1.ItemHeight:=TileHeight+8;
  ListBox1.Columns:=ListBox1.Width div (TileWidth+8);
  ListBox1.Items.Clear;
  for Dummy:=0 to MainForm.TileGroup1.FloorCount-1 do
  begin
    ListBox1.Items.Add('');
  end;
end;

procedure TChooseTile.ListBox1DblClick(Sender: TObject);
begin
  BitBtn1.Click;
end;

procedure TChooseTile.FormResize(Sender: TObject);
begin
  ListBox1.Columns:=ListBox1.Width div (TileWidth+8);
end;

end.
