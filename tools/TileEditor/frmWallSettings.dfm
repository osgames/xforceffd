object WallSettings: TWallSettings
  Left = 485
  Top = 377
  BorderStyle = bsDialog
  Caption = 'Eigenschaften Mauern'
  ClientHeight = 348
  ClientWidth = 571
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poMainFormCenter
  ShowHint = True
  OnShow = FormShow
  DesignSize = (
    571
    348)
  PixelsPerInch = 96
  TextHeight = 13
  object NormalPaintBox: TPaintBox
    Left = 8
    Top = 24
    Width = 42
    Height = 100
    OnPaint = WallLeftPaint
  end
  object MirrorBox: TPaintBox
    Left = 168
    Top = 24
    Width = 42
    Height = 100
    OnPaint = WallLeftPaint
  end
  object Label1: TLabel
    Left = 168
    Top = 8
    Width = 53
    Height = 13
    Caption = 'Gespiegelt:'
    OnClick = BitBtn1Click
  end
  object Label2: TLabel
    Left = 248
    Top = 8
    Width = 51
    Height = 13
    Caption = 'R'#252'ckseite:'
    OnClick = BitBtn1Click
  end
  object BackDoorBox: TPaintBox
    Left = 248
    Top = 24
    Width = 42
    Height = 100
    OnPaint = WallLeftPaint
  end
  object Rahmen: TLabel
    Left = 328
    Top = 8
    Width = 43
    Height = 13
    Caption = 'Rahmen:'
    OnClick = BitBtn1Click
  end
  object BorderBox: TPaintBox
    Left = 328
    Top = 24
    Width = 42
    Height = 100
    OnPaint = WallLeftPaint
  end
  object Label3: TLabel
    Left = 8
    Top = 8
    Width = 28
    Height = 13
    Caption = 'Links:'
  end
  object RightPaintBox: TPaintBox
    Left = 88
    Top = 24
    Width = 42
    Height = 100
    OnPaint = WallRightPaint
  end
  object Label5: TLabel
    Left = 88
    Top = 8
    Width = 37
    Height = 13
    Caption = 'Rechts:'
  end
  object DestroyedBox: TPaintBox
    Left = 8
    Top = 152
    Width = 42
    Height = 100
    OnPaint = WallLeftPaint
  end
  object Label7: TLabel
    Left = 8
    Top = 136
    Width = 39
    Height = 13
    Caption = 'Zerst'#246'rt:'
    OnClick = BitBtn1Click
  end
  object BitBtn1: TBitBtn
    Left = 7
    Top = 316
    Width = 91
    Height = 25
    Anchors = [akLeft, akBottom]
    TabOrder = 0
    OnClick = BitBtn1Click
    Kind = bkOK
  end
  object BitBtn2: TBitBtn
    Left = 110
    Top = 316
    Width = 91
    Height = 25
    Anchors = [akLeft, akBottom]
    TabOrder = 1
    Kind = bkCancel
  end
  object RadioGroup1: TRadioGroup
    Left = 408
    Top = 21
    Width = 153
    Height = 77
    Caption = 'als T'#252'r'
    Items.Strings = (
      'keine T'#252'r'
      'nach Links '#246'ffnend'
      'nach Rechts '#246'ffnend')
    TabOrder = 2
    OnClick = RadioGroup1Click
  end
  object Button1: TButton
    Left = 216
    Top = 24
    Width = 25
    Height = 25
    Caption = '...'
    TabOrder = 3
    OnClick = Button1Click
  end
  object Button2: TButton
    Left = 296
    Top = 24
    Width = 25
    Height = 25
    Caption = '...'
    TabOrder = 4
    OnClick = Button2Click
  end
  object Button3: TButton
    Left = 376
    Top = 24
    Width = 25
    Height = 25
    Caption = '...'
    TabOrder = 5
    OnClick = Button3Click
  end
  object Button4: TButton
    Left = 56
    Top = 24
    Width = 25
    Height = 25
    Caption = '...'
    TabOrder = 6
    OnClick = Button4Click
  end
  object GroupBox1: TGroupBox
    Left = 408
    Top = 105
    Width = 153
    Height = 233
    Caption = 'Eigenschaften'
    TabOrder = 7
    object Label4: TLabel
      Left = 8
      Top = 36
      Width = 30
      Height = 13
      Caption = 'Farbe:'
    end
    object Label6: TLabel
      Left = 8
      Top = 104
      Width = 68
      Height = 13
      Caption = 'Zerst'#246'rbarkeit:'
    end
    object Label8: TLabel
      Left = 8
      Top = 148
      Width = 44
      Height = 13
      Caption = 'Hitpoints:'
    end
    object DrawWindow: TCheckBox
      Left = 8
      Top = 16
      Width = 137
      Height = 17
      Hint = 'Zeichnet rein Blaue Fenster als transparente Fenster'
      Caption = 'als Fenster'
      TabOrder = 0
      OnClick = DrawWindowClick
    end
    object ShowThrough: TCheckBox
      Left = 8
      Top = 56
      Width = 137
      Height = 17
      Hint = 
        'Durch die Wand kann durchgeschaut werden (sinnvoll bei Z'#228'unen/Fe' +
        'nstern)'
      Caption = 'Durchschaubar'
      TabOrder = 1
      OnClick = ShowThroughClick
    end
    object PanelColor: TPanel
      Left = 48
      Top = 32
      Width = 49
      Height = 22
      BevelOuter = bvLowered
      Color = clBlack
      TabOrder = 2
    end
    object ChooseColorButton: TButton
      Left = 104
      Top = 32
      Width = 21
      Height = 22
      Caption = '...'
      Enabled = False
      TabOrder = 3
      OnClick = ChooseColorButtonClick
    end
    object BarenessBox: TComboBox
      Left = 8
      Top = 120
      Width = 135
      Height = 21
      Style = csDropDownList
      ItemHeight = 13
      TabOrder = 4
      OnChange = BarenessBoxChange
      Items.Strings = (
        'Unzerst'#246'rbar'
        'Stahl'
        'Beton'
        'Holz'
        'Pappe'
        'kein Hindernis')
    end
    object WalkThrough: TCheckBox
      Left = 8
      Top = 80
      Width = 137
      Height = 17
      Hint = 
        'Durch die Wand kann durchgelaufen werden (sinnvoll bei zerst'#246'rte' +
        'n W'#228'nden)'
      Caption = 'Durchgehbar'
      TabOrder = 5
      OnClick = WalkThroughClick
    end
    object IsEckeBox: TCheckBox
      Left = 8
      Top = 176
      Width = 137
      Height = 17
      Hint = 
        'Durch die Wand kann durchgelaufen werden (sinnvoll bei zerst'#246'rte' +
        'n W'#228'nden)'
      Caption = 'Mauerecke'
      TabOrder = 6
      OnClick = IsEckeBoxClick
    end
    object HitPointEdit: TJvSpinEdit
      Left = 70
      Top = 144
      Width = 73
      Height = 21
      BeepOnError = False
      MaxValue = 100000.000000000000000000
      TabOrder = 7
      OnChange = HitPointEditChange
      ClipboardCommands = []
    end
  end
  object Button5: TButton
    Left = 136
    Top = 24
    Width = 25
    Height = 25
    Caption = '...'
    TabOrder = 8
    OnClick = Button5Click
  end
  object ChooseDestroyed: TButton
    Left = 56
    Top = 152
    Width = 25
    Height = 25
    Caption = '...'
    TabOrder = 9
    OnClick = ChooseDestroyedClick
  end
  object DestroySettings: TButton
    Left = 56
    Top = 184
    Width = 25
    Height = 25
    Caption = '->'
    TabOrder = 10
    OnClick = DestroySettingsClick
  end
  object OpenBitmapDialog: TOpenDialog
    DefaultExt = 'bmp'
    Filter = 'Bitmaps (*.bmp)|*.bmp'
    Options = [ofHideReadOnly, ofPathMustExist, ofFileMustExist, ofEnableSizing]
    Title = 'Mauer ersetzen'
    Left = 24
    Top = 8
  end
  object ColorDialog: TColorDialog
    Left = 24
    Top = 40
  end
end
