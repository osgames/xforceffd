unit frmWallSettings;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, ExtCtrls, xforce_types, Defines, Mask, JvExMask,
  JvSpin;

type
  TWallSettings = class(TForm)
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    NormalPaintBox: TPaintBox;
    RadioGroup1: TRadioGroup;
    MirrorBox: TPaintBox;
    Label1: TLabel;
    Button1: TButton;
    Label2: TLabel;
    BackDoorBox: TPaintBox;
    Button2: TButton;
    Rahmen: TLabel;
    BorderBox: TPaintBox;
    Button3: TButton;
    Label3: TLabel;
    Button4: TButton;
    OpenBitmapDialog: TOpenDialog;
    GroupBox1: TGroupBox;
    DrawWindow: TCheckBox;
    ShowThrough: TCheckBox;
    Label4: TLabel;
    PanelColor: TPanel;
    ChooseColorButton: TButton;
    ColorDialog: TColorDialog;
    RightPaintBox: TPaintBox;
    Label5: TLabel;
    Button5: TButton;
    Label6: TLabel;
    BarenessBox: TComboBox;
    DestroyedBox: TPaintBox;
    Label7: TLabel;
    ChooseDestroyed: TButton;
    DestroySettings: TButton;
    WalkThrough: TCheckBox;
    IsEckeBox: TCheckBox;
    Label8: TLabel;
    HitPointEdit: TJvSpinEdit;
    procedure WallLeftPaint(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure RadioGroup1Click(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure Button3Click(Sender: TObject);
    procedure Button4Click(Sender: TObject);
    procedure DrawWindowClick(Sender: TObject);
    procedure ShowThroughClick(Sender: TObject);
    procedure ChooseColorButtonClick(Sender: TObject);
    procedure WallRightPaint(Sender: TObject);
    procedure Button5Click(Sender: TObject);
    procedure BarenessBoxChange(Sender: TObject);
    procedure ChooseDestroyedClick(Sender: TObject);
    procedure DestroySettingsClick(Sender: TObject);
    procedure WalkThroughClick(Sender: TObject);
    procedure IsEckeBoxClick(Sender: TObject);
    procedure HitPointEditChange(Sender: TObject);
  private
    fWallInformation : TWallInformation;
    fEditIndex       : Integer;

    function GetWallIndex: Integer;
    function GetWallInformation(Index : Integer = -1): PWallInformation;

    { Private-Deklarationen }
  public
    procedure AktuPaintBoxs;

    property EditIndex: Integer read fEditIndex write fEditIndex;
    { Public-Deklarationen }
  end;

var
  WallSettings: TWallSettings;

implementation

uses frmMain, frmChooseWall;

{$R *.DFM}

procedure TWallSettings.WallLeftPaint(Sender: TObject);
begin
  TPaintBox(Sender).Canvas.Rectangle(TPaintBox(Sender).ClientRect);
  MainForm.TileGroup1.DrawWallLeftCanvas(TPaintBox(Sender).Canvas,1,1,TPaintBox(Sender).Tag)
end;

procedure TWallSettings.FormShow(Sender: TObject);
begin
  fWallInformation:=GetWallInformation^;
  RadioGroup1.ItemIndex:=Integer(fWallInformation.DoorType);

  DrawWindow.Checked:=fWallInformation.DrawWindow;
  ShowThrough.Checked:=fWallInformation.ShowThrough;
  WalkThrough.Checked:=fWallInformation.WalkThrough;
  IsEckeBox.Checked:=fWallInformation.IsEcke;

  ChooseColorButton.Enabled:=DrawWindow.Checked;
  PanelColor.Color:=fWallInformation.WindowColor;

  HitPointEdit.Value:=fWallInformation.HitPoints;

  BarenessBox.ItemIndex:=fWallInformation.Barness;

  AktuPaintBoxs;
end;

procedure TWallSettings.BitBtn1Click(Sender: TObject);
var
  Index: Integer;
begin
  GetWallInformation^:=fWallInformation;
  Index:=GetWallIndex;

  with fWallInformation do
  begin
    if Mirror<>Index then
      GetWallInformation(Mirror).Mirror:=Index;

    if BackSide<>Index then
      GetWallInformation(BackSide).BackSide:=Index;

  end;
end;

procedure TWallSettings.RadioGroup1Click(Sender: TObject);
begin
  fWallInformation.DoorType:=TDoorType(RadioGroup1.ItemIndex);
end;

procedure TWallSettings.Button1Click(Sender: TObject);
begin
  if ChooseWall.ShowModal=mrOK then
  begin
    fWallInformation.Mirror:=ChooseWall.WallListBox.ItemIndex;
    AktuPaintBoxs;
  end;
end;

procedure TWallSettings.Button2Click(Sender: TObject);
begin
  if ChooseWall.ShowModal=mrOK then
  begin
    fWallInformation.BackSide:=ChooseWall.WallListBox.ItemIndex;
    AktuPaintBoxs;
  end;
end;

procedure TWallSettings.Button3Click(Sender: TObject);
begin
  ChooseWall.NoImageButton.Visible:=true;
  if ChooseWall.ShowModal=mrOK then
  begin
    fWallInformation.Border:=ChooseWall.WallListBox.ItemIndex;
    AktuPaintBoxs;
  end;
  ChooseWall.NoImageButton.Visible:=false;
end;

procedure TWallSettings.AktuPaintBoxs;
begin
  NormalPaintBox.Tag:=GetWallIndex;
  RightPaintBox.Tag:=GetWallIndex;
  MirrorBox.Tag:=fWallInformation.Mirror;
  BackDoorBox.Tag:=fWallInformation.BackSide;
  BorderBox.Tag:=fWallInformation.Border;
  DestroyedBox.Tag:=fWallInformation.Destroyed;

  DestroySettings.Tag:=fWallInformation.Destroyed;

  NormalPaintBox.Refresh;
  RightPaintBox.Refresh;
  MirrorBox.Refresh;
  BackDoorBox.Refresh;
  BorderBox.Refresh;
  DestroyedBox.Refresh;
end;

procedure TWallSettings.Button4Click(Sender: TObject);
var
  Bitmap: TBitmap;
begin
  if OpenBitmapDialog.Execute then
  begin
    Bitmap:=TBitmap.Create;
    Bitmap.LoadFromFile(OpenBitmapDialog.FileName);

    MainForm.MaskWallLeft(Bitmap);

    MainForm.TileGroup1.ReplaceWallLeft(Bitmap,GetWallIndex);

    MainForm.Refresh;
    Refresh;
    Bitmap.Free;
  end;

end;

procedure TWallSettings.DrawWindowClick(Sender: TObject);
begin
  fWallInformation.DrawWindow:=DrawWindow.Checked;
  ChooseColorButton.Enabled:=DrawWindow.Checked;
end;

procedure TWallSettings.ShowThroughClick(Sender: TObject);
begin
  fWallInformation.ShowThrough:=ShowThrough.Checked;
end;

procedure TWallSettings.ChooseColorButtonClick(Sender: TObject);
begin
  ColorDialog.Color:=fWallInformation.WindowColor;
  if ColorDialog.Execute then
  begin
    fWallInformation.WindowColor:=ColorDialog.Color;
    PanelColor.Color:=ColorDialog.Color;
  end;
end;

function TWallSettings.GetWallInformation(
  Index: Integer): PWallInformation;
begin
  if Index=-1 then
    Index:=GetWallIndex;

  result:=MainForm.TileGroup1.WallInformation(Index);
end;

function TWallSettings.GetWallIndex: Integer;
begin
  result:=fEditIndex;
end;

procedure TWallSettings.WallRightPaint(Sender: TObject);
begin
  TPaintBox(Sender).Canvas.FillRect(TPaintBox(Sender).ClientRect);
  MainForm.TileGroup1.DrawWallRightCanvas(TPaintBox(Sender).Canvas,0,0,TPaintBox(Sender).Tag)
end;

procedure TWallSettings.Button5Click(Sender: TObject);
var
  Bitmap: TBitmap;
begin
  if OpenBitmapDialog.Execute then
  begin
    Bitmap:=TBitmap.Create;
    Bitmap.LoadFromFile(OpenBitmapDialog.FileName);

    MainForm.MaskWallRight(Bitmap);
    MainForm.TileGroup1.ReplaceWallRight(Bitmap,GetWallIndex);

    MainForm.Refresh;
    Refresh;
    Bitmap.Free;
  end;

end;

procedure TWallSettings.BarenessBoxChange(Sender: TObject);
begin
  fWallInformation.Barness:=BarenessBox.ItemIndex;
end;

procedure TWallSettings.ChooseDestroyedClick(Sender: TObject);
begin
  ChooseWall.NoImageButton.Visible:=true;
  if ChooseWall.ShowModal=mrOK then
  begin
    fWallInformation.Destroyed:=ChooseWall.WallListBox.ItemIndex;
    AktuPaintBoxs;
  end;
  ChooseWall.NoImageButton.Visible:=false;
end;

procedure TWallSettings.DestroySettingsClick(Sender: TObject);
var
  Form: TWallSettings;
begin
  if (Sender as TButton).Tag=-1 then
    exit;
    
  Form:=TWallSettings.Create(Self);
  Form.EditIndex:=(Sender as TButton).Tag;
  Form.Left:=Left+20;
  Form.Top:=Top+10;
  Form.ShowModal;
  Form.Free;
end;

procedure TWallSettings.WalkThroughClick(Sender: TObject);
begin
  fWallInformation.WalkThrough:=WalkThrough.Checked;
end;

procedure TWallSettings.IsEckeBoxClick(Sender: TObject);
begin
  fWallInformation.IsEcke:=IsEckeBox.Checked;
end;

procedure TWallSettings.HitPointEditChange(Sender: TObject);
begin
  fWallInformation.HitPoints:=round(HitPointEdit.Value);
end;

end.
