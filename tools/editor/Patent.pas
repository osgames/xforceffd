unit Patent;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtRecord, ComCtrls;

type
  TfrmPatents = class(TForm)
    PatentList: TListView;
  private
    fWachstum  : Integer;
    fSatt      : Integer;
    fErloes    : Integer;
    fWeeks     : Integer;
    { Private-Deklarationen }
  public
    procedure RecalcPatent;
    property Weeks     : Integer read fWeeks write fWeeks;
    property Satt      : Integer read fSatt write fSatt;
    property Erloes    : Integer read fErloes write fErloes;
    property Wachstum  : Integer read fWachstum write fWachstum;

    { Public-Deklarationen }
  end;

var
  frmPatents: TfrmPatents;

implementation

{$R *.DFM}

{ TfrmPatents }

procedure TfrmPatents.RecalcPatent;
var
  Item      : TListItem;
  Dummy     : Integer;
  Summe     : Integer;
  lWachstum : double;
  lErloes   : Integer;
begin
  PatentList.Items.Clear;
  lWachstum:=1+(Wachstum/100);
  Summe:=0;
  lErloes:=Erloes;
  for Dummy:=0 to Satt-2 do
  begin
    inc(Summe,lErloes);
    Item:=PatentList.Items.Add;
    Item.Caption:=IntToStr(Dummy+1);
    Item.SubItems.Add(FormatFloat('##0.## ''%''',(lWachstum-1)*100));
    Item.SubItems.Add(Format('%.0n',[lErloes/1]));
    lErloes:=round(lErloes*lWachstum);
  end;
  for Dummy:=Satt-1 to Weeks-1 do
  begin
    inc(Summe,lErloes);
    Item:=PatentList.Items.Add;
    Item.Caption:=IntToStr(Dummy+1);
    Item.SubItems.Add(FormatFloat('##0.## ''%''',(lWachstum-1)*100));
    Item.SubItems.Add(Format('%.0n',[lErloes/1]));
    lWachstum:=1+(lWachstum-1)*(Weeks-Dummy)/(Weeks-(Satt-2));
    lErloes:=round(lErloes*lWachstum);
  end;
  Item:=PatentList.Items.Add;
  Item.Caption:='Gesamterlös:';
  Item.SubItems.Add('---');
  Item.SubItems.Add(Format('%.0n',[Summe/1]));

end;

end.
