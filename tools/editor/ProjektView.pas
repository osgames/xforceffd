unit ProjektView;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ComCtrls, ExtCtrls, Menus, ToolWin, XForce_types, StdCtrls, contnrs,
  ProjektRecords;

type
  TProjektForm = class(TFrame)
    MenuKontext: TPopupMenu;
    EditProjekt: TMenuItem;
    PopupRename: TMenuItem;
    PopupNewProjekt: TMenuItem;
    PopupAddProjekt: TMenuItem;
    Symbols: TMenuItem;
    AllProjekts: TListView;
    ParentsPanel: TPanel;
    Splitter1: TSplitter;
    ChildPanel: TPanel;
    Splitter2: TSplitter;
    ProjectPanel: TPanel;
    Panel7: TPanel;
    Panel8: TPanel;
    Panel9: TPanel;
    Panel10: TPanel;
    Panel11: TPanel;
    ParentList: TListView;
    Panel13: TPanel;
    ChildsList: TListView;
    N1: TMenuItem;
    Kopieren1: TMenuItem;
    Einfgen1: TMenuItem;
    Ausschneiden1: TMenuItem;
    PopupMenu1: TPopupMenu;
    Vorgngerhinzufgen1: TMenuItem;
    Vorgngerlschen1: TMenuItem;
    Panel2: TPanel;
    Image1: TImage;
    Label1: TLabel;
    N2: TMenuItem;
    N3: TMenuItem;
    Vorgngerhinzufgen2: TMenuItem;
    Vorgngerlschen2: TMenuItem;
    FilterBox: TEdit;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure MenuKontextPopup(Sender: TObject);
    procedure AllProjektsSelectItem(Sender: TObject; Item: TListItem;
      Selected: Boolean);
    procedure ChildsListDblClick(Sender: TObject);
    procedure AllProjektsDblClick(Sender: TObject);
    procedure AllProjektsEdited(Sender: TObject; Item: TListItem;
      var S: String);
    procedure ParentListColumnClick(Sender: TObject; Column: TListColumn);
    procedure AllProjektsCompare(Sender: TObject; Item1, Item2: TListItem;
      Data: Integer; var Compare: Integer);
    procedure FilterBoxChange(Sender: TObject);
  private
    fStackPos    : Integer;
    fNoClear     : boolean;
    fNavigate    : boolean;
    SelectStack  : TList;
    procedure SelectID(ID: Cardinal);
    procedure SymbolsClick(Sender: TObject);
    procedure SymbolsDrawItem(Sender: TObject; ACanvas: TCanvas;
      ARect: TRect; Selected: Boolean);
    procedure SymbolsMeasureItem(Sender: TObject; ACanvas: TCanvas;
      var Width, Height: Integer);

    { Private-Deklarationen }
  public
    constructor Create(AOwner: TComponent); override;
    destructor Destroy;override;
    procedure ClearList;
    procedure CreateTree;
    procedure AddToList(Index: Integer; List: TListView; Select: Boolean = false);
    procedure AddAlienToList(Index: Integer; List: TListView; Select: Boolean = false);
    function CanNext: boolean;
    function CanBack: boolean;
    procedure GoNext;
    procedure GoBack;
    { Public-Deklarationen }
  end;

implementation

uses frmMain,string_utils;

{$R *.DFM}

procedure TProjektForm.CreateTree;
var
  Dummy: Integer;
begin
  AllProjekts.Items.beginUpdate;
  AllProjekts.Items.Clear;
  ParentList.Items.Clear;
  ChildsList.Items.Clear;
  for Dummy:=0 to Length(MDIForm.Projekts)-1 do
  begin
    if (not MDIForm.Projekts[Dummy].GetBoolean('Start')) and (not MDIForm.DeletedChild[Dummy]) then
    begin
      if (FilterBox.Text<>'') then
      begin
        Text:=string_utils_GetLanguageString(MDIForm.Sprache,MDIForm.Projekts[Dummy].GetString('Name'));
        if (Pos(lowercase(FilterBox.Text),LowerCase(Text))=0) then
          continue;
      end;

      AddToList(Dummy,AllProjekts);
    end;
  end;
  AllProjekts.Items.EndUpdate;
end;

procedure TProjektForm.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action:=caFree;
  MDIForm.ProjektViewer:=nil;
end;

procedure TProjektForm.MenuKontextPopup(Sender: TObject);
var
  Dummy    : Integer;
  Item     : TMenuItem;
  Break    : Integer;
  Mods     : Integer;
  Bis      : Integer;
  Row      : Integer;
  Image    : Integer;
  ItemIndex: Integer;
  LItem    : TListItem;
begin
  LItem:=AllProjekts.Selected;
  if LItem<>nil then
    LItem.Selected:=true;
  Symbols.Enabled:=(LItem<>nil);
  if not Symbols.Enabled then exit;
  if not MDIForm.Projekts[Integer(LItem.Data)].ValueExists('ImageIndex') then Symbols.Enabled:=false;
  if not Symbols.Enabled then exit;
  Symbols.Clear;
  for Dummy:=0 to MDIForm.WaffenListe.Count-1 do
  begin
    Item:=TMenuItem.Create(Symbols);
    Item.OnMeasureItem:=SymbolsMeasureItem;
    Item.OnDrawItem:=SymbolsDrawItem;
    Item.Hint:=Symbols.Hint;
    Item.ImageIndex:=Dummy;
    Item.OnClick:=SymbolsClick;
    Symbols.Add(Item);
  end;
  Break:=MDIForm.WaffenListe.Count div 5;
  Mods:=MDIForm.WaffenListe.Count mod 5;
  ItemIndex:=0;
  for Dummy:=0 to 4 do
  begin
    Bis:=Break;
    Image:=Dummy;
    if Dummy<Mods then inc(Bis);
    for Row:=0 to Bis-1 do
    begin
      Symbols[ItemIndex].ImageIndex:=Image;
      inc(Image,5);
      inc(ItemIndex);
    end;
    if ItemIndex<Symbols.Count then
      Symbols[ItemIndex].Break:=mbBreak;
  end;
end;

procedure TProjektForm.SymbolsMeasureItem(Sender: TObject; ACanvas: TCanvas;
  var Width, Height: Integer);
begin
  Width:=24;
  Height:=36;
end;

procedure TProjektForm.SymbolsDrawItem(Sender: TObject; ACanvas: TCanvas;
  ARect: TRect; Selected: Boolean);
begin
  ARect:=Rect(ARect.Left,ARect.Top,ARect.Left+36,ARect.Top+36);
  ACanvas.Brush.Color:=clBtnFace;
  if (Sender as TMenuItem).ImageIndex=MDIForm.Projekts[Integer(AllProjekts.Selected.Data)].GetInteger('ImageIndex') then
  begin
    if not Selected then
      ACanvas.Brush.Bitmap := AllocPatternBitmap(clBtnFace, clBtnHighlight);
    ACanvas.FillRect(ARect);
    MDIForm.WaffenListe.Draw(ACanvas,ARect.Left+2,Arect.Top+2,(Sender as TMenuItem).ImageIndex);
    Frame3D(ACanvas,ARect,clBtnShadow,clBtnHighlight,1);
  end
  else
  begin
    ACanvas.FillRect(ARect);
    MDIForm.WaffenListe.Draw(ACanvas,ARect.Left+2,Arect.Top+2,(Sender as TMenuItem).ImageIndex);
    if Selected then Frame3D(ACanvas,ARect,clBtnHighlight,clBtnShadow,1)
  end;
end;

procedure TProjektForm.SymbolsClick(Sender: TObject);
begin
  if MDIForm.Projekts[Integer(AllProjekts.Selected.Data)].GetInteger('ImageIndex')<>(Sender as TMenuItem).ImageIndex then
  begin
    MDIForm.Projekts[Integer(AllProjekts.Selected.Data)].SetInteger('ImageIndex',(Sender as TMenuItem).ImageIndex);
    AllProjekts.Selected.ImageIndex:=(Sender as TMenuItem).ImageIndex;
    MDIForm.ForschChanged:=true;
  end;
end;

procedure TProjektForm.AddToList(Index: Integer; List: TListView; Select: Boolean);
var
  Item: TListItem;
begin
  if MDIForm.DeletedChild[Index] then
    exit;

  Item:=List.Items.Add;
  with MDIForm.Projekts[Index] do
  begin
    Item.Caption:=string_utils_GetLanguageString(MDIFOrm.Sprache,GetString('Name'));
    Item.Data:=Pointer(Index);
    if ValueExists('ImageIndex') then
      Item.ImageIndex:=GetInteger('ImageIndex')
    else
      Item.ImageIndex:=-1;
    if ValueExists('AlienItem') and GetBoolean('AlienItem') then
      Item.SubItems.Add('Alien - '+RecordDefinition.RecordName)
    else
      Item.SubItems.Add(RecordDefinition.RecordName);

    if GetInteger('Ver')=0 then
      Item.SubItems.Add('Anfang')
    else
      Item.SubItems.Add(IntToStr(GetInteger('Ver')));

    Item.SubItems.Add('Item');
    if Select then
    begin
      Item.Selected:=true;
      AllProjektsSelectItem(AllProjekts,Item,true);
    end;
  end;
end;

procedure TProjektForm.AllProjektsSelectItem(Sender: TObject;
  Item: TListItem; Selected: Boolean);
var
  Dummy  : Integer;
  Dummy2 : Integer;
  ItemID : Cardinal;
  Parents: TParentArray;
  Found  : Boolean;
begin
  if (Selected) then
  begin
    ParentList.Items.Clear;
    ChildsList.Items.Clear;
    // Vorgänger ermitteln
     with MDIForm.Projekts[Integer(Item.Data)] do
    begin
      if not fNavigate then
      begin
        if not fNoClear then
          ClearList
        else if fStackPos<>SelectStack.Count-1 then
          SelectStack.Count:=fStackPos+1;
        SelectStack.Add(Pointer(GetCardinal('ID')));
        fStackPos:=SelectStack.Count-1;
      end;
      GetParentArray(MDIForm.Projekts[Integer(Item.Data)],Parents);
      for Dummy:=0 to high(Parents) do
      begin
        ItemID:=Parents[Dummy];
        Found:=false;
        // Als Ausrüstung suchen
        for Dummy2:=0 to high(MDIForm.Projekts) do
        begin
          if MDIForm.Projekts[Dummy2].GetCardinal('ID')=ItemID then
          begin
            AddToList(Dummy2,ParentList);
            Found:=true;
            break;
          end;
        end;
        // Als Alien suchen
        if not Found then
        begin
          for Dummy2:=0 to high(MDIForm.Aliens) do
          begin
            if MDIForm.Aliens[Dummy2].GetCardinal('ID')=ItemID then
            begin
              AddAlienToList(Dummy2,ParentList);
              break;
            end;
	  end;
	end;
      end;
    end;

    // Nachfolger ermitteln
    ItemID:=MDIForm.Projekts[Integer(Item.Data)].GetCardinal('ID');
    for Dummy:=0 to High(MDIForm.Projekts) do
    begin
      if not MDIForm.DeletedChild[Dummy] then
      begin
        GetParentArray(MDIForm.Projekts[Dummy],Parents);
        for Dummy2:=0 to High(Parents) do
        begin
          if Parents[Dummy2]=ItemID then
            AddToList(Dummy,ChildsList);
        end;
      end;
    end;
  end
  else
  begin
    ChildsList.Items.Clear;
    ParentList.Items.Clear;
  end;
end;

procedure TProjektForm.ChildsListDblClick(Sender: TObject);
var
  View   : TListView;
  ItemID : Cardinal;
begin
  View:=(Sender as TListView);
  if View.Selected<>nil then
  begin
    if View.Selected.SubItems[2]='Item' then
    begin
      ItemID:=MDIForm.Projekts[Integer(View.Selected.Data)].GetCardinal('ID');
      SelectID(ItemID);
    end;
  end;
end;

procedure TProjektForm.AllProjektsDblClick(Sender: TObject);
begin
  MDIForm.ActionProjektEdit.Execute;
end;

procedure TProjektForm.AllProjektsEdited(Sender: TObject; Item: TListItem;
  var S: String);
var
  Dummy: Integer;
  Index: Integer;
  Text : String;
  ID   : Cardinal;
begin
  Index:=Integer(Item.Data);
  ID:=MDIForm.Projekts[Index].GetCardinal('ID');
  for Dummy:=0 to length(MDIForm.Projekts)-1 do
  begin
    if (MDIForm.Projekts[Dummy].GetString('Name')=S) and (MDIForm.Projekts[Dummy].GetCardinal('ID')<>ID) then
    begin
      Text:=Format('%s ist bereits definiert',[S]);
      Application.MessageBox(PChar(Text),PChar('Hinweis'),MB_ICONINFORMATION or MB_OK);
      S:=MDIForm.Projekts[Index].GetString('Name');
      exit;
    end;
  end;
  MDIForm.Projekts[Index].SetString('Name',string_utils_SetLanguageString(MDIForm.Sprache,MDIForm.Projekts[Index].GetString('Name'),S));
  MDIForm.ForschChanged:=true;
end;

constructor TProjektForm.Create(AOwner: TComponent);
begin
  inherited;
  SelectStack:=TList.Create;
  fNoClear:=false;
  fNavigate:=false;
end;

procedure TProjektForm.ClearList;
begin
  SelectStack.Clear;
  fStackPos:=-1;
end;

function TProjektForm.CanBack: boolean;
begin
  result:=fStackPos>0;
end;

function TProjektForm.CanNext: boolean;
begin
  result:=(fStackPos<SelectStack.Count-1);
end;

procedure TProjektForm.GoBack;
begin
  fNavigate:=true;
  dec(fStackPos);
  SelectID(Cardinal(SelectStack[fStackPos]));
  fNavigate:=false;
end;

procedure TProjektForm.GoNext;
begin
  fNavigate:=true;
  inc(fStackPos);
  SelectID(Cardinal(SelectStack[fStackPos]));
  fNavigate:=false;
end;

procedure TProjektForm.SelectID(ID: Cardinal);
var
  Dummy: Integer;
begin
  for Dummy:=0 to AllProjekts.Items.Count-1 do
  begin
    if MDIForm.Projekts[Integer(AllProjekts.Items[Dummy].Data)].GetCardinal('ID')=ID then
    begin
      fNoClear:=true;
      AllProjekts.Items[Dummy].Selected:=true;
//      AllProjektsSelectItem(AllProjekts,AllProjekts.Items[Dummy],true);
      fNoClear:=false;
      exit;
    end;
  end;
end;

procedure TProjektForm.AddAlienToList(Index: Integer; List: TListView;
  Select: Boolean);
var
  Item: TListItem;
begin
  Item:=List.Items.Add;
  with MDIForm.Aliens[Index] do
  begin
    Item.Caption:=string_utils_GetLanguageString(MDIForm.Sprache,GetString('Name'))+' - Autopsie';
    Item.Data:=Pointer(Index);
    Item.ImageIndex:=-1;
    Item.StateIndex:=-1;
    Item.SubItems.Add('Alien - Autopsie');
    
    if GetInteger('Ver')=0 then
      Item.SubItems.Add('Anfang')
    else
      Item.SubItems.Add(IntToStr(GetInteger('Ver')));

    Item.SubItems.Add('Alien');
    if Select then
    begin
      Item.Selected:=true;
      AllProjektsSelectItem(AllProjekts,Item,true);
    end;
  end;
end;

procedure TProjektForm.ParentListColumnClick(Sender: TObject;
  Column: TListColumn);
begin
  TListView(Sender).Tag:=Column.Index;
  TListView(Sender).CustomSort(nil,0);
end;

procedure TProjektForm.AllProjektsCompare(Sender: TObject; Item1,
  Item2: TListItem; Data: Integer; var Compare: Integer);

  function GetDays(Item: TListItem): Integer;
  begin
    if Item.SubItems[1]='Anfang' then
      result:=0
    else
      result:=StrToInt(Item.SubItems[1]);
  end;
begin
  if TListView(Sender).Tag=0 then
  begin
    Compare:=CompareText(Item1.Caption,Item2.Caption);
  end
  else if TListView(Sender).Tag=1 then
  begin
    if (Item1.SubItems.Count>0) and (Item2.SubItems.Count>0) then
      Compare:=CompareText(Item1.SubItems[0],Item2.SubItems[0])
    else
      Compare:=0;
  end
  else if TListView(Sender).Tag=2 then
  begin
    Compare:=GetDays(Item1)-GetDays(Item2);
  end;
end;

destructor TProjektForm.Destroy;
begin
  SelectStack.Free;

  inherited;
end;

procedure TProjektForm.FilterBoxChange(Sender: TObject);
begin
  CreateTree;
end;

end.
