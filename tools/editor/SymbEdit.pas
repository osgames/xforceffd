unit SymbEdit;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, ImgList, Buttons, ExtDlgs, ExtendImageList, XForce_types;

type
  TfrmSymbEdit = class(TForm)
    Panel1: TPanel;
    ScrollBox1: TScrollBox;
    PaintBox1: TPaintBox;
    OKButton: TSpeedButton;
    CancelButton: TSpeedButton;
    AddButton: TSpeedButton;
    OpenDialog: TOpenPictureDialog;
    EditButton: TSpeedButton;
    DeleteButton: TSpeedButton;
    procedure FormShow(Sender: TObject);
    procedure PaintBox1Paint(Sender: TObject);
    procedure PaintBox1MouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure OKButtonClick(Sender: TObject);
    procedure CancelButtonClick(Sender: TObject);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure AddButtonClick(Sender: TObject);
    procedure EditButtonClick(Sender: TObject);
    procedure DeleteButtonClick(Sender: TObject);
  private
    fImageIndex  : Integer;
    fResult      : Integer;
    fSymbList    : TExtendImageList;
    function OverlapRect(const Rect1, Rect2: TRect): Boolean;
    procedure SetImageIndex(const Value: Integer);
    { Private-Deklarationen }
  public
    property ImageIndex : Integer read fResult write SetImageIndex;

    property ImageList: TExtendImageList read fSymbList write fSymbList;
    { Public-Deklarationen }
  end;

var
  frmSymbEdit: TfrmSymbEdit;

implementation

uses KD4Utils, frmMain;

{$R *.DFM}

procedure TfrmSymbEdit.FormShow(Sender: TObject);
begin
  Assert(fSymbList<>nil);
  PaintBox1.Width:=(fSymbList.Count*35);
  EditButton.Enabled:=fImageIndex>=fSymbList.BasisImages;
//  DeleteButton.Enabled:=fImageIndex>=fSymbList.BasisImages;
end;

function TfrmSymbEdit.OverlapRect(const Rect1, Rect2: TRect): Boolean;
begin
  Result := (Rect1.Left < Rect2.Right) and
            (Rect1.Right > Rect2.Left) and
            (Rect1.Top < Rect2.Bottom) and
            (Rect1.Bottom > Rect2.Top);
end;

procedure TfrmSymbEdit.PaintBox1Paint(Sender: TObject);
var
  Dummy : Integer;
  XPos  : Integer;
  FRect : TRect;
  Bitmap: TBitmap;
begin
  XPos:=1;
  Bitmap:=TBitmap.Create;
  Bitmap.Height:=32;
  Bitmap.Transparent:=false;
  Bitmap.Width:=32;
  with fSymbList do
  begin
    for Dummy:=0 to fSymbList.Count-1 do
    begin
      FRect:=Rect(Xpos-1,0,XPos+33,34);
      if OverlapRect(FRect,PaintBox1.Canvas.ClipRect) then
      begin
        if Dummy=fImageIndex then
          Frame3D(PaintBox1.Canvas,FRect,clBtnShadow,clBtnHighlight,1)
        else
          Frame3D(PaintBox1.Canvas,FRect,clBtnHighlight,clBtnShadow,1);
        GetBitmap(Dummy,Bitmap);
        Bitmap.TransparentColor:=Bitmap.Canvas.Pixels[0,0];
        Bitmap.Transparent:=false;
        Bitmap.Transparent:=true;
        PaintBox1.Canvas.Draw(XPos,1,Bitmap);
      end;
      inc(XPos,35);
    end;
  end;
  Bitmap.Free;
end;

procedure TfrmSymbEdit.PaintBox1MouseDown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
var
  New   : Integer;
  FRect : TRect;
begin
  if (Button=mbLeft) then
  begin
    New:=X div 35;
    if fImageIndex<>New then
    begin
      FRect:=Rect((New*35),0,(New*35)+34,34);
      Frame3D(PaintBox1.Canvas,FRect,clBtnShadow,clBtnHighlight,1);
      FRect:=Rect((fImageIndex*35),0,(fImageIndex*35)+34,34);
      Frame3D(PaintBox1.Canvas,FRect,clBtnHighlight,clBtnShadow,1);
      fImageIndex:=New;
    end;
    EditButton.Enabled:=fImageIndex>=fSymbList.BasisImages;
//    DeleteButton.Enabled:=fImageIndex>=fSymbList.BasisImages;
  end;
end;

procedure TfrmSymbEdit.OKButtonClick(Sender: TObject);
begin
  ModalResult:=mrOK;
  fResult:=fImageIndex;
end;

procedure TfrmSymbEdit.CancelButtonClick(Sender: TObject);
begin
  ModalResult:=mrCancel;
end;

procedure TfrmSymbEdit.FormKeyPress(Sender: TObject; var Key: Char);
begin
  if Key=#13 then OKButton.Click
  else if Key=#27 then CancelButton.Click;
end;

procedure TfrmSymbEdit.AddButtonClick(Sender: TObject);
var
  Bitmap: TBitmap;
  Dummy : Integer;
begin
  OpenDialog.Title:='Bilder hinzufügen ... ';
  OpenDialog.Options:=OpenDialog.Options+[ofAllowMultiSelect];
  if OpenDialog.Execute then
  begin
    Bitmap:=TBitmap.Create;
    for Dummy:=0 to OpenDialog.Files.Count-1 do
    begin
      try
        Bitmap.LoadFromFile(OpenDialog.Files[Dummy]);
        fSymbList.AddBitmap(Bitmap);
        PaintBox1.Width:=PaintBox1.Width+35;
//        MDIForm.ForschChanged:=true;
      except
        on E:Exception do
        begin
          Application.MessageBox(PChar('Bei der Datei '+OpenDialog.Files[Dummy]+' trat folgender Fehler auf:'#13#10+E.Message),nil,MB_OK or MB_ICONERROR);
        end;
      end;
    end;
    Bitmap.Free;
  end;
end;

procedure TfrmSymbEdit.EditButtonClick(Sender: TObject);
var
  Bitmap: TBitmap;
begin
  OpenDialog.Title:='Bild ersetzen ... ';
  OpenDialog.Options:=OpenDialog.Options-[ofAllowMultiSelect];
  if OpenDialog.Execute then
  begin
    Bitmap:=TBitmap.Create;
    try
      Bitmap.LoadFromFile(OpenDialog.FileName);
      fSymbList.ReplaceBitmap(Bitmap,fImageIndex);
      PaintBox1.Invalidate;
//      MDIForm.ForschChanged:=true;
    except
      on E:Exception do
      begin
        Application.MessageBox(PChar('Beim ersetzen des Bildes trat folgender Fehler auf:'#13#10+E.Message),nil,MB_OK or MB_ICONERROR);
      end;
    end;
    Bitmap.Free;
  end;
end;

procedure TfrmSymbEdit.DeleteButtonClick(Sender: TObject);
var
  Dummy: Integer;
begin
  with MDIForm do
  begin
    WaffenListe.Delete(fImageIndex);
    ForschChanged:=true;
    for Dummy:=0 to length(Projekts)-1 do
    begin
      with Projekts[Dummy] do
      begin
        if ValueExists('ImageIndex') then
        begin
          if GetInteger('ImageIndex')=fImageIndex then
            SetInteger('ImageIndex',TypeIDToIndex(TProjektType(Projekts[Dummy].GetInteger('TypeID')),TWaffenType(Projekts[Dummy].GetInteger('WaffType'))))
          else if GetInteger('ImageIndex')>fImageIndex then
            SetInteger('ImageIndex',GetInteger('ImageIndex')-1);
        end;
      end;
    end;
  end;
  if fImageIndex>fSymbList.Count-1 then dec(fImageIndex);
  if fResult>fSymbList.Count-1 then dec(fResult);
  PaintBox1.Width:=PaintBox1.Width-35;
  EditButton.Enabled:=fImageIndex>=fSymbList.BasisImages;
//  DeleteButton.Enabled:=fImageIndex>=fSymbList.BasisImages;
end;

procedure TfrmSymbEdit.SetImageIndex(const Value: Integer);
begin
  fResult := Value;
  fImageIndex:=Value;
end;

end.
