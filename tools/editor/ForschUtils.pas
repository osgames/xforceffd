unit ForschUtils;

interface

uses XForce_types, EditConst, SysUtils, Forms, ExtRecord;

type
  TFormclass = class of TForm;

procedure ChangeProjektTyp(Projekt: TExtRecord;TypeID: TProjektType);
function SameProjekt(Projekt1: TForschProject;Projekt2: TForschProject): boolean;
function SameUFOs(UFO1: TUFOModel; UFO2: TUFOModel):boolean;
function SameAliens(Alien1: TAlien; Alien2: TAlien):boolean;
procedure CheckValue(Zahl,min,max: Integer);overload;
procedure CheckValue(Zahl,max: Integer);overload;
procedure CheckValue(Str: String;Zahl: double; Max: double);overload;
procedure CheckPercent(Zahl: Integer);
procedure CheckFileName(Str: String);
procedure CheckClass(var Ref: TForm; ClassRef: TFormClass);

implementation

uses KD4Utils;

procedure ChangeProjektTyp(Projekt: TExtRecord;TypeID: TProjektType);
var
  OldImage : Integer;
  Image    : Integer;
  Typ      : TWaffenType;
begin
  if not Projekt.ValueExists('ImageIndex') then
    exit;
  if Projekt.ValueExists('WaffType') then
    Typ:=TWaffenType(Projekt.GetInteger('WaffType'))
  else
    Typ:=wtProjektil;
  OldImage:=TypeIDToIndex(TProjektType(Projekt.GetInteger('TypeID')),Typ);
  Image:=Projekt.GetInteger('ImageIndex');
  if (Image=-1) or (OldImage=Image) then
  begin
    Projekt.SetInteger('ImageIndex',TypeIDToIndex(TProjektType(Projekt.GetInteger('TypeID')),Typ));
  end;
//  Projekt.Schuss:=[];
//  if (TypeID=ptMunition) and (Projekt.WaffType=wtLaser) then Projekt.WaffType:=wtProjektil;
{  if TypeID=ptWaffe then Projekt.Schuss:=[stGezielt,stAuto,stSpontan];
  if TypeID=ptGranate then
  begin
    Projekt.Schuss:=[stWerfen];
    Projekt.Munition:=1;
  end;
  if TypeID in [ptMine,ptSensor] then Projekt.Schuss:=[stBenutzen];
  if not (TypeID in [ptMunition,ptRMunition]) then Projekt.Munfor:=0;
  Projekt.TypeID:=TypeID;}
end;

function SameProjekt(Projekt1: TForschProject;Projekt2: TForschProject): boolean;
var
  Dummy: Integer;
begin
  result:=Projekt1.Name=Projekt2.Name;
  if not result then exit;
  result:=result and (Projekt1.ID=Projekt2.ID);
  result:=result and (Projekt1.Stunden=Projekt2.Stunden);
  result:=result and (Projekt1.NextID1=Projekt2.NextID1);
  result:=result and (Projekt1.NextID2=Projekt2.NextID2);
  result:=result and (Projekt1.NextID3=Projekt2.NextID3);
  result:=result and (Projekt1.LagerV=Projekt2.LagerV);
  result:=result and (Projekt1.Gewicht=Projekt2.Gewicht);
  result:=result and (Projekt1.KaufPreis=Projekt2.KaufPreis);
  result:=result and (Projekt1.VerKaufPreis=Projekt2.VerKaufPreis);
  if not result then exit;
  result:=result and (Projekt1.Munition=Projekt2.Munition);
  result:=result and (Projekt1.TypeID=Projekt2.TypeID);
  result:=result and (Projekt1.WaffType=Projekt2.WaffType);
  result:=result and (Projekt1.Strength=Projekt2.Strength);
  result:=result and (Projekt1.Schuss=Projekt2.Schuss);
  result:=result and (Projekt1.Munfor=Projekt2.Munfor);
  result:=result and (Projekt1.SmallSchiff=Projekt2.SmallSchiff);
  result:=result and (Projekt1.LargeSchiff=Projekt2.LargeSchiff);
  result:=result and (Projekt1.LagerRaum=Projekt2.LagerRaum);
  result:=result and (Projekt1.Quartiere=Projekt2.Quartiere);
  result:=result and (Projekt1.WerkRaum=Projekt2.WerkRaum);
  result:=result and (Projekt1.Abwehr=Projekt2.Abwehr);
  result:=result and (Projekt1.LaborRaum=Projekt2.LaborRaum);
  if not result then exit;
  result:=result and (Projekt1.ManuAlphatron=Projekt2.ManuAlphatron);
  result:=result and (Projekt1.BauZeit=Projekt2.BauZeit);
  result:=result and (Projekt1.Panzerung=Projekt2.Panzerung);
  result:=result and (Projekt1.WaffenZellen=Projekt2.WaffenZellen);
  result:=result and (Projekt1.HitPoints=Projekt2.HitPoints);
  result:=result and (Projekt1.LagerPlatz=Projekt2.LagerPlatz);
  result:=result and (Projekt1.Soldaten=Projekt2.Soldaten);
  result:=result and (Projekt1.PixPerSec=Projekt2.PixPerSec);
  result:=result and (Projekt1.Laden=Projekt2.Laden);
  result:=result and (Projekt1.Verfolgung=Projekt2.Verfolgung);
  result:=result and (Projekt1.Start=Projekt2.Start);
  if not result then exit;
  result:=result and (Projekt1.Anzahl=Projekt2.Anzahl);
  result:=result and (Projekt1.ImageIndex=Projekt2.ImageIndex);
  result:=result and (Projekt1.Info=Projekt2.Info);
  result:=result and (Projekt1.Land=Projekt2.Land);
  result:=result and (Projekt1.Herstellbar=Projekt2.Herstellbar);
  result:=result and (Projekt1.Ver=Projekt2.Ver);
  result:=result and (Projekt1.Reichweite=Projekt2.Reichweite);
  result:=result and (Projekt1.Einhand=Projekt2.Einhand);
  result:=result and (Projekt1.ExtendSlots=Projekt2.ExtendSlots);
  result:=result and (Projekt1.SensorWeite=Projekt2.SensorWeite);
  if not result then exit;
  result:=result and (Projekt1.TimeUnits=Projekt2.TimeUnits);
  result:=result and (Projekt1.Power=Projekt2.Power);
  if Projekt1.TypeID=ptExtension then
  begin
    result:=result and (Projekt1.ExtCount=Projekt1.ExtCount);
    if not result then exit;
    for Dummy:=0 to Projekt1.ExtCount-1 do
    begin
      result:=result and (Projekt1.Extensions[Dummy].Prop=Projekt2.Extensions[Dummy].Prop);
      result:=result and (Projekt1.Extensions[Dummy].Value=Projekt2.Extensions[Dummy].Value);
      if not result then exit;
    end;
  end;
end;

function SameUFOs(UFO1: TUFOModel; UFO2: TUFOModel):boolean;
begin
  result:=UFO1.Name=UFO2.Name;
  result:=result and (UFO1.ID=UFO2.ID);
  result:=result and (UFO1.HitPoints=UFO2.HitPoints);
  result:=result and (UFO1.Shield=UFO2.Shield);
  result:=result and (UFO1.ShieldType.Projektil=UFO2.ShieldType.Projektil);
  result:=result and (UFO1.ShieldType.Rakete=UFO2.ShieldType.Rakete);
  result:=result and (UFO1.ShieldType.Laser=UFO2.ShieldType.Laser);
  result:=result and (UFO1.ShieldType.Laden=UFO2.ShieldType.Laden);
  result:=result and (UFO1.MinBesatz=UFO2.MinBesatz);
  result:=result and (UFO1.ZusBesatz=UFO2.ZusBesatz);
  result:=result and (UFO1.Angriff=UFO2.Angriff);
  result:=result and (UFO1.Pps=UFO2.Pps);
  result:=result and (UFO1.Ver=UFO2.Ver);
  result:=result and (UFO1.Treffsicherheit=UFO2.Treffsicherheit);
  result:=result and (UFO1.Info=UFO2.Info);
end;

function SameAliens(Alien1: TAlien; Alien2: TAlien):boolean;
begin
  result:=Alien1.Name=Alien2.Name;
  result:=result and (Alien1.ID=Alien2.ID);
  result:=result and (Alien1.Ges=Alien2.Ges);
  result:=result and (Alien1.Pan=Alien2.Pan);
  result:=result and (Alien1.Waf=Alien2.Waf);
  result:=result and (Alien1.PSAn=Alien2.PSAn);
  result:=result and (Alien1.PSAb=Alien2.PSAb);
  result:=result and (Alien1.Zei=Alien2.Zei);
  result:=result and (Alien1.Ver=Alien2.Ver);
  result:=result and (Alien1.Info=Alien2.Info);
  result:=result and (Alien1.Power=Alien2.Power);
  result:=result and (Alien1.Treff=Alien2.Treff);
end;

procedure CheckValue(Zahl,min,max: Integer);
begin
  if Zahl<min then raise Exception.Create(Format(MValueToSmall,[min/1]));
  if Zahl>max then raise Exception.Create(Format(MValueToLarge,[max/1]));
end;

procedure CheckValue(Str: String;Zahl: double; Max: double);
var
  Komma: Integer;
begin
  Komma:=Pos(',',Str);
  if Komma<>0 then
  begin
    while Str[length(str)]='0' do Delete(Str,length(Str),1);
    if (Komma<Length(Str)-1) then raise Exception.Create(MValueInvLength);
  end;
  if Zahl<=0 then raise Exception.Create(MValueNotNull);
  if Zahl>Max then raise Exception.Create(Format(MValueToLarge,[max/1]));
end;

procedure CheckPercent(Zahl: Integer);
begin
  if Zahl<0 then raise Exception.Create(MPercentToSmall);
  if Zahl>100 then raise Exception.Create(MPercentToLarge);
end;

procedure CheckValue(Zahl,max: Integer);overload;
begin
  if Zahl<=0 then raise Exception.Create(MValueNotNull);
  if Zahl>Max then raise Exception.Create(Format(MValueToLarge,[max/1]));
end;

procedure CheckFileName(Str: String);
var
  Dummy: Integer;
  Ch   : Char;
begin
  if length(Str)=0 then
    raise Exception.Create('Kein Dateiname angegeben');
  for Dummy:=1 to length(Str) do
  begin
    Ch:=Str[Dummy];
    if (Ch in ['/','\',':','*','?','"','<','>','|','.']) then
      raise Exception.Create('Der Dateiname darf keines der folgenden Zeichen enthalten:'#13#10+
                             '/ \ : * ? " < > | .');
  end;
end;

procedure CheckClass(var Ref: TForm; ClassRef: TFormClass);
begin
  if Ref=nil then Application.CreateForm(ClassRef,ref);
end;

end.
