unit frmListDialog;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls;

type
  TListDialog = class(TForm)
    OKButton: TButton;
    Button2: TButton;
    ListItems: TListBox;
    procedure ListItemsDrawItem(Control: TWinControl; Index: Integer;
      Rect: TRect; State: TOwnerDrawState);
    procedure OKButtonClick(Sender: TObject);
    procedure ListItemsDblClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    fIndex: Integer;
    procedure SetIndex(const Value: Integer);
    { Private-Deklarationen }
  public

    property SelectedIndex: Integer read fIndex write SetIndex;
    { Public-Deklarationen }
  end;

var
  ListDialog: TListDialog;

implementation

{$R *.DFM}

procedure TListDialog.ListItemsDrawItem(Control: TWinControl;
  Index: Integer; Rect: TRect; State: TOwnerDrawState);
var
  ComboBox: TListBox;
begin
  ComboBox:=Control as TListBox;
  ComboBox.Canvas.FillRect(Rect);
  ComboBox.Canvas.TextOut(Rect.Left+3,Rect.Top+3,ComboBox.Items[Index]);
//  if ComboBox.Items.Objects[Index]<>nil then
//    ComboBox.Canvas.Draw(Rect.Left+2,Rect.Top+3,(ComboBox.Items.Objects[Index] as TBitmap));
end;

procedure TListDialog.OKButtonClick(Sender: TObject);
begin
  if ListItems.ItemIndex=-1 then
    ModalResult:=mrNone;
  fIndex:=ListItems.ItemIndex;
end;

procedure TListDialog.ListItemsDblClick(Sender: TObject);
begin
  OKButton.Click;
end;

procedure TListDialog.SetIndex(const Value: Integer);
begin
  fIndex := Value;
  ListItems.ItemIndex:=fIndex;
end;

procedure TListDialog.FormShow(Sender: TObject);
begin
  if @ListItems.OnDrawItem=nil then
  begin
    ListItems.ItemHeight:=20;
    ListItems.OnDrawItem:=ListItemsDrawItem;
  end;
end;

end.
