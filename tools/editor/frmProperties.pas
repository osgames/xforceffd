unit frmProperties;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, Mask, ExtCtrls, ExtendEdit;

type
  TFileProperties = class(TForm)
    SetnameEdit: TExtendEdit;
    FirstPass: TExtendEdit;
    SecondPass: TExtendEdit;
    OKButton: TBitBtn;
    CancelButton: TBitBtn;
    OrganEdit: TExtendEdit;
    AutorEdit: TExtendEdit;
    Import: TExtendCheckBox;
    Panel1: TPanel;
    Memo1: TMemo;
    procedure FormShow(Sender: TObject);
    procedure OKButtonClick(Sender: TObject);
    procedure CancelButtonClick(Sender: TObject);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure FormCreate(Sender: TObject);
  private
    { Private-Deklarationen }
  public
    { Public-Deklarationen }
  end;

var
  FileProperties: TFileProperties;

implementation

{$R *.DFM}

procedure TFileProperties.FormShow(Sender: TObject);
begin
  SetNameEdit.SetFocus;
end;

procedure TFileProperties.OKButtonClick(Sender: TObject);
begin
  if SetnameEdit.Text='' then
  begin
    Application.MessageBox(PChar('Bitte geben Sie einen Namen f�r den Spielsatz ein.'),PChar('Hinweis'),MB_OK or MB_ICONINFORMATION);
    SetNameEdit.SetFocus;
    ModalResult:=mrNone;
    exit;
  end;
  if FirstPass.Text<>SecondPass.Text then
  begin
    Application.MessageBox(PChar('Passw�rter stimmen nicht �berein. Bitte geben Sie in beiden Felder das gleiche Passwort ein.'),PChar('Hinweis'),MB_OK or MB_ICONINFORMATION);
    FirstPass.SetFocus;
    FirstPass.Text:='';
    SecondPass.Text:='';
    ModalResult:=mrNone;
    exit;
  end;
  ModalResult:=mrOK;
end;

procedure TFileProperties.CancelButtonClick(Sender: TObject);
begin
  ModalResult:=mrCancel;
end;

procedure TFileProperties.FormKeyPress(Sender: TObject; var Key: Char);
begin
  if (Key=#13) and (not Memo1.Focused) then OKButton.Click
  else if Key=#27 then CancelButton.Click;
end;

procedure TFileProperties.FormCreate(Sender: TObject);
begin
  FirstPass.DeactiveReset;
  SecondPass.DeactiveReset;
end;

end.
