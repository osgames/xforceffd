program Edit;

uses
  MemCheck,
  Windows,
  Messages,
  dialogs,
  Forms,
  frmMain in 'frmMain.pas' {MDIForm},
  ProjektView in 'ProjektView.pas' {ProjektForm: TFrame},
  StartView in 'StartView.pas' {StartForm: TFrame},
  RecycleForm in 'RecycleForm.pas' {RecycledForm: TFrame},
  frmSave in 'frmSave.pas' {SaveDialog},
  ForschUtils in 'ForschUtils.pas',
  frmAbout in 'frmAbout.pas' {AboutBox},
  frmAlien in 'frmAlien.pas' {AlienForm: TFrame},
  frmUFO in 'frmUFO.pas' {UFOForm: TFrame},
  Defines in '..\..\game\source\Defines.pas',
  LoadSet in 'LoadSet.pas' {LoadGameSet},
  frmPasswort in 'frmPasswort.pas' {PasswordDlg},
  frmProperties in 'frmProperties.pas' {FileProperties},
  NewSet in 'NewSet.pas' {frmNew},
  EditConst in 'EditConst.pas',
  frmErrorAssist in 'frmErrorAssist.pas' {FehlerAssistent},
  SymbEdit in 'SymbEdit.pas' {frmSymbEdit},
  SelectProjekt in 'SelectProjekt.pas' {frmSelectList},
  frmEditProjekt in 'frmEditProjekt.pas' {EditProjektForm},
  frmListDialog in 'frmListDialog.pas' {ListDialog},
  frmPaedieImage in 'frmPaedieImage.pas' {PaedieImage},
  AlienItemsView in 'AlienItemsView.pas' {AlienItemsForm: TFrame},
  Patent in 'Patent.pas' {frmPatents},
  frmLanguages in 'frmLanguages.pas' {LanguageForm},
  frmNameEdit in 'frmNameEdit.pas' {NameEdit},
  frmEditLanguages in 'frmEditLanguages.pas' {EditLanguages},
  frmAliensInUFO in 'frmAliensInUFO.pas' {AliensInUFO},
  frmIntelligence in 'frmIntelligence.pas' {IntelligenzForm},
  ProjektRecords in '..\..\game\source\Units\ProjektRecords.pas',
  frmInfo in 'frmInfo.pas' {InfoEdit},
  ChangeTyp in 'ChangeTyp.pas' {TypForm},
  ObjektRecords in '..\..\game\source\Units\ObjektRecords.pas',
  MissionSkriptViews in 'MissionSkriptViews.pas' {MissionSkripts: TFrame},
  SkriptEdit in 'SkriptEdit.pas' {frmSkriptEditor},
  MissionTriggers in 'MissionTriggers.pas' {frmMissionSkriptTriggers},
  frmSoundEdit in 'frmSoundEdit.pas' {SoundEdit},
  ObjektView in 'ObjektView.pas' {ObjektForm: TFrame},
  {$I language.inc};

{$R *.RES}

var
  SH_SEM: HWND;
  SH_Ins: boolean;
  SH_Wind: HWND;
begin
  SH_SEM:=CreateSemaphore(nil,0,1,'KD4 Editor');
  SH_Ins:=(SH_Sem<>0) and (GetLastError=ERROR_ALREADY_EXISTS);
  if SH_Ins then
  begin
    Closehandle(SH_Sem);
    SH_Wind:=FindWindow(nil,'Editor - '+LGameName);
    if SH_Wind<>0 then
    begin
      PostMessage(SH_Wind,WM_SYSCOMMAND,SC_RESTORE,0);
      SetForegroundWindow(SH_Wind);
      exit;
    end;
  end;           
  Application.Initialize;
  Application.Title := 'Editor';
  Application.HelpFile := '';
  Application.CreateForm(TMDIForm, MDIForm);
  Application.CreateForm(TSaveDialog, SaveDialog);
  Application.CreateForm(TAboutBox, AboutBox);
  Application.CreateForm(TLoadGameSet, LoadGameSet);
  Application.CreateForm(TPasswordDlg, PasswordDlg);
  Application.CreateForm(TFileProperties, FileProperties);
  Application.CreateForm(TfrmNew, frmNew);
  Application.CreateForm(TfrmSelectList, frmSelectList);
  Application.CreateForm(TfrmSymbEdit, frmSymbEdit);
  Application.CreateForm(TListDialog, ListDialog);
  Application.CreateForm(TPaedieImage, PaedieImage);
  Application.CreateForm(TfrmPatents, frmPatents);
  Application.CreateForm(TLanguageForm, LanguageForm);
  Application.CreateForm(TNameEdit, NameEdit);
  Application.CreateForm(TEditLanguages, EditLanguages);
  Application.CreateForm(TAliensInUFO, AliensInUFO);
  Application.CreateForm(TIntelligenzForm, IntelligenzForm);
  Application.CreateForm(TInfoEdit, InfoEdit);
  Application.CreateForm(TTypForm, TypForm);
  Application.CreateForm(TfrmSkriptEditor, frmSkriptEditor);
  Application.CreateForm(TfrmMissionSkriptTriggers, frmMissionSkriptTriggers);
  Application.CreateForm(TSoundEdit, SoundEdit);
  Application.Run;
end.
