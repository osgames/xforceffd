object MDIForm: TMDIForm
  Left = 354
  Top = 382
  Width = 858
  Height = 567
  HorzScrollBar.Tracking = True
  HorzScrollBar.Visible = False
  VertScrollBar.Tracking = True
  Color = clBtnFace
  Constraints.MinHeight = 300
  Constraints.MinWidth = 450
  DefaultMonitor = dmDesktop
  DockSite = True
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poMainFormCenter
  WindowState = wsMaximized
  OnCloseQuery = FormCloseQuery
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 519
    Width = 850
    Height = 19
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 0
    object StatusBar: TStatusBar
      Left = 0
      Top = 0
      Width = 850
      Height = 19
      Align = alClient
      Panels = <
        item
          Bevel = pbRaised
          Width = 550
        end
        item
          Bevel = pbRaised
          Width = 113
        end>
    end
  end
  object FramePanel: TPanel
    Left = 9
    Top = 50
    Width = 832
    Height = 460
    Align = alClient
    BevelOuter = bvNone
    ParentBackground = False
    TabOrder = 1
  end
  object TBDock1: TTBXDock
    Left = 0
    Top = 0
    Width = 850
    Height = 50
    BoundLines = [blBottom]
    object ProjektBar: TTBXToolbar
      Tag = 1
      Left = 432
      Top = 23
      Caption = 'Projekte'
      CloseButton = False
      DockPos = 324
      DragHandleStyle = dhDouble
      Images = ImageList1
      ParentShowHint = False
      ShowHint = True
      TabOrder = 0
      Visible = False
      object TBItem13: TTBXItem
        Action = ActionProjektNew
      end
      object TBItem11: TTBXItem
        Action = ActionProjektDelete
      end
      object TBItem10: TTBXItem
        Action = ActionProjektEdit
      end
      object TBSeparatorItem3: TTBXSeparatorItem
      end
      object TBItem28: TTBXItem
        Action = ActionProjektCut
      end
      object TBItem24: TTBXItem
        Action = ActionProjektCopy
      end
      object TBItem12: TTBXItem
        Action = ActionProjektPaste
      end
      object TBSeparatorItem5: TTBXSeparatorItem
      end
      object TBItem31: TTBXItem
        Action = ActionProjektAddFather
      end
      object TBItem32: TTBXItem
        Action = ActionProjektDeleteFather
      end
      object TBSeparatorItem6: TTBXSeparatorItem
      end
      object TBItem30: TTBXItem
        Action = ActionProjektBack
      end
      object TBItem29: TTBXItem
        Action = ActionProjektNext
      end
    end
    object ItemToolBar: TTBXToolbar
      Tag = 2
      Left = 276
      Top = 23
      Caption = 'Ausr'#252'stung'
      CloseButton = False
      DockPos = 301
      DragHandleStyle = dhDouble
      Images = ImageList1
      ParentShowHint = False
      ShowHint = True
      TabOrder = 1
      Visible = False
      object TBItem16: TTBXItem
        Action = ActionItemAdd
      end
      object TBItem15: TTBXItem
        Action = ActionItemDelete
      end
      object TBItem14: TTBXItem
        Action = ActionItemEdit
      end
      object TBSeparatorItem4: TTBXSeparatorItem
      end
      object TBItem27: TTBXItem
        Action = ActionItemCut
      end
      object TBItem26: TTBXItem
        Action = ActionItemCopy
      end
      object TBItem25: TTBXItem
        Action = ActionItemPaste
      end
    end
    object UFOBar: TTBXToolbar
      Left = 228
      Top = 23
      Caption = 'UFOs'
      CloseButton = False
      DockPos = 301
      DragHandleStyle = dhDouble
      Images = ImageList1
      ParentShowHint = False
      ShowHint = True
      TabOrder = 2
      Visible = False
      object TBItem19: TTBXItem
        Action = ActionUFOAdd
      end
      object TBItem18: TTBXItem
        Action = ActionUFODelete
      end
      object TBItem17: TTBXItem
        Action = ActionUFOEdit
      end
      object TBSeparatorItem11: TTBXSeparatorItem
      end
      object TBItem49: TTBXItem
        Action = ActionUFOCut
      end
      object TBItem48: TTBXItem
        Action = ActionUFOCopy
      end
      object TBItem47: TTBXItem
        Action = ActionUFOCopy
      end
    end
    object AlienBar: TTBXToolbar
      Tag = 3
      Left = 693
      Top = 23
      Cursor = crArrow
      Caption = 'Aliens'
      DockPos = 324
      DragHandleStyle = dhDouble
      Images = ImageList1
      ParentShowHint = False
      ShowHint = True
      TabOrder = 3
      Visible = False
      object TBItem22: TTBXItem
        Action = ActionAlienAdd
      end
      object TBItem21: TTBXItem
        Action = ActionAlienDelete
      end
      object TBItem20: TTBXItem
        Action = ActionAlienEdit
      end
      object TBSeparatorItem10: TTBXSeparatorItem
      end
      object TBItem46: TTBXItem
        Action = ActionAlienCut
      end
      object TBItem44: TTBXItem
        Action = ActionAlienCopy
      end
      object TBItem45: TTBXItem
        Action = ActionAlienPaste
      end
    end
    object DefaultBar: TTBXToolbar
      Left = 0
      Top = 23
      Caption = 'Standard'
      CloseButton = False
      DockPos = 0
      DragHandleStyle = dhDouble
      Images = ImageList1
      Options = [tboToolbarStyle]
      ParentShowHint = False
      ShowHint = True
      TabOrder = 4
      object TBItem4: TTBXItem
        Action = ActionFileNew
      end
      object TBItem3: TTBXItem
        Action = ActionFileOpen
      end
      object TBItem2: TTBXItem
        Action = ActionFileSave
      end
      object TBItem1: TTBXItem
        Action = ActionFileClose
      end
      object TBSeparatorItem1: TTBXSeparatorItem
      end
      object TBItem34: TTBXItem
        Action = ActionFileCheckGameSet
      end
      object TBItem5: TTBXItem
        Action = ActionFileProperties
      end
      object TBSeparatorItem2: TTBXSeparatorItem
      end
      object TBItem6: TTBXItem
        Action = ActionViewProjektList
      end
      object TBItem23: TTBXItem
        Action = ActionViewStartList
      end
      object TBItem7: TTBXItem
        Action = ActionViewUFOList
      end
      object TBItem8: TTBXItem
        Action = ActionViewAlienList
      end
      object TBItem9: TTBXItem
        Action = ActionViewAlienItems
      end
      object TBItem33: TTBXItem
        Action = ActionViewSkripts
      end
      object TBItem56: TTBXItem
        Action = ActionViewObjects
      end
      object TBXSeparatorItem1: TTBXSeparatorItem
      end
      object TBXItem1: TTBXItem
        Action = ActionViewAsHTML
      end
    end
    object MenuBar: TTBXToolbar
      Left = 0
      Top = 0
      Caption = 'Men'#252
      CloseButton = False
      DragHandleStyle = dhDouble
      FullSize = True
      Images = ImageList1
      MenuBar = True
      ProcessShortCuts = True
      ShrinkMode = tbsmWrap
      TabOrder = 5
      object MenuFile: TTBXSubmenuItem
        Caption = '&Datei'
        object NeuerSpielsatz1: TTBXItem
          Action = ActionFileNew
        end
        object ffnen1: TTBXItem
          Action = ActionFileOpen
        end
        object Importieren1: TTBXItem
          Action = ActionFileImport
        end
        object Speichern1: TTBXItem
          Action = ActionFileSave
        end
        object Schliessen1: TTBXItem
          Action = ActionFileClose
        end
        object N12: TTBXSeparatorItem
        end
        object TBItem35: TTBXItem
          Action = ActionFileCheckGameSet
        end
        object Sprachen1: TTBXItem
          Action = ActionFileLanguages
        end
        object Eigenschaften1: TTBXItem
          Action = ActionFileProperties
        end
        object N1: TTBXSeparatorItem
        end
        object MenuExit: TTBXItem
          Action = ActionFileExit
        end
      end
      object Ansicht1: TTBXSubmenuItem
        Caption = '&Ansicht'
        object Projektliste1: TTBXItem
          Action = ActionViewProjektList
        end
        object Ausrstung1: TTBXItem
          Action = ActionViewStartList
        end
        object UFOs1: TTBXItem
          Action = ActionViewUFOList
        end
        object Aliens1: TTBXItem
          Action = ActionViewAlienList
        end
        object Alienausrstung1: TTBXItem
          Action = ActionViewAlienItems
        end
        object Skripts: TTBXItem
          Action = ActionViewSkripts
        end
        object TBXItem2: TTBXItem
          Action = ActionViewObjects
        end
      end
      object MenuProjekt: TTBXSubmenuItem
        Caption = '&Projekt'
        Enabled = False
        object Projektbearbeiten2: TTBXItem
          Action = ActionProjektEdit
        end
        object Umbennen3: TTBXItem
          Action = ActionProjektRename
        end
        object N3: TTBXSeparatorItem
        end
        object Ausschneiden1: TTBXItem
          Action = ActionProjektCut
        end
        object Kopieren1: TTBXItem
          Action = ActionProjektCopy
        end
        object Einfgen1: TTBXItem
          Action = ActionProjektPaste
        end
        object N4: TTBXSeparatorItem
        end
        object Projekthinzufgen1: TTBXItem
          Action = ActionProjektNew
        end
        object Projektlschen2: TTBXItem
          Action = ActionProjektDelete
        end
        object TBSeparatorItem9: TTBXSeparatorItem
        end
        object TBItem43: TTBXItem
          Action = ActionProjektAddFather
        end
        object TBItem42: TTBXItem
          Action = ActionProjektDeleteFather
        end
      end
      object MenuStart: TTBXSubmenuItem
        Caption = '&Ausr'#252'stung'
        Enabled = False
        object Ausrstungbearbeiten1: TTBXItem
          Action = ActionItemEdit
        end
        object Umbenennen1: TTBXItem
          Action = ActionItemRename
        end
        object N2: TTBXSeparatorItem
        end
        object Ausrstunghinzufgen1: TTBXItem
          Action = ActionItemAdd
        end
        object Ausrstunglschen1: TTBXItem
          Action = ActionItemDelete
        end
        object N7: TTBXSeparatorItem
        end
        object Ausschneiden2: TTBXItem
          Action = ActionItemCut
        end
        object Kopieren2: TTBXItem
          Action = ActionItemCopy
        end
        object Einfgen2: TTBXItem
          Action = ActionItemPaste
        end
        object N6: TTBXSeparatorItem
        end
        object Sortierennach1: TTBXSubmenuItem
          Caption = 'Sortieren nach'
          Hint = 'Sortieren der Liste'
          object MenuSName: TTBXItem
            Caption = '&Name'
            Checked = True
            Hint = 'Nach Namen sortieren'
            OnClick = SortClick
          end
          object MenuSTyp: TTBXItem
            Tag = 1
            Caption = '&Typ'
            Hint = 'Nach Typ sortieren'
            OnClick = SortClick
          end
          object MenuSAnzahl: TTBXItem
            Tag = 2
            Caption = '&Anzahl'
            Hint = 'Nach Anzahl sortieren'
            OnClick = SortClick
          end
          object MenuSVer: TTBXItem
            Tag = 3
            Caption = '&Verf'#252'gbar nach'
            Hint = 'Nach Verf'#252'gbar nach sortieren'
            OnClick = SortClick
          end
        end
      end
      object MenuUFO: TTBXSubmenuItem
        Caption = 'UFO'
        Enabled = False
        object MenuUFOEdit: TTBXItem
          Action = ActionUFOEdit
        end
        object Umbenennen2: TTBXItem
          Action = ActionUFORename
        end
        object N10: TTBXSeparatorItem
        end
        object UFObearbeiten1: TTBXItem
          Action = ActionUFOAdd
        end
        object UFOlschen1: TTBXItem
          Action = ActionUFODelete
        end
        object N11: TTBXSeparatorItem
        end
        object TBItem41: TTBXItem
          Action = ActionUFOCut
        end
        object TBItem40: TTBXItem
          Action = ActionUFOCopy
        end
        object TBItem36: TTBXItem
          Action = ActionUFOPaste
        end
        object TBSeparatorItem8: TTBXSeparatorItem
        end
        object SortUFO: TTBXSubmenuItem
          Caption = 'Sortieren nach'
          object Name1: TTBXItem
            Caption = 'Name'
            Checked = True
            OnClick = SortUFOClick
          end
          object Hitpoints1: TTBXItem
            Tag = 1
            Caption = 'Hitpoints'
            OnClick = SortUFOClick
          end
          object Schildpunkte1: TTBXItem
            Tag = 2
            Caption = 'Schildpunkte'
            OnClick = SortUFOClick
          end
          object Angriffsstrke1: TTBXItem
            Tag = 3
            Caption = 'Angriffsst'#228'rke'
            OnClick = SortUFOClick
          end
          object minBesatzung1: TTBXItem
            Tag = 4
            Caption = 'min. Besatzung'
            OnClick = SortUFOClick
          end
          object zusBesatzung1: TTBXItem
            Tag = 5
            Caption = 'zus. Besatzung'
            OnClick = SortUFOClick
          end
          object Angriffnach1: TTBXItem
            Tag = 6
            Caption = 'Angriff nach'
            OnClick = SortUFOClick
          end
          object Gesamtstrke1: TTBXItem
            Tag = 7
            Caption = 'Gesamtst'#228'rke'
            OnClick = SortUFOClick
          end
        end
      end
      object MenuAlien: TTBXSubmenuItem
        Caption = 'Alien'
        Enabled = False
        object Alienbearbeiten1: TTBXItem
          Action = ActionAlienEdit
        end
        object Umbennen2: TTBXItem
          Action = ActionAlienRename
        end
        object N8: TTBXSeparatorItem
        end
        object MenuAlienAdd: TTBXItem
          Action = ActionAlienAdd
        end
        object MenuAliendelete: TTBXItem
          Action = ActionAlienDelete
        end
        object N15: TTBXSeparatorItem
        end
        object TBItem39: TTBXItem
          Action = ActionAlienCut
        end
        object TBItem38: TTBXItem
          Action = ActionAlienCopy
        end
        object TBItem37: TTBXItem
          Action = ActionAlienPaste
        end
        object TBSeparatorItem7: TTBXSeparatorItem
        end
        object SortAlien: TTBXSubmenuItem
          Caption = 'Sortieren nach'
          object Name2: TTBXItem
            Caption = 'Name'
            Checked = True
            OnClick = SortAlienClick
          end
          object Gesundheit1: TTBXItem
            Tag = 1
            Caption = 'Gesundheit'
            OnClick = SortAlienClick
          end
          object Panzerung1: TTBXItem
            Tag = 2
            Caption = 'Panzerung'
            OnClick = SortAlienClick
          end
          object Strke1: TTBXItem
            Tag = 3
            Caption = 'St'#228'rke'
            OnClick = SortAlienClick
          end
          object PSIAngriff1: TTBXItem
            Tag = 4
            Caption = 'PSI-Angriff'
            OnClick = SortAlienClick
          end
          object PSIAbweh1: TTBXItem
            Tag = 5
            Caption = 'PSI-Abwehr'
            OnClick = SortAlienClick
          end
          object Zeiteinheiten1: TTBXItem
            Tag = 6
            Caption = 'Zeiteinheiten'
            OnClick = SortAlienClick
          end
          object Angriffnach2: TTBXItem
            Tag = 7
            Caption = 'Angriff nach'
            OnClick = SortAlienClick
          end
          object Gesamtstrke2: TTBXItem
            Tag = 8
            Caption = 'Gesamtst'#228'rke'
            OnClick = SortAlienClick
          end
        end
      end
      object MenuSkript: TTBXSubmenuItem
        Caption = 'Skript'
        Enabled = False
        object TBItem57: TTBXItem
          Action = ActionSkriptEdit
        end
        object TBItem58: TTBXItem
          Action = ActionSkriptRename
        end
        object TBSeparatorItem13: TTBXSeparatorItem
        end
        object TBItem59: TTBXItem
          Action = ActionSkriptAdd
        end
        object TBItem60: TTBXItem
          Action = ActionSkriptDelete
        end
        object TBSeparatorItem14: TTBXSeparatorItem
        end
        object TBItem63: TTBXItem
          Action = ActionSkriptCut
        end
        object TBItem62: TTBXItem
          Action = ActionSkriptCopy
        end
        object TBItem61: TTBXItem
          Action = ActionSkriptPaste
        end
      end
      object Hilfe1: TTBXSubmenuItem
        Caption = '&Hilfe'
        object About: TTBXItem
          Caption = '&'#220'ber ...'
          OnClick = AboutClick
        end
      end
    end
    object SkriptBar: TTBXToolbar
      Tag = 3
      Left = 324
      Top = 23
      Cursor = crArrow
      Caption = 'Skripte'
      DockPos = 324
      DragHandleStyle = dhDouble
      Images = ImageList1
      ParentShowHint = False
      ShowHint = True
      TabOrder = 6
      Visible = False
      object TBItem50: TTBXItem
        Action = ActionSkriptAdd
      end
      object TBItem51: TTBXItem
        Action = ActionSkriptDelete
      end
      object TBItem52: TTBXItem
        Action = ActionSkriptEdit
      end
      object TBSeparatorItem12: TTBXSeparatorItem
      end
      object TBItem53: TTBXItem
        Action = ActionSkriptCut
      end
      object TBItem54: TTBXItem
        Action = ActionSkriptCopy
      end
      object TBItem55: TTBXItem
        Action = ActionSkriptPaste
      end
    end
  end
  object TBDock2: TTBXDock
    Left = 0
    Top = 510
    Width = 850
    Height = 9
    Position = dpBottom
  end
  object TBDock3: TTBXDock
    Left = 0
    Top = 50
    Width = 9
    Height = 460
    Position = dpLeft
  end
  object TBDock4: TTBXDock
    Left = 841
    Top = 50
    Width = 9
    Height = 460
    Position = dpRight
  end
  object ImageList1: TImageList
    Left = 40
    Top = 72
    Bitmap = {
      494C010124002700040010001000FFFFFFFF0510FFFFFFFFFFFFFFFF424D7600
      000000000000760000002800000040000000A000000001000400000000000014
      0000000000000000000000000000000000000000000000008000008000000080
      8000800000008000800080800000C0C0C000808080000000FF0000FF000000FF
      FF00FF000000FF00FF00FFFF0000FFFFFF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000F87FF778F000000000000000
      00000000000000000000000800000000000000007F70004887F0000000000000
      000000000000000000000008777777777770000F8F800488F870000000888888
      888800000000000000000008FFFFFFFFFF700007770048FFFF8F0000008F22FF
      FFF80000088888800000888888888888888800F8F800677FFF870000008F2A2F
      FFF800000888888000008FFFFFFFFFFFFFF80078F80878777787222222222AA2
      FFF800000008800000008F0F0F0F000F00F8008FF7078777778F02AAAAAAAAAA
      2FF800000008800000008F000F0F000F0FF80F8FF7877F77778F028888888228
      2FF800000888800000008F0F00000F0F0FF8077FF7887FF776F0222222222282
      FFF800000888800000008FFFFFFFFFFFFFF8F8FFFFF877F74F000000008F282F
      FFF80000000000000000888888888888888878FFFFFFF77F70000000008F22FF
      888800000008800000000008FFFFFFFFFF708FFFFF7FFFF7F0000000008FFFF7
      8FF800000088880000000008FFFFFFFFFF708FFFF77F7FF70000000000877778
      FF8000000088880000000008FFFFFFFF0000F777FFFF7F7F0000000000888888
      880000000008800000000008FFFFFFFF8F00000F777FFF700000000000000000
      000000000000000000000008FFFFFFFF8000000000F787F00000000000000000
      0000000000000000000000088888888880000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000700000000000000000000000000000000
      0000000000000000000000000088807770000000000000000000000000000000
      0000000000000000000000000888877777000000000000000000000000000000
      0000000000000000000000008808077F77000000000002222200000000000222
      220000000000000000000008888887F7F7000000000002222200000000000222
      2200000000000000000000088888887770000000000000000000000000000000
      0000000000000000000000088008008700000000000000000000000000000000
      0000000000000000000000088008008800000000000000000000000000000000
      0000000000000000000000080088800800000000000CC0000000000000000000
      0000000000000000000000088888888800000011111CC0000000001111100000
      00000000000000000000000888888888000000111CCCCCC00000001119999990
      00000000000000000000000088888880000000000CCCCCC00000000009999990
      0000000000000000000000008888888000000000000CC0000000000000000000
      0000000000000000000000000888880000000000000CC0000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000044000000000000000000000
      0000000000000000000000000000000000000000400400440000000000000000
      0000000000000000000000000000000000000000400404004000000000000000
      0000000000000000000000000000000000000000400404004000000000000000
      0000000000000000000000000000000000000000044404004000000000000000
      00000000000000E0000000000E00000000000000000404440000000000000000
      00000000000000EE00000000EE00000000000000000404000000000000000000
      0000000EEEEEEEEEE000000EEEEEEEEEE0000000000000000000000000000000
      0000000EEEEEEEEEE000000EEEEEEEEEE0000000000000000000000000000000
      00000000000000EE00000000EE00000000000000000000000000000000000000
      00000000000000E0000000000E00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000077700000CC00000077700000
      0000000000000000000000000044444444440077CCC77000CC000077CCC77000
      000000000004444444440000004FFFFFFFF4007C777C70CCCCCC007C777C7011
      111100000004FFFFFFF40838384F000000F407CCCCCCC7CCCCCC07CCCCCCC711
      111100000004F00000F40383834FFFFFFFF407CECCCCC700CC0007CECCCCC700
      000000000004FFFFFFF40838384F000F444407CEECCCC700CC0007CEECCCC700
      000000FFFFF4F00000F40383834FFFFF4F40007C444C70000000007C444C7000
      000000F00004FFFFFFF40838384FFFFF44000077FFF7700000000077FFF77000
      000000FFFFF4F00F444403838344444440000007FFF7000000000007FFF70000
      000000F00004FFFF4F40083838383838300000007F700000000000007F700000
      000000FFFFF4FFFF4400038000000008800000007F700000000000007F700000
      000000F00F0444444000088000000008300000007F700000000000007F700000
      000000FFFF0F0000000003830B00B083800000007F700000000000007F700000
      000000FFFF0000000000000000BB000000000007777700000000000777770000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000CC000000000000000000000004F000000000000000000000
      0000000000000000CC0000000000000000000000444F0000000000000CCCCCC0
      00000000000000CCCCCC00000000001111110004444F000000000000CCFFFFCC
      00000000000000CCCCCC000000000011111100444F44F0000000000CCCCFFCCC
      C000000000000000CC0000000000000000000444F0044F00000000CCCCCFFCCC
      CC00000000000000CC000000000000000000004F00004F00000000CCCCCFFCCC
      CC00000000000000000000000000000000000000000004F0000000CCCCCFFCCC
      CC000000000000000000000000000000000000000000004F000000CCCCFFFFCC
      CC0000000000000000000000000000000000000000000004F00000CCCCCCCCCC
      CC00000000000000000000000000000000000000000000004F0000CCCCCFFCCC
      CC000000000000000000000000000000000000000000000004F0000CCCCFFCCC
      C0000000000000000000000000000000000000000000000000400000CCCCCCCC
      000000000000000000000000000000000000000000000000000000000CCCCCC0
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000008F80000000000000000000000000000000
      0000000000000000000000000000F88F88F00000008880000000000000000000
      00000000099999900000800000008F8F8F800000088888000000000000000000
      000000009999999900000000000888FFF8880000880808800000000000000000
      000000099999999990000000000FFFFFFFFF0008888888880000000000000000
      000000999F9999F999000000000888FFF8880008888888880000000000000000
      0000009999F99F999900000000008F8F8F800008800800880000000000000000
      00000099999FF999990000000000F88F88F00008800800880000000588855585
      10000099999FF99999000FFFFFF0008F80000008008880080000000877778885
      5500009999F99F9999000F0000F0000000000008888888880000000000000550
      000000999F9999F999000FFFFFF0000000000008888888880000000000005000
      000000099999999990000F0000F0000000000000888888800000000000000000
      000000009999999900000FFFFFF0000000000000888888800000000000000000
      000000000999999000000F0F0000000000000000088888000000000000000000
      000000000000000000000FFF0F00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000009900000000000000000000000
      0000000000088888000008777777777770000009900000000000000100000000
      0010000008877788000008F7F7F7F7F770000999999000000000001110000000
      0000008887277788800008FF7F44444F70000999999000000000001111000000
      0100008772228788800008F7F7F7F7F770000009900000000000000111000000
      1000008727272788880008FF4444444F70000009900000000000000011100001
      1000087727777778880008F7F7F7F7F770000000000000000000000001110011
      0000087222722778880008FF7F44444F70000000000C00000000000000111110
      0000087727782778888008F7F7F7F7F770000000000CC0000000000000011100
      0000877772270000888008FF7F44444F70000CCCCCCCCCC00099000000111110
      0000877777008777088008F7F7F7F7F770000CCCCCCCCCC00099000001110011
      0000877000887777008008FF44444F0000000000000CC0000000000111100001
      1000800880877700880008F7F7F7F77F80000000000C00000000001111000000
      1100887780770088000008FFFFFFFF7800000000000000000000001110000000
      0010008878008800000008888888888000000000000000000000000000000000
      0000000088880000000000000000000000000000000000000000000000000000
      000000000000000000000000000000F707F70000000000000000000000000000
      0000000000777000000000000000000F0F0000000000000000000FFFFFFFFFF0
      0000000077CCC7700000444400000FFF444409999990000000000F00F00000F0
      000000007C777C7000000004C4000FFF400009999990000000000FFFFFFFFFF0
      00000007CCCCCCC7000000044C40FFFF400000000000000000000F00F00000F0
      00000007CECCCCC700000004C4C0FFFF400000000000000000000FFFFFFFFFF0
      00000007CEECCCC7000000044C40FEFE400000000000000000000FFFFFFF0FF0
      000000007C444C7000000004C4C0EFEF40000000C000000000000F00FFF070F0
      0000000077FFF770000000044C40FEFE4000000CC000000000000F070F070700
      0044000007FFF70000000004C4C0EFEF40000CCCCCCCCCC099000FF070707077
      70440000007F7000000000044444444440000CCCCCCCCCC09900000007070777
      77440000007F700000000000000000000000000CC00000000000000000707777
      77440000007F7000000000000000000000000000C00000000000000000077777
      70440000007F7000000000000022220000000000000000000000000000000000
      0044000007777700000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000099000000000000000000000000000000000000000
      000000330000007703000009900000000000000FFFFFFFFF0000003333333330
      000000330000007703000999999000000000000FFFFFFFFF00000B0333333333
      000000330000007703000999999000000000000FFFFFFFFF00000FB033333333
      300000330000000003000009900000000000000FFFFFFFFF00000BFB03333333
      330000333333333333000009900000000000000FFFFFFFFF00000FBFB0000000
      000000330000000033000000000000000000000FFFFFFFFF00000BFBFBFBFB00
      000000307777777703000000000C00000000000FFFFFFFFF00000FBFBFBFBF00
      000000307777777703000000000CC0000000000FFFFFFFFF00000BFB00000000
      000000307777777703000CCCCCCCCCC09900000FFFFFF0000000000000000000
      000000307777777703000CCCCCCCCCC09900000FFFFFF0F00000000000000000
      000000307777777700000000000CC0000000000FFFFFF0000000000000000000
      000000307777777707000000000C000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000424D3E000000000000003E000000
      2800000040000000A00000000100010000000000000500000000000000000000
      000000000000000000000000FFFFFF0000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000F007FFFFFFFFE000F001FFFFFFFFE000
      E001FC00FFFFE000E000FC00F81F0000C000FC00F81F0000C0000000FE7F0000
      C0008000FE7F000080008000F87F000080010000F87F00000003FC00FFFF0000
      0007FC00FE7FE0000007FC00FC3FE000000FFC01FC3FE000000FFC03FE7FE001
      E01FFFFFFFFFE003FC1FFFFFFFFFE007FFFFFFFFFFFFFFFFFFFFFFFFFFFFFC6F
      FFFFFFFFFFFFF807FFFFFFFFFFFFF003FF01FF01FFFFE003F001F001FFFFC003
      F701F701FFFFC007F701F701FFFFC007F7FFF7FFFFFFC007F7FFF7FFFFFFC007
      807F80FFFFFFC007807F80FFFFFFC007801F801FFFFFE00F801F801FFFFFE00F
      FE7FFFFFFFFFF01FFE7FFFFFFFFFF83FFFFFFEBFFFFFFFFFF9FFFC9FFFFFFFFF
      F6CFF88FFFFFFFFFF6B7F087FFBFFDFFF6B7E083FF9FF9FFF8B7E083FF8FF1FF
      FE8FE083C007E003FE3FE083C003C003FF7FE143C003C003FE3FE223C007E003
      FEBFE413FF8FF1FFFC9FE80BFF9FF9FFFDDFF007FFBFFDFFFDDFF80FFFFFFFFF
      FDDFFC1FFFFFFFFFFFFFFE3FFFFFFFFFF1FFF1FFFFFFFFFFC073C07FFFFFFC00
      8033803FFE00800080008000FE00000000000000FE0000000013001F80000000
      0013001F80000001803F803F80000003803F803F80000003C07FC07F80010003
      E0FFE0FF80030003E0FFE0FF80070FC3E0FFE0FF807F0003E0FFE0FF80FF8007
      C07FC07F81FFF87FE0FFE0FFFFFFFFFFFFFFFFFFFEB3FEBFF9FFF81FFC93FC9F
      F0FFF00FF880F880E0FFE007F080F080C07FC003E083E083863F8001E083E083
      CF3F8001E083E083FF9F8001E083E083FFCF8001E143E143FFE78001E223E223
      A9C38001E413E413AAB9C003E80BE80B89BDE007F007F007AABFF00FF80FF80F
      D9CFF81FFC1FFC1FFFFFFFFFFE3FFE3FFFFFFFFFFFFFFFC7FC7FFFFFF81FF701
      F83FFFFFF00F0301F01FFFFFE0070200E00FE667C0037600C007F24F80017E00
      C007F81F8001FF01C007E00780010001C0078003800100C7C0078001800100FF
      C007E007800100FFC007F81FC00300FFE00FFE7FE00700FFE00FFFFFF00F00FF
      F01FFFFFF81F01FFF83FFFFFFFFF03FFFFFFFFFFFE0F8003E7FCEFFDF8078003
      E7FCC7FDE007800381FFC3FBC003800381FCC3F3C0038003E7FCE1E7C0018003
      E7FFF0C780018003FFFCF80F80018003FEFCFC1F80008003FE7FFC1F00F08003
      801CF80F03088003801CE0C71C0C8003FE7FC1E360338007FEFCC3F100CF800F
      FFFCC7FDC33F801FFFFFFFFFF0FFFFFFFFFFFFFFFC7FFFC8FFF8000FF01FFFEB
      FFF8000FE00F038081FF000FE00FE08781FC000FC007E007FFFC000FC007E007
      FFFF000FC007E007FFFC000FE00FE007F7FC000FE00FE007E7FF0004F01FE007
      80130000F83FE00780130000F83FFFFFE7FFF800F83FF81FF7F8FC00F83FF81F
      FFF8FE04F01FF81FFFFFFFFFF83FFFFFFFFFFFFFFFFFFFFFFFFFFFFFC001E7F8
      C007001F8001E7F8C007000F800181FFC0070007800181FCC00700038001E7FC
      C00700018001E7FFC00700008001FFFCC007001F8001FEFCC007001F8001FE7F
      C007001F80018013C0078FF180018013C00FFFF98001FE7FC01FFF758001FEF8
      C03FFF8F8001FFF8FFFFFFFFFFFFFFFF00000000000000000000000000000000
      000000000000}
  end
  object ActionList1: TActionList
    Images = ImageList1
    OnUpdate = ActionList1Update
    Left = 8
    Top = 72
    object ActionFileOpen: TAction
      Category = 'Datei'
      Caption = #214'&ffnen'
      Hint = #214'ffnen ... |L'#228'dt die Forschungsdatei neu ein'
      ImageIndex = 1
      ShortCut = 16463
      OnExecute = ActionFileOpenExecute
    end
    object ActionAlienEdit: TAction
      Category = 'Alien'
      Caption = 'Alien bearbeiten'
      Hint = 'Alien bearbeiten ... |Bearbeitet den ausgew'#228'hlten Alien'
      ImageIndex = 5
      ShortCut = 122
      SecondaryShortCuts.Strings = (
        'Enter')
      OnExecute = ActionAlienEditExecute
      OnUpdate = ActionAlienEditUpdate
    end
    object ActionProjektNew: TAction
      Category = 'Projekt'
      Caption = 'Projekt hinzuf'#252'gen'
      Hint = 
        'Startprojekt einf'#252'gen ... |F'#252'gt ein Projekt in der ersten Ebene ' +
        'ein'
      ImageIndex = 20
      ShortCut = 114
      OnExecute = ActionProjektNewExecute
    end
    object ActionFileSave: TAction
      Category = 'Datei'
      Caption = '&Speichern'
      Enabled = False
      Hint = 'Speichern|Speichert die aktuellen '#196'nderungen'
      ImageIndex = 2
      ShortCut = 16467
      OnExecute = ActionFileSaveExecute
      OnUpdate = CanSave
    end
    object ActionFileExit: TAction
      Category = 'Datei'
      Caption = '&Beenden'
      Hint = 'Beenden|Beendet den Forschungseditor'
      ImageIndex = 7
      ShortCut = 32883
      OnExecute = ActionFileExitExecute
    end
    object ActionViewProjektList: TAction
      Tag = 5
      Category = 'Ansicht'
      Caption = '&Projekte'
      Enabled = False
      Hint = 
        'Forschungsprojekte anzeigen|Zeigt eine Liste mit den Forschungsp' +
        'rojekten an'
      ImageIndex = 6
      ShortCut = 16454
      OnExecute = ActionViewFrame
    end
    object ActionProjektRename: TAction
      Category = 'Projekt'
      Caption = '&Umbennen'
      Hint = #196'ndert den Namen des gew'#228'hlten Projektes'
      ShortCut = 113
      OnExecute = ActionProjektRenameExecute
      OnUpdate = ProjektsEnabled
    end
    object ActionProjektDelete: TAction
      Category = 'Projekt'
      Caption = '&Projekt l'#246'schen'
      Hint = 
        'Projekt l'#246'schen|L'#246'scht das gew'#228'hlte und alle Untergeordneten Pro' +
        'jekte'
      ImageIndex = 21
      ShortCut = 8238
      OnExecute = ActionProjektDeleteExecute
      OnUpdate = ProjektsEnabled
    end
    object ActionProjektDeleteFather: TAction
      Category = 'Projekt'
      Caption = 'Vorg'#228'nger l'#246'schen'
      Hint = 'L'#246'scht den ausgew'#228'hlten Vorg'#228'nger'
      ImageIndex = 29
      OnExecute = ActionProjektDeleteFatherExecute
      OnUpdate = ActionProjektDeleteFatherUpdate
    end
    object ActionItemRename: TAction
      Category = 'Ausr'#252'stung'
      Caption = '&Umbenennen'
      Hint = #196'ndert den Namen der gew'#228'hlten Ausr'#252'stung'
      ShortCut = 113
      OnExecute = ActionItemRenameExecute
      OnUpdate = StartEnabled
    end
    object ActionItemAdd: TAction
      Category = 'Ausr'#252'stung'
      Caption = '&Ausr'#252'stung hinzuf'#252'gen'
      Hint = 'Neue Ausr'#252'stung ... |F'#252'gt eine neue Ausr'#252'stung hinzu'
      ImageIndex = 18
      ShortCut = 8237
      OnExecute = ActionSkriptEditUpd
    end
    object ActionItemDelete: TAction
      Category = 'Ausr'#252'stung'
      Caption = '&Ausr'#252'stung l'#246'schen'
      Hint = 'Ausr'#252'stung l'#246'schen|L'#246'scht die gew'#228'hlten Ausr'#252'stung'
      ImageIndex = 19
      ShortCut = 8238
      OnExecute = ActionItemDeleteExecute
      OnUpdate = StartEnabled
    end
    object ActionViewRecycleBin: TAction
      Category = 'Ansicht'
      Caption = '&Papierkorb'
      Enabled = False
      Hint = 'Papierkorb anzeigen|Zeigt gel'#246'schte Objekte an'
      ImageIndex = 10
      ShortCut = 16466
    end
    object ActionViewAlienList: TAction
      Tag = 2
      Category = 'Ansicht'
      Caption = '&Aliens'
      Enabled = False
      Hint = 'Aliens anzeigen|Zeigt eine Liste mit allen Aliens an'
      ImageIndex = 12
      ShortCut = 16460
      OnExecute = ActionViewFrame
    end
    object ActionViewUFOList: TAction
      Tag = 3
      Category = 'Ansicht'
      Caption = '&UFOs'
      Enabled = False
      Hint = 'UFOs anzeigen|Zeigt eine Liste mit den UFO'#39's an'
      ImageIndex = 13
      ShortCut = 16469
      OnExecute = ActionViewFrame
    end
    object ActionProjektEdit: TAction
      Category = 'Projekt'
      Caption = '&Projekt bearbeiten'
      Hint = 'Projekteigenschaften ... |Bearbeitet das ausgew'#228'hlte Objekt'
      ImageIndex = 5
      ShortCut = 122
      SecondaryShortCuts.Strings = (
        'Enter')
      OnExecute = ActionProjektEditExecute
      OnUpdate = ProjektsEnabled
    end
    object ActionItemEdit: TAction
      Category = 'Ausr'#252'stung'
      Caption = '&Ausr'#252'stung bearbeiten'
      Hint = 'Ausr'#252'stungseigenschaften ... |Bearbeitet die gew'#228'hlte Ausr'#252'stung'
      ImageIndex = 5
      ShortCut = 122
      SecondaryShortCuts.Strings = (
        'Enter')
      OnExecute = ActionItemEditExecute
      OnUpdate = StartEnabled
    end
    object ActionUFOEdit: TAction
      Category = 'UFOs'
      Caption = 'UFO bearbeiten'
      Hint = 'UFO bearbeiten ... |Bearbeitet das gew'#228'hlte UFO'
      ImageIndex = 5
      ShortCut = 122
      SecondaryShortCuts.Strings = (
        'Enter')
      OnExecute = ActionUFOEditExecute
      OnUpdate = UFOSelected
    end
    object ActionUFOShields: TAction
      Category = 'UFOs'
      Caption = '&Schutzschilde'
      Hint = 'Hier k'#246'nnen Standard Schutzschilde f'#252'r UFOs definieren'
      ShortCut = 16456
    end
    object ActionUFOAdd: TAction
      Category = 'UFOs'
      Caption = 'UFO hinzuf'#252'gen'
      Hint = 'UFO hinzuf'#252'gen ... |F'#252'gt ein neues UFO hinzu'
      ImageIndex = 8
      ShortCut = 8237
      OnExecute = ActionUFOAddExecute
    end
    object ActionUFODelete: TAction
      Category = 'UFOs'
      Caption = 'UFO l'#246'schen'
      Hint = 'UFO l'#246'schen|L'#246'scht das gew'#228'hlte UFO aus der Liste'
      ImageIndex = 9
      ShortCut = 8238
      OnExecute = ActionUFODeleteExecute
      OnUpdate = UFOSelected
    end
    object ActionUFORename: TAction
      Category = 'UFOs'
      Caption = 'Umbenennen'
      Hint = 'Bennent das UFO um'
      ShortCut = 113
      OnExecute = ActionUFORenameExecute
      OnUpdate = UFOSelected
    end
    object ActionFileNew: TAction
      Category = 'Datei'
      Caption = 'Neu'
      Hint = 'Neu ... |Erstellt ein neuen Spielsatz'
      ImageIndex = 0
      ShortCut = 16462
      OnExecute = ActionFileNewExecute
    end
    object ActionFileProperties: TAction
      Category = 'Datei'
      Caption = 'Eigenschaften'
      Hint = 
        'Eigenschaften ... |Anzeigen und bearbeiten der Eigenschaften des' +
        ' Spielsatzes'
      ImageIndex = 5
      OnExecute = ActionFilePropertiesExecute
      OnUpdate = IsFileOpen
    end
    object ActionFileClose: TAction
      Category = 'Datei'
      Caption = 'Schliessen'
      Hint = 'Schliessen|Schliesst den Aktuellen Satz'
      ImageIndex = 15
      OnExecute = ActionFileCloseExecute
      OnUpdate = IsFileOpen
    end
    object ActionAlienAdd: TAction
      Category = 'Alien'
      Caption = 'Alien hinzuf'#252'gen'
      Hint = 'Neuer Alien ... |F'#252'gt ein neues Alien hinzu'
      ImageIndex = 8
      ShortCut = 8237
      OnExecute = ActionAlienAddExecute
    end
    object ActionAlienDelete: TAction
      Category = 'Alien'
      Caption = 'Alien l'#246'schen'
      Hint = 'Alien l'#246'schen ... |L'#246'scht das ausgew'#228'hlte Alien'
      ImageIndex = 9
      ShortCut = 8238
      OnExecute = ActionAlienDeleteExecute
      OnUpdate = ActionAlienEditUpdate
    end
    object ActionHelpContents: THelpContents
      Category = 'Hilfe'
      Caption = 'I&nhalt'
      ShortCut = 112
    end
    object ActionFileImport: TAction
      Category = 'Datei'
      Caption = '&Importieren ...'
      Hint = 'Importiert Daten aus einem vorhandenen Spielsatz'
      ImageIndex = 33
      OnExecute = ActionFileImportExecute
      OnUpdate = IsFileOpen
    end
    object ActionAlienRename: TAction
      Category = 'Alien'
      Caption = 'Umbennen'
      Hint = #196'ndert den Namen desr gew'#228'hlten Aliens'
      ShortCut = 113
      OnExecute = ActionAlienRenameExecute
      OnUpdate = ActionAlienEditUpdate
    end
    object ActionViewStartList: TAction
      Category = 'Ansicht'
      Caption = '&Ausr'#252'stung'
      Enabled = False
      Hint = 'Ausr'#252'stung anzeigen|Zeigt eine Liste mit den Projekten an'
      ImageIndex = 25
      ShortCut = 16449
      OnExecute = ActionViewFrame
    end
    object ActionProjektCopy: TAction
      Category = 'Projekt'
      Caption = 'Kopieren'
      Hint = 'Kopieren'
      ImageIndex = 22
      ShortCut = 16451
      OnExecute = ActionProjektCopyExecute
      OnUpdate = ProjektsEnabled
    end
    object ActionProjektPaste: TAction
      Category = 'Projekt'
      Caption = 'Einf'#252'gen'
      Hint = 'Einf'#252'gen'
      ImageIndex = 23
      ShortCut = 16470
      OnExecute = ActionProjektPasteExecute
      OnUpdate = ItemPasteEnabled
    end
    object ActionItemCopy: TAction
      Category = 'Ausr'#252'stung'
      Caption = 'Kopieren'
      Hint = 'Kopieren'
      ImageIndex = 22
      ShortCut = 16451
      OnExecute = ActionItemCopyExecute
      OnUpdate = StartEnabled
    end
    object ActionItemPaste: TAction
      Category = 'Ausr'#252'stung'
      Caption = 'Einf'#252'gen'
      Hint = 'Einf'#252'gen'
      ImageIndex = 23
      ShortCut = 16470
      OnExecute = ActionItemPasteExecute
      OnUpdate = ItemPasteEnabled
    end
    object ActionItemCut: TAction
      Category = 'Ausr'#252'stung'
      Caption = 'Ausschneiden'
      Hint = 
        'Ausgew'#228'hltes Objekt in die Zwischenablage legen und danach l'#246'sch' +
        'en'
      ImageIndex = 24
      ShortCut = 16472
      OnExecute = ActionItemCutExecute
      OnUpdate = StartEnabled
    end
    object ActionProjektCut: TAction
      Category = 'Projekt'
      Caption = 'Ausschneiden'
      Hint = 
        'Ausgew'#228'hltes Objekt in die Zwischenablage legen und danach l'#246'sch' +
        'en'
      ImageIndex = 24
      ShortCut = 16472
      OnExecute = ActionProjektCutExecute
      OnUpdate = ProjektsEnabled
    end
    object ActionProjektAddFather: TAction
      Category = 'Projekt'
      Caption = 'Vorg'#228'nger hinzuf'#252'gen'
      Hint = 'F'#252'gt einen neuen Vorg'#228'nger (Vorraussetzung) hinzu'
      ImageIndex = 28
      OnExecute = ActionProjektAddFatherExecute
      OnUpdate = ProjektsEnabled
    end
    object ActionProjektBack: TAction
      Category = 'Projekt'
      Caption = 'Vorheriges'
      ImageIndex = 27
      OnExecute = ActionProjektBackExecute
      OnUpdate = ActionProjektBackUpdate
    end
    object ActionProjektNext: TAction
      Category = 'Projekt'
      Caption = 'N'#228'chstes'
      ImageIndex = 26
      OnExecute = ActionProjektNextExecute
      OnUpdate = ActionProjektNextUpdate
    end
    object ActionViewAlienItems: TAction
      Tag = 6
      Category = 'Ansicht'
      Caption = '&Alienausr'#252'stung'
      Enabled = False
      Hint = 
        'Alienausr'#252'stung anzeigen|Ausr'#252'stung anzeigen, die von den Aliens' +
        ' genutzt wird'
      ImageIndex = 31
      ShortCut = 16457
      OnExecute = ActionViewFrame
    end
    object ActionFileLanguages: TAction
      Category = 'Datei'
      Caption = 'Sprachen ...'
      OnExecute = ActionFileLanguagesExecute
      OnUpdate = IsFileOpen
    end
    object ActionViewSkripts: TAction
      Tag = 7
      Category = 'Ansicht'
      Caption = '&Skripte'
      Enabled = False
      Hint = 
        'Missionsskripte anzeigen|Zeigt eine Liste mit den Skripten inner' +
        'halb des Spielsatzes an'
      ImageIndex = 32
      ShortCut = 16461
      OnExecute = ActionViewFrame
    end
    object ActionSkriptAdd: TAction
      Category = 'Skripts'
      Caption = 'Skript hinzuf'#252'gen'
      Hint = 'F'#252'gt ein neues Missionsskript hinzu'
      ImageIndex = 3
      OnExecute = ActionSkriptAddExecute
    end
    object ActionSkriptDelete: TAction
      Category = 'Skripts'
      Caption = 'Skript l'#246'schen'
      Hint = 'L'#246'scht das ausgew'#228'hlte Skript'
      ImageIndex = 4
      ShortCut = 8238
      OnExecute = ActionSkriptDeleteExecute
      OnUpdate = ActionSkriptDeleteUpdate
    end
    object ActionSkriptEdit: TAction
      Category = 'Skripts'
      Caption = 'Skript bearbeiten'
      Hint = 'Bearbeitet das ausgew'#228'hlte Skript'
      ImageIndex = 5
      ShortCut = 122
      SecondaryShortCuts.Strings = (
        'Enter')
      OnExecute = ActionSkriptEditExecute
      OnUpdate = ActionSkriptDeleteUpdate
    end
    object ActionSkriptRename: TAction
      Category = 'Skripts'
      Caption = 'Umbenennen'
      ShortCut = 113
      OnExecute = ActionSkriptRenameExecute
      OnUpdate = ActionSkriptDeleteUpdate
    end
    object ActionFileCheckGameSet: TAction
      Category = 'Datei'
      Caption = 'Pr'#252'fen'
      Hint = 'Pr'#252'fen|Pr'#252'ft den Spielsatz auf Objekt'#252'bergreifende Probleme'
      ImageIndex = 16
      ShortCut = 120
      OnExecute = ActionFileCheckGameSetExecute
      OnUpdate = IsFileOpen
    end
    object ActionAlienCopy: TAction
      Category = 'Alien'
      Caption = 'Kopieren'
      Hint = 'Kopieren'
      ImageIndex = 22
      ShortCut = 16451
      OnExecute = ActionAlienCopyExecute
      OnUpdate = ActionAlienEditUpdate
    end
    object ActionAlienPaste: TAction
      Category = 'Alien'
      Caption = 'Einf'#252'gen'
      Hint = 'Einf'#252'gen'
      ImageIndex = 23
      ShortCut = 16470
      OnExecute = ActionAlienPasteExecute
      OnUpdate = ActionAlienPasteUpdate
    end
    object ActionAlienCut: TAction
      Category = 'Alien'
      Caption = 'Ausschneiden'
      Hint = 
        'Ausgew'#228'hltes Objekt in die Zwischenablage legen und danach l'#246'sch' +
        'en'
      ImageIndex = 24
      ShortCut = 16472
      OnExecute = ActionAlienCutExecute
      OnUpdate = ActionAlienEditUpdate
    end
    object ActionUFOCopy: TAction
      Category = 'UFOs'
      Caption = 'Kopieren'
      Hint = 'Kopieren'
      ImageIndex = 22
      ShortCut = 16451
      OnExecute = ActionUFOCopyExecute
      OnUpdate = UFOSelected
    end
    object ActionUFOPaste: TAction
      Category = 'UFOs'
      Caption = 'Einf'#252'gen'
      Hint = 'Einf'#252'gen'
      ImageIndex = 23
      ShortCut = 16470
      OnExecute = ActionUFOPasteExecute
      OnUpdate = ActionUFOPasteUpdate
    end
    object ActionUFOCut: TAction
      Category = 'UFOs'
      Caption = 'Ausschneiden'
      Hint = 
        'Ausgew'#228'hltes Objekt in die Zwischenablage legen und danach l'#246'sch' +
        'en'
      ImageIndex = 24
      ShortCut = 16472
      OnExecute = ActionUFOCutExecute
      OnUpdate = UFOSelected
    end
    object ActionSkriptCopy: TAction
      Category = 'Skripts'
      Caption = 'Kopieren'
      Hint = 'Kopieren'
      ImageIndex = 22
      ShortCut = 16451
      OnExecute = ActionSkriptCopyExecute
      OnUpdate = ActionSkriptDeleteUpdate
    end
    object ActionSkriptCut: TAction
      Category = 'Skripts'
      Caption = 'Ausschneiden'
      Hint = 
        'Ausgew'#228'hltes Objekt in die Zwischenablage legen und danach l'#246'sch' +
        'en'
      ImageIndex = 24
      ShortCut = 16472
      OnExecute = ActionSkriptCutExecute
      OnUpdate = ActionSkriptDeleteUpdate
    end
    object ActionSkriptPaste: TAction
      Category = 'Skripts'
      Caption = 'Einf'#252'gen'
      Hint = 'Einf'#252'gen'
      ImageIndex = 23
      ShortCut = 16470
      OnExecute = ActionSkriptPasteExecute
      OnUpdate = ActionSkriptPasteUpdate
    end
    object ActionViewObjects: TAction
      Tag = 8
      Category = 'Ansicht'
      Caption = '&'#220'bersicht'
      Enabled = False
      Hint = 
        'Alle Objekte anzeigen|Zeigt eine Liste mit allen Objekten an und' +
        ' bietet Funktionen zum gezielten Vergleichen'
      ImageIndex = 34
      ShortCut = 16463
      OnExecute = ActionViewFrame
    end
    object ActionViewAsHTML: TAction
      Caption = 'HTML-Export'
      ImageIndex = 35
      OnExecute = ActionViewAsHTMLExecute
      OnUpdate = ActionViewAsHTMLUpdate
    end
  end
  object ApplicationEvents: TApplicationEvents
    OnIdle = ApplicationEventsIdle
    OnHint = ApplicationEventsHint
    Left = 72
    Top = 72
  end
  object WaffenListe: TExtendImageList
    Height = 32
    Masked = False
    Width = 32
    Left = 40
    Top = 107
  end
  object XPManifest1: TXPManifest
    Left = 73
    Top = 107
  end
  object SaveHTMLExport: TSaveDialog
    DefaultExt = 'html'
    Filter = 'HTML-Datei (*.html)|*.html'
    Options = [ofOverwritePrompt, ofHideReadOnly, ofNoChangeDir, ofEnableSizing]
    Title = 'HTML-Export'
    Left = 105
    Top = 74
  end
end
