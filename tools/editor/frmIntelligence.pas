unit frmIntelligence;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, ComCtrls;

type
  TIntelligenzForm = class(TForm)
    Label1: TLabel;
    Edit1: TEdit;
    TrackBar1: TTrackBar;
    ListCaption: TLabel;
    ListBox1: TListBox;
    OKButton: TBitBtn;
    BitBtn2: TBitBtn;
    procedure TrackBar1Change(Sender: TObject);
    procedure Edit1Change(Sender: TObject);
  private
    fAlien           : Boolean;
    fBuildListProc   : TNotifyEvent;
    function GetIQ: Integer;
    procedure SetIQ(const Value: Integer);
    procedure SetAlien(const Value: Boolean);

    procedure BuildAlienList(Sender: TObject);
    procedure BuildItemList(Sender: TObject);
    { Private-Deklarationen }
  public
    { Public-Deklarationen }
    property IQ     : Integer read GetIQ write SetIQ;
    property Alien  : Boolean read fAlien write SetAlien;
  end;

var
  IntelligenzForm: TIntelligenzForm;

implementation

uses frmMain, string_utils;

{$R *.DFM}

{ TIntelligenzForm }

function TIntelligenzForm.GetIQ: Integer;
begin
  result:=TrackBar1.Position;
end;

procedure TIntelligenzForm.SetIQ(const Value: Integer);
begin
  TrackBar1.Position:=Value;
  TrackBar1Change(Self);
end;

procedure TIntelligenzForm.TrackBar1Change(Sender: TObject);
begin
  Edit1.Text:=IntToStr(TrackBar1.Position);
  if Assigned(fBuildListProc) then
    fBuildListProc(nil);
end;

procedure TIntelligenzForm.Edit1Change(Sender: TObject);
begin
  try
    TrackBar1.Position:=StrToInt(Edit1.Text);
    OKButton.Enabled:=true;
    TrackBar1Change(Self);
  except
    OKButton.Enabled:=false;
  end;
end;

procedure TIntelligenzForm.SetAlien(const Value: Boolean);
begin
  fAlien := Value;
  if fAlien then
  begin
    ListCaption.Caption:='Alien kann folgende Alien-Ausrüstung nutzen:';
    fBuildListProc:=BuildItemList;
  end
  else
  begin
    ListCaption.Caption:='Ausrüstung kann von folgenden Aliens genutzt werden:';
    fBuildListProc:=BuildAlienList;
  end;
end;

procedure TIntelligenzForm.BuildAlienList(Sender: TObject);
var
  Dummy: Integer;
begin
  ListBox1.Items.BeginUpdate;
  ListBox1.Items.Clear;
  for Dummy:=0 to high(MDIForm.Aliens) do
  begin
    if MDIForm.Aliens[Dummy].GetInteger('IQ')>=TrackBar1.Position then
      ListBox1.Items.Add(string_utils_GetLanguageString(MDIForm.Sprache,MDIForm.Aliens[Dummy].GetString('Name')));
  end;
  ListBox1.Items.EndUpdate;
  ListBox1.Refresh;
end;

procedure TIntelligenzForm.BuildItemList(Sender: TObject);
var
  Dummy: Integer;
begin
  ListBox1.Items.BeginUpdate;
  ListBox1.Items.Clear;
  for Dummy:=0 to high(MDIForm.Projekts) do
  begin
    with MDIForm.Projekts[Dummy] do
    begin
      if ValueExists('NeededIQ') and GetBoolean('AlienItem') then
      begin
        if GetInteger('NeededIQ')<=TrackBar1.Position then
          ListBox1.Items.Add(string_utils_GetLanguageString(MDIForm.Sprache,MDIForm.Projekts[Dummy].GetString('Name')));
      end;
    end;
  end;
  ListBox1.Items.EndUpdate;
  ListBox1.Refresh;
end;

end.
