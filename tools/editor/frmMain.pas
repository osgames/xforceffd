unit frmMain;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Menus, StdActns, ActnList, ImgList, AppEvnts, ToolWin, ProjektView,
  ArchivFile, XForce_types, StartView, ExtendImageList, TB2Item, ExtCtrls,
  ForschUtils, StdCtrls, frmAlien, Koding, frmUFO, ExtRecord,
  KD4Utils, Loader, ComCtrls, RecycleForm,  Clipbrd,  TB2Dock, TB2Toolbar,
  TB2ToolWindow, ProjektRecords, AlienItemsView, ObjektRecords, Defines,
  record_utils, string_utils, MissionSkriptViews, ObjektView, XPMan, TBX, {$I language.inc};

type
  TSetBool = Set of boolean;

  TMDIForm = class(TForm)
    ImageList1: TImageList;
    ActionList1: TActionList;
    ActionFileOpen: TAction;
    ActionFileExit: TAction;
    ApplicationEvents: TApplicationEvents;
    ActionFileSave: TAction;
    ActionViewProjektList: TAction;
    ActionProjektRename: TAction;
    ActionProjektNew: TAction;
    ActionProjektDelete: TAction;
    ActionItemRename: TAction;
    ActionItemAdd: TAction;
    ActionItemDelete: TAction;
    ActionViewRecycleBin: TAction;
    ActionProjektEdit: TAction;
    ActionItemEdit: TAction;
    Panel1: TPanel;
    StatusBar: TStatusBar;
    ActionViewAlienList: TAction;
    ActionViewUFOList: TAction;
    ActionUFOEdit: TAction;
    ActionUFOShields: TAction;
    ActionUFOAdd: TAction;
    ActionUFODelete: TAction;
    ActionUFORename: TAction;
    ActionFileNew: TAction;
    ActionFileProperties: TAction;
    ActionFileClose: TAction;
    ActionAlienAdd: TAction;
    ActionAlienDelete: TAction;
    ActionAlienEdit: TAction;
    ActionHelpContents: THelpContents;
    ActionFileImport: TAction;
    FramePanel: TPanel;
    TBDock1: TTBXDock;
    TBDock2: TTBXDock;
    ActionAlienRename: TAction;
    ActionViewStartList: TAction;
    ActionProjektCopy: TAction;
    ActionProjektPaste: TAction;
    ActionItemCopy: TAction;
    ActionItemPaste: TAction;
    ActionItemCut: TAction;
    ActionProjektCut: TAction;
    ActionProjektAddFather: TAction;
    ActionProjektBack: TAction;
    ActionProjektNext: TAction;
    ActionProjektDeleteFather: TAction;
    ActionViewAlienItems: TAction;
    ActionFileLanguages: TAction;
    WaffenListe: TExtendImageList;
    ActionViewSkripts: TAction;
    ActionSkriptAdd: TAction;
    ActionSkriptDelete: TAction;
    ActionSkriptEdit: TAction;
    ActionSkriptRename: TAction;
    ProjektBar: TTBXToolbar;
    TBItem13: TTBXItem;
    TBItem11: TTBXItem;
    TBItem10: TTBXItem;
    TBSeparatorItem3: TTBXSeparatorItem;
    TBItem28: TTBXItem;
    TBItem24: TTBXItem;
    TBItem12: TTBXItem;
    TBSeparatorItem5: TTBXSeparatorItem;
    TBItem31: TTBXItem;
    TBItem32: TTBXItem;
    TBSeparatorItem6: TTBXSeparatorItem;
    TBItem30: TTBXItem;
    TBItem29: TTBXItem;
    ItemToolBar: TTBXToolbar;
    TBItem16: TTBXItem;
    TBItem15: TTBXItem;
    TBItem14: TTBXItem;
    TBSeparatorItem4: TTBXSeparatorItem;
    TBItem27: TTBXItem;
    TBItem26: TTBXItem;
    TBItem25: TTBXItem;
    UFOBar: TTBXToolbar;
    TBItem19: TTBXItem;
    TBItem18: TTBXItem;
    TBItem17: TTBXItem;
    AlienBar: TTBXToolbar;
    TBItem22: TTBXItem;
    TBItem21: TTBXItem;
    TBItem20: TTBXItem;
    DefaultBar: TTBXToolbar;
    TBItem4: TTBXItem;
    TBItem3: TTBXItem;
    TBItem2: TTBXItem;
    TBItem1: TTBXItem;
    TBSeparatorItem1: TTBXSeparatorItem;
    TBItem5: TTBXItem;
    TBSeparatorItem2: TTBXSeparatorItem;
    TBItem6: TTBXItem;
    TBItem23: TTBXItem;
    TBItem7: TTBXItem;
    TBItem8: TTBXItem;
    TBItem9: TTBXItem;
    TBItem33: TTBXItem;
    MenuBar: TTBXToolbar;
    MenuFile: TTBXSubmenuItem;
    NeuerSpielsatz1: TTBXItem;
    ffnen1: TTBXItem;
    Speichern1: TTBXItem;
    Schliessen1: TTBXItem;
    N12: TTBXSeparatorItem;
    Sprachen1: TTBXItem;
    Importieren1: TTBXItem;
    Eigenschaften1: TTBXItem;
    N1: TTBXSeparatorItem;
    MenuExit: TTBXItem;
    Ansicht1: TTBXSubmenuItem;
    Projektliste1: TTBXItem;
    Ausrstung1: TTBXItem;
    UFOs1: TTBXItem;
    Aliens1: TTBXItem;
    Alienausrstung1: TTBXItem;
    Skripts: TTBXItem;
    MenuProjekt: TTBXSubmenuItem;
    Projektbearbeiten2: TTBXItem;
    Umbennen3: TTBXItem;
    N3: TTBXSeparatorItem;
    Ausschneiden1: TTBXItem;
    Kopieren1: TTBXItem;
    Einfgen1: TTBXItem;
    N4: TTBXSeparatorItem;
    Projekthinzufgen1: TTBXItem;
    Projektlschen2: TTBXItem;
    MenuStart: TTBXSubmenuItem;
    Ausrstungbearbeiten1: TTBXItem;
    Umbenennen1: TTBXItem;
    N2: TTBXSeparatorItem;
    Ausschneiden2: TTBXItem;
    Kopieren2: TTBXItem;
    Einfgen2: TTBXItem;
    N6: TTBXSeparatorItem;
    Ausrstunghinzufgen1: TTBXItem;
    Ausrstunglschen1: TTBXItem;
    N7: TTBXSeparatorItem;
    Sortierennach1: TTBXSubmenuItem;
    MenuSName: TTBXItem;
    MenuSTyp: TTBXItem;
    MenuSAnzahl: TTBXItem;
    MenuUFO: TTBXSubmenuItem;
    MenuUFOEdit: TTBXItem;
    Umbenennen2: TTBXItem;
    N10: TTBXSeparatorItem;
    UFObearbeiten1: TTBXItem;
    UFOlschen1: TTBXItem;
    N11: TTBXSeparatorItem;
    SortUFO: TTBXSubmenuItem;
    Name1: TTBXItem;
    Hitpoints1: TTBXItem;
    Schildpunkte1: TTBXItem;
    Angriffsstrke1: TTBXItem;
    minBesatzung1: TTBXItem;
    zusBesatzung1: TTBXItem;
    Angriffnach1: TTBXItem;
    Gesamtstrke1: TTBXItem;
    MenuAlien: TTBXSubmenuItem;
    Alienbearbeiten1: TTBXItem;
    Umbennen2: TTBXItem;
    N8: TTBXSeparatorItem;
    MenuAlienAdd: TTBXItem;
    MenuAliendelete: TTBXItem;
    N15: TTBXSeparatorItem;
    SortAlien: TTBXSubmenuItem;
    Name2: TTBXItem;
    Gesundheit1: TTBXItem;
    Panzerung1: TTBXItem;
    Strke1: TTBXItem;
    PSIAngriff1: TTBXItem;
    PSIAbweh1: TTBXItem;
    Zeiteinheiten1: TTBXItem;
    Angriffnach2: TTBXItem;
    Gesamtstrke2: TTBXItem;
    Hilfe1: TTBXSubmenuItem;
    About: TTBXItem;
    TBDock3: TTBXDock;
    TBDock4: TTBXDock;
    ActionFileCheckGameSet: TAction;
    TBItem34: TTBXItem;
    TBItem35: TTBXItem;
    ActionAlienCopy: TAction;
    ActionAlienPaste: TAction;
    ActionAlienCut: TAction;
    ActionUFOCopy: TAction;
    ActionUFOPaste: TAction;
    ActionUFOCut: TAction;
    TBItem37: TTBXItem;
    TBItem38: TTBXItem;
    TBItem39: TTBXItem;
    TBSeparatorItem7: TTBXSeparatorItem;
    TBItem36: TTBXItem;
    TBItem40: TTBXItem;
    TBItem41: TTBXItem;
    TBSeparatorItem8: TTBXSeparatorItem;
    TBSeparatorItem9: TTBXSeparatorItem;
    TBItem42: TTBXItem;
    TBItem43: TTBXItem;
    TBSeparatorItem10: TTBXSeparatorItem;
    TBItem44: TTBXItem;
    TBItem45: TTBXItem;
    TBItem46: TTBXItem;
    TBItem47: TTBXItem;
    TBItem48: TTBXItem;
    TBItem49: TTBXItem;
    TBSeparatorItem11: TTBXSeparatorItem;
    MenuSkript: TTBXSubmenuItem;
    ActionSkriptCopy: TAction;
    ActionSkriptCut: TAction;
    ActionSkriptPaste: TAction;
    SkriptBar: TTBXToolbar;
    TBItem50: TTBXItem;
    TBItem51: TTBXItem;
    TBItem52: TTBXItem;
    TBSeparatorItem12: TTBXSeparatorItem;
    TBItem53: TTBXItem;
    TBItem54: TTBXItem;
    TBItem55: TTBXItem;
    ActionViewObjects: TAction;
    TBItem56: TTBXItem;
    TBItem57: TTBXItem;
    TBItem58: TTBXItem;
    TBSeparatorItem13: TTBXSeparatorItem;
    TBItem59: TTBXItem;
    TBItem60: TTBXItem;
    TBSeparatorItem14: TTBXSeparatorItem;
    TBItem61: TTBXItem;
    TBItem62: TTBXItem;
    TBItem63: TTBXItem;
    XPManifest1: TXPManifest;
    ActionViewAsHTML: TAction;
    TBXSeparatorItem1: TTBXSeparatorItem;
    TBXItem1: TTBXItem;
    SaveHTMLExport: TSaveDialog;
    TBXItem2: TTBXItem;
    MenuSVer: TTBXItem;
    procedure MenuExitClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure ActionFileExitExecute(Sender: TObject);
    procedure ActionFileSaveExecute(Sender: TObject);
    procedure ActionProjektRenameExecute(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure ActionProjektNewExecute(Sender: TObject);
    procedure ActionItemRenameExecute(Sender: TObject);
    procedure ActionFileOpenExecute(Sender: TObject);
    procedure ActionSkriptEditUpd(Sender: TObject);
    procedure ActionItemDeleteExecute(Sender: TObject);
    procedure ActionProjektEditExecute(Sender: TObject);
    procedure ActionItemEditExecute(Sender: TObject);
    procedure AboutClick(Sender: TObject);
    procedure ActionList1Update(Action: TBasicAction;
      var Handled: Boolean);
    procedure SortClick(Sender: TObject);
    procedure ActionUFOEditExecute(Sender: TObject);
    procedure ActionUFOAddExecute(Sender: TObject);
    procedure UFOSelected(Sender: TObject);
    procedure CanSave(Sender: TObject);
    procedure ActionUFORenameExecute(Sender: TObject);
    procedure ActionUFODeleteExecute(Sender: TObject);
    procedure SortUFOClick(Sender: TObject);
    procedure IsFileOpen(Sender: TObject);
    procedure ActionFilePropertiesExecute(Sender: TObject);
    procedure ActionFileCloseExecute(Sender: TObject);
    procedure ActionFileNewExecute(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure ActionAlienAddExecute(Sender: TObject);
    procedure ActionAlienEditExecute(Sender: TObject);
    procedure SortAlienClick(Sender: TObject);
    procedure ActionAlienEditUpdate(Sender: TObject);
    procedure ActionFileImportExecute(Sender: TObject);
    procedure ActionAlienRenameExecute(Sender: TObject);
    procedure ActionAlienDeleteExecute(Sender: TObject);
    procedure ProjektsEnabled(Sender: TObject);
    procedure ActionProjektDeleteExecute(Sender: TObject);
    procedure ActionProjektCopyExecute(Sender: TObject);
    procedure ItemPasteEnabled(Sender: TObject);
    procedure ActionProjektPasteExecute(Sender: TObject);
    procedure ActionItemCopyExecute(Sender: TObject);
    procedure ActionItemPasteExecute(Sender: TObject);
    procedure StartEnabled(Sender: TObject);
    procedure ActionItemCutExecute(Sender: TObject);
    procedure ActionViewFrame(Sender: TObject);
    procedure ActionProjektCutExecute(Sender: TObject);
    procedure ActionProjektAddFatherExecute(Sender: TObject);
    procedure ActionProjektBackUpdate(Sender: TObject);
    procedure ActionProjektNextUpdate(Sender: TObject);
    procedure ActionProjektBackExecute(Sender: TObject);
    procedure ActionProjektNextExecute(Sender: TObject);
    procedure ActionProjektDeleteFatherUpdate(Sender: TObject);
    procedure ActionProjektDeleteFatherExecute(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure ActionFileLanguagesExecute(Sender: TObject);
    procedure ActionSkriptAddExecute(Sender: TObject);
    procedure ActionSkriptDeleteUpdate(Sender: TObject);
    procedure ActionSkriptDeleteExecute(Sender: TObject);
    procedure ActionSkriptRenameExecute(Sender: TObject);
    procedure ActionSkriptEditExecute(Sender: TObject);
    procedure ApplicationEventsHint(Sender: TObject);
    procedure ActionFileCheckGameSetExecute(Sender: TObject);
    procedure ActionAlienPasteUpdate(Sender: TObject);
    procedure ActionAlienCutExecute(Sender: TObject);
    procedure ActionAlienCopyExecute(Sender: TObject);
    procedure ActionAlienPasteExecute(Sender: TObject);
    procedure ActionUFOCutExecute(Sender: TObject);
    procedure ActionUFOPasteUpdate(Sender: TObject);
    procedure ActionUFOCopyExecute(Sender: TObject);
    procedure ActionUFOPasteExecute(Sender: TObject);
    procedure ActionSkriptPasteUpdate(Sender: TObject);
    procedure ActionSkriptPasteExecute(Sender: TObject);
    procedure ActionSkriptCopyExecute(Sender: TObject);
    procedure ActionSkriptCutExecute(Sender: TObject);
    procedure ApplicationEventsIdle(Sender: TObject; var Done: Boolean);
    procedure ActionViewAsHTMLUpdate(Sender: TObject);
    procedure ActionViewAsHTMLExecute(Sender: TObject);
  private
    fChange      : boolean;
    fLoadShields : boolean;
    fHeader      : TSetHeader;
    fFileName    : String;
    fNewSet      : boolean;
    fFrameChange : boolean;
    procedure SetChange(const Value: boolean);
    procedure LoadLanguages(Archiv: TArchivFile);
    procedure LoadForschDat(Archiv: TArchivFile);

    procedure ImportGameSet;
    procedure ImportForschdata(Archiv: TArchivFile; Start: TSetBool; AlienItems: Boolean);
    procedure ImportUFOdata(Archiv: TArchivFile);
    procedure ImportAliendata(Archiv: TArchivFile);
    procedure ImportSymbols(Archiv: TArchivFile);
    procedure ImportUFOPadie(Archiv: TArchivFile);
    procedure ImportSkripts(Archiv: TArchivFile);

    procedure SetFileName(const Value: String);
    procedure SetSatzName(const Value: String);
    procedure EnableViewers(Enabled: boolean);
    function GetSetName: String;

    procedure CopyRecordToClipboard(Rec: TExtRecord; Format: Integer);
    function GetRecordFromClipboard(Format: Integer): TExtRecord;
    function GenerateID(List: TRecordArray): Cardinal;
    { Private-Deklarationen }
  public
    Projekts         : TRecordArray;
    Aliens           : TRecordArray;
    UFOs             : TRecordArray;
    Skripte          : TRecordArray;
    Shields          : Array of TShieldDefault;
    Sprachen         : TStringArray;
    DeletedChild     : Array of boolean;
    Sprache          : String;

    ProjektViewer    : TProjektForm;
    RecycleViewer    : TRecycledForm;
    StartViewer      : TStartForm;
    AlienViewer      : TAlienForm;
    UFOViewer        : TUFOForm;
    AlienItemsViewer : TAlienItemsForm;
    SkriptViewer     : TMissionSkripts;
    ObjektViewer     : TObjektForm;

    function GetIndexOfId(List: TRecordArray; ID: Cardinal): Integer;
    function Save: boolean;
    property ForschChanged : boolean read fChange write SetChange;
    procedure ShowFrame(Frame: Integer);
    function GetNameofType(Typ: TProjektType): String;
    function EditProjekt(Projekt: TExtRecord;New: boolean=false): boolean;
    property Filename: String read fFileName write SetFileName;
    property SatzName: String read GetSetName write SetSatzName;
    property SetHead : TSetHeader read fHeader;
    property NewSet  : boolean read fNewSet write fNewSet;
    { Public-Deklarationen }
  end;

var
  MDIForm : TMDIForm;
  LoadSetStatus        : boolean;
  ClpFormatProject     : Integer;
  ClpFormatAlien       : Integer;
  ClpFormatUFO         : Integer;
  ClpFormatSkript      : Integer;

implementation

uses
  frmSave, frmAbout, frmPasswort, LoadSet, NewSet, CommCtrl,
  frmProperties, SelectProjekt, frmEditProjekt, SymbEdit, frmLanguages,
  frmEditLanguages, gameset_api, ChangeTyp, game_utils, array_utils, ShellAPI;

{$R *.DFM}

procedure TMDIForm.MenuExitClick(Sender: TObject);
begin
  Close;
end;

procedure TMDIForm.FormCreate(Sender: TObject);
var
  Archiv   : TArchivFile;
  OldImage : TImageList;
  Bitmap   : TBitmap;

  procedure Replace(Index: Integer; MessageType: TMessageType);
  const
    IconSize = 16;
  var
    Icon   : TIcon;
    Temp   : TBitmap;
    Img    : TBitmap;
  begin
    Icon:=TIcon.Create;
    case MessageType of
      mtError       : Icon.Handle:=LoadIcon(0,IDI_ERROR);
      mtWarning     : Icon.Handle:=LoadIcon(0,IDI_WARNING);
      mtHinweis     : Icon.Handle:=LoadIcon(0,IDI_INFORMATION);
    end;
    Temp:=TBitmap.Create;
    Temp.Width:=Icon.Width;
    Temp.Height:=Icon.Height;
    Temp.Canvas.Brush.Color:=clWindow;
    Temp.Canvas.FillRect(Rect(0,0,Temp.Width,Temp.Height));
    Temp.Canvas.Draw(0,0,Icon);

    Img:=TBitmap.Create;
    Img.Width:=Iconsize;
    Img.Height:=Iconsize;
    Img.Canvas.StretchDraw(Rect(0,0,Iconsize,Iconsize),Temp);

    ImageList1.Replace(Index,Img,nil);

    Icon.Free;
    Temp.Free;
    Img.Free;
  end;

begin
  fFrameChange:=false;
  Application.HintPause:=250;
  ActionFileProperties.ShortCut:=ShortCut(Word(#13), [ssAlt]);
  randomize;
  Application.Title := LShortName+' - Editor';
  Caption:='Editor - '+LGameName;
  fLoadShields:=false;
  StartViewer:=TStartForm.Create(FramePanel);
  StartViewer.Parent:=FramePanel;
  StartViewer.StartView.SmallImages:=WaffenListe.SmallImageList;
//  ShowMessage(IntToStr(WaffenListe.SmallImageList.Width));

  ProjektViewer:=TProjektForm.Create(FramePanel);
  ProjektViewer.Parent:=FramePanel;
  ProjektViewer.AllProjekts.SMallImages:=WaffenListe.SmallImageList;
  ProjektViewer.ChildsList.SMallImages:=WaffenListe.SmallImageList;
  ProjektViewer.ParentList.SMallImages:=WaffenListe.SmallImageList;

  AlienViewer:=TAlienForm.Create(FramePanel);
  AlienViewer.Parent:=FramePanel;
  AlienViewer.Visible:=false;

  RecycleViewer:=TRecycledForm.Create(FramePanel);
  RecycleViewer.Parent:=FramePanel;
  RecycleViewer.Visible:=false;

  UFOViewer:=TUFOForm.Create(FramePanel);
  UFOViewer.Parent:=FramePanel;
  UFOViewer.Visible:=false;

  AlienItemsViewer:=TAlienItemsForm.Create(FramePanel);
  AlienItemsViewer.Parent:=FramePanel;
  AlienItemsViewer.Visible:=false;
  AlienItemsViewer.StartView.SmallImages:=WaffenListe.SmallImageList;

  SkriptViewer:=TMissionSkripts.Create(FramePanel);
  SkriptViewer.Parent:=FramePanel;
  SkriptViewer.Visible:=false;

  ObjektViewer:=TObjektForm.Create(FramePanel);
  ObjektViewer.Parent:=FramePanel;
  ObjektViewer.Visible:=false;
  ObjektViewer.ObjektView.SmallImages:=WaffenListe.SmallImageList;

  Archiv:=TArchivFile.Create;
  Archiv.OpenArchiv('data\data.pak',true);
  Bitmap:=TBitmap.Create;
  try
    Archiv.OpenRessource(RNWaffenSymbols);
    Bitmap.LoadFromStream(Archiv.Stream);
  except
    Bitmap.Width:=0;
  end;
  WaffenListe.SetBasisImage(Bitmap);
  Bitmap.Free;
  Archiv.Free;

  OldImage:=TImageList.Create(Self);
  OldImage.Assign(ImageList1);

  ImageList1.Handle:=ImageList_Create(16,16,ILC_COLOR32 or ILC_MASK	,0,10);
  ImageList1.AddImages(OldImage);

  OldImage.Free;

  Replace(14,mtError);
  Replace(17,mtHinweis);
  Replace(30,mtWarning);

  TBRegLoadPositions(Self,HKEY_CURRENT_USER,'Software\Rich Entertainment\Edit\Toolbars');

  ShowFrame(-1);
end;

procedure TMDIForm.ActionFileExitExecute(Sender: TObject);
begin
  Close;
end;

function TMDIForm.GetIndexOfId(List: TRecordArray; ID: Cardinal): Integer;
var
  Dummy: Integer;
begin
  result:=-1;
  for Dummy:=0 to Length(List)-1 do
  begin
    if (List[Dummy].GetCardinal('ID')=ID) and ((List<>Projekts) or (not DeletedChild[Dummy])) then
    begin
      result:=dummy;
      exit;
    end;
  end;
end;

procedure TMDIForm.SetChange(const Value: boolean);
begin
  if fChange=Value then exit;
  fChange := Value;
  if fChange then StatusBar.Panels[1].Text:='Ge�ndert' else StatusBar.Panels[1].Text:='';
end;

procedure TMDIForm.ActionFileSaveExecute(Sender: TObject);
begin
  Save;
end;

procedure TMDIForm.ActionProjektRenameExecute(Sender: TObject);
begin
  ProjektViewer.AllProjekts.Selected.EditCaption;
end;

procedure TMDIForm.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
var
  result: Integer;
begin
  if ForschChanged then
  begin
    result:=Application.MessageBox(PChar('Die Daten wurden ge�ndert! M�chten Sie die �nderungen speichern?'),PChar(LGameName+' Editor'),MB_ICONQUESTION+MB_YESNOCANCEL);
    if result=ID_CANCEL then CanClose:=false;
    if result=ID_YES then
    begin
      if not Save then CanClose:=false;
    end;
  end;
end;

procedure TMDIForm.ActionProjektNewExecute(Sender: TObject);
var
  Index   : Integer;
  Projekt : TExtRecord;
begin
  TypForm.Caption:='Projekttyp festlegen';
  TypForm.Typ:=ptNone;
  if TypForm.ShowModal=mrCancel then
  begin
    TypForm.Caption:='Projekttyp �ndern';
    exit;
  end;
  TypForm.Caption:='Projekttyp �ndern';

  Projekt:=TExtRecord.Create(GetDefinitionFromType(TypForm.Typ));
  Projekt.SetCardinal('ID',GenerateID(Projekts));
  Projekt.SetBoolean('Start',false);
  Projekt.SetInteger('TypeID',Integer(TypForm.Typ));
  ChangeProjektTyp(Projekt,TypForm.Typ);
  if (not EditProjekt(Projekt,true)) then
  begin
    Projekt.Free;
    exit;
  end;

  SetLength(Projekts,length(Projekts)+1);
  SetLength(DeletedChild,Length(Projekts));
  DeletedChild[high(Projekts)]:=false;

  Index:=High(Projekts);
  Projekts[Index]:=Projekt;
  ProjektViewer.AddToList(Index,ProjektViewer.AllProjekts);
  ProjektViewer.AllProjekts.CustomSort(nil,0);
  ForschChanged:=true;
end;

function TMDIForm.GetNameofType(Typ: TProjektType): String;
begin
  result:=game_utils_TypeToStr(Typ);
end;

procedure TMDIForm.ActionItemRenameExecute(Sender: TObject);
begin
  if AlienItemsViewer.Visible then
    AlienItemsViewer.StartView.Selected.EditCaption
  else
    StartViewer.StartView.Selected.EditCaption;
end;

procedure TMDIForm.ActionFileOpenExecute(Sender: TObject);
begin
  PassWordDlg.Password.Text:='';
  if LoadGameSet.ShowModal=mrOK then
  begin
    fNewSet:=false;
    FileName:=LoadGameSet.FileName;
    SatzName:=LoadGameSet.SatzName;
    fHeader.Password:=PassWordDlg.Password.Text;
  end;
end;

procedure TMDIForm.LoadForschDat(Archiv: TArchivFile);
var
  Bitmap       : TBitmap;
begin
  try
    gameset_api_ReadHeader(Archiv,fHeader);
  except
    with fHeader do
    begin
      Name:='';
      Autor:='';
      Organisation:='';
      Beschreibung:='';
      CanImport:=true;
    end;
  end;
  try
    gameset_api_LoadProjects(Archiv,Projekts);

    SetLength(DeletedChild,length(Projekts));
    FillChar(DeletedChild[0],length(DeletedChild)*sizeOf(DeletedChild[0]),#0);
//    if ProjektLoader.Version<=7 then
//      ConvertForschProjekts(Projekts);
  except
    SetLength(DeletedChild,0);
    record_utils_ClearArray(Projekts);
  end;
  Bitmap:=TBitmap.Create;
  try
    Archiv.OpenRessource(RNWaffenSymbols);
    Bitmap.LoadFromStream(Archiv.Stream);
    WaffenListe.SetLokalImage(Bitmap);
  except
    WaffenListe.SetLokalImage(nil);
  end;
  Bitmap.Free;
end;

procedure TMDIForm.ActionSkriptEditUpd(Sender: TObject);
var
  Index   : Integer;
  Projekt : TExtRecord;
begin
{  FillChar(Projekt,SizeOf(TForschProject),#0);
  Projekt.Name:='Unbenannt';
  Projekt.ID:=GenerateProjektID;
  Projekt.Land:=-1;
  Projekt.NextID1:=0;
  Projekt.NextID2:=0;
  Projekt.NextID3:=0;
  Projekt.TypeID:=ptNone;
  Projekt.ImageIndex:=-1;      
  Projekt.Start:=true;}
  TypForm.Caption:='Ausr�stungstyp festlegen';
  TypForm.Typ:=ptWaffe;
  if TypForm.ShowModal=mrCancel then
  begin
    TypForm.Caption:='Projekttyp �ndern';
    exit;
  end;
  TypForm.Caption:='Projekttyp �ndern';
  if TypForm.Typ=ptNone then
  begin
    ShowMessage('Ausr�stung kann keine Technologie sein');
    exit;
  end;
  if AlienItemsViewer.Visible then
  begin
    // Typen pr�fen
    if not (TypForm.Typ in SoldatenTypes) then
    begin
      ShowMessage('Alienausr�stung darf nur folgende Typen sein: '#13#10+
                  'Waffe,Panzerung,Munition,Mine,Sensor,Granate oder G�rtel');
      exit;
    end;
  end;
  Projekt:=TExtRecord.Create(GetDefinitionFromType(TypForm.Typ));
  if Projekt.ValueExists('AlienItem') then
    Projekt.SetBoolean('AlienItem',AlienItemsViewer.Visible);
  Projekt.SetCardinal('ID',GenerateID(Projekts));
  Projekt.SetBoolean('Start',true);
  Projekt.SetInteger('TypeID',Integer(TypForm.Typ));
  ChangeProjektTyp(Projekt,TypForm.Typ);
  if not EditProjekt(Projekt,true) then
  begin
    Projekt.Free;
    exit;
  end;

  SetLength(Projekts,length(Projekts)+1);
  SetLength(DeletedChild,Length(Projekts));
  DeletedChild[high(Projekts)]:=false;

  Index:=High(Projekts);
  Projekts[Index]:=Projekt;

  if StartViewer.Visible then
  begin
    StartViewer.AddToList(Index,true);
    StartViewer.StartView.CustomSort(nil,0);
  end
  else
  begin
    AlienItemsViewer.AddToList(Index,true);
    AlienItemsViewer.StartView.CustomSort(nil,0);
  end;

  ForschChanged:=true;
end;

procedure TMDIForm.ActionItemDeleteExecute(Sender: TObject);
var
  Item: TListItem;
begin
  if AlienItemsViewer.Visible then
    Item:=AlienItemsViewer.StartView.Selected
  else
    Item:=StartViewer.StartView.Selected;

  if MessageDlg('Soll das Objekt '''+Item.Caption+''' gel�scht werden?',mtConfirmation,[mbYes,mbNo],0) = mrNo then
    exit;
    
  DeletedChild[Integer(Item.Data)]:=true;
  ForschChanged:=true;
  if RecycleViewer<>nil then
    RecycleViewer.AddDeleted(Integer(Item.Data));
  Item.Delete;

end;

procedure TMDIForm.ActionProjektEditExecute(Sender: TObject);
var
  Projekt : TExtRecord;
  Item    : TListItem;
begin
  if ProjektViewer.AllProjekts.Selected <> nil then
  begin
    Item:=ProjektViewer.AllProjekts.Selected;
    Projekt:=TExtRecord.Create(Projekts[Integer(Item.Data)].RecordDefinition);
    Projekt.Assign(Projekts[Integer(Item.Data)]);
    if not EditProjekt(Projekt) then
    begin
      if Projekt.ValueExists('ImageIndex') then
        Item.ImageIndex:=Projekts[Integer(Item.Data)].GetInteger('ImageIndex')
      else
        Item.ImageIndex:=-1;
      Item.StateIndex:=Item.ImageIndex;
      Projekt.Free;
      exit;
    end;
    if not Projekts[Integer(Item.Data)].Compare(Projekt) then
    begin
      Projekts[Integer(Item.Data)].Assign(Projekt);

      StartViewer.UpdateItem(Projekt,Item);
      
      ForschChanged:=true;
    end;
    Projekt.Free;
  end;
end;

procedure TMDIForm.ActionItemEditExecute(Sender: TObject);
var
  Projekt: TExtRecord;
  Item   : TListItem;
begin
  if AlienItemsViewer.Visible then
    Item:=AlienItemsViewer.StartView.Selected
  else
    Item:=StartViewer.StartView.Selected;

  Projekt:=TExtRecord.Create(Projekts[Integer(Item.Data)].RecordDefinition);
  Projekt.Assign(Projekts[Integer(Item.Data)]);

  if not EditProjekt(Projekt) then
  begin
    if Projekt.ValueExists('ImageIndex') then
      Item.ImageIndex:=Projekts[Integer(Item.Data)].GetInteger('ImageIndex')
    else
      Item.ImageIndex:=-1;
    Item.StateIndex:=Item.ImageIndex;
    Projekt.Free;
    exit;
  end;

  if not Projekt.Compare(Projekts[Integer(Item.Data)]) then
  begin
    Projekts[Integer(Item.Data)].Assign(Projekt);

    if AlienItemsViewer.Visible then
      AlienItemsViewer.UpdateItem(Projekt,Item)
    else
      StartViewer.UpdateItem(Projekt,Item);

    ForschChanged:=true;

    if AlienItemsViewer.Visible then
      AlienItemsViewer.StartView.CustomSort(nil,0)
    else
      StartViewer.StartView.CustomSort(nil,0);
  end;
  Projekt.Free;
end;

procedure TMDIForm.AboutClick(Sender: TObject);
begin
  AboutBox.ShowModal;
end;

procedure TMDIForm.ActionList1Update(Action: TBasicAction;
  var Handled: Boolean);
var
  Dummy: Integer;
begin
  MenuProjekt.Enabled:=ProjektViewer.Visible;
  MenuStart.Enabled:=StartViewer.Visible or AlienItemsViewer.Visible;
  MenuUFO.Enabled:=UFOViewer.Visible;
  MenuAlien.Enabled:=AlienViewer.Visible;
  MenuSkript.Enabled:=SkriptViewer.Visible;

  if StartViewer.Visible then
  begin
    MenuSName.Checked:=StartViewer.StartView.Tag=0;
    MenuSTyp.Checked:=StartViewer.StartView.Tag=1;
    MenuSAnzahl.Checked:=StartViewer.StartView.Tag=2;
    MenuSAnzahl.Enabled:=true;
    MenuSVer.Checked:=StartViewer.StartView.Tag=3;
    StartViewer.MenuSName.Checked:=StartViewer.StartView.Tag=0;
    StartViewer.MenuSTyp.Checked:=StartViewer.StartView.Tag=1;
    StartViewer.MenuSAnzahl.Checked:=StartViewer.StartView.Tag=2;
    StartViewer.MenuSVer.Checked:=StartViewer.StartView.Tag=3;
  end;

  if AlienItemsViewer.Visible then
  begin
    MenuSName.Checked:=AlienItemsViewer.StartView.Tag=0;
    MenuSTyp.Checked:=AlienItemsViewer.StartView.Tag=1;
    MenuSAnzahl.Checked:=false;
    MenuSAnzahl.Enabled:=false;
    MenuSVer.Checked:=AlienItemsViewer.StartView.Tag=3;
    AlienItemsViewer.MenuSName.Checked:=AlienItemsViewer.StartView.Tag=0;
    AlienItemsViewer.MenuSTyp.Checked:=AlienItemsViewer.StartView.Tag=1;
    AlienItemsViewer.MenuSVer.Checked:=AlienItemsViewer.StartView.Tag=3;
  end;

  if UFOViewer.Visible then
  begin
    for Dummy:=0 to SortUFO.Count-1 do
    begin
      SortUFO.Items[Dummy].Checked:=SortUFO.Items[Dummy].Tag=UFOViewer.UFOList.Tag;
      UFOViewer.SortUFO.Items[Dummy].Checked:=SortUFO.Items[Dummy].Tag=UFOViewer.UFOList.Tag;
    end;
  end;
  if AlienViewer.Visible then
  begin
    for Dummy:=0 to SortAlien.Count-1 do
    begin
      SortAlien.Items[Dummy].Checked:=SortAlien.Items[Dummy].Tag=AlienViewer.AlienList.Tag;
      AlienViewer.SortAlien.Items[Dummy].Checked:=SortAlien.Items[Dummy].Tag=AlienViewer.AlienList.Tag;
    end;
  end;
end;

procedure TMDIForm.SortClick(Sender: TObject);
begin
  if StartViewer.Visible then
  begin
    StartViewer.StartView.Tag:=(Sender as TComponent).Tag;
    StartViewer.StartView.CustomSort(nil,0);
  end
  else
  begin
    AlienItemsViewer.StartView.Tag:=(Sender as TComponent).Tag;
    AlienItemsViewer.StartView.CustomSort(nil,0);
  end;
end;

procedure TMDIForm.ActionUFOEditExecute(Sender: TObject);
var
  Rec   : TExtRecord;
  Index : Integer;
  Item  : TListItem;
begin
  Rec:=TExtRecord.Create(RecordUFOModel);
  Index:=UfoViewer.UfoList.Selected.ImageIndex;
  Rec.Assign(Ufos[Index]);

  if not EditProjekt(Rec) then
  begin
    Rec.Free;
    exit;
  end;

  if not Rec.Compare(Ufos[Index]) then
  begin
    Ufos[Index].Assign(Rec);

    Item:=UfoViewer.UfoList.Selected;
    Item.Caption:=string_utils_GetLanguageString(Sprache,Rec.GetString('Name'));
    Item.SubItems[0]:=IntToStr(Rec.GetInteger('HitPoints'));
    Item.SubItems[1]:=IntToStr(Rec.GetInteger('Shield'));
    Item.SubItems[2]:=IntToStr(Rec.GetInteger('Angriff'));
    Item.SubItems[3]:=IntToStr(Rec.GetInteger('MinBesatz'));
    Item.SubItems[4]:=IntToStr(Rec.GetInteger('ZusBesatz'));
    Item.SubItems[5]:=ZahlString('%d Tag','%d Tagen',Rec.GetInteger('Ver'));

    ForschChanged:=true;
    UfoViewer.UfoList.CustomSort(nil,0);
  end;
  Rec.Free;
end;

procedure TMDIForm.ActionUFOAddExecute(Sender: TObject);
var
  OK: boolean;
  NewID: Cardinal;
  Dummy: Integer;
  Index: Integer;
  Rec  : TExtRecord;
begin
  OK:=false;
  NewId:=0;
  while not OK do
  begin
    try
      NewId:=random(High(Cardinal));
      OK:=(NewID<>0);
      for Dummy:=0 to length(UFOs)-1 do
      begin
        if UFOs[Dummy].GetCardinal('ID')=NewId then OK:=false;
      end;
    except
    end;
  end;

  Rec:=TExtRecord.Create(RecordUFOModel);
  Rec.SetCardinal('ID',NewID);
  if not EditProjekt(Rec,true) then
  begin
    Rec.Free;
    exit;
  end;

  Index:=record_utils_AddRecord(UFOs,Rec);

  UFOViewer.AddUFO(Rec,Index);
  UFOViewer.UFOList.CustomSort(nil,0);
  ForschChanged:=true;
end;

procedure TMDIForm.UFOSelected(Sender: TObject);
begin
  if (UFOViewer.Visible) then
    (Sender as TAction).Enabled:=(UFOViewer.UFOList.Selected<>nil)
  else
    (Sender as TAction).Enabled:=false;
end;

procedure TMDIForm.CanSave(Sender: TObject);
begin
  (Sender as TAction).Enabled:=ForschChanged;
end;

procedure TMDIForm.ActionUFORenameExecute(Sender: TObject);
begin
  UFOViewer.UFOList.Selected.EditCaption;
end;

procedure TMDIForm.ActionUFODeleteExecute(Sender: TObject);
var
  Index: Integer;
begin
  if MessageDlg('Soll das Objekt '''+UFOViewer.UFOList.Selected.Caption+''' gel�scht werden?',mtConfirmation,[mbYes,mbNo],0) = mrNo then
    exit;

  Index:=UFOViewer.UFOList.Selected.ImageIndex;
  UFOs[Index].Free;
  DeleteArray(Addr(UFOs),TypeInfo(TRecordArray),Index);

  UFOViewer.CreateTree;
  ForschChanged:=true;
end;

procedure TMDIForm.SortUFOClick(Sender: TObject);
begin
  UFOViewer.UFOList.Tag:=(Sender as TComponent).Tag;
  UFOViewer.UFOList.CustomSort(nil,0);
end;

procedure TMDIForm.SetFileName(const Value: String);
var
  Archiv : TArchivFile;
  LoadOK : Boolean;
begin
  if (Value=fFileName) and (not fNewSet) then exit;
  if (ForschChanged) then
  begin
    case Application.MessageBox(PChar('M�chten Sie die �nderungen an '+fFileName+' speichern?'),PChar('Frage'),MB_ICONQUESTION+MB_YESNOCANCEL) of
      ID_YES :
      begin
        if not Save then exit;
      end;
      ID_CANCEL : exit;
    end;
  end;
  ForschChanged:=false;
  fFileName := Value;
  if fFileName<>'' then
  begin
    LoadOK:=false;
    if not fNewSet then
    begin
      Archiv:=TArchivFile.Create;
      try
        Archiv.OpenArchiv('data\GameSets\'+FileName);
      except
        Archiv.Free;
        Application.MessageBox(PChar('Fehler beim �ffnen der Datei'),PChar('Fehler'),MB_OK or MB_ICONERROR);
        FileName:='';
        exit;
      end;
      LoadSetStatus:=true;
      LoadLanguages(Archiv);
      if Length(Sprachen)=0 then
      begin
        MessageDlg('F�r den Spielsatz muss die Sprache festgelegt werden.'#13#10'Der Ladevorgang wird abgebrochen',Dialogs.mtError,[mbok],0);
        Archiv.Free;
        ActionFileClose.Execute;
        exit;
      end
      else
      begin
        Loader.ReadingLanguage:='';
        LoadForschDat(Archiv);
        try
          gameset_api_LoadAliens(Archiv,Aliens);
        except
          record_utils_ClearArray(Aliens);
        end;
        try
          gameset_api_LoadUFos(Archiv,UFOs);
        except
          record_utils_ClearArray(UFOs);
        end;
        try
          gameset_api_LoadSkripts(Archiv,Skripte);
        except
          record_utils_ClearArray(Skripte);
        end;

        LoadSetStatus:=false;
        LoadOK:=true;
      end;
      Archiv.Free;
    end;
    if not LoadOK then
    begin
      FillChar(fHeader,SizeOf(fHeader),#0);
      fheader.ID:=Cardinal(round(random*GetTickCount));
      WaffenListe.SetLokalImage(nil);
      SetLength(Sprachen,0);
      record_utils_ClearArray(Projekts);
      record_utils_ClearArray(Aliens);
      record_utils_ClearArray(UFOs);
      record_utils_ClearArray(Skripte);
      SetLength(DeletedChild,0);
    end;
    EnableViewers(true);
    ProjektViewer.CreateTree;
    AlienViewer.CreateTree;
    StartViewer.CreateTree;
    RecycleViewer.GetDeleted;
    UFOViewer.CreateTree;
    AlienItemsViewer.CreateTree;
    SkriptViewer.CreateTree;
    ObjektViewer.CreateTree;
  end
  else
  begin
    Caption:='Editor - '+LGameName;
    ShowFrame(-1);
    EnableViewers(false);
    record_utils_ClearArray(Projekts);
    record_utils_ClearArray(Aliens);
    record_utils_ClearArray(UFOs);
    record_utils_ClearArray(Skripte);
    SetLength(DeletedChild,0);

    ProjektViewer.CreateTree;
    AlienViewer.CreateTree;
    StartViewer.CreateTree;
    AlienItemsViewer.CreateTree;
    UFOViewer.CreateTree;
    SkriptViewer.CreateTree;
    ObjektViewer.CreateTree;

    RecycleViewer.GetDeleted;
    ForschChanged:=false;
  end;
end;

procedure TMDIForm.IsFileOpen(Sender: TObject);
begin
  (Sender as TAction).Enabled:=fFilename<>'';
end;

procedure TMDIForm.ActionFilePropertiesExecute(Sender: TObject);
begin
  FileProperties.SetNameEdit.Text:=SatzName;
  FileProperties.AutorEdit.Text:=fHeader.Autor;
  FileProperties.OrganEdit.Text:=fHeader.Organisation;
  FileProperties.FirstPass.Text:=fHeader.Password;
  FileProperties.SecondPass.Text:=fHeader.Password;
  FileProperties.Import.Checked:=fHeader.CanImport;
  FileProperties.Memo1.Lines.Text:=fHeader.Beschreibung;
  if FileProperties.ShowModal=mrOk then
  begin
    if SatzName<>FileProperties.SetNameEdit.Text then
    begin
      SatzName:=FileProperties.SetNameEdit.Text;
      ForschChanged:=true;
    end;
    if fHeader.Password<>FileProperties.FirstPass.Text then
    begin
      fHeader.Password:=FileProperties.FirstPass.Text;
      ForschChanged:=true;
    end;
    if fHeader.Autor<>FileProperties.AutorEdit.Text then
    begin
      fHeader.Autor:=FileProperties.AutorEdit.Text;
      ForschChanged:=true;
    end;
    if fHeader.Organisation<>FileProperties.OrganEdit.Text then
    begin
      fHeader.Organisation:=FileProperties.OrganEdit.Text;
      ForschChanged:=true;
    end;
    if fHeader.CanImport<>FileProperties.Import.Checked then
    begin
      fHeader.CanImport:=FileProperties.Import.Checked;
      ForschChanged:=true;
    end;
    if fHeader.Beschreibung<>FileProperties.Memo1.Lines.Text then
    begin
      fHeader.Beschreibung:=FileProperties.Memo1.Lines.Text;
      ForschChanged:=true;
    end;
  end;
end;

procedure TMDIForm.SetSatzName(const Value: String);
begin
  fHeader.Name := Value;
  if fHeader.Name<>'' then
    Caption:='Editor - '+LGameName+' ['+fHeader.Name+']'
  else
    Caption:='Editor - '+LGameName;
end;

procedure TMDIForm.ActionFileCloseExecute(Sender: TObject);
begin
  FileName:='';
end;

procedure TMDIForm.ActionFileNewExecute(Sender: TObject);
begin
  frmNew.NewSetDialog:=true;
  if frmNew.ShowModal=mrOk then
  begin
    // Sorgt daf�r, dass beim Speichern, der Spielsatz angelegt wird und keine
    // Pr�fungen der importierten Daten durchgef�hrt werden
    fNewSet:=true;
    FileName:=frmNew.FileName.Text+'.pak';
    SatzName:=frmNew.Satzname.Text;

    ForschChanged:=true;

    fHeader.CanImport:=true;

    if frmNew.ImportBox.Checked then
      ImportGameSet;

    if Length(Sprachen)=0 then
      EditLanguages.AddLanguage(Defines.Language);

    Save;

    fNewSet:=false;

  end;
end;

function TMDIForm.EditProjekt(Projekt: TExtRecord;New: boolean): boolean;
var
  Form: TEditProjektForm;
begin
  result:=false;
  CheckClass(TForm(EditProjektForm),TEditProjektForm);

  Form:=EditProjektForm;
  Form.ExtRecord:=Projekt;

  if New then
    Form.NewProjekt;

  if Form.ShowModal=mrOK then
  begin
    Projekt.Assign(Form.ExtRecord);
    result:=true;
  end;
end;

function TMDIForm.Save: boolean;
begin
  result:=SaveDialog.StartSave;
end;

procedure TMDIForm.ShowFrame(Frame: Integer);
begin
  fFrameChange:=true;
  StartViewer.Visible:=Frame=0;
  AlienViewer.Visible:=Frame=2;
  UFOViewer.Visible:=Frame=3;
  ProjektViewer.Visible:=Frame=5;
  AlienItemsViewer.Visible:=Frame=6;
  SkriptViewer.Visible:=Frame=7;
  ObjektViewer.Visible:=Frame=8;

  ActionViewProjektList.Checked:=Frame=5;
  ActionViewAlienItems.Checked:=Frame=6;
  ActionViewAlienList.Checked:=Frame=2;
  ActionViewSkripts.Checked:=Frame=7;
  ActionViewStartList.Checked:=Frame=0;
  ActionViewUFOList.Checked:=Frame=3;
  ActionViewObjects.Checked:=Frame=8;

  ItemToolbar.Visible:=(Frame=0) or (Frame=6);
  Projektbar.Visible:=Frame=5;
  AlienBar.Visible:=Frame=2;
  UFObar.Visible:=Frame=3;
  SkriptBar.Visible:=Frame=7;

  if Frame=8 then
    // Sicherstellen, dass gel�schte Objekte ausgeblendet werden
    ObjektViewer.CreateTree;

  fFrameChange:=false;
end;

procedure TMDIForm.FormDestroy(Sender: TObject);
begin
  TBRegSavePositions(Self,HKEY_CURRENT_USER,'Software\Rich Entertainment\Edit\Toolbars');

  AlienViewer.Free;
  ProjektViewer.Free;
  StartViewer.Free;
  UFOViewer.Free;
  RecycleViewer.Free;
  SkriptViewer.Free;
  ObjektViewer.Free;

  record_utils_ClearArray(Skripte);
  record_utils_ClearArray(Projekts);
  record_utils_ClearArray(UFOs);
  record_utils_ClearArray(Aliens);
end;

procedure TMDIForm.ActionAlienAddExecute(Sender: TObject);
var
  Index   : Integer;
  Dummy   : Integer;
  OK      : boolean;
  NewID   : Cardinal;
  Rec     : TExtRecord;
begin
  OK:=false;
  NewId:=0;
  while not OK do
  begin
    try
      NewId:=random(High(Cardinal));
      OK:=(NewID<>0);
      for Dummy:=0 to high(Aliens) do
      begin
        if Aliens[Dummy].GetCardinal('ID')=NewId then OK:=false;
      end;
    except
    end;
  end;
  Rec:=TExtRecord.Create(RecordAlien);
  Rec.SetCardinal('ID',NewID);
  if not EditProjekt(Rec,true) then
  begin
    Rec.Free;
    exit;
  end;

  Index:=record_utils_AddRecord(Aliens,Rec);

  AlienViewer.AddAlien(Rec,Index);
  AlienViewer.AlienList.CustomSort(nil,0);
  ForschChanged:=true;
end;

procedure TMDIForm.ActionAlienEditExecute(Sender: TObject);
var
  ListItem  : TListItem;
  Rec       : TExtRecord;
  Index     : Integer;
begin
  Rec:=TExtRecord.Create(RecordAlien);
  Index:=AlienViewer.AlienList.Selected.ImageIndex;
  Rec.Assign(Aliens[Index]);

  if not EditProjekt(Rec) then
  begin
    Rec.Free;
    exit;
  end;
  if not Rec.Compare(Aliens[Index]) then
  begin
    Aliens[Index].Assign(Rec);

    ListItem:=AlienViewer.AlienList.Selected;
    ListItem.Caption:=string_utils_GetLanguageString(Sprache,Rec.GetString('Name'));
    ListItem.SubItems[0]:=(IntToStr(Rec.GetInteger('Ges')));
    ListItem.SubItems[1]:=(IntToStr(Rec.GetInteger('Pan')));
    ListItem.SubItems[2]:=(IntToStr(Rec.GetInteger('PSAn')));
    ListItem.SubItems[3]:=(IntToStr(Rec.GetInteger('PSAb')));
    ListItem.SubItems[4]:=(IntToStr(Rec.GetInteger('Zei')));
    ListItem.SubItems[5]:=(ZahlString('%d Tage','%d Tagen',Rec.GetInteger('Ver')));
    ListItem.SubItems[6]:=(IntToStr(AlienViewer.CalculateGesamt(Rec))+' %');
    ForschChanged:=true;
    AlienViewer.AlienList.CustomSort(nil,0);
  end;
  Rec.Free;
end;

procedure TMDIForm.SortAlienClick(Sender: TObject);
begin
  AlienViewer.AlienList.Tag:=(Sender as TComponent).Tag;
  AlienViewer.AlienList.CustomSort(nil,0);
end;

procedure TMDIForm.ActionAlienEditUpdate(Sender: TObject);
begin
  (Sender as TAction).Enabled:=AlienViewer.Visible and (AlienViewer.AlienList.Selected<>nil);
end;

function TMDIForm.GetSetName: String;
begin
  result:=fHeader.Name;
end;

procedure TMDIForm.EnableViewers(Enabled: boolean);
begin
  ActionViewStartList.Enabled:=Enabled;
  ActionViewProjektList.Enabled:=Enabled;
  ActionViewRecycleBin.Enabled:=Enabled;
  ActionViewUFOList.Enabled:=Enabled;
  ActionViewAlienList.Enabled:=Enabled;
  ActionViewRecycleBin.Enabled:=Enabled;
  ActionViewAlienItems.Enabled:=Enabled;
  ActionViewSkripts.Enabled:=Enabled;
  ActionViewObjects.Enabled:=Enabled;
end;

procedure TMDIForm.ImportForschdata(Archiv: TArchivFile; Start: TSetBool; AlienItems: Boolean);
var
  Dummy        : Integer;
  Projekt      : TExtRecord;
  Image        : Integer;
  AlienItem    : Boolean;
  NewProjects  : TRecordArray;
begin
  gameset_api_LoadProjects(Archiv,NewProjects);
  for Dummy:=0 to high(NewProjects) do
  begin
    Projekt:=NewProjects[Dummy];
    AlienItem:=false;
    if Projekt.ValueExists('AlienItem') then
    begin
      AlienItem:=Projekt.GetBoolean('AlienItem');

      if AlienItem and (not AlienItems) then
        continue;
    end;

    if (Projekt.GetBoolean('Start') in Start) or AlienItem then
    begin
      if record_utils_FindID(Projekts,Projekt.GetCardinal('ID'))=nil then
      begin
        SetLength(Projekts,length(Projekts)+1);
        SetLength(DeletedChild,length(Projekts));
        if Projekt.ValueExists('ImageIndex') then
        begin
          Image:=Projekt.GetInteger('ImageIndex');

          if Image>WaffenListe.BasisImages then
            Image:=Image+WaffenListe.LokalImages;

          Projekt.SetInteger('ImageIndex',Image);
        end;
        Projekts[high(Projekts)]:=TExtRecord.Create(Projekt.RecordDefinition);
        Projekts[high(Projekts)].Assign(Projekt);
        DeletedChild[high(Projekts)]:=false;
        ForschChanged:=true;
      end;
    end;
  end;
  record_utils_ClearArray(NewProjects);
end;

procedure TMDIForm.ImportUFOdata(Archiv: TArchivFile);
var
  Dummy  : Integer;
  Records: TRecordArray;
begin
  gameset_api_LoadUFos(Archiv,Records);
  for Dummy:=0 to high(Records) do
  begin
    if record_utils_FindID(UFOs,Records[Dummy].GetCardinal('ID'))=nil then
    begin
      SetLength(UFOs,length(UFOs)+1);
      UFOS[high(UFOs)]:=TExtRecord.Create(Records[Dummy].RecordDefinition);
      UFOS[high(UFOs)].Assign(Records[Dummy]);
      ForschChanged:=true;
    end;
  end;
  record_utils_ClearArray(Records);
end;

procedure TMDIForm.ImportAliendata(Archiv: TArchivFile);
var
  Dummy     : Integer;
  NewAliens : TRecordArray;
begin
  gameset_api_LoadAliens(Archiv,NewAliens);
  for Dummy:=0 to high(NewAliens) do
  begin
    if record_utils_FindID(Aliens,NewAliens[Dummy].GetCardinal('ID'))=nil then
    begin
      SetLength(Aliens,length(Aliens)+1);
      Aliens[high(Aliens)]:=TExtRecord.Create(NewAliens[Dummy].RecordDefinition);
      Aliens[high(Aliens)].Assign(NewAliens[Dummy]);
      ForschChanged:=true;
    end;
  end;

  record_utils_ClearArray(NewAliens);
end;

procedure TMDIForm.ImportSymbols(Archiv: TArchivFile);
var
  Bitmap: TBitmap;
  Image : TBitmap;
  Dummy : Integer;
begin
  Bitmap:=TBitmap.Create;
  try
    Archiv.OpenRessource(RNWaffenSymbols);
    Bitmap.LoadFromStream(Archiv.Stream);
  except
    Bitmap.Free;
    exit;
  end;
  Image:=TBitmap.Create;
  Image.Width:=32;
  Image.Height:=32;
  for Dummy:=0 to (Bitmap.Width div 32)-1 do
  begin
    Image.Canvas.CopyRect(Rect(0,0,32,32),Bitmap.Canvas,Rect(Dummy*32,0,(Dummy+1)*32,32));
    WaffenListe.AddBitmap(Image);
    ForschChanged:=true;
  end;
  Image.Free;
  Bitmap.Free;
end;

procedure TMDIForm.ActionFileImportExecute(Sender: TObject);
begin
  frmNew.NewSetDialog:=false;
  if frmNew.ShowModal=mrOK then
  begin
    ImportGameSet;
  end;
end;

procedure TMDIForm.ActionAlienRenameExecute(Sender: TObject);
begin
  AlienViewer.AlienList.Selected.EditCaption;
end;

procedure TMDIForm.ActionAlienDeleteExecute(Sender: TObject);
var
  Index: Integer;
begin
  if MessageDlg('Soll das Objekt '''+AlienViewer.AlienList.Selected.Caption+''' gel�scht werden?',mtConfirmation,[mbYes,mbNo],0) = mrNo then
    exit;

  Index:=AlienViewer.AlienList.Selected.ImageIndex;
  Aliens[Index].Free;
  DeleteArray(Addr(Aliens),TypeInfo(TRecordArray),Index);

  AlienViewer.CreateTree;
  ForschChanged:=true;
end;

procedure TMDIForm.ProjektsEnabled(Sender: TObject);
begin
  (Sender as TAction).Enabled:=ProjektViewer.AllProjekts.Selected<>nil;
end;

procedure TMDIForm.ActionProjektDeleteExecute(Sender: TObject);
begin
  if MessageDlg('Soll das Objekt '''+ProjektViewer.AllProjekts.Selected.Caption+''' gel�scht werden?',mtConfirmation,[mbYes,mbNo],0) = mrNo then
    exit;

  DeletedChild[Integer(ProjektViewer.AllProjekts.Selected.Data)]:=true;
  ForschChanged:=true;
  if RecycleViewer<>nil then RecycleViewer.AddDeleted(Integer(ProjektViewer.AllProjekts.Selected.Data));
  ProjektViewer.AllProjekts.Selected.Delete;
  if ProjektViewer.AllProjekts.Items.Count>0 then
  begin
    ProjektViewer.AllProjekts.ItemFocused.Selected:=true;
    ProjektViewer.AllProjektsSelectItem(Self,ProjektViewer.AllProjekts.ItemFocused,true);
  end;
end;

procedure TMDIForm.ActionProjektCopyExecute(Sender: TObject);
begin
  CopyRecordToClipboard(Projekts[Integer(ProjektViewer.AllProjekts.Selected.Data)],ClpFormatProject);
end;

procedure TMDIForm.ItemPasteEnabled(Sender: TObject);
begin
  (Sender as TAction).Enabled:=Clipboard.HasFormat(ClpFormatProject);
end;

procedure TMDIForm.ActionProjektPasteExecute(Sender: TObject);
var
  Projekt: TExtRecord;
  Name   : String;
  Dummy  : Integer;
begin
  if not Clipboard.HasFormat(ClpFormatProject) then
    exit;

  Projekt:=GetRecordFromClipboard(ClpFormatProject);
  Name:=Projekt.GetString('Name');
  if not InputQuery('Objektname','Name',Name) then
    exit;
  for Dummy:=0 to high(Projekts) do
  begin
    if (not DeletedChild[Dummy]) and (Name=Projekts[Dummy].GetString('Name')) then
    begin
      Application.MessageBox('Mit diesem Namen existiert bereits ein Objekt.','Fehler',MB_ICONWARNING OR MB_OK);
      exit;
    end;
  end;
  Projekt.SetString('Name',Name);
  Projekt.SetBoolean('Start',false);
  if Projekt.ValueExists('AlienItem') then
    Projekt.SetBoolean('AlienItem',false);
  if GetIndexOfId(MDIForm.Projekts,Projekt.GetCardinal('ID'))<>-1 then
    Projekt.SetCardinal('ID',GenerateID(Projekts));

  SetLength(MDIForm.Projekts,length(MDIForm.Projekts)+1);
  MDIForm.Projekts[High(MDIForm.Projekts)]:=Projekt;

  SetLength(DeletedChild,length(DeletedChild)+1);
  DeletedChild[high(DeletedChild)]:=false;

  ProjektViewer.AddToList(High(MDIForm.Projekts),ProjektViewer.AllProjekts,true);

  ForschChanged:=true;
end;

procedure TMDIForm.CopyRecordToClipboard(Rec: TExtRecord;Format: Integer);
var
  Handle     : THandle;
  Ptr        : PByte;
  Mem        : TMemoryStream;
begin
  Mem:=TMemoryStream.Create;
  if Format=ClpFormatProject then
    SaveProjectRecord(Rec,Mem)
  else
    Rec.SaveToStream(Mem);
  Mem.Position:=0;

  Handle:=GlobalAlloc(GMEM_FIXED,Mem.Size);
  Ptr:=GlobalLock(Handle);
  MoveMemory(Ptr,Mem.Memory,Mem.Size);
  GlobalUnlock(Handle);
  Clipboard.SetAsHandle(Format,Handle);
  Mem.Free;
end;

procedure TMDIForm.ActionItemCopyExecute(Sender: TObject);
begin
//  raise Exception.Create('Wird noch nicht unterst�tzt');
  if StartViewer.Visible then
    CopyRecordToClipboard(Projekts[Integer(StartViewer.StartView.Selected.Data)],ClpFormatProject)
  else
    CopyRecordToClipboard(Projekts[Integer(AlienItemsViewer.StartView.Selected.Data)],ClpFormatProject)
end;

function TMDIForm.GetRecordFromClipboard(Format: Integer): TExtRecord;
var
  Handle     : THandle;
  Ptr        : PByte;
  Mem        : TMemoryStream;
begin
  result:=nil;

  Handle:=Clipboard.GetAsHandle(Format);
  Ptr:=GlobalLock(Handle);
  Mem:=TMemoryStream.Create;
  Mem.Size:=GlobalSize(Handle);
  MoveMemory(Mem.Memory,Ptr,Mem.Size);
  if Format=ClpFormatProject then
    result:=ReadProjectRecord(Mem)
  else if Format=ClpFormatAlien then
  begin
    result:=TExtRecord.Create(RecordAlien);
    result.LoadFromStream(Mem);
  end
  else if Format=ClpFormatUFO then
  begin
    result:=TExtRecord.Create(RecordUFOModel);
    result.LoadFromStream(Mem);
  end
  else if Format=ClpFormatSkript then
  begin
    result:=TExtRecord.Create(RecordMissionSkript);
    result.LoadFromStream(Mem);
  end
  else
    Assert(false);
  GlobalUnlock(Handle);
  Mem.Free;
end;

procedure TMDIForm.ActionItemPasteExecute(Sender: TObject);
var
  Projekt: TExtRecord;
  Name   : String;
  Dummy  : Integer;
begin
//  raise Exception.Create('Wird noch nicht unterst�tzt');
  if not Clipboard.HasFormat(ClpFormatProject) then
    exit;
  Projekt:=GetRecordFromClipboard(ClpFormatProject);
  Assert(Projekt<>nil);
  if TProjektType(Projekt.GetInteger('TypeID'))=ptNone then
  begin
    Application.MessageBox('In der Zwischenablage befindet sich eine Technologie.'#13#10'Technolgien sind hier allerdings nicht erlaubt.','Hinweis',MB_ICONINFORMATION OR MB_OK);
    exit;
  end;
  Name:=Projekt.GetString('Name');;
  if not InputQuery('Objektname','Name',Name) then
  begin
    Projekt.Free;
    exit;
  end;
  for Dummy:=0 to high(Projekts) do
  begin
    if (not DeletedChild[Dummy]) and (Name=Projekts[Dummy].GetString('Name')) then
    begin
      Application.MessageBox('Mit diesem Namen existiert bereits ein Objekt.','Fehler',MB_ICONWARNING OR MB_OK);
      Projekt.Free;
      exit;
    end;
  end;
  Projekt.SetString('Name',Name);
  Projekt.SetBoolean('Start',true);
  if GetIndexOfId(MDIForm.Projekts,Projekt.GetCardinal('ID'))<>-1 then
    Projekt.SetCardinal('ID',GenerateID(Projekts));

  SetLength(MDIForm.Projekts,length(MDIForm.Projekts)+1);
  MDIForm.Projekts[High(MDIForm.Projekts)]:=Projekt;

  SetLength(DeletedChild,length(DeletedChild)+1);
  DeletedChild[high(DeletedChild)]:=false;

  if Projekt.ValueExists('AlienItem') then
    Projekt.SetBoolean('AlienItem',AlienItemsViewer.Visible);

  if StartViewer.Visible then
    StartViewer.AddToList(High(MDIForm.Projekts),true)
  else
    AlienItemsViewer.AddToList(High(MDIForm.Projekts),true);

  ForschChanged:=true;
end;

procedure TMDIForm.StartEnabled(Sender: TObject);
begin
  if ((StartViewer.Visible) and (StartViewer.StartView.Selected<>nil)) or
     ((AlienItemsViewer.Visible) and (AlienItemsViewer.StartView.Selected<>nil)) then
    (Sender as TAction).Enabled:=true
  else
    (Sender as TAction).Enabled:=false;
end;

procedure TMDIForm.ActionItemCutExecute(Sender: TObject);
begin
  ActionItemCopy.Execute;
  ActionItemDelete.Execute;
end;

procedure TMDIForm.ActionViewFrame(Sender: TObject);
begin
  ShowFrame((Sender As TAction).Tag);
end;

function TMDIForm.GenerateID(List: TRecordArray): Cardinal;
var
  OK    : Boolean;
  Dummy : Integer;
begin
  result:=0;
  OK:=false;
  while not OK do
  begin
    result:=random(High(Cardinal));
    OK:=(result<>0);
    for Dummy:=0 to High(List) do
    begin
      if List[Dummy].GetCardinal('ID')=result then OK:=false;
    end;
  end;
end;

procedure TMDIForm.ActionProjektCutExecute(Sender: TObject);
begin
  ActionProjektCopy.Execute;
  ActionProjektDelete.Execute;
end;

procedure TMDIForm.ActionProjektAddFatherExecute(Sender: TObject);
var
  Index : Integer;
  NewID : Cardinal;
  OwnID : Cardinal;
  Dummy : Integer;
  Parents : TParentArray;

  function CheckParents(ID: Cardinal): boolean;
  var
    Dummy   : Integer;
    Index   : Integer;
    Parents : TParentArray;
  begin
    result:=true;
    Index:=GetIndexOfId(Projekts,ID);
    if Index=-1 then exit;
    with Projekts[Index] do
    begin
      if GetCardinal('ID')=OwnID then
      begin
        result:=false;
        exit;
      end;
      GetParentArray(Projekts[Index],Parents);
      for Dummy:=0 to High(Parents) do
      begin
        result:=CheckParents(Parents[Dummy]);
        if not result then exit;
      end;
    end;
  end;

begin
  if frmSelectList.ShowModal=mrOK then
  begin
    Index:=frmSelectList.Projekt;
    if Index<>-1 then
    begin
      // Projekt ausgew�hlt
      NewID:=Projekts[Index].GetCardinal('ID');
      with Projekts[Index] do
      begin
        OwnID:=Projekts[Integer(ProjektViewer.AllProjekts.Selected.Data)].GetCardinal('ID');
        GetParentArray(Projekts[Index],Parents);
        for Dummy:=0 to High(Parents) do
        begin
          if not CheckParents(Parents[Dummy]) then
          begin
            Application.MessageBox(PChar(Format('%s ist ein Nachfolger von %s.',[Projekts[Index].GetString('Name'),Projekts[Integer(ProjektViewer.AllProjekts.Selected.Data)].GetString('Name')])),'Hinweis',MB_ICONWARNING);
            exit;
          end;
        end;
      end;
      with Projekts[Integer(ProjektViewer.AllProjekts.Selected.Data)] do
      begin
        GetParentArray(Projekts[Integer(ProjektViewer.AllProjekts.Selected.Data)],Parents);
        if NewID=GetCardinal('ID') then
        begin
          Application.MessageBox('Das sind beides die gleichen Objekte.','Hinweis',MB_ICONWARNING);
          exit;
        end;
        for Dummy:=0 to high(Parents) do
        begin
          if NewID=Parents[Dummy] then
          begin
            Application.MessageBox(PChar(Format('%s ist bereits ein Vorg�nger von %s.',[Projekts[Index].GetString('Name'),Projekts[Integer(ProjektViewer.AllProjekts.Selected.Data)].GetString('Name')])),'Hinweis',MB_ICONWARNING);
            exit;
          end;
        end;
        SetLength(Parents,System.length(Parents)+1);
        Parents[High(Parents)]:=Projekts[Index].GetCardinal('ID');
        SetParentArray(Projekts[Integer(ProjektViewer.AllProjekts.Selected.Data)],Parents);
      end;
      ProjektViewer.AddToList(Index,ProjektViewer.ParentList,false);
      ForschChanged:=true;
    end
    else
    begin
      Index:=frmSelectList.Alien;

      GetParentArray(Projekts[Integer(ProjektViewer.AllProjekts.Selected.Data)],Parents);
      NewID:=Aliens[Index].GetCardinal('ID');
      for Dummy:=0 to high(Parents) do
      begin
        if NewID=Parents[Dummy] then
        begin
          Application.MessageBox(PChar(Format('%s ist bereits ein Vorg�nger von %s.',[Aliens[Index].GetString('Name'),Projekts[Integer(ProjektViewer.AllProjekts.Selected.Data)].GetString('Name')])),'Hinweis',MB_ICONWARNING);
          exit;
        end;
      end;
      SetLength(Parents,System.length(Parents)+1);
      Parents[High(Parents)]:=Aliens[Index].GetCardinal('ID');
      SetParentArray(Projekts[Integer(ProjektViewer.AllProjekts.Selected.Data)],Parents);
      ProjektViewer.AddAlienToList(Index,ProjektViewer.ParentList,false);
      ForschChanged:=true;
    end;
  end;
end;

procedure TMDIForm.ActionProjektBackUpdate(Sender: TObject);
begin
  ActionProjektBack.Enabled:=ProjektViewer.CanBack;
end;

procedure TMDIForm.ActionProjektNextUpdate(Sender: TObject);
begin
  ActionProjektNext.Enabled:=ProjektViewer.CanNext;
end;

procedure TMDIForm.ActionProjektBackExecute(Sender: TObject);
begin
  ProjektViewer.GoBack;
end;

procedure TMDIForm.ActionProjektNextExecute(Sender: TObject);
begin
  ProjektViewer.GoNext;
end;

procedure TMDIForm.ActionProjektDeleteFatherUpdate(Sender: TObject);
begin
  ActionProjektDeleteFather.Enabled:=ProjektViewer.ParentList.Selected<>nil;
end;

procedure TMDIForm.ActionProjektDeleteFatherExecute(Sender: TObject);
var
  PDummy   : Integer;
  DelID    : Cardinal;
  Parents  : TParentArray;
begin
  if ProjektViewer.ParentList.Selected.SubItems[2]='Item' then
    DelID:=Projekts[Integer(ProjektViewer.ParentList.Selected.Data)].GetCardinal('ID')
  else
    DelID:=Aliens[Integer(ProjektViewer.ParentList.Selected.Data)].GetCardinal('ID');
    
  GetParentArray(Projekts[Integer(ProjektViewer.AllProjekts.Selected.Data)],Parents);
  for PDummy:=0 to High(Parents) do
  begin
    if Parents[PDummy]=DelID then
      Parents[PDummy]:=0;
  end;
  SetParentArray(Projekts[Integer(ProjektViewer.AllProjekts.Selected.Data)],Parents);
  ProjektViewer.ParentList.Selected.Delete;
  ForschChanged:=true;
end;

procedure TMDIForm.FormShow(Sender: TObject);
begin
  frmSymbEdit.ImageList:=WaffenListe;
end;

procedure TMDIForm.LoadLanguages(Archiv: TArchivFile);
var
  Buffer : Integer;
  Dummy  : Integer;
begin
  SetLength(Sprachen,0);
  if Archiv.ExistRessource('Languages') then
  begin
    Archiv.OpenRessource('Languages');
    Archiv.Stream.Read(Buffer,SizeOf(Buffer));
    SetLength(Sprachen,Buffer);
    for Dummy:=0 to Buffer-1 do
      Sprachen[Dummy]:=ReadString(Archiv.Stream);

  end;

  if length(Sprachen)=0 then
  begin
    if LanguageForm.ShowModal=mrOK then
    begin
      SetLength(Sprachen,1);
      Sprachen[0]:=LanguageForm.GetLanguage;
      Sprache:=LanguageForm.GetLanguage;
    end;
  end;

  if length(Sprachen)>0 then
    Sprache:=Sprachen[0];
end;

procedure TMDIForm.ActionFileLanguagesExecute(Sender: TObject);
begin
  EditLanguages.ShowModal;
end;

procedure TMDIForm.ActionSkriptAddExecute(Sender: TObject);
var
  Index   : Integer;
  Dummy   : Integer;
  OK      : boolean;
  NewID   : Cardinal;
  Rec     : TExtRecord;
begin
  OK:=false;
  NewId:=0;
  while not OK do
  begin
    NewId:=random(High(Cardinal)-1);
    OK:=(NewID<>0);
    for Dummy:=0 to high(Skripte) do
    begin
      if Skripte[Dummy].GetCardinal('ID')=NewId then OK:=false;
    end;
  end;
  Rec:=TExtRecord.Create(RecordMissionSkript);
  Rec.SetCardinal('ID',NewID);

  if not EditProjekt(Rec,true) then
  begin
    Rec.Free;
    exit;
  end;

  Index:=record_utils_AddRecord(Skripte,Rec);

  SkriptViewer.AddSkript(Rec,Index);
  ForschChanged:=true;
end;

procedure TMDIForm.ActionSkriptDeleteUpdate(Sender: TObject);
begin
  (Sender as TAction).Enabled:=SkriptViewer.Visible and (SkriptViewer.MissionsSkriptsList.Selected<>nil);
end;

procedure TMDIForm.ActionSkriptDeleteExecute(Sender: TObject);
var
  Index: Integer;
begin
  if MessageDlg('Soll das Objekt '''+SkriptViewer.MissionsSkriptsList.Selected.Caption+''' gel�scht werden?',mtConfirmation,[mbYes,mbNo],0) = mrNo then
    exit;

  Index:=Integer(SkriptViewer.MissionsSkriptsList.Selected.Data);
  Skripte[Index].Free;
  DeleteArray(Addr(Skripte),TypeInfo(TRecordArray),Index);

  SkriptViewer.CreateTree;
  ForschChanged:=true;
end;

procedure TMDIForm.ActionSkriptRenameExecute(Sender: TObject);
begin
  SkriptViewer.MissionsSkriptsList.Selected.EditCaption;
end;

procedure TMDIForm.ActionSkriptEditExecute(Sender: TObject);
var
  ListItem  : TListItem;
  Rec       : TExtRecord;
  Index     : Integer;
begin
  Rec:=TExtRecord.Create(RecordMissionSkript);
  Index:=Integer(SkriptViewer.MissionsSkriptsList.Selected.Data);
  Rec.Assign(Skripte[Index]);

  if not EditProjekt(Rec) then
  begin
    Rec.Free;
    exit;
  end;
  if not Rec.Compare(Skripte[Index]) then
  begin
    Skripte[Index].Assign(Rec);

    ListItem:=SkriptViewer.MissionsSkriptsList.Selected;
    ListItem.Caption:=string_utils_GetLanguageString(Sprache,Rec.GetString('Name'));
    ForschChanged:=true;
    SkriptViewer.SkriptToListItem(Skripte[Index],SkriptViewer.MissionsSkriptsList.Selected);
    SkriptViewer.MissionsSkriptsList.CustomSort(nil,0);
  end;
  Rec.Free;
end;

procedure TMDIForm.ImportUFOPadie(Archiv: TArchivFile);
var
  Dummy: Integer;
  GameSet : TArchivFile;
begin
  GameSet:=TArchivFile.Create;
  GameSet.OpenArchiv('data\gamesets\'+Filename);
  for Dummy:=0 to Archiv.Count-1 do
  begin
    if Copy(Archiv.Files[Dummy].Name,1,10)='UFOPaedie:' then
    begin
      Archiv.OpenRessource(Dummy);
      GameSet.AddRessource(Archiv.Stream,Archiv.Files[Dummy].Name,true);
    end;
  end;
  GameSet.Free;
end;

procedure TMDIForm.ImportSkripts(Archiv: TArchivFile);
var
  Dummy      : Integer;
  NewSkripts : TRecordArray;
begin
  gameset_api_LoadSkripts(Archiv,NewSkripts);
  for Dummy:=0 to high(NewSkripts) do
  begin
    if record_utils_FindID(Skripte,NewSkripts[Dummy].GetCardinal('ID'))=nil then
    begin
      SetLength(Skripte,length(Skripte)+1);
      Skripte[high(Skripte)]:=TExtRecord.Create(NewSkripts[Dummy].RecordDefinition);
      Skripte[high(Skripte)].Assign(NewSkripts[Dummy]);
      ForschChanged:=true;
    end;
  end;
  record_utils_ClearArray(NewSkripts);
end;

procedure TMDIForm.ImportGameSet;
var
  Archiv: TArchivFile;
  Bool: TSetBool;
begin
  if (frmNew.FileListView.Selected.SubItems[0]=Self.FileName) and (not fNewSet) then
  begin
    Application.MessageBox(PChar('Dieser Spielsatz ist zur Zeit ge�ffnet. Importieren ist nicht m�glich'),PChar('Fehler'),MB_OK or MB_ICONERROR);
    exit;
  end;

  Archiv:=TArchivFile.Create;

  Archiv.OpenArchiv('data\gameSets\'+frmNew.FileListView.Selected.SubItems[0]);

  // Sprachen importieren
  gameset_api_LoadLanguages(Archiv,Sprachen);

  if frmNew.ImportProjects.Checked or frmNew.ImportItems.Checked or frmNew.ImportAlienItems.Checked then
  begin
    Bool:=[];
    if frmNew.ImportProjects.Checked then
      Include(Bool,false);

    if frmNew.ImportItems.Checked then
      Include(Bool,true);

    ImportForschData(Archiv,bool,frmNew.ImportAlienItems.Checked);

    ProjektViewer.CreateTree;
    StartViewer.CreateTree;
    AlienItemsViewer.CreateTree;
  end;

  if frmNew.ImportUFOs.Checked then
  begin
    ImportUFOData(Archiv);
    UFOViewer.CreateTree;
  end;

  if frmNew.ImportSymbols.Checked then
    ImportSymbols(Archiv);

  if frmNew.ImportAliens.Checked then
  begin
    ImportAlienData(Archiv);
    AlienViewer.CreateTree;
  end;

  if frmNew.ImportUFOPadie.Checked then
    ImportUFOPadie(Archiv);

  if frmNew.ImportSkripts.Checked then
  begin
    ImportSkripts(Archiv);
    SkriptViewer.CreateTree;
  end;

  if fNewSet then
  begin
    Save;
    ForschChanged:=false;
  end;

  Archiv.Free;
end;

procedure TMDIForm.ApplicationEventsHint(Sender: TObject);
begin
  StatusBar.Panels[0].Text:=Application.Hint;
end;

procedure TMDIForm.ActionFileCheckGameSetExecute(Sender: TObject);
begin
  SaveDialog.CheckGameSet;
end;

procedure TMDIForm.ActionAlienPasteUpdate(Sender: TObject);
begin
  (Sender as TAction).Enabled:=Clipboard.HasFormat(ClpFormatAlien);
end;

procedure TMDIForm.ActionAlienCutExecute(Sender: TObject);
begin
  ActionAlienCopy.Execute;
  ActionAlienDelete.Execute;
end;

procedure TMDIForm.ActionAlienCopyExecute(Sender: TObject);
begin
  CopyRecordToClipboard(Aliens[Integer(AlienViewer.AlienList.Selected.ImageIndex)],ClpFormatAlien);
end;

procedure TMDIForm.ActionAlienPasteExecute(Sender: TObject);
var
  Alien  : TExtRecord;
  Name   : String;
  Dummy  : Integer;
begin
//  raise Exception.Create('Wird noch nicht unterst�tzt');
  if not Clipboard.HasFormat(ClpFormatAlien) then
    exit;

  Alien:=GetRecordFromClipboard(ClpFormatAlien);
  Assert(Alien<>nil);
  Name:=Alien.GetString('Name');

  if not InputQuery('Objektname','Name',Name) then
  begin
    Alien.Free;
    exit;
  end;

  for Dummy:=0 to high(Aliens) do
  begin
    if (Name=Aliens[Dummy].GetString('Name')) then
    begin
      Application.MessageBox('Mit diesem Namen existiert bereits ein Objekt.','Fehler',MB_ICONWARNING OR MB_OK);
      Alien.Free;
      exit;
    end;
  end;
  Alien.SetString('Name',Name);
  if GetIndexOfId(Aliens,Alien.GetCardinal('ID'))<>-1 then
    Alien.SetCardinal('ID',GenerateID(Aliens));

  SetLength(MDIForm.Aliens,length(MDIForm.Aliens)+1);
  MDIForm.Aliens[High(MDIForm.Aliens)]:=Alien;

  AlienViewer.AddAlien(Alien,High(MDIForm.Aliens));

  ForschChanged:=true;
end;

procedure TMDIForm.ActionUFOCutExecute(Sender: TObject);
begin
  ActionUFOCopy.Execute;
  ActionUFODelete.Execute;
end;

procedure TMDIForm.ActionUFOPasteUpdate(Sender: TObject);
begin
  (Sender as TAction).Enabled:=Clipboard.HasFormat(ClpFormatUFO);
end;

procedure TMDIForm.ActionUFOCopyExecute(Sender: TObject);
begin
  CopyRecordToClipboard(UFOS[Integer(UFOViewer.UFOList.Selected.ImageIndex)],ClpFormatUFO);
end;

procedure TMDIForm.ActionUFOPasteExecute(Sender: TObject);
var
  UFO    : TExtRecord;
  Name   : String;
  Dummy  : Integer;
begin
//  raise Exception.Create('Wird noch nicht unterst�tzt');
  if not Clipboard.HasFormat(ClpFormatUFO) then
    exit;

  UFO:=GetRecordFromClipboard(ClpFormatUFO);
  Assert(UFO<>nil);
  Name:=UFO.GetString('Name');

  if not InputQuery('Objektname','Name',Name) then
  begin
    UFO.Free;
    exit;
  end;

  for Dummy:=0 to high(UFOs) do
  begin
    if (Name=UFOs[Dummy].GetString('Name')) then
    begin
      Application.MessageBox('Mit diesem Namen existiert bereits ein Objekt.','Fehler',MB_ICONWARNING OR MB_OK);
      UFO.Free;
      exit;
    end;
  end;
  UFO.SetString('Name',Name);
  if GetIndexOfId(UFOs,UFO.GetCardinal('ID'))<>-1 then
    UFO.SetCardinal('ID',GenerateID(UFOs));

  SetLength(MDIForm.UFOS,length(MDIForm.UFOS)+1);
  MDIForm.UFOS[High(MDIForm.UFOS)]:=UFO;

  UFOViewer.AddUFO(UFO,High(MDIForm.UFOs));

  ForschChanged:=true;
end;

procedure TMDIForm.ActionSkriptPasteUpdate(Sender: TObject);
begin
  (Sender as TAction).Enabled:=Clipboard.HasFormat(ClpFormatSkript);
end;

procedure TMDIForm.ActionSkriptPasteExecute(Sender: TObject);
var
  Skript : TExtRecord;
  Name   : String;
  Dummy  : Integer;
begin
//  raise Exception.Create('Wird noch nicht unterst�tzt');
  if not Clipboard.HasFormat(ClpFormatSkript) then
    exit;

  Skript:=GetRecordFromClipboard(ClpFormatSkript);
  Assert(Skript<>nil);
  Name:=Skript.GetString('Name');

  if not InputQuery('Objektname','Name',Name) then
  begin
    Skript.Free;
    exit;
  end;

  for Dummy:=0 to high(Skripte) do
  begin
    if (Name=Skripte[Dummy].GetString('Name')) then
    begin
      Application.MessageBox('Mit diesem Namen existiert bereits ein Objekt.','Fehler',MB_ICONWARNING OR MB_OK);
      Skript.Free;
      exit;
    end;
  end;
  Skript.SetString('Name',Name);
  if GetIndexOfId(Skripte,Skript.GetCardinal('ID'))<>-1 then
    Skript.SetCardinal('ID',GenerateID(Skripte));

  SetLength(MDIForm.Skripte,length(MDIForm.Skripte)+1);
  MDIForm.Skripte[High(MDIForm.Skripte)]:=Skript;

  SkriptViewer.AddSkript(Skript,High(MDIForm.Skripte));

  ForschChanged:=true;
end;

procedure TMDIForm.ActionSkriptCopyExecute(Sender: TObject);
begin
  CopyRecordToClipboard(Skripte[Integer(SkriptViewer.MissionsSkriptsList.Selected.Data)],ClpFormatSkript);
end;

procedure TMDIForm.ActionSkriptCutExecute(Sender: TObject);
begin
  ActionSkriptCopy.Execute;
  ActionSkriptDelete.Execute;
end;

procedure TMDIForm.ApplicationEventsIdle(Sender: TObject;
  var Done: Boolean);
begin
  if ObjektViewer<>nil then
    ObjektViewer.Idle;
end;

procedure TMDIForm.ActionViewAsHTMLUpdate(Sender: TObject);
begin
  ActionViewAsHTML.Enabled:=Screen.ActiveControl is TListView;
end;

procedure TMDIForm.ActionViewAsHTMLExecute(Sender: TObject);
begin
  if SaveHTMLExport.Execute then
  begin
    ListViewConfHTML(Screen.ActiveControl as TListView,SaveHTMLExport.FileName,true);
    ShellExecute(Handle,'open',PChar(SaveHTMLExport.FileName),nil,nil,SW_SHOWNORMAL);
  end;
end;

initialization
  ClpFormatProject:=RegisterClipboardFormat('X-Force Projekt');
  ClpFormatAlien:=RegisterClipboardFormat('X-Force Alien');
  ClpFormatUFO:=RegisterClipboardFormat('X-Force UFO');
  ClpFormatSkript:=RegisterClipboardFormat('X-Force Skript');
end.


