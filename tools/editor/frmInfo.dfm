object InfoEdit: TInfoEdit
  Left = 517
  Top = 358
  BorderStyle = bsDialog
  Caption = 'InfoEdit'
  ClientHeight = 219
  ClientWidth = 348
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poMainFormCenter
  OnKeyPress = FormKeyPress
  OnShow = FormShow
  DesignSize = (
    348
    219)
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 8
    Top = 11
    Width = 43
    Height = 13
    Caption = 'Sprache:'
  end
  object ComboBox1: TComboBox
    Left = 64
    Top = 8
    Width = 277
    Height = 21
    Style = csDropDownList
    Anchors = [akLeft, akTop, akRight]
    ItemHeight = 13
    TabOrder = 0
    OnChange = ShowLanguageText
  end
  object EditFeld: TMemo
    Left = 8
    Top = 35
    Width = 333
    Height = 148
    Anchors = [akLeft, akTop, akRight, akBottom]
    ScrollBars = ssVertical
    TabOrder = 1
  end
  object ApplyButton: TButton
    Left = 156
    Top = 187
    Width = 89
    Height = 25
    Anchors = [akRight, akBottom]
    Caption = 'OK'
    TabOrder = 2
    OnClick = ApplyButtonClick
  end
  object CancelButton: TButton
    Left = 252
    Top = 187
    Width = 89
    Height = 25
    Anchors = [akRight, akBottom]
    Caption = 'Abbrechen'
    TabOrder = 3
    OnClick = CancelButtonClick
  end
end
