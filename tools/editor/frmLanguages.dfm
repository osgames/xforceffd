object LanguageForm: TLanguageForm
  Left = 675
  Top = 582
  BorderStyle = bsDialog
  Caption = 'Sprache'
  ClientHeight = 99
  ClientWidth = 266
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poMainFormCenter
  OnClose = FormClose
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 8
    Top = 8
    Width = 156
    Height = 13
    Caption = 'Sprache des Spielsatzes w'#228'hlen:'
  end
  object Label2: TLabel
    Left = 8
    Top = 32
    Width = 43
    Height = 13
    Caption = 'Sprache:'
  end
  object OKButton: TBitBtn
    Left = 72
    Top = 64
    Width = 89
    Height = 25
    Enabled = False
    TabOrder = 0
    Kind = bkOK
  end
  object BitBtn2: TBitBtn
    Left = 168
    Top = 64
    Width = 89
    Height = 25
    TabOrder = 1
    Kind = bkCancel
  end
  object ComboBox1: TComboBox
    Left = 61
    Top = 28
    Width = 196
    Height = 21
    ItemHeight = 13
    TabOrder = 2
    OnChange = ComboBox1Change
  end
end
