object NameEdit: TNameEdit
  Left = 440
  Top = 280
  BorderStyle = bsDialog
  Caption = 'Text bearbeiten'
  ClientHeight = 297
  ClientWidth = 363
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poMainFormCenter
  DesignSize = (
    363
    297)
  PixelsPerInch = 96
  TextHeight = 13
  object LanguageGrid: TStringGrid
    Left = 8
    Top = 8
    Width = 347
    Height = 249
    Anchors = [akLeft, akTop, akRight]
    ColCount = 2
    DefaultColWidth = 120
    DefaultRowHeight = 20
    FixedRows = 0
    Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goDrawFocusSelected, goColSizing, goEditing, goAlwaysShowEditor, goThumbTracking]
    TabOrder = 0
    OnKeyPress = LanguageGridKeyPress
    ColWidths = (
      120
      200)
  end
  object OKButton: TBitBtn
    Left = 170
    Top = 264
    Width = 89
    Height = 25
    Anchors = [akTop, akRight]
    TabOrder = 1
    OnClick = OKButtonClick
    Kind = bkOK
  end
  object CancelButton: TBitBtn
    Left = 266
    Top = 264
    Width = 89
    Height = 25
    Anchors = [akTop, akRight]
    TabOrder = 2
    Kind = bkCancel
  end
end
