unit frmUFO;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ComCtrls, KD4Utils, Menus, XForce_types, ExtCtrls, StdCtrls, ToolWin, ExtRecord,
  string_utils;

type
  TUFOForm = class(TFrame)
    UFOList: TListView;
    UFOPopup: TPopupMenu;
    PopupEditUFO: TMenuItem;
    UFOHinzufgen1: TMenuItem;
    UFOlschen1: TMenuItem;
    N2: TMenuItem;
    Umbenennen1: TMenuItem;
    N3: TMenuItem;
    SortUFO: TMenuItem;
    Name1: TMenuItem;
    Hitpoints1: TMenuItem;
    Schildpunkte1: TMenuItem;
    Angriffsstrke1: TMenuItem;
    minBesatzunh1: TMenuItem;
    zusBesatzung1: TMenuItem;
    Angriffnach1: TMenuItem;
    Gesamtstrke1: TMenuItem;
    Panel2: TPanel;
    Image1: TImage;
    Label2: TLabel;
    N1: TMenuItem;
    Alienhinzufgen1: TMenuItem;
    Kopieren1: TMenuItem;
    Einfgen1: TMenuItem;
    FilterBox: TEdit;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure UFOListDblClick(Sender: TObject);
    procedure UFOListColumnClick(Sender: TObject; Column: TListColumn);
    procedure UFOListCompare(Sender: TObject; Item1, Item2: TListItem;
      Data: Integer; var Compare: Integer);
    procedure SortClick(Sender: TObject);
    procedure UFOListEdited(Sender: TObject; Item: TListItem;
      var S: String);
    procedure FilterBoxChange(Sender: TObject);
  public
    procedure CreateTree;
    procedure AddUFO(UFO: TExtRecord; Index: Integer);
    { Public-Deklarationen }
  end;

implementation

uses frmMain;

{$R *.DFM}

{ TUFOForm }

procedure TUFOForm.CreateTree;
var
  Dummy: Integer;
begin
  UFOList.Items.beginUpdate;
  UFOList.Items.Clear;
  with MDIForm do
  begin
    for Dummy:=0 to high(UFOs) do
    begin
      if (FilterBox.Text<>'') then
      begin
        Text:=string_utils_GetLanguageString(MDIForm.Sprache,MDIForm.UFOs[Dummy].GetString('Name'));
        if (Pos(lowercase(FilterBox.Text),LowerCase(Text))=0) then
          continue;
      end;

      AddUFO(UFOs[Dummy],Dummy);
    end;
  end;
  UFOList.CustomSort(nil,0);
  UFOList.Items.EndUpdate;
end;

procedure TUFOForm.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  MDIForm.UFOViewer:=nil;
  Action:=caFree;
end;

procedure TUFOForm.UFOListDblClick(Sender: TObject);
begin
  MDIForm.ActionUFOEdit.Execute;
end;

procedure TUFOForm.UFOListColumnClick(Sender: TObject;
  Column: TListColumn);
begin
  UFOList.Tag:=Column.Index;
  UFOList.CustomSort(nil,0);
end;

procedure TUFOForm.UFOListCompare(Sender: TObject; Item1, Item2: TListItem;
  Data: Integer; var Compare: Integer);
var
  Index1,Index2: Integer;
begin
  Index1:=Item1.ImageIndex;
  Index2:=Item2.ImageIndex;
  case UFOList.Tag of
    0: Compare:=CompareText(MDIForm.UFOS[Index1].GetString('Name'),MDIForm.UFOS[Index2].GetString('Name'));
    1: Compare:=MDIForm.UFOS[Index2].GetInteger('HitPoints')-MDIForm.UFOS[Index1].GetInteger('HitPoints');
    2: Compare:=MDIForm.UFOS[Index2].GetInteger('Shield')-MDIForm.UFOS[Index1].GetInteger('Shield');
    3: Compare:=MDIForm.UFOS[Index2].GetInteger('Angriff')-MDIForm.UFOS[Index1].GetInteger('Angriff');
    4: Compare:=MDIForm.UFOS[Index2].GetInteger('MinBesatz')-MDIForm.UFOS[Index1].GetInteger('MinBesatz');
    5: Compare:=MDIForm.UFOS[Index2].GetInteger('ZusBesatz')-MDIForm.UFOS[Index1].GetInteger('ZusBesatz');
    6: Compare:=MDIForm.UFOS[Index2].GetInteger('Ver')-MDIForm.UFOS[Index1].GetInteger('Ver');
  end;
end;

procedure TUFOForm.SortClick(Sender: TObject);
begin
  UFOList.Tag:=(Sender as TComponent).Tag;
  UFOList.CustomSort(nil,0);
end;

procedure TUFOForm.UFOListEdited(Sender: TObject; Item: TListItem;
  var S: String);
var
  Index: Integer;
begin
  Index:=Item.ImageIndex;

  MDIForm.UFOS[Index].SetString('Name',string_utils_SetLanguageString(MDIForm.Sprache,MDIForm.UFOS[Index].GetString('Name'),S));
  MDIForm.ForschChanged:=true;
  UFOList.CustomSort(nil,0);
end;

procedure TUFOForm.AddUFO(UFO: TExtRecord; Index: Integer);
var
  ListItem: TListItem;
begin
  ListItem:=UFOList.Items.Add;
  with UFO do
  begin
    ListItem.Caption:=string_utils_GetLanguageString(MDIForm.Sprache,GetString('Name'));
    ListItem.ImageIndex:=Index;
    ListItem.SubItems.Add(IntToStr(GetInteger('HitPoints')));
    ListItem.SubItems.Add(IntToStr(GetInteger('Shield')));
    ListItem.SubItems.Add(IntToStr(GetInteger('Angriff')));
    ListItem.SubItems.Add(IntToStr(GetInteger('MinBesatz')));
    ListItem.SubItems.Add(IntToStr(GetInteger('ZusBesatz')));
    ListItem.SubItems.Add(ZahlString('%d Tag','%d Tagen',GetInteger('Ver')));
  end;
end;

procedure TUFOForm.FilterBoxChange(Sender: TObject);
begin
  CreateTree;
end;

end.
