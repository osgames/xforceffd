unit NewSet;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ComCtrls, StdCtrls, Buttons, ArchivFile, Koding, ExtCtrls, ExtendEdit;

type
  TfrmNew = class(TForm)
    Timer1: TTimer;
    FileListView: TListView;
    Panel2: TPanel;
    ImportProjects: TExtendCheckBox;
    ImportItems: TExtendCheckBox;
    ImportUFOs: TExtendCheckBox;
    ImportSymbols: TExtendCheckBox;
    ImportAliens: TExtendCheckBox;
    ImportUFOPadie: TExtendCheckBox;
    Panel4: TPanel;
    Panel3: TPanel;
    Satzname: TExtendEdit;
    ImportBox: TExtendCheckBox;
    FileName: TExtendEdit;
    OKButton: TBitBtn;
    CancelButton: TBitBtn;
    Panel1: TPanel;
    ImportAlienItems: TExtendCheckBox;
    ImportSkripts: TExtendCheckBox;
    procedure FormShow(Sender: TObject);
    procedure ImportBoxChange(Sender: TObject);
    procedure OKButtonClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure CancelButtonClick(Sender: TObject);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure FileListViewSelectItem(Sender: TObject; Item: TListItem;
      Selected: Boolean);
    procedure SatznameChange(Sender: TObject);
  private
    fNewSetDialog: boolean;
    procedure SetNewSetDialog(const Value: boolean);

    procedure CheckOKButton;
    { Private-Deklarationen }
  public
    property NewSetDialog: boolean read fNewSetDialog write SetNewSetDialog;
    { Public-Deklarationen }
  end;

var
  frmNew: TfrmNew;

implementation

uses XForce_types, KD4Utils, ForschUtils;

{$R *.DFM}

procedure TfrmNew.FormShow(Sender: TObject);
var
  Stat     : TSearchRec;
  Status   : Integer;
  Item     : TListItem;
  FileDate : FILETIME;
  SysTime  : TSystemTime;
  DateTime : TDateTime;
  Archiv   : TArchivFile;
  m        : TMemoryStream;
  Text     : String[30];
  SName    : String;
  Version  : Integer;
  Import   : boolean;
begin
  OKButton.Enabled:=false;
  FileListView.Items.Clear;
  Status:=FindFirst('data\gameSets\*.pak',faAnyFile,Stat);
  Archiv:=TArchivFile.Create;
  m:=TMemoryStream.Create;
  while Status=0 do
  begin
    SName:='';
    try
      m.Clear;
      Text:='';
      Import:=true;
      try
        Archiv.OpenArchiv('data\GameSets\'+Stat.Name);
        Archiv.OpenRessource('Header');
        Decode(Archiv.Stream,m);
        m.Read(Version,SizeOf(Version));
        if (Version<>HeadVersion) then
        begin
          m.Position:=0;
          m.Read(Text,SizeOf(Text));
          SName:=Text;
          Import:=true;
        end
        else
        begin
          SName:=ReadString(m);
          ReadString(m);ReadString(m);ReadString(m);  // Autor, Organisation, Beschreibung überlesen
          m.Read(Import,SizeOf(Boolean));             // CanImport lesen
        end;
      except
      end;
      Archiv.CloseArchiv;
      if Import then
      begin
        FileTimeToLocalFileTime(Stat.FindData.ftLastWriteTime,FileDate);
        FileTimeToSystemTime(FileDate,SysTime);
        DateTime := SystemTimeToDateTime(SysTime);
        Item:=FileListView.Items.Add;
        Item.Caption:=SName;
        Item.SubItems.Add(Stat.Name);
        Item.SubItems.Add(DateTimeToStr(DateTime));
      end;
    except
    end;
    Status:=FindNext(Stat);
  end;
  m.Free;
  Archiv.Free;
  FindClose(Stat);
  SatzName.Text:='';
  FileName.Text:='';
  FileListView.Color:=clWindow;
  FileListView.Enabled:=true;
  ImportBox.Checked:=true;

  ImportProjects.Checked:=true;
  ImportAliens.Checked:=true;
  ImportItems.Checked:=true;
  ImportUFOs.Checked:=true;
  ImportSymbols.Checked:=true;
  ImportUFOPadie.Checked:=true;
  ImportSkripts.Checked:=true;
  ImportAlienItems.Checked:=true;

  if Panel3.Visible then
    ClientHeight:=Panel1.Height+Panel3.Height+Panel4.Height
  else
    ClientHeight:=Panel1.Height+Panel4.Height;
end;

procedure TfrmNew.ImportBoxChange(Sender: TObject);
begin
  if ImportBox.Checked then
    FileListView.Color:=clWindow
  else
    FileListView.Color:=clBtnFace;

  FileListView.Enabled:=ImportBox.Checked;
  ImportProjects.Enabled:=ImportBox.Checked;
  ImportItems.Enabled:=ImportBox.Checked;
  ImportSymbols.Enabled:=ImportBox.Checked;
  ImportUFOs.Enabled:=ImportBox.Checked;
  ImportAliens.Enabled:=ImportBox.Checked;
  ImportUFOPadie.Enabled:=ImportBox.Checked;
  ImportAlienItems.Enabled:=ImportBox.Checked;
  ImportSkripts.Enabled:=ImportBox.Checked;

  CheckOKButton;
end;

procedure TfrmNew.OKButtonClick(Sender: TObject);
begin
  if ((not fNewSetDialog) or (ImportBox.Checked and fNewSetDialog)) then
  begin
    if FileListView.Selected=nil then exit;
  end;
  if fNewSetDialog then
  begin
    ModalResult:=mrNone;
    CheckFileName(frmNew.Filename.Text);
    if FileExists('data\gamesets\'+frmNew.FileName.Text+'.pak') then
    begin
      if Application.MessageBox(PChar('Die Datei '+frmNew.FileName.Text+'.pak existiert bereits. Möchten Sie sie überschreiben?'),PChar('Frage'),MB_YESNO or MB_ICONQUESTION)=ID_NO then
      begin
        exit;
      end;
    end;
  end;
  ModalResult:=mrOK;
end;

procedure TfrmNew.FormCreate(Sender: TObject);
begin
  Satzname.DeactiveReset;
  Filename.DeactiveReset;
end;

procedure TfrmNew.CancelButtonClick(Sender: TObject);
begin
  ModalResult:=mrCancel;
end;

procedure TfrmNew.FormKeyPress(Sender: TObject; var Key: Char);
begin
  if Key=#13 then
  begin
    Key:=#0;
    OKButton.Click;
  end
  else if Key=#27 then
  begin
    Key:=#0;
    CancelButton.Click;
  end;
end;

procedure TfrmNew.SetNewSetDialog(const Value: boolean);
begin
  fNewSetDialog := Value;
  Panel3.Visible:=Value;
  if Value then
    Caption:='Neuer Spielsatz'
  else
    Caption:='Daten importieren';
end;

procedure TfrmNew.CheckOKButton;
begin
  if fNewSetDialog then
  begin
    if ImportBox.Checked then
      OKButton.Enabled:=(FileListView.Selected<>nil) and (Satzname.Text<>'') and (FileName.Text<>'')
    else
      OKButton.Enabled:=(Satzname.Text<>'') and (FileName.Text<>'');
  end
  else
  begin
    OKButton.Enabled:=(FileListView.Selected<>nil);
  end;
end;

procedure TfrmNew.FileListViewSelectItem(Sender: TObject; Item: TListItem;
  Selected: Boolean);
begin
  CheckOKButton;
end;

procedure TfrmNew.SatznameChange(Sender: TObject);
begin
  CheckOKButton;
end;

end.
