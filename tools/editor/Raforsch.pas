unit Raforsch;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StartView, ProjektView, StdCtrls, ExtCtrls, math;

type
  TForschFrame = class(TFrame)
    Panel1: TPanel;
    Splitter1: TSplitter;
    Panel2: TPanel;
    ProjektForm: TProjektForm;
    StartForm: TStartForm;
    procedure FrameResize(Sender: TObject);
  private
    { Private-Deklarationen }
  public
    { Public-Deklarationen }
  end;

implementation

{$R *.DFM}

procedure TForschFrame.FrameResize(Sender: TObject);
begin
  Panel1.Width:=max(215,Width div 3);
end;

end.
