object ObjektForm: TObjektForm
  Left = 0
  Top = 0
  Width = 1051
  Height = 275
  Align = alClient
  Constraints.MinWidth = 174
  TabOrder = 0
  object ObjektView: TListView
    Left = 0
    Top = 23
    Width = 1051
    Height = 252
    Hint = 
      'Zeigt alle Gegenst'#228'nde, Einrichtungen und Raumschiffe zu Beginn ' +
      'des Spiels'
    Align = alClient
    Columns = <
      item
        Caption = 'Name'
        Tag = -2
        Width = 190
      end
      item
        Caption = 'Typ'
        Tag = -1
        Width = 110
      end>
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    FullDrag = True
    HideSelection = False
    ReadOnly = True
    RowSelect = True
    ParentFont = False
    PopupMenu = ColumnPopup
    SmallImages = MDIForm.ImageList1
    SortType = stData
    TabOrder = 0
    ViewStyle = vsReport
    OnColumnClick = ObjektViewColumnClick
    OnColumnDragged = ObjektViewColumnDragged
    OnCompare = ObjektViewCompare
    OnDblClick = ObjektViewDblClick
  end
  object Panel2: TPanel
    Left = 0
    Top = 0
    Width = 1051
    Height = 23
    Align = alTop
    Alignment = taLeftJustify
    BevelOuter = bvNone
    TabOrder = 1
    DesignSize = (
      1051
      23)
    object Image1: TImage
      Left = 2
      Top = 2
      Width = 16
      Height = 16
      Picture.Data = {
        055449636F6E0000010001001010000001000800680500001600000028000000
        1000000020000000010008000000000000010000000000000000000000010000
        0001000000000000000080000080000000808000800000008000800080800000
        C0C0C000C0DCC000F0CAA60000003E0000005D0000007C0000009B000000BA00
        0000D9000000F0002424FF004848FF006C6CFF009090FF00B4B4FF0000143E00
        001E5D0000287C0000329B00003CBA000046D9000055F000246DFF004885FF00
        6C9DFF0090B5FF00B4CDFF00002A3E00003F5D0000547C0000699B00007EBA00
        0093D90000AAF00024B6FF0048C2FF006CCEFF0090DAFF00B4E6FF00003E3E00
        005D5D00007C7C00009B9B0000BABA0000D9D90000F0F00024FFFF0048FFFF00
        6CFFFF0090FFFF00B4FFFF00003E2A00005D3F00007C5400009B690000BA7E00
        00D9930000F0AA0024FFB60048FFC2006CFFCE0090FFDA00B4FFE600003E1400
        005D1E00007C2800009B320000BA3C0000D9460000F0550024FF6D0048FF8500
        6CFF9D0090FFB500B4FFCD00003E0000005D0000007C0000009B000000BA0000
        00D9000000F0000024FF240048FF48006CFF6C0090FF9000B4FFB400143E0000
        1E5D0000287C0000329B00003CBA000046D9000055F000006DFF240085FF4800
        9DFF6C00B5FF9000CDFFB4002A3E00003F5D0000547C0000699B00007EBA0000
        93D90000AAF00000B6FF2400C2FF4800CEFF6C00DAFF9000E6FFB4003E3E0000
        5D5D00007C7C00009B9B0000BABA0000D9D90000F0F00000FFFF2400FFFF4800
        FFFF6C00FFFF9000FFFFB4003E2A00005D3F00007C5400009B690000BA7E0000
        D9930000F0AA0000FFB62400FFC24800FFCE6C00FFDA9000FFE6B4003E140000
        5D1E00007C2800009B320000BA3C0000D9460000F0550000FF6D2400FF854800
        FF9D6C00FFB59000FFCDB4003E0000005D0000007C0000009B000000BA000000
        D9000000F0000000FF242400FF484800FF6C6C00FF909000FFB4B4003E001400
        5D001E007C0028009B003200BA003C00D9004600F0005500FF246D00FF488500
        FF6C9D00FF90B500FFB4CD003E002A005D003F007C0054009B006900BA007E00
        D9009300F000AA00FF24B600FF48C200FF6CCE00FF90DA00FFB4E6003E003E00
        5D005D007C007C009B009B00BA00BA00D900D900F000F000FF24FF00FF48FF00
        FF6CFF00FF90FF00FFB4FF002A003E003F005D0054007C0069009B007E00BA00
        9300D900AA00F000B624FF00C248FF00CE6CFF00DA90FF00E6B4FF0014003E00
        1E005D0028007C0032009B003C00BA004600D9005500F0006D24FF008548FF00
        9D6CFF00B590FF00CDB4FF0006060600121212001F1F1F002C2C2C0039393900
        45454500525252005F5F5F006C6C6C007878780085858500929292009F9F9F00
        ABABAB00B8B8B800C5C5C500D2D2D200DEDEDE00EBEBEB00F8F8F800F0FBFF00
        A4A0A000808080000000FF0000FF000000FFFF00FF000000FF00FF00FFFF0000
        FFFFFF0000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        00000000FD7F0000F93F0000F11F0000E10F0000C1070000C1070000C1070000
        C1070000C2870000C4470000C8270000D0170000E00F0000F01F0000F83F0000
        FC7F0000}
    end
    object Label1: TLabel
      Left = 19
      Top = 4
      Width = 55
      Height = 13
      Caption = #220'bersicht'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object AlienItemsBox: TCheckBox
      Left = 214
      Top = 3
      Width = 100
      Height = 17
      Caption = 'Alienausr'#252'stung'
      Checked = True
      State = cbChecked
      TabOrder = 0
      OnClick = AlienItemsBoxClick
    end
    object ProjectBox: TCheckBox
      Left = 326
      Top = 3
      Width = 100
      Height = 17
      Caption = 'Forschungen'
      Checked = True
      State = cbChecked
      TabOrder = 1
      OnClick = AlienItemsBoxClick
    end
    object StartObjectsBox: TCheckBox
      Left = 102
      Top = 3
      Width = 105
      Height = 17
      Caption = 'Startausr'#252'stungen'
      Checked = True
      State = cbChecked
      TabOrder = 2
      OnClick = AlienItemsBoxClick
    end
    object UFOBox: TCheckBox
      Left = 438
      Top = 3
      Width = 100
      Height = 17
      Caption = 'UFOs'
      Checked = True
      State = cbChecked
      TabOrder = 3
      OnClick = AlienItemsBoxClick
    end
    object AlienBox: TCheckBox
      Left = 550
      Top = 3
      Width = 100
      Height = 17
      Caption = 'Aliens'
      Checked = True
      State = cbChecked
      TabOrder = 4
      OnClick = AlienItemsBoxClick
    end
    object FilterBox: TEdit
      Left = 880
      Top = 0
      Width = 170
      Height = 21
      Hint = 'Filter'
      Anchors = [akTop, akRight]
      TabOrder = 5
      OnChange = FilterBoxChange
    end
    object FilterTypeBox: TComboBox
      Left = 735
      Top = 0
      Width = 144
      Height = 21
      Style = csDropDownList
      Anchors = [akTop, akRight]
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ItemHeight = 13
      ParentFont = False
      TabOrder = 6
      OnChange = FilterBoxChange
    end
  end
  object ColumnPopup: TPopupMenu
    AutoHotkeys = maManual
    AutoLineReduction = maManual
    Left = 72
    Top = 56
  end
end
