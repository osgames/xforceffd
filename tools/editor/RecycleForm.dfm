object RecycledForm: TRecycledForm
  Left = 0
  Top = 0
  Width = 441
  Height = 275
  Align = alClient
  TabOrder = 0
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 441
    Height = 275
    Align = alClient
    BevelOuter = bvLowered
    TabOrder = 0
    object DeletedView: TListView
      Left = 1
      Top = 1
      Width = 439
      Height = 273
      Align = alClient
      BorderStyle = bsNone
      Color = clBtnFace
      Columns = <
        item
          Caption = 'Name'
          Width = 200
        end
        item
          Caption = 'Art'
          Width = 100
        end
        item
          Caption = 'Typ'
          Width = 110
        end>
      FullDrag = True
      MultiSelect = True
      ReadOnly = True
      RowSelect = True
      SortType = stData
      TabOrder = 0
      ViewStyle = vsReport
      OnColumnClick = DeletedViewColumnClick
      OnCompare = DeletedViewCompare
    end
  end
end
