object ForschFrame: TForschFrame
  Left = 0
  Top = 0
  Width = 443
  Height = 277
  Align = alClient
  TabOrder = 0
  OnResize = FrameResize
  object Splitter1: TSplitter
    Left = 215
    Top = 0
    Width = 3
    Height = 277
    Cursor = crHSplit
    AutoSnap = False
    MinSize = 215
    ResizeStyle = rsLine
  end
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 215
    Height = 277
    Align = alLeft
    BevelOuter = bvNone
    TabOrder = 0
    inline ProjektForm: TProjektForm
      Width = 215
      inherited Panel1: TPanel
        Width = 215
        inherited ProjektView: TTreeView
          Width = 213
        end
      end
      inherited Panel3: TPanel
        Width = 215
      end
      inherited Panel2: TPanel
        Width = 215
      end
    end
  end
  object Panel2: TPanel
    Left = 218
    Top = 0
    Width = 225
    Height = 277
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    inline StartForm: TStartForm
      Width = 225
      inherited Panel1: TPanel
        Width = 225
        inherited StartView: TListView
          Width = 223
        end
      end
      inherited Panel3: TPanel
        Width = 225
      end
      inherited Panel2: TPanel
        Width = 225
        BorderWidth = 1
      end
    end
  end
end
