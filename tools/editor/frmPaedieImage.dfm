object PaedieImage: TPaedieImage
  Left = 564
  Top = 358
  BorderStyle = bsDialog
  Caption = 'Bild f'#252'r Objekt w'#228'hlen'
  ClientHeight = 450
  ClientWidth = 434
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poMainFormCenter
  OnShow = FormShow
  DesignSize = (
    434
    450)
  PixelsPerInch = 96
  TextHeight = 13
  object Bevel1: TBevel
    Left = 275
    Top = 8
    Width = 152
    Height = 152
    Anchors = [akTop, akRight]
  end
  object Image1: TImage
    Left = 276
    Top = 9
    Width = 150
    Height = 150
    Anchors = [akTop, akRight]
    Stretch = True
  end
  object Label1: TLabel
    Left = 276
    Top = 192
    Width = 47
    Height = 13
    Caption = 'Bildgr'#246#223'e:'
  end
  object ImageSize: TLabel
    Left = 336
    Top = 192
    Width = 91
    Height = 13
    Alignment = taRightJustify
    AutoSize = False
  end
  object Shape1: TShape
    Left = 276
    Top = 168
    Width = 17
    Height = 17
    Brush.Color = clFuchsia
  end
  object Label2: TLabel
    Left = 296
    Top = 170
    Width = 66
    Height = 13
    Caption = '= Transparent'
  end
  object ListView1: TListView
    Left = 8
    Top = 8
    Width = 259
    Height = 434
    Anchors = [akLeft, akTop, akRight, akBottom]
    Columns = <
      item
        Caption = 'Bild'
        Width = 125
      end
      item
        Caption = 'Bildtyp'
        Width = 100
      end>
    ColumnClick = False
    HideSelection = False
    ReadOnly = True
    RowSelect = True
    SortType = stText
    TabOrder = 0
    ViewStyle = vsReport
    OnDblClick = ListView1DblClick
    OnSelectItem = ListView1SelectItem
  end
  object OKButton: TBitBtn
    Left = 275
    Top = 216
    Width = 152
    Height = 25
    Anchors = [akTop, akRight]
    TabOrder = 1
    OnClick = OKButtonClick
    Kind = bkOK
  end
  object BitBtn2: TBitBtn
    Left = 275
    Top = 280
    Width = 152
    Height = 25
    Anchors = [akTop, akRight]
    TabOrder = 2
    Kind = bkCancel
  end
  object BitBtn1: TBitBtn
    Left = 274
    Top = 386
    Width = 152
    Height = 25
    Anchors = [akRight, akBottom]
    Caption = 'eigenes Bild hinzuf'#252'gen'
    TabOrder = 3
    OnClick = BitBtn1Click
    NumGlyphs = 2
  end
  object DeleteBitmap: TBitBtn
    Left = 274
    Top = 418
    Width = 152
    Height = 25
    Anchors = [akRight, akBottom]
    Caption = 'eigenes Bild l'#246'schen'
    Enabled = False
    TabOrder = 4
    OnClick = DeleteBitmapClick
    NumGlyphs = 2
  end
  object NoImageButton: TBitBtn
    Left = 274
    Top = 248
    Width = 152
    Height = 25
    Anchors = [akTop, akRight]
    Caption = 'kein Bild'
    TabOrder = 5
    OnClick = NoImageButtonClick
    NumGlyphs = 2
  end
  object OpenDialog1: TOpenDialog
    DefaultExt = 'bmp'
    Filter = 'Bitmap (*.bmp)|*.bmp'
    Options = [ofHideReadOnly, ofNoChangeDir, ofAllowMultiSelect, ofPathMustExist, ofFileMustExist, ofEnableSizing]
    Title = 'UFOP'#228'die Bild w'#228'hlen ...'
    Left = 8
    Top = 8
  end
end
