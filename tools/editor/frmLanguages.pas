unit frmLanguages;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons;

type
  TLanguageForm = class(TForm)
    Label1: TLabel;
    Label2: TLabel;
    OKButton: TBitBtn;
    BitBtn2: TBitBtn;
    ComboBox1: TComboBox;
    procedure FormCreate(Sender: TObject);
    procedure ComboBox1Change(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormDestroy(Sender: TObject);
  private
    fInstalledLang: TStringList;
    { Private-Deklarationen }
  public
    function GetLanguage: String;
    { Public-Deklarationen }
  end;

var
  LanguageForm: TLanguageForm;

implementation

{$R *.DFM}

procedure TLanguageForm.FormCreate(Sender: TObject);
var
  Rec : TSearchRec;
begin
  fInstalledLang:=TStringList.Create;
  if FindFirst(IncludeTrailingBackslash(ExtractFilePath(Application.ExeName))+'language\*.dat',faAnyFile,Rec)=0 then
  begin
    repeat
      fInstalledLang.Add(ChangeFileExt(Rec.Name,''));
    until FindNext(Rec)<>0;
    FindClose(Rec);
  end;
  ComboBox1.Items.Assign(fInstalledLang);
end;

function TLanguageForm.GetLanguage: String;
begin
  result:=lowercase(ComboBox1.Text);
end;

procedure TLanguageForm.ComboBox1Change(Sender: TObject);
begin
  OKButton.Enabled:=ComboBox1.Text<>'';
end;

procedure TLanguageForm.FormShow(Sender: TObject);
begin
  ComboBox1.ItemIndex:=0;
  OKButton.Enabled:=ComboBox1.Text<>'';
end;

procedure TLanguageForm.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  ComboBox1.Items.Assign(fInstalledLang);
end;

procedure TLanguageForm.FormDestroy(Sender: TObject);
begin
  fInstalledLang.Free;
end;

end.
