object ProjektForm: TProjektForm
  Left = 0
  Top = 0
  Width = 1122
  Height = 275
  HelpContext = 11
  Align = alClient
  TabOrder = 0
  Visible = False
  object Splitter1: TSplitter
    Left = 371
    Top = 22
    Height = 253
    Color = clBtnFace
    ParentColor = False
    ResizeStyle = rsUpdate
  end
  object Splitter2: TSplitter
    Left = 748
    Top = 22
    Height = 253
    Align = alRight
    Color = clBtnFace
    ParentColor = False
    ResizeStyle = rsUpdate
  end
  object ProjectPanel: TPanel
    Left = 374
    Top = 22
    Width = 374
    Height = 253
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 2
    object Panel8: TPanel
      Left = 0
      Top = 0
      Width = 374
      Height = 21
      Align = alTop
      Alignment = taLeftJustify
      BevelOuter = bvNone
      BorderWidth = 2
      Caption = 'Projekte:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 1
    end
    object AllProjekts: TListView
      Left = 0
      Top = 23
      Width = 374
      Height = 230
      Hint = 
        'Zeigt alle Gegenst'#228'nde, Einrichtungen und Raumschiffe zu Beginn ' +
        'des Spiels'
      Align = alClient
      Columns = <
        item
          Caption = 'Name'
          Width = 170
        end
        item
          Caption = 'Typ'
          Width = 110
        end
        item
          Caption = 'Verf'#252'gbar nach'
          Width = 85
        end>
      DragMode = dmAutomatic
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      HideSelection = False
      RowSelect = True
      ParentFont = False
      PopupMenu = MenuKontext
      SmallImages = MDIForm.ImageList1
      SortType = stData
      TabOrder = 0
      ViewStyle = vsReport
      OnColumnClick = ParentListColumnClick
      OnCompare = AllProjektsCompare
      OnDblClick = AllProjektsDblClick
      OnEdited = AllProjektsEdited
      OnSelectItem = AllProjektsSelectItem
    end
    object Panel10: TPanel
      Left = 0
      Top = 21
      Width = 374
      Height = 2
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 2
    end
  end
  object ParentsPanel: TPanel
    Left = 0
    Top = 22
    Width = 371
    Height = 253
    Align = alLeft
    BevelOuter = bvNone
    TabOrder = 0
    object Panel7: TPanel
      Left = 0
      Top = 0
      Width = 371
      Height = 21
      Align = alTop
      Alignment = taLeftJustify
      BevelOuter = bvNone
      BorderWidth = 2
      Caption = 'Vorg'#228'nger:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 2
    end
    object Panel11: TPanel
      Left = 0
      Top = 21
      Width = 371
      Height = 2
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 1
    end
    object ParentList: TListView
      Left = 0
      Top = 23
      Width = 371
      Height = 230
      Hint = 
        'Zeigt alle Gegenst'#228'nde, Einrichtungen und Raumschiffe zu Beginn ' +
        'des Spiels'
      Align = alClient
      Columns = <
        item
          Caption = 'Name'
          Width = 170
        end
        item
          Caption = 'Typ'
          Width = 110
        end
        item
          Caption = 'Verf'#252'gbar nach'
          Width = 87
        end>
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      HideSelection = False
      ReadOnly = True
      RowSelect = True
      ParentFont = False
      PopupMenu = PopupMenu1
      SmallImages = MDIForm.WaffenListe
      SortType = stData
      TabOrder = 0
      ViewStyle = vsReport
      OnColumnClick = ParentListColumnClick
      OnCompare = AllProjektsCompare
      OnDblClick = ChildsListDblClick
    end
  end
  object ChildPanel: TPanel
    Left = 751
    Top = 22
    Width = 371
    Height = 253
    Align = alRight
    BevelOuter = bvNone
    TabOrder = 1
    object Panel9: TPanel
      Left = 0
      Top = 0
      Width = 371
      Height = 21
      Align = alTop
      Alignment = taLeftJustify
      BevelOuter = bvNone
      BorderWidth = 2
      Caption = 'Nachfolger:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 2
    end
    object Panel13: TPanel
      Left = 0
      Top = 21
      Width = 371
      Height = 2
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 1
    end
    object ChildsList: TListView
      Left = 0
      Top = 23
      Width = 371
      Height = 230
      Hint = 
        'Zeigt alle Gegenst'#228'nde, Einrichtungen und Raumschiffe zu Beginn ' +
        'des Spiels'
      Align = alClient
      Columns = <
        item
          Caption = 'Name'
          Width = 170
        end
        item
          Caption = 'Typ'
          Width = 110
        end
        item
          Caption = 'Verf'#252'gbar nach'
          Width = 87
        end>
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      HideSelection = False
      ReadOnly = True
      RowSelect = True
      ParentFont = False
      SmallImages = MDIForm.WaffenListe
      SortType = stData
      TabOrder = 0
      ViewStyle = vsReport
      OnColumnClick = ParentListColumnClick
      OnCompare = AllProjektsCompare
      OnDblClick = ChildsListDblClick
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 0
    Width = 1122
    Height = 22
    Align = alTop
    Alignment = taLeftJustify
    BevelOuter = bvNone
    BorderWidth = 1
    TabOrder = 3
    DesignSize = (
      1122
      22)
    object Image1: TImage
      Left = 2
      Top = 2
      Width = 16
      Height = 16
      AutoSize = True
      Picture.Data = {
        07544269746D617036030000424D360300000000000036000000280000001000
        0000100000000100180000000000000300000000000000000000000000000000
        0000FF00FFFF00FFFF00FFFF00FFFF00FFFF00FF000000000000000000FF00FF
        FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF0000
        00000000C0C0C0C0C0C0C0C0C0000000000000FF00FFFF00FFFF00FFFF00FFFF
        00FFFF00FFFF00FFFF00FF000000C0C0C0C0C0C0FF0000FF0000FF0000C0C0C0
        C0C0C0000000FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF000000C0C0
        C0FF0000C0C0C0C0C0C0C0C0C0FF0000C0C0C0000000FF00FFFF00FFFF00FFFF
        00FFFF00FFFF00FF000000C0C0C0FF0000FF0000FF0000FF0000FF0000FF0000
        FF0000C0C0C0000000FF00FFFF00FFFF00FFFF00FFFF00FF000000C0C0C0FF00
        00FFFF00FF0000FF0000FF0000FF0000FF0000C0C0C0000000FF00FFFF00FFFF
        00FFFF00FFFF00FF000000C0C0C0FF0000FFFF00FFFF00FF0000FF0000FF0000
        FF0000C0C0C0000000FF00FFFF00FFFF00FFFF00FFFF00FFFF00FF000000C0C0
        C0FF0000800000800000800000FF0000C0C0C0000000FF00FFFF00FFFF00FFFF
        00FFFF00FFFF00FFFF00FF000000C0C0C0C0C0C0FFFFFFFFFFFFFFFFFFC0C0C0
        C0C0C0000000FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF0000
        00C0C0C0FFFFFFFFFFFFFFFFFFC0C0C0000000FF00FFFF00FFFF00FFFF00FFFF
        00FFFF00FFFF00FFFF00FFFF00FFFF00FF000000C0C0C0FFFFFFC0C0C0000000
        FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
        FF000000C0C0C0FFFFFFC0C0C0000000FF00FFFF00FFFF00FFFF00FFFF00FFFF
        00FFFF00FFFF00FFFF00FFFF00FFFF00FF000000C0C0C0FFFFFFC0C0C0000000
        FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
        FF000000C0C0C0FFFFFFC0C0C0000000FF00FFFF00FFFF00FFFF00FFFF00FFFF
        00FFFF00FFFF00FFFF00FFFF00FF000000C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0
        000000FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
        FF000000000000000000000000000000FF00FFFF00FFFF00FFFF00FFFF00FFFF
        00FF}
      Transparent = True
    end
    object Label1: TLabel
      Left = 19
      Top = 4
      Width = 116
      Height = 13
      Caption = 'Forschungsprojekte:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object FilterBox: TEdit
      Left = 1103
      Top = 0
      Width = 170
      Height = 21
      Hint = 'Filter'
      Anchors = [akTop, akRight]
      TabOrder = 0
      OnChange = FilterBoxChange
    end
  end
  object MenuKontext: TPopupMenu
    Images = MDIForm.ImageList1
    OwnerDraw = True
    OnPopup = MenuKontextPopup
    Left = 128
    Top = 120
    object EditProjekt: TMenuItem
      Action = MDIForm.ActionProjektEdit
      Default = True
    end
    object PopupRename: TMenuItem
      Action = MDIForm.ActionProjektRename
    end
    object Symbols: TMenuItem
      Caption = 'Symbol festlegen'
      Hint = 'W'#228'hlen Sie ein neues Symbol'
    end
    object N2: TMenuItem
      Caption = '-'
    end
    object PopupNewProjekt: TMenuItem
      Action = MDIForm.ActionProjektNew
    end
    object PopupAddProjekt: TMenuItem
      Action = MDIForm.ActionProjektDelete
    end
    object N1: TMenuItem
      Caption = '-'
    end
    object Ausschneiden1: TMenuItem
      Action = MDIForm.ActionProjektCut
    end
    object Kopieren1: TMenuItem
      Action = MDIForm.ActionProjektCopy
    end
    object Einfgen1: TMenuItem
      Action = MDIForm.ActionProjektPaste
    end
    object N3: TMenuItem
      Caption = '-'
    end
    object Vorgngerhinzufgen2: TMenuItem
      Action = MDIForm.ActionProjektAddFather
    end
    object Vorgngerlschen2: TMenuItem
      Action = MDIForm.ActionProjektDeleteFather
    end
  end
  object PopupMenu1: TPopupMenu
    Images = MDIForm.ImageList1
    Left = 56
    Top = 46
    object Vorgngerlschen1: TMenuItem
      Action = MDIForm.ActionProjektDeleteFather
    end
    object Vorgngerhinzufgen1: TMenuItem
      Action = MDIForm.ActionProjektAddFather
    end
  end
end
