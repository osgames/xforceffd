object EditLanguages: TEditLanguages
  Left = 569
  Top = 347
  BorderStyle = bsDialog
  Caption = 'Sprachen bearbeiten'
  ClientHeight = 338
  ClientWidth = 376
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poMainFormCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object ListBox1: TListBox
    Left = 8
    Top = 8
    Width = 361
    Height = 289
    Style = lbOwnerDrawFixed
    ItemHeight = 16
    TabOrder = 0
    OnDblClick = ListBox1DblClick
    OnDrawItem = ListBox1DrawItem
  end
  object Button1: TButton
    Left = 296
    Top = 304
    Width = 75
    Height = 25
    Cancel = True
    Caption = 'Schliessen'
    TabOrder = 1
    OnClick = Button1Click
  end
  object Button2: TButton
    Left = 8
    Top = 304
    Width = 89
    Height = 25
    Caption = 'neue Sprache'
    TabOrder = 2
    OnClick = Button2Click
  end
  object Button3: TButton
    Left = 200
    Top = 304
    Width = 89
    Height = 25
    Caption = 'Importieren'
    TabOrder = 3
    OnClick = Button3Click
  end
  object Button4: TButton
    Left = 104
    Top = 304
    Width = 89
    Height = 25
    Caption = 'Exportieren'
    TabOrder = 4
    OnClick = Button4Click
  end
  object OpenDialog1: TOpenDialog
    DefaultExt = 'dat'
    Filter = 'Sprachdatei (*.dat)|*.dat|Alle Dateien (*.*)|*.*'
    Options = [ofHideReadOnly, ofNoChangeDir, ofPathMustExist, ofFileMustExist, ofEnableSizing]
    Left = 8
    Top = 8
  end
  object SaveDialog1: TSaveDialog
    DefaultExt = 'dat'
    Filter = 'Sprachdatei (*.dat)|*.dat|Alle Dateien (*.*)|*.*'
    Options = [ofOverwritePrompt, ofHideReadOnly, ofNoChangeDir, ofEnableSizing]
    Left = 40
    Top = 8
  end
end
