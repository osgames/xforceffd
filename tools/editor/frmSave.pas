unit frmSave;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, XForce_types, ArchivFile, Defines, ExtCtrls, Koding, ObjektRecords,
  ComCtrls, math, EditConst, ExtRecord, ProjektRecords;

type
  TProjektFunction = procedure(Index: Integer) of object;

  TMessageType = (mtNone,mtHinweis,mtWarning,mtError);

  PMessageRecord = ^TMessageRecord;

  TMessageRecord = record
    ObjektName : String;
    Assistent  : Boolean;
    MessageTyp : TMessageType;
    FehlerID   : Integer;
    Index      : Integer;
  end;

  TCheckInformations = record
    HasLagerRoom   : double;
    NeedLagerRoom  : double;
    HasHangerRoom  : Integer;
    NeedHangerRoom : Integer;
  end;

  TSaveDialog = class(TForm)
    OKButton: TBitBtn;
    Panel1: TPanel;
    ListView1: TListView;
    RetryButton: TBitBtn;
    EditButton: TBitBtn;
    SaveButton: TBitBtn;
    Panel2: TPanel;
    CloseButton: TBitBtn;
    Label1: TLabel;
    procedure ListView1DblClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure OKButtonClick(Sender: TObject);
    procedure EditButtonClick(Sender: TObject);
    procedure RetryButtonClick(Sender: TObject);
    procedure CorrectFehler(Message: TMessageRecord);

    function IsProjektInList(ID: Cardinal): Boolean;
    function IsAlienInList(ID: Cardinal): Boolean;
    procedure ListView1ColumnClick(Sender: TObject; Column: TListColumn);
    procedure ListView1Compare(Sender: TObject; Item1, Item2: TListItem;
      Data: Integer; var Compare: Integer);
    procedure ListView1SelectItem(Sender: TObject; Item: TListItem;
      Selected: Boolean);
    procedure CloseButtonClick(Sender: TObject);
  private
    fHinweise    : Integer;
    fFehler      : Integer;
    fWarnungen   : Integer;
    ReCorrect    : boolean;
    FehlerRecord : TMessageRecord;
    procedure ClearList;
    procedure AddMessage(Mess: String; Typ: TMessageType);overload;
    procedure AddMessage(Mess,Objekt: String; Typ: TMessageType;Index: Integer; FehlerID: Integer);overload;

    function StartCheck: TMessageType;
    function CheckForschung: TMessageType;
    function CheckSkripts: TMessageType;

    function GetObjektName(Index: Integer): String;
    function GetWaffeOfMunition(Index: Integer): Integer;
    procedure DeleteObjekt(Sender: TObject);
    procedure EditSkriptObjekt(Sender: TObject);
    procedure EditFehlerObjekt(Sender: TObject);
    procedure EditFehlerWaffe(Sender: TObject);
    procedure RestoreMunitionstyp(Sender: TObject);
    procedure RestoreWaffentyp(Sender: TObject);

    { Private-Deklarationen }
  public
    function StartSave: boolean;
    procedure CheckGameSet;
    { Public-Deklarationen }
  end;

var
  SaveDialog: TSaveDialog;

implementation

uses frmMain, KD4Utils, frmInfo, string_utils, archiv_utils,frmErrorAssist,
  register_scripttypes, uPSCompiler, script_utils, SkriptEdit;

{$R *.DFM}

var
  GameSetFile : String;
  ErrorForm    : TFehlerAssistent;

procedure TSaveDialog.AddMessage(Mess: String;Typ: TMessageType);
var
  Item    : TListItem;
  MessData: PMessageRecord;
begin
  Item:=ListView1.Items.Add;
  case Typ of
    mtHinweis :
    begin
      Item.Caption:='Hinweis';
      Item.ImageIndex:=17;
      inc(fHinweise);
    end;
    mtWarning :
    begin
      Item.Caption:='Warnung';
      Item.ImageIndex:=30;
      inc(fWarnungen);
    end;
    mtError :
    begin
      Item.Caption:='Fehler';
      Item.ImageIndex:=14;
      inc(fFehler);
    end;
  end;

  GetMem(MessData,SizeOf(TMessageRecord));

  MessData.Assistent:=false;
  MessData.MessageTyp:=Typ;

  Item.Data:=MessData;
  Item.SubItems.Add('');
  Item.SubItems.Add(Mess);
end;

procedure TSaveDialog.AddMessage(Mess, Objekt: String; Typ: TMessageType;
  Index: Integer; FehlerID: Integer);
var
  Item     : TListItem;
  MessData : PMessageRecord;
begin
  Item:=ListView1.Items.Add;
  case Typ of
    mtHinweis :
    begin
      Item.Caption:='Hinweis';
      Item.ImageIndex:=17;
      inc(fHinweise);
    end;
    mtWarning :
    begin
      Item.Caption:='Warnung';
      Item.ImageIndex:=30;
      inc(fWarnungen);
    end;
    mtError :
    begin
      Item.Caption:='Fehler';
      Item.ImageIndex:=14;
      inc(fFehler);
    end;
  end;
  New(MessData);
  MessData.Assistent:=true;
  MessData.FehlerID:=FehlerID;
  MessData.Index:=Index;
  MessData.MessageTyp:=Typ;
  MessData.ObjektName:=Objekt;

  Item.Data:=MessData;
  Item.SubItems.Add(Objekt);
  Item.SubItems.Add(Mess);
end;

procedure TSaveDialog.ClearList;
begin
  ListView1.Items.BeginUpdate;
  while (ListView1.Items.Count>0) do
  begin
    if ListView1.Items[0].Data<>nil then
      Dispose(PMessageRecord(ListView1.Items[0].Data));
    ListView1.Items[0].Delete;
  end;
  ListView1.Items.EndUpdate;

  fHinweise:=0;
  fWarnungen:=0;
  fFehler:=0;
end;

function TSaveDialog.StartCheck: TMessageType;
begin
  result:=CheckForschung;

  case CheckSkripts of
    mtNone    : ; // Mache nichts
    mtHinweis : if result<mtHinweis then result:=mtHinweis;
    mtWarning : if result<mtWarning then result:=mtWarning;
    mtError   : if result<mtError then result:=mtError;
  end;

  if ListView1.Items.Count=0 then
    Height:=200
  else
    Height:=(Height-ListView1.Height)+(min(ListView1.Items.Count,20)*17)+23;

  Panel2.Caption:=Format('%d Hinweise, %d Warnungen, %d Fehler',[fHinweise,fWarnungen,fFehler]);

  Label1.Visible:=result=mtError;

  ListView1.Items.EndUpdate;
  ListView1.CustomSort(nil,0);
end;

function TSaveDialog.CheckForschung: TMessageType;
var
  Dummy,Dummy1  : Integer;
  Text          : String;
  Index         : Integer;
  StringList    : TStringList;
  Parents       : TParentArray;
  NewLength     : Integer;
  Archiv        : TArchivFile;
  Found         : Boolean;
  CheckInfos    : TCheckInformations;

  function GetObjektName(List: TRecordArray; Index: Integer = -1): String;
  begin
    if Index=-1 then
      Index:=Dummy;

    result:=string_utils_GetLanguageString(MDIForm.Sprache,List[Index].GetString('Name'));
  end;

  procedure CheckImage(Rec: TExtRecord; ImageProp: String; Error: Boolean = false);
  var
    Sevrity: TMessageType;
  begin
    if Rec.ValueExists(ImageProp) then
    begin
      if Rec.GetString(ImageProp)=EmptyStr then
      begin
        if Error then
        begin
          result:=mtError;
          Sevrity:=mtError;
        end
        else
        begin
          if result<mtHinweis then
            result:=mtHinweis;

          Sevrity:=mtHinweis;
        end;
        AddMessage('Diesem Objekt ist kein '''+Rec.GetValueProperty(Rec.GetValueIndex(ImageProp),'Name')+''' zugewiesen',GetObjektName(MDIForm.Projekts),Sevrity,Dummy,ENNoImage);
      end
      else
      begin
        Archiv:=archiv_utils_OpenSpecialRessource(Rec.GetString(ImageProp),GameSetFile);
        if Archiv=nil then
        begin
          if Error then
          begin
            result:=mtError;
            Sevrity:=mtError;
          end
          else
          begin
            if result<mtWarning then
              result:=mtWarning;

            Sevrity:=mtWarning;
          end;
          AddMessage('Diesem Objekt ist ein ung�ltiges '''+Rec.GetValueProperty(Rec.GetValueIndex(ImageProp),'Name')+''' zugewiesen',GetObjektName(MDIForm.Projekts), Sevrity,Dummy,ENInvalidImage);
        end
        else
          Archiv.Free;
      end;
    end;
  end;

  procedure CheckSound(Rec: TExtRecord; SoundProp: String);
  begin
    if Rec.ValueExists(SoundProp) then
    begin
      if Rec.GetString(SoundProp)=EmptyStr then
      begin
        AddMessage('Diesem Objekt ist kein '''+Rec.GetValueProperty(Rec.GetValueIndex(SoundProp),'Name')+'''-Sound zugewiesen',GetObjektName(MDIForm.Projekts),mtHinweis,Dummy,ENNoSound);
        if result<mtHinweis then result:=mtHinweis;
      end
      else
      begin
        Archiv:=archiv_utils_OpenSpecialRessource(Rec.GetString(SoundProp),GameSetFile);
        if Archiv=nil then
        begin
          AddMessage('Diesem Objekt ist ein ung�ltiger '''+Rec.GetValueProperty(Rec.GetValueIndex(SoundProp),'Name')+'''-Sound zugewiesen',GetObjektName(MDIForm.Projekts),mtWarning,Dummy,ENInvalidSound);
          if result<mtWarning then
            result:=mtWarning;
        end
        else
          Archiv.Free;
      end;
    end;
  end;

begin
  FillChar(CheckInfos,sizeOf(TCheckInformations),#0);
  // Pr�fungen des Spielsatzes durchf�hren
  ListView1.Items.BeginUpdate;
  result:=mtNone;
  with MDIForm do
  begin
    for Dummy:=0 to length(Projekts)-1 do
    begin
      if DeletedChild[Dummy] then
      begin
        continue;
      end;

      // UFOP�die-Text vorhanden
      if (Projekts[Dummy].GetString('Info')=EmptyStr) then
      begin
        AddMessage('Zu dem Objekt existiert keine Beschreibung f�r die UFOP�die',GetObjektName(Projekts),mtHinweis,Dummy,ENNoInfo);
        if result<mtHinweis then result:=mtHinweis;
      end;

      // Forschungs-Text vorhanden
      if (Projekts[Dummy].GetString('ResearchInfo')=EmptyStr) and ((Projekts[Dummy].GetBoolean('Start')=false) or ((Projekts[Dummy].ValueExists('AlienItem') and Projekts[Dummy].GetBoolean('AlienItem'))))then
      begin
        AddMessage('Zu dem Objekt existiert keine Beschreibung f�r die Forschung',GetObjektName(Projekts),mtHinweis,Dummy,ENNoResearchInfo);
        if result<mtHinweis then result:=mtHinweis;
      end;

      // Pr�fen des UFOP�die Bildes
      CheckImage(Projekts[Dummy],'UFOPaedieImage');

      // Basisbau-Bilder pr�fen
      if TProjektType(Projekts[Dummy].GetInteger('TypeID'))=ptEinrichtung then
      begin
        case TRoomTyp(Projekts[Dummy].GetInteger('RoomTyp')) of
          rtHangar,rtLivingRoom:
          begin
            CheckImage(Projekts[Dummy],'GrafikBasement',true);
            CheckImage(Projekts[Dummy],'GrafikUnderground',true);
          end;
          rtDefense,rtSensor,rtDefenseShield:
            CheckImage(Projekts[Dummy],'GrafikBasement',true);
          rtBaseRooms:
            CheckImage(Projekts[Dummy],'GrafikUnderground',true);
        end;
      end;

      // Pr�fen des Sounds
      CheckSound(Projekts[Dummy],'ShootingSound');

      // Symbol pr�fen
      if Projekts[Dummy].ValueExists('ImageIndex') then
      begin
        if (Projekts[Dummy].GetInteger('ImageIndex')<0) or (Projekts[Dummy].GetInteger('ImageIndex')>=MDIForm.WaffenListe.Count) then
        begin
          AddMessage('Diesem Objekt ist ein ung�ltiges Symbol zugewiesen',GetObjektName(Projekts),mtWarning,Dummy,ENInvalidSymbol);
          if result<mtWarning then result:=mtWarning;
        end;
      end;

      // Start-Anzahl korrigieren, wenn es sich um ein Forschungsprojekt handelt
      if (not Projekts[Dummy].GetBoolean('Start')) and (Projekts[Dummy].GetInteger('Anzahl')<>0) then
      begin
        Projekts[Dummy].setInteger('Anzahl',0);
      end;

      // Munition pr�fen
      if (TProjektType(Projekts[Dummy].GetInteger('TypeID')) in [ptMunition,ptRMunition]) then
      begin
        Index:=GetIndexOfID(Projekts, Projekts[Dummy].GetCardinal('MunFor'));
        if Index=-1 then
        begin
          AddMessage('Die Munition ist keiner Waffe zugeordnet.',GetObjektName(Projekts),mtError,Dummy,ENKeineWaffe);
          result:=mtError;
        end
        else
        begin
          if (TProjektType(Projekts[Dummy].GetInteger('TypeID'))=ptMunition) and (TProjektType(Projekts[Index].GetInteger('TypeID'))<>ptWaffe) then
          begin
            AddMessage('Der Munition ist keine g�ltige Waffe zugewiesen.',GetObjektName(Projekts),mtError,Dummy,ENKeineWaffe);
            result:=mtError;
          end
          else if (TProjektType(Projekts[Dummy].GetInteger('TypeID'))=ptRMunition) and (TProjektType(Projekts[Index].GetInteger('TypeID'))<>ptRWaffe) then
          begin
            AddMessage('Der Raumschiffmunition ist keine g�ltige Waffe zugewiesen.',GetObjektName(Projekts),mtError,Dummy,ENKeineWaffe);
            result:=mtError;
          end
          else if TWaffenType(Projekts[Index].GetInteger('WaffType')) in NoMunition then
          begin
            AddMessage(Format('Der Munition ist einer Waffe (%s) zugeordnet, die keine Munition ben�tigt.',[GetObjektName(Projekts,Index)]),GetObjektName(Projekts),mtError,Dummy,ENLaserWaffe);
            result:=mtError;
          end;
          if TWaffenType(Projekts[Index].GetInteger('WaffType'))<>TWaffenType(Projekts[Dummy].GetInteger('WaffType')) then
          begin
            AddMessage(Format('Die Art der Munition unterscheidet sich von der Art der Waffe (%s).',[GetObjektName(Projekts,Index)]),GetObjektName(Projekts),mtError,Dummy,ENDifferentTyp);
            result:=mtError;
          end;
          if (Projekts[Index].GetInteger('Strength')=0) and (Projekts[Dummy].GetInteger('Strength')=0) then
          begin
            AddMessage(Format('Die Munitionsst�rke ist abh�ngig von der Waffe (%s), allerdings ist die Waffenst�rke abh�ngig von der Munition.',[GetObjektName(Projekts,Index)]),GetObjektName(Projekts),mtError,Dummy,ENNoStrength);
            result:=mtError;
          end;
          if (Projekts[Dummy].GetBoolean('Start')) and (not Projekts[Index].GetBoolean('Start')) then
          begin
            AddMessage('Eine Startmunition kann keine Waffe zugewiesen werden, die noch erforscht werden muss.',GetObjektName(Projekts),mtError,Dummy,ENNoStartWaffe);
            result:=mtError;
          end;
        end;
      end;

      if (TProjektType(Projekts[Dummy].GetInteger('TypeID'))=ptRWaffe) and (not (TWaffenType(Projekts[Dummy].GetInteger('WaffType')) in NoMunition)) then
      begin
        StringList:=TStringList.Create;
        for Dummy1:=0 to length(Projekts)-1 do
        begin
          if Projekts[Dummy1].ValueExists('MunFor') then
          begin
            if (TProjektType(Projekts[Dummy1].GetInteger('TypeID')) in [ptMunition,ptRMunition]) and (Projekts[Dummy1].GetCardinal('MunFor')=Projekts[Dummy].GetCardinal('ID')) then
            begin
              StringList.Add(GetObjektName(Projekts));
            end;
          end;
        end;
        if StringList.Count=0 then
        begin
          AddMessage(Format('%s: F�r die Raumschiffwaffe existiert keine Munition.',[GetObjektName(Projekts)]),mtError);
          result:=mtError;
        end
        else if StringList.Count>1 then
        begin
          Text:='';
          for Dummy1:=0 to StringList.Count-1 do
          begin
            if length(Text)>0 then
              Text:=Text+' ; ';
            Text:=Text+StringList[Dummy1];
          end;
          AddMessage(Format('%s: F�r die Raumschiffwaffe existiert mehr als eine Munition ( %s ).',[GetObjektName(Projekts),Text]),mtError);
          result:=mtError;
        end;
        StringList.Free;
      end;

      if (TProjektType(Projekts[Dummy].GetInteger('TypeID'))=ptWaffe) and (TWaffenType(Projekts[Dummy].GetInteger('WaffType')) in NoMunition) and (Projekts[Dummy].GetInteger('Strength')=0) then
      begin
        AddMessage('Bei dieser Waffe ohne Munition, ist keine Angriffsst�rke angegeben.',GetObjektName(Projekts),mtError,Dummy,ENNoWaffenStrength);
        result:=mtError;
      end;

      // Technologien als Start-Objekte?
      if Projekts[Dummy].GetBoolean('Start') and (TProjektType(Projekts[Dummy].GetInteger('TypeID'))=ptNone) then
      begin
        AddMessage('Technologien werden bei Ausr�stungen ignoriert.',GetObjektName(Projekts),mtHinweis,Dummy,-1);
        if result<mtHinweis then result:=mtHinweis;
      end;

      // Parentliste korriegieren
      if (Projekts[Dummy].GetInteger('ParentCount')>0) then
      begin
        GetParentArray(Projekts[Dummy],Parents);
        NewLength:=length(Parents);
        for Dummy1:=high(Parents) downto 0 do
        begin
          if (not IsProjektInList(Parents[Dummy1])) and (not IsAlienInList(Parents[Dummy1])) then
          begin
            Parents[Dummy1]:=Parents[NewLength-1];
            dec(NewLength);
          end;
        end;
        SetLength(Parents,NewLength);
        SetParentArray(Projekts[Dummy],Parents);
      end;
      
      // Verf�gbarer / ben�tigter Raum speichern
      if TProjektType(Projekts[Dummy].GetInteger('TypeID'))=ptEinrichtung then
      begin
        CheckInfos.HasLagerRoom:=CheckInfos.HasLagerRoom+(Projekts[Dummy].GetInteger('Anzahl')*Projekts[Dummy].GetInteger('LagerRaum'));
        CheckInfos.HasHangerRoom:=CheckInfos.HasHangerRoom+(Projekts[Dummy].GetInteger('Anzahl')*Projekts[Dummy].GetInteger('SmallSchiff'));
      end;

      if TProjektType(Projekts[Dummy].GetInteger('TypeID'))=ptRaumschiff then
      begin
        if Projekts[Dummy].GetBoolean('Start') then
          inc(CheckInfos.NeedHangerRoom,Projekts[Dummy].GetInteger('Anzahl'));
      end;

      if TProjektType(Projekts[Dummy].GetInteger('TypeID')) in LagerItemType then
      begin
        if Projekts[Dummy].GetBoolean('Start') then
          CheckInfos.NeedLagerRoom:=CheckInfos.NeedLagerRoom+(Projekts[Dummy].GetInteger('Anzahl')*Projekts[Dummy].GetDouble('LagerV'));
      end;
    end;

    StringList:=TStringList.Create;
    StringList.Duplicates:=dupError;
    for Dummy:=0 to length(Projekts)-1 do
    begin
      try
        StringList.Add(IntToStr(Projekts[Dummy].GetCardinal('ID')));
      except
        AddMessage(GetObjektName(Projekts)+': ID ist doppelt vergeben.',mtError);
        result:=mtError;
      end;
    end;
    StringList.Free;

    // Aliens pr�fen
    for Dummy:=0 to high(Aliens) do
    begin
      Found:=false;
      for Dummy1:=0 to high(Projekts) do
      begin
        if Projekts[Dummy1].ValueExists('AlienItem') and Projekts[Dummy1].GetBoolean('AlienItem') then
        begin
          if (Projekts[Dummy1].GetInteger('TypeID')=Integer(ptWaffe)) and (Projekts[Dummy1].GetInteger('NeededIQ')<=Aliens[Dummy].GetInteger('IQ')) then
          begin
            Found:=true;
            break;
          end;
        end;
      end;
      if not Found then
      begin
        AddMessage('Das Alien kann aufgrund der zu niedrigen Intelligenz, keine der definierten Waffen benutzen.',GetObjektName(Aliens),mtError,Dummy,EAlienNoWeapon);
        result:=mtError;
      end;
    end;

    // UFOs pr�fen
    for Dummy:=0 to high(UFOs) do
    begin
      if UFOs[Dummy].GetInteger('Probability')=0 then
      begin
        AddMessage('Das UFO hat eine Wahrscheinlichkeit von 0 Prozent und wird nie angreifen.',GetObjektName(UFOS),mtWarning,Dummy,EUFONotAttacking);
        if result<mtWarning then result:=mtWarning;
      end;
    end;
  end;

  if CheckInfos.NeedLagerRoom>CheckInfos.HasLagerRoom then
  begin
    AddMessage(Format('Es wird mehr Lagerraum (%.0n) ben�tigt, als in der Startbasis zur Verf�gung (%.0n) steht.',[CheckInfos.NeedLagerRoom,CheckInfos.HasLagerRoom]),mtError);
    result:=mtError;
  end;

  if CheckInfos.NeedHangerRoom>CheckInfos.HasHangerRoom then
  begin
    AddMessage(Format('Es sind mehr Raumschiffe bei Spielstart (%d) vorhanden, als Hangarr�ume (%d) zur Verf�gung stehen.',[CheckInfos.NeedHangerRoom,CheckInfos.HasHangerRoom]),mtError);
    result:=mtError;
  end;

  if MDIForm.SetHead.Beschreibung=EmptyStr then
  begin
    AddMessage('Keine Beschreibung f�r den Spielsatz angegeben',mtWarning);
    if result<mtWarning then result:=mtWarning;
  end;
end;

function TSaveDialog.CheckSkripts: TMessageType;
var
  Dummy: Integer;
  Comp : TIFPSPascalCompiler;

  Skript  : String;
begin
  result:=mtNone;

  // Compiler Objekt erzeugen
  Comp:=TIFPSPascalCompiler.Create;
  Comp.OnUses:=RegisterCompiler;


  with MDIForm do
  begin
    for Dummy:=0 to high(Skripte) do
    begin
      Skript:=Skripte[Dummy].GetString('Skript');
      Skript:=script_utils_Preprocess(Skript,MDIForm.Sprache);
      if not Comp.Compile(Skript) then
        AddMessage('Fehler im Skript: '+Comp.Msg[0].ShortMessageToString,Skripte[Dummy].GetString('Name'),mtWarning,Dummy,EInvalidScript);
    end;
  end;
  Comp.Free;
end;

procedure TSaveDialog.CheckGameSet;
begin
  ClearList;

  GameSetFile:='data\gameSets\'+MDIForm.FileName;

  RetryButton.Visible:=StartCheck<>mtNone;
  EditButton.Visible:=false;
  SaveButton.Visible:=false;
  OKButton.Visible:=false;
  CloseButton.Visible:=true;

  ShowModal;

  RetryButton.Visible:=true;
  EditButton.Visible:=true;
  SaveButton.Visible:=true;
  OKButton.Visible:=true;
  CloseButton.Visible:=false;
end;

function TSaveDialog.StartSave: boolean;
var
  f          : TMemoryStream;
  m          : TMemoryStream;
  Archiv     : TArchivFile;
  Header     : TEntryHeader;
  Dummy      : Integer;
  Name       : String[30];
  CRC        : Cardinal;
  Wert       : Cardinal;
  Correct    : TMessageType;
  Speichern  : boolean;
begin
  if not MDIForm.ForschChanged then
  begin
    result:=false;
    exit;
  end;

  GameSetFile:='data\gameSets\'+MDIForm.FileName;

  ClearList;
  Speichern:=false;

  if MDIForm.NewSet then
    Correct:=mtNone
  else
    Correct:=StartCheck;

  result:=false;

  if Correct<>mtNone then
  begin
    MDIForm.ShowFrame(-1);
    RetryButton.Enabled:=true;
    case Correct of
      mtHinweis,mtWarning: SaveButton.Enabled:=true;
      mtError  : SaveButton.Enabled:=false;
    end;

    if ShowModal=mrIgnore then
      Speichern:=true;
  end
  else
    Speichern:=true;

  if not Speichern then
    exit;

  // Gameset �ffnen
  Archiv:=TArchivFile.Create;
  if MDIForm.NewSet then
  begin
    Archiv.CreateArchiv(GameSetFile);
    MDIForm.NewSet:=true;
  end
  else
    Archiv.OpenArchiv(GameSetFile);

  result:=true;
  f:=TMemoryStream.Create;
  m:=TMemoryStream.Create;

  // Forschungsprojekte speichern
  Header.Version:=ForschVersion;
  Header.Entrys:=0;
  with MDIForm do
  begin
    for Dummy:=0 to Length(Projekts)-1 do
      if not DeletedChild[Dummy] then inc(Header.Entrys);
    f.write(Header,SizeOf(Header));
    for Dummy:=0 to Length(Projekts)-1 do
    begin
      if not DeletedChild[Dummy] then
      begin
        SaveProjectRecord(Projekts[Dummy],f);
      end;
    end;
  end;

//  Encode(f,m);
  Archiv.ReplaceRessource(f,RNForschdat,true);
  f.Clear;

  MDIForm.WaffenListe.SaveLokalToStream(f);
  Archiv.ReplaceRessource(f,RNWaffenSymbols);
  f.Clear;

  // UFOs speichern
  with MDIForm do
  begin
    Header.Version:=UFOMVersion;
    Header.Entrys:=length(UFOs);
    f.Write(Header,SizeOf(Header));
    for Dummy:=0 to high(UFOs) do
      MDIForm.UFOs[Dummy].SaveToStream(f);
  end;
  Archiv.ReplaceRessource(f,RNUFOs,true);
  f.Clear;

  // Aliens speichern
  with MDIForm do
  begin
    Header.Version:=AlienEditVersion;
    Header.Entrys:=length(Aliens);
    f.Write(Header,SizeOf(Header));

    for Dummy:=0 to high(Aliens) do
      Aliens[Dummy].SaveToStream(f);
  end;
  Archiv.ReplaceRessource(f,RNAlien,true);
  f.Clear;

  // Skripte speichern
  with MDIForm do
  begin
    Header.Version:=SkriptVersion;
    Header.Entrys:=length(Skripte);
    f.Write(Header,SizeOf(Header));

    for Dummy:=0 to high(Skripte) do
      Skripte[Dummy].SaveToStream(f);
  end;
  Archiv.ReplaceRessource(f,RNSkripte,true);
  f.Clear;

  // Sprachen Speichern
  Dummy:=Length(MDIForm.Sprachen);
  f.Write(Dummy,SizeOf(Dummy));
  for Dummy:=0 to high(MDIForm.Sprachen) do
  begin
    WriteString(f,MDIForm.Sprachen[Dummy]);
  end;
  Archiv.ReplaceRessource(f,'Languages');
  f.Clear;

  // Gameset-Header speichern
  FillChar(Name,SizeOf(Name),#0);
  f.Write(HeadVersion,SizeOf(Cardinal));
  WriteString(f,MDIForm.SatzName);
  WriteString(f,MDIForm.SetHead.Autor);
  WriteString(f,MDIForm.SetHead.Organisation);
  WriteString(f,MDIForm.SetHead.Beschreibung);
  f.Write(MDIForm.SetHead.CanImport,SizeOf(boolean));
  Name:=MDIForm.SetHead.Password;
  CRC:=0;
  if Name<>'' then
  begin
    CRC:=$80808080;
    for Dummy:=1 to length(Name) do
    begin
      Wert:=Integer(Name[Dummy]);
      CRC:=(not CRC xor (CRC shl 16) xor ((not Wert) xor ((Wert shl 30) xor (Wert shl 19))));
    end;
  end;
  f.Write(CRC,SizeOf(CRC));
  f.Write(MDIForm.SetHead.ID,4);
  f.Position:=0;
  Encode(f,m);
  Archiv.ReplaceRessource(m,'Header');
  Archiv.CloseArchiv;
  Archiv.Free;

  m.Free;
  f.Free;
  MDIForm.ForschChanged:=false;
end;

procedure TSaveDialog.ListView1DblClick(Sender: TObject);
begin
  if ListView1.Selected<>nil then
  begin
    if PMessageRecord(listView1.Selected.Data).Assistent then
    begin
      CorrectFehler(PMessageRecord(ListView1.Selected.Data)^);
    end;
  end;
end;

procedure TSaveDialog.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  ClearList;
end;

procedure TSaveDialog.OKButtonClick(Sender: TObject);
begin
  ModalResult:=mrOK;
end;

procedure TSaveDialog.EditButtonClick(Sender: TObject);
begin
  ListView1DblClick(ListView1);
end;

procedure TSaveDialog.RetryButtonClick(Sender: TObject);
var
  Correct  : TMessageType;
begin
  ClearList;
  Correct:=StartCheck;
  case Correct of
    mtNone:
      begin
        RetryButton.Enabled:=false;
        SaveButton.Enabled:=true;
      end;
    mtHinweis,mtWarning:
      begin
        RetryButton.Enabled:=true;
        SaveButton.Enabled:=true;
      end;
    mtError:
      begin
        RetryButton.Enabled:=true;
        SaveButton.Enabled:=false;
      end;
  end;
end;

procedure TSaveDialog.CorrectFehler(Message: TMessageRecord);
begin
  ReCorrect:=false;
  FehlerRecord:=Message;
  ErrorForm:=TFehlerAssistent.Create(Application);
  ErrorForm.ShowIcon(FehlerRecord.MessageTyp);
  case Message.FehlerID of
    ENNoInfo:
      begin
        ErrorForm.Label1.Caption:='Dem Objekt '+GetObjektName(Message.Index)+' wurde keine Beschreibung '#13#10+
                                  'hinzugef�gt. Dies ist nicht zwingend notwendig, sollte aber zur'#13#10+
                                  'besseren Spielbarkeit trotzdem getan werden. Sie k�nnen auf Objekt'#13#10+
                                  'bearbeiten klicken, um eine Beschreibung hinzuzuf�gen.';
        with ErrorForm do
        begin
          Button1.Caption:='Objekt bearbeiten';
          Button1.OnClick:=EditFehlerObjekt;
          Button2.Visible:=false;
          Button3.Visible:=false;
          ErrorForm.Show;
        end;
      end;
    ENNoResearchInfo:
      begin
        ErrorForm.Label1.Caption:='Dem Objekt '+GetObjektName(Message.Index)+' wurde keine Beschreibung '#13#10+
                                  'f�r die Forschung hinzugef�gt. Dies ist nicht zwingend notwendig, sollte '#13#10+
                                  'aber zur besseren Spielbarkeit trotzdem getan werden. Sie k�nnen auf Objekt'#13#10+
                                  'bearbeiten klicken, um eine Beschreibung hinzuzuf�gen.';
        with ErrorForm do
        begin
          Button1.Caption:='Objekt bearbeiten';
          Button1.OnClick:=EditFehlerObjekt;
          Button2.Visible:=false;
          Button3.Visible:=false;
          ErrorForm.Show;
        end;
      end;
    ENKeineWaffe:
      begin
        ErrorForm.Label1.Caption:='Die Munition '+GetObjektName(Message.Index)+' wurde keiner '#13#10+
                                  'g�ltigen Waffe zugeordnet. Dies kann dadurch passieren,'#13#10+
                                  'dass die Waffe gel�scht wurde oder ein Objekt in eine Munition'#13#10+
                                  'umgewandelt wurde.'#13#10#13#10+
                                  'Sie haben folgende M�glichkleiten zur Behebung des Fehlers:'#13#10#13#10+
                                  '    1.'#9'Weisen Sie der Munition eine Waffe zu. Klicken Sie dazu auf'#13#10+
                                  #9'Munition bearbeiten.'#13#10#13#10+
                                  '    2.'#9'L�schen der Munition. Klicken Sie dazu auf'#13#10+
                                  #9'Munition l�schen';
        with ErrorForm do
        begin
          Button1.Caption:='Munition bearbeiten';
          Button2.Caption:='Munition l�schen';
          Button1.OnClick:=EditFehlerObjekt;
          Button2.OnClick:=DeleteObjekt;
          Button3.Visible:=false;
          ErrorForm.Show;
        end;
      end;
    ENLaserWaffe:
      begin
        ErrorForm.Label1.Caption:='Die Munition '+GetObjektName(Message.Index)+' ist einer '#13#10+
                                  'Waffe ('+GetObjektName(GetWaffeOfMunition(Message.Index))+') zugeordnet, die'#13#10+
                                  'keine Munition ben�tigt. Dies kann dadurch passieren,'#13#10+
                                  'dass nachtr�glich die Art der Waffe ge�ndert wurde.'#13#10#13#10+
                                  'Sie haben folgende M�glichkleiten zur Behebung des Fehlers:'#13#10#13#10+
                                  '    1.'#9'Weisen Sie der Munition eine andere Waffe zu. Klicken Sie dazu auf'#13#10+
                                  #9'Munition bearbeiten.'#13#10#13#10+
                                  '    2.'#9'Geben Sie eine andere Waffenart an. Klicken Sie dazu auf'#13#10+
                                  #9'Waffe bearbeiten.'#13#10#13#10+
                                  '    3.'#9'L�schen der Munition. Klicken Sie dazu auf'#13#10+
                                  #9'Munition l�schen';
        with ErrorForm do
        begin
          Button1.Caption:='Munition bearbeiten';
          Button2.Caption:='Waffe bearbeiten';
          Button3.Caption:='Munition l�schen';
          Button1.OnClick:=EditFehlerObjekt;
          Button2.OnClick:=EditFehlerWaffe;
          Button3.OnClick:=DeleteObjekt;
          ErrorForm.Show;
        end;
      end;
    ENDifferentTyp:
      begin
        ErrorForm.Label1.Caption:='Die Art der Munition '+GetObjektName(Message.Index)+' unterscheidet sich '#13#10+
                                  'von der Art der Waffe '+GetObjektName(GetWaffeOfMunition(Message.Index))+' .'#13#10+
                                  'Dies kann dadurch passieren, dass nachtr�glich die'#13#10+
                                  'Art der Waffe ge�ndert wurde.'#13#10#13#10+
                                  'Sie haben folgende M�glichkleiten zur Behebung des Fehlers:'#13#10#13#10+
                                  '    1.'#9'�ndern Sie die Art der Munition auf die Art der Waffe.'#13#10+
                                  #9'Klicken Sie dazu auf Munitionsart �ndern.'#13#10#13#10+
                                  '    2.'#9'�ndern Sie die Art der Waffe auf die Art der Munition.'#13#10+
                                  #9'Klicken Sie dazu auf Waffenart �ndern.'#13#10#13#10+
                                  '    3.'#9'Weisen Sie der Munition eine neue Waffe zu. Klicken Sie dazu auf'#13#10+
                                  #9'Munition bearbeiten';
        with ErrorForm do
        begin
          Button1.Caption:='Munitionsart �ndern';
          Button2.Caption:='Waffenart �ndern';
          Button3.Caption:='Munition bearbeiten';
          Button1.OnClick:=RestoreMunitionstyp;
          Button2.OnClick:=RestoreWaffentyp;
          Button3.OnClick:=EditFehlerObjekt;
          ErrorForm.Show;
        end;
      end;
    ENNoStrength:
      begin
        ErrorForm.Label1.Caption:='Die Angriffsst�rke der Munition '+GetObjektName(Message.Index)+' ist abh�ngig von '#13#10+
                                  'der Angriffsst�rke der Waffe '+GetObjektName(GetWaffeOfMunition(Message.Index))+', allerdings ist die Angriffsst�rke der Waffe abh�ngig von'#13#10+
                                  'der Angriffsst�rke der Munition. Dies hat zur Folge, dass weder Waffe noch Munition einer'#13#10+
                                  'Angriffsst�rke zugewiesen wurde.'#13#10#13#10+
                                  'Sie haben folgende M�glichkleiten zur Behebung des Fehlers:'#13#10#13#10+
                                  '    1.'#9'Geben Sie bei der Munition eine Angriffst�rke an. Klicken Sie dazu auf'#13#10+
                                  #9'Munition bearbeiten.'#13#10#13#10+
                                  '    2.'#9'Deaktivieren Sie bei der Waffe ''Abh�ngig von Munition'' und'#13#10+
                                  #9'geben Sie einen festen Wert ein. Klicken Sie dazu auf'#13#10+
                                  #9'Waffe bearbeiten.'#13#10#13#10+
                                  '    3.'#9'L�schen der Munition. Klicken Sie dazu auf'#13#10+
                                  #9'Munition l�schen';
        with ErrorForm do
        begin
          Button1.Caption:='Munition bearbeiten';
          Button2.Caption:='Waffe bearbeiten';
          Button3.Caption:='Munition l�schen';
          Button1.OnClick:=EditFehlerObjekt;
          Button2.OnClick:=EditFehlerWaffe;
          Button3.OnClick:=DeleteObjekt;
          ErrorForm.Show;
        end;
      end;
    ENNoRelation:
      begin
        ErrorForm.Label1.Caption:='Es wurde eine Angriffsst�rke bei der Munition '+GetObjektName(Message.Index)+#13#10+
                                  'und bei der Waffe '+GetObjektName(GetWaffeOfMunition(Message.Index))+' angegeben. Dies hat zur Folge, dass nicht entschieden'#13#10+
                                  'werden kann, welcher Wert genommen wird.'#13#10#13#10+
                                  'Sie haben folgende M�glichkleiten zur Behebung des Fehlers:'#13#10#13#10+
                                  '    1.'#9'Aktivieren Sie bei der Munition ''Abh�ngig von Waffe''. Klicken Sie dazu auf'#13#10+
                                  #9'Munition bearbeiten.'#13#10#13#10+
                                  '    2.'#9'Aktivieren Sie bei der Waffe ''Abh�ngig von Munition''. Klicken Sie dazu auf'#13#10+
                                  #9'Waffe bearbeiten.'#13#10#13#10+
                                  '    3.'#9'L�schen der Munition. Klicken Sie dazu auf'#13#10+
                                  #9'Munition l�schen';
        with ErrorForm do
        begin
          Button1.Caption:='Munition bearbeiten';
          Button2.Caption:='Waffe bearbeiten';
          Button3.Caption:='Munition l�schen';
          Button1.OnClick:=EditFehlerObjekt;
          Button2.OnClick:=EditFehlerWaffe;
          Button3.OnClick:=DeleteObjekt;
          ErrorForm.Show;
        end;
      end;
    ENNoWaffenStrength:
      begin
        ErrorForm.Label1.Caption:='Bei der Waffe '+GetObjektName(Message.Index)+' wurde keine Angriffsst�rke '#13#10+
                                  'hinzugef�gt. Bei Waffen ohne Munition (Laser- und Nahkampfwaffen)'#13#10+
                                  'ist dies jedoch Pflicht.';
        with ErrorForm do
        begin
          Button1.Caption:='Waffe bearbeiten';
          Button1.OnClick:=EditFehlerObjekt;
          Button2.Visible:=false;
          Button3.Visible:=false;
          ErrorForm.Show;
        end;
      end;
    ENNoImage:
      begin
        ErrorForm.Label1.Caption:='Dem Objekt '+GetObjektName(Message.Index)+' wurde kein Bild in der '#13#10+
                                  'UFOP�die zugewiesen. Dies ist nicht zwingend notwendig, sollte aber zur'#13#10+
                                  'besseren Spielbarkeit trotzdem getan werden. Sie k�nnen auf Objekt'#13#10+
                                  'bearbeiten klicken, um ein neues Bild festzulegen.';
        with ErrorForm do
        begin
          Button1.Caption:='Objekt bearbeiten';
          Button1.OnClick:=EditFehlerObjekt;
          Button2.Visible:=false;
          Button3.Visible:=false;
          ErrorForm.Show;
        end;
      end;
    ENInvalidImage:
      begin
        ErrorForm.Label1.Caption:='Dem Objekt '+GetObjektName(Message.Index)+' wurde ein ung�ltiges Bild in der '#13#10+
                                  'UFOP�die zugewiesen. Vermutlich wurde die entsprechende Grafik nicht importiert oder'#13#10+
                                  'gel�scht. Sie k�nnen auf Objekt bearbeiten klicken, um ein neues Bild festzulegen.';
        with ErrorForm do
        begin
          Button1.Caption:='Objekt bearbeiten';
          Button1.OnClick:=EditFehlerObjekt;
          Button2.Visible:=false;
          Button3.Visible:=false;
          ErrorForm.Show;
        end;
      end;
    ENInvalidSymbol:
      begin
        ErrorForm.Label1.Caption:='Dem Objekt '+GetObjektName(Message.Index)+' ist einung�ltiges Symbol '#13#10+
                                  'zugewiesen. Vermutlich wurde das Objekt ohne die zugeh�rigen Symbole importiert.'#13#10+
                                  'Sie k�nnen auf Objekt bearbeiten klicken, um ein neues Symbol festzulegen.';
        with ErrorForm do
        begin
          Button1.Caption:='Objekt bearbeiten';
          Button1.OnClick:=EditFehlerObjekt;
          Button2.Visible:=false;
          Button3.Visible:=false;
          ErrorForm.Show;
        end;
      end;
    EInvalidScript:
      begin
        ErrorForm.Label1.Caption:='Das Skript '+Message.ObjektName+' enth�lt Fehler und kann im Spiel nicht gestartet werden. '#13#10+
                                  'Sie k�nnen auf Skript bearbeiten klicken, um das fehlerhafte Skript zu korrigieren.';
        with ErrorForm do
        begin
          Button1.Caption:='Skript bearbeiten';
          Button1.OnClick:=EditSkriptObjekt;
          Button2.Visible:=false;
          Button3.Visible:=false;
          ErrorForm.Show;
        end;
      end;
    else
      MessageDlg('F�r diese Meldung ist kein Assistent verf�gbar.',mtInformation,[mbOK],0);
  end;
  ErrorForm.Free;
  ErrorForm:=nil;
  if ReCorrect then
  begin
    MDIForm.ObjektViewer.CreateTree;
    MDIForm.UFOViewer.CreateTree;
    MDIForm.AlienViewer.CreateTree;
    MDIForm.SkriptViewer.CreateTree;
    MDIForm.StartViewer.CreateTree;
    MDIForm.ProjektViewer.CreateTree;

    MDIForm.ForschChanged:=true;
    RetryButton.Click;
  end;
end;

function TSaveDialog.GetObjektName(Index: Integer): String;
begin
  result:=string_utils_GetLanguageString(MDIForm.Sprache,MDIForm.Projekts[Index].GetString('Name'))
end;

procedure TSaveDialog.DeleteObjekt(Sender: TObject);
var
  ObjektName: String;
begin
  ObjektName:=GetObjektName(FehlerRecord.Index);
  if Application.MessageBox(PChar('M�chten Sie das Objekt '+ObjektName+' wirklich l�schen?'),PChar(CErrorAssist),MB_YESNO or MB_ICONQUESTION)=ID_NO then exit;
  case FehlerRecord.FehlerID of
    ENLaserWaffe: ErrorForm.Button3.Enabled:=false;
  end;
  MDIForm.DeletedChild[FehlerRecord.Index]:=true;
  MDIForm.ForschChanged:=true;
  ReCorrect:=true;
  ErrorForm.Close;
end;


procedure TSaveDialog.EditFehlerObjekt(Sender: TObject);
var
  Projekt: TExtRecord;
begin
  Projekt:=MDIForm.Projekts[FehlerRecord.Index];
  if MDIForm.EditProjekt(Projekt) then
  begin
    MDIForm.Projekts[FehlerRecord.Index]:=Projekt;
    ReCorrect:=true;
  end;
end;

procedure TSaveDialog.EditFehlerWaffe(Sender: TObject);
var
  Projekt : TExtRecord;
  Index   : Integer;
begin
  Index:=GetWaffeOfMunition(FehlerRecord.Index);
  if Index=-1 then exit;
  Projekt:=MDIForm.Projekts[Index];
  if MDIForm.EditProjekt(Projekt) then
  begin
    MDIForm.Projekts[Index]:=Projekt;
    ReCorrect:=true;
  end;
end;

procedure TSaveDialog.EditSkriptObjekt(Sender: TObject);
var
  Skript: String;
begin
  Skript:=MDIForm.Skripte[FehlerRecord.Index].GetString('Skript');

  frmSkriptEditor.Skript:=Skript;

  if frmSkriptEditor.ShowModal=mrOK then
  begin
    MDIForm.Skripte[FehlerRecord.Index].SetString('Skript',frmSkriptEditor.Skript);
    ReCorrect:=true;
  end;
end;

procedure TSaveDialog.RestoreMunitionstyp(Sender: TObject);
var
  Index : Integer;
  Typ   : String;
begin
  Index:=GetWaffeOfMunition(FehlerRecord.Index);
  case TWaffenType(MDIForm.Projekts[Index].GetInteger('WaffType')) of
    wtProjektil : Typ:='Projektil';
    wtRaketen   : Typ:='Raketen';
    wtChemic    : Typ:='Chemisch';
    wtLaser     :
    begin
      Application.MessageBox(PChar('Die Aktion kann nicht durchgef�hrt werden, da die Waffe vom Typ Laser ist. Bitte benutzen Sie einen der anderen beiden Vorschl�ge.'),PChar(CErrorAssist),MB_ICONASTERISK);
      exit;
    end;
  end;
  if Application.MessageBox(PChar('Soll der Typ der Munition auf '+Typ+' ge�ndert werden?'),PChar(CErrorAssist),MB_YESNO or MB_ICONQUESTION)=ID_NO then exit;
  MDIForm.Projekts[FehlerRecord.Index].SetInteger('WaffType',MDIForm.Projekts[Index].GetInteger('WaffType'));
  ReCorrect:=true;
  case FehlerRecord.FehlerID of
    ENDifferentTyp:
    begin
      ErrorForm.Button1.Enabled:=false;
      ErrorForm.Button2.Enabled:=false;
    end;
  end;
end;

procedure TSaveDialog.RestoreWaffentyp(Sender: TObject);
var
  Index : Integer;
  Typ   : String;
begin
  Index:=GetWaffeOfMunition(FehlerRecord.Index);
  case TWaffenType(MDIForm.Projekts[FehlerRecord.Index].GetInteger('WaffType')) of
    wtProjektil : Typ:='Projektil';
    wtRaketen   : Typ:='Raketen';
    wtChemic    : Typ:='Chemisch';
  end;
  if Application.MessageBox(PChar('Soll der Typ der Waffe auf '+Typ+' ge�ndert werden?'),PChar(CErrorAssist),MB_YESNO or MB_ICONQUESTION)=ID_NO then exit;
  MDIForm.Projekts[Index].SetInteger('WaffType',MDIForm.Projekts[FehlerRecord.Index].GetInteger('WaffType'));
  ReCorrect:=true;
  case FehlerRecord.FehlerID of
    ENDifferentTyp:
    begin
      ErrorForm.Button1.Enabled:=false;
      ErrorForm.Button2.Enabled:=false;
    end;
  end;
end;

function TSaveDialog.GetWaffeOfMunition(Index: Integer): Integer;
var
  ID      : Cardinal;
  Dummy   : Integer;
  Count   : Integer;
begin
  ID:=MDIForm.Projekts[FehlerRecord.Index].GetCardinal('MunFor');
  result:=-1;
  Dummy:=0;
  Count:=length(MDIForm.Projekts);
  while (result=-1) and (Dummy<Count) do
  begin
    if MDIForm.Projekts[Dummy].GetCardinal('ID')=ID then result:=Dummy;
    inc(Dummy);
  end;
end;

function TSaveDialog.IsProjektInList(ID: Cardinal): Boolean;
var
  Dummy: Integer;
begin
  result:=false;
  for Dummy:=0 to high(MDIForm.Projekts) do
  begin
    if (MDIForm.Projekts[Dummy].GetCardinal('ID')=ID) and (not MDIForm.DeletedChild[Dummy]) then
    begin
      result:=true;
      exit;
    end;
  end;
end;

function TSaveDialog.IsAlienInList(ID: Cardinal): Boolean;
var
  Dummy: Integer;
begin
  result:=false;
  for Dummy:=0 to high(MDIForm.Aliens) do
  begin
    if MDIForm.Aliens[Dummy].GetCardinal('ID')=ID then
    begin
      result:=true;
      exit;
    end;
  end;
end;

procedure TSaveDialog.ListView1ColumnClick(Sender: TObject;
  Column: TListColumn);
begin
  ListView1.Tag:=Column.Index;
  ListView1.CustomSort(nil,0);
end;

procedure TSaveDialog.ListView1Compare(Sender: TObject; Item1,
  Item2: TListItem; Data: Integer; var Compare: Integer);
begin
  if TListView(Sender).Tag=0 then
  begin
    Assert(Item1.Data<>nil);
    Assert(Item2.Data<>nil);
    Compare:=Integer(PMessageRecord(Item2.Data).MessageTyp)-Integer(PMessageRecord(Item1.Data).MessageTyp);
  end
  else if TListView(Sender).Tag>=1 then
  begin
    if (Item1.SubItems.Count>=TListView(Sender).Tag-1) and (Item2.SubItems.Count>=TListView(Sender).Tag-1) then
      Compare:=CompareText(Item1.SubItems[TListView(Sender).Tag-1],Item2.SubItems[TListView(Sender).Tag-1])
    else
      Compare:=0;
  end;
end;

procedure TSaveDialog.ListView1SelectItem(Sender: TObject; Item: TListItem;
  Selected: Boolean);
begin
  Assert(Item.Data<>nil);
  EditButton.Visible:=(Selected) and PMessageRecord(Item.Data).Assistent;
end;

procedure TSaveDialog.CloseButtonClick(Sender: TObject);
begin
  Close;
end;

end.
