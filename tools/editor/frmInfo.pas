unit frmInfo;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, ExtCtrls, Buttons;

type
  TInfoEdit = class(TForm)
    Label1: TLabel;
    ComboBox1: TComboBox;
    EditFeld: TMemo;
    ApplyButton: TButton;
    CancelButton: TButton;
    procedure CancelButtonClick(Sender: TObject);
    procedure ApplyButtonClick(Sender: TObject);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure FormShow(Sender: TObject);
    procedure ShowLanguageText(Sender: TObject);
  private
    fText       : String;
    fOldLanguage: String;
    { Private-Deklarationen }
  public
    property Text: String read fText write fText;
    { Public-Deklarationen }
  end;

var
  InfoEdit: TInfoEdit;

implementation

uses frmMain, string_utils;

{$R *.DFM}

procedure TInfoEdit.CancelButtonClick(Sender: TObject);
begin
  ModalResult:=mrCancel;
end;

procedure TInfoEdit.ApplyButtonClick(Sender: TObject);
begin
  ModalResult:=mrOK;
  ShowLanguageText(nil);
end;

procedure TInfoEdit.FormKeyPress(Sender: TObject; var Key: Char);
begin
  if Key=#27 then CancelButton.Click;
end;

procedure TInfoEdit.FormShow(Sender: TObject);
var
  Dummy: Integer;
begin
  fOldLanguage:='';
  ComboBox1.Items.Clear;
  for Dummy:=0 to High(MDIForm.Sprachen) do
  begin
    ComboBox1.Items.Add(MDIForm.Sprachen[Dummy]);
  end;
  ComboBox1.ItemIndex:=0;

  ShowLanguageText(nil);
end;

procedure TInfoEdit.ShowLanguageText(Sender: TObject);
begin
  if fOldLanguage<>'' then
    fText:=string_utils_SetLanguageString(fOldLanguage,Text,EditFeld.Text);

  EditFeld.Text:=string_utils_GetLanguageString(ComboBox1.Text,fText);

  fOldLanguage:=ComboBox1.Text;
end;

end.
