unit frmEditLanguages;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, record_utils, string_utils, ExtRecord, TraceFile;

type
  TEditLanguages = class(TForm)
    ListBox1: TListBox;
    Button1: TButton;
    Button2: TButton;
    Button3: TButton;
    OpenDialog1: TOpenDialog;
    Button4: TButton;
    SaveDialog1: TSaveDialog;
    procedure Button1Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure ListBox1DrawItem(Control: TWinControl; Index: Integer;
      Rect: TRect; State: TOwnerDrawState);
    procedure Button2Click(Sender: TObject);
    procedure ListBox1DblClick(Sender: TObject);
    procedure Button3Click(Sender: TObject);
    procedure Button4Click(Sender: TObject);
  private
    { Private-Deklarationen }
  public
    { Public-Deklarationen }
    procedure AddLanguage(Language: String);
  end;

var
  EditLanguages: TEditLanguages;

implementation

uses frmMain, frmLanguages, gameset_api, ArchivFile, XForce_types;

{$R *.DFM}

procedure TEditLanguages.Button1Click(Sender: TObject);
begin
  Close;
end;

procedure TEditLanguages.FormShow(Sender: TObject);
var
  Dummy: Integer;
begin
  ListBox1.Items.Clear;
  for Dummy:=0 to high(MDIForm.Sprachen) do
  begin
    ListBox1.Items.Add(MDIForm.Sprachen[Dummy]);
  end;
end;

procedure TEditLanguages.ListBox1DrawItem(Control: TWinControl;
  Index: Integer; Rect: TRect; State: TOwnerDrawState);
begin
  with ListBox1.Canvas do
  begin
    FillRect(Rect);
    if ListBox1.Items[Index]=MDIForm.Sprache then
      Font.Style:=[fsBOld]
    else
      Font.Style:=[];

    TextOut(Rect.Left+2,Rect.Top+2,ListBox1.Items[Index]);
  end;
end;

procedure TEditLanguages.Button2Click(Sender: TObject);
begin
  if languageForm.ShowModal=mrOK then
  begin
    if ListBox1.Items.IndexOf(LanguageForm.GetLanguage)<>-1 then
    begin
      MessageDlg('Sprache bereits im Spielsatz',mtInformation,[mbOK],0);
      exit;
    end;

    AddLanguage(LanguageForm.GetLanguage);
  end;
end;

procedure TEditLanguages.ListBox1DblClick(Sender: TObject);
begin
  if ListBox1.ItemIndex<>-1 then
  begin
    MDIForm.Sprache:=ListBox1.Items[ListBox1.ItemIndex];
    ListBox1.Refresh;

    MDIForm.ProjektViewer.CreateTree;
    MDIForm.StartViewer.CreateTree;
    MDIForm.AlienViewer.CreateTree;
    MDIForm.UFOViewer.CreateTree;
    MDIForm.AlienItemsViewer.CreateTree;
    MDIForm.SkriptViewer.CreateTree;
  end;
end;

procedure TEditLanguages.Button3Click(Sender: TObject);
var
  List       : TStringList;
  Lang       : String;

  procedure ImportToArray(ID: Cardinal;var Arr: TRecordArray;Prop: String);
  var
    Dummy: Integer;
    Text : String;
    NName: String;
    NDesc: String;
  begin
     if Prop='Info' then
    begin
      NName:=Copy(List[0],3,length(List[0]));
      List.Delete(0);
    end;
    List.Delete(0);
    NDesc:='';
    while (length(List[0])=0) or (Copy(List[0],1,59)<>'-----------------------------------------------------------') do
    begin
      if NDesc<>'' then
        NDesc:=NDesc+#13#10;
      NDesc:=NDesc+List[0];
      List.Delete(0);
    end;

    for Dummy:=0 to high(Arr) do
    begin
      if Arr[Dummy].GetCardinal('ID')=ID then
      begin
        if Prop='Info' then
        begin
          Text:=Arr[Dummy].GetString('Name');
          Text:=string_utils_SetLanguageString(lang,Text,NName);
          Arr[Dummy].SetString('Name',Text);
        end;

        Text:=Arr[Dummy].GetString(Prop);
        Text:=string_utils_SetLanguageString(lang,Text,NDesc);
        Arr[Dummy].SetString(Prop,Text);
        exit;
      end;
    end;
  end;
  
  procedure ImportObject;
  var
    ID  : Cardinal;
    Typ : Char;
    Prop: String;
    expl: TStringArray;
  begin
    expl:=string_utils_explode('/',copy(List[0],2,length(List[0])-2));
    Assert(length(Expl)>=2,'Invalid Gameset-Language File');
    Assert(length(expl[0])>0);
    Typ:=expl[0][1];
    ID:=StrToInt64(Expl[1]);
    if length(expl)>2 then
      Prop:=expl[2]
    else
      Prop:='Info';

    List.Delete(0);

    case Typ of
      'P' : ImportToArray(ID,MDIForm.Projekts,Prop);
      'U' : ImportToArray(ID,MDIForm.UFOS,Prop);
      'A' : ImportToArray(ID,MDIForm.Aliens,Prop);
    end;
  end;

begin
  if not OpenDialog1.Execute then
    exit;

  Lang:=ChangeFileExt(ExtractFileName(OpenDialog1.FileName),'');

  if not InputQuery('Sprache','Sprache angeben',Lang) then
    exit;

  Lang:=lowercase(Lang);

  if ListBox1.Items.IndexOf(Lang)=-1 then
    AddLanguage(Lang);
    
  List:=TStringList.Create;
  List.LoadFromFile(OpenDialog1.FileName);

  try
    // Text importieren
    while List.Count>0 do
    begin
      if (length(trim(List[0]))>0) and (trim(List[0])[1]='[') then
        ImportObject;
        
      List.Delete(0);
    end;
  finally
    List.Free;
  end;
end;

procedure TEditLanguages.AddLanguage(Language: String);
begin
  if ListBox1.Items.IndexOf(Language)<>-1 then
    exit;

  ListBox1.Items.Add(Language);
  SetLEngth(MDIForm.Sprachen,length(MDIForm.Sprachen)+1);
  MDIForm.Sprachen[high(MDIForm.Sprachen)]:=Language;
  MDIForm.ForschChanged:=true;
end;

procedure TEditLanguages.Button4Click(Sender: TObject);

  procedure ExtractLanguage(Language: String);
  var
    Records: TRecordArray;
    List   : TStringList;

    procedure Extract(var Records: TRecordArray;Typ: String);
    var
      Dummy: Integer;
      Lines: TStringArray;
      Dummy2: Integer;

      procedure SaveProperty(Prop: String);
      begin
        if (Prop<>'Info') and (Records[Dummy].GetString(Prop)='') then
          exit;
        List.Add('['+Typ+'/'+IntToStr(Records[Dummy].GetCardinal('ID'))+'/'+Prop+']');
        if Prop='Info' then
          List.Add('$ '+string_utils_GetLanguageString(Language,Records[Dummy].GetString('Name')));
        List.Add('-----------------------------------------------------------');
        Lines:=string_utils_explode(#13#10,string_utils_GetLanguageString(Language,Records[Dummy].GetString(Prop)));
        string_utils_AddToTStrings(Lines,List);
        List.Add('-----------------------------------------------------------');
        List.Add('');
      end;

    begin
      for Dummy:=0 to high(Records) do
      begin
        SaveProperty('Info');
        SaveProperty('ResearchInfo');
      end;
      record_utils_ClearArray(Records);
    end;

  begin
    List:=TStringList.Create;
    List.Add('! Note for translators:');
    List.Add('! Don''t change anything beetween [ and ]');
    List.Add('! Text after $ is the name of the object. Please translate it.');
    List.Add('! Text beetween the "------" Lines is the description for the object. Please translate it.');

    Extract(MDIForm.Projekts,'P');
    Extract(MDIForm.Aliens,'A');
    Extract(MDIForm.UFOs,'U');

    List.SaveToFile(SaveDialog1.FileName);
    List.Free;
  end;

begin
  SaveDialog1.FileName:=ListBox1.Items[ListBox1.ItemIndex]+'.dat';
  if SaveDialog1.Execute then
    ExtractLanguage(ListBox1.Items[ListBox1.ItemIndex]);
end;

end.
