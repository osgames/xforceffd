unit frmPaedieImage;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, ExtCtrls, ComCtrls, ArchivFile, Defines;

type
  TCheckBitmap = procedure(Bitmap: TBitmap) of object;

  TPaedieImage = class(TForm)
    ListView1: TListView;
    Image1: TImage;
    OKButton: TBitBtn;
    BitBtn2: TBitBtn;
    Bevel1: TBevel;
    BitBtn1: TBitBtn;
    DeleteBitmap: TBitBtn;
    OpenDialog1: TOpenDialog;
    NoImageButton: TBitBtn;
    Label1: TLabel;
    ImageSize: TLabel;
    Shape1: TShape;
    Label2: TLabel;
    procedure FormShow(Sender: TObject);
    procedure ListView1SelectItem(Sender: TObject; Item: TListItem;
      Selected: Boolean);
    procedure OKButtonClick(Sender: TObject);
    procedure ListView1DblClick(Sender: TObject);
    procedure DeleteBitmapClick(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure NoImageButtonClick(Sender: TObject);
  private
    fImageBitmap     : String;
    fKat             : String;
    fAllowedNoImage  : Boolean;
    fCheckBitmap     : TCheckBitmap;
    function GetImage: TBitmap;
    { Private-Deklarationen }
  public
    property ImageBitmap: String read fImageBitmap write fImageBitmap;

    property ImageKategorie   : String read fKat write fKat;
    property Image            : TBitmap read GetImage;
    property AllowedNoImage   : Boolean read fAllowedNoImage write fAllowedNoImage;
    property CheckBitmap      : TCheckBitmap read fCheckBitmap write fCheckBitmap;
    { Public-Deklarationen }
  end;

var
  PaedieImage: TPaedieImage;

implementation

uses frmMain;

{$R *.DFM}

procedure TPaedieImage.FormShow(Sender: TObject);
var
  Archiv   : TArchivFile;
  SelOrt   : String;
  SelRess  : String;

  procedure ReadArchiv(ViewOrt: String;Ort: String);
  var
    Dummy: Integer;
    Name : String;
  begin
    for Dummy:=0 to Archiv.Count-1 do
    begin
      Name:=Archiv.Files[Dummy].Name;
      if Copy(Name,1,Pos(':',Name)-1)=fKat then
      begin
        with ListView1.Items.Add do
        begin
          Caption:=Copy(Name,Pos(':',Name)+1,length(Name));
          SubItems.Add(ViewOrt);
          SubItems.Add(Ort);
          if (Ort=SelOrt) and (SelRess=Copy(Name,Pos(':',Name)+1,100)) then
            Selected:=true;
	end;
      end;
    end;
  end;

begin
  SelOrt:=Copy(fImageBitmap,1,Pos('\',fImageBitmap)-1);
  SelRess:=Copy(fImageBitmap,Pos(':',fImageBitmap)+1,length(fImageBitmap));

  ListView1.Items.Clear;
  Archiv:=TArchivFile.Create;
  Archiv.OpenArchiv(FGameDataFile,true);
  ReadArchiv('Standard','default');
  Archiv.OpenArchiv('data\GameSets\'+MDIForm.Filename,true);
  ReadArchiv('Eigenes','user');
  Archiv.Free;
  ListView1.SetFocus;

  NoImageButton.Enabled:=fAllowedNoImage;
end;

procedure TPaedieImage.ListView1SelectItem(Sender: TObject;
  Item: TListItem; Selected: Boolean);
var
  Archiv: TArchivFile;
begin
  OKButton.Enabled:=Selected or (fAllowedNoImage);
  if not Selected then
  begin
    Image1.Picture.Bitmap.Width:=0;
    DeleteBitmap.Enabled:=false;
    exit;
  end;

  Archiv:=TArchivFile.Create;
  if Item.SubItems[1]='default' then
  begin
    Archiv.OpenArchiv(FGameDataFile,true);
    DeleteBitmap.Enabled:=false;
  end
  else
  begin
    Archiv.OpenArchiv('data\gamesets\'+MDIForm.Filename,true);
    DeleteBitmap.Enabled:=true;
  end;

  Archiv.OpenRessource(fKat+':'+Item.Caption);
  Image1.Picture.Bitmap.LoadFromStream(Archiv.Stream);

  ImageSize.Caption:=Format('%dx%d',[Image1.Picture.Bitmap.Width,Image1.Picture.Bitmap.Height]);
  Archiv.Free;

end;

procedure TPaedieImage.OKButtonClick(Sender: TObject);
begin
  if ListView1.Selected=nil then
    fImageBitmap:=''
  else
    fImageBitmap:=ListView1.Selected.SubItems[1]+'\'+fKat+':'+ListView1.Selected.Caption;

end;

procedure TPaedieImage.ListView1DblClick(Sender: TObject);
begin
  if ListView1.Selected<>nil then
    OKButton.Click;
end;

procedure TPaedieImage.DeleteBitmapClick(Sender: TObject);
var
  Archiv: TArchivFile;
begin
  if MessageDlg('M�chten Sie das Bild wirklich aus dem Spielsatz l�schen? Diese �nderung wird sofort gespeichert und kann nicht R�ckg�ngig gemacht werden!',mtConfirmation,[mbYes,mbNo],0)=mryes then
  begin
    Archiv:=TArchivFile.Create;
    Archiv.OpenArchiv('data\gamesets\'+MDIForm.Filename);
    Archiv.DeleteRessource(fKat+':'+ListView1.Selected.Caption);
    ListView1.Selected.Free;
    Archiv.Free;
  end;
end;

procedure TPaedieImage.BitBtn1Click(Sender: TObject);
var
  Bitmap: TBitmap;
  Archiv: TArchivFile;
  fs    : TFileStream;
  Name  : String;
  Dummy : Integer;
  FileN : String;
begin
  if OpenDialog1.Execute then
  begin
    Bitmap:=TBitmap.Create;
    for Dummy:=0 to OpenDialog1.Files.Count-1 do
    begin
      try
        FileN:=OpenDialog1.Files[Dummy];

        fs:=TFileStream.Create(FileN,fmOpenRead);
        Bitmap.LoadFromStream(fs);

        // Gr��e pr�fen
        if Assigned(fCheckBitmap) then
          fCheckBitmap(Bitmap);

        Name:=ChangeFileExt(ExtractFileName(FileN),'');
        if not InputQuery('Bildname f�r '+FileN,'Bildname:',Name) then
        begin
          fs.Free;
          break;
        end;

        Archiv:=TArchivFile.Create;
        Archiv.OpenArchiv('data\gamesets\'+MDIForm.Filename);
        Archiv.AddRessource(fs,fKat+':'+Name,true);
        with ListView1.Items.Add do
        begin
          Caption:=Name;
          SubItems.Add('Eigenes');
          SubItems.Add('user');
        end;
        Archiv.Free;
      except
        on E: Exception do
        begin
          MessageDlg(Format('Das Bild %s konnte nicht hinzugef�gt werden:'#13#10'%s',[ExtractFileName(FileN),E.Message]),mtError,[mbOK],0);
        end;
      end;
      fs.Free;
    end;
    Bitmap.Free;
  end;
end;

procedure TPaedieImage.NoImageButtonClick(Sender: TObject);
begin
  ListView1.Selected:=nil;
end;

function TPaedieImage.GetImage: TBitmap;
begin
  result:=Image1.Picture.Bitmap;
end;

end.
