unit frmErrorAssist;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, ExtCtrls, frmSave;

type
  TFehlerAssistent = class(TForm)
    CloseButton: TBitBtn;
    Button1: TButton;
    Button2: TButton;
    Button3: TButton;
    Label1: TLabel;
    Panel1: TPanel;
    InfoIcon: TImage;
    procedure CloseButtonClick(Sender: TObject);
  private
    { Private-Deklarationen }
  public
    procedure ShowIcon(Icon: TMessageType);
    procedure Show;
    { Public-Deklarationen }
  end;

var
  FehlerAssistent: TFehlerAssistent;

implementation

{$R *.DFM}

{ TFehlerAssistent }

procedure TFehlerAssistent.Show;
begin
  ClientHeight:=Label1.Height+58;
  ClientWidth:=Label1.Width+72;
//  Panel1.Height:=Label1.Height+18;
  ShowModal;
end;

procedure TFehlerAssistent.ShowIcon(Icon: TMessageType);
begin
  case Icon of
    mtError       : InfoIcon.Picture.Icon.Handle:=LoadIcon(0,IDI_ERROR);
    mtWarning     : InfoIcon.Picture.Icon.Handle:=LoadIcon(0,IDI_WARNING);
    mtHinweis     : InfoIcon.Picture.Icon.Handle:=LoadIcon(0,IDI_INFORMATION);
  end;
end;

procedure TFehlerAssistent.CloseButtonClick(Sender: TObject);
begin
  Close;
end;

end.
