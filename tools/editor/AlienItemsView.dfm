object AlienItemsForm: TAlienItemsForm
  Left = 0
  Top = 0
  Width = 443
  Height = 275
  Align = alClient
  Constraints.MinWidth = 174
  Color = clWindow
  ParentColor = False
  TabOrder = 0
  object StartView: TListView
    Left = 0
    Top = 22
    Width = 443
    Height = 253
    Hint = 
      'Zeigt alle Gegenst'#228'nde, Einrichtungen und Raumschiffe zu Beginn ' +
      'des Spiels'
    Align = alClient
    Columns = <
      item
        Caption = 'Name'
        Width = 190
      end
      item
        Caption = 'Typ'
        Width = 110
      end
      item
        Caption = 'Verf'#252'gbar nach'
        Width = 90
      end>
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    HideSelection = False
    LargeImages = MDIForm.WaffenListe
    RowSelect = True
    ParentFont = False
    PopupMenu = PopupMenu1
    SmallImages = MDIForm.WaffenListe
    SortType = stData
    TabOrder = 0
    ViewStyle = vsReport
    OnColumnClick = StartViewColumnClick
    OnCompare = StartViewCompare
    OnDblClick = StartViewDblClick
    OnEdited = StartViewEdited
  end
  object Panel2: TPanel
    Left = 0
    Top = 0
    Width = 443
    Height = 22
    Align = alTop
    Alignment = taLeftJustify
    BevelOuter = bvNone
    TabOrder = 1
    DesignSize = (
      443
      22)
    object Image1: TImage
      Left = 2
      Top = 2
      Width = 16
      Height = 16
      Picture.Data = {
        07544269746D617036030000424D360300000000000036000000280000001000
        0000100000000100180000000000000300000000000000000000000000000000
        0000FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
        FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
        FFFF00FF000000000000000000FF00FFFF00FFC0C0C0FF00FFFF00FFFF00FFFF
        00FFFF00FFFF00FFFF00FFFF00FFFF00FF000000808080808080808080000000
        C0C0C0C0C0C0C0C0C0FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF0000
        00808080808080808080808080C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0FF00FFFF
        00FFFF00FFFF00FFFF00FF000000808080808080000000808080000000C0C0C0
        C0C0C0FFFFFFC0C0C0C0C0C0FF00FFFF00FFFF00FFFF00FF0000008080808080
        80808080808080808080808080C0C0C0FFFFFFC0C0C0FFFFFFC0C0C0FF00FFFF
        00FFFF00FFFF00FF000000808080808080808080808080808080808080808080
        C0C0C0C0C0C0C0C0C0FF00FFFF00FFFF00FFFF00FFFF00FF0000008080808080
        80000000000000808080000000000000808080C0C0C0000000FF00FFFF00FFFF
        00FFFF00FFFF00FF000000808080808080000000000000808080000000000000
        808080808080000000FF00FFFF00FFFF00FFFF00FFFF00FF0000008080800000
        00000000808080808080808080000000000000808080000000FF00FFFF00FFFF
        00FFFF00FFFF00FF000000808080808080808080808080808080808080808080
        808080808080000000FF00FFFF00FFFF00FFFF00FFFF00FF0000008080808080
        80808080808080808080808080808080808080808080000000FF00FFFF00FFFF
        00FFFF00FFFF00FFFF00FF000000808080808080808080808080808080808080
        808080000000FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF0000008080
        80808080808080808080808080808080808080000000FF00FFFF00FFFF00FFFF
        00FFFF00FFFF00FFFF00FFFF00FF000000808080808080808080808080808080
        000000FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
        FF000000000000000000000000000000FF00FFFF00FFFF00FFFF00FFFF00FFFF
        00FF}
      Transparent = True
    end
    object Label1: TLabel
      Left = 19
      Top = 4
      Width = 95
      Height = 13
      Caption = 'Alienausr'#252'stung:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object FilterBox: TEdit
      Left = 272
      Top = 0
      Width = 170
      Height = 21
      Hint = 'Filter'
      Anchors = [akTop, akRight]
      TabOrder = 0
      OnChange = FilterBoxChange
    end
  end
  object PopupMenu1: TPopupMenu
    Images = MDIForm.ImageList1
    OwnerDraw = True
    OnPopup = PopupMenu1Popup
    Left = 32
    Top = 32
    object EditStart: TMenuItem
      Action = MDIForm.ActionItemEdit
      Default = True
    end
    object Umbenennen2: TMenuItem
      Action = MDIForm.ActionItemRename
    end
    object Symbols: TMenuItem
      Caption = 'Symbol festlegen'
      Hint = 'W'#228'hlen Sie ein neues Symbol'
    end
    object N2: TMenuItem
      Caption = '-'
    end
    object Ausrstunghinzufgen2: TMenuItem
      Action = MDIForm.ActionItemAdd
    end
    object Ausrstunglschen2: TMenuItem
      Action = MDIForm.ActionItemDelete
    end
    object N3: TMenuItem
      Caption = '-'
    end
    object Ausschneiden1: TMenuItem
      Action = MDIForm.ActionItemCut
    end
    object Kopieren1: TMenuItem
      Action = MDIForm.ActionItemCopy
    end
    object Einfgen1: TMenuItem
      Action = MDIForm.ActionItemPaste
    end
    object N1: TMenuItem
      Caption = '-'
    end
    object Sortierennach1: TMenuItem
      Caption = '&Sortieren nach'
      object MenuSName: TMenuItem
        Caption = '&Name'
        Checked = True
        GroupIndex = 1
        Hint = 'Nach Name sortieren'
        RadioItem = True
        OnClick = SortClick
      end
      object MenuSTyp: TMenuItem
        Tag = 1
        Caption = '&Typ'
        GroupIndex = 1
        Hint = 'Nach Typ sortieren'
        RadioItem = True
        OnClick = SortClick
      end
      object MenuSVer: TMenuItem
        Tag = 3
        Caption = '&Verf'#252'gbar nach'
        GroupIndex = 1
        Hint = 'Nach Verf'#252'gbar nach sortieren'
        RadioItem = True
        OnClick = SortClick
      end
    end
  end
end
