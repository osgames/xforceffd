unit frmAlien;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ComCtrls, XForce_types, KD4Utils, ExtCtrls, Menus, StdCtrls, ExtRecord;

type
  TAlienForm = class(TFrame)
    AlienList: TListView;
    PopupMenu1: TPopupMenu;
    Alienhinzufgen1: TMenuItem;
    Alienlschen1: TMenuItem;
    PopupAlienEdit: TMenuItem;
    N1: TMenuItem;
    N2: TMenuItem;
    SortAlien: TMenuItem;
    Name1: TMenuItem;
    Gesundheit1: TMenuItem;
    Panzerung1: TMenuItem;
    Strke1: TMenuItem;
    PSIAngriff1: TMenuItem;
    PSIAbweh1: TMenuItem;
    Zeiteinheiten1: TMenuItem;
    Angriffnach1: TMenuItem;
    Gesamtstrke1: TMenuItem;
    Panel2: TPanel;
    Image1: TImage;
    Label2: TLabel;
    Umbennen1: TMenuItem;
    N3: TMenuItem;
    Kopieren1: TMenuItem;
    Einfgen1: TMenuItem;
    Ausschneiden1: TMenuItem;
    FilterBox: TEdit;
    procedure AlienListCompare(Sender: TObject; Item1, Item2: TListItem;
      Data: Integer; var Compare: Integer);
    procedure AlienListColumnClick(Sender: TObject; Column: TListColumn);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure SortAlienClick(Sender: TObject);
    procedure AlienListDblClick(Sender: TObject);
    procedure AlienListEdited(Sender: TObject; Item: TListItem;
      var S: String);
    procedure FilterBoxChange(Sender: TObject);
  public
    procedure AddAlien(Alien: TExtRecord; Index: Integer);
    procedure CreateTree;
    function CalculateGesamt(Alien: TExtRecord): Integer;
    { Public-Deklarationen }
  end;

implementation

uses frmMain, string_utils;

{$R *.DFM}

{ TAlienForm }

procedure TAlienForm.CreateTree;
var
  Dummy : Integer;
begin
  AlienList.Items.BeginUpdate;
  AlienList.Items.Clear;
  with MDIForm do
  begin
    for Dummy:=0 to length(Aliens)-1 do
    begin
      if (FilterBox.Text<>'') then
      begin
        Text:=string_utils_GetLanguageString(MDIForm.Sprache,MDIForm.Aliens[Dummy].GetString('Name'));
        if (Pos(lowercase(FilterBox.Text),LowerCase(Text))=0) then
          continue;
      end;

      AddAlien(Aliens[Dummy],Dummy);
    end;
  end;
  AlienList.CustomSort(nil,0);
  AlienList.Items.EndUpdate;
end;

procedure TAlienForm.AlienListCompare(Sender: TObject; Item1,
  Item2: TListItem; Data: Integer; var Compare: Integer);
const
  SortField : Array[1..6] of String = ('Ges','Pan','PSAn','PSAb','Zei','Ver');
var
  Index1,Index2: Integer;
begin
  Index1:=Item1.ImageIndex;
  Index2:=Item2.ImageIndex;
  case AlienList.Tag of
    0: Compare:=CompareText(Item1.Caption,Item2.Caption);
    1..6: Compare:=MDIForm.Aliens[Index2].GetInteger(SortField[AlienList.Tag])-MDIForm.Aliens[Index1].GetInteger(SortField[AlienList.Tag]);
    7: Compare:=CalculateGesamt(MDIForm.Aliens[Index2])-CalculateGesamt(MDIForm.Aliens[Index1]);
  end;
end;

procedure TAlienForm.AlienListColumnClick(Sender: TObject;
  Column: TListColumn);
begin
  AlienList.Tag:=Column.Index;
  AlienList.CustomSort(nil,0);
end;

procedure TAlienForm.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  Action:=caFree;
  MDIForm.AlienViewer:=nil;
end;

function TAlienForm.CalculateGesamt(Alien: TExtRecord): Integer;
var
  Gesamt: Integer;
begin
  Gesamt:=Alien.GetInteger('Ges')+
          Alien.GetInteger('Pan')+
          Alien.GetInteger('PSAn')+
          Alien.GetInteger('Zei')+
          Alien.GetInteger('PSAb')-40;

  result:=round(Gesamt/860*100);
end;

procedure TAlienForm.AddAlien(Alien: TExtRecord; Index: Integer);
var
  ListItem: TListItem;
begin
  ListItem:=AlienList.Items.Add;
  ListItem.Caption:=string_utils_GetLanguageString(MDIForm.Sprache,Alien.GetString('Name'));
  ListItem.ImageIndex:=Index;
  ListItem.SubItems.Add(IntToStr(Alien.GetInteger('Ges')));
  ListItem.SubItems.Add(IntToStr(Alien.GetInteger('Pan')));
  ListItem.SubItems.Add(IntToStr(Alien.GetInteger('PSAn')));
  ListItem.SubItems.Add(IntToStr(Alien.GetInteger('PSAb')));
  ListItem.SubItems.Add(IntToStr(Alien.GetInteger('Zei')));
  ListItem.SubItems.Add(ZahlString('%d Tage','%d Tagen',Alien.GetInteger('Ver')));
  ListItem.SubItems.Add(IntToStr(CalculateGesamt(Alien))+' %');
end;

procedure TAlienForm.SortAlienClick(Sender: TObject);
begin
  AlienList.Tag:=(Sender as TComponent).Tag;
  AlienList.CustomSort(nil,0);
end;

procedure TAlienForm.AlienListDblClick(Sender: TObject);
begin
  MDIForm.ActionAlienEdit.Execute;
end;

procedure TAlienForm.AlienListEdited(Sender: TObject; Item: TListItem;
  var S: String);
var
  Dummy: Integer;
  Index: Integer;
  Text : String;
begin
  Index:=Integer(Item.ImageIndex);
  for Dummy:=0 to length(MDIForm.Aliens)-1 do
  begin
    if (MDIForm.Aliens[Dummy].GetString('Name')=S) and (MDIForm.Aliens[Dummy].GetCardinal('ID')<>MDIForm.Aliens[Index].GetCardinal('ID')) then
    begin
      Text:=Format('%s ist bereits definiert',[S]);
      Application.MessageBox(PChar(Text),PChar('Hinweis'),MB_ICONINFORMATION or MB_OK);
      S:=MDIForm.Aliens[Index].GetString('Name');
      exit;
    end;
  end;
  MDIForm.Aliens[Index].SetString('Name',string_utils_SetLanguageString(MDIForm.Sprache,MDIForm.Aliens[Index].GetString('Name'),S));
  MDIForm.ForschChanged:=true;
  AlienList.CustomSort(nil,0);
end;

procedure TAlienForm.FilterBoxChange(Sender: TObject);
begin
  CreateTree; 
end;

end.
