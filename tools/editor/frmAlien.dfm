object AlienForm: TAlienForm
  Left = 0
  Top = 0
  Width = 443
  Height = 275
  Align = alClient
  TabOrder = 0
  object Panel2: TPanel
    Left = 0
    Top = 0
    Width = 443
    Height = 22
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    DesignSize = (
      443
      22)
    object Image1: TImage
      Left = 2
      Top = 2
      Width = 16
      Height = 16
      AutoSize = True
      Picture.Data = {
        07544269746D617036030000424D360300000000000036000000280000001000
        0000100000000100180000000000000300000000000000000000000000000000
        0000FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
        FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
        FFFF00FF000000000000000000FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF
        00FFFF00FFFF00FFFF00FFFF00FFFF00FF000000808080808080808080000000
        FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF0000
        00808080808080808080808080808080000000FF00FFFF00FFFF00FFFF00FFFF
        00FFFF00FFFF00FFFF00FF000000808080808080000000808080000000808080
        808080000000FF00FFFF00FFFF00FFFF00FFFF00FFFF00FF0000008080808080
        80808080808080808080808080808080808080808080000000FF00FFFF00FFFF
        00FFFF00FFFF00FF000000808080808080808080808080808080808080808080
        808080808080000000FF00FFFF00FFFF00FFFF00FFFF00FF0000008080808080
        80000000000000808080000000000000808080808080000000FF00FFFF00FFFF
        00FFFF00FFFF00FF000000808080808080000000000000808080000000000000
        808080808080000000FF00FFFF00FFFF00FFFF00FFFF00FF0000008080800000
        00000000808080808080808080000000000000808080000000FF00FFFF00FFFF
        00FFFF00FFFF00FF000000808080808080808080808080808080808080808080
        808080808080000000FF00FFFF00FFFF00FFFF00FFFF00FF0000008080808080
        80808080808080808080808080808080808080808080000000FF00FFFF00FFFF
        00FFFF00FFFF00FFFF00FF000000808080808080808080808080808080808080
        808080000000FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF0000008080
        80808080808080808080808080808080808080000000FF00FFFF00FFFF00FFFF
        00FFFF00FFFF00FFFF00FFFF00FF000000808080808080808080808080808080
        000000FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
        FF000000000000000000000000000000FF00FFFF00FFFF00FFFF00FFFF00FFFF
        00FF}
      Transparent = True
    end
    object Label2: TLabel
      Left = 19
      Top = 4
      Width = 39
      Height = 13
      Caption = 'Aliens:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object FilterBox: TEdit
      Left = 272
      Top = 0
      Width = 170
      Height = 21
      Hint = 'Filter'
      Anchors = [akTop, akRight]
      TabOrder = 0
      OnChange = FilterBoxChange
    end
  end
  object AlienList: TListView
    Left = 0
    Top = 22
    Width = 443
    Height = 253
    Hint = 'Zeigt alle Aliens zum bearbeiten an.'
    Align = alClient
    Columns = <
      item
        Caption = 'Name'
        Width = 150
      end
      item
        Alignment = taRightJustify
        Caption = 'Gesundheit'
        Width = 75
      end
      item
        Alignment = taRightJustify
        Caption = 'Panzerung'
        Width = 75
      end
      item
        Caption = 'PSI-Angriff'
      end
      item
        Alignment = taRightJustify
        Caption = 'PSI-Abwehr'
        Width = 75
      end
      item
        Alignment = taRightJustify
        Caption = 'Zeiteinheiten'
        Width = 80
      end
      item
        Alignment = taRightJustify
        Caption = 'Angriff nach '
        Width = 75
      end
      item
        Alignment = taRightJustify
        Caption = 'Gesamtst'#228'rke'
        Width = 90
      end>
    FullDrag = True
    HideSelection = False
    RowSelect = True
    PopupMenu = PopupMenu1
    TabOrder = 0
    ViewStyle = vsReport
    OnColumnClick = AlienListColumnClick
    OnCompare = AlienListCompare
    OnDblClick = AlienListDblClick
    OnEdited = AlienListEdited
  end
  object PopupMenu1: TPopupMenu
    Images = MDIForm.ImageList1
    Left = 32
    Top = 40
    object PopupAlienEdit: TMenuItem
      Action = MDIForm.ActionAlienEdit
      Default = True
    end
    object Umbennen1: TMenuItem
      Action = MDIForm.ActionAlienRename
    end
    object N1: TMenuItem
      Caption = '-'
    end
    object Alienhinzufgen1: TMenuItem
      Action = MDIForm.ActionAlienAdd
    end
    object Alienlschen1: TMenuItem
      Action = MDIForm.ActionAlienDelete
    end
    object N3: TMenuItem
      Caption = '-'
    end
    object Ausschneiden1: TMenuItem
      Action = MDIForm.ActionAlienCut
    end
    object Kopieren1: TMenuItem
      Action = MDIForm.ActionAlienCopy
    end
    object Einfgen1: TMenuItem
      Action = MDIForm.ActionAlienPaste
    end
    object N2: TMenuItem
      Caption = '-'
    end
    object SortAlien: TMenuItem
      Caption = 'Sortieren nach'
      object Name1: TMenuItem
        Caption = 'Name'
        Checked = True
        GroupIndex = 1
        RadioItem = True
        OnClick = SortAlienClick
      end
      object Gesundheit1: TMenuItem
        Tag = 1
        Caption = 'Gesundheit'
        GroupIndex = 1
        RadioItem = True
        OnClick = SortAlienClick
      end
      object Panzerung1: TMenuItem
        Tag = 2
        Caption = 'Panzerung'
        GroupIndex = 1
        RadioItem = True
        OnClick = SortAlienClick
      end
      object Strke1: TMenuItem
        Tag = 3
        Caption = 'St'#228'rke'
        GroupIndex = 1
        RadioItem = True
        OnClick = SortAlienClick
      end
      object PSIAngriff1: TMenuItem
        Tag = 4
        Caption = 'PSI-Angriff'
        GroupIndex = 1
        RadioItem = True
        OnClick = SortAlienClick
      end
      object PSIAbweh1: TMenuItem
        Tag = 5
        Caption = 'PSI-Abwehr'
        GroupIndex = 1
        RadioItem = True
        OnClick = SortAlienClick
      end
      object Zeiteinheiten1: TMenuItem
        Tag = 6
        Caption = 'Zeiteinheiten'
        GroupIndex = 1
        RadioItem = True
        OnClick = SortAlienClick
      end
      object Angriffnach1: TMenuItem
        Tag = 7
        Caption = 'Angriff nach'
        GroupIndex = 1
        RadioItem = True
        OnClick = SortAlienClick
      end
      object Gesamtstrke1: TMenuItem
        Tag = 8
        Caption = 'Gesamtst'#228'rke'
        GroupIndex = 1
        RadioItem = True
        OnClick = SortAlienClick
      end
    end
  end
end
