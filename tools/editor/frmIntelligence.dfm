object IntelligenzForm: TIntelligenzForm
  Left = 606
  Top = 297
  BorderStyle = bsDialog
  Caption = 'Intelligenz einstellen'
  ClientHeight = 298
  ClientWidth = 283
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poMainFormCenter
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 8
    Top = 8
    Width = 50
    Height = 13
    Caption = 'Intelligenz:'
  end
  object ListCaption: TLabel
    Left = 8
    Top = 56
    Width = 145
    Height = 13
    Caption = 'Waffen die %s benutzen kann:'
  end
  object Edit1: TEdit
    Left = 88
    Top = 3
    Width = 33
    Height = 21
    TabOrder = 0
    OnChange = Edit1Change
  end
  object TrackBar1: TTrackBar
    Left = 3
    Top = 24
    Width = 275
    Height = 27
    Max = 200
    PageSize = 25
    Frequency = 10
    TabOrder = 1
    ThumbLength = 15
    OnChange = TrackBar1Change
  end
  object ListBox1: TListBox
    Left = 8
    Top = 72
    Width = 265
    Height = 185
    Style = lbOwnerDrawFixed
    ItemHeight = 16
    TabOrder = 2
  end
  object OKButton: TBitBtn
    Left = 88
    Top = 264
    Width = 89
    Height = 25
    TabOrder = 3
    Kind = bkOK
  end
  object BitBtn2: TBitBtn
    Left = 184
    Top = 264
    Width = 89
    Height = 25
    TabOrder = 4
    Kind = bkCancel
  end
end
