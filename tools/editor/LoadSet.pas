unit LoadSet;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, ComCtrls, ExtCtrls, ArchivFile, Koding, ImgList;

type
  TLoadGameSet = class(TForm)
    FileListView: TListView;
    OKButton: TBitBtn;
    CancelButton: TBitBtn;
    ImageList1: TImageList;
    DeleteButton: TBitBtn;
    CopyButton: TBitBtn;
    procedure FormShow(Sender: TObject);
    procedure OKButtonClick(Sender: TObject);
    procedure CancelButtonClick(Sender: TObject);
    procedure FileListViewDblClick(Sender: TObject);
    procedure FileListViewSelectItem(Sender: TObject; Item: TListItem;
      Selected: Boolean);
    procedure DeleteButtonClick(Sender: TObject);
    procedure FileListViewColumnClick(Sender: TObject;
      Column: TListColumn);
    procedure FileListViewCompare(Sender: TObject; Item1, Item2: TListItem;
      Data: Integer; var Compare: Integer);
  private
    function GetFileName: String;
    function GetSetName: String;
    { Private-Deklarationen }
  public
    property FileName: String read GetFileName;
    property SatzName: String read GetSetName;
    { Public-Deklarationen }
  end;

var
  LoadGameSet: TLoadGameSet;

implementation

uses frmPasswort, KD4Utils, XForce_types;

{$R *.DFM}

procedure TLoadGameSet.FormShow(Sender: TObject);
var
  Stat     : TSearchRec;
  Status   : Integer;
  Item     : TListItem;
  FileDate : FILETIME;
  SysTime  : TSystemTime;
  DateTime : TDateTime;
  Archiv   : TArchivFile;
  m        : TMemoryStream;
  PassCRC  : Cardinal;
  Text     : String[30];
  SName    : String;
  Version  : Integer;
begin
  OKButton.Enabled:=false;
  DeleteButton.Enabled:=false;
  
  FileListView.Items.Clear;
  Status:=FindFirst('data\gameSets\*.pak',faAnyFile,Stat);
  Archiv:=TArchivFile.Create;
  m:=TMemoryStream.Create;
  while Status=0 do
  begin
    SName:='';
    try
      m.Clear;
      PassCRC:=0;
      Text:='';
      try
        Archiv.OpenArchiv('data\GameSets\'+Stat.Name);
        Archiv.OpenRessource('Header');
        Decode(Archiv.Stream,m);
        m.Read(Version,SizeOf(Version));
        if (Version<>HeadVersion) then
        begin
          m.Position:=0;
          m.Read(Text,SizeOf(Text));
          SName:=Text;
          m.Read(PassCRC,SizeOf(PassCRC));
        end
        else
        begin
          SName:=ReadString(m);
          ReadString(m);ReadString(m);ReadString(m);  // Autor, Organisation, Beschreibung �berlesen
          m.Read(Version,SizeOf(Boolean));  // CanImport �berlesen
          m.Read(PassCRC,SizeOf(PassCRC));
        end;
      except
      end;
      Archiv.CloseArchiv;
      FileTimeToLocalFileTime(Stat.FindData.ftLastWriteTime,FileDate);
      FileTimeToSystemTime(FileDate,SysTime);
      DateTime := SystemTimeToDateTime(SysTime);
      Item:=FileListView.Items.Add;
      Item.Caption:=SName;
      Item.SubItems.Add(Stat.Name);
      Item.SubItems.Add(DateTimeToStr(DateTime));
      if PassCRC<>0 then
      begin
        Item.SubItems.Add('vorhanden');
        Item.SubItems.Add('$'+IntToHex(PassCRC,8));
      end
      else
        Item.SubItems.Add('');
    except
    end;
    Status:=FindNext(Stat);
  end;
  m.Free;
  Archiv.Free;
  FindClose(Stat);
end;

function TLoadGameSet.GetFileName: String;
begin
  result:=FileListView.Selected.SubItems[0];
end;

function TLoadGameSet.GetSetName: String;
begin
  result:=FileListView.Selected.Caption;
end;

procedure TLoadGameSet.OKButtonClick(Sender: TObject);
var
  Pass      : String[30];
  FileCRC   : Cardinal;
  CRC       : Cardinal;
  Dummy     : Integer;
  Wert      : Cardinal;
begin
  if FileListView.Selected.SubItems[2]<>'' then
  begin
    PasswordDlg.Password.Text:='';
    if PasswordDlg.ShowModal=mrCancel then
    begin
      ModalResult:=mrNone;
      exit;
    end
    else
    begin
      FillChar(Pass,SizeOf(Pass),#0);
      Pass:=PasswordDlg.Password.Text;
      CRC:=$80808080;
      for Dummy:=1 to length(Pass) do
      begin
        Wert:=Integer(Pass[Dummy]);
        CRC:=(not CRC xor (CRC shl 16) xor ((not Wert) xor ((Wert shl 30) xor (Wert shl 19))));
      end;
      FileCRC:=StrToInt(FileListView.Selected.SubItems[3]);
      if CRC<>FileCRC then
      begin
        Application.MessageBox(PChar('Ung�ltiges Passwort.'#13#10'Achten Sie auf Gro�- und Kleinschreibung.'),PCHar('Fehler'),MB_OK or MB_ICONERROR);
        ModalResult:=mrNone;
        exit;
      end;
    end;
  end;
  ModalResult:=mrOk;
end;

procedure TLoadGameSet.CancelButtonClick(Sender: TObject);
begin
  ModalResult:=mrCancel;
end;

procedure TLoadGameSet.FileListViewDblClick(Sender: TObject);
begin
  if OKButton.Enabled then OKButton.Click;
end;

procedure TLoadGameSet.FileListViewSelectItem(Sender: TObject;
  Item: TListItem; Selected: Boolean);
begin
  OKButton.Enabled:=Selected;
  DeleteButton.Enabled:=Selected;
end;

procedure TLoadGameSet.DeleteButtonClick(Sender: TObject);
begin
  if MessageDlg('Soll der Spielsatz '+FileListView.Selected.Caption+' gel�scht werden?',mtConfirmation,[mbYes,mbNo],0) = mrYes then
  begin
    Assert(DeleteFile('data\gamesets\'+FileListView.Selected.SubItems[0]));
    FileListView.Selected.Delete;
  end;
end;

procedure TLoadGameSet.FileListViewColumnClick(Sender: TObject;
  Column: TListColumn);
begin
  FileListView.Tag:=Column.Index;
  FileListView.CustomSort(nil,0);
end;

procedure TLoadGameSet.FileListViewCompare(Sender: TObject; Item1,
  Item2: TListItem; Data: Integer; var Compare: Integer);
begin
  case FileListView.Tag of
    0: Compare:=CompareText(Item1.Caption,Item2.Caption);
    1,3: Compare:=CompareText(Item1.SubItems[FileListView.Tag-1],Item2.SubItems[FileListView.Tag-1]);
    2: Compare:=round((StrToDateTime(Item1.SubItems[1])-StrToDateTime(Item2.SubItems[1]))*10000);
  end;
end;

end.
