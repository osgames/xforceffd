unit RecycleForm;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ComCtrls, ExtCtrls,KD4Utils;

type
  TRecycledTyp = (rtProjekt,rtStart,rtAlient);
  TRecycledObject = record
    Typ   : TRecycledTyp;
    Index : Integer;
  end;
  TRecycledForm = class(TFrame)
    DeletedView: TListView;
    Panel1: TPanel;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure DeletedViewCompare(Sender: TObject; Item1, Item2: TListItem;
      Data: Integer; var Compare: Integer);
    procedure DeletedViewColumnClick(Sender: TObject; Column: TListColumn);
  private
    Objekts: Array of TRecycledObject;
    function AddObject(Typ: TRecycledTyp;Index: Integer): Integer;
    { Private-Deklarationen }
  public
    procedure GetDeleted;
    procedure AddDeleted(Index: Integer);
    { Public-Deklarationen }
  end;

implementation

uses frmMain;

{$R *.DFM}

procedure TRecycledForm.GetDeleted;
var
  Dummy: Integer;
begin
  SetLength(Objekts,0);
  DeletedView.Items.Clear;
  for Dummy:=0 to length(MDIForm.Projekts)-1 do
  begin
    if MDIForm.DeletedChild[Dummy] then
    begin
      AddDeleted(Dummy);
    end;
  end;
end;

procedure TRecycledForm.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action:=caFree;
  MDIForm.RecycleViewer:=nil;
end;

procedure TRecycledForm.AddDeleted(Index: Integer);
var
  Item: TListItem;
begin
  Item:=DeletedView.Items.Add;
  Item.Caption:=MDIForm.Projekts[Index].GetString('Name');
  if MDIForm.Projekts[Index].GetBoolean('Start') then
  begin
    Item.Subitems.Add('Ausrüstung');
    Item.ImageIndex:=AddObject(rtStart,Index);
  end
  else
  begin
    Item.SubItems.Add('Forschungsprojekt');
    Item.ImageIndex:=AddObject(rtProjekt,Index);
  end;
  Item.SubItems.Add(MDIForm.Projekts[Index].RecordDefinition.RecordName);
  DeletedView.CustomSort(nil,0);
end;

procedure TRecycledForm.DeletedViewCompare(Sender: TObject; Item1,
  Item2: TListItem; Data: Integer; var Compare: Integer);
begin
  if DeletedView.Tag=0 then
  begin
    Compare:=CompareText(Item1.Caption,Item2.Caption);
  end
  else
    Compare:=CompareText(Item1.SubItems[DeletedView.Tag-1],Item2.SubItems[DeletedView.Tag-1]);
end;

procedure TRecycledForm.DeletedViewColumnClick(Sender: TObject;
  Column: TListColumn);
begin
  DeletedView.Tag:=Column.Index;
  DeletedView.CustomSort(nil,0);
end;

function TRecycledForm.AddObject(Typ: TRecycledTyp;
  Index: Integer): Integer;
begin
  SetLength(Objekts,length(Objekts)+1);
  result:=length(Objekts)-1;
  Objekts[result].Typ:=Typ;
  Objekts[result].Index:=Index;
end;

end.
