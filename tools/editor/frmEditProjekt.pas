unit frmEditProjekt;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  JvComponent, JvInspector, TypInfo, XForce_types, ExtRecord, ProjektRecords,
  ImgList, StdCtrls, Buttons, ArchivFile, Koding, KD4Utils, ObjektRecords,
  JvExControls, JvComponentBase;

type

  TJvInspectorCustomItem = class;

  TEditProjektForm = class(TForm)
    JvInspector: TJvInspector;
    JvInspectorBorlandPainter: TJvInspectorBorlandPainter;
    BitBtn2: TBitBtn;
    ListBox1: TListBox;
    Label1: TLabel;
    Button1: TBitBtn;
    Button2: TBitBtn;
    PatentButton: TBitBtn;
    procedure FormDestroy(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BitBtn2Click(Sender: TObject);
    procedure ListBox1DblClick(Sender: TObject);
    procedure ListBox1DrawItem(Control: TWinControl; Index: Integer;
      Rect: TRect; State: TOwnerDrawState);
    procedure Button2Click(Sender: TObject);
    procedure PatentButtonClick(Sender: TObject);
    procedure FormHide(Sender: TObject);
    procedure JvInspectorDataValueChanged(Sender: TObject;
      const Data: TJvCustomInspectorData);
    procedure JvInspectorBorlandPainterSetItemColors(
      Item: TJvCustomInspectorItem; Canvas: TCanvas);
  private
    fRecord         : TExtRecord;
    fJvitems        : Array of TJvCustomInspectorItem;
    fLandList       : TStringList;
    fWaffenTypes    : TStringList;
    fRoomTyps       : TStringList;

    fListen      : TList;
    procedure ApplyRecordData;
    function GetExtRecord: TExtRecord;
    procedure SetExtRecord(const Value: TExtRecord);

    function CheckRecord: Boolean;

    procedure OnChangedAbwehrValue(Sender: TObject);

    procedure OnEditSymbol(Sender: TObject);
    procedure OnEditLand(Sender: TObject);
    procedure OnEditWaffenType(Sender: TObject);
    procedure OnEditWaffe(Sender: TObject);
    procedure OnEditPaedieImage(Sender: TObject);
    procedure OnEditSound(Sender: TObject);
    procedure OnEditLanguageText(Sender: TObject);
    procedure OnEditInfoText(Sender: TObject);
    procedure OnEditAliens(Sender: TObject);
    procedure OnEditIQ(Sender: TObject);
    procedure OnEditMissionSkript(Sender: TObject);
    procedure OnEditMissionTriggers(Sender: TObject);
    procedure OnEditBasisBauImage(Sender: TObject);
    procedure OnEditRoomTyp(Sender: TObject);

    procedure OnGetLandname(Sender: TObject; var Name: String);
    procedure OnGetWaffenType(Sender: TObject; var Name: String);
    procedure OnGetLanguageText(Sender: TObject; var Name: String);
    procedure OnGetIQ(Sender: TObject; var Name: String);
    procedure OnGetAliens(Sender: TObject; var Name: String);
    procedure OnGetRoomTyp(Sender: TObject; var Name: String);
    procedure OnGetImageName(Sender: TObject; var Name: String);

    procedure GetWaffenNamen(Item: TJvInspectorCustomItem);

    procedure CheckBitmapUFOPaedie(Bitmap: TBitmap);
    procedure CheckBitmapBasisBau(Bitmap: TBitmap);

    procedure ChangeReadOnly(Value: String; ReadOnly: Boolean);
    procedure SetToZero(Value: String);
    procedure UnCheckValues(Value: String);

    function CheckVisible(Index: Integer): boolean;
    procedure LandItemDraw(Control: TWinControl; Index: Integer;
      Rect: TRect; State: TOwnerDrawState);
    procedure WaffeItemDraw(Control: TWinControl; Index: Integer;
      Rect: TRect; State: TOwnerDrawState);
    { Private-Deklarationen }

    procedure ShowPatentForm;

    procedure ClearListen;
  public
    procedure NewProjekt;
    property ExtRecord: TExtRecord read GetExtRecord write SetExtRecord;
    { Public-Deklarationen }
  end;

  PBoolean   = ^Boolean;
  PCardinal  = ^Cardinal;
  PPointer   = ^Pointer;
  PObject    = ^TObject;

  PCustomIndex = ^TCustomIndex;
  TCustomIndex = type Integer;

  PCustomID = ^TCustomID;
  TCustomID = type Cardinal;

  PCustomPointer = ^TCustomPointer;
  TCustomPointer = type Cardinal;

  PBinaryData = ^TBinaryData;
  TBinaryData = record
    Address : Pointer;
    Length  : Integer;
  end;

  PCustomString = ^TCustomString;
  TCustomString = type String;

  PDivisorInteger = ^TDivisorInteger;
  TDivisorInteger = type Integer;

  TGetDataName = procedure(Sender: TObject; var DataName: String) of object;

  TJvInspectorCustomItem = class(TJvCustomInspectorItem)
  private
    fOnEdit: TNotifyEvent;
    fDisplayText: String;
    fGetDataName: TGetDataName;
  protected
    function GetDisplayValue: string;override;
    procedure Edit; override;
    procedure SetFlags(const Value: TInspectorItemFlags); override;
    class procedure RegisterAsDefaultItem;
  public
    property Display       : String read fDisplayText write fDisplayText;
    property OnEdit        : TNotifyEvent read fOnEdit write fOnEdit;
    property OnGetDataName : TGetDataName read fGetDataName write fGetDataName;

  end;

  TJvInspectorDivisorItem = class(TJvInspectorIntegerItem)
  private
    fDivisor: Integer;
  protected
    class procedure RegisterAsDefaultItem;
    procedure SetDisplayValue(const Value: string); override;
  public
    property Divisor: Integer read fDivisor write fDivisor;
  end;

var
  EditProjektForm: TEditProjektForm;
  MemoryPool: Pointer;

implementation

uses
  frmInfo, SymbEdit, frmListDialog, frmMain, frmPaedieImage, Patent, string_utils,
  frmNameEdit, frmAliensInUFO, frmIntelligence, SkriptEdit, MissionTriggers,
  frmSoundEdit;

{$R *.DFM}

{ TEditProjektForm }

function TEditProjektForm.CheckVisible(Index: Integer): boolean;
begin
  result:=true;
  if fRecord.GetValueProperty(Index,'Visible')='false' then
  begin
    result:=false;
    exit;
  end;

  if fRecord.GetValueProperty(Index,'Start')<>'' then
  begin
    if (fRecord.ValueExists('AlienItem')) and (fRecord.GetBoolean('AlienItem')=true) then
    begin
      // Alienausr�stung muss Start = true
      if fRecord.GetValueProperty(Index,'Start')='true' then
      begin
        result:=false;
        exit;
      end;
      exit;
    end;

    if fRecord.GetValueProperty(Index,'Start')=lowercase(BooleanIdents[not fRecord.GetBoolean('Start')]) then
    begin
      result:=false;
      exit;
    end;

    if fRecord.GetValueProperty(Index,'Alien')='true' then
    begin
      result:=false;
      exit;
    end;
  end;
end;

procedure TEditProjektForm.SetExtRecord(const Value: TExtRecord);
var
  Dummy   : Integer;
  Adress  : Integer;
  katList : TList;
  Typ     : TWaffenType;
  NewItem : TJvCustomInspectorItem;
  RoomTyp : Integer;

  function GetKategorie(Text: String): TJvCustomInspectorItem;
  var
    Dummy: Integer;
  begin
    if Text='' then
      Text:='Keine Kategorie';
    for Dummy:=0 to katList.count-1 do
    begin
      if TJvCustomInspectorItem(katList[Dummy]).DisplayName=Text then
      begin
        result:=TJvInspectorCustomItem(katList[Dummy]);
        exit;
      end;
    end;

    result:=TJvInspectorCustomItem(TJvInspectorCustomCategoryItem.Create(JvInspector.Root, nil));
    result.SortKind:=iskNone;
    result.DisplayName:=Text;
    result.Expanded:=true;
    katList.Add(Result);
  end;

begin
  JvInspector.Clear;
  JvInspector.Root.SortKind:=iskNone;
  ClearListen;
  ListBox1.Items.Clear;

  if fRecord<>nil then
    fRecord.Free;

  katList:=TList.Create;

  fRecord:=TExtRecord.Create(Value.RecordDefinition);
  fRecord.Assign(Value);

  ZeroMemory(memoryPool,20000);
  Adress:=Integer(MemoryPool);
  Caption:=fRecord.RecordDefinition.RecordName+' - '+string_utils_GetLanguageString(MDIForm.Sprache,fRecord.GetString('Name'));

  SetLength(fJVItems,fRecord.ValueCount);
  for Dummy:=0 to fRecord.ValueCount-1 do
  begin
    NewItem:=nil;
    fJVItems[Dummy]:=nil;
    if not CheckVisible(Dummy) then
      continue;


    case fRecord.Values[Dummy].ValueType of
      ervtCardinal:
      begin
        if fRecord.GetValueProperty(Dummy,'Custom')='Waffe' then
        begin
          NewItem := TJvInspectorVarData.New(GetKategorie(fRecord.GetValueProperty(Dummy,'Category')), fRecord.GetValueProperty(Dummy,'Name'), TypeInfo(TCustomID),PCustomID(Adress));
          (NewItem as TJvInspectorCustomItem).OnEdit:=OnEditWaffe;
          GetWaffennamen((NewItem as TJvInspectorCustomItem));
        end
        else
          NewItem := TJvInspectorVarData.New(GetKategorie(fRecord.GetValueProperty(Dummy,'Category')),fRecord.GetValueProperty(Dummy,'Name'),typeInfo(Cardinal),PCardinal(Adress));
        PCardinal(Adress)^:=fRecord.Values[Dummy].valueCar;
        inc(Adress,sizeOf(Cardinal));
      end;
      ervtInteger:
      begin
        if fRecord.GetValueProperty(Dummy,'Custom')='Symbol' then
        begin
          NewItem := TJvInspectorVarData.New(GetKategorie(fRecord.GetValueProperty(Dummy,'Category')), fRecord.GetValueProperty(Dummy,'Name'), TypeInfo(TCustomIndex),PCustomIndex(Adress));
          (NewItem as TJvInspectorCustomItem).OnEdit:=OnEditSymbol;
          (NewItem as TJvInspectorCustomItem).Display:='( Symbol )';
	end
        else if fRecord.GetValueProperty(Dummy,'Custom')='Land' then
        begin
          NewItem := TJvInspectorVarData.New(GetKategorie(fRecord.GetValueProperty(Dummy,'Category')), fRecord.GetValueProperty(Dummy,'Name'), TypeInfo(TCustomIndex),PCustomIndex(Adress));
          (NewItem as TJvInspectorCustomItem).OnEdit:=OnEditLand;
          (NewItem as TJvInspectorCustomItem).OnGetDataName:=OnGetLandname;
        end
        else if fRecord.GetValueProperty(Dummy,'Custom')='IQ' then
        begin
          NewItem := TJvInspectorVarData.New(GetKategorie(fRecord.GetValueProperty(Dummy,'Category')), fRecord.GetValueProperty(Dummy,'Name'), TypeInfo(TCustomIndex),PCustomIndex(Adress));
          (NewItem as TJvInspectorCustomItem).OnEdit:=OnEditIQ;
          (NewItem as TJvInspectorCustomItem).OnGetDataName:=OnGetIQ;
        end
        else if fRecord.GetValueProperty(Dummy,'Custom')='WaffenType' then
        begin
          NewItem := TJvInspectorVarData.New(GetKategorie(fRecord.GetValueProperty(Dummy,'Category')), fRecord.GetValueProperty(Dummy,'Name'), TypeInfo(TCustomIndex),PCustomIndex(Adress));
          (NewItem as TJvInspectorCustomItem).OnEdit:=OnEditWaffenType;
          (NewItem as TJvInspectorCustomItem).OnGetDataName:=OnGetWaffenType;
        end
        else if fRecord.GetValueProperty(Dummy,'Custom')='Divisor' then
        begin
          NewItem := TJvInspectorVarData.New(GetKategorie(fRecord.GetValueProperty(Dummy,'Category')), fRecord.GetValueProperty(Dummy,'Name'), TypeInfo(TDivisorInteger),PCustomIndex(Adress));
          (NewItem as TJvInspectorDivisorItem).Divisor:=StrToInt(fRecord.GetValueProperty(Dummy,'Divisor'));
        end
        else if fRecord.GetValueProperty(Dummy,'Custom')='Divisor' then
        begin
          NewItem := TJvInspectorVarData.New(GetKategorie(fRecord.GetValueProperty(Dummy,'Category')), fRecord.GetValueProperty(Dummy,'Name'), TypeInfo(TDivisorInteger),PCustomIndex(Adress));
          (NewItem as TJvInspectorDivisorItem).Divisor:=StrToInt(fRecord.GetValueProperty(Dummy,'Divisor'));
        end
        else if fRecord.GetValueProperty(Dummy,'Custom')='RoomTyp' then
        begin
          NewItem := TJvInspectorVarData.New(GetKategorie(fRecord.GetValueProperty(Dummy,'Category')), fRecord.GetValueProperty(Dummy,'Name'), TypeInfo(TCustomIndex),PCustomIndex(Adress));
          (NewItem as TJvInspectorCustomItem).OnEdit:=OnEditRoomTyp;
          (NewItem as TJvInspectorCustomItem).OnGetDataName:=OnGetRoomTyp;
        end
        else
          NewItem:=TJvInspectorVarData.New(GetKategorie(fRecord.GetValueProperty(Dummy,'Category')),fRecord.GetValueProperty(Dummy,'Name'),typeInfo(Integer),PInteger(Adress));
        PInteger(Adress)^:=fRecord.Values[Dummy].valueInt;
        inc(Adress,sizeOf(Integer));
      end;
      ervtDouble:
      begin
        NewItem := TJvInspectorVarData.New(GetKategorie(fRecord.GetValueProperty(Dummy,'Category')),fRecord.GetValueProperty(Dummy,'Name'),typeInfo(double),PDouble(Adress));
        TJvInspectorFloatItem(NewItem).Format:='0.'+StringOfChar('0',fRecord.Values[Dummy].Kommasdoub);
        Pdouble(Adress)^:=fRecord.Values[Dummy].valuedoub;
        inc(Adress,sizeOf(double));
      end;
      ervtString:
      begin
        if fRecord.GetValueProperty(Dummy,'Custom')='UFOPaedieImage' then
        begin
          NewItem := TJvInspectorVarData.New(GetKategorie(fRecord.GetValueProperty(Dummy,'Category')), fRecord.GetValueProperty(Dummy,'Name'), TypeInfo(TCustomString),PString(Adress));
          (NewItem as TJvInspectorCustomItem).OnEdit:=OnEditPaedieImage;
          (NewItem as TJvInspectorCustomItem).Display:='( Bild )';
        end
        else if fRecord.GetValueProperty(Dummy,'Custom')='ItemName' then
        begin
          NewItem := TJvInspectorVarData.New(GetKategorie(fRecord.GetValueProperty(Dummy,'Category')), fRecord.GetValueProperty(Dummy,'Name'), TypeInfo(TCustomString),PString(Adress));
          (NewItem as TJvInspectorCustomItem).OnEdit:=OnEditLanguageText;
          (NewItem as TJvInspectorCustomItem).OnGetDataName:=OnGetLanguageText;
	end
        else if fRecord.GetValueProperty(Dummy,'Custom')='InfoText' then
        begin
          NewItem := TJvInspectorVarData.New(GetKategorie(fRecord.GetValueProperty(Dummy,'Category')), fRecord.GetValueProperty(Dummy,'Name'), TypeInfo(TCustomString),PString(Adress));
          (NewItem as TJvInspectorCustomItem).OnEdit:=OnEditInfoText;
          (NewItem as TJvInspectorCustomItem).OnGetDataName:=OnGetLanguageText;
	end
        else if fRecord.GetValueProperty(Dummy,'Custom')='MissionSkript' then
        begin
          NewItem := TJvInspectorVarData.New(GetKategorie(fRecord.GetValueProperty(Dummy,'Category')), fRecord.GetValueProperty(Dummy,'Name'), TypeInfo(TCustomString),PString(Adress));
          (NewItem as TJvInspectorCustomItem).OnEdit:=OnEditMissionSkript;
          (NewItem as TJvInspectorCustomItem).Display:='( Skript )';
	end
        else if fRecord.GetValueProperty(Dummy,'Custom')='BasisBauImage' then
        begin
          NewItem := TJvInspectorVarData.New(GetKategorie(fRecord.GetValueProperty(Dummy,'Category')), fRecord.GetValueProperty(Dummy,'Name'), TypeInfo(TCustomString),PString(Adress));
          (NewItem as TJvInspectorCustomItem).OnEdit:=OnEditBasisBauImage;
          (NewItem as TJvInspectorCustomItem).OnGetDataName:=OnGetImageName;
        end
        else if fRecord.GetValueProperty(Dummy,'Custom')='Sound' then
        begin
          NewItem := TJvInspectorVarData.New(GetKategorie(fRecord.GetValueProperty(Dummy,'Category')), fRecord.GetValueProperty(Dummy,'Name'), TypeInfo(TCustomString),PString(Adress));
          (NewItem as TJvInspectorCustomItem).OnEdit:=OnEditSound;
          (NewItem as TJvInspectorCustomItem).Display:='( Sound )';
        end
        else
          NewItem := TJvInspectorVarData.New(GetKategorie(fRecord.GetValueProperty(Dummy,'Category')),fRecord.GetValueProperty(Dummy,'Name'),typeInfo(String),PString(Adress));
        PString(Adress)^:=fRecord.Values[Dummy].valueString;
        inc(Adress,sizeOf(String));
      end;
      ervtBoolean:
      begin
        NewItem := TJvInspectorVarData.New(GetKategorie(fRecord.GetValueProperty(Dummy,'Category')),fRecord.GetValueProperty(Dummy,'Name'),typeInfo(Boolean),PBoolean(Adress));
        PBoolean(Adress)^:=fRecord.Values[Dummy].valueBool;
        inc(Adress,sizeOf(Boolean));
      end;
      ervtRecordList:
      begin
        TObject(PObject(Adress)^):=TExtRecordList.Create(fRecord.Values[Dummy].valueListRec);
        if fRecord.GetValueProperty(Dummy,'Custom')='Aliens' then
        begin
          NewItem := TJvInspectorVarData.New(GetKategorie(fRecord.GetValueProperty(Dummy,'Category')), fRecord.GetValueProperty(Dummy,'Name'), TypeInfo(TCustomPointer),PCardinal(Adress));
          (NewItem as TJvInspectorCustomItem).OnEdit:=OnEditAliens;
          (NewItem as TJvInspectorCustomItem).OnGetDataName:=OnGetAliens;
        end
        else if fRecord.GetValueProperty(Dummy,'Custom')='MissionSkriptTrigger' then
        begin
          NewItem := TJvInspectorVarData.New(GetKategorie(fRecord.GetValueProperty(Dummy,'Category')), fRecord.GetValueProperty(Dummy,'Name'), TypeInfo(TCustomPointer),PCardinal(Adress));
          (NewItem as TJvInspectorCustomItem).OnEdit:=OnEditMissionTriggers;
          (NewItem as TJvInspectorCustomItem).Display:='( Bedingungen )';
        end;
        fListen.Add(PObject(Adress)^);
        TExtRecordList(PObject(Adress)^).Assign(fRecord.Values[Dummy].valueList);

//        CopyMemory(PBinaryData(PPointer(Adress)^).Address,fRecord.Values[Dummy].binaryAddress,fRecord.Values[Dummy].binaryLength);
//        PBinaryData(PPointer(Adress)^).Length:=fRecord.Values[Dummy].binaryLength;
        inc(Adress,SizeOf(TObject));
      end;
    end;
    if NewItem=nil then
      continue;

    if fRecord.GetValueProperty(Dummy,'Custom')='Abwehr' then
      NewItem.OnValueChanged:=OnChangedAbwehrValue;

    if fRecord.GetValueProperty(Dummy,'NoEdit')='true' then
    begin
      if iifEditButton in NewItem.Flags then
        NewItem.ReadOnly:=true
      else
        NewItem.Flags:=NewItem.Flags+[iifEditFixed];
    end;

    if NewItem is TJvInspectorBooleanItem then
     TJvInspectorBooleanItem(NewItem).ShowAsCheckbox:=true;

    fJvItems[Dummy]:=NewItem;
  end;

  if fRecord.RecordDefinition=RecordBasis then
  begin
    ChangeReadOnly('Abwehr',fRecord.GetInteger('Abwehr')=0);

  end;

  if fRecord.ValueExists('Land') then
    ChangeReadOnly('Land',fRecord.GetInteger('Land')=-1);

  if fRecord.ValueExists('WaffType') then
  begin
    if (fRecord.RecordDefinition=RecordWaffe) or (fRecord.RecordDefinition=RecordRWaffe) then
    begin
      Typ:=TWaffenType(fRecord.GetInteger('WaffType'));
      ChangeReadOnly('Strength',not (Typ in [wtLaser,wtShortRange]));
      ChangeReadOnly('Munition',Typ<>wtLaser);
      ChangeReadOnly('Fernkampf',Typ=wtShortRange);
      ChangeReadOnly('Nahkampf',Typ<>wtShortRange);
    end
    else
    begin
      ChangeReadOnly('Laser',fRecord.GetInteger('WaffType')=Integer(wtLaser));
      ChangeReadOnly('Raketen',fRecord.GetInteger('WaffType')<>Integer(wtRaketen));
    end;
  end;

  if fRecord.ValueExists('RoomTyp') then
  begin
    RoomTyp:=fRecord.GetInteger('RoomTyp');

    for Dummy:=0 to fRoomTyps.Count-1 do
    begin
      ChangeReadOnly('RoomTyp'+IntToStr(Dummy),true);
    end;

    ChangeReadOnly('RoomTyp'+IntToStr(RoomTyp),false);
    
    ChangeReadOnly('Basement',not (TRoomTyp(RoomTyp) in [rtDefense,rtSensor,rtDefenseShield,rtLivingRoom,rtHangar]));
    ChangeReadOnly('Underground',not (TRoomTyp(RoomTyp) in [rtHangar,rtBaseRooms,rtLivingRoom]));
  end;

  katList.Free;
  JvInspector.Root.Sort;

  PatentButton.Visible:=fRecord.ValueExists('PatenZeit');
end;

procedure TEditProjektForm.FormDestroy(Sender: TObject);
var
  Dummy: Integer;
begin
  ClearListen;

  if fRecord<>nil then
  begin
    fRecord.Free;
    fRecord:=nil;
  end;

  FreeMemory(MemoryPool);
  fListen.Free;
  // Grafiken freigeben
  for Dummy:=0 to fLandList.Count-1 do
    fLandList.Objects[Dummy].Free;
    
  fLandList.Free;
  fWaffenTypes.Free;
  fRoomTyps.Free;
end;

function TEditProjektForm.GetExtRecord: TExtRecord;
begin
  result:=fRecord;
end;

procedure TEditProjektForm.FormCreate(Sender: TObject);
var
  Archiv  : TArchivFile;
  Bitmap  : TBitmap;
  Flag    : TBitmap;
  Dummy   : Integer;
  Land    : TLandInfo;
  Stream  : TMemoryStream;
  Version : Integer;
begin
  MemoryPool:=GetMemory(20000);

  fListen:=TList.Create;

  // L�nder laden
  fLandList:=TStringList.Create;
  Archiv:=TArchivFile.Create;
  Bitmap:=TBitmap.Create;
  Archiv.OpenArchiv(IncludeTrailingBackslash(ExtractFilePath(Application.Exename))+'data\xforce.pak',true);
  Archiv.OpenRessource('Organ');
  Bitmap.LoadFromStream(Archiv.Stream);
  Archiv.OpenArchiv(IncludeTrailingBackslash(ExtractFilePath(Application.Exename))+'data\data.pak',true);
  Archiv.OpenRessource('Organisationen');
  Stream:=TMemoryStream.Create;
  Decode(Archiv.Stream,Stream);
  Stream.Read(Version,SizeOf(Version));
  if Version<>LandVersion then
    raise EInvalidVersion.Create('Ung�ltige L�nderinformationen');
  fLandList.Add('alle L�nder');
  for Dummy:=0 to 24 do
  begin
    with Land do
    begin
      Stream.Read(ID,SizeOf(ID));
      Name:=ReadString(Stream);
      Oberhaupt:=ReadString(Stream);
      Stream.Read(StartFriendly,SizeOf(StartFriendly));
      Stream.Read(NearestSquare,SizeOf(NearestSquare));
      Stream.Read(Dichte,SizeOf(Dichte));
      Stream.Read(Felder,SizeOf(Felder));
    end;
    Flag:=TBitmap.Create;
    Flag.Width:=42;
    Flag.Height:=21;
    Flag.Canvas.CopyRect(Rect(0,0,42,21),Bitmap.Canvas,Rect(Dummy*42,0,(Dummy+1)*42,21));
    fLandList.AddObject(Land.Name,Flag);
  end;
  Archiv.Free;
  Bitmap.Free;
  Stream.Free;

  fWaffenTypes:=TStringList.Create;
  fWaffenTypes.Add('Projektil');
  fWaffenTypes.Add('Raketen');
  fWaffenTypes.Add('Laser');
  fWaffenTypes.Add('Chemisch');
  fWaffenTypes.Add('Nahkampf');

  fRoomTyps:=TStringList.Create;
  fRoomTyps.Add('Basiseinrichtung');  // 0
  fRoomTyps.Add('Abwehrturm');        // 1
  fRoomTyps.Add('Sensor');            // 2
  fRoomTyps.Add('Schild');            // 3
  fRoomTyps.Add('Quartier');          // 4
  fRoomTyps.Add('Hangar');            // 5
end;

procedure TEditProjektForm.OnChangedAbwehrValue(Sender: TObject);
begin
  ChangeReadOnly('Abwehr',TJvInspectorCustomItem(Sender).Data.AsOrdinal=0);
end;

procedure TEditProjektForm.ChangeReadOnly(Value: String; ReadOnly: Boolean);
var
  Dummy: Integer;
begin
  for Dummy:=0 to fRecord.ValueCount-1 do
  begin
    if fJvItems[Dummy]<>nil then
    begin
      if fRecord.GetValueProperty(Dummy,Value)='true' then
      begin
        fJvItems[Dummy].ReadOnly:=ReadOnly;
      end;
    end;

  end;
end;

procedure TEditProjektForm.ApplyRecordData;
var
  Dummy   : Integer;
  Adress  : Integer;
begin
  if JvInspector.Selected<>nil then
    JvInspector.Selected.DoneEdit;

  Adress:=Integer(MemoryPool);
  for Dummy:=0 to fRecord.ValueCount-1 do
  begin
    if not CheckVisible(Dummy) then
      continue;

    case fRecord.Values[Dummy].ValueType of
      ervtCardinal:
      begin
        fRecord.SetCardinal(fRecord.Values[Dummy].ValueName,PInteger(Adress)^);
        inc(Adress,sizeOf(Cardinal));
      end;
      ervtInteger:
      begin
        fRecord.SetInteger(fRecord.Values[Dummy].ValueName,PInteger(Adress)^);
        inc(Adress,sizeOf(Integer));
      end;
      ervtDouble:
      begin
        fRecord.SetDouble(fRecord.Values[Dummy].ValueName,Pdouble(Adress)^);
        inc(Adress,sizeOf(double));
      end;
      ervtString:
      begin
        fRecord.SetString(fRecord.Values[Dummy].ValueName,PString(Adress)^);
        inc(Adress,sizeOf(String));
      end;
      ervtBoolean:
      begin
        fRecord.SetBoolean(fRecord.Values[Dummy].ValueName,PBoolean(Adress)^);
        inc(Adress,sizeOf(Boolean));
      end;
      ervtRecordList:
      begin
        if fRecord.GetValueProperty(Dummy,'Custom')<>'MissionSkriptTrigger' then
        begin
          fRecord.Values[Dummy].valueList.Assign(TExtRecordList(PObject(Adress)^));
        end;
        inc(Adress,sizeOf(TObject));
      end;
    end;
  end;

  if JvInspector.Selected<>nil then
    JvInspector.Selected.InitEdit;
end;

function TEditProjektForm.CheckRecord: Boolean;
var
  Dummy    : Integer;
  SchussArt: TSchussTypes;
  Floor    : Integer;
  DummySchussType : TSchussType;
  Name            : String;

  procedure AddError(Index: Integer; Error: String);
  begin
    ListBox1.items.AddObject(fRecord.GetValueProperty(Index,'Name')+'|'+Error,fJvItems[Index]);
    result:=false;
  end;

  procedure CheckValue(Dummy: Integer);
  begin
    with fRecord.Values[Dummy] do
    begin
      case ValueType of
        ervtInteger:
        begin
          if ValueInt<minValueInt then
            AddError(Dummy,Format('muss gr��er oder gleich %d sein',[minValueInt]));
          if ValueInt>maxValueInt then
            AddError(Dummy,Format('muss kleiner oder gleich %d sein',[maxValueInt]));
	end;
        ervtCardinal:
        begin
          if ValueCar<minValueCar then
            AddError(Dummy,Format('muss gr��er oder gleich %d sein',[minValueCar]));
          if ValueCar>maxValueCar then
            AddError(Dummy,Format('muss kleiner oder gleich %d sein',[maxValueCar]));
	end;
        ervtDouble:
        begin
          if Valuedoub<minValuedoub then
            AddError(Dummy,Format('muss gr��er oder gleich %.*n sein',[Kommasdoub,minValuedoub]));
          if Valuedoub>maxValuedoub then
            AddError(Dummy,Format('muss kleiner oder gleich %.*n sein',[Kommasdoub,maxValuedoub]));
	end;
      end;
    end;
  end;

  procedure CheckAttribute(Attr: String);
  var
    Dummy: Integer;
  begin
    for Dummy:=0 to fRecord.ValueCount-1 do
    begin
      if fRecord.GetValueProperty(Dummy,Attr)='true' then
        CheckValue(Dummy);
    end;
  end;

begin
  ListBox1.Items.Clear;
  result:=true;
  ApplyRecordData;

  for Dummy:=0 to fRecord.ValueCount-1 do
  begin
    if fRecord.GetValueProperty(Dummy,'NoCheck')='true' then
      continue;

    CheckValue(Dummy);
  end;

  // Pr�fung Verkaufspreis
  if fRecord.ValueExists('VerKaufPreis') then
  begin
    if fRecord.GetInteger('VerKaufPreis')>fRecord.GetInteger('KaufPreis') then
    begin
      AddError(fRecord.GetValueIndex('VerKaufPreis'),'darf nicht gr�sser als der Kaufpreis sein');
    end;
  end;

  // Spezialpr�fungen f�r Einrichtungen
  if fRecord.RecordDefinition=RecordBasis then
  begin
    // Pr�fen Schussfrequenz und Reichweite, wenn Abwehr gr��er als 0
    if fRecord.GetInteger('RoomTyp')=Integer(rtDefense) then
    begin
      CheckAttribute('Abwehr');
    end;
  end;

  // Spezialpr�fungen f�r Waffen
  if fRecord.RecordDefinition=RecordWaffe then
  begin
    if fRecord.GetInteger('WaffType')=Integer(wtLaser) then
      CheckAttribute('Munition');

    if not (TWaffenType(fRecord.GetInteger('WaffType')) in NoMunition) then
      CheckAttribute('Fernkampf')
    else
      CheckAttribute('Strength');

    SchussArt:=[];
    for DummySchussType := low(TSchussType) to high(TSchussType) do
    begin
      Name:='SchussType['+GetEnumName(TypeInfO(TSchussType), Ord(DummySchussType))+']';

      if fRecord.ValueExists(Name) and fRecord.GetBoolean(Name) then
        include(SchussArt,DummySchussType);
    end;


    SchussArt:=CheckSchussType(TProjektType(fRecord.GetInteger('TypeID')),TWaffenType(fRecord.GetInteger('WaffType')),SchussArt);
    if SchussArt=[] then
      AddError(fRecord.GetValueIndex('Gezielt'),'es muss mindestens eine Schussart ausgew�hlt sein');
  end;

  // Spezialpr�fungen f�r Raumschiff-Waffen
  if (fRecord.RecordDefinition=RecordRWaffe) or (fRecord.RecordDefinition=RecordMunition) then
  begin
    if fRecord.GetInteger('WaffType')=Integer(wtLaser) then
      CheckAttribute('Laser');
  end;
end;

procedure TEditProjektForm.BitBtn2Click(Sender: TObject);
begin
  if not CheckRecord then
    MessageBeep(MB_ICONERROR);
end;

procedure TEditProjektForm.ListBox1DblClick(Sender: TObject);
var
  Index: Integer;
  Item : TJvCustomInspectorItem;
begin
  if ListBox1.ItemIndex<>-1 then
    if ListBox1.Items.Objects[ListBox1.ItemIndex]<>nil then
    begin
      Item:=TJvCustomInspectorItem(ListBox1.Items.Objects[ListBox1.ItemIndex]);
      Index:=JvInspector.VisibleIndex(Item);
      if Index=-1 then
      begin
        Item.Parent.Expanded:=true;
        Index:=JvInspector.VisibleIndex(Item);
      end;
      JvInspector.SelectedIndex:=Index;
      Item.InitEdit;
      JvInspector.SetFocus;
    end;
end;

procedure TEditProjektForm.ListBox1DrawItem(Control: TWinControl; Index: Integer;
  Rect: TRect; State: TOwnerDrawState);
begin
  ListBox1.Canvas.FillRect(Rect);
  ListBox1.Canvas.Font.Style:=[fsBold];
  ListBox1.Canvas.TextOut(Rect.Left+2,Rect.Top+2,'['+GetShortHint(ListBox1.Items[Index])+']');
  ListBox1.Canvas.Font.Style:=[];
  ListBox1.Canvas.TextOut(Rect.Left+152,Rect.Top+2,GetLongHint(ListBox1.Items[Index]));
end;

procedure TEditProjektForm.Button2Click(Sender: TObject);
begin
  if CheckRecord then
    ModalResult:=mrOK
  else
    MessageBeep(MB_ICONERROR);
end;

procedure TEditProjektForm.OnEditSymbol(Sender: TObject);
begin
  frmSymbEdit.ImageIndex:=TJvCustomInspectorItem(Sender).Data.AsOrdinal;
  if frmSymbEdit.ShowModal=mrOK then
    TJvCustomInspectorItem(Sender).Data.AsOrdinal:=frmSymbEdit.ImageIndex;
end;

procedure TEditProjektForm.OnEditLand(Sender: TObject);
begin
  // Einstellungen f�r LandEditor
  ListDialog.Caption:='Land w�hlen';
  ListDialog.ListItems.Sorted:=false;
  ListDialog.ListItems.Items.Assign(fLandList);
  ListDialog.ListItems.OnDrawItem:=LandItemDraw;
  ListDialog.ListItems.ItemHeight:=27;

  ListDialog.SelectedIndex:=TJvCustomInspectorItem(Sender).Data.AsOrdinal+1;

  if ListDialog.ShowModal=mrOK then
  begin
    TJvCustomInspectorItem(Sender).Data.AsOrdinal:=ListDialog.SelectedIndex-1;
    TJvCustomInspectorItem(Sender).DoneEdit;
    TJvCustomInspectorItem(Sender).InitEdit;
    ChangeReadOnly('Land',ListDialog.SelectedIndex=-1);
  end;
end;

procedure TEditProjektForm.OnGetLandname(Sender: TObject; var Name: String);
begin
  Name:=fLandList[TJvCustomInspectorItem(Sender).Data.AsOrdinal+1];
end;

procedure TEditProjektForm.LandItemDraw(Control: TWinControl;
  Index: Integer; Rect: TRect; State: TOwnerDrawState);
var
  ComboBox: TListBox;
begin
  ComboBox:=Control as TListBox;
  ComboBox.Canvas.FillRect(Rect);
  ComboBox.Canvas.TextOut(Rect.Left+47,Rect.Top+5,ComboBox.Items[Index]);
  if ComboBox.Items.Objects[Index]<>nil then
    ComboBox.Canvas.Draw(Rect.Left+2,Rect.Top+3,(ComboBox.Items.Objects[Index] as TBitmap));
end;

procedure TEditProjektForm.OnEditWaffenType(Sender: TObject);
var
  Dummy: Integer;
  Typ  : TWaffenType;
begin
  // Einstellungen f�r Waffentyp Editor
  if TJvCustomInspectorItem(Sender).ReadOnly then
    exit;

  ListDialog.Caption:='Waffentyp w�hlen';
  ListDialog.ListItems.Sorted:=false;
  ListDialog.ListItems.Items.Assign(fWaffenTypes);
  ListDialog.ListItems.OnDrawItem:=nil;
  ListDialog.ListItems.ItemHeight:=20;

  if (fRecord.RecordDefinition=RecordRWaffe) or (fRecord.RecordDefinition=RecordBasis) then
  begin
    ListDialog.ListItems.Items.Delete(4); // Nahkampfwaffe l�schen
    ListDialog.ListItems.Items.Delete(3); // Chemische Waffe l�schen
  end;

  ListDialog.SelectedIndex:=TJvCustomInspectorItem(Sender).Data.AsOrdinal;

  if ListDialog.ShowModal=mrOK then
  begin
    TJvCustomInspectorItem(Sender).Data.AsOrdinal:=ListDialog.SelectedIndex;
    TJvCustomInspectorItem(Sender).DoneEdit;
    TJvCustomInspectorItem(Sender).InitEdit;
    Typ:=TWaffenType(ListDialog.SelectedIndex);
    ChangeReadOnly('Strength',not (Typ in [wtLaser,wtShortRange]));
    ChangeReadOnly('Munition',Typ<>wtLaser);
    ChangeReadOnly('Fernkampf',Typ=wtShortRange);
    ChangeReadOnly('Nahkampf',Typ<>wtShortRange);

    if Typ=wtShortRange then
      UnCheckValues('Fernkampf')
    else
      UnCheckValues('Nahkampf');

    SetToZero('Laser');
  end;
end;

procedure TEditProjektForm.OnGetWaffenType(Sender: TObject; var Name: String);
begin
  Name:=fWaffenTypes[TJvCustomInspectorItem(Sender).Data.AsOrdinal];
end;

procedure TEditProjektForm.OnEditWaffe(Sender: TObject);
var
  Dummy   : Integer;
  Rec     : TExtRecord;
  Index   : String;
  MunFor  : Cardinal;
  NeedRec : TExtRecordDefinition;
  Alien   : Boolean;
  Typ     : TWaffenType;
begin
  // Einstellungen f�r Waffentyp Editor
  ListDialog.Caption:='Waffe w�hlen';
  ListDialog.ListItems.Sorted:=true;
  ListDialog.ListItems.OnDrawItem:=WaffeItemDraw;
  ListDialog.ListItems.ItemHeight:=36;

  ListDialog.ListItems.Clear;

  if fRecord.RecordDefinition=RecordMunition then
    NeedRec:=RecordWaffe
  else
    NeedRec:=RecordRWaffe;

  Alien:=fRecord.GetBoolean('AlienItem');

  // Muss angepasst werden um alle Waffen zu ermitteln
  MunFor:=fJvItems[fRecord.GetValueIndex('MunFor')].Data.AsOrdinal;
  for Dummy:=0 to high(MDIForm.Projekts) do
  begin
    Rec:=TExtRecord(MDIForm.Projekts[Dummy]);
    if (Rec.RecordDefinition=NeedRec) and (not (TWaffenType(Rec.GetInteger('WaffType')) in NoMunition)) and (Rec.GetBoolean('AlienItem')=Alien) then
    begin
      ListDialog.ListItems.Items.AddObject(string_utils_GetLanguageString(MDIForm.Sprache,Rec.GetString('Name')),Rec);
      if Rec.GetCardinal('ID')=MunFor then
        Index:=string_utils_GetLanguageString(MDIForm.Sprache,Rec.GetString('Name'));
    end;
  end;

  ListDialog.SelectedIndex:=ListDialog.ListItems.Items.IndexOf(Index);

//  ListDialog.SelectedIndex:=TJvCustomInspectorItem(Sender).Data.AsOrdinal;

  if ListDialog.ShowModal=mrOK then
  begin
    TJvCustomInspectorItem(Sender).Data.AsOrdinal:=TExtRecord(ListDialog.ListItems.Items.Objects[ListDialog.SelectedIndex]).GetCardinal('ID');
    fJvitems[fRecord.GetValueIndex('WaffType')].Data.AsOrdinal:=TExtRecord(ListDialog.ListItems.Items.Objects[ListDialog.SelectedIndex]).GetInteger('WaffType');

    if fRecord.RecordDefinition=RecordRMunition then
    begin
      fRecord.SetInteger('Strength',TExtRecord(ListDialog.ListItems.Items.Objects[ListDialog.SelectedIndex]).GetInteger('Strength'));
      fJvitems[fRecord.GetValueIndex('Strength')].Data.AsOrdinal:=fRecord.GetInteger('Strength');
    end;

    TJvInspectorCustomItem(Sender).Display:=ListDialog.ListItems.Items[ListDialog.SelectedIndex];
    TJvCustomInspectorItem(Sender).DoneEdit;
    TJvCustomInspectorItem(Sender).InitEdit;

    Typ:=TWaffenType(TExtRecord(ListDialog.ListItems.Items.Objects[ListDialog.SelectedIndex]).GetInteger('WaffType'));
    ChangeReadOnly('Laser',Typ=wtLaser);
    ChangeReadOnly('Raketen',Typ<>wtRaketen);


  end;
end;

procedure TEditProjektForm.WaffeItemDraw(Control: TWinControl; Index: Integer;
  Rect: TRect; State: TOwnerDrawState);
var
  ComboBox: TListBox;
begin
  ComboBox:=Control as TListBox;
  ComboBox.Canvas.FillRect(Rect);
  ComboBox.Canvas.TextOut(Rect.Left+40,Rect.Top+5,ComboBox.Items[Index]);
  if ComboBox.Items.Objects[Index]<>nil then
    MDIForm.WaffenListe.Draw(ComboBox.Canvas,Rect.Left+2,Rect.Top+3,TExtRecord(ComboBox.Items.Objects[Index]).GetInteger('ImageIndex'));
//    ComboBox.Canvas.Draw(Rect.Left+2,Rect.Top+3,(ComboBox.Items.Objects[Index] as TBitmap));
end;

procedure TEditProjektForm.OnEditLanguageText(Sender: TObject);
begin
  NameEdit.Text:=TJvCustomInspectorItem(Sender).Data.AsString;
  if NameEdit.ShowModal=mrOK then
  begin
    TJvCustomInspectorItem(Sender).Data.AsString:=NameEdit.Text;
    TJvCustomInspectorItem(Sender).DoneEdit;
    TJvCustomInspectorItem(Sender).InitEdit;
  end;
end;

procedure TEditProjektForm.OnGetLanguageText(Sender: TObject;
  var Name: String);
begin
  Name:=string_utils_GetLanguageString(MDIForm.Sprache,TJvCustomInspectorItem(Sender).Data.AsString);
end;

procedure TEditProjektForm.PatentButtonClick(Sender: TObject);
begin
  ShowPatentForm;
end;

procedure TEditProjektForm.FormHide(Sender: TObject);
begin
  frmPatents.Hide;
end;

procedure TEditProjektForm.JvInspectorDataValueChanged(Sender: TObject;
  const Data: TJvCustomInspectorData);
begin
  if frmPatents.Visible then
    ShowPatentForm;
end;

procedure TEditProjektForm.ShowPatentForm;
begin
  frmPatents.Weeks:=fJvitems[fRecord.GetValueIndex('PatenZeit')].Data.AsOrdinal;
  frmPatents.Satt:=fJvitems[fRecord.GetValueIndex('PatenSatt')].Data.AsOrdinal;
  frmPatents.Erloes:=fJvitems[fRecord.GetValueIndex('Patengebuehr')].Data.AsOrdinal;
  frmPatents.Wachstum:=fJvitems[fRecord.GetValueIndex('Patenanstieg')].Data.AsOrdinal;
  frmPatents.RecalcPatent;
  frmPatents.Show;
end;

procedure TEditProjektForm.OnEditAliens(Sender: TObject);
var
  Data: TExtRecordList;
begin
  Data:=TExtRecordList(TJvCustomInspectorItem(Sender).Data.AsOrdinal);
  AliensInUFO.Aliens:=Data;
  if AliensInUFO.ShowModal=mrOK then
  begin
    TExtRecordList(TJvCustomInspectorItem(Sender).Data.AsOrdinal).Assign(AliensInUFO.Aliens);
    TJvCustomInspectorItem(Sender).DoneEdit;
    TJvCustomInspectorItem(Sender).InitEdit;
  end;
end;

procedure TEditProjektForm.OnGetAliens(Sender: TObject; var Name: String);
var
  Data  : TExtRecordList;
  Dummy : Integer;

  function GetAlienName(ID: Cardinal): String;
  var
    Dummy: Integer;
  begin
    for Dummy:=0 to high(MDIForm.Aliens) do
    begin
      if MDIForm.Aliens[Dummy].GetCardinal('ID')=ID then
      begin
        result:=string_utils_GetLanguageString(MDIFOrm.Sprache,MDIForm.Aliens[Dummy].GetString('Name'));
        exit;
      end;
    end;
    result:='';
  end;

begin
  Data:=TExtRecordList(TJvCustomInspectorItem(Sender).Data.AsOrdinal);
  for Dummy:=0 to Data.Count-1 do
  begin
    if Name<>'' then
      Name:=Name+',';

    Name:=Name+Format('%s [%d %%]',[GetAlienName(Data[Dummy].GetCardinal('ID')),Data[Dummy].GetInteger('Chance')]);
  end;
  Name:='('+Name+')';
end;

procedure TEditProjektForm.OnEditIQ(Sender: TObject);
begin
  IntelligenzForm.Alien:=fRecord.RecordDefinition=RecordAlien;
  IntelligenzForm.IQ:=TJvCustomInspectorItem(Sender).Data.AsOrdinal;

  if IntelligenzForm.ShowModal=mrOK then
  begin
    TJvCustomInspectorItem(Sender).Data.AsOrdinal:=IntelligenzForm.IQ;
    TJvCustomInspectorItem(Sender).DoneEdit;
    TJvCustomInspectorItem(Sender).InitEdit;
  end;
end;

procedure TEditProjektForm.NewProjekt;
begin
  Caption:=fRecord.RecordDefinition.RecordName+' - Neu';
end;

procedure TEditProjektForm.GetWaffenNamen(Item: TJvInspectorCustomItem);
var
  Dummy: Integer;
  ID   : Cardinal;
begin
  ID:=fRecord.GetCardinal('MunFor');
  for DUmmy:=0 to high(MDIForm.Projekts) do
  begin
    if ID=MDIForm.Projekts[Dummy].GetCardinal('ID') then
    begin
      Item.Display:=string_utils_GetLanguageString(MDIForm.Sprache,MDIForm.Projekts[Dummy].GetString('Name'));
      exit;
    end;
  end;
end;

procedure TEditProjektForm.OnEditPaedieImage(Sender: TObject);
begin
  PaedieImage.ImageBitmap:=TJvCustomInspectorItem(Sender).Data.AsString;
  PaedieImage.ImageKategorie:='UFOPaedie';
  PaedieImage.AllowedNoImage:=true;
  PaedieImage.CheckBitmap:=CheckBitmapUFOPaedie;
  if PaedieImage.ShowModal=mrOk then
    TJvCustomInspectorItem(Sender).Data.AsString:=PaedieImage.ImageBitmap;
end;

procedure TEditProjektForm.OnEditSound(Sender: TObject);
begin
  SoundEdit.SoundName:=TJvCustomInspectorItem(Sender).Data.AsString;
  if SoundEdit.ShowModal=mrOk then
    TJvCustomInspectorItem(Sender).Data.AsString:=SoundEdit.SoundName;
end;

procedure TEditProjektForm.OnEditMissionTriggers(Sender: TObject);
begin
  frmMissionSkriptTriggers.Rec:=fRecord;
  if frmMissionSkriptTriggers.ShowModal=mrOK then
  begin
    fRecord.Assign(frmMissionSkriptTriggers.Rec);
  end;
end;

procedure TEditProjektForm.ClearListen;
var
  Dummy: Integer;
begin
  for Dummy:=0 to fListen.Count-1 do
  begin
    TObject(fListen[Dummy]).Free;
  end;
  fListen.Clear;
end;

procedure TEditProjektForm.UnCheckValues(Value: String);
var
  Dummy: Integer;
begin
  for Dummy:=0 to fRecord.ValueCount-1 do
  begin
    if fJvItems[Dummy]<>nil then
    begin
      if fRecord.GetValueProperty(Dummy,Value)='true' then
      begin
        if fJvItems[Dummy] is TJvInspectorBooleanItem then
          fJvItems[Dummy].Data.AsOrdinal:=0;
      end;
    end;

  end;
end;

procedure TEditProjektForm.OnEditInfoText(Sender: TObject);
begin
  InfoEdit.Text := TJvCustomInspectorItem(Sender).Data.AsString;
  InfoEdit.Caption:=Format('Beschreibung (%s) von %s',[TJvCustomInspectorItem(Sender).DisplayName,string_utils_GetLanguageString(MDIForm.Sprache,fRecord.GetString('Name'))]);
  if InfoEdit.ShowModal = mrOK then
  begin
    TJvCustomInspectorItem(Sender).Data.AsString:=InfoEdit.Text;
    TJvCustomInspectorItem(Sender).DoneEdit;
    TJvCustomInspectorItem(Sender).InitEdit;
  end;
end;


procedure TEditProjektForm.OnEditMissionSkript(Sender: TObject);
begin
  frmSkriptEditor.Skript:=TJvCustomInspectorItem(Sender).Data.AsString;;
  if frmSkriptEditor.ShowModal=mrOk then
  begin
    TJvCustomInspectorItem(Sender).Data.AsString:=frmSkriptEditor.Skript;
    TJvCustomInspectorItem(Sender).DoneEdit;
    TJvCustomInspectorItem(Sender).InitEdit;
  end;
end;

procedure TEditProjektForm.OnEditBasisBauImage(Sender: TObject);
var
  UWidth, UHeight: Integer;
begin
  PaedieImage.ImageBitmap:=TJvCustomInspectorItem(Sender).Data.AsString;
  PaedieImage.ImageKategorie:='BasisBau';
  PaedieImage.AllowedNoImage:=false;
  PaedieImage.CheckBitmap:=CheckBitmapBasisBau;

  if PaedieImage.ShowModal=mrOk then
  begin
    UWidth:=PaedieImage.Image.Width div 64;
    UHeight:=PaedieImage.Image.Height div 64;

    Assert(fRecord.ValueExists('RoomTyp'));
    if TRoomTyp(fRecord.GetInteger('RoomTyp'))=rtHangar then
    begin
      if TJvCustomInspectorItem(Sender).Name='Grafik Obergeschoss' then
      begin
        if (PaedieImage.Image.Width<>64) or (PaedieImage.Image.Height<>64) then
        begin
          MessageDlg('Die Grafik f�r das Obergeschoss eines Hangars muss 64x64 sein',mtError,[mbOk],0);
          exit;
        end;

        UWidth:=-1;
      end
      else if TJvCustomInspectorItem(Sender).Name='Grafik Untergeschoss' then
      
      else
        Assert(false);
    end;

    TJvCustomInspectorItem(Sender).Data.AsString:=PaedieImage.ImageBitmap;
    if UWidth<>-1 then
    begin
      fRecord.SetInteger('UnitWidth',UWidth);
      fRecord.SetInteger('UnitHeight',UHeight);
    end;

    TJvCustomInspectorItem(Sender).DoneEdit;
    TJvCustomInspectorItem(Sender).InitEdit;
  end;
end;

procedure TEditProjektForm.CheckBitmapBasisBau(Bitmap: TBitmap);
begin
  if ((Bitmap.Width mod 64)<>0) or (Bitmap.Width=0) or (Bitmap.Width>256) or
     ((Bitmap.Height mod 64)<>0) or (Bitmap.Height=0) or (Bitmap.Height>256) then
  begin
    raise Exception.Create('Die Weite/H�he des Bildes muss durch 64 teilbar sein und darf maximal 256 betragen.');
  end;


end;

procedure TEditProjektForm.CheckBitmapUFOPaedie(Bitmap: TBitmap);
begin
  if (Bitmap.Width<>150) or (Bitmap.Height<>150) then
    raise Exception.Create('Das Bild muss eine Gr��e von 150x150 Pixeln haben.');
end;

procedure TEditProjektForm.OnEditRoomTyp(Sender: TObject);
var
  RoomTyp  : Integer;
  Dummy    : Integer;
begin
  ListDialog.Caption:='Bauebene w�hlen';
  ListDialog.ListItems.Sorted:=false;
  ListDialog.ListItems.Items.Assign(fRoomTyps);
  ListDialog.ListItems.OnDrawItem:=nil;
  ListDialog.ListItems.ItemHeight:=20;

  ListDialog.SelectedIndex:=TJvCustomInspectorItem(Sender).Data.AsOrdinal;

  if ListDialog.ShowModal=mrOK then
  begin
    TJvCustomInspectorItem(Sender).Data.AsOrdinal:=ListDialog.SelectedIndex;
    TJvCustomInspectorItem(Sender).DoneEdit;
    TJvCustomInspectorItem(Sender).InitEdit;

    RoomTyp:=ListDialog.SelectedIndex;

    for Dummy:=0 to fRoomTyps.Count-1 do
    begin
      ChangeReadOnly('RoomTyp'+IntToStr(Dummy),true);
      if Dummy<>RoomTyp then
        SetToZero('RoomTyp'+IntToStr(Dummy));
    end;

    ChangeReadOnly('RoomTyp'+IntToStr(RoomTyp),false);

    ChangeReadOnly('Basement',not (TRoomTyp(RoomTyp) in [rtDefense,rtSensor,rtDefenseShield,rtLivingRoom]));
    ChangeReadOnly('Underground',not (TRoomTyp(RoomTyp) in [rtHangar,rtBaseRooms,rtLivingRoom]));

  end;
end;

procedure TEditProjektForm.OnGetRoomTyp(Sender: TObject;
  var Name: String);
begin
  Name:=fRoomTyps[TJvCustomInspectorItem(Sender).Data.AsOrdinal];
end;

procedure TEditProjektForm.SetToZero(Value: String);
var
  Dummy: Integer;
begin
  for Dummy:=0 to fRecord.ValueCount-1 do
  begin
    if fJvItems[Dummy]<>nil then
    begin
      if fRecord.GetValueProperty(Dummy,Value)='true' then
      begin
        fJvItems[Dummy].Data.AsOrdinal:=0;
      end;
    end;
  end;
end;

procedure TEditProjektForm.OnGetImageName(Sender: TObject;
  var Name: String);
begin
  if TJvCustomInspectorItem(Sender).Data.AsString='' then
    Name:='( Bild )'
  else
    Name:='( Bild: '+TJvCustomInspectorItem(Sender).Data.AsString+' )';
end;

procedure TEditProjektForm.OnGetIQ(Sender: TObject; var Name: String);
begin
  Name:=IntToStr(TJvCustomInspectorItem(Sender).Data.AsOrdinal);
end;

{ TJvInspectorCustomItem }

procedure TJvInspectorCustomItem.Edit;
begin
  if ReadOnly then
    exit;
  if Assigned(fOnEdit) then
    fOnEdit(Self);
end;

function TJvInspectorCustomItem.GetDisplayValue: string;
begin
  if not Assigned(fGetDataName) then
  begin
    if fDisplaytext=EmptyStr then
      result:=inherited GetDisplayValue
    else
      result:=fDisplayText;
  end
  else
    fGetDataName(Self,result);
end;

class procedure TJvInspectorCustomItem.RegisterAsDefaultItem;
begin
  with TJvCustomInspectorData.ItemRegister do
  begin
    if IndexOf(Self) = -1 then
    begin
      Add(TJvInspectorTypeInfoRegItem.Create(Self, TypeInfo(TCustomIndex)));
      Add(TJvInspectorTypeInfoRegItem.Create(Self, TypeInfo(TCustomID)));
      Add(TJvInspectorTypeInfoRegItem.Create(Self, TypeInfo(TCustomString)));
      Add(TJvInspectorTypeInfoRegItem.Create(Self, TypeInfo(TCustomPointer)));
    end;
  end;
end;

procedure TJvInspectorCustomItem.SetFlags(const Value: TInspectorItemFlags);
var
  NewValue: TInspectorItemFlags;
begin
  NewValue := Value + [iifEditButton, iifEditFixed];
  inherited SetFlags(NewValue);
end;

{ TJvInspectorDivisorItem }

class procedure TJvInspectorDivisorItem.RegisterAsDefaultItem;
begin
  with TJvCustomInspectorData.ItemRegister do
  begin
    if IndexOf(Self) = -1 then
    begin
      Add(TJvInspectorTypeInfoRegItem.Create(Self, TypeInfo(TDivisorInteger)));
    end;
  end;
end;

procedure TJvInspectorDivisorItem.SetDisplayValue(const Value: string);
var
  Val: Integer;
begin
  Val:=StrToInt64(Value);
  if (Val mod fDivisor)<>0 then
    raise EConvertError.CreateFmt('Wert muss durch %d teilbar sein',[fDivisor]);
  Data.AsOrdinal:=Val;
end;

procedure TEditProjektForm.JvInspectorBorlandPainterSetItemColors(
  Item: TJvCustomInspectorItem; Canvas: TCanvas);
begin
  if Item.ReadOnly then
    Canvas.Font.Color:=clBtnShadow;
end;

initialization
  TJvInspectorCustomItem.RegisterAsDefaultItem;
  TJvInspectorDivisorItem.RegisterAsDefaultItem;

end.
