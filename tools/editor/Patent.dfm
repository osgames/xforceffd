object frmPatents: TfrmPatents
  Left = 3
  Top = 148
  Width = 239
  Height = 474
  BorderStyle = bsSizeToolWin
  Caption = 'Patenterl'#246'se'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  FormStyle = fsStayOnTop
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object PatentList: TListView
    Left = 0
    Top = 0
    Width = 231
    Height = 445
    Align = alClient
    Columns = <
      item
        Caption = 'Woche'
      end
      item
        Alignment = taRightJustify
        Caption = 'Anstieg'
        Width = 80
      end
      item
        Alignment = taRightJustify
        Caption = 'Patenterl'#246's'
        Width = 75
      end>
    ReadOnly = True
    RowSelect = True
    TabOrder = 0
    ViewStyle = vsReport
  end
end
