object frmMissionSkriptTriggers: TfrmMissionSkriptTriggers
  Left = 426
  Top = 425
  BorderStyle = bsDialog
  Caption = 'Ausl'#246'sebedingungen'
  ClientHeight = 540
  ClientWidth = 644
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poMainFormCenter
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  DesignSize = (
    644
    540)
  PixelsPerInch = 96
  TextHeight = 13
  object BitBtn2: TBitBtn
    Left = 548
    Top = 508
    Width = 89
    Height = 25
    Anchors = [akRight, akBottom]
    TabOrder = 0
    Kind = bkCancel
  end
  object randomPanel: TPanel
    Left = 8
    Top = 32
    Width = 628
    Height = 73
    Anchors = [akLeft, akTop, akRight]
    BevelOuter = bvLowered
    TabOrder = 1
    object ActiveBox: TCheckBox
      Left = 8
      Top = 32
      Width = 89
      Height = 17
      Caption = 'Aktiviert'
      TabOrder = 0
      OnClick = ActiveBoxClick
    end
  end
  object SinglePanel: TPanel
    Left = 8
    Top = 112
    Width = 629
    Height = 390
    Anchors = [akLeft, akTop, akRight, akBottom]
    BevelOuter = bvLowered
    TabOrder = 2
    DesignSize = (
      629
      390)
    object NewTrigger: TButton
      Left = 8
      Top = 357
      Width = 121
      Height = 25
      Anchors = [akLeft, akBottom]
      Caption = 'neue Bedigung'
      TabOrder = 0
      OnClick = NewTriggerClick
    end
    object DeleteTrigger: TButton
      Left = 136
      Top = 357
      Width = 121
      Height = 25
      Anchors = [akLeft, akBottom]
      Caption = 'Bedingung l'#246'schen'
      TabOrder = 1
      OnClick = DeleteTriggerClick
    end
    object TriggerListView: TListView
      Left = 8
      Top = 32
      Width = 610
      Height = 138
      Anchors = [akLeft, akTop, akRight]
      Columns = <
        item
          Caption = 'Bedingungstyp'
          Width = 300
        end
        item
          Caption = 'Paramter'
          Width = 150
        end>
      ColumnClick = False
      HideSelection = False
      ReadOnly = True
      RowSelect = True
      TabOrder = 2
      ViewStyle = vsReport
      OnSelectItem = TriggerListViewSelectItem
    end
    object TriggerPanel: TPanel
      Left = 8
      Top = 176
      Width = 609
      Height = 177
      BevelOuter = bvNone
      TabOrder = 3
      Visible = False
      object TriggerParamsPanel: TPanel
        Left = 0
        Top = 40
        Width = 609
        Height = 129
        BorderWidth = 1
        TabOrder = 0
        object Panel1: TPanel
          Tag = 3
          Left = 2
          Top = 2
          Width = 479
          Height = 31
          BevelOuter = bvNone
          TabOrder = 0
          object Label2: TLabel
            Left = 8
            Top = 8
            Width = 90
            Height = 13
            Caption = 'Forschungsprojekt:'
          end
          object ProjektEdit0: TEdit
            Left = 104
            Top = 4
            Width = 233
            Height = 21
            ReadOnly = True
            TabOrder = 0
          end
          object Button1: TButton
            Left = 342
            Top = 4
            Width = 22
            Height = 21
            Caption = '...'
            TabOrder = 1
            OnClick = Button1Click
          end
        end
        object Panel3: TPanel
          Tag = 2
          Left = 10
          Top = 10
          Width = 479
          Height = 31
          BevelOuter = bvNone
          TabOrder = 1
          object Label3: TLabel
            Left = 8
            Top = 8
            Width = 34
            Height = 13
            Caption = 'Datum:'
          end
          object Label4: TLabel
            Left = 204
            Top = 8
            Width = 21
            Height = 13
            Caption = 'Zeit:'
          end
          object DatePicker: TDateTimePicker
            Left = 58
            Top = 4
            Width = 111
            Height = 21
            Date = 51872.854349594900000000
            Time = 51872.854349594900000000
            TabOrder = 0
            OnChange = DatePickerChange
          end
          object TimePicker: TDateTimePicker
            Left = 242
            Top = 4
            Width = 79
            Height = 21
            Date = 51872.854349594900000000
            Time = 51872.854349594900000000
            Kind = dtkTime
            TabOrder = 1
            OnChange = TimePickerChange
          end
        end
        object Panel4: TPanel
          Tag = 4
          Left = 18
          Top = 26
          Width = 599
          Height = 31
          BevelOuter = bvNone
          TabOrder = 2
          object Label5: TLabel
            Left = 8
            Top = 8
            Width = 42
            Height = 13
            Caption = 'Schalter:'
          end
          object Label6: TLabel
            Left = 400
            Top = 8
            Width = 189
            Height = 13
            Caption = '(Parameter f'#252'r mission_api_ClearTrigger)'
          end
          object SwitchEdit: TEdit
            Left = 56
            Top = 4
            Width = 313
            Height = 21
            MaxLength = 30
            TabOrder = 0
            OnChange = SwitchEditChange
          end
        end
      end
      object Panel2: TPanel
        Left = 0
        Top = 0
        Width = 609
        Height = 33
        TabOrder = 1
        object Label1: TLabel
          Left = 10
          Top = 8
          Width = 73
          Height = 13
          Caption = 'Bedingungstyp:'
        end
        object TypBox: TComboBox
          Left = 136
          Top = 5
          Width = 337
          Height = 21
          Style = csDropDownList
          ItemHeight = 13
          TabOrder = 0
          OnChange = TypBoxChange
          Items.Strings = (
            'keine Bedigung'
            'Spielstart'
            'Datum'
            'Forschung beendet'
            'Benutzerdefiniert ('#252'ber mission_api_ClearTrigger)')
        end
        object TriggerNegationBox: TCheckBox
          Left = 488
          Top = 7
          Width = 113
          Height = 17
          Caption = 'darf nicht erf'#252'llt sein'
          TabOrder = 1
          OnClick = TriggerNegationBoxClick
        end
      end
    end
  end
  object RandomTrigger: TRadioButton
    Left = 16
    Top = 40
    Width = 613
    Height = 17
    Anchors = [akLeft, akTop, akRight]
    Caption = 'zuf'#228'llig'
    TabOrder = 3
    OnClick = ChangeTriggerMode
  end
  object singleTrigger: TRadioButton
    Left = 16
    Top = 120
    Width = 613
    Height = 17
    Anchors = [akLeft, akTop, akRight]
    Caption = 'einmalig'
    TabOrder = 4
    OnClick = ChangeTriggerMode
  end
  object ManuellTrigger: TRadioButton
    Left = 16
    Top = 8
    Width = 613
    Height = 17
    Anchors = [akLeft, akTop, akRight]
    Caption = 'manuell per mission_api_StartSkript'
    TabOrder = 5
    OnClick = ChangeTriggerMode
  end
  object BitBtn1: TBitBtn
    Left = 452
    Top = 508
    Width = 89
    Height = 25
    Anchors = [akRight, akBottom]
    TabOrder = 6
    OnClick = BitBtn1Click
    Kind = bkOK
  end
end
