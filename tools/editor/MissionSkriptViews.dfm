object MissionSkripts: TMissionSkripts
  Left = 0
  Top = 0
  Width = 443
  Height = 275
  Align = alClient
  Color = clWindow
  ParentColor = False
  TabOrder = 0
  object Panel2: TPanel
    Left = 0
    Top = 0
    Width = 443
    Height = 22
    Align = alTop
    Alignment = taLeftJustify
    BevelOuter = bvNone
    TabOrder = 1
    DesignSize = (
      443
      22)
    object Image1: TImage
      Left = 2
      Top = 2
      Width = 16
      Height = 16
      Picture.Data = {
        07544269746D617036030000424D360300000000000036000000280000001000
        0000100000000100180000000000000300000000000000000000000000000000
        0000F7F7F7F7F7F7F7F7F7F7F7F7EAEAEA848484878787C0C0C0BDBDBD818181
        6968676968689B9B9BEBEBEBF7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7B9B9
        B9B2B2B2E8E8E85453532A25223B2F2A4738324F4541A2A2A26E6C6AD9D9D9F7
        F7F7F7F7F7F7F7F7F7F7F7F4F4F4828282F2F2F298979729221F3E312B57443B
        7D6356B3ACA8EBEBEBD9D6D5696462EEEEEEF7F7F7F7F7F7F7F7F7D1D1D19999
        99E3E3E32F2C2A352A254F3E36997968C0B5B0FEFEFEFFFFFFFFFFFF8A7B74B3
        B2B2F7F7F7F7F7F7F7F7F7949494E0E0E0BDBDBD1F1917362A25695F5AC2B2A9
        B8AAA2FFFFFFF9F7F5F0E2DBB0988C868281F7F7F7F7F7F7E3E3E3858585FCFC
        FCA3A3A31C1714686563ABA09ABD9C8ACBA38EDDBDACD8A58BE9B194BD917B84
        7E7CF7F7F7F7F7F7AEAEAEC0C0C0EAEAEAC7C7C72B2928959595B4AEABD4CDC9
        D2A288E1AB8EE6AF92E8B0939E7B68A3A09FF7F7F7F0F0F07F7F7FFAFAFADDDD
        DDDDDDDD6868688C8C8CC4C4C4EDEDEDE5CEC2D8A88ED1A48BB98D78695851EC
        ECECF7F7F7C6C6C6A4A4A4FDFCFDF6F6F6D2D2D29D9D9D6E6E6EB1B1B1DFDFDF
        FBFBFBE7DFDAC5BEBB5F4D45C3C2C1F7F7F7F6F6F68A8A8AECECECFDFDFDFDFD
        FDFDFDFDFCFCFCD3D3D3A3A3A3B8B5B4ABA7A5A6A19F6E6865DAD9D9F7F7F7F7
        F7F7DADADA8D8D8DFCFCFCFDFDFDFDFDFDFDFDFDFCFCFCFDFDFDFCFCFCE9E9E9
        DDDDDDF7F7F7B9B9B9F7F7F7F7F7F7F7F7F7A5A5A5D0D0D0FDFDFDFDFDFDFDFD
        FDFCFCFCD6D6D6CDCDCDE8E8E8FCFDFDFDFDFDC3C3C3EAEAEAF7F7F7F7F7F7F7
        F7F7C0C0C0B4B4B4E7E7E7FCFCFCFDFDFDF2F2F2CCCCCCD2D2D2DADADAE6E6E6
        FCFCFCAEAEAEF7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F1F1F1D1D1D1ACACACB8B8
        B8ECEBECFCFCFCF7F7F7D5D5D5F7F7F7D7D7D7DCDCDCF7F7F7F7F7F7F7F7F7F7
        F7F7F7F7F7F7F7F7F7F7F7F7F7F7F0F0F0CECECEACACACBBBBBBEFEFEFFCFCFC
        ACACACF7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7
        F7F7F7F7F7F7F7EEEEEECACACAB0B0B0D6D6D6F7F7F7F7F7F7F7F7F7F7F7F7F7
        F7F7}
      Transparent = True
    end
    object Label1: TLabel
      Left = 19
      Top = 4
      Width = 91
      Height = 13
      Caption = 'Missionsskripte:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object FilterBox: TEdit
      Left = 272
      Top = 0
      Width = 170
      Height = 21
      Hint = 'Filter'
      Anchors = [akTop, akRight]
      TabOrder = 0
      OnChange = FilterBoxChange
    end
  end
  object MissionsSkriptsList: TListView
    Left = 0
    Top = 22
    Width = 443
    Height = 253
    Hint = 
      'Zeigt alle Gegenst'#228'nde, Einrichtungen und Raumschiffe zu Beginn ' +
      'des Spiels'
    Align = alClient
    Columns = <
      item
        Caption = 'Name'
        Width = 190
      end
      item
        Caption = 'Typ'
        Width = 100
      end>
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    HideSelection = False
    RowSelect = True
    ParentFont = False
    PopupMenu = PopupMenu1
    SmallImages = MDIForm.ImageList1
    SortType = stData
    TabOrder = 0
    ViewStyle = vsReport
    OnColumnClick = MissionsSkriptsListColumnClick
    OnCompare = MissionsSkriptsListCompare
    OnDblClick = MissionsSkriptsListDblClick
    OnEdited = MissionsSkriptsListEdited
  end
  object PopupMenu1: TPopupMenu
    Images = MDIForm.ImageList1
    Left = 72
    Top = 63
    object ActionSkriptsEdit1: TMenuItem
      Action = MDIForm.ActionSkriptEdit
    end
    object Umbenennen1: TMenuItem
      Action = MDIForm.ActionSkriptRename
    end
    object N2: TMenuItem
      Caption = '-'
    end
    object Skripthinzufgen1: TMenuItem
      Action = MDIForm.ActionSkriptAdd
    end
    object Skriptlschen1: TMenuItem
      Action = MDIForm.ActionSkriptDelete
    end
    object N1: TMenuItem
      Caption = '-'
    end
    object Kopieren1: TMenuItem
      Action = MDIForm.ActionSkriptCut
    end
    object Kopieren2: TMenuItem
      Action = MDIForm.ActionSkriptCopy
    end
    object Ausschneiden1: TMenuItem
      Action = MDIForm.ActionSkriptPaste
    end
  end
end
