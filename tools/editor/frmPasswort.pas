unit frmPasswort;

interface

uses Windows, SysUtils, Classes, Graphics, Forms, Controls, StdCtrls, 
  Buttons, ExtCtrls, ExtendEdit;

type
  TPasswordDlg = class(TForm)
    Password: TExtendEdit;
    OKButton: TBitBtn;
    CancelButton: TBitBtn;
    procedure FormShow(Sender: TObject);
    procedure OKButtonClick(Sender: TObject);
    procedure CancelButtonClick(Sender: TObject);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  PasswordDlg: TPasswordDlg;

implementation

{$R *.DFM}

procedure TPasswordDlg.FormShow(Sender: TObject);
begin
  Password.SetFocus;
end;

procedure TPasswordDlg.OKButtonClick(Sender: TObject);
begin
  ModalResult:=mrOK;
end;

procedure TPasswordDlg.CancelButtonClick(Sender: TObject);
begin
  ModalResult:=mrCancel;
end;

procedure TPasswordDlg.FormKeyPress(Sender: TObject; var Key: Char);
begin
  if Key=#13 then OKButton.Click
  else if Key=#27 then CancelButton.Click;
end;

end.
 
