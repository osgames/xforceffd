unit MissionTriggers;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Buttons, ExtRecord, ExtCtrls, ComCtrls;

type
  TfrmMissionSkriptTriggers = class(TForm)
    BitBtn2: TBitBtn;
    randomPanel: TPanel;
    SinglePanel: TPanel;
    NewTrigger: TButton;
    DeleteTrigger: TButton;
    RandomTrigger: TRadioButton;
    singleTrigger: TRadioButton;
    TriggerListView: TListView;
    TriggerPanel: TPanel;
    ManuellTrigger: TRadioButton;
    TriggerParamsPanel: TPanel;
    Panel1: TPanel;
    Label2: TLabel;
    Panel2: TPanel;
    TypBox: TComboBox;
    Label1: TLabel;
    ProjektEdit0: TEdit;
    Button1: TButton;
    ActiveBox: TCheckBox;
    Panel3: TPanel;
    Label3: TLabel;
    Label4: TLabel;
    DatePicker: TDateTimePicker;
    TimePicker: TDateTimePicker;
    TriggerNegationBox: TCheckBox;
    Panel4: TPanel;
    Label5: TLabel;
    BitBtn1: TBitBtn;
    Label6: TLabel;
    SwitchEdit: TEdit;
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure ChangeTriggerMode(Sender: TObject);
    procedure NewTriggerClick(Sender: TObject);
    procedure TriggerListViewSelectItem(Sender: TObject; Item: TListItem;
      Selected: Boolean);
    procedure TypBoxChange(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure DeleteTriggerClick(Sender: TObject);
    procedure ActiveBoxClick(Sender: TObject);
    procedure DatePickerChange(Sender: TObject);
    procedure TimePickerChange(Sender: TObject);
    procedure TriggerNegationBoxClick(Sender: TObject);
    procedure SwitchEditChange(Sender: TObject);
  private
    fPrivateRecord: TExtRecord;
    procedure SetRecord(const Value: TExtRecord);

    procedure ChangeEnablePanel(Panel: TPanel; Enabled: Boolean);
    procedure RecordToListItem(Item: TListItem; Rec: TExtRecord);

    function GetSelectedTrigger: TExtRecord;

    procedure RefreshTriggerParams;

    function GetObjektName(ID: Cardinal; Arr: TRecordArray;Sec: TRecordArray = nil; Zusatz: String = ''): String;
    { Private-Deklarationen }
  public
    { Public-Deklarationen }
    property Rec: TExtRecord read fPrivateRecord write SetRecord;
  end;

var
  frmMissionSkriptTriggers: TfrmMissionSkriptTriggers;

implementation

uses
  ObjektRecords, frmMain, string_utils, record_utils, SelectProjekt, Defines;

{$R *.dfm}

// Diese Konstanten werden verwendet um den Index zu einem Triggertyp zuzuordnen

const
  TRIGGER_NONE               = 0;          // kein Typ ausgew�hlt
  TRIGGER_STARTGAME          = 1;          // Spiel startet
  TRIGGER_DATEREACHED        = 2;          // wenn ein bestimmter Zeitpunkt erreicht ist
  TRIGGER_FORSCHPROJECTDONE  = 3;          // Forschungsprojekt erledigt
  TRIGGER_USERDEFINED        = 4;          // benutzerdefiniert �ber mission_api_ClearTrigger

{ TfrmMissionSkriptTriggers }

procedure TfrmMissionSkriptTriggers.SetRecord(const Value: TExtRecord);
var
  Dummy: Integer;
  Typ  : Integer;
begin
  fPrivateRecord.Assign(Value);

  Typ:=fPrivateRecord.GetInteger('Typ');
  ManuellTrigger.Checked:=Typ=0;
  RandomTrigger.Checked:=Typ=1;
  singleTrigger.Checked:=Typ=2;

  TriggerListView.Items.BeginUpdate;
  TriggerListView.Items.Clear;
  with fPrivateRecord.GetRecordList('Triggers') do
  begin
    for Dummy:=0 to Count-1 do
    begin
      RecordToListItem(TriggerListView.Items.Add,Item[Dummy]);
    end;
  end;
  TriggerListView.Items.EndUpdate;

  ActiveBox.Checked:=fPrivateRecord.GetBoolean('Active');
  ChangeTriggerMode(Self);
end;

procedure TfrmMissionSkriptTriggers.FormCreate(Sender: TObject);
var
  Dummy: Integer;
begin
  fPrivateRecord:=TExtRecord.Create(RecordMissionSkript);
  for Dummy:=0 to ComponentCount-1 do
  begin
    if Components[Dummy] is TPanel then
    begin
      if TPanel(Components[Dummy]).Parent=TriggerParamsPanel then
      begin
        TPanel(Components[Dummy]).Align:=alClient;
      end;
    end;
  end;
  DatePicker.MinDate:=EncodeDate(StartDate.Year,StartDate.Month,StartDate.Day);
end;

procedure TfrmMissionSkriptTriggers.FormDestroy(Sender: TObject);
begin
  fPrivateRecord.Free;
end;

procedure TfrmMissionSkriptTriggers.BitBtn1Click(Sender: TObject);
var
  Typ: Integer;
begin
  Typ:=-1;
  if ManuellTrigger.Checked then
    Typ:=0
  else if RandomTrigger.Checked then
    Typ:=1
  else if singleTrigger.Checked then
    Typ:=2;

  fPrivateRecord.SetInteger('Typ',Typ);
end;

procedure TfrmMissionSkriptTriggers.ChangeEnablePanel(Panel: TPanel;
  Enabled: Boolean);
var
  Dummy: Integer;
begin
  for Dummy:=0 to ComponentCount-1 do
  begin
    if Components[Dummy] is TWinControl then
    begin
      if TWinControl(Components[Dummy]).Parent=Panel then
      begin
        TWinControl(Components[Dummy]).Enabled:=Enabled;

        if Components[Dummy] is TPanel then
          ChangeEnablePanel(TPanel(Components[Dummy]),Enabled);
      end;
    end;
  end;
end;

procedure TfrmMissionSkriptTriggers.ChangeTriggerMode(Sender: TObject);
begin
  ChangeEnablePanel(randomPanel,RandomTrigger.Checked);
  ChangeEnablePanel(SinglePanel,SingleTrigger.Checked);

  if SingleTrigger.Checked then
    TriggerListViewSelectItem(Self,TriggerListView.Selected,TriggerListView.Selected<>nil);
end;

procedure TfrmMissionSkriptTriggers.NewTriggerClick(Sender: TObject);
var
  Rec: TExtRecord;
begin
  Rec:=fPrivateRecord.GetRecordList('Triggers').Add;

  RecordToListItem(TriggerListView.Items.Add,Rec);
end;

procedure TfrmMissionSkriptTriggers.RecordToListItem(Item: TListItem;
  Rec: TExtRecord);
begin
  Item.Caption:=TypBox.Items[Rec.GetInteger('TriggerTyp')];
  if Rec.GetBoolean('Negation') then
    Item.Caption:='[Nicht] '+Item.Caption;

  if Item.SubItems.Count=0 then
  begin
    Item.SubItems.Add('');
  end;

  case Rec.GetInteger('TriggerTyp') of
    TRIGGER_FORSCHPROJECTDONE:
      Item.SubItems[0]:=GetObjektName(Rec.GetCardinal('CarParam1'),MDIForm.Projekts,MDIForm.Aliens,' - Autopsie');
    TRIGGER_DATEREACHED: // Startzeit
      Item.SubItems[0]:=DateToStr(Rec.GetInteger('IntParam1'))+' - '+FormatDateTime('hh.mm',Rec.GetInteger('IntParam2')/1440);
    TRIGGER_STARTGAME:
      Item.SubItems[0]:='---';
    TRIGGER_USERDEFINED:
      Item.SubItems[0]:=Rec.GetString('StringParam1');
  end;
end;

procedure TfrmMissionSkriptTriggers.TriggerListViewSelectItem(
  Sender: TObject; Item: TListItem; Selected: Boolean);
var
  Rec: TExtRecord;
begin
  TriggerPanel.Visible:=Selected;
  DeleteTrigger.Enabled:=Selected;
  if Selected then
  begin
    Rec:=fPrivateRecord.GetRecordList('Triggers').Item[Item.Index];
    TypBox.ItemIndex:=Rec.GetInteger('TriggerTyp');
    TriggerNegationBox.Checked:=Rec.GetBoolean('Negation');
    TypBoxChange(Self);
  end;
end;

procedure TfrmMissionSkriptTriggers.TypBoxChange(Sender: TObject);
var
  Rec   : TExtRecord;
  Dummy : Integer;
  Negation : Boolean;
begin
  Rec:=GetSelectedTrigger;
  Assert(Rec<>nil);
  if TypBox.ItemIndex<>Rec.GetInteger('TriggerTyp') then
  begin
    Negation:=Rec.GetBoolean('Negation');
    Rec.SetDefaultValues;
    Rec.SetBoolean('Negation',Negation);
    Rec.SetInteger('TriggerTyp',TypBox.ItemIndex);
    RecordToListItem(TriggerListView.Selected,Rec);
  end;

  for Dummy:=0 to ComponentCount-1 do
  begin
    if Components[Dummy] is TPanel then
    begin
      if TPanel(Components[Dummy]).Parent=TriggerParamsPanel then
      begin
        TPanel(Components[Dummy]).Visible:=TPanel(Components[Dummy]).Tag=TypBox.ItemIndex;
      end;
    end;
  end;

  RefreshTriggerParams;
end;

function TfrmMissionSkriptTriggers.GetSelectedTrigger: TExtRecord;
begin
  result:=nil;
  if TriggerListView.Selected=nil then
    exit;
  result:=fPrivateRecord.GetRecordList('Triggers').Item[TriggerListView.Selected.Index];
end;

function TfrmMissionSkriptTriggers.GetObjektName(ID: Cardinal;
  Arr: TRecordArray;Sec: TRecordArray; Zusatz: String): String;
var
  Rec: TExtRecord;
begin
  result:='';
  Rec:=record_utils_FindID(Arr,ID);
  if Rec=nil then
  begin
    if Sec<>nil then
      Rec:=record_utils_FindID(Sec,ID);
  end
  else
    Zusatz:='';

  if Rec<>nil then
    result:=string_utils_GetLanguageString(MDIForm.Sprache,Rec.GetString('Name'))+Zusatz;
end;

procedure TfrmMissionSkriptTriggers.Button1Click(Sender: TObject);
var
  Rec: TExtRecord;
begin
  Rec:=GetSelectedTrigger;
  frmSelectList.SelectProjekt:=Rec.GetCardinal('CarParam1');
  if frmSelectList.ShowModal=mrOK then
  begin
    if frmSelectList.Projekt<>-1 then
      Rec.SetCardinal('CarParam1',MDIForm.Projekts[frmSelectList.Projekt].GetCardinal('ID'))
    else
      Rec.SetCardinal('CarParam1',MDIForm.Aliens[frmSelectList.Alien].GetCardinal('ID'));
      
    RefreshTriggerParams;
  end;
end;

procedure TfrmMissionSkriptTriggers.RefreshTriggerParams;
var
  Rec: TExtRecord;
begin
  Rec:=GetSelectedTrigger;
  case TypBox.ItemIndex of
    TRIGGER_FORSCHPROJECTDONE:
      ProjektEdit0.Text:=GetObjektName(Rec.GetCardinal('CarParam1'),MDIForm.Projekts,MDIForm.Aliens,' - Autopsie');
    TRIGGER_DATEREACHED:
    begin
      DatePicker.DateTime:=Rec.GetInteger('IntParam1');
      TimePicker.DateTime:=Rec.GetInteger('IntParam2')/1440;
    end;
    TRIGGER_USERDEFINED:
      SwitchEdit.Text:=Rec.GetString('StringParam1');
  end;

  RecordToListItem(TriggerListView.Selected,Rec);
end;

procedure TfrmMissionSkriptTriggers.DeleteTriggerClick(Sender: TObject);
var
  Index: Integer;
begin
  Index:=TriggerListView.Selected.Index;
  TriggerListView.Selected.Delete;

  fPrivateRecord.GetRecordList('Triggers').Delete(Index);
end;

procedure TfrmMissionSkriptTriggers.ActiveBoxClick(Sender: TObject);
begin
  fPrivateRecord.SetBoolean('Active',ActiveBox.Checked);
end;

procedure TfrmMissionSkriptTriggers.DatePickerChange(Sender: TObject);

begin
  GetSelectedTrigger.SetInteger('IntParam1',trunc(DatePicker.Date));
  RefreshTriggerParams;
end;

procedure TfrmMissionSkriptTriggers.TimePickerChange(Sender: TObject);
begin
  GetSelectedTrigger.SetInteger('IntParam2',round(TimePicker.Time*1440));
  RefreshTriggerParams;
end;

procedure TfrmMissionSkriptTriggers.TriggerNegationBoxClick(
  Sender: TObject);
var
  Rec : TExtRecord;
begin
  Rec:=GetSelectedTrigger;
  Rec.SetBoolean('Negation',TriggerNegationBox.Checked);
  
  RecordToListItem(TriggerListView.Selected,Rec);
end;

procedure TfrmMissionSkriptTriggers.SwitchEditChange(Sender: TObject);
begin
  GetSelectedTrigger.SetString('StringParam1',SwitchEdit.Text);
  RefreshTriggerParams;
end;

end.
