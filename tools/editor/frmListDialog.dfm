object ListDialog: TListDialog
  Left = 191
  Top = 302
  BorderStyle = bsDialog
  Caption = 'Land w'#228'hlen'
  ClientHeight = 304
  ClientWidth = 283
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poMainFormCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object OKButton: TButton
    Left = 88
    Top = 272
    Width = 89
    Height = 25
    Caption = 'OK'
    Default = True
    ModalResult = 1
    TabOrder = 0
    OnClick = OKButtonClick
  end
  object Button2: TButton
    Left = 184
    Top = 272
    Width = 89
    Height = 25
    Cancel = True
    Caption = 'Abbrechen'
    ModalResult = 2
    TabOrder = 1
  end
  object ListItems: TListBox
    Left = 8
    Top = 8
    Width = 265
    Height = 257
    Style = lbOwnerDrawFixed
    ItemHeight = 27
    TabOrder = 2
    OnDblClick = ListItemsDblClick
    OnDrawItem = ListItemsDrawItem
  end
end
