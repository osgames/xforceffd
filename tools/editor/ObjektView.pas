unit ObjektView;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ComCtrls, ExtCtrls, Menus, StdCtrls, XForce_types, KD4Utils, ToolWin,
  ExtRecord, {$I language.inc};

type
  TCustomFieldInfo = record
    FieldName      : String;
    Caption        : String;
    Definition     : TExtRecordDefinition;
  end;

  TObjektForm = class(TFrame )
    ObjektView: TListView;
    Panel2: TPanel;
    Image1: TImage;
    Label1: TLabel;
    ColumnPopup: TPopupMenu;
    AlienItemsBox: TCheckBox;
    ProjectBox: TCheckBox;
    StartObjectsBox: TCheckBox;
    UFOBox: TCheckBox;
    AlienBox: TCheckBox;
    FilterBox: TEdit;
    FilterTypeBox: TComboBox;
    procedure ObjektViewCompare(Sender: TObject; Item1, Item2: TListItem;
      Data: Integer; var Compare: Integer);
    procedure ObjektViewColumnClick(Sender: TObject; Column: TListColumn);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure ObjektViewDblClick(Sender: TObject);
    procedure AlienItemsBoxClick(Sender: TObject);
    procedure ObjektViewColumnDragged(Sender: TObject);
    procedure FilterBoxChange(Sender: TObject);
  private
    Fields  : Array of TCustomFieldInfo;

    fColumnsChange : Boolean;

    fFilterRecord  : TExtRecordDefinition;

    procedure CheckCustomColumns(Rec: TExtRecord);

    procedure ColumnPopupItemClick(Sender: TObject);
    { Private-Deklarationen }
  public
    procedure AddToList(Rec: TExtRecord;Select: boolean=false);
    procedure UpdateItem(Item: TListItem; Rec: TExtRecord);
    procedure CreateTree;

    procedure Idle;
    { Public-Deklarationen }
  end;

implementation

uses frmMain, string_utils, JclMath, Math;

{$R *.DFM}

procedure TObjektForm.CheckCustomColumns(Rec: TExtRecord);
var
  Dummy    : Integer;
  Dummy2   : Integer;
  Index    : Integer;
  Parent   : TMenuItem;
  Item     : TMenuItem;

  function IndexOfField(Field: String): Integer;
  var
    Dummy   : Integer;
    Caption : String;
  begin
    result:=-1;
    Caption:=Rec.GetValueProperty(Rec.GetValueIndex(Field),'Name');
    for Dummy:=0 to high(Fields) do
    begin
      if (Fields[Dummy].FieldName=Field) and (Fields[Dummy].Caption=Caption) then
      begin
        result:=Dummy;
        exit;
      end;
    end;
  end;

begin
  Parent:=nil;
  for Dummy2:=0 to ColumnPopup.Items.Count-1 do
  begin
    if TObject(ColumnPopup.Items[Dummy2].Tag)=Rec.RecordDefinition then
    begin
      Parent:=ColumnPopup.Items[Dummy2];
      exit;
    end;
  end;
  if Parent=nil then
  begin
    Parent:=TMenuItem.Create(Self);
    Parent.Caption:=Rec.RecordDefinition.RecordName;
    Parent.Tag:=Integer(Rec.RecordDefinition);
    ColumnPopup.Items.Add(Parent);
  end;

  for Dummy:=0 to Rec.ValueCount-1 do
  begin
    if Rec.Values[Dummy].ValueType in [ervtInteger,ervtString,ervtBoolean] then
    begin
      if Rec.GetValueProperty(Dummy,'Visible')='false' then
        continue;

      if Rec.Values[Dummy].ValueName='Name' then
        continue;

      Index:=IndexOfField(Rec.Values[Dummy].ValueName);
      if Index=-1 then
      begin
        SetLength(Fields,Length(Fields)+1);
        with Fields[high(Fields)] do
        begin
          FieldName:=Rec.Values[Dummy].ValueName;
          Caption:=Rec.GetValueProperty(Dummy,'Name');
          Definition:=Rec.RecordDefinition;
        end;
        Index:=high(Fields);
      end;

      Item:=TMenuItem.Create(Self);

      Item.Tag:=Index;
      Item.Caption:=Fields[Index].Caption;
      Item.OnClick:=ColumnPopupItemClick;
      Parent.Add(Item);
//      end;
    end;
  end;
end;

procedure TObjektForm.CreateTree;
var
  Dummy: Integer;
  Start: Boolean;

  procedure AddRecordDefinition(Rec: TExtRecordDefinition);
  var
    Dummy: Integer;
  begin
    for Dummy:=0 to FilterTypeBox.Items.Count-1 do
    begin
      if FilterTypeBox.Items.Objects[Dummy]=Rec then
        exit;
    end;
    FilterTypeBox.Items.AddObject(Rec.RecordName,Rec);
  end;
begin
  ObjektView.Items.BeginUpdate;
  ObjektView.Items.Clear;

  if FilterTypeBox.ItemIndex<>-1 then
    fFilterRecord:=TExtRecordDefinition(FilterTypeBox.Items.Objects[FilterTypeBox.ItemIndex])
  else
    fFilterRecord:=nil;
    
  FilterTypeBox.Items.BeginUpdate;
  FilterTypeBox.Items.Clear;
  for Dummy:=0 to Length(MDIForm.Projekts)-1 do
  begin
    AddRecordDefinition(MDIForm.Projekts[Dummy].RecordDefinition);
    if (MDIForm.Projekts[Dummy].ValueExists('AlienItem')) and (MDIForm.Projekts[Dummy].GetBoolean('AlienItem')) then
    begin
      if AlienItemsBox.Checked then
        AddToList(MDIForm.Projekts[Dummy]);
    end
    else
    begin
      Start:=MDIForm.Projekts[Dummy].GetBoolean('Start');
      if ((StartObjectsBox.Checked and Start) or (ProjectBox.Checked and (not Start))) and (not MDIForm.DeletedChild[Dummy]) then
        AddToList(MDIForm.Projekts[Dummy])
    end;
  end;

  if length(MDIForm.Aliens)>0 then
    AddRecordDefinition(MDIForm.Aliens[0].RecordDefinition);

  if AlienBox.Checked then
  begin
    for Dummy:=0 to Length(MDIForm.Aliens)-1 do
    begin
      AddToList(MDIForm.Aliens[Dummy]);
    end;
  end;

  if length(MDIForm.UFOs)>0 then
    AddRecordDefinition(MDIForm.UFOs[0].RecordDefinition);

  if UFOBox.Checked then
  begin
    for Dummy:=0 to Length(MDIForm.UFOs)-1 do
      AddToList(MDIForm.UFOs[Dummy]);
  end;

  // Liste sortieren
  FilterTypeBox.Sorted:=true;
  FilterTypeBox.Sorted:=false;

  FilterTypeBox.Items.InsertObject(0,'Alles',nil);

  for Dummy:=0 to FilterTypeBox.Items.Count-1 do
  begin
    if FilterTypeBox.Items.Objects[Dummy]=fFilterRecord then
    begin
      FilterTypeBox.ItemIndex:=Dummy;
      break;
    end;
  end;

  FilterTypeBox.Items.EndUpdate;

  ObjektView.CustomSort(nil,0);
  ObjektView.Items.EndUpdate;
end;

procedure TObjektForm.ObjektViewCompare(Sender: TObject; Item1,
  Item2: TListItem; Data: Integer; var Compare: Integer);
var
  Field    : TCustomFieldInfo;
  Index    : Integer;
  Rec1     : TExtRecord;
  Rec2     : TExtRecord;
begin
  if frmMain.LoadSetStatus then exit;

  Rec1:=TExtRecord(Item1.Data);
  Rec2:=TExtRecord(Item2.Data);

  Index:=ObjektView.Tag;

  if Index=-2 then
  begin
    Compare:=CompareText(string_utils_GetLanguageString(MDIForm.Sprache,Rec1.GetString('Name')),string_utils_GetLanguageString(MDIForm.Sprache,Rec2.GetString('Name')));
  end
  else if Index=-1 then
  begin
    Compare:=CompareText(Rec1.RecordDefinition.RecordName,Rec2.RecordDefinition.RecordName);
  end
  else
  begin
    Compare:=0;
    Field:=Fields[Index];

    if (Rec1.ValueExists(Field.FieldName) and Rec2.ValueExists(Field.FieldName)) then
    begin
      if Rec1.GetValueProperty(Rec1.GetValueIndex(Field.FieldName),'Name')=Rec2.GetValueProperty(Rec2.GetValueIndex(Field.FieldName),'Name') then
      begin
        case Rec1.Values[Rec1.GetValueIndex(Field.FieldName)].ValueType of
          ervtInteger: Compare:=Rec1.GetInteger(Field.FieldName)-Rec2.GetInteger(Field.FieldName);
          ervtDouble : Compare:=round((Rec1.GetDouble(Field.FieldName)-Rec2.GetDouble(Field.FieldName))*100);
          ervtString : CompareText(Rec1.GetString(Field.FieldName),Rec2.GetString(Field.FieldName));
          ervtBoolean: Compare:=Ord(Rec1.GetBoolean(Field.FieldName))-Ord(Rec2.GetBoolean(Field.FieldName));
        end;
      end
      else
      begin
        if Rec1.GetValueProperty(Rec1.GetValueIndex(Field.FieldName),'Name')=Field.Caption then
          Compare:=-1
        else if Rec2.GetValueProperty(Rec2.GetValueIndex(Field.FieldName),'Name')=Field.Caption then
          Compare:=1
        else
          Compare:=0;
      end;
    end
    else
    begin
      if Rec1.ValueExists(Field.FieldName) then
        Compare:=-1
      else if Rec2.ValueExists(Field.FieldName) then
        Compare:=1
    end;
  end;
end;

procedure TObjektForm.ColumnPopupItemClick(Sender: TObject);
var
  Item  : TMenuItem;
  Dummy : Integer;
  Column: TListColumn;

  procedure CheckItem(Parent: TMenuItem);
  var
    Dummy: Integer;
  begin
    for Dummy:=0 to Parent.Count-1 do
    begin
      if Parent.Items[Dummy].Tag=Item.Tag then
        Parent.Items[Dummy].Checked:=Item.Checked;
    end;
  end;
begin
  Item:=TMenuItem(Sender);
  if Item.Checked then
  begin
    for Dummy:=0 to ObjektView.Columns.Count-1 do
    begin
      if ObjektView.Columns[Dummy].Tag=Item.Tag then
      begin
        ObjektView.Columns[Dummy].Free;
        break;
      end;
    end;
    Item.Checked:=false;
  end
  else
  begin
    Column:=ObjektView.Columns.Add;
    Column.Caption:=Item.Caption;
    Column.Width:=100;
    Column.Tag:=Item.Tag;

    Item.Checked:=true;
  end;

  ObjektView.Items.BeginUpdate;

  for Dummy:=0 to ObjektView.Items.Count-1 do
    UpdateItem(ObjektView.Items[Dummy],TExtRecord(ObjektView.Items[Dummy].Data));

  for Dummy:=0 to ColumnPopup.Items.Count-1 do
    CheckItem(ColumnPopup.Items[Dummy]);

  ObjektView.Items.EndUpdate;
end;

procedure TObjektForm.ObjektViewColumnClick(Sender: TObject;
  Column: TListColumn);
begin
  ObjektView.Tag:=Column.Tag;
  ObjektView.CustomSort(nil,0);
end;

procedure TObjektForm.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  Action:=caFree;
  MDIForm.ObjektViewer:=nil;
end;

procedure TObjektForm.ObjektViewDblClick(Sender: TObject);
var
  Rec: TExtRecord;
begin
  if ObjektView.Selected<>nil then
  begin
    Rec:=TExtRecord.Create(TExtRecord(ObjektView.Selected.Data).RecordDefinition);
    Rec.Assign(TExtRecord(ObjektView.Selected.Data));

    if MDIForm.EditProjekt(Rec) then
    begin
      if not Rec.Compare(TExtRecord(ObjektView.Selected.Data)) then
      begin
        TExtRecord(ObjektView.Selected.Data).Assign(Rec);
        UpdateItem(ObjektView.Selected,Rec);

        MDIForm.ForschChanged:=true;
      end;
    end;

    Rec.Free;
  end;

end;

procedure TObjektForm.AddToList(Rec: TExtRecord;Select: boolean);
var
  Item: TListItem;
  Text: String;
begin
  if (FilterBox.Text<>'') then
  begin
    Text:=string_utils_GetLanguageString(MDIForm.Sprache,Rec.GetString('Name'));
    if (Pos(lowercase(FilterBox.Text),LowerCase(Text))=0) then
      exit;
  end;

  if (fFilterRecord<>nil) and (fFilterRecord<>Rec.RecordDefinition) then
    exit;
    
  CheckCustomColumns(Rec);
  Item:=ObjektView.Items.Add;
  Item.Data:=Rec;
  UpdateItem(Item,Rec);
end;

procedure TObjektForm.UpdateItem(Item: TListItem; Rec: TExtRecord);
var
  Text   : String;
  Field  : TCustomFieldInfo;
  Dummy  : Integer;
begin
  Item.Caption:=string_utils_GetLanguageString(MDIForm.Sprache,Rec.GetString('Name'));
  if Rec.ValueExists('ImageIndex') then
    Item.ImageIndex:=Rec.GetInteger('ImageIndex')
  else
    Item.ImageIndex:=-1;

  Item.SubItems.Clear;

  for Dummy:=0 to ObjektView.Columns.Count-1 do
  begin
    if ObjektView.Columns[Dummy].Tag=-1 then
      Text:=Rec.RecordDefinition.RecordName
    else if ObjektView.Columns[Dummy].Tag=-2 then
      Text:=string_utils_GetLanguageString(MDIForm.Sprache,Rec.GetString('Name'))
    else
    begin
      Text:='';
      Field:=Fields[ObjektView.Columns[Dummy].Tag];
      if Rec.ValueExists(Field.FieldName) then
      begin
        if Field.Caption=Rec.GetValueProperty(Rec.GetValueIndex(Field.FieldName),'Name') then
        begin
          with Rec.Values[Rec.GetValueIndex(Field.FieldName)] do
          begin
            case ValueType of
              ervtInteger : Text:=IntToStr(valueInt);
              ervtString  : Text:=valueString;
              ervtDouble  : Text:=Format('%.'+IntToStr(Kommasdoub)+'n',[valuedoub]);
              ervtBoolean :
                if valueBool then
                  Text:='Ja'
                else
                  Text:='Nein';
              else
                Text:='Ungültiger Typ';
            end;
          end;
        end;
      end;
    end;

    if Dummy=0 then
      Item.Caption:=Text
    else
      Item.SubItems.Add(Text);
  end;
end;

procedure TObjektForm.AlienItemsBoxClick(Sender: TObject);
begin
  CreateTree;
end;

procedure TObjektForm.ObjektViewColumnDragged(Sender: TObject);
begin
  fColumnsChange:=true;
end;

procedure TObjektForm.Idle;
var
  Dummy: Integer;
begin
  if fColumnsChange then
  begin
    ObjektView.Columns.Add.Free;
    ObjektView.Items.BeginUpdate;
    for Dummy:=0 to ObjektView.Items.Count-1 do
      UpdateItem(ObjektView.Items[Dummy],TExtRecord(ObjektView.Items[Dummy].Data));
    ObjektView.Items.EndUpdate;
    
    fColumnsChange:=false;
  end;
end;

procedure TObjektForm.FilterBoxChange(Sender: TObject);
begin
  CreateTree;
end;

end.
