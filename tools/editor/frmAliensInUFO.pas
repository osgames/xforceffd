unit frmAliensInUFO;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, ComCtrls, ExtRecord, ObjektRecords, Grids;

type

  TAliensInUFO = class(TForm)
    OKButton: TBitBtn;
    CancelButton: TBitBtn;
    Label1: TLabel;
    AlienList: TStringGrid;
    ErrorLabel: TLabel;
    Button1: TButton;
    procedure OKButtonClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure AlienListSetEditText(Sender: TObject; ACol, ARow: Integer;
      const Value: String);
    procedure Button1Click(Sender: TObject);
  private
    fAliens   : TExtRecordList;
    procedure SetAliens(const Value: TExtRecordList);

    procedure CheckAliens;
    { Private-Deklarationen }
  public
    property Aliens: TExtRecordList read fAliens write SetAliens;
    { Public-Deklarationen }
  end;

var
  AliensInUFO: TAliensInUFO;

implementation

uses string_utils, frmMain;

{$R *.DFM}

procedure TAliensInUFO.SetAliens(const Value: TExtRecordList);
var
  Dummy: Integer;

  function GetChance(ID: Cardinal): Integer;
  var
    Dummy: Integer;
  begin
    for DUmmy:=0 to fAliens.Count-1 do
    begin
      if fAliens[Dummy].GetCardinal('ID')=ID then
      begin
        result:=fAliens[Dummy].GetInteger('Chance');
        exit;
      end;
    end;
    result:=0;
  end;

begin
  fAliens.Assign(Value);

  AlienList.RowCount:=length(MDIForm.Aliens)+1;

  for Dummy:=0 to high(MDIForm.Aliens) do
  begin
    AlienList.Cells[0,Dummy+1]:=string_utils_GetLanguageString(MDIForm.Sprache,MDIForm.Aliens[Dummy].GetString('Name'));
    AlienList.Cells[1,Dummy+1]:=IntToStr(GetChance(MDIForm.ALiens[Dummy].GetCardinal('ID')));
  end;
end;

procedure TAliensInUFO.OKButtonClick(Sender: TObject);
var
  Dummy: Integer;
begin
  fAliens.Clear;
  for Dummy:=1 to AlienList.RowCount-1 do
  begin
    if StrToInt(AlienList.Cells[1,Dummy])>0 then
    begin
      with fAliens.Add do
      begin
        SetCardinal('ID',Cardinal(MDIForm.Aliens[Dummy-1].GetCardinal('ID')));
        SetInteger('Chance',StrToInt(AlienList.Cells[1,Dummy]));
      end;
    end;
  end;
end;

procedure TAliensInUFO.FormCreate(Sender: TObject);
begin
  fAliens:=TExtRecordList.Create(RecordAlienInUFO);
  AlienList.Cells[0,0]:='Alien';
  AlienList.Cells[1,0]:='H�ufigkeit (%)';
end;

procedure TAliensInUFO.FormDestroy(Sender: TObject);
begin
  fAliens.Free;
end;

procedure TAliensInUFO.AlienListSetEditText(Sender: TObject; ACol,
  ARow: Integer; const Value: String);
begin
  CheckAliens;
end;

procedure TAliensInUFO.CheckAliens;
var
  Dummy: Integer;
  Err  : Integer;
  Value: Integer;
  Summe: Integer;
begin
  ErrorLabel.Caption:='';

  Summe:=0;
  for Dummy:=1 to AlienList.RowCount-1 do
  begin
    Val(AlienList.Cells[1,Dummy],Value,Err);
    if (Err<>0) or (Value<0) or (Value>100) then
    begin
      ErrorLabel.Caption:=AlienList.Cells[0,Dummy]+': un�ltige Eingabe (0-100)';
      Summe:=0;
      break;
    end;
    inc(Summe,Value);
  end;

  if (Summe<>0) and (Summe<>100) then
  begin
    ErrorLabel.Caption:=Format('Summe muss 0 oder 100 ergeben. Bisher: %d',[Summe]);
  end;

  OKButton.Enabled:=ErrorLabel.Caption='';
end;

procedure TAliensInUFO.Button1Click(Sender: TObject);
var
  Dummy: Integer;
begin
  for Dummy:=1 to AlienList.RowCount-1 do
  begin
    AlienList.Cells[1,Dummy]:='0';
  end;
  CheckAliens;
end;

end.
