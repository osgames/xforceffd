object AliensInUFO: TAliensInUFO
  Left = 429
  Top = 435
  BorderStyle = bsDialog
  Caption = 'Aliens'
  ClientHeight = 358
  ClientWidth = 321
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poMainFormCenter
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 8
    Top = 6
    Width = 209
    Height = 13
    Caption = 'Aliens die in diesem Raumschiff vorkommen:'
  end
  object ErrorLabel: TLabel
    Left = 8
    Top = 304
    Width = 305
    Height = 13
    AutoSize = False
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clRed
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object OKButton: TBitBtn
    Left = 128
    Top = 324
    Width = 89
    Height = 25
    TabOrder = 0
    OnClick = OKButtonClick
    Kind = bkOK
  end
  object CancelButton: TBitBtn
    Left = 224
    Top = 324
    Width = 89
    Height = 25
    TabOrder = 1
    Kind = bkCancel
  end
  object AlienList: TStringGrid
    Left = 8
    Top = 24
    Width = 305
    Height = 277
    ColCount = 2
    DefaultRowHeight = 20
    RowCount = 2
    Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goDrawFocusSelected, goColSizing, goEditing, goAlwaysShowEditor, goThumbTracking]
    TabOrder = 2
    OnSetEditText = AlienListSetEditText
    ColWidths = (
      192
      87)
  end
  object Button1: TButton
    Left = 8
    Top = 324
    Width = 75
    Height = 25
    Caption = 'Alle Aliens'
    TabOrder = 3
    OnClick = Button1Click
  end
end
