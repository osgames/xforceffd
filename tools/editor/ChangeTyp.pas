unit ChangeTyp;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, XForce_types, ExtCtrls, ExtendEdit;

  const TypeArray: Array[0..13] of TProjektType = (ptWaffe,ptMunition,ptGranate,ptMine,ptPanzerung,ptGuertel,
                    ptSensor,ptRaumSchiff,ptRWaffe,ptRMunition,ptMotor,ptEinrichtung,ptNone,ptExtension);
type
  TTypForm = class(TForm)
    OKButton: TBitBtn;
    CancelButton: TBitBtn;
    TypBox: TExtendComboBox;
    Panel2: TPanel;
    TypInfo: TMemo;
    procedure TypBoxChange(Sender: TObject);
    procedure OKButtonClick(Sender: TObject);
    procedure CancelButtonClick(Sender: TObject);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
  private
    function GetType: TProjektType;
    procedure SetType(const Value: TProjektType);
    function GetIndexofType(Typ: TProjektType): Integer;
    { Private-Deklarationen }
  public
    property Typ: TProjektType read GetType write SetType;
    { Public-Deklarationen }
  end;

var
  TypForm: TTypForm;

implementation

{$R *.DFM}

{ TTypForm }

function TTypForm.GetIndexofType(Typ: TProjektType): Integer;
var
  Dummy: Integer;
begin
  result:=-1;
  for Dummy:=0 to high(TypeArray) do
  begin
    if TypeArray[Dummy]=typ then
    begin
      result:=dummy;
      exit;
    end;
  end;
end;

function TTypForm.GetType: TProjektType;
begin
  result:=TypeArray[TypBox.ItemIndex];
end;

procedure TTypForm.SetType(const Value: TProjektType);
begin
  TypBox.ItemIndex:=GetIndexofType(Value);
  TypBoxChange(TypBox);
end;

procedure TTypForm.TypBoxChange(Sender: TObject);
begin
  TypInfo.Lines.Clear;
  case Typ of
    ptWaffe :
      begin
        TypInfo.Lines.Text:='Die Waffe ist die Standardausr�stung von einem Soldaten.'+
        'Sie kann beliebig viele Munitionsarten haben. Die einzige Ausnahme bildet dort '+
        'die Laserpistole und Selbstverst�ndlich Nahkampfwaffen.';
      end;
    ptMunition :
      begin
        TypInfo.Lines.Text:='Die Munition muss einer Waffe zugewiesen werden. Es gibt keine '+
        'Munition f�r Laserwaffen.';
      end;
    ptRWaffe :
      begin
        TypInfo.Lines.Text:='Eine Angriffswaffe f�r ein Raumschiff. Es gibt keine Chemischen Raumschiff-'+
        'Waffen. F�r jede Waffe darf nur eine Munition definiert sein. Die einzigste Ausnahme bilden '+
        'die Laserwaffen, dort darf keine Munition angegeben sein.';
      end;
    ptEinrichtung :
      begin
        TypInfo.Lines.Text:='Ein Geb�ude in Ihrer Einrichtung. Die Baukosten verteilen sich auf die gesamte '+
        'Bauzeit. Die Monatlichen Kosten sind auch von der Bauzeit abh�ngig';
      end;
    ptPanzerung :
      begin
        TypInfo.Lines.Text:='Panzerung f�r Ihren Soldaten, um den Schaden von Angriffen zu verringern.';
      end;
    ptRaumSchiff :
      begin
        TypInfo.Lines.Text:='Ein Raumschiffmodell. Jedes Raumschiff ben�tigt einen Freien Hangar Platz.'+
        'Die Baukosten verteilen sich �ber die gesamte Bauzeit';
      end;
    ptRMunition :
      begin
        TypInfo.Lines.Text:='Munition zu einer Raumschiffwaffe. Es gibt keine Lasermunition.';
      end;
    ptGranate :
      begin
        TypInfo.Lines.Text:='Eine Granate kann im Spiel geworfen werden und explodiert dann.';
      end;
    ptMotor :
      begin
        TypInfo.Lines.Text:='Motor f�r Ihre Raumschiffe.';
      end;
    ptMine :
      begin
        TypInfo.Lines.Text:='Eine Mine kann im Spiel auf den Boden gelegt werden. Wenn sich eine '+
        'gegnerische Einheit n�hert explodiert sie';
      end;
    ptNone :
      begin
        TypInfo.Lines.Text:='Technologien sind nur als Zwischenforschungen gedacht. Z.B. mu� man erst '+
        'die Sonartechnologie erforschen, bevor es m�glich ist Sonarpistole und �hnliche Ausr�stung zu '+
        'benutzen. Ausr�stung vom Typ ''Technologie'' wird ignoriert';
      end;                       
    ptSensor :
      begin
        TypInfo.Lines.Text:='Sensor dienen im Spiel zum permanenten Aufdecken eines Gebietes. Sie werden '+
        'auf dem Spielfeld abgelegt und bleiben dort solange bis man sie wieder aufnimmt.';
      end;
    ptShield :
      begin
        TypInfo.Lines.Text:='Schutzschilde sch�tzen Raumschiffe vor Treffern. ';
      end;
    ptExtension:
      begin
        TypInfo.Lines.Text:='Diverese Erweiterung f�r ein Raumschiff.';
      end;
    ptGuertel:
      begin
        TypInfo.Lines.Text:='In G�rteln k�nnen Munitionen von den Soldaten abgelegt werden, um im Bodeneinsatz'+
        ' schnell drauf zugreifen zu k�nnen';
      end;
  end;
end;

procedure TTypForm.OKButtonClick(Sender: TObject);
begin
  ModalResult:=mrOK;
end;

procedure TTypForm.CancelButtonClick(Sender: TObject);
begin
  ModalResult:=mrCancel;
end;

procedure TTypForm.FormKeyPress(Sender: TObject; var Key: Char);
begin
  if Key=#13 then OKButton.Click
  else if Key=#27 then CancelButton.Click;
end;

end.
