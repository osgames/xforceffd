object FehlerAssistent: TFehlerAssistent
  Left = 461
  Top = 841
  BorderStyle = bsDialog
  Caption = 'Fehlerassistent'
  ClientHeight = 86
  ClientWidth = 434
  Color = clBtnFace
  Constraints.MinHeight = 100
  Constraints.MinWidth = 442
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poMainFormCenter
  DesignSize = (
    434
    86)
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 56
    Top = 8
    Width = 32
    Height = 13
    Caption = 'Label1'
  end
  object InfoIcon: TImage
    Left = 8
    Top = 8
    Width = 32
    Height = 33
  end
  object CloseButton: TBitBtn
    Left = 341
    Top = 57
    Width = 89
    Height = 25
    Anchors = [akRight, akBottom]
    Cancel = True
    Caption = 'Schliessen'
    TabOrder = 0
    OnClick = CloseButtonClick
  end
  object Button1: TButton
    Left = 5
    Top = 57
    Width = 105
    Height = 25
    Anchors = [akLeft, akBottom]
    Caption = 'Munitionstyp '#228'ndern'
    TabOrder = 1
  end
  object Button2: TButton
    Left = 117
    Top = 57
    Width = 105
    Height = 25
    Anchors = [akLeft, akBottom]
    Caption = 'Munition l'#246'schen'
    TabOrder = 2
  end
  object Button3: TButton
    Left = 229
    Top = 57
    Width = 105
    Height = 25
    Anchors = [akLeft, akBottom]
    Caption = 'Munition l'#246'schen'
    TabOrder = 3
  end
  object Panel1: TPanel
    Left = 5
    Top = 50
    Width = 426
    Height = 2
    Anchors = [akLeft, akRight, akBottom]
    BevelOuter = bvLowered
    TabOrder = 4
  end
end
