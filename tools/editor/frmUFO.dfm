object UFOForm: TUFOForm
  Left = 0
  Top = 0
  Width = 443
  Height = 275
  Align = alClient
  TabOrder = 0
  object UFOList: TListView
    Left = 0
    Top = 22
    Width = 443
    Height = 253
    Hint = 'Zeigt alle UFO'#39's zum bearbeiten an.'
    Align = alClient
    Columns = <
      item
        Caption = 'Name'
        Width = 200
      end
      item
        Alignment = taRightJustify
        Caption = 'Hitpoints'
        Width = 75
      end
      item
        Alignment = taRightJustify
        Caption = 'Schildpunkte'
        Width = 75
      end
      item
        Alignment = taRightJustify
        Caption = 'Angriffst'#228'rke'
        Width = 75
      end
      item
        Alignment = taRightJustify
        Caption = 'min. Besatzung'
        Width = 85
      end
      item
        Alignment = taRightJustify
        Caption = 'zus. Besatzung'
        Width = 85
      end
      item
        Alignment = taRightJustify
        Caption = 'Angriff nach'
        Width = 75
      end>
    FullDrag = True
    HideSelection = False
    RowSelect = True
    PopupMenu = UFOPopup
    SortType = stData
    TabOrder = 0
    ViewStyle = vsReport
    OnColumnClick = UFOListColumnClick
    OnCompare = UFOListCompare
    OnDblClick = UFOListDblClick
    OnEdited = UFOListEdited
  end
  object Panel2: TPanel
    Left = 0
    Top = 0
    Width = 443
    Height = 22
    Align = alTop
    Alignment = taLeftJustify
    BevelOuter = bvNone
    BorderWidth = 1
    TabOrder = 1
    DesignSize = (
      443
      22)
    object Image1: TImage
      Left = 2
      Top = 2
      Width = 32
      Height = 32
      AutoSize = True
      Picture.Data = {
        055449636F6E0000010001001010000001000800680500001600000028000000
        1000000020000000010008000000000000010000000000000000000000010000
        000100000000000055000000AA000000FF0000000024000055240000AA240000
        FF2400000049000055490000AA490000FF490000006D0000556D0000AA6D0000
        FF6D00000092000055920000AA920000FF92000000B6000055B60000AAB60000
        FFB6000000DB000055DB0000AADB0000FFDB000000FF000055FF0000AAFF0000
        FFFF00000000240055002400AA002400FF0024000024240055242400AA242400
        FF2424000049240055492400AA492400FF492400006D2400556D2400AA6D2400
        FF6D24000092240055922400AA922400FF92240000B6240055B62400AAB62400
        FFB6240000DB240055DB2400AADB2400FFDB240000FF240055FF2400AAFF2400
        FFFF24000000490055004900AA004900FF0049000024490055244900AA244900
        FF2449000049490055494900AA494900FF494900006D4900556D4900AA6D4900
        FF6D49000092490055924900AA924900FF92490000B6490055B64900AAB64900
        FFB6490000DB490055DB4900AADB4900FFDB490000FF490055FF4900AAFF4900
        FFFF490000006D0055006D00AA006D00FF006D0000246D0055246D00AA246D00
        FF246D0000496D0055496D00AA496D00FF496D00006D6D00556D6D00AA6D6D00
        FF6D6D0000926D0055926D00AA926D00FF926D0000B66D0055B66D00AAB66D00
        FFB66D0000DB6D0055DB6D00AADB6D00FFDB6D0000FF6D0055FF6D00AAFF6D00
        FFFF6D000000920055009200AA009200FF0092000024920055249200AA249200
        FF2492000049920055499200AA499200FF499200006D9200556D9200AA6D9200
        FF6D92000092920055929200AA929200FF92920000B6920055B69200AAB69200
        FFB6920000DB920055DB9200AADB9200FFDB920000FF920055FF9200AAFF9200
        FFFF92000000B6005500B600AA00B600FF00B6000024B6005524B600AA24B600
        FF24B6000049B6005549B600AA49B600FF49B600006DB600556DB600AA6DB600
        FF6DB6000092B6005592B600AA92B600FF92B60000B6B60055B6B600AAB6B600
        FFB6B60000DBB60055DBB600AADBB600FFDBB60000FFB60055FFB600AAFFB600
        FFFFB6000000DB005500DB00AA00DB00FF00DB000024DB005524DB00AA24DB00
        FF24DB000049DB005549DB00AA49DB00FF49DB00006DDB00556DDB00AA6DDB00
        FF6DDB000092DB005592DB00AA92DB00FF92DB0000B6DB0055B6DB00AAB6DB00
        FFB6DB0000DBDB0055DBDB00AADBDB00FFDBDB0000FFDB0055FFDB00AAFFDB00
        FFFFDB000000FF005500FF00AA00FF00FF00FF000024FF005524FF00AA24FF00
        FF24FF000049FF005549FF00AA49FF00FF49FF00006DFF00556DFF00AA6DFF00
        FF6DFF000092FF005592FF00AA92FF00FF92FF0000B6FF0055B6FF00AAB6FF00
        FFB6FF0000DBFF0055DBFF00AADBFF00FFDBFF0000FFFF0055FFFF00AAFFFF00
        FFFFFF0000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        000000000000000000000000240000000000000000000045698E896A698A698A
        45442500000000AACED3F6CEABAAAAAA8A694500000000AAAEF2D3CEAAAEAAAA
        8966650000000000000000000000694524000000000000000000002449490000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        00000000FFFF0000FFFF0000FFFF0000E6330000F2270000F80F0000E0030000
        800100008000000080000000E0030000F80F0000FE3F0000FFFF0000FFFF0000
        FFFF0000}
    end
    object Label2: TLabel
      Left = 19
      Top = 4
      Width = 36
      Height = 13
      Caption = 'UFOs:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object FilterBox: TEdit
      Left = 272
      Top = 0
      Width = 170
      Height = 21
      Hint = 'Filter'
      Anchors = [akTop, akRight]
      TabOrder = 0
      OnChange = FilterBoxChange
    end
  end
  object UFOPopup: TPopupMenu
    Images = MDIForm.ImageList1
    Left = 24
    Top = 16
    object PopupEditUFO: TMenuItem
      Action = MDIForm.ActionUFOEdit
      Default = True
    end
    object Umbenennen1: TMenuItem
      Action = MDIForm.ActionUFORename
    end
    object N2: TMenuItem
      Caption = '-'
    end
    object UFOHinzufgen1: TMenuItem
      Action = MDIForm.ActionUFOAdd
    end
    object UFOlschen1: TMenuItem
      Action = MDIForm.ActionUFODelete
    end
    object N3: TMenuItem
      Caption = '-'
    end
    object Alienhinzufgen1: TMenuItem
      Action = MDIForm.ActionUFOCut
    end
    object Kopieren1: TMenuItem
      Action = MDIForm.ActionUFOCopy
    end
    object Einfgen1: TMenuItem
      Action = MDIForm.ActionUFOPaste
    end
    object N1: TMenuItem
      Caption = '-'
    end
    object SortUFO: TMenuItem
      Caption = 'Sortieren nach'
      object Name1: TMenuItem
        Caption = 'Name'
        Checked = True
        RadioItem = True
        OnClick = SortClick
      end
      object Hitpoints1: TMenuItem
        Tag = 1
        Caption = 'Hitpoints'
        RadioItem = True
        OnClick = SortClick
      end
      object Schildpunkte1: TMenuItem
        Tag = 2
        Caption = 'Schildpunkte'
        RadioItem = True
        OnClick = SortClick
      end
      object Angriffsstrke1: TMenuItem
        Tag = 3
        Caption = 'Angriffsst'#228'rke'
        RadioItem = True
        OnClick = SortClick
      end
      object minBesatzunh1: TMenuItem
        Tag = 4
        Caption = 'min. Besatzung'
        RadioItem = True
        OnClick = SortClick
      end
      object zusBesatzung1: TMenuItem
        Tag = 5
        Caption = 'zus. Besatzung'
        RadioItem = True
        OnClick = SortClick
      end
      object Angriffnach1: TMenuItem
        Tag = 6
        Caption = 'Angriff nach'
        RadioItem = True
        OnClick = SortClick
      end
      object Gesamtstrke1: TMenuItem
        Tag = 7
        Caption = 'Gesamtst'#228'rke'
        RadioItem = True
        OnClick = SortClick
      end
    end
  end
end
