object frmNew: TfrmNew
  Left = 425
  Top = 315
  BorderStyle = bsDialog
  Caption = 'Neuen Spielsatz'
  ClientHeight = 381
  ClientWidth = 469
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poMainFormCenter
  OnCreate = FormCreate
  OnKeyPress = FormKeyPress
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Panel4: TPanel
    Left = 0
    Top = 57
    Width = 469
    Height = 320
    Align = alTop
    BevelOuter = bvNone
    Caption = 'Panel4'
    TabOrder = 0
    DesignSize = (
      469
      320)
    object OKButton: TBitBtn
      Left = 248
      Top = 288
      Width = 106
      Height = 25
      Hint = 'Klicken Sie hier um, den neuen Satz zu erstellen'
      Anchors = [akLeft, akBottom]
      Caption = 'OK'
      TabOrder = 9
      OnClick = OKButtonClick
      Glyph.Data = {
        36060000424D3606000000000000360400002800000020000000100000000100
        0800000000000002000000000000000000000001000000000000000000004000
        000080000000FF000000002000004020000080200000FF200000004000004040
        000080400000FF400000006000004060000080600000FF600000008000004080
        000080800000FF80000000A0000040A0000080A00000FFA0000000C0000040C0
        000080C00000FFC0000000FF000040FF000080FF0000FFFF0000000020004000
        200080002000FF002000002020004020200080202000FF202000004020004040
        200080402000FF402000006020004060200080602000FF602000008020004080
        200080802000FF80200000A0200040A0200080A02000FFA0200000C0200040C0
        200080C02000FFC0200000FF200040FF200080FF2000FFFF2000000040004000
        400080004000FF004000002040004020400080204000FF204000004040004040
        400080404000FF404000006040004060400080604000FF604000008040004080
        400080804000FF80400000A0400040A0400080A04000FFA0400000C0400040C0
        400080C04000FFC0400000FF400040FF400080FF4000FFFF4000000060004000
        600080006000FF006000002060004020600080206000FF206000004060004040
        600080406000FF406000006060004060600080606000FF606000008060004080
        600080806000FF80600000A0600040A0600080A06000FFA0600000C0600040C0
        600080C06000FFC0600000FF600040FF600080FF6000FFFF6000000080004000
        800080008000FF008000002080004020800080208000FF208000004080004040
        800080408000FF408000006080004060800080608000FF608000008080004080
        800080808000FF80800000A0800040A0800080A08000FFA0800000C0800040C0
        800080C08000FFC0800000FF800040FF800080FF8000FFFF80000000A0004000
        A0008000A000FF00A0000020A0004020A0008020A000FF20A0000040A0004040
        A0008040A000FF40A0000060A0004060A0008060A000FF60A0000080A0004080
        A0008080A000FF80A00000A0A00040A0A00080A0A000FFA0A00000C0A00040C0
        A00080C0A000FFC0A00000FFA00040FFA00080FFA000FFFFA0000000C0004000
        C0008000C000FF00C0000020C0004020C0008020C000FF20C0000040C0004040
        C0008040C000FF40C0000060C0004060C0008060C000FF60C0000080C0004080
        C0008080C000FF80C00000A0C00040A0C00080A0C000FFA0C00000C0C00040C0
        C00080C0C000FFC0C00000FFC00040FFC00080FFC000FFFFC0000000FF004000
        FF008000FF00FF00FF000020FF004020FF008020FF00FF20FF000040FF004040
        FF008040FF00FF40FF000060FF004060FF008060FF00FF60FF000080FF004080
        FF008080FF00FF80FF0000A0FF0040A0FF0080A0FF00FFA0FF0000C0FF0040C0
        FF0080C0FF00FFC0FF0000FFFF0040FFFF0080FFFF00FFFFFF00B6B6B6B6B6B6
        B6B6B6B6B6B6B6B6B6B6B6B6B6B6B6B6B6B6B6B6B6B6B6B6B6B6B6B6B6B6B6B6
        B6B6B6B6B6B6B6B6B6B6B6B6B6B6B6B6B6B6B6B6B6B6B6B6B6B6B6B6B6B6B6B6
        2F00B6B6B6B6B6B6B6B6B6B6B6B6B6B6FF6DB6B6B6B6B6B6B6B6B6B6B6B6B673
        000000B6B6B6B6B6B6B6B6B6B6B6B6FF6D6D6DB6B6B6B6B6B6B6B6B6B6B6B673
        000000B6B6B6B6B6B6B6B6B6B6B6B6FF6D6D6DB6B6B6B6B6B6B6B6B6B6B69300
        00000000B6B6B6B6B6B6B6B6B6B6FF6D6D6D6D6DB6B6B6B6B6B6B6B6B69F0000
        00000000B6B6B6B6B6B6B6B6B6FF6D6D6D6D6D6DB6B6B6B6B6B6B6B69F0F0400
        B62F000000B6B6B6B6B6B6B6FFFF6D6DB6FF6D6D6DB6B6B6B6B6B69F2F00B6B6
        B6B62F0000B6B6B6B6B6B6FFFF6DB6B6B6B6FF6D6DB6B6B6B6B6B6B6B6B6B6B6
        B6B673040000B6B6B6B6B6B6B6B6B6B6B6B6FF6D6D6DB6B6B6B6B6B6B6B6B6B6
        B6B6B6930400B6B6B6B6B6B6B6B6B6B6B6B6B6FF6D6DB6B6B6B6B6B6B6B6B6B6
        B6B6B6B6930700B6B6B6B6B6B6B6B6B6B6B6B6B6FFB66DB6B6B6B6B6B6B6B6B6
        B6B6B6B6B69F0700B6B6B6B6B6B6B6B6B6B6B6B6B6FFB66DB6B6B6B6B6B6B6B6
        B6B6B6B6B6B6FF2F00B6B6B6B6B6B6B6B6B6B6B6B6B6FFB66DB6B6B6B6B6B6B6
        B6B6B6B6B6B6B6FF5353B6B6B6B6B6B6B6B6B6B6B6B6B6FFFFFFB6B6B6B6B6B6
        B6B6B6B6B6B6B6B6B6B6B6B6B6B6B6B6B6B6B6B6B6B6B6B6B6B6}
      NumGlyphs = 2
    end
    object CancelButton: TBitBtn
      Left = 360
      Top = 288
      Width = 101
      Height = 25
      Hint = 'Klicken Sie hier um, die Aktion abzubrechen'
      Anchors = [akLeft, akBottom]
      Caption = 'Abbrechen'
      TabOrder = 11
      OnClick = CancelButtonClick
      Glyph.Data = {
        36060000424D3606000000000000360400002800000020000000100000000100
        0800000000000002000000000000000000000001000000000000000000004000
        000080000000FF000000002000004020000080200000FF200000004000004040
        000080400000FF400000006000004060000080600000FF600000008000004080
        000080800000FF80000000A0000040A0000080A00000FFA0000000C0000040C0
        000080C00000FFC0000000FF000040FF000080FF0000FFFF0000000020004000
        200080002000FF002000002020004020200080202000FF202000004020004040
        200080402000FF402000006020004060200080602000FF602000008020004080
        200080802000FF80200000A0200040A0200080A02000FFA0200000C0200040C0
        200080C02000FFC0200000FF200040FF200080FF2000FFFF2000000040004000
        400080004000FF004000002040004020400080204000FF204000004040004040
        400080404000FF404000006040004060400080604000FF604000008040004080
        400080804000FF80400000A0400040A0400080A04000FFA0400000C0400040C0
        400080C04000FFC0400000FF400040FF400080FF4000FFFF4000000060004000
        600080006000FF006000002060004020600080206000FF206000004060004040
        600080406000FF406000006060004060600080606000FF606000008060004080
        600080806000FF80600000A0600040A0600080A06000FFA0600000C0600040C0
        600080C06000FFC0600000FF600040FF600080FF6000FFFF6000000080004000
        800080008000FF008000002080004020800080208000FF208000004080004040
        800080408000FF408000006080004060800080608000FF608000008080004080
        800080808000FF80800000A0800040A0800080A08000FFA0800000C0800040C0
        800080C08000FFC0800000FF800040FF800080FF8000FFFF80000000A0004000
        A0008000A000FF00A0000020A0004020A0008020A000FF20A0000040A0004040
        A0008040A000FF40A0000060A0004060A0008060A000FF60A0000080A0004080
        A0008080A000FF80A00000A0A00040A0A00080A0A000FFA0A00000C0A00040C0
        A00080C0A000FFC0A00000FFA00040FFA00080FFA000FFFFA0000000C0004000
        C0008000C000FF00C0000020C0004020C0008020C000FF20C0000040C0004040
        C0008040C000FF40C0000060C0004060C0008060C000FF60C0000080C0004080
        C0008080C000FF80C00000A0C00040A0C00080A0C000FFA0C00000C0C00040C0
        C00080C0C000FFC0C00000FFC00040FFC00080FFC000FFFFC0000000FF004000
        FF008000FF00FF00FF000020FF004020FF008020FF00FF20FF000040FF004040
        FF008040FF00FF40FF000060FF004060FF008060FF00FF60FF000080FF004080
        FF008080FF00FF80FF0000A0FF0040A0FF0080A0FF00FFA0FF0000C0FF0040C0
        FF0080C0FF00FFC0FF0000FFFF0040FFFF0080FFFF00FFFFFF00B6B6B6B6B6B6
        B6B6B6B6B6B6B6B6B6B6B6B6B6B6B6B6B6B6B6B6B6B6B6B6B6B6B6B6B6B6B6B6
        B6B6B6B6B6B6B640D1B6B6B6B6B6B6B6B6B6B6B6B6B6B66DFFB6B6B6B620D1B6
        B6B6B6B6B6B6B6B6B6B6B6B6B66DFFB6B6B6B6B6B6B6B6B6B6B6B6B6202020D1
        B6B6B6B6B6B640D1B6B6B6B66D6D6DFFB6B6B6B6B6B66DFFB6B6B6B6204040D1
        B6B6B6B6B640D1B6B6B6B6B66D6D6DFFB6B6B6B6B66DFFB6B6B6B6B6B6404040
        F3B6B6B64060F3B6B6B6B6B6B66D6D6DFFB6B6B66D6DFFB6B6B6B6B6B6B64040
        60F3B66060F3B6B6B6B6B6B6B6B66D6D6DFFB66D6DFFB6B6B6B6B6B6B6B6B640
        60606060F3B6B6B6B6B6B6B6B6B6B66D6D6D6D6DFFB6B6B6B6B6B6B6B6B6B6B6
        606060F3B6B6B6B6B6B6B6B6B6B6B6B66D6D6DFFB6B6B6B6B6B6B6B6B6B6B660
        60606084F3B6B6B6B6B6B6B6B6B6B66D6D6D6D6DFFB6B6B6B6B6B6B6B6B66060
        60F3B684D1F3B6B6B6B6B6B6B6B66D6D6DFFB66DB6FFB6B6B6B6B6B660606080
        F3B6B6B684D1F3B6B6B6B6B66D6D6D6DFFB6B6B66DB6FFB6B6B6B660808060F3
        B6B6B6B6B68484F3B6B6B66D6D6D6DFFB6B6B6B6B66D6DFFB6B6B66084F3F3B6
        B6B6B6B6B6B68484F3B6B66D6DFFFFB6B6B6B6B6B6B66D6DFFB6B6B6B6B6B6B6
        B6B6B6B6B6B6B6B6B6B6B6B6B6B6B6B6B6B6B6B6B6B6B6B6B6B6B6B6B6B6B6B6
        B6B6B6B6B6B6B6B6B6B6B6B6B6B6B6B6B6B6B6B6B6B6B6B6B6B6}
      NumGlyphs = 2
    end
    object FileListView: TListView
      Left = 7
      Top = 3
      Width = 452
      Height = 178
      Hint = 'W'#228'hlen Sie einen Satz als Vorlage aus'
      Columns = <
        item
          Caption = 'Name'
          Width = 190
        end
        item
          Caption = 'Dateiname'
          Width = 120
        end
        item
          Caption = 'Letzte '#196'nderung'
          Width = 115
        end>
      ColumnClick = False
      HideSelection = False
      ReadOnly = True
      RowSelect = True
      SortType = stText
      TabOrder = 10
      ViewStyle = vsReport
      OnSelectItem = FileListViewSelectItem
    end
    object Panel2: TPanel
      Left = 7
      Top = 184
      Width = 453
      Height = 21
      Alignment = taLeftJustify
      BevelOuter = bvNone
      Caption = 'Importierung von:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 0
    end
    object ImportProjects: TExtendCheckBox
      Left = 7
      Top = 207
      Width = 150
      Height = 25
      Hint = 
        'Geben Sie hier an, ob Forschungsprojekte importiert werden solle' +
        'n'
      Alignment = taRightJustify
      BevelOuter = bvNone
      Caption = 'Forschungsprojekte'
      Enabled = True
      TabOrder = 1
      Checked = False
    end
    object ImportItems: TExtendCheckBox
      Left = 159
      Top = 207
      Width = 150
      Height = 25
      Hint = 'Geben Sie hier an, ob Startausr'#252'stung importiert werden sollen'
      Alignment = taRightJustify
      BevelOuter = bvNone
      Caption = 'Startausr'#252'stung'
      Enabled = True
      TabOrder = 2
      Checked = False
    end
    object ImportUFOs: TExtendCheckBox
      Left = 7
      Top = 255
      Width = 149
      Height = 25
      Hint = 'Geben Sie hier an, ob UFOs importiert werden sollen'
      Alignment = taRightJustify
      BevelOuter = bvNone
      Caption = 'UFOs'
      Enabled = True
      TabOrder = 6
      Checked = False
    end
    object ImportSymbols: TExtendCheckBox
      Left = 7
      Top = 231
      Width = 149
      Height = 25
      Hint = 
        'Geben Sie hier an, ob Ausr'#252'stungssymbole importiert werden solle' +
        'n'
      Alignment = taRightJustify
      BevelOuter = bvNone
      Caption = 'Ausr'#252'stungssymbole'
      Enabled = True
      TabOrder = 4
      Checked = False
    end
    object ImportAliens: TExtendCheckBox
      Left = 159
      Top = 255
      Width = 146
      Height = 25
      Hint = 'Geben Sie hier an, ob Aliens importiert werden sollen'
      Alignment = taRightJustify
      BevelOuter = bvNone
      Caption = 'Aliens'
      Enabled = True
      TabOrder = 7
      Checked = False
    end
    object ImportUFOPadie: TExtendCheckBox
      Left = 159
      Top = 231
      Width = 149
      Height = 25
      Hint = 'Geben Sie hier an, ob Personennamen importiert werden sollen'
      Alignment = taRightJustify
      BevelOuter = bvNone
      Caption = 'UFO-P'#228'die Grafiken'
      Enabled = True
      TabOrder = 5
      Checked = False
    end
    object ImportAlienItems: TExtendCheckBox
      Left = 311
      Top = 207
      Width = 150
      Height = 25
      Hint = 'Geben Sie hier an, ob Alienausr'#252'stung importiert werden sollen'
      Alignment = taRightJustify
      BevelOuter = bvNone
      Caption = 'Alienausr'#252'stung'
      Enabled = True
      TabOrder = 3
      Checked = False
    end
    object ImportSkripts: TExtendCheckBox
      Left = 311
      Top = 255
      Width = 150
      Height = 25
      Hint = 'Geben Sie hier an, ob Skripte importiert werden sollen'
      Alignment = taRightJustify
      BevelOuter = bvNone
      Caption = 'Skripte'
      Enabled = True
      TabOrder = 8
      Checked = False
    end
  end
  object Panel3: TPanel
    Left = 0
    Top = 8
    Width = 469
    Height = 49
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    Visible = False
    object Satzname: TExtendEdit
      Left = 7
      Top = 1
      Width = 240
      Height = 22
      Hint = 'Geben Sie hier den Namen des Spielsatzes ein'
      Alignment = taRightJustify
      BevelOuter = bvNone
      BorderWidth = 2
      Caption = 'Satzname:'
      TabOrder = 0
      EditEnabled = True
      LabelWidth = 88
      PasswordChar = #0
      OnChange = SatznameChange
    end
    object ImportBox: TExtendCheckBox
      Left = 249
      Top = 1
      Width = 211
      Height = 25
      Hint = 
        'Geben Sie hier an, ob Daten aus einem vorhandenen Spielsatz impo' +
        'rtiert werden sollen'
      Alignment = taRightJustify
      BevelOuter = bvNone
      Caption = 'Daten importieren'
      Enabled = True
      TabOrder = 1
      Checked = False
      OnChange = ImportBoxChange
    end
    object FileName: TExtendEdit
      Left = 7
      Top = 25
      Width = 240
      Height = 22
      Hint = 'Geben Sie hier den Namen des Spielsatzes ein'
      Alignment = taRightJustify
      BevelOuter = bvNone
      BorderWidth = 2
      Caption = 'Dateiname:'
      TabOrder = 2
      EditEnabled = True
      Einheit = '.pak'
      LabelWidth = 88
      PasswordChar = #0
      OnChange = SatznameChange
    end
  end
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 469
    Height = 8
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 2
  end
  object Timer1: TTimer
    Interval = 50
    Left = 8
    Top = 8
  end
end
