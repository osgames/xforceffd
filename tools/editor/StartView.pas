unit StartView;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ComCtrls, ExtCtrls, Menus, StdCtrls, XForce_types, KD4Utils, ToolWin,
  ExtRecord, {$I language.inc};

type
  TStartForm = class(TFrame)
    PopupMenu1: TPopupMenu;
    N2: TMenuItem;
    Umbenennen2: TMenuItem;
    Ausrstunghinzufgen2: TMenuItem;
    Ausrstunglschen2: TMenuItem;
    EditStart: TMenuItem;
    StartView: TListView;
    N1: TMenuItem;
    Sortierennach1: TMenuItem;
    MenuSName: TMenuItem;
    MenuSTyp: TMenuItem;
    Symbols: TMenuItem;
    Panel2: TPanel;
    Image1: TImage;
    Label1: TLabel;
    N3: TMenuItem;
    Kopieren1: TMenuItem;
    Einfgen1: TMenuItem;
    Ausschneiden1: TMenuItem;
    MenuSAnzahl: TMenuItem;
    FilterBox: TEdit;
    MenuSVer: TMenuItem;
    procedure StartViewCompare(Sender: TObject; Item1, Item2: TListItem;
      Data: Integer; var Compare: Integer);
    procedure StartViewColumnClick(Sender: TObject; Column: TListColumn);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure StartViewEdited(Sender: TObject; Item: TListItem;
      var S: String);
    procedure StartViewDblClick(Sender: TObject);
    procedure SortClick(Sender: TObject);
    procedure PopupMenu1Popup(Sender: TObject);
    procedure SymbolsMeasureItem(Sender: TObject; ACanvas: TCanvas;
      var Width, Height: Integer);
    procedure SymbolsDrawItem(Sender: TObject; ACanvas: TCanvas;
      ARect: TRect; Selected: Boolean);
    procedure SymbolsClick(Sender: TObject);
    procedure FilterBoxChange(Sender: TObject);
  private
    { Private-Deklarationen }
  public
    procedure AddToList(Index: Integer;Select: boolean=false);
    procedure UpdateItem(Rec: TExtRecord; Item: TListItem);
    procedure CreateTree;
    { Public-Deklarationen }
  end;

implementation

uses frmMain, string_utils;

{$R *.DFM}

procedure TStartForm.CreateTree;
var
  Dummy: Integer;
  Text : String;
begin
  StartView.Items.beginUpdate;
  StartView.Items.Clear;
  for Dummy:=0 to Length(MDIForm.Projekts)-1 do
  begin
    if (MDIForm.Projekts[Dummy].GetBoolean('Start')) and (not MDIForm.DeletedChild[Dummy]) then
    begin
      if (FilterBox.Text<>'') then
      begin
        Text:=string_utils_GetLanguageString(MDIForm.Sprache,MDIForm.Projekts[Dummy].GetString('Name'));
        if (Pos(lowercase(FilterBox.Text),LowerCase(Text))=0) then
          continue;
      end;

      if (not MDIForm.Projekts[Dummy].ValueExists('AlienItem')) or (not MDIForm.Projekts[Dummy].GetBoolean('AlienItem')) then
        AddToList(Dummy);
    end;
  end;
  StartView.CustomSort(nil,0);
  StartView.Items.EndUpdate;
end;

procedure TStartForm.StartViewCompare(Sender: TObject; Item1,
  Item2: TListItem; Data: Integer; var Compare: Integer);
begin
  if frmMain.LoadSetStatus then exit;
  if StartView.Tag=0 then
  begin
    Compare:=CompareText(Item1.Caption,Item2.Caption);
  end
  else if StartView.Tag=1 then
  begin
    Compare:=CompareText(Item1.SubItems[0],Item2.SubItems[0]);
  end
  else if StartView.Tag=2 then
    Compare:=MDIForm.Projekts[Integer(Item1.Data)].GetInteger('Anzahl')-MDIForm.Projekts[Integer(Item2.Data)].GetInteger('Anzahl')
  else if StartView.Tag=3 then
    Compare:=MDIForm.Projekts[Integer(Item1.Data)].GetInteger('Ver')-MDIForm.Projekts[Integer(Item2.Data)].GetInteger('Ver')
end;

procedure TStartForm.StartViewColumnClick(Sender: TObject;
  Column: TListColumn);
begin
  StartView.Tag:=Column.Index;
  StartView.CustomSort(nil,0);
end;

procedure TStartForm.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  Action:=caFree;
  MDIForm.StartViewer:=nil;
end;

procedure TStartForm.StartViewEdited(Sender: TObject; Item: TListItem;
  var S: String);
var
  Dummy: Integer;
  Index: Integer;
  Text : String;
  ID   : Cardinal;
begin
  Index:=Integer(Item.Data);
  ID:=MDIForm.Projekts[Index].GetCardinal('ID');
  for Dummy:=0 to length(MDIForm.Projekts)-1 do
  begin
    if (MDIForm.Projekts[Dummy].GetString('Name')=S) and (MDIForm.Projekts[Dummy].GetCardinal('ID')<>ID) then
    begin
      Text:=Format('%s ist bereits definiert',[S]);
      Application.MessageBox(PChar(Text),PChar('Hinweis'),MB_ICONINFORMATION or MB_OK);
      S:=MDIForm.Projekts[Index].GetString('Name');
      exit;
    end;
  end;
  MDIForm.Projekts[Index].SetString('Name',string_utils_SetLanguageString(MDIForm.Sprache,MDIForm.Projekts[Index].GetString('Name'),S));
  MDIForm.ForschChanged:=true;
  StartView.CustomSort(nil,0);
end;

procedure TStartForm.StartViewDblClick(Sender: TObject);
begin
  MDIForm.ActionItemEdit.Execute;
end;

procedure TStartForm.SortClick(Sender: TObject);
begin
  StartView.Tag:=(Sender as TComponent).Tag;
  StartView.CustomSort(nil,0);
end;

procedure TStartForm.PopupMenu1Popup(Sender: TObject);
var
  Dummy    : Integer;
  Item     : TMenuItem;
  Break    : Integer;
  Mods     : Integer;
  Bis      : Integer;
  Row      : Integer;
  Image    : Integer;
  ItemIndex: Integer;
begin
  Symbols.Enabled:=(StartView.Selected<>nil);
  if not Symbols.Enabled then exit;
  if not MDIForm.Projekts[Integer(StartView.Selected.Data)].ValueExists('ImageIndex') then Symbols.Enabled:=false;
  if not Symbols.Enabled then exit;
  Symbols.Clear;
  for Dummy:=0 to MDIForm.WaffenListe.Count-1 do
  begin
    Item:=TMenuItem.Create(Symbols);
    Item.OnMeasureItem:=SymbolsMeasureItem;
    Item.OnDrawItem:=SymbolsDrawItem;
    Item.ImageIndex:=Dummy;
    Item.Hint:=Symbols.Hint;
    Item.OnClick:=SymbolsClick;
    Symbols.Add(Item);
  end;
  Break:=MDIForm.WaffenListe.Count div 5;
  Mods:=MDIForm.WaffenListe.Count mod 5;
  ItemIndex:=0;
  for Dummy:=0 to 4 do
  begin
    Bis:=Break;
    Image:=Dummy;
    if Dummy<Mods then inc(Bis);
    for Row:=0 to Bis-1 do
    begin
      Symbols[ItemIndex].ImageIndex:=Image;
      inc(Image,5);
      inc(ItemIndex);
    end;
    if ItemIndex<Symbols.Count then
    Symbols[ItemIndex].Break:=mbBreak;
  end;
end;

procedure TStartForm.SymbolsMeasureItem(Sender: TObject; ACanvas: TCanvas;
  var Width, Height: Integer);
begin
  Width:=24;
  Height:=36;
end;

procedure TStartForm.SymbolsDrawItem(Sender: TObject; ACanvas: TCanvas;
  ARect: TRect; Selected: Boolean);
begin
  ARect:=Rect(ARect.Left,ARect.Top,ARect.Left+36,ARect.Top+36);
  ACanvas.Brush.Color:=clBtnFace;
  if (Sender as TMenuItem).ImageIndex=MDIForm.Projekts[Integer(StartView.Selected.Data)].GetInteger('ImageIndex') then
  begin
    if not Selected then
      ACanvas.Brush.Bitmap := AllocPatternBitmap(clBtnFace, clBtnHighlight);
    ACanvas.FillRect(ARect);
    MDIForm.WaffenListe.Draw(ACanvas,ARect.Left+2,Arect.Top+2,(Sender as TMenuItem).ImageIndex);
    Frame3D(ACanvas,ARect,clBtnShadow,clBtnHighlight,1);
  end
  else
  begin
    ACanvas.FillRect(ARect);
    MDIForm.WaffenListe.Draw(ACanvas,ARect.Left+2,Arect.Top+2,(Sender as TMenuItem).ImageIndex);
    if Selected then Frame3D(ACanvas,ARect,clBtnHighlight,clBtnShadow,1)
  end;
end;

procedure TStartForm.SymbolsClick(Sender: TObject);
begin
  if MDIForm.Projekts[Integer(StartView.Selected.Data)].GetInteger('ImageIndex')<>(Sender as TMenuItem).ImageIndex then
  begin
    MDIForm.Projekts[Integer(StartView.Selected.Data)].SetInteger('ImageIndex',(Sender as TMenuItem).ImageIndex);
    StartView.Selected.ImageIndex:=(Sender as TMenuItem).ImageIndex;
    MDIForm.ForschChanged:=true;
  end;
end;

procedure TStartForm.AddToList(Index: Integer;Select: boolean);
var
  Item: TListItem;
begin
  Item:=StartView.Items.Add;
  Item.Data:=Pointer(Index);
  UpdateItem(MDIForm.Projekts[Index],Item);

  if Select then
    Item.Selected:=true;
end;

procedure TStartForm.UpdateItem(Rec: TExtRecord; Item: TListItem);
begin
  Item.Caption:=string_utils_GetLanguageString(MDIForm.Sprache,Rec.GetString('Name'));
  if Rec.ValueExists('ImageIndex') then
    Item.ImageIndex:=Rec.GetInteger('ImageIndex')
  else
    Item.ImageIndex:=-1;

  Item.SubItems.Clear;
  Item.SubItems.Add(Rec.RecordDefinition.RecordName);
  Item.SubItems.Add(IntToStr(Rec.GetInteger('Anzahl')));
  if Rec.GetInteger('Ver')=0 then
    Item.SubItems.Add('Anfang')
  else
    Item.SubItems.Add(IntToStr(Rec.GetInteger('Ver')));
end;

procedure TStartForm.FilterBoxChange(Sender: TObject);
begin
  CreateTree;
end;

end.
