unit MissionSkriptViews;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ComCtrls, StdCtrls, ExtCtrls, Menus, ExtRecord, string_utils;

type
  TMissionSkripts = class(TFrame)
    Panel2: TPanel;
    Image1: TImage;
    Label1: TLabel;
    MissionsSkriptsList: TListView;
    PopupMenu1: TPopupMenu;
    Skripthinzufgen1: TMenuItem;
    Skriptlschen1: TMenuItem;
    ActionSkriptsEdit1: TMenuItem;
    N1: TMenuItem;
    Umbenennen1: TMenuItem;
    N2: TMenuItem;
    Kopieren1: TMenuItem;
    Kopieren2: TMenuItem;
    Ausschneiden1: TMenuItem;
    FilterBox: TEdit;
    procedure MissionsSkriptsListEdited(Sender: TObject; Item: TListItem;
      var S: String);
    procedure MissionsSkriptsListDblClick(Sender: TObject);
    procedure MissionsSkriptsListCompare(Sender: TObject; Item1,
      Item2: TListItem; Data: Integer; var Compare: Integer);
    procedure MissionsSkriptsListColumnClick(Sender: TObject;
      Column: TListColumn);
    procedure FilterBoxChange(Sender: TObject);
  private
    { Private-Deklarationen }
  public
    procedure CreateTree;
    procedure AddSkript(Rec: TExtRecord; Index: Integer);
    procedure SkriptToListItem(Rec: TExtRecord; Item: TListItem);
    { Public-Deklarationen }
  end;

implementation

{$R *.DFM}

uses frmMain;
{ TMissionSkripts }

procedure TMissionSkripts.AddSkript(Rec: TExtRecord; Index: Integer);
var
  Item: TListItem;
begin
  Item:=MissionsSkriptsList.Items.Add;
  SkriptToListItem(Rec,Item);
  Item.Data:=Pointer(Index);
end;

procedure TMissionSkripts.CreateTree;
var
  Dummy: Integer;
begin
  MissionsSkriptsList.Items.BeginUpdate;
  MissionsSkriptsList.Items.Clear;
  for Dummy:=0 to high(MDIForm.Skripte) do
  begin
    if (FilterBox.Text<>'') then
    begin
      Text:=string_utils_GetLanguageString(MDIForm.Sprache,MDIForm.Skripte[Dummy].GetString('Name'));
      if (Pos(lowercase(FilterBox.Text),LowerCase(Text))=0) then
        continue;
    end;

    AddSkript(MDIForm.Skripte[Dummy],Dummy);
  end;
  MissionsSkriptsList.CustomSort(nil,0);
  MissionsSkriptsList.Items.EndUpdate;
end;

procedure TMissionSkripts.MissionsSkriptsListEdited(Sender: TObject;
  Item: TListItem; var S: String);
var
  Index: Integer;
begin
  Index:=Integer(Item.Data);
  MDIForm.Skripte[Index].SetString('Name',string_utils_SetLanguageString(MDIForm.Sprache,MDIForm.Skripte[Index].GetString('Name'),S));
  MDIForm.ForschChanged:=true;
  MissionsSkriptsList.CustomSort(nil,0);
end;

procedure TMissionSkripts.MissionsSkriptsListDblClick(Sender: TObject);
begin
  MDIForm.ActionSkriptEdit.Execute
end;

procedure TMissionSkripts.SkriptToListItem(Rec: TExtRecord;
  Item: TListItem);
begin
  Item.Caption:=string_utils_GetLanguageString(MDIForm.Sprache,Rec.GetString('Name'));
  Item.SubItems.Clear;
  case Rec.GetInteger('Typ') of
    0: Item.SubItems.Add('manuell');
    1: Item.SubItems.Add('zuf�llig');
    2: Item.SubItems.Add('einmalig');
  else
    Assert(false);
  end;
end;

procedure TMissionSkripts.MissionsSkriptsListCompare(Sender: TObject;
  Item1, Item2: TListItem; Data: Integer; var Compare: Integer);
begin
  if frmMain.LoadSetStatus then
    exit;
  if MissionsSkriptsList.Tag=0 then
  begin
    Compare:=CompareText(Item1.Caption,Item2.Caption);
  end
  else
    Compare:=CompareText(Item1.SubItems[MissionsSkriptsList.Tag-1],Item2.SubItems[MissionsSkriptsList.Tag-1]);
end;

procedure TMissionSkripts.MissionsSkriptsListColumnClick(Sender: TObject;
  Column: TListColumn);
begin
  MissionsSkriptsList.Tag:=Column.Index;
  MissionsSkriptsList.CustomSort(nil,0);
end;

procedure TMissionSkripts.FilterBoxChange(Sender: TObject);
begin
  CreateTree;
end;

end.
