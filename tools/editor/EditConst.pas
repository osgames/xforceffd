unit EditConst;

interface

const

  MEmptyField       = 'Bitte geben Sie einen Wert ein.';
  MValueAlreadyUsed = 'Der Wert wird bereits genutzt. Bitte geben Sie einen anderen Wert ein.';
  MValueToLarge     = 'Bitte geben Sie einen Wert ein, der kleiner oder gleich %.0n ist.';
  MValueNotNull     = 'Bitte geben Sie einen Wert ein, der gr�sser 0 ist.';
  MValueInvLength   = 'Der Wert darf maximal eine Nachkommastelle haben.';
  MValueToSmall     = 'Bitte geben Sie einen Wert ein, der gr�sser oder gleich %.0n ist.';
  MVerkauftoLarge   = 'Bitte geben Sie einen Wert ein, der gr�sser als der Kaufpreis ist.';
  MPercentToLarge   = 'Bitte geben Sie einen Wert ein, der kleiner oder gleich 100 % ist.';
  MPercentToSmall   = 'Bitte geben Sie einen Wert ein, der gr�sser oder gleich 0 % ist.';
  MTooManyAliens    = 'Es d�rfen nicht mehr als 50 Aliens an Bord sein.';

  CErrorAssist      = 'Fehlerassistent';

{ Fehlernummer }

  ENKeineWaffe       =   0; // Munition keiner Waffe zugeordnet
  ENLaserWaffe       =   1; // Munition ist einer Laserwaffe zugeordnet
  ENDifferentTyp     =   2; // Unterschiedliche Typen
  ENNoStrength       =   3; // Keine St�rke angegeben
  ENNoRelation       =   4; // Keine Abh�ngigkeit bei Waffe und Munition
  ENNoStartWaffe     =   5; // Keine Startwaffe
  ENNoInfo           =   6; // Keine Info zu Objekt
  ENNoWaffenStrength =   7; // Bei einer Waffe ohne Munition ist keine St�rke angegeben

  ENNoImage          =   8; // Kein UFOP�die Bild
  ENInvalidImage     =   9; // Ung�ltiges UFOP�die Bild
  ENInvalidSymbol    =  10; // Ung�ltiges Symbol
  ENNoResearchInfo   =  11; // Keine Forschungs-Info zum Objekt

  ENNoSound          =  12; // Kein Sound zugewiesen
  ENInvalidSound     =  13; // Ung�ltiger Sound

  ENMaxProjektErrors =  25; // H�chste Fehlernummer f�r Ausr�stung

  EAlienNoWeapon     = 100; // Alien kann keine Waffe benutzen
  EUFONotAttacking   = 101; // UFO hat eine Wahrscheinlichkeit von 0

  EInvalidScript     = 200; // Compilefehler in einem Skript


implementation

end.
