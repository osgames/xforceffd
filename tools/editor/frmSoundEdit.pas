unit frmSoundEdit;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, ExtCtrls, ComCtrls, ArchivFile, Defines;

type
  TSoundEdit = class(TForm)
    ListView1: TListView;
    OKButton: TBitBtn;
    BitBtn2: TBitBtn;
    BitBtn1: TBitBtn;
    DeleteBitmap: TBitBtn;
    OpenDialog1: TOpenDialog;
    NoImageButton: TBitBtn;
    PlaySound: TBitBtn;
    procedure FormShow(Sender: TObject);
    procedure ListView1SelectItem(Sender: TObject; Item: TListItem;
      Selected: Boolean);
    procedure OKButtonClick(Sender: TObject);
    procedure ListView1DblClick(Sender: TObject);
    procedure DeleteBitmapClick(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure NoImageButtonClick(Sender: TObject);
    procedure PlaySoundClick(Sender: TObject);
  private
    fSoundName       : String;
    { Private-Deklarationen }
  public
    property SoundName: String read fSoundName write fSoundName;
    { Public-Deklarationen }
  end;

var
  SoundEdit: TSoundEdit;

implementation

uses frmMain, archiv_utils, MMSystem;

{$R *.DFM}

procedure TSoundEdit.FormShow(Sender: TObject);
var
  Archiv   : TArchivFile;
  SelOrt   : String;
  SelRess  : String;

  procedure ReadArchiv(ViewOrt: String;Ort: String);
  var
    Dummy: Integer;
    Name : String;
  begin
    for Dummy:=0 to Archiv.Count-1 do
    begin
      Name:=Archiv.Files[Dummy].Name;
      if Copy(Name,1,Pos(':',Name)-1)='Sound' then
      begin
        with ListView1.Items.Add do
        begin
          Caption:=Copy(Name,Pos(':',Name)+1,length(Name));
          SubItems.Add(ViewOrt);
          SubItems.Add(Ort);
          if (Ort=SelOrt) and (SelRess=Copy(Name,Pos(':',Name)+1,100)) then
            Selected:=true;
	end;
      end;
    end;
  end;

begin
  SelOrt:=Copy(fSoundName,1,Pos('\',fSoundName)-1);
  SelRess:=Copy(fSoundName,Pos(':',fSoundName)+1,length(fSoundName));

  ListView1.Items.Clear;
  Archiv:=TArchivFile.Create;
  Archiv.OpenArchiv(FSoundFile,true);
  ReadArchiv('Standard','default');
  Archiv.OpenArchiv('data\GameSets\'+MDIForm.Filename,true);
  ReadArchiv('Eigenes','user');
  Archiv.Free;
  ListView1.SetFocus;

  PlaySound.Enabled:=false;
end;

procedure TSoundEdit.ListView1SelectItem(Sender: TObject;
  Item: TListItem; Selected: Boolean);
begin
  PlaySound.Enabled:=Selected;
  DeleteBitmap.Enabled:=Selected and (Item.SubItems[1]='user');
end;

procedure TSoundEdit.OKButtonClick(Sender: TObject);
begin
  if ListView1.Selected=nil then
    fSoundName:=''
  else
    fSoundName:=ListView1.Selected.SubItems[1]+'\Sound:'+ListView1.Selected.Caption;

end;

procedure TSoundEdit.ListView1DblClick(Sender: TObject);
begin
  if ListView1.Selected<>nil then
    OKButton.Click;
end;

procedure TSoundEdit.DeleteBitmapClick(Sender: TObject);
var
  Archiv: TArchivFile;
begin
  if MessageDlg('M�chten Sie den Sound wirklich aus dem Spielsatz l�schen? Diese �nderung wird sofort gespeichert und kann nicht R�ckg�ngig gemacht werden!',mtConfirmation,[mbYes,mbNo],0)=mryes then
  begin
    Archiv:=TArchivFile.Create;
    Archiv.OpenArchiv('data\gamesets\'+MDIForm.Filename);
    Archiv.DeleteRessource('Sound:'+ListView1.Selected.Caption);
    ListView1.Selected.Free;
    Archiv.Free;
  end;
end;

procedure TSoundEdit.BitBtn1Click(Sender: TObject);
var
  Archiv: TArchivFile;
  Mem   : TMemoryStream;
  Name  : String;
  Dummy : Integer;
  FileN : String;
begin
  if OpenDialog1.Execute then
  begin
    for Dummy:=0 to OpenDialog1.Files.Count-1 do
    begin
      Mem:=TMemoryStream.Create;
      try
        FileN:=OpenDialog1.Files[Dummy];

        Mem.LoadFromFile(FileN);

        Name:=ChangeFileExt(ExtractFileName(FileN),'');
        if not InputQuery('Soundname f�r '+FileN,'Soundname:',Name) then
        begin
          Mem.Free;
          break;
        end;

        Archiv:=TArchivFile.Create;
        Archiv.OpenArchiv('data\gamesets\'+MDIForm.Filename);
        Archiv.AddRessource(Mem,'Sound:'+Name,true);
        with ListView1.Items.Add do
        begin
          Caption:=Name;
          SubItems.Add('Eigenes');
          SubItems.Add('user');
        end;
        Archiv.Free;
      except
        on E: Exception do
        begin
          MessageDlg(Format('Der Sound %s konnte nicht hinzugef�gt werden:'#13#10'%s',[ExtractFileName(FileN),E.Message]),mtError,[mbOK],0);
        end;
      end;
      Mem.Free;
    end;
  end;
end;

procedure TSoundEdit.NoImageButtonClick(Sender: TObject);
begin
  ListView1.Selected:=nil;
end;

procedure TSoundEdit.PlaySoundClick(Sender: TObject);
var
  Archiv: TArchivFile;
  Item  : TListItem;
begin
  Archiv:=TArchivFile.Create;

  Item:=ListView1.Selected;

  if Item.SubItems[1]='default' then
    Archiv.OpenArchiv(FSoundFile,true)
  else
    Archiv.OpenArchiv('data\gamesets\'+MDIForm.Filename,true);

  Archiv.OpenRessource('Sound:'+Item.Caption);

  sndPlaySound(Archiv.Stream.Memory,SND_MEMORY or SND_SYNC);
  Archiv.Free;
end;

end.
