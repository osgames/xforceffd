unit SkriptEdit;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, JclShell, JClFileUtils;

type
  TfrmSkriptEditor = class(TForm)
    SkriptMemo: TMemo;
    OKButton: TBitBtn;
    BitBtn2: TBitBtn;
    ImportButton: TButton;
    Button1: TButton;
    SaveButton: TButton;
    OpenDialog1: TOpenDialog;
    SaveDialog1: TSaveDialog;
    Warning: TLabel;
    procedure ImportButtonClick(Sender: TObject);
    procedure SaveButtonClick(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure SkriptMemoKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
  private
    function GetSkript: String;
    procedure SetSkript(const Value: String);
    { Private-Deklarationen }
  public
    property Skript: String read GetSkript write SetSkript;
    { Public-Deklarationen }
  end;

var
  frmSkriptEditor: TfrmSkriptEditor;

implementation

uses Math;

{$R *.DFM}

function TfrmSkriptEditor.GetSkript: String;
begin
  result:=SkriptMemo.Lines.Text;
end;

procedure TfrmSkriptEditor.SetSkript(const Value: String);
begin
  SkriptMemo.Lines.Text:=Value;
end;

procedure TfrmSkriptEditor.ImportButtonClick(Sender: TObject);
begin
  if OpenDialog1.Execute then
    SkriptMemo.Lines.LoadFromFile(OpenDialog1.FileName);
end;

procedure TfrmSkriptEditor.SaveButtonClick(Sender: TObject);
begin
  if SaveDialog1.Execute then
    SkriptMemo.Lines.SaveToFile(SaveDialog1.FileName);
end;

procedure TfrmSkriptEditor.Button1Click(Sender: TObject);
var
  tmpFile: String;

  procedure ChangeEnableOfTButton(Enabled: Boolean);
  var
    Dummy  : Integer;
  begin
    for Dummy:=0 to ComponentCount-1 do
    begin
      if Components[Dummy] is TButton then
        TButton(Components[Dummy]).Enabled:=Enabled;
    end;
    Warning.Visible:=not Enabled;
  end;

begin
  tmpFile:=FileGetTempName('mission');

  SkriptMemo.Lines.SaveToFile(tmpFile);

  ChangeEnableOfTButton(false);

  ShellExecAndWait('medit.exe',tmpFile,'open',SW_SHOWNORMAL);

  ChangeEnableOfTButton(true);

  SkriptMemo.Lines.LoadFromFile(tmpFile);

  DeleteFile(tmpFile);
end;

procedure TfrmSkriptEditor.SkriptMemoKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
  if Key=VK_ESCAPE then
    BitBtn2.Click; 
end;

end.
