unit SelectProjekt;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  frmMain, ComCtrls, ExtCtrls, ProjektView, Buttons, StdCtrls;

type
  TfrmSelectList = class(TForm)
    ProjektList: TListView;
    OKButton: TBitBtn;
    CancelButton: TBitBtn;
    Label1: TLabel;
    FilterBox: TEdit;
    procedure FormShow(Sender: TObject);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure OKButtonClick(Sender: TObject);
    procedure CancelButtonClick(Sender: TObject);
    procedure ProjektListSelectItem(Sender: TObject; Item: TListItem;
      Selected: Boolean);
    procedure ProjektListDblClick(Sender: TObject);
    procedure ProjektListColumnClick(Sender: TObject; Column: TListColumn);
    procedure FilterBoxChange(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    fSelectedProjekt: Cardinal;
    function GetSelectedProjekt: Integer;
    function GetSelectedAlien: Integer;

    procedure RebuildProjektList;
    { Private-Deklarationen }
  public
    property Projekt: Integer read GetSelectedProjekt;
    property Alien: Integer read GetSelectedAlien;
    property SelectProjekt: Cardinal read fSelectedProjekt write fSelectedProjekt;
    { Public-Deklarationen }
  end;

var
  frmSelectList: TfrmSelectList;

implementation

uses
  string_utils;

{$R *.DFM}

procedure TfrmSelectList.FormShow(Sender: TObject);
begin
  FilterBox.Text:='';

  RebuildProjektList;
end;

procedure TfrmSelectList.FormKeyPress(Sender: TObject; var Key: Char);
begin
  if Key=#13 then
    OKButton.Click
  else if Key=#27 then
    CancelButton.Click;
end;

procedure TfrmSelectList.OKButtonClick(Sender: TObject);
begin
  ModalResult:=mrOK;
end;

procedure TfrmSelectList.CancelButtonClick(Sender: TObject);
begin
  ModalResult:=mrCancel;          
end;

procedure TfrmSelectList.ProjektListSelectItem(Sender: TObject;
  Item: TListItem; Selected: Boolean);
begin
  OKButton.Enabled:=Selected;
end;

function TfrmSelectList.GetSelectedProjekt: Integer;
begin
  result:=-1;
  if ProjektList.Selected=nil then exit;
  if ProjektList.Selected.SubItems[2]='Alien' then
    exit;
  result:=Integer(ProjektList.Selected.Data);
end;

procedure TfrmSelectList.ProjektListDblClick(Sender: TObject);
begin
  OKButton.Click;
end;

function TfrmSelectList.GetSelectedAlien: Integer;
begin
  result:=-1;
  if ProjektList.Selected=nil then exit;
  if ProjektList.Selected.SubItems[2]='Item' then
    exit;
  result:=Integer(ProjektList.Selected.Data);
end;

procedure TfrmSelectList.ProjektListColumnClick(Sender: TObject;
  Column: TListColumn);
begin
  ProjektList.Tag:=Column.Index;
  ProjektList.CustomSort(nil,0);
end;

procedure TfrmSelectList.RebuildProjektList;
var
  Dummy: Integer;
  Text : String;
begin
  ProjektList.SmallImages:=MDIForm.WaffenListe.SmallImageList;
  ProjektList.Items.BeginUpdate;
  ProjektList.Items.Clear;
  for Dummy:=0 to high(MDIForm.Projekts) do
  begin
    Text:=LowerCase(string_utils_GetLanguageString(MDIForm.Sprache,MDIForm.Projekts[Dummy].GetString('Name')));
    if (FilterBox.Text='') or (Pos(lowercase(FilterBox.Text),Text)<>0) then

    begin
      if ((not MDIForm.DeletedChild[Dummy]) and (not MDIForm.Projekts[Dummy].GetBoolean('Start'))) then
        MDIForm.ProjektViewer.AddToList(Dummy,ProjektList)
      else if (MDIForm.Projekts[Dummy].ValueExists('AlienItem') and (MDIForm.Projekts[Dummy].GetBoolean('AlienItem'))) then
        MDIForm.ProjektViewer.AddToList(Dummy,ProjektList);
    end;
  end;

  for Dummy:=0 to high(MDIForm.Aliens) do
  begin
    Text:=LowerCase(string_utils_GetLanguageString(MDIForm.Sprache,MDIForm.Aliens[Dummy].GetString('Name'))+' - Autopsie');
    if (FilterBox.Text='') or (Pos(lowercase(FilterBox.Text),LowerCase(Text))<>0) then
      MDIForm.ProjektViewer.AddAlienToList(Dummy,ProjektList);
  end;

  for Dummy:=0 to ProjektList.Items.Count-1 do
  begin
    if ProjektList.Items[Dummy].SubItems[2]='Alien' then
    begin
      if MDIForm.Aliens[Integer(ProjektList.Items[Dummy].Data)].GetCardinal('ID')=fSelectedProjekt then
      begin
        ProjektList.Selected:=ProjektList.Items[Dummy];
        break;
      end;
    end
    else
    begin
      if MDIForm.Projekts[Integer(ProjektList.Items[Dummy].Data)].GetCardinal('ID')=fSelectedProjekt then
      begin
        ProjektList.Selected:=ProjektList.Items[Dummy];
        break;
      end;
    end;
  end;

  fSelectedProjekt:=0;
  OKButton.Enabled:=ProjektList.Selected<>nil;

  if ProjektList.Selected<>nil then
    ProjektList.Selected.MakeVisible(true);

  ProjektList.Items.EndUpdate;
end;

procedure TfrmSelectList.FilterBoxChange(Sender: TObject);
begin
  RebuildProjektList;
end;

procedure TfrmSelectList.FormCreate(Sender: TObject);
begin
  ProjektList.OnCompare:=MDIForm.ProjektViewer.AllProjektsCompare;
end;

end.
