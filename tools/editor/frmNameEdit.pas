unit frmNameEdit;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, Grids;

type
  TNameEdit = class(TForm)
    LanguageGrid: TStringGrid;
    OKButton: TBitBtn;
    CancelButton: TBitBtn;
    procedure OKButtonClick(Sender: TObject);
    procedure LanguageGridKeyPress(Sender: TObject; var Key: Char);
  private
    fText: String;
    procedure SetText(const Value: String);
    { Private-Deklarationen }
  public
    property Text: String read fText write SetText;
    { Public-Deklarationen }
  end;

var
  NameEdit: TNameEdit;

implementation

uses frmMain,string_utils;

{$R *.DFM}

{ TNameEdit }

procedure TNameEdit.SetText(const Value: String);
var
  Dummy: Integer;
begin
  fText := Value;
  LanguageGrid.RowCount:=length(MDIForm.Sprachen);
  for Dummy:=0 to high(MDIForm.Sprachen) do
  begin
    LanguageGrid.Cells[0,Dummy]:=MDIForm.Sprachen[Dummy];
    LanguageGrid.Cells[1,Dummy]:=string_utils_GetLanguageString(MDIForm.Sprachen[Dummy],Value);
  end;
end;

procedure TNameEdit.OKButtonClick(Sender: TObject);
var
  Dummy: Integer;
begin
  fText:='';
  for Dummy:=0 to LanguageGrid.RowCount-1 do
  begin
    fText:=string_utils_SetLanguageString(LanguageGrid.Cells[0,Dummy],fText,LanguageGrid.Cells[1,Dummy]);
  end;
end;

procedure TNameEdit.LanguageGridKeyPress(Sender: TObject; var Key: Char);
begin
  if Key = #13 then
    OKButton.Click
  else if Key = #27 then
    CancelButton.Click;
end;

end.
