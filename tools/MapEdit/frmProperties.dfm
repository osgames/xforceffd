object Settings: TSettings
  Left = 325
  Top = 374
  BorderStyle = bsDialog
  Caption = 'Karten-Eigenschaften'
  ClientHeight = 94
  ClientWidth = 236
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poMainFormCenter
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  PixelsPerInch = 96
  TextHeight = 13
  object MapNameLabel: TLabel
    Left = 8
    Top = 4
    Width = 62
    Height = 13
    Caption = 'Raum-Name:'
  end
  object MapNameEdit: TEdit
    Left = 8
    Top = 24
    Width = 217
    Height = 21
    MaxLength = 30
    TabOrder = 0
    Text = 'MapNameEdit'
  end
  object BitBtn1: TBitBtn
    Left = 40
    Top = 56
    Width = 89
    Height = 25
    TabOrder = 1
    Kind = bkOK
  end
  object BitBtn2: TBitBtn
    Left = 136
    Top = 56
    Width = 89
    Height = 25
    TabOrder = 2
    Kind = bkCancel
  end
end
