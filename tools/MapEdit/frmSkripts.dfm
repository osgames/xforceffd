object FromSkripts: TFromSkripts
  Left = 184
  Top = 159
  BorderStyle = bsDialog
  Caption = 'eingebundene Skripte'
  ClientHeight = 500
  ClientWidth = 639
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Skripts: TListBox
    Left = 8
    Top = 8
    Width = 623
    Height = 193
    Anchors = [akLeft, akTop, akRight]
    ItemHeight = 13
    TabOrder = 0
    OnClick = SkriptsClick
  end
  object SkriptMemo: TMemo
    Left = 8
    Top = 208
    Width = 623
    Height = 250
    Anchors = [akLeft, akTop, akRight, akBottom]
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Courier New'
    Font.Style = []
    ParentFont = False
    ReadOnly = True
    ScrollBars = ssBoth
    TabOrder = 1
  end
  object Button2: TButton
    Left = 8
    Top = 463
    Width = 105
    Height = 25
    Anchors = [akLeft, akBottom]
    Caption = 'Skript hinzuf�gen'
    TabOrder = 2
    OnClick = Button2Click
  end
  object DeleteSkript: TButton
    Left = 120
    Top = 463
    Width = 105
    Height = 25
    Anchors = [akLeft, akBottom]
    Caption = 'Skript l�schen'
    Enabled = False
    TabOrder = 3
    OnClick = DeleteSkriptClick
  end
  object Button1: TButton
    Left = 526
    Top = 463
    Width = 105
    Height = 25
    Anchors = [akRight, akBottom]
    Caption = 'Schliessen'
    ModalResult = 1
    TabOrder = 4
  end
  object SaveSkript: TButton
    Left = 232
    Top = 463
    Width = 105
    Height = 25
    Anchors = [akLeft, akBottom]
    Caption = 'Skript speichern'
    Enabled = False
    TabOrder = 5
    OnClick = SaveSkriptClick
  end
  object OpenSkript: TOpenDialog
    DefaultExt = 'xma'
    Filter = 'Karten-Skript (*.xma)|*.xma'
    InitialDir = 'data\scripts\maps'
    Options = [ofHideReadOnly, ofPathMustExist, ofFileMustExist, ofEnableSizing]
    Left = 16
    Top = 8
  end
  object SaveSkriptDialog: TSaveDialog
    DefaultExt = 'xma'
    Filter = 'Karten-Skript (*.xma)|*.xma'
    InitialDir = 'data\scripts\maps'
    Left = 48
    Top = 8
  end
end
