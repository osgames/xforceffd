unit frmSkripts;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls;

type
  TFromSkripts = class(TForm)
    Skripts: TListBox;
    SkriptMemo: TMemo;
    Button2: TButton;
    DeleteSkript: TButton;
    Button1: TButton;
    SaveSkript: TButton;
    OpenSkript: TOpenDialog;
    SaveSkriptDialog: TSaveDialog;
    procedure Button2Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure SkriptsClick(Sender: TObject);
    procedure DeleteSkriptClick(Sender: TObject);
    procedure SaveSkriptClick(Sender: TObject);
  private
    procedure RefreshSkriptList;
    { Private-Deklarationen }
  public
    { Public-Deklarationen }
  end;

var
  FromSkripts: TFromSkripts;

implementation

uses frmMain;

{$R *.DFM}

procedure TFromSkripts.Button2Click(Sender: TObject);
var
  list: TStringList;
begin
  if OpenSkript.Execute then
  begin
    SetLength(Main.Skripts,length(Main.Skripts)+1);
    with Main.Skripts[high(Main.Skripts)] do
    begin
      Name:=ExtractFileName(OpenSkript.FileName);
      List:=TStringList.Create;
      List.LoadFromFile(OpenSkript.FileName);
      Skript:=List.Text;
      List.Free;
      Main.IsoKDMap1.Modify:=true;
    end;
  end;
  RefreshSkriptList;
end;

procedure TFromSkripts.FormShow(Sender: TObject);
begin
  RefreshSkriptList;
end;

procedure TFromSkripts.RefreshSkriptList;
var
  Dummy: Integer;
begin
  Skripts.Items.Clear;
  for Dummy:=0 to high(Main.Skripts) do
  begin
    Skripts.Items.Add(Main.Skripts[Dummy].Name);
  end;
  Skripts.ItemIndex:=0;
  SkriptsClick(Self);

  DeleteSkript.Enabled:=Skripts.Items.Count>0;
  SaveSkript.Enabled:=Skripts.Items.Count>0;
end;

procedure TFromSkripts.SkriptsClick(Sender: TObject);
begin
  if Skripts.ItemIndex=-1 then
  begin
    SkriptMemo.Lines.Clear;
    exit;
  end;

  SkriptMemo.Lines.Text:=Main.Skripts[Skripts.ItemIndex].Skript;
end;

procedure TFromSkripts.DeleteSkriptClick(Sender: TObject);
var
  Dummy: integer;
begin
  for Dummy:=Skripts.ItemIndex to high(Main.Skripts)-1 do
  begin
    Main.Skripts[Dummy]:=Main.Skripts[Dummy+1];
  end;
  SetLength(Main.Skripts,length(Main.Skripts)-1);

  RefreshSkriptList;
  Main.IsoKDMap1.Modify:=true;
end;

procedure TFromSkripts.SaveSkriptClick(Sender: TObject);
var
  List: TStringList;
begin
  if SaveSkriptDialog.Execute then
  begin
    SkriptMemo.Lines.SaveToFile(SaveSkriptDialog.FileName);
  end;
 end;

end.
