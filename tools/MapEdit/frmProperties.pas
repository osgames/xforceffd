unit frmProperties;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, xforce_types;

type
  TSettings = class(TForm)
    MapNameLabel: TLabel;
    MapNameEdit: TEdit;
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
  private
    { Private-Deklarationen }
  public
    Files: TStringList;
    { Public-Deklarationen }
  end;

var
  Settings: TSettings;

implementation

uses frmMain;

{$R *.DFM}

procedure TSettings.FormCreate(Sender: TObject);
begin
  Files:=TStringList.Create;
end;

procedure TSettings.FormDestroy(Sender: TObject);
begin
  Files.Free;
end;

end.
