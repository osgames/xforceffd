unit frmMain;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  DXDraws,DXClass, IsoKDMap, ComCtrls, StdCtrls, ExtCtrls, DIB, xforce_types,
  Buttons, DXInput, ImgList, ToolWin, Menus, ActnList, Spin, JcLFileUtils,
  TraceFile, Map_Utils, Registry, TileGroup, JvComponent, JvChangeNotify,
  JvComponentBase, TB2Item, TBX, TB2Dock, TB2Toolbar, TBXDkPanels,{$i language.inc};

type
  TPage = (pFloor,pWallTL,pWallTR,pWallBL,pWallBR,pObject);

  TSkriptInfo = record
    Name   : String;
    Skript : String;
  end;

  TTileInfo = record
    ID       : Cardinal;
    TileSet  : String;
    Index    : Integer;
    Imported : Boolean;
    Used     : Boolean;
  end;

  TArrayTileInfo = Array of TTileInfo;
  TRoomArray     = Array of TRoomInfo;

type
  TMain = class(TDXForm)
    IsoKDMap1: TIsoKDMap;
    StatusBar1: TStatusBar;
    ToolBarImages: TImageList;
    OpenDialog1: TOpenDialog;
    OpenDialog2: TOpenDialog;
    SaveDialog1: TSaveDialog;
    Panel1: TPanel;
    SpeedButton7: TSpeedButton;
    ActionList1: TActionList;
    ActionFileNew: TAction;
    ActionFileOpen: TAction;
    ActionFileSave: TAction;
    ActionRoomNew: TAction;
    ActionRoomDelete: TAction;
    ActionRoomHFlip: TAction;
    ActionRoomVFlip: TAction;
    ActionRoomLRotate: TAction;
    ActionRoomRRotate: TAction;
    ActionRoomSettings: TAction;
    WallButton: TSpeedButton;
    ActionSettingShowWall: TAction;
    ActionSettingShowGrid: TAction;
    GridButton: TSpeedButton;
    ActionRoomCopy: TAction;
    JvChangeNotify1: TJvChangeNotify;
    FriendlyHotSpotButton: TSpeedButton;
    EnemyHotSpotButton: TSpeedButton;
    DeleteHotSpotButton: TSpeedButton;
    ActionFileSkripts: TAction;
    DockTop: TTBXDock;
    TBXToolbar1: TTBXToolbar;
    TBXItem1: TTBXItem;
    TBXSeparatorItem1: TTBXSeparatorItem;
    TBXItem2: TTBXItem;
    TBXSeparatorItem2: TTBXSeparatorItem;
    TBXItem3: TTBXItem;
    TBXItem4: TTBXItem;
    TBXItem5: TTBXItem;
    TBXItem6: TTBXItem;
    TBXSeparatorItem3: TTBXSeparatorItem;
    TBXItem7: TTBXItem;
    TBXItem8: TTBXItem;
    TBXItem9: TTBXItem;
    TBXSeparatorItem4: TTBXSeparatorItem;
    TBXItem10: TTBXItem;
    TBXItem11: TTBXItem;
    TBXSeparatorItem5: TTBXSeparatorItem;
    TBXItem12: TTBXItem;
    TBXMultiDock1: TTBXMultiDock;
    TBXMultiDock2: TTBXMultiDock;
    TBXDockablePanel1: TTBXDockablePanel;
    ListBox1: TListBox;
    TBXMultiDock3: TTBXMultiDock;
    TBXDockablePanel2: TTBXDockablePanel;
    EditPanel: TPanel;
    Panel2: TPanel;
    Panel3: TPanel;
    ButtonF: TSpeedButton;
    ButtonTL: TSpeedButton;
    ButtonBL: TSpeedButton;
    ButtonTR: TSpeedButton;
    ButtonO: TSpeedButton;
    ButtonBR: TSpeedButton;
    Panel4: TPanel;
    TileSetScrollBox: TScrollBox;
    Tileselector: TPaintBox;
    TileSetsTabControl: TTabControl;
    procedure IsoKDMap1Change(Sender: TObject);
    procedure ListBox1Click(Sender: TObject);
    procedure PaintFloor;
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure TileSelectorPaint(Sender: TObject);
    procedure TileSelectorMouseLeave(Sender: TObject);
    procedure TileSelectorMouseMove(Sender: TObject; Shift: TShiftState; X,
      Y: Integer);
    procedure TileSelectorMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure ButtonFClick(Sender: TObject);
    procedure ButtonTLClick(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure ButtonTRClick(Sender: TObject);
    procedure ButtonBLClick(Sender: TObject);
    procedure ButtonBRClick(Sender: TObject);
    procedure ButtonOClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure Splitter1Moved(Sender: TObject);
    procedure IsoKDMap1FileChange(Sender: TObject);
    function GetMapName(Ordner: String): String;
    procedure ListBox1DblClick(Sender: TObject);
    procedure SpeedButton7Click(Sender: TObject);
    procedure ListBox1DrawItem(Control: TWinControl; Index: Integer;
      Rect: TRect; State: TOwnerDrawState);
    procedure UpButtonClick(Sender: TObject);
    procedure LeftButtonClick(Sender: TObject);
    procedure DownButtonClick(Sender: TObject);
    procedure RightButtonClick(Sender: TObject);
    procedure IsoKDMap1TileMove(Sender: TObject; X, Y: Integer;
      Button: TMouseButton);
    procedure ActionFileNewExecute(Sender: TObject);
    procedure ActionFileOpenExecute(Sender: TObject);
    procedure ActionFileSaveExecute(Sender: TObject);
    procedure IsFileOpen(Sender: TObject);
    procedure ActionRoomNewExecute(Sender: TObject);
    procedure ActionRoomHFlipExecute(Sender: TObject);
    procedure IsRoomOpen(Sender: TObject);
    procedure ActionRoomVFlipExecute(Sender: TObject);
    procedure ActionRoomLRotateExecute(Sender: TObject);
    procedure ActionRoomRRotateExecute(Sender: TObject);
    procedure ActionRoomSettingsExecute(Sender: TObject);
    procedure TileSetsTabControlChange(Sender: TObject);
    procedure ActionSettingShowWallExecute(Sender: TObject);
    procedure ActionSettingShowGridExecute(Sender: TObject);
    procedure ToolButton8Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure JvChangeNotify1Notifications0Change(Sender: TObject);
    procedure OnTileGroupReload(Sender: TObject);
    procedure FriendlyHotSpotButtonClick(Sender: TObject);
    procedure EnemyHotSpotButtonClick(Sender: TObject);
    procedure DeleteHotSpotButtonClick(Sender: TObject);
    procedure ActionRoomDeleteExecute(Sender: TObject);
    procedure ActionFileSkriptsExecute(Sender: TObject);
    procedure ActionRoomDeleteUpdate(Sender: TObject);
    procedure EditPanelResize(Sender: TObject);
  private
    fBitmap   : TBitmap;
    fOver     : Integer;
    SWidth    : Integer;
    SHeight   : Integer;
    SRows     : Integer;

    fSelected : Integer;

    Minus     : Integer;
    fPage     : TPage;
    Scount    : Integer;
    OpenNode  : TTreeNode;
    fRooms    : TRoomArray;
    fFileName : String;
    OldIndex  : Integer;
    fTile     : String;
    fSettings : TMapSettings;
    fCopy     : TTileArray;
    fAktButton: TSpeedButton;
    fTileSets : Array of TTileGroup;
    // Tileverwaltung
    fUsedTiles      : TArrayTileInfo;
    fUsedWalls      : TArrayTileInfo;

    function GenerateID: Cardinal;

    procedure ClearMapTiles;
    function ImportTile(TileSet: String; Index: Integer): Integer;
    function ImportWall(TileSet: String; Index: Integer): Integer;

    function AddTile(ID: Cardinal; TileSet: String; Index: Integer): Integer;
    function AddWall(ID: Cardinal; TileSet: String; Index: Integer): Integer;

    procedure CheckDependingTiles(Index: Integer);
    procedure CheckDependingWalls(Index: Integer);

    procedure CountUsedTiles;

    procedure CopyTo(var Feld: TTileArray);
    procedure SaveFile;
    procedure LoadFile(FileName: String);
    function CreateRoom(Name: String; Width: Integer; Height: Integer): Integer;
    function CheckSave: Boolean;
    procedure Save;
    procedure Load(Index: Integer);

    procedure MirrorWall(var Dest: Smallint; Source: Integer; WallInfo: Pointer);
    procedure BackSideWall(var Dest: Smallint; Source: Integer; WallInfo: Pointer);

    procedure ReadTileSets;

    function TileGroup: TTilegroup;
    function FindTileGroup(TileSet: String): TTileGroup;
    { Private-Deklarationen }
  public
    FileName: String;
    Skripts : Array of TSkriptInfo;
    { Public-Deklarationen }
  end;

var
  Main: TMain;

const
  RoomWidth : Integer = 15;
  RoomHeight: Integer = 15;

implementation

uses frmProperties, frmNewRoom, Defines, frmSkripts, array_utils;
{$R *.DFM}

{ Schreibt einen String in einen Stream. Mit ReadString kann er wieder gelesen werden }
procedure WriteString(Stream: TStream; Str: String);
var
  Len : Integer;
begin
  Len:=Length(Str);
  Stream.Write(Len,SizeOf(Len));
  Stream.Write(Pointer(Str)^,Len);
end;

function ReadString(Stream: TStream): String;
var
  Len : Integer;
begin
  Stream.Read(Len,SizeOf(Len));
  SetString(result, nil, Len);
  Stream.Read(Pointer(result)^,len);
end;

procedure TMain.IsoKDMap1Change(Sender: TObject);
begin
  if IsoKDMap1.Modify then
  begin
    StatusBar1.Panels[1].Text:='Ge�ndert';
  end
  else
  begin
    StatusBar1.Panels[1].Text:='';
  end;
end;

procedure TMain.ListBox1Click(Sender: TObject);
begin
{  case fPage of
    pFloor     : Scount:=TileGroup.FloorCount;
    pWallTL    : Scount:=TileGroup.WallLeftCount+1;
    pWallTR    : Scount:=TileGroup.WallRightCount+1;
    pWallBR    : Scount:=TileGroup.WallLeftCount+1;
    pWallBL    : Scount:=TileGroup.WallRightCount+1;
    pObject    : Scount:=TileGroup.ObjektCount+1;
  end;
  PaintFloor;}
end;

procedure TMain.PaintFloor;
var
  Dummy: Integer;
  x: Integer;
  Y: Integer;
  Size: Integer;
begin
  SRows:=max(1,TileSetScrollBox.Height div SHeight);
  x:=0;
  Size:=(Scount+1) div SRows;
  fSelected:=-1;
  if (SCount mod SRows)<>0 then
    inc(Size);
  fBitmap.Width:=Size*SWidth;
  fBitmap.Height:=SHeight*SRows;
  fBitmap.Canvas.Brush.Color:=clBtnFace;
  fBitmap.Canvas.FillRect(Rect(0,0,fBitmap.Width,fBitmap.Height));
  TileSelector.Height:=SHeight*SRows;
  if fPage=pfloor then
  begin
    Y:=0;
    for Dummy:=0 to Scount-1 do
    begin
      TileGroup.DrawFloorCanvas(fBitmap.Canvas,x,Y,Dummy);
      inc(Y,TileHeight);
      if Y+TileHeight>TileSelector.Height then
      begin
        inc(x,TileWidth);
        Y:=0;
      end;
    end;
  end
  else if fPage in [pWallTL,pWallBR] then
  begin
    Y:=0;
    for Dummy:=-1 to Scount-1 do
    begin
      TileGroup.DrawWallLeftCanvas(fBitmap.Canvas,x,y,Dummy);
      inc(Y,WallHeight);
      if Y=TileSelector.Height then
      begin
        Y:=0;
        inc(x,WallWidth);
      end;
    end;
  end
  else if fPage in [pWallTR,pWallBL] then
  begin
    Y:=0;
    for Dummy:=-1 to Scount-1 do
    begin
      TileGroup.DrawWallRightCanvas(fBitmap.Canvas,x,y,Dummy);
      inc(Y,WallHeight);
      if Y=TileSelector.Height then
      begin
        Y:=0;
        inc(x,WallWidth);
      end;
    end
  end
  else if fPage=pObject then
  begin
    for Dummy:=-1 to Scount-1 do
    begin
//      TileGroup[ListBox1.ItemIndex].DrawObject(fBitmap.Canvas,x,0,Dummy);
      x:=x+TileWidth;
    end;
  end;
  TileSelector.Width:=fBitmap.Width;
  TileSelectorPaint(TileSelector);
end;

procedure TMain.FormCreate(Sender: TObject);
begin
  randomize;
  Application.Title:=LShortName+' Karten-Editor';
  Caption:=LShortName+' Karten-Editor';
  OpenNode:=nil;
  fBitmap:=TBitmap.Create;
  fPage:=pFloor;
  fBitmap.Height:=65;
  SRows:=1;
  SetLength(fCopy,RoomWidth,RoomHeight);
  Width:=Screen.Width;
  Height:=Screen.Height;
  Left:=0;
  Top:=0;
  OldIndex:=-1;

  ReadTileSets;

  TileSetScrollBox.DoubleBuffered:=true;

  TBRegLoadPositions(Self,HKEY_CURRENT_USER,'Software\Rich Entertainment\MapEdit\Toolbars');
  FormShow(Self);
  ActionFileNew.Execute;
//  CreateTree;
end;

procedure TMain.FormDestroy(Sender: TObject);
begin
  TBRegSavePositions(Self,HKEY_CURRENT_USER,'Software\Rich Entertainment\MapEdit\Toolbars');
  fBitmap.Free;
end;

procedure TMain.TileSelectorPaint(Sender: TObject);
begin
  TileSelector.Canvas.draw(0,0,fBitmap);
  TileSelector.Canvas.Brush.Style:=bsClear;
  TileSelector.Canvas.Pen.Color:=clWhite;
  TileSelector.Canvas.Rectangle((fOver div SRows)*SWidth,(fOver mod SRows)*SHeight,((fOver div SRows)*SWidth)+SWidth,((fOver mod SRows)*SHeight)+SHeight);
  TileSelector.Canvas.Pen.Color:=clBlue;
  TileSelector.Canvas.Rectangle((fSelected div SRows)*SWidth,(fSelected mod SRows)*SHeight,((fSelected div SRows)*SWidth)+SWidth,((fSelected mod SRows)*SHeight)+SHeight);
end;

procedure TMain.TileSelectorMouseLeave(Sender: TObject);
begin
  fOver:=-1;
  TileSelector.Refresh;
end;

procedure TMain.TileSelectorMouseMove(Sender: TObject; Shift: TShiftState; X,
  Y: Integer);
var
  New:  Integer;
begin
  if TileGroup=nil then exit;
  New:=(x div SWidth)*SRows+(Y div SHeight);
  if fOver<>New then
  begin
    fOver:=New;
    TileSelector.Refresh;
  end;
end;

procedure TMain.TileSelectorMouseDown(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
var
  Struct: TSetTile;
begin
  if fOver>=SCount then
    exit;

  fSelected:=fOver;

  SpeedButton7.Down:=true;

  case fPage of
    pFloor : Struct.Floor.Index:=ImportTile(TileSetsTabControl.Tabs[TileSetsTabControl.TabIndex],fSelected);
    pWallTL..pWallBR :
    begin
      Struct.BLeft.Index:=ImportWall(TileSetsTabControl.Tabs[TileSetsTabControl.TabIndex],fSelected-minus);
      Struct.BRight.Index:=ImportWall(TileSetsTabControl.Tabs[TileSetsTabControl.TabIndex],fSelected-minus);
      Struct.TLeft.Index:=ImportWall(TileSetsTabControl.Tabs[TileSetsTabControl.TabIndex],fSelected-minus);
      Struct.TRight.Index:=ImportWall(TileSetsTabControl.Tabs[TileSetsTabControl.TabIndex],fSelected-minus);
    end;
  end;
  Struct.Obj.Index:=fSelected-minus;
  Struct.Floor.SetTile:=(fPage=pFloor);
  Struct.BLeft.SetTile:=(fPage=pWallBL);
  Struct.BRight.SetTile:=(fPage=pWallBR);
  Struct.TLeft.SetTile:=(fPage=pWallTL);
  Struct.TRight.SetTile:=(fPage=pWallTR);
  Struct.Obj.SetTile:=(fPage=pObject);
  IsoKDMap1.SetPaintStruct(Struct);

  TileSelector.Refresh;
end;

procedure TMain.ButtonFClick(Sender: TObject);
begin
  if TileGroup=nil then
    exit;
  fPage:=pFloor;
  TileSelector.Height:=84;
  SWidth:=TileWidth;
  SHeight:=TileHeight;
  Scount:=TileGroup.FloorCount;
  minus:=0;
  fAktButton:=ButtonF;
  PaintFloor;
end;

procedure TMain.ButtonTLClick(Sender: TObject);
begin
  if TileGroup=nil then exit;
  fPage:=pWallTL;
  SWidth:=WallWidth;
  SHeight:=WallHeight;
  minus:=1;
  Scount:=TileGroup.WallLeftCount+1;
  TileSelector.Height:=WallHeight;
  fAktButton:=ButtonTL;
  PaintFloor;
end;

procedure TMain.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
begin
  CanClose:=CheckSave;
end;

procedure TMain.ButtonTRClick(Sender: TObject);
begin
  if TileGroup=nil then exit;
  fPage:=pWallTR;
  SWidth:=WallWidth;
  SHeight:=WallHeight;
  minus:=1;
  Scount:=TileGroup.WallRightCount+1;
  TileSelector.Height:=WallHeight;
  fAktButton:=ButtonTR;
  PaintFloor;
end;

procedure TMain.ButtonBLClick(Sender: TObject);
begin
  if TileGroup=nil then exit;
  fPage:=pWallBL;
  SWidth:=WallWidth;
  SHeight:=WallHeight;
  minus:=1;
  Scount:=TileGroup.WallRightCount+1;
  TileSelector.Height:=WallHeight;
  fAktButton:=ButtonBL;
  PaintFloor;
end;

procedure TMain.ButtonBRClick(Sender: TObject);
begin
  if TileGroup=nil then exit;
  fPage:=pWallBR;
  SWidth:=WallWidth;
  SHeight:=WallHeight;
  minus:=1;
  Scount:=TileGroup.WallLeftCount+1;
  TileSelector.Height:=WallHeight;
  fAktButton:=ButtonBR;
  PaintFloor;
end;

procedure TMain.ButtonOClick(Sender: TObject);
begin
  if TileGroup=nil then exit;
  fPage:=pObject;
  SWidth:=48;
  SHeight:=65;
  minus :=1;
  Scount:=TileGroup.ObjektCount+1;
  TileSelector.Height:=65;
  fAktButton:=ButtonO;
  PaintFloor;
end;

procedure TMain.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  IsoKDMap1.Finalize;
end;

procedure TMain.Splitter1Moved(Sender: TObject);
begin
  IsoKDMap1.Draw;
end;

procedure TMain.IsoKDMap1FileChange(Sender: TObject);
begin
  StatusBar1.Panels[2].Text:=IsoKDMap1.MapName;
  FileName:=IsoKDMap1.FileName;
  FormShow(Self);
end;

function TMain.GetMapName(Ordner: String): String;
var
  f: TFileStream;
  Head: TMapHeader;
begin
  f:=TFileStream.Create(Ordner,fmOpenRead);
  f.Read(Head,sizeOf(TMapHeader));
  if Head.Version=MapVersion then
  begin
    result:=Head.MapName;
  end;
  f.Free;
end;

procedure TMain.ListBox1DblClick(Sender: TObject);
var
  Index: Integer;
begin
  Index:=ListBox1.ItemIndex;
  if Index=-1 then exit;

  Save;
  Load(Index);
end;

procedure TMain.SaveFile;
var
  f     : TFileStream;
  ID    : Cardinal;
  Count : Integer;
  Dummy : Integer;
  X,Y   : Integer;
  List  : TStringList;

  procedure WriteFloor(Index: Integer);
  var
    ID: Cardinal;
  begin
    ID:=fUsedTiles[Index].ID;
    f.Write(ID,SizeOf(ID));
  end;

  procedure WriteWall(Index: Integer);
  var
    ID: Cardinal;
  begin
    if Index=-1 then
      ID:=0
    else
      ID:=fUsedWalls[Index].ID;

    f.Write(ID,SizeOf(ID));
  end;

  procedure SaveTileSets;
  var
    Dummy: Integer;

    procedure AddTileSet(Name: String);
    begin
      if List.IndexOf(Name)=-1 then
      begin
        List.Add(Name);
      end;
    end;

  begin
    for Dummy:=0 to high(fUsedTiles) do
    begin
      if fUsedTiles[Dummy].Used then
        AddTileSet(fUsedTiles[Dummy].TileSet);
    end;
    for Dummy:=0 to high(fUsedWalls) do
    begin
      if fUsedWalls[Dummy].Used then
        AddTileSet(fUsedWalls[Dummy].TileSet);
    end;

    WriteString(f,List.Text);
  end;

  procedure WriteTileInfo(const Info: TTileInfo);
  var
    Index: Integer;
  begin
    Index:=List.IndexOf(Info.TileSet);

    // ID abspeichern
    f.Write(Info.ID,SizeOf(Info.ID));
    // Nummer des Tilesets
    f.Write(Index,SizeOf(Index));
    // Index im Tileset
    f.Write(Info.Index,SizeOf(Info.Index));
  end;

  procedure SaveUsedArray(Arr: TArrayTileInfo);
  var
    Count: Integer;
    Dummy: Integer;
  begin
    Count:=0;
    for Dummy:=0 to high(Arr) do
    begin
      if Arr[Dummy].Used then
        inc(Count);
    end;

    f.Write(Count,SizeOf(Count));
    for Dummy:=0 to high(Arr) do
    begin
      if Arr[Dummy].Used then
        WriteTileInfo(Arr[Dummy]);
    end;
  end;
begin
  if fFilename='' then
  begin
    if not SaveDialog1.Execute then
      exit;
    fFileName:=SaveDialog1.FileName;
  end;
  Save;

  CountUsedTiles;

  f:=TFileStream.Create(fFileName,fmCreate);
  ID:=$1512AB37 { $0D16A44F };
  f.Write(ID,SizeOf(ID));

// Informationen zu den Tiles abspeichern
  List:=TStringList.Create;

  // Alle Benutzten Tilesets ermitteln und namen abspeichern
  SaveTileSets;

  // Benutzte Tiles und Walls abspeichern
  SaveUsedArray(fUsedTiles);
  SaveUsedArray(fUsedWalls);

  List.Free;

  // R�ume abspeichern
  Count:=length(fRooms);
  f.Write(Count,SizeOf(Count));
  for Dummy:=0 to COunt-1 do
  begin
    with fRooms[Dummy] do
    begin
      WriteString(f,Name);
      f.Write(Width,SizeOf(Integer));
      f.Write(Height,SizeOf(Integer));
      for X:=0 to Width-1 do
      begin
        for Y:=0 to Height-1 do
        begin
          with Tiles[X,Y] do
          begin
            WriteFloor(Floor);
            WriteWall(BLeft);
            WriteWall(Bright);
            WriteWall(TLeft);
            WriteWall(TRight);
            f.Write(HotSpot,SizeOf(HotSpot));
          end;
        end;
      end;
    end;
  end;

  // eingebundene Kartenskripte speichern
  ID:=MapScriptVersion;
  f.Write(ID,SizeOf(ID));
  Count:=length(Skripts);
  f.Write(Count,SizeOf(Count));
  for Dummy:=0 to Count-1 do
  begin
    WriteString(f,Skripts[Dummy].Name);
    WriteString(f,Skripts[Dummy].Skript);
  end;

  f.Free;

  IsoKDMap1.Modify:=false;
end;

function TMain.CreateRoom(Name: String; Width, Height: Integer): Integer;
var
  Index: Integer;
  X,Y: Integer;
begin
  ListBox1.Items.Add(Name);
  Index:=length(fRooms);
  result:=Index;
  SetLength(frooms,Index+1);
  fRooms[Index].Name:=Name;
  fRooms[Index].Width:=Width;
  fRooms[Index].Height:=Height;
  SetLength(fRooms[Index].Tiles,Width,Height);
  for X:=0 to Width-1 do
  begin
    for Y:=0 to Height-1 do
    begin
      FillChar(fRooms[Index].Tiles[x,y],SizeOf(TMapTileEntry),#0);
      with fRooms[Index].Tiles[x,y] do
      begin
        Floor:=0;
        TLeft:=-1;
        BLeft:=-1;
        Bright:=-1;
        TRight:=-1;
        Obj:=-1;
        HotSpot:=0;
      end;
    end;
  end;
end;

procedure TMain.Load(Index: Integer);
var
  X,Y: Integer;
begin
  IsoKDMap1.SetDimension(fRooms[Index].Width,fRooms[Index].Height);
  for X:=0 to fRooms[Index].Width-1 do
  begin
    for Y:=0 to fRooms[Index].Height-1 do
    begin
      IsoKDMap1.Tiles[X,Y]:=fRooms[Index].Tiles[X,Y];
    end;
  end;
  ISOKDMap1.Draw;
  OldIndex:=Index;
  ISOKDMap1.Modify:=false;
  ListBox1.Refresh;
end;

procedure TMain.Save;
begin
  if OldIndex=-1 then
    exit;
  CopyTo(fRooms[OldIndex].Tiles);
end;

type
  TImportFunc    = function(ID: Cardinal; TileSet: String; Index: Integer): Integer of object;
  TDependentFunc = procedure(Index: Integer) of object;

procedure TMain.LoadFile(FileName: String);
var
  f: TFileStream;
  ID: Cardinal;
  Count : Integer;
  Dummy : Integer;
  X,Y   : Integer;
  Wi,Hi : Integer;
  Name  : String;
  Ind   : Integer;
  HasHotSpots : Boolean;

  List  : TStringList;

  procedure ReadTileInfoArray(var Arr: TArrayTileInfo; Func: TImportFunc; DepFunc: TDependentFunc);
  var
    Count: Integer;
    Dummy: Integer;

    TileSet : Integer;
    ID      : Cardinal;
    Index   : Integer;
  begin
    f.Read(Count,SizeOf(Count));
    for Dummy:=0 to Count-1 do
    begin
      f.Read(ID,sizeOf(Cardinal));
      f.Read(TileSet,SizeOf(TileSet));
      f.Read(Index,SizeOf(Integer));
      Func(ID,List[TileSet],Index);
    end;
    for Dummy:=0 to Count-1 do
      DepFunc(Dummy);
  end;

  function ReadFloor: Integer;
  var
    ID    : Cardinal;
    Dummy : Integer;
  begin
    f.Read(ID,SizeOf(Cardinal));
    result:=-1;
    for Dummy:=0 to high(fUsedTiles) do
    begin
      if fUsedTiles[Dummy].ID=ID then
      begin
        result:=Dummy;
        exit;
      end;
    end;
  end;

  function ReadWall: Integer;
  var
    ID    : Cardinal;
    Dummy : Integer;
  begin
    f.Read(ID,SizeOf(Cardinal));
    result:=-1;
    if ID=0 then
      exit;
    for Dummy:=0 to high(fUsedWalls) do
    begin
      if fUsedWalls[Dummy].ID=ID then
      begin
        result:=Dummy;
        exit;
      end;
    end;
  end;

begin
  SetLength(fRooms,0);
  f:=TFileStream.Create(FileName,fmOpenRead);
  try
    fFileName:=FileName;
    f.Read(ID,SizeOf(ID));
    if not ValidMapVersion(ID) then
      raise Exception.Create('Ung�ltiges Format der Karte');

    HasHotSpots:=MapVersionHasInfo(ID,miHotSpots);

    // Auf Startzustand einstellen
    ClearMapTiles;
    ISOKdMap1.SetDimension(0,0);
    oldIndex:=-1;
    ListBox1.Items.Clear;

    // Benutzte TileSets laden
    List:=TStringList.Create;
    List.Text:=ReadString(f);

    ReadTileInfoArray(fUsedTiles,AddTile,CheckDependingTiles);
    ReadTileInfoArray(fUsedWalls,AddWall,CheckDependingWalls);

    f.Read(Count,SizeOf(Count));
    for Dummy:=0 to Count-1 do
    begin
      Name:=ReadString(f);
      f.Read(Wi,SizeOf(Integer));
      f.Read(Hi,SizeOf(Integer));
      Ind:=CreateRoom(Name,Wi,Hi);
      with fRooms[Ind] do
      begin

        for X:=0 to Width-1 do
        begin
          for Y:=0 to Height-1 do
          begin
            with Tiles[X,Y] do
            begin
              Floor:=ReadFloor;
              BLeft:=ReadWall;
              Bright:=ReadWall;
              TLeft:=ReadWall;
              TRight:=ReadWall;
              if HasHotSpots then
                f.Read(HotSpot,sizeof(HotSpot))
              else
                HotSpot:=0;
            end;
          end;
        end;

      end;
    end;
    f.Read(ID,SizeOf(ID));
    if ID=MapScriptVersion then
    begin
      f.Read(Count,SizeOf(Count));
      SetLength(Skripts,Count);
      for Dummy:=0 to Count-1 do
      begin
        Skripts[Dummy].Name:=ReadString(f);
        Skripts[Dummy].Skript:=ReadString(f);
      end;
    end;
  except
    f.Free;
    List.Free;

    raise;
  end;

  f.Free;
  List.Free;

  if length(fRooms)=0 then
    CreateRoom('default',RoomWidth,RoomHeight);
  Load(0);
end;

procedure TMain.SpeedButton7Click(Sender: TObject);
var
  Struct : TSetTile;
begin
  FillChar(Struct,SizeOf(Struct),#0);
  IsoKDMap1.SetPaintStruct(Struct);
end;

procedure TMain.CopyTo(var Feld: TTileArray);
var
  X,Y     : Integer;
  SrcTile : PMapTileEntry;
  DesTile : PMapTileEntry;
begin
  for X:=0 to IsoKDMap1.MapWidth-1 do
  begin
    SrcTile:=Addr(ISOKDMap1.Tiles[X,0]);
    DesTile:=Addr(Feld[X,0]);
    for Y:=0 to IsoKDMap1.MapHeight-1 do
    begin
      DesTile^:=SrcTile^;
      inc(SrcTile);
      inc(DesTile);
    end;
  end;
end;

procedure TMain.ListBox1DrawItem(Control: TWinControl; Index: Integer;
  Rect: TRect; State: TOwnerDrawState);
begin
  ListBox1.Canvas.FillRect(Rect);
  if OldIndex=Index then
    ListBox1.Canvas.Font.Style:=[fsBold];
  ListBox1.Canvas.TextOut(Rect.Left+2,Rect.Top+2,fRooms[Index].Name);
  if OldIndex=Index then
    ListBox1.Canvas.Font.Style:=[];
end;

procedure TMain.UpButtonClick(Sender: TObject);
begin
  IsoKDMap1.yOff:=ISOKDMap1.yOff+HalfTileHeight;
  IsoKDMap1.Draw;
end;

procedure TMain.LeftButtonClick(Sender: TObject);
begin
  IsoKDMap1.xOff:=ISOKDMap1.XOff-HalfTileWidth;
  IsoKDMap1.Draw;
end;

procedure TMain.DownButtonClick(Sender: TObject);
begin
  IsoKDMap1.yOff:=ISOKDMap1.yOff-HalfTileHeight;
  IsoKDMap1.Draw;
end;

procedure TMain.RightButtonClick(Sender: TObject);
begin
  IsoKDMap1.xOff:=ISOKDMap1.XOff+HalfTileWidth;
  IsoKDMap1.Draw;
end;

procedure TMain.BackSideWall(var Dest: Smallint; Source: Integer; WallInfo: Pointer);
begin
  if Source<>-1 then
    Dest:=PWallInformation(Integer(WallInfo)+(Source*SizeOf(TWallInformation)))^.BackSide
  else
    Dest:=-1;
end;

procedure TMain.MirrorWall(var Dest: Smallint; Source: Integer; WallInfo: Pointer);
begin
  if Source<>-1 then
    Dest:=PWallInformation(Integer(WallInfo)+(Source*SizeOf(TWallInformation)))^.Mirror
  else
    Dest:=-1;
end;

procedure TMain.IsoKDMap1TileMove(Sender: TObject; X, Y: Integer;
  Button: TMouseButton);
begin
  StatusBar1.Panels[0].Text:='Position: '+IntToStr(x+1)+':'+IntToStr(y+1);
end;

procedure TMain.ActionFileNewExecute(Sender: TObject);
var
  TileGroup: TTileGroup;
begin
  if not CheckSave then
    exit;

  fFilename:='';
  ClearMapTiles;
  ButtonF.Click;

  TileGroup:=FindTileGroup(TileSetsTabControl.Tabs[TileSetsTabControl.TabIndex]);
  Assert(TileGroup<>nil);
  if TileGroup.FloorCount>0 then
    fSelected:=0
  else
    fSelected:=-1;
  ImportTile(TileSetsTabControl.Tabs[TileSetsTabControl.TabIndex],fSelected);

  IsoKDMap1.SetDimension(RoomWidth,RoomHeight);
  ISOKDMap1.Modify:=false;
  fTile:=ExtractFileName(OpenDialog2.FileName);
  ListBox1.Items.Clear;
  SetLength(fRooms,0);
  CreateRoom('default',RoomWidth,RoomHeight);
  ButtonF.Click;
  OldIndex:=-1;
  fSettings.DefZu:=false;
  Load(0);
  SetLength(Skripts,0);
end;

procedure TMain.ActionFileOpenExecute(Sender: TObject);
begin
  if not CheckSave then
    exit;
    
  if OpenDialog1.Execute then
  begin
    try
      LoadFile(OpenDialog1.FileName);
      IsoKDMap1.SetDimension(RoomWidth,RoomHeight);
      ListBox1.Refresh;
      ISOKDMap1.Modify:=false;
      ButtonF.Click;
      OldIndex:=-1;
      if length(fRooms)>1 then
        Load(0);
    except
      on E: Exception do
      begin
        MessageDlg('Beim Laden der Karte trat folgender Fehler auf:'#13#10+E.Message,mtError,[mbOK],0);
        ActionFileNew.Execute;
      end;
    end;
  end;
end;

procedure TMain.ActionFileSaveExecute(Sender: TObject);
begin
  SaveFile;
end;

procedure TMain.IsFileOpen(Sender: TObject);
begin
  (Sender as TAction).Enabled:=(TileGroup<>nil) and (TileGroup.FileName<>'');
end;

procedure TMain.ActionRoomNewExecute(Sender: TObject);
begin
  if NewRoom.ShowModal=mrOk then
  begin
    CreateRoom(NewRoom.Edit1.Text,RoomWidth,RoomHeight);
  end;
end;

procedure TMain.ActionRoomHFlipExecute(Sender: TObject);
var
  X1,Y1      : Integer;
  X2         : Integer;
  SrcTile    : PMapTileEntry;
  DesTile    : PMapTileEntry;
  Info       : PTileInformation;
  WallInfo   : PWallInformation;
begin
  Info:=ISOKDMap1.TileGroup.TileInformation(0);
  WallInfo:=ISOKDMap1.TileGroup.WallInformation(0);
  CopyTo(fCopy);
  X2:=0;
  for X1:=0 to IsoKDMap1.MapWidth-1 do
  begin
    SrcTile:=Addr(fCopy[X1,IsoKDMap1.MapHeight-1]);
    DesTile:=Addr(IsoKDMap1.Tiles[X2,0]);
    for Y1:=IsoKDMap1.MapHeight-1 downto 0 do
    begin
      with SrcTile^ do
      begin
        DesTile^.Floor:=PTileInformation(Integer(Info)+(Floor*SizeOf(TTileInformation)))^.HMirror;

        BackSideWall(DesTile^.BLeft,TRight,WallInfo);
        MirrorWall(DesTile^.Bright,BRight,WallInfo);
        MirrorWall(DesTile^.TLeft,TLeft,WallInfo);
        BackSideWall(DesTile^.TRight,BLeft,WallInfo);
        
        DesTile^.Obj:=Obj;
      end;
      dec(SrcTile);
      inc(DesTile);
    end;
    inc(X2);
  end;
  ISOKDMap1.Draw;
  ISOKDMap1.Modify:=true;
end;

procedure TMain.IsRoomOpen(Sender: TObject);
begin
  (Sender as TAction).Enabled:=OldIndex<>-1;
end;

procedure TMain.ActionRoomVFlipExecute(Sender: TObject);
var
  X1,Y1      : Integer;
  X2         : Integer;
  SrcTile    : PMapTileEntry;
  DesTile    : PMapTileEntry;
  Info       : PTileInformation;
  WallInfo   : PWallInformation;
begin
  Info:=ISOKDMap1.TileGroup.TileInformation(0);
  WallInfo:=ISOKDMap1.TileGroup.WallInformation(0);
  CopyTo(fCopy);
  X2:=0;
  for X1:=IsoKDMap1.MapWidth-1 downto 0 do
  begin
    SrcTile:=Addr(fCopy[X1,0]);
    DesTile:=Addr(IsoKDMap1.Tiles[X2,0]);
    for Y1:=0 to IsoKDMap1.MapHeight-1 do
    begin
      with SrcTile^ do
      begin
        DesTile^.Floor:=PTileInformation(Integer(Info)+(Floor*SizeOf(TTileInformation)))^.VMirror;;

        MirrorWall(DesTile^.BLeft,BLeft,WallInfo);
        BackSideWall(DesTile^.Bright,TLeft,WallInfo);
        BackSideWall(DesTile^.TLeft,BRight,WallInfo);
        MirrorWall(DesTile^.TRight,TRight,WallInfo);

        DesTile^.Obj:=Obj;
      end;
      inc(DesTile);
      inc(SrcTile);
    end;
    inc(X2);
  end;
  ISOKDMap1.Draw;
  ISOKDMap1.Modify:=true;
end;

procedure TMain.ActionRoomLRotateExecute(Sender: TObject);
var
  X1,Y1      : Integer;
  X2         : Integer;
  DesTile    : PMapTileEntry;
  Info       : PTileInformation;
  WallInfo   : PWallInformation;
begin
  Info:=ISOKDMap1.TileGroup.TileInformation(0);
  WallInfo:=ISOKDMap1.TileGroup.WallInformation(0);
  CopyTo(fCopy);
  X2:=0;
  for X1:=0 to IsoKDMap1.MapWidth-1 do
  begin
    DesTile:=Addr(IsoKDMap1.Tiles[X2,0]);
    for Y1:=0 to IsoKDMap1.MapHeight-1 do
    begin
      with fCopy[Y1,(IsoKDMap1.MapWidth-1)-X1] do
      begin
        DesTile^.Floor:=PTileInformation(Integer(Info)+(Floor*SizeOf(TTileInformation)))^.LeftRotate;

        DesTile^.BLeft:=BRight;
        BackSideWall(DesTile^.Bright,TRight,WallInfo);
        BackSideWall(DesTile^.TLeft,BLeft,WallInfo);
        DesTile^.TRight:=TLeft;

        DesTile^.Obj:=Obj;
      end;
      inc(DesTile);
    end;
    inc(X2);
  end;
  ISOKDMap1.Draw;
  ISOKDMap1.Modify:=true;
end;

procedure TMain.ActionRoomRRotateExecute(Sender: TObject);
var
  X1,Y1      : Integer;
  X2         : Integer;
  DesTile    : PMapTileEntry;
  Info       : PTileInformation;
  WallInfo   : PWallInformation;
begin
  Info:=ISOKDMap1.TileGroup.TileInformation(0);
  WallInfo:=ISOKDMap1.TileGroup.WallInformation(0);
  CopyTo(fCopy);
  X2:=0;
  for X1:=0 to IsoKDMap1.MapWidth-1 do
  begin
    DesTile:=Addr(IsoKDMap1.Tiles[X2,0]);
    for Y1:=0 to IsoKDMap1.MapHeight-1 do
    begin
      with fCopy[(IsoKDMap1.MapWidth-1)-Y1,X1] do
      begin
        DesTile^.Floor:=PTileInformation(Integer(Info)+(Floor*SizeOf(TTileInformation)))^.RightRotate;

        BackSideWall(DesTile^.BLeft,TLeft,WallInfo);
        DesTile^.Bright:=BLeft;
        DesTile^.TLeft:=TRight;
        BackSideWall(DesTile^.TRight,BRight,WallInfo);

        DesTile^.Obj:=Obj;
      end;
      inc(DesTile);
    end;
    inc(X2);
  end;
  ISOKDMap1.Draw;
  ISOKDMap1.Modify:=true;
end;

procedure TMain.ReadTileSets;
var
  Files: TSearchRec;
  Reg  : TRegistry;
  Dir  : String;
  Group: TTileGroup;
begin
  Reg:=TRegistry.Create;
  if Reg.OpenKey('Software\Rich Entertainment\Xforce',false) then
  begin
    Dir:=Reg.ReadString('InstallDir');
    Dir:=IncludeTrailingBackslash(Dir)+'data\tileSets\';
    if FindFirst(Dir+'*.t3d',faAnyFile,Files)=0 then
    begin
      repeat
        Group:=TTileGroup.Create(Self);
        try
          Group.LoadFromFile(Dir+Files.Name);
          Group.OnFileReload:=OnTileGroupReload;
          TileSetsTabControl.Tabs.Add(ChangeFileExt(Files.Name,''));
          SetLength(fTileSets,length(fTileSets)+1);
          fTileSets[high(fTileSets)]:=Group;
        except
          Group.Free;
        end;
      until FindNext(Files)<>0;
      FindClose(Files);
    end;

    // Notification setzen
    with JvChangeNotify1.Notifications.Add do
    begin
      Directory:=Dir;
      Actions:=[caChangeLastWrite];
      OnChange:=JvChangeNotify1Notifications0Change;
    end;
    JvChangeNotify1.Active:=true;
  end;

  Reg.Free;
  ButtonF.Click;
end;

function TMain.TileGroup: TTilegroup;
begin
  result:=nil;
  if TileSetsTabControl.Tabs.Count>0 then
    result:=fTileSets[TileSetsTabControl.TabIndex];
end;

procedure TMain.ActionRoomSettingsExecute(Sender: TObject);
var
  Settings: TSettings;
begin
  if OldIndex=-1 then
    exit;
  Settings:=TSettings.Create(Self);
  Settings.MapNameEdit.Text:=fRooms[OldIndex].Name;
//  Settings.Rotate.Checked:=fROoms[OldIndex].CanRotate;
  Settings.ShowModal;
  if Settings.ModalResult=mrOk then
  begin
    fRooms[OldIndex].Name:=Settings.MapNameEdit.Text;
//    fROoms[OldIndex].CanRotate:=Settings.Rotate.Checked;
    ListBox1.Refresh;
  end;
  Settings.Free;
  StatusBar1.Panels[2].Text:=IsoKDMap1.MapName;
end;

procedure TMain.TileSetsTabControlChange(Sender: TObject);
begin
  fAktButton.Click;
end;

procedure TMain.ActionSettingShowWallExecute(Sender: TObject);
begin
  ActionSettingShowWall.Checked:=not ActionSettingShowWall.Checked;

  WallButton.Down:=ActionSettingShowWall.Checked;
  IsoKDMap1.ShowWall:=ActionSettingShowWall.Checked;
  IsoKDMap1.Draw;
end;

procedure TMain.ActionSettingShowGridExecute(Sender: TObject);
begin
  ActionSettingShowGrid.Checked:=not ActionSettingShowGrid.Checked;
  
  GridButton.Down:=ActionSettingShowGrid.Checked;
  IsoKDMap1.Grid:=ActionSettingShowGrid.Checked;
  IsoKDMap1.Draw;
end;

function TMain.CheckSave: Boolean;
var
  res: Integer;
begin
  result:=true;
  if IsoKDMap1.Modify then
  begin
    res:=Application.MessageBox(PChar('M�chten Sie die �nderungen speichern?'),PChar('Frage'),MB_YESNOCANCEL+ MB_ICONQUESTION);
    if res=IDCancel then result:=false;
    if res=IDYes then
    begin
      Save;
      SaveFile;
    end;
  end;
end;

procedure TMain.ClearMapTiles;
begin
  ISOKDMap1.ClearTileSet;
  SetLength(fUsedTiles,0);
  SetLength(fUsedWalls,0);
end;

function TMain.ImportTile(TileSet: String; Index: Integer): Integer;
var
  Dummy    : Integer;
  TileGroup: TTileGroup;
begin
  result:=-1;
  if Index=-1 then
    exit;
    
  for Dummy:=0 to Length(fUsedTiles)-1 do
  begin
    if (fUsedTiles[Dummy].TileSet=TileSet) and (fUsedTiles[Dummy].Index=Index) then
    begin
      result:=Dummy;
      exit;
    end;
  end;
  TileGroup:=FindTileGroup(TileSet);
  if TileGroup=nil then
    raise Exception.Create('Ben�tigtes Tileset '+TileSet+' nicht gefunden');

  result:=AddTile(GenerateID,TileSet,Index);

  CheckDependingTiles(result);
end;

function TMain.ImportWall(TileSet: String; Index: Integer): Integer;
var
  Dummy    : Integer;
  TileGroup: TTileGroup;
begin
  if Index<0 then
  begin
    result:=Index;
    exit;
  end;

  for Dummy:=0 to Length(fUsedWalls)-1 do
  begin
    if (fUsedWalls[Dummy].TileSet=TileSet) and (fUsedWalls[Dummy].Index=Index) then
    begin
      result:=Dummy;
      exit;
    end;
  end;
  TileGroup:=FindTileGroup(TileSet);
  if TileGroup=nil then
    raise Exception.Create('Ben�tigtes Tileset '+TileSet+' nicht gefunden');

  result:=AddWall(GenerateID,TileSet,Index);

  CheckDependingWalls(result);
end;

procedure TMain.CountUsedTiles;
var
  Dummy: Integer;
  X,Y  : Integer;

  procedure CountTile(Index: Integer);
  begin
    if Index=-1 then
      exit;

    fUsedTiles[Index].Used:=true;
  end;

  procedure CountWall(Index: Integer);
  begin
    if (Index=-1) or fUsedWalls[Index].Used then
      exit;

    fUsedWalls[Index].Used:=true;

    with IsoKDMap1.TileGroup.WallInformation(Index)^ do
      CountWall(Destroyed);
  end;

begin
  // Anzahl auf 0 setzen
  for Dummy:=0 to high(fUsedWalls) do
    fUsedWalls[Dummy].Used:=false;

  for Dummy:=0 to high(fUsedTiles) do
    fUsedTiles[Dummy].Used:=false;

  // Alle R�ume durchgehen und z�hlen wieoft jede Mauer/Tile benutzt wird
  for Dummy:=0 to high(fRooms) do
  begin
    with fRooms[Dummy] do
    begin
      for X:=0 to Width-1 do
      begin
        for Y:=0 to Height-1 do
        begin
          with Tiles[X,Y] do
          begin
            CountTile(Floor);
            CountWall(BLeft);
            CountWall(Bright);
            CountWall(TLeft);
            CountWall(TRight);
          end;
        end;
      end;
    end;
  end;
end;

function TMain.FindTileGroup(TileSet: String): TTileGroup;
var
  Dummy: Integer;
begin
  result:=nil;
  for Dummy:=0 to high(fTileSets) do
  begin
    if StrIComp(PChar(ExtractFileName(ChangeFileExt(fTileSets[Dummy].FileName,''))),PChar(TileSet))=0 then
    begin
      result:=fTileSets[Dummy];
      exit;
    end;
  end;
end;

function TMain.AddTile(ID: Cardinal; TileSet: String; Index: Integer): Integer;
var
  Bitmap     : TBitmap;
  TileGroup  : TTileGroup;
begin
  TileGroup:=FindTileGroup(TileSet);
  if TileGroup=nil then
    raise Exception.Create('Ben�tigtes Tileset '+TileSet+' nicht gefunden');

  Bitmap:=TBitmap.Create;
  Bitmap.Width:=TileWidth;
  Bitmap.Height:=TileHeight;
  TileGroup.DrawFloorCanvas(Bitmap.Canvas,0,0,Index);
  IsoKDMap1.TileGroup.AddFloor(Bitmap);

  SetLength(fUsedTiles,length(fUsedTiles)+1);
  result:=high(fUsedTiles);
  fUsedTiles[result].ID:=ID;
  fUsedTiles[result].TileSet:=TileSet;
  fUsedTiles[result].Index:=Index;
  fUsedTiles[result].Imported:=false;

  Bitmap.Free;
end;

function TMain.AddWall(ID: Cardinal; TileSet: String; Index: Integer): Integer;
var
  Bitmap    : TBitmap;
  TileGroup : TTileGroup;
begin
  TileGroup:=FindTileGroup(TileSet);
  if TileGroup=nil then
    raise Exception.Create('Ben�tigtes Tileset '+TileSet+' nicht gefunden');

  Bitmap:=TBitmap.Create;
  Bitmap.Width:=WallWidth;
  Bitmap.Height:=WallHeight;
  TileGroup.DrawWallLeftCanvas(Bitmap.Canvas,0,0,Index);
  IsoKDMap1.TileGroup.AddWall(Bitmap);

  TileGroup.DrawWallRightCanvas(Bitmap.Canvas,0,0,Index);
  IsoKDMap1.TileGroup.ReplaceWallRight(Bitmap,ISOKDMap1.TileGroup.WallLeftCount-1);

  SetLength(fUsedWalls,length(fUsedWalls)+1);
  result:=high(fUsedWalls);
  fUsedWalls[result].ID:=ID;
  fUsedWalls[result].TileSet:=TileSet;
  fUsedWalls[result].Index:=Index;
  fUsedWalls[result].Imported:=false;

  Bitmap.Free;
end;

procedure TMain.CheckDependingTiles(Index: Integer);
var
  Info : TTileInformation;
  TileGroup : TTileGroup;
begin
  TileGroup:=FindTileGroup(fUsedTiles[Index].TileSet);
  if TileGroup=nil then
    raise Exception.Create('Ben�tigtes Tileset '+fUsedTiles[Index].TileSet+' nicht gefunden');

  if (fUsedTiles[Index].Index<0) or (fUsedTiles[Index].Index>=TileGroup.FloorCount) then
    exit;
    
  Info:=TileGroup.TileInformation(fUsedTiles[Index].Index)^;
  Info.LeftRotate:=ImportTile(fUsedTiles[Index].TileSet,Info.LeftRotate);
  Info.HMirror:=ImportTile(fUsedTiles[Index].TileSet,Info.HMirror);
  Info.VMirror:=ImportTile(fUsedTiles[Index].TileSet,Info.VMirror);
  Info.RightRotate:=ImportTile(fUsedTiles[Index].TileSet,Info.RightRotate);
  ISOKDMap1.TileGroup.TileInformation(Index)^:=Info;

  fUsedTiles[Index].Imported:=true;
end;

procedure TMain.CheckDependingWalls(Index: Integer);
var
  Info: TWallInformation;
  TileGroup : TTileGroup;
begin
  TileGroup:=FindTileGroup(fUsedWalls[Index].TileSet);
  if TileGroup=nil then
    raise Exception.Create('Ben�tigtes Tileset '+fUsedTiles[Index].TileSet+' nicht gefunden');

  if (fUsedWalls[Index].Index<0) or (fUsedWalls[Index].Index>TileGroup.WallLeftCount) then
    exit;

  Info:=TileGroup.WallInformation(fUsedWalls[Index].Index)^;
  Info.Mirror:=ImportWall(fUsedWalls[Index].TileSet,Info.Mirror);
  Info.BackSide:=ImportWall(fUsedWalls[Index].TileSet,Info.BackSide);
  Info.Border:=ImportWall(fUsedWalls[Index].TileSet,Info.Border);
  Info.Destroyed:=ImportWall(fUsedWalls[Index].TileSet,Info.Destroyed);
  ISOKDMap1.TileGroup.WallInformation(Index)^:=Info;
  fUsedWalls[Index].Imported:=true;
end;

procedure TMain.ToolButton8Click(Sender: TObject);
var
  X: Integer;
begin
  CreateRoom('Kopie von '+fRooms[OldIndex].Name,fRooms[OldIndex].Width,fRooms[OldIndex].Height);
  with fRooms[OldIndex] do
  begin
    for X:=0 to Width-1 do
    begin
      CopyMemory(Addr(fRooms[high(fRooms)].Tiles[X,0]),Addr(Tiles[X,0]),length(Tiles[X])*SizeOf(Tiles[X,0]));
    end;
  end;
  
end;

procedure TMain.FormShow(Sender: TObject);
begin
  IsoKDMap1.SetDimension(0,0);
  ISoKDMap1.Modify:=false;
  fPage:=pFloor;
  fOver:=-2;
  fPage:=pFloor;
  TileSelector.Height:=TileHeight;

  SWidth:=TileWidth;
  SHeight:=Tileheight;
  minus:=0;
  PaintFloor;
end;

function TMain.GenerateID: Cardinal;
var
  OK    : Boolean;
  Dummy : Integer;
begin
  repeat
    OK:=true;
    result:=random(High(Cardinal));
    for Dummy:=0 to high(fUsedTiles) do
    begin
      if fUsedTiles[Dummy].ID=result then
      begin
        OK:=false;
        break;
      end;
    end;
    if OK then
    begin
      for Dummy:=0 to high(fUsedWalls) do
      begin
        if fUsedWalls[Dummy].ID=result then
        begin
          OK:=false;
          break;
        end;
      end;
    end;
  until OK;
end;

procedure TMain.JvChangeNotify1Notifications0Change(Sender: TObject);
var
  Dummy: Integer;
begin
  for Dummy:=0 to high(fTileSets) do
    fTileSets[Dummy].CheckReload;
end;

procedure TMain.OnTileGroupReload(Sender: TObject);
var
  OldFile : String;
  OldMod  : Boolean;
  OldRoom : Integer;
begin
  OldFile:=fFileName;
  OldMod:=IsoKDMap1.Modify;
  OldRoom:=OldIndex;

  fFileName:=FileGetTempName('map');
  SaveFile;
  LoadFile(fFileName);
  Load(OldRoom);
  DeleteFile(fFileName);

  fFileName:=OldFile;
  IsoKDMap1.Modify:=OldMod;

  fAktButton.Click;
  IsoKDMap1.Draw;
end;

procedure TMain.FriendlyHotSpotButtonClick(Sender: TObject);
var
  Struct : TSetTile;
begin
  FillChar(Struct,SizeOf(Struct),#0);
  Struct.HotSpot.Index:=1;
  Struct.HotSpot.SetTile:=true;

  IsoKDMap1.SetPaintStruct(Struct);
end;

procedure TMain.EnemyHotSpotButtonClick(Sender: TObject);
var
  Struct : TSetTile;
begin
  FillChar(Struct,SizeOf(Struct),#0);
  Struct.HotSpot.Index:=2;
  Struct.HotSpot.SetTile:=true;

  IsoKDMap1.SetPaintStruct(Struct);
end;

procedure TMain.DeleteHotSpotButtonClick(Sender: TObject);
var
  Struct : TSetTile;
begin
  FillChar(Struct,SizeOf(Struct),#0);
  Struct.HotSpot.Index:=0;
  Struct.HotSpot.SetTile:=true;

  IsoKDMap1.SetPaintStruct(Struct);
end;

procedure TMain.ActionRoomDeleteExecute(Sender: TObject);
var
  Index: Integer;
begin
  if Length(fRooms)<=1 then
  begin
    MessageDlg('Der letzte Raum kaum nicht gel�scht werden',mtError,[mbOK],0);
    exit;
  end;
  if MessageDlg(Format('Soll der Raum %s gel�scht werden',[fRooms[OldIndex].Name]),mtConfirmation,[mbYes,mbNo],0)=mrYes then
  begin
    Index:=ListBox1.ItemIndex;
    ListBox1.Items.Delete(OldIndex);
    DeleteArray(Addr(fRooms),TypeInfo(TRoomArray),OldIndex);
    OldIndex:=-1;
    Load(min(Index,ListBox1.Items.Count-1));
    ISoKDMap1.Modify:=true;
  end;
end;

procedure TMain.ActionFileSkriptsExecute(Sender: TObject);
begin
  FromSkripts.ShowModal;
end;

procedure TMain.ActionRoomDeleteUpdate(Sender: TObject);
begin
  ActionRoomDelete.Enabled:=OldIndex<>-1;
end;

procedure TMain.EditPanelResize(Sender: TObject);
begin
  if fAktButton<>nil then
    fAktButton.Click;
end;

end.
