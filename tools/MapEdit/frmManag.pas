unit frmManag;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ImgList, ComCtrls, ToolWin, ExtCtrls, PlaceMnt,BitmapButton,types;

function ImageList_Create(CX, CY: Integer; Flags: UINT;Initial, Grow: Integer): HWND; stdcall;

type
  TManag = class(TForm)
    ListView1: TListView;
    ToolBar1: TToolBar;
    LargeIcons: TToolButton;
    ToolBarImages: TImageList;
    SmallIcons: TToolButton;
    List: TToolButton;
    Details: TToolButton;
    CreateMap: TToolButton;
    OpenMap: TToolButton;
    Refresh: TToolButton;
    Seperator2: TToolButton;
    Seperator1: TToolButton;
    DeleteMap: TToolButton;
    Store: TFormStorage;
    Image1: TImage;
    Image2: TImage;
    Large: TImageList;
    Small: TImageList;
    procedure FormCreate(Sender: TObject);
    procedure LargeIconsClick(Sender: TObject);
    procedure ListView1SelectItem(Sender: TObject; Item: TListItem;
      Selected: Boolean);
    procedure CreateMapClick(Sender: TObject);
    procedure OpenFile(Sender: TObject);
    procedure DeleteClick(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure RefreshClick(Sender: TObject);
    procedure ListView1ColumnClick(Sender: TObject; Column: TListColumn);
    procedure ListView1DblClick(Sender: TObject);
  private
    fSort: Integer;
    procedure RefreshList;
    { Private-Deklarationen }
  public
    { Public-Deklarationen }
  end;

var
  Manag: TManag;

implementation

uses frmMain, frmNewMap;

function ImageList_Create; external 'comctl32.dll' name 'ImageList_Create';

{$R *.DFM}

function CustomSortProc(Item1, Item2: TListItem; ParamSort: integer): integer; stdcall;
begin
  if (ParamSort=0) then Result := CompareText(Item1.Caption,Item2.Caption)
  else if (ParamSort=3) then Result := -CompareText(Item1.Caption,Item2.Caption)
  else if (ParamSort=1) or (ParamSort=2) then Result := CompareText(Item1.SubItems.Strings[ParamSort-1],Item2.SubItems.Strings[ParamSort-1])
  else if (ParamSort=4) or (ParamSort=5) then Result :=-CompareText(Item1.SubItems.Strings[ParamSort-4],Item2.SubItems.Strings[ParamSort-4])
  else Result:=0;
end;

procedure TManag.FormCreate(Sender: TObject);
begin
  Large.Handle:=ImageList_Create(32,32,$0011,1,1);
  Large.AddIcon(Image1.Picture.Icon);
  Small.Handle:=ImageList_Create(16,16,$0011,1,1);
  Small.AddIcon(Image2.Picture.Icon);
  Store.RestoreFormPlacement;
  LargeIcons.Tag:=Integer(vsIcon);
  SmallIcons.Tag:=Integer(vsSmallIcon);
  List.Tag:=Integer(vsList);
  Details.Tag:=Integer(vsReport);
  RefreshList;
  fSort:=0;
end;

procedure TManag.LargeIconsClick(Sender: TObject);
begin
  ListView1.ViewStyle:=TViewStyle((Sender as TToolButton).Tag);
end;

procedure TManag.ListView1SelectItem(Sender: TObject; Item: TListItem;
  Selected: Boolean);
begin
  OpenMap.Enabled:=Selected;
  DeleteMap.Enabled:=Selected;
end;

procedure TManag.CreateMapClick(Sender: TObject);
begin
  NewMap.ShowModal;
end;

procedure TManag.OpenFile(Sender: TObject);
begin
{  Main.IsoKDMap1.LoadFromFile(ListView1.Selected.Subitems.Strings[1]);
  Main.Caption:='KD4 Karten Editor [ '+ListView1.Selected.Caption+' ]';
  Main.IsoKDMap1.Initialize;
  Main.FileName:=ListView1.Selected.Subitems.Strings[1];
  Main.Show;}
end;

procedure TManag.DeleteClick(Sender: TObject);
begin
  if Application.MessageBox(PChar('Soll die Karte '''+ListView1.Selected.Caption+''' gel�scht werden?'),PChar('Frage'),MB_YESNO or MB_ICONQUESTION) = ID_YES then
  begin
    DeleteFile(ListView1.Selected.SubItems.Strings[1]);
    ListView1.Selected.Delete;
  end;
end;

procedure TManag.FormDestroy(Sender: TObject);
begin
  Store.SaveFormPlacement;
end;

procedure TManag.RefreshClick(Sender: TObject);
begin
  RefreshList;
end;

procedure TManag.RefreshList;
var
  Stat: TSearchRec;
  Status: Integer;
  Item: TListItem;
  f: TFileStream;
  Head: TMapHeader;
begin
  ListView1.Items.Clear;
  Status:=FindFirst('maps\*.m3d',faAnyFile,Stat);
  while Status=0 do
  begin
    f:=TFileStream.Create('maps\'+Stat.Name,fmOpenRead);
    f.Read(Head,sizeOf(TMapHeader));
    if Head.Version=MapVersion then
    begin
      Item:=ListView1.Items.Add;
      Item.Caption:=Head.MapName;
      Item.Subitems.Add(IntToStr(Head.Width)+'x'+IntToStr(Head.height));
      Item.Subitems.Add('maps\'+Stat.Name);
      Status:=FindNext(Stat);
    end;
    f.Free;
  end;
  FindClose(Stat);
  ListView1.CustomSort(@CustomSortProc, fSort);
end;

procedure TManag.ListView1ColumnClick(Sender: TObject;
  Column: TListColumn);
begin
  if fSort=Column.Index then fSort:=fSort+3 else fSort:=Column.Index;
  ListView1.CustomSort(@CustomSortProc, fSort);
end;

procedure TManag.ListView1DblClick(Sender: TObject);
begin
  if ListView1.Selected<>nil then
    OpenFile(Self);
end;

end.

