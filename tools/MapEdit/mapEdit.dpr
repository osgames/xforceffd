program mapEdit;

uses
  Forms,
  Windows,
  Messages,
  frmMain in 'frmMain.pas' {Main},
  frmProperties in 'frmProperties.pas' {Settings},
  frmNewRoom in 'frmNewRoom.pas' {NewRoom},
  frmSkripts in 'frmSkripts.pas' {FromSkripts},
  {$i language.inc};

{$R *.RES}

var
  SH_Sem: HWND;
  SH_Wind: HWND;
  SH_Ins : boolean;
begin
  SH_Sem:=CreateSemaphore(nil,0,1,'KD4 - Karten-Editor');
  SH_Ins:=(SH_Sem<>0) and (GetLastError=ERROR_ALREADY_EXISTS);
  If SH_Ins then
  begin
    CloseHandle(SH_Sem);
    SH_Wind:=FindWindow(nil,LShortName+' Karten-Editor');
    If SH_Wind<>0 then
    begin
      PostMessage(SH_Wind,WM_SYSCOMMAND,SC_RESTORE,0);
      SetForegroundWindow(SH_Wind);
      exit;
    end;
  end;
  Application.Initialize;
  Application.CreateForm(TMain, Main);
  Application.CreateForm(TNewRoom, NewRoom);
  Application.CreateForm(TFromSkripts, FromSkripts);
  Application.Run;
end.
