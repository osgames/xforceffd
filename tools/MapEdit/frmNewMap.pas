unit frmNewMap;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, RXSpin, Buttons, RXCtrls, ExtCtrls, HotButton, types;

type
  TNewMap = class(TForm)
    MapSizePanel: TGroupBox;
    WidthLabel: TLabel;
    Width: TRxSpinEdit;
    Height: TRxSpinEdit;
    HeightLabel: TLabel;
    Sqaure: TCheckBox;
    MapPanel: TGroupBox;
    MapNameLabel: TLabel;
    MapFile: TLabel;
    MapNameEdit: TEdit;
    MapFileEdit: TEdit;
    ExtensionsLabel: TLabel;
    TileSetPanel: TPanel;
    TileSets: TRxCheckListBox;
    CancelButton: THotButton;
    OKButton: THotButton;
    procedure FormCreate(Sender: TObject);
    procedure WidthChange(Sender: TObject);
    procedure SqaureClick(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure TileSetsClickCheck(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
  private
    Files: TStringList;
    { Private-Deklarationen }
  public
    { Public-Deklarationen }
  end;

var
  NewMap: TNewMap;

implementation

uses frmMain, frmManag;

{$R *.DFM}

procedure TNewMap.FormCreate(Sender: TObject);
var
  Stat: TSearchRec;
  Status: Integer;
  f: TFileStream;
  Head: TTileSetHeader;
begin
  Files:=TStringList.Create;
  Status:=FindFirst('TileSets\*.t3d',faAnyFile,Stat);
  while Status=0 do
  begin
    f:=TFileStream.Create('TileSets\'+Stat.Name,fmOpenRead);
    f.Read(Head,SizeOf(TTileSetHeader));
    TileSets.Items.Add(Head.Name);
    Files.Add('TileSets\'+Stat.Name);
    Status:=FindNext(Stat);
    f.Free;
  end;
  FindClose(Stat);
end;

procedure TNewMap.WidthChange(Sender: TObject);
begin
  if Sqaure.Checked then
  Height.Value:=Width.Value;
end;

procedure TNewMap.SqaureClick(Sender: TObject);
begin
  Height.Enabled:=not Sqaure.Checked;
  if not Height.Enabled then Height.Value:=Width.Value;
end;

procedure TNewMap.BitBtn1Click(Sender: TObject);
var
  Dummy: integer;
begin
  Main.Caption:='KD4 Karten Editor [ '+MapNameEdit.Text+' ]';
  Main.IsoKDMap1.SetDimension(Round(Width.Value),Round(Height.Value));
  Main.IsoKDMap1.MapName:=MapNameEdit.Text;
  Main.FileName:='maps\'+MapFileEdit.Text+'.m3d';
  Main.IsoKDMap1.CreateNewMap(Main.FileName);
  for DUmmy:=0 to TileSets.Items.Count-1 do
  begin
    if TileSets.Checked[Dummy] then
    Main.IsoKDMap1.LoadTile(Files[Dummy]);
  end;
  Main.IsoKDMap1.Initialize;
  Main.Show;
end;

procedure TNewMap.TileSetsClickCheck(Sender: TObject);
var
  Dummy: Integer;
  Enabled: boolean;
begin
  Enabled:=false;
  for Dummy:=0 to TileSets.Items.Count-1 do
  begin
    if TileSets.Checked[Dummy] then Enabled:=true;
  end;
  if (MapNameEdit.Text='') or (MapFileEdit.Text='') then Enabled:=false;
  OKButton.Enabled:=Enabled;
end;

procedure TNewMap.FormShow(Sender: TObject);
begin
  FIles.Clear;
  TileSets.Items.Clear;
  FormCreate(Self);
  MapNameEdit.Text:='';
  MapFileEdit.Text:='';
  Width.Value:=20;
  Height.Value:=20;
  Sqaure.Checked:=true;
  OKButton.Enabled:=false;
end;

procedure TNewMap.FormDestroy(Sender: TObject);
begin
  Files.Free;
end;

end.
