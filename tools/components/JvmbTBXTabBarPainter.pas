unit JvmbTBXTabBarPainter;

interface

uses
  Messages, SysUtils, Windows, Graphics, Classes, JvTabBar, TBX, TBXThemes, Forms,
  TBXUtils, Math;

type
  TJvmbTBXTabBarPainter = class(TJvTabBarPainter)
  private
    FFont: TFont;
    FDisabledFont: TFont;
    FSelectedFont: TFont;
    FTabWidth: Integer;
    ii: TTBXItemInfo;
    FToolbarBackground: boolean;
    FAllowSelectedHot: boolean;
    FHandle: HWnd;

    function GetHandle: HWnd;
    procedure SetToolbarBackground(Value: boolean);
    procedure SetAllowSelectedHot(Value: boolean);
    procedure SetFont(const Value: TFont);
    procedure SetDisabledFont(const Value: TFont);
    procedure SetSelectedFont(const Value: TFont);

    procedure FontChanged(Sender: TObject);
    procedure SetTabWidth(Value: Integer);
  protected
    procedure TBMThemeChange(var Message: TMessage); message TBM_THEMECHANGE;
    procedure MainWndProc(var Message: TMessage);
    procedure WndProc(var Message: TMessage);
    procedure PaintItem(Canvas: TCanvas; R: TRect; state: string; Orientation: TJvTabBarOrientation);
    procedure DrawBackground(Canvas: TCanvas; TabBar: TJvCustomTabBar; R: TRect); override;
    procedure DrawTab(Canvas: TCanvas; Tab: TJvTabBarItem; R: TRect); override;
    procedure DrawDivider(Canvas: TCanvas; LeftTab: TJvTabBarItem; R: TRect); override;
    procedure DrawMoveDivider(Canvas: TCanvas; Tab: TJvTabBarItem; MoveLeft: Boolean); override;
    procedure DrawScrollButton(Canvas: TCanvas; TabBar: TJvCustomTabBar; Button: TJvTabBarScrollButtonKind;
      State: TJvTabBarScrollButtonState; R: TRect); override;
    function GetDividerWidth(Canvas: TCanvas; LeftTab: TJvTabBarItem): Integer; override;
    function GetTabSize(Canvas: TCanvas; Tab: TJvTabBarItem): TSize; override;
    function GetCloseRect(Canvas: TCanvas; Tab: TJvTabBarItem; R: TRect): TRect; override;
    function Options: TJvTabBarPainterOptions; override;
  public
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
    procedure DefaultHandler(var Message); override;
    property Handle: HWnd read GetHandle;
  published
    property TabWidth: Integer read FTabWidth write SetTabWidth default 0;

    property Font: TFont read FFont write SetFont;
    property DisabledFont: TFont read FDisabledFont write SetDisabledFont;
    property SelectedFont: TFont read FSelectedFont write SetSelectedFont;

    property ToolbarBackground: boolean read FToolbarBackground write SetToolbarBackground default true;
    property AllowSelectedHot: boolean read FAllowSelectedHot write SetAllowSelectedHot default true;
  end;

procedure Register;

implementation

procedure Register;
begin
  RegisterComponents('mbTBX JVCL', [TJvmbTBXTabBarPainter]);
end;

procedure SetItemInfo(State: string; var IInfo: TTBXItemInfo);
begin
  FillChar(IInfo, SizeOf(TTBXItemInfo), 0);
  IInfo.ViewType := TVT_NORMALTOOLBAR;
  IInfo.ItemOptions := IO_TOOLBARSTYLE or IO_APPACTIVE;
  IInfo.IsVertical := False; 
  IInfo.Enabled := true;

  if State = 'normal' then
  begin
   IInfo.Pushed := False;
   IInfo.Selected := False;
   IInfo.HoverKind := hkNone;
  end;
  if State = 'hot' then
  begin
    IInfo.Pushed := False;
    IInfo.Selected := False;
    IInfo.HoverKind := hkMouseHover;
  end;
  if State = 'down' then
  begin
    IInfo.Pushed := True;
    IInfo.Selected := True;
    IInfo.HoverKind := hkMouseHover;
  end;
  if State = 'checked' then
  begin
    IInfo.Pushed := False;
    IInfo.Selected := True;
    IInfo.HoverKind := hkNone;
  end;
  if State = 'checked-hot' then
  begin
    IInfo.Pushed := False;
    IInfo.Selected := True;
    IInfo.HoverKind := hkMouseHover;
  end;
  if State = 'checked-down' then
  begin
    IInfo.Pushed := True;
    IInfo.Selected := True;
    IInfo.HoverKind := hkMouseHover;
  end;
end;

function GetBorderColor(state: string): TColor;
var
 Bmp: TBitmap;
 i: TTBXItemInfo;
begin
 Bmp:= TBitmap.Create;
 try
  Bmp.PixelFormat := pf32Bit;
  Bmp.Width := 19;
  Bmp.Height := 19;

  SetItemInfo(state, i);
  CurrentTheme.PaintBackgnd(BMP.Canvas, BMP.Canvas.ClipRect, BMP.Canvas.ClipRect, BMP.Canvas.ClipRect, CurrentTheme.GetViewColor(TVT_NORMALTOOLBAR), false, TVT_NORMALTOOLBAR);
  CurrentTheme.PaintButton(BMP.Canvas, BMP.Canvas.ClipRect, i);

  Result := Bmp.Canvas.Pixels[10, 0];
 finally
  Bmp.Free;
 end;
end;

{===========================}
{== TJvmbTBXTabBarPainter ==}
{===========================}

constructor TJvmbTBXTabBarPainter.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
  AddThemeNotification(Self);
  FFont := TFont.Create;
  FDisabledFont := TFont.Create;
  FSelectedFont := TFont.Create;

  FFont.Color := clWindowText;
  FDisabledFont.Color := clGrayText;
  FSelectedFont.Assign(FFont);

  FFont.OnChange := FontChanged;
  FDisabledFont.OnChange := FontChanged;
  FSelectedFont.OnChange := FontChanged;
  FTabWidth := 0;
  FToolbarBackground := true;
  FAllowSelectedHot := true;
  FHandle := 0;
end;

destructor TJvmbTBXTabBarPainter.Destroy;
begin
  FFont.Free;
  FDisabledFont.Free;
  FSelectedFont.Free;
  RemoveThemeNotification(Self);
  Classes.DeallocateHWnd(Handle);
  inherited Destroy;
end;

function TJvmbTBXTabBarPainter.GetHandle: HWnd;
begin
  if FHandle = 0 then FHandle := Classes.AllocateHWnd(MainWndProc);
  Result := FHandle
end;

procedure TJvmbTBXTabBarPainter.MainWndProc(var Message: TMessage);
begin
  try
    WndProc(Message)
  except
    on Exception do
      if Assigned(Application) then Application.HandleException(Self)
      else raise
  end
end;

procedure TJvmbTBXTabBarPainter.WndProc(var Message: TMessage);
begin
  Dispatch(Message)
end;

procedure TJvmbTBXTabBarPainter.DefaultHandler(var Message);
begin
  TMessage(Message).Result := DefWindowProc(Handle, TMessage(Message).Msg,
    TMessage(Message).wParam, TMessage(Message).lParam)
end;

procedure TJvmbTBXTabBarPainter.TBMThemeChange(var Message: TMessage);
begin
 if Message.WParam = TSC_VIEWCHANGE then
  Changed;
end;

procedure TJvmbTBXTabBarPainter.DrawBackground(Canvas: TCanvas; TabBar: TJvCustomTabBar; R: TRect);
begin
 with Canvas do
  begin
   if CurrentTheme.PaintDockBackground then
    CurrentTheme.PaintDock(Canvas, R, R, DP_TOP);
   if FToolbarBackground then
    CurrentTheme.PaintBackgnd(Canvas, r, r, r,currenttheme.GetViewColor(TVT_NORMALTOOLBAR), false, TVT_NORMALTOOLBAR);

   Pen.Color := GetBorderColor('checked');

   case TabBar.Orientation of
    toBottom:
     begin
      MoveTo(0, 0);
      LineTo(R.Right - 1, 0);
     end;
    toTop:
     begin
      MoveTo(0, R.Bottom - 1);
      LineTo(R.Right - 1, R.Bottom - 1);
     end;
   end;
  end;
end;

procedure PaintSeparator(Canvas: TCanvas; R: TRect);
var
 Bmp: TBitmap;
 si: TTBXItemInfo;
begin
 Bmp:= TBitmap.Create;
 try
  Bmp.PixelFormat := pf32Bit;
  Bmp.Width := 9;
  Bmp.Height := R.Bottom - R.Top;

  Bmp.Canvas.CopyRect(Bmp.Canvas.ClipRect, Canvas, R);

  SetItemInfo('normal', si);
  si.Enabled := true;
  si.ViewType := TVT_NORMALTOOLBAR;

  CurrentTheme.PaintSeparator(Bmp.Canvas, Bmp.Canvas.ClipRect, si, false, true);

  Canvas.Draw(R.Left, R.Top, Bmp);
 finally
  Bmp.Free;
 end;
end;

procedure TJvmbTBXTabBarPainter.DrawDivider(Canvas: TCanvas; LeftTab: TJvTabBarItem; R: TRect);
var
 SR: TRect;
begin
 if not LeftTab.Selected then
  begin
   if not Assigned(LeftTab.TabBar.SelectedTab) or (LeftTab.GetNextVisible <> LeftTab.TabBar.SelectedTab) then
   begin
    SR := Rect(R.Right - 1 - 4, R.Top + 3, R.Right - 1 + 4, R.Bottom - 3);
    PaintSeparator(Canvas, SR);
   end;
  end;
end;

procedure TJvmbTBXTabBarPainter.DrawMoveDivider(Canvas: TCanvas; Tab: TJvTabBarItem; MoveLeft: Boolean);
var
  R: TRect;
begin
  with Canvas do
  begin
    R := Tab.DisplayRect;
    Inc(R.Top, 4);
    Dec(R.Bottom, 2);
    if MoveLeft then
     begin
      Dec(R.Left);
      R.Right := R.Left + 4
     end
    else
     begin
      Dec(R.Right, 1);
      R.Left := R.Right - 4;
     end;
    SetItemInfo('checked', ii);
    CurrentTheme.PaintButton(Canvas, R, ii);
  end;
end;

procedure TJvmbTBXTabBarPainter.PaintItem(Canvas: TCanvas; R: TRect; state: string; Orientation: TJvTabBarOrientation);
var
 Bmp: TBitmap;
 A: TRect;
begin
 Bmp:= TBitmap.Create;
 try
  Bmp.PixelFormat := pf32Bit;
  Bmp.Width := R.Right - R.Left;
  Bmp.Height := R.Bottom - R.Top;
  A := BMP.Canvas.ClipRect;
  case Orientation of
   toTop: Inc(A.Bottom, 3);
   toBottom: Dec(A.Top, 3);
  end;

  SetItemInfo(state, ii);
  CurrentTheme.PaintBackgnd(BMP.Canvas, A, A, A, CurrentTheme.GetViewColor(TVT_NORMALTOOLBAR), false, TVT_NORMALTOOLBAR);
  CurrentTheme.PaintButton(BMP.Canvas, A, ii);

  Canvas.Draw(R.Left, R.Top, Bmp);
 finally
  Bmp.Free;
 end;
end;

procedure TJvmbTBXTabBarPainter.DrawTab(Canvas: TCanvas; Tab: TJvTabBarItem; R: TRect);
var
  CloseR: TRect;
begin
  with Canvas do
  begin

    if Tab.Selected then
    begin
     if Tab.Hot and FAllowSelectedHot then
      PaintItem(Canvas, R, 'checked-hot', Tab.TabBar.Orientation)
     else
      PaintItem(Canvas, R, 'checked', Tab.TabBar.Orientation);
    end;

    if Tab.Enabled and not Tab.Selected and Tab.Hot then
    begin
     Pen.Color := GetBorderColor('hot');
     case Tab.TabBar.Orientation of
      toTop:
       begin
        MoveTo(R.Left, R.Top);
        LineTo(R.Right - 1 - 1, R.Top);
       end;
      toBottom:
       begin
        MoveTo(R.Left, R.Bottom);
        LineTo(R.Right - 1 - 1, R.Bottom);
       end;
     end;
    end;

    if Tab.TabBar.CloseButton then
    begin
      if Tab.Selected then
       begin
        SetItemInfo('checked', ii);
        if Tab.Closing then SetItemInfo('checked-down', ii);
       end
      else
       begin
        SetItemInfo('hot', ii);
        if Tab.Closing then SetItemInfo('down', ii);        
       end;

       CloseR := GetCloseRect(Canvas, Tab, Tab.DisplayRect);
      CurrentTheme.PaintButton(Canvas, CloseR, ii);

      ii.Enabled := Tab.Enabled;
      Pen.Color := CurrentTheme.GetItemTextColor(ii);

      // close cross
      MoveTo(CloseR.Left + 3, CloseR.Top + 3);
      LineTo(CloseR.Right - 3, CloseR.Bottom - 3);
      MoveTo(CloseR.Left + 4, CloseR.Top + 3);
      LineTo(CloseR.Right - 4, CloseR.Bottom - 3);

      MoveTo(CloseR.Right - 4, CloseR.Top + 3);
      LineTo(CloseR.Left + 2, CloseR.Bottom - 3);
      MoveTo(CloseR.Right - 5, CloseR.Top + 3);
      LineTo(CloseR.Left + 3, CloseR.Bottom - 3);


      Brush.Color := CurrentTheme.GetItemColor(ii);
      // remove intersection
      if Tab.Modified then
       FillRect(Rect(CloseR.Left + 5, CloseR.Top + 4, CloseR.Right - 5, CloseR.Bottom - 4));

      R.Left := CloseR.Right;
    end;

    InflateRect(R, -1, -1);

    if not Tab.TabBar.CloseButton then
      Inc(R.Left, 2);

    if Tab.Selected then
    begin
     if Tab.Hot then
      SetItemInfo('checked-hot', ii)
     else
      SetItemInfo('checked', ii);
    end
    else
     if Tab.Hot then
      SetItemInfo('hot', ii)
     else
      SetItemInfo('normal', ii);

    if (Tab.ImageIndex <> -1) and (Tab.GetImages <> nil) then
    begin
     CurrentTheme.PaintImage(Canvas, Rect(R.Left+2, R.Top + (R.Bottom - R.Top - Tab.GetImages.Height) div 2,
          R.Left + Tab.GetImages.Width+2, R.Top + (R.Bottom - R.Top - Tab.GetImages.Height) div 2 + Tab.GetImages.Height),
          ii, Tab.GetImages, Tab.ImageIndex);

     Inc(R.Left, Tab.GetImages.Width + 2);
    end;

    if Tab.Enabled then
    begin
     if Tab.Selected then
      Font.Assign(Self.SelectedFont)
     else
      Font.Assign(Self.Font);
    end
    else
     Font.Assign(Self.DisabledFont);

    Brush.Style := bsClear;
    TextRect(R, R.Left + 3, R.Top + 3, Tab.Caption);
  end;
end;

procedure TJvmbTBXTabBarPainter.DrawScrollButton(Canvas: TCanvas; TabBar: TJvCustomTabBar; Button: TJvTabBarScrollButtonKind;
  State: TJvTabBarScrollButtonState; R: TRect);

 procedure PaintScroller(Canvas: TCanvas; R: TRect; IsLeft:boolean; ii: TTBXItemInfo);
 var
  X, Y, Sz: Integer;
  C: TColor;
 begin
  InflateRect(R, -2, -2);
  X := (R.Left + R.Right) div 2;
  Y := (R.Top + R.Bottom) div 2;
  Sz := Min(X - R.Left, Y - R.Top) * 3 div 4;
  c := CurrentTheme.GetItemTextColor(ii);
  if IsLeft then
   begin
    Inc(X, Sz div 2);
    PolygonEx(Canvas.Handle, [Point(X, Y + Sz), Point(X - Sz, Y), Point(X, Y - Sz)], C, C);
   end
  else
   begin
    X := (R.Left + R.Right - 1) div 2;
    Dec(X, Sz div 2);
    PolygonEx(Canvas.Handle, [Point(X, Y + Sz), Point(X + Sz, Y), Point(X, Y - Sz)], C, C);
   end;
 end;
var
 ii: TTBXItemInfo;
begin
 case State of
  sbsNormal, sbsDisabled: SetItemInfo('normal', ii);
  sbsHot: SetItemInfo('hot', ii);
  sbsPressed: SetItemInfo('down', ii);
 end;
 ii.Enabled := (State <> sbsDisabled);
 CurrentTheme.PaintButton(Canvas, R, ii);
 PaintScroller(Canvas, R, Button = sbScrollLeft, ii);
end;

function TJvmbTBXTabBarPainter.GetCloseRect(Canvas: TCanvas; Tab: TJvTabBarItem; R: TRect): TRect;
begin
  Result.Left := R.Left + 5;
  Result.Top :=  R.Top + 5;
  Result.Right := Result.Left + 12;
  Result.Bottom := Result.Top + 11;
end;

function TJvmbTBXTabBarPainter.GetDividerWidth(Canvas: TCanvas; LeftTab: TJvTabBarItem): Integer;
begin
  Result := 1;
end;

function TJvmbTBXTabBarPainter.GetTabSize(Canvas: TCanvas; Tab: TJvTabBarItem): TSize;
begin
  if Tab.Enabled then
  begin
    if Tab.Selected then
      Canvas.Font.Assign(SelectedFont)
    else
      Canvas.Font.Assign(Font)
  end
  else
    Canvas.Font.Assign(DisabledFont);

  Result.cx := Canvas.TextWidth(Tab.Caption) + 11;
  Result.cy := Canvas.TextHeight(Tab.Caption + 'Ag') + 7;
  if Tab.TabBar.CloseButton then
    Result.cx := Result.cx + 15;
  if (Tab.ImageIndex <> -1) and (Tab.GetImages <> nil) then
    Result.cx := Result.cx + Tab.GetImages.Width + 2;

  if TabWidth > 0 then
    Result.cx := TabWidth;
end;

function TJvmbTBXTabBarPainter.Options: TJvTabBarPainterOptions;
begin
  Result := [poPaintsHotTab];
end;

procedure TJvmbTBXTabBarPainter.FontChanged(Sender: TObject);
begin
  Changed;
end;

procedure TJvmbTBXTabBarPainter.SetTabWidth(Value: Integer);
begin
  if Value < 0 then
    Value := 0;
  if Value <> FTabWidth then
  begin
    FTabWidth := Value;
    Changed;
  end;
end;

procedure TJvmbTBXTabBarPainter.SetFont(const Value: TFont);
begin
  if Value <> FFont then
    FFont.Assign(Value);
end;

procedure TJvmbTBXTabBarPainter.SetDisabledFont(const Value: TFont);
begin
  if Value <> FDisabledFont then
    FDisabledFont.Assign(Value);
end;

procedure TJvmbTBXTabBarPainter.SetSelectedFont(const Value: TFont);
begin
  if Value <> FSelectedFont then
    FSelectedFont.Assign(Value);
end;

procedure TJvmbTBXTabBarPainter.SetToolbarBackground(Value: boolean);
begin
 if FToolbarBackground <> Value then
  begin
   FToolbarBackground := Value;
   Changed;
  end;
end;

procedure TJvmbTBXTabBarPainter.SetAllowSelectedHot(Value: boolean);
begin
 if FAllowSelectedHot <> Value then
  begin
   FAllowSelectedHot := Value;
   Changed;
  end;
end;

end.
