Dieses Verzeichnis enth�lt dynamische Einstellungen die statisch in die data.pak gebunden werden.
Dazu m�ssen in der settings.inc die Debugsymbole DEBUGMODE und INIDATA aktiviert sein. Das Laden der Einstellungen erfolgt in der Unit Defines \game\source\units\ mit der Prozedure ImportFile.
Diese Dateien m�ssen im settings-Verzeichnis des Installationspfades von X-Force liegen.