object MainForm: TMainForm
  Left = 1762
  Top = 426
  BorderIcons = [biSystemMenu, biMinimize]
  BorderStyle = bsSingle
  Caption = 'X-Force'
  ClientHeight = 539
  ClientWidth = 790
  Color = clBtnFace
  DefaultMonitor = dmPrimary
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Arial Rounded MT Bold'
  Font.Style = []
  OldCreateOrder = False
  Position = poMainFormCenter
  Scaled = False
  OnClose = FormClose
  OnCloseQuery = FormCloseQuery
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  OnKeyDown = FormKeyDown
  OnKeyPress = FormKeyPress
  OnKeyUp = FormKeyUp
  OnMouseWheelDown = FormMouseWheelDown
  OnMouseWheelUp = FormMouseWheelUp
  PixelsPerInch = 96
  TextHeight = 14
  object KD4HighScore: TKD4HighScore
    Left = 56
    Top = 28
  end
  object StartTimer: TTimer
    Enabled = False
    Interval = 100
    OnTimer = StartGame
  end
  object SaveGame: TKD4SaveGame
    Punkte = 0
    LongMessages = []
    MonthBilanz = False
    SaveDirectory = 'save\'
    OnUFOKampf = SaveGameUFOKampf
    Left = 28
  end
  object MessageHandler: TApplicationEvents
    OnActivate = MessageHandlerActivate
    OnDeactivate = MessageHandlerDeactivate
    OnException = MessageHandlerException
    OnMessage = MessageHandlerMessage
    Left = 28
    Top = 28
  end
  object Timer1: TTimer
    Enabled = False
    Interval = 50
    OnTimer = Timer1Timer
    Top = 28
  end
  object DXInput: TDXInput
    ActiveOnly = True
    Joystick.BindInputStates = True
    Joystick.Effects.Effects = {
      FF0A0044454C50484958464F524345464545444241434B454646454354003010
      7F000000545046301D54466F726365466565646261636B456666656374436F6D
      706F6E656E74025F3107456666656374730E01044E616D650607456666656374
      730A45666665637454797065070665744E6F6E6506506572696F64023205506F
      7765720310270454696D6503E8030E537461727444656C617954696D65020000
      000000}
    Joystick.Enabled = True
    Joystick.ForceFeedback = False
    Joystick.AutoCenter = True
    Joystick.DeadZoneX = 50
    Joystick.DeadZoneY = 50
    Joystick.DeadZoneZ = 50
    Joystick.ID = 0
    Joystick.RangeX = 1000
    Joystick.RangeY = 1000
    Joystick.RangeZ = 1000
    Keyboard.BindInputStates = True
    Keyboard.Effects.Effects = {
      FF0A0044454C50484958464F524345464545444241434B454646454354003010
      7F000000545046301D54466F726365466565646261636B456666656374436F6D
      706F6E656E74025F3107456666656374730E01044E616D650607456666656374
      730A45666665637454797065070665744E6F6E6506506572696F64023205506F
      7765720310270454696D6503E8030E537461727444656C617954696D65020000
      000000}
    Keyboard.Enabled = True
    Keyboard.ForceFeedback = False
    Keyboard.Assigns = {
      2600000000000000000000002800000000000000000000002500000000000000
      0000000027000000000000000000000052000000000000000000000054000000
      0000000000000000590000000000000000000000000000000000000000000000
      5800000000000000000000005A00000000000000000000006100000000000000
      0000000062000000000000000000000063000000000000000000000064000000
      0000000000000000660000000000000000000000670000000000000000000000
      6800000000000000000000006900000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000025000000000000000000000027000000000000000000000026000000
      0000000000000000280000000000000000000000570000000000000000000000
      5100000000000000000000004F00000000000000000000001B00000000000000
      00000000700000000000000000000000}
    Mouse.BindInputStates = False
    Mouse.Effects.Effects = {
      FF0A0044454C50484958464F524345464545444241434B454646454354003010
      7F000000545046301D54466F726365466565646261636B456666656374436F6D
      706F6E656E74025F3107456666656374730E01044E616D650607456666656374
      730A45666665637454797065070665744E6F6E6506506572696F64023205506F
      7765720310270454696D6503E8030E537461727444656C617954696D65020000
      000000}
    Mouse.Enabled = False
    Mouse.ForceFeedback = False
    UseDirectInput = True
    Left = 84
    Top = 28
  end
  object Arrows: TDXImageList
    Items = <
      item
        Name = 'SmallIcons'
        PatternHeight = 12
        PatternWidth = 12
        SystemMemory = True
        Transparent = True
        TransparentColor = clFuchsia
      end
      item
        Name = 'UIIcons'
        PatternHeight = 0
        PatternWidth = 0
        SystemMemory = False
        Transparent = True
        TransparentColor = clFuchsia
      end>
    Left = 112
  end
end
