{********************************************************************************
*										*
* Copyright (C) 2004  X-Force Team - jim_raynor@web.de				*
*										*
* This file is part of X-Force: Fight For Destiny (X-Force).			*
*										*
* X-Force is free software; you can redistribute it and/or modify		*
* it under the terms of the GNU General Public License as published by		*
* the Free Software Foundation; either version 2 of the License, or		*
* (at your option) any later version.						*
*										*
* X-Force is distributed in the hope that it will be useful,			*
* but WITHOUT ANY WARRANTY; without even the implied warranty of		*
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the			*
* GNU General Public License for more details.					*
*										*
* You should have received a copy of the GNU General Public License		*
* along with X-Force; if not, write to the Free Software			*
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA	*
*										*
*********************************************************************************
*										*
* Konstantendefinitionen							*
*										*
********************************************************************************}

{$I Settings.inc}

unit Defines;

interface

uses
  Graphics, XForce_types, ExtRecord, ArchivFile, Classes, SysUtils, Dialogs,
  ObjektRecords, StringConst;

procedure InitDefines;

const
  StartDate : TKD4Date = (Year: 2042; Month: 1; Week: 1;Day: 6; Hour: 0; Minute: 0);
  
  {$IFNDEF HAS_DELPHI6}
  MaxDouble : Double =  1.7e+308;
  {$ENDIF}


  { Semaphoreendefinition }
  KD4Semaphore     : PChar = LGameName+' Semaphore';

  { Name f�r Ressourcen }
  RNLogo           : String = 'KD4Logo';
  RNMouseCursor    : String = 'Mouse';
  RNNewGame        : String = 'NewGame';
  RNMessageBox     : String = 'MessageBox';
  RNGameMenu       : String = 'GameMenu';
  RNHelpIndex      : String = 'HilfeIndex';
  RNAlien          : String = 'Aliens';
  RNSkripte        : String = 'Skripts';
  RNEinsatzIntro   : String = 'EinsatzIntro';
  RNUFOPaedie      : String = 'UFOPaedie';
  RNEinsatzBild    : String = 'Einsatz';
  RNUFOs           : String = 'UFOs';
  RNSoldaten       : String = 'Soldaten';
  RNFinanzen       : String = 'Finanzen';
  RNBasisBau       : String = 'BasisBau';
  RNGesichter      : String = 'Soldatenliste';
  RNLabor          : String = 'Labor';
  RNWaffenSymbols  : String = 'WaffenListe';
  RNRWaffen        : String = 'RWaffen';
  RNEinrichtIcon   : String = 'Einrichtung';
  RNForschDat      : String = 'Forschung';
  RNNameDat        : String = 'FigurNamen';
  RNFixedNameDat   : String = 'FixedNames';
  RNOrganBMP       : String = 'Organ';
  RNKampfSchiffe   : String = 'SchiffeKS';
  RNLager          : String = 'Lager';
  RNHangar         : String = 'Hangar';
  RNOrganisations  : String = 'Organisationen';
  RNTownData       : String = 'TownData';
  RNArbeitsamt     : String = 'Arbeitsamt';
  RNSchiffe        : String = 'Schiffe';
  RNGSchiffe       : String = 'SchiffeGross';
  RNAlienList      : String = 'AlienList';
  RNSchiffSmall    : String = 'SchiffeKlein';
  RNSymbols        : String = 'Symbole';                                  
  RNExplo          : String = 'Explosion';
  RNRaketenSch     : String = 'RaketenSchweif';
  RNHighScore      : String = 'HighScore';
  RNPlayers        : String = 'Players';
  RNShields        : String = 'ShieldTypes';
  RNMouseMap       : String = 'MouseMap';
  RNEarthShem      : String = 'ErdeShema';
  RNEarthMini      : String = 'ErdeShemaMini';
  RNSaveGame       : String = 'SGHeaderRess';
  RNEinsatzSymbol  : String = 'EinsatzSymbole';
  RNShieldMask     : String = 'SchutzSchild';
  RNEarthMask      : String = 'ErdeMask';
  RNUFOKampfIntro  : String = 'UFOKampfIntro';
  RNHelpData       : String = 'HelpData';
  RNSoldatClasses  : String = 'SoldatClasses';

  { Definitionen f�r Dateien }
  FDataFile        : String    = 'data\'+LShortName+'.pak';
  FGameDataFile    : String    = 'data\data.pak';
  FUserDataFile    : String    = 'data\userdata.pak';
  FGameFile        : String    = 'data\GameSets\default.pak';
  FSoundFile       : String    = 'data\Sounds.pak';
  FHelpFile        : String    = 'data\Help.pak';
  FPlayerFile      : String    = 'data\Players.dat';
  FForschFile      : String    = 'data\Forsch.dat';
  FOrganFile       : String    = 'data\Orga.dat';
  FNamesFile       : String    = 'data\namen.dat';
  FHighFile        : String    = 'data\HighScore.dat';
  MMusicFile       : String    = '\music.ogg';
  MCreditsFile     : String    = '\Credits.dat';

  { Definitionen von Sound Index }
  SClick           = 'MouseClick';
  SOver            = 'MouseOver';
  SPageIn          = 'swish';
  SMessage         = 'MSG in';
  SScoreFill       = 'scrollfill';
  SScoreFillEnd    = 'scrollfillend';
  SExplosion       = 'Explosion';
  SLaserFire       = 'LaserFire';
  SRocketFire      = 'RocketFire';
  SPressKey        = 'Wert Enter';
  SInkrement       = 'Wert inc';
  SDekrement       = 'Wert dec';
  SMenuPopup       = 'Menu popup';

  { Schriftarten Definitionen }
  coFontName       : String    = 'Ms Sans serif';
  coFontColor      : TColor    = clYellow;
  coButtonColor    : TColor    = clWhite;
  coAccerlateColor : TColor    = clYellow;
  coFontSize       : Integer   = 8;

  { Definition von Farben }
  coFirstColor     : TColor    = clMaroon;
  coSecondColor    : TColor    = clRed;
  coScrollPen      : TColor    = clMaroon;
  coCheckBoxColor  : TColor    = clRed;
  coLineColor      : TColor    = clYellow;
  coAllianz        : TColor    = clLime;
  coFriendly       : TColor    = clGreen;
  coNeutral        : TColor    = clYellow;
  coUnFriendly     : TColor    = clMaroon;
  coEnemy          : TColor    = clRed;

  { ImageIndex LagerListe }
  IILLRWaffeP      = 0;
  IILLRWaffeR      = 1;
  IILLRWaffeL      = 2;
  IILLMotor        = 3;
  IILLShield       = 4;
  IILGranate       = 0;
  IILMine          = 1;
  IILMunitionP     = 2;
  IILMunitionR     = 3;
  IILMunitionC     = 4;
  IILWaffeP        = 5;
  IILWaffeR        = 6;
  IILWaffeL        = 7;
  IILWaffeC        = 8;
  IILPanzerung     = 9;
  IILRWaffeP       = 10;
  IILRWaffeR       = 11;
  IILRWaffeL       = 12;
  IILRMunitionP    = 2;
  IILRMunitionR    = 13;
  IILMotor         = 14;
  IILSensor        = 15;
  IILShield        = 16;

  { Definition der Pages }
  PageMainMenu     : Integer   = 1;
  PageNewGame      : Integer   = 2;
  PageGameMenu     : Integer   = 3;
  PageSettingGame  : Integer   = 4;
  PageCredits      : Integer   = 5;
  PageMonthEnd     : Integer   = 6;
  PageVerSoldat    : Integer   = 7;
  PageInfoBasis    : Integer   = 8;
  PageVerLabor     : Integer   = 9;
  PageInfoStatus   : Integer   = 10;
  PageWerkStatt    : Integer   = 11;
  PageNewProjekt   : Integer   = 12;
  PageUfoPadie     : Integer   = 13;
  PageVerLager     : Integer   = 14;
//  PageVerAuftrag   : Integer   = 15;
  PageVerMarkt     : Integer   = 16;
  PageVerBasis     : Integer   = 17;
  PageVerOrgan     : Integer   = 18;
  PageOptions      : Integer   = 19;
  PageVerSchiffe   : Integer   = 20;
  PageVerSchiff    : Integer   = 21;
  PageVerEinsatz   : Integer   = 22;
  PageUFOIntro     : Integer   = 23;
  PageUFOKampf     : Integer   = 24;
  PageFinanzHilfe  : Integer   = 25;
  PageKampfSimul   : Integer   = 26;
  PageVerUpgrades  : Integer   = 27;
  PageProjektHist  : Integer   = 28;
  PageKeyBoard     : Integer   = 29;
  PageEinsatzIntro : Integer   = 30;
  PageBodenEinsatz : Integer   = 31;
  PageEinsatzEnde  : Integer   = 32;
  PageSoldatConfig : Integer   = 33;

  { Wertmultiplikatoren }
  ForscherBuy      : Integer   = 126;
  ForscherSell     : Integer   = 104;
  TechnikerBuy     : Integer   = 125;
  TechnikerSell    : Integer   = 106;
  ProdAlphatron    : Single    = 0.658;
  ProdHours        : Single    = 0.356;

  TrainKost        : Integer   = 100;

  // Raumschiffe
  Benzinkosten     : Single    = 1.89;

  SensorMapWidth   : Integer   = 512;
  SensorMapHeight  : Integer   = 512;

  { Basisverwaltung }
  BaseCosts        : Array[TBaseType] of Integer = ({btBase}          500000,
                                                    {btAlphatronMine} 200000,
                                                    {btOutPost}       100000);

  MaxFloors        : Integer = 6; // 1 Obergeschoss + 5 Untergeschosse

  // Kosten bei Bau der neuen Ebenen
  FloorCosts       : Array[0..5] of Integer = ({Obergeschoss}          0,
                                               {1. Untergeschoss}      0,
                                               {2. Untergeschoss} 250000,
                                               {3. Untergeschoss} 500000,
                                               {4. Untergeschoss}1000000,
                                               {5. Untergeschoss}1500000);

  // Berechnung der Alphatron-Karte
  AlphatronHotSpotCount   : Integer = 1000;
  AlphatronRange          : Integer = 15;
  AlphatronMin            : Integer = 2;
  AlphatronAdd            : Integer = 2;
  AlphatronMax            : Integer = 30;

  // Geringste F�rderleistung in Prozent
  AlphatronMinMiningEffectiveness : Integer = 10;

  // Ben�tigte Techniker F�higkeiten f�r 100% F�rderleistung
  AlphatronMaxMiningEffectiveness : Integer = 250;

  // Anzahl der Tage, bis eine Einrichtung an Wert verliert
  DaysToDecValue          : Integer = 30;

  // Einrichtung verliert nach DaysToDecValue DecValuePercent % des Wertes
  DecValuePercent         : Integer = 25;

  // Einrichtung hat nach Bau InitValuePercent % des Kaufpreis als Wert
  InitValuePercent        : Integer = 75;

  // Grafiken f�r den Basisbau
  BackGroundTextureO  = 'default\BasisBau:Background';
  BackGroundTextureU  = 'default\BasisBau:Underground';
  BackGroundAlphatron = 'default\BasisBau:AlphatronBergwerk';

  { Progress Anzahl in StartPage }
  MaxProgess       : Integer   = 37;

  { Mousecursors }
  CZeiger          : TDXMouseCursor = (BeginFrame: 0;EndFrame: 27;AnimTime: 25;HotSpot: (X:0;Y:0));
  CAuswahl         : TDXMouseCursor = (BeginFrame:28;EndFrame: 40;AnimTime: 50;HotSpot: (X:15;Y:15));

  { Versionsnummer }
  KD4Version       : Integer = 903;
  MainVersion      : String = '903';

  RegFolder        : String = 'XForce';

  Language         : String = 'deutsch';

  AutosaveName     : String = '- AutoSave -';

  { UFOListe }
  UFOImages        : Integer = 6;
  AlienImages      : Integer = 13;
  UFOShootTime     : Integer = 250;

  // Einstellungen Bodeneinsatz
  RucksackSlots    = 16;
  Gravitation      = 9.80665;

  AniFrames        : Integer = 14;

  EPThrow          = 25;

  // Zeiteinheitenkosten
  TUMoveField           : Integer = 3;
  TUChangeView          : Integer = 1;
//  TUTakeItem            : Integer = 5;
  TUThrowItem           : Integer = 6;
  TUUseItem             : Integer = 2;
  {$IFNDEF DISABLEBEAM}
  TUBeam                : Integer = 30;
  {$ENDIF}

  TUTakeBoden           : Integer = 5;
  TUTakeRucksack        : Integer = 6;
  TUTakeFromGuertel     : Integer = 2;
  TUTakePanzerung       : Integer = 30;
  TUTakeGuertel         : Integer = 10;
  TUTakeMunition        : Integer = 3;
  TUTakeHand            : Integer = 2;

  TUPutBoden            : Integer = 1;
  TUPutRucksack         : Integer = 5;
  TUPutFromGuertel      : Integer = 2;
  TUPutPanzerung        : Integer = 30;
  TUPutGuertel          : Integer = 10;
  TUPutMunition         : Integer = 3;
  TUPutHand             : Integer = 2;

  // Parameter f�r den Bodeneinsatz zu den verschiedenen Schusstypen

                                                             {stNichtSchiessen} {stGezielt} {stSpontan} {stAuto} {stWerfen} {stBenutzen} {stSchlag} {stSchwingen} {stStossen}

  // Zeiteinheitenmultiplikator f�r die verschiedenen Schusstypen (0 = Schuss wird nicht �ber TGameFigure.DoShoot abgefeuert)
  TimeUnitsMultiplikator  : Array[TSchussType] of double  = (0,                 1,          2,          3,       0,         0,           1,         1.2,          1.4);

  // Nachladezeitmultiplikator
  ReloadTimeMultiplikator : Array[TSchussType] of Integer = (0,                 200,        100,        50,      0,         0,           100,       120,          150);

  // Treffsicherheitsmodifikator (Treff*Modifikator/100)
  TreffModifikator        : Array[TSchussType] of Integer = (0,                 100,        60,         25,      0,         0,           0,        0,            0);

  // Schadensmodifikator (in +/- %)
  StrengthModifikator     : Array[TSchussType] of Integer = (0,                 0,          0,          0,       0,         0,           0,         0,            +5);

  // Durchschlagsmodifikator (in +/- %)
  PowerModifikator        : Array[TSchussType] of Integer = (0,                 0,          0,          0,       0,         0,           0,         -10,          +10);


  // ben�tigte Durchschlagskraft f�r die Mauern
  BarnessPower          : Array[0..5] of Integer =
                          (150,           // unzerst�rbar
                           75,            // Stahl
                           50,            // Beton
                           25,            // Holz
                           15,            // Pappe
                           5);            // Kein Hinderniss

  // wird einmalig berechnet, wenn Ausr�stung im Rucksack verstaut wird
  TUUseRucksack         : Integer = 6;

  GroupSize    = 5;


  // Neu - Editor
  TileWidth        = 64;

  // H�he mit 3D effekt
  TileHeight       = 32;

  // Normal h�he (ohne 3D-Effekt)
  NormalTileHeight = 32;
  NormalTileWidth  = 64;

  // H�he eines Objektes
  ObjektHeight     = 111;

  // Versatz beim Zeichnen
  HalfTileWidth    = 32;
  HalfTileHeight   = 16;

  ZOffSet          = ObjektHeight-NormalTileheight;

  // Alt
  // TileWidth       = 49;
  // TileHeight      = 28;
  // HalfTileWidth   = 24;
  // HalfTileHeight  = 12;
  // NormalTileHeight = 24;
  // ZOffSet          = 35;
  // ObjektHeight     = 60;

  // Mauern
  WallWidth           = 40;
  Wallheight          = 98;

  Wall3DEffekt        = 8;

  ObjectOffSetX       = 7;
  ObjectOffSetY       = 36;
  OldObjectOffSetY    = 40;

  BottomZOffset       = 000;

  UnitShootHeight     = 35;

  ClippingWidth       = (HalfTileWidth*4);
  ClippingHeight      = (TileHeight*2)+WallHeight+60;

  ClippingX           = HalfTileWidth;
  ClippingY           = (TileHeight*2)+50;

  DrawingRangeX       = 150; 

  // Alt
  // WallWidth        = 25
  // WallHeight       = 51

  // Aufl�sung
  ScreenWidth         = 800;
  ScreenHeight        = 600;

  // Personenliste
  PersonImageWidth   : Integer = 40;
  PersonImageHeight  : Integer = 46;

  PersonSmallImageWidth : Integer = 25;
  PersonSmallImageHeight: Integer = 29;

  // Wird in InitDefines gef�llt
var
  CDPath              : String = '';
  ModelInfos          : Array of TModelInformation;

implementation

uses
  record_utils, TraceFile;

{$IFDEF INIDATA}
procedure ImportFile(Archiv: TArchivFile; FileName: String; Ressource: String);
var
  Stream: TMemoryStream;
begin
  if FileExists('settings\'+FileName) then
  begin
    Stream:=TMemoryStream.Create;
    Stream.LoadFromFile('settings\'+FileName);
    Archiv.ReplaceRessource(Stream,Ressource,true);
    Stream.Free;
    GlobalFile.Write('Ressource ('+Ressource+') importiert',Filename);
  end;
end;
{$ENDIF}

procedure InitDefines;
var
  Options   : TExtRecordDefinition;
  OptionsRec: TExtRecord;
  XMLData   : String;
  Models    : TRecordArray;
  Dummy     : Integer;

  Archiv    : TArchivFile;
  {$IFDEF INIDATA}
  List      : TStringList;
  Stream    : TMemoryStream;
  {$ENDIF}
begin
  if not FileExists(FDataFile) then
    exit;

  Options:=TExtRecordDefinition.Create('Options');
  Options.AddInteger('Ani-Height',low(Integer),high(Integer),81);
  Options.AddInteger('Ani-Width',low(Integer),high(Integer),86);
  Options.AddInteger('TodesAni-Frames',low(Integer),high(Integer),14);
  Options.AddInteger('Ani-OffSetX',low(Integer),high(Integer),21);
  Options.AddInteger('Ani-OffSetY',low(Integer),high(Integer),10);

  Options.AddInteger('TU-MoveField',low(Integer),high(Integer),3);
  Options.AddInteger('TU-ChangeView',low(Integer),high(Integer),1);
  Options.AddInteger('TU-ThrowItem',low(Integer),high(Integer),10);
  Options.AddInteger('TU-UseItem',low(Integer),high(Integer),10);
  {$IFNDEF DISABLEBEAM}
  Options.AddInteger('TU-Beam',low(Integer),high(Integer),30);
  {$ENDIF}
  Options.AddDouble('TU-Gezielt',0,100,1,3);
  Options.AddDouble('TU-Spontan',0,100,1,2);
  Options.AddDouble('TU-Auto',0,100,1,1);

  Options.AddInteger('BarnessPower[0]',low(Integer),high(Integer),150);
  Options.AddInteger('BarnessPower[1]',low(Integer),high(Integer),75);
  Options.AddInteger('BarnessPower[2]',low(Integer),high(Integer),50);
  Options.AddInteger('BarnessPower[3]',low(Integer),high(Integer),25);
  Options.AddInteger('BarnessPower[4]',low(Integer),high(Integer),15);
  Options.AddInteger('BarnessPower[5]',low(Integer),high(Integer),10);

  OptionsRec:=TExtRecord.Create(Options);
  Archiv:=TArchivFile.Create;
  {$IFDEF INIDATA}
  Archiv.OpenArchiv(FGameDataFile);
  ImportFile(Archiv,'credits.txt','CreditsText');
  ImportFile(Archiv,'classes.ini',RNSoldatClasses);
  ImportFile(Archiv,'models.xml','ModelData');
  ImportFile(Archiv,'pageinfos.xml','PageInfos');
  ImportFile(Archiv,'namen.txt','FixedNames');

  if FileExists('settings\xforce.ini') then
  begin
    GlobalFile.Write('Einstellungen neu eingelesen');
    List:=TStringList.Create;
    List.LoadFromFile('settings\xforce.ini');
    for Dummy:=0 to OptionsRec.ValueCount-1 do
    begin
      try
        case OptionsRec.Values[Dummy].ValueType of
          ervtInteger : OptionsRec.SetInteger(OptionsRec.Values[Dummy].ValueName,StrToInt(List.Values[OptionsRec.Values[Dummy].ValueName]));
          ervtDouble  : OptionsRec.SetDouble(OptionsRec.Values[Dummy].ValueName,StrToFloat(List.Values[OptionsRec.Values[Dummy].ValueName]));
          else
          begin
            raise Exception.Create('Nicht unterst�tzter Datentyp');
          end;
        end;
      except
        on E: Exception do
        begin
          ShowMessage('Option '+OptionsRec.Values[Dummy].ValueName+' kann nicht gelesen werden. '+E.Message);
        end;
      end;
    end;
    List.Free;

    Stream:=TMemoryStream.Create;
    OptionsRec.SaveToStream(Stream);
    Archiv.ReplaceRessource(Stream,'Settings');
    Stream.Free;
  end;
  {$ENDIF}
  Archiv.OpenArchiv(FGameDataFile,true);
  Archiv.OpenRessource('Settings');
  OptionsRec.LoadFromStream(Archiv.Stream);

  // Einstellungen laden
  TUMoveField:=OptionsRec.GetInteger('TU-MoveField');
  TUChangeView:=OptionsRec.GetInteger('TU-ChangeView');
  TUThrowItem:=OptionsRec.GetInteger('TU-ThrowItem');
  TUUseItem:=OptionsRec.GetInteger('TU-UseItem');
  {$IFNDEF DISABLEBEAM}
  TUBeam:=OptionsRec.GetInteger('TU-Beam');
  {$ENDIF}

  AniFrames:=OptionsRec.GetInteger('TodesAni-Frames');

  BarnessPower[0]:=OptionsRec.GetInteger('BarnessPower[0]');
  BarnessPower[1]:=OptionsRec.GetInteger('BarnessPower[1]');
  BarnessPower[2]:=OptionsRec.GetInteger('BarnessPower[2]');
  BarnessPower[3]:=OptionsRec.GetInteger('BarnessPower[3]');
  BarnessPower[4]:=OptionsRec.GetInteger('BarnessPower[4]');
  BarnessPower[5]:=OptionsRec.GetInteger('BarnessPower[5]');

  TimeUnitsMultiplikator[stGezielt]:=OptionsRec.Getdouble('TU-Gezielt');
  TimeUnitsMultiplikator[stSpontan]:=OptionsRec.Getdouble('TU-Spontan');
  TimeUnitsMultiplikator[stAuto]:=OptionsRec.Getdouble('TU-Auto');

  OptionsRec.Free;
  Options.Free;

  // Informationen zu den Models laden
  Archiv.OpenRessource('ModelData');
  SetString(XMLData, nil, Archiv.Stream.Size);
  Archiv.Stream.Read(Pointer(XMLData)^,Archiv.Stream.Size);

  Models:=ModelData.ReadRecordsFromXML(XMLData,'model','gamemodels');
  SetLength(ModelInfos,length(Models));
  for Dummy:=0 to high(Models) do
  begin
    with ModelInfos[Dummy],TExtRecord(Models[Dummy]) do
    begin
      imageset:=GetString('imageset');
      TorsoSize:=GetInteger('torsosize');
      FootSize:=GetInteger('footsize');
      HeadSize:=GetInteger('headsize');

      FootHeight:=GetInteger('footheight');
      TorsoHeight:=GetInteger('torsoheight');
      HeadHeight:=GetInteger('headheight');

      FootMaxZ:=GetInteger('footheight');
      TorsoMaxZ:=FootMaxZ+GetInteger('torsoheight');
      HeadMaxZ:=TorsoMaxZ+GetInteger('headheight');

      SelWidth:=GetInteger('selectionwidth');
      SelHeight:=GetInteger('selectionheight');

      AniHeight:=GetInteger('aniheight');
      AniWidth:=GetInteger('aniwidth');

      AniOffSetX:=GetInteger('anioffsetx');
      AniOffSetY:=GetInteger('anioffsety');
    end;
  end;

  record_utils_ClearArray(Models);
  
  Archiv.Free;
end;


end.
