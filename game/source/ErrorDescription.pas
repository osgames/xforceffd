unit ErrorDescription;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls;

type
  TfrmErrorDescription = class(TForm)
    Description: TMemo;
    Button1: TButton;
    Label1: TLabel;
    procedure Button1Click(Sender: TObject);
  private
    { Private-Deklarationen }
  public
    { Public-Deklarationen }
  end;

var
  frmErrorDescription: TfrmErrorDescription;

implementation

{$R *.DFM}

procedure TfrmErrorDescription.Button1Click(Sender: TObject);
begin
  Close;
end;

end.
