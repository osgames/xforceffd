Dieses Verzeichnis enth�lt ge�nderte Version von Komponenten von Drittanbietern. Um X-Force fehlerfrei kompilieren zu k�nnen m�ssen diese Dateien nach der Installation der entsprechenden Komponente ersetzt werden. Achtet dabei auf die Versionsangabe der Komponente, da es sein kann, dass es bereits neue Versionen gibt.

Pascal Script 3.0.3.53

uPSPreProcessor.pas 
  - korrigiert den Pr�Prozessor von Pascal Script, um auch in Verbindung im DEFINES innerhalb von IFDEF zu funktionieren
  
uPSCompiler.pas
  - Erm�glicht die Anzeige aller Funktionen und typen im Missionseditor
  
uPSRuntime.pas
  - korrigiert ein Problem mit WideStrings. Diese sind deaktiviert, da es mit Delphi 5 nicht unterst�tzt wird. Leider gab es einen Fehler beim dem die Deaktivierung der WideStrings nicht korrekt funktionierte.
  
uPSDebugger.pas
uPSUtils.pas

Diese Dateien m�ssen in das Source Verzeichnis kopiert werden, das im Installationsverzeichnis von Pascal Script liegt. DIE VORHANDENEN DATEIEN M�SSEN �BERSCHRIEBEN WERDEN.