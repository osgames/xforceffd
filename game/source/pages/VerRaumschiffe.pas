{********************************************************************************
*										*
* Copyright (C) 2004  X-Force Team - jim_raynor@web.de				*
*										*
* This file is part of X-Force: Fight For Destiny (X-Force).			*
*										*
* X-Force is free software; you can redistribute it and/or modify		*
* it under the terms of the GNU General Public License as published by		*
* the Free Software Foundation; either version 2 of the License, or		*
* (at your option) any later version.						*
*										*
* X-Force is distributed in the hope that it will be useful,			*
* but WITHOUT ANY WARRANTY; without even the implied warranty of		*
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the			*
* GNU General Public License for more details.					*
*										*
* You should have received a copy of the GNU General Public License		*
* along with X-Force; if not, write to the Free Software			*
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA	*
*										*
*********************************************************************************
*										*
* Verwaltungsbildschirm Raumschiffe						*
*										*
*********************************************************************************
*										*
* CVS-Informationen                                                             *
* Autor           $Author$
* CheckInDate     $Date$
* Tag/Branch      $Name$
* Revision        $Revision$
*										*
********************************************************************************}

{$I ../Settings.inc}

unit VerRaumschiffe;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  KD4Page,DXContainer, DXBitmapButton, Defines, DXListBox, DXLabel,
  Blending, KD4Utils, DXDraws, DXInfoFrame, DXItemInfo, XForce_types, RaumschiffList,
  VerRaumschiff, DirectDraw, DirectFont, Menus, StringConst;

type
  TVerRaumschiffe = class(TKD4Page)
  private
    fButtonRect : TRect;
    fAreaRect   : TRect;
    fSchiffSel  : boolean;
    fOldIndex   : Integer;
    fAktu       : boolean;
    { Private-Deklarationen }
  protected
    procedure DrawSchiff(Sender: TDXComponent;Surface: TDirectDrawSurface;const Mem: TDDSurfaceDesc;Rect: TRect; Index: Integer;Selected: boolean);
    procedure DrawModel(Sender: TDXComponent;Surface: TDirectDrawSurface;const Mem: TDDSurfaceDesc;Rect: TRect; Index: Integer;Selected: boolean);
    procedure OnTypChange(Sender: TObject);
    procedure OnSchiffChange(Sender: TObject);
    procedure ModelMouseUp(Sender: TObject;Button: TMouseButton;Shift: TShiftState;x,y: Integer);
    procedure BuildSchiff(Sender: TObject);
    procedure CancelBuildClick(Sender: TObject);
    procedure RenameSchiff(Sender: TObject);
    procedure SellSchiff(Sender: TObject);
    procedure SettingClick(Sender: TObject);
    procedure SchiffDblClick(Sender: TObject;Button: TMouseButton;Shift: TShiftState;x,y: Integer);
    procedure OnSelectedBasisChange(Sender: TObject);
    { Protected-Deklarationen }
  public
    InfoFrame      : TDXInfoFrame;
    RaumschiffInfo : TDXItemInfo;
    Kauf           : TDXListBox;
    Schiffe        : TDXListBox;
    Buildbutton    : TDXBitmapButton;
    SettingButton  : TDXBitmapButton;
    CancelBuild    : TDXBitmapButton;
    RenameButton   : TDXBitmapButton;
    SellButton     : TDXBitmapButton;
    constructor Create(Container: TDXContainer);override;
    procedure SetDefaults;override;
  end;

implementation

uses
  basis_api, game_api, ufopaedie_api;

{ TVerRaumschiffe }

procedure TVerRaumschiffe.BuildSchiff(Sender: TObject);
begin
  Container.Lock;
  try
    SaveGame.Raumschiffe.Build(Kauf.ItemIndex);
    Schiffe.Items.Add(IntToStr(SaveGame.Raumschiffe.Count-1));
    RenameButton.Enabled:=true;
    OnSchiffChange(Self);
  except
    on E: Exception do
    begin
      Container.UnLock;
      game_api_MessageBox(E.Message,CCancelAction);
      exit;
    end;
  end;
  Container.UnLock;
  Container.RedrawArea(fAreaRect,Container.Surface);
end;

procedure TVerRaumschiffe.CancelBuildClick(Sender: TObject);
var
  Result: Boolean;
  Index : Integer;
begin
  Result:=true;
  Index:=StrToInt(Schiffe.Items[Schiffe.ItemIndex]);

  if not (sabCancelWithOutRequest in SaveGame.Raumschiffe.Schiffe[Index].Abilities) then
    Result:=game_api_Question(Format(HBauAbbrechen,[SaveGame.Raumschiffe.Schiffe[Index].Name]),CBauAbbrechen);

  if Result then
  begin
    SaveGame.Raumschiffe.Schiffe[Index].CancelBuild;
    Container.IncLock;
    OnSelectedBasisChange(nil);
    Container.DecLock;
    InfoFrame.Redraw;
  end;
end;

constructor TVerRaumschiffe.Create(Container: TDXContainer);
begin
  inherited;
  Animation:=paRightToLeft;
  fAktu:=false;
  fOldIndex:=-1;

  SetPageInfo(StripHotKey(BSchiffe));

  { Informationsframe }
  InfoFrame:=TDXInfoFrame.Create(Self);
  with InfoFrame do
  begin
    SetRect(388,10,244,70);
    SetFont(Font);
    ColorSheme:=icsRed;
    Infos:=[ifCredits,ifHangar];
    SystemKeyChange:=true;
  end;

  { Raumschiffinformationen }
  RaumschiffInfo:=TDXItemInfo.Create(Self);
  with RaumschiffInfo do
  begin
    SetFont(Font);
    BorderColor:=coFirstColor;
    SetRect(508,54,284,469);
    Hint:=HRaumschiffInfo;
  end;

{ Ihre Raumschiffe }
  { ListBox }
  Schiffe:=TDXListBox.Create(Self);
  with Schiffe do
  begin
    SetRect(8,54,370,286);
    BorderColor:=coFirstColor;
    FocusColor:=coSecondColor;
    SetFont(Font);
    SetButtonFont(ScrollBarFont);
    FirstColor:=coScrollBrush;
    SecondColor:=coScrollPen;
    Hint:=HSchiffList;
    ItemHeight:=40;
    OwnerDraw:=true;
    OnDrawItem:=DrawSchiff;
    OnChange:=OnSchiffChange;
    OnDblClick:=SchiffDblClick;
    AlwaysChange:=true;
    HeadRowHeight:=17;
    HeadData:=MakeHeadData(LRaumschiffe,FSchiff,FSchiffe);
  end;

  { Ausr�sten }
  SettingButton:=TDXBitmapButton.Create(Self);
  with SettingButton do
  begin
    SetRect(388,54,110,28);
    FirstColor:=coFirstColor;
    SecondColor:=coSecondColor;
    AccerlateColor:=coAccerlateColor;
    SetButtonFont(Font);
    RoundCorners:=rcTop;
    Text:=BSetting;
    Hint:=HSetting;
    OnClick:=SettingClick;
  end;

  { Verkaufenbutton }
  SellButton:=TDXBitmapButton.Create(Self);
  with SellButton do
  begin
    SetRect(388,83,110,28);
    SetButtonFont(Font);
    FirstColor:=coFirstColor;
    SecondColor:=coSecondColor;
    AccerlateColor:=coAccerlateColor;
    OnClick:=SellSchiff;
    Hint:=HSellSchiff;
    RoundCorners:=rcNone;
    Text:=BSell;
  end;

  { Bau abbrechen }
  CancelBuild:=TDXBitmapButton.Create(Self);
  with CancelBuild do
  begin
    SetRect(388,83,110,28);
    FirstColor:=coFirstColor;
    SecondColor:=coSecondColor;
    AccerlateColor:=coAccerlateColor;
    SetButtonFont(Font);
    Text:=BCancel;
    RoundCorners:=rcNone;
    Hint:=HRCancelBuild;
    OnClick:=CancelBuildClick;
  end;

  { Umbennenbutton }
  RenameButton:=TDXBitmapButton.Create(Self);
  with RenameButton do
  begin
    SetRect(388,112,110,28);
    SetButtonFont(Font);
    FirstColor:=coFirstColor;
    SecondColor:=coSecondColor;
    AccerlateColor:=coAccerlateColor;
    OnClick:=RenameSchiff;
    RoundCorners:=rcBottom;
    Hint:=HRenameSchiff;
    Text:=BSRename;
  end;

{ Raumschifftypen }
  { ListBox }
  Kauf:=TDXListBox.Create(Self);
  with Kauf do
  begin
    SetRect(8,348,370,175);
    BorderColor:=coFirstColor;
    FocusColor:=coSecondColor;
    SetFont(Font);
    SetButtonFont(ScrollBarFont);
    FirstColor:=coScrollBrush;
    SecondColor:=coScrollPen;
    Hint:=HTypListe;
    ItemHeight:=40;
    OwnerDraw:=true;
    OnDrawItem:=DrawModel;
    OnChange:=OnTypChange;
    OnMouseUp:=ModelMouseUp;
    AlwaysChange:=true;
    HeadRowHeight:=17;
    HeadData:=MakeHeadData(LModels,FTyp,FTyps);
  end;

  { Raumschiff bauen }
  BuildButton:=TDXBitmapButton.Create(Self);
  with BuildButton do
  begin
    SetRect(388,348,110,28);
    FirstColor:=coFirstColor;
    SecondColor:=coSecondColor;
    AccerlateColor:=coAccerlateColor;
    SetButtonFont(Font);
    Text:=ST0501080001;
    OnClick:=BuildSchiff;
    Hint:=HBuildSchiff;
  end;

  UnionRect(fAreaRect,InfoFrame.ClientRect,Schiffe.ClientRect);
  UnionRect(fAreaRect,fAreaRect,RaumschiffInfo.ClientRect);

  UnionRect(fButtonRect,SettingButton.ClientRect,RenameButton.ClientRect);

  basis_api_RegisterChangeBaseHandler(OnSelectedBasisChange);
end;

procedure TVerRaumschiffe.DrawModel(Sender: TDXComponent;
  Surface: TDirectDrawSurface;const Mem: TDDSurfaceDesc; Rect: TRect; Index: Integer;
  Selected: boolean);
begin
  if Selected then
  begin
    Kauf.DrawSelection(Surface,Mem,Rect,bcMaroon);
  end;
  SaveGame.Raumschiffe.DrawIcon(Surface,Rect.Left+4,Rect.Top+4,SaveGame.Raumschiffe.Models[Index].WaffenZellen);
  YellowStdFont.Draw(Surface,Rect.Left+38,Rect.Top+4,Kauf.Items[Index]);
  WhiteStdFont.Draw(Surface,Rect.Left+38,Rect.Top+20,Format(FCredits,[SaveGame.Raumschiffe.Models[Index].KaufPreis/1]));
end;

procedure TVerRaumschiffe.DrawSchiff(Sender: TDXComponent;
  Surface: TDirectDrawSurface;const Mem: TDDSurfaceDesc; Rect: TRect; Index: Integer;
  Selected: boolean);
var
  Text    : String;
  Schaden : Integer;
  Font    : TDirectFont;
begin
  Index:=StrToInt(Schiffe.Items[Index]);
  if Selected then
  begin
    Schiffe.DrawSelection(Surface,Mem,Rect,bcMaroon);
  end;
  SaveGame.Raumschiffe.DrawIcon(Surface,Rect.Left+4,Rect.Top+4,SaveGame.Raumschiffe.Schiffe[Index].WaffenZellen);
  YellowStdFont.Draw(Surface,Rect.Left+38,Rect.Top+4,SaveGame.Raumschiffe.Schiffe[Index].Name);
  WhiteStdFont.Draw(Surface,Rect.Left+38,Rect.Top+20,SaveGame.Raumschiffe.Schiffe[Index].StatusText);
  Schaden:=SaveGame.Raumschiffe[Index].GetSchaden;
  Font:=YellowStdFont;
  case Schaden of
    66..100 : Font:=RedStdFont;
    33..65  : Font:=YellowStdFont;
    0..32   : Font:=LimeStdFont;
  end;
  Text:=Format(LSchaden,[Schaden]);
  Font.Draw(Surface,Rect.Right-8-Font.TextWidth(Text),Rect.Top+20,Text);
end;

procedure TVerRaumschiffe.ModelMouseUp(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; x, y: Integer);
begin
  if Button=mbRight then
    ufopaedie_api_ShowEntry(upeRaumschiff,Kauf.ItemIndex);
end;

procedure TVerRaumschiffe.OnSchiffChange(Sender: TObject);
var
  Ability: TAbilities;
begin
  if Schiffe.Items.Count=0 then
    exit;

  if (fOldIndex=Schiffe.ItemIndex) and fSchiffSel and (not fAktu) then
    exit;

  fOldIndex:=Schiffe.ItemIndex;
  Container.Lock;
  Ability:=SaveGame.Raumschiffe.Schiffe[StrToInt(Schiffe.Items[Schiffe.ItemIndex])].Abilities;
  RaumschiffInfo.Caption:=LRaumschiff;
  RaumschiffInfo.Raumschiff:=SaveGame.Raumschiffe.Schiffe[StrToInt(Schiffe.Items[Schiffe.ItemIndex])];
  CancelBuild.Visible:=sabCancelBuild in Ability;
  SellButton.Visible:=not CancelBuild.Visible;

  CancelBuild.Enabled:=CancelBuild.Visible;
  SellButton.Enabled:=sabSelling in Ability;
  SettingButton.Enabled:=true;

  RenameButton.Enabled:=true;
  Container.Unlock;
  Container.RedrawArea(fButtonRect,Container.Surface,false);
  RaumschiffInfo.Redraw;
  fSchiffSel:=true;
end;

procedure TVerRaumschiffe.OnSelectedBasisChange(Sender: TObject);
var
  Dummy: Integer;
begin
  if Container.ActivePage<>Self then
    exit;

  Container.Lock;
  Schiffe.ChangeItem(true);
  Schiffe.Items.Clear;
  for Dummy:=0 to SaveGame.Raumschiffe.Count-1 do
  begin
    if (SaveGame.Raumschiffe[Dummy].XCOMSchiff) and SaveGame.Raumschiffe[Dummy].InSelectedBasis then
      Schiffe.Items.Add(IntToStr(Dummy));
  end;
  Schiffe.ChangeItem(false);
  Schiffe.Items.OnChange(Self);
  Container.LoadGame(false);
  Schiffe.Redraw;
  fOldIndex:=-1;
  if Schiffe.Items.Count=0 then
  begin
    SellButton.Visible:=true;
    SellButton.Enabled:=false;
    CancelBuild.Enabled:=false;
    CancelBuild.Visible:=false;
    RenameButton.Enabled:=false;
    SettingButton.Enabled:=false;
    OnTypChange(Self);
  end
  else
    OnSchiffChange(Self);
  Buildbutton.Enabled:=SaveGame.Raumschiffe.ModelCount>0;
  Container.DecLock;
  Container.DoFlip;
end;

procedure TVerRaumschiffe.OnTypChange(Sender: TObject);
begin
  if Kauf.ItemIndex=-1 then exit;
  if (fOldIndex=Kauf.ItemIndex) and (not fSchiffSel) and (not fAktu) then exit;
  Container.Lock;
  fOldIndex:=Kauf.ItemIndex;
  fSchiffSel:=false;
  RaumschiffInfo.Caption:=LModel;
  RaumschiffInfo.RaumschiffTyp:=SaveGame.Raumschiffe.Models[Kauf.ItemIndex];
  Container.UnLock;
  RaumschiffInfo.Redraw;
end;

procedure TVerRaumschiffe.RenameSchiff(Sender: TObject);
var
  Name: String;
  Index : Integer;
begin
  Index:=StrToInt(Schiffe.Items[Schiffe.ItemIndex]);
  Name:=SaveGame.Raumschiffe.Schiffe[Index].Name;
  QueryText(CRename,20,Name,ctBlue);
  if Name='' then exit;
  SaveGame.Raumschiffe.Schiffe[Index].Name:=Name;
  Container.IncLock;
  RaumschiffInfo.Redraw;
  Container.DecLock;
  Schiffe.Redraw;
end;

procedure TVerRaumschiffe.SchiffDblClick(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; x, y: Integer);
begin
  SettingButton.DoClick;
end;

procedure TVerRaumschiffe.SellSchiff(Sender: TObject);
var
  Index  : Integer;
begin
  Index:=StrToInt(Schiffe.Items[Schiffe.ItemIndex]);
  if game_api_Question(Format(MSellSchiff,[SaveGame.Raumschiffe.Schiffe[Index].Name,SaveGame.Raumschiffe.Schiffe[Index].Model.VerKaufPreis/1]),CSellSchiff) then
  begin
    if SaveGame.Raumschiffe[Index].LagerRoomBelegt/10>basis_api_FreierLagerRaum then
    begin
      if not game_api_Question(MNoLagerRoom,CSellSchiff) then exit;
    end;
    Container.IncLock;
    SaveGame.Raumschiffe[Index].SellSchiff;
    OnSelectedBasisChange(Self);
    Container.DecLock;
    InfoFrame.Redraw;
  end;
end;

procedure TVerRaumschiffe.SetDefaults;
var
  Dummy: Integer;
begin
  InfoFrame.Reset;
  Kauf.Items.Clear;
  Schiffe.Items.Clear;
  fOldIndex:=-1;
  for Dummy:=0 to SaveGame.Raumschiffe.ModelCount-1 do Kauf.Items.Add(SaveGame.Raumschiffe.Models[Dummy].Name);
  for Dummy:=0 to SaveGame.Raumschiffe.Count-1 do
  begin
    if (SaveGame.Raumschiffe[Dummy].XCOMSchiff) and SaveGame.Raumschiffe[Dummy].InSelectedBasis then
      Schiffe.Items.Add(IntToStr(Dummy));
  end;
  if Schiffe.Items.Count=0 then
  begin
    SellButton.Enabled:=false;
    CancelBuild.Enabled:=false;
    CancelBuild.Visible:=false;
    RenameButton.Enabled:=false;
    SettingButton.Enabled:=false;
    OnTypChange(Self);
  end
  else
    OnSchiffChange(Self);
  Buildbutton.Enabled:=SaveGame.Raumschiffe.ModelCount>0;
end;

procedure TVerRaumschiffe.SettingClick(Sender: TObject);
begin
  RaumschiffIndex:=StrToInt(Schiffe.Items[Schiffe.ItemIndex]);
  ChangePage(PageVerSchiff);
end;

end.
