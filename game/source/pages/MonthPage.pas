{********************************************************************************
*										*
* Copyright (C) 2004  X-Force Team - jim_raynor@web.de				*
*										*
* This file is part of X-Force: Fight For Destiny (X-Force).			*
*										*
* X-Force is free software; you can redistribute it and/or modify		*
* it under the terms of the GNU General Public License as published by		*
* the Free Software Foundation; either version 2 of the License, or		*
* (at your option) any later version.						*
*										*
* X-Force is distributed in the hope that it will be useful,			*
* but WITHOUT ANY WARRANTY; without even the implied warranty of		*
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the			*
* GNU General Public License for more details.					*
*										*
* You should have received a copy of the GNU General Public License		*
* along with X-Force; if not, write to the Free Software			*
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA	*
*										*
*********************************************************************************
*										*
* Anzeige der Wochenübersicht (Finanzen)					*
*										*
********************************************************************************}

{$I ../Settings.inc}

unit MonthPage;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  KD4Page,DXBitmapButton, DXContainer, StringConst, Defines,DXLabel,DXMonthStatus;

type
  TMonthPage = class(TKD4Page)
  private
    { Private-Deklarationen }
  protected
    procedure CloseMonth(Sender: TObject);
    { Protected-Deklarationen }
  public
    CloseButton  : TDXBitmapButton;
    CaptionLabel : TDXLabel;
    MonthStatus  : TDXMonthStatus;
    constructor Create(Container: TDXContainer);override;
    procedure SetDefaults;override;
  end;

implementation

{ TMonthPage }

procedure TMonthPage.CloseMonth(Sender: TObject);
begin
  CloseModal;
end;

constructor TMonthPage.Create(Container: TDXContainer);
begin
  inherited;
  Animation:=paUpToBottom;

  { Monatsbilanz }
  MonthStatus:=TDXMonthStatus.Create(Self);
  MonthStatus.SetRect(8,42,624,396);

  { Schliessen Button }
  CloseButton:=TDXBitmapButton.Create(Self);
  with CloseButton do
  begin
    Text:=BClose;
    SetRect(522,400,110,28);
    FirstColor:=coFirstColor;
    SecondColor:=coSecondColor;
    AccerlateColor:=coAccerlateColor;
    OnClick:=CloseMonth;
    SetButtonFont(Font);
    EscButton:=true;
    Text:=BClose;
    Hint:=HCloseButton;
  end;

  { Monatsbilanz }
  CaptionLabel:=TDXLabel.Create(Self);
  SetFont(CaptionLabel.Font);
  CaptionLabel.Font.Size:=16;
  CaptionLabel.SetRect(0,8,640,25);
  CaptionLabel.AutoSize:=false;
  CaptionLabel.Alignment:=taCenter;
end;

procedure TMonthPage.SetDefaults;
begin
  CaptionLabel.Text:=SaveGame.GetSettlementWeek(LMonthStat);
  MonthStatus.Finanz:=SaveGame.MonthStatus;
end;

end.
