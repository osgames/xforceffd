{********************************************************************************
*										*
* Copyright (C) 2004  X-Force Team - jim_raynor@web.de				*
*										*
* This file is part of X-Force: Fight For Destiny (X-Force).			*
*										*
* X-Force is free software; you can redistribute it and/or modify		*
* it under the terms of the GNU General Public License as published by		*
* the Free Software Foundation; either version 2 of the License, or		*
* (at your option) any later version.						*
*										*
* X-Force is distributed in the hope that it will be useful,			*
* but WITHOUT ANY WARRANTY; without even the implied warranty of		*
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the			*
* GNU General Public License for more details.					*
*										*
* You should have received a copy of the GNU General Public License		*
* along with X-Force; if not, write to the Free Software			*
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA	*
*										*
*********************************************************************************
*										*
* Einsatzauswertung nach einem Bodeneinsatz					*
*										*
********************************************************************************}

{$I ../Settings.inc}

unit EinsatzAuswertung;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  KD4Page, DXContainer, DXBitmapButton, Defines, DXISOEngine, DirectFont, math,
  DXCanvas, DirectDraw, DXDraws, XForce_types, Blending, StringConst;

type
  TEinsatzAuswertungLine = record
    TextLine: Boolean;
    Font    : TDirectFont;
    Color   : TBlendColor;
    Left    : double;
    Frames  : Integer;
    LPunkte : Integer;
    Text    : String;
    Second  : String;
  end;

  TEinsatzAuswertung = class(TKD4Page)
  private
    fLines    : Array[0..12] of TEinsatzAuswertungLine;
    procedure CloseButtonClick(Sender: TObject);
    procedure DrawAuswertung(Sender: TDXComponent;Surface: TDirectDrawSurface;var Mem: TDDSurfaceDesc);
    function PerformFrame(Sender: TObject;Frames: Integer): boolean;
    { Private-Deklarationen }
  protected
    { Protected-Deklarationen }
  public
    CloseButton    : TDXBitmapButton;
    Canvas         : TDXCanvas;
    constructor Create(Container: TDXContainer);override;
    procedure SetDefaults;override;
    procedure PageShown;override;
    { Public-Deklarationen }
  end;

var
  Statistik: TEinsatzStatistik;

implementation

{ TEinsatzAuswertung }

procedure TEinsatzAuswertung.CloseButtonClick(Sender: TObject);
begin
  CloseModal;
end;

constructor TEinsatzAuswertung.Create(Container: TDXContainer);
begin
  inherited;

  { SchliessenButton }
  CloseButton:=TDXBitmapButton.Create(Self);
  with CloseButton do
  begin
    SetRect(679,424,110,28);
    FirstColor:=coFirstColor;
    EscButton:=true;
    SecondColor:=coSecondColor;
    AccerlateColor:=coAccerlateColor;
    SetButtonFont(Font);
    Text:=BClose;
    Hint:=HCloseButton;
    OnClick:=CloseButtonClick;
  end;

  Canvas:=TDXCanvas.Create(Self);
  Canvas.SetRect(10,30,790,370);
  Canvas.OnPaint:=DrawAuswertung;
end;

procedure TEinsatzAuswertung.DrawAuswertung(Sender: TDXComponent;
  Surface: TDirectDrawSurface; var Mem: TDDSurfaceDesc);
var
  Dummy: Integer;
  YPos : Integer;
begin
  YPos:=30;
  for Dummy:=0 to High(fLines) do
  begin
    with fLines[Dummy] do
    begin
      if TextLine then
      begin
        if Color<>$12345678 then
          BlendRectangle(Rect(round(Left),YPos,Round(Left)+(Canvas.Width-20),YPos+28),100,Color,Surface,Mem);

        Font.Draw(Surface,round(Left)+5,YPos+5,Text);
        if Second<>'' then
          Font.Draw(Surface,round(Left)+(Canvas.Width-25)-Font.TextWidth(Second),YPos+5,Second);

        inc(YPos,Font.TextHeight('pW')+10);
      end
      else
      begin
        Rectangle(Surface,Mem,round(Left),YPos,round(Left)+(Canvas.Width-20),YPos+5,bcOlive);
        BlendRectangle(Rect(round(Left+1),YPos+1,round(Left)+(Canvas.Width-21),YPos+3),150,bcYellow,Surface,Mem);

        inc(YPos,6);
      end;
    end;
  end;
end;

procedure TEinsatzAuswertung.PageShown;
begin
  Container.AddFrameFunction(PerformFrame,Self,25);
end;

function TEinsatzAuswertung.PerformFrame(Sender: TObject;
  Frames: Integer): boolean;
var
  Dummy: Integer;
begin
  if Container.ActivePage<>Self then
  begin
    Container.DeleteFrameFunction(PerformFrame,Self);
    exit;
  end;
  for Dummy:=0 to High(fLines) do
  begin
    with fLines[Dummy] do
    begin
      if Left<=15 then
        continue
      else if Left<200 then
      begin
        Left:=Left-((1/Frames)*40);
        if Left<=15 then
        begin
          Left:=15;
          Container.PlaySound(SScoreFillEnd);
        end;
        inc(Frames);
      end
      else
        Left:=Left-40;
    end;
  end;
  if fLines[High(fLines)].Left<=15 then
    Container.DeleteFrameFunction(PerformFrame,Self);
  Canvas.Redraw;
end;

procedure TEinsatzAuswertung.SetDefaults;
var
  CFont   : TFont;
  Dummy   : Integer;
  LStart  : Integer;
  Punkte  : Integer;
  Summe   : Integer;
begin
  CFont:=TFont.Create;
  SetFont(CFont);
  Summe:=0;
  with fLines[0] do
  begin
    CFont.Size:=25;
    case Statistik.EinsatzErgebnis of
      frWin:
      begin
        CFont.Color:=clLime;
        Text:=ST0309230001;
        Punkte:=200;
      end;
      frLoose:
      begin
        CFont.Color:=clRed;
        Text:=ST0309230002;
        Punkte:=-200;
      end;
      frCancel:
      begin
        CFont.Color:=clYellow;
        Text:=ST0309230003;
        Punkte:=0;
      end;
    end;
    Second:='';
    Font:=FontEngine.FindDirectFont(CFont,clBlack);
    Color:=$12345678;
    LPunkte:=High(Integer);
  end;
  with fLines[2] do
  begin
    CFont.Size:=13;
    CFont.Color:=clWhite;
    CFont.Style:=[fsBold];
    Font:=FontEngine.FindDirectFont(CFont,clBlack);
    Text:=fLines[0].Text+':';
    Second:=Format(ST0309230004,[Punkte]);
    inc(Summe,Punkte);
    LPunkte:=Punkte;
  end;
  with fLines[3] do
  begin
    Font:=FontEngine.FindDirectFont(CFont,clBlack);
    Text:=ST0309230005;
    Second:=Format(ST0309230010,[Statistik.AlienAbgeschossen.Anzahl/1,20,Statistik.AlienAbgeschossen.Punkte/1]);
    inc(Summe,Statistik.AlienAbgeschossen.Punkte);
    LPunkte:=Statistik.AlienAbgeschossen.Punkte;
  end;
  with fLines[4] do
  begin
    Font:=FontEngine.FindDirectFont(CFont,clBlack);
    Text:=ST0309230006;
    Second:=Format(ST0309230010,[Statistik.SolVerloren.Anzahl/1,-30,Statistik.SolVerloren.Punkte/1]);
    inc(Summe,Statistik.SolVerloren.Punkte);
    LPunkte:=Statistik.SolVerloren.Punkte;
  end;
  with fLines[5] do
  begin
    Font:=FontEngine.FindDirectFont(CFont,clBlack);
    Text:=ST0309230007;
    Second:=Format(ST0309230011,[Statistik.AusErbeutet.Anzahl/1,Statistik.AusErbeutet.Punkte/1]);
    inc(Summe,Statistik.AusErbeutet.Punkte);
    LPunkte:=Statistik.AusErbeutet.Punkte;
  end;
  with fLines[6] do
  begin
    Font:=FontEngine.FindDirectFont(CFont,clBlack);
    Text:=ST0309230008;
    Second:=Format(ST0309230011,[Statistik.AusVerloren.Anzahl/1,Statistik.AusVerloren.Punkte/1]);
    inc(Summe,Statistik.AusVerloren.Punkte);
    LPunkte:=Statistik.AusVerloren.Punkte;
  end;
  with fLines[7] do
  begin
    Font:=FontEngine.FindDirectFont(CFont,clBlack);
    Text:=ST0309230009;
    Second:=Format(ST0309230010,[Statistik.ohneVerlustBonus.Anzahl/1,15,Statistik.ohneVerlustBonus.Punkte/1]);
    inc(Summe,Statistik.ohneVerlustBonus.Punkte);
    LPunkte:=Statistik.ohneVerlustBonus.Punkte;
  end;
  with fLines[8] do
  begin
    Font:=FontEngine.FindDirectFont(CFont,clBlack);
    Text:=IITreff;
    Second:=Format(ST0309230012,[Statistik.TrefferBilanz.Anzahl/1,50,Statistik.TrefferBilanz.Punkte/1]);
    inc(Summe,Statistik.TrefferBilanz.Punkte);
    LPunkte:=Statistik.TrefferBilanz.Punkte;
  end;
  with fLines[10] do
  begin
    Font:=FontEngine.FindDirectFont(CFont,clBlack);
    Text:=ST0309230013;
    Second:=Format(ST0309230014,[Summe/1]);
    LPunkte:=Summe;
  end;

  with fLines[12] do
  begin
    Font:=FontEngine.FindDirectFont(CFont,clBlack);
    Text:=CR0507020001;
    Second:=Format(FCredits,[Statistik.Contributation/1]);
    LPunkte:=Summe;
  end;

  CFont.Free;
  LStart:=ScreenWidth;
  for Dummy:=0 to high(fLines) do
  begin
    with fLines[Dummy] do
    begin
      TextLine:=true;
      Frames:=1;
      Left:=LStart;
      if LPunkte=high(Integer) then
        Color:=$12345678
      else if LPunkte=0 then
        Color:=bcOlive
      else if LPunkte>0 then
        Color:=bcGreen
      else
        Color:=bcMaroon;
    end;
    LStart:=LStart+200;
  end;
  fLines[11].TextLine:=false;
  fLines[9].TextLine:=false;
  fLines[1].TextLine:=false;
end;

end.
