{********************************************************************************
*										*
* Copyright (C) 2004  X-Force Team - jim_raynor@web.de				*
*										*
* This file is part of X-Force: Fight For Destiny (X-Force).			*
*										*
* X-Force is free software; you can redistribute it and/or modify		*
* it under the terms of the GNU General Public License as published by		*
* the Free Software Foundation; either version 2 of the License, or		*
* (at your option) any later version.						*
*										*
* X-Force is distributed in the hope that it will be useful,			*
* but WITHOUT ANY WARRANTY; without even the implied warranty of		*
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the			*
* GNU General Public License for more details.					*
*										*
* You should have received a copy of the GNU General Public License		*
* along with X-Force; if not, write to the Free Software			*
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA	*
*										*
*********************************************************************************
*										*
* Anzeige der Informationen zum Einsatz						*
*										*
********************************************************************************}

{$I ../Settings.inc}

unit EinsatzIntro;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  KD4Page, DXContainer, DXDraws, RaumschiffList, EinsatzListe, KD4Utils,
  Defines, DXCanvas, DXBitmapButton, Blending, XForce_types,BodenEinsatz,
  DirectDraw, DXListBox, DirectFont, DXFrameBox, DXSoldatConfig, GameFigureManager,
  DXItemInfo, VerSoldatEinsatz, DXTextViewer, DXProgressBar, TraceFile,
  StringConst;

type
  TBodenEinsatzData = record
    Raumschiff: TRaumschiff;
    Einsatz   : TEinsatz;
  end;

  TEinsatzIntro = class(TKD4Page)
  private
    fKaempfe       : Array of TBodenEinsatzData;
    fClose         : Boolean;
    fParentPage    : Integer;
    fEinsatzPage   : TBodenEinsatz;
    fSolList       : TList;
    fAlienList     : TList;
    fSoldat        : Integer;
    fShowAusruest  : boolean;
    AktRaumschiff  : TRaumschiff;
    AktEinsatz     : TEinsatz;
    AktManager     : TGameFigureManager;
  protected
    procedure UebernehmeSoldaten;
    procedure CloseClick(Sender: TObject);
    procedure EchtZeitClick(Sender: TObject);
    procedure RundenClick(Sender: TObject);
    procedure ShowAusruesten(Sender: TObject);
    procedure PaintMissionInfo(Sender: TDXComponent;Surface: TDirectDrawSurface;var Mem: TDDSurfaceDesc);
    procedure SetRaumschiff;
    procedure StartEinsatz;
  public
    CloseButton    : TDXBitmapButton;
    EquipButton    : TDXBitmapButton;
    {$IFNDEF NOROUND}
    RundenButton   : TDXBitmapButton;
    {$ENDIF}
    {$IFNDEF HIDEECHTZEITBUTTON}
    EchtZeitButton : TDXBitmapButton;
    {$ENDIF}
    MissionInfo    : TDXTextViewer;
    Info           : TDXCanvas;
    LoadProgress   : TDXProgressBar;

    constructor Create(Container: TDXContainer);override;
    destructor destroy;override;

    function ReshowPage: boolean;override;
    procedure AddEinsatz(Raumschiff: TRaumschiff; Einsatz: TEinsatz);
    procedure BerechneEinsatze;
    procedure SetDefaults;override;

    procedure IncProgress;

    property ParentPage  : Integer read fParentPage write fParentPage;
    property EinsatzPage : TBodenEinsatz read fEinsatzPage write fEinsatzPage;
  end;

  procedure IncEinsatzLoadProgress;

implementation

uses
  DXISOEngine;
  
var
  gEinsatzIntro: TEinsatzIntro = nil;

procedure IncEinsatzLoadProgress;
begin
  if gEinsatzIntro<>nil then
  begin
    gEinsatzIntro.IncProgress;
    GlobalFile.Write('Progress',gEinsatzIntro.LoadProgress.Value);
  end;
end;

{ TEinsatzIntro }

procedure TEinsatzIntro.AddEinsatz(Raumschiff: TRaumschiff;
  Einsatz: TEinsatz);
var
  Index: Integer;
begin
  Index:=length(fKaempfe);
  SetLength(fKaempfe,Index+1);
  fKaempfe[Index].Raumschiff:=Raumschiff;
  fKaempfe[Index].Einsatz:=Einsatz;
end;

procedure TEinsatzIntro.BerechneEinsatze;
var
  Dummy      : Integer;
begin
  SetLength(fKaempfe,0);
  for Dummy:=0 to SaveGame.Raumschiffe.Count-1 do
  begin
    with SaveGame.Raumschiffe[Dummy] do
    begin
      if (Status in [ssAir,ssFlyToEinsatz]) and (sabBodenKampf in Abilities) then
      begin
        if (Einsatz<>nil) and (CalculateEntfern(Position,Einsatz.Position)=0) then
        begin
          AddEinsatz(SaveGame.Raumschiffe[Dummy],SaveGame.Raumschiffe[Dummy].Einsatz);
        end;
      end;
    end;
  end;
  if length(fKaempfe)>0 then ChangePage(PageEinsatzIntro);
end;

procedure TEinsatzIntro.CloseClick(Sender: TObject);
begin
  if not ReshowPage then
  begin
    ChangePage(fParentPage);
    exit;
  end;
  Info.Redraw;
end;

constructor TEinsatzIntro.Create(Container: TDXContainer);
begin
  inherited;
  Animation:=paLeftToRight;
  fSolList:=TList.Create;
  fAlienList:=TList.Create;

  { SchliessenButton }
  CloseButton:=TDXBitmapButton.Create(Self);
  with CloseButton do
  begin
    SetRect(674,520,110,28);
    FirstColor:=coFirstColor;
    EscButton:=true;
    SecondColor:=coSecondColor;
    AccerlateColor:=coAccerlateColor;
    SetButtonFont(Font);
    RoundCorners:=rcRightBottom;
    Text:=BCancel;
    Hint:=HCloseButton;
    OnClick:=CloseClick;
  end;

  { Echtzeit Button }
  {$IFNDEF HIDEECHTZEITBUTTON}
  EchtZeitButton:=TDXBitmapButton.Create(Self);
  with EchtZeitButton do
  begin
    Text:=BEchtZeit;
    SetRect(563,520,110,28);
    FirstColor:=coFirstColor;
    SecondColor:=coSecondColor;
    AccerlateColor:=coAccerlateColor;
    {$IFDEF NOROUND}
    RoundCorners:=rcLeftBottom;
    {$ELSE}
    RoundCorners:=rcNone;
    {$ENDIF}
    SetButtonFont(Font);
    Hint:=HEchtzeit;
    OnClick:=EchtzeitClick;
  end;
  {$ENDIF}

  {$IFNDEF NOROUND}
  { Runden Button }
  RundenButton:=TDXBitmapButton.Create(Self);
  with RundenButton do
  begin
    Text:=BRunden;
    {$IFDEF HIDEECHTZEITBUTTON}
    SetRect(563,520,110,28);
    {$ELSE}
    SetRect(452,520,110,28);
    {$ENDIF}
    FirstColor:=coFirstColor;
    SecondColor:=coSecondColor;
    AccerlateColor:=coAccerlateColor;
    RoundCorners:=rcLeftBottom;
    SetButtonFont(Font);
    Hint:=HRunden;
    OnClick:=RundenClick;
  end;
  {$ENDIF}

  { Ausrüsten Button }
  EquipButton:=TDXBitmapButton.Create(Self);
  with EquipButton do
  begin
    Text:=BAusruesten;
    SetRect(16,520,110,28);
    FirstColor:=coFirstColor;
    SecondColor:=coSecondColor;
    AccerlateColor:=coAccerlateColor;
    RoundCorners:=rcBottom;
    SetButtonFont(Font);
    Hint:=HAusruestenEin;
    OnClick:=ShowAusruesten;
  end;

  { MissionsInfo }
  Info:=TDXCanvas.Create(Self);
  with Info do
  begin
    SetFont(Font);
    SetRect(16,16,768,503);
    OnPaint:=PaintMissionInfo;
  end;

  { MissionsInfo }
  MissionInfo:=TDXTextViewer.Create(Self);
  with MissionInfo do
  begin
    SetFont(Font);
    Font.Color:=clWhite;
    BorderColor:=clMaroon;
    ShowBorder:=false;
    RegisterFont('white',WhiteStdFont);
    RegisterFont('whitebold',WhiteBStdFont);
    RegisterFont('yellow',YellowStdFont);
    RegisterFont('yellowbold',YellowBStdFont);
    SetRect(21,61,400,440);
    RoundCorners:=rcNone;
  end;

  LoadProgress:=TDXProgressBar.Create(Self);
  with LoadProgress do
  begin
    SetRect(32,450,736,25);
    Kind:=sbHorizontal;
    BorderColor:=bcMaroon;
    FillColor:=bcMaroon;
    Visible:=false;
  end;

  gEinsatzIntro:=Self;
end;

procedure TEinsatzIntro.PaintMissionInfo(Sender: TDXComponent;Surface: TDirectDrawSurface;var Mem: TDDSurfaceDesc);
var
  Dummy  : Integer;
  STop   : Integer;
begin
  if AlphaElements then
    BlendRoundRect(CorrectBottomOfRect(Info.ClientRect),100,bcMaroon,Surface,Mem,11,[cRightTop,cLeftTop],Info.ClientRect);

  FramingRect(Surface,Mem,Info.ClientRect,[cRightTop,cLeftTop],11,bcMaroon);
  with Info do
  begin
    Font.Size:=24;
    Font.Style:=[fsBold];
    FontEngine.TextOut(Font,Surface,Left+4,Top+4,AktEinsatz.Name,clBlack);
    FontEngine.TextOut(Font,Surface,Left+438,Top+4,AktRaumschiff.Name,clBlack);

    STop:=Top+73;
    for Dummy:=0 to fSolList.Count-1 do
    begin
      WhiteStdFont.Draw(Surface,Left+438,STop,TSoldatManager(fSolList[Dummy]).Name);
      inc(STop,18);
    end;
    YellowStdFont.Draw(Surface,Left+436,Top+48,LEinsatzTrupp);
  end;
end;

function TEinsatzIntro.ReshowPage: boolean;
var
  Delete : boolean;
  Dummy  : Integer;

  procedure DeleteFirst;
  var
    Dummy: Integer;
  begin
    for Dummy:=0 to length(fKaempfe)-2 do fKaempfe[Dummy]:=fKaempfe[Dummy+1];
    SetLength(fKaempfe,length(fKaempfe)-1);
  end;

begin
  if fShowAusruest then
  begin
    result:=true;
    exit;
  end;
  Container.Lock;

  for Dummy:=0 to fAlienList.Count-1 do
    TObject(fAlienList[Dummy]).Free;
    
  Container.Unlock;
  fSolList.Clear;
  fAlienList.Clear;
  
  Delete:=length(fKaempfe)>0;
  while Delete do
  begin
    DeleteFirst;
    if length(fKaempfe)>0 then
    begin
      Delete:=not (AktRaumschiff.Status in [ssFlyToEinsatz,ssAir]);
    end
    else Delete:=false;
  end;
  if length(fKaempfe)>0 then
  begin
    Container.IncLock;
    SetRaumschiff;
    Container.DecLock;
    result:=true;
  end
  else
  begin
    result:=false;
    Container.LockAll;
    fClose:=true;
    Container.ShowCursor(true);
  end;
end;

procedure TEinsatzIntro.RundenClick(Sender: TObject);
begin
  fEinsatzPage.Echtzeit:=false;
  StartEinsatz;
end;

procedure TEinsatzIntro.SetDefaults;
begin
  fShowAusruest:=false;
  fClose:=false;
  SetRaumschiff;
  Container.PlayMusicCategory('Bodeneinsatz - Ankunft');

  CloseButton.Enabled:=true;
  RundenButton.Enabled:=true;
  EquipButton.Enabled:=true;

  LoadProgress.Visible:=false;

  LoadProgress.Max:=14+length(ImageNames);

  LoadProgress.Value:=0;
end;

procedure TEinsatzIntro.EchtzeitClick(Sender: TObject);
begin
  fEinsatzPage.Echtzeit:=true;
  StartEinsatz;
end;

procedure TEinsatzIntro.SetRaumschiff;
var
  Dummy  : Integer;
  Soldat : PSoldatInfo;
  Index  : Integer;
  AliObj : TAlienManager;
begin
  AktEinsatz:=fKaempfe[0].Einsatz;
  AktRaumschiff:=fKaempfe[0].Raumschiff;
  Container.Lock;

  // Soldaten im Raumschiff zum Einsatz hinzufügen
  for Dummy:=0 to AktRaumschiff.SoldatenSlots-1 do
  begin
    Index:=AktRaumschiff.GetSoldat(Dummy,Soldat);
    if Index<>-1 then
      fSolList.Add(SaveGame.SoldatenListe.Managers[Index]);
  end;

  // Aliens zum Einsatz hinzufügen
  for Dummy:=0 to AktEinsatz.AlienCount-1 do
  begin
    AliObj:=TAlienManager.Create(AktEinsatz.AddrOfAlien(Dummy));
    AliObj.FigureStatus:=fsEnemy;
    AliObj.FigureMap:=1;
    fAlienList.Add(AliObj);
  end;
  AktManager:=TSoldatManager(fSolList[0]);
  fSoldat:=0;
  Container.UnLock;

  // Missionsbeschreibung
  MissionInfo.Text:=AktEinsatz.GetMissionInfo;

end;

destructor TEinsatzIntro.destroy;
begin
  fSolList.Clear;
  while fAlienList.Count>0 do
  begin
    TSoldatManager(fAlienList[0]).Free;
    fAlienList.Delete(0);
  end;
  fSolList.Free;
  fAlienList.Free;
  inherited;
end;

procedure TEinsatzIntro.UebernehmeSoldaten;
var
  Dummy: Integer;
begin
  with EinsatzPage.ManagerListe do
  begin
    Clear;
    for Dummy:=0 to fSolList.Count-1 do
      EinsatzPage.ManagerListe.Add(fSolList[Dummy]);

    for Dummy:=0 to fAlienList.Count-1 do
      EinsatzPage.ManagerListe.Add(fAlienList[Dummy]);
  end;
end;

procedure TEinsatzIntro.ShowAusruesten(Sender: TObject);
begin
  fShowAusruest:=true;
  
  Managers:=fSolList;
  ActiveManager:=TSoldatManager(fSolList[0]);
  ChangePage(PageSoldatConfig);

  fShowAusruest:=false;
end;

procedure TEinsatzIntro.StartEinsatz;
begin
  Container.Inclock;
  Container.ShowCursor(false);
  LoadProgress.Visible:=true;
  CloseButton.Enabled:=false;
  RundenButton.Enabled:=false;
  EquipButton.Enabled:=false;
  Container.DecLock;
  Container.Flip;

  SaveGame.SaveGame(AutosaveName,true);
  IncProgress;

  UebernehmeSoldaten;
  IncProgress;

  fEinsatzPage.Einsatz:=AktEinsatz;
  fEinsatzPage.Raumschiff:=AktRaumschiff;
  fEinsatzPage.LoadData;

  Container.ShowCursor(true);
  ChangePage(PageBodenEinsatz);
end;

procedure TEinsatzIntro.IncProgress;
begin
  LoadProgress.Value:=LoadProgress.Value+1;
  Container.Flip;
end;

end.
