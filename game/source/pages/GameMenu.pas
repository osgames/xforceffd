{********************************************************************************
*										*
* Copyright (C) 2004  X-Force Team - jim_raynor@web.de				*
*										*
* This file is part of X-Force: Fight For Destiny (X-Force).			*
*										*
* X-Force is free software; you can redistribute it and/or modify		*
* it under the terms of the GNU General Public License as published by		*
* the Free Software Foundation; either version 2 of the License, or		*
* (at your option) any later version.						*
*										*
* X-Force is distributed in the hope that it will be useful,			*
* but WITHOUT ANY WARRANTY; without even the implied warranty of		*
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the			*
* GNU General Public License for more details.					*
*										*
* You should have received a copy of the GNU General Public License		*
* along with X-Force; if not, write to the Free Software			*
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA	*
*										*
*********************************************************************************
*										*
* Ansicht mit Geoscape (Spielmen�)						*
*										*
********************************************************************************}

{$I ../Settings.inc}

unit GameMenu;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  KD4Page, DXContainer, DXBitmapButton, Defines, XForce_types, DXDraws, DXLabel,
  DXPager, Blending, ArchivFile, TraceFile, DXInfoFrame, UFOList, DXTakticScreen,
  DXListBoxWindow, RaumschiffList, EinsatzListe, KD4Utils, DXListBox, BasisListe,
  DirectDraw, DirectFont, DXGameMessage, OrganisationList, TakticPlugins,
  KD4SaveGame, VerRaumschiff, StringConst;

type
  TGameMessageArray = Array of TGameMessage;

  TGameMenu = class(TKD4Page)
  private
    fTime          : Integer;
    fTimeMode      : Integer;
    fMessageBuffer : TGameMessageArray;

    // Plugins f�r den TakticScreen
    fTimeDrawer      : TTakticTimeDrawer;
    fTakticButtons   : TTakticButtons;
    fTakticMessages  : TTakticMessages;

    function FrameNextRound(Sender: TObject;Frames: Integer): boolean;
    procedure SaveButtonClick(Data: Integer);
    procedure ShowBufferedMessages;
    procedure AbfangenClick(Sender: TObject);
    procedure SelectObject(Objekt: TObject);
    procedure TownSelect(Town: TTown);
    procedure UFOSelect(UFO: TUFO);
    procedure EinsatzSelect(Einsatz: TEinsatz);
    procedure BasisSelect(Basis: TBasis);
    procedure PositionSelect(Point: TFloatPoint);
    procedure RaumschiffSelect(Raumschiff: TRaumschiff);
    function Multiple(Objekts: TStringList): Integer;

    procedure PlayMusic;

    procedure ShowDangerMessage(Sender: TObject);

    // Nachrichten speichern
    procedure SaveCustomSaveData(Stream: TStream);
    procedure LoadCustomSaveData(Stream: TStream);

    procedure NewGameHandler(Sender: TObject);
    procedure StartGameHandler(Sender: TObject);

    procedure MessageHandler(Sender: TObject);

    procedure ChangeSaveHandler(Sender: TObject);

    procedure DeleteMessageObject(Sender: TObject);
    { Private-Deklarationen }
  protected
    fPage          : Integer;
    fSelected      : TObject;
    fSaveButton    : Integer;
    procedure AktuTimeDrawer;
    procedure SetSaveGame(const Value: TKD4SaveGame);override;
  public
    TakticScreen   : TDXTakticScreen;
    ListBoxWindow  : TDXListBoxWindow;
    Abfangen       : TDXBitmapButton;
    GameMessage    : TDXGameMessage;
    constructor Create(Container: TDXContainer);override;
    destructor Destroy;override;
    procedure DeactivatePage;override;
    procedure SetDefaults;override;
    procedure PageShown;override;
    procedure LeaveModal;override;
    procedure BufferMessage(MessageInfo: TGameMessage);
    procedure SetTime(Time: Integer);
    procedure AddMessage(Text: String; Date: String);

    procedure ClearMessages;

    procedure ShowGameMessage(var MessageInfo: TGameMessage);
  end;

var
  AlwaysFastTime: Boolean = false;

implementation

uses
  savegame_api, game_api, highscore_api, forsch_api, ufopaedie_api, ForschList,
  lager_api, basis_api, raumschiff_api, soldaten_api, GameFigureManager,
  array_utils;

{ TGameMenu }

procedure TGameMenu.DeactivatePage;
begin
  SetTime(0);
end;

constructor TGameMenu.Create(Container: TDXContainer);
begin
  inherited;
  Animation:=paUpToBottom;
  fPage:=0;

  SetPageInfo(CR0506060001);

  TakticScreen:=TDXTakticScreen.Create(Self);
  with TakticScreen do
  begin
    SetRect(0,0,ScreenWidth,ScreenHeight);
    OnSelectUFO:=UFOSelect;
    OnSelectBasis:=BasisSelect;
    OnMultipleObjekts:=Multiple;
    OnSelectObject:=SelectObject;
    OnSelectEinsatz:=EinsatzSelect;
    OnSelectPosition:=PositionSelect;
    OnSelectSchiff:=RaumschiffSelect;
    OnSelectTown:=TownSelect;

    OnHunting:=AbfangenClick;
//    Visible:=false;
  end;

  { Listbox Fenster zur Auswahl bei mehreren Objekten }
  ListBoxWindow:=TDXListBoxWindow.Create(Self);
  with ListBoxWindow do
  begin
    SetRect(0,0,300,200);
    Caption:=CChooseObjekt;
    SetButtonFont(Font);
    CaptionColor:=coFontColor;
    AccerlateColor:=coAccerlateColor;
    Font.Size:=coFontSize;
    FirstColor:=$00400000;
    SecondColor:=clBlue;
    BlendColor:=bcDarkNavy;
    SetOwnerDraw(36,4,TakticScreen.DrawListItem);
  end;

  GameMessage:=TDXGameMessage.Create(Self);
  SetButtonFont(GameMessage.Font);

  //****************************************************************/
  //                                                                /
  //   Plugins f�r den Takticscreen                                 /
  //                                                                /
  //****************************************************************/

  // Zeitanzeige
  fTimeDrawer:=TTakticTimeDrawer.Create(TakticScreen);
  fTimeDrawer.Left:=10;
  fTimeDrawer.Top:=10;
  fTimeDrawer.OnSetTime:=SetTime;

  // Navigationsbutton hinzuf�gen
  fTakticButtons:=TTakticButtons.Create(TakticScreen);
  fTakticButtons.AddNavigationButton(BVerBasis,ChangePage,PageVerBasis);
  fTakticButtons.AddNavigationButton(BSchiffe,ChangePage,PageVerSchiffe);
  fTakticButtons.AddNavigationButton(BSoldaten,ChangePage,PageVerSoldat);

  fTakticButtons.AddNavigationButton(BLabor,ChangePage,PageVerLabor,5);
  fTakticButtons.AddNavigationButton(BWerkStatt,ChangePage,PageWerkStatt);
  fTakticButtons.AddNavigationButton(BLager,ChangePage,PageVerLager);
  fTakticButtons.AddNavigationButton(BVerMarkt,ChangePage,PageVerMarkt);

  fTakticButtons.AddNavigationButton(BBasisInfo,ChangePage,PageInfoBasis,5);
  fTakticButtons.AddNavigationButton(BVerOrgan,ChangePage,PageVerOrgan);
  fTakticButtons.AddNavigationButton(BPunkteStat,ChangePage,PageInfoStatus);
  fTakticButtons.AddNavigationButton(BUfoPadie,ChangePage,PageUfoPadie);

//  fTakticButtons.AddNavigationButton(BPunkteStat,ChangePage,PageInfoStatus);

  fSaveButton:=fTakticButtons.AddNavigationButton(BSaveGame,SaveButtonClick,0,250);
  fTakticButtons.AddNavigationButton(BOptions,ChangePage,PageOptions);
  fTakticButtons.AddNavigationButton(BMainMenu,ChangePage,PageMainMenu);

  // Nachrichten
  fTakticMessages:=TTakticMessages.Create(TakticScreen);
  fTakticMessages.Left:=10;

  // Shortcuts f�r Zeiten
  TakticScreen.SetHotKey('0',true,SetTime,0);
  TakticScreen.SetHotKey('1',true,SetTime,1);
  TakticScreen.SetHotKey('2',true,SetTime,2);
  TakticScreen.SetHotKey('3',true,SetTime,3);
  TakticScreen.SetHotKey('4',true,SetTime,4);

  savegame_api_RegisterCustomSaveHandler(SaveCustomSaveData,LoadCustomSaveData,nil,$54095225);

  savegame_api_RegisterNewGameHandler(NewGameHandler);
  savegame_api_RegisterStartGameHandler(StartGameHandler);

  savegame_api_RegisterMessageHandler(MessageHandler);

  game_api_RegisterChangeCanSaveGameHandler(ChangeSaveHandler);
end;

function TGameMenu.FrameNextRound(Sender: TObject;Frames: Integer): boolean;
begin
  result:=false;
  if Frames=0 then exit;
  Container.Lock;

  SaveGame.NextRound(fTime);
  Container.LoadGame(false);

  fTakticMessages.DoRound(fTime);
  AktuTimeDrawer;

  TakticScreen.AktuMousePosition;
  TakticScreen.Redraw;
  {$IFNDEF DEBUGMODE}
  if (SaveGame.UFOListe.Gesichtet>0) and (fTime>1)  and (not AlwaysFastTime)then
    SetTime(1);
  {$ENDIF}
  Container.DecLock;
  result:=true;

  PlayMusic;
end;

procedure TGameMenu.SaveButtonClick(Data: Integer);
var
  Archiv        : TArchivFile;
  Name          : String;
  OldName       : String;
  SaveGameFile  : String;
begin
  SetTime(0);
  Name:=SaveGame.FileName;
  OldName:=Name;

  QueryText(CSaveGame,15,Name,ctBlue);

  if Name=EmptyStr then exit;
  if Name=RNSaveGame then
  begin
    game_api_MessageBox(Format(EInvGameName,[Name]),StringConst.CError);
    exit;
  end;
  SaveGame.FileName:=Name;

  SaveGameFile:='save\'+SaveGame.PlayerName+'.sav';
  Archiv:=TArchivFile.Create;
  if not FileExists(SaveGameFile) then
  begin
    if game_api_Question(Format(MRecreate,[SaveGame.PlayerName]),CQuestion) then
    begin
      game_api_MessageBox(Format(EInvPlayer,[SaveGame.PlayerName]),StringConst.CError);
      Archiv.Free;
      exit;
    end;
    Archiv.CreateArchiv(SaveGameFile);
  end
  else
    Archiv.OpenArchiv(SaveGameFile);

  if Archiv.ExistRessource(SaveGame.FileName) then
  begin
    if not game_api_Question(Format(MOverwrite,[SaveGame.FileName]),CQuestion) then
    begin
      Archiv.Free;
      SaveGame.FileName:=OldName;
      exit;
    end;
  end;
  Archiv.Free;
  try
    SaveGame.SaveGame(SaveGame.FileName);
  except
    on E: Exception do
    begin
      game_api_MessageBox(Format(MGameNotSaved,[E.Message]),StringConst.CError);
      exit;
    end;
  end;
end;

procedure TGameMenu.SetDefaults;
begin
  TakticScreen.Reset;
  AktuTimeDrawer;
  SetTime(0);
  PlayMusic;
end;

procedure TGameMenu.SetTime(Time: Integer);
begin
  Container.DeleteFrameFunction(FrameNextRound,nil);
  fTime:=0;
  // Wenn UFOs gesichtet sind, darf nur langsam vorangeschritten werden
  {$IFNDEF DEBUGMODE}
  if (Time>1) and (SaveGame.UFOListe.Gesichtet>0) and (not AlwaysFastTime) then
    Time:=1;
  {$ENDIF}

  fTimeMode:=Time;

  case Time of
    0 : TakticScreen.MoveRedraw:=true;
    1 :
      begin
        Container.AddFrameFunction(FrameNextRound,nil,100);
        fTime:=1;
        TakticScreen.MoveRedraw:=true;
      end;
    2 :
      begin
        Container.AddFrameFunction(FrameNextRound,nil,50);
        TakticScreen.MoveRedraw:=false;
        fTime:=2;
      end;
    3 :
      begin
        Container.AddFrameFunction(FrameNextRound,nil,50);
        TakticScreen.MoveRedraw:=false;
        fTime:=5;
      end;
    4 :
      begin
        Container.AddFrameFunction(FrameNextRound,nil,50);
        TakticScreen.MoveRedraw:=false;
        fTime:=10;
      end;
  end;
  fTimeDrawer.SetTimeMode(Time);
  TakticScreen.Redraw;
end;

procedure TGameMenu.LeaveModal;
begin
  SetTime(0);
end;

procedure TGameMenu.BufferMessage(MessageInfo: TGameMessage);
begin
  SetLength(fMessageBuffer,Length(fMessageBuffer)+1);
  MessageInfo.WasShowed:=false;

  if MessageInfo.Objekt<>nil then
  begin
    if MessageInfo.Objekt is TGameFigureManager then
      MessageInfo.DEvent:=TGameFigureManager(MessageInfo.Objekt).NotifyList.RegisterEvent(EVENT_MANAGER_ONDESTROY,DeleteMessageObject)
    else if MessageInfo.Objekt is TUFO then
      MessageInfo.DEvent:=TUFO(MessageInfo.Objekt).NotifyList.RegisterEvent(EVENT_ONUFODESTROY,DeleteMessageObject)
    else if MessageInfo.Objekt is TRaumschiff then
      MessageInfo.DEvent:=TRaumschiff(MessageInfo.Objekt).NotifyList.RegisterEvent(EVENT_SCHIFFONDESTROY,DeleteMessageObject)
    else if MessageInfo.Objekt is TEinsatz then
      MessageInfo.DEvent:=TEinsatz(MessageInfo.Objekt).NotifyList.RegisterEvent(EVENT_EINSATZONDESTROY,DeleteMessageObject)
  end;

  fMessageBuffer[high(fMessageBuffer)]:=MessageInfo;

end;

procedure TGameMenu.PageShown;
begin
  ShowBufferedMessages;
  TakticScreen.Redraw;
end;

procedure TGameMenu.ShowBufferedMessages;
const
  Protector: Boolean = false;
begin
  if Protector then
    exit;

  Protector:=true;

  Container.UnLockAll;
  while (length(fMessageBuffer)>0) do
  begin
    ShowGameMessage(fMessageBuffer[0]);

    if fMessageBuffer[0].Objekt is TGameFigureManager then
      TGameFigureManager(fMessageBuffer[0].Objekt).NotifyList.RemoveEvent(fMessageBuffer[0].DEvent)
    else if fMessageBuffer[0].Objekt is TUFO then
      TUFO(fMessageBuffer[0].Objekt).NotifyList.RemoveEvent(fMessageBuffer[0].DEvent)
    else if fMessageBuffer[0].Objekt is TRaumschiff then
      TRaumschiff(fMessageBuffer[0].Objekt).NotifyList.RemoveEvent(fMessageBuffer[0].DEvent)
    else if fMessageBuffer[0].Objekt is TEinsatz then
      TEinsatz(fMessageBuffer[0].Objekt).NotifyList.RemoveEvent(fMessageBuffer[0].DEvent);


    DeleteArray(Addr(fMessageBuffer),TypeInfo(TGameMessageArray),0);

{    // Abbruch, falls eine neue Seite ausgesucht wurde
    if (length(fMessageBuffer)=0) or (Container.ActivePage<>Self) then
    begin
      Container.LockAll;
      exit;
    end;}
  end;

  Protector:=false;

  Container.LockAll;
end;

function TGameMenu.Multiple(Objekts: TStringList): Integer;
begin
  SetTime(0);
  ListBoxWindow.Items.Assign(Objekts);
  result:=ListBoxWindow.Show;
end;

procedure TGameMenu.UFOSelect(UFO: TUFO);
begin
  if not (sabHuntUFO in TRaumschiff(fSelected).Abilities) then
  begin
    game_api_MessageBox(Format(MNoWeapons,[TRaumschiff(fSelected).Name]),CInformation);
    exit;
  end;
  if (not TRaumschiff(fSelected).GenugTreibstoff(0)) then
  begin
    game_api_MessageBox(Format(MRueckkehr,[TRaumschiff(fSelected).Name]),CInformation);
    exit;
  end;
  TRaumschiff(fSelected).HuntingUFO(UFO.ID);
end;

procedure TGameMenu.BasisSelect(Basis: TBasis);
begin
  try
    TRaumschiff(fSelected).SetHomeBase(Basis);
    TRaumschiff(fSelected).HuntingUFO(0);
  except
    on E: Exception do
      game_api_MessageBox(E.Message,CCancelAction);
  end;
end;

procedure TGameMenu.EinsatzSelect(Einsatz: TEinsatz);
begin
  if not (sabBodenKampf in TRaumschiff(fSelected).Abilities) then
  begin
    game_api_MessageBox(Format(MNoSoldaten,[TRaumschiff(fSelected).Name]),CInformation);
    exit;
  end;
  if (not TRaumschiff(fSelected).GenugTreibstoff(0)) then
  begin
    game_api_MessageBox(Format(MRueckkehr,[TRaumschiff(fSelected).Name]),CInformation);
    exit;
  end;
  TRaumschiff(fSelected).FlyToEinsatz(Einsatz.ID);
end;

procedure TGameMenu.PositionSelect(Point: TFloatPoint);
begin
  if (not TRaumschiff(fSelected).GenugTreibstoff(0)) then
  begin
    game_api_MessageBox(Format(MRueckkehr,[TRaumschiff(fSelected).Name]),CInformation);
    exit;
  end;
  TRaumschiff(fSelected).FlyToPos(Point);
end;

procedure TGameMenu.RaumschiffSelect(Raumschiff: TRaumschiff);
begin
  TRaumschiff(fSelected).EskortSchiff(Raumschiff.ID);
end;

procedure TGameMenu.AbfangenClick(Sender: TObject);
var
  MStringList : TStringList;
  Raumschiff  : TRaumschiff;
  Dummy       : Integer;
  Index       : Integer;
begin
  // Kein Objekt ausgew�hlt
  if fSelected = nil then
    exit;

  MStringList:=TStringList.Create;
  for Dummy:=0 to SaveGame.Raumschiffe.Count-1 do
  begin
    Raumschiff:=SaveGame.Raumschiffe[Dummy];
    if (sabChooseTarget in Raumschiff.Abilities) and (
        ((fSelected is TUFO) and (sabHuntUFO in Raumschiff.Abilities)) or
        ((fSelected is TEinsatz) and (sabBodenKampf in Raumschiff.Abilities))) then
      MStringList.AddObject(Raumschiff.Name,Raumschiff);
  end;
  if MStringList.Count=0 then
  begin
    if SaveGame.Raumschiffe.Count=0 then
    begin
      // Keine Raumschiff vorhanden -> Verweis auf Raumschiff kaufen
      if game_api_MessageBoxDlg(ST0311040001,CInformation,ST0311040002,BCancel) then
        ChangePage(PageVerSchiffe);
    end
    else if game_api_MessageBoxDlg(MAbfangen+ST0311230004,CInformation,ST0311040003,BCancel) then
    begin
      // Kein Raumschiff ausger�stet -> Verweis auf Raumschiffe ausr�sten
      ChangePage(PageVerSchiff);
    end;
  end
  else
  begin
    Index:=Multiple(MStringList);
    if Index>=0 then
    begin
      Raumschiff:=TRaumschiff(MStringList.Objects[Index]);
      if (not Raumschiff.GenugTreibstoff(0)) then
      begin
        game_api_MessageBox(Format(MRueckkehr,[Raumschiff.Name]),CInformation);
        MStringList.Free;
        exit;
      end;
      if fSelected is TUFO then
      begin
        if not (sabHuntUFO in Raumschiff.Abilities) then
        begin
          if Raumschiff.Waffenzellen = 0 then
            // Raumschiff hat keine Waffenzellen
            game_api_MessageBox(Format(ST0311230001,[Raumschiff.Name]),CInformation)
          else if not Raumschiff.IsInHomeBase then
            // Raumschiff ist noch unterwegs
            game_api_MessageBox(Format(MNoWeapons+ST0311230003,[Raumschiff.Name]),CInformation)
          else
          begin
            // Raumschiff ausr�sten
            if game_api_MessageBoxDlg(Format(MNoWeapons+ST0311230004,[Raumschiff.Name]),CInformation,ST0311040003,BCancel) then
            begin
              RaumschiffIndex:=SaveGame.Raumschiffe.IndexOfSchiff(Raumschiff);
              ChangePage(PageVerSchiff);
	    end;
          end;

          MStringList.Free;
          exit;
        end;
        Raumschiff.HuntingUFO(TUFO(fSelected).ID);
      end
      else if fSelected is TEinsatz then
      begin                                                    
        if not (sabBodenKampf in Raumschiff.Abilities) then
        begin
          if (Raumschiff.SoldatenSlots = 0) then
            // Raumschiff hat keinen Platz f�r Soldaten
            game_api_MessageBox(Format(ST0311230002,[Raumschiff.Name]),CInformation)
          else if not Raumschiff.IsInHomeBase then
            // Raumschiff ist noch unterwegs
            game_api_MessageBox(Format(MNoSoldaten+ST0311230003,[Raumschiff.Name]),CInformation)
          else
          begin
            if game_api_MessageBoxDlg(Format(MNoSoldaten+ST0311230004,[Raumschiff.Name]),CInformation,ST0311040003,BCancel) then
            begin
              RaumschiffIndex:=SaveGame.Raumschiffe.IndexOfSchiff(Raumschiff);
              ChangePage(PageVerSchiff);
	    end;                                           
          end;
          MStringList.Free;
          exit;
        end;
        Raumschiff.FlyToEinsatz(TEinsatz(fSelected).ID);
      end;
    end;
  end;
  MStringList.Free;
end;

procedure TGameMenu.ShowGameMessage(var MessageInfo: TGameMessage);
var
  OldSelected  : TObject;
  LastTime     : Integer;
  Projekt      : TForschungen;
  Index        : Integer;
  PadieType    : TUFOPadieEntry;
begin
  if MessageInfo.WasShowed then
    exit;

  Container.PlaySound(SMessage);
  
  MessageInfo.WasShowed:=true;

  LastTime:=fTimeMode;
  SetTime(0);
  GameMessage.Text:=MessageInfo.Text;
  GameMessage.Objekt:=MessageInfo.Objekt;
  GameMessage.MType:=MessageInfo.MType;
  GameMessage.ShowModal;
  if not GameMessage.Checked then
    SaveGame.LongMessages:=SaveGame.LongMessages-[MessageInfo.MType];

  case GameMessage.Button of
    mbWeiter : SetTime(LastTime);
    mbAktion :
    begin
      if MessageInfo.Objekt is TForschList then
      begin
        Projekt:=forsch_api_GetLastForschprojekt;
        if Projekt.TypeId in LagerItemType then
        begin
          PadieType:=upeLagerItem;
          Index:=lager_api_GetItemIndex(Projekt.ID);
        end
        else if Projekt.TypeID=ptEinrichtung then
        begin
          PadieType:=upeEinrichtung;
          Index:=basis_api_GetEinrichtungIndex(Projekt.ID);
        end
        else if Projekt.TypeID=ptRaumschiff then
        begin
          PadieType:=upeRaumschiff;
          Index:=raumschiff_api_GetRaumschiffModel(Projekt.ID);
        end
        else if Projekt.TypeID=ptNone then
        begin
          PadieType:=upeTechnologie;
          Index:=forsch_api_IndexOfCompletetProject(Projekt.ID);
        end;
        ufopaedie_api_ShowEntry(PadieType,Index);
      end
      else if (MessageInfo.Objekt is TGameFigureManager) then
      begin
        soldaten_api_ShowEquipView(TGameFigureManager(MessageInfo.Objekt));
      end
      else
      begin
        OldSelected:=fSelected;
        fSelected:=MessageInfo.Objekt;
        AbfangenClick(Self);
        fSelected:=OldSelected;
      end;
    end;
  end;
end;

procedure TGameMenu.TownSelect(Town: TTown);
begin
  if (not TRaumschiff(fSelected).GenugTreibstoff(0)) then
  begin
    game_api_MessageBox(Format(MRueckkehr,[TRaumschiff(fSelected).Name]),CInformation);
    exit;
  end;
  TRaumschiff(fSelected).FlyToTown(Town.ID);
end;

procedure TGameMenu.AddMessage(Text, Date: String);
begin
  fTakticMessages.Addmessage(Text,Date);
end;

procedure TGameMenu.AktuTimeDrawer;
begin
  fTimeDrawer.SetDate(SaveGame.Date);
  fTimeDrawer.SetCredits(SaveGame.Kapital);
  fTimeDrawer.CanFastTime:=(SaveGame.UFOListe.Gesichtet=0) or (AlwaysFastTime);
end;

procedure TGameMenu.SaveCustomSaveData(Stream: TStream);
begin
  // Nachrichten speichern
  SaveStringList(fTakticMessages.Messages,Stream);

  // Hotkeys (F-Tasten) zu den Ansichten speichern
  Stream.Write(FKeys,SizeOf(FKeys));
end;

procedure TGameMenu.LoadCustomSaveData(Stream: TStream);
begin
  // Nachrichten laden
  LoadStringList(fTakticMessages.Messages,Stream);

  // Hotkeys (F-Tasten) zu den Ansichten laden
  Stream.Read(FKeys,SizeOf(FKeys))
end;

procedure TGameMenu.ClearMessages;
begin
  fTakticMessages.Messages.Clear;
end;

procedure TGameMenu.SelectObject(Objekt: TObject);
begin
  fSelected:=Objekt;
end;

procedure TGameMenu.PlayMusic;
var
  Category: String;
begin
  if (SaveGame.UFOListe.Gesichtet>0) or (SaveGame.EinsatzList.Count>0) then
    Category:='Spielmen� - Action'
  else
    Category:='Spielmen� - Normal';

  if SaveGame.Organisations.IsDangerMode then
    Category:=Category+' / Gefahr';

  Container.PlayMusicCategory(Category);
end;

procedure TGameMenu.ShowDangerMessage(Sender: TObject);
begin
  PlayMusic;
  Container.UnLockAll;
  game_api_MessageBox(ST0310180001,ST0310180002);
  Container.LockAll;
end;

procedure TGameMenu.SetSaveGame(const Value: TKD4SaveGame);
begin
  inherited;
  SaveGame.Organisations.NotifyList.RegisterEvent(EVENT_ONDANGERMODE,ShowDangerMessage);
end;

procedure TGameMenu.NewGameHandler(Sender: TObject);
begin
  // Nachrichten l�schen
  ClearMessages;

  // F-Tasten zur Men�steuerung zur�cksetzen
  FKeys:=FDefaultKeys;
end;

procedure TGameMenu.StartGameHandler(Sender: TObject);
begin
  TakticScreen.Reset;
end;

procedure TGameMenu.MessageHandler(Sender: TObject);
var
  MessageInfo: TGameMessage;
begin
  MessageInfo:=savegame_api_GetLastMessage;
  MessageInfo.WasShowed:=false;

  if not (MessageInfo.MType in SaveGame.LongMessages+[lmReserved]) then
  begin
    // Sound nur abspielen, wenn Geoscape angezeigt wird
    if Container.ActivePage=Self then
      Container.PlaySound(SMessage);

    Container.Lock;
    AddMessage(MessageInfo.Text,SaveGame.DateString(true));
    Container.UnLock;
    TakticScreen.Redraw;
  end
  else
  begin
    if Container.ActivePage=Self then
    begin
      Container.UnLockAll;
      TakticScreen.Redraw;
      ShowGameMessage(MessageInfo);
      Container.LockAll;
    end
    else
    begin
      BufferMessage(MessageInfo);
    end;
  end;
end;

procedure TGameMenu.ChangeSaveHandler(Sender: TObject);
begin
  fTakticButtons.SetVisible(fSaveButton,game_api_GetCanSaveGame);
end;

destructor TGameMenu.Destroy;
begin
  GameMessage.Free;
  inherited;
end;

procedure TGameMenu.DeleteMessageObject(Sender: TObject);
var
  Dummy: Integer;
begin
  for Dummy:=0 to high(fMessageBuffer) do
  begin
    if fMessageBuffer[Dummy].Objekt=Sender then
      fMessageBuffer[Dummy].Objekt:=nil;
  end;
end;

end.
