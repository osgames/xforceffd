{********************************************************************************
*										*
* Copyright (C) 2004  X-Force Team - jim_raynor@web.de				*
*										*
* This file is part of X-Force: Fight For Destiny (X-Force).			*
*										*
* X-Force is free software; you can redistribute it and/or modify		*
* it under the terms of the GNU General Public License as published by		*
* the Free Software Foundation; either version 2 of the License, or		*
* (at your option) any later version.						*
*										*
* X-Force is distributed in the hope that it will be useful,			*
* but WITHOUT ANY WARRANTY; without even the implied warranty of		*
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the			*
* GNU General Public License for more details.					*
*										*
* You should have received a copy of the GNU General Public License		*
* along with X-Force; if not, write to the Free Software			*
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA	*
*										*
*********************************************************************************
*										*
* Anzeige des Ladebalkens beim Starten von X-Force				*
*										*
********************************************************************************}

{$I ../Settings.inc}

unit StartPage;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  DXProgressBar, KD4Page, DXContainer, StringConst, DXLabel, Defines, Blending;

type
  TStartPage = class(TKD4Page)
  private
    { Private-Deklarationen }
  protected
    { Protected-Deklarationen }
  public
    StartBar: TDXProgressBar;
    CreditLabel: TDXLabel;
    constructor Create(Container: TDXContainer);override;
    procedure SetDefaults;override;

    procedure IncLoadProgress;
  end;

implementation

{ TStartPage }

constructor TStartPage.Create(Container: TDXContainer);
begin
  inherited;
  HintLabel.Visible:=false;

  StartBar:=TDXProgressBar.Create(Self);
  with StartBar do
  begin
    SetRect(16,520,768,25);
    Kind:=sbHorizontal;
    Max:=MaxProgess;
  end;

  CreditLabel:=TDXLabel.Create(Self);
  with CreditLabel do
  begin
    SetRect(0,ScreenHeight-15,ScreenWidth,15);
    Alignment:=taCenter;
    AutoSize:=false;
  end;
end;

procedure TStartPage.IncLoadProgress;
begin
  Container.UnLock;
  StartBar.Value:=StartBar.Value+1;
  Container.Lock;
end;

procedure TStartPage.SetDefaults;
begin
  with StartBar do
  begin
    BorderColor:=bcNavy;
    FillColor:=bcBlue;
  end;
  SetFont(CreditLabel.Font);
  CreditLabel.Text:=ICopyRight;
  Container.PlayMusicCategory('Hauptmen�');
end;

end.
