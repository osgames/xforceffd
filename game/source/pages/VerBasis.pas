{********************************************************************************
*										*
* Copyright (C) 2004  X-Force Team - jim_raynor@web.de				*
*										*
* This file is part of X-Force: Fight For Destiny (X-Force).			*
*										*
* X-Force is free software; you can redistribute it and/or modify		*
* it under the terms of the GNU General Public License as published by		*
* the Free Software Foundation; either version 2 of the License, or		*
* (at your option) any later version.						*
*										*
* X-Force is distributed in the hope that it will be useful,			*
* but WITHOUT ANY WARRANTY; without even the implied warranty of		*
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the			*
* GNU General Public License for more details.					*
*										*
* You should have received a copy of the GNU General Public License		*
* along with X-Force; if not, write to the Free Software			*
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA	*
*										*
*********************************************************************************
*										*
* Verwaltungsbildschirm Basis							*
*										*
********************************************************************************}

{$I ../Settings.inc}

unit VerBasis;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  KD4Page, DXContainer, DXBitmapButton, Defines, DXListBox, DXLabel,
  DialogBaseType, DXDraws, DXInfoFrame, DXItemInfo, Blending, KD4Utils,
  XForce_types, DXBasisSelector, DXClassicInfoFrame, DirectDraw, DirectFont,
  Menus, DXBaseBuilder, DialogTransferAlphatron, DXCheckListBoxWindow,
  DXCheckList, DXListBoxWindow, KD4SaveGame, StringConst;

const
  coFirstColor  : TColor = clNavy;
  coSecondColor : TColor = clBlue;
  coScrollPen   : TColor = clNavy;

type
  TVerBasis = class(TKD4Page)
  private
    { Private-Deklarationen }
    fRedrawArea        : TRect;
    procedure DrawEinrichtung(Sender: TDXComponent;Surface: TDirectDrawSurface;const Mem: TDDSurfaceDesc;Rect: TRect; Index: Integer;Selected: boolean);
    procedure EinrichtChange(Sender: TObject);
    function GetSelectedIndex: Integer;
    
    procedure RenameClick(Sender: TObject);
    procedure NewBasisClick(Sender: TObject);
    procedure NewFloorClick(Sender: TObject);
    procedure ChangeSelectedBasis(Sender: TObject);

    procedure BuildClick(Sender: TObject);
    procedure AlphatronTransferClick(Sender: TObject);
    procedure AssignTechnicanClick(Sender: TObject);

    procedure BaseBuilderBuildRoom(BuildFloor,X,Y: Integer; Room: PEinrichtung);
    procedure BaseBuilderOverRoom(BuildFloor,X,Y: Integer);
    procedure BaseBuilderChangeFloor(Sender: TObject);

    procedure DrawTechniker(Sender: TDXComponent;Surface: TDirectDrawSurface;const Mem: TDDSurfaceDesc;Rect: TRect; Index: Integer;Selected: boolean);
    procedure AssignTechnikerDrawInfoCanvas(Sender: TDXComponent;Surface: TDirectDrawSurface;var Mem: TDDSurfaceDesc);

    procedure ShowBaseInfos;

    procedure ShowBuildList(Visible: Boolean);
    procedure FillRoomListBox;

    procedure RepairSingleRoom(Room: TObject);
    procedure DestroyRoom(Room: TObject);

    function BaseBuilderButtonRepair(Room: PEinrichtung): TBaseBuildButtonOption;
  public
    CloseButton            : TDXBitmapButton;

    InfoButton             : TDXBitmapButton;
    Build                  : TDXBitmapButton;
    NewBasis               : TDXBitmapButton;
    NewFloor               : TDXBitmapButton;
    Rename                 : TDXBitmapButton;
    AssignTechnican        : TDXBitmapButton;
    AlphatronTransfer      : TDXBitmapButton;

    BaseBuilder            : TDXBaseBuilder;
    InfoFrame              : TDXInfoFrame;
    EinRichtInfo           : TDXItemInfo;
    EinRicht               : TDXListBox;
    BasisSelect            : TDXBasisSelector;
    AssignTechnicanDialog  : TDXCheckListBoxWindow;
    TechnicanDialog        : TDXListBoxWindow;
    BaseTypSelect          : TDialogBaseType;
    DialogTransferAlphatron: TDialogTransferAlphatron;
    constructor Create(Container: TDXContainer);override;
    destructor destroy;override;
    procedure SetDefaults;override;
  end;


implementation

uses
  game_api, basis_api, BasisListe, raumschiff_api, werkstatt_api, math;

{ TVerBasis }

procedure TVerBasis.BaseBuilderBuildRoom(BuildFloor,X, Y: Integer;
  Room: PEinrichtung);
begin
  basis_api_GetSelectedBasis.Build(BuildFloor,X,Y,Room^);

  if Room.RoomTyp=rtLift then
    FillRoomListBox;
    
  Container.IncLock;
  BaseBuilder.ReDraw;
  Container.DecLock;
  InfoFrame.Redraw;
end;

procedure TVerBasis.DestroyRoom(Room: TObject);
var
  Basis : TBasis;
  Index : Integer;
begin
  Basis:=BaseBuilder.Base;

  Index:=Basis.GetRoomIndex(PEinrichtung(Room).RoomID);

  case Basis.CanSellRoom(Index) of
    csrInUse:
      begin
        game_api_MessageBox(ST0411300001,CInformation);
       exit;
      end;
    csrNeedForCennection:
      begin
        game_api_MessageBox(ST0503170001,CInformation);
        exit;
      end;
    else
    begin
      if game_api_Question(Format(ST0411300002,[PEinrichtung(Room).ActualValue/1,PEinrichtung(Room).Name]),CQuestion) then
      begin
        Basis.SellRoom(Index);
        BaseBuilder.Redraw;
      end;
    end;
  end;
end;

procedure TVerBasis.BaseBuilderOverRoom(BuildFloor, X, Y: Integer);
var
  Info: TBaseField;
  Basis : TBasis;
begin
  if X=-1 then
  begin
    ShowBaseInfos;
    exit;
  end;

  Basis:=basis_api_GetSelectedBasis;
  Info:=Basis.GetBaseFieldInfo(BuildFloor,X,Y);
  Container.Lock;
  if Info.Objekt<>nil then
  begin
    EinrichtInfo.EinStatus:=true;
    EinrichtInfo.Einrichtung:=Basis.Einrichtungen[Info.Index];
    EinrichtInfo.Caption:=LEinrichtung;
  end
  else
    ShowBaseInfos;

  Container.UnLock;
  EinrichtInfo.Redraw;
end;

procedure TVerBasis.BuildClick(Sender: TObject);
begin
  ShowBuildList(not Build.Highlight);
end;

procedure TVerBasis.ChangeSelectedBasis(Sender: TObject);
begin
  if Container.ActivePage<>Self then
    exit;

  Container.IncLock;
  SetDefaults;
  Container.Redraw(Container.Surface);

  Container.DecLock;
  Container.DoFlip;
end;

constructor TVerBasis.Create(Container: TDXContainer);
begin
  inherited;
  Animation:=paRightToLeft;
  HintLabel.BlendColor:=bcBlue;
  HintLabel.BorderColor:=coFirstColor;

  SetPageInfo(StripHotKey(BVerBasis));

  { SchliessenButton }
  CloseButton:=TDXBitmapButton.Create(Self);
  with CloseButton do
  begin
    Text:=BClose;
    SetRect(510,400,122,28);
    FirstColor:=coFirstColor;
    EscButton:=true;
    RoundCorners:=rcRight;
    SecondColor:=coSecondColor;
    AccerlateColor:=coAccerlateColor;
    PageIndex:=PageGameMenu;
    SetButtonFont(Font);
    Hint:=HCloseButton;
    BlendColor:=bcNavy;
  end;

  { Informationsframe }
  InfoFrame:=TDXInfoFrame.Create(Self);
  with InfoFrame do
  begin
    SetRect(388,2,244,150);
    SetFont(Font);
    ColorSheme:=icsBlue;
    Infos:=[ifCredits,ifWohn,ifLager,ifWerk,ifLabor,ifHangar];
    SystemKeyChange:=true;
  end;

  { Einrichtung bauen }
  Build:=TDXBitmapButton.Create(Self);
  with Build do
  begin
    SetRect(8,50,230,28);
    FirstColor:=coFirstColor;
    SecondColor:=coSecondColor;
    AccerlateColor:=coAccerlateColor;
    SetButtonFont(Font);
    RoundCorners:=rcTop;
    Text:=BBuild;
    OnClick:=BuildClick;
    Hint:=HBuild;
    BlendColor:=bcNavy;
  end;

  { Neue Basis bauen }
  NewBasis:=TDXBitmapButton.Create(Self);
  with NewBasis do
  begin
    SetRect(8,79,230,28);
    FirstColor:=coFirstColor;
    SecondColor:=coSecondColor;
    AccerlateColor:=coAccerlateColor;
    RoundCorners:=rcNone;
    SetButtonFont(Font);
    Text:=BNewBasis;
    Hint:=HNewBasis;
    OnClick:=NewBasisClick;
    BlendColor:=bcNavy;
  end;

  { Neue Ebene bauen }
  NewFloor:=TDXBitmapButton.Create(Self);
  with NewFloor do
  begin
    SetRect(8,108,230,28);
    FirstColor:=coFirstColor;
    SecondColor:=coSecondColor;
    AccerlateColor:=coAccerlateColor;
    RoundCorners:=rcNone;
    SetButtonFont(Font);
    Text:=ST0501130003;
    Hint:=ST0501130004;
    OnClick:=NewFloorClick;
    BlendColor:=bcNavy;
  end;

  { Basis umbennen }
  Rename:=TDXBitmapButton.Create(Self);
  with Rename do
  begin
    SetRect(8,137,230,28);
    FirstColor:=coFirstColor;
    SecondColor:=coSecondColor;
    AccerlateColor:=coAccerlateColor;
    RoundCorners:=rcNone;
    SetButtonFont(Font);
    Text:=BSRename;
    Hint:=HRename;
    OnClick:=RenameClick;
    BlendColor:=bcNavy;
  end;

  { BasisInfoButton }
  InfoButton:=TDXBitmapButton.Create(Self);
  with InfoButton do
  begin
    Text:=BBasisInfo;
    SetRect(8,166,230,28);
    FirstColor:=coFirstColor;
    SecondColor:=coSecondColor;
    AccerlateColor:=coAccerlateColor;
    RoundCorners:=rcNone;
    PageIndex:=PageInfoBasis;
    SetButtonFont(Font);
    Hint:=HBaseInfos;
    BlendColor:=bcNavy;
  end;

  { Alphatron transferieren }
  AssignTechnican:=TDXBitmapButton.Create(Self);
  with AssignTechnican do
  begin
    SetRect(8,195,230,28);
    FirstColor:=coFirstColor;
    SecondColor:=coSecondColor;
    AccerlateColor:=coAccerlateColor;
    RoundCorners:=rcNone;
    SetButtonFont(Font);
    Text:=ST0501020001;
    Hint:=ST0501020002;
    OnClick:=AssignTechnicanClick;
    BlendColor:=bcNavy;
  end;

  { Alphatron transferieren }
  AlphatronTransfer:=TDXBitmapButton.Create(Self);
  with AlphatronTransfer do
  begin
    SetRect(8,224,230,28);
    FirstColor:=coFirstColor;
    SecondColor:=coSecondColor;
    AccerlateColor:=coAccerlateColor;
    RoundCorners:=rcBottom;
    SetButtonFont(Font);
    Text:=ST0412200001;
    Hint:=ST0412200002;
    OnClick:=AlphatronTransferClick;
    BlendColor:=bcNavy;
  end;

{ Verf�gbare Einrichtungen }
  { Einrichtungslistbox }
  EinRicht:=TDXListBox.Create(Self);
  with EinRicht do
  begin
    SetRect(8,79,230,211);
    BorderColor:=coFirstColor;
    FocusColor:=coSecondColor;
    SetFont(Font);
    SetButtonFont(ScrollBarFont);
    FirstColor:=bcNavy;
    RoundCorners:=rcNone;
    SecondColor:=coScrollPen;
    Hint:=HBauEinrichtung;
    ItemHeight:=44;
    AlwaysChange:=true;
    OwnerDraw:=true;
    OnChange:=EinrichtChange;
    OnDrawItem:=DrawEinrichtung;
    HeadRowHeight:=17;
    HeadData:=MakeHeadData(LEinricht,FEinrichtung,FEinrichtungen);

    Visible:=false;
  end;

  { Einrichtungsinformationen }
  EinRichtInfo:=TDXItemInfo.Create(Self);
  with EinRichtInfo do
  begin
    SetFont(Font);
    RoundCorners:=rcBottom;
    BorderColor:=coFirstColor;
    SetRect(8,291,230,289);
    Caption:=LEinrichtung;
    Hint:=HEinrichtInfo;
    BlendColor:=bcNavy;
  end;

  { BaseBuilder }
  BaseBuilder:=TDXBaseBuilder.Create(Self);
  BaseBuilder.SetRect(275,50,514,514);
  BaseBuilder.OnBuildRoom:=BaseBuilderBuildRoom;
  BaseBuilder.OnChangeFloor:=BaseBuilderChangeFloor;
  BaseBuilder.OnOverRoom:=BaseBuilderOverRoom;

  BaseTypSelect:=TDialogBaseType.Create(Self);
  SetButtonFont(BaseTypSelect.Font);

  DialogTransferAlphatron:=TDialogTransferAlphatron.Create(Self);
  SetButtonFont(DialogTransferAlphatron.Font);

  { Listbox Fenster zur Auswahl der Technicker }
  AssignTechnicanDialog:=TDXCheckListBoxWindow.Create(Self);
  with AssignTechnicanDialog do
  begin
    SetRect(0,0,300,200);
    Caption:=ST0501030001;
    SetButtonFont(Font);
    CaptionColor:=coFontColor;
    AccerlateColor:=coAccerlateColor;
    Font.Size:=coFontSize;
    FirstColor:=$00400000;
    SecondColor:=clBlue;
    BlendColor:=bcDarkNavy;
    SetOwnerDraw(22,8,DrawTechniker);
    OnDrawInfoCanvas:=AssignTechnikerDrawInfoCanvas;
  end;

  { Listbox Fenster zur Auswahl der Technicker }
  TechnicanDialog:=TDXListBoxWindow.Create(Self);
  with TechnicanDialog do
  begin
    SetRect(0,0,300,200);
    Caption:=ST0501240001;
    SetButtonFont(Font);
    CaptionColor:=coFontColor;
    AccerlateColor:=coAccerlateColor;
    Font.Size:=coFontSize;
    FirstColor:=$00400000;
    SecondColor:=clBlue;
    BlendColor:=bcDarkNavy;
    SetOwnerDraw(22,8,DrawTechniker);
  end;

  { Basisselector }
  BasisSelect:=TDXBasisSelector.Create(Self);

  UnionRect(fRedrawArea,Build.ClientRect,EinRichtInfo.ClientRect);

  basis_api_RegisterChangeBaseHandler(ChangeSelectedBasis);

  BaseBuilder.AddRoomButton(BaseBuilderButtonRepair,RepairSingleRoom,Point(2,2),Point(3,3),Rect(223,20,239,36),ST0501300001);

  BaseBuilder.AddRoomButton(nil,DestroyRoom,Point(-2,2),Point(5,5),Rect(141,67,152,79),ST0501300002);
end;

destructor TVerBasis.destroy;
begin
  BaseTypSelect.Free;
  DialogTransferAlphatron.Free;
  AssignTechnicanDialog.Free;
  inherited;
end;

procedure TVerBasis.DrawEinrichtung(Sender: TDXComponent;
  Surface: TDirectDrawSurface;const Mem: TDDSurfaceDesc; Rect: TRect; Index: Integer;
  Selected: boolean);
var
  LIndex: Integer;
begin
  if Selected then
    Einricht.DrawSelection(Surface,Mem,Rect,bcNavy);

  LIndex:=Integer(Einricht.Items.Objects[Index]);
  SaveGame.BasisListe.DrawIcon(Surface,Rect.Left+6,Rect.Top+6);
  YellowStdFont.Draw(Surface,Rect.Left+40,Rect.Top+6,SaveGame.BasisListe.Einrichtung[LIndex].Name);
  WhiteStdFont.Draw(Surface,Rect.Left+40,Rect.Top+24,Format(FBauZeit,[SaveGame.BasisListe.Einrichtung[LIndex].KaufPreis/1]));
end;

procedure TVerBasis.EinrichtChange(Sender: TObject);
begin
  Container.Lock;
  EinrichtInfo.Caption:=CUEinricht;
  EinrichtInfo.EinStatus:=false;
  Container.Unlock;

  if GetSelectedIndex=-1 then
    EinrichtInfo.ItemValid:=false
  else
  begin
    EinRichtInfo.Einrichtung:=SaveGame.BasisListe[GetSelectedIndex];
    BaseBuilder.BuildingRoom:=SaveGame.BasisListe.GetAddrOfEinrichtung(GetSelectedIndex);
  end;
end;

procedure TVerBasis.NewBasisClick(Sender: TObject);
var
  Pt  : TFloatPoint;
  Typ : TBaseType;
begin
  BaseTypSelect.ShowModal;
  if BaseTypSelect.Result=-1 then
    exit;

  Typ:=TBaseType(BaseTypSelect.Result);

  BasisSelect.BaseType:=Typ;
  if not BasisSelect.SelectPos(Pt,false) then
    exit;

  try
    SaveGame.BasisListe.BuildNewBasis(Pt,Typ);
  except
    on E: Exception do
    begin
      game_api_MessageBox(E.Message,CInformation);
      exit;
    end;
  end;

  AlphatronTransfer.Enabled:=basis_api_GetBasisCount>1;

  InfoFrame.Reset;
  InfoFrame.Redraw;
end;

procedure TVerBasis.RenameClick(Sender: TObject);
var
  Name: String;
begin
  Name:=SaveGame.BasisListe.Selected.Name;
  QueryText(CRename,30,Name,ctBlue);
  if Name='' then
    exit;
  SaveGame.BasisListe.Selected.Name:=Name;

  InfoFrame.Redraw;
end;

procedure TVerBasis.SetDefaults;
begin
  InfoFrame.Reset;

  BasisSelect.BasisListe:=SaveGame.BasisListe;

  BaseBuilder.Base:=basis_api_GetSelectedBasis;

  NewFloor.Enabled:=(basis_api_GetSelectedBasis.BaseType=btBase) and (basis_api_GetSelectedBasis.BuildingFloors<MaxFloors);
  AssignTechnican.Enabled:=basis_api_GetSelectedBasis.BaseType=btAlphatronMine;

  AlphatronTransfer.Enabled:=basis_api_GetBasisCount>1;

  ShowBuildList(false);
end;

procedure TVerBasis.ShowBuildList(Visible: Boolean);
begin
  Container.IncLock;

  if Visible then
  begin
    FillRoomListBox;
  end
  else
  begin
    BaseBuilder.BuildingRoom:=nil;
    ShowBaseInfos;
  end;

  Container.LoadGame(true);
  Build.HighLight:=Visible;
  EinRicht.Visible:=Visible;

  Rename.Visible:=not Visible;
  NewBasis.Visible:=not Visible;
  NewFloor.Visible:=not Visible;
  InfoButton.Visible:=not Visible;
  AssignTechnican.Visible:=not Visible;
  AlphatronTransfer.Visible:=not Visible;

  Container.Unlock;
  Container.RedrawArea(fRedrawArea,Container.Surface);

end;

procedure TVerBasis.AlphatronTransferClick(Sender: TObject);
begin
  if DialogTransferAlphatron.ShowModal=mrOK then
  begin
    // Automatischen Transfer aktivieren/deaktivieren
    if DialogTransferAlphatron.AutoTransfer then
      basis_api_GetSelectedBasis.AutoTransferBase:=DialogTransferAlphatron.TargetBase
    else
      basis_api_GetSelectedBasis.AutoTransferBase:=nil;

    // Alphatron transferieren
    if (DialogTransferAlphatron.AlphatronCount>0) and (DialogTransferAlphatron.TargetBase<>nil) then
      basis_api_TransferAlphatron(basis_api_GetSelectedBasis,DialogTransferAlphatron.TargetBase,DialogTransferAlphatron.AlphatronCount);
  end;
end;

procedure TVerBasis.AssignTechnicanClick(Sender: TObject);
var
  Dummy  : Integer;
  BaseID : Cardinal;
  LIndex : Integer;
begin
  BaseID:=basis_api_GetSelectedBasis.ID;

  AssignTechnicanDialog.ListBox.Items.beginUpdate;
  AssignTechnicanDialog.ListBox.Items.Clear;

  for Dummy:=0 to werkstatt_api_GetTechnikerCount-1 do
  begin
    if werkstatt_api_GetTechniker(Dummy).BasisID=BaseID then
    begin
      AssignTechnicanDialog.ListBox.AddItem(IntToStr(Dummy),'');
      AssignTechnicanDialog.ListBox.Checked[AssignTechnicanDialog.ListBox.Items.Count-1]:=werkstatt_api_GetTechniker(Dummy).AlphatronMining;
    end;
  end;
  AssignTechnicanDialog.ListBox.Items.EndUpdate;

  if AssignTechnicanDialog.Show=mrOK then
  begin
    for Dummy:=0 to AssignTechnicanDialog.ListBox.Items.Count-1 do
    begin
      LIndex:=StrToInt(AssignTechnicanDialog.ListBox.Items[Dummy]);
      if AssignTechnicanDialog.ListBox.Checked[Dummy] then
        werkstatt_api_SetMiningState(LIndex)
      else
      begin
        if werkstatt_api_GetTechniker(LIndex).AlphatronMining then
        begin
          werkstatt_api_FreeTechniker(LIndex);
        end;
      end;
    end;
  end;
end;

procedure TVerBasis.DrawTechniker(Sender: TDXComponent; Surface: TDirectDrawSurface;
  const Mem: TDDSurfaceDesc; Rect: TRect; Index: Integer; Selected: boolean);
var
  LIndex    : Integer;
  Techniker : TTechniker;
  ListBox   : TDXListBox;
  Font      : TDirectFont;
  Text      : String;
begin
  ListBox:=(Sender as TDXListBox);
  LIndex:=StrToInt(ListBox.Items[Index]);

  if Selected then
  begin
    ListBox.DrawSelection(Surface,Mem,Rect,bcBlue);
    Font:=WhiteStdFont;
  end
  else
    Font:=YellowStdFont;

  Techniker:=werkstatt_api_GetTechniker(LIndex);

  if (Techniker.ProjektID<>0) or (Techniker.AlphatronMining) or (Techniker.Repair) then
    Font:=RedStdFont;

  Font.Draw(Surface,Rect.Left+5,Rect.Top+4,Techniker.Name);

  Text:=Format(FFaehig,[Techniker.Strength]);

  Font.Draw(Surface,Rect.Right-5-Font.TextWidth(Text),Rect.Top+4,Text);

  if Techniker.ProjektID<>0 then
    RedStdFont.Draw(Surface,Rect.Right-140,Rect.Top+4,LProduktion)
  else if Techniker.AlphatronMining then
    RedStdFont.Draw(Surface,Rect.Right-140,Rect.Top+4,ST0501250001)
  else if Techniker.Repair then
    RedStdFont.Draw(Surface,Rect.Right-140,Rect.Top+4,ST0501250002);
end;

procedure TVerBasis.AssignTechnikerDrawInfoCanvas(Sender: TDXComponent;
  Surface: TDirectDrawSurface; var Mem: TDDSurfaceDesc);
var
  Dummy         : Integer;
  LIndex        : Integer;
  Sum           : double;
  Text          : String;
  Effectiveness : double;
begin
  Rectangle(Surface,Mem,Sender.ClientRect,bcDarkNavy);
  BlendRectangle(CorrectBottomOfRect(Sender.ClientRect),150,bcDarkNavy,Surface,Mem);

  Sum:=0;
  for Dummy:=0 to AssignTechnicanDialog.ListBox.Items.Count-1 do
  begin
    if AssignTechnicanDialog.ListBox.Checked[Dummy] then
    begin
      LIndex:=StrToInt(AssignTechnicanDialog.ListBox.Items[Dummy]);
      Sum:=Sum+werkstatt_api_GetTechniker(LIndex).Strength;
    end;
  end;

  WhiteStdFont.Draw(Surface,Sender.Left+5,Sender.Top+2,ST0501040001);

  Effectiveness:=min(100,max(AlphatronMinMiningEffectiveness,Sum/AlphatronMaxMiningEffectiveness*100));
  
  Text:=Format(ST0501040002,[Sum,Effectiveness]);
  WhiteStdFont.Draw(Surface,Sender.Right-5-WhiteStdFont.TextWidth(Text),Sender.Top+2,Text);
end;

procedure TVerBasis.ShowBaseInfos;
begin
  EinrichtInfo.Basis:=basis_api_GetSelectedBasis;
  EinRichtInfo.Caption:=StripHotkey(BBasisInfo);
end;

procedure TVerBasis.NewFloorClick(Sender: TObject);
begin
  if game_api_Question(Format(ST0502240001,[FloorCosts[basis_api_GetSelectedBasis.BuildingFloors]/1]),CQuestion) then
  begin
    basis_api_GetSelectedBasis.BuildNewFloor;

    BaseBuilder.Base:=basis_api_GetSelectedBasis;

    NewFloor.Enabled:=(basis_api_GetSelectedBasis.BuildingFloors<MaxFloors);
  end;
end;

procedure TVerBasis.FillRoomListBox;
var
  Dummy: Integer;
begin
  Einricht.ChangeItem(true);
  Einricht.Items.Clear;

  for Dummy:=0 to SaveGame.BasisListe.Einrichtungen-1 do
  begin
    if (basis_api_GetEinrichtung(Dummy).RoomTyp=rtLift) and (not basis_api_GetSelectedBasis.NeedBuildLift) then
      continue;

    if not basis_api_GetSelectedBasis.CanPlaceRoomInFloor(basis_api_GetEinrichtung(Dummy).RoomTyp,BaseBuilder.BuildingFloor) then
      continue;
      
    Einricht.Items.AddObject(basis_api_GetEinrichtung(Dummy).Name,TObject(Dummy));
  end;

  Einricht.ChangeItem(false);

  EinrichtChange(Self);
end;

function TVerBasis.GetSelectedIndex: Integer;
begin
  if Einricht.ItemIndex=-1 then
    result:=-1
  else
    result:=Integer(Einricht.Items.Objects[Einricht.ItemIndex]);
end;

procedure TVerBasis.RepairSingleRoom(Room: TObject);
var
  Dummy  : Integer;
  BaseID : Cardinal;
  res    : Integer;
begin
  BaseID:=basis_api_GetSelectedBasis.ID;
  TechnicanDialog.Items.Clear;
  for Dummy:=0 to werkstatt_api_GetTechnikerCount-1 do
  begin
    if werkstatt_api_GetTechniker(Dummy).BasisID=BaseID then
      TechnicanDialog.Items.Add(IntToStr(Dummy));
  end;
  res:=TechnicanDialog.Show;
  if res=-1 then
    exit;

  res:=StrToInt(TechnicanDialog.Items[res]);
  basis_api_GetSelectedBasis.RepairRoom(PEinrichtung(Room).RoomID,werkstatt_api_GetTechniker(res).TechnikerID);

  BaseBuilder.Redraw;
end;

procedure TVerBasis.BaseBuilderChangeFloor(Sender: TObject);
begin
  if EinRicht.Visible then
    FillRoomListBox;
end;

function TVerBasis.BaseBuilderButtonRepair(
  Room: PEinrichtung): TBaseBuildButtonOption;
begin
  if SaveGame.BasisListe.CanRoomRepair(Room) then
    result:=bbboActive
  else if SaveGame.BasisListe.RoomInRepair(Room) then
    result:=bbboImageOnly
  else
    result:=bbboInvisible;
end;

end.
