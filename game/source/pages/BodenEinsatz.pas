{********************************************************************************
*										*
* Copyright (C) 2004  X-Force Team - jim_raynor@web.de				*
*										*
* This file is part of X-Force: Fight For Destiny (X-Force).			*
*										*
* X-Force is free software; you can redistribute it and/or modify		*
* it under the terms of the GNU General Public License as published by		*
* the Free Software Foundation; either version 2 of the License, or		*
* (at your option) any later version.						*
*										*
* X-Force is distributed in the hope that it will be useful,			*
* but WITHOUT ANY WARRANTY; without even the implied warranty of		*
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the			*
* GNU General Public License for more details.					*
*										*
* You should have received a copy of the GNU General Public License		*
* along with X-Force; if not, write to the Free Software			*
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA	*
*										*
*********************************************************************************
*										*
* Hier spielt sich der Bodeneinsatz ab. Hier werden die Verteilernachrichten	*
* vom Bodeneinsatz (SendVMessage) verarbeitet und an die entsprechenden 	*
* Komponenten weitergeleitet.							*
*										*
********************************************************************************}

{$I ../Settings.inc}

unit BodenEinsatz;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  KD4Page, DXContainer, DXDraws, DXIsoEngine, RaumschiffList, XForce_types, Defines,
  DXUIBodenEinsatz, EinsatzListe, DXISOMinimap, GameFigure, ISOMessages,
  DXEinsatzPager, ISOObjects, EinsatzAuswertung, KD4Utils, TraceFile,
  EinsatzOptionsDialog, DXInput, TypInfo, StringConst;

type
  TBodenEinsatz = class(TKD4Page)
  private
    fEchtzeit    : boolean;
    fEinsatz     : TEinsatz;
    fManagList   : TList;
    fResult      : TFightResult;
    fRaumschiff  : TRaumschiff;
    fCancel      : boolean;
    { Private-Deklarationen }
  protected
    {$IFDEF MUSICTEST}
    fShowOptions: Boolean;
    procedure SelectMusic(Data: Integer);override;
    {$ENDIF}
    procedure OnSelectFigure(Figure: TGameFigure);
    procedure RedrawUI(UI: TUIInterface);
    procedure Verteiler(var Rec: TMessageRecord);
    procedure OnShowOptions(Sender: TObject);
    { Protected-Deklarationen }
  public
    Engine  : TDXIsoEngine;
    UI      : TDXUIBodenEinsatz;
    Figures : TDXUISoldatenList;
    MiniMap : TDXISOMiniMap;
    Pager   : TDXEinsatzPager;
    Options : TEinsatzOptionsDialog;

    constructor Create(Container: TDXContainer);override;
    destructor destroy;override;
    procedure PageShown;override;

    procedure LoadData;

    function ReshowPage: boolean;override;
    property Einsatz         : TEinsatz read fEinsatz write fEinsatz;
    property Echtzeit        : boolean read fEchtzeit write fEchtzeit;
    property ManagerListe    : TList read fManagList write fManagList;
    property MissionResult   : TFightResult read fResult;
    property Raumschiff      : TRaumschiff  read fRaumschiff write fRaumschiff;
    { Public-Deklarationen }
  end;

implementation

uses
  GameFigureManager, game_api, registry, EinsatzIntro, lager_api;

{ TBodenEinsatz }

constructor TBodenEinsatz.Create(Container: TDXContainer);
const
  UIWidth       = 150;
  PagerHeight   =  20;
  SListHeight   =  52;
{$IFNDEF DISABLEFORMATION}
var
  Dummy: Integer;
{$ENDIF}
begin
  inherited;
  Animation:=paNone;
  HintLabel.AutoSize:=false;
  HintLabel.Visible:=false;
  fManagList:=TList.Create;

  Engine:=TDXIsoEngine.Create(Self);
  with Engine do
  begin
    SetFont(Font);
    SetRect(0,0,ScreenWidth-UIWidth,ScreenHeight-(PagerHeight+SListHeight));
    OnNeedRedrawUI:=RedrawUI;
    OnShowOptions:=Self.OnShowOptions;
  end;

  MiniMap:=TDXISOMinimap.Create(Self);
  MiniMap.SetRect(0,0,ScreenWidth-UIWidth,ScreenHeight-(PagerHeight+SListHeight));
  MiniMap.Visible:=false;
  MiniMap.IsoEngine:=Engine;

  Figures:=TDXUISoldatenList.Create(Self);
  with Figures do
  begin
    SetRect(0,ScreenHeight-(SListHeight),ScreenWidth-UIWidth,SListHeight);
  end;

  Pager:=TDXEinsatzPager.Create(Self);
  Pager.SetRect(0,ScreenHeight-(PagerHeight+SListHeight),ScreenWidth-UIWidth,PagerHeight);

  UI:=TDXUIBodenEinsatz.Create(Self);
  with UI do
  begin
    SetRect(ScreenWidth-UIWidth,0,UIWidth,ScreenHeight);
    ISOEngine:=Engine;
    Soldaten:=Figures;
    ISOMiniMap:=MiniMap;
  end;

  {$IFNDEF DISABLEFORMATION}
  for Dummy:=0 to Engine.FormationCount-1 do
  begin
    UI.RegisterFormation(Engine.Formation[Dummy]);
  end;
  {$ENDIF}

  MessageHandler:=Verteiler;

  Options:=TEinsatzOptionsDialog.Create(Self);
  SetButtonFont(Options.Font);
  {$IFDEF MUSICTEST}
  fShowOptions:=false;
  {$ENDIF}
end;

destructor TBodenEinsatz.destroy;
begin
  Options.Free;
  fManagList.Free;
  inherited;
end;

procedure TBodenEinsatz.LoadData;
var
  Dummy  : Integer;
  Count  : Integer;
  Reg    : TRegistry;
//  Soldat : TSoldatInfo;
  MapName: String;
begin
  UI.Echtzeit:=Echtzeit;
  fCancel:=false;

  Engine.Init(Echtzeit);

  IncEinsatzLoadProgress;

  // Karte laden => zum Debuggen kann eine bestimmte Karte in der Registry
  // abgelegt werden
  Reg:=OpenXForceRegistry;
  if Reg.ValueExists('EinsatzMap') then
    MapName := Reg.ReadString('EinsatzMap')
  else
    MapName := fEinsatz.MapName;

  Reg.Free;

  try
     Engine.LoadMap(MapName);
  except
     // Falls laden der Karte scheitert, wird zuerst noch eine Map generieren, bevor es aufgegeben wird.
     on E: Exception do begin
        fEinsatz.GenerateNewMap;
        Engine.LoadMap(fEinsatz.MapName);
     end;
  end;


  // Feinde z�hlen, um die Kartengr��e zu ermitteln
  Count:=0;
  for Dummy:=0 to fManagList.Count-1 do
  begin
    if TGameFigureManager(fManagList[Dummy]).FigureStatus=fsEnemy then
      inc(Count);
  end;
  Engine.ISOMap.CreateMap(Count);

  // Einheiten �bernehmen
  for Dummy:=0 to fManagList.Count-1 do
    Engine.AddFigure(fManagList[Dummy]);

  Engine.Startvorbereitungen;

  UI.SetStartState;

  // Dies sorgt daf�r, dass die Hotkeys registriert werden
  UI.ChangeHotKeys(true);

  Pager.StartGame(Echtzeit);

  fResult:=frStart;
end;

procedure TBodenEinsatz.OnSelectFigure(Figure: TGameFigure);
begin
  UI.Figure:=Figure;
end;

procedure TBodenEinsatz.OnShowOptions(Sender: TObject);
var
  Count : Integer;
  Dummy : Integer;
  Text  : String;
begin
  {$IFDEF MUSICTEST}
  fShowOptions:=true;
  {$ENDIF}
  Options.Messages:=Pager.PauseBei;
  Options.ShowModal;

  if Options.ChooseOption<>coCancel then
    Pager.PauseBei:=Options.Messages;

  case Options.ChooseOption of
    coCancelEinsatz:
    begin
      // Einheit auf dem Kampffeld z�hlen
      Count:=0;
      for Dummy:=0 to Engine.Figures.Count-1 do
      begin
        if (Engine.Figures[Dummy].FigureStatus=fsFriendly) and (Engine.Figures[Dummy].IsOnField) then
          inc(Count);
      end;

      if Count=0 then
        Text:=ST0309210012
      else
        Text:=Format(ST0309210013,[Count]);
      if game_api_Question(Text,ST0309210014) then
      begin
        Engine.EndGame;
        fCancel:=true;
      end;
    end;
    coKeyBoardConfig:
    begin
      Container.MessageLock(true);
      ChangePage(PageKeyBoard);
      Container.MessageLock(false);
    end;
    coShowMission:
    begin
      game_api_MessageBox(fEinsatz.GetMissionInfo,ST0410100001);
    end;
  end;
  {$IFDEF MUSICTEST}
  fShowOptions:=false;
  {$ENDIF}
end;

procedure TBodenEinsatz.PageShown;
var
  Dummy        : Integer;
  SolDummy     : Integer;
  Soldat       : TGameFigure;
  Summe        : Integer;
  Item         : TLagerItem;
  BecameSoldat : Boolean;
  ISOItem      : TISOItem;

  function FindNearestFigure(Point: TPoint): TGameFigure;
  var
    Dummy  : Integer;
    Figure : TGameFigure;
  begin
    result:=nil;
    for Dummy:=0 to Engine.Figures.Count-1 do
    begin
      Figure:=Engine.Figures[Dummy];
      if (Figure.FigureStatus=fsFriendly) and (not Figure.IsDead) then
      begin
        if (result=nil) or (Figure.IsOnField and (not result.IsOnField)) then
        begin
          Result:=Figure;
          continue;
        end;
        // Entfernung pr�fen
        if CalculateEntfern(PointToFloat(Point),FloatPoint(Figure.XPos,Figure.YPos))<CalculateEntfern(PointToFloat(Point),FloatPoint(result.XPos,result.YPos)) then
          Result:=Figure;
      end;
    end;
  end;

  function OwnUnit(Figure: TGameFigure): Boolean;
  begin
    result:=(Figure<>nil) and (Figure.FigureStatus=fsFriendly) and (not Figure.IsDead);
  end;

  procedure TakeItem(Manager: TGameFigureManager;var Item: TISOItem);
  begin
    Manager.Echtzeit:=true;
    if Manager.TakeItem(Item,ISOItem.Figure.Manager<>Manager) then
      Item:=nil;
  end;

begin
  if fResult<>frStart then exit;

  Container.MessageLock(true);
  Container.ReactiveFrameFunctions;

  fResult:=frNone;
  Engine.PlayGame;

  Pager.EndGame;

  // Dies sorgt daf�r, dass die Hotkeys wieder entfernt werden
  UI.ChangeHotKeys(false);

  Container.MessageLock(false);

  if fResult=frWin then
  begin
    // Ausr�stung aufsammeln
    for Dummy:=Engine.ItemList.Count-1 downto 0 do
    begin
      ISOItem:=TISOItem(Engine.ItemList[Dummy]);
      SaveGame.LagerListe.RegisterAlienItem(ISOItem.Item);
      Soldat:=nil;

      if OwnUnit(ISOItem.Figure) then
        Soldat:=ISOItem.Figure
      else
      begin
        if TISOItem(ISOItem).Item.TypeID=ptMunition then
        begin
          // Ausr�stung mit Munition suchen, um zu pr�fen, ob die Munition bereits benutzt wurde
          Item:=lager_api_GetItem(TISOItem(ISOItem).Item.ID)^;

          if (TISOItem(ISOItem).Item.Munition<>-1) and (Item.Munition<>TISOItem(ISOItem).Item.Munition) then
            Soldat:=FindNearestFigure(Point(TISOItem(ISOItem).X,TISOItem(ISOItem).Y));
        end;
      end;

      if Soldat<>nil then
        TakeItem(Soldat.Manager,ISOItem);

      // Ausr�stung wurde noch nicht eingesammelt
      if ISOItem<>nil then
      begin
        if (fRaumschiff.ChangeItem(TISOItem(Engine.ItemList[Dummy]).Item.ID,1,false)<>1) then
        begin
          for SolDummy:=0 to Engine.Figures.Count-1 do
          begin
            if OwnUnit(Engine.Figures[SolDummy]) then
            begin
              TakeItem(Engine.Figures[SolDummy].Manager,ISOItem);
              if ISOItem=nil then
                break;
            end;
          end;
        end;

      end;
      // Punkte berechnen f�r erbeutete Ausr�stung, falls diese noch nicht vom Soldaten genommen wurde
      if (ISOItem<>nil) and (not (TISOItem(Engine.ItemList[Dummy]).FriendStatus=fsFriendly)) then
        Engine.BerechnePunkte(bptItemFound,TISOItem(Engine.ItemList[Dummy]).PunkteWert);
    end;

    // Aliens "aufsammeln"
    for Dummy:=0 to Engine.Figures.Count-1 do
    begin
      if Engine.Figures[Dummy].IsDead and (Engine.Figures[Dummy].Manager is TAlienManager) then
      begin
        SaveGame.ForschListe.AddAlienAutopsie(TAlienManager(Engine.Figures[Dummy].Manager).Alien^);
      end;
    end;
  end
  else
  begin
    // Eigene Ausr�stung als Verlust speichern
    for Dummy:=0 to Engine.ItemList.Count-1 do
    begin
      if TISOItem(Engine.ItemList[Dummy]).FriendStatus=fsFriendly then
        Engine.BerechnePunkte(bptItemLost,TISOItem(Engine.ItemList[Dummy]).PunkteWert);
    end;
  end;
  Engine.ItemList.Clear;

  // Wenn der Einsatz gewonnen wurde, werden die Einheiten vom Bodeneinsatz
  // geholt. Dies verhindert, das CheckSoldaten diese Einheiten entfernt
  for SolDummy:=0 to fManagList.Count-1 do
  begin
    if fResult=frWin then
      TGameFigureManager(fManagList[SolDummy]).OnBattleField:=false
    else
    begin
      // Befindet sich der Soldat noch auf dem Bodeneinsatz muss er wohl oder �ber dran glauben
      if TGameFigureManager(fManagList[SolDummy]).OnBattleField and (TObject(fManagList[SolDummy]) is TSoldatManager) then
      begin
        if TSoldatManager(fManagList[SolDummy]).Gesundheit>0 then
        begin
          TSoldatManager(fManagList[SolDummy]).ChangeGesundheit(-1000);
          BuchPunkte(bptSoldatTot);
        end;
      end;
    end;
  end;

  Statistik:=Engine.Statistik;

  // Summe der Punkte berechnen
  Summe:=0;
  Inc(Summe,Statistik.Alienabgeschossen.Punkte);
  Inc(Summe,Statistik.SolVerloren.Punkte);
  Inc(Summe,Statistik.AusErbeutet.Punkte);
  Inc(Summe,Statistik.AusVerloren.Punkte);
  Inc(Summe,Statistik.ohneVerlustBonus.Punkte);
  Inc(Summe,Statistik.TrefferBilanz.Punkte);

  case fResult of
    { Einsatz gewonnen }
    frWin   :
    begin
      game_api_MessageBox(ST0309210015,ST0309210015);
      inc(Summe,200);
      Statistik.Contributation:=SaveGame.Organisations.EinsatzGewonnen(Einsatz,Summe);
      Einsatz.Win;
      SaveGame.BuchungPunkte(200,pbEinsatzWin);
    end;

    { Einsatz verloren }
    frLoose :
    begin
      game_api_MessageBox(ST0309210016,ST0309210016);
      dec(Summe,200);
      SaveGame.Organisations.EinsatzVerloren(Einsatz,Summe);
      SaveGame.BuchungPunkte(200,pbEinsatzLoose);

      Einsatz.AktuAlienList;
    end;

    { Einsatz abgebrochen }
    frCancel :
    begin
      Einsatz.AktuAlienList;
    end;
  end;

  fRaumschiff.BackToBase;
  
  SaveGame.SoldatenListe.CheckSoldaten;

  SaveGame.BuchungPunkte(Statistik.Alienabgeschossen.Punkte,pbAlienGetoetet);
  SaveGame.BuchungPunkte(Statistik.SolVerloren.Punkte,pbSoldatGetoetet);
  SaveGame.BuchungPunkte(Statistik.AusErbeutet.Punkte,pbItemFound);
  SaveGame.BuchungPunkte(Statistik.AusVerloren.Punkte,pbItemLost);
  SaveGame.BuchungPunkte(Statistik.ohneVerlustBonus.Punkte,pbAlienGetoetet);
  SaveGame.BuchungPunkte(Statistik.TrefferBilanz.Punkte,pbTrefferBilanz);

  ChangePage(PageEinsatzEnde);
  Container.LeaveModal:=true;
  Engine.Clear;
end;

procedure TBodenEinsatz.RedrawUI(UI: TUIInterface);
begin
  case UI of
    uiSoldat     : Self.UI.Redraw;
    uiSoldatList :
    begin
      if not Pager.Collaps then
        Self.Figures.Redraw;
    end;
  end;
end;

function TBodenEinsatz.ReshowPage: boolean;
begin
  result:=(fResult=frNone) and (not fCancel);
  if not Result then
    exit;
  UI.NotifyMessage(nuitSelectionChange,UI.Figure);
end;

{$IFDEF MUSICTEST}
procedure TBodenEinsatz.SelectMusic(Data: Integer);
begin
  if fShowOptions then
    exit;
  Container.MessageLock(false);
  Container.UnlockAll;
  inherited;
  Container.LockAll;
  Container.MessageLock(true);
  Container.ReactiveFrameFunctions;
end;
{$ENDIF}

procedure TBodenEinsatz.Verteiler(var Rec: TMessageRecord);
begin
  case Rec.Message of
    vmSelection              : Engine.SetSelected(TGameFigure(Rec.Figure));
    vmAktuTempSurface        : Engine.AktuTempSurface;
    vmAktuTempSurfaceRect    : Engine.AktuTempSurface(Rec.Rect);
    vmEngineRedraw           : Engine.Redraw;
    vmRedrawSolList          : RedrawUI(uiSoldatList);
    vmAddInfoText            : Rec.InfoTextID:=Engine.AddInfoText(Rec.Text,Rec.Rect,TISOObject(Rec.Objekt));
    vmDeleteInfoText         : Engine.DeleteInfoText(Rec.InfoTextID);
    vmCanDrawUnit            : Rec.Result:=Engine.ISOMap.CanDrawFigure(TGameFigure(Rec.Figure));
    vmCenterPoint            : Engine.ISOMap.CenterTo(Rec.Point);
    vmGetPause               : Rec.Result:=Engine.Pause or Minimap.Visible;
    vmIncSeeing              : Engine.ISOMap.IncSeeing(TGameFigure(Rec.Figure));
    vmDecSeeing              : Engine.ISOMap.DecSeeing(TGameFigure(Rec.Figure));
    vmContainerLock          : Container.Lock;
    vmContainerUnlock        : Container.Unlock;
    vmContainerIncLock       : Container.IncLock;
    vmContainerDeclock       : Container.Declock;
    vmSetFigure              : Rec.result:=Engine.ISOMap.SetFigureToPos(Rec.Point,TGameFigure(Rec.Figure));
    vmGetFigure              : Rec.Figure:=Engine.ISOMap.GetFigureAtPos(Rec.Point);
    {$IFNDEF CANNOTWIN}
    vmMissionWin             :
    begin
      Engine.EndGame;
      Engine.BerechnePunkte(bptGewonnen);
      fResult:=frWin;
    end;
    vmMissionLose            :
    begin
      Engine.EndGame;
      Engine.BerechnePunkte(bptVerloren);
      fResult:=frLoose;
    end;
    {$ENDIF}
    vmMissionCancel          : begin Engine.EndGame;fResult:=frCancel; end;
    vmMessage                : Pager.SendMessage(Rec.Text,Rec.Point,Rec.MessageType,TGameFigure(Rec.Figure));
    vmCreateDeathObject      : Engine.CreateDeathObject(TGameFigure(Rec.Figure));
    {$IFNDEF DISABLEBEAM}
    vmCreateBeamObject       : Engine.CreateBeamObject(TGameFigure(Rec.Figure),Rec.Bool);
    {$ENDIF}
    vmShoot                  : Rec.resultObj:=Engine.DoShoot(TGameFigure(Rec.Figure),Rec.Ziel,Rec.Infos);
    vmGetTileRect            : Rec.Rect:=Engine.GetTileRect(Self,Rec.Point);
    vmCheckRedrawOfSolList   : Figures.CheckRedraw;
    vmNotifyUI               : rec.result:=UI.NotifyMessage(Rec.NotifyUIType,TGameFigure(Rec.Figure));
    vmCreateZielObject       : Engine.CreateZielObject(TGameFigure(Rec.Figure));
    vmDeleteZielObject       : Engine.DeleteZielObject(TGameFigure(Rec.Figure));
    {$IFNDEF DISABLEFORMATION}
    vmGetSelectedFormation   : rec.resultObj:=UI.Formation;
    {$ELSE}
    vmGetSelectedFormation   : rec.resultObj:=Engine.Formation[0];
    {$ENDIF}
    vmCreateItemObject       : Engine.CreateItemObject(Rec.Item,TGameFigure(Rec.Figure),Rec.Point,Rec.OffSet);
    {$IFNDEF DISABLEBEAM}
    vmCanBeamTo              : rec.result:=Engine.CanBeamTo(Rec.Point);
    {$ENDIF}
    vmTextMessage            : Engine.ShowTextMessage(Rec.Text);
    vmRoundEnd               : Engine.NextRound(rec.ZugSeite);
    vmBuchungPunkte          : Engine.BerechnePunkte(Rec.PunkteType,Rec.Punkte);
    vmFriendTreffer          : Engine.ZaehleTreffer;
    vmShowNextUnit           : UI.SetNextFigure(true);
    vmShowPrevUnit           : UI.SetNextFigure(false);
    vmPlaySound              : Container.PlaySound(Rec.Text);
    vmStopSound              : Container.StopSound(Rec.Text);
    vmSetPause               : Engine.Pause:=Rec.Bool;
    vmSetUserInterfaceSperre :
    begin
      if Rec.Bool then
        Engine.UserSperre:=Engine.UserSperre+1
      else
        Engine.UserSperre:=Engine.UserSperre-1;
    end;
    vmGetUserInterfaceSperre : rec.Int:=Engine.UserSperre;
    vmKameraVerfolgung       : rec.Figure:=Engine.SetCameraTo(TGameFigure(Rec.Figure));
    vmUseObject              : Engine.UseObject(TGameFigure(Rec.Figure),Rec.Slot,Rec.SlotIndex,Rec.Point);
    vmMiniMapSelectionChange : MiniMap.ChangeUnitSelection(TGameFigure(Rec.Figure));
    vmWaffePosChanged        : Engine.Figures.WaffenPosChanged(Rec.Figure);
    vmTerrainChange          : Engine.TerrainChange;
    vmCenterToPauseObject    : Pager.CenterToPausedMessage;
    vmGoOnAfterMessage       : Pager.EndMessage(Rec.Bool);
    vmReleaseEscButton       :
    begin
      game_api_GetDirectInput.Update;
      game_api_GetDirectInput.States:=game_api_GetDirectInput.States - [isButton31];
    end;
    vmCreateFieldExplosion   : Engine.CreateFieldExplosion(Rec.Explosion);
    vmChangeHotkeys          : UI.ChangeHotKeys(Rec.Bool);
    vmGetISOObjects          : Engine.GetObjects(Rec);
    vmThrowObject            : Engine.UseObject((Rec.Figure as TGameFigure),Rec.Slot,Rec.SlotIndex,Rec.Point,true);
    vmCanFriendMove          : Rec.Result:=Engine.CanFriendsMove;
    vmGetObjectList          : rec.resultObj:=TObject(Engine.GetObjectList);
    vmCenterUnit             : Engine.ISOMap.CenterTo((Rec.Figure as TGameFigure));
    vmShowWayPoint           : Engine.CreateWayPoint(Rec.Point,Rec.Direction,Rec.ColorInd,Rec.Bool);
    vmIsWayFree              : Engine.ISOMap.IsWayFree(Rec.FromP,Rec.ToP,Rec.WayFreeRes);
    vmCreateHintForFigure    : rec.resultObj:=Engine.CreateFigureObject((Rec.Figure as TGameFigure));
    vmGetLagerItem           : Rec.Item:=SaveGame.LagerListe.AddrLagerItem(SaveGame.LagerListe.IndexOfID(Rec.ID));
    vmGetNamedSurface        : Rec.Surface:=Engine.GetSurface(Rec.Text);
    {$IFNDEF DISABLEBEAM}
    vmShowBeamMenue          : UI.ShowBeamMenu((Rec.Figure as TGameFigure));
    {$ENDIF}
    vmBreakRound             : rec.result:=Engine.BreakRound((Rec.WantBreakUnit as TGameFigure),(Rec.BreakingUnit as TGameFigure));
    vmSetDrawingRect         : Engine.ISOMap.SetDrawingRect((Rec.Figure as TGameFigure),Rec.Rect);
    vmCanUnitDoAction        : rec.result:=Engine.CanUnitMove((Rec.Figure as TGameFigure));
    vmGetUnitTimeUnits       : rec.TimeUnits:=Engine.Figures.GetFigureOfManager(Rec.Objekt as TGameFigureManager).Zeiteinheiten;
    vmSetUnitTimeUnits       : Engine.Figures.GetFigureOfManager(Rec.Objekt as TGameFigureManager).Zeiteinheiten:=rec.TimeUnits;
    vmNeedTimeUnits          : rec.TUresult:=Engine.Figures.GetFigureOfManager(Rec.Objekt as TGameFigureManager).NeedTU(rec.TimeUnits);
    vmGetXANBitmap           : rec.XANBitmap:=Engine.GetModelAnimation(Rec.Text);
    vmGetFigureOfManager     : rec.Figure:=Engine.Figures.GetFigureOfManager(Rec.Manager);
    else
      raise Exception.Create('Unbekannte Verteiler-Message '+GetEnumName(TypeInfo(TVerteilerMessage),ord(Rec.Message)));
  end;
end;

end.
