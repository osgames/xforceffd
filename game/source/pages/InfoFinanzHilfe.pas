{********************************************************************************
*										*
* Copyright (C) 2004  X-Force Team - jim_raynor@web.de				*
*										*
* This file is part of X-Force: Fight For Destiny (X-Force).			*
*										*
* X-Force is free software; you can redistribute it and/or modify		*
* it under the terms of the GNU General Public License as published by		*
* the Free Software Foundation; either version 2 of the License, or		*
* (at your option) any later version.						*
*										*
* X-Force is distributed in the hope that it will be useful,			*
* but WITHOUT ANY WARRANTY; without even the implied warranty of		*
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the			*
* GNU General Public License for more details.					*
*										*
* You should have received a copy of the GNU General Public License		*
* along with X-Force; if not, write to the Free Software			*
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA	*
*										*
*********************************************************************************
*										*
* Anzeige der Unterst�zung der L�nder						*
*										*
********************************************************************************}

{$I ../Settings.inc}

unit InfoFinanzHilfe;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  KD4Page, DXContainer, DXBitmapButton, StringConst, Defines, XForce_types, DXListBox,
  DXDraws, Blending, KD4Utils, DXLabel, DXPointStatus, DirectFont,DirectDraw;

type
  TInfoFinanzHilfe = class(TKD4Page)
  private
    { Private-Deklarationen }
    procedure ClosePage(Sender: TObject);
  protected
    procedure DrawOrganisation(Sender: TDXComponent;Surface: TDirectDrawSurface;const Mem: TDDSurfaceDesc; Rect: TRect; Index: Integer;Selected: boolean);
    procedure OrganMouseUp(Sender: TObject;Button: TMouseButton;Shift: TShiftState;x,y: Integer);
    procedure DrawOrganHeader(Sender: TDXComponent;Surface: TDirectDrawSurface;Rect: TRect);
    { Protected-Deklarationen }
  public
    CloseButton   : TDXBitmapButton;

    OrganBox      : TDXListBox;
    CaptionLabel  : TDXLabel;
    HilfeGesamt   : TDXPointStatus;
    constructor Create(Container: TDXContainer);override;
    procedure SetDefaults;override;
  end;

implementation

uses
  ufopaedie_api;

{ TInfoFinanzHilfe }

procedure TInfoFinanzHilfe.ClosePage(Sender: TObject);
begin
  CloseModal;
end;

constructor TInfoFinanzHilfe.Create(Container: TDXContainer);
begin
  inherited;
  Animation:=paUpToBottom;

  { SchliessenButton }
  CloseButton:=TDXBitmapButton.Create(Self);
  with CloseButton do
  begin
    Text:=BClose;
    SetRect(522,400,110,28);
    FirstColor:=coFirstColor;
    SecondColor:=coSecondColor;
    AccerlateColor:=coAccerlateColor;
    RoundCorners:=rcAll;
    OnClick:=ClosePage;
    SetButtonFont(Font);
    EscButton:=true;
    Text:=BClose;
    Hint:=HCloseButton;
  end;

  { Captionlabel }
  CaptionLabel:=TDXLabel.Create(Self);
  with CaptionLabel do
  begin
    SetRect(0,8,640,25);
    AutoSize:=false;
    SetFont(Font);
    Font.Size:=16;
    Alignment:=taCenter;
  end;

  { Zusammenfassung }
  HilfeGesamt:=TDXPointStatus.Create(Self);
  with HilfeGesamt do
  begin
    SetRect(370,46,262,200);
    SetFont(Font);
  end;

  { Box zur Anzeige der Organisationen }
  OrganBox:=TDXListBox.Create(Self);
  with OrganBox do
  begin
    SetRect(8,46,350,347);
    SetFont(Font);
    SetButtonFont(ScrollBarFont);
    BorderColor:=coFirstColor;
    FocusColor:=coSecondColor;
    ItemHeight:=30;
    FirstColor:=coScrollBrush;
    SecondColor:=coScrollPen;
    OwnerDraw:=true;
    OnDrawItem:=DrawOrganisation;
    OnMouseUp:=OrganMouseUp;
    HeadRowHeight:=17;
    OnDrawHeadRow:=DrawOrganHeader;
//    Hint:=HPlayerSelect;
  end;

end;

procedure TInfoFinanzHilfe.DrawOrganHeader(Sender: TDXComponent;
  Surface: TDirectDrawSurface; Rect: TRect);
begin
  WhiteStdFont.Draw(Surface,Rect.Right-8-WhiteStdFont.TextWidth(LBudget),Rect.Top+2,LBudget);
  WhiteStdFont.Draw(Surface,Rect.Left+8,Rect.Top+2,IIOrganisation);
end;

procedure TInfoFinanzHilfe.DrawOrganisation(Sender: TDXComponent;
  Surface: TDirectDrawSurface;const Mem: TDDSurfaceDesc; Rect: TRect; Index: Integer;
  Selected: boolean);
var
  Text    : String;
  Font    : TDirectFont;
  Organ   : Integer;
begin
  Organ:=StrToInt(OrganBox.Items[Index]);
  if Selected then
  begin
    OrganBox.DrawSelection(Surface,Mem,Rect,bcMaroon);
  end;
  Font:=YellowStdFont;
  case OrganStatus(SaveGame.Organisations[Organ].Friendly) of
    osAllianz    : Font:=LimeStdFont;
    osFriendly   : Font:=GreenStdFont;
    osNeutral    : Font:=YellowStdFont;
    osUnFriendly : Font:=MaroonStdFont;
    osEnemy      : Font:=RedStdFont;
  end;
  SaveGame.Organisations.DrawOrgan(Surface,Rect.Left+4,Rect.Top+4,Organ);
  Font.Draw(Surface,Rect.Left+49,Rect.Top+8,SaveGame.Organisations[Organ].Name);
  Text:=Format(FCredits,[SaveGame.Organisations.Hilfe(Organ)/1]);
  Font.Draw(Surface,Rect.Right-8-Font.TextWidth(Text),Rect.Top+8,Text);
end;

procedure TInfoFinanzHilfe.OrganMouseUp(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; x, y: Integer);
begin
  if Button<>mbRight then exit;
  if OrganBox.Items.Count=0 then exit;
  ufopaedie_api_ShowCountryInfo(StrToInt(OrganBox.Items[OrganBox.ItemIndex]));
end;

procedure TInfoFinanzHilfe.SetDefaults;
var
  Dummy   : Integer;
  Dummy2  : Integer;
  Insert  : boolean;
begin
  OrganBox.Items.Clear;
  for Dummy:=0 to SaveGame.Organisations.Count-1 do
  begin
    Insert:=false;
    Dummy2:=0;
    while (not Insert) and (Dummy2<OrganBox.Items.Count) do
    begin
      if SaveGame.Organisations.Hilfe(StrToInt(OrganBox.Items[Dummy2]))<SaveGame.Organisations.Hilfe(Dummy) then
      begin
        Insert:=true;
        OrganBox.Items.Insert(Dummy2,IntToStr(Dummy));
      end;
      inc(Dummy2);
    end;
    if not Insert then OrganBox.Items.Add(IntToStr(Dummy));
  end;

  if SaveGame.MonthEnd then
    CaptionLabel.Text:=SaveGame.GetSettlementWeek(LFHilfe)
  else
    CaptionLabel.Text:=SaveGame.GetSettlementWeek(LKalkulation);

  HilfeGesamt.Organisation:=SaveGame.Organisations;
end;

end.
