{********************************************************************************
*										*
* Copyright (C) 2004  X-Force Team - jim_raynor@web.de				*
*										*
* This file is part of X-Force: Fight For Destiny (X-Force).			*
*										*
* X-Force is free software; you can redistribute it and/or modify		*
* it under the terms of the GNU General Public License as published by		*
* the Free Software Foundation; either version 2 of the License, or		*
* (at your option) any later version.						*
*										*
* X-Force is distributed in the hope that it will be useful,			*
* but WITHOUT ANY WARRANTY; without even the implied warranty of		*
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the			*
* GNU General Public License for more details.					*
*										*
* You should have received a copy of the GNU General Public License		*
* along with X-Force; if not, write to the Free Software			*
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA	*
*										*
*********************************************************************************
*										*
* Verwaltungsbildschirm Arbeitsmarkt						*
*										*
********************************************************************************}

{$I ../Settings.inc}

unit VerArbeitsmarkt;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, KD4Page,
  DXContainer, Defines, DXBitmapButton, DXInfoFrame, DXListBox, DXDraws,
  DXLabel, Blending, KD4Utils, XForce_types, DirectDraw, DirectFont, WatchMarktDialog,
  Menus,StringConst;

type
  TPageType = (ptTechnik,ptForscher);

const
  coFirstColor  : TColor = clNavy;
  coSecondColor : TColor = clBlue;
  coScrollPen   : TColor = clNavy;

var
  ActivePage: TPageType;

type
  TVerArbeitsmarkt = class(TKD4Page)
  private
    fAreaRect: TRect;
    { Private-Deklarationen }
  protected
    procedure TechnikClick(Sender: TObject);
    procedure RenameClick(Sender: TObject);
    procedure ForscherClick(Sender: TObject);
    procedure BuyClick(Sender: TObject);
    procedure SellClick(Sender: TObject);
    procedure WatchMarktClick(Sender: TObject);
    procedure ListDblClick(Sender: TObject; Button: TMouseButton;Shift: TShiftState; X, Y: Integer);
    procedure ChangePageState;
    procedure DrawYourList(Sender: TDXComponent;Surface: TDirectDrawSurface;const Mem: TDDSurfaceDesc;Rect: TRect; Index: Integer;Selected: boolean);
    procedure DrawKaufList(Sender: TDXComponent;Surface: TDirectDrawSurface;const Mem: TDDSurfaceDesc;Rect: TRect; Index: Integer;Selected: boolean);
    procedure AktuButtons;
    procedure OnSelectedBaseChange(Sender: TObject);
    function GetSelectedIndex: Integer;
    { Protected-Deklarationen }
  public
    InfoFrame     : TDXInfoFrame;
    TechnikButton : TDXBitmapButton;
    ForschButton  : TDXBitmapButton;
    BuyButton     : TDXBitmapButton;
    SellButton    : TDXBitmapButton;
    RenameButton  : TDXBitmapButton;
    WatchButton   : TDXBitmapButton;
    YourListBox   : TDXListBox;
    KaufListBox   : TDXListBox;
    WatchDialog   : TWatchMarktDialog;
    constructor Create(Container: TDXContainer);override;
    destructor Destroy;override;
    procedure SetDefaults;override;
  end;

implementation

uses
  game_api, basis_api;

{ TVerArbeitsmarkt }

procedure TVerArbeitsmarkt.AktuButtons;
begin
  SellButton.Enabled:=not (YourListBox.Items.Count=0);
  RenameButton.Enabled:=not (YourListBox.Items.Count=0);
  BuyButton.Enabled:=not (KaufListBox.Items.Count=0);
end;

procedure TVerArbeitsmarkt.BuyClick(Sender: TObject);
begin
  Container.IncLock;
  Container.LoadGame(true);
  try
    if ActivePage=ptTechnik then
    begin
      SaveGame.WerkStatt.BuyTechniker(KaufListBox.ItemIndex);
      KaufListBox.Items.Delete(KaufListBox.ItemIndex);
      YourListBox.Items.Add(IntToStr(SaveGame.WerkStatt.Count-1));
    end
    else
    begin
      SaveGame.ForschListe.BuyForscher(KaufListBox.ItemIndex);
      KaufListBox.Items.Delete(KaufListBox.Itemindex);
      YourListBox.Items.Add(IntToStr(SaveGame.ForschListe.ForscherCount-1));
    end;
  except
    on E: Exception do
    begin
      Container.DecLock;
      Container.LoadGame(false);
      game_api_MessageBox(E.Message,CCancelAction);
      exit;
    end;
  end;
  AktuButtons;
  ChangePageState;
  Container.LoadGame(false);
  Container.DecLock;
  Container.RedrawArea(fAreaRect,Container.Surface);
end;

procedure TVerArbeitsmarkt.ChangePageState;
var
  Dummy: Integer;
begin
  Container.IncLock;
  Container.LoadGame(true);
  YourListBox.ChangeItem(true);
  YourListBox.Items.Clear;
  KaufListBox.ChangeItem(true);
  KaufListBox.Items.Clear;
  if ActivePage=ptTechnik then
  begin
    TechnikButton.HighLight:=true;
    ForschButton.HighLight:=false;
    with YourListBox do
    begin
      HeadData:=MakeHeadData(LYourTechnik,FTechniker,FTechniker);
      Hint:=HAMTechList;
      for Dummy:=0 to SaveGame.WerkStatt.Count-1 do
      begin
        if SaveGame.WerkStatt.InSelectedBasis(Dummy) then
          Items.Add(IntToStr(Dummy));
      end;
      WatchButton.Hint:=ST0309270017;
    end;
    KaufListBox.Hint:=HAMKTechList;
    SellButton.Hint:=HAMSellTech;
    RenameButton.Hint:=HRenameTech;
    for Dummy:=0 to SaveGame.WerkStatt.KaufCount-1 do
      KaufListBox.Items.Add(SaveGame.WerkStatt.KaufTechniker[Dummy].Name);
  end
  else
  begin
    TechnikButton.HighLight:=false;
    ForschButton.HighLight:=true;
    with YourListBox do
    begin
      HeadData:=MakeHeadData(LYourForsch,FForsch,FForsch);
      Hint:=HAMForsList;
      for Dummy:=0 to SaveGame.ForschListe.ForscherCount-1 do
      begin
        if SaveGame.ForschListe.ForscherInSelectedBasis(Dummy) then
          Items.Add(IntToStr(Dummy));
      end;
    end;
    SellButton.Hint:=HAMSellFors;
    KaufListBox.Hint:=HAMKForsList;
    RenameButton.Hint:=HRenameFors;
    WatchButton.Hint:=ST0309270018;
    for Dummy:=0 to SaveGame.ForschListe.KaufCount-1 do
      KaufListBox.Items.Add(SaveGame.ForschListe.KaufForscher[Dummy].Name);
  end;
  KaufListBox.ChangeItem(false);
  KaufListBox.Items.OnChange(nil);
  YourListBox.ChangeItem(false);
  YourListBox.Items.OnChange(nil);
  AktuButtons;
  Container.LoadGame(false);
  Container.DecLock;
  Container.RedrawArea(fAreaRect,Container.Surface);
end;

constructor TVerArbeitsmarkt.Create(Container: TDXContainer);
begin
  inherited;
  Animation:=paUpToBottom;
  HintLabel.BlendColor:=bcNavy;
  HintLabel.BorderColor:=coFirstColor;

  SetPageInfo(StripHotKey(BVerMarkt));

{ Informationsframe }
  InfoFrame:=TDXInfoFrame.Create(Self);
  with InfoFrame do
  begin
    SetRect(388,10,244,70);
    SetFont(Font);
    ColorSheme:=icsBlue;
    Infos:=[ifCredits,ifWohn];
    SystemKeyChange:=true;
  end;


{ Ihre Techniker/Wissenschaftler }
  { ListBox }
  YourListBox:=TDXListBox.Create(Self);
  with YourListBox do
  begin
    SetRect(8,103,370,420);
    BorderColor:=coFirstColor;
    FocusColor:=coSecondColor;
    SetFont(Font);
    SetButtonFont(ScrollBarFont);
    ItemHeight:=58;
    SecondColor:=coScrollPen;
    HeadBlendColor:=bcNavy;
    OwnerDraw:=true;
    OnDrawItem:=DrawYourList;
    FirstColor:=bcNavy;
    OnDblClick:=ListDblClick;
    HeadRowHeight:=17;
  end;

  { Techniker Button }
  TechnikButton:=TDXBitmapButton.Create(Self);
  with TechnikButton do
  begin
    SetRect(8,54,120,28);
    SetButtonFont(Font);
    FirstColor:=coFirstColor;
    SecondColor:=coSecondColor;
    AccerlateColor:=coAccerlateColor;
    RoundCorners:=rcLeft;
    BlendColor:=bcNavy;
    Text:=BTechniker;
    Hint:=HAMTechniker;
    OnClick:=TechnikClick;
  end;

  { Forscher Button }
  ForschButton:=TDXBitmapButton.Create(Self);
  with ForschButton do
  begin
    SetRect(129,54,120,28);
    SetButtonFont(Font);
    FirstColor:=coFirstColor;
    SecondColor:=coSecondColor;
    BlendColor:=bcNavy;
    AccerlateColor:=coAccerlateColor;
    RoundCorners:=rcRight;
    Text:=BForscher;
    Hint:=HAMForscher;
    OnClick:=ForscherClick;
  end;

  { Techniker/Wissenschaftler auf dem Arbeitsmarkt}
  { ListBox }
  KaufListBox:=TDXListBox.Create(Self);
  with KaufListBox do
  begin
    SetRect(508,103,284,420);
    HeadBlendColor:=bcNavy;
    BorderColor:=coFirstColor;
    FocusColor:=coSecondColor;
    SetFont(Font);
    SetButtonFont(ScrollBarFont);
    ItemHeight:=58;
    FirstColor:=bcNavy;
    SecondColor:=coScrollPen;
    OwnerDraw:=true;
    OnDrawItem:=DrawKaufList;
    OnDblClick:=ListDblClick;
    HeadRowHeight:=17;
    HeadData:=MakeHeadData(LZivilist,FAMZivilist,FAMZivilisten);
  end;

{ Buttons }
  { Kaufen }
  BuyButton:=TDXBitmapButton.Create(Self);
  with BuyButton do
  begin
    SetRect(388,103,110,28);
    SetButtonFont(Font);
    BlendColor:=bcNavy;
    FirstColor:=coFirstColor;
    SecondColor:=coSecondColor;
    AccerlateColor:=coAccerlateColor;
    OnClick:=BuyClick;
    Text:=BBuy;
    RoundCorners:=rcTop;
    Hint:=HAMBuyZivi;
  end;

  { Verkaufen }
  SellButton:=TDXBitmapButton.Create(Self);
  with SellButton do
  begin
    SetRect(388,132,110,28);
    SetButtonFont(Font);
    FirstColor:=coFirstColor;
    SecondColor:=coSecondColor;
    BlendColor:=bcNavy;
    AccerlateColor:=coAccerlateColor;
    Text:=BSell;
    OnClick:=SellClick;
    RoundCorners:=rcNone;
  end;

  { Umbennenbutton }
  RenameButton:=TDXBitmapButton.Create(Self);
  with RenameButton do
  begin
    SetRect(388,161,110,28);
    SetButtonFont(Font);
    FirstColor:=coFirstColor;
    SecondColor:=coSecondColor;
    RoundCorners:=rcNone;
    AccerlateColor:=coAccerlateColor;
    BlendColor:=bcNavy;
    OnClick:=RenameClick;
    Text:=BSRename;
  end;

  { �berwachen }
  WatchButton:=TDXBitmapButton.Create(Self);
  with WatchBUtton do
  begin
    FirstColor:=coFirstColor;
    SecondColor:=coSecondColor;
    AccerlateColor:=coAccerlateColor;
    RoundCorners:=rcBottom;
    SetButtonFont(Font);
    SetRect(388,190,110,28);
    Text:=ST0309270019;
    OnClick:=WatchMarktClick;
    BlendColor:=bcNavy;
  end;

  WatchDialog:=TWatchMarktDialog.Create(Self);
  SetButtonFont(WatchDialog.Font);
  WatchDialog.Gruppenanhang:='s';

  UnionRect(fAreaRect,HintLabel.ClientRect,InfoFrame.ClientRect);

  basis_api_RegisterChangeBaseHandler(OnSelectedBaseChange);
end;

procedure TVerArbeitsmarkt.DrawKaufList(Sender: TDXComponent;
  Surface: TDirectDrawSurface;const Mem: TDDSurfaceDesc; Rect: TRect; Index: Integer;
  Selected: boolean);
var
  Text   : String;
begin
  if Selected then
  begin
    KaufListBox.DrawSelection(Surface,Mem,Rect,bcBlue);
  end;
  if ActivePage=ptTechnik then
  begin
    SaveGame.WerkStatt.DrawDXKaufTechniker(Index,Surface,Mem,Rect.Left+6,Rect.Top+6);
    YellowStdFont.Draw(Surface,Rect.Left+49,Rect.Top+6,KaufListBox.Items[Index]);
    WhiteStdFont.Draw(Surface,Rect.Left+49,Rect.Top+26,Format(FFaehig,[SaveGame.WerkStatt.KaufTechniker[Index].Strength]));
    Text:=Format(FWert,[SaveGame.WerkStatt.KaufTechniker[Index].Strength*TechnikerBuy]);
  end
  else
  begin
    SaveGame.ForschListe.DrawDXKaufForscher(Index,Surface,Mem,Rect.Left+6,Rect.Top+6);
    YellowStdFont.Draw(Surface,Rect.Left+49,Rect.Top+6,KaufListBox.Items[Index]);
    WhiteStdFont.Draw(Surface,Rect.Left+49,Rect.Top+26,Format(FFaehig,[SaveGame.ForschListe.KaufForscher[Index].Strength]));
    Text:=Format(FWert,[SaveGame.ForschListe.KaufForscher[Index].Strength*ForscherBuy]);
  end;
  WhiteStdFont.Draw(Surface,Rect.Right-8-WhiteStdFont.TextWidth(Text),Rect.Top+26,Text);
end;

procedure TVerArbeitsmarkt.DrawYourList(Sender: TDXComponent;
  Surface: TDirectDrawSurface;const Mem: TDDSurfaceDesc; Rect: TRect; Index: Integer;
  Selected: boolean);
var
  Text   : String;
  SIndex : Integer;
begin
  if Selected then
  begin
    YourListBox.DrawSelection(Surface,Mem,Rect,bcBlue);
  end;
  SIndex:=StrToInt(YourListBox.Items[Index]);
  if ActivePage=ptTechnik then
  begin
    SaveGame.WerkStatt.DrawDXTechniker(SIndex,Surface,Mem,Rect.Left+6,Rect.Top+6);
    YellowStdFont.Draw(Surface,Rect.Left+49,Rect.Top+6,SaveGame.WerkStatt.Techniker[SIndex].Name);
    WhiteStdFont.Draw(Surface,Rect.Left+49,Rect.Top+26,Format(FFaehig,[trunc(SaveGame.WerkStatt.Techniker[SIndex].Strength)/1]));
    Text:=Format(FWert,[SaveGame.WerkStatt[SIndex].Strength*TechnikerSell]);
  end
  else
  begin
    SaveGame.ForschListe.DrawDXForscher(SIndex,Surface,Mem,Rect.Left+6,Rect.Top+6);
    YellowStdFont.Draw(Surface,Rect.Left+49,Rect.Top+6,SaveGame.ForschListe.Forscher[SIndex].Name);
    WhiteStdFont.Draw(Surface,Rect.Left+49,Rect.Top+26,Format(FFaehig,[trunc(SaveGame.ForschListe.Forscher[SIndex].Strength)/1]));
    Text:=Format(FWert,[SaveGame.ForschListe[SIndex].Strength*ForscherSell]);
  end;
  WhiteStdFont.Draw(Surface,Rect.Right-8-WhiteStdFont.TextWidth(Text),Rect.Top+26,Text);
end;

procedure TVerArbeitsmarkt.ForscherClick(Sender: TObject);
begin
  if ActivePage=ptForscher then exit;
  ActivePage:=ptForscher;
  ChangePageState;
end;

procedure TVerArbeitsmarkt.RenameClick(Sender: TObject);
var
  Name    : String;
begin
  if ActivePage=ptTechnik then
  begin
    Name:=SaveGame.WerkStatt.Techniker[GetSelectedIndex].Name;
    QueryText(CRename,30,Name,ctBlue);
    if Name='' then exit;
    SaveGame.WerkStatt.Rename(GetSelectedIndex,Name);
  end
  else
  begin
    Name:=SaveGame.ForschListe.Forscher[GetSelectedIndex].Name;
    QueryText(CRename,30,Name,ctBlue);
    if Name='' then exit;
    SaveGame.ForschListe.RenameForscher(GetSelectedIndex,Name);
  end;
  YourListBox.Redraw;
end;

procedure TVerArbeitsmarkt.SellClick(Sender: TObject);
begin
  Container.incLock;
  Container.LoadGame(true);
  try
    if ActivePage=ptTechnik then
    begin
      SaveGame.WerkStatt.SellTechniker(GetSelectedIndex);
    end
    else
    begin
      SaveGame.ForschListe.SellForscher(GetSelectedIndex);
    end;
  except
    on E: Exception do
    begin
      Container.LoadGame(false);
      Container.decLock;
      game_api_MessageBox(E.Message,CCancelAction);
      exit;
    end;
  end;
  AktuButtons;
  ChangePageState;
  Container.LoadGame(false);
  Container.DecLock;
  Container.RedrawArea(fAreaRect,Container.Surface);
end;

procedure TVerArbeitsmarkt.SetDefaults;
begin
  InfoFrame.Reset;
  ChangePageState;
end;

procedure TVerArbeitsmarkt.TechnikClick(Sender: TObject);
begin
  if ActivePage=ptTechnik then exit;
  ActivePage:=ptTechnik;
  ChangePageState;
end;

procedure TVerArbeitsmarkt.ListDblClick(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  if Button=mbLeft then
  begin
    if Sender=YourListBox then SellButton.DoClick
    else if Sender=KaufListBox then BuyButton.DoClick;
  end;
end;

{ Wird ausgel�st, wenn die Basis �ber den Infoframe ge�ndert wurde
  aktualisiert die Liste mit den Technikern/Wissenschaftlern }
procedure TVerArbeitsmarkt.OnSelectedBaseChange(Sender: TObject);
begin
  if Container.ActivePage<>Self then
    exit;

  ChangePageState;
end;

{ Gibt den Index des ausgew�hlten Techniker/Wissenschaftler zur�ck }
function TVerArbeitsmarkt.GetSelectedIndex: Integer;
begin
  if YourListBox.ItemIndex=-1 then result:=-1
  else result:=StrToInt(YourListBox.Items[YourListBox.ItemIndex]);
end;

procedure TVerArbeitsmarkt.WatchMarktClick(Sender: TObject);
var
  Watch: TWatchMarktSettings;
begin
  if ActivePage=ptTechnik then
  begin
    WatchDialog.PersonenGruppe:=STechniker;
    if SaveGame.Werkstatt.GetWatch(SaveGame.BasisListe.Selected.ID,Watch) then
    begin
      WatchDialog.Aktiv:=true;
      WatchDialog.Faehigkeiten:=Watch.minFaehigkeiten;
      WatchDialog.UeberwachungsType:=Watch.WatchType;
    end
    else
      WatchDialog.Aktiv:=false;
  end
  else
  begin
    WatchDialog.PersonenGruppe:=SForscher;
    if SaveGame.ForschListe.GetWatch(SaveGame.BasisListe.Selected.ID,Watch) then
    begin
      WatchDialog.Aktiv:=true;
      WatchDialog.Faehigkeiten:=Watch.minFaehigkeiten;
      WatchDialog.UeberwachungsType:=Watch.WatchType;
    end
    else
      WatchDialog.Aktiv:=false;
  end;
  if WatchDialog.ShowModal=mrOK then
  begin
    if ActivePage=ptTechnik then
    begin
      if WatchDialog.Aktiv then
        SaveGame.Werkstatt.SetWatch(SaveGame.BasisListe.Selected.ID,WatchDialog.Faehigkeiten,WatchDialog.UeberwachungsType)
      else
        SaveGame.WerkStatt.CancelWatch(SaveGame.BasisListe.Selected.ID);
    end
    else
    begin
      if WatchDialog.Aktiv then
        SaveGame.ForschListe.SetWatch(SaveGame.BasisListe.Selected.ID,WatchDialog.Faehigkeiten,WatchDialog.UeberwachungsType)
      else
        SaveGame.ForschListe.CancelWatch(SaveGame.BasisListe.Selected.ID);
    end;
  end;
end;

destructor TVerArbeitsmarkt.Destroy;
begin
  WatchDialog.Free;
  inherited;
end;

end.
