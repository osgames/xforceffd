{********************************************************************************
*										*
* Copyright (C) 2004  X-Force Team - jim_raynor@web.de				*
*										*
* This file is part of X-Force: Fight For Destiny (X-Force).			*
*										*
* X-Force is free software; you can redistribute it and/or modify		*
* it under the terms of the GNU General Public License as published by		*
* the Free Software Foundation; either version 2 of the License, or		*
* (at your option) any later version.						*
*										*
* X-Force is distributed in the hope that it will be useful,			*
* but WITHOUT ANY WARRANTY; without even the implied warranty of		*
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the			*
* GNU General Public License for more details.					*
*										*
* You should have received a copy of the GNU General Public License		*
* along with X-Force; if not, write to the Free Software			*
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA	*
*										*
*********************************************************************************
*										*
* Hauptmen� zu X-Force								*
*										*
********************************************************************************}

{$I ../Settings.inc}

unit MainPage;

{$DEFINE TRYEXCEPT}
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  DXBitmapButton,KD4Page,DXContainer, Defines, DXMessage, DXLabel, DXListBox,
  XForce_types, DXDraws, KD4HighScore, Blending, KD4Utils,DXGroupCaption, ArchivFile,
  DXScrollBar, DXCheckBox, TraceFile, DXQueryBox, DXCheckList, DXUFOKampf,
  DXTakticScreen, DXListBoxWindow, DirectDraw, DirectFont, DXSpinEdit,
  DXISOEngine, DXTextViewer, HelpManager, Sequenzer, Registry, ShlObj, ShellApi,
  StringConst, NGTypes;

const
  coFirstColor  = clNavy;
  coSecondColor = clBlue;
  coScrollPen   = clNavy;

type
  TListStatus=(lsNone,lsLoad,lsDelete);

  TMainPage = class(TKD4Page)
  private
    // Tempor�res Spiel speichern
    fTempStream: TStream;
    fTempName  : String;

    procedure ChangeChecked(Sender: TObject);
    procedure ChangeVolume(Sender: TObject);
    procedure ChangeMusic(Sender: TObject);
    procedure ChangeAlpha(Sender: TObject);
    procedure ChangeShadow(Sender: TObject);
    procedure ChangeGamma(Sender: TObject);
    procedure SetButtons;
    procedure InitTexte;

    procedure ShowSettings(Show: boolean);
    procedure ShowSoundGraphic(Show: boolean);
    procedure ShowGamePlayOptions(Show: boolean);

    procedure ShowMission(Show: boolean);
    procedure FillMissionList;
    procedure NeedDetailName(Value: Integer;var Text: String);
    procedure ShowTipps(Visible: Boolean);

    procedure StartGameHandler(Sender: TObject);
    procedure LoadGameSetHandler(Sender: TObject);
    
    procedure HighScoreChangeHandler(Sender: TObject);
    { Private-Deklarationen }
  protected
    ListStatus   : TListStatus;
    ButtonState  : TListStatus;
    GameStatus   : Array of TGameStatus;
    Archiv       : TArchivFile;
    fLoadPlayers : TStringList;
    fDelePlayers : TStringList;
    Files        : Array of TSetIdentifier;
    procedure SetLoadStatus(Sender: TObject);
    procedure SimulationClick(Sender: TObject);
    procedure SettingsClick(Sender: TObject);
    procedure CloseClick(Sender: TObject);

    procedure GamePlayClick(Sender: TObject);
    procedure GraphicSoundClick(Sender: TObject);

    procedure BackButtonClick(Sender: TObject);

    // Gameplay Eigenschaften
    procedure AlwaysFastChange(Sender: TObject);
    procedure FreeLookChange(Sender: TObject);
    procedure ChangeTastaturScroll(Sender: TObject);
    procedure ChangeCompatibleMode(Sender: TObject);

    procedure NewGameClick(Sender: TObject);
    procedure SinglePlayerClick(Sender: TObject);
    procedure MultiPlayerClick(Sender: TObject);
    procedure SetDeleteStatus(Sender: TObject);
    procedure DeletePlayerClick(Sender: TObject);
    procedure FillGames(Sender: TObject);
    procedure GameDblClick(Sender: TObject;Button: TMouseButton;Shift: TShiftState;X,Y: Integer);

    procedure DrawSaveGame(Sender: TDXComponent;Surface: TDirectDrawSurface;const Mem: TDDSurfaceDesc;Rect: TRect; Index: Integer;Selected: boolean);
    procedure DrawHighScore(Sender: TDXComponent;Surface: TDirectDrawSurface;const Mem: TDDSurfaceDesc;Rect: TRect; Index: Integer;Selected: boolean);
    procedure DrawMission(Sender: TDXComponent;Surface: TDirectDrawSurface;const Mem: TDDSurfaceDesc;Rect: TRect; Index: Integer;Selected: boolean);

    procedure DeleteGameClick(Sender: TObject);
    procedure LoadGameClick(Sender: TObject);
    procedure HighScoreClick(Sender: TObject);

    { Skriptdebugging}
    procedure MissionsTestClick(Sender: TObject);
    procedure MissionStartClick(Sender: TObject);
    procedure RefreshMissions(Sender: TObject);
    procedure MissionEditClick(Sender: TObject);
    procedure MissionListDblClick(Sender: TObject;Button: TMouseButton;Shift: TShiftState;X,Y: Integer);

    { Tipps und Tricks }
    procedure LastClick(Sender: TObject);
    procedure NextClick(Sender: TObject);

    procedure LoadLastGame(Sender: TObject);
    procedure FillPlayers;
    procedure ReadGameSets;

    procedure InitApi;
    { Protected-Deklarationen }
  public
    BackButtonActive  : boolean;
    MessageBox        : TDXMessage;
    QueryTextBox      : TDXQueryBox;
    Version           : TDXLabel;
    Modus             : TDXGroupCaption;
    NewGame           : TDXBitmapButton;
    LoadGame          : TDXBitmapButton;
    DeleteGame        : TDXBitmapButton;
    KampfSimu         : TDXBitmapButton;
    MissionsTest      : TDXBitmapButton;
    BackButton        : TDXBitmapButton;
    SinglePlayer      : TDXBitmapButton;
    MultiPlayer       : TDXBitmapButton;
    HighScore         : TDXBitmapButton;
    CloseButton       : TDXBitmapButton;
    Settings          : TDXBitmapButton;
    Credits           : TDXBitmapButton;
    KeyBoard          : TDXBitmapButton;
    GraphicSound      : TDXBitmapButton;

    // Komponenten Gameplay
    SchiffCaption     : TDXGroupCaption;
    FreeLookBox       : TDXCheckBox;
    EinsatzCaption    : TDXGroupCaption;
    TastaturCaption   : TDXGroupCaption;
    TastaturLabel     : TDXGroupCaption;
    TastaturScroll    : TDXScrollBar;
    AllgemeinCaption  : TDXGroupCaption;
    AlwaysFast        : TDXCheckBox;
    CompatibleBox     : TDXCheckBox;

    // Ende Komponenten Gameplay

    Gameplay          : TDXBitmapButton;
    ListLabel         : TDXGroupCaption;
    PlayerList        : TDXListBox;
    GamesBox          : TDXListBox;
    HighScoreBox      : TDXListBox;
    DeletePlayer      : TDXBitmapButton;
    LDGame            : TDXBitmapButton;
    SoundFrame        : TDXGroupCaption;

    // Soundeffektlautst�rke
    VolumeText        : TDXGroupCaption;
    VolumeLabel       : TDXGroupCaption;
    VolumeBar         : TDXScrollBar;

    // Musiklautst�rke
    MusicText         : TDXGroupCaption;
    MusicLabel        : TDXGroupCaption;
    MusicBar          : TDXScrollBar;

    // Men�transparenz
    AlphaText         : TDXGroupCaption;
    AlphaLabel        : TDXGroupCaption;
    AlphaBar          : TDXScrollBar;

    // Gamma-Korrektur
    GammaText         : TDXGroupCaption;
    GammaLabel        : TDXGroupCaption;
    GammaBar          : TDXScrollBar;

    GraphicLabel      : TDXGroupCaption;
    ShadowLabel       : TDXGroupCaption;
    ShadowEdit        : TDXSpinEdit;
    GraphicList       : TDXCheckList;

    // Skriptdebugging
    MissionList       : TDXListBox;
    MissRefresh       : TDXBitmapButton;
    MissEdit          : TDXBitmapButton;
    MissStart         : TDXBitmapButton;

    // Allgemein
    ListBoxWindow     : TDXListBoxWindow;

    { Tipps und Tricks }
    fTextViewer       : TDXTextViewer;
    GCTipp            : TDXGroupCaption;
    LastButton        : TDXBitmapButton;
    NextButton        : TDXBitmapButton;
    HManager          : THelpManager;

    constructor Create(Container: TDXContainer);override;
    destructor Destroy;override;
    procedure SetDefaults;override;
    procedure ActivateBack;
    procedure LoadHelpData(Stream : TStream); overload;
    procedure LoadHelpData; overload;

  end;

implementation

uses GlobeRenderer, GameMenu, game_api, highscore_api, savegame_api,
  gameset_api, MissionList, string_utils, D3DRender, ReadLanguage;

{ TMainPage }

procedure TMainPage.ActivateBack;
begin
  BackButtonActive:=true;
  BackButton.Text:=BBack;
  BackButton.Onclick:=BackButtonClick;
end;

procedure TMainPage.AlwaysFastChange(Sender: TObject);
begin
  AlwaysFastTime:=AlwaysFast.Checked;
end;

procedure TMainPage.BackButtonClick(Sender: TObject);
begin
  SaveGame.LoadGame(fTempName,fTempStream);
  fTempStream.Free;
  fTempStream:=nil;

  ChangePage(PageGameMenu);
end;

procedure TMainPage.ChangeAlpha(Sender: TObject);
begin
  Container.Lock;
  if AlphaBar.Value>100 then
    AlphaLabel.Caption:=Format('+%d %%',[AlphaBar.Value-100])
  else if AlphaBar.Value=100 then
    AlphaLabel.Caption:='Standard'
  else
    AlphaLabel.Caption:=Format('%d %%',[AlphaBar.Value-100]);
  Container.Unlock;

  AlphaBlendFaktor:=AlphaBar.Value/100;
  Container.ReDraw(Container.Surface);
end;

procedure TMainPage.ChangeChecked(Sender: TObject);
begin
{  if AlphaElements<>GraphicList.Checked[0] then
  begin
    Container.Lock;
    AlphaElements:=GraphicList.Checked[0];
    GraphicList.ItemEnabled[1]:=GraphicList.Checked[0];
    if not AlphaElements then
    begin
      AlphaControls:=false;
      GraphicList.Checked[1]:=false;
    end;
    Container.Unlock;
    Container.ReDraw(Container.Surface);
  end;}
  if AlphaControls<>GraphicList.Checked[0] then
  begin
    AlphaControls:=GraphicList.Checked[0];
    Container.ReDraw(Container.Surface);
  end;
{  if Container.PageAnimation<>GraphicList.Checked[2] then
  begin
    Container.PageAnimation:=GraphicList.Checked[2];
    GraphicList.ItemEnabled[3]:=GraphicList.Checked[2];
  end;}
  Container.PageAnimation:=GraphicList.Checked[1];
  Container.Scrolling:=not GraphicList.Checked[1];

  if Container.MouseAnimation<>GraphicList.Checked[2] then
    Container.MouseAnimation:=GraphicList.Checked[2];

  if Container.MouseShadow<>GraphicList.Checked[3] then
    Container.MouseShadow:=GraphicList.Checked[3];

  ShowStarField:=GraphicList.Checked[4];
  TransparentEnergy:=GraphicList.Checked[5];
  GradNetz:=GraphicList.Checked[6];

  Enabled3DAlphaBlend:=GraphicList.Checked[7];

  ShowSensorMap:=GraphicList.Checked[8];
end;

procedure TMainPage.ChangeCompatibleMode(Sender: TObject);
begin
  DXTakticScreen.SetCompatibleMode(CompatibleBox.Checked);
end;

procedure TMainPage.ChangeGamma(Sender: TObject);
begin
  if GammaBar.Value>0 then
    GammaLabel.Caption:=Format('+%d %%',[GammaBar.Value])
  else if GammaBar.Value=0 then
    GammaLabel.Caption:='Standard'
  else
    GammaLabel.Caption:=Format('%d %%',[GammaBar.Value]);

  Container.Gamma:=GammaBar.Value;
end;

procedure TMainPage.ChangeMusic(Sender: TObject);
begin
  if MusicBar.Value<1 then
    MusicLabel.Caption:=LNoMusic else
  begin
    MusicLabel.Caption:=Format(FPercent,[MusicBar.Value]);
  end;
  Container.MusicVolume:=MusicBar.Value;
end;

procedure TMainPage.ChangeShadow(Sender: TObject);
begin
  LightDetail:=TLightDetail(ShadowEdit.Value);
end;

procedure TMainPage.ChangeTastaturScroll(Sender: TObject);
begin
  TastaturLabel.Caption:=Format(FPercent,[TastaturScroll.Value]);
  DXISOEngine.ScrollTempo:=TastaturScroll.Value/100;
end;

procedure TMainPage.ChangeVolume(Sender: TObject);
begin
  if VolumeBar.Value<1 then
    VolumeLabel.Caption:=LNoSound else
  VolumeLabel.Caption:=Format(FPercent,[VolumeBar.Value]);
  Container.Volume:=VolumeBar.Value;
end;

procedure TMainPage.CloseClick(Sender: TObject);
begin
  game_api_quitGame;
end;

constructor TMainPage.Create(Container: TDXContainer);
var
  Reg: TRegistry;
  LoadStream : TFileStream;
begin
  inherited;
  ListStatus:=lsNone;
  ButtonState:=lsNone;
  fLoadPlayers:=TStringList.Create;
  fDelePlayers:=TStringList.Create;
  Archiv:=TArchivFile.Create;
  HintLabel.BorderColor:=coFirstColor;
  HintLabel.BlendColor:=bcNavy;

  { Versions Label}
  Version:=TDXLabel.Create(Self);
  with Version do
  begin
    SetFont(Font);
    SetRect(ScreenWidth-300,0,300,13);
    Alignment:=taRightJustify;
    AutoSize:=false;
  end;

  { NachrichtenBox erstellen und initialisieren }
  MessageBox:=TDXMessage.Create(Self);
  with MessageBox do
  begin
    SetButtonFont(Font);
    AccerlateColor:=coAccerlateColor;
    Font.Size:=coFontSize;
  end;

  { QueryTextbox erstellen }
  QueryTextBox:=TDXQueryBox.Create(Self);
  SetButtonFont(QueryTextBox.Font);

//******************************************************************
// Hauptmen� - Buttons
//******************************************************************
  { Button f�r Neues Spiel erstellen und initialisieren }
  SinglePlayer:=TDXBitmapButton.Create(Self);
  with SinglePlayer do
  begin
    SetButtonFont(Font);
    AccerlateColor:=coAccerlateColor;
    FirstColor:=clNavy;
    SecondColor:=coSecondColor;
    RoundCorners:=rcLeftTop;
    Text:=BSinglePlayer;
    Hint:=HSinglePlayer;
    SetRect(99,504,200,28);
    BlendColor:=bcNavy;
    OnClick:=SinglePlayerClick;
  end;

  { Button f�r Multiplayer erstellen und initialisieren }
  MultiPlayer:=TDXBitmapButton.Create(Self);
  with MultiPlayer do
  begin
    SetButtonFont(Font);
    AccerlateColor:=coAccerlateColor;
    FirstColor:=clNavy;
    SecondColor:=coSecondColor;
    RoundCorners:=rcNone;
    BlendColor:=bcNavy;
    Text:=BMultiPlayer;
    Hint:=HMultiPlayer;
    SetRect(300,504,200,28);
    OnClick:=MultiplayerClick;
  end;

  { CloseButton}
  CloseButton:=TDXBitmapButton.Create(Self);
  with CloseButton do
  begin
    SetButtonFont(Font);
    Text:=BExitGame;
    BlendColor:=bcNavy;
    AccerlateColor:=coAccerlateColor;
    FirstColor:=coFirstColor;
    SecondColor:=coSecondColor;
    Hint:=HExit;
    RoundCorners:=rcRightTop;
    SetRect(501,504,200,28);
    EscButton:=true;
    OnClick:=CloseClick;
  end;

  { HighScore Button }
  HighScore:=TDXBitmapButton.Create(Self);
  with HighScore do
  begin
    SetButtonFont(Font);
    Text:=BHighScore;
    AccerlateColor:=coAccerlateColor;
    BlendColor:=bcNavy;
    FirstColor:=coFirstColor;
    SecondColor:=coSecondColor;
    RoundCorners:=rcLeftBottom;
    Hint:=HHighScore;
    SetRect(99,533,200,28);
    OnClick:=HighScoreClick;
  end;

  { Settings Button }
  Settings:=TDXBitmapButton.Create(Self);
  with Settings do
  begin
    SetButtonFont(Font);
    Text:=BSettings;
    AccerlateColor:=coAccerlateColor;
    BlendColor:=bcNavy;
    FirstColor:=coFirstColor;
    SecondColor:=coSecondColor;
    Hint:=HSettings;
    RoundCorners:=rcNone;
    SetRect(300,533,200,28);
    OnClick:=SettingsClick;
  end;

  { Credits Button }
  Credits:=TDXBitmapButton.Create(Self);
  with Credits do
  begin
    SetButtonFont(Font);
    Text:=BCredits;
    BlendColor:=bcNavy;
    PageIndex:=PageCredits;
    AccerlateColor:=coAccerlateColor;
    FirstColor:=coFirstColor;
    RoundCorners:=rcRightBottom;
    SecondColor:=coSecondColor;
    Hint:=HCredits;
    SetRect(501,533,200,28);
  end;

//******************************************************************
// Einzelspieler
//******************************************************************

  // Men�

  { Moduslabel }
  Modus:=TDXGroupCaption.Create(Self);
  with Modus do
  begin
    SetFont(Font);
    Font.Size:=Font.Size+2;
    SetRect(99,30,130,24);
    RoundCorners:=rcTop;
    Caption:=LSinglePlayer;
    BlendColor:=bcNavy;
    BorderColor:=coFirstColor;
  end;

  { neues Spiel }
  NewGame:=TDXBitmapButton.Create(Self);
  with NewGame do
  begin
    SetButtonFont(Font);
    Text:=BNewGame;
    AccerlateColor:=coAccerlateColor;
    FirstColor:=clNavy;
    RoundCorners:=rcNone;
    BlendColor:=bcNavy;
    SecondColor:=coSecondColor;
    Hint:=HNewGame;
    SetRect(99,55,130,28);
    OnClick:=NewGameClick;
  end;

  { Kampfsimulator }
  KampfSimu:=TDXBitmapButton.Create(Self);
  with KampfSimu do
  begin
    SetButtonFont(Font);
    Text:=BKampfSimu;
    AccerlateColor:=coAccerlateColor;
    BlendColor:=bcNavy;
    FirstColor:=clNavy;
    RoundCorners:=rcNone;
    SecondColor:=coSecondColor;
    Hint:=HKampfSimu;
    SetRect(99,84,130,28);
    OnClick:=SimulationClick;

    Visible:=false;
  end;

  { Mission Test }
  MissionsTest:=TDXBitmapButton.Create(Self);
  with MissionsTest do
  begin
    SetButtonFont(Font);
    Text:=BMissionsTest;
    BlendColor:=bcNavy;
    AccerlateColor:=coAccerlateColor;
    FirstColor:=coFirstColor;
    RoundCorners:=rcAll;
    SecondColor:=coSecondColor;
    Hint:=HMissionsTest;
    OnClick:=MissionsTestClick;
    RoundCorners:=rcNone;
    SetRect(99,84,130,28);
  end;

  { BackButton }
  BackButton:=TDXBitmapButton.Create(Self);
  with BackButton do
  begin
    SetButtonFont(Font);
    Text:=BBack;
    BlendColor:=bcNavy;
    AccerlateColor:=coAccerlateColor;
    FirstColor:=coFirstColor;
    RoundCorners:=rcAll;
    SecondColor:=coSecondColor;
    Hint:=HBack;
    PageIndex:=PageGameMenu;
    RoundCorners:=rcNone;
    SetRect(99,113,130,28);
  end;

  { LoadGame Button}
  LoadGame:=TDXBitmapButton.Create(Self);
  with LoadGame do
  begin
    SetButtonFont(Font);
    Text:=BLoadGame;
    AccerlateColor:=coAccerlateColor;
    FirstColor:=coFirstColor;
    BlendColor:=bcNavy;
    RoundCorners:=rcNone;
    SecondColor:=coSecondColor;
    Hint:=HLoadGame;
    SetRect(99,142,130,28);
    OnClick:=SetLoadStatus;
  end;

  { DeleteGame Button}
  DeleteGame:=TDXBitmapButton.Create(Self);
  with DeleteGame do
  begin
    SetButtonFont(Font);
    Text:=BDeleteGame;
    AccerlateColor:=coAccerlateColor;
    FirstColor:=coFirstColor;
    BlendColor:=bcNavy;
    SecondColor:=coSecondColor;
    RoundCorners:=rcBottom;
    Hint:=HDelete;
    SetRect(99,171,130,28);
    OnClick:=SetDeleteStatus;
  end;

  // Laden/l�schen Listen

  { Laden/L�schen Label }
  ListLabel:=TDXGroupCaption.Create(Self);
  with ListLabel do
  begin
    SetFont(Font);
    Font.Size:=Font.Size+2;
    SetRect(245,30,456,24);
    RoundCorners:=rcTop;
    Visible:=false;
    BlendColor:=bcNavy;
    BorderColor:=coFirstColor;
  end;

  { Listbox zur Anzeige der verf�gbaren Spieler }
  PlayerList:=TDXListBox.Create(Self);
  with PlayerList do
  begin
    SetFont(Font);
    SetRect(245,55,175,437);
    BorderColor:=coFirstColor;
    FocusColor:=coSecondColor;
    FirstColor:=bcNavy;
    SecondColor:=coScrollPen;
    SetButtonFont(ScrollBarFont);
    ItemHeight:=30;
    TopMargin:=8;
    LeftMargin:=6;
    Visible:=false;
    RoundCorners:=rcLeftBottom;
    OnChange:=FillGames;
    HeadRowHeight:=17;
    HeadData:=MakeHeadData(LPlayers,FPlayers,FPlayers);
  end;

  { Listbox zur Anzeige der verf�gbaren Savegames }
  GamesBox:=TDXListBox.Create(Self);
  with GamesBox do
  begin
    SetFont(Font);
    SetRect(421,55,280,408);
    BorderColor:=coFirstColor;
    FocusColor:=coSecondColor;
    FirstColor:=bcNavy;
    SecondColor:=coScrollPen;
    SetButtonFont(ScrollBarFont);
    ItemHeight:=45;
    TopMargin:=5;
    Visible:=false;
    Hint:=HSelectGame;
    RoundCorners:=rcNone;
//    Items.Sorted:=true;
//    Items.Duplicates:=dupAccept;
    OnDrawItem:=DrawSaveGame;
    OwnerDraw:=true;
    OnDblClick:=GameDblClick;
    HeadRowHeight:=17;
    HeadData:=MakeHeadData(LSaveGame,FSaveGame,FSaveGames);
  end;

  { Spieler l�schen }
  DeletePlayer:=TDXBitmapButton.Create(Self);
  with DeletePlayer do
  begin
    SetButtonFont(Font);
    SetRect(494,344,20,28);
    FirstColor:=clNavy;
    SecondColor:=coSecondColor;
    AccerlateColor:=coAccerlateColor;
    BlendColor:=bcNavy;
    RoundCorners:=rcNone;
    Hint:=HDeletePlayer;
    Text:=BDeletePlayer;
    Visible:=false;
    OnClick:=DeletePlayerClick;
  end;

  { Spiele laden/l�schen }
  LDGame:=TDXBitmapButton.Create(Self);
  with LDGame do
  begin
    SetButtonFont(Font);
    SetRect(481,344,300,28);
    FirstColor:=clNavy;
    SecondColor:=coSecondColor;
    RoundCorners:=rcRightBottom;
    AccerlateColor:=coAccerlateColor;
    BlendColor:=bcNavy;
    Visible:=false;
  end;

//******************************************************************
// Highscore
//******************************************************************

  { HighScore-Box }
  HighScoreBox:=TDXListBox.Create(Self);
  with HighScoreBox do
  begin
    SetRect(133,50,534,437);
    BorderColor:=coFirstColor;
    FocusColor:=coSecondColor;
    FirstColor:=bcNavy;
    SecondColor:=coScrollPen;
    SetButtonFont(Font);
    SetButtonFont(ScrollBarFont);
    ItemHeight:=20;
    OwnerDraw:=true;
    HeadRowHeight:=17;
    HeadData:=MakeHeadData(LHighscore,FEintrag,FEintrage);
    OnDrawItem:=DrawHighScore;
    Visible:=false;
  end;

//******************************************************************
// Einstellungen
//******************************************************************

// Men�

  { TastaturEinstellungen }
  KeyBoard:=TDXBitmapButton.Create(Self);
  with KeyBoard do
  begin
    SetButtonFont(Font);
    Text:=BKeyBoard;
    AccerlateColor:=coAccerlateColor;
    FirstColor:=coFirstColor;
    SecondColor:=coSecondColor;
    BlendColor:=bcNavy;
    Hint:=HKeyBoard;
    SetRect(99,55,130,28);
    RoundCorners:=rcNone;
    PageIndex:=PageKeyBoard;
  end;

  { Grafik/Sound-Einstellungen }
  GraphicSound:=TDXBitmapButton.Create(Self);
  with GraphicSound do
  begin
    SetButtonFont(Font);
    Text:=ST0309260035;
    AccerlateColor:=coAccerlateColor;
    FirstColor:=coFirstColor;
    SecondColor:=coSecondColor;
    BlendColor:=bcNavy;
    Hint:=ST0309260036;
    SetRect(99,84,130,28);
    RoundCorners:=rcNone;
    OnClick:=GraphicSoundClick;
  end;

  { Grafik/Sound-Einstellungen }
  GamePlay:=TDXBitmapButton.Create(Self);
  with GamePlay do
  begin
    SetButtonFont(Font);
    Text:=ST0309260037;
    AccerlateColor:=coAccerlateColor;
    FirstColor:=coFirstColor;
    SecondColor:=coSecondColor;
    BlendColor:=bcNavy;
    Hint:=ST0309260038;
    SetRect(99,113,130,28);
    RoundCorners:=rcBottom;
    OnClick:=GameplayClick;
  end;


// Soundeinstellungen
  { Frame um die Einstellungen }
  SoundFrame:=TDXGroupCaption.Create(Self);
  with SoundFrame do
  begin
    SetRect(245,30,411,21);
    SetFont(Font);
    Font.Style:=[fsBold];
    RoundCorners:=rcTop;
    Visible:=false;
    Caption:=LSoundSett;
    BlendColor:=bcNavy;
    BorderColor:=coFirstColor;
  end;

  { Lautst�rke Text }
  VolumeText:=TDXGroupCaption.Create(Self);
  with VolumeText do
  begin
    SetRect(245,52,300,21);
    SetFont(Font);
    Caption:=LVolumeBar;
    Visible:=false;
    BlendColor:=bcNavy;
    BorderColor:=coFirstColor;
  end;

  { Lautst�rkeregelung }
  VolumeBar:=TDXScrollBar.Create(Self);
  with VolumeBar do
  begin
    max:=100;
    FirstColor:=bcNavy;
    SecondColor:=coScrollPen;
    SetButtonFont(Font);
    RoundCorners:=rcNone;
    Kind:=sbHorizontal;
    SetRect(245,74,411,16);
    Hint:=HVolumeBar;
    LargeChange:=10;
    OnChange:=ChangeVolume;
    Interval:=50;
    SmallChange:=1;
    Visible:=false;
  end;

  { Lautst�rke Anzeige }
  VolumeLabel:=TDXGroupCaption.Create(Self);
  with VolumeLabel do
  begin
    SetRect(546,52,110,21);
    SetFont(Font);
    Visible:=false;
    Alignment:=taRightJustify;
    BlendColor:=bcNavy;
    BorderColor:=coFirstColor;
  end;

  { Musiklautst�rke Text }
  MusicText:=TDXGroupCaption.Create(Self);
  with MusicText do
  begin
    SetRect(245,92,300,21);
    SetFont(Font);
    Caption:=LMVolumeBar;
    Visible:=false;
    BlendColor:=bcNavy;
    BorderColor:=coFirstColor;
  end;

  { Mausiklautst�rke Regelung }
  MusicBar:=TDXScrollBar.Create(Self);
  with MusicBar do
  begin
    max:=100;
    FirstColor:=bcNavy;
    SecondColor:=coScrollPen;
    min:=0;
    SetButtonFont(Font);
    Kind:=sbHorizontal;
    SetRect(245,114,411,16);
    Hint:=HMVolumeBar;
    LargeChange:=10;
    RoundCorners:=rcNone;
    OnChange:=ChangeMusic;
    Interval:=50;
    SmallChange:=1;
    Visible:=false;
  end;

  { Musiklautst�rke Anzeige }
  MusicLabel:=TDXGroupCaption.Create(Self);
  with MusicLabel do
  begin
    SetRect(546,92,110,21);
    SetFont(Font);
    Visible:=false;
    Alignment:=taRightJustify;
    BlendColor:=bcNavy;
    BorderColor:=coFirstColor;
  end;

// Grafikeinstellungen
  { Grafik-Label }
  GraphicLabel:=TDXGroupCaption.Create(Self);
  with GraphicLabel do
  begin
    SetRect(245,132,411,20);
    SetFont(Font);
    Font.Style:=[fsBold];
    Caption:=LGraphicSett;
    Visible:=false;
    BlendColor:=bcNavy;
    BorderColor:=coFirstColor;
  end;

  // Option Transparenz der Men�s
  { Text }
  AlphaText:=TDXGroupCaption.Create(Self);
  with AlphaText do
  begin
    SetRect(245,153,300,21);
    SetFont(Font);
    Caption:=ST0309280001;
    Visible:=false;
    BlendColor:=bcNavy;
    BorderColor:=coFirstColor;
  end;

  { Regelung }
  AlphaBar:=TDXScrollBar.Create(Self);
  with AlphaBar do
  begin
    max:=200;
    FirstColor:=bcNavy;
    SecondColor:=coScrollPen;
    min:=0;
    SetButtonFont(Font);
    Kind:=sbHorizontal;
    SetRect(245,175,411,16);
    Hint:=ST0309280002;
    LargeChange:=10;
    RoundCorners:=rcNone;
    OnChange:=ChangeAlpha;
    Interval:=50;
    SmallChange:=1;
    Visible:=false;
  end;

  { Anzeige }
  AlphaLabel:=TDXGroupCaption.Create(Self);
  with AlphaLabel do
  begin
    SetRect(546,153,110,21);
    SetFont(Font);
    Visible:=false;
    Alignment:=taRightJustify;
    BlendColor:=bcNavy;
    BorderColor:=coFirstColor;
  end;

  // Option Gamma-Korrektur der Men�s
  { Text }
  GammaText:=TDXGroupCaption.Create(Self);
  with GammaText do
  begin
    SetRect(245,193,300,21);
    SetFont(Font);
    Caption:=ST0411280001;
    Visible:=false;
    BlendColor:=bcNavy;
    BorderColor:=coFirstColor;
  end;

  { Regelung }
  GammaBar:=TDXScrollBar.Create(Self);
  with GammaBar do
  begin
    max:=75;
    FirstColor:=bcNavy;
    SecondColor:=coScrollPen;
    min:=-75;
    SetButtonFont(Font);
    Kind:=sbHorizontal;
    SetRect(245,215,411,16);
    Hint:=ST0411280002;
    LargeChange:=10;
    RoundCorners:=rcNone;
    OnChange:=ChangeGamma;
    Interval:=50;
    SmallChange:=1;
    Visible:=false;
    Enabled:=Container.Primary.GammaControl<>nil;
  end;

  { Anzeige }
  GammaLabel:=TDXGroupCaption.Create(Self);
  with GammaLabel do
  begin
    SetRect(546,193,110,21);
    SetFont(Font);
    Visible:=false;
    Alignment:=taRightJustify;
    BlendColor:=bcNavy;
    BorderColor:=coFirstColor;
  end;

  { Schattenverlauf-Label }
  ShadowLabel:=TDXGroupCaption.Create(Self);
  with ShadowLabel do
  begin
    SetRect(245,233,300,21);
    SetFont(Font);
    Font.Style:=[];
    Caption:=LShadow;
    Visible:=false;
    BlendColor:=bcNavy;
    BorderColor:=coFirstColor;
  end;

  { Schattenverlauf Detail }
  ShadowEdit:=TDXSpinEdit.Create(Self);
  with ShadowEdit do
  begin
    SetRect(546,233,110,21);
    Min:=0;
    Max:=2;
    BlendColor:=bcNavy;
    BorderColor:=coFirstColor;
    RoundCorners:=rcNone;
    FocusColor:=coSecondColor;
    TopMargin:=5;
    Interval:=500;
    FormatString:=EmptyStr;
    GetValueText:=NeedDetailName;
//    Hint:=HMaxMessage;
    SetButtonFont(Font);
    OnIncrement:=ChangeShadow;
    OnDecrement:=ChangeShadow;
    Visible:=false;

    {$IFDEF LOWLIGHTDETAIL}
    Enabled:=false;
    {$ENDIF}
  end;

  { Checklist f�r Grafik Einstellungen }
  GraphicList:=TDXCheckList.Create(Self);
  with GraphicList do
  begin
    SetRect(245,255,411,198);
    RoundCorners:=rcBottom;
    BorderColor:=coFirstColor;
    FocusColor:=coSecondColor;
    ItemHeight:=22;
    FirstColor:=bcNavy;
    SecondColor:=coScrollPen;
    OnChange:=ChangeChecked;
    SetFont(Font);
    SetButtonFont(ScrollBarFont);
    TabStop:=false;
    Visible:=false;
  end;

//***************************************
// Komponenten f�r Gameplay Einstellungen
//***************************************

  // �berschrift Raumschiffkampf
  SchiffCaption:=TDXGroupCaption.Create(Self);
  with SchiffCaption do
  begin
    SetRect(245,30,411,21);
    SetFont(Font);
    Font.Style:=[fsBold];
    RoundCorners:=rcTop;
    Visible:=false;
    Caption:=ST0309260039;
    BlendColor:=bcNavy;
    BorderColor:=coFirstColor;
  end;

  // Freelook
  FreelookBox:=TDXCheckbox.Create(Self);
  with FreelookBox do
  begin
    SetRect(245,52,411,21);
    BorderColor:=coFirstColor;
    BlendColor:=bcNavy;
    SetButtonFont(Font);
    Caption:=ST0309260040;
    Hint:=ST0309260041;
    Visible:=false;
    RoundCorners:=rcNone;
    LeftMargin:=6;
    OnChange:=FreeLookChange;
  end;

  // �berschrift Bodeneinsatz
  EinsatzCaption:=TDXGroupCaption.Create(Self);
  with EinsatzCaption do
  begin
    SetRect(245,74,411,21);
    SetFont(Font);
    Font.Style:=[fsBold];
    RoundCorners:=rcNone;
    Visible:=false;
    Caption:=ST0309260042;
    BlendColor:=bcNavy;
    BorderColor:=coFirstColor;
  end;

  // �berschrift Tasturscroll
  TastaturCaption:=TDXGroupCaption.Create(Self);
  with TastaturCaption do
  begin
    SetRect(245,96,300,21);
    SetFont(Font);
    RoundCorners:=rcNone;
    Visible:=false;
    Caption:=ST0309260043;
    BlendColor:=bcNavy;
    BorderColor:=coFirstColor;
  end;

  // Anzeige TastaturScroll
  TastaturLabel:=TDXGroupCaption.Create(Self);
  with TastaturLabel do
  begin
    SetRect(546,96,110,21);
    SetFont(Font);
    Visible:=false;
    RoundCorners:=rcNone;
    Alignment:=taRightJustify;
    BlendColor:=bcNavy;
    BorderColor:=coFirstColor;
  end;

  // Scrollbar Tastatur
  TastaturScroll:=TDXScrollBar.Create(Self);
  with TastaturScroll do
  begin
    max:=200;
    min:=50;
    FirstColor:=bcNavy;
    SecondColor:=coScrollPen;
    SetButtonFont(Font);
    RoundCorners:=rcNone;
    Kind:=sbHorizontal;
    SetRect(245,118,411,16);
    Hint:=ST0309260044;
    LargeChange:=10;
    OnChange:=ChangeTastaturScroll;
    Interval:=50;
    Visible:=false;
  end;

  // �berschrift Bodeneinsatz
  AllgemeinCaption:=TDXGroupCaption.Create(Self);
  with AllgemeinCaption do
  begin
    SetRect(245,136,411,21);
    SetFont(Font);
    Font.Style:=[fsBold];
    RoundCorners:=rcNone;
    Visible:=false;
    Caption:=CAllgemein;
    BlendColor:=bcNavy;
    BorderColor:=coFirstColor;
  end;

  // Immer Schnelle Geschwindigkeit zulassen
  AlwaysFast:=TDXCheckbox.Create(Self);
  with AlwaysFast do
  begin
    SetRect(245,158,411,21);
    BorderColor:=coFirstColor;
    BlendColor:=bcNavy;
    SetButtonFont(Font);
    Caption:=CBAlwaysFast;
    Hint:=HAlwaysFast;
    Visible:=false;
    RoundCorners:=rcNone;
    LeftMargin:=6;
    OnChange:=AlwaysFastChange;
  end;

  CompatibleBox:=TDXCheckbox.Create(Self);
  with CompatibleBox do
  begin
    SetRect(245,180,411,21);
    BorderColor:=coFirstColor;
    BlendColor:=bcNavy;
    SetButtonFont(Font);
    Caption:=ST0310090002;
    Hint:=ST0310090003;
    Visible:=false;
    RoundCorners:=rcNone;
    LeftMargin:=6;
    OnChange:=ChangeCompatibleMode;
  end;

//********************************************
// Ende Komponenten f�r Gameplay Einstellungen
//********************************************

//************************************
// Komponenten f�r die Missionsskripte
//************************************
{ Missionstest }
  MissionList:=TDXListBox.Create(Self);
  with MissionList do
  begin
    SetRect(245,30,359,417);
    BorderColor:=coFirstColor;
    FocusColor:=coSecondColor;
    FirstColor:=bcNavy;
    SecondColor:=coScrollPen;
    SetButtonFont(Font);
    SetButtonFont(ScrollBarFont);
    RoundCorners:=rcTop;
    ItemHeight:=38;
    OwnerDraw:=true;
    HeadRowHeight:=17;
    HeadData:=MakeHeadData(LSkripte,FMission,FMissions);
    OnDrawItem:=DrawMission;
    Visible:=false;
    OnDblClick:=MissionListDblClick;
  end;

  { Liste aktualisieren }
  MissRefresh:=TDXBitmapButton.Create(Self);
  with MissRefresh do
  begin
    SetButtonFont(Font);
    Text:=ST0312070001;
    BlendColor:=bcNavy;
    AccerlateColor:=coAccerlateColor;
    FirstColor:=coFirstColor;
    RoundCorners:=rcLeftBottom;
    SecondColor:=coSecondColor;
    Hint:=ST0312070002;
    OnClick:=RefreshMissions;
    SetRect(245,448,119,28);
  end;

  { Mission bearbeiten }
  MissEdit:=TDXBitmapButton.Create(Self);
  with MissEdit do
  begin
    SetButtonFont(Font);
    Text:=ST0312070003;
    BlendColor:=bcNavy;
    AccerlateColor:=coAccerlateColor;
    FirstColor:=coFirstColor;
    RoundCorners:=rcNone;
    SecondColor:=coSecondColor;
    Hint:=ST0312070004;
    OnClick:=MissionEditClick;
    SetRect(365,448,119,28);
  end;

  { Mission Starten }
  MissStart:=TDXBitmapButton.Create(Self);
  with MissStart do
  begin
    SetButtonFont(Font);
    Text:=BMissionStart;
    BlendColor:=bcNavy;
    AccerlateColor:=coAccerlateColor;
    FirstColor:=coFirstColor;
    RoundCorners:=rcRightBottom;
    SecondColor:=coSecondColor;
    Hint:=HMissionStart;
    OnClick:=MissionStartClick;
    SetRect(485,448,119,28);
  end;

//*****************************************
// Ende Komponenten f�r die Missionsskripte
//*****************************************

  { Listbox Fenster zur Auswahl bei mehreren Objekten }
  ListBoxWindow:=TDXListBoxWindow.Create(Self);
  with ListBoxWindow do
  begin
    SetRect(0,0,300,200);
    Caption:=CChooseGameSet;
    SetButtonFont(Font);
    CaptionColor:=coFontColor;
    AccerlateColor:=coAccerlateColor;
    Font.Size:=coFontSize;
    FirstColor:=$00400000;
    SecondColor:=clBlue;
    BlendColor:=bcDarkNavy;
    Items.Sorted:=true;
    Items.Duplicates:=dupAccept;
  end;

//*****************************************
// Tipps und Tricks
//*****************************************

  { Group Caption f�r den TextViewer }
  GCTipp:=TDXGroupCaption.Create(Self);
  with GCTipp do
  begin
    SetFont(Font);
    Font.Size:=Font.Size+2;
    SetRect(245,100,300,24);
    RoundCorners:=rcTop;
    Caption:=LTipp;
    BlendColor:=bcNavy;
    BorderColor:=coFirstColor;
  end;

  { Tipps und Tricks im HelpManager laden }
  HManager:=THelpManager.Create;
  HManager.Kategorie:='Allgemein';

  { TextViewer zum Anzeigen der Tipps und Tricks }
  fTextViewer:=TDXTextViewer.Create(Self);
  with fTextViewer do
  begin
    BlendColor:=bcNavy;
    BorderColor:=clNavy;
    SetFont(Font);
    Text:='';
    RoundCorners:=rcNone;
    Visible:=true;
    Blending:=true;
    SetRect(245,125,300,150);
    RegisterFont('white',WhiteStdFont);
    RegisterFont('yellow',YellowStdFont);
    RegisterFont('green',GreenStdFont);
    RegisterFont('red',RedStdFont);
    RegisterFont('lime',LimeStdFont);
    RegisterFont('blue',BlueStdFont);
    RegisterFont('maroon',MaroonStdFont);
    RegisterFont('yellowbold',YellowBStdFont);
    RegisterFont('whitebold',WhiteBStdFont);
    RegisterFont('greenbold',GreenBStdFont);
    RegisterFont('redbold',RedBStdFont);
    RegisterFont('limebold',LimeBStdFont);
    RegisterFont('bluebold',BlueBStdFont);
    RegisterFont('maroonbold',MaroonBStdFont);
  end;

  { Button vorheriger Tipp }
  LastButton:=TDXBitmapButton.Create(Self);
  with LastButton do
  begin
    SetButtonFont(Font);
    Text:=BLastTipp;
    BlendColor:=bcNavy;
    AccerlateColor:=coAccerlateColor;
    FirstColor:=coFirstColor;
    RoundCorners:=rcLeftBottom;
    SecondColor:=coSecondColor;
    Hint:=HLastTipp;
    Visible:=false;
    OnClick:=LastClick;
    SetRect(245,276,150,28);
  end;

  { Button n�chster Tipp }
  NextButton:=TDXBitmapButton.Create(Self);
  with NextButton do
  begin
    SetButtonFont(Font);
    Text:=BNextTipp;
    BlendColor:=bcNavy;
    AccerlateColor:=coAccerlateColor;
    FirstColor:=coFirstColor;
    RoundCorners:=rcRightBottom;
    SecondColor:=coSecondColor;
    Hint:=HNextTipp;
    Visible:=false;
    Enabled:=false;
    OnClick:=NextClick;
    SetRect(396,276,149,28);
  end;

  InitTexte;

  RegisterHotKey(#13,LDGame);

  Reg:=OpenXForceRegistry;
  if Reg.ValueExists('LastPlayer') and Reg.ValueExists('LastGame') then
  begin
    BackButton.Text:=BLoadLastGame;
    BackButton.Enabled:=true;
    BackButton.Onclick:=LoadLastGame;
    BackButtonActive:=true;
  end;
  Reg.Free;

  InitApi;
end;

procedure TMainPage.DeleteGameClick(Sender: TObject);
var
  Dummy   : Integer;
  Start   : Integer;
  Stream  : TMemoryStream;
  Count   : Integer;

  Player  : String;
begin
  if GamesBox.Items.Count=0 then
    exit;

  if game_api_Question(Format(MDeleteGame,[GamesBox.Items[GamesBox.ItemIndex]]),CQuestion) then
  begin
    Player:=PlayerList.Items[PlayerList.ItemIndex];
    Container.Lock;
    Start:=GamesBox.ItemIndex;
    Archiv.OpenArchiv('save\'+Player+'.sav');
    Archiv.DeleteRessource(GamesBox.Items[GamesBox.ItemIndex]);

    for Dummy:=Start+1 to GamesBox.Items.Count-1 do
    begin
      GameStatus[Start]:=GameStatus[Dummy];
      inc(Start);
    end;
    SetLength(GameStatus,Length(GameStatus)-1);
    Count:=Length(GameStatus);
    GamesBox.Items.Delete(GamesBox.ItemIndex);

    Stream:=TMemoryStream.Create;
    Stream.Write(Count,SizeOf(Count));
    for Dummy:=0 to Count-1 do
      Stream.Write(GameStatus[Dummy],SizeOf(GameStatus[Dummy]));
    Archiv.ReplaceRessource(Stream,RNSaveGame);
    Stream.Free;

    Archiv.CloseArchiv;
    if GamesBox.Items.Count=0 then
    begin
      fLoadPlayers.Delete(fLoadPlayers.IndexOf(Player));
      LDGame.Visible:=false;
    end;

    SetButtons;

    Container.UnLock;
    Container.RedrawArea(Rect(312,55,701,492),Container.Surface);
  end;
end;

procedure TMainPage.DeletePlayerClick(Sender: TObject);
var
  Player: String;
begin
  Player:=PlayerList.Items[PlayerList.ItemIndex];
  if game_api_Question(Format(MDeletePlayer,[Player]),CQuestion) then
  begin
    Container.Lock;
    DeleteFile('save\'+Player+'.sav');

    // Spieler aus den Listen l�schen
    fDelePlayers.Delete(PlayerList.ItemIndex);
    if fLoadPlayers.IndexOf(Player)<>-1 then
      fLoadPlayers.Delete(fLoadPlayers.IndexOf(Player));

    FillPlayers;
    FillGames(Self);
    
    SetButtons;
    Container.UnLock;
    Container.RedrawArea(Rect(145,55,701,492),Container.Surface);
  end;
end;

destructor TMainPage.Destroy;
begin
  HManager.Free;

  fLoadPlayers.Free;
  fDelePlayers.Free;
  QueryTextBox.Free;
  MessageBox.Free;
  Archiv.Free;

  if fTempStream<>nil then
    fTempStream.Free;
    
  inherited;
end;

procedure TMainPage.DrawHighScore(Sender: TDXComponent;
  Surface: TDirectDrawSurface;const Mem: TDDSurfaceDesc; Rect: TRect; Index: Integer;
  Selected: boolean);
var
  Text    : String;
  Entry   : THighScoreEntry;
begin
  Entry:=highscore_api_GetEntry(Index);
  Text:=IntToStr(Index+1)+'.';
  YellowStdFont.Draw(Surface,(Rect.Left+30)-YellowStdFont.TextWidth(Text),Rect.Top+2,Text);
  YellowStdFont.Draw(Surface,Rect.Left+30,Rect.Top+2,Entry.Spieler);
  Text:=IntToStr(Entry.Week)+'.'+LWeek+' '+IntToStr(Entry.Jahr);
  WhiteStdFont.Draw(Surface,Rect.Left+(HighScoreBox.Width shr 1)-20,Rect.Top+2,Text);
  Text:=Format(FPoints,[Entry.Punkte/1]);
  WhiteStdFont.Draw(Surface,Rect.Right-4-WhiteStdFont.TextWidth(Text),Rect.Top+2,Text);
end;

procedure TMainPage.DrawMission(Sender: TDXComponent;
  Surface: TDirectDrawSurface; const Mem: TDDSurfaceDesc; Rect: TRect;
  Index: Integer; Selected: boolean);
begin
  if Selected then
    MissionList.DrawSelection(Surface,Mem,Rect,bcNavy);
  with SaveGame.MissionList.MissionsFile[Index] do
  begin
    if Name='' then
      YellowStdFont.Draw(Surface,Rect.Left+2,Rect.Top+2,FileName)
    else
      WhiteStdFont.Draw(Surface,Rect.Left+2,Rect.Top+2,Name);

    if Error<>'' then
      RedStdFont.Draw(Surface,Rect.Left+10,Rect.Top+19,Error)
    else
      LimeStdFont.Draw(Surface,Rect.Left+10,Rect.Top+19,ST0312070005)
  end;
end;

procedure TMainPage.DrawSaveGame(Sender: TDXComponent;
  Surface: TDirectDrawSurface;const Mem: TDDSurfaceDesc; Rect: TRect; Index: Integer;
  Selected: boolean);
var
  Text    : String;
begin
  if Selected then GamesBox.DrawSelection(Surface,Mem,Rect,bcNavy);
  YellowStdFont.Draw(Surface,Rect.Left+5,Rect.Top+5,GamesBox.Items[Index]);
  if IsValidSaveGameVersion(GameStatus[Index].Version) then
  begin
    Text:=Format(FDate,[GameStatus[Index].Date.Day,GameStatus[Index].Date.Month,GameStatus[Index].Date.Year]);
    WhiteStdFont.Draw(Surface,Rect.Right-WhiteStdFont.TextWidth(Text)-5,Rect.Top+5,Text);
    Text:=Format(ST0502160001,[GameStatus[Index].Date.Hour,GameStatus[Index].Date.Minute]);
    WhiteStdFont.Draw(Surface,Rect.Right-WhiteStdFont.TextWidth(Text)-5,Rect.Top+25,Text);
    Text:=LKapital+Format(FFloat,[GameStatus[Index].Kapital/1]);
    WhiteStdFont.Draw(Surface,Rect.Left+5,Rect.Top+25,Text);
  end
  else
    RedStdFont.Draw(Surface,Rect.Left+5,Rect.Top+25,SInvSaveGame);
end;

procedure TMainPage.FillGames(Sender: TObject);
var
  Dummy    : Integer;
  Count    : Integer;
  SaveGame : String;
begin
  if PlayerList.Items.Count=0 then
  begin
    GamesBox.Items.Clear;
    SetButtons;
    exit;
  end;
  Container.Lock;
  GamesBox.ChangeItem(true);
  GamesBox.Items.Clear;

  SaveGame:='save\'+PlayerList.Items[PlayerList.ItemIndex]+'.sav';

  if not FileExists(SaveGame) then
  begin
    GamesBox.ChangeItem(false);
    GamesBox.Items.OnChange(Self);
    LDGame.Visible:=false;
    SetButtons;
    Container.UnLock;
    Container.RedrawArea(Rect(321,30,701,492),Container.Surface);
    exit;
  end;
  SetLength(GameStatus,0);
  Archiv.OpenArchiv(SaveGame);
  if Archiv.ExistRessource(RNSaveGame) then
  begin
    Archiv.OpenRessource(RNSaveGame);
    with Archiv.Stream do
    begin
      Read(Count,SizeOf(Count));
      SetLength(GameStatus,Count);
      Dummy:=0;
      while Dummy<Count do
      begin
        Read(GameStatus[Dummy],SizeOf(TGameStatus));
        GamesBox.Items.Add(GameStatus[Dummy].Name);
        inc(Dummy);
      end;
    end;
  end;
  Archiv.CloseArchiv;
  GamesBox.ChangeItem(false);
  GamesBox.Items.OnChange(Self);
  LDGame.Visible:=GamesBox.Items.Count<>0;
  SetButtons;
  Container.UnLock;
  Container.RedrawArea(Rect(341,55,701,492),Container.Surface);
end;

procedure TMainPage.FillMissionList;
var
  Dummy: Integer;
begin
//  SaveGame.MissionList.SearchForMissions;

  MissionList.Items.Clear;
  for Dummy:=0 to SaveGame.MissionList.Files-1 do
  begin
    MissionList.Items.Add(SaveGame.MissionList.MissionsFile[Dummy].Name);
  end;

  MissEdit.Enabled:=MissionList.Items.Count>0;
  MissStart.Enabled:=MissionList.Items.Count>0;
end;

procedure TMainPage.FillPlayers;
begin
  case ListStatus of
    lsLoad   : PlayerList.Items:=fLoadPlayers;
    lsDelete : PlayerList.Items:=fDelePlayers;
  end;
  PlayerList.ItemIndex:=PlayerList.ItemIndex;
  FillGames(Self);
end;

procedure TMainPage.FreeLookChange(Sender: TObject);
begin
  FreeLook:=FreeLookBox.Checked;
end;

procedure TMainPage.GameDblClick(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  if GamesBox.Items.Count=0 then exit;
  LDGame.OnClick(Self);
end;

procedure TMainPage.GamePlayClick(Sender: TObject);
begin
  Container.Lock;
  ShowGamePlayOptions(true);
  Container.Unlock;
  Container.RedrawArea(Bounds(99,18,701,480),Container.Surface);
end;

procedure TMainPage.GraphicSoundClick(Sender: TObject);
begin
  Container.Lock;
  ShowSoundGraphic(true);
  Container.Unlock;
  Container.RedrawArea(Bounds(9,18,702,480),Container.Surface);
end;

procedure TMainPage.ShowTipps(Visible: Boolean);
begin
  fTextViewer.Visible:=Visible;
  GCTipp.Visible:=Visible;
  LastButton.Visible:=Visible;
  NextButton.Visible:=Visible;
  NextButton.Enabled:=HManager.PTippAnzahl>0;
  LastButton.Enabled:=HManager.PTippAnzahl>0;
end;

procedure TMainPage.HighScoreChangeHandler(Sender: TObject);
var
  Dummy: Integer;
begin
  HighScoreBox.Items.Clear;
  for Dummy:=0 to highscore_api_GetEntryCount-1 do
    HighScoreBox.Items.Add('');
end;

procedure TMainPage.HighScoreClick(Sender: TObject);
begin
  Container.IncLock;
  Container.LoadGame(true);
  ListLabel.Visible:=false;
  DeletePlayer.Visible:=false;
  GamesBox.Visible:=false;
  PlayerList.Visible:=false;
  LDGame.Visible:=false;
  HighScoreBox.Visible:=not HighScoreBox.Visible;
  ShowSettings(false);
  ShowMission(false);
  MissionsTest.Visible:=false;
  NewGame.Visible:=false;
  LoadGame.Visible:=false;
  DeleteGame.Visible:=false;
  BackButton.Visible:=false;
  Modus.Visible:=false;
  KampfSimu.Visible:=false;
  ShowTipps(false);
  Container.LoadGame(false);
  HintLabel.ReDraw;
  Container.DecLock;
  Container.RedrawArea(Bounds(99,18,702,480),Container.Surface);
  ListStatus:=lsNone;
end;

procedure TMainPage.InitApi;
begin
  game_api_SetDXMessage(MessageBox);
  game_api_SetQueryTextBox(QueryTextBox);
  game_api_SetListBoxWindow(ListBoxWindow);

  savegame_api_RegisterStartGameHandler(StartGameHandler);
  savegame_api_RegisterLoadGameSetHandler(LoadGameSetHandler);

  highscore_api_RegisterChangeHandler(HighScoreChangeHandler);
end;

procedure TMainPage.InitTexte;
begin
{00}  GraphicList.AddItem(CBAlphaControls,HNAlphaControls);
{01}  GraphicList.AddItem(CBAnimation,HNAnimation);
{02}  GraphicList.AddItem(CBAnimatedMaus,HNAnimatedMaus);
{03}  GraphicList.AddItem(CBMouseShadow,HNMouseShadow);
{04}  GraphicList.AddItem(CBStarField,HNStarField);
{05}  GraphicList.AddItem(CBTransEnergy,HNTransEnergy);
{06}  GraphicList.AddItem(ST0309260045,ST0309260046);
{07}  GraphicList.AddItem(ST0501310001,ST0501310002);
{08}  GraphicList.AddItem(CR0504050001,CR0504050002);

//  GraphicList.ItemEnabled[0]:=false;
//  GraphicList.ItemEnabled[4]:=false;
end;

procedure TMainPage.LastClick(Sender: TObject);
begin
  fTextViewer.Text:=HManager.LastTipp;
end;

procedure TMainPage.LoadGameClick(Sender: TObject);
begin
{$IFDEF TRYEXCEPT}
  try
{$ENDIF}
    Archiv.OpenArchiv('save\'+PlayerList.Items[PlayerList.ItemIndex]+'.sav',true);
    Archiv.OpenRessource(GamesBox.Items[GamesBox.ItemIndex]);
    Container.IncLock;
    Container.LoadGame(true);
    SaveGame.LoadGame(GamesBox.Items[GamesBox.ItemIndex],Archiv.Stream);
    Archiv.CloseArchiv;

    game_api_SetCanSaveGame(true);

    Container.DecLock;
    Container.LoadGame(false);
    ChangePage(PageGameMenu);
{$IFDEF TRYEXCEPT}
  except
    on E: Exception do
    begin
      Archiv.CloseArchiv;
      Container.DecLock;
      Container.LoadGame(false);
      game_api_MessageBox(Format(MGameNotLoaded,[E.Message]),CError);
    end;
  end;
{$ENDIF}
end;

procedure TMainPage.LoadHelpData;
begin
  if FileExists(GetLanguageFile('tips_')) then
  begin
    HManager.LoadFromTextFile(GetLanguageFile('tips_'));
    FTextViewer.Text:=HManager.RandomTipp;
    ShowTipps(true);
  end
  else
    ShowTipps(false);
end;

procedure TMainPage.LoadHelpData(Stream: TStream);
begin
  HManager.Load(Stream);
  fTextViewer.Text:=HManager.RandomTipp;
end;

procedure TMainPage.LoadLastGame(Sender: TObject);
var
  Reg     : TRegistry;
  Player  : String;
  Game    : String;
begin
  try
    Reg:=OpenXForceRegistry;
    Player:=Reg.ReadString('LastPlayer');
    Game:=Reg.ReadString('LastGame');
    Reg.Free;
    Archiv.OpenArchiv('save\'+Player+'.sav',true);
    Archiv.OpenRessource(Game);
    Container.IncLock;
    Container.LoadGame(true);
    SaveGame.LoadGame(Game,Archiv.Stream);
    Archiv.CloseArchiv;

    game_api_SetCanSaveGame(true);

    Container.DecLock;
    Container.LoadGame(false);
    ChangePage(PageGameMenu);
  except
    on E: Exception do
    begin
      Archiv.CloseArchiv;
      Container.DecLock;
      Container.LoadGame(false);
      game_api_MessageBox(Format(MGameNotLoaded,[E.Message]),CError);
    end;
  end;
end;

procedure TMainPage.MissionEditClick(Sender: TObject);
var
  Skript: String;
  Instance: Integer;
begin
  Skript:=ExpandUNCFileName(SaveGame.MissionList.MissionsFile[MissionList.ItemIndex].FileName);
  Instance:=ShellExecute(0,'open',PChar(IncludeTrailingBackslash(ExtractFilepath(Application.ExeName))+'MEdit.exe'),PChar(Skript),PChar(ExtractFilePath(Skript)),SW_NORMAL);
  if (Instance=ERROR_FILE_NOT_FOUND) or (Instance=ERROR_PATH_NOT_FOUND)  or (Instance=ERROR_ACCESS_DENIED)then
  begin
    game_api_MessageBox('Missionseditor wurde nicht gefunden. Skript kann nicht direkt bearbeitet werden.',CError);
  end;
end;

procedure TMainPage.MissionListDblClick(Sender: TObject;Button: TMouseButton;Shift: TShiftState;X,Y: Integer);
begin
  if Button=mbLeft then
    MissStart.DoClick;
end;

procedure TMainPage.MissionStartClick(Sender: TObject);
var
  Data     : TNewGameRec;
  Dummy    : Integer;
  Result   : Integer;
  FileName : String;
  Mission  : TMission;
  tmpDef   : TStringList;
begin
  if SaveGame.PlayerName<>'' then
  begin
    if not game_api_Question(MTestModus,CInformation) then
      exit;
  end;
  ListBoxWindow.Items.Clear;
  for Dummy:=0 to High(Files) do
  begin
    ListBoxWindow.Items.AddObject(Files[Dummy].Name,Pointer(Dummy));
  end;
  Result:=ListBoxWindow.Show;
  if Result=-1 then exit;
  Result:=Integer(ListBoxWindow.Items.Objects[Result]);
  with Data do
  begin
    Name:='Missions-Test';
    Kapital:=1000000;
    StartAlphatron:= 50000;
    Forsch:=100;
    Prod:=100;
    Alien:=0;
    UFO:=0;
    Soldat:=10;
    Forscher:=10;
    Techniker:=10;
    GameFile:=Files[Result];
    Basis:=FloatPoint(210,120);
    Language:=Defines.Language;
    Difficult:=gdNormal;
  end;
  try
    Container.Lock;
    SaveGame.NewGame(Data);
    {$IFNDEF SAVEMISSIONTEST}
    game_api_SetCanSaveGame(false);
    {$ELSE}
    SaveGame.MissionList.DebugMode:=true;
    {$ENDIF}
    Container.UnLock;
  except
    on E: ENotEnoughRoom do
    begin
      Container.UnLock;
      game_api_MessageBox(ENoRoomAtStart,CError);
      BackButton.Enabled:=false;
      exit;
    end;
    on E: Exception do
    begin
      Container.UnLock;
      game_api_MessageBox(E.Message,CError);
      BackButton.Enabled:=false;
      exit;
    end;
  end;
  ChangePage(PageGameMenu);
  Container.IncLock;

  tmpDef:=TStringList.Create;
  tmpDef.Add('DEBUG');

  // Mission starten
  FileName:=SaveGame.MissionList.MissionsFile[MissionList.ItemIndex].FileName;

  Mission:=SaveGame.MissionList.CreateMission(string_utils_LoadFromFile(FileName),FileName,tmpDef);

  tmpDef.Free;

  if Mission<>nil then
    Mission.Start;

  Container.DecLock;
  Container.DoFlip;
end;

procedure TMainPage.MissionsTestClick(Sender: TObject);
begin
  Container.LoadGame(true);
  ListLabel.Visible:=false;
  LDGame.Visible:=false;
  PlayerList.Visible:=false;
  DeletePlayer.Visible:=false;
  GamesBox.Visible:=false;
  ShowTipps(false);
  ShowMission(not MissionList.Visible);
  MissStart.Enabled:=MissionList.Items.Count>0;
  Container.LoadGame(false);
  ListStatus:=lsNone;
  Container.RedrawArea(Rect(225,30,701,492),Container.Surface);
end;

procedure TMainPage.MultiPlayerClick(Sender: TObject);
begin
  Container.Lock;
  ListLabel.Visible:=false;
  DeletePlayer.Visible:=false;
  GamesBox.Visible:=false;
  PlayerList.Visible:=false;
  Modus.Visible:=false;
  LDGame.Visible:=false;
  NewGame.Visible:=false;
  LoadGame.Visible:=false;
  MissionsTest.Visible:=false;
  DeleteGame.Visible:=false;
  BackButton.Visible:=false;
  BackButton.Enabled:=BackButtonActive;
  KampfSimu.Visible:=false;
  HighScoreBox.Visible:=false;
  ShowTipps(false);
  ShowSettings(false);
  ShowMission(false);
  Container.DecLock;
  HintLabel.Redraw;
  Container.LoadGame(false);
  Container.RedrawArea(Bounds(99,18,702,480),Container.Surface);
  ListStatus:=lsNone;
  game_api_MessageBox(MNoMultiplayer,CHinweis);
end;

procedure TMainPage.NeedDetailName(Value: Integer; var Text: String);
begin
  case Value of
    0: Text:=LShadowLow;
    1: Text:=LShadowMedium;
    2: Text:=LShadowHigh;
  end;
end;

procedure TMainPage.NewGameClick(Sender: TObject);
begin
  Archiv.CloseArchiv;
  ChangePage(PageNewGame);
end;

procedure TMainPage.NextClick(Sender: TObject);
begin
  LastButton.Enabled:=true;
  fTextViewer.Text:=HManager.NextTipp;
end;

procedure TMainPage.ReadGameSets;
var
  Data    : TSearchRec;
  Stat    : Integer;
  Ident   : TSetIdentifier;
  Index   : Integer;
begin
  Stat:=FindFirst('data\GameSets\*.pak',faAnyFile,Data);
  Index:=0;
  while Stat=0 do
  begin
    try
      Ident:=gameset_api_GetIdentOfSet('data\GameSets\'+Data.Name);
      SetLength(Files,Index+1);
      Files[Index]:=Ident;
      inc(Index);
    except
    end;
    Stat:=FindNext(Data);
  end;
  FindClose(Data);
end;

procedure TMainPage.RefreshMissions(Sender: TObject);
begin
  Container.IncLock;
  FillMissionList;
  Container.DecLock;
end;

procedure TMainPage.SetButtons;
begin
  if (LDGame.Visible and (not DeletePlayer.Visible)) then
  begin
    LDGame.SetRect(421,464,280,28);
  end
  else if (LDGame.Visible and DeletePlayer.Visible) then
  begin
    DeletePlayer.SetRect(421,464,139,28);
    DeletePlayer.RoundCorners:=rcNone;

    LDGame.SetRect(561,464,140,28);
  end
  else if ((not LDGame.Visible) and (DeletePlayer.Visible)) then
  begin
    DeletePlayer.SetRect(421,464,280,28);
    DeletePlayer.RoundCorners:=rcRightBottom;
  end;
end;

procedure TMainPage.SetDefaults;
var
  Data      : TSearchRec;
begin
  // Spielernamen anhand der Savegame-Dateien ermitteln
  fLoadPlayers.Clear;
  fDelePlayers.Clear;
  if FindFirst('save\*.sav',faAnyFile,Data)=0 then
  begin
    repeat
      try
        Archiv.OpenArchiv('save\'+Data.Name);
        fDelePlayers.Add(ChangeFileExt(Data.Name,''));
        if Archiv.Count>1 then
          fLoadPlayers.Add(ChangeFileExt(Data.Name,''));
      except
      end;
    until FindNext(Data)<>0;
  end;
  FindClose(Data);
  Archiv.CloseArchiv;

  FillGames(Self);

  Container.IncLock;
  PlayerList.Visible:=false;
  ListLabel.Visible:=false;
  GraphicList.Checked[0]:=AlphaControls;
  GraphicList.Checked[1]:=Container.PageAnimation;
  GraphicList.Checked[2]:=Container.MouseAnimation;
  GraphicList.Checked[3]:=Container.MouseShadow;
  GraphicList.Checked[4]:=ShowStarField;
  GraphicList.Checked[5]:=TransparentEnergy;
  GraphicList.Checked[6]:=Gradnetz;
  GraphicList.Checked[7]:=Enabled3DAlphaBlend;
  GraphicList.Checked[8]:=ShowSensorMap;

  FreeLookBox.Checked:=FreeLook;
  AlwaysFast.Checked:=AlwaysFastTime;
  TastaturScroll.Value:=round(DXISOEngine.ScrollTempo*100);
  CompatibleBox.Checked:=DXTakticScreen.GetCompatibleMode;

  LoadGame.Visible:=false;
  DeleteGame.Visible:=false;
  NewGame.Visible:=false;
  BackButton.Visible:=false;
  DeletePlayer.Visible:=false;
  Modus.Visible:=false;
  MissionsTest.Visible:=false;
  LDGame.Visible:=false;
  KampfSimu.Visible:=false;
  GamesBox.Visible:=false;
  ShadowEdit.Value:=Ord(LightDetail);
  ListStatus:=lsNone;
  ShowMission(false);
  ShowSettings(false);
  HighScoreBox.Visible:=false;
  VolumeBar.Value:=Container.Volume;
  MusicBar.Value:=Container.MusicVolume;
  AlphaBar.Value:=round(Blending.AlphaBlendFaktor*100);
  GammaBar.Value:=Container.Gamma;

  ChangeAlpha(AlphaBar);
  ChangeVolume(VolumeBar);
  ChangeMusic(MusicBar);
  ChangeGamma(GammaBar);

  Container.DecLock;
  ReadGameSets;

  Container.PlayMusicCategory('Hauptmen�');

  // Bei Anzeige des Hauptmen�s wird das laufende Spiel gespeichert, um
  if SaveGame.OpenGame and (fTempStream=nil) then
  begin
    fTempStream:=TMemoryStream.Create;
    SaveGame.SaveGameToStream(fTempStream);
    fTempName:=SaveGame.FileName;
  end;
end;

procedure TMainPage.SetDeleteStatus(Sender: TObject);
var
  ViewState: boolean;
  ChangeState: boolean;
begin
  if (ListStatus<>lsDelete) then
  begin
    ViewState:=true;
    if ButtonState<>lsDelete then ChangeState:=true else ChangeState:=false;
    ListStatus:=lsDelete;
  end
  else
  begin
    ViewState:=false;
    ChangeState:=false;
    ListStatus:=lsNone;
  end;
  ButtonState:=lsDelete;
  Container.Lock;
  FillPlayers;
  if ViewState then
  begin
    if ChangeState then
    begin
      PlayerList.Hint:=HPlayerDelete;
      ListLabel.Caption:=CDeleteGame;
      LDGame.Hint:=HDeleteGame;
      LDGame.Text:=BDeleteGameS;
      LDGame.OnClick:=DeleteGameClick;
    end;
    LDGame.Visible:=(GamesBox.Items.Count<>0);
    DeletePlayer.Visible:=(PlayerList.Items.Count<>0);
  end
  else
  begin
    DeletePlayer.Visible:=false;
    LDGame.Visible:=false;
  end;
  ShowMission(false);
  Container.FocusControl:=PlayerList;
  ListLabel.Visible:=ViewState;
  PlayerList.Visible:=ViewState;
  SetButtons;
  GamesBox.Visible:=ViewState;
  Container.LoadGame(false);
  HintLabel.ReDraw;
  Container.DecLock;
  Container.RedrawArea(Rect(225,30,701,492),Container.Surface);
end;

procedure TMainPage.SetLoadStatus(Sender: TObject);
var
  ViewState: boolean;
  ChangeState: boolean;
begin
  if (ListStatus<>lsLoad) then
  begin
    ViewState:=true;
    if ButtonState<>lsLoad then ChangeState:=true else ChangeState:=false;
    ListStatus:=lsLoad;
  end
  else
  begin
    ViewState:=false;
    ChangeState:=false;
    ListStatus:=lsNone;
  end;
  ButtonState:=lsLoad;
  Container.Lock;
  FillPlayers;
  if ViewState then
  begin
    if ChangeState then
    begin
      PlayerList.Hint:=HPlayerLoad;
      ListLabel.Caption:=CLoadGame;
      LDGame.Text:=BLoadGameS;
      LDGame.Hint:=HLoadButton;
      LDGame.OnClick:=LoadGameClick;
    end;
    LDGame.Visible:=ViewState and (not (GamesBox.Items.Count=0));
  end
  else
  begin
    LDGame.Visible:=false;
  end;
  ShowMission(false);
  DeletePlayer.Visible:=false;
  ListLabel.Visible:=ViewState;
  PlayerList.Visible:=ViewState;
  GamesBox.Visible:=ViewState;
  SetButtons;
  Container.LoadGame(false);
  HintLabel.ReDraw;
  Container.DecLock;
  Container.RedrawArea(Rect(225,30,701,492),Container.Surface);
end;

procedure TMainPage.SettingsClick(Sender: TObject);
begin
  Container.IncLock;
  Container.LoadGame(true);
  NewGame.Visible:=false;
  MissionsTest.Visible:=false;
  LoadGame.Visible:=false;
  DeleteGame.Visible:=false;
  BackButton.Visible:=false;
  Modus.Caption:=ST0309230015;
  KampfSimu.Visible:=false;
  ShowTipps(false);
  ShowMission(false);
  ShowSettings(not SoundFrame.Visible);
  Container.LoadGame(false);
  Container.DecLock;
  Container.RedrawArea(Bounds(99,18,702,480),Container.Surface);
end;

procedure TMainPage.ShowGamePlayOptions(Show: boolean);
begin
  if Gameplay.HighLight=Show then
    exit;
  //
  SchiffCaption.Visible:=Show;
  FreelookBox.Visible:=Show;
  EinsatzCaption.Visible:=Show;
  TastaturCaption.Visible:=Show;
  TastaturLabel.Visible:=Show;
  TastaturScroll.Visible:=Show;
  AllgemeinCaption.Visible:=Show;
  AlwaysFast.Visible:=Show;
  Gameplay.HighLight:=Show;
  CompatibleBox.Visible:=Show;

  if Show then
    ShowSoundGraphic(false);
end;

procedure TMainPage.ShowMission(Show: boolean);
begin
  if Show then
  begin
    FillMissionList;
  end;
  MissionList.Visible:=Show;
  MissStart.Visible:=Show;
  MissRefresh.Visible:=Show;
  MissEdit.Visible:=Show;
end;

procedure TMainPage.ShowSettings(Show: boolean);
begin
  if Show then
  begin
    ListLabel.Visible:=false;
    DeletePlayer.Visible:=false;
    GamesBox.Visible:=false;
    PlayerList.Visible:=false;
    LDGame.Visible:=false;
    HighScoreBox.Visible:=false;
    ListStatus:=lsNone;
  end;
  KeyBoard.Visible:=Show;
  GraphicSound.Visible:=Show;
  Gameplay.Visible:=Show;
  Modus.Visible:=Show;

  ShowSoundGraphic(Show);
  ShowGamePlayOptions(false);
end;

procedure TMainPage.ShowSoundGraphic(Show: boolean);
begin
  if GraphicSound.HighLight=Show then
    exit;

  SoundFrame.Visible:=Show;

  MusicBar.Visible:=Show;
  MusicLabel.Visible:=Show;
  MusicText.Visible:=Show;

  VolumeBar.Visible:=Show;
  VolumeLabel.Visible:=Show;
  VolumeText.Visible:=Show;

  AlphaBar.Visible:=Show;
  AlphaLabel.Visible:=Show;
  AlphaText.Visible:=Show;

  GammaBar.Visible:=Show;
  GammaLabel.Visible:=Show;
  GammaText.Visible:=Show;

  GraphicLabel.Visible:=Show;
  GraphicList.Visible:=Show;
  ShadowLabel.Visible:=Show;
  ShadowEdit.Visible:=Show;

  GraphicSound.HighLight:=Show;

  if Show then
    ShowGamePlayOptions(false);
end;

procedure TMainPage.SimulationClick(Sender: TObject);
begin
  Archiv.CloseArchiv;
  ChangePage(PageKampfSimul);
end;

procedure TMainPage.SinglePlayerClick(Sender: TObject);
var
  Show: Boolean;
begin
  Container.Lock;
  ListLabel.Visible:=false;
  DeletePlayer.Visible:=false;
  GamesBox.Visible:=false;
  PlayerList.Visible:=false;

  Show:=not NewGame.Visible;

  Modus.Caption:=LSinglePlayer;
  LDGame.Visible:=false;
  NewGame.Visible:=Show;
  LoadGame.Visible:=Show;
  MissionsTest.Visible:=Show;
  DeleteGame.Visible:=Show;
  BackButton.Visible:=Show;
  BackButton.Enabled:=BackButtonActive;
//  KampfSimu.Visible:=Show;
  HighScoreBox.Visible:=false;
  ShowTipps(false);
  ShowSettings(false);
  ShowMission(false);

  Modus.Visible:=Show;

  Container.DecLock;
  HintLabel.Redraw;
  Container.LoadGame(false);
  Container.RedrawArea(Bounds(99,18,702,480),Container.Surface);
  ListStatus:=lsNone;
end;

procedure TMainPage.StartGameHandler(Sender: TObject);
begin
  ActivateBack;

  if fTempStream<>nil then
  begin
    fTempStream.Free;
    fTempStream:=nil;
  end;
end;

procedure TMainPage.LoadGameSetHandler(Sender: TObject);
begin
  Container.ReadSounds(savegame_api_GetGameSet.FileName,'user');
end;

end.
