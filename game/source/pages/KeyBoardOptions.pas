{********************************************************************************
*										*
* Copyright (C) 2004  X-Force Team - jim_raynor@web.de				*
*										*
* This file is part of X-Force: Fight For Destiny (X-Force).			*
*										*
* X-Force is free software; you can redistribute it and/or modify		*
* it under the terms of the GNU General Public License as published by		*
* the Free Software Foundation; either version 2 of the License, or		*
* (at your option) any later version.						*
*										*
* X-Force is distributed in the hope that it will be useful,			*
* but WITHOUT ANY WARRANTY; without even the implied warranty of		*
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the			*
* GNU General Public License for more details.					*
*										*
* You should have received a copy of the GNU General Public License		*
* along with X-Force; if not, write to the Free Software			*
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA	*
*										*
*********************************************************************************
*										*
* Tastatureinstellungen								*
*										*
********************************************************************************}

{$I ../Settings.inc}

unit KeyBoardOptions;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  KD4Page, DXContainer, Defines, DXBitmapButton, DXGroupCaption, XForce_types,
  DXHotKey, Blending, Registry, DXInput, DXUFOKampf, KD4Utils, math,
  DXScrollBar, StringConst;

const
  coFirstColor  : TColor = clNavy;
  coSecondColor : TColor = clBlue;
type
  TRowType   = (rtCaption,rtHotKey);
  TKeyConfig = record
    RowType    : TRowType;
    CapLabel   : TDXGroupCaption;
    HotKeyEdit : TDXHotKey;
    KeyGroup   : Integer;
    Button     : TDXInputState;
    Key        : PChar;
  end;

  TKeyBoardOptions = class(TKD4Page)
  private
    fOldKeys   : TAllHotKeys;
    fKeys      : Array of TKeyConfig;
    { Private-Deklarationen }
  protected
    procedure ShowKeys(Start: Integer);
    procedure CanChangeKey(Sender: TObject;HotKey: Char;var CanChange: boolean);
    procedure OnChangeKey(Sender: TObject);
    procedure ScrollChange(Sender: TObject);
    procedure OKClick(Sender: TObject);
    procedure CancelClick(Sender: TObject);

    procedure AddHotKey(Cap: String;Group: Integer;KeyPtr: PChar; But: TDXInputState);
    procedure AddCaption(Cap: String);
    { Protected-Deklarationen }
  public
    CapBox      : TDXGroupCaption;
    ScrollBar   : TDXScrollBar;
    constructor Create(Container: TDXContainer);override;
    procedure SetDefaults;override;
    { Public-Deklarationen }
  end;

implementation

uses
  game_api, ui_utils;

{ TKeyBoardOptions }

procedure TKeyBoardOptions.AddCaption(Cap: String);
var
  Index: Integer;
begin
  Index:=length(fKeys);
  SetLength(fKeys,Index+1);
  with fKeys[Index] do
  begin
    RowType:=rtCaption;
    CapLabel:=TDXGroupCaption.Create(Self);
    with CapLabel do
    begin
      Caption:=Cap;
      SetRect(CapBox.Left,0,CapBox.Width-18,21);
      RoundCorners:=rcNone;
      BorderColor:=clNavy;
      AlphaValue:=200;
      BlendColor:=bcNavy;
      SetFont(Font);
      Font.Style:=[fsBold];
      Alignment:=taCenter;
      Visible:=false;
    end;
  end;
end;

procedure TKeyBoardOptions.AddHotKey(Cap: String; Group: Integer;
  KeyPtr: PChar; But: TDXInputState);
var
  Index: Integer;
begin
  Index:=length(fKeys);
  SetLength(fKeys,Index+1);
  with fKeys[Index] do
  begin
    RowType:=rtHotKey;
    CapLabel:=TDXGroupCaption.Create(Self);
    KeyGroup:=Group;
    Button:=But;
    Key:=KeyPtr;
    with CapLabel do
    begin
      Caption:=Cap;
      SetRect(CapBox.Left,0,CapBox.Width-126,21);
      RoundCorners:=rcNone;
      BorderColor:=clNavy;
      BlendColor:=bcNavy;
      SetFont(Font);
      Visible:=false;
    end;
    HotKeyEdit:=TDXHotKey.Create(Self);
    with HotKeyEdit do
    begin
      SetRect(CapBox.Right-125,29,107,21);
      RoundCorners:=rcNone;
      SetButtonFont(Font);
      BorderColor:=clNavy;
      FocusColor:=clBlue;
      BlendColor:=bcNavy;
      Tag:=Index;
      CanChange:=CanChangeKey;
      OnChange:=OnChangeKey;
    end;
  end;
end;

procedure TKeyBoardOptions.CancelClick(Sender: TObject);
begin
  KeyConfiguration:=fOldKeys;
  CloseModal;
end;

procedure TKeyBoardOptions.CanChangeKey(Sender: TObject; HotKey: Char;
  var CanChange: boolean);
var
  Dummy: Integer;
  Group: Integer;
begin
  Group:=fKeys[(Sender as TDXHotKey).Tag].KeyGroup;
  CanChange:=true;
  for Dummy:=0 to length(fKeys)-1 do
  begin
    if fKeys[Dummy].RowType=rtCaption then continue;
    if (Group=fKeys[Dummy].KeyGroup) and (fKeys[Dummy].HotKeyEdit<>Sender) then
    begin
      if HotKey=fKeys[Dummy].Key^ then
      begin
        CanChange:=false;
        exit;
      end;
    end;
  end;
end;

constructor TKeyBoardOptions.Create(Container: TDXContainer);
begin
  inherited;
  Animation:=paRightToLeft;
  HintLabel.BlendColor:=bcNavy;
  HintLabel.BorderColor:=clNavy;

  { Abbrechen Button }
  with TDXBitmapButton.Create(Self) do
  begin
    SetRect(479,530,110,28);
    FirstColor:=coFirstColor;
    EscButton:=true;
    SecondColor:=coSecondColor;
    AccerlateColor:=coAccerlateColor;
    RoundCorners:=rcLeft;
    BlendColor:=bcNavy;
    OnClick:=CancelClick;
    SetButtonFont(Font);
    DirectDraw:=true;
    Text:=BCancelWA;
    Hint:=HCancelButton;
  end;

  { OK Button }
  with TDXBitmapButton.Create(Self) do
  begin
    SetRect(590,530,110,28);
    FirstColor:=coFirstColor;
    SecondColor:=coSecondColor;
    AccerlateColor:=coAccerlateColor;
    BlendColor:=bcNavy;
    RoundCorners:=rcRight;
    SetButtonFont(Font);
    OnClick:=OKClick;
    DirectDraw:=true;
    Text:=BOKWA;
    Hint:=HOKButton;
  end;

  { �berschrift }
  CapBox:=TDXGroupCaption.Create(Self);
  with CapBox do
  begin
    Caption:=CKeyConfig;
    SetRect(100,8,600,20);
    RoundCorners:=rcTop;
    BorderColor:=clNavy;
    BlendColor:=bcNavy;
    SetFont(Font);
  end;

  { ScrollBar }
  ScrollBar:=TDXScrollBar.Create(Self);
  with ScrollBar do
  begin
    Kind:=sbVertical;
    SetRect(CapBox.Right-17,CapBox.Bottom+1,16,461);
    SetButtonFont(Font);
    FirstColor:=bcBlue;
    SecondColor:=clNavy;
    RoundCorners:=rcRightBottom;
    min:=0;
    LargeChange:=15;
    OnChange:=ScrollChange;
  end;

  { Tasten f�r UFOKampf hinzuf�gen }
  AddCaption(CUFOKampf);
  AddHotKey(CRotateLeft,0,Addr(KeyConfiguration.UFOKeys.RLeft),isLeft);
  AddHotKey(CRotateRight,0,Addr(KeyConfiguration.UFOKeys.RRight),isRight);
  AddHotKey(CGibSchub,0,Addr(KeyConfiguration.UFOKeys.GibSchub),isUp);
  AddHotKey(CMoveLeft,0,Addr(KeyConfiguration.UFOKeys.MLeft),isButton1);
  AddHotKey(CMoveRight,0,Addr(KeyConfiguration.UFOKeys.MRight),isButton2);
//  AddHotKey(Format(CShootzelle,[1]),0,Addr(KeyConfiguration.UFOKeys.WZelle1),isButton3);
//  AddHotKey(Format(CShootzelle,[2]),0,Addr(KeyConfiguration.UFOKeys.WZelle2),isButton5);
//  AddHotKey(Format(CShootzelle,[3]),0,Addr(KeyConfiguration.UFOKeys.WZelle3),isButton6);
  AddCaption(CBodenEinsatz);
  AddHotKey(ST0309260017,1,Addr(KeyConfiguration.BodenKeys.Pause),isButton32);
  AddHotKey(ST0309260018,1,Addr(KeyConfiguration.BodenKeys.RundenEnde),isButton32);
  AddHotKey(ST0309260019,1,Addr(KeyConfiguration.BodenKeys.Karte),isButton32);
  AddHotKey(ST0309260020,1,Addr(KeyConfiguration.BodenKeys.ScrollLeft),isButton24);
  AddHotKey(ST0309260021,1,Addr(KeyConfiguration.BodenKeys.ScrollRight),isButton25);
  AddHotKey(ST0309260022,1,Addr(KeyConfiguration.BodenKeys.ScrollUp),isButton26);
  AddHotKey(ST0309260023,1,Addr(KeyConfiguration.BodenKeys.ScrollDown),isButton27);
  AddHotKey(ST0309260024,1,Addr(KeyConfiguration.BodenKeys.ObjektNamen),isButton30);
  AddHotKey(ST0505260001,1,Addr(KeyConfiguration.BodenKeys.WallsTrans),isButton23);
  AddHotKey(ST0309260025,1,Addr(KeyConfiguration.BodenKeys.NextSoldat),isButton28);
  AddHotKey(ST0309260026,1,Addr(KeyConfiguration.BodenKeys.PrevSoldat),isButton29);
  AddHotKey(ST0309260027,1,Addr(KeyConfiguration.BodenKeys.Camera),isButton32);
  AddHotKey(ST0309260028,1,Addr(KeyConfiguration.BodenKeys.Ausruesten),isButton32);
  AddHotKey(ST0309260029,1,Addr(KeyConfiguration.BodenKeys.TakeAll),isButton32);
  {$IFNDEF DISABLEBEAM}
  AddHotKey(ST0409270002,1,Addr(KeyConfiguration.BodenKeys.BeamUp),isButton32);
  {$ENDIF}
  AddHotKey(ST0410080001,1,Addr(KeyConfiguration.BodenKeys.ShowGrid),isButton32);
  AddHotKey(ST0410250001,1,Addr(KeyConfiguration.BodenKeys.UseGrenade),isButton32);
  AddHotKey(ST0410250002,1,Addr(KeyConfiguration.BodenKeys.UseMine),isButton32);
  AddHotKey(ST0410250003,1,Addr(KeyConfiguration.BodenKeys.UseSensor),isButton32);

  ScrollBar.Max:=length(fKeys)-21;
end;

procedure TKeyBoardOptions.OKClick(Sender: TObject);
var
  Reg    : TRegistry;
  Dummy  : Integer;
begin
  Reg:=OpenXForceRegistry;
  Reg.WriteBinaryData('HotKeys',KeyConfiguration,SizeOf(KeyConfiguration));
  Reg.Free;
  for Dummy:=0 to Length(fKeys)-1 do
  begin
    if fKeys[Dummy].RowType=rtHotKey then
    begin
      if fKeys[Dummy].Button<>isButton32 then
      begin
        ui_utils_ChangeKeyAssign(game_api_GetDirectInput,fKeys[Dummy].Button,fKeys[Dummy].Key^);
      end;
    end;
  end;
  CloseModal;
end;

procedure TKeyBoardOptions.OnChangeKey(Sender: TObject);
begin
  fKeys[(Sender as TDXHotKey).Tag].Key^:=(Sender as TDXHotKey).HotKey;
end;

procedure TKeyBoardOptions.ScrollChange(Sender: TObject);
begin
  ShowKeys(ScrollBar.Value);
end;

procedure TKeyBoardOptions.SetDefaults;
begin
  { Anzeigen }
  ScrollBar.Value:=0;
  fOldKeys:=KeyConfiguration;
  ShowKeys(0);
end;

procedure TKeyBoardOptions.ShowKeys(Start: Integer);
var
  Dummy  : Integer;
  TopPos : Integer;
  Focus  : TDXComponent;
begin
  Container.Lock;
  Focus:=Container.FocusControl;
  for Dummy:=0 to length(fKeys)-1 do
  begin
    with fKeys[Dummy] do
    begin
      CapLabel.Visible:=false;
      if RowType=rtHotKey then
        HotKeyEdit.Visible:=false;
    end;
//    fKeys[Dummy].HotKeyEdit.Visible:=false;
  end;
  TopPos:=CapBox.Bottom+1;
  for Dummy:=Start to min(Start+20,length(fKeys)-1) do
  begin
    with fKeys[Dummy] do
    begin
      CapLabel.Visible:=true;
      CapLabel.Top:=TopPos;
      if RowType=rtHotKey then
      begin
        HotKeyEdit.Visible:=true;
        HotKeyEdit.Top:=TopPos;
        HotKeyEdit.HotKey:=Key^;
      end;
      if Dummy=Start+20 then
        CapLabel.RoundCorners:=rcLeftBottom
      else
        CapLabel.RoundCorners:=rcNone;
    end;
    inc(TopPos,22);
  end;
  Container.FocusControl:=Focus;
  Container.UnLock;
  Container.RedrawArea(Rect(CapBox.Left,CapBox.Bottom+1,CapBox.Right,490),Container.Surface);
end;

end.
