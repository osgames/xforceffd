{********************************************************************************
*										*
* Copyright (C) 2004  X-Force Team - jim_raynor@web.de				*
*										*
* This file is part of X-Force: Fight For Destiny (X-Force).			*
*										*
* X-Force is free software; you can redistribute it and/or modify		*
* it under the terms of the GNU General Public License as published by		*
* the Free Software Foundation; either version 2 of the License, or		*
* (at your option) any later version.						*
*										*
* X-Force is distributed in the hope that it will be useful,			*
* but WITHOUT ANY WARRANTY; without even the implied warranty of		*
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the			*
* GNU General Public License for more details.					*
*										*
* You should have received a copy of the GNU General Public License		*
* along with X-Force; if not, write to the Free Software			*
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA	*
*										*
*********************************************************************************
*										*
* Verwaltungsbildschirm Raumschiff ausrüsten					*
*										*
********************************************************************************}

{$I ../Settings.inc}

unit VerRaumschiff;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  KD4Page, DXContainer, DXBitmapButton, Defines, DXLabel,DirectFont,
  DXSchiffViewer, DXDraws ,Blending, DXListBox, XForce_types, KD4Utils,
  RaumschiffList, DXItemInfo, TraceFile, DXGroupCaption, DXSchiffConfig,
  DirectDraw, VerSoldatEinsatz, BasisListe, DXInfoFrame, StringConst;

type
  TAktPage = (apSoldaten,apItems,apWaffen);
  TVerRaumschiff = class(TKD4Page)
  private
    fChangeArea : TRect;
    fItemsCou   : Integer;
    fPageArea   : TRect;
    fDelArea    : TRect;
    AktPage     : TAktPage;
    AktSchiff   : TRaumschiff;
    ChangeInfo  : boolean;
    procedure SetPage(Page: TAktPage);
    procedure PageButtonClick(Sender: TObject);
    procedure ShowPadie(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);

    procedure DrawItems(Sender: TDXComponent;Surface: TDirectDrawSurface;const Mem: TDDSurfaceDesc;Rect: TRect; Index: Integer;Selected: boolean);
    procedure DrawSoldat(Sender: TDXComponent;Surface: TDirectDrawSurface;const Mem: TDDSurfaceDesc;Rect: TRect; Index: Integer;Selected: boolean);
    procedure DrawSchiff(Sender: TDXComponent;Surface: TDirectDrawSurface;const Mem: TDDSurfaceDesc;Rect: TRect; Index: Integer;Selected: boolean);
    
    procedure ItemsChange(Sender: TObject);
    procedure ItemsDblClick(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
//    procedure SetzeShield(Einbau: boolean=true);
    procedure SetzeMotor(Einbau: boolean=true);
    procedure SetzeWaffe(Zelle: Integer;Einbau: boolean=true);
    procedure SetzeExtension(Zelle: Integer;Einbau: boolean=true);
    procedure KlickKomponent(Sender: TObject; Button: TMouseButton;Zelle: TSchiffZelle);
    procedure OverKomponent(Sender: TObject; Zelle: TSchiffZelle);
    procedure DrawHeadRow(Sender: TDXComponent;Surface: TDirectDrawSurface;Rect: TRect);
    procedure EnumSoldaten;
    procedure SoldatenLeave(Sender: TObject);
    procedure SoldatenSlotLeave;
    procedure SoldatenSlotOver(Slot: Integer;HighLight: boolean; Rect: TRect; Soldat: TSoldatInfo);
    procedure ClickSlot(Slot: Integer; Button: TMouseButton);
    procedure OverListItem(Index: Integer; Rect: TRect);
    procedure OverItem(Index: Integer; Rect: TRect);
    procedure LeaveListItem;
    procedure GetNameOfNewItem(var Index: Integer);
    procedure SetAktSchiff(Schiff: TRaumschiff);
    function IndexOfItem(Item: Integer; var Index: Integer): boolean;

    procedure OnSelectedBasisChange(Sender: TObject);

    procedure ShipChange(Sender: TObject);

    procedure AktuShipList;
    { Private-Deklarationen }
  protected
    procedure SoldatClick(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
    { Protected-Deklarationen }
  public
    CloseButton    : TDXBitmapButton;
    WaffButton     : TDXBitmapButton;
    AusButton      : TDXBitmapButton;
    SolButton      : TDXBitmapButton;
    SchiffConfig   : TDXSchiffViewer;
    InfoFrame      : TDXInfoFrame;

    StatusLabel    : TDXGroupCaption;

    Items          : TDXListBox;
    ShipList       : TDXListBox;

    Soldaten       : TDXSchiffSoldaten;
    SchiffItems    : TDXSchiffItems;
    InfoPopup      : TDXPopupItemInfo;
    constructor Create(Container: TDXContainer);override;
    procedure SetDefaults;override;
  end;

var
  RaumschiffIndex: Integer;


implementation

uses
  game_api, ufopaedie_api, game_utils, ui_utils, basis_api;

{ TVerRaumschiff }

procedure TVerRaumschiff.AktuShipList;
var
  Dummy: Integer;
begin
  ShipList.ChangeItem(true);
  ShipList.Items.Clear;
  for Dummy:=0 to SaveGame.Raumschiffe.Count-1 do
  begin
    if (SaveGame.Raumschiffe[Dummy].XCOMSchiff) and SaveGame.Raumschiffe[Dummy].InSelectedBasis then
    begin
      ShipList.Items.AddObject(SaveGame.Raumschiffe[Dummy].Name,SaveGame.Raumschiffe[Dummy]);

      if SaveGame.Raumschiffe[Dummy] = AktSchiff then
        ShipList.ItemIndex:=ShipList.Items.Count-1
    end;
  end;
  ShipList.ChangeItem(false);

  if ShipList.Items.Count = 0 then
    SetAktSchiff(nil)
  else
  begin
    if AktSchiff = nil then
    begin
      ShipList.ItemIndex:=0;
      SetAktSchiff(TRaumschiff(ShipList.Items.Objects[0]));
    end;
  end;
    
  ShipList.Redraw;
end;

procedure TVerRaumschiff.ClickSlot(Slot: Integer; Button: TMouseButton);
var
  Soldat: PSoldatInfo;
  Index : Integer;
begin
  if AktSchiff = nil then
    exit;

  Container.IncLock;
  if Button=mbLeft then
  begin
    try
      if Items.Items.Count>0 then
      begin
        Assert(IndexOfItem(Items.ItemIndex,Index));
        AktSchiff.SetSoldaten(Index,Slot);
      end;
    except
      on E: Exception do
      begin
        Container.DecLock;
        game_api_MessageBox(E.Message,CInformation);
        exit;
      end;
    end;
  end
  else if Button=mbRight then
  begin
    Index:=AktSchiff.GetSoldat(Slot,Soldat);
    if Index>-1 then
      AktSchiff.SetSoldaten(Index,30);
  end;
  Soldaten.Redraw;
  EnumSoldaten;
  Container.DecLock;
  Items.Redraw;
end;

constructor TVerRaumschiff.Create(Container: TDXContainer);
begin
  inherited;
  Animation:=paRightToLeft;
  ChangeInfo:=false;

  SetPageInfo(StripHotKey(ST0311040003));

  { Informationsframe }
  InfoFrame:=TDXInfoFrame.Create(Self);
  with InfoFrame do
  begin
    SetRect(388,10,244,70);
    SetFont(Font);
    ColorSheme:=icsRed;
    Infos:=[ifCredits,ifHangar];
    SystemKeyChange:=true;
  end;

  { Schiffsliste }
  ShipList:=TDXListBox.Create(Self);
  with ShipList do
  begin
    SetRect(8,54,230,469);
    BorderColor:=coFirstColor;
    FocusColor:=coSecondColor;
    SetFont(Font);
    SetButtonFont(ScrollBarFont);
    FirstColor:=coScrollBrush;
    DirectDraw:=true;
    SecondColor:=coScrollPen;
//    Hint:=HItemList;
    ItemHeight:=40;
    OwnerDraw:=True;
    OnDrawItem:=DrawSchiff;
    OnChange:=ShipChange;
//    OnDblClick:=ItemsDblClick;
    HeadRowHeight:=17;
    HeadData:=MakeHeadData(LRaumschiffe,FSchiff,FSchiffe);
  end;

  { Waffen button }
  WaffButton:=TDXBitmapButton.Create(Self);
  with WaffButton do
  begin
    SetRect(248,54,181,28);
    SetButtonFont(Font);
    FirstColor:=coFirstColor;
    SecondColor:=coSecondColor;
    AccerlateColor:=coAccerlateColor;
    DirectDraw:=true;
    RoundCorners:=rcLeftTop;
    Text:=BWaffen;
    Hint:=HWaffButt;
    OnClick:=PageButtonClick;
  end;

  { Ausrüstung button }
  AusButton:=TDXBitmapButton.Create(Self);
  with AusButton do
  begin
    SetRect(430,54,180,28);
    SetButtonFont(Font);
    FirstColor:=coFirstColor;
    DirectDraw:=true;
    SecondColor:=coSecondColor;
    AccerlateColor:=coAccerlateColor;
    RoundCorners:=rcNone;
    Text:=BAusrust;
    Hint:=HAusButt;
    OnClick:=PageButtonClick;
  end;

  { Soldaten button }
  SolButton:=TDXBitmapButton.Create(Self);
  with SolButton do
  begin
    SetRect(611,54,181,28);
    SetButtonFont(Font);
    FirstColor:=coFirstColor;
    SecondColor:=coSecondColor;
    DirectDraw:=true;
    AccerlateColor:=coAccerlateColor;
    Text:=BSoldaten;
    RoundCorners:=rcRightTop;
    Hint:=HSolButt;
    OnClick:=PageButtonClick;
  end;

  { Statuslabel }
  StatusLabel:=TDXGroupCaption.Create(Self);
  with StatusLabel do
  begin
    SetFont(Font);
    Font.Size:=Font.Size+2;
    Font.Style:=[fsBold];
    Alignment:=taCenter;
    SetRect(248,83,544,24);
    RoundCorners:=rcNone;
    Caption:=LSinglePlayer;
    BorderColor:=coFirstColor;
  end;

  { Gegenstandslistbox }
  Items:=TDXListBox.Create(Self);
  with Items do
  begin
    SetRect(248,108,227,415);
    BorderColor:=coFirstColor;
    FocusColor:=coSecondColor;
    SetFont(Font);
    SetButtonFont(ScrollBarFont);
    FirstColor:=coScrollBrush;
    DirectDraw:=true;
    SecondColor:=coScrollPen;
    Hint:=HItemList;
    ItemHeight:=40;
    OwnerDraw:=True;
    OnDrawItem:=DrawItems;
    OnChange:=ItemsChange;
    OnOverItem:=OverListItem;
    RoundCorners:=rcLeftBottom;
    OnDblClick:=ItemsDblClick;
    OnLeaveItem:=LeaveListItem;
    HeadRowHeight:=17;
    OnDrawHeadRow:=DrawHeadRow;
  end;

  { Raumschiff Ansicht }
  SchiffConfig:=TDXSchiffViewer.Create(Self);
  SchiffConfig.SetRect(476,108,316,415);
  SchiffConfig.OnClickKomponent:=KlickKomponent;
  SchiffConfig.OnOverKomponent:=OverKomponent;
  SetFont(SchiffConfig.Font);

  { Soldatenverwaltung }
  Soldaten:=TDXSchiffSoldaten.Create(Self);
  with Soldaten do
  begin
    SetRect(476,108,316,415);
    OnClickSlot:=ClickSlot;
    Visible:=false;
    OnMouseLeave:=SoldatenLeave;
    OnOverSlot:=SoldatenSlotOver;
    OnLeaveSlot:=SoldatenSlotLeave;
    Hint:=HSoldatConfig;
  end;

  { Soldatenausrüstung }
  SchiffItems:=TDXSchiffItems.Create(Self);
  with SchiffItems do
  begin
    SetFont(Font);
    SetRect(248,108,544,415);
    Visible:=false;
    OnOverSlot:=OverItem;
    OnLeaveSlot:=SoldatenSlotLeave;
    GetNameOfNew:=GetNameOfNewItem;
  end;

  { Infopopup }
  InfoPopup:=TDXPopupItemInfo.Create(Self);
  SetFont(InfoPopup.Font);

  fChangeArea:=SchiffConfig.ClientRect;
  UnionRect(fDelArea,fChangeArea,Items.ClientRect);
  fPageArea:=Items.ClientRect;
  UnionRect(fPageArea,fPageArea,SchiffConfig.ClientRect);

  basis_api_RegisterChangeBaseHandler(OnSelectedBasisChange);
end;

procedure TVerRaumschiff.DrawHeadRow(Sender: TDXComponent;
  Surface: TDirectDrawSurface; Rect: TRect);
var
  Text : String;
begin
  WhiteStdFont.Draw(Surface,Rect.Left+6,Rect.Top+1,Items.HeadData.Titel);
  Text:=ZahlString(Items.HeadData.Einzel,Items.HeadData.MehrZahl,fItemsCou);
  WhiteStdFont.Draw(Surface,Rect.Right-6-WhiteStdFont.TextWidth(Text),Rect.Top+1,Text);
end;

procedure TVerRaumschiff.DrawItems(Sender: TDXComponent;
  Surface: TDirectDrawSurface;const Mem: TDDSurfaceDesc;Rect: TRect; Index: Integer;
  Selected: boolean);
var
  ItemIndex : Integer;
  Text      : String;
  TypeID    : TProjektType;
  Font      : TDirectFont;
  Count     : Integer;
  Item      : TLagerItem;
begin
  if Selected then
    Items.DrawSelection(Surface,Mem,Rect,bcMaroon);
  if not IndexOfItem(Index,ItemIndex) then
  begin
    TypeID:=TProjektType(StrToInt(Cut(Items.Items[Index],2,length(Items.Items[Index]))));
    SaveGame.LagerListe.DrawImage(TypeIDToIndex(TypeID,wtProjektil),Surface,Rect.Left+3,Rect.Top+3);
    WhiteBStdFont.Draw(Surface,Rect.Left+43,Rect.Top+3,game_utils_TypeToStr(TypeID));
  end
  else
  begin
    SaveGame.LagerListe.DrawImage(SaveGame.LagerListe.Items[ItemIndex].ImageIndex,Surface,Rect.Left+19,Rect.Top+3);
    YellowStdFont.Draw(Surface,Rect.Left+59,Rect.Top+6,SaveGame.LagerListe[ItemIndex].Name);
    Text:=LVerfuegbar;
    Item:=SaveGame.LagerListe[ItemIndex];
    Count:=Item.Anzahl;
    if Count=0 then
    begin
      Text:=LNot+Text;
      Font:=MaroonStdFont;
    end
    else
    begin
      Font:=GreenStdFont;
      Text:=IntToStr(Count)+'x '+Text;
    end;
    Font.Draw(Surface,Rect.Left+59,Rect.Top+22,Text);
  end;
end;

procedure TVerRaumschiff.DrawSchiff(Sender: TDXComponent;
  Surface: TDirectDrawSurface; const Mem: TDDSurfaceDesc; Rect: TRect;
  Index: Integer; Selected: boolean);
var
  Text    : String;
  Schaden : Integer;
  Font    : TDirectFont;
  Ship    : TRaumschiff;
begin
  Ship:=TRaumschiff(ShipList.Items.Objects[Index]);
  Assert(Ship<>nil);

  if Selected then
    ShipList.DrawSelection(Surface,Mem,Rect,bcMaroon);

  SaveGame.Raumschiffe.DrawIcon(Surface,Rect.Left+4,Rect.Top+4,Ship.WaffenZellen);
  YellowStdFont.Draw(Surface,Rect.Left+38,Rect.Top+4,Ship.Name);
  WhiteStdFont.Draw(Surface,Rect.Left+38,Rect.Top+20,Ship.StatusText);
  Schaden:=Ship.GetSchaden;
  Font:=YellowStdFont;
  case Schaden of
    66..100 : Font:=RedStdFont;
    33..65  : Font:=YellowStdFont;
    0..32   : Font:=LimeStdFont;
  end;
  Text:=Format(LSchaden,[Schaden]);
  Font.Draw(Surface,Rect.Right-8-Font.TextWidth(Text),Rect.Top+20,Text);
end;

procedure TVerRaumschiff.DrawSoldat(Sender: TDXComponent;
  Surface: TDirectDrawSurface;const Mem: TDDSurfaceDesc;Rect: TRect; Index: Integer;
  Selected: boolean);
var
  SIndex    : Integer;
  Text      : String;
begin
  if Selected then
    Items.DrawSelection(Surface,Mem,Rect,bcMaroon);
  Assert(IndexOfItem(Index,SIndex));
  SaveGame.SoldatenListe.DrawDXSoldatenBitmap(SIndex,Surface,Mem,Rect.Left+5,Rect.Top+5,true);
  YellowStdFont.Draw(Surface,Rect.Left+53,Rect.Top+5,SaveGame.SoldatenListe[SIndex].Name);
  Text:=SaveGame.SoldatenListe.GetRaumschiffName(SIndex);
  if Text='' then
  begin
    if SaveGame.SoldatenListe[SIndex].Ges<SaveGame.SoldatenListe[SIndex].MaxGes then
      YellowStdFont.Draw(Surface,Rect.Left+53,Rect.Top+22,LKranken)
    else if SaveGame.SoldatenListe[SIndex].Train then
      WhiteStdFont.Draw(Surface,Rect.Left+53,Rect.Top+22,LTraining)
    else
      WhiteStdFont.Draw(Surface,Rect.Left+53,Rect.Top+22,LNoCommand);
  end
  else
  begin
    WhiteStdFont.Draw(Surface,Rect.Left+53,Rect.Top+22,LStarShip+Text);
    if SaveGame.SoldatenListe[SIndex].Ges<SaveGame.SoldatenListe[SIndex].MaxGes then
      YellowStdFont.Draw(Surface,Rect.Left+53,Rect.Top+40,LKranken)
    else if SaveGame.SoldatenListe[SIndex].Train then
      WhiteStdFont.Draw(Surface,Rect.Left+53,Rect.Top+40,LTraining);
  end;
end;

procedure TVerRaumschiff.EnumSoldaten;
var
  Dummy   : Integer;
  BasisID : Cardinal;
  Index   : Integer;
begin
  Index:=Items.ItemIndex;
  Items.Items.Clear;
  BasisID:=basis_api_getSelectedBasis.ID;
  Container.Lock;
  for Dummy:=0 to SaveGame.SoldatenListe.Count-1 do
  begin
    if SaveGame.SoldatenListe[Dummy].BasisID=BasisID then
      Items.Items.Add(IntToStr(Dummy));
  end;
  fItemsCou:=Items.Items.Count;
  Items.ItemIndex:=Index;
  Container.Unlock;
  Items.Redraw;
end;

procedure TVerRaumschiff.GetNameOfNewItem(var Index: Integer);
begin
  if not IndexOfItem(Items.ItemIndex,Index) then
    Index:=-1;
end;

function TVerRaumschiff.IndexOfItem(Item: Integer;
  var Index: Integer): boolean;
var
  Code: Integer;
begin
  if Item=-1 then
  begin
    result:=false;
    exit;
  end;
  val(Items.Items[Item],Index,Code);
  result:=Code=0;
end;

procedure TVerRaumschiff.ItemsChange(Sender: TObject);
var
  Index: Integer;
begin
  if Items.ItemIndex=-1 then exit;
  if AktPage=apWaffen then
  begin
    if not IndexOfItem(Items.ItemIndex,Index) then
    begin
      SchiffConfig.HebeVor:=rtNone;
      exit;
    end;
    case SaveGame.LagerListe[Index].TypeID of
      ptRWaffe     : SchiffConfig.HebeVor:=rtWaffen;
      ptMotor      : SchiffConfig.HebeVor:=rtMotor;
      ptShield     : SchiffConfig.HebeVor:=rtShield;
      ptExtension  : SchiffConfig.HebeVor:=rtExtension;
    end;
  end
  else if AktPage=apSoldaten then
  begin
    Assert(IndexOfItem(Items.ItemIndex,Index));
    Soldaten.Highlight:=Index;
  end;
end;

procedure TVerRaumschiff.ItemsDblClick(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
var
  Index   : Integer;
  Dummy   : Integer;
  Soldat  : PSoldatInfo;
begin
  if AktSchiff = nil then
    exit;

  if Button<>mbLeft then
    exit;

  if AktPage=apWaffen then
  begin
    if not IndexOfItem(Items.ItemIndex,Index) then exit;
    case SaveGame.LagerListe[Index].TypeID of
      ptRWaffe :
      begin
        for Dummy:=0 to AktSchiff.WaffenZellen-1 do
        begin
          if not AktSchiff.WaffenZelle[Dummy].Installiert then
          begin
            SetzeWaffe(Dummy);
            exit;
          end;
        end;
      end;
      ptMotor     : SetzeMotor;
//      ptShield    : SetzeShield;
      ptExtension :
      begin
        for Dummy:=0 to AktSchiff.ExtendsSlots-1 do
        begin
          if not AktSchiff.ExtendZelle[Dummy].Installiert then
          begin
            SetzeExtension(Dummy);
            exit;
          end;
        end;
      end;
    end;
  end
  else if AktPage=apSoldaten then
  begin
    Index:=StrToInt(Items.Items[Items.ItemIndex]);
    if SaveGame.SoldatenListe[Index].Raum=AktSchiff.ID then exit;
    for Dummy:=0 to AktSchiff.SoldatenSlots-1 do
    begin
      if AktSchiff.GetSoldat(Dummy,Soldat)=-1 then
      begin
        try
          AktSchiff.SetSoldaten(Index,Dummy);
        except
          on E: Exception do
          begin
            game_api_MessageBox(E.Message,CInformation);
            exit;
          end;
        end;
        Container.IncLock;
        Items.Redraw;
        Container.decLock;
        Soldaten.Redraw;
        exit;
      end;
    end;
  end;
end;

procedure TVerRaumschiff.KlickKomponent(Sender: TObject;
  Button: TMouseButton; Zelle: TSchiffZelle);
begin
  case Zelle.Komponent of
    hlkMotor  : SetzeMotor(Button=mbLeft);
    hlkWZelle : SetzeWaffe(Zelle.KompIndex,Button=mbLeft);
//    hlkShield : SetzeShield(Button=mbLeft);
    hlkESlot  : SetzeExtension(Zelle.KompIndex,Button=mbLeft);
  end;
end;

procedure TVerRaumschiff.LeaveListItem;
begin
  InfoPopup.Visible:=false;
end;

procedure TVerRaumschiff.OnSelectedBasisChange(Sender: TObject);
begin
  AktuShipList;

  ShipChange(Self);

  if AktPage = apSoldaten then
    EnumSoldaten
  else
    Items.Redraw;
end;

procedure TVerRaumschiff.OverItem(Index: Integer; Rect: TRect);
begin
  Container.IncLock;
  InfoPopup.UnVisible;
  Container.LoadGame(true);
  InfoPopup.BorderColor:=clRed;
  InfoPopup.BlendColor:=bcMaroon;
  InfoPopup.Item:=SchiffItems.ItemAtIndex(Index)^;
  InfoPopup.ReserveMuni:=-1;
  InfoPopup.SetAlignRect(Rect);
  Container.LoadGame(false);
  Container.DecLock;
  InfoPopup.Visible:=true;
  Container.PlaySound(SMenuPopup);
end;

procedure TVerRaumschiff.OverKomponent(Sender: TObject;Zelle: TSchiffZelle);
var
  Index  : Integer;
  Hints  : String;
begin
  if AktSchiff = nil then
    exit;

  if Items.ItemIndex=-1 then
    exit;

  Container.Lock;
  if not IndexOfItem(Items.ItemIndex,Index) then
    Index:=0;
  case Zelle.Komponent of
    hlkNone :
    begin
      Container.Unlock;
      Container.IncLock;
      InfoPopup.Visible:=false;
      Container.LoadGame(true);
      SchiffConfig.Hint:='';
    end;
    hlkMotor:
    begin
      if Items.Items[Items.ItemIndex][1]='C' then
        Hints:=''
      else
      begin
        if SaveGame.LagerListe[Index].TypeID<>ptMotor then
          Hints:=Format(HWrongPlace,[SaveGame.LagerListe[Index].Name])
        else
          Hints:=HClickHere+Format(HMotorEinbauen,[SaveGame.LagerListe[Index].Name]);
      end;
      if AktSchiff.Motor.Installiert then
      begin
        if Hints<>'' then Hints:=Hints+#10;
        Hints:=Hints+HMotorAusbauen;
      end;
      SchiffConfig.Hint:=Hints;
      if AktSchiff.GetMotorIndex<>-1 then
      begin
        Container.Unlock;
        Container.IncLock;
        InfoPopup.Unvisible;
        Container.LoadGame(true);
        InfoPopup.BorderColor:=clRed;
        InfoPopup.BlendColor:=bcMaroon;
        InfoPopup.Munition:=round(AktSchiff.TreibStoff);
        InfoPopup.Item:=SaveGame.LagerListe[AktSchiff.GetMotorIndex];
        InfoPopup.SetAlignRect(SchiffConfig.GetObjektRect(Zelle));
        InfoPopup.Visible:=true;
        Container.PlaySound(SMenuPopup);
      end
      else InfoPopup.Visible:=false;
    end;
    hlkWZelle:
    begin
      if Items.Items[Items.ItemIndex][1]='C' then
        Hints:=''
      else
      begin
        if SaveGame.LagerListe[Index].TypeID<>ptRWaffe then
          Hints:=Format(HWrongPlace,[SaveGame.LagerListe[Index].Name])
        else
          Hints:=HClickHere+Format(HWaffeEinbauen,[SaveGame.LagerListe[Index].Name]);
      end;
      if AktSchiff.WaffenZelle[Zelle.KompIndex].Installiert then
      begin
        if Hints<>'' then Hints:=Hints+#10;
        Hints:=Hints+HWaffeAusbauen;
      end;
      SchiffConfig.Hint:=Hints;
      if AktSchiff.GetWaffenIndex(Zelle.KompIndex)<>-1 then
      begin
        Container.Unlock;
        Container.IncLock;
        InfoPopup.Unvisible;
        Container.LoadGame(true);
        InfoPopup.BorderColor:=clRed;
        InfoPopup.BlendColor:=bcMaroon;
        InfoPopup.Munition:=AktSchiff.WaffenZelle[Zelle.KompIndex].Munition;
        InfoPopup.MaxMunition:=AktSchiff.GetMaxMunition(Zelle.KompIndex);
        InfoPopup.ReserveMuni:=AktSchiff.WaffenZelle[Zelle.KompIndex].ReserveMunition;
        InfoPopup.Item:=SaveGame.LagerListe[AktSchiff.GetWaffenIndex(Zelle.KompIndex)];
        InfoPopup.SetAlignRect(SchiffConfig.GetObjektRect(Zelle));
        InfoPopup.Visible:=true;
        Container.PlaySound(SMenuPopup);
      end
      else InfoPopup.Visible:=false;
    end;
    hlkESlot:
    begin
      if Items.Items[Items.ItemIndex][1]='C' then
        Hints:=''
      else
      begin
        if SaveGame.LagerListe[Index].TypeID<>ptExtension then
          Hints:=Format(HWrongPlace,[SaveGame.LagerListe[Index].Name])
        else
          Hints:=HClickHere+Format(HExtEinbauen,[SaveGame.LagerListe[Index].Name]);
      end;
      if AktSchiff.ExtendZelle[Zelle.KompIndex].Installiert then
      begin
        if Hints<>'' then Hints:=Hints+#10;
        Hints:=Hints+HExtendAusbauen;
      end;
      SchiffConfig.Hint:=Hints;
      if AktSchiff.GetExtendIndex(Zelle.KompIndex)<>-1 then
      begin
        Container.Unlock;
        Container.IncLock;
        InfoPopup.Unvisible;
        Container.LoadGame(true);
        InfoPopup.BorderColor:=clRed;
        InfoPopup.BlendColor:=bcMaroon;
        InfoPopup.Item:=SaveGame.LagerListe[AktSchiff.GetExtendIndex(Zelle.KompIndex)];
        InfoPopup.SetAlignRect(SchiffConfig.GetObjektRect(Zelle));
        InfoPopup.Visible:=true;
        Container.PlaySound(SMenuPopup);
      end
      else InfoPopup.Visible:=false;
    end;
    hlkShield:
    begin
      if (AktPage=apWaffen) then
      begin
        if Items.Items[Items.ItemIndex][1]='C' then
          Hints:=''
        else
        begin
          if SaveGame.LagerListe[Index].TypeID<>ptShield then
            Hints:=Format(HWrongPlace,[SaveGame.LagerListe[Index].Name])
          else
            Hints:=HClickHere+Format(HShieldEinbauen,[SaveGame.LagerListe[Index].Name]);
        end;
        if AktSchiff.ShieldInstalliert then
        begin
          if Hints<>'' then Hints:=Hints+#10;
          Hints:=Hints+HShieldAusbauen;
        end;
        SchiffConfig.Hint:=Hints;
      end;
      (*
      if AktSchiff.GetShieldIndex<>-1 then
      begin
        Container.Unlock;
        Container.IncLock;
        InfoPopup.Unvisible;
        Container.LoadGame(true);
        InfoPopup.BorderColor:=clRed;
        InfoPopup.BlendColor:=bcMaroon;
        InfoPopup.Item:=SaveGame.LagerListe[AktSchiff.GetShieldIndex];
        InfoPopup.SetAlignRect(SchiffConfig.GetObjektRect(Zelle));
        InfoPopup.Visible:=true;
        Container.PlaySound(SMenuPopup);
      end
      else InfoPopup.Visible:=false;
      *)
    end;
  end;
  Container.LoadGame(false);
  InfoPopup.Redraw;
  Container.DecLock;
  HintLabel.Redraw;
end;

procedure TVerRaumschiff.OverListItem(Index: Integer; Rect: TRect);
var
  LIndex: Integer;
begin
  if AktPage=apSoldaten then
  begin
    if not IndexOfItem(Index,LIndex) then
      exit;
    Container.IncLock;
    InfoPopup.UnVisible;
    Container.LoadGame(true);
    InfoPopup.BorderColor:=clRed;
    InfoPopup.BlendColor:=bcMaroon;
    InfoPopup.Soldat:=SaveGame.SoldatenListe[LIndex];
    InfoPopup.SetAlignRect(Rect);
    Container.LoadGame(false);
    Container.DecLock;
    InfoPopup.Visible:=true;
    Container.PlaySound(SMenuPopup);
  end
  else
  begin
    if not IndexOfItem(Index,LIndex) then
      exit;
    Container.IncLock;
    InfoPopup.Unvisible;
    Container.LoadGame(true);
    InfoPopup.BorderColor:=clRed;
    InfoPopup.BlendColor:=bcMaroon;
    InfoPopup.Munition:=-1;
    InfoPopup.Item:=SaveGame.LagerListe[LIndex];
    InfoPopup.MaxMunition:=SaveGame.LagerListe.GetMaxMunition(LIndex);
    InfoPopup.SetAlignRect(Rect);
    Container.Unlock;
    InfoPopup.Visible:=true;
    Container.PlaySound(SMenuPopup);
  end;
end;

procedure TVerRaumschiff.PageButtonClick(Sender: TObject);
begin
  if Sender=AusButton then
    SetPage(apItems)
  else if Sender=SolButton then
    SetPage(apSoldaten)
  else
    SetPage(apWaffen);
end;

procedure TVerRaumschiff.SetAktSchiff(Schiff: TRaumschiff);
begin
  AktSchiff:=Schiff;

  if AktSchiff = nil then
    StatusLabel.Caption := ST0502130001
  else if not (sabEquipt in AktSchiff.Abilities) then
    StatusLabel.Caption := ST0502140001
  else
    StatusLabel.Caption := LInBasis;

  SchiffConfig.Schiff:=AktSchiff;
  Soldaten.Schiff:=AktSchiff;
  SchiffItems.Schiff:=AktSchiff;
end;

procedure TVerRaumschiff.SetDefaults;
begin
  inherited;
  InfoFrame.Reset;

  SetAktSchiff(SaveGame.Raumschiffe.Schiffe[RaumschiffIndex]);

  AktuShipList;

  SetPage(apWaffen);
  
  fItemsCou:=ui_utils_FillItems(Items,SaveGame.LagerListe,ifSchiff);
  SchiffConfig.HebeVor:=rtNone;
end;

procedure TVerRaumschiff.SetPage(Page: TAktPage);
var
  RedrawButton1: TDXComponent;
  RedrawButton2: TDXComponent;
begin
  if AktPage=Page then
    exit;
  Container.IncLock;
  InfoPopup.UnVisible(true);
  Container.DecLock;
  Container.Lock;
  RedrawButton1:=nil;
  RedrawButton2:=nil;
  case AktPage of
    apSoldaten : RedrawButton2:=SolButton;
    apItems    : RedrawButton2:=AusButton;
    apWaffen   : RedrawButton2:=WaffButton;
  end;
  AktPage:=Page;
  WaffButton.HighLight:=Page=apWaffen;
  AusButton.HighLight:=Page=apItems;
  SolButton.HighLight:=Page=apSoldaten;
  case Page of
    apWaffen:
    begin
      RedrawButton1:=WaffButton;
      Items.Visible:=true;
      Items.ItemHeight:=40;
      Items.Hint:=HWaffen;
      Items.HeadData:=MakeHeadData(LWaffen,FWaffe,FWaffen);
      Items.OnDrawItem:=DrawItems;
      Items.OnMouseUp:=ShowPadie;

      fItemsCou:=ui_utils_FillItems(Items,SaveGame.LagerListe,ifSchiff);
      ItemsChange(Self);

      SchiffConfig.MouseLeave;
      SchiffConfig.Visible:=true;
      SchiffItems.Visible:=false;
      Soldaten.Visible:=false;
    end;
    apItems:
    begin
      RedrawButton1:=AusButton;
      Items.Visible:=false;
      SchiffItems.Visible:=true;
      SchiffConfig.Visible:=false;
      Soldaten.Visible:=false;
    end;
    apSoldaten:
    begin
      RedrawButton1:=SolButton;
      Items.Visible:=true;
      Items.ItemHeight:=56;
      Items.Hint:=HRSoldaten;
      Items.HeadData:=MakeHeadData(IASoldaten,FSoldat,FSoldaten);
      Items.OnDrawItem:=DrawSoldat;
      Items.Items.Clear;
      Items.OnMouseUp:=SoldatClick;

      EnumSoldaten;
      fItemsCou:=Items.Items.Count;
      ItemsChange(Self);

      SchiffConfig.Visible:=false;
      Soldaten.Visible:=true;
      SchiffItems.Visible:=false;
    end;
  end;
  Container.LoadGame(false);
  Container.DoMouseMessage;
  RedrawButton2.Redraw;
  RedrawButton1.Redraw;
  SchiffConfig.Hint:='';
  Container.RedrawArea(fPageArea,Container.Surface);
  Container.DecLock;
  HintLabel.Redraw;
end;

procedure TVerRaumschiff.SetzeExtension(Zelle: Integer; Einbau: boolean);
var
  Index: Integer;
begin
  if AktSchiff = nil then
    exit;

  if not Einbau then
  begin
    AktSchiff.ExtensionAusbauen(Zelle);
    SchiffConfig.Hint:='';
  end
  else
  begin
    if not IndexOfItem(Items.ItemIndex,Index) then exit;
    try
      if not AktSchiff.SetzeExtension(Index,Zelle) then exit;
    except
      on E: Exception do
        game_api_MessageBox(E.Message,CInformation);
    end;
  end;
  Container.IncLock;
  SchiffConfig.Redraw;
  SchiffConfig.MouseLeave;
  Container.DoMouseMessage;
  Container.DecLock;
  Items.Redraw;
end;

procedure TVerRaumschiff.SetzeMotor(Einbau: boolean);
var
  Index: Integer;
begin
  if AktSchiff = nil then
    exit;

  if not Einbau then
  begin
    AktSchiff.MotorAusbauen;
    SchiffConfig.Hint:='';
  end
  else
  begin
    if not IndexOfItem(Items.ItemIndex,Index) then exit;
    try
      if not AktSchiff.SetzeMotor(Index) then exit;
    except
      on E: ENotEnoughMoney do
        game_api_MessageBox(E.Message,CInformation);
        
      on E: Exception do
      begin
        game_api_MessageBox(E.Message,CInformation);
        exit;
      end;
    end;
  end;
  Container.IncLock;
  SchiffConfig.Redraw;
  SchiffConfig.MouseLeave;
  Container.DoMouseMessage;
  Container.DecLock;
  Items.Redraw;
end;

(*
procedure TVerRaumschiff.SetzeShield(Einbau: boolean);
var
  Index: Integer;
begin
  if AktSchiff = nil then
    exit;

  if not Einbau then
  begin
    AktSchiff.ShieldAusbauen;
    SchiffConfig.Hint:='';
  end
  else
  begin
    if not IndexOfItem(Items.ItemIndex,Index) then exit;
    try
      if not AktSchiff.SetzeShield(Index) then exit;
    except
      on E: Exception do
      begin
        game_api_MessageBox(E.Message,CInformation);
        exit;
      end;
    end;
  end;
  Container.IncLock;
  SchiffConfig.Redraw;
  SchiffConfig.MouseLeave;
  Container.DoMouseMessage;
  Container.DecLock;
  Items.Redraw;;
end;
*)

procedure TVerRaumschiff.SetzeWaffe(Zelle: Integer;Einbau: boolean=true);
var
  Index: Integer;
begin
  if AktSchiff = nil then
    exit;

  if not Einbau then
  begin
    AktSchiff.WaffeAusbauen(Zelle);
    SchiffConfig.Hint:='';
  end
  else
  begin
    if not IndexOfItem(Items.ItemIndex,Index) then exit;
    try
      if not AktSchiff.SetzeWaffe(Index,Zelle) then exit;
    except
      on E: Exception do
        game_api_MessageBox(E.Message,CInformation);
    end;
  end;
  Container.IncLock;
  SchiffConfig.Redraw;
  SchiffConfig.MouseLeave;
  Container.DoMouseMessage;
  Container.DecLock;
  Items.Redraw;
end;

procedure TVerRaumschiff.ShipChange(Sender: TObject);
begin
  if ShipList.ItemIndex=-1 then
    SetAktSchiff(nil)
  else
    SetAktSchiff(TRaumschiff(ShipList.Items.Objects[ShipList.ItemIndex]));
end;

procedure TVerRaumschiff.ShowPadie(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
var
  Index: Integer;
begin
  if (Button=mbRight) and (AktPage<>apSoldaten) and (Items.Items.Count<>0) then
  begin
    if not IndexOfItem(Items.ItemIndex,Index) then
      exit;

    ufopaedie_api_ShowEntry(upeLagerItem,Index);
  end;
end;

procedure TVerRaumschiff.SoldatClick(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
var
  Dummy   : Integer;
  List    : TList;
  SIndex  : Integer;
begin
  if Button=mbRight then
  begin
    if Items.Items.Count=0 then
      exit;

    List:=TList.Create;
    for Dummy:=0 to Items.Items.Count-1 do
    begin
      IndexOfItem(Dummy,SIndex);
      List.Add(SaveGame.SoldatenListe.Managers[SIndex]);
    end;
    Managers:=List;
    IndexOfItem(Items.ItemIndex,SIndex);
    ActiveManager:=SaveGame.SoldatenListe.Managers[SIndex];
    ChangePage(PageSoldatConfig);
    List.Free;
  end;
end;

procedure TVerRaumschiff.SoldatenLeave(Sender: TObject);
begin
  InfoPopup.Visible:=false;
end;

procedure TVerRaumschiff.SoldatenSlotLeave;
begin
//  if AktPage=apSoldaten then
  InfoPopup.Visible:=false;
end;

procedure TVerRaumschiff.SoldatenSlotOver(Slot: Integer;HighLight: boolean; Rect: TRect; Soldat: TSoldatInfo);
begin
  Container.IncLock;
  InfoPopup.UnVisible;
  Container.LoadGame(true);
  if HighLight then
  begin
    InfoPopup.BorderColor:=clBlue;
    InfoPopup.BlendColor:=bcNavy;
  end
  else
  begin
    InfoPopup.BorderColor:=clLime;
    InfoPopup.BlendColor:=bcGreen;
  end;
  InfoPopup.Soldat:=Soldat;
  InfoPopup.SetAlignRect(Rect);
  Container.LoadGame(false);
  Container.DecLock;
  InfoPopup.Visible:=true;
  Container.PlaySound(SMenuPopup);
end;

end.
