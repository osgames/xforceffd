{********************************************************************************
*										*
* Copyright (C) 2004  X-Force Team - jim_raynor@web.de				*
*										*
* This file is part of X-Force: Fight For Destiny (X-Force).			*
*										*
* X-Force is free software; you can redistribute it and/or modify		*
* it under the terms of the GNU General Public License as published by		*
* the Free Software Foundation; either version 2 of the License, or		*
* (at your option) any later version.						*
*										*
* X-Force is distributed in the hope that it will be useful,			*
* but WITHOUT ANY WARRANTY; without even the implied warranty of		*
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the			*
* GNU General Public License for more details.					*
*										*
* You should have received a copy of the GNU General Public License		*
* along with X-Force; if not, write to the Free Software			*
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA	*
*										*
*********************************************************************************
*										*
* Bildschirm, auf dem Raumschiffkampf ausgetragen wird.				*
*										*
********************************************************************************}

{$I ../Settings.inc}

unit UFOKampfPage;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  DXContainer, KD4Page, RaumschiffList, UFOList, DXFrameBox, XForce_Types, Blending,
  DXLabel, DXUFOKampf, Defines, StringConst;

type
  TUFOKampfPage = class(TKD4Page)
  private
    fRaumschiff: TRaumschiff;
    fUFO       : TUFO;
    { Private-Deklarationen }
  protected
    { Protected-Deklarationen }
  public
    { Kampfbereich }
    UFOKampf        : TDXUFOKampf;
    constructor Create(Container: TDXContainer);override;
    procedure SetDefaults;override;
    procedure PageShown;override;

    property UFO: TUFO read fUFO write fUFO;
    property Raumschiff: TRaumschiff read fRaumschiff write fRaumschiff;
  end;

implementation

{ TUFOKampfPage }

constructor TUFOKampfPage.Create(Container: TDXContainer);
begin
  inherited;
  Animation:=paLeftToRight;

  { Kampfbereich }
  UFOKampf:=TDXUFOKampf.Create(Self);
  with UFOKampf do
  begin
    SetRect(0,0,800,600);
  end;
end;

procedure TUFOKampfPage.PageShown;
begin
  Container.MessageLock(true);
//  Container.MoveMessage:=false;
  Container.SetCursor(CAuswahl);
  UFOKampf.StartGame;
  Container.SetCursor(CZeiger);
//  Container.MoveMessage:=true;
  Container.MessageLock(false);
//  Container.ShowCursor(true);
  Container.LeaveModal:=true;
end;

procedure TUFOKampfPage.SetDefaults;
begin
//  Container.ShowCursor(false);
  UFOKampf.Raumschiff:=fRaumschiff;
  UFOKampf.UFO:=fUFO;
  UFOKampf.Init;
end;

end.
