{********************************************************************************
*										*
* Copyright (C) 2004  X-Force Team - jim_raynor@web.de				*
*										*
* This file is part of X-Force: Fight For Destiny (X-Force).			*
*										*
* X-Force is free software; you can redistribute it and/or modify		*
* it under the terms of the GNU General Public License as published by		*
* the Free Software Foundation; either version 2 of the License, or		*
* (at your option) any later version.						*
*										*
* X-Force is distributed in the hope that it will be useful,			*
* but WITHOUT ANY WARRANTY; without even the implied warranty of		*
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the			*
* GNU General Public License for more details.					*
*										*
* You should have received a copy of the GNU General Public License		*
* along with X-Force; if not, write to the Free Software			*
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA	*
*										*
*********************************************************************************
*										*
* UFOP�die									*
*										*
********************************************************************************}

{$I ../Settings.inc}

unit InfoUfoPaedie;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  KD4Page, DXContainer, Defines, DXBitmapButton, DXListBox, DirectDraw,
  DirectFont, XForce_types, DXClass, DXDraws, DXItemInfo, Blending, KD4Utils, DXCanvas,
  TraceFile, DXGroupCaption, DXListBoxWindow, DXTextViewer, ArchivFile, DXInfoFrame,
  StringConst;

type
  TPadieCaption = record
    Name   : String[30];
    Image  : Integer;
    Group  : TUFOPadieEntry;
    TypeID : TProjektType;
    Info   : String[50];
    Button : TDXBitmapButton;
  end;

  TInfoUfoPaedie = class(TKD4Page)
  private
    fNextButtonInfo: record
                       Left     : Integer;
                       Top      : Integer;
                       Corners  : TRoundCorners;
                     end;
    procedure ShowItemList;
    function ShowKategorie(Kategorie: Integer): boolean;
    procedure ShowEntry(Entry: Integer);
    procedure ShowIndexInKat(Index: Integer);
    procedure CreateRelatedItems;
    procedure ShowRelatedButtons(Beginn: Integer);

    procedure SetInfoText(Text: String);

    procedure OpenRessourceBitmap(BitmapName: String);

    procedure CreateKategorieList(Kategorie: Integer);
    function  SortFunc(Index1, Index2: Integer; Typ: TFunctionType): Integer;

    function NewEntry: PPadieEntry;
    
    // Aufbau der Kategoriebuttons
    procedure CreateKategoriesButtons;
    procedure RowSpace(Pixels: Integer = 0);

    function FindKategorie(Group: TUFOPadieEntry; TypeID : TProjektType = ptWaffe): Integer;
    { Private-Deklarationen }
  protected
    fItemsArray    : Array of TPadieEntry;
    fCaptions      : Array of TPadieCaption;
    fRelatedItems  : Array of record
                                Button : TDXBitmapButton;
                                Group  : TUFOPadieEntry;
                                Index  : Integer;
                              end;
    fRelatedStart  : Integer;
    fKategorie     : Integer;
    fEntry         : Integer;
    fDontRefreshRel: Boolean;
    fImage         : TDirectDrawSurface;

    fFirst         : boolean;

    procedure AddCaption(Caption: String; TypeID: TProjektType; Image: Integer;Group: TUFOPadieEntry);

    procedure DrawImage(Sender: TDXComponent;Surface: TDirectDrawSurface;var Mem: TDDSurfaceDesc);

    procedure ClickKategorieButton(Sender: TObject);
    procedure ClickItemList(Sender: TObject);

    procedure ClickChangeKat(Sender: TObject);
    procedure ClickChangeItem(Sender: TObject);

    procedure ClickRelatedItem(Sender: TObject);
    procedure ClickRelNav(Sender: TObject);
    procedure DrawEntry(Sender: TDXComponent; Surface: TDirectDrawSurface;
      const Mem: TDDSurfaceDesc; Rect: TRect; Index: Integer;
      Selected: boolean);
    { Protected-Deklarationen }
  public
    CloseButton         : TDXBitmapButton;

    KategorieBox        : TDXGroupCaption;
    NextKatButton       : TDXBitmapButton;
    PrevKatButton       : TDXBitmapButton;

    BackButton          : TDXBitmapButton;
    NextButton          : TDXBitmapButton;

    NameBox             : TDXGroupCaption;
    ChooseItemButton    : TDXBitmapButton;
    NextItemButton      : TDXBitmapButton;
    PrevItemButton      : TDXBitmapButton;

    ItemInfo            : TDXItemInfo;
    InfoText            : TDXTextViewer;
    RelNext             : TDXBitmapButton;
    RelPrev             : TDXBitmapButton;
    ImageCanvas         : TDXCanvas;

    ListBoxWindow       : TDXListBoxWindow;
    constructor Create(Container: TDXContainer);override;
    procedure ShowUFOPadieEntry(PadieEntry: TUFOPadieEntry; Index: Integer);

    procedure DontAllowChangePageViaNavigator;

    procedure SetDefaults;override;
  end;

const
  ButtonTop = 5;

implementation

uses
  lager_api, game_api, ufopaedie_api, archiv_utils;

var
  UseNavigator: Boolean = true;

{ TInfoUfoPaedie }

procedure TInfoUfoPaedie.AddCaption(Caption: String; TypeID: TProjektType;
  Image: Integer;Group: TUFOPadieEntry);
begin
  SetLength(fCaptions,Length(fCaptions)+1);
  fCaptions[high(fCaptions)].Name:=Caption;
  fCaptions[high(fCaptions)].Image:=Image;
  fCaptions[high(fCaptions)].TypeID:=TypeID;
  fCaptions[high(fCaptions)].Group:=Group;

  // Button der Kategorie anlegen
  fCaptions[high(fCaptions)].Button:=TDXBitmapButton.Create(Self);

  with fCaptions[high(fCaptions)].Button do
  begin
    SetButtonFont(Font);
    Text:=Caption;
    SetRect(fNextButtonInfo.Left,fNextButtonInfo.Top,125,28);
    FirstColor:=coFirstColor;
    SecondColor:=coSecondColor;
    AccerlateColor:=coAccerlateColor;
    RoundCorners:=fNextButtonInfo.Corners;
    Tag:=high(fCaptions);
    OnClick:=ClickKategorieButton;
  end;

  inc(fNextButtonInfo.Top,29);
  fNextButtonInfo.Corners:=rcNone;
end;

procedure TInfoUfoPaedie.ClickKategorieButton(Sender: TObject);
begin
  if not ShowKategorie((Sender as TDXBitmapButton).Tag) then
    game_api_MessageBox(ST0310090001,CInformation);
end;

constructor TInfoUfoPaedie.Create(Container: TDXContainer);
var
  DeltaTop: Integer;
begin
  inherited;
  Animation:=paBottomToUp;

  ufopaedie_api_init(Self);

  SetPageInfo(StripHotKey(BUfoPadie));

  // Sorgt daf�r, dass beim ersten Aufruf der UFOP�die eine Ausr�stung angezeigt wird
  fFirst:=true;

  {$IFNDEF OLD_STYLE_PAEDIA}
  DeltaTop := 50;
  with TDXInfoFrame.Create(Self) do
  begin
    SetRect(388,10,324,90);
    SetFont(Font);
    ColorSheme:=icsRed;
    Infos:=[ifCredits];
    SystemKeyChange:=true;
    Reset;
  end;
  {$ELSE}
  DeltaTop := 0;
  { SchliessenButton }
  CloseButton:=TDXBitmapButton.Create(Self);
  with CloseButton do
  begin
    SetButtonFont(Font);
    Text:=BClose;
    SetRect(569,400,110,28);
    FirstColor:=coFirstColor;
    SecondColor:=coSecondColor;
    AccerlateColor:=coAccerlateColor;
    OnClick:=ClosePage;
    EscButton:=true;
    Hint:=HCloseButton;
  end;
  {$ENDIF}

  // Auswahl der Kategorie
  KategorieBox:=TDXGroupCaption.Create(Self);
  with KategorieBox do
  begin
    SetFont(Font);
    FontColor:=clYellow;
    RoundCorners:=rcNone;
    Alignment:=taCenter;
    Font.Style:=[fsBold];
    TopMargin:=3;
    SetRect(182,5+DeltaTop,588,21);
  end;

  PrevKatButton:=TDXBitmapButton.Create(Self);
  with PrevKatButton do
  begin
    SetButtonFont(Font);
    Text:='<';
    SetRect(160,5+DeltaTop,21,21);
    FirstColor:=coFirstColor;
    SecondColor:=coSecondColor;
    AccerlateColor:=coAccerlateColor;
    RoundCorners:=rcLeftTop;
    OnClick:=ClickChangeKat;
    Tag:=-1;
    Hint:=ST0312170002;
  end;

  NextKatButton:=TDXBitmapButton.Create(Self);
  with NextKatButton do
  begin
    SetButtonFont(Font);
    Text:='>';
    SetRect(771,5+DeltaTop,21,21);
    FirstColor:=coFirstColor;
    SecondColor:=coSecondColor;
    AccerlateColor:=coAccerlateColor;
    RoundCorners:=rcRightTop;
    OnClick:=ClickChangeKat;
    Tag:=1;
    Hint:=ST0312170001;
  end;

  // Auswahl des Eintrages
  NameBox:=TDXGroupCaption.Create(Self);
  with NameBox do
  begin
    SetFont(Font);
    FontColor:=clWhite;
    Alignment:=taCenter;
    RoundCorners:=rcNone;
    TopMargin:=3;
    SetRect(182,27+DeltaTop,566,21);
  end;

  ChooseItemButton:=TDXBitmapButton.Create(Self);
  with ChooseItemButton do
  begin
    SetButtonFont(Font);
    Text:='...';
    SetRect(749,27+DeltaTop,21,21);
    FirstColor:=coFirstColor;
    SecondColor:=coSecondColor;
    AccerlateColor:=coAccerlateColor;
    RoundCorners:=rcNone;
    OnClick:=ClickItemList;
    Hint:=ST0310070003;
  end;

  PrevItemButton:=TDXBitmapButton.Create(Self);
  with PrevItemButton do
  begin
    SetButtonFont(Font);
    Text:='<';
    SetRect(160,27+DeltaTop,21,21);
    FirstColor:=coFirstColor;
    SecondColor:=coSecondColor;
    AccerlateColor:=coAccerlateColor;
    RoundCorners:=rcNone;
    OnClick:=ClickChangeItem;
    Tag:=-1;
    Hint:=ST0312170004;
  end;

  NextItemButton:=TDXBitmapButton.Create(Self);
  with NextItemButton do
  begin
    SetButtonFont(Font);
    Text:='>';
    SetRect(771,27+DeltaTop,21,21);
    FirstColor:=coFirstColor;
    SecondColor:=coSecondColor;
    AccerlateColor:=coAccerlateColor;
    RoundCorners:=rcNone;
    OnClick:=ClickChangeItem;
    Tag:=1;
    Hint:=ST0312170003;
  end;

  // Anzeige des Eintrages
  InfoText:=TDXTextViewer.Create(Self);
  with InfoText do
  begin
    SetFont(Font);
    SetRect(160,210+DeltaTop,343,251);
    BlendColor:=bcMaroon;
    Blending:=true;
    RoundCorners:=rcNone;
    BorderColor:=clMaroon;
  end;

  ImageCanvas:=TDXCanvas.Create(Self);
  with ImageCanvas do
  begin
    SetRect(160,49+DeltaTop,343,160);
    OnPaint:=DrawImage;
  end;

  { Navigation f�r RelatedItems }
  RelPrev:=TDXBitmapButton.Create(Self);
  with RelPrev do
  begin
    SetButtonFont(Font);
    Text:='<';
    SetRect(735,462+DeltaTop,28,28);
    FirstColor:=coFirstColor;
    SecondColor:=coSecondColor;
    AccerlateColor:=coAccerlateColor;
    RoundCorners:=rcLeftBottom;
    Tag:=-1;
    OnClick:=ClickRelNav;
  end;


  RelNext:=TDXBitmapButton.Create(Self);
  with RelNext do
  begin
    SetButtonFont(Font);
    Text:='>';
    SetRect(764,462+DeltaTop,28,28);
    FirstColor:=coFirstColor;
    SecondColor:=coSecondColor;
    AccerlateColor:=coAccerlateColor;
    RoundCorners:=rcRightBottom;
    Tag:=1;
    OnClick:=ClickRelNav;
  end;

  { ItemInfo }
  ItemInfo:=TDXItemInfo.Create(Self);
  with ItemInfo do
  begin
    SetFont(Font);
    BorderColor:=coFirstColor;
    SetRect(504,49+DeltaTop,288,412);
    Caption:=ST0310070005;
    Infos:=[low(TInfo)..high(TInfo)];
    Hint:=HUFOItemInfo;
    ShowInfoButton:=false;
    RoundCorners:=rcNone;
  end;

  { Listbox Fenster zur Auswahl bei mehreren Objekten }
  ListBoxWindow:=TDXListBoxWindow.Create(Self);
  with ListBoxWindow do
  begin
    SetRect(0,DeltaTop,300,200);
    Caption:=ST0310070004;
    SetButtonFont(Font);
    CaptionColor:=coFontColor;
    AccerlateColor:=coAccerlateColor;
    Font.Size:=coFontSize;
    FirstColor:=$00400000;
    SecondColor:=clBlue;
    BlendColor:=bcDarkNavy;
    SetOwnerDraw(36,4,DrawEntry);
  end;

  // Informationen f�r den ersten Kategoriebutton setzen
  fNextButtonInfo.Left:=20;
  fNextButtonInfo.Top:=ButtonTop+DeltaTop;
  fNextButtonInfo.Corners:=rcTop;

  CreateKategoriesButtons;

  fDontRefreshRel:=false;
end;

procedure TInfoUfoPaedie.CreateKategoriesButtons;
begin
  AddCaption(IAWaffe,ptWaffe,3,upeLagerItem);
  AddCaption(IAMunition,ptMunition,2,upeLagerItem);
  AddCaption(IAGranate,ptGranate,0,upeLagerItem);
  AddCaption(IAMine,ptMine,1,upeLagerItem);
  AddCaption(IAPanzerung,ptPanzerung,4,upeLagerItem);
  AddCaption(ST0309260009,ptGuertel,17,upeLagerItem);
  AddCaption(IASensor,ptSensor,8,upeLagerItem);

  RowSpace(5);
  AddCaption(LRaumschiff,ptNone,0,upeRaumschiff);
  AddCaption(IARWaffe,ptRWaffe,5,upeLagerItem);
  AddCaption(IARMunition,ptRMunition,6,upeLagerItem);
  AddCaption(IAMotor,ptMotor,7,upeLagerItem);
//  AddCaption(IAShield,ptShield,7,upeLagerItem);
  AddCaption(IAExtension,ptExtension,16,upeLagerItem);

  RowSpace(10);
  AddCaption(LEinrichtung,ptNone,0,upeEinrichtung);

  RowSpace(5);
  AddCaption(LUFOs,ptNone,0,upeUFO);
  AddCaption(LAliens,ptNone,0,upeAlien);

  RowSpace(5);
  AddCaption(LTechnologie,ptNone,0,upeTechnologie);

  RowSpace(0);
end;

procedure TInfoUfoPaedie.CreateKategorieList(Kategorie: Integer);
var
  Dummy    : Integer;
  ItemType : TProjektType;
  Project  : TForschProject;
  Entry    : PPadieEntry;
begin
  SetLength(fItemsArray,0);

  case fCaptions[Kategorie].Group of
    upeLagerItem :
    begin
      ItemType:=fCaptions[Kategorie].TypeID;
      for Dummy:=0 to SaveGame.LagerListe.Count-1 do
      begin
        if (SaveGame.LagerListe[Dummy].TypeID=ItemType) and (lager_api_ItemVisible(SaveGame.LagerListe[Dummy])) then
        begin
          Entry:=NewEntry;
          Entry.Name:=SaveGame.LagerListe[Dummy].Name;
          Entry.Group:=upeLagerItem;
          Entry.Index:=Dummy;
          Entry.TypeID:=ItemType;
          Entry.Image:=SaveGame.LagerListe[Dummy].ImageIndex;
          Entry.PaedieImage:=SaveGame.LagerListe[Dummy].UFOPaedieImage;
          Entry.Info:=SaveGame.LagerListe[Dummy].Info;
        end;
      end;
    end;
    upeEinrichtung :
    begin
      for Dummy:=0 to SaveGame.BasisListe.Einrichtungen-1 do
      begin
        Entry:=NewEntry;
        Entry.Name:=SaveGame.BasisListe.Einrichtung[Dummy].Name;
        Entry.Group:=upeEinrichtung;
        Entry.Index:=Dummy;
        Entry.Info:=SaveGame.BasisListe.Einrichtung[Dummy].Info;
        if SaveGame.BasisListe[Dummy].ImageBase<>'' then
          Entry.PaedieImage:=SaveGame.BasisListe[Dummy].ImageBase
        else
          Entry.PaedieImage:=SaveGame.BasisListe[Dummy].ImageUnder
      end;
    end;
    upeRaumschiff :
    begin
      for Dummy:=0 to SaveGame.Raumschiffe.ModelCount-1 do
      begin
        Entry:=NewEntry;
        Entry.Name:=SaveGame.Raumschiffe.Models[Dummy].Name;
        Entry.Group:=upeRaumschiff;
        Entry.Index:=Dummy;
        Entry.Image:=SaveGame.Raumschiffe.Models[Dummy].WaffenZellen;
        Entry.PaedieImage:='default\UFOPaedie:SchiffWZ'+IntToStr(SaveGame.Raumschiffe.Models[Dummy].WaffenZellen);
        Entry.Info:=SaveGame.Raumschiffe.Models[Dummy].Info;
      end;
    end;
    upeUFO :
    begin
      for Dummy:=0 to SaveGame.UFOListe.ModelCount-1 do
      begin
        if SaveGame.UFOListe.Models[Dummy].FirstMes then
        begin
          Entry:=NewEntry;
          Entry.Name:=SaveGame.UFOListe.Models[Dummy].Name;
          Entry.Group:=upeUFO;
          Entry.Index:=Dummy;
          Entry.Image:=SaveGame.UFOListe.Models[Dummy].Image;
          Entry.PaedieImage:='default\UFOPaedie:UFO '+IntToStr(SaveGame.UFOListe.Models[Dummy].Image);
          Entry.Info:=SaveGame.UFOListe.Models[Dummy].Info;
        end;
      end;
    end;
    upeAlien :
    begin
      for Dummy:=0 to SaveGame.AlienList.Count-1 do
      begin
        if SaveGame.AlienList[Dummy].Autopsie then
        begin
          Entry:=NewEntry;
          Entry.Name:=SaveGame.AlienList[Dummy].Name;
          Entry.Group:=upeAlien;
          Entry.Index:=Dummy;
          Entry.Image:=SaveGame.AlienList[Dummy].Image;
          Entry.Info:=SaveGame.AlienList[Dummy].Info;
        end;
      end;
    end;
    upeTechnologie :
    begin
      for Dummy:=0 to SaveGame.ForschListe.CompletedProjects-1 do
      begin
        if SaveGame.ForschListe.CompletedAsProject(Dummy,Project) then
        begin
          if Project.TypeID=ptNone then
          begin
            Entry:=NewEntry;
            Entry.Name:=Project.Name;
            Entry.Group:=upeTechnologie;
            Entry.Index:=Dummy;
            Entry.PaedieImage:=Project.UFOPaedieImage;
            Entry.Info:=Project.Info;
          end;
        end;
      end;
    end;
  end;

  QuickSort(0,high(fItemsArray),SortFunc);
end;

procedure TInfoUfoPaedie.RowSpace(Pixels: Integer);
begin
  if fCaptions[high(fCaptions)].Button.RoundCorners=rcNone then
    fCaptions[high(fCaptions)].Button.RoundCorners:=rcBottom
  else
    fCaptions[high(fCaptions)].Button.RoundCorners:=rcAll;

  inc(fNextButtonInfo.Top,Pixels);

  fNextButtonInfo.Corners:=rcTop;
end;

function TInfoUfoPaedie.ShowKategorie(Kategorie: Integer): Boolean;
var
  Dummy: Integer;
begin
  Container.Lock;

  CreateKategorieList(Kategorie);

  // Keine Eintr�ge in dieser Kategorie vorhanden
  if Length(fItemsArray)=0 then
  begin
    // Alte Liste wiederherstellen
    CreateKategorieList(fKategorie);

    Container.Unlock;
    result:=false;
    exit;
  end;
  result:=true;

  for Dummy:=0 to high(fCaptions) do
    fCaptions[Dummy].Button.HighLight:=Dummy=Kategorie;

  KategorieBox.Caption:=fCaptions[Kategorie].Name;

  if Kategorie<>fKategorie then
    ShowEntry(0)
  else
    ShowEntry(fEntry);

  fKategorie:=Kategorie;

  Container.Unlock;
  Container.Redraw(Container.Surface);
end;

function TInfoUfoPaedie.SortFunc(Index1, Index2: Integer;
  Typ: TFunctionType): Integer;
var
  Temp : TPadieEntry;
begin
  result:=0;
  case Typ of
    ftCompare  : result:=StrIComp(PChar(fItemsArray[Index1].Name),PChar(fItemsArray[Index2].Name));
    ftExchange :
    begin
      Temp:=fItemsArray[Index1];
      fItemsArray[Index1]:=fItemsArray[Index2];
      fItemsArray[Index2]:=Temp;
    end;
  end;
end;

procedure TInfoUfoPaedie.ShowEntry(Entry: Integer);
begin
  if (Entry<0) or (Entry>high(fItemsArray)) then
    exit;

  Container.Lock;
  NameBox.Caption:=fItemsArray[Entry].Name;

  case fItemsArray[Entry].Group of
    upeLagerItem:    ItemInfo.Item:=SaveGame.LagerListe[fItemsArray[Entry].Index];
    upeEinrichtung:  ItemInfo.Einrichtung:=SaveGame.BasisListe.Einrichtung[fItemsArray[Entry].Index];
    upeRaumschiff:   ItemInfo.RaumschiffTyp:=SaveGame.Raumschiffe.Models[fItemsArray[Entry].Index];
    upeUFO:          ItemInfo.UFOModel:=SaveGame.UFOListe.Models[fItemsArray[Entry].Index];
    upeAlien:        ItemInfo.Alien:=SaveGame.AlienList[fItemsArray[Entry].Index];
    upeTechnologie:  ItemInfo.ItemValid:=false;
  end;

  OpenRessourceBitmap(fItemsArray[Entry].PaedieImage);
  SetInfoText(fItemsArray[Entry].Info);

  fEntry:=Entry;

  CreateRelatedItems;

  Container.Unlock;
  Container.Redraw(Container.Surface);
end;

procedure TInfoUfoPaedie.ShowItemList;
var
  Dummy: Integer;
begin
  ListBoxWindow.Items.Clear;
  for Dummy:=0 to high(fItemsArray) do
  begin
    ListBoxWindow.Items.Add(fItemsArray[Dummy].Name);
  end;
  ShowEntry(ListBoxWindow.Show);
end;

procedure TInfoUfoPaedie.ClickItemList(Sender: TObject);
begin
  ShowItemList;
end;

procedure TInfoUfoPaedie.DrawEntry(Sender: TDXComponent;
  Surface: TDirectDrawSurface;const Mem: TDDSurfaceDesc; Rect: TRect; Index: Integer;
  Selected: boolean);
const Margin = 3;
var
  Text   : String;
  Item   : TLagerItem;
begin
  if Selected then
    (Sender as TDXListBox).DrawSelection(Surface,Mem,Rect,bcMaroon);
  if fItemsArray[Index].Group=upeLagerItem then
  begin
    Item:=SaveGame.LagerListe.Items[fItemsArray[Index].Index];
    SaveGame.LagerListe.DrawImage(Item.ImageIndex,Surface,Rect.Left+Margin,Rect.Top+2);
    Text:=Format(FLevel,[Item.Level]);
    YellowStdFont.Draw(Surface,Rect.Right-8-YellowStdFont.TextWidth(Text),Rect.Top+10,Text);
    if Item.Land<>-1 then
    begin
      SaveGame.Organisations.DrawSmallOrgan(Surface,Rect.Right-78,Rect.Top+11,Item.Land);
    end;
  end
  else if fItemsArray[Index].Group=upeEinrichtung then
    SaveGame.BasisListe.DrawIcon(Surface,Rect.Left+Margin,Rect.Top+2)
  else if fItemsArray[Index].Group=upeUFO then
    SaveGame.UFOListe.DrawIcon(Surface,Rect.Left+Margin,Rect.Top+2,SaveGame.UFOListe.Models[fItemsArray[Index].Index].Image)
  else if fItemsArray[Index].Group=upeRaumschiff then
    SaveGame.Raumschiffe.DrawIcon(Surface,Rect.Left+Margin,Rect.Top+2,fItemsArray[Index].Image)
  else if fItemsArray[Index].Group=upeAlien then
    SaveGame.AlienList.DrawImage(SaveGame.AlienList[fItemsArray[Index].Index].Image,Surface,Rect.Left+Margin,Rect.Top+2);
  YellowStdFont.Draw(Surface,Rect.Left+Margin+36,Rect.Top+10,fItemsArray[Index].Name);
end;

procedure TInfoUfoPaedie.SetInfoText(Text: String);
begin
  if Text=EmptyStr then
    InfoText.Text:=ST0310070006
  else
    InfoText.Text:=Text;
end;

procedure TInfoUfoPaedie.DrawImage(Sender: TDXComponent;Surface: TDirectDrawSurface;
  var Mem: TDDSurfaceDesc);
var
  SWidth, SHeight: Integer;
  MaxSize : Integer;
begin
  BlendRectangle(ResizeRect(ImageCanvas.ClientRect,1),100,bcMaroon,Surface,Mem);
  Rectangle(Surface,Mem,ImageCanvas.ClientRect,bcMaroon);
  if fImage<>nil then
  begin
    if (fImage.Width<150) or (fImage.Height<150) then
    begin
      Surface.Draw(ImageCanvas.Left+115-(fImage.Width div 2),ImageCanvas.Top+80-(fImage.Height div 2),fImage);
    end
    else if (fImage.Width=150) and (fImage.Height=150) then
      Surface.Draw(ImageCanvas.Left+40,ImageCanvas.Top+5,fImage)
    else
    begin
      MaxSize:=max(fImage.Width,fImage.Height);
      SWidth:=round(fImage.Width/MaxSize*150);
      SHeight:=round(fImage.Height/MaxSize*150);
      Surface.StretchDraw(Bounds(ImageCanvas.Left+115-(SWidth div 2),ImageCanvas.Top+80-(SHeight div 2),SWidth,SHeight),fImage);
    end;
  end;
end;

function TInfoUfoPaedie.FindKategorie(Group: TUFOPadieEntry;
  TypeID: TProjektType): Integer;
var
  Dummy: Integer;
begin
  result:=-1;
  for Dummy:=0 to high(fCaptions) do
  begin
    if fCaptions[Dummy].Group=Group then
    begin
      if Group=upeLagerItem then
      begin
        if fCaptions[Dummy].TypeID=TypeID then
        begin
          result:=Dummy;
          exit;
	end;
      end
      else
      begin
        result:=Dummy;
        exit;
      end;
    end;
  end;
end;

procedure TInfoUfoPaedie.CreateRelatedItems;
var
  Dummy  : Integer;
  WaffID : Cardinal;

  procedure CreateRelatedButton(RelGroup: TUFOPadieEntry; RelIndex: Integer; RelName: String);
  begin
    // Pr�fen, ob diese Ausr�stung �berhaupt angezeigt werden darf
    if (RelGroup=upeLagerItem) and not (lager_api_ItemVisible(Savegame.LagerListe[RelIndex])) then
      exit;

    SetLength(fRelatedItems,length(fRelatedItems)+1);
    with fRelatedItems[high(fRelatedItems)] do
    begin
      Group:=RelGroup;
      Index:=RelIndex;
      Button:=TDXBitmapButton.Create(Self);
      with Button do
      begin
        SetButtonFont(Font);
        SetRect(0,InfoText.Bottom+1,YellowStdFont.TextWidth(RelName)+20,28);
        Text:=RelName;
        Visible:=false;
        Tag:=high(fRelatedItems);
        FirstColor:=coFirstColor;
        SecondColor:=coSecondColor;
        AccerlateColor:=coAccerlateColor;
        OnClick:=ClickRelatedItem;
      end;
    end;
    if Container.ActivePage=Self then
      Container.AddComponent(fRelatedItems[high(fRelatedItems)].Button);
  end;

begin
  {$IFDEF CRASHTEST}
  Free;
  {$ENDIF}
  if not fDontRefreshRel then
  begin
    for Dummy:=0 to high(fRelatedItems) do
      fRelatedItems[Dummy].Button.Free;

    SetLength(fRelatedItems,0);

    if fItemsArray[fEntry].Group=upeLagerItem then
    begin
      case SaveGame.LagerListe[fItemsArray[fEntry].Index].TypeID of
        ptWaffe,ptRWaffe:
        begin
          CreateRelatedButton(fItemsArray[fEntry].Group,fItemsArray[fEntry].Index,fItemsArray[fEntry].Name);
          WaffID:=SaveGame.LagerListe[fItemsArray[fEntry].Index].ID;
        end;
        ptMunition,ptRMunition:
        begin
          WaffID:=SaveGame.LagerListe[fItemsArray[fEntry].Index].MunFor;

          // Waffe zur Munition suchen
          for Dummy:=0 to SaveGame.LagerListe.Count-1 do
          begin
            if SaveGame.LagerListe[Dummy].TypeID in [ptWaffe,ptRWaffe] then
            begin
              if SaveGame.LagerListe[Dummy].ID=WaffID then
              begin
                CreateRelatedButton(upeLagerItem,Dummy,SaveGame.LagerListe[Dummy].Name);
              end;
            end;
          end;
        end;
        else
        begin
          WaffID:=0;
          CreateRelatedButton(fItemsArray[fEntry].Group,fItemsArray[fEntry].Index,fItemsArray[fEntry].Name);
        end;
      end;

      if WaffID<>0 then
      begin
        for Dummy:=0 to SaveGame.LagerListe.Count-1 do
        begin
          if SaveGame.LagerListe[Dummy].TypeID in [ptMunition,ptRMunition] then
          begin
            if SaveGame.LagerListe[Dummy].Munfor=WaffID then
            begin
              CreateRelatedButton(upeLagerItem,Dummy,SaveGame.LagerListe[Dummy].Name);
            end;
          end;
        end;
      end;
    end
    else
      CreateRelatedButton(fItemsArray[fEntry].Group,fItemsArray[fEntry].Index, fItemsArray[fEntry].Name);
  end;

  // Aktuellen Eintrag hervorheben
  for Dummy:=0 to high(fRelatedItems) do
  begin
    fRelatedItems[Dummy].Button.HighLight:=(fRelatedItems[Dummy].Group=fItemsArray[fEntry].Group) and (fRelatedItems[Dummy].Index=fItemsArray[fEntry].Index);
  end;

  ShowRelatedButtons(0);
end;

procedure TInfoUfoPaedie.ShowRelatedButtons(Beginn: Integer);
var
  Dummy  : Integer;
  Last   : Integer;
  Pos    : Integer;
  Corner : TRoundCorners;
begin
  if fDontRefreshRel then
    exit;
    
  Beginn:=min(max(Beginn,0),high(fRelatedItems));
  Container.Lock;
  RelNext.Enabled:=false;
  // Alle Buttons ausblenden
  for Dummy:=0 to high(fRelatedItems) do
    fRelatedItems[Dummy].Button.Visible:=false;

  Pos:=InfoText.Left;
  Corner:=rcLeftBottom;
  Last:=-1;
  for Dummy:=Beginn to high(fRelatedItems) do
  begin
    with fRelatedItems[Dummy].Button do
    begin
      if Pos>ItemInfo.Right-60-Width then
      begin
        RelNext.Enabled:=true;
        break;
      end;
      Left:=Pos;
      RoundCorners:=Corner;
      Visible:=true;
      inc(Pos,Width+1);
      Last:=Dummy;
    end;
    Corner:=rcNone;
  end;
  fRelatedStart:=Beginn;
  if Last<>-1 then
  begin
    with fRelatedItems[Last] do
    begin
      if Button.RoundCorners=rcNone then
        Button.RoundCorners:=rcRightBottom
      else
        Button.RoundCorners:=rcBottom;
    end;
  end;
  RelPrev.Enabled:=Beginn>0;
  Container.Unlock;
  Redraw(Container.Surface);
end;

procedure TInfoUfoPaedie.ClickRelatedItem(Sender: TObject);
var
  Index      : Integer;
begin
  if (Sender as TDXBitmapButton).Highlight then
    exit;

  Container.Lock;
  Index:=(Sender as TDXBitmapButton).Tag;
  fDontRefreshRel:=true;
  if fRelatedItems[Index].Group=upeLagerItem then
  begin
    ShowUFOPadieEntry(upeLagerItem,fRelatedItems[Index].Index);
  end;
  fDontRefreshRel:=false;
  Container.Unlock;
  Container.ReDraw(Container.Surface);
end;

procedure TInfoUfoPaedie.ClickRelNav(Sender: TObject);
begin
  ShowRelatedButtons(fRelatedStart+(Sender as TDXBitmapButton).Tag);
end;

procedure TInfoUfoPaedie.OpenRessourceBitmap(BitmapName: String);
var
  Archiv: TArchivFile;
begin
  Archiv:=archiv_utils_OpenSpecialRessource(BitmapName,SaveGame.GameSet.FileName);

  // Kein Bild angegeben, oder Ressource existiert nicht
  if Archiv=nil then
  begin
    if fImage<>nil then
    begin
      fImage.Free;
      fImage:=nil;
    end;
    exit;
  end;

  if fImage=nil then
    fImage:=TDirectDrawSurface.Create(Container.DDraw);

  fImage.LoadFromStream(Archiv.Stream);
  fImage.TransparentColor:=fImage.ColorMatch(clFuchsia);
  fImage.Restore;

  Archiv.Free;
end;

procedure TInfoUfoPaedie.ShowIndexInKat(Index: Integer);
var
  Dummy: Integer;
begin
  for Dummy:=0 to high(fItemsArray) do
  begin
    if fItemsArray[Dummy].Index=Index then
    begin
      ShowEntry(Dummy);
      exit;
    end;
  end;
end;

procedure TInfoUfoPaedie.ClickChangeItem(Sender: TObject);
begin
  if (Sender as TDXBitmapButton).Tag=1 then
    fEntry:=(fEntry+1) mod length(fItemsArray)
  else
  begin
    dec(fEntry);
    if fEntry<0 then
      fEntry:=high(fItemsArray);
  end;

  ShowEntry(fEntry);
end;

procedure TInfoUfoPaedie.ClickChangeKat(Sender: TObject);
begin
  repeat
    if (Sender as TDXBitmapButton).Tag=1 then
      fKategorie:=(fKategorie+1) mod length(fCaptions)
    else
    begin
      dec(fKategorie);
      if fKategorie<0 then
        fKategorie:=high(fCaptions);
    end;

  until ShowKategorie(fKategorie);
end;

function TInfoUfoPaedie.NewEntry: PPadieEntry;
begin
  SetLength(fItemsArray,length(fItemsArray)+1);
  result:=Addr(fItemsArray[high(fItemsArray)]);
end;

procedure TInfoUfoPaedie.ShowUFOPadieEntry(PadieEntry: TUFOPadieEntry;
  Index: Integer);
var
  Kategorie : Integer;
begin
  if PadieEntry=upeLagerItem then
    Kategorie:=FindKategorie(PadieEntry,SaveGame.LagerListe[Index].TypeID)
  else
    Kategorie:=FindKategorie(PadieEntry);
  Container.Lock;
  ShowKategorie(Kategorie);
  Container.Unlock;

  ShowIndexInKat(Index);

  fFirst:=false;
end;

procedure TInfoUfoPaedie.SetDefaults;
begin
  inherited;

  TDXPageNavigator(fNavigator).Visible:=UseNavigator;

  UseNavigator:=true;

  if fFirst then
  begin
    ShowKategorie(0);
    ShowEntry(0);
  end
  else
  begin
    ShowKategorie(fKategorie);
    ShowEntry(fEntry);
  end;
end;

procedure TInfoUfoPaedie.DontAllowChangePageViaNavigator;
begin
  // In SetDefaults wird der Navigator ausgeblendet und UseNavigator wieder
  // auf true gesetzt

  UseNavigator:=false;
end;

end.
