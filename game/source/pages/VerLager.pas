{********************************************************************************
*										*
* Copyright (C) 2004  X-Force Team - jim_raynor@web.de				*
*										*
* This file is part of X-Force: Fight For Destiny (X-Force).			*
*										*
* X-Force is free software; you can redistribute it and/or modify		*
* it under the terms of the GNU General Public License as published by		*
* the Free Software Foundation; either version 2 of the License, or		*
* (at your option) any later version.						*
*										*
* X-Force is distributed in the hope that it will be useful,			*
* but WITHOUT ANY WARRANTY; without even the implied warranty of		*
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the			*
* GNU General Public License for more details.					*
*										*
* You should have received a copy of the GNU General Public License		*
* along with X-Force; if not, write to the Free Software			*
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA	*
*										*
*********************************************************************************
*										*
* Verwaltungsbildschirm Lager							*
*										*
********************************************************************************}

{$I ../Settings.inc}

unit VerLager;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  DXContainer, KD4Page, Defines, DXBitmapButton, DXLabel,
  DirectFont, DXListBox, DXItemInfo, XForce_types, DXDraws, DXScrollBar, DXInfoFrame,
  DirectDraw, Blending, KD4Utils, DXFrameBox, DXEdit, math, TraceFile, Menus,
  DXGroupCaption, DialogTransfer, DXListBoxWindow, AuftragWatch, StringConst;

const
  coFirstColor  : TColor = clNavy;
  coSecondColor : TColor = clBlue;
  coScrollPen   : TColor = clNavy;
  coScrollBrush : TColor = clNavy;

type
  TLagerState = (lsNone,lsBuy,lsSell);

  TVerLager = class(TKD4Page)
  private
    fAreaRect  : TRect;
    fState     : TLagerState;
    fFilter    : TItemFilter;
    fItemsCou  : Integer;
    procedure ChangeCount(Sender: TObject);

    procedure DrawItems(Sender: TDXComponent;Surface: TDirectDrawSurface;const Mem: TDDSurfaceDesc;Rect: TRect; Index: Integer;Selected: boolean);
    procedure DrawAngebot(Sender: TDXComponent;Surface: TDirectDrawSurface;const Mem: TDDSurfaceDesc;Rect: TRect; Index: Integer;Selected: boolean);

    procedure ShowPadie(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
    procedure OnChangeItem(Sender: TObject);

    procedure StartAuftrag(Sender: TObject);
    procedure AngebotButtonClick(Sender: TObject);
    procedure RecycleButtonClick(Sender: TObject);

    procedure ChangeFilter(Sender: TObject);
    procedure SchnellSuche(Sender: TObject);
    procedure DrawAnzahlHeader(Sender: TDXComponent;Surface: TDirectDrawSurface;Rect: TRect);
    procedure DrawHeadRow(Sender: TDXComponent;Surface: TDirectDrawSurface;Rect: TRect);
    procedure OnSelectedBasisChange(Sender: TObject);
    procedure ListBoxKeyPress(Sender: TObject; var Key: Word; Shift: TShiftState);

    procedure ClickTransferButton(Sender: TObject);

    procedure WatchButtonClick(Sender: TObject);
    procedure StornoButtonClick(Sender: TObject);
    { Private-Deklarationen }
  protected
    { Protected-Deklarationen }
  public
    WerkButton     : TDXBitmapButton;
    InfoFrame      : TDXInfoFrame;
    Items          : TDXListBox;
    ItemInfo       : TDXItemInfo;
    CountBar       : TDXScrollBar;
    CountHeader    : TDXGroupCaption;
    SSLabel        : TDXGroupCaption;
    SSuche         : TDXEdit;
    SAnzahl        : TDXEdit;

    AngebotDialog  : TDXListBoxWindow;
    AuftragWatch   : TAuftragWatch;

    BuyButton      : TDXBitmapButton;
    SellButton     : TDXBitmapButton;
    RecyclenButton : TDXBitmapButton;
    StornoButton   : TDXBitmapButton;
    AngebotButton  : TDXBitmapButton;
    WatchButton    : TDXBitmapButton;

    FAllButton     : TDXBitmapButton;
    FWafButton     : TDXBitmapButton;
    FGraButton     : TDXBitmapButton;
    FMinButton     : TDXBitmapButton;
    FSenButton     : TDXBitmapButton;
    FMotButton     : TDXBitmapButton;
    FPanButton     : TDXBitmapButton;
    TransferButton : TDXBitmapButton;
    TransferDialog : TDialogTransfer;
    constructor Create(Container: TDXContainer);override;
    destructor Destroy;override;
    procedure SetDefaults;override;
  end;

implementation

uses
  basis_api, lager_api, raumschiff_api, game_api, ufopaedie_api, ui_utils,
  game_utils;

{ TVerLager }

procedure TVerLager.AngebotButtonClick(Sender: TObject);
var
  Index    : Integer;
  Dummy    : Integer;
  AufIndex : Integer;
  Auftrag  : TAuftrag;
begin
  Index:=StrToInt(Items.Items[Items.ItemIndex]);

  AufIndex:=SaveGame.LagerListe.FindAuftrag(Index);
  Assert(AufIndex<>-1);

  Auftrag:=SaveGame.LagerListe.Auftrag[AufIndex];

  AngebotDialog.Items.Clear;
  for Dummy:=0 to SaveGame.LagerListe.AngebotCount-1 do
  begin
    if Auftrag.ID=SaveGame.LagerListe.Angebote[Dummy].ID then
      AngebotDialog.Items.Add(IntToStr(Dummy));
  end;

  Index:=AngebotDialog.Show;
  if Index<>-1 then
  begin
    SaveGame.LagerListe.AcceptAngebot(StrToInt(AngebotDialog.Items[Index]));
    OnChangeItem(Self);
    Items.Redraw;
  end;
end;

procedure TVerLager.ChangeCount(Sender: TObject);
var
  CanBuySell   : boolean;
begin
  CanBuySell:=((Items.Items.Count>0) and (Items.Items[Items.ItemIndex][1]<>'C'));
  if (CountBar.Value>0) and CanBuySell then
  begin
    BuyButton.Enabled:=true;
    SellButton.Enabled:=false;
    fState:=lsBuy;
  end
  else if (CountBar.Value<0) and CanBuySell then
  begin
    BuyButton.Enabled:=false;
    SellButton.Enabled:=true;
    fState:=lsSell;
  end
  else
  begin
    BuyButton.Enabled:=false;
    SellButton.Enabled:=false;
    fState:=lsNone;
  end;

  RecyclenButton.Enabled := SellButton.Enabled;

  Container.RedrawArea(CountHeader.ClientRect,Container.Surface);
end;

procedure TVerLager.ChangeFilter(Sender: TObject);
var
  Send: TDXBitmapButton;
begin
  Send:=(Sender as TDXBitmapButton);
  if Send.HighLight then exit;
  Container.Lock;
  Items.ItemIndex:=0;
  FAllButton.HighLight:=false;
  FWafButton.HighLight:=false;
  FGraButton.HighLight:=false;
  FMinButton.HighLight:=false;
  FSenButton.HighLight:=false;
  FMotButton.HighLight:=false;
  FPanButton.HighLight:=false;
  Send.HighLight:=true;
  if Send=FAllButton then
    fFilter:=ifAll
  else if Send=FWafButton then
    fFilter:=ifWaffen
  else if Send=FGraButton then
    fFilter:=ifGranate
  else if Send=FMinButton then
    fFilter:=ifMine
  else if Send=FSenButton then
    fFilter:=ifSensoren
  else if Send=FMotButton then
    fFilter:=ifMotoren
  else if Send=FPanButton then
    fFilter:=ifPanzerung;

  fItemsCou:=ui_utils_FillItems(Items,SaveGame.LagerListe,fFilter,SSuche.Text,SAnzahl.Text);

  OnChangeItem(Self);
  Container.LoadGame(false);
  Container.DecLock;
  Container.RedrawArea(fAreaRect,Container.Surface);
end;

procedure TVerLager.ClickTransferButton(Sender: TObject);
var
  Item: TLagerItem;
begin
  Item:=lager_api_GetItem(StrToInt(Items.Items[Items.ItemIndex]))^;
  TransferDialog.Item:=Item;
  if TransferDialog.ShowModal=mrOK then
  begin
    Assert(lager_api_DeleteItem(basis_api_GetSelectedBasis.ID,Item.ID,TransferDialog.ItemCount));
    Item.Anzahl:=TransferDialog.ItemCount;
    raumschiff_api_DoTransport(basis_api_GetSelectedBasis.BasisPos,TransferDialog.Basis.BasisPos,Item,0,true);

    // Damit alles neu gezeichnet wird
    Container.IncLock;
    OnChangeItem(Self);
    Items.Redraw;
    Container.DecLock;
  end;
end;

constructor TVerLager.Create(Container: TDXContainer);
begin
  inherited;
  Animation:=paUpToBottom;
  HintLabel.BlendColor:=bcBlue;
  HintLabel.BorderColor:=coFirstColor;

  SetPageInfo(StripHotKey(BLager));

  { WerkStattButton }
  WerkButton:=TDXBitmapButton.Create(Self);
  with WerkButton do
  begin
    SetRect(682,530,110,28);
    FirstColor:=coFirstColor;
    SecondColor:=coSecondColor;
    AccerlateColor:=coAccerlateColor;
    PageIndex:=PageWerkStatt;
    SetButtonFont(Font);
    BlendColor:=bcBlue;
    RoundCorners:=rcAll;
    Text:=BWerkStatt;
    Hint:=HWerkstatt;
  end;

{ Schnellsuche }
  { Label }
  SSLabel:=TDXGroupCaption.Create(Self);
  with SSLabel do
  begin
    SetFont(Font);
    SetRect(8,54,95,23);
    RoundCorners:=rcLeftTop;
    TopMargin:=5;
    BlendColor:=bcBlue;
    BorderColor:=coFirstColor;
    Caption:=BSSuche;
  end;

  { Eingabebox }
  SSuche:=TDXEdit.Create(Self);
  with SSuche do
  begin
    SetButtonFont(Font);
    BorderColor:=coFirstColor;
    Hint:=HSSuche;
    BlendColor:=bcBlue;
    RoundCorners:=rcRightTop;
    SetRect(104,54,274,23);
    FocusColor:=coSecondColor;
    LeftMargin:=5;
    TopMargin:=5;
    OnChange:=SchnellSuche;
  end;

{ Anzahlfilter }
  { Label }
  with TDXGroupCaption.Create(Self) do
  begin
    SetFont(Font);
    SetRect(8,78,95,23);
    RoundCorners:=rcNone;
    TopMargin:=5;
    BlendColor:=bcBlue;
    BorderColor:=coFirstColor;
    Caption:=IACount;
  end;

  { Eingabebox }
  SAnzahl:=TDXEdit.Create(Self);
  with SAnzahl do
  begin
    SetButtonFont(Font);
    BorderColor:=coFirstColor;
    Hint:=ST0502070001;
    BlendColor:=bcBlue;
    RoundCorners:=rcNone;
    SetRect(104,78,274,23);
    FocusColor:=coSecondColor;
    LeftMargin:=5;                                                            
    TopMargin:=5;
    OnChange:=SchnellSuche;
  end;

{ Gegenstände }
  { Gegenstandslistbox }
  Items:=TDXListBox.Create(Self);
  with Items do
  begin
    SetRect(8,102,370,354);
    RoundCorners:=rcNone;
    BorderColor:=coFirstColor;
    FocusColor:=coSecondColor;
    SetFont(Font);
    SetButtonFont(ScrollBarFont);
    FirstColor:=bcBlue;
    SecondColor:=coScrollPen;
    Hint:=HItemList;
    ItemHeight:=40;
    OnChange:=OnChangeItem;
    OwnerDraw:=True;
    OnDrawItem:=DrawItems;
    OnMouseUp:=ShowPadie;
    HeadRowHeight:=17;
    HeadBlendColor:=bcBlue;
    OnDrawHeadRow:=DrawHeadRow;
    OnKeyDown:=ListBoxKeyPress;
  end;

  { Gegenstandsinformationen }
  ItemInfo:=TDXItemInfo.Create(Self);
  with ItemInfo do
  begin
    SetFont(Font);
    BorderColor:=coFirstColor;
    SetRect(508,54,284,469);
    BlendColor:=bcBlue;
    Caption:=LItem;
    Infos:=[inPreis,inAnzahl,inLager,inStrength,inPanzerung,inRInfos,inMotor];
    Hint:=HItemInfo;
    InfoType:=itLagerItem;
  end;

{ Informationsframe }
  InfoFrame:=TDXInfoFrame.Create(Self);
  with InfoFrame do
  begin
    SetRect(388,10,244,70);
    ColorSheme:=icsBlue;
    SetFont(Font);
    Infos:=[ifCredits,ifLager];
    SystemKeyChange:=true;
  end;

{ Anzahlregelung }

  { FrameBox }
  CountHeader:=TDXGroupCaption.Create(Self);
  with CountHeader do
  begin
    SetRect(8,457,370,19);
    SetFont(Font);
    BlendColor:=bcBlue;
    RoundCorners:=rcNone;
    BorderColor:=coFirstColor;
    DrawCaption:=DrawAnzahlHeader;
  end;

  { ScrollBar }
  CountBar:=TDXScrollBar.Create(Self);
  with CountBar do
  begin
    Kind:=sbHorizontal;
    SetRect(8,477,370,16);
    SetButtonFont(Font);
    FirstColor:=bcBlue;
    SecondColor:=coScrollPen;
    RoundCorners:=rcNone;
    max:=100;
    min:=0;
    LargeChange:=10;
    Hint:=HKaufBar;
    OnChange:=ChangeCount;
  end;

  { Verkaufen Button }
  SellButton:=TDXBitmapButton.Create(Self);
  with SellButton do
  begin
    SetRect(8,495,73,28);
    FirstColor:=coFirstColor;
    OnClick:=StartAuftrag;
    SecondColor:=coSecondColor;
    BlendColor:=bcBlue;
    AccerlateColor:=coAccerlateColor;
    RoundCorners:=rcLeftBottom;
    Text:=BSell;
    SetButtonFont(Font);
  end;

  { Recyclen Button }
  RecyclenButton:=TDXBitmapButton.Create(Self);
  with RecyclenButton do
  begin
    SetRect(82,495,73,28);
    FirstColor:=coFirstColor;
    OnClick:=RecycleButtonClick;
    SecondColor:=coSecondColor;
    BlendColor:=bcBlue;
    AccerlateColor:=coAccerlateColor;
    RoundCorners:=rcNone;
    Text:=BRecycle;
    SetButtonFont(Font);
  end;

  { Angebtoe Button }
  AngebotButton:=TDXBitmapButton.Create(Self);
  with AngebotButton do
  begin
    SetRect(156,495,73,28);
    FirstColor:=coFirstColor;
    OnClick:=AngebotButtonClick;
    SecondColor:=coSecondColor;
    BlendColor:=bcBlue;
    AccerlateColor:=coAccerlateColor;
    RoundCorners:=rcNone;
    Text:=ST0502080005;
    SetButtonFont(Font);
  end;

  { Stornieren Button }
  StornoButton:=TDXBitmapButton.Create(Self);
  with StornoButton do
  begin
    SetRect(230,495,73,28);
    FirstColor:=coFirstColor;
    OnClick:=StornoButtonClick;
    SecondColor:=coSecondColor;
    BlendColor:=bcBlue;
    AccerlateColor:=coAccerlateColor;
    RoundCorners:=rcNone;
    Text:=LACancel;
    SetButtonFont(Font);
    Hint:=HStonieren;
  end;

  { Kaufen Button }
  BuyButton:=TDXBitmapButton.Create(Self);
  with BuyButton do
  begin
    SetRect(304,495,73,28);
    FirstColor:=coFirstColor;
    OnClick:=StartAuftrag;
    SecondColor:=coSecondColor;
    BlendColor:=bcBlue;
    AccerlateColor:=coAccerlateColor;
    RoundCorners:=rcRightBottom;
    Text:=BBuy;
    SetButtonFont(Font);
  end;

  { Überwachung Button }
  WatchButton:=TDXBitmapButton.Create(Self);
  with WatchButton do
  begin
    SetRect(388,495,110,28);
    FirstColor:=coFirstColor;
    OnClick:=WatchButtonClick;
    SecondColor:=coSecondColor;
    BlendColor:=bcBlue;
    AccerlateColor:=coAccerlateColor;
    Text:=ST0309270021;
    SetButtonFont(Font);
    Hint:=ST0309270020;
  end;

  { Alle Kategorien Button }
  FAllButton:=TDXBitmapButton.Create(Self);
  with FAllButton do
  begin
    BlendColor:=bcBlue;
    SetRect(388,54,110,28);
    FirstColor:=coFirstColor;
    SecondColor:=coSecondColor;
    AccerlateColor:=coAccerlateColor;
    SetButtonFont(Font);
    HighLight:=true;
    RoundCorners:=rcTop;
    OnClick:=ChangeFilter;
    Hint:=HFAll;
    Text:=BFAll;
  end;

  { Waffen Button }
  FWafButton:=TDXBitmapButton.Create(Self);
  with FWafButton do
  begin
    SetRect(388,83,110,28);
    FirstColor:=coFirstColor;
    SecondColor:=coSecondColor;
    BlendColor:=bcBlue;
    AccerlateColor:=coAccerlateColor;
    SetButtonFont(Font);
    HighLight:=false;
    OnClick:=ChangeFilter;
    RoundCorners:=rcNone;
    Hint:=HFWaf;
    Text:=BFWaf;
  end;

  { Granate Button }
  FGraButton:=TDXBitmapButton.Create(Self);
  with FGraButton do
  begin
    SetRect(388,112,110,28);
    FirstColor:=coFirstColor;
    BlendColor:=bcBlue;
    SecondColor:=coSecondColor;
    AccerlateColor:=coAccerlateColor;
    SetButtonFont(Font);
    HighLight:=false;
    RoundCorners:=rcNone;
    OnClick:=ChangeFilter;
    Hint:=HFGra;
    Text:=BFGra;
  end;

  { Minen Button }
  FMinButton:=TDXBitmapButton.Create(Self);
  with FMinButton do
  begin
    SetRect(388,141,110,28);
    FirstColor:=coFirstColor;
    SecondColor:=coSecondColor;
    AccerlateColor:=coAccerlateColor;
    BlendColor:=bcBlue;
    SetButtonFont(Font);
    RoundCorners:=rcNone;
    HighLight:=false;
    OnClick:=ChangeFilter;
    Hint:=HFMin;
    Text:=BFMin;
  end;

  { Sensoren Button }
  FSenButton:=TDXBitmapButton.Create(Self);
  with FSenButton do
  begin
    SetRect(388,170,110,28);
    FirstColor:=coFirstColor;
    SecondColor:=coSecondColor;
    AccerlateColor:=coAccerlateColor;
    SetButtonFont(Font);
    BlendColor:=bcBlue;
    HighLight:=false;
    RoundCorners:=rcNone;
    OnClick:=ChangeFilter;
    Hint:=HFSen;
    Text:=BFSen;
  end;

  { Motoren Button }
  FMotButton:=TDXBitmapButton.Create(Self);
  with FMotButton do
  begin
    SetRect(388,199,110,28);
    FirstColor:=coFirstColor;
    SecondColor:=coSecondColor;
    AccerlateColor:=coAccerlateColor;
    SetButtonFont(Font);
    HighLight:=false;
    BlendColor:=bcBlue;
    OnClick:=ChangeFilter;
    Hint:=HFMot;
    RoundCorners:=rcNone;
    Text:=BFMot;
  end;

  { Panzerung Button }
  FPanButton:=TDXBitmapButton.Create(Self);
  with FPanButton do
  begin
    SetRect(388,228,110,28);
    FirstColor:=coFirstColor;
    SecondColor:=coSecondColor;
    AccerlateColor:=coAccerlateColor;
    RoundCorners:=rcBottom;
    BlendColor:=bcBlue;
    SetButtonFont(Font);
    HighLight:=false;
    OnClick:=ChangeFilter;
    Hint:=HFPan;
    Text:=BFPan;
  end;

  { Transfer Button }
  TransferButton:=TDXBitmapButton.Create(Self);
  with TransferButton do
  begin
    SetRect(388,263,110,28);
    FirstColor:=coFirstColor;
    SecondColor:=coSecondColor;
    AccerlateColor:=coAccerlateColor;
    RoundCorners:=rcAll;
    BlendColor:=bcBlue;
    SetButtonFont(Font);
    HighLight:=false;
    OnClick:=ClickTransferButton;
    Text:=ST0403260001;
    Hint:=ST0403260002;
  end;

  { Listbox Fenster zur Anzeige der Angebote }
  AngebotDialog:=TDXListBoxWindow.Create(Self);
  with AngebotDialog do
  begin
    SetRect(0,0,400,200);
    Caption:=ST0309210008;
    SetButtonFont(Font);
    CaptionColor:=coFontColor;
    AccerlateColor:=coAccerlateColor;
    Font.Size:=coFontSize;
    FirstColor:=$00400000;
    SecondColor:=clBlue;
    BlendColor:=bcDarkNavy;
    SetOwnerDraw(55,5,DrawAngebot);
  end;

  TransferDialog:=TDialogTransfer.Create(Self);
  SetButtonFont(TransferDialog.Font);

  AuftragWatch:=TAuftragWatch.Create(Self);
  SetButtonFont(AuftragWatch.Font);

  UnionRect(fAreaRect,ItemInfo.ClientRect,Items.ClientRect);

  basis_api_RegisterChangeBaseHandler(OnSelectedBasisChange);
end;

destructor TVerLager.Destroy;
begin
  TransferDialog.Free;
  AuftragWatch.Free;
  inherited;
end;

procedure TVerLager.DrawAngebot(Sender: TDXComponent;
  Surface: TDirectDrawSurface; const Mem: TDDSurfaceDesc; Rect: TRect;
  Index: Integer; Selected: boolean);
var
  Annr    : Integer;
  Organ   : Integer;
  TempAn  : TAngebot;
  Auftrag : TAuftrag;
  Text    : String;
  Item    : PLagerItem;
  Font    : TDirectFont;

  ListBox : TDXListBox;
begin
  ListBox:=(Sender as TDXListBOx);
  Annr:=StrToInt(AngebotDialog.Items[Index]);
  TempAn:=SaveGame.LagerListe.Angebote[Annr];
  Organ:=SaveGame.LagerListe.Angebote[Annr].Organisation;

  if Selected then
    ListBox.DrawSelection(Surface,Mem,Rect,bcBlue);
    
  YellowStdFont.Draw(Surface,Rect.Left+51,Rect.Top+9,SaveGame.Organisations[Organ].Name);
  SaveGame.Organisations.DrawOrgan(Surface,Rect.Left+6,Rect.Top+6,Organ);
  Auftrag:=SaveGame.LagerListe.Auftrag[SaveGame.LagerListe.AuftragOfID(TempAn.ID)];
  Item:=SaveGame.LagerListe.AddrLagerItem(Auftrag.Item);
  WhiteBStdFont.Draw(Surface,Rect.Left+6,Rect.Top+33,IntToStr(TempAn.Anzahl));
  WhiteStdFont.Draw(Surface,Rect.Left+31,Rect.Top+33,Format('* '+FCredits,[TempAn.Preis/1]));

  if Auftrag.Kauf then
  begin
    Text:=Format(ST0309270022,[TempAn.Preis*100/Item.Kaufpreis]);
    if TempAn.Preis<Item.KaufPreis then
      Font:=LimeBStdFont
    else
      Font:=RedBStdFont;
  end
  else
  begin
    Text:=Format(ST0309270022,[TempAn.Preis*100/Item.VerKaufpreis]);
    if TempAn.Preis>Item.VerkaufPreis then
      Font:=LimeBStdFont
    else
      Font:=RedBStdFont;
  end;
  Font.Draw(Surface,Rect.Right-6-Font.TextWidth(Text),Rect.Top+33,Text);

  WhiteStdFont.Draw(Surface,Rect.Left+111,Rect.Top+33,Format('= '+FCredits,[(TempAn.Preis*TempAn.Anzahl)/1]));

  Text:=ZahlString(LARound,LARounds,TempAn.Rounds);
  YellowStdFont.Draw(Surface,Rect.Right-YellowStdFont.TextWidth(Text)-6,Rect.Top+9,Text);
//  HLine(Surface,Mem,Rect.Left,Rect.Right,Rect.Top,bcNavy);
end;

procedure TVerLager.DrawAnzahlHeader(Sender: TDXComponent;
  Surface: TDirectDrawSurface; Rect: TRect);
var
  Text   : String;
begin
  WhiteBStdFont.Draw(Surface,Rect.Left+6,Rect.Top+3,ST0502080001);
  if CountBar.Value<0 then
    Text:=Format(FVerKauf,[-CountBar.Value])
  else if CountBar.Value>0 then
    Text:=Format(FKauf,[CountBar.Value]);
  WhiteStdFont.Draw(Surface,Rect.Right-8-WhiteStdFont.TextWidth(Text),Rect.Top+3,Text);
end;

procedure TVerLager.DrawHeadRow(Sender: TDXComponent;
  Surface: TDirectDrawSurface; Rect: TRect);
var
  Text   : String;
begin
  WhiteStdFont.Draw(Surface,Rect.Left+6,Rect.Top+1,LItems);
  Text:=ZahlString(FItems,FItems,fItemsCou);
  WhiteStdFont.Draw(Surface,Rect.Right-6-WhiteStdFont.TextWidth(Text),Rect.Top+1,Text);
end;

procedure TVerLager.DrawItems(Sender: TDXComponent;
  Surface: TDirectDrawSurface;const Mem: TDDSurfaceDesc;Rect: TRect; Index: Integer;
  Selected: boolean);
var
  ItemIndex : Integer;
  TypeID    : TProjektType;
  AufIndex  : Integer;
  Auftrag   : TAuftrag;
  Angebote  : Integer;
begin
  if Selected then
  begin
    Items.DrawSelection(Surface,Mem,Rect,bcBlue);
  end;
  if Items.Items[Index][1]='C' then
  begin
    TypeID:=TProjektType(StrToInt(Cut(Items.Items[Index],2,length(Items.Items[Index]))));
    SaveGame.LagerListe.DrawImage(TypeIDToIndex(TypeID,wtProjektil),Surface,Rect.Left+3,Rect.Top+3);
    WhiteBStdFont.Draw(Surface,Rect.Left+43,Rect.Top+3,game_utils_TypeToStr(TypeID));
  end
  else
  begin
    ItemIndex:=StrToInt(Items.Items[Index]);
    SaveGame.LagerListe.DrawImage(SaveGame.LagerListe.Items[ItemIndex].ImageIndex,Surface,Rect.Left+35,Rect.Top+3);
    YellowStdFont.Draw(Surface,Rect.Left+75,Rect.Top+3,SaveGame.LagerListe[ItemIndex].Name);
    WhiteStdFont.Draw(Surface,Rect.Left+75,Rect.Top+20,Format(FAnzahl,[SaveGame.LagerListe[ItemIndex].Anzahl]));
    if SaveGame.LagerListe.Items[ItemIndex].Land<>-1 then
    begin
      SaveGame.Organisations.DrawSmallOrgan(Surface,Rect.Left+10,Rect.Top+15,SaveGame.LagerListe.Items[ItemIndex].Land);
    end;

    AufIndex:=SaveGame.LagerListe.FindAuftrag(ItemIndex);
    if AufIndex<>-1 then
    begin
      Auftrag:=SaveGame.LagerListe.Auftrag[AufIndex];

      if Auftrag.Kauf then
        LimeBStdFont.Draw(Surface,Rect.Left+150,Rect.Top+20,Format(ST0502080002,[Auftrag.Anzahl]))
      else
        RedBStdFont.Draw(Surface,Rect.Left+150,Rect.Top+20,Format(ST0502080003,[Auftrag.Anzahl]));

      Angebote:=SaveGame.LagerListe.GetAngebotCountForAuftrag(Auftrag.ID);
      if Angebote = 0 then
        RedStdFont.Draw(Surface,Rect.Left+250,Rect.Top+20,ST0502080004)
      else
        LimeStdFont.Draw(Surface,Rect.Left+250,Rect.Top+20,ZahlString(LAAngebot,LAAngebote,Angebote));
    end;
  end;
end;

procedure TVerLager.ListBoxKeyPress(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_LEFT then
    CountBar.Value:=CountBar.Value-1
  else if Key = VK_RIGHT then
    CountBar.Value:=CountBar.Value+1
  else if Key = VK_RETURN then
  begin
    if BuyButton.Enabled then
      BuyButton.DoClick
    else
      SellButton.DoClick;
  end;
end;

procedure TVerLager.OnChangeItem(Sender: TObject);
var
  Index   : Integer;
  Auftrag : Integer;
  Angebote: Integer;
begin
  if (Items.Items.Count=0) or (Items.Items[Items.ItemIndex][1]='C') then
  begin
    Container.IncLock;
    ItemInfo.ItemValid:=false;
    BuyButton.Enabled:=false;
    SellButton.Enabled:=false;
    RecyclenButton.Enabled:=false;
    Container.DecLock;
    fState:=lsNone;
    TransferButton.Enabled:=false;
    AngebotButton.Enabled:=false;
    StornoButton.Enabled:=false;
    exit;
  end;

  Index:=StrToInt(Items.Items[Items.ItemIndex]);

  ItemInfo.Item:=SaveGame.LagerListe[Index];
  CountBar.Min:=-SaveGame.LagerListe[Index].Anzahl;

  Auftrag:=SaveGame.LagerListe.FindAuftrag(Index);

  StornoButton.Enabled:=Auftrag<>-1;

  if Auftrag=-1 then
    Angebote:=0
  else
    Angebote:=SaveGame.LagerListe.GetAngebotCountForAuftrag(SaveGame.LagerListe.Auftrag[Auftrag].ID);

  AngebotButton.Enabled:=Angebote>0;

  TransferButton.Enabled:=true;

  ChangeCount(Self);
end;

procedure TVerLager.OnSelectedBasisChange(Sender: TObject);
begin
  if Container.ActivePage<>Self then
    exit;

  Container.IncLock;
  OnChangeItem(Self);
  Items.Redraw;
  Container.DecLock;
end;

procedure TVerLager.RecycleButtonClick(Sender: TObject);
var
  Count   : Integer;
begin
  Count:=abs(CountBar.Value);
  try
    SaveGame.LagerListe.RecycleItem(StrToInt(Items.Items[Items.ItemIndex]), basis_api_GetSelectedBasis.ID, Count);
  except
    on E: Exception do
    begin
      game_api_MessageBox(E.Message,CInformation);
      exit;
    end;
  end;
  CountBar.Value:=0;
  OnChangeItem(Self);
  InfoFrame.Reset;
  Items.Redraw;
end;

procedure TVerLager.SchnellSuche(Sender: TObject);
begin
  fItemsCou:=ui_utils_FillItems(Items,SaveGame.LagerListe,fFilter,SSuche.Text,SAnzahl.Text);
  Items.OnChange(Self);
  Items.Redraw;
end;

procedure TVerLager.SetDefaults;
begin
  InfoFrame.Reset;
  FAllButton.HighLight:=false;
  SSuche.Text:='';
  ChangeFilter(FAllButton);
  OnChangeItem(Self);
  Container.FocusControl:=Items;
  fState:=lsNone;
  CountBar.Value:=0;

  TransferButton.Visible:=basis_api_GetBasisCount>1;
end;

procedure TVerLager.ShowPadie(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  if Button=mbRight then
  begin
    if (Items.Items.Count=0) or (Items.Items[Items.ItemIndex][1]='C') then exit;
    ufopaedie_api_ShowEntry(upeLagerItem,StrToInt(Items.Items[Items.ItemIndex]));
  end;
end;

procedure TVerLager.StartAuftrag(Sender: TObject);
var
  Buy   : boolean;
  Count : Integer;
begin
  Buy:=(CountBar.Value>0);
  Count:=abs(CountBar.Value);
  try
    SaveGame.LagerListe.NewAuftrag(StrToInt(Items.Items[Items.ItemIndex]),Buy,Count);
  except
    on E: EHintGUIException do
    begin
      game_api_MessageBox(E.Message,CInformation);
    end;
    on E: Exception do
    begin
      game_api_MessageBox(E.Message,CInformation);
      exit;
    end;
  end;
  CountBar.Value:=0;
  OnChangeItem(Self);
{  if Buy then
    game_api_MessageBox(Format(MBuyAuftrag,[Count,SaveGame.LagerListe[StrToInt(Items.Items[Items.ItemIndex])].Name]),CInformation)
  else
    game_api_MessageBox(Format(MSellAuftrag,[Count,SaveGame.LagerListe[StrToInt(Items.Items[Items.ItemIndex])].Name]),CInformation);}
  Items.Redraw;
end;

procedure TVerLager.StornoButtonClick(Sender: TObject);
var
  Index  : Integer;
  Auftrag: Integer;
begin
  Index:=StrToInt(Items.Items[Items.ItemIndex]);

  Auftrag:=SaveGame.LagerListe.FindAuftrag(Index);

  Assert(Auftrag<>-1);

  if SaveGame.LagerListe.GetAngebotCountForAuftrag(Savegame.LagerListe.Auftrag[Auftrag].ID)>0 then
  begin
    if not game_api_Question(ST0502110001,ST0310180002) then
      exit;
  end;

  SaveGame.LagerListe.DeleteAuftrag(Auftrag);

  Items.OnChange(Self);
  Items.Redraw;
end;

procedure TVerLager.WatchButtonClick(Sender: TObject);
begin
  AuftragWatch.Settings:=SaveGame.LagerListe.WatchSettings;
  AuftragWatch.ShowModal;
  SaveGame.LagerListe.WatchSettings:=AuftragWatch.Settings;
end;

end.
