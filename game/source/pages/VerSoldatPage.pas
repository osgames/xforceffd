{********************************************************************************
*										*
* Copyright (C) 2004  X-Force Team - jim_raynor@web.de				*
*										*
* This file is part of X-Force: Fight For Destiny (X-Force).			*
*										*
* X-Force is free software; you can redistribute it and/or modify		*
* it under the terms of the GNU General Public License as published by		*
* the Free Software Foundation; either version 2 of the License, or		*
* (at your option) any later version.						*
*										*
* X-Force is distributed in the hope that it will be useful,			*
* but WITHOUT ANY WARRANTY; without even the implied warranty of		*
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the			*
* GNU General Public License for more details.					*
*										*
* You should have received a copy of the GNU General Public License		*
* along with X-Force; if not, write to the Free Software			*
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA	*
*										*
*********************************************************************************
*										*
* Verwaltungsbildschirm Soldaten						*
*										*
********************************************************************************}

{$I ../Settings.inc}


unit VerSoldatPage;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  DXContainer, KD4Page, DXBitmapButton, Defines, DXListBox, DXLabel, DXDraws,
  DXInfoFrame, DXItemInfo, Blending, KD4Utils, XForce_types, DirectDraw, DirectFont,
  VerSoldatEinsatz, WatchMarktDialog, TraceFile, Menus, StringConst;

{ �berladene Konstanten }
const
  coFirstColor  : TColor = clGreen;
  coSecondColor : TColor = clLime;
  coScrollPen   : TColor = clGreen;

type
  TVerSoldatPage = class(TKD4Page)
  private
    fKauf            : TRect;
    fKaufSelected    : Boolean;
    { Private-Deklarationen }
  protected
    function GetSelectedSoldat: Integer;
    procedure RebuildSoldatenList;
    procedure SetTrainButton(InTraining: boolean);
    procedure CheckAllTraining;
    procedure TrainSoldat(Sender: TObject);
    procedure AusrustSoldat(Sender: TObject);
    procedure TrainAllSoldat(Sender: TObject);
    procedure RenameSoldat(Sender: TObject);
    procedure SoldatChange(Sender: TObject);
    procedure KaufChange(Sender: TObject);
    procedure BuySoldat(Sender: TObject);
    procedure SellSoldat(Sender: TObject);
    procedure WatchSoldatMarkt(Sender: TObject);
    procedure OnBasisChange(Sender: TObject);
    procedure DrawSelfSoldat(Sender: TDXComponent;Surface: TDirectDrawSurface;const Mem: TDDSurfaceDesc;Rect: TRect; Index: Integer;Selected: boolean);
    procedure DrawKaufSoldat(Sender: TDXComponent;Surface: TDirectDrawSurface;const Mem: TDDSurfaceDesc;Rect: TRect; Index: Integer;Selected: boolean);
    { Protected-Deklarationen }
  public
    InfoFrame      : TDXInfoFrame;
    SoldatView     : TDXItemInfo;
    TrainButton    : TDXBitmapButton;
    TraallButton   : TDXBitmapButton;
    AusrustButton  : TDXBitmapButton;
    RenameButton   : TDXBitmapButton;

    Soldaten       : TDXListBox;
    SellButton     : TDXBitmapButton;
    BuyButton      : TDXBitmapButton;
    Kauf           : TDXListBox;
    WatchDialog    : TWatchMarktDialog;
    constructor Create(Container: TDXContainer);override;
    destructor Destroy;override;
    procedure SetDefaults;override;
  end;

implementation

uses
  game_api, basis_api;

{ TVerSoldatPage }

procedure TVerSoldatPage.BuySoldat(Sender: TObject);
begin
  Container.IncLock;
  Container.LoadGame(true);
  try
    SaveGame.BuySoldat(Kauf.ItemIndex);
  except
    on E: Exception do
    begin
      Container.DecLock;
      Container.LoadGame(false);
      game_api_MessageBox(E.Message,CCancelAction);
      exit;
    end;
  end;
  if (Kauf.Items.Count=1) then BuyButton.Enabled:=false;
  if not SellButton.Enabled then
  begin
    SellButton.Enabled:=true;
    TrainButton.Enabled:=true;
    TraallButton.Enabled:=true;
    RenameButton.Enabled:=true;
    AusrustButton.Enabled:=true;
  end;

  Soldaten.Items.Add(IntToStr(Savegame.SoldatenListe.Count-1));
  Kauf.Items.Delete(Kauf.ItemIndex);
  
  Container.LoadGame(false);
  InfoFrame.Redraw;
  Container.DecLock;
  Container.RedrawArea(fKauf,Container.Surface);
end;

constructor TVerSoldatPage.Create(Container: TDXContainer);
begin
  inherited;
  Animation:=paLeftToRight;
  fKauf:=Rect(0,0,0,0);
  HintLabel.BlendColor:=bcGreen;
  HintLabel.BorderColor:=coFirstColor;

  SetPageInfo(StripHotKey(BSoldaten));

{ Informationsframe }
  InfoFrame:=TDXInfoFrame.Create(Self);
  with InfoFrame do
  begin
    SetRect(388,10,244,70);
    SetFont(Font);
    ColorSheme:=icsGreen;
    Infos:=[ifCredits,ifWohn];
    SystemKeyChange:=true;
  end;

{ Ihre Soldaten }
  { ListBox }
  Soldaten:=TDXListBox.Create(Self);
  with Soldaten do
  begin
    SetRect(8,54,370,286);
    BorderColor:=coFirstColor;
    FocusColor:=coSecondColor;
    SetFont(Font);
    SetButtonFont(ScrollBarFont);
    FirstColor:=bcLime;
    SecondColor:=coScrollPen;
    Hint:=HSoldatenList;
    ItemHeight:=56;
    OwnerDraw:=true;
    OnDrawItem:=DrawSelfSoldat;
    OnChange:=SoldatChange;
    AlwaysChange:=true;
    HeadRowHeight:=17;
    HeadBlendColor:=bcGreen;
    HeadData:=MakeHeadData(LYourSold,FSoldat,FSoldaten);
  end;

  { Ausr�sten button }
  AusrustButton:=TDXBitmapButton.Create(Self);
  with AusrustButton do
  begin
    SetRect(388,54,110,28);
    SetButtonFont(Font);
    FirstColor:=coFirstColor;
    SecondColor:=coSecondColor;
    AccerlateColor:=coAccerlateColor;
    OnClick:=AusrustSoldat;
    Hint:=HAusruesten;
    Text:=BAusruesten;
    BlendColor:=bcGreen;
  end;

  { Ausbildungsbutton }
  TrainButton:=TDXBitmapButton.Create(Self);
  with TrainButton do
  begin
    SetRect(388,118,110,28);
    SetButtonFont(Font);
    FirstColor:=coFirstColor;
    SecondColor:=coSecondColor;
    AccerlateColor:=coAccerlateColor;
    OnClick:=TrainSoldat;
    RoundCorners:=rcNone;
    Hint:=HTrainSoldat;
    Text:=BSTraining;
    BlendColor:=bcGreen;
  end;

  { Ausbildungsbutton }
  TraallButton:=TDXBitmapButton.Create(Self);
  with TraallButton do
  begin
    SetRect(388,147,110,28);
    SetButtonFont(Font);
    FirstColor:=coFirstColor;
    SecondColor:=coSecondColor;
    RoundCorners:=rcNone;
    AccerlateColor:=coAccerlateColor;
    OnClick:=TrainAllSoldat;
    Hint:=HTrainAllSoldat;
    Text:=BSAllTraining;
    BlendColor:=bcGreen;
  end;

  { Umbennenbutton }
  RenameButton:=TDXBitmapButton.Create(Self);
  with RenameButton do
  begin
    SetRect(388,176,110,28);
    SetButtonFont(Font);
    FirstColor:=coFirstColor;
    SecondColor:=coSecondColor;
    RoundCorners:=rcBottom;
    AccerlateColor:=coAccerlateColor;
    OnClick:=RenameSoldat;
    Hint:=HRenameSoldat;
    Text:=BSRename;
    BlendColor:=bcGreen;
  end;

  { Verkaufenbutton }
  SellButton:=TDXBitmapButton.Create(Self);
  with SellButton do
  begin
    FirstColor:=coFirstColor;
    SecondColor:=coSecondColor;
    AccerlateColor:=coAccerlateColor;
    RoundCorners:=rcTop;
    SetButtonFont(Font);
    SetRect(388,89,110,28);
    OnClick:=SellSoldat;
    Text:=BFire;
    Hint:=HSellSoldat;
    BlendColor:=bcGreen;
  end;

{ Arbeitsmarkt }
  { ListBox }
  Kauf:=TDXListBox.Create(Self);
  with Kauf do
  begin
    SetRect(8,348,370,175);
    BorderColor:=coFirstColor;
    FocusColor:=coSecondColor;
    SetFont(Font);
    SetButtonFont(ScrollBarFont);
    FirstColor:=bcLime;
    SecondColor:=coScrollPen;
    Hint:=HBuySoldatList;
    ItemHeight:=56;
    OwnerDraw:=true;
    OnDrawItem:=DrawKaufSoldat;
    OnChange:=KaufChange;
    AlwaysChange:=true;
    HeadRowHeight:=17;
    HeadBlendColor:=bcGreen;
    HeadData:=MakeHeadData(LArbeit,FAMZivilist,FAMZivilisten);
  end;

  { Kaufbutton }
  BuyButton:=TDXBitmapButton.Create(Self);
  with BuyButton do
  begin
    FirstColor:=coFirstColor;
    SecondColor:=coSecondColor;
    AccerlateColor:=coAccerlateColor;
    SetButtonFont(Font);
    SetRect(388,348,110,28);
    Text:=BHire;
    OnClick:=BuySoldat;
    Hint:=HBuySoldat;
    RoundCorners:=rcTop;
    BlendColor:=bcGreen;
  end;

  { �berwachen }
  with TDXBitmapButton.Create(Self) do
  begin
    FirstColor:=coFirstColor;
    SecondColor:=coSecondColor;
    AccerlateColor:=coAccerlateColor;
    RoundCorners:=rcBottom;
    SetButtonFont(Font);
    SetRect(388,377,110,28);
    Text:=ST0309270019;
    OnClick:=WatchSoldatMarkt;
    Hint:=ST0309270024;
    BlendColor:=bcGreen;
  end;

{ Soldatenviewer }
  SoldatView:=TDXItemInfo.Create(Self);
  with SoldatView do
  begin
    SetRect(508,54,284,469);
    SetFont(Font);
    BorderColor:=coFirstColor;
    Caption:=IIYourSoldat;
    Hint:=HSoldatView;
    BlendColor:=bcGreen;
  end;

  WatchDialog:=TWatchMarktDialog.Create(Self);
  SetButtonFont(WatchDialog.Font);
  WatchDialog.PersonenGruppe:=ST0309270025;

  UnionRect(fKauf,fKauf,Soldaten.ClientRect);
  UnionRect(fKauf,fKauf,SoldatView.ClientRect);

  basis_api_RegisterChangeBaseHandler(OnBasisChange);
end;

procedure TVerSoldatPage.DrawKaufSoldat(Sender: TDXComponent;
  Surface: TDirectDrawSurface;const Mem: TDDSurfaceDesc; Rect: TRect; Index: Integer;
  Selected: boolean);
var
  Text   : String;
begin
  if Selected then
  begin
    if fKaufSelected then
      Kauf.DrawSelection(Surface,Mem,Rect,bcLime)
    else
      Kauf.DrawSelection(Surface,Mem,Rect,bcDarkGreen);
  end;
  SaveGame.SoldatenKauf.DrawDXSoldatenBitmap(Index,Surface,Mem,Rect.Left+5,Rect.Top+5,true);
  YellowStdFont.Draw(Surface,Rect.Left+53,Rect.Top+5,SaveGame.SoldatenKauf[Index].Name);
  WhiteStdFont.Draw(Surface,Rect.Left+53,Rect.Top+25,IIKaufPreis);
  Text:=Format(FCredits,[SaveGame.SoldatenKauf.Wert[Index]/1]);
  WhiteStdFont.Draw(Surface,Rect.Right-8-WhiteStdFont.TextWidth(Text),Rect.Top+25,Text);
end;

procedure TVerSoldatPage.DrawSelfSoldat(Sender: TDXComponent;
  Surface: TDirectDrawSurface;const Mem: TDDSurfaceDesc; Rect: TRect; Index: Integer;
  Selected: boolean);
var
  Text   : String;
  SIndex : Integer;
  Font   : TDirectFont;
begin
  SIndex:=StrToInt(Soldaten.Items[Index]);
  if Selected then
  begin
    if fKaufSelected then
      Soldaten.DrawSelection(Surface,Mem,Rect,bcDarkGreen)
    else
      Soldaten.DrawSelection(Surface,Mem,Rect,bcLime);
  end;
  SaveGame.SoldatenListe.DrawDXSoldatenBitmap(SIndex,Surface,Mem,Rect.Left+5,Rect.Top+5,true);
  YellowStdFont.Draw(Surface,Rect.Left+53,Rect.Top+5,SaveGame.SoldatenListe[SIndex].Name);

  Text:=SaveGame.SoldatenListe.GetRaumschiffName(SIndex);
  if Text='' then
    Text:=LNoCommand
  else
    Text:=LStarShip+Text;

  WhiteStdFont.Draw(Surface,Rect.Left+53,Rect.Top+30,Text);

  Text:='';
  Font:=WhiteStdFont;
  if SaveGame.SoldatenListe[SIndex].Ges<SaveGame.SoldatenListe[SIndex].MaxGes then
  begin
    with SaveGame.SoldatenListe[SIndex] do
      Text:=LKranken+' ('+Format(FHours,[round((MaxGes-Ges)/(MaxGes/5)*24)])+')';

    Font:=RedBStdFont;
  end
  else if SaveGame.SoldatenListe[SIndex].Train then
    Text:=LTraining;

  if Text<>'' then
    Font.Draw(Surface,Rect.Right-8-Font.TextWidth(Text),Rect.Top+30,Text);
end;

procedure TVerSoldatPage.KaufChange(Sender: TObject);
begin
  if Kauf.Items.Count=0 then
    exit;

  if SoldatView.ItemValid and (SameSoldat(SoldatView.Soldat,SaveGame.SoldatenKauf[Kauf.ItemIndex])) then
    exit;

  Container.incLock;
  Container.LoadGame(true);
  SoldatView.Caption:=IIMarktSoldat;
  SoldatView.ShowSoldatInfo(SaveGame.SoldatenKauf.GetSoldatAddr(Kauf.ItemIndex)^);

  Container.LoadGame(false);

  if not fKaufSelected then
  begin
    fKaufSelected:=true;
    Soldaten.Redraw;
  end;

  Container.DecLock;
  Container.RedrawArea(SoldatView.ClientRect,Container.Surface);
end;

procedure TVerSoldatPage.SoldatChange(Sender: TObject);
var
  SIndex: Integer;
begin
  if Soldaten.Items.Count=0 then
    exit;

  SIndex:=GetSelectedSoldat;
  if SoldatView.ItemValid and SameSoldat(SoldatView.Soldat,SaveGame.SoldatenListe[SIndex]) then
    exit;

  Container.Lock;
  SoldatView.Caption:=IIYourSoldat;

  SoldatView.ShowSoldatInfo(SaveGame.SoldatenListe.GetSoldatAddr(SIndex)^);

  Container.LoadGame(false);
  SetTrainButton(SaveGame.SoldatenListe[SIndex].Train);

  if fKaufSelected then
  begin
    fKaufSelected:=false;
    Kauf.Redraw;
  end;

  Container.decLock;
  Container.RedrawArea(SoldatView.ClientRect,Container.Surface);
end;

procedure TVerSoldatPage.SellSoldat(Sender: TObject);
var
  Index: Integer;
begin
  Container.Lock;
  Index:=GetSelectedSoldat;
  if not SaveGame.SellSoldat(Index) then
  begin
    Container.UnLockAll;
    game_api_MessageBox(ST0309270026,CCancelAction);
    exit;
  end;

  BuyButton.Enabled:=true;
  Kauf.Items.Add('');
  RebuildSoldatenList;

  Container.LoadGame(false);
  InfoFrame.Redraw;
  Container.DecLock;
  Container.RedrawArea(fKauf,Container.Surface);
end;

procedure TVerSoldatPage.SetDefaults;
var
  Dummy: Integer;
begin
  InfoFrame.Reset;
  Kauf.ChangeItem(true);
  Kauf.Items.Clear;
  for DUmmy:=0 to SaveGame.SoldatenKauf.Count-1 do
  begin
    Kauf.Items.Add(SaveGame.SoldatenKauf[Dummy].Name);
  end;
  BuyButton.Enabled:=not (Kauf.Items.Count=0);
  Kauf.ItemIndex:=0;
  Kauf.ChangeItem(false);
  Kauf.Items.OnChange(Self);
  RebuildSoldatenList;
end;


procedure TVerSoldatPage.TrainAllSoldat(Sender: TObject);
var
  Dummy  : Integer;
  Change : Boolean;
begin
  Change:=true;
  for Dummy:=0 to Soldaten.Items.Count-1 do
  begin
    if not SaveGame.SoldatenListe[StrToInt(Soldaten.Items[Dummy])].Train then Change:=false;
  end;
  for Dummy:=0 to Soldaten.Items.Count-1 do
  begin
    try
      SaveGame.SoldatenListe.SetTraining(StrToInt(Soldaten.Items[Dummy]),Change);
    except
{      on E:Exception do
      begin
        game_api_MessageBox(E.Message,CCancelAction);
      end;}
    end;
  end;
  Container.IncLock;
  SetTrainButton(SaveGame.SoldatenListe[GetSelectedSoldat].Train);
  CheckAllTraining;
  Container.DecLock;
  Soldaten.Redraw;
end;

procedure TVerSoldatPage.TrainSoldat(Sender: TObject);
begin
  try
    SaveGame.SoldatenListe.SetTraining(GetSelectedSoldat);
    Container.IncLock;
    SetTrainButton(SaveGame.SoldatenListe[GetSelectedSoldat].Train);
    CheckAllTraining;
    Container.DecLock;
  except
    on E:Exception do
    begin
      game_api_MessageBox(E.Message,CCancelAction);
      exit;
    end;
  end;
  Soldaten.ItemIndex:=Soldaten.ItemIndex+1;
end;

procedure TVerSoldatPage.RenameSoldat(Sender: TObject);
var
  Name: String;
begin
  Name:=SaveGame.SoldatenListe.Soldaten[GetSelectedSoldat].Name;
  QueryText(CRename,30,Name,ctGreen);
  if Name='' then exit;
  SaveGame.SoldatenListe.RenameSoldat(GetSelectedSoldat,Name);
  Container.IncLock;
  SoldatChange(Self);
  Container.DecLock;
  Soldaten.Redraw;
end;

procedure TVerSoldatPage.SetTrainButton(InTraining: boolean);
begin
  if InTraining then
  begin
    TrainButton.Text:=BSFree;
    TrainButton.Hint:=HFreeSoldat;
  end
  else
  begin
    TrainButton.Text:=BSTraining;
    TrainButton.Hint:=HTrainSoldat;
  end;
end;

procedure TVerSoldatPage.CheckAllTraining;
var
  Dummy       : Integer;
  AllTraining : boolean;
begin
  Dummy:=0;
  AllTraining:=true;
  while (Dummy<Soldaten.Items.Count) and AllTraining do
  begin
    if not SaveGame.SoldatenListe[StrToInt(Soldaten.Items[Dummy])].Train then AllTraining:=false;
    inc(Dummy);
  end;
  if AllTraining then
  begin
    TraallButton.Text:=BSAllFree;
    TraallButton.Hint:=HFreeAllSoldat;
  end
  else
  begin
    TraallButton.Text:=BSAllTraining;
    TraallButton.Hint:=HTrainAllSoldat;
  end;
end;

procedure TVerSoldatPage.OnBasisChange(Sender: TObject);
begin
  if Container.ActivePage<>Self then
    exit;

  RebuildSoldatenList;
end;

{ Gibt den Index des ausgew�hlten Soldaten zur�ck }
function TVerSoldatPage.GetSelectedSoldat: Integer;
begin
  if Soldaten.ItemIndex=-1 then result:=-1
  else result:=StrToInt(Soldaten.Items[Soldaten.ItemIndex]);
end;

{ aktualisiert die Soldatenliste }
procedure TVerSoldatPage.RebuildSoldatenList;
var
  Dummy: Integer;
begin
  Container.Lock;
  Soldaten.ChangeItem(true);
  Soldaten.Items.Clear;
  for Dummy:=0 to SaveGame.SoldatenListe.Count-1 do
  begin
    if SaveGame.SoldatenListe.InSelectedBasis(Dummy) then
      Soldaten.Items.Add(IntToStr(Dummy));
  end;
  Soldaten.ItemIndex:=0;
  Soldaten.ChangeItem(false);
  Soldaten.Items.OnChange(Self);
  Container.LoadGame(false);
  Soldaten.Redraw;
  SellButton.Enabled:=not (Soldaten.Items.Count=0);
  TrainButton.Enabled:=not (Soldaten.Items.Count=0);
  TraallButton.Enabled:=not (Soldaten.Items.Count=0);
  RenameButton.Enabled:=not (Soldaten.Items.Count=0);
  AusrustButton.Enabled:=not (Soldaten.Items.Count=0);

  SoldatView.Redraw;
  CheckAllTraining;
  if GetSelectedSoldat<>-1 then
    SetTrainButton(SaveGame.SoldatenListe[GetSelectedSoldat].Train);
  Container.DecLock;
  Container.DoFlip;
end;

procedure TVerSoldatPage.AusrustSoldat(Sender: TObject);
var
  Dummy: Integer;
  List : TList;
begin
  List:=TList.Create;
  for Dummy:=0 to SaveGame.SoldatenListe.Count-1 do
    List.Add(SaveGame.SoldatenListe.Managers[Dummy]);
  Managers:=List;
  VerSoldatEinsatz.UseTimeUnits:=true;
  ActiveManager:=SaveGame.SoldatenListe.Managers[GetSelectedSoldat];
  ChangePage(PageSoldatConfig);
  List.Free;
end;

procedure TVerSoldatPage.WatchSoldatMarkt(Sender: TObject);
var
  Watch: TWatchMarktSettings;
begin
  if SaveGame.SoldatenKauf.GetWatch(SaveGame.BasisListe.Selected.ID,Watch) then
  begin
    WatchDialog.Aktiv:=true;
    WatchDialog.Faehigkeiten:=Watch.minFaehigkeiten;
    WatchDialog.UeberwachungsType:=Watch.WatchType;
  end
  else
    WatchDialog.Aktiv:=false;
  if WatchDialog.ShowModal=mrOK then
  begin
    if WatchDialog.Aktiv then
      SaveGame.SoldatenKauf.SetWatch(SaveGame.BasisListe.Selected.ID,WatchDialog.Faehigkeiten,WatchDialog.UeberwachungsType)
    else
      SaveGame.SoldatenKauf.CancelWatch(SaveGame.BasisListe.Selected.ID);
  end;
end;

destructor TVerSoldatPage.Destroy;
begin
  WatchDialog.Free;
  inherited;
end;

end.
