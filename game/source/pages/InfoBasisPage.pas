{********************************************************************************
*										*
* Copyright (C) 2004  X-Force Team - jim_raynor@web.de				*
*										*
* This file is part of X-Force: Fight For Destiny (X-Force).			*
*										*
* X-Force is free software; you can redistribute it and/or modify		*
* it under the terms of the GNU General Public License as published by		*
* the Free Software Foundation; either version 2 of the License, or		*
* (at your option) any later version.						*
*										*
* X-Force is distributed in the hope that it will be useful,			*
* but WITHOUT ANY WARRANTY; without even the implied warranty of		*
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the			*
* GNU General Public License for more details.					*
*										*
* You should have received a copy of the GNU General Public License		*
* along with X-Force; if not, write to the Free Software			*
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA	*
*										*
*********************************************************************************
*										*
* Basisinformationen                                                            *
*										*
*********************************************************************************

{$I ../Settings.inc}

unit InfoBasisPage;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  DXContainer, KD4Page, DXLabel, DXBitmapButton, Defines,StringConst,
  DXFrameBox, DXProgressBar, Blending, XForce_types, DXDraws, DXGroupCaption,
  DXBasisInfo, TraceFile, DXInfoFrame, menus, DXItemInfo;

type
  TInfoBasis = class(TKD4Page)
  private
  protected
    procedure OnSelectedBasisChange(Sender: TObject);
  public
    Captions     : TDXGroupCaption;
    RaumFrame    : TDXFrameBox;
    InfoFrame    : TDXInfoFrame;
    BasisInfo    : TDXBasisInfo;
    ItemInfo     : TDXItemInfo;
    constructor Create(Container: TDXContainer);override;
    procedure SetDefaults;override;
  end;

implementation

uses
  basis_api;

{ TInfoBasis }

constructor TInfoBasis.Create(Container: TDXContainer);
begin
  inherited;
  Animation:=paBottomToUp;

  SetPageInfo(StripHotKey(BBasisInfo));

{ Informationsframe }
  InfoFrame:=TDXInfoFrame.Create(Self);
  with InfoFrame do
  begin
    SetRect(388,10,324,90);
    SetFont(Font);
    ColorSheme:=icsRed;
    Infos:=[ifCredits];
    SystemKeyChange:=true;
  end;

  { BasisInfos }
  BasisInfo:=TDXBasisInfo.Create(Self);
  with BasisInfo do
  begin
    SetFont(Font);
    SetRect(16,54,388,446);
    BorderColor:=coFirstColor;
    RoundCorners:=rcLeft;
  end;

  { BasisInfos }
  ItemInfo:=TDXItemInfo.Create(Self);
  with ItemInfo do
  begin
    SetFont(Font);
    SetRect(405,54,388,446);
    BorderColor:=coFirstColor;
    RoundCorners:=rcRight;
    Caption:=StripHotkey(BBasisInfo);
  end;

  basis_api_RegisterChangeBaseHandler(OnSelectedBasisChange);
end;

procedure TInfoBasis.OnSelectedBasisChange(Sender: TObject);
begin
  if Container.ActivePage<>Self then
    exit;

  with ItemInfo do
    BasisStatus:=basis_api_GetSelectedBasis;

  Container.Lock;
  SetDefaults;
  Container.UnLock;
end;

procedure TInfoBasis.SetDefaults;
begin
  InfoFrame.Reset;
  with ItemInfo do
    BasisStatus:=basis_api_GetSelectedBasis;

  BasisInfo.Update;
end;

end.
