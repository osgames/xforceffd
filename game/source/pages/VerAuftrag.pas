{********************************************************************************
*										*
* Copyright (C) 2004  X-Force Team - jim_raynor@web.de				*
*										*
* This file is part of X-Force: Fight For Destiny (X-Force).			*
*										*
* X-Force is free software; you can redistribute it and/or modify		*
* it under the terms of the GNU General Public License as published by		*
* the Free Software Foundation; either version 2 of the License, or		*
* (at your option) any later version.						*
*										*
* X-Force is distributed in the hope that it will be useful,			*
* but WITHOUT ANY WARRANTY; without even the implied warranty of		*
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the			*
* GNU General Public License for more details.					*
*										*
* You should have received a copy of the GNU General Public License		*
* along with X-Force; if not, write to the Free Software			*
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA	*
*										*
*********************************************************************************
*										*
* Verwaltungsbildschirm Kauf-/Verkaufaufträge (Lager->Ausschreibungen)		*
*										*
********************************************************************************}

{$I ../Settings.inc}

unit VerAuftrag;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  KD4Page,DXContainer,DXDraws,XForce_types,Defines, DXBitmapButton, DXListBox, DXLabel,
  DXInfoFrame, Blending, KD4Utils, DirectDraw, DirectFont, TraceFile,
  AuftragWatch, Menus, StringConst;

const
  coFirstColor  : TColor = clNavy;
  coSecondColor : TColor = clBlue;
  coScrollPen   : TColor = clNavy;
  coScrollBrush : TColor = clNavy;

type
  TVerAuftrag = class(TKD4Page)
  private
    fAngebotRect  : TRect;
    fAllRect      : TRect;
    fMarkSettings : record
                      Preis : Integer;
                      Kauf  : Boolean;
                    end;
    procedure ClosePage(Sender: TObject);
    procedure DrawItems(Sender: TDXComponent;Surface: TDirectDrawSurface;const Mem: TDDSurfaceDesc;Rect: TRect; Index: Integer;Selected: boolean);
    procedure DrawAngebot(Sender: TDXComponent;Surface: TDirectDrawSurface;const Mem: TDDSurfaceDesc;Rect: TRect; Index: Integer;Selected: boolean);
    procedure AuftragMouseUp(Sender: TObject;Button: TMouseButton;Shift: TShiftState;x,y: Integer);
    procedure OrganMouseUp(Sender: TObject;Button: TMouseButton;Shift: TShiftState;x,y: Integer);
    procedure AktuButtons;
    procedure AktuAuftraege;
    procedure OnSelectedBasisChange(Sender: TObject);
    { Private-Deklarationen }
  protected
    procedure OnChangeItem(Sender: TObject);
    procedure StornAuftrag(Sender: TObject);
    procedure ShowWatchDialog(Sender: TObject);
    procedure AcceptAngebot(Sender: TObject);
    { Protected-Deklarationen }
  public
    CloseButton   : TDXBitmapButton;
    Auftrag       : TDXListBox;
    InfoFrame     : TDXInfoFrame;
    Angebot       : TDXListBox;
    AcceptButton  : TDXBitmapButton;
    CancelButton  : TDXBitmapButton;
    ConfAnnahme   : TDXBitmapButton;
    AuftragWatch  : TAuftragWatch;
    constructor Create(Container: TDXContainer);override;
    destructor Destroy;override;
    procedure SetDefaults;override;
    procedure AktuAngebote;
  end;

implementation

uses
  game_api, ufopaedie_api, basis_api;

{ TVerAuftrag }

procedure TVerAuftrag.AktuAngebote;
var
  Dummy: Integer;
  ID : LongWord;
  Index: Integer;
begin
  if Auftrag.Items.Count=0 then
  begin
    Angebot.Items.Clear;
    AcceptButton.Enabled:=false;
    exit;
  end;
  Container.Lock;
  Index:=StrToInt(Auftrag.Items[Auftrag.ItemIndex]);
  Angebot.ChangeItem(true);
  Angebot.Items.Clear;
  ID:=SaveGame.LagerListe.Auftrag[Index].ID;
  for Dummy:=0 to SaveGame.LagerListe.AngebotCount-1 do
  begin
    if ID=SaveGame.LagerListe.Angebote[Dummy].ID then
      Angebot.Items.Add(IntToStr(Dummy));
  end;
  Angebot.ChangeItem(false);
  Angebot.Items.OnChange(Angebot);
  AktuButtons;
  Container.UnLock;
  AcceptButton.Enabled:=Angebot.Items.Count>0;
  Container.RedrawArea(fAngebotRect,Container.Surface);
end;

procedure TVerAuftrag.AuftragMouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; x, y: Integer);
begin
  if Button<>mbRight then exit;
  if Auftrag.Items.Count=0 then exit;
  ufopaedie_api_ShowEntry(upeLagerItem,SaveGame.LagerListe.Auftrag[Auftrag.ItemIndex].Item);
end;

procedure TVerAuftrag.OnChangeItem(Sender: TObject);
begin
  AktuAngebote;
end;

procedure TVerAuftrag.ClosePage(Sender: TObject);
begin
  if not CloseModal then
    ChangePage(PageGameMenu);
end;

constructor TVerAuftrag.Create(Container: TDXContainer);
begin
  inherited;
  HintLabel.BlendColor:=bcBlue;
  HintLabel.BorderColor:=coFirstColor;

  Animation:=paRightToLeft;

  SetPageInfo(StripHotKey(BAuftrag));

  { SchliessenButton }
  CloseButton:=TDXBitmapButton.Create(Self);
  with CloseButton do
  begin
    Text:=BClose;
    SetRect(522,424,110,28);
    FirstColor:=coFirstColor;
    EscButton:=true;
    SecondColor:=coSecondColor;
    AccerlateColor:=coAccerlateColor;
    BlendColor:=bcBlue;
    OnClick:=ClosePage;
    SetButtonFont(Font);
    Text:=BClose;
    Hint:=HCloseButton;
  end;

{ Informationsframe }
  InfoFrame:=TDXInfoFrame.Create(Self);
  with InfoFrame do
  begin
    SetRect(388,34,244,70);
    SetFont(Font);
    ColorSheme:=icsBlue;
    Infos:=[ifCredits,ifLager];
    SystemKeyChange:=true;
  end;

{ Aufträge }
  { Auftragslistbox }
  Auftrag:=TDXListBox.Create(Self);
  with Auftrag do
  begin
    SetRect(8,34,250,382);
    BorderColor:=coFirstColor;
    FocusColor:=coSecondColor;
    SetFont(Font);
    SetButtonFont(ScrollBarFont);
    FirstColor:=bcBlue;
    SecondColor:=coScrollPen;
    Hint:=HAuftragsList;
    ItemHeight:=52;
    OnChange:=OnChangeItem;
    OwnerDraw:=True;
    OnDrawItem:=DrawItems;
    OnMouseUp:=AuftragMouseUp;
    HeadRowHeight:=17;
    HeadBlendColor:=bcBlue;
    HeadData:=MakeHeadData(LAuftrag,LAAuftrag,LAAuftrage);
  end;

{ Angebote }
  { ListBox }
  Angebot:=TDXListBox.Create(Self);
  with Angebot do
  begin
    SetFont(Font);
    setButtonFont(ScrollBarFont);
    SetRect(388,34,244,382);
    BorderColor:=coFirstColor;
    FocusColor:=coSecondColor;
    FirstColor:=bcBlue;
    SecondColor:=coScrollPen;
    ItemHeight:=68;
    Hint:=HAngebotsList;
    OwnerDraw:=true;
    OnDrawItem:=DrawAngebot;
    OnMouseUp:=OrganMouseUp;
    HeadRowHeight:=17;
    HeadBlendColor:=bcBlue;
    HeadData:=MakeHeadData(LACAngebot,LAAngebot,LAAngebote);
  end;

  { Angebot annehmen }
  AcceptButton:=TDXBitmapButton.Create(Self);
  with AcceptButton do
  begin
    SetRect(268,34,110,28);
    SetButtonFont(Font);
    Hint:=HAcceptAngebot;
    FirstColor:=coFirstColor;
    SecondColor:=coSecondColor;
    AccerlateColor:=coAccerlateColor;
    BlendColor:=bcBlue;
    OnClick:=AcceptAngebot;
    RoundCorners:=rcTop;
    Text:=LAAccept;
  end;


  { Auftrag stornieren }
  CancelButton:=TDXBitmapButton.Create(Self);
  with CancelButton do
  begin
    SetRect(268,63,110,28);
    SetButtonFont(Font);
    Hint:=HStonieren;
    FirstColor:=coFirstColor;
    SecondColor:=coSecondColor;
    AccerlateColor:=coAccerlateColor;
    BlendColor:=bcBlue;
    RoundCorners:=rcBottom;
    OnClick:=StornAuftrag;
    Text:=LACancel;
  end;

  { Auftrag stornieren }
  ConfAnnahme:=TDXBitmapButton.Create(Self);
  with ConfAnnahme do
  begin
    SetRect(268,103,110,28);
    SetButtonFont(Font);
    Hint:=ST0309270020;
    FirstColor:=coFirstColor;
    SecondColor:=coSecondColor;
    AccerlateColor:=coAccerlateColor;
    BlendColor:=bcBlue;
    RoundCorners:=rcAll;
    OnClick:=ShowWatchDialog;
    Text:=ST0309270021;
  end;

  AuftragWatch:=TAuftragWatch.Create(Self);
  SetButtonFont(AuftragWatch.Font);

  UnionRect(fAngebotRect,AcceptButton.ClientRect,Angebot.ClientRect);
  UnionRect(fAllRect,fAngebotRect,Auftrag.ClientRect);

  fMarkSettings.Preis:=1000;
  fMarkSettings.Kauf:=true;

  basis_api_RegisterChangeBaseHandler(OnSelectedBasisChange);
end;

procedure TVerAuftrag.DrawItems(Sender: TDXComponent;
  Surface: TDirectDrawSurface;const Mem: TDDSurfaceDesc; Rect: TRect; Index: Integer;
  Selected: boolean);
var
  IIndex  : Integer;
  Font    : TDirectFont;
  FontB   : TDirectFont;
  Auftrag : TAuftrag;
begin
  Index:=StrToInt(Self.Auftrag.Items[Index]);
  Auftrag:=SaveGame.LagerListe.Auftrag[Index];
  IIndex:=Auftrag.Item;
  if Selected then
  begin
    Self.Auftrag.DrawSelection(Surface,Mem,Rect,bcBlue);
  end;
  SaveGame.LagerListe.DrawImage(SaveGame.LagerListe[IIndex].ImageIndex,Surface,Rect.Left+4,Rect.Top+4);
  YellowStdFont.Draw(Surface,Rect.Left+40,Rect.Top+8,SaveGame.LagerListe[IIndex].Name);
  if Auftrag.Kauf then
  begin
    Font:=RedStdFont;
    FontB:=RedBStdFont;
  end
  else
  begin
    Font:=LimeStdFont;
    FontB:=LimeBStdFont;
  end;
  FontB.Draw(Surface,Rect.Left+40,Rect.Top+28,IntToStr(Auftrag.Anzahl));
  Font.Draw(Surface,Rect.Left+65,Rect.Top+28,Format('* '+FCredits,[(Auftrag.Preis/1)]));
  Font.Draw(Surface,Rect.Left+150,Rect.Top+28,Format('= '+FCredits,[(Auftrag.Preis/1)*Auftrag.Anzahl]));
end;

procedure TVerAuftrag.SetDefaults;
begin
  InfoFrame.Reset;
  AktuAuftraege;
end;

procedure TVerAuftrag.DrawAngebot(Sender: TDXComponent;
  Surface: TDirectDrawSurface;const Mem: TDDSurfaceDesc;Rect: TRect; Index: Integer;
  Selected: boolean);
var
  Annr    : Integer;
  Organ   : Integer;
  TempAn  : TAngebot;
  Auftrag : TAuftrag;
  Text    : String;
  Item    : PLagerItem;
  Font    : TDirectFont;
begin
  Annr:=StrToInt(Angebot.Items[Index]);
  TempAn:=SaveGame.LagerListe.Angebote[Annr];
  Organ:=SaveGame.LagerListe.Angebote[Annr].Organisation;
  if Selected then
  begin
    Angebot.DrawSelection(Surface,Mem,Rect,bcBlue);
  end;
  YellowStdFont.Draw(Surface,Rect.Left+51,Rect.Top+9,SaveGame.Organisations[Organ].Name);
  SaveGame.Organisations.DrawOrgan(Surface,Rect.Left+6,Rect.Top+6,Organ);
  Auftrag:=SaveGame.LagerListe.Auftrag[SaveGame.LagerListe.AuftragOfID(TempAn.ID)];
  Item:=SaveGame.LagerListe.AddrLagerItem(Auftrag.Item);
  WhiteBStdFont.Draw(Surface,Rect.Left+6,Rect.Top+33,IntToStr(TempAn.Anzahl));
  WhiteStdFont.Draw(Surface,Rect.Left+31,Rect.Top+33,Format('* '+FCredits,[TempAn.Preis/1]));

  if Auftrag.Kauf then
  begin
    Text:=Format(ST0309270022,[TempAn.Preis*100/Item.Kaufpreis]);
    if TempAn.Preis<Item.KaufPreis then
      Font:=LimeBStdFont
    else
      Font:=RedBStdFont;
  end
  else
  begin
    Text:=Format(ST0309270022,[TempAn.Preis*100/Item.VerKaufpreis]);
    if TempAn.Preis>Item.VerkaufPreis then
      Font:=LimeBStdFont
    else
      Font:=RedBStdFont;
  end;
  Font.Draw(Surface,Rect.Left+38,Rect.Top+50,Text);

  WhiteStdFont.Draw(Surface,Rect.Left+111,Rect.Top+33,Format('= '+FCredits,[(TempAn.Preis*TempAn.Anzahl)/1]));
  Text:=ZahlString(LARound,LARounds,TempAn.Rounds);
  YellowStdFont.Draw(Surface,Rect.Right-YellowStdFont.TextWidth(Text)-6,Rect.Top+9,Text);
//  HLine(Surface,Mem,Rect.Left,Rect.Right,Rect.Top,bcNavy);
end;

procedure TVerAuftrag.StornAuftrag(Sender: TObject);
begin
  Container.IncLock;
  Container.LoadGame(true);
  SaveGame.LagerListe.DeleteAuftrag(StrToInt(Auftrag.Items[Auftrag.ItemIndex]));
  AktuAuftraege;
  Container.DecLock;
  Container.LoadGame(false);
  Container.RedrawArea(fAllRect,Container.Surface);
end;

procedure TVerAuftrag.AcceptAngebot(Sender: TObject);
var
  Index: Integer;
begin
  Index:=StrToInt(Angebot.Items[Angebot.ItemIndex]);
  Container.IncLock;
  Container.LoadGame(true);
  try
    SaveGame.LagerListe.AcceptAngebot(Index);
  except
    on E:Exception do
    begin
      Container.LoadGame(false);
      Container.decLock;
      game_api_MessageBox(E.Message,CCancelAction);
      exit;
    end;
  end;
  AktuAuftraege;
  Container.LoadGame(false);
  InfoFrame.Redraw;
  Container.DecLock;
  Container.RedrawArea(fAllRect,Container.Surface);
end;

procedure TVerAuftrag.OrganMouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; x, y: Integer);
var
  Annr   : Integer;
begin
  if Button<>mbRight then exit;
  if Angebot.Items.Count=0 then exit;
  Annr:=StrToInt(Angebot.Items[Angebot.ItemIndex]);
  ufopaedie_api_ShowCountryInfo(SaveGame.LagerListe.Angebote[Annr].Organisation);
end;

procedure TVerAuftrag.AktuButtons;
begin
  if Auftrag.Items.Count=0 then CancelButton.Enabled:=false else CancelButton.Enabled:=true;
  if Angebot.Items.Count=0 then AcceptButton.Enabled:=false else AcceptButton.Enabled:=true;
end;

procedure TVerAuftrag.AktuAuftraege;
var
  Dummy: Integer;
begin
  Container.Lock;
  Auftrag.ChangeItem(true);
  Auftrag.Items.Clear;
  for Dummy:=0 to SaveGame.LagerListe.AuftragCount-1 do
    if SaveGame.LagerListe.AuftragInSelectedBasis(Dummy) then
      Auftrag.Items.Add(IntToStr(Dummy));

  Auftrag.ChangeItem(false);
  Auftrag.Items.OnChange(nil);
  CancelButton.Enabled:=Auftrag.Items.Count>0;
  AktuAngebote;
  Container.UnLock;
  Container.RedrawArea(fAllRect,Container.Surface);
end;

procedure TVerAuftrag.OnSelectedBasisChange(Sender: TObject);
begin
  if Container.ActivePage<>Self then
    exit;

  AktuAuftraege;
end;

procedure TVerAuftrag.ShowWatchDialog(Sender: TObject);
begin
  AuftragWatch.Settings:=SaveGame.LagerListe.WatchSettings;
  AuftragWatch.ShowModal;
  SaveGame.LagerListe.WatchSettings:=AuftragWatch.Settings;
end;

destructor TVerAuftrag.Destroy;
begin
  AuftragWatch.Free;
  inherited;
end;

end.
