{********************************************************************************
*										*
* Copyright (C) 2004  X-Force Team - jim_raynor@web.de				*
*										*
* This file is part of X-Force: Fight For Destiny (X-Force).			*
*										*
* X-Force is free software; you can redistribute it and/or modify		*
* it under the terms of the GNU General Public License as published by		*
* the Free Software Foundation; either version 2 of the License, or		*
* (at your option) any later version.						*
*										*
* X-Force is distributed in the hope that it will be useful,			*
* but WITHOUT ANY WARRANTY; without even the implied warranty of		*
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the			*
* GNU General Public License for more details.					*
*										*
* You should have received a copy of the GNU General Public License		*
* along with X-Force; if not, write to the Free Software			*
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA	*
*										*
*********************************************************************************
*										*
* Verwaltungsbildschirm Soldaten Ausr�sten					*S
*										*
********************************************************************************}

{$I ../Settings.inc}

unit VerSoldatEinsatz;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  KD4Page, DXContainer, DXBitmapButton, Defines, Blending, DXSoldatChooser,
  GameFigureManager, DXSoldatConfig, XForce_types, TraceFile, DXItemInfo,
  NGTypes, DXInfoFrame,
  StringConst;

type
  TVerSoldatEinsatz = class(TKD4Page)
  private
    procedure ChooseSoldat(Sender: TObject);
    procedure ConfigDragEnd(Sender: TObject);
    procedure ChooserKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    { Private-Deklarationen }
  protected
    { Protected-Deklarationen }
  public
    Soldaten       : TDXSoldatChooser;
    SoldatConfig   : TDXSoldatConfig;
    SoldatInfo     : TDXItemInfo;
    InfoFrame      : TDXInfoFrame;
    constructor Create(Container: TDXContainer);override;
    procedure SetDefaults;override;
    { Public-Deklarationen }
  end;

var
  Managers      : TList;
  ActiveManager : TGameFigureManager;
  UseTimeUnits  : Boolean = true;
  ISOComun      : TConfigCommunication = nil;

implementation

{ TVerSoldatEinsatz }

procedure TVerSoldatEinsatz.ChooserKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key=VK_UP then
    SoldatConfig.ScrollFromItems(true)
  else if Key=VK_DOWN then
    SoldatConfig.ScrollFromItems(false);
end;

procedure TVerSoldatEinsatz.ChooseSoldat(Sender: TObject);
begin
  Container.lock;
  SoldatConfig.Manager:=Soldaten.ActiveManager;
  if Soldaten.ActiveManager is TSoldatManager then
    SoldatInfo.ShowSoldatInfo((Soldaten.ActiveManager as TSoldatManager).SoldatInfo^,Soldaten.ActiveManager)
  else
    SoldatInfo.ItemValid:=false;

  Container.UnLock;
  Container.RedrawArea(Rect(SoldatConfig.Left,SoldatConfig.Top,SoldatInfo.Right,SoldatInfo.Bottom),Container.Surface);
end;

procedure TVerSoldatEinsatz.ConfigDragEnd(Sender: TObject);
begin
  Soldaten.CheckRedraw;
end;

constructor TVerSoldatEinsatz.Create(Container: TDXContainer);
begin
  inherited;
  Animation:=paLeftToRight;

{ Informationsframe }
  InfoFrame:=TDXInfoFrame.Create(Self);
  with InfoFrame do
  begin
    SetRect(388,10,244,70);
    SetFont(Font);
    ColorSheme:=icsRed;
    Infos:=[ifCredits,ifWohn];
    SystemKeyChange:=true;
    Reset;
  end;

  Soldaten:=TDXSoldatChooser.Create(Self);
  with Soldaten do
  begin
    SetRect(7,52,785,74);
    RoundCorners:=rcTop;
    OnChange:=ChooseSoldat;
    OnKeyDown:=ChooserKeyDown;
  end;

  SoldatInfo:=TDXItemInfo.Create(Self);
  with SoldatInfo do
  begin
    SetFont(Font);
    RoundCorners:=rcNone;
    BorderColor:=clMaroon;
  end;

  { Soldaten ausr�sten }
  SoldatConfig:=TDXSoldatConfig.Create(Self);
  with SoldatConfig do
  begin
    SetRect(7,127,785,430);
    CreatePopupInfo;
    OnDragnDropEnd:=ConfigDragEnd;
    SoldatChooser:=Soldaten;
//    GetIDofWaffe:=GetID;
//    OnMessage:=ShowMessage;
  end;

  SoldatInfo.SetRect(SoldatConfig.Left+RightSplitter+1,SoldatConfig.Top,Soldaten.Right-(SoldatConfig.Left+RightSplitter+1),SoldatConfig.Height);
end;

procedure TVerSoldatEinsatz.SetDefaults;
begin
  SoldatConfig.Comun:=ISOComun;
  Soldaten.ManagerList:=Managers;
  Soldaten.ActiveManager:=ActiveManager;
  SoldatConfig.Manager:=ActiveManager;
  SoldatConfig.UseTimeUnits:=UseTimeUnits;
end;

end.
