{********************************************************************************
*										*
* Copyright (C) 2004  X-Force Team - jim_raynor@web.de				*
*										*
* This file is part of X-Force: Fight For Destiny (X-Force).			*
*										*
* X-Force is free software; you can redistribute it and/or modify		*
* it under the terms of the GNU General Public License as published by		*
* the Free Software Foundation; either version 2 of the License, or		*
* (at your option) any later version.						*
*										*
* X-Force is distributed in the hope that it will be useful,			*
* but WITHOUT ANY WARRANTY; without even the implied warranty of		*
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the			*
* GNU General Public License for more details.					*
*										*
* You should have received a copy of the GNU General Public License		*
* along with X-Force; if not, write to the Free Software			*
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA	*
*										*
*********************************************************************************
*										*
* Verwaltungsbildschirm Labor							*
*										*
********************************************************************************}

{$I ../Settings.inc}

unit VerLaborPage;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  KD4Page, DXContainer, DXBitmapButton, Defines, DXListBox, DXDraws,
  DXLabel, DXInfoFrame, KD4Utils, Blending, VerArbeitsMarkt, XForce_types, DirectDraw,
  DXTextViewer, math, DirectFont, Menus, StringConst;

{ Konstantendefinition }
const
  coFirstColor  : TColor = clNavy;
  coSecondColor : TColor = clBlue;
  coScrollPen   : TColor = clNavy;

{ Definition der Klasse TVerLaborPage }
type
  TVerLaborPage = class(TKD4Page)
  private
    GrayFont    : TDirectFont;
    fRedrawRect : TRect;
    procedure DrawYourForscher(Sender: TDXComponent;Surface: TDirectDrawSurface;const Mem: TDDSurfaceDesc;Rect: TRect; Index: Integer;Selected: boolean);
    procedure DrawProject(Sender: TDXComponent;Surface: TDirectDrawSurface;const Mem: TDDSurfaceDesc;Rect: TRect; Index: Integer;Selected: boolean);
    procedure SetButtons;

    procedure MoveProject(Sender: TObject);
    procedure CancelProject(Sender: TObject);
    { Private-Deklarationen }
  protected
    procedure MarktClick(Sender: TObject);
    procedure SetProjekt(Sender: TObject);
    procedure SetAllProjekt(Sender: TObject);
    procedure SetFreeProjekt(Sender: TObject);
    procedure SetAusbildung(Sender: TObject);
    procedure SetAusbildungAll(Sender: TObject);
    procedure SetAusbildungFree(Sender: TObject);
    procedure LaborDblClick(Sender: TObject;Button: TMouseButton;Shift: TShiftState;x,y: Integer);
    procedure DrawFortSchritt(Surface: TDirectDrawSurface;const Mem: TDDSurfaceDesc;X,Y,Width: Integer;Prozent: double; Valid: boolean);
    procedure ForscherChange(Sender: TObject);
    procedure ProjektChange(Sender: TObject);
    procedure OnSelectedBasisChange(Sender: TObject);
    procedure AktuForscherList;
    procedure AktuProjectList;
    function GetSelectedIndex: Integer;
    function GetSelectedProjekt: Integer;
    { Protected-Deklarationen }
  public
    ArbeitsButton : TDXBitmapButton;
    Zuweisen      : TDXBitmapButton;
    ZuweisenAll   : TDXBitmapButton;
    ZuweisenFree  : TDXBitmapButton;
    Ausbilden     : TDXBitmapButton;
    AusbildenAll  : TDXBitmapButton;
    AusbildFree   : TDXBitmapButton;
    CancelButton  : TDXBitmapButton;
    InfoFrame     : TDXInfoFrame;
    Forscher      : TDXListBox;
    ProjectList   : TDXListBox;
    TextViewer    : TDXTextViewer;
    constructor Create(Container: TDXContainer);override;
    procedure LeaveModal;override;
    procedure SetDefaults;override;
  end;

implementation

uses
  game_api, basis_api;

{ TVerLaborPage }

procedure TVerLaborPage.AktuForscherList;
var
  Dummy: Integer;
begin
  Container.Lock;
  Forscher.Items.Clear;
  for Dummy:=0 to SaveGame.ForschListe.ForscherCount-1 do
  begin
    if SaveGame.ForschListe.ForscherInSelectedBasis(Dummy) then
      Forscher.Items.Add(IntToStr(Dummy));
  end;
  Container.LoadGame(False);
  Forscher.Redraw;
  Zuweisen.Enabled:=(not (Forscher.Items.Count=0)) and (not (ProjectList.Items.Count=0));
  ZuweisenAll.Enabled:=(not (Forscher.Items.Count=0)) and (not (ProjectList.Items.Count=0));
  ZuweisenFree.Enabled:=(not (Forscher.Items.Count=0)) and (not (ProjectList.Items.Count=0));
  Ausbilden.Enabled:=not (Forscher.Items.Count=0);
  AusbildenAll.Enabled:=not (Forscher.Items.Count=0);
  AusbildFree.Enabled:=not (Forscher.Items.Count=0);
  SetButtons;
  Container.DecLock;
  Container.DoFlip;
end;

procedure TVerLaborPage.AktuProjectList;
var
  Dummy: Integer;
begin
  ProjectList.ChangeItem(true);
  ProjectList.Items.Clear;

  for Dummy:=0 to SaveGame.ForschListe.ProjektCount-1 do
    if SaveGame.ForschListe.GetProjektInfo(Dummy).Started then
      ProjectList.Items.Add(IntToStr(Dummy));

  AktuForscherList;
  CancelButton.Enabled:=not (ProjectList.Items.Count=0);

  ProjectList.ChangeItem(false);
  ProjectList.Items.OnChange(Self);
  ProjektChange(Self);
  ProjectList.Redraw;
end;

procedure TVerLaborPage.CancelProject(Sender: TObject);
var
  Index: Integer;
begin
  Index:=GetSelectedProjekt;
  if Index=-1 then
    exit;
  try
    SaveGame.ForschListe.CancelResearch(Index);
    AktuProjectList;
  except
    on E: Exception do
      game_api_MessageBox(E.Message,CError);
  end;
end;

constructor TVerLaborPage.Create(Container: TDXContainer);
var
  Font: TFont;
begin
  inherited;
  Animation:=paRightToLeft;
  HintLabel.BlendColor:=bcBlue;
  HintLabel.BorderColor:=coFirstColor;

  SetPageInfo(StripHotKey(BLabor));

  { Erstellen der grauen Schriftart f�r Projekte in anderen Basen }
  Font:=TFont.Create;
  Font.Size:=coFontSize;
  Font.Name:=coFontName;
  Font.Color:=clGray;
  GrayFont:=FontEngine.FindDirectFont(Font,clBlack);
  Font.Free;

  { Informationsframe }
  InfoFrame:=TDXInfoFrame.Create(Self);
  with InfoFrame do
  begin
    SetRect(388,10,244,70);
    SetFont(Font);
    ColorSheme:=icsBlue;
    Infos:=[ifCredits,ifLabor];
    SystemKeyChange:=true;
  end;

  { Arbeitsmarkt Button }
  ArbeitsButton:=TDXBitmapButton.Create(Self);
  with ArbeitsButton do
  begin
    SetRect(682,530,110,28);
    FirstColor:=coFirstColor;
    SecondColor:=coSecondColor;
    RoundCorners:=rcAll;
    AccerlateColor:=coAccerlateColor;
    SetButtonFont(Font);
    OnClick:=MarktClick;
    Text:=BVerMarkt;
    Hint:=HArbeitsmarkt;
    BlendColor:=bcNavy;
  end;

{ Ihre Forscher }
  { Forscherlistbox }
  Forscher:=TDXListBox.Create(Self);
  with Forscher do
  begin
    SetRect(8,54,370,469);
    BorderColor:=coFirstColor;
    FocusColor:=coSecondColor;
    SetFont(Font);
    SetButtonFont(ScrollBarFont);
    FirstColor:=bcBlue;
    SecondColor:=coScrollPen;
    Hint:=HForscherList;
    ItemHeight:=61;
    OwnerDraw:=true;
    OnDrawItem:=DrawYourForscher;
    OnDblClick:=LaborDblClick;
    OnChange:=ForscherChange;
    HeadRowHeight:=17;
    HeadBlendColor:=bcBlue;
    HeadData:=MakeHeadData(LYourForsch,FForsch,FForsch);
  end;

{ Projekte }
  { ListBox }
  ProjectList:=TDXListBox.Create(Self);
  with ProjectList do
  begin
    SetFont(Font);
    setButtonFont(ScrollBarFont);
    SetRect(508,54,284,354);
    BorderColor:=coFirstColor;
    FocusColor:=coSecondColor;
    FirstColor:=bcBlue;
    SecondColor:=coScrollPen;
    ItemHeight:=57;
    Hint:=HProjektList;
    RoundCorners:=rcTop;
    OwnerDraw:=true;
    OnDrawItem:=DrawProject;
    OnChange:=ProjektChange;
    HeadRowHeight:=17;
    HeadBlendColor:=bcBlue;
    HeadData:=MakeHeadData(LProjekte,FProjekt,FProjekte);
  end;

  { Textbetrachter f�r Infotext }
  TextViewer:=TDXTextViewer.Create(Self);
  with TextViewer do
  begin
    SetRect(508,409,284,114);
    SetFont(Font);
    BlendColor:=bcBlue;
    BorderColor:=coFirstColor;
    RoundCorners:=rcBottom;
    Blending:=true;
  end;

{ Buttons }
  { Zuweisen }
  Zuweisen:=TDXBitmapButton.Create(Self);
  with Zuweisen do
  begin
    SetRect(388,54,110,28);
    SetButtonFont(Font);
    Hint:=HZuweisen;
    FirstColor:=coFirstColor;
    SecondColor:=coSecondColor;
    AccerlateColor:=coAccerlateColor;
    OnClick:=SetProjekt;
    RoundCorners:=rcTop;
    Text:=BLZuweisen;
    BlendColor:=bcNavy;
  end;

  { Alle Zuweisen }
  ZuweisenAll:=TDXBitmapButton.Create(Self);
  with ZuweisenAll do
  begin
    SetRect(388,83,110,28);
    SetButtonFont(Font);
    Hint:=HZuweisenAll;
    FirstColor:=coFirstColor;
    SecondColor:=coSecondColor;
    AccerlateColor:=coAccerlateColor;
    OnClick:=SetAllProjekt;
    RoundCorners:=rcNone;
    Text:=BLZuweisenAll;
    BlendColor:=bcNavy;
  end;

  { Freie Zuweisen }
  ZuweisenFree:=TDXBitmapButton.Create(Self);
  with ZuweisenFree do
  begin
    SetRect(388,112,110,28);
    SetButtonFont(Font);
    Hint:=HZuweisenFree;
    FirstColor:=coFirstColor;
    SecondColor:=coSecondColor;
    RoundCorners:=rcBottom;
    AccerlateColor:=coAccerlateColor;
    OnClick:=SetFreeProjekt;
    Text:=BLZuweisenFree;
    BlendColor:=bcNavy;
  end;

  { Ausbilden }
  Ausbilden:=TDXBitmapButton.Create(Self);
  with Ausbilden do
  begin
    SetRect(388,154,110,28);
    SetButtonFont(Font);
    Hint:=HAusbildung;
    FirstColor:=coFirstColor;
    SecondColor:=coSecondColor;
    AccerlateColor:=coAccerlateColor;
    RoundCorners:=rcTop;
    OnClick:=SetAusbildung;
    Text:=BLAusbilden;
    BlendColor:=bcNavy;
  end;

  { Alle Ausbilden }
  AusbildenAll:=TDXBitmapButton.Create(Self);
  with AusbildenAll do
  begin
    SetRect(388,183,110,28);
    SetButtonFont(Font);
    Hint:=HAusbildungAll;
    FirstColor:=coFirstColor;
    SecondColor:=coSecondColor;
    RoundCorners:=rcNone;
    AccerlateColor:=coAccerlateColor;
    OnClick:=SetAusbildungAll;
    Text:=BLAusbildenAll;
    BlendColor:=bcNavy;
  end;

  { Freie Ausbilden }
  AusbildFree:=TDXBitmapButton.Create(Self);
  with AusbildFree do
  begin
    SetRect(388,212,110,28);
    SetButtonFont(Font);
    Hint:=HAusbildungFree;
    FirstColor:=coFirstColor;
    SecondColor:=coSecondColor;
    RoundCorners:=rcBottom;
    AccerlateColor:=coAccerlateColor;
    OnClick:=SetAusbildungFree;
    Text:=BLAusbildenFree;
    BlendColor:=bcNavy;
  end;

  { Projekt starten Button }
  with TDXBitmapButton.Create(Self) do
  begin
    SetRect(388,252,110,28);
    SetButtonFont(Font);
    FirstColor:=coFirstColor;
    SecondColor:=coSecondColor;
    AccerlateColor:=coAccerlateColor;
    Text:=ST0401200001;
    Hint:=ST0401200002;
    PageIndex:=PageVerUpgrades;
    BlendColor:=bcNavy;
    RoundCorners:=rcTop;
  end;

  { Nach Oben Button }
  with TDXBitmapButton.Create(Self) do
  begin
    SetRect(388,281,110,28);
    SetButtonFont(Font);
    FirstColor:=coFirstColor;
    SecondColor:=coSecondColor;
    AccerlateColor:=coAccerlateColor;
    Text:=ST0402040001;
    Hint:=ST0402040003;
    BlendColor:=bcNavy;
    RoundCorners:=rcNone;
    OnClick:=MoveProject;
    Tag:=Integer(false);
  end;

  { Nach Unten Button }
  with TDXBitmapButton.Create(Self) do
  begin
    SetRect(388,310,110,28);
    SetButtonFont(Font);
    FirstColor:=coFirstColor;
    SecondColor:=coSecondColor;
    AccerlateColor:=coAccerlateColor;
    Text:=ST0402040002;
    Hint:=ST0402040004;
    RoundCorners:=rcNone;
    BlendColor:=bcNavy;
    OnClick:=MoveProject;
    Tag:=Integer(true);
  end;

  { Abbrechen Button }
  CancelButton:=TDXBitmapButton.Create(Self);
  with CancelButton do
  begin
    SetRect(388,339,110,28);
    SetButtonFont(Font);
    FirstColor:=coFirstColor;
    SecondColor:=coSecondColor;
    AccerlateColor:=coAccerlateColor;
    Text:=BbCancel;
    Hint:=ST0402160001;
    RoundCorners:=rcBottom;
    BlendColor:=bcNavy;
    OnClick:=CancelProject;
  end;

  UnionRect(fRedrawRect,Zuweisen.ClientRect,AusbildFree.ClientRect);

  basis_api_RegisterChangeBaseHandler(OnSelectedBasisChange);
end;

procedure TVerLaborPage.DrawFortSchritt(Surface: TDirectDrawSurface;const Mem: TDDSurfaceDesc; X, Y,
  Width: Integer; Prozent: double; Valid: boolean);
var
  Links  : Integer;
  Oben   : Integer;
  Full   : boolean;
  Zeiger : Cardinal;
  Farbe  : Cardinal;
  Beginn : Integer;
  Ende   : Integer;
begin
  Beginn:=0;
  Ende:=16;
  if Surface.ClippingRect.Top>Y then Beginn:=Surface.ClippingRect.Top-Y;
  if Surface.ClippingRect.Bottom<Y+16 then Ende:=min(16,Surface.ClippingRect.Bottom-Y-1);
  if Mode32Bit then
  begin
    for Links:=0 to Width-1 do
    begin
      if Valid then
        Farbe:=ShieldColorTable[100-round(Links/Width*100)]
      else
        Farbe:=TimeColorTable[100-round(Links/Width*100)];
      Full:=round(Prozent/100*Width)<=Links;
      Zeiger:=Integer(Mem.lpSurface)+((Y+Beginn)*Mem.lPitch)+((X+Links) shl 2);
      for Oben:=Beginn to Ende do
      begin
        if not Full then
          PLongWord(Zeiger)^:=Farbe
        else
          PLongWord(Zeiger)^:=((Farbe and Mask) shr 1);
        inc(Zeiger,Mem.lPitch)
      end;
    end;
  end
  else
  begin
    for Links:=0 to Width-1 do
    begin
      if Valid then
        Farbe:=ShieldColorTable[100-round(Links/Width*100)]
      else
        Farbe:=TimeColorTable[100-round(Links/Width*100)];
      Full:=round(Prozent/100*Width)<=Links;
      Zeiger:=Integer(Mem.lpSurface)+((Y+Beginn)*Mem.lPitch)+((X+Links) shl 1);
      for Oben:=Beginn to Ende do
      begin
        if not Full then
          PWord(Zeiger)^:=Farbe
        else
          PWord(Zeiger)^:=(Farbe and Mask) shr 1;
        inc(Zeiger,Mem.lPitch)
      end;
    end;
  end;
end;

procedure TVerLaborPage.DrawProject(Sender: TDXComponent;
  Surface: TDirectDrawSurface;const Mem: TDDSurfaceDesc; Rect: TRect; Index: Integer;
  Selected: boolean);
var
  Text                 : String;
  Font                 : TDirectFont;
  BColor               : TBlendColor;
  Valid                : boolean;
  ProRect              : TRect;
  ItemIndex            : Integer;
  DummyHours           : Single;
  DummyProjektStrength : Integer;
begin
  ItemIndex:=StrToInt(ProjectList.Items[Index]);
  Valid:=SaveGame.ForschListe.ProjektInSelectedBasis(ItemIndex);
  if Selected then
  begin
    ProjectList.DrawSelection(Surface,Mem,Rect,bcBlue);
  end;
  if Valid then
  begin
    Font:=YellowStdFont;
    BColor:=bcNavy;
  end
  else
  begin
    Font:=GrayFont;
    BColor:=bcGray;
  end;
  Font.Draw(Surface,Rect.Left+6,Rect.Top+6,SaveGame.ForschListe.ProjektName[ItemIndex]);
  { Zeichnen des Fortschrittes }
  ProRect:=Classes.Rect(Rect.Left+7,Rect.Top+33,Rect.Right-7,Rect.Bottom-5);
  IntersectRect(ProRect,ProRect,Surface.ClippingRect);
  Rectangle(Surface,Mem,ProRect,BColor);
  DrawFortschritt(Surface,Mem,Rect.Left+8,Rect.Top+34,ProjectList.Width-16,SaveGame.ForschListe.PercentDone[ItemIndex],Valid);
  if Valid then
  begin
    DummyProjektStrength := SaveGame.ForschListe.ProjektStrength(ItemIndex);
    if DummyProjektStrength=0 then
      Text:=MK0505040001 //wird nicht erforscht
    else
    begin
      DummyHours := SaveGame.ForschListe.GetProjektInfo(ItemIndex).Hour;
      DummyHours := DummyHours / (DummyProjektStrength / 80);  //normalisiert
      if DummyHours>=48 then
      begin
        Text:=Format(MK0505040002, [Trunc(DummyHours/24)]); //ca. XX Tage
      end
      else if DummyHours>=24 then
      begin
        Text:=MK0505040003; //ca. Tag
      end else if DummyHours>2 then
      begin
        Text:=Format(MK0505040004, [Trunc(DummyHours)]); //ca. XX Stunden
      end else
        Text:=MK0505040005; //kurz vorm Durchbruch
    end;
  end
  else
    Text:=SaveGame.ForschListe.ProjektBasis[ItemIndex].Name;
  Font.Draw(Surface,Rect.Right-10-Font.TextWidth(text),Rect.Top+35,Text);
end;

procedure TVerLaborPage.DrawYourForscher(Sender: TDXComponent;
  Surface: TDirectDrawSurface;const Mem: TDDSurfaceDesc; Rect: TRect; Index: Integer;
  Selected: boolean);
var
  Text   : String;
  Font   : TDirectFont;
  SIndex : Integer;
begin
  SIndex:=StrToInt(Forscher.Items[Index]);
  Font:=YellowStdFont;
  if Selected then Forscher.DrawSelection(Surface,Mem,Rect,bcBlue);
  SaveGame.ForschListe.DrawDXForscher(SIndex,Surface,Mem,Rect.Left+5,Rect.Top+5);
  Font.Draw(Surface,Rect.Left+48,Rect.Top+5,SaveGame.ForschListe.Forscher[SIndex].Name);
  Text:=Format(FFaehig,[trunc(SaveGame.ForschListe.Forscher[SIndex].Strength)/1]);
  Font.Draw(Surface,Rect.Right-6-Font.TextWidth(Text),Rect.Top+5,Text);
  if SaveGame.ForschListe.Forscher[SIndex].Train then
  begin
    Text:=LTraining;
    Font:=GreenStdFont;
  end
  else if SaveGame.ForschListe.ForscherProjekt[SIndex]<>-1 then Text:=SaveGame.ForschListe.ProjektName[SaveGame.ForschListe.ForscherProjekt[SIndex]]
  else
  begin
    Text:=LNoProjekt;
    Font:=RedStdFont;
  end;
  Font.Draw(Surface,Rect.Left+48,Rect.Top+30,Text);
end;

procedure TVerLaborPage.ForscherChange(Sender: TObject);
begin
  SetButtons;
end;

function TVerLaborPage.GetSelectedIndex: Integer;
begin
  if Forscher.Items.Count=0 then
    result:=-1
  else
    result:=StrToInt(Forscher.Items[Forscher.ItemIndex]);
end;

function TVerLaborPage.GetSelectedProjekt: Integer;
begin
  if ProjectList.Items.Count=0 then
    result:=-1
  else
    result:=StrToInt(ProjectList.Items[ProjectList.ItemIndex]);
end;

procedure TVerLaborPage.LaborDblClick(Sender: TObject;
  Button: TMouseButton;Shift: TShiftState; x, y: Integer);
var
  Index: Integer;
  Dummy: Integer;
begin
  if SaveGame.ForschListe.ForscherCount>0 then
  begin
    Index:=SaveGame.ForschListe.ForscherProjekt[GetSelectedIndex];
    if Index<>-1 then
    begin
      for Dummy:=0 to ProjectList.Items.Count-1 do
      begin
        if StrToInt(ProjectList.Items[Dummy])=Index then
          ProjectList.ItemIndex:=Dummy;
      end;
    end
  end;
end;

procedure TVerLaborPage.LeaveModal;
begin
  SetDefaults;
end;

procedure TVerLaborPage.MarktClick(Sender: TObject);
begin
  ActivePage:=ptForscher;
  ChangePage(PageVerMarkt);
end;

procedure TVerLaborPage.MoveProject(Sender: TObject);
begin
  if ProjectList.ItemIndex<>-1 then
  begin
    SaveGame.ForschListe.MoveProject(GetSelectedProjekt,Boolean((Sender as TDXBitmapButton).Tag));
    Container.Lock;
    if Boolean((Sender as TDXBitmapButton).Tag) then
      ProjectList.ItemIndex:=ProjectList.ItemIndex+1
    else
      ProjectList.ItemIndex:=ProjectList.ItemIndex-1;
    Container.UnLock;
    AktuProjectList;
  end;
end;

procedure TVerLaborPage.OnSelectedBasisChange(Sender: TObject);
begin
  if Container.ActivePage<>Self then
    exit;

  AktuForscherList;
  ProjectList.Redraw;
end;

procedure TVerLaborPage.ProjektChange(Sender: TObject);
begin
  if GetSelectedProjekt=-1 then
  begin
    TextViewer.Text:='';
    exit;
  end;
  TextViewer.Text:=SaveGame.ForschListe.ProjektText[GetSelectedProjekt];
end;

procedure TVerLaborPage.SetAllProjekt(Sender: TObject);
var
  Dummy: Integer;
begin
  Container.LoadGame(true);
  try
    for Dummy:=0 to Forscher.Items.Count-1 do
      SaveGame.ForschListe.ForscherProjekt[StrToInt(Forscher.Items[Dummy])]:=GetSelectedProjekt;
  except
    on E:Exception do
    begin
      Container.LoadGame(false);
      game_api_MessageBox(E.Message,CCancelAction);
      Container.LoadGame(true);
    end;
  end;
  Container.LoadGame(false);
  Container.IncLock;
  SetButtons;
  Forscher.Redraw;
  ProjectList.Redraw;
  Container.DecLock;
  InfoFrame.Redraw;
end;

procedure TVerLaborPage.SetAusbildung(Sender: TObject);
begin
  try
    SaveGame.ForschListe.ChangeTraining(GetSelectedIndex);
  except
    on E:Exception do
      game_api_MessageBox(E.Message,CCancelAction);
  end;
  Container.Lock;
  Forscher.ItemIndex:=Forscher.ItemIndex+1;
  Container.LoadGame(false);
  Forscher.Redraw;
  InfoFrame.Redraw;
  SetButtons;
  Container.DecLock;
  ProjectList.Redraw;
end;

procedure TVerLaborPage.SetAusbildungAll(Sender: TObject);
var
  Dummy  : Integer;
  Change : boolean;
begin
  Change:=true;
  for Dummy:=0 to Forscher.Items.Count-1 do
  begin
    if not SaveGame.ForschListe.Forscher[StrToInt(Forscher.Items[Dummy])].Train then Change:=false;
  end;
  Container.LoadGame(true);
  for Dummy:=0 to Forscher.Items.Count-1 do
  begin
    try
      if Change then
        SaveGame.ForschListe.ChangeTraining(StrToInt(Forscher.Items[Dummy]))
      else
        SaveGame.ForschListe.SetTraining(StrToInt(Forscher.Items[Dummy]));
    except
{      on E:Exception do
      begin
        Container.LoadGame(false);
        game_api_MessageBox(E.Message,CCancelAction);
        Container.LoadGame(true);
      end;}
    end;
  end;
  Container.LoadGame(false);
  Container.IncLock;
  Forscher.Redraw;
  SetButtons;
  ProjectList.Redraw;
  Container.DecLock;
  InfoFrame.Redraw;
end;

procedure TVerLaborPage.SetAusbildungFree(Sender: TObject);
var
  Dummy  : Integer;
  SIndex : Integer;
begin
  Container.LoadGame(true);
  for Dummy:=0 to Forscher.Items.Count-1 do
  begin
    try
      SIndex:=StrToInt(Forscher.Items[Dummy]);
      if SaveGame.ForschListe.ForscherProjekt[SIndex]=-1 then
        SaveGame.ForschListe.SetTraining(SIndex);
    except
      on E:Exception do
      begin
        Container.LoadGame(false);
        game_api_MessageBox(E.Message,CCancelAction);
        Container.LoadGame(true);
      end;
    end;
  end;
  Container.LoadGame(false);
  Container.IncLock;
  SetButtons;
  Forscher.Redraw;
  ProjectList.Redraw;
  Container.DecLock;
  InfoFrame.Redraw;
end;

procedure TVerLaborPage.SetButtons;
var
  Dummy       : Integer;
  AllTraining : boolean;
  NobodyFree  : boolean;

begin
  if Forscher.Items.Count=0 then exit;
  Container.Lock;
  AllTraining:=true;
  Dummy:=0;
  while (Dummy<SaveGame.ForschListe.ForscherCount) and AllTraining do
  begin
    if SaveGame.ForschListe.ForscherInSelectedBasis(Dummy) then
    begin
      if not SaveGame.ForschListe.Forscher[Dummy].Train then
        AllTraining:=false;
    end;
    inc(Dummy);
  end;
  if SaveGame.ForschListe.Forscher[Forscher.ItemIndex].Train then
  begin
    Ausbilden.Text:=BLFree;
    Ausbilden.Hint:=HFFree;
  end
  else
  begin
    Ausbilden.Text:=BLAusbilden;
    Ausbilden.Hint:=HAusbildung;
  end;
  if AllTraining then
  begin
    AusbildenAll.Text:=BLFreeAll;
    AusbildenAll.Hint:=HFFreeAll;
  end
  else
  begin
    AusbildenAll.Text:=BLAusbildenAll;
    AusbildenAll.Hint:=HAusbildungAll;
  end;

  NobodyFree:=true;
  Dummy:=0;
  while (Dummy<SaveGame.ForschListe.ForscherCount) and NobodyFree do
  begin
    if SaveGame.ForschListe.ForscherInSelectedBasis(Dummy) then
    begin
      if not SaveGame.ForschListe.Forscher[Dummy].Train and
        not (SaveGame.ForschListe.ForscherProjekt[Dummy]>=0) then
      begin
        NobodyFree:=false;
      end;
    end;
    inc(Dummy);
  end;
  if NobodyFree then
  begin
    AusbildFree.Enabled:=false;
    ZuweisenFree.Enabled:=false;
  end
  else
  begin
    AusbildFree.Enabled:=true;
    ZuweisenFree.Enabled:=true;
  end;

  Container.Unlock;
  Container.RedrawArea(fRedrawRect,Container.Surface);
end;

procedure TVerLaborPage.SetDefaults;
begin
  InfoFrame.Reset;
  AktuProjectList;
  AktuForscherList;
end;

procedure TVerLaborPage.SetFreeProjekt(Sender: TObject);
var
  Dummy  : Integer;
  SIndex : Integer;
begin
  Container.LoadGame(true);
  try
    for Dummy:=0 to Forscher.Items.Count-1 do
    begin
      SIndex:=StrToInt(Forscher.Items[Dummy]);
      if (SaveGame.ForschListe.ForscherProjekt[SIndex]=-1) and
        not (SaveGame.ForschListe.Forscher[SIndex].Train) then
      begin
        SaveGame.ForschListe.ForscherProjekt[SIndex]:=GetSelectedProjekt;
      end;
    end;
  except
    on E:Exception do
    begin
      Container.LoadGame(false);
      game_api_MessageBox(E.Message,CCancelAction);
      Container.LoadGame(true);
    end;
  end;
  Container.LoadGame(false);
  Container.IncLock;
  SetButtons;
  Forscher.Redraw;
  ProjectList.Redraw;
  Container.DecLock;
  InfoFrame.Redraw;
end;

procedure TVerLaborPage.SetProjekt(Sender: TObject);
begin
  try
    SaveGame.ForschListe.ForscherProjekt[GetSelectedIndex]:=GetSelectedProjekt;
  except
    on E:Exception do
    begin
      game_api_MessageBox(E.Message,CCancelAction);
      exit;
    end;
  end;
  Container.Lock;
  Forscher.ItemIndex:=Forscher.ItemIndex+1;
  Container.LoadGame(false);
  Forscher.Redraw;
  InfoFrame.Redraw;
  SetButtons;
  Container.DecLock;
  ProjectList.Redraw;
end;

end.
