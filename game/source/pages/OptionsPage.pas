{********************************************************************************
*										*
* Copyright (C) 2004  X-Force Team - jim_raynor@web.de				*
*										*
* This file is part of X-Force: Fight For Destiny (X-Force).			*
*										*
* X-Force is free software; you can redistribute it and/or modify		*
* it under the terms of the GNU General Public License as published by		*
* the Free Software Foundation; either version 2 of the License, or		*
* (at your option) any later version.						*
*										*
* X-Force is distributed in the hope that it will be useful,			*
* but WITHOUT ANY WARRANTY; without even the implied warranty of		*
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the			*
* GNU General Public License for more details.					*
*										*
* You should have received a copy of the GNU General Public License		*
* along with X-Force; if not, write to the Free Software			*
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA	*
*										*
*********************************************************************************
*										*
* Optionsseite mit den Einstellungen f�r den Spielstand (Nachrichten, Spielstand*
* speichern)									*
*										*
********************************************************************************}

{$I ../Settings.inc}

unit OptionsPage;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  DXContainer, KD4Page, DXBitmapButton, Defines, DXEdit, DXListBox, ArchivFile,
  XForce_types,Koding, DXDraws, Blending, DXLabel, DXFrameBox, DXSpinEdit, DXCheckBox,
  DXCheckList, KD4Utils, DXGroupCaption, DirectDraw, DirectFont,
  StringConst;

const
  coFirstColor   : TBlendColor = clNavy;
  coSecondColor  : TBlendColor = clBlue;
  coScrollPen    : TBlendColor = clNavy;

type
  TOptionsPage = class(TKD4Page)
  private
    fArray    : Array of TGameStatus;
    fAreaRect : TRect;
    procedure DrawSaveGame(Sender: TDXComponent;Surface: TDirectDrawSurface;const Mem: TDDSurfaceDesc;Rect: TRect; Index: Integer;Selected: boolean);
    procedure SaveSettings(Sender: TObject);
    procedure ChangeChecked(Sender: TObject);
    procedure ListBoxChange(Sender: TObject);
    procedure NameChange(Sender: TObject);
    procedure LoadClick(Sender: TObject);
    procedure SaveClick(Sender: TObject);
    procedure ListDblClick(Sender: TObject;Button: TMouseButton;Shift: TShiftState;X,Y: Integer);
    procedure AcDeacAll(Sender: TObject);
    procedure InitTexte;
    procedure InitChecked;
    procedure AddInfoMessageBoxOption(Text,Hint: String; Message: TLongMessage);
    { Private-Deklarationen }
  protected
    { Protected-Deklarationen }
  public
    CancelButton    : TDXBitmapButton;
    OKButton        : TDXBitmapButton;
    SaveGameName    : TDXEdit;
    NameList        : TDXListBox;
    SaveGameButton  : TDXBitmapButton;
    LoadGameButton  : TDXBitmapButton;
    MaxMess         : TDXSpinEdit;
    MonthBilanz     : TDXCheckBox;
    CheckList       : TDXCheckList;
    DeactAll        : TDXBitmapButton;
    ActAll          : TDXBitmapButton;
    constructor Create(Container: TDXContainer);override;
    procedure SetDefaults;override;
  end;

implementation

uses
  game_api;

{ TOptionsPage }

procedure TOptionsPage.AcDeacAll(Sender: TObject);
var
  Activate: boolean;
  Dummy: Integer;
begin
  Container.Lock;
  Activate:=Sender=ActAll;
  for Dummy:=0 to CheckList.Items.Count-1 do
  begin
    CheckList.Checked[Dummy]:=Activate;
  end;
  Container.LoadGame(false);
  ChangeChecked(Self);
  Container.DecLock;
  CheckList.Redraw;
end;

procedure TOptionsPage.AddInfoMessageBoxOption(Text, Hint: String;
  Message: TLongMessage);
begin
  CheckList.AddItem(Text,Hint);
  CheckList.Items.Objects[CheckList.Items.Count-1]:=TObject(Message);
end;

procedure TOptionsPage.ChangeChecked(Sender: TObject);
var
  Dummy   : Integer;
  Checked : Integer;
begin
  Checked:=0;
  for Dummy:=0 to CheckList.Items.Count-1 do
    if CheckList.Checked[Dummy] then inc(Checked);

  ActAll.Enabled:=not (Checked=CheckList.Items.Count);
  DeactAll.Enabled:=not (Checked=0);
end;

constructor TOptionsPage.Create(Container: TDXContainer);
begin
  inherited;
  Animation:=paLeftToRight;
  HintLabel.BorderColor:=coFirstColor;
  HintLabel.BlendColor:=bcNavy;

  { Initialisieren des OK Buttons }
  OKButton:=TDXBitmapButton.Create(Self);
  with OKButton do
  begin
    SetRect(524,530,110,28);
    SetButtonFont(Font);
    Text:=BOK;
    FirstColor:=coFirstColor;
    SecondColor:=coSecondColor;
    BlendColor:=bcNavy;
    AccerlateColor:=coAccerlateColor;
    RoundCorners:=rcLeft;
    OnClick:=SaveSettings;
    Hint:=HOKButton;
  end;

  { Initialisieren des Abbrechen Buttons }
  CancelButton:=TDXBitmapButton.Create(Self);
  with CancelButton do
  begin
    SetRect(635,530,110,28);
    SetButtonFont(Font);
    Text:=BCancel;
    FirstColor:=coFirstColor;
    RoundCorners:=rcRight;
    SecondColor:=coSecondColor;
    AccerlateColor:=coAccerlateColor;
    BlendColor:=bcNavy;
    EscButton:=true;
    PageIndex:=PageGameMenu;
    Hint:=HCancelButton;
  end;

  { Label 'Spiel speichern' }
  with TDXGroupCaption.Create(Self) do
  begin
    SetRect(16,16,300,26);
    SetFont(Font);
    RoundCorners:=rcTop;
    BlendColor:=bcNavy;
    Font.Size:=Font.Size+4;
    Font.Style:=[fsBold];
    Caption:=CSaveGame;
    BorderColor:=clNavy;
  end;

  { Editor zum eingeben vom Savegamenamen }
  SaveGameName:=TDXEdit.Create(Self);
  with SaveGameName do
  begin
    SetRect(16,43,300,23);
    SetButtonFont(Font);
    BorderColor:=coFirstColor;
    FocusColor:=coSecondColor;
    MaxLength:=20;
    BlendColor:=bcNavy;
    FileName:=true;
    Hint:=HEditSaveGame;
    OnChange:=NameChange;
    RoundCorners:=rcNone;
  end;

  { Liste mit verf�gbaren SaveGames }
  NameList:=TDXListBox.Create(Self);
  with NameList do
  begin
    SetRect(16,67,300,400);
    BorderColor:=coFirstColor;
    FocusColor:=coSecondColor;
    SetFont(Font);
    SetButtonFont(ScrollBarFont);
    FirstColor:=bcNavy;
    SecondColor:=coScrollPen;
    RoundCorners:=rcNone;
    ItemHeight:=45;
    Hint:=HListSaveGame;
    LeftMargin:=5;
    TopMargin:=3;
    OnChange:=ListBoxChange;
    AlwaysChange:=true;
    OwnerDraw:=true;
    OnDrawItem:=DrawSaveGame;
    Items.Duplicates:=dupAccept;
    OnDblClick:=ListDblClick;
  end;

  { Spiel speichern Button }
  SaveGameButton:=TDXBitmapButton.Create(Self);
  with SaveGameButton do
  begin
    SetRect(16,NameList.Bottom+1,149,28);
    SetButtonFont(Font);
    FirstColor:=coFirstColor;
    SecondColor:=coSecondColor;
    AccerlateColor:=coAccerlateColor;
    RoundCorners:=rcLeftBottom;
    Text:=BSaveGame;
    Hint:=HSaveOK;
    BlendColor:=bcNavy;
    OnClick:=SaveClick;
  end;

  { Spiel laden Button }
  LoadGameButton:=TDXBitmapButton.Create(Self);
  with LoadGameButton do
  begin
    SetRect(SaveGameButton.Right+1,NameList.Bottom+1,150,28);
    SetButtonFont(Font);
    FirstColor:=coFirstColor;
    SecondColor:=coSecondColor;
    AccerlateColor:=coAccerlateColor;
    RoundCorners:=rcRightBottom;
    Text:=BLoadGame;
    Hint:=HLoadGame;
    BlendColor:=bcNavy;
    OnClick:=LoadClick;
  end;

  { Label 'Einstellungen' }
  with TDXGroupCaption.Create(Self) do
  begin
    SetRect(435,16,310,26);
    SetFont(Font);
    BorderColor:=clNavy;
    Font.Size:=Font.Size+4;
    Font.Style:=[fsBold];
    RoundCorners:=rcTop;
    BlendColor:=bcNavy;
    Caption:=CSettings;
  end;

  { Nachrichten-Label }
  with TDXGroupCaption.Create(Self) do
  begin
    SetRect(435,43,210,22);
    SetFont(Font);
    Caption:=LMaxMessages;
    BorderColor:=clNavy;
    BlendColor:=bcNavy;
    RoundCorners:=rcNone;
  end;

  { Maximale Anzahl an Nachrichten }
  MaxMess:=TDXSpinEdit.Create(Self);
  with MaxMess do
  begin
    SetRect(646,43,99,22);
    Min:=5;
    Max:=25;
    BlendColor:=bcNavy;
    BorderColor:=coFirstColor;
    RoundCorners:=rcNone;
    FocusColor:=coSecondColor;
    TopMargin:=5;
    Hint:=HMaxMessage;
    SetButtonFont(Font);
  end;

  { Monatsbilanz am Wochenende }
  MonthBilanz:=TDXCheckbox.Create(Self);
  with MonthBilanz do
  begin
    SetRect(435,66,310,23);
    BorderColor:=coFirstColor;
    SetButtonFont(Font);
    BlendColor:=bcNavy;
    RoundCorners:=rcNone;
    Caption:=BPunkteStat;
    Hint:=HNMonthBilanz;
    LeftMargin:=6;
  end;

  { Label 'Nachrichtenfenster f�r' }
  with TDXGroupCaption.Create(Self) do
  begin
    SetRect(435,90,310,26);
    SetFont(Font);
    Font.Size:=Font.Size+4;
    Font.Style:=[fsBold];
    BorderColor:=clNavy;
    Caption:=LMessage;
    RoundCorners:=rcNone;
    BlendColor:=bcNavy;
  end;

  { Checklist f�r Nachrichten Einstellungen }
  CheckList:=TDXCheckList.Create(Self);
  with CheckList do
  begin
    SetRect(435,117,310,350);
    RoundCorners:=rcNone;
    BorderColor:=coFirstColor;
    FocusColor:=coSecondColor;
    FirstColor:=bcNavy;
    SecondColor:=coScrollPen;
    OnChange:=ChangeChecked;
    SetFont(Font);
    SetButtonFont(ScrollBarFont);
    TabStop:=false;
  end;
  InitTexte;

  { Alles Aktivieren Button }
  ActAll:=TDXBitmapButton.Create(Self);
  with ActAll do
  begin
    SetRect(CheckList.Left,CheckList.Bottom+1,154,28);
    SetButtonFont(Font);
    Text:=BActAll;
    FirstColor:=coFirstColor;
    SecondColor:=coSecondColor;
    AccerlateColor:=coAccerlateColor;
    BlendColor:=bcNavy;
    RoundCorners:=rcLeftBottom;
    OnClick:=AcDeacAll;
    Hint:=HActAll;
  end;

  { Alles Deaktivieren Button }
  DeactAll:=TDXBitmapButton.Create(Self);
  with DeactAll do
  begin
    SetRect(ActAll.Right+1,CheckList.Bottom+1,155,28);
    SetButtonFont(Font);
    Text:=BDeactAll;
    FirstColor:=coFirstColor;
    SecondColor:=coSecondColor;
    BlendColor:=bcNavy;
    AccerlateColor:=coAccerlateColor;
    RoundCorners:=rcRightBottom;
    OnClick:=AcDeacAll;
    Hint:=HDeactAll;
  end;

  RegisterHotKey(#13,SaveGameButton);

  UnionRect(fAreaRect,NameList.ClientRect,SaveGameName.ClientRect);
end;

procedure TOptionsPage.DrawSaveGame(Sender: TDXComponent;
  Surface: TDirectDrawSurface;const Mem: TDDSurfaceDesc;Rect: TRect; Index: Integer;
  Selected: boolean);
var
  Text    : String;
begin
  if SaveGameName.Text=NameList.Items[Index] then
    NameList.DrawSelection(Surface,Mem,Rect,bcNavy);
    
  YellowStdFont.Draw(Surface,Rect.Left+5,Rect.Top+5,NameList.Items[Index]);
  if IsValidSaveGameVersion(fArray[Index].Version) then
  begin
    Text:=Format(FDate,[fArray[Index].Date.Day,fArray[Index].Date.Month,fArray[Index].Date.Year]);
    WhiteStdFont.Draw(Surface,Rect.Right-WhiteStdFont.TextWidth(Text)-5,Rect.Top+5,Text);
    Text:=Format(ST0502160001,[fArray[Index].Date.Hour,fArray[Index].Date.Minute]);
    WhiteStdFont.Draw(Surface,Rect.Right-WhiteStdFont.TextWidth(Text)-5,Rect.Top+25,Text);
    Text:=LKapital+Format(FFloat,[fArray[Index].Kapital/1]);
    WhiteStdFont.Draw(Surface,Rect.Left+5,Rect.Top+25,Text);
  end
  else
    RedStdFont.Draw(Surface,Rect.Left+5,Rect.Top+25,SInvSaveGame);
end;

procedure TOptionsPage.InitChecked;
var
  Dummy: Integer;
begin
  for Dummy:=0 to CheckList.Items.Count-1 do
    CheckList.Checked[Dummy]:=(TLongMessage(CheckList.Items.Objects[Dummy]) in SaveGame.LongMessages);
end;

procedure TOptionsPage.InitTexte;
begin
  AddInfoMessageBoxOption(CBSpend,HNSpend,lmSpend);
  AddInfoMessageBoxOption(CBAngebot,HNAngebot,lmAngebote);                
  AddInfoMessageBoxOption(CBTrainEnd,HNTrainEnd,lmTrainEnd);              
  AddInfoMessageBoxOption(CBTrainCancel,HNTrainCancel,lmTrainCancel);
  AddInfoMessageBoxOption(ST0501230003,ST0501230004,lmSoldierCured);
  AddInfoMessageBoxOption(CBPension,HNPension,lmPension);
  AddInfoMessageBoxOption(CBForschEnd,HNForschEnd,lmProjektEnd);          
  AddInfoMessageBoxOption(CBProdCancel,HNProdCancel,lmProdCancel);        
  AddInfoMessageBoxOption(CBProdEnd,HNProdEnd,lmProduktionEnd);           
  AddInfoMessageBoxOption(CBBuildEnd,HNBuildEnd,lmBauEnd);                
  AddInfoMessageBoxOption(CBOrgan,HNOrgan,lmOrganStat);                   
  AddInfoMessageBoxOption(CBSchiffEnd,HNSchiffEnd,lmSchiffEnd);           
  AddInfoMessageBoxOption(CBUFOs,HNUFOs,lmUFOs);                          
  AddInfoMessageBoxOption(CBLaden,HNLaden,lmNachLaden);                   
  AddInfoMessageBoxOption(CBMangel,HNMangel,lmTreibstoffMangel);          
  AddInfoMessageBoxOption(CBNewItems,HNNewItems,lmNewItems);              
  AddInfoMessageBoxOption(CBMission,HNMission,lmMissionMessage);          
  AddInfoMessageBoxOption(ST0309270007,ST0309270008,lmFindPerson);        
  AddInfoMessageBoxOption(ST0311060003,ST0311060004,lmEinsaetze);         
  AddInfoMessageBoxOption(ST0412200005,ST0412200006,lmAlphatronTransport);
  AddInfoMessageBoxOption(ST0501130001,ST0501130002,lmRaumschiffe);       
  AddInfoMessageBoxOption(ST0503220001,ST0503220002,lmBaseAttacked);       
end;

procedure TOptionsPage.ListBoxChange(Sender: TObject);
begin
  if NameList.Items.Count=0 then
    exit;

  if SaveGameName.Text=NameList.Items[NameList.ItemIndex] then
    exit;

  Container.Lock;
  SaveGameName.Text:=NameList.Items[NameList.ItemIndex];
  Container.LoadGame(false);
  SaveGameButton.Redraw;
  LoadGameButton.Redraw;
  Container.DecLock;
  Container.ReDrawArea(SaveGameName.ClientRect,Container.Surface);
end;

procedure TOptionsPage.ListDblClick(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  SaveClick(Self);
end;

procedure TOptionsPage.LoadClick(Sender: TObject);
var
  Mem   : TMemoryStream;
  Name  : String;
  Archiv: TArchivFile;
begin
  try
    try
      // Aktuellen Spielstand sichern, um bei einem Fehler diesen neu
      // laden zu k�nnen
      Mem:=TMemoryStream.Create;
      Archiv:=TArchivFile.Create;

      SaveGame.SaveGameToStream(Mem);
      Name:=SaveGame.FileName;

      // Neuen Spielstand laden
      Archiv.OpenArchiv('save\'+Savegame.PlayerName+'.sav',true);
      Archiv.OpenRessource(SaveGameName.Text);
      Container.Lock;
      SaveGame.LoadGame(SaveGameName.Text,Archiv.Stream);
      Archiv.CloseArchiv;

      game_api_SetCanSaveGame(true);

      Container.UnLock;
      ChangePage(PageGameMenu);
    except
      on E: Exception do
      begin
        Container.UnLock;
        game_api_MessageBox(Format(MGameNotLoaded,[E.Message]),CError);
        Container.Lock;
        SaveGame.LoadGame(Name,Mem);
        Container.UnLock;
      end;
    end;
  finally
    Mem.Free;
    Archiv.Free;
  end;
end;

procedure TOptionsPage.NameChange(Sender: TObject);
begin
  if SaveGameName.Text='' then
  begin
    SaveGameButton.Enabled:=false;
  end
  else
  begin
    SaveGameButton.Enabled:=true and game_api_GetCanSaveGame;
    if NameList.Items.Count=0 then exit;
    NameList.Redraw;
  end;

  LoadGameButton.Enabled:=NameList.Items.IndexOf(SaveGameName.Text)<>-1;
end;

procedure TOptionsPage.SaveClick(Sender: TObject);
var
  Archiv        : TArchivFile;
  SaveGameFile  : String;
begin
  if SaveGameName.Text=RNSaveGame then
  begin
    game_api_MessageBox(Format(EInvGameName,[SaveGameName.Text]),CError);
    exit;
  end;
  SaveGameFile:='save\'+SaveGame.PlayerName+'.sav';
  Archiv:=TArchivFile.Create;
  if not FileExists(SaveGameFile) then
  begin
    if game_api_Question(Format(MRecreate,[SaveGame.PlayerName]),CQuestion) then
    begin
      game_api_MessageBox(Format(EInvPlayer,[SaveGame.PlayerName]),CError);
      Archiv.Free;
      exit;
    end;
    Archiv.CreateArchiv(SaveGameFile);
  end
  else
    Archiv.OpenArchiv(SaveGameFile);

  if Archiv.ExistRessource(SaveGameName.Text) then
  begin
    if not game_api_Question(Format(MOverwrite,[SaveGameName.Text]),CQuestion) then
    begin
      Archiv.Free;
      exit;
    end;
  end;

  Archiv.Free;

  try
    // Spielstand abspeichern
    SaveSettings(nil);
    SaveGame.SaveGame(SaveGameName.Text);
  except
    on E: Exception do
    begin
      game_api_MessageBox(Format(MGameNotSaved,[E.Message]),CError);
      exit;
    end;
  end;
  ChangePage(PageGameMenu);
end;

procedure TOptionsPage.SaveSettings(Sender: TObject);
var
  Mes: TMessages;
  Dummy: Integer;
begin
  Mes:=[lmReserved];

  for Dummy:=0 to CheckList.Items.Count-1 do
  begin
    if CheckList.Checked[Dummy] then
      Include(Mes,TLongMessage(CheckList.Items.Objects[Dummy]));
  end;

  Container.IncLock;
  Container.LoadGame(true);
  SaveGame.LongMessages:=Mes;
//  SaveGame.MaxMessages:=MaxMess.Value;

  SaveGame.MonthBilanz:=MonthBilanz.Checked;
  Container.DecLock;
  Container.LoadGame(false);
  if Sender<>nil then
  ChangePage(PageGameMenu);
end;

procedure TOptionsPage.SetDefaults;
var
  Archiv : TArchivFile;
  Dummy  : Integer;
  Count  : Integer;
begin
//  MaxMess.Value:=SaveGame.MaxMessages;

  MonthBilanz.Checked:=SaveGame.MonthBilanz;
  InitChecked;
  NameList.Items.Clear;
  SetLength(fArray,0);

  if FileExists('save\'+SaveGame.PlayerName+'.sav') then
  begin
    Archiv:=TArchivFile.Create;
    try
      Archiv.OpenArchiv('save\'+SaveGame.PlayerName+'.sav');
      if Archiv.ExistRessource(RNSaveGame) then
      begin
        Archiv.OpenRessource(RNSaveGame);
        Archiv.Stream.Read(Count,SizeOf(Count));
        SetLength(fArray,Count);
        for Dummy:=0 to Count-1 do
        begin
          Archiv.Stream.Read(fArray[Dummy],SizeOf(fArray[Dummy]));
          NameList.Items.Add(fArray[Dummy].Name);
        end;
      end;
      Archiv.Free;
    except
      Archiv.Free;

      DeleteFile('save\'+SaveGame.PlayerName+'.sav'); 
    end;
  end;
  SaveGameName.Text:=SaveGame.FileName;
  SaveGameButton.Enabled:=(not (SaveGameName.Text='')) and game_api_GetCanSaveGame;
  
  ChangeChecked(Self);
end;

end.
