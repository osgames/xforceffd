{********************************************************************************
*										*
* Copyright (C) 2004  X-Force Team - jim_raynor@web.de				*
*										*
* This file is part of X-Force: Fight For Destiny (X-Force).			*
*										*
* X-Force is free software; you can redistribute it and/or modify		*
* it under the terms of the GNU General Public License as published by		*
* the Free Software Foundation; either version 2 of the License, or		*
* (at your option) any later version.						*
*										*
* X-Force is distributed in the hope that it will be useful,			*
* but WITHOUT ANY WARRANTY; without even the implied warranty of		*
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the			*
* GNU General Public License for more details.					*
*										*
* You should have received a copy of the GNU General Public License		*
* along with X-Force; if not, write to the Free Software			*
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA	*
*										*
*********************************************************************************
*										*
* Verwaltungsbildschirm Forschungsprojekte (Labor->Projekte)			*
*										*
********************************************************************************}

{$I ../Settings.inc}

unit VerUpgrades;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  DXContainer, KD4Page, DXBitmapButton, Defines, StringConst, XForce_types,
  Blending, DXListBox, KD4Utils, DXDraws, DXItemInfo, TraceFile, DirectFont,
  DirectDraw, DXInfoFrame;

const
  coFirstColor  : TColor = clNavy;
  coSecondColor : TColor = clBlue;
  coScrollPen   : TColor = clNavy;

type
  TVerUpgrades = class(TKD4Page)
  private
    fItemsCou      : Integer;
    fPage          : (pProjects,pUpgrade,pAutopsie);

    procedure DrawHeadRow(Sender: TDXComponent;Surface: TDirectDrawSurface;Rect: TRect);
    procedure DrawUpgrades(Sender: TDXComponent;Surface: TDirectDrawSurface;const Mem: TDDSurfaceDesc; Rect: TRect; Index: Integer;Selected: boolean);
    procedure DrawProjekts(Sender: TDXComponent;Surface: TDirectDrawSurface;const Mem: TDDSurfaceDesc; Rect: TRect; Index: Integer;Selected: boolean);
    procedure OnChangeItem(Sender: TObject);
    procedure StartProjekt(Sender: TObject);

    procedure FillProjektList(Types: TForschTypes);

    procedure ShowPage(Sender: TObject);

  protected
    { Protected-Deklarationen }
  public
    ProjektButton  : TDXBitmapButton;
    UpgradeButton  : TDXBitmapButton;
    AutopsieButton : TDXBitmapButton;
    StartButton    : TDXBitmapButton;
    Items          : TDXListBox;
    ItemInfo       : TDXItemInfo;
    InfoFrame      : TDXInfoFrame;
    constructor Create(Container: TDXContainer);override;
    procedure SetDefaults;override;
    { Public-Deklarationen }
  end;

implementation

uses
  game_api, game_utils, lager_api, ui_utils;
const
  ProjektType  = [ftNormal,ftItemResearch];
  AutopsieType = [ftAlienAutopsie];
{ TVerUpgrades }

constructor TVerUpgrades.Create(Container: TDXContainer);
begin
  inherited;
  Animation:=paRightToLeft;
  HintLabel.BlendColor:=bcBlue;
  HintLabel.BorderColor:=coFirstColor;

  SetPageInfo(ST0502200001);

{ Informationsframe }
  InfoFrame:=TDXInfoFrame.Create(Self);
  with InfoFrame do
  begin
    SetRect(388,10,244,70);
    ColorSheme:=icsBlue;
    SetFont(Font);
    Infos:=[ifCredits,ifLabor];
    SystemKeyChange:=true;

    ReturnPage:=PageVerLabor;
  end;

  { Button Projekte }
  ProjektButton:=TDXBitmapButton.Create(Self);
  with ProjektButton do
  begin
    SetRect(8,54,110,28);
    SetButtonFont(Font);
//    Hint:=HZuweisen;
    RoundCorners:=rcLeft;
    FirstColor:=coFirstColor;
    SecondColor:=coSecondColor;
    AccerlateColor:=coAccerlateColor;
    OnClick:=ShowPage;
    Text:=ST0401200001;
    BlendColor:=bcNavy;
    Tag:=0;
  end;

  { Button Autopsien }
  AutopsieButton:=TDXBitmapButton.Create(Self);
  with AutopsieButton do
  begin
    SetRect(119,54,110,28);
    SetButtonFont(Font);
    Hint:=HZuweisen;
    RoundCorners:=rcNone;
    FirstColor:=coFirstColor;
    SecondColor:=coSecondColor;
    AccerlateColor:=coAccerlateColor;
    OnClick:=ShowPage;
    Text:=ST0401200003;
    BlendColor:=bcNavy;
    Tag:=1;
  end;

  { Button Upgrades }
  UpgradeButton:=TDXBitmapButton.Create(Self);
  with UpgradeButton do
  begin
    SetRect(230,54,110,28);
    SetButtonFont(Font);
//    Hint:=HZuweisen;
    RoundCorners:=rcRight;
    FirstColor:=coFirstColor;
    SecondColor:=coSecondColor;
    AccerlateColor:=coAccerlateColor;
    OnClick:=ShowPage;
    Text:=BUpgrade;
    BlendColor:=bcNavy;
    Tag:=2;
  end;

  { Gegenstandslistbox }
  Items:=TDXListBox.Create(Self);
  with Items do
  begin
    SetRect(8,103,370,420);
    BorderColor:=coFirstColor;
    FocusColor:=coSecondColor;
    SetFont(Font);
    SetButtonFont(ScrollBarFont);
    FirstColor:=bcBlue;
    SecondColor:=coScrollPen;
//    Hint:=HItemList;
    ItemHeight:=40;
    HeadBlendColor:=bcBlue;
    OnChange:=OnChangeItem;
    OwnerDraw:=True;
    HeadRowHeight:=17;
    OnDrawHeadRow:=DrawHeadRow;;
  end;

  { Gegenstandsinformationen }
  ItemInfo:=TDXItemInfo.Create(Self);
  with ItemInfo do
  begin
    SetFont(Font);
    BorderColor:=coFirstColor;
    SetRect(508,103,284,420);
    Caption:=LUpgrade;
    Hint:=HUpgrade;
    InfoType:=itUpgrade;
    BlendColor:=bcBlue;
  end;

  { Upgrade }
  StartButton:=TDXBitmapButton.Create(Self);
  with StartButton do
  begin
    SetRect(388,103,110,28);
    SetButtonFont(Font);
    Hint:=ST0401200006;
    FirstColor:=coFirstColor;
    SecondColor:=coSecondColor;
    AccerlateColor:=coAccerlateColor;
    OnClick:=StartProjekt;
    Text:=ST0401200005;
    BlendColor:=bcNavy;
  end;

end;

procedure TVerUpgrades.DrawHeadRow(Sender: TDXComponent;
  Surface: TDirectDrawSurface; Rect: TRect);
var
  Text   : String;
begin
  WhiteStdFont.Draw(Surface,Rect.Left+6,Rect.Top+1,Items.HeadData.Titel);
  Text:=ZahlString(Items.HeadData.Einzel,Items.HeadData.MehrZahl,fItemsCou);
  WhiteStdFont.Draw(Surface,Rect.Right-6-WhiteStdFont.TextWidth(Text),Rect.Top+1,Text);
end;

procedure TVerUpgrades.DrawUpgrades(Sender: TDXComponent;
  Surface: TDirectDrawSurface;const Mem: TDDSurfaceDesc; Rect: TRect; Index: Integer;
  Selected: boolean);
var
  ItemIndex : Integer;
  TypeID    : TProjektType;
  Text      : String;
  Projekt   : Integer;
begin
  if Selected then
    Items.DrawSelection(Surface,Mem,Rect,bcBlue);
  if Items.Items[Index][1]='C' then
  begin
    TypeID:=TProjektType(StrToInt(Cut(Items.Items[Index],2,length(Items.Items[Index]))));
    SaveGame.LagerListe.DrawImage(TypeIDToIndex(TypeID,wtProjektil),Surface,Rect.Left+3,Rect.Top+3);
    WhiteBStdFont.Draw(Surface,Rect.Left+43,Rect.Top+3,game_utils_TypeToStr(TypeID));
  end
  else
  begin
    ItemIndex:=StrToInt(Items.Items[Index]);
    SaveGame.LagerListe.DrawImage(SaveGame.LagerListe.Items[ItemIndex].ImageIndex,Surface,Rect.Left+35,Rect.Top+3);
    YellowStdFont.Draw(Surface,Rect.Left+75,Rect.Top+3,SaveGame.LagerListe[ItemIndex].Name);
    if SaveGame.LagerListe.Items[ItemIndex].Land<>-1 then
    begin
      SaveGame.Organisations.DrawSmallOrgan(Surface,Rect.Left+10,Rect.Top+15,SaveGame.LagerListe.Items[ItemIndex].Land);
    end;
    Projekt:=SaveGame.ForschListe.IndexOfUpgrade(SaveGame.LagerListe[ItemIndex].ID);
    if Projekt<>-1 then
    begin
      Text:=Format(FFaehig,[SaveGame.ForschListe.ProjektStrength(Projekt)/1]);
      RedStdFont.Draw(Surface,Rect.Right-8-RedStdFont.TextWidth(Text),Rect.Top+20,Text);
      Text:=Format(LUpgradeStarted,[SaveGame.LagerListe[ItemIndex].Level+1]);
      RedStdFont.Draw(Surface,Rect.Left+75,Rect.Top+20,Text);
    end
    else
    begin
      WhiteStdFont.Draw(Surface,Rect.Left+75,Rect.Top+20,Format(FLevel,[SaveGame.LagerListe[ItemIndex].Level]));
    end;
  end;
end;

procedure TVerUpgrades.DrawProjekts(Sender: TDXComponent;
  Surface: TDirectDrawSurface; const Mem: TDDSurfaceDesc; Rect: TRect;
  Index: Integer; Selected: boolean);
var
  ItemIndex: Integer;
begin
  if Selected then
    Items.DrawSelection(Surface,Mem,Rect,bcBlue);

  ItemIndex:=StrToInt(Items.Items[Index]);
  YellowStdFont.Draw(Surface,Rect.Left+3,Rect.Top+3,SaveGame.ForschListe.ProjektName[ItemIndex]);
end;

procedure TVerUpgrades.FillProjektList(Types: TForschTypes);
var
  Dummy: Integer;
begin
  Items.ChangeItem(true);
  Items.Items.Clear;
  for Dummy:=0 to SaveGame.ForschListe.ProjektCount-1 do
  begin
    with SaveGame.ForschListe.GetProjektInfo(Dummy) do
    begin
      if (not Started) and (ResearchType in Types) then
        Items.Items.Add(IntToStr(Dummy));
    end;
  end;

  fItemsCou:=Items.Items.Count;
  Items.ChangeItem(false);
  Items.Items.OnChange(Self);
  Items.ItemIndex:=Items.ItemIndex;

  OnChangeItem(Items);
end;

procedure TVerUpgrades.OnChangeItem(Sender: TObject);
var
  Upgrade   : TItemUpgrade;
  ItemIndex : Integer;
begin
  if Items.Items.Count=0 then
  begin
    ItemInfo.ItemValid:=false;
    StartButton.Enabled:=false;
    exit;
  end;

  case fPage of
    pUpgrade :
    begin
      if (Items.Items[Items.ItemIndex][1]<>'C') then
      begin
        ItemIndex:=StrToInt(Items.Items[Items.ItemIndex]);
        Upgrade.Old:=SaveGame.LagerListe[ItemIndex];
        lager_api_UpgradeItem(Upgrade);
        ItemInfo.ItemUpgrade:=Upgrade;
        if not (SaveGame.ForschListe.IndexOfUpgrade(SaveGame.LagerListe[ItemIndex].ID)<>-1) then
          StartButton.Enabled:=true
        else
          StartButton.Enabled:=false;
      end
      else
      begin
        StartButton.Enabled:=false;
        ItemInfo.ItemValid:=false;
      end;
    end;
    pProjects,pAutopsie:
    begin
      ItemIndex:=StrToInt(Items.Items[Items.ItemIndex]);
      ItemInfo.SetProjekt(SaveGame.ForschListe.GetProjektInfo(ItemIndex),SaveGame.ForschListe.ProjektName[ItemIndex]);
      StartButton.Enabled:=true;
    end;
  end;
end;

procedure TVerUpgrades.SetDefaults;
begin
  ShowPage(ProjektButton);

  InfoFrame.Reset;
end;

procedure TVerUpgrades.ShowPage(Sender: TObject);
begin
  Container.Lock;

  Items.Items.Clear;
  case TDXBitmapButton(Sender).Tag of
    0,1:
    begin
      Items.ItemHeight:=22;
      Items.OnDrawItem:=DrawProjekts;
      if TDXBitmapButton(Sender).Tag = 0 then
      begin
        // Projekt
        fPage:=pProjects;
        FillProjektList(ProjektType);
        ItemInfo.Caption:=ST0401200004;
      end
      else
      begin
        // Autopsie
        fPage:=pAutopsie;
        FillProjektList(AutopsieType);
        ItemInfo.Caption:=ST0401200007;
      end;
      Items.HeadData:=MakeHeadData(ItemInfo.Caption,FProjekt,FProjekte);
    end;
    2:
    begin
      fPage:=pUpgrade;
      Items.ItemHeight:=40;
      Items.OnDrawItem:=DrawUpgrades;
      fItemsCou:=ui_utils_FillItems(Items,SaveGame.LagerListe,ifAll,'','',true);
      ItemInfo.Caption:=LUpgrade;
      Items.HeadData:=MakeHeadData(ItemInfo.Caption,LAusruest,LAusruests);

      OnChangeItem(Items);
    end;
  end;


  Container.LoadGame(false);

  ProjektButton.Highlight:=Sender=ProjektButton;
  UpgradeButton.Highlight:=Sender=UpgradeButton;
  AutopsieButton.Highlight:=Sender=AutopsieButton;

  StartButton.Redraw;
  ItemInfo.Redraw;
  Container.Declock;
  Items.Redraw;
end;

procedure TVerUpgrades.StartProjekt(Sender: TObject);
var
  Index: Integer;
begin
  try
    Index:=StrToInt(Items.Items[Items.ItemIndex]);
    Container.IncLock;
    case fPage of
      pUpgrade :
      begin
        SaveGame.ForschListe.StartUpgrade(SaveGame.LagerListe[Index]);
        StartButton.Enabled:=false;
      end;
      pProjects,pAutopsie :
      begin
        SaveGame.ForschListe.StartResearch(Index);
        if fPage=pProjects then
          FillProjektList(ProjektType)
        else
          FillProjektList(AutopsieType);
      end;
    end;
    Container.Declock;
    Items.Redraw;
  except
    on E: Exception do
    begin
      Container.UnlockAll;
      game_api_MessageBox(E.Message,CInformation);
    end;
  end;
end;

end.
