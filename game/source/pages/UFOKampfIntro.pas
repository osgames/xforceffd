{********************************************************************************
*										*
* Copyright (C) 2004  X-Force Team - jim_raynor@web.de				*
*										*
* This file is part of X-Force: Fight For Destiny (X-Force).			*
*										*
* X-Force is free software; you can redistribute it and/or modify		*
* it under the terms of the GNU General Public License as published by		*
* the Free Software Foundation; either version 2 of the License, or		*
* (at your option) any later version.						*
*										*
* X-Force is distributed in the hope that it will be useful,			*
* but WITHOUT ANY WARRANTY; without even the implied warranty of		*
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the			*
* GNU General Public License for more details.					*
*										*
* You should have received a copy of the GNU General Public License		*
* along with X-Force; if not, write to the Free Software			*
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA	*
*										*
*********************************************************************************
*										*
* Diese Seite wird vorm dem Raumschiffkampf angezeigt (Darstellung UFO/		*
* Raumschiff.									*
*										*
********************************************************************************}

{$I ../Settings.inc}

unit UFOKampfIntro;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  KD4Page, DXContainer, RaumschiffList, UFOList, DXItemInfo, Defines,
  DXBitmapButton, UFOKampfPage, XForce_types, KD4Utils, EarthCalcs, DXListBox,
  DXCanvas, DirectDraw, DXDraws, Blending, DirectFont, ColorBars,
  NotifyList, StringConst, MemCheck;

type
  TUFOKampf = record
    Raumschiff  : TRaumschiff;
    UFO         : TUFO;
    REvent      : TEventHandle;
    UEvent      : TEventHandle;
  end;

  TArrayUFOKampf = Array of TUFOKampf;

  TUFOKampfIntro = class(TKD4Page)
  private
    fRaumschiff  : TRaumschiff;
    fUFO         : TUFO;
    fUFOKampfPage: TUFOKampfPage;
    fKaempfe     : TArrayUFOKampf;
    fClose       : Boolean;
    fParentPage  : Integer;
    function GetKaempfe: Integer;
    { Private-Deklarationen }

    procedure SimuRaumschiffDestroy(Sender: TObject);
    procedure SimuUFODestroy(Sender: TObject);
  protected
    procedure ScrollOut;
    procedure CloseClick(Sender: TObject);
    procedure StartClick(Sender: TObject);
    procedure SimuClick(Sender: TObject);

    procedure OnSimulatorMessage(Text: String);
    procedure CanvasDraw(Sender: TDXComponent;Surface: TDirectDrawSurface;var Mem: TDDSurfaceDesc);

    procedure ShowSimulation(Visible: Boolean);

    procedure OnRaumschiffDestroy(Sender: TObject);
    procedure OnUFODestroy(Sender: TObject);

    procedure SetNextFight;
    { Protected-Deklarationen }
  public
    RaumschiffInfo   : TDXItemInfo;
    UFOInfo          : TDXItemInfo;
    CloseButton      : TDXBitmapButton;
    StartButton      : TDXBitmapButton;
    SimuButton       : TDXBitmapButton;
    SimulationLog    : TDXListBox;
    Canvas           : TDXCanvas;
    constructor Create(Container: TDXContainer);override;
    procedure SetDefaults;override;
    procedure PageShown;override;
    procedure LeaveModal;override;
    procedure BerechneKampf;
    procedure AddKampf(Raumschiff: TRaumschiff; UFO: TUFO);
    procedure DeleteKampf(Index: Integer);
    function ReshowPage: boolean;override;

    property UFO        : TUFO read fUFO write fUFO;
    property Raumschiff : TRaumschiff read fRaumschiff write fRaumschiff;
    property KampfPage  : TUFOKampfPage read fUFOKampfPage write fUFOKampfPage;
    property Kaempfe    : Integer read GetKaempfe;
    property ParentPage : Integer read fParentPage write fParentPage;
    { Public-Deklarationen }
  end;

implementation

uses Simulator, array_utils;

{ TUFOKampfIntro }

procedure TUFOKampfIntro.AddKampf(Raumschiff: TRaumschiff; UFO: TUFO);
var
  Index  : Integer;
  Dummy  : Integer;
  RIndex : Integer;
  UIndex : Integer;
begin
  RIndex:=-1;
  UIndex:=-1;
  for Dummy:=0 to high(fKaempfe) do
  begin
    if fKaempfe[Dummy].Raumschiff=Raumschiff then
      RIndex:=Dummy;

    if fKaempfe[Dummy].UFO=UFO then
      UIndex:=Dummy;
  end;

  Index:=length(fKaempfe);
  SetLength(fKaempfe,Index+1);
  fKaempfe[Index].Raumschiff:=Raumschiff;
  fKaempfe[Index].UFO:=UFO;

  if RIndex=-1 then
    fKaempfe[Index].REvent:=Raumschiff.NotifyList.RegisterEvent(EVENT_SCHIFFONDESTROY,OnRaumschiffDestroy)
  else
    fKaempfe[Index].REvent:=fKaempfe[RIndex].REvent;

  if UIndex=-1 then
    fKaempfe[Index].UEvent:=UFO.NotifyList.RegisterEvent(EVENT_ONUFODESTROY,OnUFODestroy)
  else
    fKaempfe[Index].UEvent:=fKaempfe[UIndex].UEvent;

end;

procedure TUFOKampfIntro.BerechneKampf;
var
  Dummy      : Integer;
  Dummy2     : Integer;
  Raumschiff : TRaumschiff;
begin
  SetLength(fKaempfe,0);
  for Dummy:=0 to SaveGame.Raumschiffe.Count-1 do
  begin
    Raumschiff:=SaveGame.Raumschiffe[Dummy];
    if (sabHuntUFO in Raumschiff.Abilities) then
    begin
      if (Raumschiff.Status in [ssAir,ssBackToBasis]) then
      begin
        for Dummy2:=0 to SaveGame.UFOListe.Count-1 do
        begin
          if EarthEntfernung(Raumschiff.Position,SaveGame.UFOListe[Dummy2].Position)<50 then
          begin
            AddKampf(Raumschiff,SaveGame.UFOListe[Dummy2]);
          end;
        end;
      end
      else
      begin
        // Sonderfall UFO direkt �ber der Basis ber�cksichtigen
        if (Raumschiff.HuntedUFO<>nil) and Raumschiff.HuntedUFO.DirektUeberBasis(Raumschiff.GetHomeBasis) then
          AddKampf(Raumschiff,Raumschiff.HuntedUFO);
      end;
    end;
  end;
  if Kaempfe>0 then ChangePage(PageUFOIntro);
end;

procedure TUFOKampfIntro.CanvasDraw(Sender: TDXComponent;Surface: TDirectDrawSurface;
  var Mem: TDDSurfaceDesc);
begin
  Surface.FillRect(Canvas.ClientRect,bcBlack);
  Rectangle(Surface,Mem,Canvas.ClientRect,bcMaroon);

  WhiteStdFont.Draw(Surface,Canvas.Left+10,Canvas.Top+20,IAHitPoints);
  WhiteStdFont.Draw(Surface,Canvas.Left+10,Canvas.Top+40,LShield);

  if Raumschiff<>nil then
  begin
    WhiteStdFont.Draw(Surface,Canvas.Left+75,Canvas.Top+4,Raumschiff.Name);
    DrawLifeBar(Surface,Canvas.Left+75,Canvas.Top+24,round(Raumschiff.AktHitpoints/Raumschiff.Hitpoints*100));
    if Raumschiff.MaxShieldPoints>0 then
      DrawShieldBar(Surface,Canvas.Left+75,Canvas.Top+44,round(Raumschiff.ShieldPoints/Raumschiff.MaxShieldPoints*100));
  end;

  if UFO<>nil then
  begin
    WhiteStdFont.Draw(Surface,Canvas.Left+300,Canvas.Top+4,UFO.Name);
    DrawLifeBar(Surface,Canvas.Left+300,Canvas.Top+24,round(UFO.Hitpoints/UFO.Model.Hitpoints*100));
    if UFO.ShieldPoints>0 then
      DrawShieldBar(Surface,Canvas.Left+300,Canvas.Top+44,round(UFO.ShieldPoints/UFO.Model.Shield*100));
  end;
{  WhiteStdFont.Draw(Surface,Canvas.Left+10,Canvas.Top+24,'Hitpoints:');
  WhiteStdFont.Draw(Surface,Canvas.Left+75,Canvas.Top+24,Format('%.0n / %.0n',[]));

  WhiteStdFont.Draw(Surface,Canvas.Left+10,Canvas.Top+44,'Schildpunkte:');
  WhiteStdFont.Draw(Surface,Canvas.Left+75,Canvas.Top+44,Format('%.0n / %.0n',[Raumschiff.ShieldPoints/1,Raumschiff.Shield.Points/1]));}
end;

procedure TUFOKampfIntro.CloseClick(Sender: TObject);
begin
  ScrollOut;
  if not ReshowPage then
  begin
    ChangePage(fParentPage);
    exit;
  end;
  if fClose then exit;
  PageShown;
  Container.ShowCursor(true);
end;

constructor TUFOKampfIntro.Create(Container: TDXContainer);
begin
  inherited;
  Animation:=paRightToLeft;
  fParentPage:=PageGameMenu;

  { AbbrechenButton }
  CloseButton:=TDXBitmapButton.Create(Self);
  with CloseButton do
  begin
    SetRect(659,540,125,28);
    FirstColor:=coFirstColor;
    EscButton:=true;
    SecondColor:=coSecondColor;
    AccerlateColor:=coAccerlateColor;
    OnClick:=CloseClick;
    SetButtonFont(Font);
    RoundCorners:=rcRight;
    Text:=BCancel;
    Hint:=HCancelKampf;
  end;

  { Kampf starten Button }
  StartButton:=TDXBitmapButton.Create(Self);
  with StartButton do
  begin
    SetRect(533,540,125,28);
    FirstColor:=coFirstColor;
    SecondColor:=coSecondColor;
    AccerlateColor:=coAccerlateColor;
    RoundCorners:=rcLeft;
    OnClick:=StartClick;
    SetButtonFont(Font);
    Text:=BStartKampf;
    Hint:=HStartKampf;
  end;

  { Simulieren Button }
  SimuButton:=TDXBitmapButton.Create(Self);
  with SimuButton do
  begin
    SetRect(16,540,125,28);
    FirstColor:=coFirstColor;
    SecondColor:=coSecondColor;
    AccerlateColor:=coAccerlateColor;
    RoundCorners:=rcAll;
    OnClick:=SimuClick;
    SetButtonFont(Font);
    Text:=ST0311070008;
    Hint:=ST0311070009;
  end;

  { Raumschiffinfo }
  RaumschiffInfo:=TDXItemInfo.Create(Self);
  with RaumschiffInfo do
  begin
    SetRect(16,16,330,515);
    SetFont(Font);
    Caption:=LRaumschiff;
    BorderColor:=coFirstColor;
    UFOKampf:=true;
  end;

  { UFOinfo }
  UFOInfo:=TDXItemInfo.Create(Self);
  with UFOInfo do
  begin
    SetRect(454,16,330,515);
    SetFont(Font);
    Caption:=LUFOs;
    BorderColor:=coFirstColor;
  end;

  Canvas:=TDXCanvas.Create(Self);
  with Canvas do
  begin
    SetRect(50,16,540,408);
    Visible:=false;
    OnPaint:=CanvasDraw;
  end;

  SimulationLog:=TDXListBox.Create(Self);
  with SimulationLog do
  begin
    Visible:=false;
    SetRect(100,100,450,300);
  end;

end;

procedure TUFOKampfIntro.DeleteKampf(Index: Integer);
var
  Dummy  : Integer;
  RFound : Boolean;
  UFound : Boolean;
begin
  RFound:=false;
  UFound:=false;

  // Pr�fen, ob das Raumschiff oder UFO einen weiteren Kampf macht
  for Dummy:=0 to high(fKaempfe) do
  begin
    if Dummy<>Index then
    begin
      if fKaempfe[Dummy].Raumschiff=Raumschiff then
        RFound:=true;

      if fKaempfe[Dummy].UFO=UFO then
        UFound:=true;
    end;
  end;

  if not RFound then
    fKaempfe[Index].Raumschiff.NotifyList.RemoveEvent(fKaempfe[Index].REvent);

  if not UFound then
    fKaempfe[Index].UFO.NotifyList.RemoveEvent(fKaempfe[Index].UEvent);

  DeleteArray(Addr(fKaempfe),TypeInfo(TArrayUFOKampf),Index);
end;

function TUFOKampfIntro.GetKaempfe: Integer;
begin
  result:=length(fKaempfe);
end;

procedure TUFOKampfIntro.LeaveModal;
begin
  SetDefaults;
end;

procedure TUFOKampfIntro.OnRaumschiffDestroy(Sender: TObject);
var
  Dummy: Integer;
begin
  for Dummy:=high(fKaempfe) downto 0 do
  begin
    if fKaempfe[Dummy].Raumschiff=Sender then
      DeleteKampf(Dummy);
  end;

  if RaumschiffInfo.Raumschiff=Sender then
    RaumschiffInfo.Raumschiff:=nil;
end;

procedure TUFOKampfIntro.OnSimulatorMessage(Text: String);
begin
  Container.Lock;
  SimulationLog.Items.Insert(0,Text);
  if SimulationLog.ItemsInWindow<SimulationLog.Items.Count then
    SimulationLog.Items.Delete(SimulationLog.ItemsInWindow);
  Container.Unlock;

  SimulationLog.Redraw;
  Canvas.Redraw;
end;

procedure TUFOKampfIntro.OnUFODestroy(Sender: TObject);
var
  Dummy: Integer;
begin
  for Dummy:=high(fKaempfe) downto 0 do
  begin
    if fKaempfe[Dummy].UFO=Sender then
      DeleteKampf(Dummy);
  end;

  if UFOInfo.UFO=Sender then
    UFOInfo.UFO:=nil;
end;

procedure TUFOKampfIntro.PageShown;
var
  Dummy : Integer;
  Time  : Cardinal;
begin
  Container.PlaySound(SPageIn);
  CloseButton.Enabled:=Raumschiff.HuntedUFO=UFO;
  Container.UnLockAll;
  for Dummy:=0 to 12 do
  begin
    Time:=GetTickCount;
    Container.Lock;
    RaumschiffInfo.Left:=RaumschiffInfo.Left+30;
    UFOInfo.Left:=UFOInfo.Left-30;
    CloseButton.Top:=CloseButton.Top-9;
    StartButton.Top:=CloseButton.Top;
    SimuButton.Top:=CloseButton.Top;
    Container.UnLock;
    Container.RedrawArea(Rect(0,16,800,600),Container.Surface);
    While (GetTickCount-Time<30) do;
  end;
  Container.ShowCursor(true);
end;

function TUFOKampfIntro.ReshowPage: boolean;
begin
  if length(fKaempfe)>0 then
  begin
    result:=true;
  end
  else
  begin
    result:=false;
    Container.LockAll;
    fClose:=true;
    Container.ShowCursor(true);
  end;
end;

procedure TUFOKampfIntro.ScrollOut;
var
  Dummy: Integer;
begin
  Container.ShowCursor(false);
  Container.PlaySound(SPageIn);
  Container.UnLock;
  for Dummy:=0 to 12 do
  begin
    Container.Lock;
    RaumschiffInfo.Left:=RaumschiffInfo.Left-30;
    UFOInfo.Left:=UFOInfo.Left+30;
    CloseButton.Top:=CloseButton.Top+9;
    StartButton.Top:=CloseButton.Top;
    SimuButton.Top:=CloseButton.Top;
    Container.UnLock;
    Container.RedrawArea(Rect(0,16,800,600),Container.Surface);
  end;
end;

procedure TUFOKampfIntro.SetDefaults;
begin
  Container.PlayMusicCategory('UFOKampf');
  SimulationLog.Visible:=false;
  Canvas.Visible:=false;
  fClose:=false;

  SetNextFight;

  CloseButton.Top:=657;
  StartButton.Top:=657;
  SimuButton.Top:=657;
  RaumschiffInfo.Left:=-374;
  UFOInfo.Left:=844;

  Container.ShowCursor(false);
end;

procedure TUFOKampfIntro.SetNextFight;
begin
  Assert(length(fKaempfe)>0,'Keine K�mpfe vorhanden');
  Raumschiff:=fKaempfe[0].Raumschiff;
  RaumschiffInfo.Raumschiff:=fRaumschiff;

  UFO:=fKaempfe[0].UFO;
  UFOInfo.UFO:=fUFO;

  DeleteKampf(0);
end;

procedure TUFOKampfIntro.ShowSimulation(Visible: Boolean);
begin
  SimulationLog.Visible:=Visible;
  Canvas.Visible:=Visible;

  StartButton.Enabled:=not Visible;
  SimuButton.Enabled:=not Visible;
  CloseButton.Enabled:=not Visible;
end;

procedure TUFOKampfIntro.SimuClick(Sender: TObject);
var
  Simulator  : TSimulator;

  REvent     : TEventHandle;
  UEvent     : TEventHandle;
begin
  ScrollOut;

  RaumschiffInfo.Raumschiff:=nil;
  UFOInfo.UFO:=nil;

  InitBarSurface(Container);

  SimulationLog.Items.Clear;
  ShowSimulation(true);

  Container.MessageLock(true);

  Simulator:=TSimulator.Create;
  Simulator.OnMessage:=OnSimulatorMessage;

  REvent:=Raumschiff.NotifyList.RegisterEvent(EVENT_SCHIFFONDESTROY,SimuRaumschiffDestroy);
  UEvent:=UFO.NotifyList.RegisterEvent(EVENT_ONUFODESTROY,SimuUFODestroy);

  Simulator.Simulate(Raumschiff,UFO);

  if Raumschiff<>nil then
    Raumschiff.NotifyList.RemoveEvent(REvent);

  if UFO<>nil then
    UFO.NotifyList.RemoveEvent(UEvent);

  Simulator.Free;

  Container.MessageLock(false);

  ShowSimulation(false);

  if not ReshowPage then
  begin
    ChangePage(fParentPage);
    exit;
  end;

  if fClose then
    exit;

  SetNextFight;

  PageShown;
  Container.ShowCursor(true);
end;

procedure TUFOKampfIntro.SimuRaumschiffDestroy(Sender: TObject);
begin
  Raumschiff:=nil;
end;

procedure TUFOKampfIntro.SimuUFODestroy(Sender: TObject);
begin
  UFO:=nil;
end;

procedure TUFOKampfIntro.StartClick(Sender: TObject);
begin
  if not Assigned(fUFOKampfPage) then
    exit;

  fUFOKampfPage.Raumschiff:=Raumschiff;
  fUFOKampfPage.UFO:=UFO;

  ScrollOut;
  Container.ShowCursor(true);
  Container.Lock;

  RaumschiffInfo.Raumschiff:=nil;
  UFOInfo.UFO:=nil;

  Container.UnLock;

  // Kampfstarten
  ChangePage(PageUFOKampf);


end;

end.
