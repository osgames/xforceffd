{********************************************************************************
*										*
* Copyright (C) 2004  X-Force Team - jim_raynor@web.de				*
*										*
* This file is part of X-Force: Fight For Destiny (X-Force).			*
*										*
* X-Force is free software; you can redistribute it and/or modify		*
* it under the terms of the GNU General Public License as published by		*
* the Free Software Foundation; either version 2 of the License, or		*
* (at your option) any later version.						*
*										*
* X-Force is distributed in the hope that it will be useful,			*
* but WITHOUT ANY WARRANTY; without even the implied warranty of		*
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the			*
* GNU General Public License for more details.					*
*										*
* You should have received a copy of the GNU General Public License		*
* along with X-Force; if not, write to the Free Software			*
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA	*
*										*
*********************************************************************************
*										*
* Verwaltungsbildschirm L�nder							*
*										*
********************************************************************************}

{$I ../Settings.inc}


unit VerOrgan;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  DXContainer, KD4Page, DXBitmapButton, Defines, DXListBox, DXLabel, DXItemInfo,
  DXDraws, KD4Utils, Blending, XForce_types, DXInfoFrame, TraceFile, DirectDraw,
  DirectFont, Menus, StringConst;

type
  TVerOrgan = class(TKD4Page)
  private
    fIndex       : Integer;
    fRedrawRect  : TRect;
    fSortButtons : TRect;
    Typ          : Integer;
    fSort        : Integer;
    procedure OrganListChange(Sender: TObject);
    procedure ChangeView(Sender: TObject);
    procedure DrawOrgan(Sender: TDXComponent;Surface: TDirectDrawSurface;const Mem: TDDSurfaceDesc;Rect: TRect; Index: Integer;Selected: boolean);
    procedure AktuOrgan;
    procedure SetHighLightButton;
    procedure ChangeSort(Sender: TObject);
    { Private-Deklarationen }
  protected
    procedure Sort;
    function SortFunc(Organ1,Organ2: Integer;Typ: TFunctionType): Integer;
//    function SortKapi(Organ1,Organ2: Integer): Integer;
    { Protected-Deklarationen }
  public
    OrganList       : TDXListBox;
    ItemInfo        : TDXItemInfo;
    InfoFrame       : TDXInfoFrame;
    AllOrgan        : TDXBitmapButton;
    AllianzOrgan    : TDXBitmapButton;
    FriendlyOrgan   : TDXBitmapButton;
    NeutralOrgan    : TDXBitmapButton;
    UnFriendlyOrgan : TDXBitmapButton;
    EnemyOrgan      : TDXBitmapButton;
    SortNameButton  : TDXBitmapButton;
    SortMitgButton  : TDXBitmapButton;
    SortStatButton  : TDXBitmapButton;
    constructor Create(Container: TDXContainer);override;
    procedure SetDefaults;override;
    property ShowOrgan: Integer read fIndex write fIndex;
  end;

implementation

uses
  ufopaedie_api;

{ TVerOrgan }

procedure TVerOrgan.AktuOrgan;
var
  Organ: Integer;
  Dummy: Integer;
  Add: boolean;
begin
  Container.Lock;
  OrganList.ChangeItem(true);
  OrganList.Items.Clear;
  for Dummy:=0 to SaveGame.Organisations.Count-1 do
  begin
    Add:=false;
    case typ of
      0 : Add:=true;
      1 : Add:=(OrganStatus(SaveGame.Organisations[Dummy].Friendly)=osAllianz);
      2 : Add:=(OrganStatus(SaveGame.Organisations[Dummy].Friendly)=osFriendly);
      3 : Add:=(OrganStatus(SaveGame.Organisations[Dummy].Friendly)=osNeutral);
      4 : Add:=(OrganStatus(SaveGame.Organisations[Dummy].Friendly)=osUnFriendly);
      5 : Add:=(OrganStatus(SaveGame.Organisations[Dummy].Friendly)=osEnemy);
    end;
    if Add then OrganList.Items.Add(IntToStr(Dummy));
  end;
  OrganList.ChangeItem(false);
  OrganList.Items.OnChange(Self);
  if OrganList.Items.Count>0 then
  begin
    Sort;
    Organ:=StrToInt(OrganList.Items[OrganList.ItemIndex]);
    ItemInfo.Organisation:=SaveGame.Organisations[Organ];
  end
  else
  begin
    ItemInfo.ItemValid:=false;
  end;
  Container.UnLock;
  Container.RedrawArea(fRedrawRect,Container.Surface);
end;

procedure TVerOrgan.ChangeSort(Sender: TObject);
begin
  Container.Lock;
  fSort:=(Sender as TDXBitmapButton).Tag;
  Sort;
  Container.UnLock;
  if OrganList.Items.Count>0 then
  begin
    ItemInfo.Organisation:=SaveGame.Organisations[StrToInt(OrganList.Items[OrganList.ItemIndex])];
    Container.RedrawArea(ItemInfo.ClientRect,Container.Surface,false);
  end;
  Container.RedrawArea(fSortButtons,Container.Surface,false);
  Container.RedrawArea(OrganList.ClientRect,Container.Surface);
end;

procedure TVerOrgan.ChangeView(Sender: TObject);
begin
  Container.Lock;
  Typ:=TDXBitmapButton(Sender).Tag;
  SetHighLightButton;
  AktuOrgan;
  Container.UnLock;
  Container.RedrawArea(fRedrawRect,Container.Surface);
end;

constructor TVerOrgan.Create(Container: TDXContainer);
begin
  inherited;
  Animation:=paRightToLeft;
  fIndex:=-1;
  Typ:=0;
  fSort:=0;

  SetPageInfo(StripHotKey(BVerOrgan));

{ Informationsframe }
  InfoFrame:=TDXInfoFrame.Create(Self);
  with InfoFrame do
  begin
    SetRect(388,10,244,50);
    SetFont(Font);
    ColorSheme:=icsRed;
    Infos:=[ifCredits];
    SystemKeyChange:=true;
  end;

{ Organisationen }
  { Organisationenlistbox }
  OrganList:=TDXListBox.Create(Self);
  with OrganList do
  begin
    SetRect(8,54,370,469);
    BorderColor:=coFirstColor;
    FocusColor:=coSecondColor;
    SetFont(Font);
    SetButtonFont(ScrollBarFont);
    FirstColor:=coScrollBrush;
    SecondColor:=coScrollPen;
    Hint:=HOrganList;
    ItemHeight:=30;
    OwnerDraw:=true;
    OnChange:=OrganListChange;
    OnDrawItem:=DrawOrgan;
    HeadRowHeight:=17;
    HeadData:=MakeHeadData(LOrgan,FOrgan,FOrgans);
  end;

  { Organisationsinformationen }
  ItemInfo:=TDXItemInfo.Create(Self);
  with ItemInfo do
  begin
    SetFont(Font);
    BorderColor:=coFirstColor;
    SetRect(508,54,284,469);
    Caption:=IIOrganisation;
    Hint:=HOrganInfo;
  end;

  { Alle Organisationen }
  AllOrgan:=TDXBitmapButton.Create(Self);
  with AllOrgan do
  begin
    SetButtonFont(Font);
    Text:=LAllOrgan;
    SetRect(388,54,110,28);
    FirstColor:=coFirstColor;
    SecondColor:=coSecondColor;
    RoundCorners:=rcTop;
    AccerlateColor:=coAccerlateColor;
    OnClick:=ChangeView;
    Hint:=HAllOrgan;
    Tag:=0;
  end;

  { Verb�ndete Organisationen }
  AllianzOrgan:=TDXBitmapButton.Create(Self);
  with AllianzOrgan do
  begin
    SetButtonFont(Font);
    Text:=LAllianzOrgan;
    SetRect(388,83,110,28);
    RoundCorners:=rcNone;
    FirstColor:=coFirstColor;
    SecondColor:=coSecondColor;
    AccerlateColor:=coAccerlateColor;
    OnClick:=ChangeView;
    Hint:=HAllianzOrgan;
    Tag:=1;
  end;

  { Freundliche Organisationen }
  FriendlyOrgan:=TDXBitmapButton.Create(Self);
  with FriendlyOrgan do
  begin
    SetButtonFont(Font);
    Text:=LFriendlyOrgan;
    SetRect(388,112,110,28);
    FirstColor:=coFirstColor;
    RoundCorners:=rcNone;
    SecondColor:=coSecondColor;
    AccerlateColor:=coAccerlateColor;
    OnClick:=ChangeView;
    Hint:=HFriendlyOrgan;
    Tag:=2;
  end;

  { Neutral Organisationen }
  NeutralOrgan:=TDXBitmapButton.Create(Self);
  with NeutralOrgan do
  begin
    SetButtonFont(Font);
    Text:=LNeutralOrgan;
    SetRect(388,141,110,28);
    FirstColor:=coFirstColor;
    RoundCorners:=rcNone;
    SecondColor:=coSecondColor;
    AccerlateColor:=coAccerlateColor;
    OnClick:=ChangeView;
    Hint:=HNeutralOrgan;
    Tag:=3;
  end;

  { Unfreundlich Organisationen }
  UnfriendlyOrgan:=TDXBitmapButton.Create(Self);
  with UnfriendlyOrgan do
  begin
    SetButtonFont(Font);
    Text:=LUnFriendlyOrgan;
    SetRect(388,170,110,28);
    FirstColor:=coFirstColor;
    RoundCorners:=rcNone;
    SecondColor:=coSecondColor;
    AccerlateColor:=coAccerlateColor;
    OnClick:=ChangeView;
    Hint:=HUnFriendlyOrgan;
    Tag:=4;
  end;

  { Feindliche Organisationen }
  EnemyOrgan:=TDXBitmapButton.Create(Self);
  with EnemyOrgan do
  begin
    SetButtonFont(Font);
    RoundCorners:=rcBottom;
    Text:=LEnemyOrgan;
    SetRect(388,199,110,28);
    FirstColor:=coFirstColor;
    SecondColor:=coSecondColor;
    AccerlateColor:=coAccerlateColor;
    OnClick:=ChangeView;
    Hint:=HEnemyOrgan;
    Tag:=5;
  end;

  { Sortier Buttons }
  SortNameButton:=TDXBitmapButton.Create(Self);
  with SortNameButton do
  begin
    SetButtonFont(Font);
    RoundCorners:=rcTop;
    Text:=LSortName;
    SetRect(388,234,110,28);
    FirstColor:=coFirstColor;
    SecondColor:=coSecondColor;
    AccerlateColor:=coAccerlateColor;
    OnClick:=ChangeSort;
    Hint:=HSortName;
    Tag:=0;
  end;

  SortMitgButton:=TDXBitmapButton.Create(Self);
  with SortMitgButton do
  begin
    SetButtonFont(Font);
    RoundCorners:=rcNone;
    Text:=LSortMitg;
    SetRect(388,263,110,28);
    FirstColor:=coFirstColor;
    SecondColor:=coSecondColor;
    AccerlateColor:=coAccerlateColor;
    OnClick:=ChangeSort;
    Hint:=HSortMitg;
    Tag:=2;
  end;

  SortStatButton:=TDXBitmapButton.Create(Self);
  with SortStatButton do
  begin
    SetButtonFont(Font);
    RoundCorners:=rcBottom;
    Text:=LSortStat;
    SetRect(388,292,110,28);
    FirstColor:=coFirstColor;
    SecondColor:=coSecondColor;
    AccerlateColor:=coAccerlateColor;
    OnClick:=ChangeSort;
    Hint:=HSortStat;
    Tag:=3;
  end;

  UnionRect(fRedrawRect,ItemInfo.ClientRect,OrganList.ClientRect);
  UnionRect(fSortButtons,SortNameButton.ClientRect,SortStatButton.ClientRect);

  ufopaedie_api_initCountrys(Self);
end;

procedure TVerOrgan.DrawOrgan(Sender: TDXComponent;
  Surface: TDirectDrawSurface; const Mem: TDDSurfaceDesc; Rect: TRect; Index: Integer;
  Selected: boolean);
var
  Organ   : Integer;
  Font    : TDirectFont;
begin
  Organ:=StrToInt(OrganList.Items[Index]);
  if Selected then
  begin
    OrganList.DrawSelection(Surface,Mem,Rect,bcMaroon);
  end;
  Font:=YellowStdFont;
  case OrganStatus(SaveGame.Organisations[Organ].Friendly) of
    osAllianz    : Font:=LimeStdFont;
    osFriendly   : Font:=GreenStdFont;
    osNeutral    : Font:=YellowStdFont;
    osUnFriendly : Font:=MaroonStdFont;
    osEnemy      : Font:=RedStdFont;
  end;
  SaveGame.Organisations.DrawOrgan(Surface,Rect.Left+4,Rect.Top+4,Organ);
  Font.Draw(Surface,Rect.Left+49,Rect.Top+9,SaveGame.Organisations[Organ].Name);
end;

procedure TVerOrgan.OrganListChange(Sender: TObject);
var
  Organ: Integer;
begin
  if OrganList.Items.Count=0 then exit;
  Organ:=StrToInt(OrganList.Items[OrganList.ItemIndex]);
  ItemInfo.Organisation:=SaveGame.Organisations[Organ];
end;

procedure TVerOrgan.SetDefaults;
var
  Dummy: Integer;
  Text : String;
begin
  InfoFrame.Reset;
  if fIndex<>-1 then Typ:=0;
  AktuOrgan;
  if fIndex<>-1 then
  begin
    Text:=IntToStr(fIndex);
    for Dummy:=0 to OrganList.Items.Count-1 do
    begin
      if OrganList.Items[Dummy]=Text then OrganList.ItemIndex:=Dummy;
    end;
    fIndex:=-1;
  end;
  OrganList.ChangeItem(false);
  OrganList.Items.OnChange(Self);
  OrganList.OnChange(Self);
  SetHighLightButton;
end;

procedure TVerOrgan.SetHighLightButton;
begin
  AllOrgan.HighLight:=(typ=0);
  AllianzOrgan.HighLight:=(typ=1);
  FriendlyOrgan.HighLight:=(typ=2);
  NeutralOrgan.HighLight:=(typ=3);
  UnFriendlyOrgan.HighLight:=(typ=4);
  EnemyOrgan.HighLight:=(typ=5);
end;

procedure TVerOrgan.Sort;
begin
  SortNameButton.HighLight:=(fSort=0);
  SortMitgButton.HighLight:=(fSort=2);
  SortStatButton.HighLight:=(fSort=3);
  QuickSort(0,OrganList.Items.Count-1,SortFunc);
end;

function TVerOrgan.SortFunc(Organ1, Organ2: Integer;
  Typ: TFunctionType): Integer;
var
  Index1 : Integer;
  Index2 : Integer;
begin
  case Typ of
    ftCompare :
    begin
      Index1:=StrToInt(OrganList.Items[Organ1]);
      Index2:=StrToInt(OrganList.Items[Organ2]);
      case fSort of
        0: result:=AnsiCompareText(SaveGame.Organisations[Index1].Name,SaveGame.Organisations[Index2].Name);
        2: result:=SaveGame.Organisations[Index2].Einwohner-SaveGame.Organisations[Index1].Einwohner;
        3: result:=SaveGame.Organisations[Index2].Friendly-SaveGame.Organisations[Index1].Friendly;
      end;
    end;
    ftExchange : OrganList.Items.Exchange(Organ1,Organ2);
  end;
end;

end.
