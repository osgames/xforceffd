{********************************************************************************
*										*
* Copyright (C) 2004  X-Force Team - jim_raynor@web.de				*
*										*
* This file is part of X-Force: Fight For Destiny (X-Force).			*
*										*
* X-Force is free software; you can redistribute it and/or modify		*
* it under the terms of the GNU General Public License as published by		*
* the Free Software Foundation; either version 2 of the License, or		*
* (at your option) any later version.						*
*										*
* X-Force is distributed in the hope that it will be useful,			*
* but WITHOUT ANY WARRANTY; without even the implied warranty of		*
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the			*
* GNU General Public License for more details.					*
*										*
* You should have received a copy of the GNU General Public License		*
* along with X-Force; if not, write to the Free Software			*
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA	*
*										*
*********************************************************************************
*										*
* Verwaltungsbildschirm Werkstatt						*
*										*
********************************************************************************}

{$I ../Settings.inc}

unit VerWerkStatt;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  DXContainer,KD4Page, DXBitmapButton, DXLabel, DXListBox, DXDraws, DXInfoFrame,
  XForce_types, DXItemInfo, KD4Utils, Blending,VerArbeitsmarkt, VerNewProjekt,Defines,
  math, DirectDraw, DirectFont, Menus, StringConst;

type
  TVerWerkStatt = class(TKD4Page)
  private
    fRedrawRect: TRect;
    fButtonRect: TRect;
    procedure MarktClick(Sender: TObject);
    procedure CancelProjektClick(Sender: TObject);
    procedure ProjektChange(Sender: TObject);
    procedure TechnikerListDblClick(Sender: TObject;Button: TMouseButton;Shift: TShiftState;x,y: Integer);
    procedure ProjektDblClick(Sender: TObject;Button: TMouseButton;Shift: TShiftState;x,y: Integer);
    procedure ProjektMouseUp(Sender: TObject;Button: TMouseButton;Shift: TShiftState;x,y: Integer);
    procedure SetAusbildung(Sender: TObject);
    procedure SetAusbildungAll(Sender: TObject);
    procedure SetAusbildungFree(Sender: TObject);
    procedure SetProjekt(Sender: TObject);
    procedure SetProjektAll(Sender: TObject);
    procedure SetProjektFree(Sender: TObject);
    procedure SetButtons;
    procedure TechnikerListChange(Sender: TObject);
    procedure AktuTechnikerList;
    procedure OnSelectedBasisChange(Sender: TObject);
    procedure DrawFortSchritt(Surface: TDirectDrawSurface;const Mem: TDDSurfaceDesc;X,Y,Width: Integer;Prozent: double);
    { Private-Deklarationen }
  protected
    procedure DrawYourTechniker(Sender: TDXComponent;Surface: TDirectDrawSurface;const Mem: TDDSurfaceDesc;Rect: TRect; Index: Integer;Selected: boolean);
    procedure DrawProjekt(Sender: TDXComponent;Surface: TDirectDrawSurface;const Mem: TDDSurfaceDesc;Rect: TRect; Index: Integer;Selected: boolean);
    { Protected-Deklarationen }
  public
    InfoFrame     : TDXInfoFrame;
    TechnikerList : TDXListBox;
    Projekte      : TDXListBox;
    ProduktInfo   : TDXItemInfo;
    Zuweisen      : TDXBitmapButton;
    ZuweisenAll   : TDXBitmapButton;
    ZuweisenFree  : TDXBitmapButton;
    Ausbilden     : TDXBitmapButton;
    AusbildenAll  : TDXBitmapButton;
    AusbildFree   : TDXBitmapButton;
    NewProjekt    : TDXBitmapButton;
    CancelProjekt : TDXBitmapButton;
    LagerButton   : TDXBitmapButton;
    ArbeitsButton : TDXBitmapButton;
    constructor Create(Container: TDXContainer);override;
    procedure LeaveModal;override;
    procedure SetDefaults;override;
  end;

implementation

uses
  game_api, ufopaedie_api, basis_api, lager_api;

{ TVerWerkStatt }

// Aktualisiert die Liste mit den Technikern;
procedure TVerWerkStatt.AktuTechnikerList;
var
  Dummy: Integer;
begin
  Container.Lock;
  TechnikerList.Items.Clear;
  for Dummy:=0 to SaveGame.WerkStatt.Count-1 do
    if SaveGame.WerkStatt.InSelectedBasis(Dummy) then
      TechnikerList.Items.Add(IntToStr(Dummy));
  Projekte.Items.Clear;
  for Dummy:=0 to SaveGame.WerkStatt.ProjektCount-1 do
    if SaveGame.WerkStatt.ProjektInSelectedBasis(Dummy) then
      Projekte.Items.Add(IntToStr(Dummy));
  if Projekte.Items.Count>0 then
    ProduktInfo.Production:=SaveGame.WerkStatt.Projekte[StrToInt(Projekte.Items[Projekte.ItemIndex])]
  else
    ProduktInfo.ItemValid:=false;
  Container.LoadGame(false);
  Container.RedrawArea(fRedrawRect,Container.Surface);
  SetButtons;
  Container.DecLock;
  Container.DoFlip;
end;

procedure TVerWerkStatt.CancelProjektClick(Sender: TObject);
var
  Index : Integer;
  Text  : String;
begin
  Index:=StrToInt(Projekte.Items[Projekte.ItemIndex]);
  Text:=SaveGame.LagerListe[SaveGame.LagerListe.IndexOfID(SaveGame.WerkStatt.Projekte[Index].ItemID)].Name;
  if SaveGame.WerkStatt.Projekte[Index].Anzahl=101 then
  begin
    if not game_api_Question(Format(MCancelEndProd,[Text]),CCancelProdukt) then
      exit;
  end
  else
    if not game_api_Question(Format(MCancelProdukt,[SaveGame.WerkStatt.Projekte[Index].Anzahl,Text]),CCancelProdukt) then
      exit;
  Container.IncLock;
  Container.LoadGame(true);
  SaveGame.WerkStatt.CancelProjekt(Index,true);
  Container.LoadGame(false);
  AktuTechnikerList;
  Container.DecLock;
  InfoFrame.Redraw;
end;

constructor TVerWerkStatt.Create(Container: TDXContainer);
begin
  inherited;
  Animation:=paBottomToUp;

  SetPageInfo(StripHotKey(BWerkstatt));

  { Arbeitsmarkt Button }
  ArbeitsButton:=TDXBitmapButton.Create(Self);
  with ArbeitsButton do
  begin
    SetRect(571,530,110,28);
    FirstColor:=coFirstColor;
    SecondColor:=coSecondColor;
    AccerlateColor:=coAccerlateColor;
    OnClick:=MarktClick;
    RoundCorners:=rcLeft;
    SetButtonFont(Font);
    Text:=BVerMarkt;
    Hint:=HArbeitsmarkt;
  end;

  { LagerButton }
  LagerButton:=TDXBitmapButton.Create(Self);
  with LagerButton do
  begin
    SetRect(682,530,110,28);
    FirstColor:=coFirstColor;
    SecondColor:=coSecondColor;
    AccerlateColor:=coAccerlateColor;
    PageIndex:=PageVerLager;
    SetButtonFont(Font);
    RoundCorners:=rcRight;
    Text:=BLager;
    Hint:=HLager;
  end;

{ Informationsframe }
  InfoFrame:=TDXInfoFrame.Create(Self);
  with InfoFrame do
  begin
    SetRect(388,10,244,90);
    SetFont(Font);
    ColorSheme:=icsRed;
    Infos:=[ifAlphatron,ifWerk,ifLager];
    SystemKeyChange:=true;
  end;

{ Ihre Techniker }
  { Technikerlistbox }
  TechnikerList:=TDXListBox.Create(Self);
  with TechnikerList do
  begin
    SetRect(8,54,370,286);
    BorderColor:=coFirstColor;
    FocusColor:=coSecondColor;
    OnDblClick:=TechnikerListDblClick;
    SetFont(Font);
    SetButtonFont(ScrollBarFont);
    FirstColor:=coScrollBrush;
    SecondColor:=coScrollPen;
    Hint:=HTechniker;
    ItemHeight:=54;
    OwnerDraw:=true;
    OnChange:=TechnikerListChange;
    OnDrawItem:=DrawYourTechniker;
    HeadRowHeight:=17;
    HeadData:=MakeHeadData(LYourTechnik,FTechniker,FTechniker);
  end;

{ Ihre Projekte }
  { Projektlistbox }
  Projekte:=TDXListBox.Create(Self);
  with Projekte do
  begin
    SetRect(8,348,370,175);
    BorderColor:=coFirstColor;
    FocusColor:=coSecondColor;
    SetFont(Font);
    SetButtonFont(ScrollBarFont);
    FirstColor:=coScrollBrush;
    SecondColor:=coScrollPen;
    Hint:=HHersProjList;
    ItemHeight:=51;
    AlwaysChange:=true;
    OwnerDraw:=true;
    OnDrawItem:=DrawProjekt;
    OnDblClick:=ProjektDblClick;
    OnChange:=ProjektChange;
    OnMouseUp:=ProjektMouseUp;
    HeadRowHeight:=17;
    HeadData:=MakeHeadData(LProjekte,FProjekt,FProjekte);
  end;

  { Zuweisen }
  Zuweisen:=TDXBitmapButton.Create(Self);
  with Zuweisen do
  begin
    SetRect(388,54,110,28);
    SetButtonFont(Font);
    Hint:=HZuweisen;
    FirstColor:=coFirstColor;
    SecondColor:=coSecondColor;
    RoundCorners:=rcTop;
    AccerlateColor:=coAccerlateColor;
    OnClick:=SetProjekt;
    Text:=BLZuweisen;
  end;

  { Alle Zuweisen }
  ZuweisenAll:=TDXBitmapButton.Create(Self);
  with ZuweisenAll do
  begin
    SetRect(388,83,110,28);
    SetButtonFont(Font);
    Hint:=HZuweisenAll;
    FirstColor:=coFirstColor;
    SecondColor:=coSecondColor;
    RoundCorners:=rcNone;
    AccerlateColor:=coAccerlateColor;
    OnClick:=SetProjektAll;
    Text:=BLZuweisenAll;
  end;

  { Freie Zuweisen }
  ZuweisenFree:=TDXBitmapButton.Create(Self);
  with ZuweisenFree do
  begin
    SetRect(388,112,110,28);
    SetButtonFont(Font);
    Hint:=HZuweisenFree;
    FirstColor:=coFirstColor;
    SecondColor:=coSecondColor;
    RoundCorners:=rcBottom;
    AccerlateColor:=coAccerlateColor;
    OnClick:=SetProjektFree;
    Text:=BLZuweisenFree;
  end;

  { Ausbilden }
  Ausbilden:=TDXBitmapButton.Create(Self);
  with Ausbilden do
  begin
    SetRect(388,154,110,28);
    SetButtonFont(Font);
    Hint:=HTrainTech;
    FirstColor:=coFirstColor;
    SecondColor:=coSecondColor;
    AccerlateColor:=coAccerlateColor;
    OnClick:=SetAusbildung;
    RoundCorners:=rcTop;
    Text:=BLAusbilden;
  end;

  { Alle Ausbilden }
  AusbildenAll:=TDXBitmapButton.Create(Self);
  with AusbildenAll do
  begin
    SetRect(388,183,110,28);
    SetButtonFont(Font);
    Hint:=HATrainTech;
    FirstColor:=coFirstColor;
    SecondColor:=coSecondColor;
    AccerlateColor:=coAccerlateColor;
    OnClick:=SetAusbildungAll;
    RoundCorners:=rcNone;
    Text:=BLAusbildenAll;
  end;

  { Freie Ausbilden }
  AusbildFree:=TDXBitmapButton.Create(Self);
  with AusbildFree do
  begin
    SetRect(388,212,110,28);
    SetButtonFont(Font);
    Hint:=HFTrainTech;
    FirstColor:=coFirstColor;
    SecondColor:=coSecondColor;
    AccerlateColor:=coAccerlateColor;
    OnClick:=SetAusbildungFree;
    Text:=BLAusbildenFree;
    RoundCorners:=rcBottom;
  end;

  { Neues Projekt }
  NewProjekt:=TDXBitmapButton.Create(Self);
  with NewProjekt do
  begin
    SetRect(388,298,110,28);
    SetButtonFont(Font);
    Hint:=HNewProjekt;
    FirstColor:=coFirstColor;
    SecondColor:=coSecondColor;
    PageIndex:=PageNewProjekt;
    AccerlateColor:=coAccerlateColor;
    Text:=BWNewProjekt;
    RoundCorners:=rcTop;
  end;

  { Projekt abbrechen }
  CancelProjekt:=TDXBitmapButton.Create(Self);
  with CancelProjekt do
  begin
    SetRect(388,327,110,28);
    SetButtonFont(Font);
    Hint:=HCancelProj;
    FirstColor:=coFirstColor;
    SecondColor:=coSecondColor;
    AccerlateColor:=coAccerlateColor;
    OnClick:=CancelProjektClick;
    RoundCorners:=rcBottom;
    Text:=BbCancel;
  end;

  { Produktionsinformationen }
  ProduktInfo:=TDXItemInfo.Create(Self);
  with ProduktInfo do
  begin
    InfoType:=itProduktion;
    ItemValid:=false;
    SetFont(Font);
    BorderColor:=coFirstColor;
    SetRect(508,54,284,469);
    Caption:=LProduktion;
    Hint:=HProduktInfo;
  end;

  UnionRect(fRedrawRect,ProduktInfo.ClientRect,Projekte.ClientRect);
  UnionRect(fRedrawRect,fRedrawRect,TechnikerList.ClientRect);

  UnionRect(fButtonRect,CancelProjekt.ClientRect,Zuweisen.ClientRect);

  basis_api_RegisterChangeBaseHandler(OnSelectedBasisChange);
end;

procedure TVerWerkStatt.DrawFortSchritt(Surface: TDirectDrawSurface;const Mem: TDDSurfaceDesc;
  X, Y, Width: Integer; Prozent: double);
var
  Links  : Integer;
  Oben   : Integer;
  Full   : boolean;
  Zeiger : Cardinal;
  Farbe  : Cardinal;
  Beginn : Integer;
  Ende   : Integer;
begin
  Beginn:=0;
  Ende:=17;
  if Surface.ClippingRect.Top>Y then Beginn:=Surface.ClippingRect.Top-Y;
  if Surface.ClippingRect.Bottom<Y+17 then Ende:=min(17,Surface.ClippingRect.Bottom-Y-1);
  if Mode32Bit then
  begin
    for Links:=0 to Width-1 do
    begin
      Farbe:=WerkColorTable[100-round(Links/Width*100)];
      Full:=round(Prozent/100*Width)<=Links;
      Zeiger:=Integer(Mem.lpSurface)+((Y+Beginn)*Mem.lPitch)+((X+Links) shl 2);
      for Oben:=Beginn to Ende do
      begin
        if not Full then
          PLongWord(Zeiger)^:=Farbe
        else
          PLongWord(Zeiger)^:=((Farbe and Mask) shr 1);
        inc(Zeiger,Mem.lPitch)
      end;
    end;
  end
  else
  begin
    for Links:=0 to Width-1 do
    begin
      Farbe:=WerkColorTable[100-round(Links/Width*100)];
      Full:=round(Prozent/100*Width)<=Links;
      Zeiger:=Integer(Mem.lpSurface)+((Y+Beginn)*Mem.lPitch)+((X+Links) shl 1);
      for Oben:=Beginn to Ende do
      begin
        if not Full then
          PWord(Zeiger)^:=Farbe
        else
          PWord(Zeiger)^:=(Farbe and Mask) shr 1;
        inc(Zeiger,Mem.lPitch)
      end;
    end;
  end;
end;

procedure TVerWerkStatt.DrawProjekt(Sender: TDXComponent;
  Surface: TDirectDrawSurface;const Mem: TDDSurfaceDesc; Rect: TRect; Index: Integer;
  Selected: boolean);
var
  Text   : String;
  Item   : TProduktion;
  SIndex : Integer;
  ProRect: TRect;
begin
  SIndex:=StrToInt(Projekte.Items[Index]);
  Item:=SaveGame.WerkStatt.Projekte[SIndex];
  if Selected then
  begin
    Projekte.DrawSelection(Surface,Mem,Rect,bcMaroon);
  end;
  if Item.Anzahl=101 then
    WhiteStdFont.Draw(Surface,Rect.Left+6,Rect.Top+6,SaveGame.LagerListe[SaveGame.LagerListe.IndexOfID(Item.ItemID)].Name)
  else
  begin
    YellowStdFont.Draw(Surface,Rect.Left+6,Rect.Top+6,Format(FIntegerx,[Item.Anzahl]));
    WhiteStdFont.Draw(Surface,Rect.Left+36,Rect.Top+6,SaveGame.LagerListe[SaveGame.LagerListe.IndexOfID(Item.ItemID)].Name);
  end;
  ProRect:=Classes.Rect(Rect.Left+6,Rect.Top+25,Rect.Right-6,Rect.Bottom-6);
  IntersectRect(ProRect,ProRect,Surface.ClippingRect);
  Rectangle(Surface,Mem,ProRect,bcMaroon);
  DrawFortSchritt(Surface,Mem,Rect.Left+7,Rect.Top+26,Projekte.Width-14,SaveGame.WerkStatt.PercentDone(SIndex));
  if SaveGame.WerkStatt.Projekte[SIndex].Product then
  begin
    Text:=Format(FFloatPercent,[SaveGame.WerkStatt.PercentDone(SIndex)]);
    YellowStdFont.Draw(Surface,Rect.Left+10,Rect.Top+28,Text);
    Text:=Format(FFaehig,[SaveGame.WerkStatt.ProjektStrength(SIndex)/1]);
    YellowStdFont.Draw(Surface,Rect.Right-10-WhiteStdFont.TextWidth(text),Rect.Top+28,Text);
  end
  else
  begin
    Text:=Format(FWNeededAlphatron,[(SaveGame.WerkStatt.Projekte[SIndex].Kost-SaveGame.WerkStatt.Projekte[SIndex].Reserved)/1]);
    YellowStdFont.Draw(Surface,Rect.Left+10,Rect.Top+28,Text);
  end;
end;

procedure TVerWerkStatt.DrawYourTechniker(Sender: TDXComponent;
  Surface: TDirectDrawSurface;const Mem: TDDSurfaceDesc;Rect: TRect; Index: Integer;
  Selected: boolean);
var
  Text      : String;
  Projekt   : Integer;
  Item      : Cardinal;
  Font      : TDirectFont;
  SIndex    : Integer;
  Techniker : TTechniker;
begin
  if Selected then
    TechnikerList.DrawSelection(Surface,Mem,Rect,bcMaroon);

  SIndex:=StrToInt(TechnikerList.Items[Index]);

  Techniker:=SaveGame.WerkStatt.Techniker[SIndex];

  SaveGame.WerkStatt.DrawDXTechniker(Index,Surface,Mem,Rect.Left+5,Rect.Top+5);
  YellowStdFont.Draw(Surface,Rect.Left+48,Rect.Top+8,Techniker.Name);

  Text:=Format(FFaehig,[trunc(Techniker.Strength)/1]);
  WhiteStdFont.Draw(Surface,Rect.Right-6-WhiteStdFont.TextWidth(Text),Rect.Top+8,Text);

  Font:=WhiteStdFont;
  if Techniker.AlphatronMining then
  begin
    Text:=ST0501040003;
    Font:=RedStdFont;
  end
  else if Techniker.Ausbil then
  begin
    Text:=LTraining;
    Font:=GreenStdFont;
  end
  else if Techniker.ProjektID<>0 then
  begin
    Projekt:=SaveGame.WerkStatt.GetProjektOfID(Techniker.ProjektID);
    Item:=SaveGame.WerkStatt.Projekte[Projekt].ItemID;
    Text:=lager_api_GetItem(Item).Name;
  end
  else if Techniker.Repair then
  begin
    Text:=ST0501250002;
    Font:=RedStdFont;
  end
  else
  begin
    Text:=LNoProjekt;
    Font:=RedStdFont;
  end;
  Font.Draw(Surface,Rect.Left+48,Rect.Top+28,Text);
end;

procedure TVerWerkStatt.LeaveModal;
begin
  SetDefaults;
end;

procedure TVerWerkStatt.MarktClick(Sender: TObject);
begin
  ActivePage:=ptTechnik;
  ChangePage(PageVerMarkt);
end;

// Wird aufgerufen, wenn die Basis �ber den Infoframe gewechselt wurde
procedure TVerWerkStatt.OnSelectedBasisChange(Sender: TObject);
begin
  if Container.ActivePage<>Self then
    exit;

  AktuTechnikerList;  
end;

procedure TVerWerkStatt.ProjektChange(Sender: TObject);
begin
  if Projekte.Items.Count>0 then
    ProduktInfo.Production:=SaveGame.WerkStatt.Projekte[StrToInt(Projekte.Items[Projekte.ItemIndex])]
  else
    ProduktInfo.ItemValid:=false;
end;

procedure TVerWerkStatt.ProjektDblClick(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; x, y: Integer);
begin
  if Button=mbLeft then
  begin
    if Projekte.Items.Count=0 then exit;
    ViewProjekt:=StrToInt(Projekte.Items[Projekte.ItemIndex]);
    ChangePage(PageNewProjekt);
  end;
end;

procedure TVerWerkStatt.ProjektMouseUp(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; x, y: Integer);
begin
  if Button<>mbRight then exit;
  if Projekte.Items.Count=0 then exit;
  ufopaedie_api_ShowEntry(upeLagerItem,SaveGame.LagerListe.IndexOfID(SaveGame.WerkStatt.Projekte[StrToInt(Projekte.Items[Projekte.ItemIndex])].ItemID));
end;

procedure TVerWerkStatt.SetAusbildung(Sender: TObject);
begin
  Container.IncLock;
  try
    SaveGame.WerkStatt.ChangeTraining(StrToInt(TechnikerList.Items[TechnikerList.ItemIndex]));
  except
    on E: Exception do
    begin
      Container.DecLock;
      game_api_MessageBox(E.Message,CCancelAction);
      Container.IncLock;
    end;
  end;
  Container.Lock;
  TechnikerList.ItemIndex:=TechnikerList.ItemIndex+1;
  Container.Unlock;
  TechnikerList.Redraw;
  ProduktInfo.Redraw;
  InfoFrame.Redraw;
  SetButtons;
  Container.DecLock;
  Projekte.Redraw;
end;

procedure TVerWerkStatt.SetAusbildungAll(Sender: TObject);
var
  Dummy  : Integer;
  Change : boolean;
begin
  Change:=true;
  for Dummy:=0 to TechnikerList.Items.Count-1 do
    if not SaveGame.WerkStatt.Techniker[StrToInt(TechnikerList.Items[Dummy])].Ausbil then
      Change:=false;

  Container.Lock;
  for Dummy:=0 to TechnikerList.Items.Count-1 do
  begin
    try
      if Change then
        SaveGame.WerkStatt.ChangeTraining(StrToInt(TechnikerList.Items[Dummy]))
      else
        SaveGame.WerkStatt.SetTraining(StrToInt(TechnikerList.Items[Dummy]));
    except
{      on E: Exception do
      begin
        Container.Unlock;
        game_api_MessageBox(E.Message,CCancelAction);
        Container.Lock;
      end;}
    end;
  end;
  Container.LoadGame(false);
  TechnikerList.Redraw;
  InfoFrame.Redraw;
  ProduktInfo.Redraw;
  SetButtons;
  Container.DecLock;
  Projekte.Redraw;
end;

procedure TVerWerkStatt.SetAusbildungFree(Sender: TObject);
var
  Dummy: Integer;
begin
  Container.IncLock;
  Container.LoadGame(true);
  for Dummy:=0 to TechnikerList.Items.Count-1 do
  begin
    try
      if SaveGame.WerkStatt.IsTechnikerFree(StrToInt(TechnikerList.Items[Dummy])) then
        SaveGame.WerkStatt.SetTraining(StrToInt(TechnikerList.Items[Dummy]));
    except
      on E: Exception do
      begin
        Container.Unlock;
        game_api_MessageBox(E.Message,CCancelAction);
        Container.Lock;
      end;
    end;
  end;
  Container.LoadGame(false);
  InfoFrame.Redraw;
  TechnikerList.Redraw;
  ProduktInfo.Redraw;
  SetButtons;
  Container.DecLock;
  Projekte.Redraw;
end;

procedure TVerWerkStatt.SetButtons;
var
  Dummy       : Integer;
  AllTraining : boolean;
  Count       : Integer;
  NobodyFree  : boolean;

begin
  Container.Lock;
  Zuweisen.Enabled:=(TechnikerList.Items.Count>0) and (Projekte.Items.Count>0);
  ZuweisenAll.Enabled:=(TechnikerList.Items.Count>0) and (Projekte.Items.Count>0);
  ZuweisenFree.Enabled:=(TechnikerList.Items.Count>0) and (Projekte.Items.Count>0);
  Ausbilden.Enabled:=TechnikerList.Items.Count>0;
  AusbildenAll.Enabled:=TechnikerList.Items.Count>0;
  AusbildFree.Enabled:=TechnikerList.Items.Count>0;
  CancelProjekt.Enabled:=Projekte.Items.Count>0;
  if TechnikerList.Items.Count>0 then
  begin
    AllTraining:=true;
    Dummy:=0;
    Count:=TechnikerList.Items.Count;
    while (Dummy<Count) and AllTraining do
    begin
      if not SaveGame.WerkStatt.Techniker[StrToInt(TechnikerList.Items[Dummy])].Ausbil then AllTraining:=false;
      inc(Dummy);
    end;
    if SaveGame.WerkStatt.Techniker[StrToInt(TechnikerList.Items[TechnikerList.ItemIndex])].Ausbil then
    begin
      Ausbilden.Text:=BLFree;
      Ausbilden.Hint:=HFreeTech;
    end
    else
    begin
      Ausbilden.Text:=BLAusbilden;
      Ausbilden.Hint:=HTrainTech;
    end;
    if AllTraining then
    begin
      AusbildenAll.Text:=BLFreeAll;
      AusbildenAll.Hint:=HAFreeTech;
    end
    else
    begin
      AusbildenAll.Text:=BLAusbildenAll;
      AusbildenAll.Hint:=HATrainTech;
    end;
  end;

  NobodyFree:=true;
  Dummy:=0;
  Count:=SaveGame.WerkStatt.Count;
  while (Dummy<Count) and NobodyFree do
  begin
    if SaveGame.WerkStatt.InSelectedBasis(Dummy) then
    begin
      if not SaveGame.WerkStatt.Techniker[Dummy].Ausbil and
        (SaveGame.WerkStatt.GetProjektOfID(SaveGame.WerkStatt.Techniker[Dummy].ProjektID) = -1) then
      begin
        NobodyFree:=false;
      end;
    end;
    inc(Dummy);
  end;
  if NobodyFree then
  begin
    AusbildFree.Enabled:=false;
    ZuweisenFree.Enabled:=false;
  end
  else
  begin
    AusbildFree.Enabled:=true;
    ZuweisenFree.Enabled:=Projekte.Items.Count>0;
  end;

  Container.Unlock;
  Container.RedrawArea(fButtonRect,Container.Surface);
end;

procedure TVerWerkStatt.SetDefaults;
begin
  InfoFrame.Reset;
  AktuTechnikerList;
  SetButtons;
end;

procedure TVerWerkStatt.SetProjekt(Sender: TObject);
begin
  try
    SaveGame.WerkStatt.SetProjekt(StrToInt(TechnikerList.Items[TechnikerList.Itemindex]),StrToInt(Projekte.Items[Projekte.Itemindex]));
  except
    on E:Exception do
    begin
      game_api_MessageBox(E.Message,CCancelAction);
      exit;
    end;
  end;
  Container.Lock;
  TechnikerList.ItemIndex:=TechnikerList.ItemIndex+1;
  Container.LoadGame(false);
  TechnikerList.Redraw;         
  ProduktInfo.Redraw;
  InfoFrame.Redraw;
  SetButtons;
  Container.DecLock;
  Projekte.Redraw;
end;

procedure TVerWerkStatt.SetProjektAll(Sender: TObject);
var
  Dummy  : Integer;
  PIndex : Integer;
begin
  Container.IncLock;
  Container.LoadGame(true);
  try
    PIndex:=StrToInt(Projekte.Items[Projekte.ItemIndex]);
    for Dummy:=0 to TechnikerList.Items.Count-1 do
      SaveGame.WerkStatt.SetProjekt(StrToInt(TechnikerList.Items[Dummy]),PIndex);
  except
    on E:Exception do
    begin
      Container.Unlock;
      game_api_MessageBox(E.Message,CCancelAction);
      Container.Lock;
    end;
  end;
  Container.LoadGame(false);
  Container.DecLock;
  InfoFrame.Redraw;
  ProduktInfo.Redraw;
  TechnikerList.Redraw;
  SetButtons;
  Container.DecLock;
  Projekte.Redraw;
end;

procedure TVerWerkStatt.SetProjektFree(Sender: TObject);
var
  Dummy  : Integer;
  PIndex : Integer;
  TIndex : Integer;
begin
  Container.Lock;
  try
    PIndex:=StrToInt(Projekte.Items[Projekte.ItemIndex]);
    for Dummy:=0 to TechnikerList.Items.Count-1 do
    begin
      TIndex:=StrToInt(TechnikerList.Items[Dummy]);
      if SaveGame.WerkStatt.IsTechnikerFree(TIndex) then
        SaveGame.WerkStatt.SetProjekt(TIndex,PIndex);
    end;
  except
    on E:Exception do
    begin
      Container.Unlock;
      game_api_MessageBox(E.Message,CCancelAction);
      Container.Lock;
    end;
  end;
  Container.UnLock;
  InfoFrame.Redraw;
  ProduktInfo.Redraw;
  TechnikerList.Redraw;
  SetButtons;
  Container.DecLock;
  Projekte.Redraw;
end;

procedure TVerWerkStatt.TechnikerListChange(Sender: TObject);
begin
  SetButtons;
end;

procedure TVerWerkStatt.TechnikerListDblClick(Sender: TObject;Button: TMouseButton;Shift: TShiftState;x,y: Integer);
var
  Index: Integer;
begin
  if TechnikerList.Items.Count=0 then exit;
  Index:=SaveGame.WerkStatt.GetProjektOfID(SaveGame.WerkStatt.Techniker[StrToInt(TechnikerList.Items[TechnikerList.ItemIndex])].ProjektID);
  if Index=-1 then exit;
  Projekte.ItemIndex:=Index;
end;

end.
