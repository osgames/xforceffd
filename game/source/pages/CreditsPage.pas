{********************************************************************************
*										*
* Copyright (C) 2004  X-Force Team - jim_raynor@web.de				*
*										*
* This file is part of X-Force: Fight For Destiny (X-Force).			*
*										*
* X-Force is free software; you can redistribute it and/or modify		*
* it under the terms of the GNU General Public License as published by		*
* the Free Software Foundation; either version 2 of the License, or		*
* (at your option) any later version.						*
*										*
* X-Force is distributed in the hope that it will be useful,			*
* but WITHOUT ANY WARRANTY; without even the implied warranty of		*
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the			*
* GNU General Public License for more details.					*
*										*
* You should have received a copy of the GNU General Public License		*
* along with X-Force; if not, write to the Free Software			*
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA	*
*										*
*********************************************************************************
*										*
* Anzeige der Mitwirkenden zu X-Force (Credits)					*
*										*
********************************************************************************}

{$I ../Settings.inc}

unit CreditsPage;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  KD4Page, DXContainer, DXCreditsScroll, Defines, StringConst, ArchivFile,
  DXLabel, Blending;

type
  TCreditsPage = class(TKD4Page)
  private
    { Private-Deklarationen }
  protected
    MStream: TMemoryStream;
    procedure StopCredits(Sender: TObject);
    { Protected-Deklarationen }
  public
    Scroller    : TDXCreditsScroll;
    constructor Create(Container: TDXContainer);override;
    destructor Destroy;override;
    procedure SetDefaults;override;
    procedure PageShown;override;
    procedure SetHint(Hint: String);override;
  end;

implementation

{ TCreditsPage }

constructor TCreditsPage.Create(Container: TDXContainer);
var
  Archiv: TArchivFile;
begin
  inherited;
  Animation:=paBottomToUp;
  Archiv:=TArchivFile.Create;
  Archiv.OpenArchiv(FGameDataFile,true);
  Archiv.OpenRessource('CreditsText');
  MStream:=TMemoryStream.Create;
  MStream.CopyFrom(Archiv.Stream,0);
  Archiv.Free;
  Hint:=HEscToEnd;
  HintLabel.BorderColor:=clNavy;
  HintLabel.BlendColor:=bcNavy;

  Scroller:=TDXCreditsScroll.Create(Self);
  with Scroller do
  begin
    SetButtonFont(Font);
    Font.Color:=clWhite;
    SetRect(150,50,500,500);
    Stream:=MStream;
    OnStop:=StopCredits;
  end;

end;

destructor TCreditsPage.Destroy;
begin
  MStream.Free;
  inherited;
end;

procedure TCreditsPage.StopCredits(Sender: TObject);
begin
//  Container.PlayMusic(fMenuFile);
  ChangePage(PageMainMenu);
end;

procedure TCreditsPage.PageShown;
begin
//  Container.PlayMusic(fCreditFile);
  Scroller.Show;
end;

procedure TCreditsPage.SetDefaults;
begin
  inherited;
  Scroller.Start;
end;

procedure TCreditsPage.SetHint(Hint: String);
begin
  HintLabel.Text:=HEscToEnd;
end;

end.
