{********************************************************************************
*										*
* Copyright (C) 2004  X-Force Team - jim_raynor@web.de				*
*										*
* This file is part of X-Force: Fight For Destiny (X-Force).			*
*										*
* X-Force is free software; you can redistribute it and/or modify		*
* it under the terms of the GNU General Public License as published by		*
* the Free Software Foundation; either version 2 of the License, or		*
* (at your option) any later version.						*
*										*
* X-Force is distributed in the hope that it will be useful,			*
* but WITHOUT ANY WARRANTY; without even the implied warranty of		*
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the			*
* GNU General Public License for more details.					*
*										*
* You should have received a copy of the GNU General Public License		*
* along with X-Force; if not, write to the Free Software			*
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA	*
*										*
*********************************************************************************
*										*
* Basisklasse f�r alle Seiten in X-Force. (Abgeleitet von TDXPage aus		*
* DXContainer									*
*										*
********************************************************************************}

{$I ../Settings.inc}

unit KD4Page;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  KD4SaveGame, DXContainer, DXLabel, Defines, KD4Utils, Blending,
  XForce_types, TraceFile, NGTypes;

type
  TKD4Page = class(TDXPage)
  private
    fSaveGame          : TKD4SaveGame;
    { Private-Deklarationen }
  protected
    fNavigator         : TObject;
    {$IFDEF MUSICTEST}
    fIsShowMusic: Boolean;
    procedure SelectMusic(Data: Integer);dynamic;
    {$ENDIF}
    {$IFDEF SHOWMEMORY}
    function ShowMemory(Sender: TObject;Frames: Integer): Boolean;dynamic;
    {$ENDIF}
    procedure HandleFKey(Number: Byte);override;
    procedure SetSaveGame(const Value: TKD4SaveGame);virtual;
    { Protected-Deklarationen }
  public
    HintLabel : TDXLabel;
    {$IFDEF SHOWMEMORY}
    MemoryLabel: TDXLabel;
    {$ENDIF}
    constructor Create(Container: TDXContainer);override;
    destructor Destroy;override;
    procedure SetHint(Hint: String);override;
    procedure QueryText(Caption: String;MaxLength: Integer;var Text: String;Theme: TColorTheme);
    procedure SetDefaults;override;
    procedure SetPageInfo(Caption: String);

    property SaveGame       : TKD4SaveGame read fSaveGame write SetSaveGame;
  end;

implementation

uses {$IFDEF DEBUGMODE}DXListBoxWindow, {$ENDIF}
  DXInfoFrame, ExtRecord, game_api, archiv_utils, record_utils;

var
{$IFDEF MUSICTEST}
  MusikList : TDXListBoxWindow;
{$ENDIF}
  PageInfos : TRecordArray;
  PageInfo  : TExtRecordDefinition;

{ TKD4Page }

constructor TKD4Page.Create(Container: TDXContainer);
var
  Page     : TExtRecord;
begin
  inherited;
  HintLabel:=TDXLabel.Create(Self);
  with HintLabel do
  begin
    SetFont(Font);
    Blending:=true;
    BlendColor:=bcMaroon;
    BorderColor:=clMaroon;
    SetRect(0,ScreenHeight+(HintLabel.Font.Height shl 1)-4,ScreenWidth,-((HintLabel.Font.Height shl 1)-4)+1);
    Alignment:=taCenter;
    AutoSize:=false;
  end;

  fNavigator:=nil;

  // Einstellungen f�r die Seiten einlesen
  if PageInfos=nil then
  begin
    PageInfo:=TExtRecordDefinition.Create('PageInfos');
    PageInfo.AddString('classname',100);
    PageInfo.AddString('background',100);
    PageInfo.AddInteger('id',0,high(Integer));
    PageInfo.AddBoolean('modal');

    PageInfos:=PageInfo.ReadRecordsFromXML(archiv_utils_ReadToString(FGameDataFile,'PageInfos'),'page','pageinfos');
  end;

  Page:=record_utils_FindString(PageInfos,'classname',Self.ClassName);
  Assert(Page<>nil,Format('Zur Seite %s sind keine Informationen verf�gbar',[Self.ClassName]));

  BackGroundName:=Page.GetString('background');
  PageIndex:=Page.GetInteger('id');
  ShowPageModal:=Page.GetBoolean('modal');

  {$IFDEF SHOWMEMORY}
//  RegisterSystemKey('s',ShowMemory,0);
  MemoryLabel:=TDXLabel.Create(Self);
  with MemoryLabel do
  begin
    SetFont(Font);
    Blending:=true;
    BlendColor:=bcMaroon;
    BorderColor:=clMaroon;
    SetRect(700,580,100,20);
    Alignment:=taRightJustify;
    AutoSize:=false;
  end;

  Container.AddFrameFunction(ShowMemory,Self,25);
  {$ENDIF}

  {$IFDEF MUSICTEST}
  RegisterSystemKey('m',SelectMusic,0);

  fIsShowMusic:=false;

  if MusikList=nil then
  begin
    MusikList:=TDXListBoxWindow.Create(Self);
    with MusikList do
    begin
      SetRect(0,0,300,200);
      Caption:='Musik w�hlen';
      SetButtonFont(Font);
      CaptionColor:=coFontColor;
      AccerlateColor:=coAccerlateColor;
      Font.Size:=coFontSize;
      FirstColor:=$00400000;
      SecondColor:=clBlue;
      BlendColor:=bcDarkNavy;
    end;
  end;
  {$ENDIF}
end;

destructor TKD4Page.Destroy;
begin
  fNavigator.Free;
  inherited;
end;

procedure TKD4Page.HandleFKey(Number: Byte);
var
  NewPage : TDXPage;
  Dummy   : Integer;
begin
  if (fNavigator<>nil) and TDXPageNavigator(fNavigator).Visible then
  begin
    if GetKeyState(VK_CONTROL)<0 then
    begin
      // Alte F-Taste l�schen
      for Dummy:=1 to 12 do
      begin
        if Fkeys[Dummy]=PageIndex then
          fKeys[Dummy]:=-1;
      end;
      // Neue F-Taste Festlegen
      FKeys[Number]:=PageIndex;
      TDXPageNavigator(fNavigator).AktuFKeyInfo;
    end
    else
    begin
      NewPage:=Container.GetPageOfIndex(FKeys[Number]);
      if (NewPage<>Container.ActivePage) and (NewPage<>nil) then
      begin
        Container.ActivePage:=NewPage;
      end;
    end;
  end;
end;

procedure TKD4Page.QueryText(Caption: String;MaxLength: Integer; var Text: String;Theme: TColorTheme);
begin
  game_api_QueryText(Caption,MaxLength,Text);
end;

{$IFDEF MUSICTEST}
procedure TKD4Page.SelectMusic(Data: Integer);
var
  Dummy: Integer;
  Index: Integer;

  procedure AddFiles(Ext: String);
  var
    Rec  : TSearchRec;
  begin
    if FindFirst('musik\*.'+Ext,faAnyFile,Rec)=0 then
    begin
      repeat
        MusikList.Items.Add(Rec.Name);
      until FindNext(Rec)<>0;
    end;
    FindClose(Rec);
  end;

begin
  if fIsShowMusic then
    exit;
  fIsShowMusic:=true;
  MusikList.Items.Clear;
  AddFiles('ogg');
  AddFiles('mp3');
  AddFiles('wav');
  Index:=MusikList.Show;
  if Index<>-1 then
    Container.PlayMusic('musik\'+MusikList.Items[Index],true);
  fIsShowMusic:=false;
end;
{$ENDIF}

procedure TKD4Page.SetDefaults;
begin
end;

procedure TKD4Page.SetHint(Hint: String);
begin
  HintLabel.Text:=Hint;
end;

procedure TKD4Page.SetPageInfo(Caption: String);
begin
  fNavigator:=TDXPageNavigator.Create(Self);
  TDXPageNavigator(fNavigator).RegisterPage(Caption,PageIndex);

  Self.Caption:=Caption;
end;

procedure TKD4Page.SetSaveGame(const Value: TKD4SaveGame);
begin
  fSaveGame := Value;
end;

{$IFDEF SHOWMEMORY}
function TKD4Page.ShowMemory(Sender: TObject;Frames: Integer): Boolean;
begin
  MemoryLabel.Text:=IntToStr(GetProcessMemory);
  result:=true;
end;
{$ENDIF}

initialization
finalization
  if PageInfos<>nil then
  begin
    record_utils_ClearArray(PageInfos);
    PageInfo.Free;
  end;
end.
