{********************************************************************************
*										*
* Copyright (C) 2004  X-Force Team - jim_raynor@web.de				*
*										*
* This file is part of X-Force: Fight For Destiny (X-Force).			*
*										*
* X-Force is free software; you can redistribute it and/or modify		*
* it under the terms of the GNU General Public License as published by		*
* the Free Software Foundation; either version 2 of the License, or		*
* (at your option) any later version.						*
*										*
* X-Force is distributed in the hope that it will be useful,			*
* but WITHOUT ANY WARRANTY; without even the implied warranty of		*
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the			*
* GNU General Public License for more details.					*
*										*
* You should have received a copy of the GNU General Public License		*
* along with X-Force; if not, write to the Free Software			*
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA	*
*										*
*********************************************************************************
*										*
* Einstellungen um einen Raumschiffkampf zu testen (Hauptmen�->Kampfsimulation) *
*										*
********************************************************************************}

{$I ../Settings.inc}

unit KampfSimulation;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  DXContainer, KD4Page, DXBitmapButton, Defines, DXGroupCaption,
  XForce_types, DXEdit, ArchivFile, Koding, DXListBox, KD4Utils, DXDraws, Blending,
  DXSpinEdit, UFOKampfIntro, RaumschiffList, UFOList, Loader, DXListBoxWindow,
  DirectDraw, DirectFont, TraceFile, math, ExtRecord, ProjektRecords,
  ObjektRecords, StringConst;

type
  TControlType  = (ctNone,ctSchiff,ctShield,ctMotor,ctWaffe);

  TSimuControls = record
    CType      : TControlType;
    Component  : TDXComponent;
  end;

  TUFOControls = record
    Visible    : boolean;
    Component  : TDXComponent;
  end;

  TSelectedComponent = record
    SatzID     : Cardinal;
    Index      : Integer;
  end;

  TSavedWeapon = record
    Name         : String;
    ID           : Cardinal;
    Munition     : Word;
    WaffType     : Word;
    Strength     : Word;
    PPS          : Word;
    LadeZeit     : Word;
    Verfolgung   : Word;
    ReichWeite   : Word;
  end;

  TKampfSchiff = record
    Name         : String;
    Model        : String;
    Hitpoints    : Word;
    WZellen      : Word;
    Schild       : String;
    Schildpunkte : Word;
    Abwehr       : Word;
    AufladeZeit  : Word;
    Motor        : String;
    Geschwindig  : Word;
    Duesen       : Word;
    Waffen       : Array[1..3] of TSavedWeapon;
  end;

  TKampfSimulation = class(TKD4Page)
  private
    fSchiffRect    : TRect;
    fUFORect       : TRect;
    fModells       : Array of TRaumschiffModel;
    fShields       : Array of TShield;
    fWaffen        : Array of TWaffenZelle;
    fMotors        : Array of TMotor;
    fUFOs          : Array of TUFOModel;
    fUFOComponents : Array of TUFOControls;
    fSchComponents : Array of TSimuControls;
    fSelected      : Array[0..6] of TSelectedComponent;
    fWZellen       : Array[1..3] of TWaffenZelle;
    fKampfIntro    : TUFOKampfIntro;
    fRaumschiffe   : TRaumschiffList;
    fUFOList       : TUFOList;
    fList          : Integer;  // 0 - Schiff, 1 - Schild, 2 - Motor, 3 - Waffe, 4 - UFO
    Files          : Array of TSetIdentifier;
    fSelectedSatz  : Cardinal;
    Ships          : Array of TKampfSchiff;
    procedure DrawMotor(Sender: TDXComponent;Surface: TDirectDrawSurface;const Mem: TDDSurfaceDesc; Rect: TRect; Index: Integer;Selected: boolean);
    procedure DrawShield(Sender: TDXComponent;Surface: TDirectDrawSurface;const Mem: TDDSurfaceDesc; Rect: TRect; Index: Integer;Selected: boolean);
    procedure DrawModel(Sender: TDXComponent;Surface: TDirectDrawSurface;const Mem: TDDSurfaceDesc; Rect: TRect; Index: Integer;Selected: boolean);
    procedure DrawUFO(Sender: TDXComponent;Surface: TDirectDrawSurface;const Mem: TDDSurfaceDesc; Rect: TRect; Index: Integer;Selected: boolean);
    procedure DrawWaffen(Sender: TDXComponent;Surface: TDirectDrawSurface;const Mem: TDDSurfaceDesc; Rect: TRect; Index: Integer;Selected: boolean);
    procedure ApplyShip(Sender: TObject);
    procedure ApplyShield(Sender: TObject);
    procedure ApplyMotor(Sender: TObject);
    procedure ApplyUFO(Sender: TObject);
    procedure ApplyWaffe(Sender: TObject);
    procedure ShowUFO(Show: boolean);
    procedure ChangeShipSetting(Sender: TObject);
    procedure ChangeShieldSetting(Sender: TObject);
    procedure ChangeMotorSetting(Sender: TObject);
    procedure NeedDuesen(Value: Integer;var Text: String);
    procedure NeedWaffe(Value: Integer;var Text: String);
    procedure NeedWaffTyp(Value: Integer;var Text: String);
    procedure NeedVerfolgen(Value: Integer;var Text: String);
    procedure AddComponent(Compo: TDXComponent;DefVis: Boolean);overload;
    procedure AddComponent(Compo: TDXComponent;Typ: TControlType);overload;
    procedure WaffenZellenChanged(Sender: TObject);
    procedure WaffeChanged(Sender: TObject);
    procedure WNameChange(Sender: TObject);
    procedure WaffenSettingChange(Sender: TObject);
    procedure StartKampf(Sender: TObject);
    procedure ChooseSchiffSet(Sender: TObject);
    procedure ChooseUFOSet(Sender: TObject);
    procedure ShowPage(Sender: TObject);
    procedure UserButtonClick(Sender: TObject);
    procedure UFOUserButtonClick(Sender: TObject);
    procedure SaveButtonClick(Sender: TObject);
    procedure LoadButtonClick(Sender: TObject);

    procedure LoadShips;
    { Private-Deklarationen }
  protected
    procedure LoadModells(FileName: String);
    procedure LoadUFOModells(FileName: String);
    procedure ReadGameSets;
    function ChooseGameSet: Integer;
    { Protected-Deklarationen }
  public
    CloseButton  : TDXBitmapButton;
    Raumschiff   : TDXGroupCaption;
    UFOName      : TDXEdit;
    UFOHitPoints : TDXSpinEdit;
    UFOShield    : TDXSpinEdit;
    UFOAttack    : TDXSpinEdit;
    UFOTreff     : TDXSpinEdit;
    UFOPDefense  : TDXSpinEdit;
    UFORDefense  : TDXSpinEdit;
    UFOLDefense  : TDXSpinEdit;
    UFOLadeZeit  : TDXSpinEdit;
    UFOPPSEdit   : TDXSpinEdit;
    NameEdit     : TDXEdit;
    UserButton   : TDXBitmapButton;
    ModelButton  : TDXBitmapButton;
    ShieldButton : TDXBitmapButton;
    MotorButton  : TDXBitmapButton;
    WaffenButton : TDXBitmapButton;
    List         : TDXListBox;
    UFOList      : TDXListBox;
    ModelBox     : TDXEdit;
    ShieldBox    : TDXEdit;
    MotorBox     : TDXEdit;
    WName        : TDXEdit;
    WaffenBox    : TDXSpinEdit;
    HitPointEdit : TDXSpinEdit;
    WZEdit       : TDXSpinEdit;
    AttackEdit   : TDXSpinEdit;
    SchussEdit   : TDXSpinEdit;
    TypeEdit     : TDXSpinEdit;
    LadeEdit     : TDXSpinEdit;
    FolgeEdit    : TDXSpinEdit;
    WPPSEdit     : TDXSpinEdit;
    SPointEdit   : TDXSpinEdit;
    SAbwehrEdit  : TDXSpinEdit;
    LadeZeitEdit : TDXSpinEdit;
    PPSEdit      : TDXSpinEdit;
    WeiteEdit    : TDXSpinEdit;
    DuesenEdit   : TDXSpinEdit;
    GameSetUFOBox: TDXEdit;
    GameSetBox   : TDXEdit;
    GameSetChoose: TDXBitmapButton;
    ListBoxWindow: TDXListBoxWindow;
    EditCaption  : TDXGroupCaption;
    { Speichern und Laden der Raumschiffeinstellungen }
    LoadButton   : TDXBitmapButton;
    SaveButton   : TDXBitmapButton;
    constructor Create(Container: TDXContainer);override;
    destructor destroy;override;
    procedure SetModel(Sender: TObject);
    procedure SetShields(Sender: TObject);
    procedure SetMotor(Sender: TObject);
    procedure SetUFO(Sender: TObject);
    procedure SetWaffen(Sender: TObject);

    procedure SetDefaults;override;
    property KampfIntro      : TUFOKampfIntro read fKampfIntro write fKampfIntro;
    property Raumschiffe     : TRaumschiffList read fRaumschiffe;
    property UFOListe        : TUFOList read fUFOList;
  end;

implementation

uses
  game_api, gameset_api;

{ TKampfSimulation }

procedure TKampfSimulation.AddComponent(Compo: TDXComponent; DefVis: Boolean);
begin
  SetLength(fUFOComponents,length(fUFOComponents)+1);
  with fUFOComponents[length(fUFOComponents)-1] do
  begin
    Visible:=DefVis;
    Component:=Compo;
  end;
end;

procedure TKampfSimulation.AddComponent(Compo: TDXComponent;
  Typ: TControlType);
begin
  SetLength(fSchComponents,length(fSchComponents)+1);
  with fSchComponents[length(fSchComponents)-1] do
  begin
    Component:=Compo;
    CType:=Typ;
  end;
end;

procedure TKampfSimulation.ApplyMotor(Sender: TObject);
begin
  if (List.ItemIndex=-1) or ((List.ItemIndex=fSelected[2].Index) and (fSelected[2].SatzID=fSelectedSatz)) then exit;
  Container.Lock;
  PPSEdit.Value:=fMotors[List.ItemIndex].PpS;
  MotorBox.Text:=fMotors[List.ItemIndex].Name;
  DuesenEdit.Value:=Integer(not fMotors[List.ItemIndex].Duesen);
  Container.Unlock;
  MotorBox.Redraw;
  fSelected[2].Index:=List.ItemIndex;
  fSelected[2].SatzID:=fSelectedSatz;
end;

procedure TKampfSimulation.ApplyShield(Sender: TObject);
begin
  if (List.ItemIndex=-1) or ((List.ItemIndex=fSelected[1].Index) and (fSelected[1].SatzID=fSelectedSatz)) then exit;
  Container.Lock;
  SPointEdit.Value:=fShields[List.ItemIndex].Points;
  ShieldBox.Text:=fShields[List.ItemIndex].Name;
  SAbwehrEdit.Value:=fShields[List.ItemIndex].Abwehr;
  LadeZeitEdit.Value:=fShields[List.ItemIndex].Laden;
  ShieldBox.Text:=fShields[List.ItemIndex].Name;
  Container.Unlock;
  ShieldBox.Redraw;
  fSelected[1].Index:=List.ItemIndex;
  fSelected[1].SatzID:=fSelectedSatz;
end;

procedure TKampfSimulation.ApplyShip(Sender: TObject);
begin
  if (List.ItemIndex=-1) or ((List.ItemIndex=fSelected[0].Index) and (fSelected[0].SatzID=fSelectedSatz)) then exit;
  Container.Lock;
  HitPointEdit.Value:=fModells[List.ItemIndex].HitPoints;
  ModelBox.Text:=fModells[List.ItemIndex].Name;
  WZEdit.Value:=fModells[List.ItemIndex].WaffenZellen;
  WaffenZellenChanged(nil);
  ModelBox.Text:=fModells[List.ItemIndex].Name;
  Container.Unlock;
  ModelBox.Redraw;
  fSelected[0].Index:=List.ItemIndex;
  fSelected[0].SatzID:=fSelectedSatz;
end;

procedure TKampfSimulation.ApplyUFO(Sender: TObject);
begin
  if (UFOList.ItemIndex=-1) or ((UFOList.ItemIndex=fSelected[3].Index) and (fSelected[3].SatzID=fSelectedSatz)) then exit;
  Container.Lock;
  { Code zum �bernehmen der Eigenschaften }
  with fUFOs[UFOList.ItemIndex] do
  begin
    UFOName.Text:=Name;
    UFOHitPoints.Value:=HitPoints;
    UFOShield.Value:=Shield;
    UFOAttack.Value:=Angriff;
    UFOTreff.Value:=Treffsicherheit;
    UFOPDefense.Value:=ShieldType.Projektil;
    UFORDefense.Value:=ShieldType.Rakete;
    UFOLDefense.Value:=ShieldType.Laser;
    UFOLadeZeit.Value:=ShieldType.Laden;
    UFOPPSEdit.Value:=Pps;
  end;
  { Ende Code }
  Container.UnLock;
  UFOName.Redraw;
  fSelected[3].Index:=UFOList.ItemIndex;
  fSelected[3].SatzID:=fSelectedSatz;
end;

procedure TKampfSimulation.ApplyWaffe(Sender: TObject);
begin
  if (List.ItemIndex=-1) or ((List.ItemIndex=fSelected[WaffenBox.Value+3].Index) and (fSelected[WaffenBox.Value+3].SatzID=fSelectedSatz)) then exit;
  Container.Lock;
  { Code zum �bernehmen der Eigenschaften }
  WName.Text:=fWaffen[List.ItemIndex].Name;
  AttackEdit.Value:=fWaffen[List.ItemIndex].Strength;
  SchussEdit.Value:=fWaffen[List.ItemIndex].Munition;
  LadeEdit.Value:=fWaffen[List.ItemIndex].Laden;
  WeiteEdit.Value:=max(500,fWaffen[List.ItemIndex].ReichWeite);
  case fWaffen[List.ItemIndex].WaffType of
    wtProjektil : TypeEdit.Value:=1;
    wtRaketen   : TypeEdit.Value:=2;
    wtLaser     : TypeEdit.Value:=3;
  end;
  case fWaffen[List.ItemIndex].Verfolgung of
    true  : FolgeEdit.Value:=1;
    false : FolgeEdit.Value:=2;
  end;
  WPPSEdit.Value:=fWaffen[List.ItemIndex].PixPerSec;
  WaffenSettingChange(Sender);
  { Ende Code }
  Container.Unlock;
  WaffenBox.Redraw;
  fSelected[WaffenBox.Value+3].Index:=List.ItemIndex;
  fSelected[WaffenBox.Value+3].SatzID:=fSelectedSatz;
end;

procedure TKampfSimulation.ChangeMotorSetting(Sender: TObject);
begin
  MotorBox.Text:=LNoStandard;
  fSelected[2].Index:=-1;
end;

procedure TKampfSimulation.ChangeShieldSetting(Sender: TObject);
begin
  ShieldBox.Text:=LNoStandard;
  fSelected[1].Index:=-1;
end;

procedure TKampfSimulation.ChangeShipSetting(Sender: TObject);
begin
  fSelected[0].Index:=-1;
  ModelBox.Text:=LNoStandard;
end;

procedure TKampfSimulation.ChooseSchiffSet(Sender: TObject);
var
  Dummy   : Integer;
  Result  : Integer;
  LChange : TNotifyEvent;
begin
  result:=ChooseGameSet;
  if result=-1 then exit;
  Container.Lock;
  LoadModells(Files[Result].FileName);
  LChange:=List.OnChange;
  List.OnChange:=nil;
  List.ChangeItem(true);
  List.Items.Clear;
  case fList of
    0 : for Dummy:=0 to Length(fModells)-1 do List.Items.Add(fModells[Dummy].Name);
    1 : for Dummy:=0 to Length(fShields)-1 do List.Items.Add(fShields[Dummy].Name);
    2 : for Dummy:=0 to Length(fMotors)-1 do List.Items.Add(fMotors[Dummy].Name);
    3 : for Dummy:=0 to Length(fWaffen)-1 do List.Items.Add(fWaffen[Dummy].Name);
  end;
  List.ChangeItem(false);
  List.Items.OnChange(nil);
  List.OnChange:=LChange;
  Container.UnLock;
  Container.IncLock;
  GameSetBox.Redraw;
  Container.DecLock;
  List.Redraw;
end;

function TKampfSimulation.ChooseGameSet: Integer;
var
  Dummy: Integer;
begin
  ListBoxWindow.Items.Clear;
  for Dummy:=0 to length(Files)-1 do
  begin
    ListBoxWindow.Items.AddObject(Files[Dummy].Name,Pointer(Dummy));
  end;
  Result:=ListBoxWindow.Show;
  if Result=-1 then exit;
  Result:=Integer(ListBoxWindow.Items.Objects[Result]);
end;

constructor TKampfSimulation.Create(Container: TDXContainer);
var
  Temp  : TDXComponent;
  Dummy : Integer;
begin
  inherited;
  Animation:=paUpToBottom;

  { SchliessenButton }
  CloseButton:=TDXBitmapButton.Create(Self);
  with CloseButton do
  begin
    Text:=BClose;
    SetRect(522,404,110,28);
    FirstColor:=coFirstColor;
    EscButton:=true;
    SecondColor:=coSecondColor;
    AccerlateColor:=coAccerlateColor;
    RoundCorners:=rcRight;
    PageIndex:=PageMainMenu;
    SetButtonFont(Font);
    Hint:=HCloseButton;
  end;

  { Starten-Button }
  with TDXBitmapButton.Create(Self) do
  begin
    Text:=BWStart;
    SetRect(411,404,110,28);
    FirstColor:=coFirstColor;
    SecondColor:=coSecondColor;
    AccerlateColor:=coAccerlateColor;
    RoundCorners:=rcLeft;
    OnClick:=StartKampf;
    SetButtonFont(Font);
    Hint:=HStartButton;
  end;

  { Liste f�r Modelle und Raumschiffe }
  List:=TDXListBox.Create(Self);
  AddComponent(List,ctNone);
  with List do
  begin
    SetRect(16,247,325,177);
    SetFont(Font);
    BorderColor:=coFirstColor;
    FocusColor:=coSecondColor;
    FirstColor:=coScrollBrush;
    SecondColor:=coScrollPen;
    SetButtonFont(ScrollBarFont);
    ItemHeight:=40;
    AlwaysChange:=true;
    Visible:=false;
    RoundCorners:=rcBottom;
    OwnerDraw:=true;
    HeadRowHeight:=17;
  end;

{ ******************************** Spielsatzausw�hler f�rs Raumschiff **************************** }
  { Spielsatz: }
  Temp:=TDXGroupCaption.Create(Self);
  AddComponent(Temp,ctNone);
  with TDXGroupCaption(Temp) do
  begin
    SetRect(16,226,125,20);
    RoundCorners:=rcNone;
    Caption:=LGameSet;
    SetFont(Font);
  end;

  { SpielsatzBox }
  GameSetBox:=TDXEdit.Create(Self);
  AddComponent(GameSetBox,ctNone);
  with GameSetBox do
  begin
    SetRect(142,226,178,20);
    TabStop:=false;
    SetButtonFont(Font);
    RoundCorners:=rcNone;
    Visible:=false;
    TopMargin:=4;
    BorderColor:=coFirstColor;
    Visible:=false;
    ReadOnly:=true;
    FocusColor:=coSecondColor;
  end;

  { Spielsatz w�hlen }
  GameSetChoose:=TDXBitmapButton.Create(Self);
  AddComponent(GameSetChoose,ctNone);
  with GameSetChoose do
  begin
    SetRect(321,226,20,20);
    SetButtonFont(Font);
    FirstColor:=coFirstColor;
    SecondColor:=coSecondColor;
    AccerlateColor:=coAccerlateColor;
    Text:=BSelector;
    RoundCorners:=rcNone;
    Hint:=HChooseSchiffSet;
    OnClick:=ChooseSchiffSet;
  end;

  { Benutzerdefiniert Button }
  UserButton:=TDXBitmapButton.Create(Self);
  with UserButton do
  begin
    SetRect(16,176,325,28);
    SetButtonFont(Font);
    FirstColor:=coFirstColor;
    SecondColor:=coSecondColor;
    Hint:=HUserDefine;
    AccerlateColor:=coAccerlateColor;
    OnClick:=UserButtonClick;
    Text:=BUserButton;
    RoundCorners:=rcTop;
  end;

{ ******************************** Spielsatzausw�hler f�rs UFO **************************** }
  { Spielsatz: }
  Temp:=TDXGroupCaption.Create(Self);
  AddComponent(Temp,false);
  with TDXGroupCaption(Temp) do
  begin
    SetRect(375,87,100,20);
    RoundCorners:=rcNone;
    Caption:=LGameSet;
    SetFont(Font);
  end;

  { SpielsatzBox }
  GameSetUFOBox:=TDXEdit.Create(Self);
  AddComponent(GameSetUFOBox,false);
  with TDXEdit(GameSetUFOBox) do
  begin
    SetRect(476,87,128,20);
    TabStop:=false;
    SetButtonFont(Font);
    RoundCorners:=rcNone;
    Visible:=false;
    TopMargin:=4;
    BorderColor:=coFirstColor;
    Visible:=false;
    ReadOnly:=true;
    FocusColor:=coSecondColor;
  end;

  { Spielsatz w�hlen }
  Temp:=TDXBitmapButton.Create(Self);
  AddComponent(Temp,false);
  with TDXBitmapButton(Temp) do
  begin
    SetRect(605,87,20,20);
    SetButtonFont(Font);
    FirstColor:=coFirstColor;
    SecondColor:=coSecondColor;
    AccerlateColor:=coAccerlateColor;
    Text:=BSelector;
    RoundCorners:=rcNone;
    Hint:=HChooseUFOSet;
    OnClick:=ChooseUFOSet;
  end;

  { Liste zur Auswahl der UFOs }
  UFOList:=TDXListBox.Create(Self);
  AddComponent(UFOList,false);
  with UFOList do
  begin
    SetRect(375,108,250,257);
    SetFont(Font);
    BorderColor:=coFirstColor;
    FocusColor:=coSecondColor;
    FirstColor:=coScrollBrush;
    SecondColor:=coScrollPen;
    SetButtonFont(ScrollBarFont);
    HeadData:=MakeHeadData(LEUFO,FUFO,StringConst.FUFOs);
    Hint:=HSUFOList;
    ItemHeight:=40;
    AlwaysChange:=true;
    Visible:=false;
    OnChange:=ApplyUFO;
    RoundCorners:=rcBottom;
    OwnerDraw:=true;
    HeadRowHeight:=17;
    OnDrawItem:=DrawUFO;
  end;

  { Benutzerdefiniert Button }
  with TDXBitmapButton.Create(Self) do
  begin
    SetRect(375,37,250,28);
    SetButtonFont(Font);
    FirstColor:=coFirstColor;
    SecondColor:=coSecondColor;
    AccerlateColor:=coAccerlateColor;
    Hint:=HUserDefine;
    OnClick:=UFOUserButtonClick;
    Text:=BUFOUserButton;
    RoundCorners:=rcNone;
  end;

{ ******************************** Speichern und Laden Button   **************************** }
  LoadButton:=TDXBitmapButton.Create(Self);
  with LoadButton do
  begin
    SetRect(16,142,162,28);
    RoundCorners:=rcLeftBottom;
    SecondColor:=clRed;
    SetButtonFont(Font);
    AccerlateColor:=clYellow;
    Text:=LLoad;
    Hint:=HLoad;
    OnClick:=LoadButtonClick;
  end;

  SaveButton:=TDXBitmapButton.Create(Self);
  with SaveButton do
  begin
    SetRect(179,142,162,28);
    RoundCorners:=rcRightBottom;
    SecondColor:=clRed;
    SetButtonFont(Font);
    AccerlateColor:=clYellow;
    OnClick:=SaveButtonClick;
    Text:=LSave;
    Hint:=HSave;
  end;

  EditCaption:=TDXGroupCaption.Create(Self);
  with EditCaption do
  begin
    SetRect(16,205,325,20);
    RoundCorners:=rcNone;
    Caption:=LYourSchiff;
    SetFont(Font);
    Font.Style:=[fsBold];
  end;

{ ******************************** Einstellungen f�rs Raumschiff **************************** }
  { Raumschiff�berschrift }
  Raumschiff:=TDXGroupCaption.Create(Self);
  with Raumschiff do
  begin
    SetRect(16,16,325,20);
    RoundCorners:=rcTop;
    Caption:=LYourSchiff;
    SetFont(Font);
  end;

  { Schiffsname }
  with TDXGroupCaption.Create(Self) do
  begin
    SetRect(16,37,125,20);
    RoundCorners:=rcNone;
    Caption:=LSchiffName;
    SetFont(Font);
  end;

  NameEdit:=TDXEdit.Create(Self);
  with NameEdit do
  begin
    SetRect(142,37,199,20);
    SetButtonFont(Font);
    RoundCorners:=rcNone;
    MaxLength:=15;
    TopMargin:=4;
    Hint:=HSchiffName;
    BorderColor:=coFirstColor;
    FocusColor:=coSecondColor;
  end;

  { Raumschiff-Modell ausw�hlen }
  with TDXGroupCaption.Create(Self) do
  begin
    SetRect(16,58,125,20);
    RoundCorners:=rcNone;
    Caption:=LStarShip;
    SetFont(Font);
  end;

  ModelBox:=TDXEdit.Create(Self);
  with ModelBox do
  begin
    SetRect(142,58,178,20);
    TabStop:=false;
    SetButtonFont(Font);
    RoundCorners:=rcNone;
    TopMargin:=4;
    BorderColor:=coFirstColor;
    ReadOnly:=true;
    FocusColor:=coSecondColor;
  end;

  ModelButton:=TDXBitmapButton.Create(Self);
  with ModelButton do
  begin
    SetRect(321,58,20,20);
    SetButtonFont(Font);
    FirstColor:=coFirstColor;
    SecondColor:=coSecondColor;
    AccerlateColor:=coAccerlateColor;
    Text:=BSelector;
    RoundCorners:=rcNone;
    OnClick:=SetModel;
    Hint:=HSchiffModell;
    Tag:=Integer(ctSchiff);
  end;

  { Hitpoints }
  Temp:=TDXGroupCaption.Create(Self);
  AddComponent(Temp,ctSchiff);
  with TDXGroupCaption(Temp) do
  begin
    SetRect(16,226,125,20);
    RoundCorners:=rcNone;
    Caption:=IAHitPoints;
    SetFont(Font);
    Visible:=false;
  end;

  HitPointEdit:=TDXSpinEdit.Create(Self);
  AddComponent(HitPointEdit,ctSchiff);
  with HitPointEdit do
  begin
    SetRect(142,226,199,20);
    BorderColor:=coFirstColor;
    FocusColor:=coSecondColor;
    Min:=100;
    Max:=5000;
    Step:=200;
    SmallStep:=10;
    SmallStepButton:=true;
    RoundCorners:=rcNone;
    Hint:=HHitPoints;
    SetButtonFont(Font);
    OnIncrement:=ChangeShipSetting;
    OnDecrement:=ChangeShipSetting;
    Visible:=false;
  end;

  { Waffenzellen }
  Temp:=TDXGroupCaption.Create(Self);
  AddComponent(Temp,ctSchiff);
  with TDXGroupCaption(Temp) do
  begin
    SetRect(16,247,125,20);
    RoundCorners:=rcLeftBottom;
    Caption:=IAWaffenZ;
    SetFont(Font);
    Visible:=false;
  end;

  WZEdit:=TDXSpinEdit.Create(Self);
  AddComponent(WZEdit,ctSchiff);
  with WZEdit do
  begin
    SetRect(142,247,199,20);
    BorderColor:=coFirstColor;
    FocusColor:=coSecondColor;
    Min:=1;
    Max:=3;
    Value:=1;
    Hint:=HWaffenzellen;
    RoundCorners:=rcRightBottom;
    SetButtonFont(Font);
    OnIncrement:=WaffenZellenChanged;
    OnDecrement:=WaffenZellenChanged;
    Visible:=false;
  end;

{ ******************************** Einstellungen f�r Schutzschild **************************** }
  { Schutzschild ausw�hlen }
  with TDXGroupCaption.Create(Self) do
  begin
    SetRect(16,79,125,20);
    RoundCorners:=rcNone;
    Caption:=LShield;
    SetFont(Font);
  end;

  ShieldBox:=TDXEdit.Create(Self);
  with ShieldBox do
  begin
    SetRect(142,79,178,20);
    TabStop:=false;
    SetButtonFont(Font);
    RoundCorners:=rcNone;
    TopMargin:=4;
    BorderColor:=coFirstColor;
    ReadOnly:=true;
    FocusColor:=coSecondColor;
  end;

  ShieldButton:=TDXBitmapButton.Create(Self);
  with ShieldButton do
  begin
    SetRect(321,79,20,20);
    SetButtonFont(Font);
    FirstColor:=coFirstColor;
    SecondColor:=coSecondColor;
    AccerlateColor:=coAccerlateColor;
    Text:=BSelector;
    RoundCorners:=rcNone;
    Hint:=HShieldModel;
    OnClick:=SetShields;
    Tag:=Integer(ctShield);
  end;

  { Schildpunkte }
  Temp:=TDXGroupCaption.Create(Self);
  AddComponent(Temp,ctShield);
  with TDXGroupCaption(Temp) do
  begin
    SetRect(16,226,125,20);
    RoundCorners:=rcNone;
    Caption:=IAShieldPoints;
    SetFont(Font);
    Visible:=false;
  end;

  SPointEdit:=TDXSpinEdit.Create(Self);
  AddComponent(SPointEdit,ctShield);
  with SPointEdit do
  begin
    SetRect(142,226,199,20);
    BorderColor:=coFirstColor;
    FocusColor:=coSecondColor;
    Min:=0;
    Max:=1500;
    Step:=100;
    SmallStep:=5;
    Hint:=HShieldPoints;
    RoundCorners:=rcNone;
    SetButtonFont(Font);
    OnIncrement:=ChangeShieldSetting;
    OnDecrement:=ChangeShieldSetting;
    Visible:=false;
  end;

  { Schadensabwehr }
  Temp:=TDXGroupCaption.Create(Self);
  AddComponent(Temp,ctShield);
  with TDXGroupCaption(Temp) do
  begin
    SetRect(16,247,125,20);
    RoundCorners:=rcNone;
    Caption:=IADefend;
    SetFont(Font);
    Visible:=false;
  end;

  SAbwehrEdit:=TDXSpinEdit.Create(Self);
  AddComponent(SAbwehrEdit,ctShield);
  with SAbwehrEdit do
  begin
    SetRect(142,247,199,20);
    BorderColor:=coFirstColor;
    FocusColor:=coSecondColor;
    Min:=1;
    Hint:=HAbwehr;
    Max:=100;
    Step:=10;
    SmallStep:=1;
    FormatString:=FFloat0Percent;
    RoundCorners:=rcNone;
    SetButtonFont(Font);
    OnIncrement:=ChangeShieldSetting;
    OnDecrement:=ChangeShieldSetting;
    Visible:=false;
  end;

  { Nachladezeit }
  Temp:=TDXGroupCaption.Create(Self);
  AddComponent(Temp,ctShield);
  with TDXGroupCaption(Temp) do
  begin
    SetRect(16,268,125,20);
    RoundCorners:=rcLeftBottom;
    Caption:=IAAufLade;
    SetFont(Font);
    Visible:=false;
  end;

  LadeZeitEdit:=TDXSpinEdit.Create(Self);
  AddComponent(LadeZeitEdit,ctShield);
  with LadeZeitEdit do
  begin
    SetRect(142,268,199,20);
    BorderColor:=coFirstColor;
    FocusColor:=coSecondColor;
    Min:=50;
    Max:=1000;
    Step:=100;
    SmallStep:=5;
    Hint:=HLadeZeit;
    RoundCorners:=rcRightBottom;
    SetButtonFont(Font);
    FormatString:=FFloatMSeconds;
    OnIncrement:=ChangeShieldSetting;
    OnDecrement:=ChangeShieldSetting;
    Visible:=false;
  end;

{ ******************************** Einstellungen f�r Motor **************************** }
  { Motor ausw�hlen }
  with TDXGroupCaption.Create(Self) do
  begin
    SetRect(16,100,125,20);
    RoundCorners:=rcNone;
    Caption:=LSMotor;
    SetFont(Font);
  end;

  MotorBox:=TDXEdit.Create(Self);
  with MotorBox do
  begin
    SetRect(142,100,178,20);
    TabStop:=false;
    SetButtonFont(Font);
    RoundCorners:=rcNone;
    TopMargin:=4;
    BorderColor:=coFirstColor;
    ReadOnly:=true;
    FocusColor:=coSecondColor;
  end;

  MotorButton:=TDXBitmapButton.Create(Self);
  with MotorButton do
  begin
    SetRect(321,100,20,20);
    SetButtonFont(Font);
    FirstColor:=coFirstColor;
    SecondColor:=coSecondColor;
    AccerlateColor:=coAccerlateColor;
    Text:=BSelector;
    RoundCorners:=rcNone;
    OnClick:=SetMotor;
    Tag:=Integer(ctMotor);
    Hint:=HMotorModel;
  end;

  { Geschwindigkeit }
  Temp:=TDXGroupCaption.Create(Self);
  AddComponent(Temp,ctMotor);
  with TDXGroupCaption(Temp) do
  begin
    SetRect(16,226,125,20);
    RoundCorners:=rcNone;
    Caption:=LPPS;
    SetFont(Font);
    Visible:=false;
  end;

  PPSEdit:=TDXSpinEdit.Create(Self);
  AddComponent(PPSEdit,ctMotor);
  with PPSEdit do
  begin
    SetRect(142,226,199,20);
    BorderColor:=coFirstColor;
    FocusColor:=coSecondColor;
    Min:=1;
    Max:=15;
    Hint:=HPPSEdit;
    RoundCorners:=rcNone;
    SetButtonFont(Font);
    OnIncrement:=ChangeMotorSetting;
    OnDecrement:=ChangeMotorSetting;
    Visible:=false;
  end;

  { Man�vrierd�sen }
  Temp:=TDXGroupCaption.Create(Self);
  AddComponent(Temp,ctMotor);
  with TDXGroupCaption(Temp) do
  begin
    SetRect(16,247,125,20);
    RoundCorners:=rcLeftBottom;
    Caption:=IADuesen;
    SetFont(Font);
    Visible:=false;
  end;

  DuesenEdit:=TDXSpinEdit.Create(Self);
  AddComponent(DuesenEdit,ctMotor);
  with DuesenEdit do
  begin
    SetRect(142,247,199,20);
    BorderColor:=coFirstColor;
    FocusColor:=coSecondColor;
    Min:=0;
    Max:=1;
    Hint:=HDuesenEdit;
    RoundCorners:=rcRightBottom;
    FormatString:=EmptyStr;
    GetValueText:=NeedDuesen;
    SetButtonFont(Font);
    OnIncrement:=ChangeMotorSetting;
    OnDecrement:=ChangeMotorSetting;
    Visible:=false;
  end;

{ ******************************** Einstellungen f�r Waffen **************************** }
  { Waffen-Einstellungen ausw�hlen }
  with TDXGroupCaption.Create(Self) do
  begin
    SetRect(16,121,125,20);
    RoundCorners:=rcNone;
    Caption:=LSWZelle;
    SetFont(Font);
  end;

  WaffenBox:=TDXSpinEdit.Create(Self);
  with WaffenBox do
  begin
    SetRect(142,121,178,20);
    SetButtonFont(Font);
    RoundCorners:=rcNone;
    Min:=1;
    Max:=3;
    GetValueText:=NeedWaffe;
    Interval:=200;
    TopMargin:=4;
    FormatString:='';
    BorderColor:=coFirstColor;
    Hint:=HWaffenBox;
    FocusColor:=coSecondColor;
    OnIncrement:=WaffeChanged;
    OnDecrement:=WaffeChanged;
  end;

  WaffenButton:=TDXBitmapButton.Create(Self);
  with WaffenButton do
  begin
    SetRect(321,121,20,20);
    SetButtonFont(Font);
    FirstColor:=coFirstColor;
    SecondColor:=coSecondColor;
    AccerlateColor:=coAccerlateColor;
    Text:=BSelector;
    RoundCorners:=rcNone;
    OnClick:=SetWaffen;
    Tag:=Integer(ctWaffe);
    Hint:=HWeaponModel;
  end;

  { Waffenname }
  Temp:=TDXGroupCaption.Create(Self);
  AddComponent(Temp,ctWaffe);
  with TDXGroupCaption(Temp) do
  begin
    SetRect(16,226,125,20);
    RoundCorners:=rcNone;
    Caption:=LSchiffName;
    SetFont(Font);
    Visible:=false;
  end;

  WName:=TDXEdit.Create(Self);
  AddComponent(WName,ctWaffe);
  with WName do
  begin
    SetRect(142,226,199,20);
    SetButtonFont(Font);
    RoundCorners:=rcNone;
    MaxLength:=25;
    TopMargin:=4;
    Hint:=HWeaponName;
    BorderColor:=coFirstColor;
    FocusColor:=coSecondColor;
    OnChange:=WNameChange;
    Visible:=false;
  end;

  { Waffen-Angriffst�rke }
  Temp:=TDXGroupCaption.Create(Self);
  AddComponent(Temp,ctWaffe);
  with TDXGroupCaption(Temp) do
  begin
    SetRect(16,247,125,20);
    RoundCorners:=rcNone;
    Caption:=IAStrengthW;
    SetFont(Font);
    Visible:=false;
  end;

  AttackEdit:=TDXSpinEdit.Create(Self);
  AddComponent(AttackEdit,ctWaffe);
  with AttackEdit do
  begin
    SetRect(142,247,199,20);
    BorderColor:=coFirstColor;
    FocusColor:=coSecondColor;
    Min:=5;
    Max:=1000;
    Step:=100;
    SmallStep:=5;
    Hint:=HAttackEdit;
    OnIncrement:=WaffenSettingChange;
    OnDecrement:=WaffenSettingChange;
    RoundCorners:=rcNone;
    SetButtonFont(Font);
    Visible:=false;
  end;

  { Sch�sse }
  Temp:=TDXGroupCaption.Create(Self);
  AddComponent(Temp,ctWaffe);
  with TDXGroupCaption(Temp) do
  begin
    SetRect(16,268,125,20);
    RoundCorners:=rcNone;
    Caption:=IASchuss;
    SetFont(Font);
    Visible:=false;
  end;

  SchussEdit:=TDXSpinEdit.Create(Self);
  AddComponent(SchussEdit,ctWaffe);
  with SchussEdit do
  begin
    SetRect(142,268,199,20);
    BorderColor:=coFirstColor;
    FocusColor:=coSecondColor;
    Min:=1;
    Max:=500;
    Step:=50;
    SmallStep:=1;
    Hint:=HSchussEdit;
    OnIncrement:=WaffenSettingChange;
    OnDecrement:=WaffenSettingChange;
    RoundCorners:=rcNone;
    SetButtonFont(Font);
    Visible:=false;
  end;

  { Waffentyp }
  Temp:=TDXGroupCaption.Create(Self);
  AddComponent(Temp,ctWaffe);
  with TDXGroupCaption(Temp) do
  begin
    SetRect(16,289,125,20);
    RoundCorners:=rcNone;
    Caption:=IAArt;
    SetFont(Font);
    Visible:=false;
  end;

  TypeEdit:=TDXSpinEdit.Create(Self);
  AddComponent(TypeEdit,ctWaffe);
  with TypeEdit do
  begin
    SetRect(142,289,199,20);
    BorderColor:=coFirstColor;
    FocusColor:=coSecondColor;
    Min:=1;
    Max:=3;
    Step:=1;
    Hint:=HTypeEdit;
    FormatString:='';
    OnIncrement:=WaffenSettingChange;
    OnDecrement:=WaffenSettingChange;
    GetValueText:=NeedWaffTyp;
    RoundCorners:=rcNone;
    Interval:=100;
    SetButtonFont(Font);
    Visible:=false;
  end;

  { Geschwindigkeit }
  Temp:=TDXGroupCaption.Create(Self);
  AddComponent(Temp,ctWaffe);
  with TDXGroupCaption(Temp) do
  begin
    SetRect(16,310,125,20);
    RoundCorners:=rcNone;
    Caption:=LPPS;
    SetFont(Font);
    Visible:=false;
  end;

  WPPSEdit:=TDXSpinEdit.Create(Self);
  AddComponent(WPPSEdit,ctWaffe);
  with WPPSEdit do
  begin
    SetRect(142,310,199,20);
    BorderColor:=coFirstColor;
    FocusColor:=coSecondColor;
    Min:=1;
    Max:=30;
    Hint:=HWPPSEdit;
    RoundCorners:=rcNone;
    SetButtonFont(Font);
    OnIncrement:=WaffenSettingChange;
    OnDecrement:=WaffenSettingChange;
    Visible:=false;
  end;

  { Nachladezeit }
  Temp:=TDXGroupCaption.Create(Self);
  AddComponent(Temp,ctWaffe);
  with TDXGroupCaption(Temp) do
  begin
    SetRect(16,331,125,20);
    RoundCorners:=rcNone;
    Caption:=IANachLade;
    SetFont(Font);
    Visible:=false;
  end;

  LadeEdit:=TDXSpinEdit.Create(Self);
  AddComponent(LadeEdit,ctWaffe);
  with LadeEdit do
  begin
    SetRect(142,331,199,20);
    BorderColor:=coFirstColor;
    FocusColor:=coSecondColor;
    Min:=100;
    Max:=2500;
    Step:=100;
    SmallStep:=5;
    Hint:=HLadeEdit;
    FormatString:=FFloatMSeconds;
    OnIncrement:=WaffenSettingChange;
    OnDecrement:=WaffenSettingChange;
    RoundCorners:=rcNone;
    SetButtonFont(Font);
    Visible:=false;
  end;

  { Reichweite }
  Temp:=TDXGroupCaption.Create(Self);
  AddComponent(Temp,ctWaffe);
  with TDXGroupCaption(Temp) do
  begin
    SetRect(16,352,125,20);
    RoundCorners:=rcNone;
    Caption:=IARange;
    SetFont(Font);
    Visible:=false;
  end;

  WeiteEdit:=TDXSpinEdit.Create(Self);
  AddComponent(WeiteEdit,ctWaffe);
  with WeiteEdit do
  begin
    SetRect(142,352,199,20);
    BorderColor:=coFirstColor;
    FocusColor:=coSecondColor;
    Min:=100;
    Value:=2000;
    Max:=7500;
    Step:=500;
    SmallStep:=100;
    Hint:=HLadeEdit;
    FormatString:=FMetres;
    OnIncrement:=WaffenSettingChange;
    OnDecrement:=WaffenSettingChange;
    RoundCorners:=rcNone;
    SetButtonFont(Font);
    Visible:=false;
  end;

  { Verfolgung }
  Temp:=TDXGroupCaption.Create(Self);
  AddComponent(Temp,ctWaffe);
  with TDXGroupCaption(Temp) do
  begin
    SetRect(16,373,125,20);
    RoundCorners:=rcLeftBottom;
    Caption:=IAVerfolg;
    SetFont(Font);
    Visible:=false;
  end;

  FolgeEdit:=TDXSpinEdit.Create(Self);
  AddComponent(FolgeEdit,ctWaffe);
  with FolgeEdit do
  begin
    SetRect(142,373,199,20);
    BorderColor:=coFirstColor;
    FocusColor:=coSecondColor;
    Min:=1;
    Max:=2;
    Step:=1;
    Hint:=HFolgeEdit;
    FormatString:='';
    GetValueText:=NeedVerfolgen;
    OnIncrement:=WaffenSettingChange;
    OnDecrement:=WaffenSettingChange;
    RoundCorners:=rcRightBottom;
    SetButtonFont(Font);
    Visible:=false;
  end;

{ ******************************** Einstellungen f�r das UFO **************************** }
  { UFO-Einstellungen }
  with TDXGroupCaption.Create(Self) do
  begin
    SetRect(375,16,250,20);
    RoundCorners:=rcTop;
    Caption:=LEnemy;
    SetFont(Font);
  end;

  { UFOname }
  Temp:=TDXGroupCaption.Create(Self);
  with TDXGroupCaption(Temp) do
  begin
    SetRect(375,66,100,20);
    RoundCorners:=rcNone;
    Caption:=LSchiffName;
    SetFont(Font);
  end;

  UFOName:=TDXEdit.Create(Self);
  with UFOName do
  begin
    SetRect(476,66,149,20);
    SetButtonFont(Font);
    RoundCorners:=rcNone;
    MaxLength:=25;
    TopMargin:=4;
    Hint:=HUFOName;
    BorderColor:=coFirstColor;
    FocusColor:=coSecondColor;
  end;

  { UFO - Hitpoints }
  Temp:=TDXGroupCaption.Create(Self);
  AddComponent(Temp,true);
  with TDXGroupCaption(Temp) do
  begin
    SetRect(375,87,100,20);
    RoundCorners:=rcNone;
    Caption:=IAHitPoints;
    SetFont(Font);
  end;

  UFOHitPoints:=TDXSpinEdit.Create(Self);
  AddComponent(UFOHitPoints,true);
  with UFOHitPoints do
  begin
    SetRect(476,87,149,20);
    BorderColor:=coFirstColor;
    FocusColor:=coSecondColor;
    Min:=100;
    Max:=7500;
    Step:=250;
    SmallStep:=10;
    RoundCorners:=rcNone;
    Hint:=HUFOHitPoints;
    SetButtonFont(Font);
  end;

  { UFO - Angriffst�rke }
  Temp:=TDXGroupCaption.Create(Self);
  AddComponent(Temp,true);
  with TDXGroupCaption(Temp) do
  begin
    SetRect(375,108,100,20);
    RoundCorners:=rcNone;
    Caption:=IAStrengthW;
    SetFont(Font);
  end;

  UFOAttack:=TDXSpinEdit.Create(Self);
  AddComponent(UFOAttack,true);
  with UFOAttack do
  begin
    SetRect(476,108,149,20);
    BorderColor:=coFirstColor;
    FocusColor:=coSecondColor;
    Min:=50;
    Max:=1500;
    Step:=100;
    SmallStep:=5;
    RoundCorners:=rcNone;
    Hint:=HUFOAttack;
    SetButtonFont(Font);
  end;

  { UFO - Treffsicherheit }
  Temp:=TDXGroupCaption.Create(Self);
  AddComponent(Temp,true);
  with TDXGroupCaption(Temp) do
  begin
    SetRect(375,129,100,20);
    RoundCorners:=rcNone;
    Caption:=IITreff;
    SetFont(Font);
  end;

  UFOTreff:=TDXSpinEdit.Create(Self);
  AddComponent(UFOTreff,true);
  with UFOTreff do
  begin
    SetRect(476,129,149,20);
    BorderColor:=coFirstColor;
    FocusColor:=coSecondColor;
    Min:=0;
    Max:=100;
    Step:=1;
    RoundCorners:=rcNone;
    Hint:=HUFOTreff;
    SetButtonFont(Font);
  end;

  { UFO - Schildpunkte }
  Temp:=TDXGroupCaption.Create(Self);
  AddComponent(Temp,true);
  with TDXGroupCaption(Temp) do
  begin
    SetRect(375,150,100,20);
    RoundCorners:=rcNone;
    Caption:=IAShieldPoints;
    SetFont(Font);
  end;

  UFOShield:=TDXSpinEdit.Create(Self);
  AddComponent(UFOShield,true);
  with UFOShield do
  begin
    SetRect(476,150,149,20);
    BorderColor:=coFirstColor;
    FocusColor:=coSecondColor;
    Min:=0;
    Max:=1500;
    Step:=100;
    SmallStep:=5;
    RoundCorners:=rcNone;
    Hint:=HUFOShieldPoints;
    SetButtonFont(Font);
  end;

  { UFO - Projektil-Schadensabwehr }
  Temp:=TDXGroupCaption.Create(Self);
  AddComponent(Temp,true);
  with TDXGroupCaption(Temp) do
  begin
    SetRect(375,171,100,20);
    RoundCorners:=rcNone;
    Caption:=IAPrDefend;
    SetFont(Font);
  end;

  UFOPDefense:=TDXSpinEdit.Create(Self);
  AddComponent(UFOPDefense,true);
  with UFOPDefense do
  begin
    SetRect(476,171,149,20);
    BorderColor:=coFirstColor;
    FocusColor:=coSecondColor;
    Min:=0;
    Hint:=HPrDefense;
    Max:=100;
    Step:=10;
    SmallStep:=1;
    FormatString:=FFloat0Percent;
    RoundCorners:=rcNone;
    SetButtonFont(Font);
  end;

  { UFO - Raketen-Schadensabwehr }
  Temp:=TDXGroupCaption.Create(Self);
  AddComponent(Temp,true);
  with TDXGroupCaption(Temp) do
  begin
    SetRect(375,192,100,20);
    RoundCorners:=rcNone;
    Caption:=IARaDefend;
    SetFont(Font);
  end;

  UFORDefense:=TDXSpinEdit.Create(Self);
  AddComponent(UFORDefense,true);
  with UFORDefense do
  begin
    SetRect(476,192,149,20);
    BorderColor:=coFirstColor;
    FocusColor:=coSecondColor;
    Min:=0;
    Hint:=HRaDefense;
    Max:=100;
    Step:=10;
    SmallStep:=1;
    FormatString:=FFloat0Percent;
    RoundCorners:=rcNone;
    SetButtonFont(Font);
  end;

  { UFO - Laser-Schadensabwehr }
  Temp:=TDXGroupCaption.Create(Self);
  AddComponent(Temp,true);
  with TDXGroupCaption(Temp) do
  begin
    SetRect(375,213,100,20);
    RoundCorners:=rcNone;
    Caption:=IALaDefend;
    SetFont(Font);
  end;

  UFOLDefense:=TDXSpinEdit.Create(Self);
  AddComponent(UFOLDefense,true);
  with UFOLDefense do
  begin
    SetRect(476,213,149,20);
    BorderColor:=coFirstColor;
    FocusColor:=coSecondColor;
    Min:=0;
    Hint:=HLaDefense;
    Max:=100;
    Step:=10;
    SmallStep:=1;
    FormatString:=FFloat0Percent;
    RoundCorners:=rcNone;
    SetButtonFont(Font);
  end;

  { UFO - Nachladezeit }
  Temp:=TDXGroupCaption.Create(Self);
  AddComponent(Temp,true);
  with TDXGroupCaption(Temp) do
  begin
    SetRect(375,234,100,20);
    RoundCorners:=rcNone;
    Caption:=IAAufLade;
    SetFont(Font);
  end;

  UFOLadeZeit:=TDXSpinEdit.Create(Self);
  AddComponent(UFOLadeZeit,true);
  with UFOLadeZeit do
  begin
    SetRect(476,234,149,20);
    BorderColor:=coFirstColor;
    FocusColor:=coSecondColor;
    Min:=50;
    Max:=2000;
    Step:=100;
    SmallStep:=5;
    Hint:=HUFOLadeZeit;
    RoundCorners:=rcNone;
    SetButtonFont(Font);
    FormatString:=FFloatMSeconds;
  end;

  { UFO-Geschwindigkeit }
  Temp:=TDXGroupCaption.Create(Self);
  AddComponent(Temp,true);
  with TDXGroupCaption(Temp) do
  begin
    SetRect(375,255,100,20);
    RoundCorners:=rcLeftBottom;
    Caption:=LPPS;
    SetFont(Font);
  end;

  UFOPPSEdit:=TDXSpinEdit.Create(Self);
  AddComponent(UFOPPSEdit,true);
  with UFOPPSEdit do
  begin
    SetRect(476,255,149,20);
    BorderColor:=coFirstColor;
    FocusColor:=coSecondColor;
    Min:=1;
    Max:=20;
    Hint:=HUFOPPSEdit;
    RoundCorners:=rcRightBottom;
    SetButtonFont(Font);
  end;

  UnionRect(fUFORect,UFOList.ClientRect,UFOHitPoints.ClientRect);
  UnionRect(fSchiffRect,List.ClientRect,UserButton.ClientRect);

  { Starteinstellungen setzen }
  NameEdit.Text:='NCC - '+IntToStr(random(89999)+10000);
  List.Visible:=false;
  ChangeShipSetting(Self);
  ChangeShieldSetting(Self);
  ChangeMotorSetting(Self);
  fWZellen[1].Installiert:=true;
  for Dummy:=1 to 3 do
  begin
    with fWZellen[Dummy] do
    begin
      Name:=Format(FDefaultName,[Dummy]);
      Strength:=5;
      WaffType:=wtProjektil;
      Munition:=1;
      Laden:=50;
      MunIndex:=-1;
      PixPerSec:=1;
      Verfolgung:=false;
      Reichweite:=2000;
    end;
  end;
  WName.Text:=Format(FDefaultName,[1]);
  UFOName.Text:=LUFOs;
  WaffenBox.max:=WZEdit.Value;
  ShowUFO(false);

  { Listbox Fenster zur Auswahl bei mehreren Objekten }
  ListBoxWindow:=TDXListBoxWindow.Create(Self);
  with ListBoxWindow do
  begin
    SetRect(0,0,300,200);
    Caption:=CChooseGameSet;
    SetButtonFont(Font);
    CaptionColor:=coFontColor;
    AccerlateColor:=coAccerlateColor;
    Font.Size:=coFontSize;
    FirstColor:=$00400000;
    SecondColor:=clBlue;
    BlendColor:=bcDarkNavy;
    Items.Sorted:=true;
  end;

(*
  { Listen erstellen }
  fRaumschiffe:=TRaumschiffList.Create;
  fUFOList:=TUFOList.Create;

  {Initsialisierung der Felder und Laden des Standardspiel-Satzes}
  LoadModells(FGameFile);
  LoadUFOModells(FGameFile);

  ReadGameSets;
  { Einstellung des Raumschiffes anzeigen }
  SetModel(nil);
  fSelected[0].Index:=-1;
  fSelected[1].Index:=-1;
  fSelected[2].Index:=-1;
  fSelected[3].Index:=-1;
  fSelected[4].Index:=-1;
  fSelected[5].Index:=-1;
  fSelected[6].Index:=-1;
  *)
end;

destructor TKampfSimulation.destroy;
begin
  fUFOList.Free;
  fRaumschiffe.Free;
  inherited;
end;

procedure TKampfSimulation.DrawModel(Sender: TDXComponent;
  Surface: TDirectDrawSurface;const Mem: TDDSurfaceDesc; Rect: TRect; Index: Integer;
  Selected: boolean);
var
  Text    : String;
begin
  if (fSelected[0].SatzID=fSelectedSatz) and (Index=fSelected[0].Index) then
    List.DrawSelection(Surface,Mem,Rect,bcMaroon);
  SaveGame.Raumschiffe.DrawIcon(Surface,Rect.Left+4,Rect.Top+4,fModells[Index].WaffenZellen);
  YellowStdFont.Draw(Surface,Rect.Left+38,Rect.Top+4,List.Items[Index]);
  WhiteStdFont.Draw(Surface,Rect.Left+38,Rect.Top+20,Format(FHitPoints,[fModells[Index].HitPoints/1]));
  Text:=ZahlString(FWaffenZelle,FWaffenZellen,fModells[Index].WaffenZellen);
  WhiteStdFont.Draw(Surface,Rect.Right-8-WhiteStdFont.TextWidth(Text),Rect.Top+20,Text);
end;

procedure TKampfSimulation.DrawMotor(Sender: TDXComponent;
  Surface: TDirectDrawSurface;const Mem: TDDSurfaceDesc; Rect: TRect; Index: Integer;
  Selected: boolean);
begin
  if (fSelected[2].SatzID=fSelectedSatz) and (Index=fSelected[2].Index) then
    List.DrawSelection(Surface,Mem,Rect,bcMaroon);
  SaveGame.LagerListe.DrawImage(IILMotor,Surface,Rect.Left+4,Rect.Top+4);
  YellowStdFont.Draw(Surface,Rect.Left+38,Rect.Top+4,List.Items[Index]);
  WhiteStdFont.Draw(Surface,Rect.Left+38,Rect.Top+20,Format(FSGeschwindig,[fMotors[Index].PpS]));
  if fMotors[Index].Duesen then
    WhiteStdFont.Draw(Surface,Rect.Right-8-WhiteStdFont.TextWidth(LManoevrier),Rect.Top+20,LManoevrier);
end;

procedure TKampfSimulation.DrawShield(Sender: TDXComponent;
  Surface: TDirectDrawSurface;const Mem: TDDSurfaceDesc; Rect: TRect; Index: Integer;
  Selected: boolean);
var
  Text    : String;
begin
  if (fSelected[1].SatzID=fSelectedSatz) and (Index=fSelected[1].Index) then
    List.DrawSelection(Surface,Mem,Rect,bcMaroon);
  SaveGame.LagerListe.DrawImage(IILShield,Surface,Rect.Left+4,Rect.Top+4);
  YellowStdFont.Draw(Surface,Rect.Left+38,Rect.Top+4,List.Items[Index]);
  WhiteStdFont.Draw(Surface,Rect.Left+38,Rect.Top+20,Format(FSPoints,[fShields[Index].Points/1]));
  Text:=Format(FSAbwehr,[fShields[Index].Abwehr]);
  WhiteStdFont.Draw(Surface,Rect.Right-8-WhiteStdFont.TextWidth(Text),Rect.Top+20,Text);
end;

procedure TKampfSimulation.DrawUFO(Sender: TDXComponent;
  Surface: TDirectDrawSurface;const Mem: TDDSurfaceDesc; Rect: TRect; Index: Integer;
  Selected: boolean);
var
  Text    : String;
begin
  if (fSelected[3].SatzID=fSelectedSatz) and (Index=fSelected[3].Index) then
    UFOList.DrawSelection(Surface,Mem,Rect,bcMaroon);
  SaveGame.UFOListe.DrawIcon(Surface,Rect.Left+4,Rect.Top+4,fUFOS[Index].Image);
  YellowStdFont.Draw(Surface,Rect.Left+38,Rect.Top+4,fUFOS[Index].Name);
  WhiteStdFont.Draw(Surface,Rect.Left+38,Rect.Top+20,Format(FHitPoints,[fUFOS[Index].HitPoints/1]));
  Text:=Format(FSPoints,[fUFOS[Index].Shield/1]);
  WhiteStdFont.Draw(Surface,Rect.Right-8-WhiteStdFont.TextWidth(Text),Rect.Top+20,Text);
end;

procedure TKampfSimulation.DrawWaffen(Sender: TDXComponent;
  Surface: TDirectDrawSurface;const Mem: TDDSurfaceDesc; Rect: TRect; Index: Integer;
  Selected: boolean);
var
  Text    : String;
begin
  if (fSelected[WaffenBox.Value+3].SatzID=fSelectedSatz) and (Index=fSelected[WaffenBox.Value+3].Index) then
    List.DrawSelection(Surface,Mem,Rect,bcMaroon);
  SaveGame.LagerListe.DrawImage(TypeIDToIndex(ptRWaffe,fWaffen[Index].WaffType),Surface,Rect.Left+4,Rect.Top+4);
  YellowStdFont.Draw(Surface,Rect.Left+38,Rect.Top+4,List.Items[Index]);
  WhiteStdFont.Draw(Surface,Rect.Left+38,Rect.Top+20,Format(FStrength,[fWaffen[Index].Strength/1]));
  Text:=Format(FSchuesse,[fWaffen[Index].Munition/1]);
  WhiteStdFont.Draw(Surface,Rect.Right-8-WhiteStdFont.TextWidth(Text),Rect.Top+20,Text);
end;

procedure TKampfSimulation.LoadModells(FileName: String);
var
  Dummy      : Integer;
  Temp       : TForschProject;
  Munitionen : Array of TForschProject;
  RIndex     : Integer;
  SIndex     : Integer;
  MIndex     : Integer;
  WIndex     : Integer;
  MuIndex    : Integer;
  TempID     : Cardinal;
  Dummy2     : Integer;
  SetIdent   : TSetIdentifier;
  Records    : TRecordArray;
  Archiv     : TArchivFile;
begin
  SetIdent:=gameset_api_GetIdentOfSet(FileName);
  GameSetBox.Text:=SetIdent.Name;
  fSelectedSatz:=SetIdent.ID;

  Archiv:=TArchivFile.Create;
  Archiv.OpenArchiv(FileName);

  SetLength(fModells,0);
  SetLength(fShields,0);
  SetLength(fMotors,0);
  SetLength(fWaffen,0);
  SetLength(Munitionen,0);
  RIndex:=0;
  SIndex:=0;
  MIndex:=0;
  WIndex:=0;
  MuIndex:=0;

  gameset_api_SetLanguage(defines.Language);
  Loader.ReadingLanguage:=defines.Language;

  gameset_api_LoadProjects(Archiv,Records);

  Archiv.Free;

  for Dummy:=0 to high(Records) do
  begin
    Temp:=RecordToProject(Records[Dummy]);
    Records[Dummy].Free;
    if (Temp.TypeID=ptRaumschiff) and (Temp.WaffenZellen>0) then
    begin
      SetLength(fModells,RIndex+1);
      with fModells[RIndex] do
      begin
        Name:=Temp.Name;
        WaffenZellen:=Temp.WaffenZellen;
        HitPoints:=Temp.HitPoints;
      end;
      inc(RIndex);
    end
    else if (Temp.TypeID=ptShield) then
    begin
      SetLength(fShields,SIndex+1);
      with fShields[SIndex] do
      begin
        Name:=Temp.Name;
        Points:=Temp.Strength;
        Abwehr:=Temp.Panzerung;
        Laden:=Temp.Laden;
      end;
      inc(SIndex);
    end
    else if (Temp.TypeID=ptMotor) then
    begin
      SetLength(fMotors,MIndex+1);
      with fMotors[MIndex] do
      begin
        Name:=Temp.Name;
        Duesen:=Temp.Verfolgung;
        PPs:=Temp.PixPerSec;
      end;
      inc(MIndex);
    end
    else if (Temp.TypeID=ptRWaffe) then
    begin
      SetLength(fWaffen,WIndex+1);
      with fWaffen[WIndex] do
      begin
        Name:=Temp.Name;
        WaffType:=Temp.WaffType;
        Strength:=Temp.Strength;
        PixPerSec:=Temp.PixPerSec;
        Laden:=Temp.Laden;
        Verfolgung:=Temp.Verfolgung;
        ID:=Temp.ID;
        if WaffType=wtLaser then
          Munition:=Temp.Munition;
      end;
      inc(WIndex);
    end
    else if (Temp.TypeID=ptRMunition) then
    begin
      SetLength(Munitionen,MuIndex+1);
      Munitionen[MuIndex]:=Temp;
      inc(MuIndex);
    end;
  end;

  for Dummy:=0 to high(fWaffen) do
  begin
    if fWaffen[Dummy].WaffType<>wtLaser then
    begin
      TempID:=fWaffen[Dummy].ID;
      for Dummy2:=0 to length(Munitionen)-1 do
      begin
        if Munitionen[Dummy2].Munfor=TempID then
          fWaffen[Dummy].Munition:=Munitionen[Dummy2].Munition;
      end;
    end;
  end;
end;

procedure TKampfSimulation.LoadUFOModells(FileName: String);
var
  SetIdent   : TSetIdentifier;
  Archiv     : TArchivFile;
  Dummy      : Integer;
  Records    : TRecordArray;
begin
  SetIdent:=gameset_api_GetIdentOfSet(FileName);
  GameSetUFOBox.Text:=SetIdent.Name;
  Archiv:=TArchivFile.Create;
  Archiv.OpenArchiv(FileName);

  gameset_api_SetLanguage(defines.Language);
  Loader.ReadingLanguage:=defines.Language;

  gameset_api_LoadUFos(Archiv,Records);

  SetLength(fUFOs,length(Records));

  ImageProducer.Reset;
  UFOList.Items.Clear;

  for Dummy:=0 to high(Records) do
  begin
    fUFOs[Dummy]:=ExtRecordToUFO(Records[Dummy]);
    Records[Dummy].Free;
    UFOList.Items.Add('');
  end;
  Archiv.Free;
end;

procedure TKampfSimulation.NeedDuesen(Value: Integer; var Text: String);
begin
  Text:=LVerfuegbar;
  if Value=1 then Text:=LNot+Text;
end;

procedure TKampfSimulation.NeedVerfolgen(Value: Integer; var Text: String);
begin
  case Value of
    1: Text:=LYes;
    2: Text:=LNo;
  end;
end;

procedure TKampfSimulation.NeedWaffe(Value: Integer; var Text: String);
begin
  Text:=Format(FWName,[Value,fWZellen[Value].Name]);
end;

procedure TKampfSimulation.NeedWaffTyp(Value: Integer; var Text: String);
begin
  case Value of
    1: Text:=IAProjektil;
    2: Text:=IARaketen;
    3: Text:=IALaser;
  end;
end;

procedure TKampfSimulation.ReadGameSets;
var
  Data    : TSearchRec;
  Stat    : Integer;
  Ident   : TSetIdentifier;
  Index   : Integer;
begin
  SetLength(Files,0);
  Stat:=FindFirst('data\GameSets\*.pak',faAnyFile,Data);
  Index:=0;
  while Stat=0 do
  begin
    try
      Ident:=gameset_api_GetIdentOfSet('data\GameSets\'+Data.Name);
      SetLength(Files,Index+1);
      Files[Index]:=Ident;
      inc(Index);
    except
    end;
    Stat:=FindNext(Data);
  end;
  FindClose(Data);
end;

procedure TKampfSimulation.SetModel(Sender: TObject);
var
  Dummy: Integer;
begin
  List.OnChange:=nil;
  Container.Lock;
  EditCaption.Caption:=LStarShip;
  List.HeadData:=MakeHeadData(LModels,FTyp,FTyps);
  List.OnDrawItem:=DrawModel;
  List.Hint:=HSSchiffList;
  List.Items.Clear;
  for Dummy:=0 to Length(fModells)-1 do List.Items.Add(fModells[Dummy].Name);
  UserButton.Tag:=Integer(ctSchiff);
  if UserButton.HighLight then
    ShowPage(UserButton)
  else
    ShowPage(nil);
  List.OnChange:=ApplyShip;
  Container.LoadGame(false);
  List.SetFocus;
  Container.decLock;
  Container.RedrawArea(fSchiffRect,Container.Surface,true);
  fList:=0;
end;

procedure TKampfSimulation.SetMotor(Sender: TObject);
var
  Dummy: Integer;
begin
  List.OnChange:=nil;
  Container.Lock;
  EditCaption.Caption:=LMotor;
  List.HeadData:=MakeHeadData(LMotor,FMotor,FMotoren);
  List.OnDrawItem:=DrawMotor;
  List.Hint:=HSMotorList;
  List.Items.Clear;
  for Dummy:=0 to Length(fMotors)-1 do List.Items.Add(fMotors[Dummy].Name);
  UserButton.Tag:=Integer(ctMotor);
  if UserButton.HighLight then
    ShowPage(UserButton)
  else
    ShowPage(nil);
  List.OnChange:=ApplyMotor;
  Container.LoadGame(false);
  List.SetFocus;
  Container.decLock;
  Container.RedrawArea(fSchiffRect,Container.Surface,true);
  fList:=2;
end;

procedure TKampfSimulation.SetShields(Sender: TObject);
var
  Dummy: Integer;
begin
  List.OnChange:=nil;
  Container.Lock;
  EditCaption.Caption:=LShield;
  List.HeadData:=MakeHeadData(LSchields,FSchield,FSchields);
  List.OnDrawItem:=DrawShield;
  List.Hint:=HSSchieldList;
  List.Items.Clear;
  for Dummy:=0 to Length(fShields)-1 do List.Items.Add(fShields[Dummy].Name);
  UserButton.Tag:=Integer(ctShield);
  if UserButton.HighLight then
    ShowPage(UserButton)
  else
    ShowPage(nil);
  List.OnChange:=ApplyShield;
  Container.LoadGame(false);
  List.SetFocus;
  Container.decLock;
  Container.RedrawArea(fSchiffRect,Container.Surface,true);
  fList:=1;
end;

procedure TKampfSimulation.SetUFO(Sender: TObject);
var
  Dummy: Integer;
begin
  Container.Lock;
  List.Visible:=true;
  List.OnDrawItem:=DrawUFO;
  List.OnChange:=nil;
  List.Items.Clear;
  for Dummy:=0 to Length(fUFOs)-1 do List.Items.Add(fUFOs[Dummy].Name);
  ShowUFO(false);
  ShowPage(nil);
  Container.LoadGame(false);
  List.SetFocus;
  Container.decLock;
  Container.RedrawArea(fSchiffRect,Container.Surface,true);
  fList:=4;
end;

procedure TKampfSimulation.SetWaffen(Sender: TObject);
var
  Dummy: Integer;
begin
  List.OnChange:=nil;
  Container.Lock;
  EditCaption.Caption:=Format(LWZelle,[WaffenBox.Value]);
  List.HeadData:=MakeHeadData(LWaffen,FWaffe,StringConst.FWaffen);
  List.OnDrawItem:=DrawWaffen;
  List.Hint:=HSWaffenList;
  List.Items.Clear;
  for Dummy:=0 to Length(fWaffen)-1 do List.Items.Add(fWaffen[Dummy].Name);
  UserButton.Tag:=Integer(ctWaffe);
  if UserButton.HighLight then
    ShowPage(UserButton)
  else
    ShowPage(nil);
  List.OnChange:=ApplyWaffe;
  Container.LoadGame(false);
  List.SetFocus;
  Container.decLock;
  Container.RedrawArea(fSchiffRect,Container.Surface,true);
  fList:=3;
end;

procedure TKampfSimulation.ShowPage(Sender: TObject);
var
  Dummy : Integer;
  Typ   : TControlType;
begin
  if Sender=nil then
    Typ:=ctNone
  else
    Typ:=TControlType((Sender as TDXComponent).Tag);
  Container.IncLock;
  for Dummy:=0 to High(fSchComponents) do
  begin
    fSchComponents[Dummy].Component.Visible:=Typ=fSchComponents[Dummy].CType;
  end;
  Container.DecLock;
  Container.DoFlip;
end;

procedure TKampfSimulation.ShowUFO(Show: boolean);
var
  Dummy: Integer;
begin
  Show:=not Show;
  for Dummy:=0 to Length(fUFOCOmponents)-1 do
  begin
    TDXComponent(fUFOComponents[Dummy].Component).Visible:=fUFOComponents[Dummy].Visible xor Show;
  end;
end;

procedure TKampfSimulation.StartKampf(Sender: TObject);
var
  Raumschiff : TRaumschiff;
  UFO        : TUFO;
  Model      : TForschProject;
//  Shield     : TShield;
  Motor      : TMotor;
  UFOModel   : TUFOModel;
begin
  FillChar(Model,SizeOf(Model),#0);
  Model.TypeID:=ptRaumSchiff;
  Model.Name:=ModelBox.Text;
  Model.ID:=random(High(Cardinal));
  Model.WaffenZellen:=WZEdit.Value;
  Model.HitPoints:=HitPointEdit.Value;
  fRaumschiffe.ClearModels;
  fRaumschiffe.AddForschItem(TObject(Addr(Model)));
  Raumschiff:=TRaumschiff.Create(fRaumschiffe,0);
  Raumschiff.Name:=NameEdit.Text;

  (*
  { Schutzschild f�r Raumschiff initialisieren }
  if SPointEdit.Value>0 then
  begin
    FillChar(Shield,SizeOf(Shield),#0);
    Shield.Installiert:=true;
    Shield.Points:=SPointEdit.Value;
    Shield.Abwehr:=SAbwehrEdit.Value;
    Shield.Laden:=LadeZeitEdit.Value;
    Raumschiff.Shield:=Shield;
    Raumschiff.RechargeShield(0,true);
  end;
  *)
  { Motor f�r Raumschiff initsialisieren }
  FillChar(Motor,SizeOf(Motor),#0);
  Motor.Installiert:=true;
  Motor.Duesen:=DuesenEdit.Value=0;
  Motor.PpS:=PPSEdit.Value;
  Raumschiff.Motor:=Motor;
  FillChar(UFOModel,SizeOf(UFOModel),#0);
  { Waffen initsialisieren }
  Raumschiff.WaffenZelle[0]:=fWZellen[1];
  Raumschiff.WaffenZelle[1]:=fWZellen[2];
  Raumschiff.WaffenZelle[2]:=fWZellen[3];

  { UFO initialisieren }
  UFO:=TUFO.Create(fUFOList);
  UFOModel.Name:=UFOName.Text;
  UFOModel.HitPoints:=UFOHitPoints.Value;
  UFOModel.Shield:=UFOShield.Value;
  UFOModel.ShieldType.Projektil:=UFOPDefense.Value;
  UFOModel.ShieldType.Rakete:=UFORDefense.Value;
  UFOModel.ShieldType.Laser:=UFOLDefense.Value;
  UFOModel.ShieldType.Laden:=UFOLadeZeit.Value;
  UFOModel.Angriff:=UFOAttack.Value;
  UFOModel.Treffsicherheit:=UFOTreff.Value;
  UFOModel.Pps:=UFOPPSEdit.Value;
  UFOModel.Image:=ImageProducer.GetNumber;

  UFO.SetModel(UFOModel);
  UFO.Name:=UFOName.Text;
  UFO.Init;
  Raumschiff.HuntingUFO(UFO.ID);
  fKampfIntro.ParentPage:=PageKampfSimul;
  fKampfIntro.AddKampf(Raumschiff,UFO);
  ChangePage(PageUFOIntro);
  Container.PlayMusicCategory('Hauptmen�');

  if fKampfIntro.Raumschiff<>nil then
    Raumschiff.Free;

  if fKampfIntro.UFO<>nil then
    UFO.Free;

  fKampfIntro.ParentPage:=PageGameMenu;
end;

procedure TKampfSimulation.UserButtonClick(Sender: TObject);
var
  LChange: TNotifyEvent;
begin
  // Hier darf kein OnChange der Listbox aufgerufen werden (Sichtbarschalten der ListBox)
  // Dies f�hrt dazu, dass der Index in fSelected wieder ge�ndert wird und dann das erste
  // Objekt ausgew�hlt w�re
  Container.Lock;
  LChange:=List.OnChange;
  List.OnChange:=nil;
  if (Sender as TDXBitmapButton).HighLight then
    ShowPage(nil)
  else
    ShowPage(Sender);
  (Sender as TDXBitmapButton).HighLight:=not (Sender as TDXBitmapButton).HighLight;
  List.OnChange:=LChange;
  Container.Unlock;
  Container.RedrawArea(fSchiffRect,Container.Surface);
end;

procedure TKampfSimulation.WaffeChanged(Sender: TObject);
begin
  Container.Lock;
  if fList=3 then
    EditCaption.Caption:=Format(LWZelle,[WaffenBox.Value]);
  WName.Text:=fWZellen[WaffenBox.Value].Name;
  AttackEdit.Value:=fWZellen[WaffenBox.Value].Strength;
  SchussEdit.Value:=fWZellen[WaffenBox.Value].Munition;
  LadeEdit.Value:=fWZellen[WaffenBox.Value].Laden;
  WPPSEdit.Value:=fWZellen[WaffenBox.Value].PixPerSec;
  WeiteEdit.Value:=fWZellen[WaffenBox.Value].Reichweite;
  case fWZellen[WaffenBox.Value].WaffType of
    wtProjektil : TypeEdit.Value:=1;
    wtRaketen   : TypeEdit.Value:=2;
    wtLaser     : TypeEdit.Value:=3;
  end;
  case fWZellen[WaffenBox.Value].Verfolgung of
    true  : FolgeEdit.Value:=1;
    false : FolgeEdit.Value:=2;
  end;
  Container.Unlock;
  Container.RedrawArea(fSchiffRect,Container.Surface);
end;

procedure TKampfSimulation.WaffenSettingChange(Sender: TObject);
begin
  fWZellen[WaffenBox.Value].Strength:=AttackEdit.Value;
  fWZellen[WaffenBox.Value].Munition:=SchussEdit.Value;
  fWZellen[WaffenBox.Value].Munition:=SchussEdit.Value;
  fWZellen[WaffenBox.Value].Laden:=LadeEdit.Value;
  fWZellen[WaffenBox.Value].PixPerSec:=WPPSEdit.Value;
  fWZellen[WaffenBox.Value].Reichweite:=WeiteEdit.Value;
  case TypeEdit.Value of
    1: fWZellen[WaffenBox.Value].WaffType:=wtProjektil;
    2: fWZellen[WaffenBox.Value].WaffType:=wtRaketen;
    3: fWZellen[WaffenBox.Value].WaffType:=wtLaser;
  end;
  case FolgeEdit.Value of
    1: fWZellen[WaffenBox.Value].Verfolgung:=true;
    2: fWZellen[WaffenBox.Value].Verfolgung:=false;
  end;
  fSelected[WaffenBox.Value+3].Index:=-1;
end;

procedure TKampfSimulation.WaffenZellenChanged(Sender: TObject);
begin
  WaffenBox.max:=WZEdit.Value;
  fWZellen[1].Installiert:=true;
  fWZellen[2].Installiert:=WZEdit.Value>=2;
  fWZellen[3].Installiert:=WZEdit.Value>=3;
  Container.Lock;
  WaffeChanged(nil);
  Container.Unlock;
  Container.RedrawArea(fSchiffRect,Container.Surface,false);
  ModelBox.Text:=LNoStandard;
  fSelected[0].Index:=-1;
end;

procedure TKampfSimulation.WNameChange(Sender: TObject);
begin
  fWZellen[WaffenBox.Value].Name:=WName.Text;
  WaffenBox.Redraw;
end;

procedure TKampfSimulation.ChooseUFOSet(Sender: TObject);
var
  Result  : Integer;
  LChange : TNotifyEvent;
begin
  result:=ChooseGameSet;
  if result=-1 then exit;
  Container.Lock;
  LChange:=List.OnChange;
  List.OnChange:=nil;
  LoadUFOModells(Files[Result].FileName);
  List.OnChange:=LChange;
  Container.UnLock;
  Container.IncLock;
  GameSetUFOBox.Redraw;
  Container.DecLock;
  UFOList.Redraw;
end;

procedure TKampfSimulation.UFOUserButtonClick(Sender: TObject);
begin
  Container.Lock;
  (Sender as TDXBitmapButton).HighLight:=not (Sender as TDXBitmapButton).HighLight;
  ShowUFO((Sender as TDXBitmapButton).HighLight);
  Container.UnLock;
  Container.RedrawArea(fUFORect,Container.Surface);
end;

procedure TKampfSimulation.SaveButtonClick(Sender: TObject);
var
  Text    : String;
  Archiv  : TArchivFile;
  Stream  : TMemoryStream;
  Zaehler : Integer;
  Index   : Integer;
  Dummy   : Integer;
  WriteTo : Integer;

  procedure WriteWord(Wert: Word);
  begin
    Stream.Write(Wert,SizeOf(Wert));
  end;

begin
  Text:=NameEdit.Text;
  QueryText(CSaveName,20,Text,ctBlue);
  if Text='' then exit;
  LoadShips;

  { Jetzt muss erstmal geschaut werden, ob bereits eine Konfiguration unter dem  }
  { angegebenen Namen gespeichert ist. Der Index, wo die neue Konfiguration      }
  { wird in WriteTo gespeichert. Wenn es noch keine Konfiguration unter diesem   }
  { Namen gibt, ist WriteTo noch -1 und ein neuer Datensatz muss angelegt werden }
  WriteTo:=-1;
  Index:=length(Ships);
  for Dummy:=0 to Index-1 do
  begin
    if Ships[Dummy].Name=Text then
    begin
      if not game_api_Question(Format(MOverwriteShip,[Text]),CQuestion) then
      begin
        exit;
      end;
      WriteTo:=Dummy;
      break;
    end;
  end;
  if WriteTo=-1 then
  begin
    SetLength(Ships,Index+1);
    WriteTo:=Index;
    inc(Index);
  end;
  { Speichern der Konfiguration an Index WriteTo }
  with Ships[WriteTo] do
  begin
    Name:=Text;
    Model:=ModelBox.Text;
    Hitpoints:=HitPointEdit.Value;
    WZellen:=WZEdit.Value;
    Schild:=ShieldBox.Text;
    Schildpunkte:=SPointEdit.Value;
    Abwehr:=SAbwehrEdit.Value;
    AufladeZeit:=LadeZeitEdit.Value;
    Motor:=MotorBox.Text;
    Geschwindig:=PPSEdit.Value;
    Duesen:=DuesenEdit.Value;
    for Dummy:=1 to WZellen do
    begin
      with Waffen[Dummy] do
      begin
        Name:=fWZellen[Dummy].Name;
        ID:=fWZellen[Dummy].ID;
        Munition:=fWZellen[Dummy].Munition;
        WaffType:=Word(fWZellen[Dummy].WaffType);
        Strength:=fWZellen[Dummy].Strength;
        PPS:=fWZellen[Dummy].PixPerSec;
        LadeZeit:=fWZellen[Dummy].Laden;
        Verfolgung:=Word(fWZellen[Dummy].Verfolgung);
        ReichWeite:=fWZellen[Dummy].Reichweite;
      end;
    end;
  end;
  { Hier wird jetzt das komplette Ships-Array gespeichert }
  Stream:=TMemoryStream.Create;
  for Dummy:=0 to Index-1 do
  begin
    with Ships[Dummy] do
    begin
      Stream.Write(SchiffKSVersion,SizeOf(Cardinal));
      WriteString(Stream,Name);
      WriteString(Stream,Model);
      WriteWord(Hitpoints);
      WriteWord(WZellen);
      WriteString(Stream,Schild);
      WriteWord(Schildpunkte);
      WriteWord(Abwehr);
      WriteWord(AufladeZeit);
      WriteString(Stream,Motor);
      WriteWord(Geschwindig);
      WriteWord(Duesen);
      for Zaehler:=1 to WZellen do
      begin
        with Waffen[Zaehler] do
        begin
          WriteString(Stream,Name);
          Stream.Write(ID,SizeOf(Cardinal));
          WriteWord(Munition);
          WriteWord(WaffType);
          WriteWord(Strength);
          WriteWord(PPS);
          WriteWord(LadeZeit);
          WriteWord(Verfolgung);
          WriteWord(Reichweite);
        end;
      end;
    end;
  end;
  NameEdit.Text:=Text;
  Archiv:=TArchivFile.Create;
  Archiv.OpenArchiv(FGameDataFile);
  Archiv.ReplaceRessource(Stream,RNKampfSchiffe);
  Archiv.Free;
  Stream.Free;
end;

procedure TKampfSimulation.LoadButtonClick(Sender: TObject);
var
  Dummy    : Integer;
  Result   : Integer;
begin
  LoadShips;
  if length(Ships)=0 then
  begin
    game_api_MessageBox(MNoSchiffsSaved,CInformation);
    exit;
  end;
  ListBoxWindow.Items.Clear;
  for Dummy:=0 to high(Ships) do
  begin
    ListBoxWindow.Items.AddObject(Ships[Dummy].Name,TObject(Dummy));
  end;
  ListBoxWindow.Caption:=CChooseShip;
  Result:=ListBoxWindow.Show;
  ListBoxWindow.Caption:=CChooseGameSet;
  if Result=-1 then exit;
  Container.Lock;
  with Ships[Integer(ListBoxWindow.Items.Objects[Result])] do
  begin
    NameEdit.Text:=Name;
    ModelBox.Text:=Model;
    HitPointEdit.Value:=HitPoints;
    WZEdit.Value:=WZellen;
    ShieldBox.Text:=Schild;
    SPointEdit.Value:=Schildpunkte;
    SAbwehrEdit.Value:=Abwehr;
    LadeZeitEdit.Value:=AufladeZeit;
    MotorBox.Text:=Motor;
    PPSEdit.Value:=Geschwindig;
    DuesenEdit.Value:=Duesen;
    for Dummy:=1 to WZellen do
    begin
      with fWZellen[Dummy] do
      begin
        Name:=Waffen[Dummy].Name;
        ID:=Waffen[Dummy].ID;
        Munition:=Waffen[Dummy].Munition;
        WaffType:=TWaffenType(Waffen[Dummy].WaffType);
        Strength:=Waffen[Dummy].Strength;
        PixPerSec:=Waffen[Dummy].PPS;
        Laden:=Waffen[Dummy].LadeZeit;
        Verfolgung:=Boolean(Waffen[Dummy].Verfolgung);
        Reichweite:=Waffen[Dummy].ReichWeite;
      end;
    end;
    WaffeChanged(nil);
    WaffenZellenChanged(nil);
  end;
  Container.UnLock;
  Container.IncLock;
  ModelBox.Redraw;
  NameEdit.ReDraw;
  ShieldBox.Redraw;
  WaffenBox.Redraw;
  MotorBox.Redraw;
  Container.DecLock;
  Container.RedrawArea(fSchiffRect,Container.Surface);
end;

procedure TKampfSimulation.LoadShips;
var
  Archiv   : TArchivFile;
  Version  : Cardinal;
  Stream   : TStream;
  Dummy    : Integer;
  Index    : Integer;

  function ReadWord: Word;
  begin
    Stream.Read(result,SizeOf(Word));
  end;

begin
  Archiv:=TArchivFile.Create;
  Archiv.OpenArchiv(FGameDataFile,true);
  try
    Archiv.OpenRessource(RNKampfSchiffe);
  except
    Archiv.Free;
    exit;
  end;
  Stream:=Archiv.Stream;
  Index:=0;
  while Stream.Read(Version,SizeOf(Integer))=SizeOf(Integer) do
  begin
    if Version<>SchiffKSVersion then break;
    SetLength(Ships,Index+1);
    with Ships[Index] do
    begin
      Name:=ReadString(Stream);
      Model:=ReadString(Stream);
      Hitpoints:=ReadWord;
      WZellen:=ReadWord;
      Schild:=ReadString(Stream);
      Schildpunkte:=ReadWord;
      Abwehr:=ReadWord;
      AufladeZeit:=ReadWord;
      Motor:=ReadString(Stream);
      Geschwindig:=ReadWord;
      Duesen:=ReadWord;
      for Dummy:=1 to WZellen do
      begin
        with Waffen[Dummy] do
        begin
          Name:=ReadString(Stream);
          Stream.Read(ID,SizeOf(Cardinal));
          Munition:=ReadWord;
          WaffType:=ReadWord;
          Strength:=ReadWord;
          PPS:=ReadWord;
          LadeZeit:=ReadWord;
          Verfolgung:=ReadWord;
          ReichWeite:=ReadWord;
        end;
      end;
    end;
    inc(Index);
  end;
  Archiv.Free;
end;

procedure TKampfSimulation.SetDefaults;
begin
  inherited;
  Assert(false,'Kampfsimulator wird zur Zeit nicht unterst�tzt');
end;

end.
