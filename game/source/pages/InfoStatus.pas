{********************************************************************************
*										*
* Copyright (C) 2004  X-Force Team - jim_raynor@web.de				*
*										*
* This file is part of X-Force: Fight For Destiny (X-Force).			*
*										*
* X-Force is free software; you can redistribute it and/or modify		*
* it under the terms of the GNU General Public License as published by		*
* the Free Software Foundation; either version 2 of the License, or		*
* (at your option) any later version.						*
*										*
* X-Force is distributed in the hope that it will be useful,			*
* but WITHOUT ANY WARRANTY; without even the implied warranty of		*
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the			*
* GNU General Public License for more details.					*
*										*
* You should have received a copy of the GNU General Public License		*
* along with X-Force; if not, write to the Free Software			*
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA	*
*										*
*********************************************************************************
*										*
* Wochenbilanz (Punkte und Credits)						*
*										*
********************************************************************************}

{$I ../Settings.inc}

unit InfoStatus;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  KD4Page, DXContainer, Defines, DXBitmapButton, StringConst, DXLabel,
  XForce_types, DXPointStatus, DXInfoFrame, DXMonthStatus, DXListBox, Blending,
  DirectDraw, DXDraws, DirectFont, KD4Utils, ufopaedie_api;

type
  TInfoStatus = class(TKD4Page)
  private
    { Private-Deklarationen }
    fComponentList  : TList;
    procedure DrawOrganHeader(Sender: TDXComponent;
      Surface: TDirectDrawSurface; Rect: TRect);
    procedure DrawOrganisation(Sender: TDXComponent;
      Surface: TDirectDrawSurface; const Mem: TDDSurfaceDesc; Rect: TRect;
      Index: Integer; Selected: boolean);
    procedure OrganMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; x, y: Integer);
  protected
    procedure WeekEndHandler(Sender: TObject);

    procedure ShowBilanzObjects(Sender: TObject);

    procedure ShowPage(Page: Integer);

    { Protected-Deklarationen }
  public
    BilanzButton  : TDXBitmapButton;
    FinanzButton  : TDXBitmapButton;
    HilfeButton   : TDXBitmapButton;

    CaptionLabel  : TDXLabel;

    PointStatus   : TDXPointStatus;
    FinanzStatus  : TDXPointStatus;
    MonthStatus   : TDXMonthStatus;

    OrganBox      : TDXListBox;
    HilfeGesamt   : TDXPointStatus;

    InfoFrame     : TDXInfoFrame;

    constructor Create(Container: TDXContainer);override;
    destructor Destroy;override;
    procedure SetDefaults;override;
  end;

var
  Captions : Array[0..2] of String;

implementation

uses
  savegame_api;

{ TInfoStatus }

constructor TInfoStatus.Create(Container: TDXContainer);
begin
  inherited;
  Animation:=paLeftToRight;

  fComponentList:=TList.Create;

  SetPageInfo(ST0502200002);

  { Informationsframe }
  InfoFrame:=TDXInfoFrame.Create(Self);
  with InfoFrame do
  begin
    SetRect(388,10,244,70);
    ColorSheme:=icsRed;
    SetFont(Font);
    Infos:=[];
    SystemKeyChange:=true;
    Tag:=-1;
  end;

  { Zusammenfassung }
  HilfeGesamt:=TDXPointStatus.Create(Self);
  with HilfeGesamt do
  begin
    SetRect(450,140,262,200);
    SetFont(Font);
    Tag:=0;
  end;
  fComponentList.Add(HilfeGesamt);

  { Box zur Anzeige der Organisationen }
  OrganBox:=TDXListBox.Create(Self);
  with OrganBox do
  begin
    SetRect(88,140,350,347);
    SetFont(Font);
    SetButtonFont(ScrollBarFont);
    BorderColor:=coFirstColor;
    FocusColor:=coSecondColor;
    ItemHeight:=30;
    FirstColor:=coScrollBrush;
    SecondColor:=coScrollPen;
    OwnerDraw:=true;
    OnDrawItem:=DrawOrganisation;
    OnMouseUp:=OrganMouseUp;
    HeadRowHeight:=17;
    OnDrawHeadRow:=DrawOrganHeader;
    Tag:=0;
  end;
  fComponentList.Add(OrganBox);

  { Überschrift }
  CaptionLabel:=TDXLabel.Create(Self);
  with CaptionLabel do
  begin
    SetFont(Font);
    Font.Size:=16;
    SetRect(0,103,800,25);
    AutoSize:=false;
    Alignment:=taCenter;
    Tag:=-1;
  end;

  { Punktebilanz }
  PointStatus:=TDXPointStatus.Create(Self);
  with PointStatus do
  begin
    SetRect(90,140,300,300);
    SetFont(Font);
    Tag:=2;
  end;
  fComponentList.Add(PointStatus);

  { Finanzbilanz - Zusammenfassung}
  FinanzStatus:=TDXPointStatus.Create(Self);
  with FinanzStatus do
  begin
    SetRect(400,140,300,300);
    SetFont(Font);
    Tag:=2;
  end;
  fComponentList.Add(FinanzStatus);

  { Monatsbilanz }
  MonthStatus:=TDXMonthStatus.Create(Self);
  MonthStatus.SetRect(88,140,624,396);
  MonthStatus.Tag:=1;

  fComponentList.Add(MonthStatus);

  { Unterstützung Button }
  HilfeButton:=TDXBitmapButton.Create(Self);
  with HilfeButton do
  begin
    Text:=BClose;
    SetRect(8,54,110,28);
    FirstColor:=coFirstColor;
    SecondColor:=coSecondColor;
    AccerlateColor:=coAccerlateColor;
    RoundCorners:=rcLeft;
    SetButtonFont(Font);
    Text:=BFHilfe;
    Hint:=HFHilfe;
    OnClick:=ShowBilanzObjects;
    Tag:=0;                  // Nur Objekte mit Tag 0 werden angezeigt
  end;
  fComponentList.Add(HilfeButton);

  { Finanzen Button }
  FinanzButton:=TDXBitmapButton.Create(Self);
  with FinanzButton do
  begin
    Text:=BClose;
    SetRect(119,54,110,28);
    FirstColor:=coFirstColor;
    SecondColor:=coSecondColor;
    AccerlateColor:=coAccerlateColor;
    RoundCorners:=rcNone;
    SetButtonFont(Font);
    Text:=BMonthStat;
    Hint:=HMonthStat;
    OnClick:=ShowBilanzObjects;
    Tag:=1;                  // Nur Objekte mit Tag 1 werden angezeigt
  end;
  fComponentList.Add(FinanzButton);

  { Wochenbilanz Button }
  BilanzButton:=TDXBitmapButton.Create(Self);
  with BilanzButton do
  begin
    Text:=BClose;
    SetRect(230,54,110,28);
    FirstColor:=coFirstColor;
    SecondColor:=coSecondColor;
    AccerlateColor:=coAccerlateColor;
    RoundCorners:=rcRight;
    SetButtonFont(Font);
    Text:=BPunkteStat;
//    Hint:=HMonthStat;
    OnClick:=ShowBilanzObjects;
    Tag:=2;                  // Nur Objekte mit Tag 2 werden angezeigt
  end;
  fComponentList.Add(BilanzButton);

  savegame_api_RegisterWeekEndHandler(WeekEndHandler);

  Captions[0]:=LPunkteStat;
  Captions[1]:=LKalkulation;
  Captions[2]:=LMonthStat;
end;

procedure TInfoStatus.DrawOrganHeader(Sender: TDXComponent;
  Surface: TDirectDrawSurface; Rect: TRect);
begin
  WhiteStdFont.Draw(Surface,Rect.Right-8-WhiteStdFont.TextWidth(LBudget),Rect.Top+2,LBudget);
  WhiteStdFont.Draw(Surface,Rect.Left+8,Rect.Top+2,IIOrganisation);
end;

procedure TInfoStatus.DrawOrganisation(Sender: TDXComponent;
  Surface: TDirectDrawSurface;const Mem: TDDSurfaceDesc; Rect: TRect; Index: Integer;
  Selected: boolean);
var
  Text    : String;
  Font    : TDirectFont;
  Organ   : Integer;
begin
  Organ:=StrToInt(OrganBox.Items[Index]);
  if Selected then
  begin
    OrganBox.DrawSelection(Surface,Mem,Rect,bcMaroon);
  end;
  Font:=YellowStdFont;
  case OrganStatus(SaveGame.Organisations[Organ].Friendly) of
    osAllianz    : Font:=LimeStdFont;
    osFriendly   : Font:=GreenStdFont;
    osNeutral    : Font:=YellowStdFont;
    osUnFriendly : Font:=MaroonStdFont;
    osEnemy      : Font:=RedStdFont;
  end;
  SaveGame.Organisations.DrawOrgan(Surface,Rect.Left+4,Rect.Top+4,Organ);
  Font.Draw(Surface,Rect.Left+49,Rect.Top+8,SaveGame.Organisations[Organ].Name);
  Text:=Format(FCredits,[SaveGame.Organisations.Hilfe(Organ)/1]);
  Font.Draw(Surface,Rect.Right-8-Font.TextWidth(Text),Rect.Top+8,Text);
end;

procedure TInfoStatus.OrganMouseUp(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; x, y: Integer);
begin
  if Button<>mbRight then exit;
  if OrganBox.Items.Count=0 then exit;
  ufopaedie_api_ShowCountryInfo(StrToInt(OrganBox.Items[OrganBox.ItemIndex]));
end;

procedure TInfoStatus.WeekEndHandler(Sender: TObject);
begin
  if SaveGame.MonthEnd then
    ChangePage(PageInfoStatus);
end;

procedure TInfoStatus.SetDefaults;
var
  Dummy   : Integer;
  Dummy2  : Integer;
  Insert  : boolean;
begin
  CaptionLabel.Text:=SaveGame.GetSettlementWeek(LPunkteStat);
  PointStatus.Points:=SaveGame.PunkteStatus;
  FinanzStatus.Finanz:=SaveGame.MonthStatus;
  FinanzStatus.Kapital:=SaveGame.Kapital;

  OrganBox.Items.Clear;
  for Dummy:=0 to SaveGame.Organisations.Count-1 do
  begin
    Insert:=false;
    Dummy2:=0;
    while (not Insert) and (Dummy2<OrganBox.Items.Count) do
    begin
      if SaveGame.Organisations.Hilfe(StrToInt(OrganBox.Items[Dummy2]))<SaveGame.Organisations.Hilfe(Dummy) then
      begin
        Insert:=true;
        OrganBox.Items.Insert(Dummy2,IntToStr(Dummy));
      end;
      inc(Dummy2);
    end;
    if not Insert then OrganBox.Items.Add(IntToStr(Dummy));
  end;

  if SaveGame.MonthEnd then
    CaptionLabel.Text:=SaveGame.GetSettlementWeek(LFHilfe)
  else
    CaptionLabel.Text:=SaveGame.GetSettlementWeek(LKalkulation);

  HilfeGesamt.Organisation:=SaveGame.Organisations;

  ShowPage(0);

  InfoFrame.Reset;
end;

procedure TInfoStatus.ShowBilanzObjects(Sender: TObject);
begin
  ShowPage((Sender as TDXBitmapButton).Tag)
end;

destructor TInfoStatus.Destroy;
begin
  fComponentList.Free;
  inherited;
end;

procedure TInfoStatus.ShowPage(Page: Integer);
var
  Comp    : TDXComponent;
  Dummy   : Integer;
begin
  Container.IncLock;
  for Dummy:=0 to fComponentList.Count-1 do
  begin
    Comp:=(TObject(fComponentList[Dummy]) as TDXComponent);
    if Comp is TDXBitmapButton then
      TDXBitmapButton(Comp).HighLight:=Comp.Tag=Page
    else
      Comp.Visible:=(TObject(fComponentList[Dummy]) as TDXComponent).Tag=Page;
  end;

  MonthStatus.Finanz:=SaveGame.MonthStatus;
  
  CaptionLabel.Text:=SaveGame.GetSettlementWeek(Captions[Page]);
  Container.DecLock;
end;

end.
