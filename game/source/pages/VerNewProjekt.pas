{********************************************************************************
*										*
* Copyright (C) 2004  X-Force Team - jim_raynor@web.de				*
*										*
* This file is part of X-Force: Fight For Destiny (X-Force).			*
*										*
* X-Force is free software; you can redistribute it and/or modify		*
* it under the terms of the GNU General Public License as published by		*
* the Free Software Foundation; either version 2 of the License, or		*
* (at your option) any later version.						*
*										*
* X-Force is distributed in the hope that it will be useful,			*
* but WITHOUT ANY WARRANTY; without even the implied warranty of		*
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the			*
* GNU General Public License for more details.					*
*										*
* You should have received a copy of the GNU General Public License		*
* along with X-Force; if not, write to the Free Software			*
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA	*
*										*
*********************************************************************************
*										*
* Verwaltungsbildschirm Herstellungsprojekte verwalten 				*
* (Werkstatt->Neues Projekte)							*
*										*
*********************************************************************************
*										*
********************************************************************************}

{$I ../Settings.inc}

unit VerNewProjekt;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  DXContainer, KD4Page, DXBitmapButton, Defines, StringConst, DXLabel, DXListBox,
  DXDraws, XForce_types, DXItemInfo, DXInfoFrame, DXScrollBar, Blending, TraceFile,
  DXEdit,math, DXGroupCaption, DirectDraw, DirectFont;


type
  TVerNewProjekt = class(TKD4Page)
  private
    fAreaRect   : TRect;
    fFilter     : TItemFilter;
    fItemsCou   : Integer;
    procedure StartProjekt(Sender: TObject);
    procedure BuyConstruction(Sender: TObject);
    procedure ChangeFilter(Sender: TObject);
    procedure OnChangeBar(Sender: TObject);
    procedure ShowPadie(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
    procedure SchnellSuche(Sender: TObject);
    procedure DrawAnzahlHeader(Sender: TDXComponent;Surface: TDirectDrawSurface;Rect: TRect);
    procedure DrawHeadRow(Sender: TDXComponent;Surface: TDirectDrawSurface;Rect: TRect);

    procedure ListBoxKeyPress(Sender: TObject; var Key: Word; Shift: TShiftState);
    { Private-Deklarationen }
  protected
    procedure DrawItems(Sender: TDXComponent;Surface: TDirectDrawSurface;const Mem: TDDSurfaceDesc;Rect: TRect; Index: Integer;Selected: boolean);
    procedure OnChangeItem(Sender: TObject);
    procedure OnSelectedBasisChange(Sender: TObject);
    { Protected-Deklarationen }
  public
    CreateButton   : TDXBitmapButton;
    BuyConstButton : TDXBitmapButton;
    InfoFrame      : TDXInfoFrame;
    Items          : TDXListBox;
    ItemInfo       : TDXItemInfo;
    CountHeader    : TDXGroupCaption;
    CountBar       : TDXScrollBar;

    SSLabel        : TDXGroupCaption;
    SSuche         : TDXEdit;
    SAnzahl        : TDXEdit;

    FAllButton     : TDXBitmapButton;
    FWafButton     : TDXBitmapButton;
    FGraButton     : TDXBitmapButton;
    FMinButton     : TDXBitmapButton;
    FSenButton     : TDXBitmapButton;
    FMotButton     : TDXBitmapButton;
    FPanButton     : TDXBitmapButton;
    constructor Create(Container: TDXContainer);override;
    procedure SetDefaults;override;
    procedure PageShown;override;
  end;

var
  ViewProjekt: Integer;

implementation

uses KD4Utils, game_api, ufopaedie_api, basis_api, ui_utils, game_utils;

{ TVerNewProjekt }

procedure TVerNewProjekt.BuyConstruction(Sender: TObject);
var
  Index : Integer;
  Land  : Integer;
  Preis : Double;
begin
  Index:=StrToInt(Items.Items[Items.ItemIndex]);
  Preis:=SaveGame.LagerListe.PreisOfConstruction(Index);
  Land:=SaveGame.LagerListe[Index].Land;
  if Preis=0 then exit;
  if not game_api_Question(Format(MBConstruction,[SaveGame.LagerListe[Index].Name,SaveGame.Organisations[Land].Name,Preis]),CBConstruction) then
    exit;
  try
    SaveGame.LagerListe.BuyPlan(Index);
  except
    on E: Exception do
    begin
      game_api_MessageBox(E.Message,CInformation);
    end;
  end;
  ItemInfo.Item:=SaveGame.LagerListe[Index];
  BuyConstButton.Enabled:=false;

  Items.Redraw;
end;

procedure TVerNewProjekt.ChangeFilter(Sender: TObject);
var
  Send: TDXBitmapButton;
begin
  Send:=(Sender as TDXBitmapButton);
  if Send.HighLight then exit;
  Container.IncLock;
  Container.LoadGame(true);
  Items.ItemIndex:=0;
  FAllButton.HighLight:=false;
  FWafButton.HighLight:=false;
  FGraButton.HighLight:=false;
  FMinButton.HighLight:=false;
  FSenButton.HighLight:=false;
  FMotButton.HighLight:=false;
  FPanButton.HighLight:=false;
  Send.HighLight:=true;
  if Send=FAllButton then
    fFilter:=ifAll
  else if Send=FWafButton then
    fFilter:=ifWaffen
  else if Send=FGraButton then
    fFilter:=ifGranate
  else if Send=FMinButton then
    fFilter:=ifMine
  else if Send=FSenButton then
    fFilter:=ifSensoren
  else if Send=FMotButton then
    fFilter:=ifMotoren
  else if Send=FPanButton then
    fFilter:=ifPanzerung;

  fItemsCou:=ui_utils_FillItems(Items,SaveGame.LagerListe,fFilter,SSuche.Text,SAnzahl.Text);

  OnChangeItem(Self);
  Container.LoadGame(false);
  CreateButton.Redraw;
  Container.DecLock;
  Container.RedrawArea(fAreaRect,Container.Surface);
end;

constructor TVerNewProjekt.Create(Container: TDXContainer);
begin
  inherited;
  Animation:=paRightToLeft;
  ViewProjekt:=-1;

  SetPageInfo(ST0309270023);

{ Informationsframe }
  InfoFrame:=TDXInfoFrame.Create(Self);
  with InfoFrame do
  begin
    SetRect(388,10,244,70);
    ColorSheme:=icsRed;
    SetFont(Font);
    Infos:=[ifAlphatron, ifLager];
    SystemKeyChange:=true;

    ReturnPage:=PageWerkStatt;
  end;

{ Schnellsuche }
  { Label }
  SSLabel:=TDXGroupCaption.Create(Self);
  with SSLabel do
  begin
    SetFont(Font);
    SetRect(8,54,95,23);
    RoundCorners:=rcLeftTop;
    Caption:=BSSuche;
    TopMargin:=5;
  end;

  { Eingabebox }
  SSuche:=TDXEdit.Create(Self);
  with SSuche do
  begin
    SetButtonFont(Font);
    BorderColor:=coFirstColor;
    FocusColor:=coSecondColor;
    RoundCorners:=rcRightTop;
    SetRect(104,54,274,23);
    LeftMargin:=5;
    TopMargin:=5;
    Hint:=HSSuche;
    OnChange:=SchnellSuche;
  end;

{ Anzahlfilter }
  { Label }
  with TDXGroupCaption.Create(Self) do
  begin
    SetFont(Font);
    SetRect(8,78,95,23);
    RoundCorners:=rcNone;
    TopMargin:=5;
    BorderColor:=coFirstColor;
    Caption:=IACount;
  end;

  { Eingabebox }
  SAnzahl:=TDXEdit.Create(Self);
  with SAnzahl do
  begin
    SetButtonFont(Font);
    BorderColor:=coFirstColor;
    Hint:=ST0502070001;
    RoundCorners:=rcNone;
    SetRect(104,78,274,23);
    FocusColor:=coSecondColor;
    LeftMargin:=5;
    TopMargin:=5;
    OnChange:=SchnellSuche;
  end;

{ Gegenstšnde }
  { Gegenstandslistbox }
  Items:=TDXListBox.Create(Self);
  with Items do
  begin
    SetRect(8,102,370,383);
    RoundCorners:=rcNone;
    BorderColor:=coFirstColor;
    FocusColor:=coSecondColor;
    SetFont(Font);
    SetButtonFont(ScrollBarFont);
    FirstColor:=coScrollBrush;
    SecondColor:=coScrollPen;
    Hint:=HItemList;
    ItemHeight:=40;
    OnChange:=OnChangeItem;
    OwnerDraw:=True;
    OnDrawItem:=DrawItems;
    OnMouseUp:=ShowPadie;
    HeadRowHeight:=17;
    OnDrawHeadRow:=DrawHeadRow;

    OnKeyDown:=ListBoxKeyPress;
  end;

  { Gegenstandsinformationen }
  ItemInfo:=TDXItemInfo.Create(Self);
  with ItemInfo do
  begin
    SetFont(Font);
    BorderColor:=coFirstColor;
    SetRect(508,54,284,469);
    Caption:=LItem;
    Infos:=[inProduktion,inAnzahl,inLager,inStrength,inPanzerung,inRInfos,inMotor];
    Hint:=HItemInfo;
  end;

{ Anzahlregelung }
  { FrameBox }
  CountHeader:=TDXGroupCaption.Create(Self);
  with CountHeader do
  begin
    SetRect(8,486,370,19);
    RoundCorners:=rcNone;
    SetFont(Font);
    DrawCaption:=DrawAnzahlHeader;
  end;

  { ScrollBar }
  CountBar:=TDXScrollBar.Create(Self);
  with CountBar do
  begin
    Kind:=sbHorizontal;
    SetRect(8,506,370,17);
    SetButtonFont(Font);
    FirstColor:=coScrollBrush;
    SecondColor:=coScrollPen;
    RoundCorners:=rcBottom;
    max:=101;
    min:=1;
    Hint:=HAnzBar;
    LargeChange:=10;
    OnChange:=OnChangeBar;
  end;

  { Neues Projekt }
  CreateButton:=TDXBitmapButton.Create(Self);
  with CreateButton do
  begin
    SetRect(388,54,110,28);
    FirstColor:=coFirstColor;
    SecondColor:=coSecondColor;
    AccerlateColor:=coAccerlateColor;
    SetButtonFont(Font);
    RoundCorners:=rcTop;
    Text:=BWStart;
    Hint:=HChangeProjekt;
    OnClick:=StartProjekt;
  end;

  { Konstruktionsplšne kaufen }
  BuyConstButton:=TDXBitmapButton.Create(Self);
  with BuyConstButton do
  begin
    SetRect(388,83,110,28);
    FirstColor:=coFirstColor;
    SecondColor:=coSecondColor;
    AccerlateColor:=coAccerlateColor;
    SetButtonFont(Font);
    RoundCorners:=rcBottom;
    Text:=BBConstruction;
    Hint:=HBConstruction;
    OnClick:=BuyConstruction;
  end;

  { Alle Kategorien Button }
  FAllButton:=TDXBitmapButton.Create(Self);
  with FAllButton do
  begin
    SetRect(388,121,110,28);
    FirstColor:=coFirstColor;
    SecondColor:=coSecondColor;
    AccerlateColor:=coAccerlateColor;
    SetButtonFont(Font);
    RoundCorners:=rcTop;
    HighLight:=true;
    OnClick:=ChangeFilter;
    Hint:=HFAll;
    Text:=BFAll;
  end;

  { Waffen Button }
  FWafButton:=TDXBitmapButton.Create(Self);
  with FWafButton do
  begin
    SetRect(388,150,110,28);
    FirstColor:=coFirstColor;
    SecondColor:=coSecondColor;
    AccerlateColor:=coAccerlateColor;
    SetButtonFont(Font);
    RoundCorners:=rcNone;
    HighLight:=false;
    OnClick:=ChangeFilter;
    Hint:=HFWaf;
    Text:=BFWaf;
  end;

  { Granate Button }
  FGraButton:=TDXBitmapButton.Create(Self);
  with FGraButton do
  begin
    SetRect(388,179,110,28);
    FirstColor:=coFirstColor;
    SecondColor:=coSecondColor;
    RoundCorners:=rcNone;
    AccerlateColor:=coAccerlateColor;
    SetButtonFont(Font);
    HighLight:=false;
    OnClick:=ChangeFilter;
    Hint:=HFGra;
    Text:=BFGra;
  end;

  { Minen Button }
  FMinButton:=TDXBitmapButton.Create(Self);
  with FMinButton do
  begin
    SetRect(388,208,110,28);
    FirstColor:=coFirstColor;
    SecondColor:=coSecondColor;
    AccerlateColor:=coAccerlateColor;
    RoundCorners:=rcNone;
    SetButtonFont(Font);
    HighLight:=false;
    OnClick:=ChangeFilter;
    Hint:=HFMin;
    Text:=BFMin;
  end;

  { Sensoren Button }
  FSenButton:=TDXBitmapButton.Create(Self);
  with FSenButton do
  begin
    SetRect(388,237,110,28);
    FirstColor:=coFirstColor;
    SecondColor:=coSecondColor;
    AccerlateColor:=coAccerlateColor;
    SetButtonFont(Font);
    HighLight:=false;
    RoundCorners:=rcNone;
    OnClick:=ChangeFilter;
    Hint:=HFSen;
    Text:=BFSen;
  end;

  { Motoren Button }
  FMotButton:=TDXBitmapButton.Create(Self);
  with FMotButton do
  begin
    SetRect(388,266,110,28);
    FirstColor:=coFirstColor;
    SecondColor:=coSecondColor;
    AccerlateColor:=coAccerlateColor;
    SetButtonFont(Font);
    HighLight:=false;
    OnClick:=ChangeFilter;
    RoundCorners:=rcNone;
    Hint:=HFMot;
    Text:=BFMot;
  end;

  { Panzerung Button }
  FPanButton:=TDXBitmapButton.Create(Self);
  with FPanButton do
  begin
    SetRect(388,295,110,28);
    FirstColor:=coFirstColor;
    SecondColor:=coSecondColor;
    AccerlateColor:=coAccerlateColor;
    RoundCorners:=rcBottom;
    SetButtonFont(Font);
    HighLight:=false;
    OnClick:=ChangeFilter;
    Hint:=HFPan;
    Text:=BFPan;
  end;

  UnionRect(fAreaRect,ItemInfo.ClientRect,Items.ClientRect);

  basis_api_RegisterChangeBaseHandler(OnSelectedBasisChange);
end;

procedure TVerNewProjekt.DrawAnzahlHeader(Sender: TDXComponent;
  Surface: TDirectDrawSurface; Rect: TRect);
var
  Text    : String;
begin
  WhiteStdFont.Draw(Surface,Rect.Left+6,Rect.Top+3,LCount);
  if CountBar.Value=101 then
    Text:=LUnbegrentzt
  else
    Text:=Format(FHerstellen,[CountBar.Value]);
  WhiteStdFont.Draw(Surface,Rect.Right-8-WhiteStdFont.TextWidth(Text),Rect.Top+3,Text);
end;

procedure TVerNewProjekt.DrawHeadRow(Sender: TDXComponent;
  Surface: TDirectDrawSurface;Rect: TRect);
var
  Text   : String;
begin
  WhiteStdFont.Draw(Surface,Rect.Left+6,Rect.Top+1,LItems);
  Text:=ZahlString(FItems,FItems,fItemsCou);
  WhiteStdFont.Draw(Surface,Rect.Right-6-WhiteStdFont.TextWidth(Text),Rect.Top+1,Text);
end;

procedure TVerNewProjekt.DrawItems(Sender: TDXComponent;
  Surface: TDirectDrawSurface;const Mem: TDDSurfaceDesc; Rect: TRect; Index: Integer;
  Selected: boolean);
var
  ItemIndex : Integer;
  Dummy     : Integer;
  Text      : String;
  TypeID    : TProjektType;
begin
  if Selected then
    Items.DrawSelection(Surface,Mem,Rect,bcMaroon);
  if Items.Items[Index][1]='C' then
  begin
    TypeID:=TProjektType(StrToInt(Cut(Items.Items[Index],2,length(Items.Items[Index]))));
    SaveGame.LagerListe.DrawImage(TypeIDToIndex(TypeID,wtProjektil),Surface,Rect.Left+3,Rect.Top+3);
    WhiteBStdFont.Draw(Surface,Rect.Left+43,Rect.Top+3,game_utils_TypeToStr(TypeID));
  end
  else
  begin
    ItemIndex:=StrToInt(Items.Items[Index]);
    SaveGame.LagerListe.DrawImage(SaveGame.LagerListe.Items[ItemIndex].ImageIndex,Surface,Rect.Left+35,Rect.Top+3);

    if SaveGame.LagerListe[ItemIndex].HerstellBar then
      YellowStdFont.Draw(Surface,Rect.Left+75,Rect.Top+6,SaveGame.LagerListe[ItemIndex].Name)
    else
      DisabledStdFont.Draw(Surface,Rect.Left+75,Rect.Top+6,SaveGame.LagerListe[ItemIndex].Name);

    WhiteStdFont.Draw(Surface,Rect.Left+75,Rect.Top+20,Format(FAnzahl,[SaveGame.LagerListe[ItemIndex].Anzahl]));
    if SaveGame.LagerListe.Items[ItemIndex].Land<>-1 then
      SaveGame.Organisations.DrawSmallOrgan(Surface,Rect.Left+10,Rect.Top+15,SaveGame.LagerListe.Items[ItemIndex].Land);
    Text:='';
    for Dummy:=0 to SaveGame.WerkStatt.ProjektCount-1 do
    begin
      if (SaveGame.WerkStatt.Projekte[Dummy].ItemID=SaveGame.LagerListe[ItemIndex].ID) and
         (SaveGame.WerkStatt.Projekte[Dummy].BasisID=SaveGame.BasisListe.Selected.ID) then
      begin
        if SaveGame.WerkStatt.Projekte[Dummy].Anzahl<101 then
          Text:=Format(LProdukt,[SaveGame.WerkStatt.Projekte[Dummy].Anzahl])
        else
          Text:=LEndLessProdukt;

        LimeBStdFont.Draw(Surface,Rect.Right-8-LimeBStdFont.TextWidth(Text),Rect.Top+20,Text);
        exit;
      end;
    end;
  end;
end;

procedure TVerNewProjekt.ListBoxKeyPress(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_LEFT then
    CountBar.Value:=CountBar.Value-1
  else if Key = VK_RIGHT then
    CountBar.Value:=CountBar.Value+1
  else if Key = VK_RETURN then
    CreateButton.DoClick;
end;

procedure TVerNewProjekt.OnChangeBar(Sender: TObject);
begin
  Container.RedrawArea(CountHeader.ClientRect,Container.Surface,false);
end;

procedure TVerNewProjekt.OnChangeItem(Sender: TObject);
var
  Dummy: Integer;
  ItemIndex: Integer;
begin
  if (Items.Items.Count=0) or (Items.Items[Items.ItemIndex][1]='C') then
  begin
    ItemInfo.ItemValid:=false;
    CreateButton.Enabled:=false;
    BuyConstButton.Enabled:=false;
    exit;
  end;
  ItemIndex:=StrToInt(Items.Items[Items.ItemIndex]);
  ItemInfo.Item:=SaveGame.LagerListe[ItemIndex];
  CreateButton.Enabled:=true;
  BuyConstButton.Enabled:=not SaveGame.LagerListe[ItemIndex].HerstellBar;
  for Dummy:=0 to SaveGame.WerkStatt.ProjektCount-1 do
  begin
    if (SaveGame.WerkStatt.Projekte[Dummy].ItemID=Savegame.LagerListe[ItemIndex].ID) and
       (SaveGame.WerkStatt.Projekte[Dummy].BasisID=SaveGame.BasisListe.Selected.ID) then
    begin
      CreateButton.Text:=BWChange;
      CreateButton.Hint:=HChangeProjekt;
      CountBar.Value:=SaveGame.WerkStatt.Projekte[Dummy].Anzahl;
      exit;
    end;
  end;
  CreateButton.Text:=BWStart;
  CreateButton.Hint:=HBegin;
end;

procedure TVerNewProjekt.OnSelectedBasisChange(Sender: TObject);
begin
  if Container.ActivePage<>Self then
    exit;

  Container.IncLock;
  Items.Redraw;
  OnChangeItem(Self);
  Container.DecLock;
  Container.DoFlip;
end;

procedure TVerNewProjekt.PageShown;
begin
  if ViewProjekt>-1 then
  begin
    Items.ChangeItem(false);
    ViewProjekt:=-1;
  end;
end;

procedure TVerNewProjekt.SchnellSuche(Sender: TObject);
var
  OldVis  : boolean;
begin
  OldVis:=CreateButton.Enabled;
  Container.Lock;
  fItemsCou:=ui_utils_FillItems(Items,SaveGame.LagerListe,fFilter,SSuche.Text,SAnzahl.Text);
  Items.OnChange(Self);
  Container.UnLock;
  Container.RedrawArea(ItemInfo.ClientRect,Container.Surface,false);
  if (OldVis<>CreateButton.Enabled) then Container.RedrawArea(CreateButton.ClientRect,Container.Surface,false);
  Items.Redraw;
end;

procedure TVerNewProjekt.SetDefaults;
var
  Dummy: Integer;
begin
  InfoFrame.Reset;
  FAllButton.HighLight:=false;
  SSuche.Text:='';
  ChangeFilter(FAllButton);
  if Items.Items[Items.ItemIndex][1]<>'C' then
    ItemInfo.Item:=SaveGame.LagerListe[StrToInt(Items.Items[Items.ItemIndex])];
  Container.FocusControl:=Items;
  if ViewProjekt>-1 then
  begin
    for Dummy:=0 to Items.Items.Count-1 do
    begin
      if Items.Items[Dummy][1]<>'C' then
      begin
        if SaveGame.LagerListe[StrToInt(Items.Items[Dummy])].ID=SaveGame.WerkStatt.Projekte[ViewProjekt].ItemID then
        begin
          Items.ItemIndex:=Dummy;
//          Items.TopItem:=Items.ItemIndex;
          Items.ChangeItem(true);
          OnChangeItem(nil);
          exit;
        end;
      end;
    end;
  end;
  OnChangeItem(nil);
end;

procedure TVerNewProjekt.ShowPadie(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  if Button=mbRight then
  begin
    if (Items.Items.Count=0) or (Items.Items[Items.ItemIndex][1]='C') then exit;
    ufopaedie_api_ShowEntry(upeLagerItem,StrToInt(Items.Items[Items.ItemIndex]));
  end;
end;

procedure TVerNewProjekt.StartProjekt(Sender: TObject);
var
  result: Boolean;
begin
  try
    Container.IncLock;
    Container.LoadGame(true);
    SaveGame.WerkStatt.AddProjekt(SaveGame.LagerListe[StrToInt(Items.Items[Items.ItemIndex])],CountBar.Value);
    Container.LoadGame(false);
    Items.Redraw;
    InfoFrame.Redraw;
    Container.DecLock;
    if CountBar.Value=101 then
      game_api_MessageBox(Format(MHersStart,[SaveGame.LagerListe[StrToInt(Items.Items[Items.ItemIndex])].Name]),CInformation)
    else
      game_api_MessageBox(Format(MCHersStart,[CountBar.Value,SaveGame.LagerListe[StrToInt(Items.Items[Items.ItemIndex])].Name]),CInformation);
    CreateButton.Text:=BWChange;
    CreateButton.Hint:=HChangeProjekt;
  except
    on E: EProjektExists do
    begin
      Container.LoadGame(false);
      Container.DecLock;
      if CountBar.Value=101 then
        result:=game_api_Question(E.Message+' '+MEndlessProjekt,CChangeProjekt)
      else
        result:=game_api_Question(Format(E.Message+' '+MChangeProjekt,[CountBar.Value]),CChangeProjekt);
      if result then
      begin
        SaveGame.WerkStatt.ChangeProjekt(SaveGame.LagerListe[StrToInt(Items.Items[Items.ItemIndex])].ID,CountBar.Value);
        Items.Redraw;
        InfoFrame.Redraw;
      end;
    end;
    on E: Exception do
    begin
      Container.LoadGame(false);
      Container.DecLock;
      game_api_MessageBox(E.Message,CInformation);
    end;
  end;
end;

end.
