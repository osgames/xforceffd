{********************************************************************************
*										*
* Copyright (C) 2004  X-Force Team - jim_raynor@web.de				*
*										*
* This file is part of X-Force: Fight For Destiny (X-Force).			*
*										*
* X-Force is free software; you can redistribute it and/or modify		*
* it under the terms of the GNU General Public License as published by		*
* the Free Software Foundation; either version 2 of the License, or		*
* (at your option) any later version.						*
*										*
* X-Force is distributed in the hope that it will be useful,			*
* but WITHOUT ANY WARRANTY; without even the implied warranty of		*
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the			*
* GNU General Public License for more details.					*
*										*
* You should have received a copy of the GNU General Public License		*
* along with X-Force; if not, write to the Free Software			*
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA	*
*										*
*********************************************************************************
*										*
* Bildschirm Neues Spiel erstellen						*
*										*
********************************************************************************}

{$I ../Settings.inc}

unit NewGame;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  DXContainer, DXBitmapButton, KD4Page, Defines, StringConst, DXProgressBar,
  DXEdit, DXGroupCaption, DXSpinEdit, DXListBox, DXDraws, XForce_types, Blending, KD4Utils,
  ArchivFile,DXLabel, Koding, Loader, DXListBoxWindow, DXBasisSelector,
  TraceFile, DirectDraw, DirectFont, NGTypes;

const
  StartKap : Array[0..4] of Integer = (1000000,750000,500000,250000,100000);

type
  TNewGame = class(TKD4Page)
  private
    fKapital   : Int64;
    fAlphatron : Int64;
    fMaxWohn   : Integer;
    fWohnRaum  : Integer;
    fAreaRect  : TRect;
    fGameSet   : TSetIdentifier;
    procedure SetKapital(const Value: Int64);
    procedure SetAlphatron(const Value: Int64);
    { Private-Deklarationen }
  protected
    Files   : Array of TSetIdentifier;
    procedure IncKapital(Sender: TObject);
    procedure DecKapital(Sender: TObject);
    procedure IncMenschen(Sender: TObject);
    procedure DecMenschen(Sender: TObject);
    procedure CanDecHers(Sender: TDXComponent;var CanChange: boolean);
    procedure CanIncAlien(Sender: TDXComponent;var CanChange: boolean);
    procedure CanIncSoldat(Sender: TDXComponent;var CanChange: boolean);
    procedure CanIncAlphatron(Sender: TDXComponent;var CanChange: boolean);
    procedure OnLevelChange(Sender: TObject);
    procedure OnPlayerChange(Sender: TObject);
    procedure DrawPlayerItem(Sender: TDXComponent;Surface: TDirectDrawSurface;const Mem: TDDSurfaceDesc;Rect: TRect; Index: Integer;Selected: boolean);
    procedure PlayerTextChange(Sender: TObject);
    procedure GameSetClick(Sender: TObject);
    procedure GameSetInfoClick(Sender: TObject);
    procedure StartGame(Sender: TObject);
    procedure NeedLevelName(Value: Integer;var Text: String);
    procedure NeedDayName(Value: Integer;var Text: String);
    procedure ReadGameSets;

    function GetGameSetLanguage: String;
    { Protected-Deklarationen }
  public
    CloseButton         : TDXBitmapButton;
    StartButton         : TDXBitmapButton;
    PlayerEdit          : TDXEdit;
    GameEdit            : TDXEdit;
    PlayerEditLabel     : TDXGroupCaption;
    HersEdit            : TDXSpinEdit;
    ForsEdit            : TDXSpinEdit;
    AlienEdit           : TDXSpinEdit;
    UFOEdit             : TDXSpinEdit;
    WohnBox             : TDXEdit;
    KapitalBox          : TDXEdit;
    AlphatronBox        : TDXSpinEdit;
    SoldatBox           : TDXSpinEdit;
    ForscherBox         : TDXSpinEdit;
    TechnikerBox        : TDXSpinEdit;
    PlayersBox          : TDXListBox;
    GameSetBox          : TDXEdit;
    LevelFrameBox       : TDXGroupCaption;
    LevelSpinEdit       : TDXSpinEdit;
    ListBoxWindow       : TDXListBoxWindow;
    BasisSelect         : TDXBasisSelector;
    constructor Create(Container: TDXContainer);override;
    procedure SetDefaults;override;
    property Kapital               : Int64 read fKapital write SetKapital;
    property Alphatron              : Int64 read fAlphatron write SetAlphatron;
  end;

implementation

uses
  game_api, gameset_api, string_utils;

{ TNewGame }

procedure TNewGame.CanIncAlien(Sender: TDXComponent;
  var CanChange: boolean);
begin
  if Kapital<15000 then CanChange:=false;
end;

procedure TNewGame.CanDecHers(Sender: TDXComponent;
  var CanChange: boolean);
begin
  if Kapital<10000 then CanChange:=false;
end;

constructor TNewGame.Create(Container: TDXContainer);
begin
  inherited;
  Animation:=paLeftToRight;

  { Initialisieren des Abbrechen Buttons }
  CloseButton:=TDXBitmapButton.Create(Self);
  with CloseButton do
  begin
    SetRect(576,384,110,28);
    SetButtonFont(Font);
    Text:=BCancel;
    FirstColor:=coFirstColor;
    RoundCorners:=rcRight;
    SecondColor:=coSecondColor;
    AccerlateColor:=coAccerlateColor;
    PageIndex:=PageMainMenu;
    Hint:=HCancelSS;
    EscButton:=true;
  end;

  { Initialisieren des Start Buttons }
  StartButton:=TDXBitmapButton.Create(Self);
  with StartButton do
  begin
    SetRect(465,384,110,28);
    SetButtonFont(Font);
    RoundCorners:=rcLeft;
    Text:=BStartGameWA;
    FirstColor:=coFirstColor;
    AccerlateColor:=coAccerlateColor;
    SecondColor:=coSecondColor;
    OnClick:=StartGame;
    Hint:=HStartGame;
  end;
  RegisterHotKey(#13,StartButton);

  { Label zur Beschriftung von PlayerEdit }
  PlayerEditLabel:=TDXGroupCaption.Create(Self);
  with PlayerEditLabel do
  begin
    SetRect(104,70,275,20);
    SetFont(Font);
    RoundCorners:=rcLeftTop;
    Caption:=LPlayerEdit;
  end;

  { Editor Feld zum Eingaben des Spielernamens }
  PlayerEdit:=TDXEdit.Create(Self);
  with PlayerEdit do
  begin
    SetRect(104,91,275,20);
    OnChange:=PlayerTextChange;
    BorderColor:=coFirstColor;
    FocusColor:=coSecondColor;
    SetButtonFont(Font);
    FileName:=true;
    MaxLength:=25;
    Hint:=HNameEdit;
    TopMargin:=3;
    RoundCorners:=rcNone;
  end;

  { Box zum Ausw�hlen eines Spielers }
  PlayersBox:=TDXListBox.Create(Self);
  with PlayersBox do
  begin
    AlwaysChange:=true;
    SetRect(104,112,275,231);
    SetFont(Font);
    Items.Sorted:=true;
    SetButtonFont(ScrollBarFont);
    BorderColor:=coFirstColor;
    FocusColor:=coSecondColor;
    ItemHeight:=21;
    FirstColor:=coScrollBrush;
    SecondColor:=coScrollPen;
    OnChange:=OnPlayerChange;
    OwnerDraw:=true;
    OnDrawItem:=DrawPlayerItem;
    Hint:=HPlayerSelect;
    RoundCorners:=rcLeftBottom;
  end;

  { Spieleinstellungen FrameBox }
  LevelFrameBox:=TDXGroupCaption.Create(Self);
  with LevelFrameBox do
  begin
    SetRect(380,70,306,20);
    SetFont(Font);
    Caption:=LStartSettings;
    BorderColor:=clMaroon;
    RoundCorners:=rcRightTop;
  end;

  { SpielSet-Label }
  with TDXGroupCaption.Create(Self) do
  begin
    SetRect(380,91,137,21);
    SetFont(Font);
    RoundCorners:=rcNone;
    Caption:=LGameSet;
  end;

  { SpielSet-Box }
  GameSetBox:=TDXEdit.Create(Self);
  with GameSetBox do
  begin
    SetRect(518,91,124,21);
    SetButtonFont(Font);
    BorderColor:=coFirstColor;
    TabStop:=false;
    Alignment:=taCenter;
    RoundCorners:=rcNone;
    ReadOnly:=true;
    Hint:=HGameSet;
    TopMargin:=3;
  end;

  { SpielSet - Informationen - Button }
  with TDXBitmapButton.Create(Self) do
  begin
    SetRect(643,91,21,21);
    FirstColor:=coFirstColor;
    SecondColor:=coSecondColor;
    SetButtonFont(Font);
    AccerlateColor:=coAccerlateColor;
    OnClick:=GameSetInfoClick;
    Text:='?';
    Hint:=HInfoGameSet;
    RoundCorners:=rcNone;
  end;

  { SpielSet - Ausw�hlen - Button }
  with TDXBitmapButton.Create(Self) do
  begin
    SetRect(665,91,21,21);
    FirstColor:=coFirstColor;
    SecondColor:=coSecondColor;
    SetButtonFont(Font);
    AccerlateColor:=coAccerlateColor;
    OnClick:=GameSetClick;
    Text:=BSelector;
    Hint:=HChooseGameSet;
    RoundCorners:=rcNone;
  end;

  { Schwierigkeitsgrad-Label }
  with TDXGroupCaption.Create(Self) do
  begin
    SetRect(380,113,137,20);
    SetFont(Font);
    RoundCorners:=rcNone;
    Caption:=LLevelEdit;
  end;

  { Schwierigkeitsgrad Spinedit }
  LevelSpinEdit:=TDXSpinEdit.Create(Self);
  with LevelSpinEdit do
  begin
    SetRect(518,113,168,20);
    FormatString:=EmptyStr;
    SetButtonFont(Font);
    Max:=2;
    Min:=-2;
    Value:=0;
    Interval:=200;
    BorderColor:=coFirstColor;
    RoundCorners:=rcNone;
    FocusColor:=coSecondColor;
    Hint:=HLevelBox;
    GetValueText:=NeedLevelName;
    OnIncrement:=OnLevelChange;
    OnDecrement:=OnLevelChange;
  end;

  { Wohnraum-Label }
  with TDXGroupCaption.Create(Self) do
  begin
    SetRect(380,155,137,20);
    SetFont(Font);
    RoundCorners:=rcNone;
    Caption:=LBWohnRaum;
  end;

  { Wohnraum-Box }
  WohnBox:=TDXEdit.Create(Self);
  with WohnBox do
  begin
    SetRect(518,155,168,20);
    SetButtonFont(Font);
    BorderColor:=coFirstColor;
    TabStop:=false;
    Alignment:=taCenter;
    RoundCorners:=rcNone;
    ReadOnly:=true;
    Hint:=HWohnRaumBel;
    TopMargin:=3;
  end;

  { Kapital Label }
  with TDXGroupCaption.Create(Self) do
  begin
    SetRect(380,134,137,20);
    SetFont(Font);
    RoundCorners:=rcNone;
    Caption:=LStartKaptial;
  end;

  { Kapital Box }
  KapitalBox:=TDXEdit.Create(Self);
  with KapitalBox do
  begin
    SetRect(518,134,168,20);
    SetButtonFont(Font);
    BorderColor:=coFirstColor;
    TabStop:=false;
    Alignment:=taCenter;
    RoundCorners:=rcNone;
    ReadOnly:=true;
    Hint:=HKapital;
    TopMargin:=3;
  end;

  { Alphatron Label }
  with TDXGroupCaption.Create(Self) do
  begin
    SetRect(380,176,137,20);
    SetFont(Font);
    RoundCorners:=rcNone;
    Caption:= LAlphatron;
  end;

  { Alphatron Box }
  AlphatronBox:=TDXSpinEdit.Create(Self);
  with AlphatronBox do
  begin
    SetRect(518,176,168,20);
    SetButtonFont(Font);
    BorderColor:=coFirstColor;
    FocusColor:=coSecondColor;
    Step:=1000;
    min:=0;
    max:=15000;
    RoundCorners:=rcNone;
    Hint:=HAlphatron;
    TopMargin:=3;
    CanIncrement:=CanIncAlphatron;
    OnIncrement:=IncKapital;
    OnDecrement:=DecKapital;
  end;

  { Herstellungszeit-Label }
  with TDXGroupCaption.Create(Self) do
  begin
    SetRect(380,197,137,20);
    SetFont(Font);
    RoundCorners:=rcNone;
    Caption:=LHersEdit;
  end;

  { Herstellungszeit Editor }
  HersEdit:=TDXSpinEdit.Create(Self);
  with HersEdit do
  begin
    SetButtonFont(Font);
    FormatString:=FFloat0Percent;
    SetRect(518,197,168,20);
    {$IFDEF STARTDAYMAX}
    Max:=200;
    Min:=5;
    {$ELSE}
    Max:=110;
    Min:=90;
    {$ENDIF}
    BorderColor:=coFirstColor;
    FocusColor:=coSecondColor;
    Hint:=HProdTimeEdit;
    RoundCorners:=rcNone;
    CanDecrement:=CanDecHers;
    OnIncrement:=IncKapital;
    OnDecrement:=DecKapital;
  end;

  { Forschungszeit-Label }
  with TDXGroupCaption.Create(Self) do
  begin
    SetRect(380,218,137,20);
    SetFont(Font);
    RoundCorners:=rcNone;
    Caption:=LForsEdit;
  end;

  { Forschungszeit Editor }
  ForsEdit:=TDXSpinEdit.Create(Self);
  with ForsEdit do
  begin
    SetButtonFont(Font);
    FormatString:=FFloat0Percent;
    SetRect(518,218,168,20);
    {$IFDEF STARTDAYMAX}
    Max:=200;
    Min:=5;
    {$ELSE}
    Max:=110;
    Min:=90;
    {$ENDIF}
    BorderColor:=coFirstColor;
    FocusColor:=coSecondColor;
    Hint:=HForschTimeEdit;
    RoundCorners:=rcNone;
    CanDecrement:=CanDecHers;
    OnIncrement:=IncKapital;
    OnDecrement:=DecKapital;
  end;

  { Alienangriffszeit-Label }
  with TDXGroupCaption.Create(Self) do
  begin
    SetRect(380,239,137,20);
    SetFont(Font);
    RoundCorners:=rcNone;
    Caption:=LAlienDayEdit;
  end;

  { Alienangriffszeit-Editor }
  AlienEdit:=TDXSpinEdit.Create(Self);
  with AlienEdit do
  begin
    SetButtonFont(Font);
    FormatString:=EmptyStr;
    SetRect(518,239,168,20);
    {$IFDEF STARTDAYMAX}
    Min:=-720;
    Max:=720;
    Step:=25;
    SmallStepButton:=true;
    SmallStep:=1;
    {$ELSE}
    Max:=10;
    Min:=-10;
    {$ENDIF}
    BorderColor:=coFirstColor;
    FocusColor:=coSecondColor;
    Hint:=HAlienDay;
    RoundCorners:=rcNone;
    GetValueText:=NeedDayName;
    CanIncrement:=CanIncAlien;
    OnIncrement:=IncKapital;
    OnDecrement:=DecKapital;
  end;

  { UFO-Angriffszeit-Label }
  with TDXGroupCaption.Create(Self) do
  begin
    SetRect(380,260,137,20);
    SetFont(Font);
    RoundCorners:=rcNone;
    Caption:=LUFODayEdit;
  end;

  { UFO-Angriffszeit-Editor }
  UFOEdit:=TDXSpinEdit.Create(Self);
  with UFOEdit do
  begin
    SetButtonFont(Font);
    FormatString:=EmptyStr;
    SetRect(518,260,168,20);
    {$IFDEF STARTDAYMAX}
    Min:=-720;
    Max:=720;
    Step:=25;
    SmallStepButton:=true;
    SmallStep:=1;
    {$ELSE}
    Max:=10;
    Min:=-10;
    {$ENDIF}
    BorderColor:=coFirstColor;
    FocusColor:=coSecondColor;
    RoundCorners:=rcNone;
    Hint:=HUFODay;
    GetValueText:=NeedDayName;
    CanIncrement:=CanIncAlien;
    OnIncrement:=IncKapital;
    OnDecrement:=DecKapital;
  end;

  { Soldatenanzahl-Label }
  with TDXGroupCaption.Create(Self) do
  begin
    SetRect(380,281,137,20);
    SetFont(Font);
    RoundCorners:=rcNone;
    Caption:=IASoldaten;
  end;

  { Soldatenanzahl-Editor }
  SoldatBox:=TDXSpinEdit.Create(Self);
  with SoldatBox do
  begin
    SetButtonFont(Font);
    SetRect(518,281,168,20);
    Max:=30;
    Min:=0;
    BorderColor:=coFirstColor;
    FocusColor:=coSecondColor;
    RoundCorners:=rcNone;
    Hint:=HSoldatCount;
    CanIncrement:=CanIncSoldat;
    OnIncrement:=IncMenschen;
    OnDecrement:=DecMenschen;
  end;

  { Wissenschaftleranzahl-Label }
  with TDXGroupCaption.Create(Self) do
  begin
    SetRect(380,302,137,20);
    SetFont(Font);
    RoundCorners:=rcNone;
    Caption:=LWissenschaf;
  end;

  { Wissenschaftleranzahl-Editor }
  ForscherBox:=TDXSpinEdit.Create(Self);
  with ForscherBox do
  begin
    SetButtonFont(Font);
    SetRect(518,302,168,20);
    Max:=30;
    Min:=0;
    BorderColor:=coFirstColor;
    FocusColor:=coSecondColor;
    RoundCorners:=rcNone;
    Hint:=HForschCount;
    CanIncrement:=CanIncSoldat;
    OnIncrement:=IncMenschen;
    OnDecrement:=DecMenschen;
  end;

  { Technikeranzahl-Label }
  with TDXGroupCaption.Create(Self) do
  begin
    SetRect(380,323,137,20);
    SetFont(Font);
    RoundCorners:=rcNone;
    Caption:=IATechniker;
  end;

  { Technikeranzahl-Editor }
  TechnikerBox:=TDXSpinEdit.Create(Self);
  with TechnikerBox do
  begin
    SetButtonFont(Font);
    SetRect(518,323,168,20);
    Max:=30;
    Min:=0;
    BorderColor:=coFirstColor;
    FocusColor:=coSecondColor;
    RoundCorners:=rcRightBottom;
    Hint:=HTechnikCount;
    CanIncrement:=CanIncSoldat;
    OnIncrement:=IncMenschen;
    OnDecrement:=DecMenschen;
  end;

  { Listbox Fenster zur Auswahl bei mehreren Objekten }
  ListBoxWindow:=TDXListBoxWindow.Create(Self);
  with ListBoxWindow do
  begin
    SetRect(0,0,300,200);
    Caption:=CChooseGameSet;
    SetButtonFont(Font);
    CaptionColor:=coFontColor;
    AccerlateColor:=coAccerlateColor;
    Font.Size:=coFontSize;
    FirstColor:=$00400000;
    SecondColor:=clBlue;
    BlendColor:=bcDarkNavy;
    Items.Sorted:=true;
  end;

  { Basisselector }
  BasisSelect:=TDXBasisSelector.Create(Self);
  BasisSelect.BasisListe:=nil;

  UnionRect(fAreaRect,KapitalBox.ClientRect,TechnikerBox.ClientRect);
end;

procedure TNewGame.DecKapital(Sender: TObject);
begin
  if (Sender=AlienEdit) or (Sender=UFOEdit) then Kapital:=Kapital+15000
  else if (Sender=ForsEdit) or (Sender=HersEdit) then Kapital:=Kapital-10000
  else if (Sender= AlphatronBox) then Kapital:=Kapital+1000
  else Kapital:=Kapital+5000;
end;

procedure TNewGame.IncKapital(Sender: TObject);
begin
  if (Sender=AlienEdit) or (Sender=UFOEdit) then Kapital:=Kapital-15000
  else if (Sender=ForsEdit) or (Sender=HersEdit) then Kapital:=Kapital+10000
  else if (Sender= AlphatronBox) then Kapital:=Kapital-1000
  else Kapital:=Kapital-5000;
end;

procedure TNewGame.SetDefaults;
var
  Data    : TSearchRec;
begin
  inherited;
  try
    fGameSet:=gameset_api_GetIdentOfSet(FGameFile);
    GameSetBox.Text:=fGameSet.Name;
  except
  end;

  // Spielernamen anhand der Savegame-Dateien ermitteln
  PlayersBox.Items.Clear;
  if FindFirst('save\*.sav',faAnyFile,Data)=0 then
  begin
    repeat
      PlayersBox.Items.Add(ChangeFileExt(Data.Name,''));
    until FindNext(Data)<>0;
  end;
  fMaxWohn:=fGameSet.WohnRaum;
  LevelSpinEdit.Value:=0;
  OnLevelChange(Self);
  PlayerEdit.Text:='';
  ReadGameSets;

end;

procedure TNewGame.SetKapital(const Value: Int64);
begin
  fKapital := Value;
  KapitalBox.Text:=Format(StringConst.FFloat,[fKapital/1]);
end;

procedure TNewGame.CanIncSoldat(Sender: TDXComponent;
  var CanChange: boolean);
begin
  if Kapital<5000 then CanChange:=false;
  if fWohnRaum=fMaxWohn then CanChange:=false;
end;

procedure TNewGame.OnLevelChange(Sender: TObject);
begin
  Container.Lock;
  Kapital:=StartKap[LevelSpinEdit.Value+2];
  Alphatron:=(5-(LevelSpinEdit.Value+2))*2000;
  HersEdit.Value:=100+(LevelSpinEdit.Value shl 1);
  AlienEdit.Value:=-(LevelSpinEdit.Value shl 1);
  UFOEdit.Value:=-(LevelSpinEdit.Value shl 1);
  SoldatBox.Value:=4+((5-(LevelSpinEdit.Value+2))*2);
  TechnikerBox.Value:=4+((5-(LevelSpinEdit.Value+2))*2);
  ForscherBox.Value:=4+((5-(LevelSpinEdit.Value+2))*2);
  fWohnRaum:=(4+((5-(LevelSpinEdit.Value+2))*2))*3;
  while fWohnRaum>fMaxWohn do
  begin
    SoldatBox.Value:=SoldatBox.Value-1;
    dec(fWohnRaum);
    if fWohnRaum>fMaxWohn then
    begin
      TechnikerBox.Value:=TechnikerBox.Value-1;
      dec(fWohnRaum);
      if fWohnRaum>fMaxWohn then
      begin
        ForscherBox.Value:=ForscherBox.Value-1;
        dec(fWohnRaum);
      end;
    end;
  end;
  WohnBox.Text:=Format(FRoom,[fWohnRaum/1,fMaxWohn/1]);
  ForsEdit.Value:=100+(LevelSpinEdit.Value shl 1);
  Container.UnLock;
  Container.RedrawArea(fAreaRect,Container.Surface);
end;

procedure TNewGame.SetAlphatron(const Value: Int64);
begin
  fAlphatron := Value;
  AlphatronBox.Value:= Value;
end;

procedure TNewGame.OnPlayerChange(Sender: TObject);
begin
  if PlayersBox.Items.Count>0 then
  begin
    if PlayerEdit.Text=PlayersBox.Items[PlayersBox.ItemIndex] then exit;
    PlayerEdit.Text:=PlayersBox.Items[PlayersBox.ItemIndex];
  end;
end;

procedure TNewGame.DrawPlayerItem(Sender: TDXComponent;
  Surface: TDirectDrawSurface;const Mem: TDDSurfaceDesc; Rect: TRect; Index: Integer;
  Selected: boolean);
var
  Font    : TDirectFont;
begin
  if UpperCase(PlayersBox.Items[Index])=UpperCase(PlayerEdit.Text) then
  begin
    PlayersBox.DrawSelection(Surface,Mem,Rect,bcMaroon);
    Font:=WhiteStdFont;
  end
  else Font:=YellowStdFont;
  Font.Draw(Surface,Rect.Left+6,Rect.Top+4,PlayersBox.Items[Index]);
end;

procedure TNewGame.PlayerTextChange(Sender: TObject);
begin
  Container.IncLock;
  StartButton.Enabled:=PlayerEdit.Text<>EmptyStr;
  Container.DecLock;
  Container.RedrawControl(PlayersBox,Container.Surface);
end;

procedure TNewGame.StartGame(Sender: TObject);
var
  Data     : TNewGameRec;
  Archiv   : TArchivFile;
begin
  if PlayerEdit.Text='' then
  begin
    game_api_MessageBox(SNoPlayer,CInformation);
    exit;
  end;

  if PlayersBox.Items.IndexOf(PlayerEdit.Text)=-1 then
  begin
    if not game_api_Question(Format(MNewPlayer,[PlayerEdit.Text]),CQuestion) then
      exit;
    Container.Lock;
    Archiv:=TArchivFile.Create;
    Archiv.CreateArchiv('save\'+PlayerEdit.Text+'.sav');
    Archiv.Free;
    Container.UnLock;
  end;
  Data.Name:=PlayerEdit.Text;
  Data.Kapital:=fKapital;
  Data.StartAlphatron:= AlphatronBox.Value;
  Data.Forsch:=ForsEdit.Value;
  Data.Prod:=HersEdit.Value;
  Data.Alien:=AlienEdit.Value;
  Data.UFO:=UFOEdit.Value;
  Data.Soldat:=SoldatBox.Value;
  Data.GameFile:=fGameSet;
  Data.Forscher:=ForscherBox.Value;
  Data.Techniker:=TechnikerBox.Value;
  Data.Difficult:=TGameDifficult(LevelSpinEdit.Value+2);

  // Sprache des Spielsatzes pr�fen
  Data.Language:=GetGameSetLanguage;

  if Data.Language='' then
    exit;

  // Basisposition w�hlen
  BasisSelect.SelectPos(Data.Basis,true);

  try
    Container.Lock;
    SaveGame.NewGame(Data);

    game_api_SetCanSaveGame(true);

    Container.UnLock;
  except
    on E: ENotEnoughRoom do
    begin
      Container.UnLockAll;
      game_api_MessageBox(ENoRoomAtStart,CError);
      exit;
    end;
    on E: Exception do
    begin
      Container.UnLockAll;
      game_api_MessageBox(E.Message,CError);
      exit;
    end;
  end;
  ChangePage(PageGameMenu);
end;

procedure TNewGame.NeedLevelName(Value: Integer; var Text: String);
begin
  Text:='';
  case Value of
    -2 : Text:=SVeryEasy;
    -1 : Text:=SEasy;
     0 : Text:=SNormal;
     1 : Text:=SDifficult;
     2 : Text:=SVeryDifficult;
  end;
end;

procedure TNewGame.DecMenschen(Sender: TObject);
begin
  Container.IncLock;
  dec(fWohnRaum);
  WohnBox.Text:=Format(FRoom,[fWohnRaum/1,fMaxWohn/1]);
  Container.DecLock;
  Kapital:=Kapital+5000;
end;

procedure TNewGame.IncMenschen(Sender: TObject);
begin
  Container.IncLock;
  inc(fWohnRaum);
  WohnBox.Text:=Format(FRoom,[fWohnRaum/1,fMaxWohn/1]);
  Container.DecLock;
  Kapital:=Kapital-5000;
end;

procedure TNewGame.CanIncAlphatron(Sender: TDXComponent;
  var CanChange: boolean);
begin
  if Kapital<1000 then CanChange:=false;
end;

procedure TNewGame.NeedDayName(Value: Integer; var Text: String);
begin
  Text:=ZahlString(FDay,FDays,Value);
end;

procedure TNewGame.GameSetClick(Sender: TObject);
var
  Dummy   : Integer;
  Result  : Integer;
begin
  ListBoxWindow.Items.Clear;
  for Dummy:=0 to length(Files)-1 do
  begin
    ListBoxWindow.Items.AddObject(Files[Dummy].Name,Pointer(Dummy));
  end;
  Result:=ListBoxWindow.Show;
  if Result<>-1 then
  begin
    Result:=Integer(ListBoxWindow.Items.Objects[Result]);
    fGameSet:=Files[Result];
    Container.IncLock;
    GameSetBox.Text:=fGameSet.Name;
    fMaxWohn:=fGameSet.WohnRaum;
    OnLevelChange(Self);
    Container.DecLock;
    Container.DoFlip;
  end;
end;

procedure TNewGame.ReadGameSets;
var
  Data    : TSearchRec;
  Stat    : Integer;
  Ident   : TSetIdentifier;
  Index   : Integer;
begin
  Stat:=FindFirst('data\GameSets\*.pak',faAnyFile,Data);
  Index:=0;
  while Stat=0 do
  begin
    try
      Ident:=gameset_api_GetIdentOfSet('data\GameSets\'+Data.Name);
      SetLength(Files,Index+1);
      Files[Index]:=Ident;
      inc(Index);
    except
    end;
    Stat:=FindNext(Data);
  end;
  FindClose(Data);
end;

procedure TNewGame.GameSetInfoClick(Sender: TObject);
begin
  game_api_MessageBox(Format(ST0309270006,[fGameset.Autor,fGameSet.Organ,Remove0A(fGameSet.Desc)]),fGameSet.Name);
end;

function TNewGame.GetGameSetLanguage: String;
var
  Archiv    : TArchivFile;
  Languages : TStringArray;
  Count     : Integer;
  Dummy     : Integer;
  Item      : Integer;
begin
  // Sprachen im Spielsatz laden
  Archiv:=TArchivFile.Create;
  Archiv.OpenArchiv(fGameSet.FileName,true);

  gameset_api_LoadLanguages(Archiv,Languages);

  Archiv.Free;

  result:=LowerCase(Defines.Language);

  // Pr�fen, ob die eingestellte Sprache im Spielsatz existiert
  if length(Languages)>1 then
  begin
    if string_utils_inArray(result,Languages) then
      exit;
  end;

  // Auswahlbox f�r Sprache anzeigen
  ListBoxWindow.Items.Clear;
  ListBoxWindow.Caption:=ST0402290001;

  // Sprachen hinzuf�gen
  for Dummy:=0 to high(Languages) do
    ListBoxWindow.Items.AddObject(Languages[Dummy],Pointer(Dummy));

  Item:=ListBoxWindow.Show;

  ListBoxWindow.Caption:=CChooseGameSet;
  if Item<>-1 then
    result:=Languages[Item];
end;

end.
