{********************************************************************************
*										*
* Copyright (C) 2004  X-Force Team - jim_raynor@web.de				*
*										*
* This file is part of X-Force: Fight For Destiny (X-Force).			*
*										*
* X-Force is free software; you can redistribute it and/or modify		*
* it under the terms of the GNU General Public License as published by		*
* the Free Software Foundation; either version 2 of the License, or		*
* (at your option) any later version.						*
*										*
* X-Force is distributed in the hope that it will be useful,			*
* but WITHOUT ANY WARRANTY; without even the implied warranty of		*
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the			*
* GNU General Public License for more details.					*
*										*
* You should have received a copy of the GNU General Public License		*
* along with X-Force; if not, write to the Free Software			*
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA	*
*										*
*********************************************************************************
*										*
* gameset_api bildet eine Schnittstelle um auf Daten innerhalb eines		*
* zuzugreifen. Der Spielsatz wird dabei als ge�ffnetes Archiv �bergeben.	*
*										*
********************************************************************************}

{$I ../Settings.inc}

unit gameset_api;

interface

uses
  Loader, XForce_types, ExtRecord, ArchivFile, Classes, Koding, Windows, string_utils,
  record_utils;

// Gibt die Sprache an, in der Texte in den Load... Funktionen geladen werden
// sollen
procedure gameset_api_SetLanguage(Language: String);

// Ermittelt Informationen zum Spielsatz aus dem Header
procedure gameset_api_ReadHeader(Archiv: TArchivFile; var Header: TSetHeader);

// Ermittelt die wichtisten Informationen zu einem Spielsatz
function gameset_api_GetIdentOfSet(FileName: String): TSetIdentifier;

// L�dt die verf�gbaren Sprachen im Spielsatz
procedure gameset_api_LoadLanguages(Archiv: TArchivFile; var Languages: TStringArray);

// L�dt alle m�glichen Forschungen und Startausr�stungen aus dem Spielsatz
procedure gameset_api_LoadProjects(Archiv: TArchivFile; var Projekts: TRecordArray);

// L�dt alle Aliens aus dem Spielsatz
procedure gameset_api_LoadAliens(Archiv: TArchivFile; var Aliens: TRecordArray);

// L�dt alle UFOs aus dem Spielsatz
procedure gameset_api_LoadUFos(Archiv: TArchivFile; var UFOs: TRecordArray);

// L�dt alle Skripte aus dem Spielsatz
procedure gameset_api_LoadSkripts(Archiv: TArchivFile; var Skripts: TRecordArray);

implementation

uses Defines, sysutils, ObjektRecords;

var
  g_Language: String;

procedure gameset_api_SetLanguage(Language: String);
begin
  g_Language:=Language;
  Loader.ReadingLanguage:=Language;
end;

procedure gameset_api_ReadHeader(Archiv: TArchivFile; var Header: TSetHeader);
var
  m          : TMemoryStream;
  Version    : Integer;
  Text       : String[30];
begin
  m:=TMemoryStream.Create;
  if not Archiv.ExistRessource('Header') then
    raise Exception.Create('Ung�ltiger Spielsatz: '+Archiv.ArchivFile);
  Archiv.OpenRessource('Header');
  Decode(Archiv.Stream,m);
  m.Read(Version,4);
  if (Version<>HeadVersion) then
  begin
    m.Position:=0;
    m.Read(Text,SizeOf(Text));
    with Header do
    begin
      Name:=Text;
      Autor:='';
      Organisation:='';
      Beschreibung:='';
      CanImport:=true;
      ID:=Cardinal(round(random*GetTickCount));
    end;
  end
  else
  begin
    with Header do
    begin
      Name:=ReadString(m);
      Autor:=ReadString(m);
      Organisation:=ReadString(m);
      Beschreibung:=ReadString(m);
      m.Read(CanImport,SizeOf(CanImport));
      m.Read(Version,4);
      m.Read(ID,4);
      if ID=0 then ID:=Cardinal(round(random*GetTickCount));
    end;
  end;
  m.Free;
end;

procedure gameset_api_LoadLanguages(Archiv: TArchivFile; var Languages: TStringArray);
var
  Dummy   : Integer;
  Buffer  : Integer;
begin
  if Archiv.ExistRessource('Languages') then
  begin
    Archiv.OpenRessource('Languages');
    Archiv.Stream.Read(Buffer,SizeOf(Buffer));
    SetLength(Languages,Buffer);
    for Dummy:=0 to Buffer-1 do
      Languages[Dummy]:=ReadString(Archiv.Stream);
  end
  else
    SetLength(Languages,0);
end;

procedure gameset_api_LoadProjects(Archiv: TArchivFile; var Projekts: TRecordArray);
var
  Loader: TProjektLoader;
  Dummy : Integer;
begin
  record_utils_ClearArray(Projekts);

  Loader:=TProjektLoader.Create;

  try
    Archiv.OpenRessource(RNForschDat);
    Loader.Stream:=Archiv.Stream;
    SetLength(Projekts,Loader.Count);
    Dummy:=0;

    while (Loader.GetNextObject(Projekts[Dummy])) do
      inc(Dummy);
  finally
    Loader.Free;
  end;
end;

procedure gameset_api_LoadAliens(Archiv: TArchivFile; var Aliens: TRecordArray);
var
  Loader: TAlienLoader;
  Dummy : Integer;
begin
  record_utils_ClearArray(Aliens);

  Loader:=TAlienLoader.Create;

  Archiv.OpenRessource(RNAlien);
  Loader.Stream:=Archiv.Stream;
  SetLength(Aliens,Loader.Count);
  Dummy:=0;

  while (Loader.GetNextObject(Aliens[Dummy])) do
    inc(Dummy);

  Loader.Free;
end;

procedure gameset_api_LoadUFos(Archiv: TArchivFile; var UFOs: TRecordArray);
var
  Loader: TUFOModelLoader;
  Dummy : Integer;
begin
  record_utils_ClearArray(UFOs);

  Loader:=TUFOModelLoader.Create;

  Archiv.OpenRessource(RNUFOs);
  Loader.Stream:=Archiv.Stream;
  SetLength(UFOs,Loader.Count);
  Dummy:=0;

  while (Loader.GetNextObject(UFOs[Dummy])) do
    inc(Dummy);

  Loader.Free;
end;

procedure gameset_api_LoadSkripts(Archiv: TArchivFile; var Skripts: TRecordArray);
var
  Dummy   : Integer;
  Header  : TEntryHeader;
begin
  record_utils_ClearArray(Skripts);

  if not Archiv.ExistRessource(RNSkripte) then
    exit;

  Archiv.OpenRessource(RNSkripte);

  Archiv.Stream.Read(Header,SizeOf(Header));
  Assert(Header.Version=$113397FD);

  SetLength(Skripts,Header.Entrys);
  for Dummy:=0 to Header.Entrys-1 do
  begin
    Skripts[Dummy]:=TExtRecord.Create(RecordMissionSkript);
    Skripts[Dummy].LoadFromStream(Archiv.Stream);
  end;
end;

function gameset_api_GetIdentOfSet(FileName: String): TSetIdentifier;
var
  Archiv  : TArchivFile;
  Projects: TRecordArray;
  Head    : TSetHeader;
  Dummy   : Integer;
begin
  result.FileName:=FileName;
  result.WohnRaum:=0;

  Archiv:=TArchivFile.Create;
  try
    Archiv.OpenArchiv(FileName,true);

    gameset_api_ReadHeader(Archiv,Head);
    result.Name:=Head.Name;
    result.Autor:=Head.Autor;
    result.Organ:=Head.Organisation;
    result.Desc:=Head.Beschreibung;
    result.ID:=Head.ID;

    gameset_api_LoadProjects(Archiv,Projects);

    for Dummy:=0 to high(Projects) do
    begin
      if Projects[Dummy].GetBoolean('Start') and (Projects[Dummy].GetInteger('Ver')=0) and (Projects[Dummy].GetInteger('TypeID')=Integer(ptEinrichtung)) then
      begin
        inc(result.WohnRaum,Projects[Dummy].GetInteger('Quartiere')*Projects[Dummy].GetInteger('Anzahl'));
      end;
    end;
  finally
    record_utils_ClearArray(Projects);

    Archiv.Free;
  end;
end;

end.
