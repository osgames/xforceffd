{********************************************************************************
*										*
* Copyright (C) 2004  X-Force Team - jim_raynor@web.de				*
*										*
* This file is part of X-Force: Fight For Destiny (X-Force).			*
*										*
* X-Force is free software; you can redistribute it and/or modify		*
* it under the terms of the GNU General Public License as published by		*
* the Free Software Foundation; either version 2 of the License, or		*
* (at your option) any later version.						*
*										*
* X-Force is distributed in the hope that it will be useful,			*
* but WITHOUT ANY WARRANTY; without even the implied warranty of		*
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the			*
* GNU General Public License for more details.					*
*										*
* You should have received a copy of the GNU General Public License		*
* along with X-Force; if not, write to the Free Software			*
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA	*
*										*
*********************************************************************************
*										*
* town_api bildet die Schnittstelle zu einer Country-Liste (TOrganisationList), *
* die L�nder und St�dte verwaltet. 	                                        *
*										*
* WICHTIG!! Bevor die Funktionen genutzt werden k�nnen muss �ber 		*
*           town_api_init die Country-Liste gesetzt werden. Sollte durch 	*
*           savegame_api_init geschehen sein.					*
*										*
*********************************************************************************
*										*
* CVS-Informationen                                                             *
* Autor           $Author$
* CheckInDate     $Date$
* Tag/Branch      $Name$
* Revision        $Revision$
*										*
********************************************************************************}

{$I ../Settings.inc}

unit town_api;

interface

uses OrganisationList, XForce_types;

procedure town_api_init(CountryListe: TOrganisationList);

// Gibt die Anzahl der St�dte zur�ck
function town_api_GetTownCount: Integer;

// Gibt die Stadt mit der ID zur�ck. Ist Check aktiviert wird eine Assertion
// ausgel�st, wenn die ID ung�ltig ist
function town_api_GetTown(ID: Cardinal;Check: Boolean = false): TTown;overload;

function town_api_GetTown(Index: Integer;Check: Boolean = false): TTown;overload;

implementation

var
  g_CountryList: TOrganisationList;

procedure town_api_init(CountryListe: TOrganisationList);
begin
  g_CountryList:=CountryListe;
end;

function town_api_GetTownCount: Integer;
begin
  result:=g_CountryList.TownCount;
end;

function town_api_GetTown(ID: Cardinal;Check: Boolean = false): TTown;
begin
  result:=g_CountryList.TownOfID(ID);
  if Check then
    Assert(result<>nil);
end;

function town_api_GetTown(Index: Integer;Check: Boolean = false): TTown;overload;
begin
  result:=g_CountryList.Towns[Index];
  if Check then
    Assert(result<>nil);
end;

end.
