{********************************************************************************
*										*
* Copyright (C) 2004  X-Force Team - jim_raynor@web.de				*
*										*
* This file is part of X-Force: Fight For Destiny (X-Force).			*
*										*
* X-Force is free software; you can redistribute it and/or modify		*
* it under the terms of the GNU General Public License as published by		*
* the Free Software Foundation; either version 2 of the License, or		*
* (at your option) any later version.						*
*										*
* X-Force is distributed in the hope that it will be useful,			*
* but WITHOUT ANY WARRANTY; without even the implied warranty of		*
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the			*
* GNU General Public License for more details.					*
*										*
* You should have received a copy of the GNU General Public License		*
* along with X-Force; if not, write to the Free Software			*
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA	*
*										*
*********************************************************************************
*										*
* basis_api bildet die Schnittstelle zu einer Basisliste, die bis zu 8 Basen	*
* verwaltet									*
*										*
* WICHTIG!! Bevor die Funktionen genutzt werden k�nnen muss �ber 		*
*           basis_api_init die Basisliste gesetzt werden. Sollte durch 		*
*           savegame_api_init geschehen sein.					*
*										*
********************************************************************************}

{$I ../Settings.inc}

unit basis_api;

interface

uses BasisListe, UFOList, DXDraws, Classes, SysUtils,XForce_Types;

procedure basis_api_init(BasisListe: TBasisListe);

function basis_api_GetBasisCount: Integer;

function basis_api_GetBasisFromID(BasisID: Cardinal): TBasis;overload;
function basis_api_GetBasisFromIndex(Index: Integer): TBasis;overload;
//function basis_api_GetBasisIndex(BasisID: Cardinal): Integer;

function basis_api_GetMainBasis: TBasis;
function basis_api_GetSelectedBasis: TBasis;

procedure basis_api_SetSelectedBasis(Basis: TBasis);

function basis_api_GetEinrichtungIndex(ID: Cardinal): Integer;
function basis_api_GetEinrichtungCount: Integer;
function basis_api_GetEinrichtung(Index: Integer): TEinrichtung;

// Wohnraum
procedure basis_api_FreeWohnRaum(BasisID: Cardinal = 0);
function  basis_api_NeedWohnRaum(BasisID: Cardinal = 0; RaiseException: Boolean = true): Boolean;
function  basis_api_BelegterWohnRaum(BasisID: Cardinal = 0): Integer;
function  basis_api_FreierWohnRaum(BasisID: Cardinal = 0): Integer;
function  basis_api_WohnRaum(BasisID: Cardinal = 0): Integer;

// Laborraum
procedure basis_api_FreeLaborRaum(BasisID: Cardinal = 0);
function  basis_api_NeedLaborRaum(BasisID: Cardinal = 0; RaiseException: Boolean = true): Boolean;
function  basis_api_BelegterLaborRaum(BasisID: Cardinal = 0): Integer;
function  basis_api_FreierLaborRaum(BasisID: Cardinal = 0): Integer;
function  basis_api_LaborRaum(BasisID: Cardinal = 0): Integer;

// Werkraum
procedure basis_api_FreeWerkRaum(BasisID: Cardinal = 0);
function  basis_api_NeedWerkRaum(BasisID: Cardinal = 0; RaiseException: Boolean = true): Boolean;
function  basis_api_BelegterWerkRaum(BasisID: Cardinal = 0): Integer;
function  basis_api_FreierWerkRaum(BasisID: Cardinal = 0): Integer;
function  basis_api_WerkRaum(BasisID: Cardinal = 0): Integer;

// Lagerraum
procedure basis_api_FreeLagerRaum(Room: double;BasisID: Cardinal = 0);
function  basis_api_NeedLagerRaum(Room: double;BasisID: Cardinal = 0; RaiseException: Boolean = true): Boolean;
function  basis_api_BelegterLagerRaum(BasisID: Cardinal = 0): double;
function  basis_api_FreierLagerRaum(BasisID: Cardinal = 0): double;
function  basis_api_LagerRaum(BasisID: Cardinal = 0): double;

// Werkraum
procedure basis_api_FreeHangarRaum(BasisID: Cardinal = 0);
function  basis_api_NeedHangarRaum(BasisID: Cardinal = 0; RaiseException: Boolean = true): Boolean;
function  basis_api_BelegterHangarRaum(BasisID: Cardinal = 0): Integer;
function  basis_api_FreierHangarRaum(BasisID: Cardinal = 0): Integer;
function  basis_api_HangarRaum(BasisID: Cardinal = 0): Integer;

// Gibt die W�chentlichen Kosten f�r alle Baseneinrichtungen zur�ck
function  basis_api_GetWeeklyKost: Integer;

// Gibt die max. Alphatronf�rderung aller Basen innerhalb einer Stunde zur�ck
function  basis_api_GetAlphatronperHour: double;

// Basisabwehr
// Aufruf am Beginn einer Runde
procedure basis_api_KAMPFInitShootTable(Minuten: Integer);

// Angriff, wenn UFO in der Reichweite der Basis ist
function  basis_api_KAMPFShootToUFO(UFORef: TUFO; Minute: Integer): boolean;

// Zeichnet ein BasisSymbol
procedure basis_api_DrawIcon(Surface: TDirectDrawSurface; x, y: Integer);

// Handler registrieren, um auf Ereignisse Reagieren zu k�nnen
  // Neue Basis gebaut
procedure basis_api_RegisterNewBaseHandler(Handler: TNotifyEvent);

  // Basis zerst�rt
procedure basis_api_RegisterDestroyBaseHandler(Handler: TNotifyEvent);

  // Ausgew�hlte Basis wurde ge�ndert
procedure basis_api_RegisterChangeBaseHandler(Handler: TNotifyEvent);

procedure basis_api_TransferAlphatron(FromBase, ToBase: TBasis; Alphatron: Integer);

implementation

uses
  StringConst;

var
  g_BasisList: TBasisListe;

procedure basis_api_init(BasisListe: TBasisListe);
begin
  g_BasisList:=BasisListe;
end;

function basis_api_GetBasisFromID(BasisID: Cardinal): TBasis;
begin
  if BasisID=0 then            
    result:=g_BasisList.Selected
  else
    result:=g_BasisList.GetBasisFromID(BasisID);
  Assert(result<>nil, Format('Ung�ltige BasisID %d',[BasisID]));
end;

function basis_api_GetBasisFromIndex(Index: Integer): TBasis;overload;
begin
  if (Index>=0) and (Index<basis_api_GetBasisCount) then
    result:=g_BasisList.Basen[Index]
  else
    result:=nil;
end;

function basis_api_GetBasisIndex(BasisID: Cardinal): Integer;
begin
  result:=g_BasisList.GetBasisIndex(BasisID);

  Assert(result<>-1);
end;

function basis_api_GetMainBasis: TBasis;
begin
  result:=g_BasisList.HauptBasis;
  Assert(result<>nil);
end;

function basis_api_GetSelectedBasis: TBasis;
begin
  result:=g_BasisList.Selected;
  Assert(result<>nil);
end;

function basis_api_GetBasisCount: Integer;
begin
  result:=g_BasisList.Count;
end;

function basis_api_GetEinrichtungIndex(ID: Cardinal): Integer;
begin
  result:=g_BasisList.IndexOfID(ID);
  Assert(result<>-1);
end;

function basis_api_GetEinrichtungCount: Integer;
begin
  result:=g_BasisList.Einrichtungen;
end;

function basis_api_GetEinrichtung(Index: Integer): TEinrichtung;
begin
  Assert((Index>=0) and (Index<g_BasisList.Einrichtungen));
  result:=g_BasisList.Einrichtung[Index];
end;

// Wohnraum
function basis_api_NeedWohnRaum(BasisID: Cardinal = 0; RaiseException: Boolean = true): Boolean;
begin
  result:=basis_api_GetBasisFromID(BasisID).NeedWohnRoom;
  if RaiseException and (not result) then
    raise ENotEnoughRoom.Create(ENoRoom);
end;

procedure basis_api_FreeWohnRaum(BasisID: Cardinal);
begin
  basis_api_GetBasisFromID(BasisID).FreeWohnRoom;
end;

function  basis_api_BelegterWohnRaum(BasisID: Cardinal = 0): Integer;
begin
  result:=basis_api_GetBasisFromID(BasisID).WohnBelegt;
end;

function  basis_api_FreierWohnRaum(BasisID: Cardinal = 0): Integer;
begin
  result:=basis_api_GetBasisFromID(BasisID).WohnRaum-basis_api_GetBasisFromID(BasisID).WohnBelegt;
end;

function  basis_api_WohnRaum(BasisID: Cardinal = 0): Integer;
begin
  result:=basis_api_GetBasisFromID(BasisID).WohnRaum;
end;

// Laborraum
procedure basis_api_FreeLaborRaum(BasisID: Cardinal = 0);
begin
  basis_api_GetBasisFromID(BasisID).FreeLaborRoom;
end;

function basis_api_NeedLaborRaum(BasisID: Cardinal = 0; RaiseException: Boolean = true): Boolean;
begin
  result:=basis_api_GetBasisFromID(BasisID).NeedLaborRoom;
  if RaiseException and (not result) then
    raise ENotEnoughRoom.Create(ENoLabRoom);
end;

function  basis_api_BelegterLaborRaum(BasisID: Cardinal = 0): Integer;
begin
  result:=basis_api_GetBasisFromID(BasisID).LaborBelegt;
end;

function  basis_api_FreierLaborRaum(BasisID: Cardinal = 0): Integer;
begin
  result:=basis_api_GetBasisFromID(BasisID).LaborRaum-basis_api_GetBasisFromID(BasisID).LaborBelegt;
end;

function  basis_api_LaborRaum(BasisID: Cardinal = 0): Integer;
begin
  result:=basis_api_GetBasisFromID(BasisID).LaborRaum;
end;

// Werkraum
procedure basis_api_FreeWerkRaum(BasisID: Cardinal = 0);
begin
  basis_api_GetBasisFromID(BasisID).FreeWerkRoom;
end;

function basis_api_NeedWerkRaum(BasisID: Cardinal = 0; RaiseException: Boolean = true): Boolean;
begin
  result:=basis_api_GetBasisFromID(BasisID).NeedWerkRoom;
  if RaiseException and (not result) then
    raise ENotEnoughRoom.Create(ENoWerRoom);
end;

function  basis_api_BelegterWerkRaum(BasisID: Cardinal = 0): Integer;
begin
  result:=basis_api_GetBasisFromID(BasisID).WerkBelegt;
end;

function  basis_api_FreierWerkRaum(BasisID: Cardinal = 0): Integer;
begin
  result:=basis_api_GetBasisFromID(BasisID).WerkRaum-basis_api_GetBasisFromID(BasisID).WerkBelegt;
end;

function  basis_api_WerkRaum(BasisID: Cardinal = 0): Integer;
begin
  result:=basis_api_GetBasisFromID(BasisID).WerkRaum;
end;

// Lagerraum
procedure basis_api_FreeLagerRaum(Room: double;BasisID: Cardinal = 0);
begin
  basis_api_GetBasisFromID(BasisID).FreeLagerRoom(Room);
end;

function basis_api_NeedLagerRaum(Room: double;BasisID: Cardinal = 0; RaiseException: Boolean = true): Boolean;
var
  Basis: TBasis;
begin
  Basis:=basis_api_GetBasisFromID(BasisID);
  result:=Basis.NeedLagerRoom(Room);
  if RaiseException and (not result) then
    raise ENotEnoughRoom.CreateFmt(ENoLagerRoom,[Basis.Name]);
end;

function  basis_api_BelegterLagerRaum(BasisID: Cardinal = 0): double;
begin
  result:=basis_api_GetBasisFromID(BasisID).LagerBelegt;
end;

function  basis_api_FreierLagerRaum(BasisID: Cardinal = 0): double;
begin
  result:=basis_api_GetBasisFromID(BasisID).LagerRaum-basis_api_GetBasisFromID(BasisID).LagerBelegt;
end;

function  basis_api_LagerRaum(BasisID: Cardinal = 0): double;
begin
  result:=basis_api_GetBasisFromID(BasisID).LagerRaum;
end;

// Hangar
procedure basis_api_FreeHangarRaum(BasisID: Cardinal = 0);
begin
  basis_api_GetBasisFromID(BasisID).FreeHangar;
end;

function basis_api_NeedHangarRaum(BasisID: Cardinal = 0; RaiseException: Boolean = true): Boolean;
begin
  result:=basis_api_GetBasisFromID(BasisID).NeedHangar;
  if RaiseException and (not result) then
    raise ENotEnoughRoom.Create(ENoHangar);
end;

function  basis_api_BelegterHangarRaum(BasisID: Cardinal = 0): Integer;
begin
  result:=basis_api_GetBasisFromID(BasisID).HangerBelegt;
end;

function  basis_api_FreierHangarRaum(BasisID: Cardinal = 0): Integer;
begin
  result:=basis_api_GetBasisFromID(BasisID).HangerRaum-basis_api_GetBasisFromID(BasisID).HangerBelegt;
end;

function  basis_api_HangarRaum(BasisID: Cardinal = 0): Integer;
begin
  result:=basis_api_GetBasisFromID(BasisID).HangerRaum;
end;

function  basis_api_GetWeeklyKost: Integer;
begin
  result:=g_BasisList.GetWeeklyKost;
end;

procedure basis_api_KAMPFInitShootTable(Minuten: Integer);
begin
  g_BasisList.KAMPFInitShootTable(Minuten);
end;

function basis_api_KAMPFShootToUFO(UFORef: TUFO; Minute: Integer): boolean;
begin
  result:=g_BasisList.KAMPFShootToUFO(UFORef,Minute);
end;

procedure basis_api_SetSelectedBasis(Basis: TBasis);
begin
  g_BasisList.Selected:=Basis;
end;

procedure basis_api_DrawIcon(Surface: TDirectDrawSurface; x, y: Integer);
begin
  g_BasisList.DrawIcon(Surface,X,Y);
end;

function  basis_api_GetAlphatronperHour: double;
begin
  result:=g_BasisList.AlphatronPerHour;
end;

procedure basis_api_RegisterNewBaseHandler(Handler: TNotifyEvent);
begin
  g_BasisList.NotifyList.RegisterEvent(EVENT_BASISLISTBUILDNEW,Handler);
end;

procedure basis_api_RegisterDestroyBaseHandler(Handler: TNotifyEvent);
begin
  g_BasisList.NotifyList.RegisterEvent(EVENT_BASISLISTDESTROY,Handler);
end;

procedure basis_api_RegisterChangeBaseHandler(Handler: TNotifyEvent);
begin
  g_BasisList.NotifyList.RegisterEvent(EVENT_BASISLISTCHANGE,Handler);
end;

procedure basis_api_TransferAlphatron(FromBase, ToBase: TBasis; Alphatron: Integer);
begin
 FromBase.TransferAlphatron(Alphatron,ToBase);
end;

end.
