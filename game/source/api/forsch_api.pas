{********************************************************************************
*										*
* Copyright (C) 2004  X-Force Team - jim_raynor@web.de				*
*										*
* This file is part of X-Force: Fight For Destiny (X-Force).			*
*										*
* X-Force is free software; you can redistribute it and/or modify		*
* it under the terms of the GNU General Public License as published by		*
* the Free Software Foundation; either version 2 of the License, or		*
* (at your option) any later version.						*
*										*
* X-Force is distributed in the hope that it will be useful,			*
* but WITHOUT ANY WARRANTY; without even the implied warranty of		*
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the			*
* GNU General Public License for more details.					*
*										*
* You should have received a copy of the GNU General Public License		*
* along with X-Force; if not, write to the Free Software			*
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA	*
*										*
*********************************************************************************
*										*
* forsch_api bildet die Schnittstelle zu einer Forschungsliste, die verf�gbare	*
* Forschungsprojekte verwaltet							*
*										*
* WICHTIG!! Bevor die Funktionen genutzt werden k�nnen muss �ber 		*
*           forsch_api_init die Personenliste gesetzt werden. Sollte durch      *
*           savegame_api_init geschehen sein.					*
*										*
********************************************************************************}

{$I ../Settings.inc}

unit forsch_api;

interface

uses
  ForschList, XForce_types, Classes, StringConst;

procedure forsch_api_init(ForschListe: TForschList);

// Gibt die Anzahl der Wissenschaftler in allen Basen zur�ck
function forsch_api_GetForscherCount: Integer; overload;

// Gibt die Anzahl der Wissenschaftler der Basis zur�ck
function forsch_api_GetForscherCount(BasisID: Cardinal): Integer; overload;

// Gibt das w�chentliche Gehalt f�r alle Wissenschaftler zur�ck
function forsch_api_GetWeeklySold: Integer; overload;

// Gibt das w�chentliche Gehalt f�r alle Wissenschaftler der Basis zur�ck
function forsch_api_GetWeeklySold(BasisID: Cardinal): Integer; overload;

// Gibt die gesamten F�higkeiten von allen Wissenschaftlern zur�ck
function forsch_api_GetAllStrength: Integer; overload;

// Gibt die gesamten F�higkeiten von allen Wissenschaftlern der Basis zur�ck
function forsch_api_GetAllStrength(BasisID: Cardinal): Integer; overload;

// Ermittelt die praktische Forschungszeit f�r diesen Spielstand unter
// ber�cksichtigung des Parameters Forschungszeit
function forsch_api_calculateForschTime(Hours: Integer): Integer;

// Schaltet die Forschung f�r ein Forschungsprojekt frei, das im Spielsatz
// definiert ist. In der Regel wird es genutzt um Alienausr�stungen zu erforschen
procedure forsch_api_AddAlienItemResearch(ItemID: Cardinal);

// Pr�ft, ob ein Forschungsprojekt bereits beim start des Spiels
// verwendet werden kann
function forsch_api_isStartProject(const Projekt: TForschProject): Boolean;

// Registriert einen Handler, der Aufgerufen wird, wenn die Forschungsliste
// ein neues Objekt (z.B. Bei Beendeter Forschung) verf�gbar macht)
// Sender ist dabei ein Zeiger auf den TForschProject record
procedure forsch_api_RegisterProjektEndHandler(Handler: TNotifyEvent);

// Registriert einen Handler, der Aufgerufen wird, wenn das Upgrade einer
// Ausr�stung abgeschlossen ist
// Sender ist dabei die ID der Ausr�stung
procedure forsch_api_RegisterUpgradeEndHandler(Handler: TNotifyEvent);

// Gibt das letzte Projekt zur�ck, dass erforscht wurde.
function forsch_api_GetLastForschprojekt: TForschungen;

// Gibt den Index eines bereits abgeschlossenen Projektes zur�ck zum Zugriff �ber
// TForschList.CompletedAsProject
function forsch_api_IndexOfCompletetProject(ID: Cardinal): Integer;

// Diese Funktion t�tet einen zuf�lligen Forscher innerhalb einer Basis
// ohne den Wohnraum freizugeben. Ben�tigt wird dies, um bei Zerst�rung einer
// Einrichtung Platz zu schaffen. Gibt false zur�ck, falls kein Forscher in der Basis
// stationiert ist.
function forsch_api_KillRandomForschInBase(BaseID: Cardinal; out Name: String): Boolean;

// Diese Funktion pr�ft, ob noch genug Laborplatz f�r alle aktiven Wissenschaftler
// vorhanden ist. Dies wird bei Zerst�rung einer Einrichtung aufgerufen
function forsch_api_CheckLaborRoom(BaseID: Cardinal): String;

// Gibt Informationen zu einem Forschungsprojekt zur�ck
function forsch_api_GetProject(ID: Cardinal): TForschProject;

implementation

var
  g_ForschListe: TForschList;

procedure forsch_api_init(ForschListe: TForschList);
begin
  g_ForschListe:=ForschListe;
end;

function forsch_api_GetForscherCount: Integer;
begin
  result:=g_ForschListe.ForscherCount;
end;

function forsch_api_GetForscherCount(BasisID: Cardinal): Integer;
begin
  result:=g_ForschListe.CountInBase(BasisID);
end;

function forsch_api_GetWeeklySold: Integer;
begin
  result:=g_ForschListe.CalculateWeeklySold;
end;

function forsch_api_GetWeeklySold(BasisID: Cardinal): Integer;
begin
  result:=g_ForschListe.CalculateWeeklySold(BasisID);
end;

function forsch_api_GetAllStrength: Integer;
var
  Dummy: Integer;
begin
  result:=0;
  for Dummy:=0 to g_ForschListe.ForscherCount-1 do
    inc(result,round(g_ForschListe.Forscher[Dummy].Strength));
end;

function forsch_api_GetAllStrength(BasisID: Cardinal): Integer;
var
  Dummy: Integer;
begin
  result:=0;
  for Dummy:=0 to g_ForschListe.ForscherCount-1 do
    if g_ForschListe.Forscher[Dummy].BasisID=BasisID then
      inc(result,round(g_ForschListe.Forscher[Dummy].Strength));
end;

function forsch_api_calculateForschTime(Hours: Integer): Integer;
begin
  {$IFDEF FASTRESEARCH}
  result:=1;
  {$ELSE}
  result:=round(Hours*(g_ForschListe.ProjektTime/100));
  {$ENDIF}
end;

procedure forsch_api_AddAlienItemResearch(ItemID: Cardinal);
begin
  g_ForschListe.AddAlienItemResearch(ItemID);
end;

function forsch_api_isStartProject(const Projekt: TForschProject): Boolean;
begin
  result:=Projekt.Start and (Projekt.Ver=0);
end;

procedure forsch_api_RegisterProjektEndHandler(Handler: TNotifyEvent);
begin
  g_ForschListe.NotifyList.RegisterEvent(EVENT_FORSCHLISTPROJEKTEND,Handler);
end;

procedure forsch_api_RegisterUpgradeEndHandler(Handler: TNotifyEvent);
begin
  g_ForschListe.NotifyList.RegisterEvent(EVENT_FORSCHLISTUPGRADEEND,Handler);
end;

function forsch_api_GetLastForschprojekt: TForschungen;
begin
  result:=g_ForschListe.GetLastProject;
end;

function forsch_api_IndexOfCompletetProject(ID: Cardinal): Integer;
begin
  result:=g_ForschListe.IndexOfCompletedProject(ID);
  Assert(result<>-1);
end;

function forsch_api_KillRandomForschInBase(BaseID: Cardinal; out Name: String): Boolean;
var
  Forscher: Array of Integer;
  Dummy   : Integer;
  Index   : Integer;
begin
  for Dummy:=0 to g_ForschListe.ForscherCount-1 do
  begin
    if g_ForschListe[Dummy].BasisID=BaseID then
    begin
      SetLength(Forscher,length(Forscher)+1);
      Forscher[high(Forscher)]:=Dummy;
    end;
  end;

  if length(Forscher)=0 then
  begin
    result:=false;
    exit;
  end;

  result:=true;

  Index:=random(length(Forscher));
  Assert((Index>=0) and (Index<length(Forscher)));

  Name:=SForscher+' '+g_ForschListe[Forscher[Index]].Name;

  g_ForschListe.DeleteForscher(Forscher[Index],false);
end;

function forsch_api_CheckLaborRoom(BaseID: Cardinal): String;
begin
  result:=g_ForschListe.CheckLaborRoom(BaseID);
end;

function forsch_api_GetProject(ID: Cardinal): TForschProject;
var
  Index: Integer;
begin
  Index:=g_ForschListe.GetIndexOfProjectId(ID);
  Assert(Index<>-1);
  result:=g_ForschListe.GetForschProject(Index);
end;

end.
