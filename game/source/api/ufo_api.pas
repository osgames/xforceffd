{********************************************************************************
*										*
* Copyright (C) 2004  X-Force Team - jim_raynor@web.de				*
*										*
* This file is part of X-Force: Fight For Destiny (X-Force).			*
*										*
* X-Force is free software; you can redistribute it and/or modify		*
* it under the terms of the GNU General Public License as published by		*
* the Free Software Foundation; either version 2 of the License, or		*
* (at your option) any later version.						*
*										*
* X-Force is distributed in the hope that it will be useful,			*
* but WITHOUT ANY WARRANTY; without even the implied warranty of		*
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the			*
* GNU General Public License for more details.					*
*										*
* You should have received a copy of the GNU General Public License		*
* along with X-Force; if not, write to the Free Software			*
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA	*
*										*
*********************************************************************************
*										*
* ufo_ap bildet die Schnittstelle zu einer UFO-Liste, die die UFOS verwaltet. 	*
*										*
* WICHTIG!! Bevor die Funktionen genutzt werden k�nnen muss �ber 		*
*           ufo_api_init die UFO-Liste gesetzt werden. Sollte durch 		*
*           savegame_api_init geschehen sein.					*
*										*
*********************************************************************************
*										*
* CVS-Informationen                                                             *
* Autor           $Author$
* CheckInDate     $Date$
* Tag/Branch      $Name$
* Revision        $Revision$
*										*
********************************************************************************}

{$I ../Settings.inc}

unit ufo_api;

interface

uses UFOList, DXDraws, XForce_types;

procedure ufo_api_init(UFOListe: TUFOList);

// Gibt die Anzahl der verf�gbaren UFO-Modelle zur�ck
function ufo_api_GetUFOModelCount: Integer;

// Gibt ein UFO-Modell anhand des Index zur�ck
function ufo_api_GetUFOModelByIndex(Index: Integer; out Model: TUFOModel): Boolean;

// Gibt ein UFO-Modell anhand der Model-ID zur�ck
function ufo_api_GetUFOModelByID(ID: Cardinal; out Model: TUFOModel): Boolean;

// Gibt die Anzahl der UFOs auf der Erde zur�ck
function ufo_api_GetUFOCount: Integer;

// Gibt die Anzahl der gesichteten UFOs zur�ck
function ufo_api_GetVisibleUFOCount: Integer;

// Gibt das UFO mit der ID zur�ck. Ist Check aktiviert wird eine Assertion
// ausgel�st, wenn die ID ung�ltig ist
function ufo_api_GetUFO(ID: Cardinal;Check: Boolean = false): TUFO;overload;

function ufo_api_GetUFO(Index: Integer): TUFO;overload;

// Erstellt eine neues UFO
function ufo_api_CreateUFO: TUFO;

// Erstellt ein neues UFO aus einem Model heraus
function ufo_api_CreateUFOFromModel(Model: TUFOModel): TUFO;

// Zeichnet ein Symbol des UFOs
procedure ufo_api_DrawIcon(Surface: TDirectDrawSurface;x,y: Integer;Symbol: Integer);

implementation

uses
  savegame_api;

var
  g_UFOList: TUFOList;

procedure ufo_api_init(UFOListe: TUFOList);
begin
  g_UFOList:=UFOListe;

  savegame_api_RegisterStartGameHandler(g_UFOList.StartGameHandlerEarthMap);
end;

function ufo_api_GetUFOModelCount: Integer;
begin
  result:=g_UFOList.ModelCount;
end;

function ufo_api_GetUFOModelByIndex(Index: Integer; out Model: TUFOModel): Boolean;
begin
  if (Index<0) or (Index>=ufo_api_GetUFOModelCount) then
    result:=false
  else
  begin
    Model:=g_UFOList.Models[Index];
    result:=true;
  end;
end;

function ufo_api_GetUFOModelByID(ID: Cardinal; out Model: TUFOModel): Boolean;
var
  Index: Integer;
begin
  Index:=g_UFOList.GetIndexOfModel(ID);
  result:=ufo_api_GetUFOModelByIndex(Index,Model);
end;

function ufo_api_GetUFOCount: Integer;
begin
  result:=g_UFOList.Count;
end;

function ufo_api_GetVisibleUFOCount: Integer;
begin
  result:=g_UFOList.Gesichtet;
end;

function ufo_api_GetUFO(ID: Cardinal;Check: Boolean): TUFO;
begin
  result:=g_UFOList.IDofUFO(ID);
  if Check then
    Assert(result<>nil);
end;

function ufo_api_GetUFO(Index: Integer): TUFO;overload;
begin
  result:=g_UFOList.UFOs[Index];
  Assert(result<>nil);
end;

function ufo_api_CreateUFO: TUFO;
begin
  result:=g_UFOList.CreateUFO;
  Assert(result<>nil,'Keine verf�gbaren UFO-Modelle');
end;

// Erstellt ein neues UFO aus einem Model heraus
function ufo_api_CreateUFOFromModel(Model: TUFOModel): TUFO;
begin
  result:=g_UFOList.CreateUFOFromModel(Model);
  Assert(result<>nil);
end;

procedure ufo_api_DrawIcon(Surface: TDirectDrawSurface;x,y: Integer;Symbol: Integer);
begin
  g_UFOList.DrawIcon(Surface,X,Y,Symbol);
end;

end.
