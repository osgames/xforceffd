{********************************************************************************
*										*
* Copyright (C) 2004  X-Force Team - jim_raynor@web.de				*
*										*
* This file is part of X-Force: Fight For Destiny (X-Force).			*
*										*
* X-Force is free software; you can redistribute it and/or modify		*
* it under the terms of the GNU General Public License as published by		*
* the Free Software Foundation; either version 2 of the License, or		*
* (at your option) any later version.						*
*										*
* X-Force is distributed in the hope that it will be useful,			*
* but WITHOUT ANY WARRANTY; without even the implied warranty of		*
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the			*
* GNU General Public License for more details.					*
*										*
* You should have received a copy of the GNU General Public License		*
* along with X-Force; if not, write to the Free Software			*
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA	*
*										*
*********************************************************************************
*										*
* lager_api bildet die Schnittstelle zu einer Lagerliste, die Ausr�stungen	*
* verwaltet.
*										*
* WICHTIG!! Bevor die Funktionen genutzt werden k�nnen muss �ber 		*
*           lager_api_init die Personenliste gesetzt werden. Sollte durch      	*
*           savegame_api_init geschehen sein.					*
*										*
********************************************************************************}

{$I ../Settings.inc}

unit lager_api;

interface

uses
  LagerListe, XForce_types, DXDraws, SysUtils, string_utils, StringConst;

procedure lager_api_init(LagerListe: TLagerListe);

// Zeiger auf die Ausr�stung zur angegeben ID (ACHTUNG: Bei ung�ltiger ID Raise Exception,
//                                                     ausser Check = false)
function lager_api_GetItem(ID: Cardinal; Check: Boolean = true): PLagerItem;overload

// Zeiger auf die Ausr�stung mit angegeben Index. Bei ung�ltigen Index Returnwert nil)
function lager_api_GetItem(Index: Integer): PLagerItem;overload

// Ermittelt den Index der Ausr�stung
// Ist CheckIndex true, wird eine Exception ausgel�st, wenn eine ung�ltige ID �bergeben
// wird
function lager_api_GetItemIndex(ID: Cardinal; CheckIndex: Boolean = true): Integer;

// Anzahl der Ausr�stung in einer Basis
function lager_api_GetItemCountInBase(BaseID: Cardinal; ItemID: Cardinal): Integer;overload;
function lager_api_GetItemCountInBase(BaseID: Cardinal; Index: Integer): Integer;overload;

// Anzahl der Ausr�stungen in der Lagerliste
function lager_api_Count: Integer;

//***************************************************************************************
// Ausr�stung verwalten
//***************************************************************************************
// Ausr�stung aus der basis entfernen (false, wenn nicht gen�gend in der Basis sind)
function lager_api_DeleteItem(BaseID: Cardinal; ItemID: Cardinal; Count: Integer = 1): Boolean;

// packt die angegebene Ausr�stung einmal in die Basis unter Pr�fung des Lagerverbrauches
// wenn nicht genug Platz in der Basis ist, wird eine ENotEnoughRoom ausgel�st
procedure lager_api_PutItem(BaseID: Cardinal; ItemID: Cardinal);

// packt die angegebene Munition in die ausgew�hlte Basis unter ber�cksichtigung der Anzahl der Sch�sse
// Es wird ShootSurPlus erh�ht und wenn dieses �ber die Anzahl der Sch�sse steigt, wird ein Munitionspack
// in der Basis abgelegt
// Ist Schuss negativ, wird die Munition aus dem Lager genommen
// wenn nicht genug Platz in der Basis ist, wird eine ENotEnoughRoom ausgel�st
function lager_api_PutMunition(BaseID: Cardinal; ItemID: Cardinal; Schuss: Integer): Boolean;

// packt die angegebene Ausr�stung in die Basis unter Pr�fung des Lagerverbrauches
// gibt zur�ck, wieviele tats�chlich in der Basis Platz hatten
function lager_api_PutItems(BaseID: Cardinal; ItemID: Cardinal; Count: Integer): Integer;

// F�gt einmal die angegebe Ausr�stung in die Basis ohbe den Lagerverbrauch zu
// erh�hen (Fertigstellung einer Herstellung)
procedure lager_api_PutItemDirekt(BaseID: Cardinal; ItemID: Cardinal);

// TForschProject aus Forschungsliste als Ausr�stung einf�gen
procedure lager_api_AddForschItem(const Forschung: TForschProject);

// Pr�fen, ob Ausr�stung dem Benutzer angezeigt wird
// N�tigt, da auch Alienausr�stungen in der Liste verwaltet werden, von
// der der Benutzer noch nichts mitbekommen hat
function lager_api_ItemVisible(const Item: TLagerItem): Boolean;overload;
function lager_api_ItemVisible(ID: Cardinal): Boolean;overload;

// Gibt eine zuf�llige Alienausr�stung zur�ck
// TypeID = Type der Ausr�stung die ermittelt werden soll
// IQ     = IQ den der Alien besitzt
function lager_api_GetRandomItem(TypeID: TProjektTypes; IQ: Integer; Aliens: Boolean = false): PLagerItem;

// Gibt zu einer Alienwaffe eine zuf�llige Alienmunition zur�ck
// Weapon = ID der Waffe zu der Munition ermittelt werden soll
// IQ     = IQ den der Alien besitzt
function lager_api_GetRandomMunition(Weapon: Cardinal; IQ: Integer; Aliens: Boolean = false): PLagerItem;

// Sucht zu der angegeben Raumschiffwaffe den Index der passenden Munition raus
function lager_api_GetRMunitionIndex(Weapon: Cardinal): Integer;

// Symbol zeichnen
procedure lager_api_DrawItem(ID: Cardinal;Surface: TDirectDrawSurface;x,y: Integer; Alpha: Integer = 255);overload;
procedure lager_api_DrawItem(Image: Integer;Surface: TDirectDrawSurface;x,y: Integer; Alpha: Integer = 255);overload;

procedure lager_api_DrawLargeImage(Image: Integer;Surface: TDirectDrawSurface;x,y: Integer;Shadow: boolean = false);

// kleines Symbol im Bodeneinsatz zeichnen
procedure lager_api_DrawISOImage(Image: Integer; Surface: TDirectDrawSurface; x, y: Integer);

// Alienausr�stung zum Forschen freigeben
procedure lager_api_RegisterAlienItem(const Item: TLagerItem);

// Schaltet erforschte Ausr�stung zur Benutzung frei
procedure lager_api_ResearchComplete(Item: Cardinal);

// Ermittelt zum angegebenen Land alle Ausr�stungen, die vom Land hergestellt werden
procedure lager_api_EnumLandItems(var Items: TStringArray; Land: Integer);

// Ermittelt zu einer Waffe die Namen aller Verf�gbaren Munitionen
procedure lager_api_EnumMunitionen(var Items: TStringArray; ID: Cardinal);

// Berechnet das Upgrade zu einer Ausr�stung
procedure lager_api_UpgradeItem(var UpgradeItem: TItemUpgrade);

// Zerst�rt in einer Basis Ausr�stung. Wird ben�tigt um eine Einrichtung durch
// einen Angriff zu zerst�ren und Ausr�stung zu zerst�ren
function lager_api_DestroyItems(BasisID: Cardinal; LagerV: double): String;

// gibt den Alphatron Wert zur�ck, der beim Recycling gewonnen wird
function lager_api_GetRecyclingAlphatron(const Item: TLagerItem): Integer;

implementation

uses
  KD4Utils, forsch_api, math, basis_api;

var
  g_LagerList: TLagerListe;

procedure lager_api_init(LagerListe: TLagerListe);
begin
  g_LagerList:=LagerListe;
end;

function lager_api_GetItem(ID: Cardinal; Check: Boolean): PLagerItem;
begin
  result:=lager_api_GetItem(g_LagerList.IndexOfID(ID));
  Assert((not Check) or (result<>nil),Format('Ausr�stung nicht gefunden : %d',[ID]));
end;

function lager_api_GetItem(Index: Integer): PLagerItem;overload;
begin
  if (Index>=0) and (Index<lager_api_count) then
    result:=g_LagerList.AddrLagerItem(Index)
  else
    result:=nil;
end;

function lager_api_GetItemIndex(ID: Cardinal; CheckIndex: Boolean): Integer;
begin
  result:=g_LagerList.IndexOfID(ID);
  assert((not CheckIndex) or (result<>-1),Format('Requested ID: %d',[ID]));
end;

function lager_api_GetItemCountInBase(BaseID: Cardinal; ItemID: Cardinal): Integer;
begin
  result:=lager_api_GetItemCountInBase(BaseID,lager_api_GetItemIndex(ItemID));
end;

function lager_api_GetItemCountInBase(BaseID: Cardinal; Index: Integer): Integer;
begin
  result:=g_LagerList.GetItemCountInBase(Index,BaseID);
end;

function lager_api_Count: Integer;
begin
  result:=g_LagerList.Count;
end;

function lager_api_DeleteItem(BaseID: Cardinal; ItemID: Cardinal; Count: Integer = 1): Boolean;
begin
  result:=g_LagerList.DeleteItem(lager_api_GetItemIndex(ItemID),BaseID,Count);
end;

procedure lager_api_PutItem(BaseID: Cardinal; ItemID: Cardinal);
begin
  g_LagerList.AddItem(ItemID,BaseID);
end;

function lager_api_PutMunition(BaseID: Cardinal; ItemID: Cardinal; Schuss: Integer): boolean;
begin
  result:=g_LagerList.AddMunition(ItemID,BaseID,Schuss);
end;

function lager_api_PutItems(BaseID: Cardinal; ItemID: Cardinal; Count: Integer): Integer;
begin
  result:=g_LagerList.AddItem(ItemID,BaseID,Count);
end;

procedure lager_api_PutItemDirekt(BaseID: Cardinal; ItemID: Cardinal);
begin
  g_LagerList.AddItem(ItemID,BaseID,false);
end;

procedure lager_api_AddForschItem(const Forschung: TForschProject);
begin
  g_LagerList.AddForschItem(TObject(Addr(Forschung)));
end;

procedure lager_api_ResearchComplete(Item: Cardinal);
begin
  g_LagerList.ResearchComplete(Item);
end;

function lager_api_ItemVisible(const Item: TLagerItem): Boolean;
begin
  result:=Item.Visible;
end;

function lager_api_ItemVisible(ID: Cardinal): Boolean;
begin
  result:=lager_api_ItemVisible(lager_api_GetItem(ID)^);
end;

function lager_api_GetRandomItem(TypeID: TProjektTypes; IQ: Integer;Aliens: Boolean = false): PLagerItem;
begin
  result:=g_LagerList.GetRandomItem(TypeID,IQ,0,Aliens);
end;

function lager_api_GetRandomMunition(Weapon: Cardinal; IQ: Integer;Aliens: Boolean = false): PLagerItem;
begin
  result:=g_LagerList.GetRandomItem([ptMunition],IQ,Weapon,Aliens);
end;

function lager_api_GetRMunitionIndex(Weapon: Cardinal): Integer;
var
  Dummy: Integer;
begin
  result:=-1;
  for Dummy:=0 to g_LagerList.Count-1 do
  begin
    if (g_LagerList[Dummy].TypeID=ptRMunition) and (g_LagerList[Dummy].Munfor=Weapon) then
    begin
      if lager_api_ItemVisible(g_LagerList[Dummy]) then
      begin
        result:=Dummy;
        exit;
      end;
    end;
  end;
end;

procedure lager_api_DrawItem(ID: Cardinal;Surface: TDirectDrawSurface;x,y: Integer;Alpha: Integer);overload;
begin
  lager_api_DrawItem(lager_api_GetItem(ID)^.ImageIndex,Surface,X,Y,Alpha);
end;

procedure lager_api_DrawItem(Image: Integer;Surface: TDirectDrawSurface;x,y: Integer;Alpha: Integer);overload;
begin
  if Alpha=255 then
    g_LagerList.DrawImage(Image,Surface,X,Y)
  else
    g_LagerList.DrawImageAlpha(Image,Surface,X,Y,Alpha);
end;

procedure lager_api_DrawLargeImage(Image: Integer;Surface: TDirectDrawSurface;x,y: Integer;Shadow: boolean);
begin
  g_LagerList.DrawImage(Image,Surface,X,Y,1,Shadow);
end;

procedure lager_api_DrawISOImage(Image: Integer; Surface: TDirectDrawSurface; x, y: Integer);
begin
  g_LagerList.DrawImageISO(Image,Surface,X,Y);
end;

procedure lager_api_RegisterAlienItem(const Item: TLagerItem);
begin
  g_LagerList.RegisterAlienItem(Item);
end;

procedure lager_api_EnumLandItems(var Items: TStringArray; Land: Integer);
var
  Dummy: Integer;
begin
  SetLength(Items,0);
  for Dummy:=0 to g_LagerList.Count-1 do
  begin
    if g_LagerList[Dummy].Land=Land then
      string_utils_Add(Items,g_LagerList[Dummy].Name);
  end;
end;

procedure lager_api_EnumMunitionen(var Items: TStringArray; ID: Cardinal);
var
  Dummy : Integer;
begin
  Assert(ID<>0);
  SetLength(Items,0);
  for Dummy:=0 to g_LagerList.Count-1 do
  begin
    if (g_LagerList[Dummy].TypeID in [ptMunition,ptRMunition]) and (g_LagerList[Dummy].MunFor=ID) then
      string_utils_Add(Items,g_LagerList[Dummy].Name);
  end;
end;

procedure lager_api_UpgradeItem(var UpgradeItem: TItemUpgrade);
var
  Dummy: Integer;
  New  : PLagerItem;
  Old  : PLagerItem;

  // Val = Wert
  // Percent = H�he des Upgrades
  // minUpg = Mindest Upgrade
  // maxUpg = Maximal Upgrade
  // MinVal = Minimaler Wert
  // MaxVal = Maximaler Wert
  procedure UpgradeInteger(var Value: Integer; Percent: double; minUpg,maxUpg,minVal,maxVal: Integer);
  begin
    Value:=minMax(Value+minMax(Integer(round(Value*Percent)),minUpg,maxUpg),minVal,MaxVal);
  end;

  procedure UpgradeWord(var Value: Word; Percent: double; minUpg,maxUpg,minVal,maxVal: Integer);
  begin
    Value:=minMax(Value+minMax(Integer(round(Value*Percent)),minUpg,maxUpg),minVal,MaxVal);
  end;

  procedure UpgradeDouble(var Value: Double; Percent: double; minUpg,maxUpg,minVal,maxVal: Integer);
  begin
    Value:=minMax(Value+minMax(Integer(round(Value*Percent)),minUpg,maxUpg),minVal,MaxVal);
  end;

  procedure UpgradeByte(var Value: Byte; Percent: double; minUpg,maxUpg,minVal,maxVal: Integer);
  begin
    Value:=minMax(Value+minMax(Integer(round(Value*Percent)),minUpg,maxUpg),minVal,MaxVal);
  end;

begin

  New:=Addr(UpgradeItem.New);
  Old:=Addr(UpgradeItem.Old);
  New^:=Old^;
  inc(New.Level);

  UpgradeItem.Time:=forsch_api_calculateForschTime(round(New.Level*New.ProdTime));

  // Lagerraum wird nicht mehr geupgradet (Macht irgendwo keinen Sinn, dass die Ausr�stungen
  // beim Upgrade entscheidend kleiner werden)
//  New.LagerV:=max(New.LagerV-(New.LagerV*0.1),0.1);
  New.Gewicht:=max(New.Gewicht-New.Gewicht*0.05,0.1);

  // Angriffst�rke
  if New.TypeID in [PtWaffe,ptMunition,ptRWaffe,ptRMunition,ptMine,ptGranate] then
  begin
    if New.Strength<>0 then
    begin
      UpgradeInteger(Integer(New.Strength),0.05,1,100,1,1000);
    end;
  end;

  if New.TypeID in [ptWaffe,ptMunition] then
  begin
    UpgradeByte(New.Power,0.05,1,2,1,100);
  end;

  // Munition
  if (New.TypeID in [ptMunition,ptRMunition]) or ((New.TypeID in [ptWaffe,ptRWaffe]) and (New.WaffType=wtLaser)) then
  begin
    if (Old.Gewicht/Old.Munition)<0.1 then
      New.Munition:=New.Munition+max(round(New.Munition*0.05),1);
  end;

  case New.TypeID of
    ptWaffe     :
    begin
      // Update der Effektiven Reichweite nur bei Level 5
      if New.Level=5 then
        inc(New.Genauigkeit);
      New.Laden:=max(New.Laden-round((New.Laden*0.05)),25);
    end;
    ptMotor     :
    begin
      if New.Level=5 then
        New.Verfolgung:=true;
      New.Munition:=round(New.Munition*1.01);
      New.Strength:=New.Strength-round(max(New.Strength*0.05,New.Level*0.101));
      New.PixPerSec:=min(round((New.PixPerSec*1.05)+(New.Level*0.1)),15);
    end;
    ptRWaffe    :
    begin
      if (New.Level=5) and (New.WaffType=wtRaketen) then New.Verfolgung:=true;
      New.Laden:=max(New.Laden-round((New.Laden*0.05)),90);
      New.Reichweite:=New.Reichweite+round(Old.Reichweite*0.05);
    end;
    ptSensor    : New.Strength:=New.Strength+round((New.Strength*0.05)+(New.Level*0.2));
    ptPanzerung : New.Panzerung:=New.Panzerung+round((New.Panzerung*0.05)+(New.Level*0.2));
    ptExtension :
    begin
      for Dummy:=0 to Old.ExtCount-1 do
      begin
        case Old.Extensions[Dummy].Prop of
          epSensor: New.Extensions[Dummy].Value:=round(New.Extensions[Dummy].Value*1.02);
          epShieldHitpoints: UpgradeInteger(New.Extensions[Dummy].Value,0.05,1,100,0,2500);
          epShieldDefence: UpgradeInteger(New.Extensions[Dummy].Value,0.02,1,100,0,100);
          epShieldReloadTime: UpgradeInteger(New.Extensions[Dummy].Value,-0.02,-100,100,25,10000);
        end;
      end;
    end;
    ptMine : New.Reichweite:=New.Reichweite+Floor((New.Reichweite*0.05)+(New.Level*0.25));
    ptGuertel :
      New.Strength:=min(round((New.Strength*1.05)+(New.Level*0.1)),12);
  end;
end;

function lager_api_DestroyItems(BasisID: Cardinal; LagerV: double): String;
var
  Item  : Integer;
  Count : Integer;
  LItem : PLagerItem;
  Lager : Integer;
begin
  Lager:=round(LagerV*10);
  result:='';
  while Lager>0 do
  begin
    // Eine zuf�llige Ausr�stung innerhalb der Basis finden
    Item:=random(g_LagerList.Count);

    Count:=g_LagerList.GetItemCountInBase(Item,BasisID);
    if Count>0 then
    begin
      LItem:=lager_api_GetItem(Item);
      Count:=max(1,min(Count,round(Lager/round(LItem.LagerV*10))));

      Assert(g_LagerList.DeleteItem(Item,BasisID,Count,false));

      result:=result+Format(ST0503080001,[Count,LItem.Name])+#13#10;

      Lager:=Lager-(Count*round(LItem.LagerV*10));
    end;
  end;

  basis_api_FreeLagerRaum(-(Lager/10),BasisID);

{  Lager:=0;
  for Dummy:=0 to g_LagerList.Count-1 do
  begin
    LItem:=lager_api_GetItem(Dummy);
    Count:=g_LagerList.GetItemCountInBase(Dummy,BasisID);

    inc(Lager,round(LItem.LagerV*10)*Count);
  end;

  Assert(Lager=round(basis_api_BelegterLagerRaum(BasisID)*10));}
end;

function lager_api_GetRecyclingAlphatron(const Item: TLagerItem): Integer;
begin
  result:=Trunc(Item.ManuAlphatron*0.02);
end;

end.
