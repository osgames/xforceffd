{********************************************************************************
*										*
* Copyright (C) 2004  X-Force Team - jim_raynor@web.de				*
*										*
* This file is part of X-Force: Fight For Destiny (X-Force).			*
*										*
* X-Force is free software; you can redistribute it and/or modify		*
* it under the terms of the GNU General Public License as published by		*
* the Free Software Foundation; either version 2 of the License, or		*
* (at your option) any later version.						*
*										*
* X-Force is distributed in the hope that it will be useful,			*
* but WITHOUT ANY WARRANTY; without even the implied warranty of		*
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the			*
* GNU General Public License for more details.					*
*										*
* You should have received a copy of the GNU General Public License		*
* along with X-Force; if not, write to the Free Software			*
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA	*
*										*
*********************************************************************************
*										*
* einsatz_api bildet die Schnittstelle zu einer Einsatz-Liste, die die Eins�tze *
* verwaltet. 									*
*										*
* WICHTIG!! Bevor die Funktionen genutzt werden k�nnen muss �ber 		*
*           einsatz_api_init die Einsatz-Liste gesetzt werden. Sollte durch 	*
*           savegame_api_init geschehen sein.					*
*										*
*********************************************************************************
*										*
* CVS-Informationen                                                             *
* Autor           $Author$
* CheckInDate     $Date$
* Tag/Branch      $Name$
* Revision        $Revision$
*										*
********************************************************************************}

{$I ../Settings.inc}

unit einsatz_api;

interface

uses EinsatzListe, XForce_types;

procedure einsatz_api_init(EinsatzListe: TEinsatzListe);

// Gibt die Anzahl der verf�gbaren Eins�tze zur�ck
function einsatz_api_GetEinsatzCount: Integer;

// Gibt den Einsatz mit der ID zur�ck. Ist Check aktiviert wird eine Assertion
// ausgel�st, wenn die ID ung�ltig ist
function einsatz_api_GetEinsatz(ID: Cardinal;Check: Boolean = false): TEinsatz;overload;

function einsatz_api_GetEinsatz(Index: Integer): TEinsatz;overload;

// Erstellt einen neuen Einsatz an einer zufalligen Position
function einsatz_api_GenerateEinsatz: TEinsatz;overload;

// Erstellt einen neuen Einsatz an der �bergebenen Position
function einsatz_api_GenerateEinsatz(Position: TFloatPoint): TEinsatz;overload;

implementation

var
  g_EinsatzList: TEinsatzListe;

procedure einsatz_api_init(EinsatzListe: TEinsatzListe);
begin
  g_EinsatzList:=EinsatzListe;
end;

function einsatz_api_GetEinsatzCount: Integer;
begin
  result:=g_EinsatzList.Count;
end;

function einsatz_api_GetEinsatz(ID: Cardinal;Check: Boolean): TEinsatz;
begin
  result:=g_EinsatzList.IndexOfID(ID);
  if Check then
    Assert(result<>nil);
end;

function einsatz_api_GetEinsatz(Index: Integer): TEinsatz;
begin
  result:=g_EinsatzList.Einsatz[Index];
  Assert(result<>nil);
end;

function einsatz_api_GenerateEinsatz: TEinsatz;overload;
begin
  result:=g_EinsatzList.NewEinsatz;
  Assert(result<>nil);
end;

function einsatz_api_GenerateEinsatz(Position: TFloatPoint): TEinsatz;
begin
  result:=g_EinsatzList.NewEinsatz(Position);
  Assert(result<>nil);
end;

end.
