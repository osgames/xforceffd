{********************************************************************************
*										*
* Copyright (C) 2004  X-Force Team - jim_raynor@web.de				*
*										*
* This file is part of X-Force: Fight For Destiny (X-Force).			*
*										*
* X-Force is free software; you can redistribute it and/or modify		*
* it under the terms of the GNU General Public License as published by		*
* the Free Software Foundation; either version 2 of the License, or		*
* (at your option) any later version.						*
*										*
* X-Force is distributed in the hope that it will be useful,			*
* but WITHOUT ANY WARRANTY; without even the implied warranty of		*
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the			*
* GNU General Public License for more details.					*
*										*
* You should have received a copy of the GNU General Public License		*
* along with X-Force; if not, write to the Free Software			*
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA	*
*										*
*********************************************************************************
*										*
* game_api bildet Zugriff auf einige Grundlegende Funktionen			*
*										*
********************************************************************************}

{$I ../Settings.inc}

unit game_api;

interface

uses
  DXInput, DXMessage, DXQueryBox, Classes, Forms, DXDraws, DXContainer,
  NotifyList, Controls, StdCtrls, Consts, DXListBoxWindow, ReadLanguage;

// Diese Funktionen dienen nur zum initsialisieren der game_api
  // Legt die DirectInput-Komponente fest, die genutzt werden soll
  procedure game_api_SetDirectInput(DXInput: TDXInput);

  // Legt die Messagebox fest, die genutzt werden soll
  procedure game_api_SetDXMessage(DXMessage: TDXMessage);

  // Legt die TextBox fest, die genutzt wird um Werte Abzufragen
  procedure game_api_SetQueryTextBox(QueryTextBox: TDXQueryBox);

  // Legt die Prozedur fest, die bei game_api_quitGame aufgerufen wird
  // Normalerweise sollte diese Prozedure das Hauptformular schliessen
  procedure game_api_SetQuitEvent(QuitEvent: TNotifyEvent);

  // Setzt die Container-Komponente zur Verwendung
  procedure game_api_SetDXContainer(Container: TDXContainer);

  // Legt das ListBoxWindow fest, das genutzt wird um Werte aus einer Liste abzufragen
  procedure game_api_SetListBoxWindow(ListBoxWindow: TDXListBoxWindow);

// Diese Funktionen k�nnen verwendet werden
  // Gibt das DirectInput zur�ck
  function game_api_GetDirectInput: TDXInput;

  // zeigt eine Nachrichtenbox an
  function game_api_MessageBoxDlg(Message: String; Caption: String;Question: Boolean): TModalResult;overload;
  function game_api_MessageBoxDlg(Message: String; Caption: String;FirstButton, SecondButton: String): Boolean;overload;

  // Zeigt einen Dialog zur Eingabe eines Textes an
  function game_api_QueryText(Caption: String;MaxLength: Integer;var Text: String): Boolean;

  // zeigt eine Nachrichtenbox nur mit OK Button an
  procedure game_api_MessageBox(Message: String; Caption: String);

  // zeigt eine Frage an
  function game_api_Question(Message: String; Caption: String): Boolean;

  // Zeigt eine Liste mit der StringList an und gibt den ausgew�hlten Eintrag zur�ck
  function game_api_ChooseItemBox(ACaption: String; List: TStrings): Integer;

  // Zeigt die Spielbeenden Abfrage an
  procedure game_api_quitGame;

  // Erstellt einen Eintrag in der global DXImageList und gibt diesen zur
  // Verwendung zur�ck
  function  game_api_CreateImageListItem: TPictureCollectionItem;

  // Legt fest, ob das Spiel gespeichert werden kann oder nicht
  procedure game_api_SetCanSaveGame(CanSave: Boolean);

  // Gibt den Wert, der �ber game_api_SetCanSaveGame zur�ck
  function  game_api_GetCanSaveGame: Boolean;

  // registriert einen Event, der aufgerufen wird, wenn �ber game_api_SetCanSaveGame
  // der Wert ge�ndert wurde
  procedure game_api_RegisterChangeCanSaveGameHandler(Handler: TNotifyEvent);

  // ruft die mit Page angegebene Seite aus (Page kann in der settings/pages.xml
  // ermittelt werden)
  procedure game_api_ChangePage(Page: Integer);

  // gibt einen Text aus der Sprachdatei zur�ck
  function game_api_getConstantValue(Constant: String): String;

implementation

uses Dialogs, Windows, Graphics;

const
  EVENT_CHANGECANSAVEGAME : TEventID = 0;
var
  g_DirectInput  : TDXInput = nil;
  g_QueryTextBox : TDXQueryBox = nil;
  g_DXMessage    : TDXMessage = nil;
  g_ListBoxWind  : TDXListBoxWindow = nil;
  g_quitEvent    : TNotifyEvent = nil;
  g_ImageList    : TDXImageList = nil;
  g_Container    : TDXContainer = nil;

  g_CanSaveGame  : Boolean = true;

  g_NotifyList   : TNotifyList = nil;


// Interne Funktionen
procedure InitNotifyList;
begin
  if g_NotifyList=nil then
    g_NotifyList:=TNotifyList.Create;
end;

// �ffentliche Funktionen
procedure game_api_SetDirectInput(DXInput: TDXInput);
begin
  g_DirectInput:=DXInput;
end;

procedure game_api_SetDXMessage(DXMessage: TDXMessage);
begin
  g_DXMessage:=DXMessage;
end;

procedure game_api_SetQuitEvent(QuitEvent: TNotifyEvent);
begin
  g_quitEvent:=QuitEvent;
end;

procedure game_api_SetQueryTextBox(QueryTextBox: TDXQueryBox);
begin
  g_QueryTextBox:=QueryTextBox;
end;

procedure game_api_SetListBoxWindow(ListBoxWindow: TDXListBoxWindow);
begin
  g_ListBoxWind:=ListBoxWindow;
end;

procedure game_api_SetDXContainer(Container: TDXContainer);
begin
  g_Container:=Container;
  if g_ImageList<>nil then
    g_ImageList.DXDraw:=Container;
end;

function game_api_GetDirectInput: TDXInput;
begin
  result:=g_DirectInput;
  Assert(result<>nil);
end;

function game_api_MessageBoxDlg(Message: String; Caption: String;Question: Boolean): TModalResult;
begin
  if g_DXMessage<>nil then
    result:=g_DXMessage.Show(Message,Caption,Question)
  else
  begin
    result:=mrNone;
    if Question then
    begin
      case Application.MessageBox(PChar(Message),PChar(Caption),MB_ICONQUESTION or MB_YESNO) of
        ID_YES: result:=mrYes;
        ID_NO : result:=mrNo;
      end;
    end
    else
      game_api_MessageBox(Message,Caption);
  end;
end;

function game_api_MessageBoxDlg(Message: String; Caption: String;FirstButton, SecondButton: String): Boolean;overload;
begin
  result:=g_DXMessage.Show(Message,Caption,FirstButton,SecondButton)=mrYes;
end;

procedure game_api_MessageBox(Message: String; Caption: String);
begin
  if g_DXMessage<>nil then
    g_DXMessage.Show(Message,Caption,false)
  else
    Application.MessageBox(PChar(Message),PChar(Caption),0);
end;

function game_api_QueryText(Caption: String;MaxLength: Integer;var Text: String): Boolean;
begin
  if g_QueryTextBox<>nil then
  begin
    g_QueryTextBox.Caption:=Caption;
    g_QueryTextBox.Text:=Text;
    g_QueryTextBox.MaxLength:=MaxLength;

    g_QueryTextBox.ShowModal;

    Text:=g_QueryTextBox.Text;

    result:=Text<>'';
  end
  else
  begin
    result:=InputQuery(Caption,Caption,Text);
    if not result then
      Text:=''
    else
      Text:=Copy(Text,1,MaxLength);
  end;
end;

function game_api_Question(Message: String; Caption: String): Boolean;
begin
  if g_DXMessage<>nil then
    result:=(g_DXMessage.Show(Message,Caption,true)=mrYes)
  else
  begin
    case Application.MessageBox(PChar(Message),PChar(Caption),MB_ICONQUESTION or MB_YESNO) of
      ID_YES: result:=true;
      ID_NO : result:=false;
      else
        result:=false;
    end;
  end;
end;

function GetAveCharSize(Canvas: TCanvas): TPoint;
var
  I: Integer;
  Buffer: array[0..51] of Char;
begin
  for I := 0 to 25 do Buffer[I] := Chr(I + Ord('A'));
  for I := 0 to 25 do Buffer[I + 26] := Chr(I + Ord('a'));
  GetTextExtentPoint(Canvas.Handle, Buffer, 52, TSize(Result));
  Result.X := Result.X div 52;
end;

function game_api_ChooseItemBox(ACaption: String; List: TStrings): Integer;
var
  Form: TForm;
  ListBox: TListBox;
  DialogUnits: TPoint;
  ButtonTop, ButtonWidth, ButtonHeight: Integer;
begin
  if g_ListBoxWind<>nil then
  begin
    g_ListBoxWind.Items.Assign(List);
    result:=g_ListBoxWind.Show;
  end
  else
  begin
    Form := TForm.Create(Application);
    with Form do
    begin
      try
        Canvas.Font := Font;
        DialogUnits:=GetAveCharSize(Canvas);
        BorderStyle := bsDialog;
        Caption := ACaption;
        ClientWidth := MulDiv(180, DialogUnits.X, 4);
        ClientHeight := MulDiv(100, DialogUnits.Y, 8);
        Position := poScreenCenter;
        ListBox := TListBox.Create(Form);
        with ListBox do
        begin
          Parent := Form;
          Left := MulDiv(8, DialogUnits.X, 4);
          Top := MulDiv(8, DialogUnits.Y, 8);
          Height := MulDiv(70, DialogUnits.Y, 8);
          Width := MulDiv(164, DialogUnits.X, 4);
          Items:=List;
        end;
        ButtonTop := MulDiv(82, DialogUnits.Y, 8);
        ButtonWidth := MulDiv(50, DialogUnits.X, 4);
        ButtonHeight := MulDiv(14, DialogUnits.Y, 8);
        with TButton.Create(Form) do
        begin
          Parent := Form;
          Caption := SMsgDlgOK;
          ModalResult := mrOk;
          Default := True;
          SetBounds(MulDiv(38, DialogUnits.X, 4), ButtonTop, ButtonWidth,
            ButtonHeight);
        end;
        with TButton.Create(Form) do
        begin
          Parent := Form;
          Caption := SMsgDlgCancel;
          ModalResult := mrCancel;
          Cancel := True;
          SetBounds(MulDiv(92, DialogUnits.X, 4), ButtonTop, ButtonWidth,
            ButtonHeight);
        end;
        if ShowModal = mrOk then
          result:=ListBox.ItemIndex
        else
          result:=-1;
      finally
        Form.Free;
      end;
    end;
  end;
end;

procedure game_api_quitGame;
begin
  assert(Assigned(g_quitEvent));
  g_quitEvent(nil);
end;

function  game_api_CreateImageListItem: TPictureCollectionItem;
begin
  if g_ImageList=nil then
  begin
    g_ImageList:=TDXImageList.Create(nil);
    if g_Container<>nil then
      g_ImageList.DXDraw:=g_Container;
  end;

  result := TPictureCollectionItem.Create(g_ImageList.Items);
end;

procedure game_api_SetCanSaveGame(CanSave: Boolean);
begin
  if g_CanSaveGame=CanSave then
    exit;

  g_CanSaveGame:=CanSave;

  InitNotifyList;
  g_NotifyList.CallEvents(EVENT_CHANGECANSAVEGAME,nil);
end;

function  game_api_GetCanSaveGame: Boolean;
begin
  result:=g_CanSaveGame;
end;

procedure game_api_RegisterChangeCanSaveGameHandler(Handler: TNotifyEvent);
begin
  InitNotifyList;
  g_NotifyList.RegisterEvent(EVENT_CHANGECANSAVEGAME,Handler);
end;

procedure game_api_ChangePage(Page: Integer);
begin
  Assert(g_Container<>nil);
  g_Container.ShowPage(Page);
end;

function game_api_getConstantValue(Constant: String): String;
begin
  result:=g_GameConstants.Values[Constant]; 
end;

initialization
finalization

  if g_ImageList<>nil then
    g_ImageList.Free;

  if g_NotifyList<>nil then
    g_NotifyList.Free;
end.
