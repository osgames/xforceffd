{********************************************************************************
*										*
* Copyright (C) 2004  X-Force Team - jim_raynor@web.de				*
*										*
* This file is part of X-Force: Fight For Destiny (X-Force).			*
*										*
* X-Force is free software; you can redistribute it and/or modify		*
* it under the terms of the GNU General Public License as published by		*
* the Free Software Foundation; either version 2 of the License, or		*
* (at your option) any later version.						*
*										*
* X-Force is distributed in the hope that it will be useful,			*
* but WITHOUT ANY WARRANTY; without even the implied warranty of		*
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the			*
* GNU General Public License for more details.					*
*										*
* You should have received a copy of the GNU General Public License		*
* along with X-Force; if not, write to the Free Software			*
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA	*
*										*
*********************************************************************************
*										*
* Soldaten_api bildet die Schnittstelle zu einer Soldaten-Liste, die die 	*
* Soldaten verwaltet. 								*
*										*
* WICHTIG!! Bevor die Funktionen genutzt werden k�nnen muss �ber 		*
*           soldaten_api_init die Soldaten-Liste gesetzt werden. Sollte durch 	*
*           savegame_api_init geschehen sein.					*
*										*
********************************************************************************}

{$I ../Settings.inc}

unit soldaten_api;

interface

uses
  SoldatenList, XForce_types, DXDraws, DirectDraw, GameFigureManager, StringConst,
  RaumschiffList;

procedure soldaten_api_init(SoldatenListe: TSoldatenList);

// Gesamte Anzahl der Soldaten
function  soldaten_api_GetSoldatenCount: Integer; overload;

// Gesamte Anzahl der Soldaten der Basis
function  soldaten_api_GetSoldatenCount(BasisID: Cardinal): Integer; overload;

// Ermittelt den Soldaten im bestimmten Slot eines Raumschiffes. Der Index
// in der Soldatenliste wird zur�ckgegeben
function  soldaten_api_GetSoldatInRaumschiff(RaumID: Integer; Slot: Integer; var Soldat: PSoldatInfo): Integer;

// Setzt einen Soldaten in einen Bestimmten Slot eines Raumschiffes
procedure soldaten_api_AssignRaumschiff(Soldat: Integer; RaumID: Cardinal; Slot: Byte);

// Nimmt einen Soldaten aus gesetzen Raumschiff
procedure soldaten_api_RemoveRaumschiff(Soldat: Integer);

// Gibt das Raumschiff eines Soldaten zur�ck
function soldaten_api_GetRaumschiff(const Soldat: TSoldatInfo): TRaumschiff;

// Pr�ft von allen Soldaten die Gesundheit und l�scht alle, deren Gesundheit
// kleiner als 0 ist
procedure soldaten_api_CheckSoldaten;

// Gibt das w�chentliche Gehalt f�r alle Soldaten zur�ck
function  soldaten_api_GetWeeklySold: Integer; overload;

// Gibt das w�chentliche Gehalt f�r alle Soldaten der Basis zur�ck
function  soldaten_api_GetWeeklySold(BasisID: Cardinal): Integer; overload;

// Zeichnet das Gesicht eines Soldaten. Bei Ges = true wird auch der Gesundheits-
// balken im Rechten Rand des Soldaten gezeichnet
procedure soldaten_api_DrawDXSoldat(const Soldat: TSoldatInfo;
  Surface: TDirectDrawSurface;const Mem: TDDSurfaceDesc; x, y: Integer; Ges: boolean = true);

// �ndert die Homebase eines Soldaten
procedure soldaten_api_ChangeBase(Soldat: Integer; NewBase: Cardinal);

// �ndert die Homebase eines Soldaten
function soldaten_api_GetManager(Soldat: Integer): TGameFigureManager;

procedure soldaten_api_ShowEquipView(Soldat: TGameFigureManager);

// Diese Funktion t�tet einen zuf�lligen Soldaten innerhalb einer Basis
// ohne den Wohnraum freizugeben. Ben�tigt wird dies, um bei Zerst�rung einer
// Einrichtung Platz zu schaffen. Gibt false zur�ck, falls kein Soldat in der Basis
// stationiert ist.
function soldaten_api_KillRandomSoldatInBase(BaseID: Cardinal;out Name: String): Boolean;

implementation

uses VerSoldatEinsatz, Classes, game_api, Defines, raumschiff_api;

var
  g_SoldatenList: TSoldatenList;

procedure Soldaten_api_init(SoldatenListe: TSoldatenList);
begin
  g_SoldatenList:=SoldatenListe;
end;

function  soldaten_api_GetSoldatenCount: Integer;
begin
  result:=g_SoldatenList.Count;
end;

function  soldaten_api_GetSoldatenCount(BasisID: Cardinal): Integer;
begin
  result:=g_SoldatenList.CountInBase(BasisID);
end;

function  soldaten_api_GetSoldatInRaumschiff(RaumID: Integer; Slot: Integer; var Soldat: PSoldatInfo): Integer;
begin
  result:=g_SoldatenList.GetSoldatInRaumschiff(RaumID,Slot,Soldat);
end;

procedure soldaten_api_AssignRaumschiff(Soldat: Integer; RaumID: Cardinal; Slot: Byte);
begin
  g_SoldatenList.SetRaumschiff(Soldat,RaumID,Slot);
end;

procedure soldaten_api_RemoveRaumschiff(Soldat: Integer);
begin
  // 30 als Slot hat sich eingeb�rbert als Kennzeichen, dass ein Soldat keinem
  // Raumschiff zugewiesen. Entscheidend ist allerdings die RaumschiffID = 0
  g_SoldatenList.SetRaumschiff(Soldat,0,30);
end;

function soldaten_api_GetRaumschiff(const Soldat: TSoldatInfo): TRaumschiff;
begin
  if Soldat.Slot=30 then
    result:=nil
  else
    result:=raumschiff_api_GetRaumschiff(Soldat.Raum);
end;

procedure soldaten_api_CheckSoldaten;
begin
  g_SoldatenList.CheckSoldaten;
end;

function  soldaten_api_GetWeeklySold: Integer;
begin
  result:=g_SoldatenList.CalculateWeeklySold;
end;

function  soldaten_api_GetWeeklySold(BasisID: Cardinal): Integer;
begin
  result:=g_SoldatenList.CalculateWeeklySold(BasisID);
end;

procedure soldaten_api_DrawDXSoldat(const Soldat: TSoldatInfo;
  Surface: TDirectDrawSurface;const Mem: TDDSurfaceDesc; x, y: Integer; Ges: boolean);
begin
  g_SoldatenList.DrawDXSoldat(Soldat,Surface,Mem,X,Y,Ges);
end;

procedure soldaten_api_ChangeBase(Soldat: Integer; NewBase: Cardinal);
begin
  g_SoldatenList.ChangeBase(Soldat, NewBase);
end;

function soldaten_api_GetManager(Soldat: Integer): TGameFigureManager;
begin
  result:=g_SoldatenList.Managers[Soldat];
end;

procedure soldaten_api_ShowEquipView(Soldat: TGameFigureManager);
var
  Dummy: Integer;
  List : TList;
begin
  List:=TList.Create;
  for Dummy:=0 to g_SoldatenList.Count-1 do
    List.Add(g_SoldatenList.Managers[Dummy]);
  Managers:=List;
  VerSoldatEinsatz.UseTimeUnits:=true;
  ActiveManager:=Soldat;
  game_api_ChangePage(PageSoldatConfig);
  List.Free;
end;

function soldaten_api_KillRandomSoldatInBase(BaseID: Cardinal;out Name: String): Boolean;
var
  Soldaten: Array of Integer;
  Dummy   : Integer;
  Index   : Integer;
begin
  for Dummy:=0 to g_SoldatenList.Count-1 do
  begin
    if g_SoldatenList[Dummy].BasisID=BaseID then
    begin
      SetLength(Soldaten,length(Soldaten)+1);
      Soldaten[high(Soldaten)]:=Dummy;
    end;
  end;

  if length(Soldaten)=0 then
  begin
    result:=false;
    exit;
  end;

  result:=true;

  Index:=random(length(Soldaten));
  Assert((Index>=0) and (Index<length(Soldaten)));

  Name:=SSoldat+' '+g_SoldatenList[Soldaten[Index]].Name;
  
  g_SoldatenList.DeleteSoldat(Soldaten[Index]);
end;

end.
