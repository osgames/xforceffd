{********************************************************************************
*										*
* Copyright (C) 2004  X-Force Team - jim_raynor@web.de				*
*										*
* This file is part of X-Force: Fight For Destiny (X-Force).			*
*										*
* X-Force is free software; you can redistribute it and/or modify		*
* it under the terms of the GNU General Public License as published by		*
* the Free Software Foundation; either version 2 of the License, or		*
* (at your option) any later version.						*
*										*
* X-Force is distributed in the hope that it will be useful,			*
* but WITHOUT ANY WARRANTY; without even the implied warranty of		*
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the			*
* GNU General Public License for more details.					*
*										*
* You should have received a copy of the GNU General Public License		*
* along with X-Force; if not, write to the Free Software			*
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA	*
*										*
*********************************************************************************
*										*
* werkstatt_api bildet die Schnittstelle um auf die Werkstattliste zu zugreifen	*
*										*
* WICHTIG!! Bevor die Funktionen genutzt werden k�nnen muss �ber 		*
*           werkstatt_api_init die Werkstattliste gesetzt werden. Sollte durch  *
*           savegame_api_init geschehen sein.					*
*										*
********************************************************************************}

{$I ../Settings.inc}

unit werkstatt_api;

interface

uses WerkstattList, XForce_Types, NotifyList, classes, StringConst;

// Versorgt die API mit der Werkstattliste, damit die anderen Funktionen
// genutzt werden k�nnen
procedure werkstatt_api_init(WerkstattList: TWerkstattList);

// gibt die Anzahl der Techniker zur�ck
function  werkstatt_api_GetTechnikerCount: Integer; overload;

// gibt die Anzahl der Techniker der Basis zur�ck
function  werkstatt_api_GetTechnikerCount(BasisID: Cardinal): Integer; overload;

// gibt einen Techniker zur�ck
function  werkstatt_api_GetTechniker(Index: Integer): TTechniker;

// gibt den Index des Techniker mit der angegeben ID zur�ck
function  werkstatt_api_GetTechnikerIndex(ID: Cardinal; Check: Boolean = false): Integer;

// Gibt das w�chentliche Gehalt f�r aller Techniker zur�ck
function  werkstatt_api_GetWeeklySold: Integer; overload;

// Gibt das w�chentliche Gehalt f�r aller Techniker der Basis zur�ck
function  werkstatt_api_GetWeeklySold(BasisID: Cardinal): Integer; overload;

// Gibt die gesamten F�higkeiten von aller Techniker zur�ck
function  werkstatt_api_GetAllStrength: Integer; overload;

// Gibt die gesamten F�higkeiten von aller Techniker der Basis zur�ck
function  werkstatt_api_GetAllStrength(BasisID: Cardinal): Integer; overload;

// gibt zu einem Herstellungsauftrag die Anzahl der Techniker die
// an dem Projekt arbeiten und deren gesamte F�higkeit zur�ck
procedure werkstatt_api_GetProductInfo(ID: Cardinal; out Techniker, Strength: Integer);

// setzt den Techniker auf den Status Alphatron f�rdern
procedure werkstatt_api_SetMiningState(Index: Integer);

// setzt den Techniker auf den Status Einrichtung reparieren
procedure werkstatt_api_SetRepairStatus(Index: Integer);

// Ermittelt, ob der Techniker im Status Repair ist
function werkstatt_api_GetRepairStatus(Index: Integer): Boolean;

// setzt den Techniker von allen aufgaben Frei
procedure werkstatt_api_FreeTechniker(Index: Integer);

// Registriert einen Event, der aufgerufen wird, wenn sich an den Technikern, die
// Alphatron f�rdern etwas �ndert
function werkstatt_api_RegisterAlphatronMiningChangeHandler(Handler: TNotifyEvent): TEventHandle;

// Sucht den n�chstbesten Techniker heraus, der frei ist (gibt die ID des
// Technikers zur�ck)
function werkstatt_api_FindTechnikerForRepair(BaseID: Cardinal): Cardinal;

// Diese Funktion t�tet einen zuf�lligen Techniker innerhalb einer Basis
// ohne den Wohnraum freizugeben. Ben�tigt wird dies, um bei Zerst�rung einer
// Einrichtung Platz zu schaffen. Gibt false zur�ck, falls kein Techniker in der Basis
// stationiert ist.
function werkstatt_api_KillRandomTechnikerInBase(BaseID: Cardinal; out Name: String): Boolean;

// Diese Funktion pr�ft, ob noch genug Werkstattplatz f�r alle aktiven Techniker
// vorhanden ist. Dies wird bei Zerst�rung einer Einrichtung aufgerufen
function werkstatt_api_CheckWerkRoom(BaseID: Cardinal): String;

implementation

var
  g_WerkstattList: TWerkstattList;

procedure werkstatt_api_init(WerkstattList: TWerkstattList);
begin
  g_WerkstattList:=WerkstattList;
end;

function  werkstatt_api_GetTechnikerCount: Integer;
begin
  result:=g_WerkstattList.Count;
end;

function  werkstatt_api_GetTechnikerCount(BasisID: Cardinal): Integer;
begin
  result:=g_WerkstattList.CountInBase(BasisID);
end;

function  werkstatt_api_GetTechniker(Index: Integer): TTechniker;
begin
  Assert((Index>=0) and (Index<werkstatt_api_GetTechnikerCount));
  result:=g_WerkstattList[Index];
end;

function  werkstatt_api_GetTechnikerIndex(ID: Cardinal; Check: Boolean): Integer;
var
  Dummy: Integer;
begin
  result:=-1;
  for Dummy:=0 to g_WerkstattList.Count-1 do
  begin
    if g_WerkstattList[Dummy].TechnikerID=ID then
    begin
      result:=Dummy;
      exit;
    end;
  end;
  if Check then
    Assert(false);
end;

function  werkstatt_api_GetWeeklySold: Integer;
begin
  result:=g_WerkstattList.CalculateWeeklySold;
end;

function  werkstatt_api_GetWeeklySold(BasisID: Cardinal): Integer;
begin
  result:=g_WerkstattList.CalculateWeeklySold(BasisID);
end;

function  werkstatt_api_GetAllStrength: Integer;
var
  Dummy: Integer;
begin
  result:=0;
  for Dummy:=0 to g_WerkstattList.Count-1 do
    inc(result,round(g_WerkstattList.Techniker[Dummy].Strength));
end;

function  werkstatt_api_GetAllStrength(BasisID: Cardinal): Integer;
var
  Dummy: Integer;
begin
  result:=0;
  for Dummy:=0 to g_WerkstattList.Count-1 do
    if g_WerkstattList.Techniker[Dummy].BasisID=BasisID then
      inc(result,round(g_WerkstattList.Techniker[Dummy].Strength));
end;

procedure werkstatt_api_GetProductInfo(ID: Cardinal; out Techniker, Strength: Integer);
var
  Dummy: Integer;
  Index: Integer;
begin
  Strength:=0;
  Techniker:=0;

  for Dummy:=0 to g_WerkstattList.Count-1 do
  begin
    if g_WerkstattList.Techniker[Dummy].ProjektID=ID then
    begin
      inc(Techniker);
      inc(Strength,round(g_WerkstattList.Techniker[Dummy].Strength));
    end;
  end;
end;

procedure werkstatt_api_SetMiningState(Index: Integer);
begin
  g_WerkstattList.SetMining(Index);
end;

procedure werkstatt_api_SetRepairStatus(Index: Integer);
begin
  g_WerkstattList.SetRepair(Index);
end;

function werkstatt_api_GetRepairStatus(Index: Integer): Boolean;
begin
  Assert((Index>=0) and (Index<werkstatt_api_GetTechnikerCount));
  result:=g_WerkstattList.Techniker[Index].Repair;
end;

procedure werkstatt_api_FreeTechniker(Index: Integer);
begin
  g_WerkstattList.FreeTechniker(Index);
end;

function werkstatt_api_RegisterAlphatronMiningChangeHandler(Handler: TNotifyEvent): TEventHandle;
begin
  result:=g_WerkstattList.NotifyList.RegisterEvent(EVENT_ONCHANGEALPHATRONMINING,Handler);
end;

function werkstatt_api_FindTechnikerForRepair(BaseID: Cardinal): Cardinal;
var
  Dummy: Integer;
begin
  result:=0;
  for Dummy:=0 to g_WerkstattList.Count-1 do
  begin
    if g_WerkstattList[Dummy].BasisID=BaseID then
    begin
      if g_WerkstattList.IsTechnikerFree(Dummy) then
      begin
        result:=g_WerkstattList[Dummy].TechnikerID;
        exit;
      end;
    end;
  end;
end;

function werkstatt_api_KillRandomTechnikerInBase(BaseID: Cardinal; out Name: String): Boolean;
var
  Techniker: Array of Integer;
  Dummy    : Integer;
  Index    : Integer;
begin
  for Dummy:=0 to g_WerkstattList.Count-1 do
  begin
    if g_WerkstattList[Dummy].BasisID=BaseID then
    begin
      SetLength(Techniker,length(Techniker)+1);
      Techniker[high(Techniker)]:=Dummy;
    end;
  end;

  if length(Techniker)=0 then
  begin
    result:=false;
    exit;
  end;

  result:=true;

  Index:=random(length(Techniker));
  Assert((Index>=0) and (Index<length(Techniker)));

  Name:=STechniker+' '+g_WerkstattList[Techniker[Index]].Name;
  
  g_WerkstattList.DeleteTechniker(Techniker[Index],false);
end;

function werkstatt_api_CheckWerkRoom(BaseID: Cardinal): String;
begin
  result:=g_WerkstattList.CheckWerkRoom(BaseID);
end;

end.
