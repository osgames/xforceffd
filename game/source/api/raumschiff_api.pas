{********************************************************************************
*										*
* Copyright (C) 2004  X-Force Team - jim_raynor@web.de				*
*										*
* This file is part of X-Force: Fight For Destiny (X-Force).			*
*										*
* X-Force is free software; you can redistribute it and/or modify		*
* it under the terms of the GNU General Public License as published by		*
* the Free Software Foundation; either version 2 of the License, or		*
* (at your option) any later version.						*
*										*
* X-Force is distributed in the hope that it will be useful,			*
* but WITHOUT ANY WARRANTY; without even the implied warranty of		*
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the			*
* GNU General Public License for more details.					*
*										*
* You should have received a copy of the GNU General Public License		*
* along with X-Force; if not, write to the Free Software			*
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA	*
*										*
*********************************************************************************
*										*
* raumschiff_api bildet die Schnittstelle zu einer Raumschiffliste, die 	*
* die Raumschiff verwaltet                                                      *
*										*
* WICHTIG!! Bevor die Funktionen genutzt werden k�nnen muss �ber 		*
*           raumschiff_api_init die Raumschiffliste gesetzt werden. Sollte durch*
*           savegame_api_init geschehen sein.					*
*										*
********************************************************************************}

{$I ../Settings.inc}

unit raumschiff_api;

interface

uses
  SysUtils, RaumschiffList, DXDraws, XForce_types, graphics, StringConst;

procedure raumschiff_api_init(RaumschiffList: TRaumschiffList);

// gibt die Anzahl der Raumschiffe in der Liste zur�ck (ACHTUNG: Dazu z�hlen auch
// Transporter die Waren transportieren)
function  raumschiff_api_GetRaumschiffCount: Integer;

// gibt zu einem Raumschiff den Namen zur�ck (Leerstring, wenn ung�ltige ID)
function  raumschiff_api_GetRaumschiffName(ID: Integer): String;

// gibt das Raumschiff zur�ck (nil wenn ung�ltige ID bzw. Exception wenn Check = true)
function  raumschiff_api_GetRaumschiff(ID: Integer;Check: Boolean = false): TRaumschiff;

function  raumschiff_api_GetIndexedRaumschiff(Index: Integer): TRaumschiff;

// gibt den Index des Raumschiffmodells zur�ck
function  raumschiff_api_GetRaumschiffModel(ID: Integer): Integer;

// Transport von Ausr�stung durchf�hren
function  raumschiff_api_DoTransport(FromPos,ToPos: TFloatPoint;const Item: TLagerItem;Preis: Integer;Kauf: boolean): TRaumschiff;

// Transport von Alphatron durchf�hren
function  raumschiff_api_DoAlphatronTransport(FromPos,ToPos: TFloatPoint;Alphatron: Integer): TRaumschiff;

// Ermittelt die W�chentlichen Kosten f�r alle Raumschiffe
function  raumschiff_api_GetWeeklyCost: Integer; overload;

// Ermittelt die W�chentlichen Kosten f�r alle Raumschiffe der Basis
function  raumschiff_api_GetWeeklyCost(BasisID: Cardinal): Integer; overload;

// Pr�ft, ob ein Punkt auf der Erde in Sensorreichweite der Basen bzw.
// Raumschiffen ist
function  raumschiff_api_InScanArea(Position: TFloatPoint): Boolean;

// Ermittelt das Raumschiff, dass am N�chsten an Position ist
function  raumschiff_api_getNearestSchiff(Position: TFloatPoint): TRaumschiff;

// Zeichnet das Symbol eines Raumschiffes
procedure raumschiff_api_DrawIcon(Surface: TDirectDrawSurface; x, y: Integer; WZ: Integer);

// Zerst�rt ein zuf�lliges Raumschiff in der Basis ohne den Hangarraum freizugeben
// Wird f�r die Zerst�rung einer Einrichtung ben�tigt
function raumschiff_api_DestroyRandomShip(BaseID: Cardinal; out Name: String): Boolean;

implementation

uses
  EarthCalcs, earth_api;

var
  g_raumschiffList: TRaumschiffList;

procedure raumschiff_api_init(RaumschiffList: TRaumschiffList);
begin
  g_raumschiffList:=RaumschiffList;
  earth_api_init(RaumschiffList.Scanner);
end;

function  raumschiff_api_GetRaumschiffCount: Integer;
begin
  result:=g_raumschiffList.Count;
end;

function raumschiff_api_GetRaumschiffName(ID: Integer): String;
var
  Raum: TRaumschiff;
begin
  Raum:=g_raumschiffList.SchiffOfID(ID);
  if Raum<>nil then
    result:=Raum.Name
  else
    result:='';
end;

function raumschiff_api_GetRaumschiff(ID: Integer;Check: Boolean): TRaumschiff;
begin
  result:=g_raumschiffList.SchiffOfID(Integer(ID));
  if Check then
    Assert(result<>nil);
end;

function  raumschiff_api_GetIndexedRaumschiff(Index: Integer): TRaumschiff;
begin
  result:=g_raumschiffList.Schiffe[Index];
  Assert(result<>nil);
end;

function raumschiff_api_DoTransport(FromPos,ToPos: TFloatPoint;const Item: TLagerItem;Preis: Integer;Kauf: boolean): TRaumschiff;
begin
  result:=g_raumschiffList.DoTransport(FromPos,ToPos,Item,Preis,Kauf);
end;

function  raumschiff_api_DoAlphatronTransport(FromPos,ToPos: TFloatPoint;Alphatron: Integer): TRaumschiff;
begin
  result:=g_raumschiffList.DoAlphatronTransport(FromPos,ToPos,Alphatron);
end;

function  raumschiff_api_GetWeeklyCost: Integer;
begin
  result:=g_raumschiffList.GetWeeklyCost;
end;

function  raumschiff_api_GetWeeklyCost(BasisID: Cardinal): Integer;
begin
  result:=g_raumschiffList.GetWeeklyCost(BasisID);
end;

function  raumschiff_api_InScanArea(Position: TFloatPoint): Boolean;
begin
  result:=g_raumschiffList.InScanArea(Position);
end;

function  raumschiff_api_getNearestSchiff(Position: TFloatPoint): TRaumschiff;
var
  Dummy  : Integer;
  Temp   : TRaumschiff;
  Entf   : Integer;
  LEntf  : Integer;
begin
  result:=nil;
  LEntf:=high(LEntf);
  for Dummy:=0 to g_raumschiffList.Count-1 do
  begin
    Temp:=g_raumschiffList[Dummy];
    if (Temp.Status in [ssAir,ssBackToBasis]) then
    begin
      Entf:=round(EarthEntfernung(Position,Temp.Position));
      if Entf<=LEntf then
      begin
        result:=Temp;
        LEntf:=Entf;
      end;
    end;
  end;
end;

procedure raumschiff_api_DrawIcon(Surface: TDirectDrawSurface; x, y: Integer; WZ: Integer);
begin
  g_raumschiffList.DrawIcon(Surface,x,y,WZ);
end;

function  raumschiff_api_GetRaumschiffModel(ID: Integer): Integer;
begin
  result:=g_raumschiffList.ModelOfID(ID);
  Assert(result<>-1);
end;

function raumschiff_api_DestroyRandomShip(BaseID: Cardinal; out Name: String): Boolean;
var
  Schiffe : Array of Integer;
  Dummy   : Integer;
  Index   : Integer;
begin
  for Dummy:=0 to g_RaumschiffList.Count-1 do
  begin
    if g_RaumschiffList[Dummy].GetHomeBasis.ID=BaseID then
    begin
      SetLength(Schiffe,length(Schiffe)+1);
      Schiffe[high(Schiffe)]:=Dummy;
    end;
  end;

  if length(Schiffe)=0 then
  begin
    result:=false;
    exit;
  end;

  result:=true;

  Index:=random(length(Schiffe));
  Assert((Index>=0) and (Index<length(Schiffe)));

  Name:=Format(ST0503110001,[g_RaumschiffList[Schiffe[Index]].Name]);

  g_RaumschiffList[Schiffe[Index]].DestroyShip;
end;

end.
