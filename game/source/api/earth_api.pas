{********************************************************************************
*										*
* Copyright (C) 2004  X-Force Team - jim_raynor@web.de				*
*										*
* This file is part of X-Force: Fight For Destiny (X-Force).			*
*										*
* X-Force is free software; you can redistribute it and/or modify		*
* it under the terms of the GNU General Public License as published by		*
* the Free Software Foundation; either version 2 of the License, or		*
* (at your option) any later version.						*
*										*
* X-Force is distributed in the hope that it will be useful,			*
* but WITHOUT ANY WARRANTY; without even the implied warranty of		*
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the			*
* GNU General Public License for more details.					*
*										*
* You should have received a copy of the GNU General Public License		*
* along with X-Force; if not, write to the Free Software			*
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA	*
*										*
*********************************************************************************
*										*
* earth_api bildet die zum Geoscape und stellt unter anderem Funktionen zur     *
* Verf�gung um die Sensormap zu aktualisieren                                   *
*                                                                               *
* WICHTIG!! Bevor die Funktionen genutzt werden k�nnen muss �ber 		*
*           earth_api_init die Scanner-Objekt gesetzt werden. Sollte durch      *
*           raumschiff_api geschehen sein.					*
*										*
********************************************************************************}

unit earth_api;

interface

uses
  SysUtils, RaumschiffList, DXDraws, XForce_types, graphics, StringConst;

procedure earth_api_init(ScanWorld: TScanWorld);

// gibt true zu�rck, wenn sich seit dem letzten Aufruf am Scannerbereich �nderungen
// ergeben haben (Aufrufe von earth_api_ChangeScanArea)
function earth_api_AktuScanArea: Boolean;

// Durch diesen Aufruf wird beim n�chsten Aufruf von earth_api_AktuScanArea
// die Sensorkarte neu berechnet
procedure earth_api_ChangeScanArea;

// Erstellt einen Punkt auf der Erde
function earth_api_RandomEarthPoint(OverLand: Boolean = false): TFloatPoint;

// Ermittelt, ob ein Punkt auf der Erde �ber Land oder Wasser ist
function earth_api_PointOverLand(Point: TFloatPoint): Boolean;

implementation

uses
  KD4Utils, UFOList;

var
  g_ScanWorld: TScanWorld;

procedure earth_api_init(ScanWorld: TScanWorld);
begin
  g_ScanWorld:=ScanWorld;
end;

function earth_api_AktuScanArea: Boolean;
begin
  result:=g_ScanWorld.AktuScanArea;
end;

function earth_api_RandomEarthPoint(OverLand: Boolean): TFloatPoint;
var
  Zaehler: Integer;
begin
  if not OverLand then
    result:=FloatPoint(random*360,random*180)
  else
  begin
    Zaehler:=0;
    repeat
      if Zaehler=0 then
      begin
        result.Y:=random*180;
        Zaehler:=50;
      end;

      result.X:=random*360;
      dec(Zaehler)
    until PointOverLand(result);
  end;
end;

function earth_api_PointOverLand(Point: TFloatPoint): Boolean;
begin
  result:=PointOverLand(Point);
end;

procedure earth_api_ChangeScanArea;
begin
  g_ScanWorld.ChangeScanArea(nil);
end;

end.

