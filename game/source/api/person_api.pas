{********************************************************************************
*										*
* Copyright (C) 2004  X-Force Team - jim_raynor@web.de				*
*										*
* This file is part of X-Force: Fight For Destiny (X-Force).			*
*										*
* X-Force is free software; you can redistribute it and/or modify		*
* it under the terms of the GNU General Public License as published by		*
* the Free Software Foundation; either version 2 of the License, or		*
* (at your option) any later version.						*
*										*
* X-Force is distributed in the hope that it will be useful,			*
* but WITHOUT ANY WARRANTY; without even the implied warranty of		*
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the			*
* GNU General Public License for more details.					*
*										*
* You should have received a copy of the GNU General Public License		*
* along with X-Force; if not, write to the Free Software			*
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA	*
*										*
*********************************************************************************
*										*
* person_api bildet die Schnittstelle zu einer Personenliste, die Namen 	*
* erstellt und Gesichter von Personen zeichnet.					*
*										*
* WICHTIG!! Bevor die Funktionen genutzt werden k�nnen muss �ber 		*
*           person_api_init die Personenliste gesetzt werden. Sollte durch      *
*           savegame_api_init geschehen sein.					*
*										*
********************************************************************************}

{$I ../Settings.inc}

unit person_api;

interface

uses
  PersonList, DXDraws;

procedure person_api_init(PersonList: TPersonList);

// Zuf�lligen Namen ausgeben
function person_api_GenerateName: String;

// Gibt einen Namen
function person_api_GetFixedName: String;

// Zuf�lliges Gesicht ausgeben
function person_api_GenerateFace: Integer;

// Anzahl der Gesichter ausgeben
function person_api_GetFaceCount: Integer;

// Gesichter Zeichnen (normale Gr��e f�r Verwaltungsbildschirme)
procedure person_api_DrawFace(Surface: TDirectDrawSurface;x,y,Image: Integer);

// Gesichter Zeichnen (Klein f�r Soldaten�bersicht im Bodeneinsatz)
procedure person_api_DrawSmallFace(Surface: TDirectDrawSurface;x,y,Image: Integer);

implementation

uses
  Blending, KD4Utils, classes, windows, DirectDraw, Defines;

var
  g_PersonList: TPersonList;

procedure person_api_init(PersonList: TPersonList);
begin
  g_PersonList:=PersonList;
end;

function person_api_GenerateName: String;
begin
  result:=g_PersonList.CreateName;
end;

function person_api_GetFixedName: String;
begin
  result:=g_PersonList.CreateName(true);
end;

function person_api_GenerateFace: Integer;
begin
  result:=random(g_PersonList.FaceCount);
end;

function person_api_GetFaceCount: Integer;
begin
  result:=g_PersonList.FaceCount;
end;

procedure person_api_DrawFace(Surface: TDirectDrawSurface;x,y,Image: Integer);
var
  SolRect: TRect;
  Mem    : TDDSurfaceDesc;
begin
  g_PersonList.DrawFace(Surface,x,y,Image);

  SolRect:=Rect(X,Y,PersonImageWidth+x,PersonImageHeight+y);

  Surface.Lock(Mem);
  Surface.Unlock;
  Blending.Rectangle(Surface,Mem,SolRect,bcBlack);
end;

procedure person_api_DrawSmallFace(Surface: TDirectDrawSurface;x,y,Image: Integer);
begin
  g_PersonList.DrawSmallFace(Surface,x,y,Image);
end;

end.

