{********************************************************************************
*										*
* Copyright (C) 2004  X-Force Team - jim_raynor@web.de				*
*										*
* This file is part of X-Force: Fight For Destiny (X-Force).			*
*										*
* X-Force is free software; you can redistribute it and/or modify		*
* it under the terms of the GNU General Public License as published by		*
* the Free Software Foundation; either version 2 of the License, or		*
* (at your option) any later version.						*
*										*
* X-Force is distributed in the hope that it will be useful,			*
* but WITHOUT ANY WARRANTY; without even the implied warranty of		*
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the			*
* GNU General Public License for more details.					*
*										*
* You should have received a copy of the GNU General Public License		*
* along with X-Force; if not, write to the Free Software			*
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA	*
*										*
*********************************************************************************
*										*
* ufopaedie_api bildet die Schnittstelle um Objekte in der UFO-P�die anzeigen 	*
* zu lassen									*
*										*
********************************************************************************}

{$I ../Settings.inc}

unit ufopaedie_api;

interface

uses InfoUFOPaedie, XForce_Types, Defines, VerOrgan;

// Versorgt die API mit der UFOP�die Ansicht, damit die anderen Funktionen
// genutzt werden k�nnen
procedure ufopaedie_api_init(UFOPaedie: TInfoUFOPaedie);

// Versorgt die API mit der L�nder Ansicht, damit die anderen Funktionen
// genutzt werden k�nnen
procedure ufopaedie_api_initCountrys(VerCountry: TVerOrgan);

// Zeigt einen Eintrag aus der UFOP�die an, wobei EntryType den Typ (Ausr�stung,
// Raumschiff, Alien) angibt und Index den entsprechenden Index in der Liste
procedure ufopaedie_api_ShowEntry(EntryType: TUFOPadieEntry; Index: Integer);

// Wechselt zur L�nder-Seite und zeigt das �bergebene Land an
procedure ufopaedie_api_ShowCountryInfo(Country: Integer);

implementation

var
  g_UFOPaedie    : TInfoUFOPaedie;
  g_VerCountries : TVerOrgan;

procedure ufopaedie_api_init(UFOPaedie: TInfoUFOPaedie);
begin
  g_UFOPaedie:=UFOPaedie;
end;

procedure ufopaedie_api_initCountrys(VerCountry: TVerOrgan);
begin
  g_VerCountries:=VerCountry;
end;

procedure ufopaedie_api_ShowEntry(EntryType: TUFOPadieEntry; Index: Integer);
begin
  g_UFOPaedie.DontAllowChangePageViaNavigator;
  g_UFOPaedie.ShowUFOPadieEntry(EntryType,Index);
  g_UFOPaedie.ChangePage(PageUfoPadie);
end;

procedure ufopaedie_api_ShowCountryInfo(Country: Integer);
begin
  Assert(g_VerCountries<>nil);
  g_VerCountries.ShowOrgan:=Country;
  g_VerCountries.ChangePage(PageVerOrgan);
end;

end.

