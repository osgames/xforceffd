{********************************************************************************
*										*
* Copyright (C) 2004  X-Force Team - jim_raynor@web.de				*
*										*
* This file is part of X-Force: Fight For Destiny (X-Force).			*
*										*
* X-Force is free software; you can redistribute it and/or modify		*
* it under the terms of the GNU General Public License as published by		*
* the Free Software Foundation; either version 2 of the License, or		*
* (at your option) any later version.						*
*										*
* X-Force is distributed in the hope that it will be useful,			*
* but WITHOUT ANY WARRANTY; without even the implied warranty of		*
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the			*
* GNU General Public License for more details.					*
*										*
* You should have received a copy of the GNU General Public License		*
* along with X-Force; if not, write to the Free Software			*
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA	*
*										*
*********************************************************************************
*										*
* alien_api bildet die Schnittstelle zu einer Alienliste, die die Aliens 	*
* verwaltet.
*										*
* WICHTIG!! Bevor die Funktionen genutzt werden k�nnen muss �ber 		*
*           alien_api_init die Alienliste gesetzt werden. Sollte durch 		*
*           savegame_api_init geschehen sein.					*
*										*
********************************************************************************}

{$I ../Settings.inc}

unit script_api;

interface

uses
  XForce_Types, BasisListe, RaumschiffList;

// Funktionen f�r die lager_api
function lager_api_GetItemInfo(Index: Integer): TLagerItem;
function lager_api_GetItemByID(ID: Cardinal): TLagerItem;

function lager_api_CheckItem(ID: Cardinal): Boolean;

// Funktionen f�r die country_api
function country_api_GetCountry(Index: Integer): TLand;

// Funktionen f�r die alien_api
function alien_api_GetRandomAlien: TAlien;
function alien_api_GetAlien(ID: Cardinal): TAlien;

// Funktionen f�r die raumschiff_api
function raumschiff_api_DoTransportToBase(FromCountry: TLand; ToBase: TBasis; Item: TLagerItem; Count: Integer): TRaumschiff;
function raumschiff_api_DoTransportToCountry(FromBase: TBasis; ToCountry: TLand; Item: TLagerItem; Count: Integer): TRaumschiff;

implementation

uses lager_api, country_api, game_api, alien_api, raumschiff_api;

function lager_api_GetItemInfo(Index: Integer): TLagerItem;
begin
  result:=lager_api_GetItem(Index)^;
end;

function lager_api_GetItemByID(ID: Cardinal): TLagerItem;
begin
  result:=lager_api_GetItem(ID,true)^;
end;

function lager_api_CheckItem(ID: Cardinal): Boolean;
begin
  result:=lager_api_GetItem(ID,false)<>nil;
end;

function country_api_GetCountry(Index: Integer): TLand;
begin
  result:=country_api.country_api_GetCountry(Index)^;
end;

function alien_api_GetRandomAlien: TAlien;
begin
  result:=alien_api.alien_api_GetRandomAlien^;
end;

function alien_api_GetAlien(ID: Cardinal): TAlien;
begin
  result:=alien_api.alien_api_GetAlienInfo(ID)^;
end;

function raumschiff_api_DoTransportToBase(FromCountry: TLand; ToBase: TBasis; Item: TLagerItem; Count: Integer): TRaumschiff;
begin
  Item.Anzahl:=Count;
  result:=raumschiff_api_DoTransport(country_api_GetRandomLandPosition(FromCountry.InfoIndex,true),ToBase.BasisPos,Item,0,true);
end;

function raumschiff_api_DoTransportToCountry(FromBase: TBasis; ToCountry: TLand; Item: TLagerItem; Count: Integer): TRaumschiff;
begin
  Item.Anzahl:=Count;
  result:=raumschiff_api_DoTransport(FromBase.BasisPos,country_api_GetRandomLandPosition(ToCountry.InfoIndex,true),Item,0,false);
end;

end.
