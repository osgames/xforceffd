{********************************************************************************
*										*
* Copyright (C) 2004  X-Force Team - jim_raynor@web.de				*
*										*
* This file is part of X-Force: Fight For Destiny (X-Force).			*
*										*
* X-Force is free software; you can redistribute it and/or modify		*
* it under the terms of the GNU General Public License as published by		*
* the Free Software Foundation; either version 2 of the License, or		*
* (at your option) any later version.						*
*										*
* X-Force is distributed in the hope that it will be useful,			*
* but WITHOUT ANY WARRANTY; without even the implied warranty of		*
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the			*
* GNU General Public License for more details.					*
*										*
* You should have received a copy of the GNU General Public License		*
* along with X-Force; if not, write to the Free Software			*
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA	*
*										*
*********************************************************************************
*										*
* savegame_api bildet die Schnittstelle zum aktuellen Spielstand		*
*										*
* WICHTIG!! Bevor die Funktionen genutzt werden k�nnen muss �ber 		*
*           savegame_api_init das Savegameobject (TKD4SaveGame) gesetzt werden  *
*           Dadurch werden gleichzeitig die anderen Apis (Alien_api, Lager ...) *
*           Initsialisiert.							*
*										*
********************************************************************************}

{$I ../Settings.inc}

unit savegame_api;

interface

uses
  KD4SaveGame, soldaten_api, StringConst, XForce_types, Classes, ArchivFile,
  ExtRecord;

type
  TGameStatusArray = Array of TGameStatus;

// setzt das Savegame zur weiteren Verwendung in der Unit
procedure savegame_api_Init(Savegame: TKD4SaveGame);

// Speichert im Header einer Savegame-Datei ein neuen Spielstand
procedure savegame_api_RegisterSaveGame(Status: TGameStatus; Archiv: TArchivFile);

// Ermittelt alle Spielst�nde innerhalb einer Savegame-Datei
procedure savegame_api_ReadSaveGames(Archiv: TArchivFile; var SaveGames: TGameStatusArray);

// Setzt die Sprache, die beim Erstellen eines Spiels genutzt werden soll.
procedure savegame_api_SetGameSetLanguage(Language: String);
function savegame_api_GetGameSetLanguage: string;

// Gibt das aktuelle Datum zur�ck
function  savegame_api_GetDate: TKD4Date;

// Gibt das aktuelle Datum als String zur�ck
function  savegame_api_GetDateString(Short: Boolean = false): String;

// Gibt den Namen des Spielers zur�ck
function  savegame_api_GetPlayerName: String;

// Gibt die Credits des Spielers zur�ck
function  savegame_api_GetCredits: Integer;

// Gibt den Schwierigkeitsgrad des aktuellen Spiels zur�ck
function  savegame_api_GetGameDifficult: TGameDifficult;

// zieht das Geld vom Spielstand ab
// raiseException gibt an, ob eine Exception ausgel�st werden soll, sobald nicht genug Geld zur Verf�gung steht
function savegame_api_NeedMoney(Money: Int64; BuchType: TKapital; raiseException: boolean = false): Boolean;
procedure savegame_api_FreeMoney(Money: Int64; BuchType: TKapital);

// zieht Alphatron vom Spielstand ab
// NeedAlphatron gibt zur�ck, wieviel Alphatron tats�chlich zur Verf�gung gestellt
// wurde
function savegame_api_NeedAlphatron(Alphatron: Integer; BasisID: Cardinal = 0): Integer;
procedure savegame_api_FreeAlphatron(Alphatron: Integer; BasisID: Cardinal = 0);

// Sendet eine Nachricht die im Geoscape angezeigt wird
procedure savegame_api_Message(Message: String; MType: TLongMessage; Objekt: TObject = nil);

// Gibt Informationen zur letzten eingetroffenen Nachricht zur�ck. Sollte verwendet
// werden, um auf die Nachricht innerhalb eines MessageHandlers zuzugreifen
function savegame_api_GetLastMessage: TGameMessage;

// Bucht Punkte f�r die Wochenbilanz
procedure savegame_api_BuchPunkte(Punkte: Integer; Art: TPunkteBuch);

// Gibt zugriff auf den TNewGameRec mit dem Informationen zum neuen Spiel �bergeben
// werden.
function savegame_api_GetNewGameData: TNewGameRec;

{ Funktionen zum Zugriff auf die Daten des Spielsatzes zum Spielstand }
// Legt den Spielsatz fest und l�dt die Daten. Zugriff erfolgt dann �ber die
// savegame_api_GameSetGet... Funktionen
procedure savegame_api_SetGameSet(Archiv: TArchivFile);

function savegame_api_GetGameSet: TSetIdentifier;

function savegame_api_GameSetGetProjects: TRecordArray;
function savegame_api_GameSetGetAliens: TRecordArray;
function savegame_api_GameSetGetUFOs: TRecordArray;
function savegame_api_GameSetGetSkripts: TRecordArray;

{ Funktionen zum Registrieren von Ereignishandlern }

// Registriert eine Funktion, die aufgerufen wird, wenn eine Nachricht gesendet
// wurde
procedure savegame_api_RegisterMessageHandler(Handler: TNotifyEvent);

// Registriert Funktionen, um Daten im Spielstand abzuspeichern bzw. zu laden
// Die ID muss eindeutig sein, um die Datenteile identifizieren zu k�nnen
// NewHandler wird auf gerufen, wenn der Datenteil beim Laden nicht im Spielstand
// vorhanden ist
procedure savegame_api_RegisterCustomSaveHandler(SaveHandler: TCustomSaveGameEvent; LoadHandler: TCustomSaveGameEvent; NewHandler: TNotifyEvent; ID: Cardinal);

// Registriert einen Handler, der aufgerufen wird, wenn ein Spielsatz in KD4Savegame
// geladen wird. Die Handler sollten darauf die entsprechenden Spielsatzdaten laden
// und verarbeiten
procedure savegame_api_RegisterLoadGameSetHandler(Handler: TNotifyEvent);

// Registriert einen Handler, der aufgerufen wird, sobald ein Spiel gestartet wird
// Dabei ist es unerheblich, ob ein neues Spiel begonnen wurde oder ein altes geladen
// Bei einem neuen Spiel wird der StartGameHandler nach dem NewGameHandler aufgerufen
procedure savegame_api_RegisterStartGameHandler(Handler: TNotifyEvent);

// Registriert einen Handler, der aufgerufen wird, sobald ein neues Spiel gestartet wird
procedure savegame_api_RegisterNewGameHandler(Handler: TNotifyEvent);

// Registriert einen Handler, der aufgerufen wird, sobald ein Spielstand komplett
// geladen wurde. Der Handler wird aber noch vor dem StartGameHandlern aufgerufen.
procedure savegame_api_RegisterAfterLoadHandler(Handler: TNotifyEvent);

// Registriert einen Handler, der aufgerufen wird, sobald eine Woche vor�ber ist
procedure savegame_api_RegisterWeekEndHandler(Handler: TNotifyEvent);

// Registriert einen Handler, der aufgerufen wird, sobald eine Stunde vor�ber ist
procedure savegame_api_RegisterHourEndHandler(Handler: TNotifyEvent);

// Wird die Funktion aufgerufen werden ein paar Funktionen deaktiviert, die im
// Missionseditor nicht benutzt werden
// Zur Zeit wird nur verhindert, dass die Skripte im Spielsatz geladen werden
procedure savegame_api_SetMissionEditorMode;

implementation

uses
  basis_api, forsch_api, country_api, lager_api, alien_api, person_api,
  raumschiff_api, ufo_api, town_api, einsatz_api, werkstatt_api, Defines,
  KD4Utils, sysutils, array_utils, record_utils, gameset_api;

var
  g_savegame      : TKD4SaveGame;
  g_language      : String;

  g_LoadedGameSet : Boolean;
  g_Projects      : TRecordArray;
  g_Aliens        : TRecordArray;
  g_UFOs          : TRecordArray;
  g_Skripts       : TRecordArray;
  g_EditorMode    : Boolean = false;

procedure savegame_api_Init(Savegame: TKD4SaveGame);
begin
  g_savegame:=SaveGame;
end;

procedure savegame_api_SetGameSetLanguage(Language: String);
begin
  g_language:=Language;
end;

function savegame_api_GetGameSet: TSetIdentifier;
begin
  Assert(g_LoadedGameSet);
  result:=g_savegame.Gameset;
end;

function savegame_api_GetGameSetLanguage: string;
begin
  result:=g_language;
end;

function  savegame_api_GetGameDifficult: TGameDifficult;
begin
  result:=g_savegame.Difficult;
end;

function savegame_api_NeedMoney(Money: Int64; BuchType: TKapital; raiseException: boolean = false): Boolean;
begin
  {$IFDEF ENDLESSMONEY}
    result:=true;
  {$ELSE}
    result:=g_saveGame.Kapital>=Money;
    if result then
      g_savegame.BuchungKapital(-Money,BuchType);

    if raiseException and (not result) then
      raise ENotEnoughMoney.Create(ENoMoney);
  {$ENDIF}
end;

procedure savegame_api_FreeMoney(Money: Int64; BuchType: TKapital);
begin
  g_savegame.BuchungKapital(Money,BuchType);
end;

function savegame_api_NeedAlphatron(Alphatron: Integer;BasisID: Cardinal): Integer;
begin
  result:=basis_api_GetBasisFromID(BasisID).NeedAlphatron(Alphatron);
end;

procedure savegame_api_FreeAlphatron(Alphatron: Integer; BasisID: Cardinal);
begin
  basis_api_GetBasisFromID(BasisID).FreeAlphatron(Alphatron);
end;

procedure savegame_api_Message(Message: String; MType: TLongMessage; Objekt: TObject = nil);
begin
  g_savegame.SendMessage(nil,Message,MType,Objekt);
end;

procedure savegame_api_RegisterMessageHandler(Handler: TNotifyEvent);
begin
  g_savegame.NotifyList.RegisterEvent(EVENT_SAVEGAMEMESSAGE,Handler);
end;

function savegame_api_GetLastMessage: TGameMessage;
begin
  result:=g_savegame.GetLastMessage;
end;

procedure savegame_api_BuchPunkte(Punkte: Integer; Art: TPunkteBuch);
begin
  g_savegame.BuchungPunkte(Punkte,Art);
end;

function savegame_api_GetDate: TKD4Date;
begin
  result:=g_savegame.Date;
end;

function  savegame_api_GetDateString(Short: Boolean = false): String;
begin
  result:=g_SaveGame.DateString(Short);
end;

function  savegame_api_GetPlayerName: String;
begin
  result:=g_savegame.PlayerName;
end;

function  savegame_api_GetCredits: Integer;
begin
  result:=g_savegame.Kapital;
end;

procedure savegame_api_RegisterCustomSaveHandler(SaveHandler: TCustomSaveGameEvent; LoadHandler: TCustomSaveGameEvent; NewHandler: TNotifyEvent; ID: Cardinal);
begin
  g_savegame.RegisterCustomSaveHandler(SaveHandler,LoadHandler,NewHandler,ID);
end;

procedure savegame_api_RegisterNewGameHandler(Handler: TNotifyEvent);
begin
  g_savegame.NotifyList.RegisterEvent(EVENT_SAVEGAMENEWGAME,Handler);
end;

procedure savegame_api_RegisterStartGameHandler(Handler: TNotifyEvent);
begin
  g_savegame.NotifyList.RegisterEvent(EVENT_SAVEGAMESTARTGAME,Handler);
end;

procedure savegame_api_RegisterAfterLoadHandler(Handler: TNotifyEvent);
begin
  g_savegame.NotifyList.RegisterEvent(EVENT_SAVEGAMELOADGAME,Handler);
end;

procedure savegame_api_RegisterWeekEndHandler(Handler: TNotifyEvent);
begin
  g_savegame.NotifyList.RegisterEvent(EVENT_SAVEGAMEWEEKEND,Handler);
end;

procedure savegame_api_RegisterHourEndHandler(Handler: TNotifyEvent);
begin
  g_savegame.NotifyList.RegisterEvent(EVENT_SAVEGAMEHOUREND,Handler);
end;

procedure savegame_api_RegisterLoadGameSetHandler(Handler: TNotifyEvent);
begin
  g_savegame.NotifyList.RegisterEvent(EVENT_SAVEGAMELOADGAMESET,Handler);
end;

type
  TGamesArray = Array of TGameStatus;

procedure savegame_api_RegisterSaveGame(Status: TGameStatus; Archiv: TArchivFile);
var
  Index    : Integer;
  Count    : Integer;
  Dummy    : Integer;
  Stream   : TMemoryStream;
  Games    : TGamesArray;
begin
  Index:=-1;
  if Archiv.ExistRessource(RNSaveGame) then
  begin
    Archiv.OpenRessource(RNSaveGame);
    Archiv.Stream.Read(Count,SizeOf(Count));

    SetLength(Games,Count);
    for Dummy:=0 to Count-1 do
    begin
      Archiv.Stream.Read(Games[Dummy],SizeOf(Games[Dummy]));
      if Games[Dummy].Name=Status.Name then
        Index:=Dummy;
    end;
    Archiv.CloseRessource;

    // Eintrag entfernen
    if Index<>-1 then
      DeleteArray(Addr(Games),TypeInfo(TGamesArray),Index);
  end
  else
  begin
    Count:=0;
    SetLength(Games,0);
  end;

  SetLength(Games,length(Games)+1);
  for Dummy:=high(Games) downto 1 do
  begin
    Games[Dummy]:=Games[Dummy-1];
  end;

  if Games[0].Name=AutosaveName then
    Games[1]:=Status
  else
    Games[0]:=Status;

  // Neue Liste speicheren
  Stream:=TMemoryStream.Create;
  Stream.Clear;
  Count:=length(Games);
  Stream.Write(Count,SizeOf(Count));

  for Dummy:=0 to Count-1 do
    Stream.Write(Games[Dummy],SizeOf(Games[Dummy]));

  Archiv.ReplaceRessource(Stream,RNSaveGame);
  Stream.Free;
end;

procedure savegame_api_ReadSaveGames(Archiv: TArchivFile; var SaveGames: TGameStatusArray);
var
  Count  : Integer;
  Dummy  : Integer;
begin
  if Archiv.ExistRessource(RNSaveGame) then
  begin
    Archiv.OpenRessource(RNSaveGame);
    with Archiv.Stream do
    begin
      Read(Count,SizeOf(Count));
      SetLength(SaveGames,Count);
      Dummy:=0;
      while Dummy<Count do
      begin
        Read(SaveGames[Dummy],SizeOf(TGameStatus));
        inc(Dummy);
      end;
    end;
  end;
end;

procedure savegame_api_SetGameSet(Archiv: TArchivFile);
begin
  record_utils_ClearArray(g_Projects);
  record_utils_ClearArray(g_Aliens);
  record_utils_ClearArray(g_UFOs);
  record_utils_ClearArray(g_Skripts);

  gameset_api_LoadProjects(Archiv,g_Projects);
  gameset_api_LoadAliens(Archiv,g_Aliens);
  gameset_api_LoadUFos(Archiv,g_UFOs);

  if not g_EditorMode then
    gameset_api_LoadSkripts(Archiv,g_Skripts);

  g_LoadedGameset:=true;
end;

function savegame_api_GameSetGetProjects: TRecordArray;
begin
  Assert(g_LoadedGameset);
  result:=g_Projects;
end;

function savegame_api_GameSetGetAliens: TRecordArray;
begin
  Assert(g_LoadedGameset);
  result:=g_Aliens;
end;

function savegame_api_GameSetGetUFOs: TRecordArray;
begin
  Assert(g_LoadedGameset);
  result:=g_UFOs;
end;

function savegame_api_GameSetGetSkripts: TRecordArray;
begin
  Assert(g_LoadedGameset);
  result:=g_Skripts;
end;

function savegame_api_GetNewGameData: TNewGameRec;
begin
  result:=g_savegame.NewGameData;
end;

procedure savegame_api_SetMissionEditorMode;
begin
  g_EditorMode:=true;
end;

initialization

finalization

  record_utils_ClearArray(g_Projects);
  record_utils_ClearArray(g_Aliens);
  record_utils_ClearArray(g_UFOs);
  record_utils_ClearArray(g_Skripts);

end.
