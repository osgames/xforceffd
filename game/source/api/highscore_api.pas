{********************************************************************************
*										*
* Copyright (C) 2004  X-Force Team - jim_raynor@web.de				*
*										*
* This file is part of X-Force: Fight For Destiny (X-Force).			*
*										*
* X-Force is free software; you can redistribute it and/or modify		*
* it under the terms of the GNU General Public License as published by		*
* the Free Software Foundation; either version 2 of the License, or		*
* (at your option) any later version.						*
*										*
* X-Force is distributed in the hope that it will be useful,			*
* but WITHOUT ANY WARRANTY; without even the implied warranty of		*
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the			*
* GNU General Public License for more details.					*
*										*
* You should have received a copy of the GNU General Public License		*
* along with X-Force; if not, write to the Free Software			*
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA	*
*										*
*********************************************************************************
*										*
* highscore_api bildet die Schnittstelle um auf die Highscore zugreifen zu  	*
* zu k�nnen									*
*										*
*********************************************************************************
*										*
* CVS-Informationen                                                             *
* Autor           $Author$
* CheckInDate     $Date$
* Tag/Branch      $Name$
* Revision        $Revision$
*										*
********************************************************************************}

{$I ../Settings.inc}

unit highscore_api;

interface

uses KD4Highscore, XForce_Types, Classes;

// Versorgt die API mit dem Highscore-Objekt, damit die anderen Funktionen
// genutzt werden k�nnen
procedure highscore_api_init(HighScore: TKD4HighScore);

// Gibt die Informationen zu einem Eintrag in der Highscore aus
function highscore_api_GetEntry(Index: Integer): THighScoreEntry;

// Gibt die Anzahl der Eintr�ge in der Highscore aus
function highscore_api_GetEntryCount: Integer;

// Erstellt einen neuen Eintrag in der Highscore
procedure highscore_api_AddEntry(Player: String; Month: Byte; Year: Word; Points: Integer);

// registriert einen Handler der Aufgerufen wird, sobald sich an der Highscore
// etwas �ndert
procedure highscore_api_RegisterChangeHandler(Handler: TNotifyEvent);

implementation

var
  g_HighScore: TKD4HighScore;

procedure highscore_api_init(Highscore: TKD4HighScore);
begin
  g_HighScore:=Highscore;
end;

function highscore_api_GetEntry(Index: Integer): THighScoreEntry;
begin
  Assert(g_HighScore<>nil);
  Assert((Index>=0) and (Index<g_HighScore.Count));

  result:=g_HighScore.Entry[Index];
end;

function highscore_api_GetEntryCount: Integer;
begin
  Assert(g_HighScore<>nil);
  result:=g_HighScore.Count;
end;

procedure highscore_api_AddEntry(Player: String; Month: Byte; Year: Word; Points: Integer);
var
  Entry : THighScoreEntry;
begin
  Assert(g_HighScore<>nil);
  Entry.Spieler:=Player;
  Entry.Week:=Month;
  Entry.Jahr:=Year;
  Entry.Punkte:=Points;
  g_HighScore.AddHigh(Entry);
end;

procedure highscore_api_RegisterChangeHandler(Handler: TNotifyEvent);
begin
  Assert(g_HighScore<>nil);
  g_HighScore.NotifyList.RegisterEvent(EVENT_HIGHSCORECHANGE,Handler);
end;

end.

