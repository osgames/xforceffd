{********************************************************************************
*										*
* Copyright (C) 2004  X-Force Team - jim_raynor@web.de				*
*										*
* This file is part of X-Force: Fight For Destiny (X-Force).			*
*										*
* X-Force is free software; you can redistribute it and/or modify		*
* it under the terms of the GNU General Public License as published by		*
* the Free Software Foundation; either version 2 of the License, or		*
* (at your option) any later version.						*
*										*
* X-Force is distributed in the hope that it will be useful,			*
* but WITHOUT ANY WARRANTY; without even the implied warranty of		*
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the			*
* GNU General Public License for more details.					*
*										*
* You should have received a copy of the GNU General Public License		*
* along with X-Force; if not, write to the Free Software			*
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA	*
*										*
*********************************************************************************
*										*
* alien_api bildet die Schnittstelle zu einer Alienliste, die die Aliens 	*
* verwaltet.
*										*
* WICHTIG!! Bevor die Funktionen genutzt werden k�nnen muss �ber 		*
*           alien_api_init die Alienliste gesetzt werden. Sollte durch 		*
*           savegame_api_init geschehen sein.					*
*										*
********************************************************************************}

{$I ../Settings.inc}

unit alien_api;

interface

uses AlienList, DXDraws, XForce_types;

procedure alien_api_init(AlienListe: TAlienList);

// Informationen zu einem Alien einholen
function alien_api_GetAlienInfo(ID: Cardinal): PAlien;

// Informationen zu einem Alien einholen
function alien_api_GetAlienIndex(ID: Cardinal): Integer;

// Gibt ein zuf�lliges Alien zur�ck
function alien_api_GetRandomAlien: PAlien;

// Zeichnet ein Symbol f�r ein Alien
procedure alien_api_DrawIcon(Surface: TDirectDrawSurface;x,y: Integer; Symbol: Integer);

// Schaltet ein Alien zur Anzeige in der UFOP�die frei (nach einer Autopsie)
procedure alien_api_AutopsieEnd(AlienID: Cardinal);

// Pr�ft, ob das Alien Angreifen kann oder nicht (von der vergangenen Zeit aus gesehen)
function alien_api_CanAttack(AlienID: Cardinal): Boolean;

implementation

var
  g_AlienList: TAlienList;

procedure alien_api_init(AlienListe: TAlienList);
begin
  g_AlienList:=AlienListe;
end;

function alien_api_GetAlienInfo(ID: Cardinal): PAlien;
begin
  result:=g_AlienList.AddrOfID(ID);
  Assert(result<>nil);
end;

function alien_api_GetAlienIndex(ID: Cardinal): Integer;
begin
  result:=g_AlienList.IndexOfID(ID);
  Assert(result<>-1);
end;

function alien_api_GetRandomAlien: PAlien;
begin
  result:=g_AlienList.GenerateAlien;
  Assert(result<>nil);
end;

procedure alien_api_DrawIcon(Surface: TDirectDrawSurface;x,y: Integer; Symbol: Integer);
begin
  g_AlienList.DrawImage(Symbol,Surface,X,Y);
end;

procedure alien_api_AutopsieEnd(AlienID: Cardinal);
begin
  g_AlienList.AutopsieEnd(AlienID);
end;

function alien_api_CanAttack(AlienID: Cardinal): Boolean;
begin
  result:=g_AlienList.CanAttack(g_AlienList.IndexOfID(AlienID));
end;

end.
