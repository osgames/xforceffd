{********************************************************************************
*										*
* Copyright (C) 2004  X-Force Team - jim_raynor@web.de				*
*										*
* This file is part of X-Force: Fight For Destiny (X-Force).			*
*										*
* X-Force is free software; you can redistribute it and/or modify		*
* it under the terms of the GNU General Public License as published by		*
* the Free Software Foundation; either version 2 of the License, or		*
* (at your option) any later version.						*
*										*
* X-Force is distributed in the hope that it will be useful,			*
* but WITHOUT ANY WARRANTY; without even the implied warranty of		*
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the			*
* GNU General Public License for more details.					*
*										*
* You should have received a copy of the GNU General Public License		*
* along with X-Force; if not, write to the Free Software			*
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA	*
*										*
*********************************************************************************
*										*
* country_api bildet die Schnittstelle zu einer Organisationsliste, die 	*
* L�nder in X-Force verwaltet.
*										*
* WICHTIG!! Bevor die Funktionen genutzt werden k�nnen muss �ber 		*
*           country_api_init die Organisationliste gesetzt werden. Sollte durch *
*           savegame_api_init geschehen sein.					*
*										*
********************************************************************************}

{$I ../Settings.inc}

unit country_api;

interface

uses
  OrganisationList, UFOList, DXDraws, XForce_types;

procedure country_api_init(CountryList: TOrganisationList);

// L�ndername ermitteln
function country_api_GetCountryName(Index: Integer): String;

// Anzahl der L�nder ermitteln
function country_api_GetCountryCount: Integer;

// Informationen zu einem Land zur�ckgeben
function country_api_GetCountry(Index: Integer): PLand;

// N�chsten Punkt des Land zur Hauptbasis ermitteln
function country_api_GetNearestPoint(Index: Integer): TFloatPoint;

// gibt eine zuf�llige Position, die im �bergebenen Land steht
function country_api_GetRandomLandPosition(Index: Integer; OverLand: Boolean = true): TFloatPoint;

// �ndert die Zufriedenheit eines Landes und passt dementsprechend das Vertrauen
// an
procedure country_api_ChangeZufriedenheit(Index: Integer; Zufriedenheit: Integer);

// Nummer des Landes auf einer Erdposition ermitteln
function country_api_LandNrOverPos(Point: TFloatPoint): Integer;

// Pr�ft, ob die Position Point dem �bergebenen Land geh�rt
function country_api_IsLandAtPos(Point: TFloatPoint; Land: Integer): Boolean;

// Namen des auf einer Erdposition ermitteln
function country_api_LandOverPos(Point: TFloatPoint): String;

// Erstellt zu einem Kauf-/Verkaufauftrag ein Angebot des entsprechenden Landes
// siehe TOrganisations.CreateAngebot f�r weitere Informationen
function country_api_CreateAngebot(const Auftrag: TAuftrag; var Angebot: TAngebot;Organ: Integer; Minuten: Integer): boolean;

// Zeichnet das gro�e Symbol eines Landes
procedure country_api_DrawSymbol(Surface: TDirectDrawSurface;x,y: Integer;Index: Integer);

// Erh�ht die Unterst�tzung der L�nder f�r ein fertiggestelltes Forschungsprojektes
procedure country_api_PayForschMoney(const Projekt: TForschProject);

procedure country_api_UFOLebt(UFO: TUFO);

procedure country_api_UFOAbschuss(UFO: TUFO);

// Erh�ht/verringert das Budget (die Unterst�tzung) eines Landes
procedure country_api_ChangeBudget(Land: Integer; Budget: Integer);

implementation

uses KD4Utils, earth_api;

var
  g_CountryList: TOrganisationList;

procedure country_api_init(CountryList: TOrganisationList);
begin
  g_CountryList:=CountryList;
end;

procedure country_api_CheckIndex(Index: Integer);
begin
  Assert((Index>=0) and (Index<g_CountryList.Count));
end;

function country_api_GetCountryCount: Integer;
begin
  result:=g_CountryList.Count;
end;

function country_api_GetCountry(Index: Integer): PLand;
begin
  country_api_CheckIndex(Index);
  result:=g_CountryList.AddrOfLand(Index);
end;

function country_api_GetCountryName(Index: Integer): String;
begin
  result:=country_api_GetCountry(Index).Name;
end;

function country_api_GetNearestPoint(Index: Integer): TFloatPoint;
begin
  country_api_CheckIndex(Index);
  result:=g_CountryList.GetNearestPoint(Index);
end;

function country_api_GetRandomLandPosition(Index: Integer; OverLand: Boolean): TFloatPoint;
var
  Found : Boolean;
  Count : Integer;
begin
  Found:=false;
  // Z�hler mitf�hren, falls es in einem Land keine Landposition gibt
  Count:=1000;
  while not Found do
  begin
    result:=earth_api_RandomEarthPoint(OverLand);
    dec(Count);
    Found:=(Count=0) or (country_api_LandNrOverPos(result)=Index);
  end;
end;

procedure country_api_ChangeZufriedenheit(Index: Integer; Zufriedenheit: Integer);
begin
  g_CountryList.ChangeZufriedenheit(Index,Zufriedenheit);
end;

function country_api_LandNrOverPos(Point: TFloatPoint): Integer;
begin
  result:=g_CountryList.LandNrOverPos(Point);
end;

function country_api_LandOverPos(Point: TFloatPoint): String;
begin
  result:=g_CountryList.LandOverPos(Point);
end;

function country_api_IsLandAtPos(Point: TFloatPoint; Land: Integer): Boolean;
begin
  result:=g_CountryList.LandNrOverPos(Point)=Land;
end;

function country_api_CreateAngebot(const Auftrag: TAuftrag; var Angebot: TAngebot;Organ: Integer; Minuten: Integer): boolean;
begin
  country_api_CheckIndex(Organ);
  result:=g_CountryList.CreateAngebot(Auftrag,Angebot,Organ,Minuten);
end;

procedure country_api_PayForschMoney(const Projekt: TForschProject);
begin
  g_CountryList.PayForschMoney(Projekt);
end;

procedure country_api_DrawSymbol(Surface: TDirectDrawSurface;x,y: Integer;Index: Integer);
begin
  g_CountryList.DrawOrgan(Surface,x,y,Index);
end;

procedure country_api_UFOLebt(UFO: TUFO);
begin
  g_CountryList.UFOLebt(UFO);
end;

procedure country_api_UFOAbschuss(UFO: TUFO);
begin
  g_CountryList.Abschuss(UFO.Position,UFO.Minuten);
end;

procedure country_api_ChangeBudget(Land: Integer; Budget: Integer);
begin
  country_api_CheckIndex(Land);
  g_CountryList.AendereKapital(Land,Budget);
end;

end.
