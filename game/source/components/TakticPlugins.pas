{********************************************************************************
*										*
* Copyright (C) 2004  X-Force Team - jim_raynor@web.de				*
*										*
* This file is part of X-Force: Fight For Destiny (X-Force).			*
*										*
* X-Force is free software; you can redistribute it and/or modify		*
* it under the terms of the GNU General Public License as published by		*
* the Free Software Foundation; either version 2 of the License, or		*
* (at your option) any later version.						*
*										*
* X-Force is distributed in the hope that it will be useful,			*
* but WITHOUT ANY WARRANTY; without even the implied warranty of		*
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the			*
* GNU General Public License for more details.					*
*										*
* You should have received a copy of the GNU General Public License		*
* along with X-Force; if not, write to the Free Software			*
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA	*
*										*
*********************************************************************************
*										*
* TakticPlugins stellt verschiedene Plugins f�r den Geoscape bereit.		*
*										*
* TTakticTimeDrawer verwaltet die Zeitleiste, mit der man die Geschwindigkeit   *
* im Geoscape regeln kann.							*
*										*
* TTakticButtons verwaltet die Navigationsbuttons am Linken Rand		*
*										*
* TTakticMessages verwaltet die Nachrichten, die direkt auf dem Geoscape 	*
* angezeigt werden.
*										*
********************************************************************************}

{$I ../Settings.inc}

unit TakticPlugins;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  DXDraws, XForce_types, DirectFont, DXTakticScreen, D3DRender, KD4Utils,
  Defines, StringConst;

type
  TTakticTimeDrawer = class(TDXTakticPlugIn)
  private
    fMode        : Integer;
    fDate        : TKD4Date;
    fCredits     : Int64;
    fOnSetTime   : TButtonClick;
    fCanFastTime : Boolean;
    fOverTimeMode: Integer;
    { Private-Deklarationen }
  protected
    function GetTimeMode(X,Y: Integer): Integer;

    procedure MouseUp(Button: TMouseButton;X,Y: Integer);override;
    procedure MouseMove(X,Y: Integer; var NRedraw: Boolean);override;
    { Protected-Deklarationen }
  public
    constructor Create(Screen: TDXTakticScreen);override;
    procedure Draw(Surface: TDirectDrawSurface);override;

    procedure SetTimeMode(Mode: Integer);
    procedure SetDate(Date: TKD4Date);
    procedure SetCredits(Credits: Int64);
    property CanFastTime  : Boolean read fCanFastTime write fCanFastTime;
    property OnSetTime    : TButtonClick read fOnSetTime write fOnSetTime;
    { Public-Deklarationen }
  end;

  TTakticButtons = class(TDXTakticPlugIn)
  private
    fLastTop         : Integer;
    fOverButton      : Integer;
    fButtons         : Array of record
                         Top     : Integer;
                         Key     : Char;
                         Data    : Integer;
                         Text    : String;
                         Onclick : TButtonClick;
                         Visible : Boolean;
                       end;

    function IsOverButton(X,Y: Integer): Integer;
  protected
    procedure MouseMove(X,Y: Integer; var NRedraw: Boolean);override;
    procedure MouseUp(Button: TMouseButton;X,Y: Integer);override;
  public
    constructor Create(Screen: TDXTakticScreen);override;
    procedure Draw(Surface: TDirectDrawSurface);override;
    procedure SetVisible(ID: Integer; Visible: Boolean);

    function AddNavigationButton(Text: String; Onclick: TButtonClick; Data: Integer;Space : Integer = 0): Integer;
  end;

  TTakticMessages = class(TDXTakticPlugIn)
  private
    fMessages       : TStringList;
  public
    constructor Create(Screen: TDXTakticScreen);override;
    destructor Destroy;override;
    procedure Draw(Surface: TDirectDrawSurface);override;

    procedure Addmessage(Text: String; Date: String);
    procedure DoRound(Minuten: Integer);

    property Messages   : TStringList read fMessages;
  end;

  const
    MenuPos        = ScreenWidth-110;
    ButtonHeight   = 21;

implementation

{ TTakticTimeDrawer }

constructor TTakticTimeDrawer.Create(Screen: TDXTakticScreen);
begin
  inherited;
  fCanFastTime:=true;
  fOverTimeMode:=-1;
end;

procedure TTakticTimeDrawer.Draw(Surface: TDirectDrawSurface);
var
  WeekDay   : Integer;
  SysDate   : TDateTime;
  DateStamp : TTimeStamp;
  Dummy     : Integer;
  Left      : Integer;
begin
  { Gibt das Formatierte Datum des Spielstandes aus im Format 'TT.MM.JJ um HH.00' }
  SysDate:=EncodeDate(fDate.Year,fDate.Month,fDate.Day)+0.5;
  WeekDay:=DayOfWeek(SysDate);
  DateStamp:=DateTimeToTimeStamp(SysDate);
  SmallWhiteFont.Draw(Surface,fLeft,fTop,Format(FLongDate,[ADays[WeekDay],fDate.Day,fDate.Month,fDate.Year,fDate.Hour,fDate.Minute]));

  Surface.Draw(fLeft,fTop+20,Rect(2,136,29,155),fUISymbols,true);
  Left:=fLeft+25;
  for Dummy:=1 to 3 do
  begin
    if fMode>=Dummy then
      Surface.Draw(Left,fTop+20,Rect(29,136,53,155),fUISymbols)
    else if (not fCanFastTime) and (Dummy>1) then
      D3DRenderAlpha(fScreen.Container,Left,fTop+20,Rect(29,176,53,195),100)
    else
      D3DRenderAlpha(fScreen.Container,Left,fTop+20,Rect(29,156,53,175),100);
    inc(Left,22);
  end;

  if fMode=4 then
    Surface.Draw(Left,fTop+20,Rect(54,136,80,155),fUISymbols)
  else if not fCanFastTime then
    D3DRenderAlpha(fScreen.Container,Left,fTop+20,Rect(54,176,80,195),100)
  else
    D3DRenderAlpha(fScreen.Container,Left,fTop+20,Rect(54,156,80,175),100);

  SmallWhiteFont.Draw(Surface,Left+100,fTop,IDGeldMittel+':');
  SmallWhiteFont.Draw(Surface,Left+150,fTop,Format(StringConst.FFloat,[fCredits/1]));

//  SmallWhiteFont.Draw(Surface, Left+200, fTop, IDAlphatron+':');
//  SmallWhiteFont.Draw(Surface,Left+250,fTop,Format(StringConst.FFloat,[fAlphatron/1]));
end;

procedure TTakticTimeDrawer.MouseUp(Button: TMouseButton; X, Y: Integer);
var
  TimeMode: Integer;
begin
  if not Assigned(fOnSetTime) then
    exit;

  TimeMode:=GetTimeMode(X,Y);
  if TimeMode<>-1 then
    fOnSetTime(TimeMode);
end;

procedure TTakticTimeDrawer.SetCredits(Credits: Int64);
begin
  fCredits:=Credits;
end;

procedure TTakticTimeDrawer.SetDate(Date: TKD4Date);
begin
  fDate:=Date;
end;

procedure TTakticTimeDrawer.SetTimeMode(Mode: Integer);
begin
  fMode:=Mode;
end;

procedure TTakticTimeDrawer.MouseMove(X, Y: Integer; var NRedraw: Boolean);
var
  TimeMode: Integer;
begin
  TimeMode:=GetTimeMode(X,Y);
  if TimeMode<>fOverTimeMode then
  begin
    if TimeMode<>-1 then
      fScreen.ShowHint(TimeModeNames[TimeMode]+' ('#2+IntToStr(TimeMode)+#1')',Point(fLeft+(TimeMode*22)+3,fTop+40))
    else
      fScreen.ShowHint('',Point(10,10));
  end;

  fOverTimeMode:=TimeMode;
end;

function TTakticTimeDrawer.GetTimeMode(X, Y: Integer): Integer;
begin
  result:=-1;
  
  if (Y>=fTop+20) and (Y<fTop+39) and (X>Left) then
  begin
    X:=X-Left;
    if X<25 then
      result:=0
    else if X<47 then
      result:=1
    else if X<69 then
      result:=2
    else if X<91 then
      result:=3
    else if X<116 then
      result:=4;
  end;
end;

{ TTakticButtons }

function TTakticButtons.AddNavigationButton(Text: String; Onclick: TButtonClick;
  Data: Integer; Space: Integer): Integer;
begin
  inc(fLastTop,Space);
  SetLength(fButtons,length(fButtons)+1);
  fButtons[High(fButtons)].Top:=fLastTop;
  fButtons[High(fButtons)].Data:=Data;
  fButtons[High(fButtons)].Key:=GetShortCut(Text);
  fButtons[High(fButtons)].Text:=copy(Text,1,Pos('&',Text)-1)+#2+copy(Text,Pos('&',Text)+1,1)+#1+copy(Text,Pos('&',Text)+2,200);
  fButtons[High(fButtons)].Onclick:=Onclick;
  fButtons[High(fButtons)].Visible:=true;

  result:=high(fButtons);

  fScreen.SetHotKey(GetShortCut(Text),True,OncLick,Data);
  inc(fLastTop,ButtonHeight);
end;

constructor TTakticButtons.Create(Screen: TDXTakticScreen);
begin
  inherited;
  fLastTop:=5;
  fOverButton:=-1;
end;

procedure TTakticButtons.Draw(Surface: TDirectDrawSurface);
var
  Dummy: Integer;
begin
  for Dummy:=0 to high(fButtons) do
  begin
    with fButtons[Dummy] do
    begin
      if Visible then
      begin
        if fOverButton=Dummy then
          Surface.Draw(MenuPos,Top,Rect(108,139,228,158),fUISymbols,true)
        else
          D3DRenderAlpha(fScreen.Container,MenuPos,Top,Rect(108,162,228,181),75);
        FontEngine.DynamicText(Surface,MenuPos+30,Top+4,Text,[SmallWhiteFont,SmallYellowFont]);
      end;
    end;
  end;

end;

function TTakticButtons.IsOverButton(X, Y: Integer): Integer;
var
  Dummy: Integer;
begin
  result:=-1;
  if X>MenuPos then
  begin
    for Dummy:=0 to High(fButtons) do
    begin
      with fButtons[Dummy] do
      begin
        if Visible then
        begin
          if (Y>=Top) and (Y<Top+ButtonHeight-2) then
          begin
            result:=Dummy;
            break;
          end;
        end;
      end;
    end;
  end;
end;

procedure TTakticButtons.MouseMove(X, Y: Integer; var NRedraw: Boolean);
var
  NewButton: Integer;
begin
  // Pr�fen, �ber welchen Button die Maus ist
  NewButton:=IsOverButton(X,Y);
  if NewButton<>fOverButton then
  begin
    fOverButton:=NewButton;
    NRedraw:=true;
  end;
end;

procedure TTakticButtons.MouseUp(Button: TMouseButton; X, Y: Integer);
var
  ButtIndex: Integer;
begin
  if Button=mbLeft then
  begin
    ButtIndex:=IsOverButton(X,Y);
    if ButtIndex<>-1 then
    begin
      if Assigned(fButtons[ButtIndex].Onclick) then
        fButtons[ButtIndex].Onclick(fButtons[ButtIndex].Data);
    end;
  end;
end;

procedure TTakticButtons.SetVisible(ID: Integer; Visible: Boolean);
begin
  fButtons[ID].Visible:=Visible;
  with fButtons[ID] do
  fScreen.SetHotKey(Key,Visible,Onclick,Data);
end;

{ TTakticMessages }

procedure TTakticMessages.Addmessage(Text: String; Date: String);
var
  FirstIndex : Integer;
  Dummy      : Integer;
  Line       : String;
  Position   : Integer;
begin
  FirstIndex:=fMessages.Count;

  // Schritt 1 lange zeilen Aufteilen
  Text:=WrapText(Text,#13#10, ['.',' ',#9,'-'], 100);
  repeat
    Position:=Pos(#10,Text);
    if Position<>0 then
    begin
      Line:=Copy(Text,1,Position-1);
      if Trim(Line)<>EmptyStr then
        fMessages.AddObject(Line,TObject(120));
      Text:=Copy(Text,Position+1,999999);
    end;
  until Position=0;
  
  fMessages.AddObject(Text,TObject(120));

  fMessages[FirstIndex]:=Date+': '#2+fMessages[FirstIndex];
  for Dummy:=FirstIndex+1 to fMessages.Count-1 do
    fMessages[Dummy]:=#254+fMessages[Dummy];
//  if fMessages.Count>fMaxMessages then
//    fMessages.Delete(fMaxMessages);
end;

constructor TTakticMessages.Create(Screen: TDXTakticScreen);
begin
  inherited;
  fMessages:=TStringList.Create;
end;

destructor TTakticMessages.Destroy;
begin
  fMessages.Free;
  inherited;
end;

procedure TTakticMessages.DoRound(Minuten: Integer);
var
  Dummy: Integer;
begin
  for Dummy:=fMessages.Count-1 downto 0 do
  begin
    fMessages.Objects[Dummy]:=TObject(Integer(fMessages.Objects[Dummy])-Minuten);
    if Integer(fMessages.Objects[Dummy])<0 then
       fMessages.Delete(Dummy);
  end;
end;

procedure TTakticMessages.Draw(Surface: TDirectDrawSurface);
var
  Dummy : Integer;
  YTop  : Integer;
begin
  YTop:=440-fMessages.Count*15;
  for Dummy:=0 to fMessages.Count-1 do
  begin
    if fMessages[Dummy][1]=#254 then
      SmallYellowFont.Draw(Surface,fLeft+150,YTop,Copy(fMessages[Dummy],2,999))
    else
    begin
      FontEngine.DynamicText(Surface,fLeft+50,YTop,fMessages[Dummy],[SmallWhiteFont,SmallYellowFont]);
    end;
    inc(YTop,15);
  end;
end;

end.
