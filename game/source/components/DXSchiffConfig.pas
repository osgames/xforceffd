{********************************************************************************
*										*
* Copyright (C) 2004  X-Force Team - jim_raynor@web.de				*
*										*
* This file is part of X-Force: Fight For Destiny (X-Force).			*
*										*
* X-Force is free software; you can redistribute it and/or modify		*
* it under the terms of the GNU General Public License as published by		*
* the Free Software Foundation; either version 2 of the License, or		*
* (at your option) any later version.						*
*										*
* X-Force is distributed in the hope that it will be useful,			*
* but WITHOUT ANY WARRANTY; without even the implied warranty of		*
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the			*
* GNU General Public License for more details.					*
*										*
* You should have received a copy of the GNU General Public License		*
* along with X-Force; if not, write to the Free Software			*
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA	*
*										*
*********************************************************************************
*										*
* Komponenten zum verwalten der Soldaten bzw. Ausr�stungen in Raumschiffen	*
*										*
********************************************************************************}

{$I ../Settings.inc}

unit DXSchiffConfig;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  DXDraws, DXContainer, RaumschiffList, KD4Utils, DirectFont,
  Blending, DXScrollBar, XForce_types, NGTypes, TraceFile, DXClass, DXItemInfo,
  DirectDraw, LagerListe, math, StringConst;

const
  SlotWidth   : Integer = 115;
  SlotHeight  = 52;
  SlotPadding = 8;
  ItemHeight  = 60;
  ItemPadding = 4;

type
  TDXSchiffSoldaten = class(TDXComponent)
  private
    fRaumschiff       : TRaumschiff;
    fScrollBar        : TDXScrollBar;
    fScrollVisible    : boolean;
    fSlotsPerZeile    : Integer;
    fZeilen           : Integer;
    fClickSlot        : TClickSlot;
    fRedrawRect       : TRect;
    fHighLight        : Integer;
    fSlotInfo         : Integer;
    fLeaveSlot        : TThreadMethod;
    fOverSlot         : TOverSlot;
    procedure SetRaumschiff(const Value: TRaumschiff);
    procedure DrawSlot(X,Y,Nummer: Integer;Surface: TDirectDrawSurface;const Mem: TDDSurfaceDesc);
    procedure OnScroll(Sender: TObject);
    procedure SetHighLight(const Value: Integer);
    function SlotOverPoint(Point: TPoint;var Rect: TRect): Integer;
    { Private-Deklarationen }
  protected
    procedure DoScroll(Direction: TScrollDirection; Pos: TPoint);override;
    { Protected-Deklarationen }
  public
    constructor Create(Page: TDXPage);override;
    destructor Destroy;override;
    procedure MouseUp(Button: TMouseButton;X,Y: Integer);override;
    procedure MouseMove(X,Y: Integer);override;
    procedure MouseLeave;override;
    procedure VisibleChange;override;
    procedure Resize(var NewLeft, NewTop, NewWidth, NewHeight: Integer);override;
    procedure Draw(Surface: TDirectDrawSurface;var Mem: TDDSurfaceDesc);override;
    procedure ReDrawRect(DrawRect: TRect;Surface: TDirectDrawSurface;var Mem: TDDSurfaceDesc);override;
    property Schiff      : TRaumschiff read fRaumschiff write SetRaumschiff;
    property OnClickSlot : TClickSlot read fClickSlot write fClickSlot;
    property OnOverSlot  : TOverSlot read fOverSlot write fOverSlot;
    property OnLeaveSlot : TThreadMethod read fLeaveSlot write fLeaveSlot;
    property HighLight   : Integer read fHighLight write SetHighLight;
  end;

  TItemsEntry = record
    ID       : Cardinal;
    Schiff   : PItemsInSchiff;
  end;

  TDXSchiffItems = class(TDXComponent)
  private
    fRaumschiff       : TRaumschiff;
    fScrollBar        : TDXScrollBar;
    fItems            : Array of TItemsEntry;
    fScrollVisible    : boolean;
    fSlotsPerZeile    : Integer;
    fSlotWidth        : Integer;
    fZeilen           : Integer;
    fRedrawRect       : TRect;
    fItemClick        : TItemChangeEvent;
    fSlotInfo         : Integer;
    fSlotRect         : TRect;
    fOverSlot         : TOverItem;
    fLeaveSlot        : TThreadMethod;
    fOverButton       : Integer;
    fDeleteItem       : TDeleteItem;
    fGetName          : TGetSelectedItem;
    fBarClick         : Integer;
    fBarRect          : TRect;
    fBasisID          : Cardinal;
    procedure SetRaumschiff(const Value: TRaumschiff);
    procedure DrawSlot(X,Y,Nummer: Integer; Surface: TDirectDrawSurface;const Mem: TDDSurfaceDesc);
    procedure OnScroll(Sender: TObject);
    procedure DrawButton(Surface: TDirectDrawSurface;const Mem: TDDSurfaceDesc;Slot,X,Y: Integer;Image: Integer; Enabled: boolean);
    procedure DrawFortSchritt(Surface: TDirectDrawSurface;const Mem: TDDSurfaceDesc;X,Y,Width: Integer;Prozent: double;Enabled: boolean);

    procedure AktuItems;
  protected
    procedure DoScroll(Direction: TScrollDirection; Pos: TPoint);override;
    function SlotOverPoint(Point: TPoint;var Rect: TRect): Integer;
  public
    constructor Create(Page: TDXPage);override;
    destructor Destroy; override;
    procedure Resize(var NewLeft, NewTop, NewWidth, NewHeight: Integer);override;
    procedure Draw(Surface: TDirectDrawSurface;var Mem: TDDSurfaceDesc);override;
    procedure MouseMove(X,Y: Integer);override;
    procedure VisibleChange;override;
    procedure ReDrawRect(DrawRect: TRect;Surface: TDirectDrawSurface;var Mem: TDDSurfaceDesc);override;
    procedure MouseLeave;override;
    procedure MouseDown(Button: TMouseButton;X,Y: Integer);override;
    procedure MouseUp(Button: TMouseButton;X,Y: Integer);override;
    function ItemAtIndex(Index: Integer): PLagerItem;
    procedure Aktu;
    property Schiff         : TRaumschiff read fRaumschiff write SetRaumschiff;
    property ItemClick      : TItemChangeEvent read fItemClick write fItemClick;
    property DeleteItem     : TDeleteItem read fDeleteItem write fDeleteItem;
    property OnOverSlot     : TOverItem read fOverSlot write fOverSlot;
    property OnLeaveSlot    : TThreadMethod read fLeaveSlot write fLeaveSlot;
    property GetNameOfNew   : TGetSelectedItem read fGetName write fGetName;
  end;

implementation

uses
  soldaten_api, lager_api, game_api;

{ TDXSchiffSoldaten }

constructor TDXSchiffSoldaten.Create(Page: TDXPage);
begin
  inherited;
  fScrollBar:=TDXScrollBar.Create(Page);
  fScrollVisible:=false;
  with fScrollBar do
  begin
    RoundCorners:=rcRightBottom;
    FirstColor:=bcMaroon;
    SecondColor:=clMaroon;
    Visible:=false;
    OnChange:=OnScroll;
  end;
end;

destructor TDXSchiffSoldaten.Destroy;
begin
  fScrollBar.Free;
  inherited;
end;

procedure TDXSchiffSoldaten.DoScroll(Direction: TScrollDirection; Pos: TPoint);
begin
  Container.IncLock;
  if Direction=sdUp then fScrollBar.Value:=fScrollBar.Value-(fScrollBar.SmallChange*2);
  if Direction=sdDown then fScrollBar.Value:=fScrollBar.Value+(fScrollBar.SmallChange*2);
  if Assigned(fLeaveSlot) then fLeaveSlot;
  fSlotInfo:=-1;
  Container.DecLock;
  Container.DoMouseMessage;
end;

procedure TDXSchiffSoldaten.Draw(Surface: TDirectDrawSurface;var Mem: TDDSurfaceDesc);
begin
  ReDrawRect(ClientRect,Surface,Mem);
end;

procedure TDXSchiffSoldaten.DrawSlot(X, Y, Nummer: Integer;Surface: TDirectDrawSurface;const Mem: TDDSurfaceDesc);
var
  TempRect: TRect;
  Soldat  : PSoldatInfo;
  Index   : Integer;
  Color1  : TBlendColor;
  Color2  : TBlendColor;
begin
  TempRect:=Rect(X,Y,X+SlotWidth,Y+SlotHeight);

  if not OverlapRect(TempRect,fRedrawRect) then exit;
  Index:=fRaumschiff.GetSoldat(Nummer,Soldat);

  if not Enabled then
  begin
    Color1:=bcGray;
    Color2:=bcStdGray;
  end
  else if Index=fHighLight then
  begin
    Color1:=bcNavy;
    color2:=bcBlue;
  end
  else if Index>-1 then
  begin
    Color1:=bcGreen;
    Color2:=bcLime;
  end
  else
  begin
    Color1:=bcMaroon;
    Color2:=bcRed;
  end;

  if AlphaElements then
  begin
    dec(TempRect.Bottom);
    IntersectRect(TempRect,TempRect,fRedrawRect);
    BlendRectangle(TempRect,100,Color1,Surface,Mem);
  end;
  Frame3D(Surface,Mem,X,Y,X+SlotWidth,Y+SlotHeight,Color1,Color2);

  if Index=-1 then
    YellowStdFont.Draw(Surface,X+6,Y+8,LFree)
  else
  begin
    soldaten_api_DrawDXSoldat(Soldat^,Surface,Mem,X+3,Y+3);
    YellowStdFont.Draw(Surface,X+47,Y+8,Soldat.Name);
  end;
end;

procedure TDXSchiffSoldaten.MouseLeave;
begin
  if Assigned(fLeaveSlot) then fLeaveSlot;
  fSlotInfo:=-1;
end;

procedure TDXSchiffSoldaten.MouseMove(X, Y: Integer);
var
  Rect   : TRect;
  Slot   : Integer;
  Soldat : PSoldatInfo;
  Index  : Integer;
begin
  inherited;
  Slot:=SlotOverPoint(Point(X,Y),Rect);
  if Slot<>fSlotInfo then
  begin
    Index:=fRaumschiff.GetSoldat(Slot,Soldat);
    if (Slot<>-1) and (Index>-1) then
    begin
      if Assigned(fOverSlot) then fOverSlot(Slot,Index=fHighLight,Rect,Soldat^);
    end
    else
    begin
      if Assigned(fLeaveSlot) then fLeaveSlot;
    end;
    fSlotInfo:=Slot;
  end;
end;

procedure TDXSchiffSoldaten.MouseUp(Button: TMouseButton; X, Y: Integer);
var
  Slot : Integer;
  Rect : TRect;
begin
  Slot:=SlotOverPoint(Point(X,Y),Rect);
  if Slot>-1 then
  begin
    if Assigned(fClickSlot) then fClickSlot(Slot,Button);
    fSlotInfo:=-1;
    MouseMove(X,Y);
  end;
end;

procedure TDXSchiffSoldaten.OnScroll(Sender: TObject);
begin
  Redraw;
end;

procedure TDXSchiffSoldaten.ReDrawRect(DrawRect: TRect;
  Surface: TDirectDrawSurface;var Mem: TDDSurfaceDesc);
var
  Text     : String;
  Slot     : Integer;
  Slots    : Integer;
  XSlot    : Integer;
  X,Y      : Integer;
begin
  if EqualRect(DrawRect,fScrollBar.ClientRect) then
    exit;
  IntersectRect(fRedrawRect,DrawRect,ClientRect);
  if AlphaElements then
  begin
    BlendRectangle(Rect(Left,Top,Right,Top+22),100,bcMaroon,Surface,Mem);
  end;
  FramingRect(Surface,Mem,ClientRect,[cRightBottom],11,bcMaroon);
  HLine(Surface,Mem,Left,Right-1,Top+22,bcMaroon);
  
  if fRaumschiff = nil then
    exit;
    
  WhiteBStdFont.Draw(Surface,Left+8,Top+5,fRaumschiff.Model.Name);

  if fRaumschiff.Model.Soldaten=0 then
  begin
    if AlphaElements then
      BlendRectangle(Classes.Rect(Left+22,Top+(height div 2)-38,Right-22,Top+(height div 2)+38),140,bcMaroon,Surface,Mem);

    Frame3D(Surface,Mem,Left+20,Top+(height div 2)-40,Right-20,Top+(height div 2)+40,bcRed,bcMaroon);
    WhiteBStdFont.Draw(Surface,Left+(Width shr 1)-(WhiteBStdFont.TextWidth(ST0309220024) shr 1),Top+(height div 2)-10,ST0309220022);
  end
  else
  begin
    Text:=LPlatzFor+ZahlString(FSoldat,FSoldaten,fRaumschiff.SoldatenSlots)+LPlatzBelegt+IntToStr(fRaumschiff.Soldaten)+')';
    YellowStdFont.Draw(Surface,Right-8-YellowStdFont.TextWidth(Text),Top+5,Text);
    Slot:=fScrollBar.Value*fSlotsPerZeile;
    XSlot:=1;
    X:=Left+SlotPadding;
    Slots:=fRaumschiff.SoldatenSlots;
    Y:=Top+22+SlotPadding;
    while (Slot<Slots) do
    begin
      DrawSlot(X,Y,Slot,Surface,Mem);
      inc(X,SlotWidth+SlotPadding);
      inc(Slot);
      inc(XSlot);
      if XSlot>fSlotsPerZeile then
      begin
        X:=Left+SlotPadding;
        XSlot:=1;
        inc(Y,SlotHeight+SlotPadding);
        if Y+SlotHeight>Bottom then break;
      end;
    end;
  end;  
end;

procedure TDXSchiffSoldaten.Resize(var NewLeft, NewTop, NewWidth,
  NewHeight: Integer);
begin
  SlotWidth:=NewWidth-40;

  fScrollBar.SetRect(NewLeft+NewWidth-17,Top+22,17,NewHeight-22);
  fSlotsPerZeile:=(Width-16-SlotPadding) div (SlotWidth+SlotPadding);
  fZeilen:=(NewHeight-22-SlotPadding) div (SlotHeight+SlotPadding);
  fScrollBar.LargeChange:=fZeilen;
end;

procedure TDXSchiffSoldaten.SetHighLight(const Value: Integer);
begin
  fHighLight := Value;
  Container.IncLock;
  if Assigned(fLeaveSlot) then fLeaveSlot;
  fSlotInfo:=-1;
  Container.DoMouseMessage;
  Container.DecLock;
  Redraw;
end;

procedure TDXSchiffSoldaten.SetRaumschiff(const Value: TRaumschiff);
var
  NeedZeilen     : Integer;
begin
  fRaumschiff := Value;
  if fRaumschiff = nil then
  begin
    Enabled:=false;
    Redraw;
    exit;
  end;
  fRaumschiff.AktuFreeSlots;
  fSlotInfo:=-1;
  Container.Lock;
  NeedZeilen:=fRaumschiff.SoldatenSlots div fSlotsPerZeile;
  if (fRaumschiff.SoldatenSlots mod fSlotsPerZeile)>0 then
    inc(NeedZeilen);

  fScrollVisible:=(NeedZeilen>fZeilen);
  fScrollBar.Visible:=fScrollVisible and Visible;
  fScrollBar.Value:=0;
  if fScrollVisible then
    fScrollBar.Max:=NeedZeilen-fZeilen
  else
    fScrollBar.Max:=0;

  Enabled:=sabEquipt in fRaumschiff.Abilities;
  
  Container.Unlock;
  Redraw;
end;

function TDXSchiffSoldaten.SlotOverPoint(Point: TPoint;var Rect: TRect): Integer;
var
  Slot   : Integer;
  Slots  : Integer;
  XSlot  : Integer;
  XPos   : Integer;
  YPos   : Integer;
begin
  result:=-1;
  Slot:=fScrollBar.Value*fSlotsPerZeile;
  XSlot:=1;
  XPos:=SlotPadding;
  Slots:=fRaumschiff.SoldatenSlots;
  YPos:=22+SlotPadding;
  while (Slot<Slots) do
  begin
    if PtInRect(Classes.Rect(XPos,YPos,XPos+SlotWidth,YPos+SlotHeight),Point) then
    begin
      Rect:=Classes.Rect(Left+XPos,Top+YPos,Left+XPos+SlotWidth,Top+YPos+SlotHeight);
      result:=Slot;
      exit;
    end;
    inc(XPos,SlotWidth+SlotPadding);
    inc(Slot);
    inc(XSlot);
    if XSlot>fSlotsPerZeile then
    begin
      XPos:=SlotPadding;
      XSlot:=1;
      inc(YPos,SlotHeight+SlotPadding);
      if YPos+SlotHeight>Height then break;
    end;
  end;
end;

procedure TDXSchiffSoldaten.VisibleChange;
begin
  if Visible then
  begin
    fScrollBar.Visible:=fScrollVisible;
  end
  else fScrollBar.Visible:=false;
end;

{ TDXSchiffItems }
{ Verwaltung der Ausr�stung }

procedure TDXSchiffItems.Aktu;
var
  NeedZeilen: Integer;
begin
  AktuItems;

  if fRaumschiff = nil then
    exit;
    
  if fRaumschiff.Model.LagerPlatz=0 then
    NeedZeilen:=0
  else
    NeedZeilen:=(length(fItems)+1) div fSlotsPerZeile;
  Container.Lock;
  fScrollVisible:=(NeedZeilen>fZeilen);
  fScrollBar.Visible:=fScrollVisible and Visible;
//  fScrollBar.Value:=0;
  if fScrollVisible then
    fScrollBar.Max:=(NeedZeilen)-fZeilen
  else
  begin
    fScrollBar.Max:=0;
    fScrollBar.Value:=0;
  end;
  fSlotInfo:=-1;
  Container.UnLock;
  Container.IncLock;
  fOverButton:=-1;
  Container.DoMouseMessage;
  Container.DecLock;
  Redraw;
end;

procedure TDXSchiffItems.AktuItems;
var
  Dummy : Integer;
  Index : Integer;
  SItem : PItemsInSchiff;
  Item  : PLagerItem;
begin
  SetLength(fItems,0);
  if fRaumschiff = nil then
    exit;

  SetLength(fItems,lager_api_Count);
  Index:=0;

  for Dummy:=0 to lager_api_Count-1 do
  begin
    Item:=lager_api_GetItem(Dummy);
    if Item.TypeID in SoldatenTypes then
    begin
      SItem:=fRaumschiff.AddrOfSchiffItemID(Item.ID);
      if (Item.Anzahl>0) or ((SItem<>nil) and (SItem.Anzahl>0)) then
      begin
        fItems[Index].ID:=Item.ID;
        fItems[Index].Schiff:=SItem;
        inc(Index);
      end;
    end;
  end;

  SetLength(fItems,Index);
end;

constructor TDXSchiffItems.Create(Page: TDXPage);
begin
  inherited;
  fBarClick:=-1;
  fScrollBar:=TDXScrollBar.Create(Page);
  fScrollVisible:=false;
  with fScrollBar do
  begin
    RoundCorners:=rcRightBottom;
    FirstColor:=bcMaroon;
    SecondColor:=clMaroon;
    Visible:=false;
    OnChange:=OnScroll;
  end;
  fOverButton:=-1;
end;

destructor TDXSchiffItems.Destroy;
begin
  fScrollBar.Free;
  inherited;
end;

procedure TDXSchiffItems.DoScroll(Direction: TScrollDirection; Pos: TPoint);
begin
  fBarClick:=-1;
  Container.IncLock;
  if Direction=sdUp then fScrollBar.Value:=fScrollBar.Value-(fScrollBar.SmallChange*2);
  if Direction=sdDown then fScrollBar.Value:=fScrollBar.Value+(fScrollBar.SmallChange*2);
  Container.DecLock;
  Container.DoMouseMessage;
end;

procedure TDXSchiffItems.Draw(Surface: TDirectDrawSurface;var Mem: TDDSurfaceDesc);
begin
  ReDrawRect(ClientRect,Surface,Mem);
end;

procedure TDXSchiffItems.DrawButton(Surface: TDirectDrawSurface;const Mem: TDDSurfaceDesc;
  Slot, X, Y, Image: Integer; Enabled: Boolean);
var
  Color  : TBlendColor;
  Alpha  : Integer;
begin
  if Enabled then Color:=bcMaroon else Color:=bcDisabled;
  if AlphaElements then
  begin
    if Enabled and (Slot=fSlotInfo) and (fOverButton=Image) then
      Alpha:=150
    else
      Alpha:=100;
    BlendRectangle(Rect(X,Y,X+19,Y+19),Alpha,Color,Surface,Mem);
  end;
  Rectangle(Surface,Mem,x,Y,X+20,Y+20,Color);
  Container.ImageList.Items[0].Draw(Surface,X+4,Y+4,Image);
end;

procedure TDXSchiffItems.DrawFortSchritt(Surface: TDirectDrawSurface;
  const Mem: TDDSurfaceDesc; X, Y, Width: Integer; Prozent: double; Enabled: boolean);
var
  Links  : Integer;
  Oben   : Integer;
  Full   : boolean;
  Zeiger : Cardinal;
  Farbe  : Cardinal;
  Beginn : Integer;
  Ende   : Integer;
begin
  Beginn:=0;
  Ende:=17;
  if Surface.ClippingRect.Top>Y then Beginn:=Surface.ClippingRect.Top-Y;
  if Surface.ClippingRect.Bottom<Y+17 then Ende:=min(17,Surface.ClippingRect.Bottom-Y-1);
  if Mode32Bit then
  begin
    for Links:=0 to Width-1 do
    begin
      if Enabled and (Self.Enabled) then
        Farbe:=WerkColorTable[100-round(Links/Width*100)]
      else
        Farbe:=TimeColorTable[100-round(Links/Width*100)];
      Full:=round(Prozent/100*Width)<=Links;
      Zeiger:=Integer(Mem.lpSurface)+((Y+Beginn)*Mem.lPitch)+((X+Links) shl 2);
      for Oben:=Beginn to Ende do
      begin
        if not Full then
          PLongWord(Zeiger)^:=Farbe
        else
          PLongWord(Zeiger)^:=((Farbe and Mask) shr 1);
        inc(Zeiger,Mem.lPitch)
      end;
    end;
  end
  else
  begin
    for Links:=0 to Width-1 do
    begin
      if Enabled and (Self.Enabled) then
        Farbe:=WerkColorTable[100-round(Links/Width*100)]
      else
        Farbe:=TimeColorTable[100-round(Links/Width*100)];
      Full:=round(Prozent/100*Width)<=Links;
      Zeiger:=Integer(Mem.lpSurface)+((Y+Beginn)*Mem.lPitch)+((X+Links) shl 1);
      for Oben:=Beginn to Ende do
      begin
        if not Full then
          PWord(Zeiger)^:=Farbe
        else
          PWord(Zeiger)^:=(Farbe and Mask) shr 1;
        inc(Zeiger,Mem.lPitch)
      end;
    end;
  end;
end;

procedure TDXSchiffItems.DrawSlot(X, Y, Nummer: Integer;
  Surface: TDirectDrawSurface;const Mem: TDDSurfaceDesc);
var
  TempRect  : TRect;
  Item      : TLagerItem;
  Text      : String;
  Schiff    : Integer;
  Anzahl    : Integer;
  Prozent   : Integer;
  Color1,Color2: TBlendColor;
begin
  TempRect:=Rect(X,Y,X+fSlotWidth,Y+ItemHeight);
  if not OverlapRect(TempRect,fRedrawRect) then
    exit;

  if DrawEnabled then
  begin
    Color1:=bcMaroon;
    Color2:=bcRed;
  end
  else
  begin
    Color1:=bcGray;
    Color2:=bcStdGray;
  end;

  Frame3D(Surface,Mem,X,Y,X+fSlotWidth,Y+ItemHeight,Color1,Color2);
  if AlphaElements then
  begin
    dec(TempRect.Bottom);
    IntersectRect(TempRect,TempRect,fRedrawRect);
    BlendRectangle(TempRect,100,Color1,Surface,Mem);
  end;

  Item:=lager_api_GetItem(fItems[Nummer].ID)^;
  Item.Anzahl:=lager_api_GetItemCountInBase(fBasisID,fItems[Nummer].ID);

  lager_api_DrawItem(Item.ImageIndex,Surface,X+4,Y+4);

  YellowStdFont.Draw(Surface,X+42,Y+4,Item.Name);
  if fItems[Nummer].Schiff=nil then
  begin
    Text:=Format(FInSchiffRight,[0]);
    Schiff:=0;
  end
  else
  begin
    Schiff:=fItems[Nummer].Schiff.Anzahl;
    Text:=Format(FInSchiffRight,[Schiff]);
  end;
  Anzahl:=Schiff+Item.Anzahl;
  if Enabled and (Anzahl>0) then
  begin
    Prozent:=round(Schiff/Anzahl*100);
    Rectangle(Surface,Mem,X+61,Y+26,X+fSlotWidth-29,Y+46,bcRed);
  end
  else
  begin
    Prozent:=0;
    Rectangle(Surface,Mem,X+61,Y+26,X+fSlotWidth-29,Y+46,bcDisabled);
  end;

  DrawFortSchritt(Surface,Mem,X+62,Y+27,fSlotWidth-92,Prozent,Anzahl>0);

  if not ((Prozent=0) or (Prozent=100)) then
    VLine(Surface,Mem,Y+25,Y+46,X+62+(((fSlotWidth-92)*Prozent) div 100),bcYellow);
    
  WhiteStdFont.Draw(Surface,X+64,Y+29,Format(FInLager,[Item.Anzahl]));
  WhiteStdFont.Draw(Surface,X+fSlotWidth-33-WhiteStdFont.TextWidth(Text),Y+29,Text);
  DrawButton(Surface,Mem,Nummer,X+40,Y+26,2,(Schiff>0) and Enabled);
  DrawButton(Surface,Mem,Nummer,X+fSlotWidth-28,Y+26,0,(Item.Anzahl>0) and Enabled);
end;

function TDXSchiffItems.ItemAtIndex(Index: Integer): PLagerItem;
begin
  result:=lager_api_GetItem(fItems[Index].ID);
end;

procedure TDXSchiffItems.MouseDown(Button: TMouseButton; X, Y: Integer);
begin
  if fRaumschiff.Model.LagerPlatz=0 then
    exit;

  if Button=mbLeft then
  begin
    if fOverButton=3 then
    begin
      fBarClick:=fSlotInfo;
      Container.DoMouseMessage;
    end
  end
  else if Button=mbRight then
  begin
    if fSlotInfo<>-1 then
      if Assigned(fOverSlot) then fOverSlot(fSlotInfo,fSlotRect);
  end;
end;

procedure TDXSchiffItems.MouseLeave;
begin
  if fOverButton<>-1 then
    Container.IncLock;

  if Assigned(fLeaveSlot) then
    fLeaveSlot;

  fSlotInfo:=-1;

  if fOverButton<>-1 then
  begin
    fOverButton:=-1;
    Container.DecLock;
    Container.RedrawArea(fSlotRect,Container.Surface);
  end;
  Hint:='';
end;

procedure TDXSchiffItems.MouseMove(X, Y: Integer);
var
  Rect      : TRect;
  Slot      : Integer;
  OldButton : Integer;
  NeedFlip  : boolean;
  Prozent   : double;
  Anzahl    : Integer;
  Item      : TLagerItem;
  ItemID    : Cardinal;
begin
  inherited;

  if fRaumschiff.Model.LagerPlatz=0 then
    exit;

  if (fBarClick>=0) and (fBarClick<=high(fItems)) then
  begin
    if (GetKeyState(VK_LBUTTON)<0) then
    begin
      X:=(X+Left)-fBarRect.Left-62;
      Prozent:=1-max(min(X/(fSlotWidth-92),1),0);

      with fItems[fBarClick] do
      begin
        if ID=0 then
          exit;

        ItemID:=ID;

        Item:=lager_api_GetItem(ItemID)^;
        Item.Anzahl:=lager_api_GetItemCountInBase(fBasisID,ItemID);

        Assert((Schiff=nil) or (Item.ID=Schiff.ID));

        Anzahl:=Item.Anzahl;

        if Schiff<>nil then
          inc(Anzahl,Schiff.Anzahl);

        Anzahl:=round(Anzahl*Prozent);

        if Anzahl<>Item.Anzahl then
        begin
          fRaumschiff.ChangeItem(ID,Item.Anzahl-Anzahl);

          if Schiff=nil then
            AktuItems;

          Item:=lager_api_GetItem(ItemID)^;
          Item.Anzahl:=lager_api_GetItemCountInBase(fBasisID,ItemID);

          if Anzahl=Item.Anzahl then
          begin
            Container.IncLock;
            Container.RedrawArea(fBarRect,Container.Surface);
            Container.RedrawArea(Classes.Rect(Left,Top,Right,Top+22),Container.Surface);
            Container.DecLock;
            Container.DoFlip;
          end
          else
            game_api_MessageBox(ENoRoomToIns,CInformation);
        end;
      end;
    end
    else fBarClick:=-1;
    exit;
  end;
  Slot:=SlotOverPoint(Point(X,Y),Rect);
  Container.IncLock;
  NeedFlip:=false;
  if (fSlotInfo<>-1) and (Slot<>fSlotInfo) then
  begin
    fSlotInfo:=-2;
    fOverButton:=-1;
    Container.RedrawArea(fSlotRect,Container.Surface);
    NeedFlip:=true;
  end;
  if Slot<>-1 then
  begin
    dec(X,Rect.Left-Left);
    dec(Y,Rect.Top-Top);
    OldButton:=fOverButton;
    fOverButton:=-1;
    if PtInRect(Classes.Rect(40,26,60,47),Point(X,Y)) then fOverButton:=2;
    if PtInRect(Classes.Rect(fSlotWidth-28,26,fSlotWidth-8,46),Point(X,Y)) then fOverButton:=0;
    if PtInRect(Classes.Rect(62,26,fSlotWidth-30,47),Point(X,Y)) then fOverButton:=3;
    fBarRect:=Rect;
    if fOverButton<>OldButton then
    begin
      Container.RedrawArea(Rect,Container.Surface);
      NeedFlip:=true;

      Item:=lager_api_GetItem(fItems[Slot].ID)^;
      Item.Anzahl:=lager_api_GetItemCountInBase(fBasisID,fItems[Slot].ID);

      case fOverButton of
        -1 : Hint:='';
        0  :
        begin
          if Item.Anzahl>0 then
            Hint:=Format(HSIBeladen,[Item.Name])
          else
            Hint:='';
        end;
        2  :
        begin
          if (fItems[Slot].Schiff<>nil) and (Item.Anzahl>0) then
            Hint:=Format(HSIEntladen,[Item.Name])
          else
            Hint:='';
        end;
        3  :
        begin
          Hint:=ST0309220023;
        end;
      end;
    end;
  end;
  if Slot<>fSlotInfo then
  begin
    fSlotRect:=Rect;
    fSlotInfo:=Slot;
  end;
  Container.DecLock;
  if NeedFlip then Container.DoFlip;
end;

procedure TDXSchiffItems.MouseUp(Button: TMouseButton; X, Y: Integer);
var
  Item: TLagerItem;
begin
  if (fSlotInfo<0) or (fSlotInfo>high(fItems)) then
    exit;

  if Button=mbRight then
  begin
    if Assigned(fLeaveSlot) then fLeaveSlot;
    exit;
  end;

  fBarClick:=-1;

  case fOverButton of
    0 { Beladen } :
    begin
      Item:=lager_api_GetItem(fItems[fSlotInfo].ID)^;
      Item.Anzahl:=lager_api_GetItemCountInBase(fBasisID,fItems[fSlotInfo].ID);

      if Item.Anzahl=0 then
        exit;

      if fRaumschiff.ChangeItem(Item.ID,1)<>1 then
      begin
        game_api_MessageBox(ENoRoomToIns,CInformation);
        exit;
      end;

      if fItems[fSlotInfo].Schiff=nil then
      begin
        // Durch AddItem werden die Adressen ge�ndert
        // Es m�ssen deshalb alle Referenzen aktualisiert werden
        AktuItems;
      end;
    end;
    2 { Entladen } :
    begin
      if (fItems[fSlotInfo].Schiff<>nil) and (fItems[fSlotInfo].Schiff.Anzahl>0) then
        fRaumschiff.ChangeItem(fItems[fSlotInfo].ID,-1);
    end;
  end;

  Container.IncLock;
  Container.RedrawArea(fBarRect,Container.Surface);
  Container.RedrawArea(Rect(Left,Top,Right,Top+22),Container.Surface);
  Container.DecLock;
  Container.DoFlip;
end;

procedure TDXSchiffItems.OnScroll(Sender: TObject);
begin
  Redraw;
end;

procedure TDXSchiffItems.ReDrawRect(DrawRect: TRect;
  Surface: TDirectDrawSurface;var Mem: TDDSurfaceDesc);
var
  Text         : String;
  X,Y          : Integer;
  Dummy        : Integer;
  First        : boolean;
  NotEnd       : boolean;
  Max          : Integer;
begin
  if EqualRect(DrawRect,fScrollBar.ClientRect) then
    exit;
    
  IntersectRect(fRedrawRect,DrawRect,ClientRect);
  if AlphaElements then
  begin
    BlendRectangle(Rect(Left,Top,Right,Top+22),100,bcMaroon,Surface,Mem);
  end;
  FramingRect(Surface,Mem,ClientRect,[cLeftBottom,cRightBottom],11,bcMaroon);
  HLine(Surface,Mem,Left,Right-1,Top+22,bcMaroon);

  if fRaumschiff = nil then
    exit;
    
  WhiteBStdFont.Draw(Surface,Left+8,Top+5,fRaumschiff.Model.Name);
  if fRaumschiff.Model.LagerPlatz=0 then
  begin
    if AlphaElements then
      BlendRectangle(Classes.Rect(Left+152,Top+(height div 2)-38,Right-152,Top+(height div 2)+38),140,bcMaroon,Surface,Mem);

    Frame3D(Surface,Mem,Left+150,Top+(height div 2)-40,Right-150,Top+(height div 2)+40,bcRed,bcMaroon);
    WhiteBStdFont.Draw(Surface,Left+(Width shr 1)-(WhiteBStdFont.TextWidth(ST0309220024) shr 1),Top+(height div 2)-10,ST0309220024);
  end
  else
  begin
    Text:=LBLagerRaum+' '+IntToStr(fRaumschiff.Model.LagerPlatz)+Format(ST0309220025,[fRaumschiff.LagerRoomBelegt/10]);
    YellowStdFont.Draw(Surface,Right-8-YellowStdFont.TextWidth(Text),Top+5,Text);
    X:=Left+ItemPadding;
    Y:=Top+22+ItemPadding;
    First:=true;
    Dummy:=fScrollBar.Value*fSlotsPerZeile;
    NotEnd:=true;
    Max:=length(fItems);
    while (Dummy<Max) and NotEnd do
    begin
      DrawSlot(X,Y,Dummy,Surface,Mem);
      First:=not First;
      inc(X,fSlotWidth+ItemPadding);
      if First then
      begin
        inc(Y,ItemHeight+ItemPadding);
        X:=Left+ItemPadding;
      end;
      inc(Dummy);
      if Y-Top>Height-ItemHeight then NotEnd:=false;
    end;
  end;
end;

procedure TDXSchiffItems.Resize(var NewLeft, NewTop, NewWidth,
  NewHeight: Integer);
begin
  fScrollBar.SetRect(NewLeft+NewWidth-17,Top+22,17,NewHeight-22);
  fSlotWidth:=(Width-16-(ItemPadding*3)) div 2;
  fSlotsPerZeile:=2;
  fZeilen:=(NewHeight-22) div (ItemHeight+ItemPadding);
  fScrollBar.LargeChange:=fZeilen;
end;

procedure TDXSchiffItems.SetRaumschiff(const Value: TRaumschiff);
begin
  fRaumschiff := Value;
  if fRaumschiff = nil then
  begin
    Aktu;
    Enabled:=false;
    Redraw;
    exit;
  end;
  fBasisID:=fRaumschiff.GetHomeBasis.ID;

  Enabled:=sabEquipt in  fRaumschiff.Abilities;

  Aktu;
end;

function TDXSchiffItems.SlotOverPoint(Point: TPoint;
  var Rect: TRect): Integer;
var
  X         : Integer;
  Y         : Integer;
  First     : boolean;
  Dummy     : Integer;
  NotEnd    : boolean;
  TempRect  : TRect;

  function CheckSlot: boolean;
  begin
    result:=false;
    TempRect:=Classes.Rect(X,Y,X+fSlotWidth,Y+ItemHeight);
    if Y>Height-ItemHeight then exit;
    if PtInRect(TempRect,Point) then
    begin
      result:=true;
      Rect:=TempRect;
      OffsetRect(Rect,Left,Top);
      SlotOverPoint:=Dummy;
    end;
  end;

begin
  result:=-1;
  X:=ItemPadding;
  Y:=22+ItemPadding;
  First:=true;
  Dummy:=fScrollBar.Value*fSlotsPerZeile;
  NotEnd:=true;
  while (Dummy<=High(fItems)) and NotEnd do
  begin
    if CheckSlot then NotEnd:=true;
    First:=not First;
    inc(X,fSlotWidth+ItemPadding);
    if First then
    begin
      inc(Y,ItemHeight+ItemPadding);
      X:=ItemPadding;
    end;
    inc(Dummy);
    if Y-Top>Height-ItemHeight then NotEnd:=false;
  end;
//  if NotEnd then
//  begin
//    Dummy:=fRaumschiff.ItemCount;
//    CheckSlot;
//  end;
end;

procedure TDXSchiffItems.VisibleChange;
begin
  if Visible then
  begin
    fScrollBar.Visible:=fScrollVisible;
  end
  else fScrollBar.Visible:=false;
end;

end.
