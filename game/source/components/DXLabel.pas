{********************************************************************************
*										*
* Copyright (C) 2004  X-Force Team - jim_raynor@web.de				*
*										*
* This file is part of X-Force: Fight For Destiny (X-Force).			*
*										*
* X-Force is free software; you can redistribute it and/or modify		*
* it under the terms of the GNU General Public License as published by		*
* the Free Software Foundation; either version 2 of the License, or		*
* (at your option) any later version.						*
*										*
* X-Force is distributed in the hope that it will be useful,			*
* but WITHOUT ANY WARRANTY; without even the implied warranty of		*
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the			*
* GNU General Public License for more details.					*
*										*
* You should have received a copy of the GNU General Public License		*
* along with X-Force; if not, write to the Free Software			*
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA	*
*										*
*********************************************************************************
*										*
* Diese Unit stellt ein Label zur Verf�gung mit der man Texte anzeigen lassen	*
* kann.										*
*										*
********************************************************************************}

{$I ../Settings.inc}

unit DXLabel;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  DXContainer, DXDraws, math, Blending, TraceFile, KD4Utils, DirectDraw,
  DirectFont;

type
  TDXLabel = class(TDXComponent)
  private
    fLines      : TStringList;
    fAlign      : TAlignment;
    fAutoSize   : boolean;
    fBlending   : boolean;
    fBColor     : TBlendcolor;
    fBorColor   : TColor;
    fTextRect   : TRect;
    fFont       : TDirectFont;
    procedure SetText(const Value: String);
    procedure SetAlign(const Value: TAlignment);
    procedure SetAutoSize(const Value: boolean);
    function GetText: String;
    procedure SetBorColor(const Value: TColor);
    { Private-Deklarationen }
  protected
    procedure ChangeFont(Sender: TObject);
    procedure CalcAutoSize;
    { Protected-Deklarationen }
  public
    constructor Create(Page: TDXPage);override;
    destructor Destroy;override;
    procedure Draw(Surface: TDirectDrawSurface;var Mem: TDDSurfaceDesc);override;
    property Text        : String read GetText write SetText;
    property Alignment   : TAlignment read fAlign write SetAlign;
    property AutoSize    : boolean read fAutoSize write SetAutoSize;
    property Blending    : boolean read fBlending write fBlending;
    property BlendColor  : TBlendcolor read fBColor write fBColor;
    property BorderColor : TColor write SetBorColor;
  end;

implementation

uses
  Defines;

{ TDXLabel }

procedure TDXLabel.CalcAutoSize;
var
  Dummy         : Integer;
  Wei           : Integer;
  Hei           : Integer;
  BLeft,BRight  : Integer;
  Lines         : Integer;
begin
  if fFont=nil then exit;
  if fLines.Count=0 then exit;
  BLeft:=Right;
  BRight:=0;
  for Dummy:=0 to fLines.Count-1 do
  begin
    if fAlign=taLeftJustify then
    begin
      BLeft:=Left;
      BRight:=max(BRight,Left+fFont.TextWidth(fLines[Dummy]));
    end
    else if fAlign=taRightJustify then
    begin
      BRight:=Right;
      BLeft:=min(Right-fFont.TextWidth(fLines[Dummy])-1,BLeft);
    end
    else if fAlign=taCenter then
    begin
      BLeft:=min(BLeft,Left+(Width shr 1)-(fFont.TextWidth(fLines[Dummy]) shr 1));
      BRight:=max(BRight,(Left+(Width shr 1)-(fFont.TextWidth(fLines[Dummy]) shr 1))+fFont.TextWidth(fLines[Dummy]));
    end;
    BRight:=min(Bright,ScreenWidth-5);
    dec(BLeft,5);
    inc(BRight,5);
  end;
  Lines:=Height div fFont.TextHeight('a');
  fTextRect:=Rect(BLeft,Top+((Lines-fLines.Count)*(fFont.TextHeight('a')-1)),BRight,Bottom-1);
  if not fAutoSize then exit;
  Wei:=fFont.TextWidth(fLines[0])+1;
  hei:=fFont.TextHeight(fLines[0])+2;
  for Dummy:=1 to fLines.Count-1 do
  begin
    Wei:=max(Wei,fFont.TextWidth(fLines[dummy])+1);
    inc(Hei,fFont.TextHeight(fLines[dummy])+2);
  end;
  Width:=Wei;
  Height:=Hei;
end;

constructor TDXLabel.Create(Page: TDXPage);
begin
  inherited;
  Font.OnChange:=ChangeFont;
  fLines:=TStringList.Create;
  fAutoSize:=true;
  fFont:=nil;
end;

destructor TDXLabel.Destroy;
begin
  fLines.Free;
  inherited;
end;

procedure TDXLabel.Draw(Surface: TDirectDrawSurface;var Mem: TDDSurfaceDesc);
var
  Dummy    : Integer;
  DrawTop  : Integer;
  DrawLeft : Integer;
begin
  if fFont=nil then exit;
  if fLines.Count=0 then exit;
  DrawLeft:=Left;
  if fBlending and AlphaElements then
  begin
    BlendRectangle(fTextRect,180,fBColor,Surface);
    Rectangle(Surface,Mem,fTextRect,fBorColor);
  end;
  DrawTop:=fTextRect.Top;
  for Dummy:=0 to fLines.Count-1 do
  begin
    if fAlign=taLeftJustify then DrawLeft:=Left
    else if fAlign=taRightJustify then DrawLeft:=Right-fFont.TextWidth(fLines[Dummy])-1
    else if fAlign=taCenter then DrawLeft:=Left+(Width shr 1)-(fFont.TextWidth(fLines[Dummy]) shr 1);
    fFont.Draw(Surface,DrawLeft,DrawTop,fLines[Dummy]);
    inc(DrawTop,fFont.TextHeight('a'));
  end;
end;

procedure TDXLabel.ChangeFont(Sender: TObject);
begin
  fFont:=FontEngine.FindDirectFont(Font,clBlack);
  CalcAutoSize;
  Container.RedrawControl(Self,Container.Surface);
end;

function TDXLabel.GetText: String;
begin
  result:=fLines.Text;
end;

procedure TDXLabel.SetAlign(const Value: TAlignment);
begin
  fAlign := Value;
  Redraw;
end;

procedure TDXLabel.SetAutoSize(const Value: boolean);
begin
  fAutoSize := Value;
  CalcAutoSize;
end;

procedure TDXLabel.SetText(const Value: String);
var
  Mitte     : Integer;
  FoundWrap : boolean;
  BreakPos  : Integer;
  Dummy     : Integer;
  Line      : String;
begin
  if fFont=nil then
  begin
    fLines.Text:=Value;
    exit;
  end;
  if (not fAutoSize) and (Height>(fFont.TextHeight('a')+4)) then
  begin
    if Value<>'' then
    begin
      fLines.Clear;
      if fFont.TextWidth(Value)>Width-8 then
      begin
        FoundWrap:=false;
        BreakPos:=1;
        Dummy:=0;
        Mitte:=length(Value) shr 1;
        while not FoundWrap do
        begin
          if Value[Mitte-Dummy]=' ' then
          begin
            FoundWrap:=true;
            BreakPos:=Mitte-Dummy;
          end
          else if Value[Mitte+Dummy]=' ' then
          begin
            FoundWrap:=true;
            BreakPos:=Mitte+Dummy;
          end;
          inc(Dummy);
        end;
        Line:='';
        Line:=Cut(Value,1,BreakPos);
        fLines.Add(Trim(Line));
        Line:='';
        Line:=Cut(Value,BreakPos,length(Value));
        fLines.Add(Trim(Line));
      end
      else fLines.Text:=Value;
    end
    else fLines.Clear;
  end
  else
    fLines.Text:=Value;
  CalcAutoSize;
  Redraw;
end;

procedure TDXLabel.SetBorColor(const Value: TColor);
begin
  fBorColor:=Container.Surface.ColorMatch(Value);
end;

end.
