{********************************************************************************
*										*
* Copyright (C) 2004  X-Force Team - jim_raynor@web.de				*
*										*
* This file is part of X-Force: Fight For Destiny (X-Force).			*
*										*
* X-Force is free software; you can redistribute it and/or modify		*
* it under the terms of the GNU General Public License as published by		*
* the Free Software Foundation; either version 2 of the License, or		*
* (at your option) any later version.						*
*										*
* X-Force is distributed in the hope that it will be useful,			*
* but WITHOUT ANY WARRANTY; without even the implied warranty of		*
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the			*
* GNU General Public License for more details.					*
*										*
* You should have received a copy of the GNU General Public License		*
* along with X-Force; if not, write to the Free Software			*
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA	*
*										*
*********************************************************************************
*										*
* Diese Unit stellt ein SpinEdit zur Verf�gung mit dem man Werte erh�hen bzw.	*
* verringern kann.								*
*										*
********************************************************************************}

{$I ../Settings.inc}

unit DXSpinEdit;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  DXDraws, DXContainer, KD4Utils, Defines, extctrls, NGtypes, Blending,
  DXClass, XForce_types, TraceFile, DirectDraw, DirectFont, StringConst;

type
  TDXSpinEdit = class(TDXComponent)
  private
    fBorderColor   : TBlendColor;
    fFocusColor    : TBlendColor;
    fFormat        : String;
    fValue         : Integer;
    fLeftMargin    : Integer;
    fTopMargin     : Integer;
    fStep          : Integer;
    fMin           : Integer;
    fOverState     : TButtonState;
    fSmallOver     : TButtonState;
    fDownState     : TButtonState;
    fDelay         : Integer;
    fSmallDown     : TButtonState;
    fMax           : Integer;
    fIncrement     : TNotifyEvent;
    fDecrement     : TNotifyEvent;
    fCanIncrement  : TCanChangeEvent;
    fCanDecrement  : TCanChangeEvent;
    fCorners       : TRoundCorners;
    fCorner        : TCorners;
    fSpinName      : TGetSpinName;
    fInterval      : Integer;
    fSmallButton   : boolean;
    fSmallStep     : Integer;
    fBlendColor    : TBlendColor;
    fBlendValue    : Integer;
    procedure SetBorderColor(const Value: TColor);
    procedure SetFocusColor(const Value: TColor);
    procedure SetFormat(const Value: String);
    procedure SetValue(const Value: Integer);
    procedure SetLeftMargin(const Value: Integer);
    procedure SetTopMargin(const Value: Integer);
    procedure SetMax(const Value: Integer);
    procedure SetMin(const Value: Integer);
    procedure SetCorners(const Value: TRoundCorners);
    procedure SetSmallStep(const Value: Integer);
    function GetColor: TBlendColor;
    function GetFrameRect: TRect;
    { Private-Deklarationen }
  protected
    procedure CorrectValue;
    procedure DrawLeft(Surface: TDirectDrawSurface;Mem: TDDSurfaceDesc;Color: TBlendColor);
    procedure DrawCenter(Surface: TDirectDrawSurface;Mem: TDDSurfaceDesc;Color: TBlendColor);
    procedure DrawRight(Surface: TDirectDrawSurface;Mem: TDDSurfaceDesc;Color: TBlendColor);
    procedure CreateMouseRegion;override;
    procedure Change(Inc: boolean;Small: boolean = false);
    procedure DoScroll(Direction: TScrollDirection; Pos: TPoint);override;
    function DoInterval(Sender: TObject;Frames: Integer): boolean;
    function GetDrawBlendColor: TBlendColor;
    { Protected-Deklarationen }
  public
    constructor Create(Page: TDXPage);override;
    procedure Draw(Surface: TDirectDrawSurface;var Mem: TDDSurfaceDesc);override;
    procedure KeyDown(var Key: Word;State: TShiftState);override;
    procedure ReDrawRect(DrawRect: TRect;Surface: TDirectDrawSurface;var Mem: TDDSurfaceDesc);override;
    procedure MouseMove(X,Y: Integer);override;
    procedure MouseLeave;override;
    procedure MouseDown(Button: TMouseButton;X,Y: Integer);override;
    procedure MouseUp(Button: TMouseButton;X,Y: Integer);override;
    property BorderColor      : TColor write SetBorderColor;
    property FocusColor       : TColor write SetFocusColor;
    property BlendColor       : TBlendColor read fBlendColor write fBlendColor;
    property BlendValue       : Integer read fBlendValue write fBlendValue;
    property FormatString     : String read fFormat write SetFormat;
    property Value            : Integer read fValue write SetValue;
    property LeftMargin       : Integer read fLeftMargin write SetLeftMargin;
    property TopMargin        : Integer read fTopMargin write SetTopMargin;
    property Step             : Integer read fStep write fStep;
    property Min              : Integer read fMin write SetMin;
    property Max              : Integer read fMax write SetMax;
    property Interval         : Integer read fInterval write fInterval;
    property SmallStepButton  : boolean read fSmallButton write fSmallButton;
    property SmallStep        : Integer read fSmallStep write SetSmallStep;
    property OnIncrement      : TNotifyEvent read fIncrement write fIncrement;
    property OnDecrement      : TNotifyEvent read fDecrement write fDecrement;
    property CanIncrement     : TCanChangeEvent read fCanIncrement write fCanIncrement;
    property CanDecrement     : TCanChangeEvent read fCanDecrement write fCanDecrement;
    property RoundCorners     : TRoundCorners read fCorners write SetCorners;
    property GetValueText     : TGetSpinName read fSpinName write fSpinName;
    { Published-Deklarationen }
  end;

implementation

{ TDXSpinEdit }

procedure TDXSpinEdit.Change(Inc: boolean;Small: boolean);
var
  OldValue: Integer;
  CanChange: boolean;
begin
  if not Enabled then exit;
  CanChange:=true;
  OldValue:=Value;
  Small:=Small and fSmallButton;
  if Inc then
  begin
    if Value=max then
      CanChange:=false
    else
      if Assigned(fCanIncrement) then fCanIncrement(Self,CanChange);
    if not CanChange then exit;
    Container.PlaySound(SInkrement);
    Container.Lock;
    if Small then Value:=Value+fSmallStep else Value:=Value+fStep;
    Container.LoadGame(false);
    if Value<>OldValue then if Assigned(fIncrement) then fIncrement(Self);
    Container.DecLock;
    Container.RedrawControl(Self,Container.Surface);
  end
  else
  begin
    if Value=min then
      CanChange:=false
    else
      if Assigned(fCanDecrement) then
    fCanDecrement(Self,CanChange);
    if not CanChange then exit;
    Container.PlaySound(SDekrement);
    Container.Lock;
    if Small then Value:=Value-fSmallStep else Value:=Value-fStep;
    Container.LoadGame(false);
    if Value<>OldValue then if Assigned(fDecrement) then fDecrement(Self);
    Container.DecLock;
    Container.RedrawControl(Self,Container.Surface);
  end;
end;

procedure TDXSpinEdit.CorrectValue;
var
  Changed: boolean;
begin
  Changed:=false;
  if fValue>fMax then
  begin
    fValue:=fMax;
    Changed:=true;
  end;
  if fValue<fMin then
  begin
    fValue:=fMin;
    Changed:=true;
  end;
  if Changed then Redraw;
end;

constructor TDXSpinEdit.Create(Page: TDXPage);
begin
  inherited;
  TabStop:=true;
  fSmallButton:=false;
  fLeftMargin:=5;
  fTopMargin:=3;
  fBlendColor:=bcMaroon;
  fOverState:=bsNone;
  fSmallOver:=bsNone;
  fDownState:=bsNone;
  fSmallDown:=bsNone;
  fInterval:=50;
  Value:=0;
  fMin:=0;
  fMax:=100;
  fStep:=1;
  fSmallStep:=0;
  fFormat:=FFloat;
  fCorners:=rcBottom;
  fBlendValue:=75;
end;

procedure TDXSpinEdit.Draw(Surface: TDirectDrawSurface;var Mem: TDDSurfaceDesc);
begin
  ReDrawRect(ClientRect,Surface,Mem);
end;

procedure TDXSpinEdit.DrawCenter(Surface: TDirectDrawSurface;Mem: TDDSurfaceDesc;Color: TBlendColor);
var
  X             : Integer;
  S             : String;
  ButtonWidth   : Integer;
  DFont         : TDirectFont;
  TempRect      : TRect;
  fFontCol      : TColor;
begin
  if fSmallButton then ButtonWidth:=Height*2 else ButtonWidth:=Height;
  TempRect:=Rect(Left+ButtonWidth+1,Top+1,Right-ButtonWidth-1,Bottom-1);
  if AlphaElements then
  begin
    BlendRectangle(TempRect,fBlendValue,GetDrawBlendColor,Surface,Mem);
  end;
  VLine(Surface,Mem,Top+1,Bottom-1,Left+ButtonWidth+1,Color);
  VLine(Surface,Mem,Top+1,Bottom-1,Right-ButtonWidth-1,Color);
  S:='';
  if fFormat=EmptyStr then
  begin
    if Assigned(fSpinName) then fSpinName(fValue,S);
  end
  else
  begin
    S:=Format(fFormat,[fValue/1]);
  end;
  if DrawEnabled then
    DFont:=FontEngine.FindDirectFont(Font,clBlack)
  else
  begin
    fFontCol:=Font.Color;
    Font.Color:=$00606060;
    DFont:=FontEngine.FindDirectFont(Font,clBlack);
    Font.Color:=fFontCol;
  end;

  X:=Left+(Width shr 1)-(DFont.TextWidth(S) shr 1);
  DFont.TextRect(Surface,TempRect,X,Top+fTopMargin,S);
end;

procedure TDXSpinEdit.DrawLeft(Surface: TDirectDrawSurface;Mem: TDDSurfaceDesc;Color: TBlendColor);
var
  incx       : Integer;
  Alpha      : Integer;
  Image      : Integer;
begin
  if DrawEnabled and (fDownState=bsLeft) then incx:=6 else incx:=5;
  if DrawEnabled and (fOverState=bsLeft) then Alpha:=fBlendValue+75 else Alpha:=fBlendValue;
  if AlphaElements then
    BlendRoundRect(ClientRect,Alpha,GetDrawBlendColor,Surface,Mem,11,fCorner,Rect(Left+1,Top+1,Height+Left+1,Height+Top-1));
  if fSmallButton then Image:=9 else Image:=2;
  Container.ImageList.Items[0].Draw(Surface,Left+(Height shr 1)-incx,Top+(Height shr 1)-6,Image);
  if fSmallButton then
  begin
    if DrawEnabled and (fSmallDown=bsLeft) then incx:=6 else incx:=5;
    if DrawEnabled and (fSmallOver=bsLeft) then Alpha:=fBlendValue+75 else Alpha:=fBlendValue;
    if AlphaElements then
      BlendRoundRect(ClientRect,Alpha,GetDrawBlendColor,Surface,Mem,11,fCorner,Rect(Left+Height+2,Top+1,(Height*2)+Left+1,Height+Top-1));
    VLine(Surface,Mem,Top+1,Bottom-1,Left+Height+1,Color);
    Container.ImageList.Items[0].Draw(Surface,Left+Height+(Height shr 1)-incx,Top+(Height shr 1)-6,2);
  end;
end;

procedure TDXSpinEdit.DrawRight(Surface: TDirectDrawSurface;Mem: TDDSurfaceDesc;Color: TBlendColor);
var
  incx      : Integer;
  Alpha     : Integer;
  Image     : Integer;
begin
  if fDownState=bsRight then incx:=6 else incx:=7;
  if fOverState=bsRight then Alpha:=fBlendValue+75 else Alpha:=fBlendValue;
  if AlphaElements then
    BlendRoundRect(ClientRect,Alpha,GetDrawBlendColor,Surface,Mem,11,fCorner,Rect(Right-Height,Top+1,Right-1,Height+Top-1));
  if fSmallButton then Image:=8 else Image:=0;
  Container.ImageList.Items[0].Draw(Surface,Right-(Height shr 1)-incx,Top+(Height shr 1)-6,Image);
  if fSmallButton then
  begin
    if fSmallDown=bsRight then incx:=6 else incx:=7;
    if fSmallOver=bsRight then Alpha:=fBlendValue+75 else Alpha:=fBlendValue;
    if AlphaElements then
      BlendRoundRect(ClientRect,Alpha,GetDrawBlendColor,Surface,Mem,11,fCorner,Rect(Right-(Height*2),Top+1,Right-Height-1,Height+Top-1));
    VLine(Surface,Mem,Top+1,Bottom-1,Right-Height-1,Color);
    Container.ImageList.Items[0].Draw(Surface,Right-Height-(Height shr 1)-incx,Top+(Height shr 1)-6,0);
  end;
end;

function TDXSpinEdit.GetFrameRect: TRect;
begin
  result:=ClientRect;
  if fCorners in [rcLeft,rcLeftTop,rcLeftBottom,rcNone] then inc(result.Right,20);
  if fCorners in [rcTop,rcLeftTop,rcRightTop,rcNone] then inc(result.Bottom,20);
  if fCorners in [rcRight,rcRightTop,rcRightBottom,rcNone] then dec(result.Left,20);
  if fCorners in [rcBottom,rcLeftBottom,rcRightBottom,rcNone] then dec(result.Top,20);
end;

function TDXSpinEdit.DoInterval(Sender: TObject;Frames: Integer): boolean;
begin
  if fDelay>0 then
  begin
    dec(fDelay);
    exit;
  end;
  result:=false;
  while (Frames>0) do
  begin
    if fDownState=bsLeft then Change(false)
    else if fDownState=bsright then Change(true)
    else if fSmallDown=bsLeft then Change(false,true)
    else if fSmallDown=bsRight then Change(true,true);
    result:=true;
    dec(Frames);
  end;
end;

procedure TDXSpinEdit.KeyDown(var Key: Word; State: TShiftState);
begin
  inherited;
  if (Char(Key)=#37) or (Char(Key)=#40)then
  begin
    Change(false,(ssCtrl in State));
  end;
  if (Char(Key)=#39) or (Char(Key)=#38) then
  begin
    Change(true,(ssCtrl in State));
  end;
end;

procedure TDXSpinEdit.MouseDown(Button: TMouseButton; X, Y: Integer);
begin
  inherited;
  if Button<>mbLeft then exit;
  if PtInRect(Rect(0,0,Height,Height),Point(x,y)) then
  begin
    if fDownState<>bsLeft then
    begin
      Container.AddFrameFunction(DoInterval,nil,fInterval);
      fDownState:=bsLeft;
      fSmallDown:=bsNone;
      fDelay:=5;
      Change(false);
    end;
  end
  else if PtInRect(Rect(Width-Height,0,Width,Height),Point(x,y)) then
  begin
    if fDownState<>bsRight then
    begin
      Container.AddFrameFunction(DoInterval,nil,fInterval);
      fDownState:=bsRight;
      fSmallDown:=bsNone;
      fDelay:=5;
      Change(true);
    end;
  end
  else if fSmallButton and (PtInRect(Rect(Height,0,(Height*2),Height),Point(x,y))) then
  begin
    if fSmallDown<>bsLeft then
    begin
      Container.AddFrameFunction(DoInterval,nil,fInterval);
      fDownState:=bsNone;
      fSmallDown:=bsLeft;
      fDelay:=5;
      Change(false,true);
    end;
  end
  else if fSmallButton and (PtInRect(Rect(Width-(Height*2),0,Width-Height,Height),Point(x,y))) then
  begin
    if fSmallDown<>bsRight then
    begin
      Container.AddFrameFunction(DoInterval,nil,fInterval);
      fDownState:=bsNone;
      fSmallDown:=bsRight;
      fDelay:=5;
      Change(true,true);
    end;
  end;
end;

procedure TDXSpinEdit.MouseLeave;
begin
  inherited;
  Container.DeleteFrameFunction(DoInterval,nil);
  if (fOverstate=bsNone) and (fSmallOver=bsNone) and (fDownstate=bsNone) and (fSmallDown=bsNone) then exit;
  fOverState:=bsNone;
  fDownState:=bsNone;
  fSmallDown:=bsNone;
  fSmallOver:=bsNone;
  Redraw;
end;

procedure TDXSpinEdit.MouseMove(X, Y: Integer);
var
  MouseDown: boolean;
begin
  inherited;
  MouseDown:=(GetKeyState(VK_LBUTTON)<0);
  if PtInRect(Rect(0,0,Height,Height),Point(x,y)) then
  begin
    if fOverState<>bsLeft then
    begin
      fOverState:=bsLeft;
      fSmallOver:=bsNone;
      if MouseDown then
      begin
        fDownState:=bsLeft;
        fSmallDown:=bsNone;
        Container.AddFrameFunction(DoInterval,nil,fInterval);
      end
      else
      begin
        Container.PlaySound(SOver);
        Container.DeleteFrameFunction(DoInterval,nil);
      end;
      Redraw;
    end;
  end
  else if PtInRect(Rect(Width-Height,0,Width,Height),Point(x,y)) then
  begin
    if fOverState<>bsRight then
    begin
      fOverState:=bsRight;
      fSmallOver:=bsNone;
      if MouseDown then
      begin
        fDownState:=bsRight;
        fSmallDown:=bsNone;
        Container.AddFrameFunction(DoInterval,nil,fInterval);
      end
      else
      begin
        Container.PlaySound(SOver);
        Container.AddFrameFunction(DoInterval,nil,fInterval);
      end;
      Redraw;
    end;
  end
  else if fSmallButton and (PtInRect(Rect(Width-(Height*2),0,Width-Height,Height),Point(x,y))) then
  begin
    if fSmallOver<>bsRight then
    begin
      fSmallOver:=bsRight;
      fOverState:=bsNone;
      if MouseDown then
      begin
        fSmallDown:=bsRight;
        fDownState:=bsNone;
        Container.AddFrameFunction(DoInterval,nil,fInterval);
      end
      else
      begin
        Container.PlaySound(SOver);
        Container.DeleteFrameFunction(DoInterval,nil);
      end;
      Redraw;
    end;
  end
  else if fSmallButton and (PtInRect(Rect(Height,0,(Height*2),Height),Point(x,y))) then
  begin
    if fSmallOver<>bsLeft then
    begin
      fSmallOver:=bsLeft;
      fOverState:=bsNone;
      if MouseDown then
      begin
        fSmallDown:=bsLeft;
        fDownState:=bsNone;
        Container.AddFrameFunction(DoInterval,nil,fInterval);
      end
      else
      begin
        Container.PlaySound(SOver);
        Container.DeleteFrameFunction(DoInterval,nil);
      end;
      Redraw;
    end;
  end
  else
  begin
    if (fOverState=bsNone) and (fSmallOver=bsNone) and (fDownState=bsNone) and (fSmallDown=bsNone) then exit;
    fOverState:=bsNone;
    fSmallOver:=bsNone;
    fSmallDown:=bsNone;
    fDownState:=bsNone;
    Container.DeleteFrameFunction(DoInterval,nil);
    Redraw;
  end;
end;

procedure TDXSpinEdit.MouseUp(Button: TMouseButton; X, Y: Integer);
begin
  fDownState:=bsNone;
  fSmallDown:=bsNone;
  Container.DeleteFrameFunction(DoInterval,nil);
  Redraw;
end;

procedure TDXSpinEdit.ReDrawRect(DrawRect: TRect; Surface: TDirectDrawSurface;var Mem: TDDSurfaceDesc);
var
  ButtonWidth : Integer;
  Color       : TBlendColor;
begin
  if fSmallButton then ButtonWidth:=Height*2 else ButtonWidth:=Height;
  Color:=GetColor;
  if OverlapRect(DrawRect,Rect(Left,Top,ButtonWidth+Left,Height+Top)) then
  begin
    DrawLeft(Surface,Mem,Color);
  end;
  if OverlapRect(DrawRect,Rect(Right-ButtonWidth,Top,Right,Height+Top)) then
  begin
    DrawRight(Surface,Mem,Color);
  end;
  if OverlapRect(DrawRect,Rect(ButtonWidth+Left,Top,Right-ButtonWidth,Height+Top)) then
  begin
    DrawCenter(Surface,Mem,Color);
  end;
  FramingRect(Surface,Mem,ClientRect,fCorner,11,Color);
end;

procedure TDXSpinEdit.SetBorderColor(const Value: TColor);
begin
  fBorderColor := Container.Surface.ColorMatch(Value);
  Redraw;
end;

procedure TDXSpinEdit.SetCorners(const Value: TRoundCorners);
begin
  fCorners := Value;
  fCorner:=[];
  if fCorners in [rcLeft,rcTop,rcLeftTop,rcAll] then include(fCorner,cLeftTop);
  if fCorners in [rcRight,rcTop,rcRightTop,rcAll] then include(fCorner,cRightTop);
  if fCorners in [rcLeft,rcBottom,rcLeftBottom,rcAll] then include(fCorner,cLeftBottom);
  if fCorners in [rcRight,rcBottom,rcRightBottom,rcAll] then include(fCorner,cRightBottom);
  RecreateMouseRegion;
  Redraw;
end;

procedure TDXSpinEdit.SetFocusColor(const Value: TColor);
begin
  fFocusColor := Container.Surface.ColorMatch(Value);
  Redraw;
end;

procedure TDXSpinEdit.SetFormat(const Value: String);
begin
  fFormat := Value;
  Redraw;
end;

procedure TDXSpinEdit.SetLeftMargin(const Value: Integer);
begin
  fLeftMargin := Value;
  Redraw;
end;

procedure TDXSpinEdit.SetMax(const Value: Integer);
begin
  fMax := Value;
  if fMax<fMin then fMax:=fMin;
  CorrectValue;
end;

procedure TDXSpinEdit.SetMin(const Value: Integer);
begin
  fMin := Value;
  if fMin>fMax then fMin:=fMax;
  CorrectValue;
end;

procedure TDXSpinEdit.SetTopMargin(const Value: Integer);
begin
  fTopMargin := Value;
  Redraw;
end;

procedure TDXSpinEdit.SetValue(const Value: Integer);
begin
  fValue := Value;
  CorrectValue;
  Container.RedrawArea(Rect(Height+Left+10,Top+2,Right-Height-10,Height+Top-2),Container.Surface);
end;

procedure TDXSpinEdit.CreateMouseRegion;
var
  FrameRect: TRect;
  RoundRGN : HRGN;
begin
  FrameRect:=GetFrameRect;
  RoundRGN:=CreateRoundRectRgn(FrameRect.Left,FrameRect.Top,FrameRect.Right+1,FrameRect.Bottom+1,20,20);
  fMouseRegion:=CreateRectRgn(Left,Top,Right,Bottom);
  CombineRgn(fMouseRegion,fMouseRegion,RoundRGN,RGN_AND);
  DeleteObject(RoundRGN);
end;

procedure TDXSpinEdit.DoScroll(Direction: TScrollDirection; Pos: TPoint);
begin
  if Direction in [sdUp,sdLeft] then Change(false);
  if Direction in [sdDown,sdRight] then Change(true);
end;

procedure TDXSpinEdit.SetSmallStep(const Value: Integer);
begin
  fSmallStep := Value;
  fSmallButton:=Value<>0;
end;

function TDXSpinEdit.GetColor: TBlendColor;
begin
  if not DrawEnabled then
    result:=bcDisabled
  else if HasFocus then
    result:=fFocusColor
  else
    result:=fBorderColor;
end;

function TDXSpinEdit.GetDrawBlendColor: TBlendColor;
begin
  if DrawEnabled then
    result:=fBlendColor
  else
    result:=bcDisabled;
end;

end.
