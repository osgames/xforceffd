{********************************************************************************
*										*
* Copyright (C) 2004  X-Force Team - jim_raynor@web.de				*
*										*
* This file is part of X-Force: Fight For Destiny (X-Force).			*
*										*
* X-Force is free software; you can redistribute it and/or modify		*
* it under the terms of the GNU General Public License as published by		*
* the Free Software Foundation; either version 2 of the License, or		*
* (at your option) any later version.						*
*										*
* X-Force is distributed in the hope that it will be useful,			*
* but WITHOUT ANY WARRANTY; without even the implied warranty of		*
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the			*
* GNU General Public License for more details.					*
*										*
* You should have received a copy of the GNU General Public License		*
* along with X-Force; if not, write to the Free Software			*
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA	*
*										*
*********************************************************************************
*										*
* In dieser Unit werden die wichtigsten Typen (records, Aufz�hlungen) definiert *
* die an mehreren Stellen in X-Force verwendet werden.				*
*										*
********************************************************************************}

{$I ../Settings.inc}

unit XForce_types;

interface

uses Windows,SysUtils,Graphics, controls, classes, NotifyList;

{****************************************}
{     Versionskonstanten f�r Dateien     }
{****************************************}

const
  TileVersion         : Integer = $2153AE98;          // TileSet-Version
  MapVersion          : Integer = 2951;          // Karten-Version
  AlienVersion        : Integer = $3FE0FEF6;     // Alien.dat-Version
  AlienEditVersion    : Integer = $5D0FADFF;
  UFOVersion          : Integer = $47EC40F3;     // UFO.dat-Version
  UFOMVersion         : Integer = $18010EC8;
  UFOOVersion         : Integer = $5FB5B86A;
  SkriptVersion       : Integer = $113397FD;
  DataBlock           : Integer = $180FCE5E;
  ShieldVersion       : Integer = $16213ABC;
  NameVersion         : Integer = 480;           // Namen.dat-Version
  NewNameVersion      : Integer = $0EBA802F;
  LagerVersion        : Integer = $09BC27F3;
  SaveVersion         : Integer = $619DF710;     // SaveGame-Version
  ForschVersion       : Integer = $3C3385CD;     // Forsch.dat-Version
  SaveProjektVersion  : Integer = $4780EEE2;
  ForschungenVersion  : Integer = $1DFF97F0;
  EinrichtVersion     : Integer = $187ED2E8;
  RSchiffVersion      : Integer = $41488B29;
  OrganVersion        : Integer = $656C3F57;
  TownVersion         : Integer = $06869CB8;
  HighVersion         : Integer = 500;           // High.dat-Version
  SchiffVersion       : Integer = $3EA61D99;
  LandVersion         : Integer = $2AFE686D;
  HeadVersion         : Integer = $5219EDA7;
  EinsatzVersion      : Integer = $30F55EBB;
  MissListVers        : Cardinal = $5B4D0799;
  MissionsVers        : Cardinal = $0FC53C81;
  BasisListID         : Cardinal = $4F6EE6B4;
  BasisVersion        : Cardinal = $7C53C9BC;
  TechnikVersion      : Cardinal = $1496670D;
  ProduktionVersion   : Cardinal = $589E859E;
  ForscherVersion     : Cardinal = $472EB175;
  ProjektVersion      : Cardinal = $2FE6557C;
  AuftragVersion      : Cardinal = $794DD2B5;
  SchiffKSVersion     : Cardinal = $289E7D5A;
  SoldatInfoVersion   : Cardinal = $414F5F7E;
  PunkteStatusVersion : Cardinal = $3A2EA8EE;
  RucksackVersion     : Cardinal = $6821765A;
  FormationVersion    : Cardinal = $660A4708;
  ManagerVersion      : Cardinal = $3094D5AE;
  PatentVersion       : Cardinal = $22E83492;
  MapScriptVersion    : Cardinal = $3E433DC8;
  ObjektVersion       : Cardinal = $0C81E1B0;
  ObjektHeadVersion   : Cardinal = $49019101;
  CustomDataVersion   : Cardinal = $0C71E407;
                                               
  FloorIndex  = 0;
  LeftIndex   = 1;
  RightIndex  = 2;
  ObjectIndex = 3;

{****************************************}
{          Typendeklarationen            }
{****************************************}
  type
    TSetHeader = record
      Name           : String;
      Autor          : String;
      Organisation   : String;
      Beschreibung   : String;
      Password       : String;
      ID             : Cardinal;
      CanImport      : boolean;
    end;

    TFrameFunction = function(Sender: TObject;Frames: Integer): boolean of object;

    TSchiffAbility =    (sabCancelBuild,
                         sabCancelWithOutRequest,
                         sabSelling,
                         sabCanTanken,
                         sabFlight,
                         sabBodenKampf,
                         sabHuntUFO,
                         sabEquipt,
                         sabChooseTarget,
                         sabClearCommand,
                         sabRepair);

    TAbilities      = set of TSchiffAbility;

    { Wird von der Funktion FillItems in }
    { KD4Utils benutzt. Dient zur        }
    { Definition von allgemeinen Filtern }
    TItemFilter    =    (ifAll,                  // F�gt alle Items hinzu
                         ifWaffen,               // F�gt nur Waffen hinzu
                         ifMine,                 // F�gt nur Minen hinzu
                         ifSensoren,             // F�gt nur Sensoren hinzu
                         ifMotoren,              // F�gt nur Motoren hinzu
                         ifPanzerung,            // F�gt nur Panzerungen hinzu
                         ifRWaffen,              // F�gt nur RaumschiffWaffen ohne Munition hinzu
                         ifGranate,              // F�gt nur Granaten hinzu
                         ifSchiff,               // F�gt Ausr�stung f�r Ein Raumschiff hinzu
                         ifSoldat);              // F�gt Ausr�stung f�r einen Soldaten hinzu

    { Eigenschaft f�r Methode Show der Klasse }
    { TDXQueryBox. Dient zur Definition eines }
    { Farbschemas                             }
    TColorTheme    =    (ctBlue,                 // Blaut�ne (Standard)
                         ctGreen);               // Gr�nt�ne (Im Soldatenformular)

    { Typ eines Forschungsprojektes }
    TProjektType   =    (ptWaffe,                // Waffe
                         ptMunition,             // Munition zu einer Waffe
                         ptRWaffe,               // Raumschiff-Waffe
                         ptEinrichtung,          // Basis-Einrichtung
                         ptPanzerung,            // Personenpanzerung
                         ptRaumSchiff,           // Raumschiff
                         ptRMunition,            // Munition zu einer Raumschiff-Waffe
                         ptGranate,              // Granate
                         ptOrganisation,         // Organisation
                         ptMotor,                // Raumschiffmotor
                         ptMine,                 // Tretmine
                         ptNone,                 // wird als �bergeordnetes Projekt benutzt (z.B. Technologie)
                         ptSensor,               // Sensor
                         ptShield,               // Raumschiffschild
                         ptExtension,            // Erweiterung f�r Raumschiffe
                         ptGuertel);             // G�rtel f�r Soldaten

    TGameDifficult =    (gdVeryEasy,             // sehr leicht
                         gdEasy,                 // leicht
                         gdNormal,               // Normal
                         gdHard,                 // Schwer
                         gdVeryHard);            // Sehr schwer

    TExtensionProp =    (epSensor, epShieldHitpoints, epShieldDefence, epShieldReloadTime);
    TExtensionPropSet = set of TExtensionProp;

    TProjektTypes  = set of TProjektType;

    TRaumschiffMission= (rmKauf,                 // Einkauf von Ausr�stungen
                         rmVerkauf,              // Verkaufen von Ausr�stungen
                         rmTransferAlphatron);   // Transfer von Alphatron zu einer anderen Basis

    TWaffenType    =    (wtProjektil,            // Projektilwaffe
                         wtRaketen,              // Raketenwaffe
                         wtLaser,                // Laserwaffe
                         wtChemic,               // Chemische Waffe
                         wtShortRange);          // Nahkampfwaffe

    TWaffenTypes = set of TWaffenType;

    { Typen f�r die Schussarten einer Waffe }
    TSchussType    =    (stNichtSchiessen,
                         stGezielt,              // Gezielter Schuss
                         stSpontan,              // Spontaner Schuss
                         stAuto,                 // Automatischer Schuss
                         stWerfen,               // Werfen ( f�r Granate )
                         stBenutzen,             // Benutzen ( f�r Sensoren und Minen)
                         stSchlag,               // Schlag f�r Nahkampfwaffen
                         stSchwingen,            // Schwingen f�r Nahkampfwaffen
                         stStossen);             // Stossen f�r Nahkampfwaffen


    TSchussTypes   =     set of TSchussType;

    { Typ f�r den TBitmapSpinButton der angibt }
    { welcher Pfeil hervorgehoben wird         }
    THighLightButton =  (hbNone,                 // Kein Pfeil wird hervorgehoben
                         hbLeft,                 // Der Linke Pfeil wird hervorgehoben
                         hbRight);               // Der Rechte Pfeil wird hervorgehoben

    TCorner          =  (cLeftTop,cLeftBottom,cRightTop,cRightBottom);
    TCorners         = set of TCorner;          
    TRoundCorners    =  (rcAll,
                         rcLeft,
                         rcTop,
                         rcRight,
                         rcBottom,
                         rcLeftTop,
                         rcLeftBottom,
                         rcRightTop,
                         rcRightBottom,
                         rcNone);

    { Typ zur Angabe des Hintergrundes f�r }
    { f�r eine Bitmap-Kompoennte           }
    TBitmapStyle   =    (bsBitmap,               // Hintergrundbild wird verwendet
                         bsRoundStyle);          // Hintergrund wird selbst mit Rundungen gezeichnet

    TFrameInfo     =    (ifCredits,
                         ifAlphatron,
                         ifLager,
                         ifWohn,
                         ifLabor,
                         ifWerk,
                         ifHangar);
                         
    TFrameInfos    = set of TFrameInfo;

    { Typ der Anzeige in TItemInfo }
    
    TInfoType      =    (itNone,                 // Zeigt keine Informationen an
                         itSoldat,               // Zeigt Informationen zu einem Soldaten
                         itLagerItem,            // Zeigt einen Ausr�stungsgegenstand
                         itEinrichtung,          // Zeigt Informationen zu einer Einrichtung
                         itOrganisation,         // Zeigt Informationen zur Organisation
                         itModel,                // Zeigt Informationen zu einem Raumschiffmodell
                         itRaumschiff,           // Zeigt Informationen zu einem Raumschiff
                         itUFOModel,             // Zeigt Informationen zu einem UFOModell an
                         itUFO,                  // Zeigt Informationen zu einem UFO an
                         itPunkte,               // Zeigt Informationen zu Punkten an
                         itFinanzen,             // Zeigt Informationen zu Finanzen an
                         itUpgrade,              // Zeigt Informationen zu Ausr�stungsupgrades
                         itAlien,                // Zeigt Informationen zu Aliens
                         itProduktion,           // Zeigt eine Produktion
                         itProjekt,              // Zeigt die Beschreibung zu einem Forschungsprojekt an
                         itBasis,                // Zeigt Informationen zu einer Basis an
                         itBasisStatus);         // Zeigt kompletten Informationen zu einer Basis an

    { Typen f�r TItemInfo zur Auswahl der }
    {  anzuzeigenden Eigenschaften        }
    TInfo          =    (inPreis,                // Zeigt den Kauf und Verkaufspreis an
                         inProduktion,           // Zeigt Informationen zur Produktion (AlphatronKosten und Stunden)
                         inAnzahl,               // Zeigt die Anzahl im Lager an
                         inLager,                // Zeigt den Lagerverbrauch an
                         inGewicht,              // Zeigt das Gewicht des Ausr�stungsgegenstandes an
                         inStrength,             // Zeigt die St�rke des Ausr�stungsgegenstandes an
                         inPanzerung,            // Zeigt die Panzerung an
                         inRInfos,               // Zeigt Informationen einer Raumschiff-Waffe
                         inMotor);               // Zeigt Informationen zu einem Motor

    TInfos         =     set of TInfo;

    { R�ckgabewert der Funktion IsWayFree von TIsoKDGame }
    TWayBlocked    =    (wbNone,                 // Weg zum Feld ist frei
                         wbDoor,                 // Weg wird durch eine T�r blockiert
                         wbTransparentWall,      // Weg wird durch eine durchschaubare Mauer blockiert
                         wbInvalidField,         // Weg wird durch ein nicht betretbares Feld blockiert
                         wbObject,               // Weg wird durch ein Objekt blockiert
                         wbWall,                 // Weg wird durch eine Mauer blockiert
                         wbSoldat,               // Weg wird durch einen Soldaten blockiert
                         wbFigure,               // Weg wird durch eine Figure blockiert
                         wbOutOfBounds);         // Zielpunkt liegt au�erhalb des Bereiches

    { Hier wird angegeben, welche Nachrichten }
    { lang angezeigt werden sollen             }
    TLongMessage     =  (lmSpend,                // Spenden
                         lmAngebote,             // Angebote
                         lmTrainEnd,             // Ausbildungsende
                         lmTrainCancel,          // Ausbildung abgebrochen
                         lmPension,              // R�cktritt von Personen
                         lmProjektEnd,           // Forschung beendet
                         lmProduktionEnd,        // Herstellung beendet
                         lmBauEnd,               // Basisbau beendet
                         lmOrganStat,            // �nderung des Organisationsstatus
                         lmSchiffEnd,            // Raumschiffbau beendet
                         lmUFOs,                 // Ereignisse mit UFOs
                         lmProdCancel,           // Herstellung abgebrochen
                         lmNachLaden,            // Ereignisse beim Nachladen von Raumschiffen
                         lmNewItems,             // Neue Ausr�stung verf�gbar
                         lmMissionMessage,       // Nachricht bei Missionen
                         lmTreibstoffMangel,     // R�ckkehr eines Raumschiffes bei Treibstoffmangel
                         lmFindPerson,           // Nachricht das ein Soldat/Techniker/Forscher auf dem Arbeitsmarkt entdeckt wurde
                         lmEinsaetze,            // Informationen �ber Eins�tze
                         lmAlphatronTransport,   // Transport von Alphatron abgeschlossen
                         lmRaumschiffe,          // Raumschiff wurde mit neuer Ausr�stung beladen
                         lmSoldierCured,         // Soldat ist vollst�ndig geheilt
                         lmBaseAttacked,         // Basis wird angegriffen
                         lmReserved);            // Reserviert f�r Systemnachrichten (nicht ver�nderbar)


    TMessages =          set of TLongMessage;

    TEinsatzMessage =    (emEnemyDie,
                          emFriendDie,
                          emReloadWeapon,
                          emNoMunition,
                          emUnitViewed,
                          emUnitUnderFire,
                          emUnitHeavyInjured);

    TEinsatzMessages =   set of TEinsatzMessage;

    { Kapital Buchungen }
    TKapital =          (kbSVK,                  // Soldaten Verkaufen
                         kbSK,                   // Soldaten Kaufen
                         kbSG,                   // Soldaten Gehalt
                         kbSEn,                  // Entsch�digung f�r Soldaten
                         kbFVK,                  // Forscher Verkaufen
                         kbFK,                   // Forscher Kaufen
                         kbFG,                   // Forscher Gehalt
                         kbFEn,                  // Entsch�digung f�r Wissenschaftler
                         kbHVK,                  // Techniker Verkaufen
                         kbHK,                   // Techniker Kaufen
                         kbHG,                   // Techniker Gehalt
                         kbHEn,                  // Entsch�digung f�r Techniker
                         kbLK,                   // Ausr�stungs Kauf
                         kbLVK,                  // Ausr�stungs Verkauf
                         kbEG,                   // Einsatzgewinn
                         kbFiU,                  // Finanzhilfe
                         kbTK,                   // Trainingskosten
                         kbSpK,                  // Spenden
                         kbSE,                   // Schadensersatz
                         kbTrK,                  // Treibstoff Kauf
                         kbBB,                   // Basisbau
                         kbRB,                   // Raumschiffkosten
                         kbRV,                   // Raumschiffverkauf
                         kbPG);                  // Patentgeb�hren

    { Punkte Buchungen }
    TPunkteBuch =       (pbForsch,               // Forschungspunkte
                         pbUFOweg,               // Punkte wenn ein UFO entkommen ist
                         pbUFOAbgeschossen,      // Punkte wenn ein UFO abgeschossen wurde
                         pbRaumschiffZerstoert,  // Punkte wenn ein Raumschiff zerst�rt wurde
                         pbUpgrade,              // Punkte wenn eine Ausr�stung aufgewertet wurde
                         pbSoldatGetoetet,       // Punkte f�r verlorene Soldaten
                         pbAlienGetoetet,        // Punkte f�r get�tete Aliens
                         pbEinsatzWin,           // Punkte f�r gewonnene Eins�tze
                         pbEinsatzLoose,         // Punkte f�r verlorene Eins�tze
                         pbItemFound,            // Punkte f�r erbeutete Ausr�stungen
                         pbItemLost,             // Punkte f�r verlorene Ausr�stungen
                         pbTrefferBilanz,        // Punkte f�r Treffsicherheit
                         pbInfiltration          // Punkte f�r nichts tuen
                         );

    THighLightKomponent = (hlkNone,
                           hlkMotor,
                           hlkWZelle,
                           hlkESlot,
                           hlkShield);

    TEreignissType      = (etTime,
                           etDestroy,
                           etReachGoal);

    TBasisEventType     = (betAdd,
                           betChange,
                           betDestroy);

    TFigureStatus       = (fsFriendly,
                           fsNeutral,
                           fsEnemy,
                           fsObject,
                           fsNone);

    TFigureAI           = (faiAgressiv,
                           faiNormal,
                           faiCarefully);

    TSoldatConfigSlot = (scsNone,
                         scsRucksack,
                         scsBasisItem,
                         scsSchiffItem,
                         scsBodenItem,
                         scsLinkeHand,
                         scsLinkeHandMunition,
                         scsRechteHand,
                         scsRechteHandMunition,
                         scsPanzerung,
                         scsGuertel,
                         scsMunition,
                         scsTrash);

    TSoldatConfigSlots = set of TSoldatConfigSlot;

    TDragActionError  = (daeNone,
                         daeLinksZweiHandwaffeGesetzt,
                         daeRechteZweiHandwaffeGesetzt,
                         daeLinkeHandGesetzt,
                         daeRechteHandGesetzt,
                         daeFalscheMunition,
                         daeZuschwer,
                         daeBasisZuwenigLagerRaum,
                         daeSchiffZuwenigLagerRaum,
                         daeRucksackKeinPlatz,
                         daeBenMunNichtInBasis,
                         daeBenMunNichtInSchiff,
                         daeAbstandnichtOK,
                         daeFromKeineZeiteinheiten,
                         daeZielKeineZeiteinheiten,
                         daeGuertelNichtFrei,
                         daeNichtBenutztbar,
                         daeInvalidItemInSlot,
                         daeGuertelUeberladen,
                         daeNichtgenugIntelligenz,
                         daeUnitNotOnMove);

    TZelleNachladenError = (zneOK,
                            zneNichtInstalliert,
                            zneNoMunition,
                            zneNotImLager);

    // Typ des Zieles eines UFOs/Raumschiffes
    TDestination      = (dNone,                   // Kein Ziel
                         dPoint,                  // Punkt auf der Weltkarte
                         dBase,                   // Basis
                         dTown,                   // Stadt
                         dSchiff);                // Schiff

    TWallType =         (wtTLeft,
                         wtTRight,
                         wtBLeft,
                         wtBRight);

    TFunctionType =     (ftCompare,
                         ftExchange);

    TForschType   =     (ftNone,
                         ftNormal,
                         ftUpgrade,
                         ftItemResearch,
                         ftAlienAutopsie);


    TRoomTyp           = (rtBaseRooms,           // Basiseinrichtung
                          rtDefense,             // Abwehrturm
                          rtSensor,              // Sensor
                          rtDefenseShield,       // Schutzschild
                          rtLivingRoom,          // Quartier
                          rtHangar,              // Hangar
                          rtLift,                // Aufzug
                          rtFloor,               // Verbindungstunnel
                          rtGearHead);           // F�rderturm in Alphatronbergwerken

    TForschTypes  = set of TForschType;

    // Typ der Erfahrungspunkte
    TExpType        = (etTreffer,                // Erfahrungspunkte durch Treffer einer Einheit
                       etEntdeckt,               // Erfahrungspunkte f�r gesichtete Gegner
                       etLaufen,                 // Erfahrungspunkte f�rs Laufen
                       etSchiessen,              // Erfahrungspunkte f�rs Schiessen
                       etWerfen);                // Erfahrungspunkte f�r Werfen

    TTileType       = (ttFloor,ttWall);

    { ISO Engine }
    PViewDirection    = ^TViewDirection;
    TViewDirection    = (vdLeft,
                         vdLeftTop,
                         vdTop,
                         vdRightTop,
                         vdRight,
                         vdRightBottom,
                         vdBottom,
                         vdLeftBottom,
                         vdAll);

    TLightDetail      = (ldEinfach,
                         ldDoppelt,
                         ldVierfach);

    TFightResult      = (frWin,
                         frLoose,
                         frCancel,
                         frStart,
                         frNone);

    TZugSeite         = (zsFriend,
                         zsNeutral,
                         zsEnemy);

    TUIInterface      = (uiSoldat,
                         uiSoldatList);

    TUnitState = (usReady,          usWantViewTo,    usDead,
                  usDeadAnimation,  usMoveUnit,      usNeedTU,
                  usWaitForOK);

    TBodyRegion = (brNone,brHead,brTorso,brFoot);

    TNeedTimeUnitResult = (nurOkay,nurNoTU,nurNotOnMove);

    // Trefferzone bei einer Einheit
    TTrefferZone      = (tzNone,              tzGlobal,
                         tzHeadLeft,          tzHeadRight,
                         tzHeadFront,         tzHeadBack,
                         tzTorsoLeft,         tzTorsoRight,
                         tzTorsoFront,        tzTorsoBack,
                         tzFootLeft,          tzFootRight,
                         tzFootFront,         tzFootBack);

    TSchadensTyp      = (stGlobal,  // Der Schaden eines ISOObjektes bezieht sich auf das ganze Feld (z.B. eine Explosion)
                         stLokal);  // Der Schaden begrenzt sich auf einen genauen Punkt (bei normalen Sch�ssen)

    { Definition eines Strings mit fester L�nge }
    { f�r property deklarationen                }
    NameString =         String[50];             // 50 Zeichen
    MapString =          String[30];             // 30 Zeichen

{****************************************}
{    Recordtypen f�r die Komponente      }
{             TKD4SaveGame               }
{****************************************}

  type

    { Kill and Destroy 4 Datumsstruktur zum }
    { speichern des aktuellen Datums        }
    TKD4Date = record
      Year       : SmallInt;                     // Jahr
      Month      : 1..12;                        // Monat
      Week       : 1..52;                        // Woche
      Day        : 1..32;                        // Tag
      Hour       : 0..23;                        // Stunde
      Minute     : 0..59;                        // Minute
    end;

    TKD4DateDiff = record
      Year       : SmallInt;                     // Jahr
      Month      : 0..11;                        // Monat
      Day        : 0..31;                        // Tag
    end;

    TSetIdentifier = record
      Name     : String;
      FileName : String;
      Autor    : String;
      Organ    : String;
      Desc     : String;
      ID       : Cardinal;
      WohnRaum : Integer;
      Language : String;
    end;

    PFloatPoint    = ^TFloatPoint;
    TFloatPoint    = record
      x            : double;
      y            : double;
    end;

    { Struktur die an die Funktion NewGame }
    { �bergeben wird und die Starteigen-   }
    { schaften �bergibt                    }
    TNewGameRec = record
      FileName       : String;                       // Dateiname des SaveGames
      Name           : String;                       // Name des Spieler
      Kapital        : Integer;                      // Startkapital
      StartAlphatron : Integer;
      Forsch         : Integer;                      // Forschungszeit
      Prod           : Integer;                      // Produktionszeit
      Alien          : Integer;                      // Alien-Angriffstag
      UFO            : Integer;                      // UFO-Angriffstag
      Soldat         : Integer;                      // Anzahl der Soldaten beim Start
      Forscher       : Integer;                      // Anzahl der Forscher beim Start
      Techniker      : Integer;                      // Anzahl der Techniker beim Start
      GameFile       : TSetIdentifier;
      Basis          : TFloatPoint;                  // Position der ersten Basis
      Language       : String;                       // Sprache f�r den Spielstand
      Difficult      : TGameDifficult;               // Schwierigkeitsgrad
    end;

    { Header f�r ein SaveGame mit den wichtigsten }
    { Eigenschaften wie Geld und Alphatron        }
    TSaveGameHeader = packed record
      PlayerName : String[20];                   // Name des Spielers
      Kapital    : Int64;                        // Aktuelles Kapital
      Alphatron  : Integer;                      // Aktuelles Alphatron
      Date       : TKD4Date;                     // Datum
      ForschTime : ShortInt;                     // Forschungszeit f�r ein Projekt in Prozent
      ProdTime   : ShortInt;                     // Produktionszeit f�r ein Projekt in Prozent
      Punkte     : Integer;                      // Punkte im Monat
      MaxMessage : Integer;                      // Anzahl der maximalen Nachrichten im Pager
      SaveDelete : boolean;                      // Sicherheitsabfrage beim L�schen von Nachrichten
      MonthBilanz: boolean;                      // Monatsbilanz am Monatsende
    end;

    { Speichert ein Forschungsprojekt das im Spiel }
    { erforscht werden kann                        }
    TForschungen = record
      ID           : Cardinal;                   // ID des erforschbaren Projekt
      Hour         : Single;                     // Die Stunden die es noch ben�tigt
      Gesamt       : Single;                     // Die Gesamtstunden f�r das Projekt zur Berechnung des Fortschrittes
      TypeId       : TProjektType;               // Projekttype
      ParentID     : Cardinal;                   // ID des Vorhergehenden Projektes
      NextID1      : Cardinal;                   // ID des n�chsten Projekt
      NextID2      : Cardinal;                   // ID des n�chsten Projekt
      NextID3      : Cardinal;                   // ID des n�chsten Projekt
      Update       : Boolean;                    // Gibt an, ob das Projekt ein Update einer Waffe ist
      BasisID      : Cardinal;                   // ID der Basis, in der das Projekt erforscht wird
      Info         : String;                     // Infotext
      ResearchItem : Boolean;                    // Gibt an, das das Projekt die Erforschung einer Alien-Technologie ist
      ResearchCount: Integer;                    // Ben�tigte Anzahl im Lager, um Forschung zu beginnen
      ResearchType : TForschType;                // Type der Forschung
      Started      : Boolean;                    // Anzeige im Labor?
    end;

    { Speichert einen Forscher im SaveGame }
    TForscher = packed record
      Name       : String[50];                   // Name des Forschers
      Strength   : Single;                       // Die F�higkeit des Forschers
      BitNr      : Byte;                         // Das Bild aus der BilderListe
      Days       : Integer;                      // Dienstage des Forschers
      ID         : Cardinal;                     // ID des zu erforschenden Projektes (0=keines)
      Train      : boolean;                      // In der Ausbildung (wenn ja ist ID=0)
      BasisID    : Cardinal;                     // ID der Basis in der der Forscher angestellt ist

      TrainTime  : Integer;                      // Anzahl der Minuten pro Tag in der Ausbildung
      ForschTime : Integer;                      // Anzahl der Minuten pro Tag in der Forschung
    end;

    { Speichert einen Soldaten im SaveGame }
    PSoldatInfo = ^TSoldatInfo;
    TSoldatInfo = packed record
      Name       : String[50];                   // Name des Soldaten
      Ges        : Single;                       // Aktuelle Gesundheit des Soldaten

      MaxGes     : Single;                       // Maximale Gesundheit des Soldaten
      PSIAn      : Single;                       // PSI-Angriff des Soldten
      PSIAb      : Single;                       // PSI-Abwehr des Soldaten
      Zeit       : Single;                       // Zeiteinheiten
      Treff      : Single;                       // Treffersicherheit
      Kraft      : Single;                       // Kraft die der Soldat hat!

      Image      : Byte;                         // Image des Soldaten
      Raum       : Integer;                      // Raumschiff-ID in der der Soldat sitzt
      Slot       : Byte;                         // Nummer des Slots im Raumschiff
      BasisID    : Cardinal;                     // ID der HeimatBasis
      LeftHand   : boolean;                      // Gibt an, ob der Soldat Links- oder Rechtsh�nder ist
      Filler     : Byte;                         // Nur ein Filler der Alten Struktur
      Train      : boolean;                      // Ausbildung
      Tag        : Word;                         // Anzahl der Tage im Dienst
      Einsatz    : Word;                         // Anzahl der Eins�tze
      Treffer    : Word;                         // Anzahl der Absch�sse
      TrainTime  : Integer;                      // Anzahl der Minuten pro Tag in der Ausbildung

      // Neue Attribute kommen nur hiernach
      Sicht      : Single;                       // Sichtweite des Soldaten
      IQ         : Single;                       // Intelligenz des Soldaten
      React      : Single;                       // Reaktion des Soldaten
      EP         : Integer;                      // gesammelte Erfahrungspunkte des Soldaten

      LastMaxGes : Single;                       // Wert, bei der letzten Anzeige in DXItemInfo
      LastPSIAn  : Single;                       // Wert, bei der letzten Anzeige in DXItemInfo
      LastPSIAb  : Single;                       // Wert, bei der letzten Anzeige in DXItemInfo
      LastZeit   : Single;                       // Wert, bei der letzten Anzeige in DXItemInfo
      LastTreff  : Single;                       // Wert, bei der letzten Anzeige in DXItemInfo
      LastKraft  : Single;                       // Wert, bei der letzten Anzeige in DXItemInfo
      LastSicht  : Single;
      LastIQ     : Single;
      LastReact  : Single;


      ClassName  : String;                       // Klassenname des Soldaten
      FaehigGes  : Integer;                      // Faehigkeitenwert f�r Gesundheit
      FaehigPSIAn: Integer;                      // Faehigkeitenwert f�r PSI-Angriff
      FaehigPSIAb: Integer;                      // Faehigkeitenwert f�r PSI-Abwehr
      FaehigZeit : Integer;                      // Faehigkeitenwert f�r Zeiteinheiten
      FaehigTreff: Integer;                      // Faehigkeitenwert f�r Treffsicherheit
      FaehigKraft: Integer;                      // Faehigkeitenwert f�r Kraft
      FaehigSicht: Integer;                      // Faehigkeitenwert f�r Entdeckung
      FaehigIQ   : Integer;
      FaehigReact: Integer;
    end;

    TSoldatKlasseProperty = record
      StartMin    : Byte;
      StartMax    : Byte;
      FaehigMin   : Byte;
      FaehigMax   : Byte;
    end;

    TSoldatKlasse = record
      Name        : String;
      Ges         : TSoldatKlasseProperty;
      Treff       : TSoldatKlasseProperty;
      Sicht       : TSoldatKlasseProperty;
      Kraft       : TSoldatKlasseProperty;
      Zeit        : TSoldatKlasseProperty;
      PSIAn       : TSoldatKlasseProperty;
      PSIAb       : TSoldatKlasseProperty;
      IQ          : TSoldatKlasseProperty;
      React       : TSoldatKlasseProperty;
    end;

    { Typ zur �bergabe einer Spende von der }
    { OrganisationsList zum SaveGame        }
    TSpende = record
      Gespendet     : boolean;                   // Wenn false wurde nichts gespendet und der Rest braucht nicht beachtet zu werden
      Money         : boolean;                   // Wenn true wird Kapital gespendet, sonst Alphatron
      Organisations : Integer;                   // Der Organisations Index
      Menge         : Integer;                   // gibt die H�he der Spende an
    end;

    { Speicherung der Monatsbilanz f�r ein }
    { SaveGame                             }
    TMonthStatus = Array[TKapital] of Integer;

    { Speicherung der Punktebilanz f�r einen Monat }
    TPunkteStatus = Array[TPunkteBuch] of Integer;

{***************************************}
{ Informationen f�r die Karten          }
{***************************************}

  type

    { Eintrag eines TileSets im TileSet Editor }
    TileSetEntry = record
      ID         : Integer;                      // ID des TileSets zur eindeutigen Identifizierung
      Group      : Integer;                      // Group wird f�r den Karten Editor zur Identifizierung der Gruppe benutzt
      Opaque     : boolean;                      // Gibt an, ob man durch dieses Feld sehen kann
      TimeUnits  : Integer;                      // Gibt die Anzahl der Zeiteinheiten beim betreten des Feldes an
    end;

    { Eintrag einer Gruppe (TileSet-Datei) im }
    { Karten-Editor }
    GroupEntry = record
      Name       : String[30];                   // Der Name der Gruppe bei der Anzeige im MapEditor
      FileName   : String[30];                   // DateiName des TileSets
    end;

    { Information f�r ein Feld auf einer Karte }
    TileInfo = packed record
      ArrayIndex: SmallInt;                      // Index des TileSets im Array der geladenen TileSets
      TeleportX: Byte;                           // Teleporter X-Position
      TeleportY: Byte;                           // Teleporter Y-Position
      TimeUnits: Byte;                           // Die Anzahl der TileSets die ben�tigt werden
    end;

  const
    ExtensionPropsCount = Ord(High(TExtensionProp));

{*******************************************}
{ Datei-K�pfe f�r verschiedene Dateien      }
{*******************************************}

  type

    { Record zum Speichern von Information }
    { mit der Anzahl der Eintr�ge          }
    TEntryHeader = record
      Version    : Integer;                      // Versionsnummer der Datei
      Entrys     : Integer;                      // Anzahl der Eintr�ge
    end;

    { Header f�r Tile-Sets }
    TTileSetHeader = record
      Version    : Integer;                      // Versionsnummer der Datei
      Name       : String[50];                   // Name des Tile-Sets
      Entrys     : Integer;                      // Anzahl der Tile-Sets ( wird nicht verwendet )
    end;


{****************************************}
{ Dateieintr�ge f�r verschiedene Dateien }
{****************************************}
  type

    { Eintrag in die Highscore Tabelle von }
    { Kill and Destroy 4                   }
    THighScoreEntry = packed record
      Spieler    : String[20];                   // Name des Spielers der Highscore
      Week       : Byte;                         // Woche der Highscore
      Jahr       : Word;                         // Jahr der Highscore
      Punkte     : Integer;                      // Die Punkte
    end;

    { Ein Alien in der Alien.dat, wird im }
    { Editor und im Wirtschaftsteil ver-  }
    { wendet                              }
    PAlien = ^TAlien;
    TAlien = packed record
      Name           : String;                   // Name des Alien's
      ID             : Cardinal;                 // Nummer zum Identifizieren
      Ges            : SmallInt;                 // Normale Gesundheit (30..150)
      Pan            : Byte;                     // Panzerung          (0..100)
      Waf            : Byte;                     // Angriffsst�rke     (10..100)
      PSAn           : Byte;                     // PSI-Angriff        (0..60)
      PSAb           : Byte;                     // PSI-Abwehr         (0..100)
      Zei            : Byte;                     // Zeiteinheiten      (40..100)
      Ver            : Word;                     // Tag ab den diese Art angreifen kann (0..360)
      Info           : String;                   // Beschreibung nach der Autopsierung
      ResearchInfo   : String;                   // Beschreibung vor der Autopsierung
      Image          : Byte;                     // ImageIndex f�r das Alien
      Power          : Byte;
      Treff          : Byte;                     // Treffsicherheit
      EP             : Integer;                  // Erfahrungspunkte pro Abschuss
      Sicht          : Integer;                  // Sichtweite eines Aliens
      Autopsie       : boolean;                  // Autopsie durchgef�hrt
      AutopTime      : Integer;                  // Stunden, um die Autospie durchzuf�hren
      IQ             : Integer;                  // Intelligenz des Aliens
      Reaktion       : Integer;                  // Reaktion des Aliens
    end;

    { Ein Model f�r ein UFO , wird im }
    { Editor und im Wirtschaftsteil   }
    { verwendet                       }

    TUFOShield = packed record
      Projektil  : Byte;                         // Schutz vor Projektilwaffen (0-100)
      Rakete     : Byte;                         // Schutz vor Raketenwaffen (0-100)
      Laser      : Byte;                         // Schutz vor Laserwaffen (0-100)
      Laden      : Word;                         // Aufladezeit f�r 1 Schildpunkt (50-2000)
    end;

    PUFOModel = ^TUFOModel;
    TUFOModel = packed record
      Name             : String;                 // Name des UFO's
      ID               : Cardinal;               // Nummer zum eindeutigen identifizieren
      HitPoints        : Word;                   // Hitpoints des UFOS (100-7500)
      Shield           : Word;                   // Schutzschildpunkte (0-1500)
      ShieldType       : TUFOShield;             // Schutzschild Spezifikationen
      MinBesatz        : Byte;                   // Anzahl der Aliens die Min. im UFO sind (1-30)
      ZusBesatz        : Byte;                   // Anzahl der Aliens die Zus�tz. im UFO sind (0-20)
      Angriff          : Word;                   // Angriffsst�rke (1-1500)
      Pps              : Byte;                   // Geschwindigkeit in Pixel/sec
      Ver              : Word;                   // Angriff nach ... Tagen (0-720)
      Image            : Integer;                // Bildnummer
      FirstMes         : boolean;
      Treffsicherheit  : Byte;                   // Treffsicherheit in Prozent
      Info             : String;                 // Infotext
      ResearchInfo     : String;                 // Beschreibung vor der Autopsierung
      Probability      : Integer;                // Wahrscheinlichkeit
      Aliens           : Array of record         // Aliens die im UFO sein k�nnen
                           AlienID : Cardinal;
                           Chance  : Integer;
                         end;
      Points           : Integer;                // Anzahl der Punkte bei Abschuss des UFOs
      BaseAttackStrength: Integer;               // Angriffsst�rke des UFOs f�r eine Basis
      BaseAttackRange  : Integer;                // Reichweite f�r einen Basisangriff eines UFOs
    end;

    { Eine Name in der Namen.dat, wird }
    { zum Erstellen von Soldaten und   }
    { Forschernamen verwendet          }
    TName = record
      Vor        : String[25];                   // Vorname
      Nach       : String[25];                   // Nachname
    end;

    { Wird in der Komponente TKD4Game verwendet }
    { um Information zu einem Feld zu speichern }
    GameInfo = record
      ArrayIndex: SmallInt;                      // Index des TileSets im Array der geladenen TileSets
      TeleportX: Byte;                           // Teleporter X-Position
      TeleportY: Byte;                           // Teleporter Y-Position
      TimeUnits: Byte;                           // Die Anzahl der TileSets die ben�tigt werden
      Opaque: boolean;                           // Gibt an, ob durch dieses Feld gesehen werden kann
      Seeing: Integer;                           // Gibt an, wieviele Einheiten das Feld sehen
    end;                                         // -1 = Komplett Schwarz
                                                 //  0 = Fog of War

    { Speichert Informationen zu einem Projekt }
    { in der Forsch.dat                        }
    TExtension     = packed record
      Prop         : TExtensionProp;
      Value        : Integer;
    end;

    PForschProject = ^TForschProject;
    TForschProject = packed record
      Name             : String[50];                 // Name des Forschungsprojektes
      ID               : Cardinal;                   // ID des Projektes, zur eindeutigen Identifzierung
      Stunden          : LongWord;                   // Stunden bei einer Forschungszeit von 100 %
      NextID1          : Cardinal;                   // ID des n�chsten Projektes
      NextID2          : Cardinal;                   // ID des n�chsten Projektes
      NextID3          : Cardinal;                   // ID des n�chsten Projektes
      LagerV           : Single;                     // Verbrauch im Lager f�r Ausr�stung
      Gewicht          : Single;                     // Gewicht f�r Ausr�stungen
      KaufPreis        : LongWord;                   // Einkaufspreis
      VerKaufPreis     : LongWord;                   // Verkaufspreis
      Munition         : LongWord;                   // Munition f�r die Waffe
      TypeID           : TProjektType;               // Typ des Projektes
      WaffType         : TWaffenType;                // Typ der Waffe
      Strength         : LongWord;                   // St�rke der Waffe oder Reichweite des Sensors
      Schuss           : TSchussTypes;               // Schussarten f�r die Waffe
      Munfor           : Cardinal;                   // Angabe der Waffe f�r welche Munition
      SmallSchiff      : Word;                       // Anzahl der kleinen Raumschiffe die verwaltet werden k�nnen
      LargeSchiff      : Word;                       // Anzahl der gro�en Raumschiffe die verwaltet werden k�nnen
      LagerRaum        : Word;                       // Der Lagerraum den die Einrichtung zur Verf�gung stellt
      Quartiere        : Word;                       // Den Wohnraum den die Einrichtung zur Verf�gung stellt
      WerkRaum         : Word;                       // Den Werkraum den die Einrichtung zur Verf�gung stellt
      Abwehr           : Word;                       // Die Abwehrst�rke der Einrichtung bei einem Angriff
      LaborRaum        : Word;                       // Den Laborraum  den die Einrichtung zur Verf�gung stellt
      BauZeit          : Word;                       // Die Bauzeit der Einrichtung in Tagen
      Panzerung        : Word;                       // Die Panzerst�rke der Personenpanzerung
      WaffenZellen     : Byte;                       // Anzahl der Waffenzellen f�r das gew�hlte Raumschiff
      HitPoints        : Word;                       // Anzahl der Hitpoints f�r das Raumschiff
      LagerPlatz       : Word;                       // Lagerraum im Raumschiff
      Soldaten         : Word;                       // Soldaten im Raumschiff
      PixPerSec        : Word;                       // Die Geschwindig der Raketen einer Raumschiffwaffe
      Laden            : Word;                       // Gibt die Nachladezeit der Waffe in ms an
      Verfolgung       : boolean;                    // Gibt an ob die Raketen das UFO verfolgt
      Start            : boolean;                    // Gibt an, ob die Ausr�stung am Start verf�gbar ist
      Anzahl           : Integer;                    // Gibt die Anzahl beim Start an
      Length           : Integer;                    // L�nge der Textinfo
      ImageIndex       : Integer;                    // Bildindex
      Land             : Shortint;                   // L�nderspezifische Waffe (0-25) normal (-1)
      Herstellbar      : boolean;                    // Ausr�stung herstellbar
      Ver              : Word;                       // Verf�gbar nach ... Tagen (0..720)
      Reichweite       : Integer;
      Einhand          : boolean;                    // Gibt an, ob die Waffe eine Einhand- oder Zweihandwaffe ist
      ExtendSlots      : Byte;                       // Anzahl der Erweiterungsslots
      ExtCount         : Byte;                       // Anzahl der Eintr�ge in Extensions
      Extensions       : Array[0..ExtensionPropsCount] of TExtension;  // Erweiterungen
      SensorWeite      : Word;                       // Sensorreichweite in km
      Power            : Byte;                       // Durchschlagskraft bei Waffen
      TimeUnits        : Byte;                       // Zeiteinheiten f�r Autoschuss
      Info             : String;                     // Textinfo in der UFOP�die
      ResearchInfo     : String;                     // Textinfo beim Forschen
      Parents          : Array of Cardinal;          // Projekte die erforscht werden m�ssen
      UFOPaedieImage   : String;
      Explosion        : Integer;                    // Explosionsreichweite
      AlienItem        : Boolean;                    // Ausr�stung vom Alien?
      ResearchCount    : Integer;                    // Ben�tigte Anzahl zur Forschung
      Genauigkeit      : Integer;                    // Treffgenauigkeit f�r Waffen
      Patentgebuehr    : Integer;                    // Patentgeb�hr pro Woche
      Patentzeit       : Integer;                    // Anzahl der Wochen, die das Patent l�uft
      Patentanstieg    : Integer;                    // Anstieg der Patentgeb�hren pro Woche
      ManuAlphatron    : Integer;                    // Alphatronverbrauch f�r die Herstellung
      ProdTime         : Integer;                    // Herstellungszeit in Stunden
      PatentSatt       : Integer;                    // S�ttigung nach dem die Patengeb�hren nicht mehr steigen
      AlienChance      : Integer;                    // Chance, dass ein Alien diese Ausr�stung fallen l�sst
      NeedIQ           : Integer;                    // Intelligenz, die ben�tigt wird, um die Ausr�stung zu benutzen

      ShootSound       : String;                     // Sound der beim Abschuss der Waffe abgespielt wird

      // BasisBau N    eu
      BuildImageBase   : String;                     // Bild, dass f�r das Zeichnen beim Basisbau verwendet werden soll (Obergeschoss)
      BuildImageUnder  : String;                     // Bild, dass f�r das Zeichnen beim Basisbau verwendet werden soll (Untergeschoss)
      UnitWidth        : Integer;                    // Belegte Baufelder (Weite)
      UnitHeight       : Integer;                    // Belegte Baufelder (H�he)

      SensorWidth      : Integer;                    // Sensorreichweite
      ShieldStrength   : Integer;                    // Schildst�rke
      Treffsicherheit  : Integer;                    // Treffsicherheit Abwehreinrichtung
      maxReichweite    : Integer;                    // maxReichweite Abwehreinrichtung

      RoomTyp          : TRoomTyp;
      AlphatronStorage : Integer;                    // Alphatronlager
      RoomHitPoints    : Integer;                    // Hitpoints f�r eine Einrichtung
    end;

    TTileHeader = record
      Version : Integer;
      Title   : String[50];
      Entrys  : Integer;
    end;

    TBasisLager = packed record
      BasisID         : Cardinal;                   // ID der Basis zu den die Eintr�ge geh�hren
      ItemCount       : Array of record
        Count         : Integer;                    // Anzahl der Einzelnen Items
        ShootSurPlus  : Integer;                    // �berschuss bei Munition
      end;
    end;

    PLagerItem = ^TLagerItem;
    TLagerItem = packed record
      Name           : String;
      Anzahl         : Word;
      Level          : Integer;                    // Level der Waffe
      ID             : Cardinal;
      LagerV         : Single;
      WaffType       : TWaffenType;
      Gewicht        : Single;
      KaufPreis      : LongWord;
      VerKaufPreis   : LongWord;
      Munition       : Integer;                    // Anzahl der Sch�sse in einem Magazin
      TypeID         : TProjektType;
      Strength       : LongWord;
      Schuss         : TSchussTypes;
      Munfor         : Cardinal;
      Panzerung      : Word;
      PixPerSec      : Word;
      Laden          : Word;                       // Nachladezeit f�r Waffen
      Verfolgung     : boolean;
      ImageIndex     : Integer;                    // Index des Bildes
      Land           : Shortint;
      HerstellBar    : boolean;                    // Im Besitz der Pl�ne
      Reichweite     : Integer;
      Einhand        : boolean;                    // Gibt an, ob die Waffe eine Einhand- oder Zweihandwaffe ist
      ExtCount       : Byte;                       // Anzahl der Eintr�ge in Extensions
      Extensions     : Array[0..ExtensionPropsCount] of TExtension;  // Erweiterungen
      Power          : Byte;                       // Durchschlagskraft
      TimeUnits      : Byte;                       // Zeiteinheiten beim automatischen Schuss
      Info           : String;                     // Infotext
      UFOPaedieImage : String;
      Explosion      : Integer;                    // Explosionsreichweite
      Useable        : Boolean;                    // Benutzbar
      PublicPlan     : Boolean;                    // �ffentlicher Bauplan
      Genauigkeit    : Integer;                    // Treffgenauigkeit von Waffen
      ManuAlphatron  : Integer;                    // Alphatron
      ProdTime       : Integer;                    // Herstellungszeit in Stunden
      AlienChance    : Integer;                    // Chance, dass diese Ausr�stung vom Alien genutzt wird
      NeedIQ         : Integer;                    // Ben�tigte Intelligenz um Ausr�stung zu benutzen

      AlienItem      : Boolean;                    // Alienausr�stung                     
      Visible        : Boolean;                    // Ausr�stung in Lager/Werkstatt ... sichtbar
      ShootSurPlus   : Integer;                    // Anzahl der Restsch�sse
      ResearchedDate : TDateTime;                  // Wann es erfunden w�rde

      SoundShoot     : String;                     // Abschusssoun f�r die Waffe
    end;

    TItemUpgrade = record
      Old    : TLagerItem;
      New    : TLagerItem;
      Time   : Integer;
    end;

    TEndedForschProjekt = record
      ID       : Cardinal;
      ParentID : Cardinal;
      TypeID   : TProjektType;
      Name     : String[50];
    end;

    TPatent = record
      Gebuehr  : Integer;
      Laufzeit : Integer;
      Woche    : Integer;
      Zeit     : Integer;
      Anstieg  : double;
      Satt     : Integer;
    end;

    TWatchType = (wtNone,wtMessage,wtHire,wtAccept,wtExchange);

    TWatchMarktSettings = record
      BasisID           : Cardinal;
      Active            : Boolean;
      minFaehigkeiten   : Integer;
      WatchType         : TWatchType;
    end;

    TWatchMarktArray   = Array of TWatchMarktSettings;

    TWatchAuftragSettings = record
      KaufPreis         : Integer;
      KaufType          : TWatchType;
      VerkaufPreis      : Integer;
      VerkaufType       : TWatchType;
    end;

{ Eventdeklarationen }
  type
    { Gibt einen Event von einem TileSet an }
    TTileEvent       = procedure(Sender: TObject;X,Y: Integer; Button: TMouseButton) of object;

    { Tritt auf, wenn der Modify Status der TKD4Map ge�ndert wurde }
    TModifyEvent     = procedure(Sender: TObject;Modify: boolean) of object;

    { Tritt bei der beendigung eines Projektes auf }
    TProjektEndEvent = procedure(Sender: TObject;Projekt: TForschProject) of object;

    { Tritt bei der beendigung einer Autopsie auf }
    TAutopsieEndEvent= procedure(Sender: TObject;ID: Cardinal) of object;

    TResearchItem    = procedure(Sender: TObject;const Item: TLagerItem) of object;

    { Tritt auf, wenn ein Monat zu Ende ist und die HighScore �bertragen werden mu� }
    THighScoreEvent  = procedure(Sender: TObject;Month: Byte;Year: Word;Points: Integer) of object;
    { Tritt auf, wenn eine Komponente einen Raum (z.B. Wohn- oder }
    { Laborraum ben�tigt. Free auf false, falls nicht genug Raum  }
    { zur Verf�gung steht                                         }
    TNeedRoomEvent   = procedure(BasisID: Cardinal;var Free: boolean) of object;

    TFreeRoomEvent   = procedure(BasisID: Cardinal) of object;
    { Tritt auf, wenn eine Komponente Geld, um zum Beispiel einen   }
    { Forscher zu kaufen, ben�tigt. Need ist die Anzahl des Geldes. }
    { Enough muss von der Funktion auf false gesetzt werden falls   }
    { nicht genug Geld vorhanden ist                                }
    TNeedMoneyEvent  = procedure(Sender: TObject;Need: Integer;var Enough: boolean;BuchType: TKapital;CanCredit: boolean=false) of object;

    { Tritt auf, wenn eine Komponente zum Beispiel beim Verkaufen   }
    { Geld bekommt, Need ist die Anzahl des Geldes                  }
    TFreeMoneyEvent  = procedure(Sender: TObject;Need: Integer;BuchType: TKapital) of object;

    { Tritt auf, wenn die Ausbildung einer Einheit beendet ist }
    { Index ist der Index der Einheit                          }
    TTrainEndEvent   = procedure(Sender: TObject;Index: Integer) of object;

    { Tritt auf wenn TTilegroup einen Dateinamen zum speichern ben�tigt }
    { Setzen sie Save auf false um das Speichern abzubrechen            }
    TNeedFileName    = procedure(Sender: TObject;var FileName: String;var Save: boolean) of object;

    { Tritt auf wenn TItemInfo den Waffennamen einer Munition ben�tigt }
    TNeedName             = procedure(Sender: TObject;ID: Cardinal;var Name: String) of object;
    TMonthEnd             = procedure(Sender: TObject;Status: TMonthStatus) of object;
    TSpendEvent           = procedure(Sender: TObject;Spende: TSpende) of Object;
    TGetMessage           = procedure(Sender: TObject;Message: String;Short: boolean) of object;
    TSendMessage          = procedure(Sender: TObject;Message: String; MType: TLongMessage;Objekt: TObject = nil) of object;
    TQuestion             = procedure(Sender: TObject;Message: String; var Result: Boolean) of object;
    TNeedLagerRoom        = procedure(var Enough: boolean;BasisID: Cardinal;Room: Double) of object;
    TFreeLagerRoom        = procedure(BasisID: Cardinal;Room: Double) of object;
    TNeedAlphatronEvent   = procedure(Sender: TObject;Need: Integer;var Become: Integer) of object;
    TFreeAlphatronEvent   = procedure(Sender: TObject;Free: Integer) of object;
    TItemReady            = procedure(Sender: TObject;Item: Cardinal;BasisID: Cardinal) of object;
    TBuchPunkte           = procedure(Punkte: Integer;Art: TPunkteBuch) of object;
    TCustomSaveGameEvent  = procedure(Stream: TStream) of object;

{ Exceptions }

type
  ENotEnoughRoom            = class(Exception);
  ENotEnoughMoney           = class(Exception);
  ENotEnoughAlphatron       = class(Exception);
  ENotEnoughItems           = class(Exception);
  ECanNotDraw               = class(Exception);
  ETileSetNotFoundException = class(Exception);
  EInvalidVersion           = class(Exception);
  ENoPathFound              = class(Exception);
  EProjektExists            = class(Exception);
  ENotEnoughTimeUnits       = class(Exception);
  ELooseGame                = class(Exception);
  ECanNotBuy                = class(Exception);

  EXForceGUIException       = class(Exception);
  EErrorGUIException        = class(EXForceGUIException);
  EHintGUIException         = class(EXForceGUIException);

type
  PSmallIntArray            = ^TSmallIntArray;
  TSmallIntArray            = Array of SmallInt;

{ TileGroup types }

{ Bisher undokumentierte Strukturen }
type
  PBlockType = ^TBlockType;
  TBlockType = packed record
    Block     : TWayBlocked;
    Doors     : Array of PSmallInt;
  end;

  PMapTileEntry = ^TMapTileEntry;
  TMapTileEntry = record
    Floor            : Smallint;
    BLeft            : Smallint;
    Bright           : Smallint;
    TLeft            : Smallint;
    TRight           : Smallint;
    Obj              : Smallint;
    HotSpot          : Byte;           // 0 = kein Startpunkt / 1 = Startpunkt Friendly / 2 = Startpunkt Enemy

    // Einstellungen f�r das Tile
    Init             : Boolean;
    Begehbar         : Boolean;

    // Verkn�pfung zu angrenzenden Tiles
    PTopLeft         : PMapTileEntry;  // X - 1
    PTopRight        : PMapTileEntry;  // Y - 1
    PBottomLeft      : PMapTileEntry;  // Y + 1
    PBottomRight     : PMapTileEntry;  // X + 1

    PTop             : PMapTileEntry;  // X - 1 & Y - 1
    PRight           : PMapTileEntry;  // X + 1 & Y - 1
    PBottom          : PMapTileEntry;  // X + 1 & Y + 1
    PLeft            : PMapTileEntry;  // X - 1 & Y + 1

    // Hitpoints der Mauern
    HTTLeft          : Integer;
    HTTRight         : Integer;
    HTBLeft          : Integer;
    HTBRight         : Integer;

    // Helligkeitswert
    AlphaV : packed record
      All       : Integer;
      Objekt    : Integer;
      Discover  : boolean;
      NAll      : Integer;
      Lightning : boolean;
      ATLeft    : SmallInt;
      ATRight   : SmallInt;
      ABLeft    : SmallInt;
      ABright   : SmallInt;
      // Anzahl der Einheiten die hinter der Mauer stehen (f�r die Transparenz)
      UTTLeft   : Integer;
      UTTRight  : Integer;
      UTBLeft   : Integer;
      UTBRight  : Integer;
    end;

    // Informationen zu T�ren
    DoorInfo : packed record
      HasDoors  : Boolean;
      BLeft     : SmallInt;
      BLeftW    : TWayBlocked;
      BRight    : SmallInt;
      BRightW   : TWayBlocked;
      TLeft     : SmallInt;
      TLeftW    : TWayBlocked;
      TRight    : SmallInt;
      TRightW   : TWayBlocked;
    end;

    // Informationen zu �berg�nge auf andere Tiles
    BlockInfo : packed record
      Discover  : Boolean;
      BlockType : Array[0..7] of TBlockType;
    end;
  end;

  TTileArray = Array of Array of TMapTileEntry;

  TSelectTile = packed record
    Select : boolean;
    Temp   : boolean;
  end;

  TUsedTileInfo = record
    ID            : Cardinal;
    TileSetIndex  : Integer;
    Index         : Integer;     // Index im Ausgangstileset
    Typ           : TTileType;
    OwnIndex      : Integer;     // Index im Bodeneinsatztileset
    Imported      : Boolean;     // Importiert
  end;

  TMapHeader = packed record
    Version: Integer;
    Width  : byte;
    height : byte;
    MapName: NameString;
  end;

  type TSetTileStruct = record
    Index   : Smallint;
    SetTile : boolean;
  end;

  type TSetTile = record
    Floor   : TSetTileStruct;
    BLeft   : TSetTileStruct;
    BRight  : TSetTileStruct;
    TLeft   : TSetTileStruct;
    TRight  : TSetTileStruct;
    Obj     : TSetTileStruct;
    HotSpot : TSetTileStruct;
  end;

  type TShowTile = record
    All : Integer;
    Objekt: Integer;
  end;

  type TFriendlyWert = 0..100;
  PLand = ^TLand;
  TLand = packed record
    Name          : String;
    ID            : Cardinal;
    Image         : Byte;
    InfoIndex     : Shortint;
    Friendly      : TFriendlyWert;
    UFOHours      : Integer;
    Einwohner     : Integer;
    Budget        : Integer;
    Zufriedenheit : Integer;
    SchErsatz     : Integer;
    Towns         : Integer;
  end;

  TLandInfo = record
    ID            : Cardinal;
    Image         : Byte;
    Name          : String;
    Oberhaupt     : String;
    StartFriendly : TFriendlyWert;
    Dichte        : Integer;
    Felder        : Integer;
    NearestSquare : TPoint;
  end;


  type TOrganisationStatus = (osAllianz,osFriendly,osNeutral,osUnFriendly,osEnemy);


  PAuftrag = ^TAuftrag;
  TAuftrag = packed record
    ID      : LongWord;
    Kauf    : boolean;
    Item    : LongWord;
    Preis   : Integer;
    Anzahl  : Word;
    BasisID : Cardinal;
  end;

  PAngebot = ^TAngebot;
  TAngebot = packed record
    ID           : LongWord;
    Organisation : Word;
    Anzahl       : Word;
    Preis        : Integer;
    Rounds       : ShortInt;
  end;

  TTwoInteger = record
    ID           : Integer;
    Index        : Integer;
  end;

  TUFOPadieEntry = (upeLagerItem,
                    upeEinrichtung,
                    upeRaumschiff,
                    upeUFO,
                    upeAlien,
                    upeTechnologie);

  PPadieEntry = ^TPadieEntry;
  TPadieEntry = record
    Name       : String;
    Image      : Integer;
    PaedieImage: String;
    Info       : String;
    TypeID     : TProjektType;
    Group      : TUFOPadieEntry;
    Index      : Integer;
  end;

  TTechniker = packed record
    TechnikerID      : Cardinal;
    Name             : String[50];
    Strength         : Single;
    BitNr            : Byte;
    Days             : Integer;               // Dienstage des Technikers
    Ausbil           : boolean;
    ProjektID        : LongWord;
    BasisID          : Cardinal;
    AlphatronMining  : Boolean;               // Techniker ist f�r die F�rderung von Alphatron eingeteilt
    Repair           : Boolean;               // Techniker ist mit der Reparatur einer Einrichtung besch�ftigt
    ProdTime         : Integer;
    TrainTime        : Integer;
  end;

  PTileInformation = ^TTileInformation;
  TTileInformation = record
    LeftRotate   : Integer;
    HMirror      : Integer;
    VMirror      : Integer;
    RightRotate  : Integer;
    Begehbar     : Boolean;
  end;

  TDoorType = (dtNoDoor,dtOpenLeft,dtOpenRight);

  PWallInformation = ^TWallInformation;
  TWallInformation = record
    DoorType       : TDoorType;
    Mirror         : Integer;
    BackSide       : Integer;
    Border         : Integer;
    Destroyed      : Integer;  // Mauerindex wenn Mauer zerst�rt
    DrawWindow     : Boolean;
    ShowThrough    : Boolean;
    WalkThrough    : Boolean;
    WindowColor    : TColor;
    Barness        : Integer;  // Zerst�rbarkeitsstufe => Konkrete Angaben innerhalb der xforce.ini
    HitPoints      : Integer;
    AbsWallHeight  : Integer;  // Tats�chliche H�he der Mauer
    IsEcke         : Boolean;  // Mauer ist eine Ecke
  end;

  TProduktion = packed record
    ID       : LongWord;
    ItemID   : Cardinal;
    Kost     : Integer;
    Reserved : Integer;
    LagerV   : double;
    Anzahl   : Byte;
    Gesamt   : Integer;
    Hour     : Single;
    Product  : boolean;
    BasisID  : Cardinal;
  end;

  PGameStatus = ^TGameStatus;
  TGameStatus = record
    Name     : String[20];
    Date     : TKD4Date;
    Alphatron: Integer;
    Kapital  : Int64;
    Version  : Integer;
  end;

  PMissionRaumschiff = ^TMissionRaumschiff;
  TMissionRaumschiff = record
    SchiffID       : Integer;
    OnDestroy      : String;
    OnReached      : String;
    EventOnReached : TEventHandle;
    EventOnDestroy : TEventHandle;
  end;

  PMissionUFO = ^TMissionUFO;
  TMissionUFO = record
    UFOID              : Cardinal;
    Delete             : Boolean;
    OnShootDown        : String;
    OnEscape           : String;
    OnDiscovered       : String;
    EventOnShootDown   : TEventHandle;
    EventOnEscape      : TEventHandle;
    EventOnDiscovered  : TEventHandle;
  end;

  PEreigniss = ^TEreigniss;
  TEreigniss = packed record
    ID       : Cardinal;
    Aktiv    : boolean;
    Name     : String;
    Minuten  : Integer;
    Repeated : Boolean;
    MinutGes : Integer;
    Next     : PEreigniss;
  end;

  PMissionEinsatz    = ^TMissionEinsatz;
  TMissionEinsatz    = record
    ID                   : Cardinal;
    OnWin                : String;
    OnTimeUp             : String;
    NotifyHandleWin      : TEventHandle;
    NotifyHandleTimeUp   : TEventHandle;
  end;

  TRoomPosition      = record
    BuildFloor       : Integer;                  // Bauebene
    X                : Integer;
    Y                : Integer;
  end;

  PEinrichtung = ^TEinrichtung;
  TEinrichtung = packed record
      Name         : String;                     // Name des Forschungsprojektes
      ID           : Cardinal;
      KaufPreis    : LongWord;                   // Einkaufspreis
      SmallSchiff  : Word;                       // Anzahl der kleinen Raumschiffe die verwaltet werden k�nnen
      SmallBelegt  : Word;                       // Belegte Hangers
      LargeSchiff  : Word;                       // Anzahl der gro�en Raumschiffe die verwaltet werden k�nnen
      LagerRaum    : double;                     // Der Lagerraum den die Einrichtung zur Verf�gung stellt
      LagerBelegt  : double;                     // Gibt den Belegten Lagerraum an
      Quartiere    : Word;                       // Den Wohnraum den die Einrichtung zur Verf�gung stellt
      WohnBelegt   : Word;                       // Gibt den Belegten Wohnraum an
      WerkRaum     : Word;                       // Den Werkraum den die Einrichtung zur Verf�gung stellt
      WerkBelegt   : Word;                       // Gibt den Belegten Werkraum an
      Abwehr       : Word;                       // Die Abwehrst�rke der Einrichtung bei einem Angriff
      LaborRaum    : Word;                       // Den Laborraum  den die Einrichtung zur Verf�gung stellt
      LaborBelegt  : Word;                       // Gibt den Belegten Laborraum an
      days         : word;                       // Die Anzahl der Tage die die EInrichtung noch zum Bau ben�tigt
      BauZeit      : Word;                       // Die Bauzeit der Einrichtung in Tagen
      Reichweite   : Integer;                    // Reichweite in km der Abwehreinrichtung
      Frequenz     : Integer;                    // Schussfrequenz in Sch�sse pro Minute
      UFOID        : Cardinal;                   // ID des UFOs das derzeit Angegriffen wird
      Info         : String;                     // Infotext
      ImageBase    : String;                     // Bild f�r den Basisbau und die UFOP�die (Obergeschoss)
      ImageUnder   : String;                     // Bild f�r den Basisbau und die UFOP�die (Untergeschoss)

      Width        : Integer;                    // Belegte Baufelder (Weite)
      Height       : Integer;                    // Belegte Baufelder (H�he)

      SensorWidth      : Integer;                // Sensorreichweite
      ShieldStrength   : Integer;                // Schildst�rke
      AktShieldPoints  : Integer;                // Aktuelle Schildpunkte
      Treffsicherheit  : Integer;                // Treffsicherheit Abwehreinrichtung
      maxReichweite    : Integer;                // maxReichweite Abwehreinrichtung
      WaffenArt        : TWaffenType;            // Waffenart der Abwehreinrichtung

      RoomTyp          : TRoomTyp;               // Typ der Einrichtung

      Position         : TRoomPosition;          // Position der Einrichtung
      AlphatronStorage : Integer;                // Lagerung f�r Alphatron
      Hitpoints        : Integer;                // Maximale Hitpoints der Einrichtung
      AktHitpoints     : Integer;                // Aktuelle Hitpoints

      RoomID           : Cardinal;               // Eindeutige ID zur Identifizierung in einer Basis
      BaseID           : Cardinal;               // Verweis auf die Basis
      ActualValue      : Integer;                // Aktueller Wert der Einrichtung
      DaysToDecValue   : Integer;                // Anzahl der Tage bis zum Wertverlust
    end;

    TRaumschiffModel = packed record
      Name         : String[50];                 // Name des Raumschiffmodells
      ID           : Cardinal;
      KaufPreis    : LongWord;                   // Einkaufspreis
      VerKaufPreis : LongWord;                   // Verkaufspreis
      WaffenZellen : Byte;                       // Anzahl der Waffenzellen f�r das gew�hlte Raumschiff
      HitPoints    : Word;                       // Anzahl der Hitpoints f�r das Raumschiff
      LagerPlatz   : Word;                       // Lagerraum im Raumschiff
      Soldaten     : Word;                       // Soldaten im Raumschiff
      BauZeit      : Word;                       // Bauzeit in Tagen f�r das Raumschiff
      ExtendsSlots : Byte;                       // Erweiterungsslots f�r das Raumschiff
      SensorWeite  : Word;                       // Sensorreichweite in km
      Info         : String;                     // InfoText
    end;

    TMotor         = packed record
      Installiert  : boolean;                    // Gibt an, ob ein Motor Installiert ist
      Name         : String[50];                 // Name des Motors
      ID           : Cardinal;                   // ID des Motors zum eindeutigen Identifizieren
      MaxLiter     : Integer;                    // Maximale Kapazit�t f�r den Treibstoff
      Liter        : double;
      Verbrauch    : Integer;                    // Den Verbrauch auf 1000 km in Liter
      Duesen       : boolean;                    // Gibt an, ob Man�vrierd�sen an Bord sind
      PpS          : Integer;                    // Geschwindigkeit in Pixel pro Sekunde
    end;

    TShield        = packed record
      Installiert  : boolean;
      Name         : String[50];
      ID           : Cardinal;
      Points       : Integer;
      Abwehr       : Integer;
      Laden        : Integer;
    end;

    TWaffenZelle   = packed record
      Installiert      : boolean;                // Gibt an, ob die Waffenzelle Installiert ist
      Name             : String[50];             // Name der Waffen
      ID               : Cardinal;               // ID des Motors zum eindeutigen Identifizieren
      MunIndex         : Integer;                // Index der Munition
      Munition         : LongWord;               // Munition f�r die Waffe
      WaffType         : TWaffenType;            // Typ der Waffe
      Strength         : LongWord;               // St�rke der Waffe
      PixPerSec        : Word;                   // Die Geschwindigkeit der Raketen einer Raumschiffwaffe
      Laden            : Word;                   // Gibt die Nachladezeit der R-Waffe in ms an
      SendMessage      : boolean;                // Gibt an, ob schon der fehlschlag beim Nachladen gemeldet wurde
      Verfolgung       : boolean;                // Gibt an ob die Raketen das UFO verfolgt
      Reichweite       : Integer;                // Gibt die Reichweite der Waffe an
      ReserveMunition  : Integer;                // Reserve Munition die an Bord ist (noch nicht verbrauchte aus einem Munipack)
    end;

    TExtensionZelle= packed record
      Installiert  : boolean;
      ID           : Cardinal;
      ExtCount     : Byte;
      Extensions   : Array[0..ExtensionPropsCount] of TExtension;
    end;

    TShieldDefault = record
      Name         : String[20];
      Projektil    : Byte;
      Rakete       : Byte;
      Laser        : Byte;
    end;

    THelpEntry     = record
      Text         : String[255];
      Level        : Integer;
      Ressource    : String[30];
      ID           : Cardinal;
    end;

    TDXMouseCursor = record
      BeginFrame   : Integer;
      EndFrame     : Integer;
      AnimTime     : Integer;
      HotSpot      : TPoint;
    end;

    TFrameProcRecord = record
      Func         : TFrameFunction;
      Parameter    : TObject;
      Interval     : Integer;
      MSecs        : Integer;
      Delete       : boolean;
      Aktiv        : boolean;
    end;

    TUFOKeys       = packed record               // Struktur zum Speichern der Tasten im UFO-Kampf
      RLeft        : Char;                       // Rotate Left
      RRight       : Char;                       // Rotate Right
      GibSchub     : Char;                       // Schub geben
      MLeft        : Char;                       // Move Left
      MRight       : Char;                       // Move Right
      WZelle1      : Char;                       // Waffen Zelle 1 Feuer
      WZelle2      : Char;                       // Waffen Zelle 2 Feuer
      WZelle3      : Char;                       // Waffen Zelle 3 Feuer
    end;

    TBodenKeys     = packed record
      Pause        : Char;
      RundenEnde   : Char;
      Karte        : Char;
      ObjektNamen  : Char;
      NextSoldat   : Char;
      PrevSoldat   : Char;
      ScrollLeft   : Char;
      ScrollRight  : Char;
      ScrollUp     : Char;
      ScrollDown   : Char;
      Camera       : Char;
      Ausruesten   : Char;
      TakeAll      : Char;
      BeamUp       : Char;
      ShowGrid     : Char;
      UseGrenade   : Char;
      UseSensor    : Char;
      UseMine      : Char;
      WallsTrans   : Char;
    end;

    TAllHotKeys    = packed record
      UFOKeys      : TUFOKeys;
      BodenKeys    : TBodenKeys;
    end;

    { Satz zum Speichern eines UFOs }
    // Record veraltet. Es wird jetzt zum Laden/Speichern von UFOS ExtRecords verwendet
    TUFOName = String[40];
    TUFOSaveRec = packed record
      Name       : TUFOName;            // 41 Bytes
      Date       : TKD4Date;            // 7 Bytes
      ID         : Cardinal;
      Rounds     : Integer;
      BaseAttack : boolean;
      Size       : Integer;
      HitPoints  : Integer;
      Point      : TFloatPoint;
      ShiPoints  : Integer;
      Entfernung : Integer;
      Model      : TUFOModel;
      // Ende Fester Teil

      Visible    : boolean;
      Minutes    : Integer;
      LandMinuten: Integer;

      // Zielbestimmungen
      Destination: TDestination;
      DestPoint  : TFloatPoint;
      DestID     : Cardinal;
      DestMessage: Boolean;
    end;

    TMapSettings = record
      DefZu  : boolean;
    end;

    TRoomInfo = record
      Name      : String;
      Width     : Integer;
      Height    : Integer;
      CanRotate : boolean;
      Tiles     : TTileArray;
    end;

    TTransportItem  = record
      ID            : Cardinal;
      Anzahl        : Integer;
      Preis         : Integer;
    end;

    PItemsInSchiff  = ^TItemsInSchiff;
    TItemsInSchiff  = record
      ID            : Cardinal;
      Anzahl        : Integer;
      Wanted        : Integer;                   // Anzahl die nach einem Einsatz wieder im Raumschiff sein soll
      Verbrauch     : Integer;                   // Der Platzverbrauch mal 10
    end;

    TWayFreeResult  = record
      WayBlocked    : TWayBlocked;
      BlockObj      : TObject;
    end;

    { Satz zum Speichern eines Raumschiffes }
    TRaumschiffName = String[20];
    TSchiffSaveRecord = record
      Name          : TRaumschiffName;
      ID            : Integer;
      Model         : Integer;
      WZellen       : Integer;
      HitPoints     : Integer;
      AktHitPoi     : Integer;
      Soldaten      : Integer;
      LagerPlatz    : Integer;
      BauZeit       : Integer;
      UFOID         : Cardinal;
      Shield        : TShield;
      Abschuesse    : Integer;
      SendMessage   : boolean;
      Motor         : TMotor;
      RepairTime    : Integer;
      Position      : TFloatPoint;
      ShieldPoi     : Integer;
      EinsatzID     : Cardinal;
      ExtendsSlots  : Byte;
      SensorWeite   : Word;
      XCOM          : boolean;
      TransportItem : TTransportItem;
      Ziel          : TFloatPoint;
      From          : TFloatPoint;
      Mission       : TRaumschiffMission;
      BuchType      : TKapital;
      SchiffID      : Cardinal;
      BasisID       : Cardinal;
      TownID        : Cardinal;
    end;

    TEinsatzLoadRec = record
      Name          : String;
      ID            : Cardinal;
      Position      : TFloatPoint;
      AlienCount    : Integer;
      Description   : String;
      Objectives    : String;
      RemainTime    : Integer;
      Aliens        : Array of TAlien;
    end;

    PSoldatWaffe    = ^TSoldatWaffe;
    TSoldatWaffe    = record
      Gesetzt       : boolean;
      ID            : Cardinal;
      Name          : String[50];
      Image         : Integer;
      TypeID        : TProjektType;
      WaffenArt     : TWaffenType;
      Gewicht       : single;            // Gewicht der aktuellen Waffe
      ZweiHand      : boolean;

      { Munition }
      MunGesetzt    : boolean;
      MunImage      : Integer;
      MunID         : Cardinal;
      Schuesse      : Integer;
      TimeLeft      : Integer;
      Strength      : Integer;

      // Power
      Power         : Integer;
      SchussMax     : Integer;
      SchussArt     : TSchussType;

      TimeUnits     : Byte;
      Genauigkeit   : Integer;
      Ladezeit      : Integer;
      MunGewicht    : single;
    end;

    TSoldatPanzerung = record
      Gesetzt       : boolean;
      ID            : Cardinal;
      Name          : String[50];
      Image         : Integer;
      Gewicht       : single;            // Gewicht der aktuellen Panzerung
    end;

    TSoldatGuertel   = record
      Gesetzt       : boolean;
      ID            : Cardinal;
      Name          : String[50];
      Gewicht       : Single;
      Slots         : Integer;
    end;

    // Identfiziert eindeutig eine Zelle in einem Raumschiff
    TSchiffZelle  = record
      Komponent   : THighlightKomponent;
      KompIndex   : Byte;
    end;

    TExplosionInfo = record
      Strength : Integer;
      Power    : Integer;
      Delay    : double;
      FromPos  : TPoint;
      ToPos    : TPoint;
      Figure   : TObject;
      OnlyWall : Boolean;
    end;

    PItemSlot     = ^TItemSlot;
    TItemSlot     = record
      SlotID      : Integer;
      Left        : Integer;
      Top         : Integer;
      ID          : Cardinal;
      Color       : Cardinal;
      Slot        : TSoldatConfigSlot;
      Data        : Integer;
      ZusData     : Pointer;
      Schuesse    : Integer;
      Deleted     : boolean;
      Anzahl      : Integer;
    end;

    // Informationen zu einer Einheitenanimation (werden in Unit Defines geladen)
    TModelInformation = record
      imageset     : String;
      FootSize     : Integer;
      torsoSize    : Integer;
      HeadSize     : Integer;
      FootHeight   : Integer;
      TorsoHeight  : Integer;
      HeadHeight   : Integer;
      FootMaxZ     : Integer;
      TorsoMaxZ    : Integer;
      HeadMaxZ     : Integer;

      SelWidth     : Integer;
      SelHeight    : Integer;

      AniHeight    : Integer;
      AniWidth     : Integer;
      AniOffSetX   : Integer;
      AniOffSetY   : Integer;
    end;

    // Typ der eine Nachricht speichert
    TGameMessage      = record
      Text      : String;
      MType     : TLongMessage;
      Objekt    : TObject;
      WasShowed : Boolean;
      DEvent    : TEventHandle;
    end;

    THeadData     = record
      Titel     : String;
      Einzel    : String;
      MehrZahl  : String;
    end;

    TBackPackItem            = record
      IsSet    : Boolean;
      ID       : Cardinal;
      Schuesse : Integer;
      Gewicht  : double;
    end;

    // Informationen zu einem geplanten Schuss
    PShootInfos              = ^TShootInfos;
    TShootInfos              = record
      Range                  : double;                             // wird in TGameFigure.ShootToPos vorgegeben
      TargetHeight           : Integer;                            // Ziel-H�he des Projektils (Standard -1, wenn Wert >0 dann muss auch TargetUnit angegeben sein)
      TargetUnit             : TObject;                            // wird nur ben�tigt, wenn ein gezielter Schuss durchgef�hrt wird (Zeiger auf TGameFigure)
    end;

    TWaffenParamInfos        = record
      Treff                  : Integer;                            // Treffsicherheit
      Power                  : Integer;                            // Durchschlagskraft
      Strength               : Integer;                            // Angriffst�rke
      Art                    : TWaffenType;                        // Waffenart
      Schusstyp              : TSchussType;                        // Schusstype
      MunID                  : Cardinal;                           // ID der Munition
      TargetHeight           : Integer;                            // Zielh�he, bei der der Schuss landen soll
      ZAxisStart             : Integer;                            // Z-Axis auf der der Schuss startet
      ShootSound             : String[200];                        // Sound der beim Abschuss abgespielt werden soll
    end;

    TBaseType  = (btBase,
                  btAlphatronMine,
                  btOutPost);

    TCheckBasePosResult = (cbprOK,
                           cbprNotOnLand,
                           cbprFriendlytooSmall,
                           cbprBaseInThisCountry,
                           cbprOutPostInThisCountry,
                           cbprAlphatronMineInThisCountry,
                           cbprTooManyBases,
                           cbprTooManyOutposts,
                           cbprTooManyAlphatronMines);
    { Missionsdaten }
    TMissionSkriptTyp = (mstManuell,mstRandom,mstSingle);

    TTriggerTyp       = (ttNone,
                         ttStartGame,
                         ttDate,
                         ttProjectReady,
                         ttUserDefined);

    TMissionFile = record
      FileName : String;
      Name     : String;
      Error    : String;
    end;

    TTrigger = record
      TriggerTyp     : TTriggerTyp;
      Negation       : Boolean;
      CarParam1      : Cardinal;
      IntParam1      : Integer;
      IntParam2      : Integer;
      StringParam1   : String;
      Cleared        : Boolean;
    end;

    TArrayTrigger = Array of TTrigger;

    TMissionData = record
      ID       : Cardinal;
      Typ      : TMissionSkriptTyp;
      Name     : String;
      Active   : Boolean;
      Triggers : TArrayTrigger;
      Skript   : String;
    end;

  const
    LagerItemType         : TProjektTypes     = [ptWaffe,ptMunition,ptRWaffe,ptPanzerung,ptRMunition,ptGranate,ptMotor,ptMine,ptSensor,ptShield,ptGuertel,ptExtension];
    SoldatenTypes         : TProjektTypes     = [ptWaffe,ptPanzerung,ptMunition,ptMine,ptSensor,ptGranate,ptGuertel];
    NoImages              : TProjektTypes     = [ptEinrichtung,ptRaumSchiff,ptNone];

    NoMunition            : TWaffenTypes      = [wtLaser,wtShortRange];

    EinheitSlot           : TSoldatConfigSlots = [scsLinkeHand,scsRechteHand,scsLinkeHandMunition,scsRechteHandMunition,scsPanzerung,scsGuertel,scsRucksack,scsMunition];
    SupportMunSlots       : TSoldatConfigSlots = [scsLinkeHand,scsRechteHand,scsLinkeHandMunition,scsRechteHandMunition,scsPanzerung,scsGuertel,scsRucksack,scsMunition,scsBodenItem,scsBasisItem];

    ShieldExtensions      : TExtensionPropSet = [epShieldHitpoints, epShieldDefence, epShieldReloadTime];
    AllMessages           : TMessages         = [Low(TLongMessage)..High(TLongMessage)];
    ZugSeiteToFriendStatus : Array[TZugSeite] of TFigureStatus = (fsFriendly,fsNeutral,fsEnemy);

var
  MessageNames  : Array[TLongMessage] of String;

implementation

end.

