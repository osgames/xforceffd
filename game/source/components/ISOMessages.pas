{********************************************************************************
*										*
* Copyright (C) 2004  X-Force Team - jim_raynor@web.de				*
*										*
* This file is part of X-Force: Fight For Destiny (X-Force).			*
*										*
* X-Force is free software; you can redistribute it and/or modify		*
* it under the terms of the GNU General Public License as published by		*
* the Free Software Foundation; either version 2 of the License, or		*
* (at your option) any later version.						*
*										*
* X-Force is distributed in the hope that it will be useful,			*
* but WITHOUT ANY WARRANTY; without even the implied warranty of		*
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the			*
* GNU General Public License for more details.					*
*										*
* You should have received a copy of the GNU General Public License		*
* along with X-Force; if not, write to the Free Software			*
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA	*
*										*
*********************************************************************************
*										*
*										*
********************************************************************************}

{$I ../Settings.inc}

unit ISOMessages;

interface

uses Windows, XForce_types, DXDraws, XANBitmap, GameFigureManager;

type
  TVerteilerMessage   = (vmSelection,                 vmAktuTempSurface,
                         vmAktuTempSurfaceRect,       vmEngineRedraw,
                         vmAddInfoText,               vmDeleteInfoText,
                         vmCanDrawUnit,               vmCenterPoint,
                         vmGetPause,                  vmIncSeeing,
                         vmDecSeeing,                 vmContainerLock,
                         vmContainerUnlock,           vmContainerIncLock,
                         vmContainerdeclock,          vmSetFigure,
                         vmGetFigure,                 vmMissionWin,
                         vmMissionLose,               vmMissionCancel,
                         vmMessage,                   vmCreateDeathObject,
                         {$IFNDEF DISABLEBEAM}vmCreateBeamObject,vmCanBeamTo,vmShowBeamMenue,{$ENDIF}
                         vmShoot,                     vmIsFieldDiscover,
                         vmGetTileRect,               vmCheckRedrawOfSolList,
                         vmNotifyUI,                  vmRedrawSolList,
                         vmCreateZielObject,          vmDeleteZielObject,
                         vmGetSelectedFormation,      vmCreateItemObject,
                         vmRoundEnd,                  vmTextMessage,
                         vmBuchungPunkte,             vmFriendTreffer,
                         vmShowNextUnit,              vmShowPrevUnit,
                         vmPlaySound,                 vmStopSound,
                         vmSetPause,                  vmSetUserInterfaceSperre,
                         vmGetUserInterfaceSperre,    vmKameraVerfolgung,
                         vmUseObject,                 vmBreakRound,
                         vmMiniMapSelectionChange,    vmWaffePosChanged,
                         vmTerrainChange,             vmCenterToPauseObject,
                         vmGoOnAfterMessage,          vmReleaseEscButton,
                         vmCreateFieldExplosion,      vmChangeHotkeys,
                         vmGetISOObjects,             vmThrowObject,
                         vmCanFriendMove,             vmGetObjectList,
                         vmCenterUnit,                vmShowWayPoint,
                         vmIsWayFree,                 vmCreateHintForFigure,
                         vmGetLagerItem,              vmGetFigureOfManager,
                         vmGetNamedSurface,           vmSetDrawingRect,
                         vmCanUnitDoAction,           vmGetUnitTimeUnits,
                         vmSetUnitTimeUnits,          vmNeedTimeUnits,
                         vmGetXANBitmap);

  TNotifyUIType       = (nuitSelectionChange,
                         nuitBeamReady,
                         nuitUnitDeath,
                         nuitCheckMerkeRedraw,
                         nuitPauseChange,
                         nuitMunitionChange,
                         nuitNextRound,
                         nuitTimeUnitChange,
                         nuitHitPointsChange);

  TBuchungPunkteType = (bptAlienTot,
                        bptSoldatTot,
                        bptGewonnen,
                        bptVerloren,
                        bptItemFound,
                        bptItemLost);

  TMessageRecord = record
    message      : TVerteilerMessage;
    result       : boolean;
    resultObj    : TObject;
    Point        : TPoint;
    OffSet       : TPoint;
    Figure       : TObject;
    InfoTextID   : Integer;
    Text         : String;
    Rect         : TRect;
    NotifyUIType : TNotifyUIType;
    PunkteType   : TBuchungPunkteType;
    Punkte       : Integer;
    Bool         : Boolean;
    Int          : Integer;
    Item         : PLagerItem;
    Status       : TFigureStatus;
    MessageType  : TEinsatzMessage;
    Slot         : TSoldatConfigSlot;
    Explosion    : TExplosionInfo;
    Objekt       : TObject;
    Objects      : Array of TObject;
    case Integer of
      0: ( // Wegpunkte
       Direction    : TViewDirection;
       ColorInd     : Integer;);
      1: ( // Shoot
       Ziel         : TPoint;
       Infos        : TWaffenParamInfos;);
      2: ( // IsWayFree
       FromP        : TPoint;
       ToP          : TPoint;
       WayFreeRes   : TWayFreeResult;);
      3: ( // GetLagerItem
       ID           : Cardinal;
       LagerItem    : PLagerItem;);
      4: ( // UseGrenade, UseObject
       SlotIndex    : Integer;);
      5: ( // GetSurface
       Surface      : TDirectDrawSurface;);
      6: ( // RoundEnde
       ZugSeite     : TZugSeite);
      7: ( // GetUnitTimeUnits
       TimeUnits    : Integer);
      8: ( // NeedTimeUnits
       TUresult     : TNeedTimeUnitResult);
      9: ( // GetXANBitmap
       XANBitmap    : TXANBitmap);
      10: ( // BreakRound
       WantBreakUnit: TObject;         // Einheit die die Runde unterbrechen m�chte
       BreakingUnit : TObject;);       // Einheit die unterbrochen wird
      11: ( // GetFigureOfManager
       Manager      : TGameFigureManager;);

  end;


  TOnMessage = procedure(var Rec: TMessageRecord) of object;

  var
    Rec            : TMessageRecord;
    MessageHandler : TOnMessage;

procedure SendVMessage(Message: TVerteilerMessage);
procedure BuchPunkte(Typ: TBuchungPunkteType; Punkte: Integer = 0);

implementation

procedure BuchPunkte(Typ: TBuchungPunkteType; Punkte: Integer = 0);
begin
  Rec.PunkteType:=Typ;
  Rec.Punkte:=Punkte;
  SendVMessage(vmBuchungPunkte);
end;

procedure SendVMessage(Message: TVerteilerMessage);
begin
  Rec.Message:=Message;
  MessageHandler(Rec);
end;

end.
