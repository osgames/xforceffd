{********************************************************************************
*										*
* Copyright (C) 2004  X-Force Team - jim_raynor@web.de				*
*										*
* This file is part of X-Force: Fight For Destiny (X-Force).			*
*										*
* X-Force is free software; you can redistribute it and/or modify		*
* it under the terms of the GNU General Public License as published by		*
* the Free Software Foundation; either version 2 of the License, or		*
* (at your option) any later version.						*
*										*
* X-Force is distributed in the hope that it will be useful,			*
* but WITHOUT ANY WARRANTY; without even the implied warranty of		*
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the			*
* GNU General Public License for more details.					*
*										*
* You should have received a copy of the GNU General Public License		*
* along with X-Force; if not, write to the Free Software			*
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA	*
*										*
*********************************************************************************
*										*
* Diese Unit stellt die Skriptsprache in X-Force zur Verf�gung			*
*										*
********************************************************************************}

{$I ../Settings.inc}

unit MissionSkriptEngine;

interface

uses
  Forms,Classes, SysUtils, TypInfo, Dialogs, Windows, TraceFile,
  Controls;

{ Define f�r Standardprozeduren }
{ Letzte Fehler - ID : 73 }

const
  MAX_TOKENS    = 50;

type
  PBoolean      = ^Boolean;
  EParseError   = class(Exception);
  EEmptyLine    = class(Exception);
  EAusdruckEnde = class(Exception);
  ERunTimeError = class(Exception);
  ETypeMisMatch = class(ERunTimeError);
  EComment      = class(Exception);

  TTokenType  = (
    ttBezeichner,
    ttString,
    ttZahl,
    ttKomma,
    ttBlockBegin,
    ttBlockEnd,
    ttThen,
    ttKlammerAuf,
    ttKlammerZu,
    ttLineEnd,
    ttNone,
    ttMission,
    ttBei,
    ttEndMission,
    ttVar,
    ttNicht,
    ttFor,
    ttTo,
    ttDownTo,
    ttWhile,
    ttDo,
    ttwenn,
    ttansonsten,
    ttUnd,
    ttOder,
    ttDivide,
    ttAdd,
    ttSubtract,
    ttMultiply,
    ttAssign,
    ttEqual,
    ttGreater,
    ttGreaterEqual,
    ttSmaller,
    ttSmallerEqual,
    ttGreaterSmaller,
    ttWahr,
    ttFalsch,
    ttPoint,
    ttInvalid,
    ttDoublePoint);

const
   Operatoren : set of TTokenType = [ttUnd,ttOder,ttAdd,ttSubtract,
                                     ttDivide,ttMultiply,ttEqual,ttGreater,ttGreaterEqual,
                                     ttSmaller,ttSmallerEqual,ttGreaterSmaller];

type
  TObjectType = (otFunction,otOwnerFunction,otVariable,otRecord,otObject);
  TValueType = (
    vtNone,
    vtString,
    vtZahl,
    vtBoolean,
    vtRecord);

  TOperation  = (opNone,opAdd,opSubtract,opMultiply,opDivision,opEqual,opSmaller,opHigher,opAnd,opOr,opNot,opMinus,opGreaterEqual,opSmallerEqual,opGreaterSmaller);

  TBasisObject = class;
  TVariable = class;
  TValue = class;
  TValueAusdruck = class;
  TValueConst = class;
  TValueMemberAccess = class;
  TValueVar = class;
  TChangeVar = procedure(Varia: TVariable) of object;
  TOwnerFunction = class;
  TRecordDiscriptor = class;
  TRecordInstance = class;
  TLineParser = class;
  TParameterList = class;
  TFunction = class;
  TCommand = class;

  TSkript  = class;

  TCallExternal = procedure(Skript: TSkript; Command: String;Param: TParameterList;Return: TValue) of object;

  PCharArray = ^TCharArray;
  TCharArray = Array[1..65535] of Char;

  PToken = ^TToken;
  TToken = record
    Typ   : TTokenType;
    XPos  : Integer;
    Text  : String;
  end;

  PAusdruck = ^TAusdruck;
  TAusdruck = record
    Value : TValue;
    Oper  : TOperation;
    XPos  : Integer;
  end;

  TIfPath = record
    Bedingung : TValue;
    Command   : TCommand;
    Line      : Integer;
  end;

  TMemberTyp = (mtMember, mtMethod);

  TMemberSourceTyp = (mstMethodCall,
                      mstShortString,
                      mstLongString,
                      mstByte,
                      mstWord,
                      mstInteger,
                      mstBoolean);

  TMemberSource = record
    Typ         : TMemberSourceTyp;
    OffSet      : Integer;
    maxLen      : Integer;
    ValueKat    : String;
  end;

  TRecordMember = record
    Name        : String;
    HashValue   : Cardinal;
    Typ         : TMemberTyp;
    ValueType   : TValueType;
    Discriptor  : TRecordDiscriptor;
    Func        : TFunction;
    ReadOnly    : boolean;
    Source      : TMemberSource;
    IsEvent     : Boolean;
  end;

  TOnError = procedure(Msg: String; Skript: TSkript) of object;

  TMissionSkriptEngine = class(TObject)
  private
    fObjekts       : TList;
    fAllObjekts    : TList;
    fAParsers      : TList;
    fValuesDefault : Boolean;
    fExternal      : TCallExternal;
    fChangeVar     : TChangeVar;
    fValuesList    : TList;
    fOnError       : TOnError;
    fParsing       : Boolean;
    fSkriptBeginn  : String;
    procedure Clear;
  public
    constructor Create;
    destructor destroy;override;
    function LoadSkript(Strings: TStrings): TSkript;overload;
    function LoadSkript(FileName: String): TSkript;overload;
    procedure AddFunc(Name: String;ParamCount: Integer;Return: TValueType;ObjektTyp: TRecordDiscriptor = nil);
    procedure AddVariable(Name: String;Value: String; Typ: TValueType; Kat: String = ''; Rec: TRecordDiscriptor = nil);
    function NewRecord(Name: String;Publ: Boolean = true): TRecordDiscriptor;

    function IsDefine(Text: String): boolean;
    function FindObjectFromName(Text: String; All: Boolean = false): TBasisObject;
    property Objekts: TList read fObjekts write fObjekts;
    property OnCallExternal: TCallExternal read fExternal write fExternal;
    property OnChangeVar   : TChangeVar read fChangeVar write fChangeVar;
    property OnError       : TOnError read fOnError write fOnError;
    property SkriptBeginn  : String read fSkriptBeginn write fSkriptBeginn;
  end;

  TSkript = class(TObject)
  private
    List           : TStringList;
    fObjekts       : TList;
    fObjectList    : TList;
    fEngine        : TMissionSkriptEngine;
    fCommands      : TList;
    fValuesDefault : Boolean;
    fHasEndMission : Boolean;
    fHasVariables  : boolean;
    fInVariables   : boolean;
    fHasMission    : Boolean;
    fSetNoCommands : Boolean;
    fSkriptName    : String;
    fErrors        : Boolean;
    fErrorMsg      : String;
    fData          : Pointer;
    procedure ParseLine;
    procedure CheckHeadLine;

    procedure Clear;
  public
    constructor Create(Engine: TMissionSkriptEngine);
    destructor Destroy;override;
    procedure Parse(FileName: String);overload;
    procedure Parse(Strings: TStrings);overload;

    procedure ParseOnlyDeklarations(Strings: TStrings);

    procedure AddObject(Obj: TObject);
    procedure RemoveObject(Obj: TObject);

    procedure AddNamedObject(Obj: TBasisObject);

    procedure Call(Text: String);
    function IsCommand(Line: Integer): boolean;
    function FindObjectFromName(Text: String): TBasisObject;

    procedure Error(Error: String);

    property SkriptName    : String read fSkriptName;
    property Errors        : boolean read fErrors;
    property ErrorMessage  : String read fErrorMsg;
    property Engine        : TMissionSkriptEngine read fEngine;
    property Objekts       : TList read fObjekts write fObjekts;

    property Data          : Pointer read fData write fData;
  end;

  TParameterList = class(TObject)
  private
    fList: TList;
    function GetParamCount: Integer;
    function GetParameter(Index: Integer): TValue;
  public
    constructor Create;
    destructor Destroy;override;
    procedure ParseParams(Parser: TLineParser;ParamCount: Integer);
    procedure Reset;

    function HasParameter(Index: Integer): boolean;
    property ParamCount: Integer read GetParamCount;
    property Parameter[Index: Integer]: TValue read GetParameter;default;
  end;

  TLineParser = class(TObject)
  private
    fLine      : String;
    fSLine     : Integer;
    fGesamt    : String;
    fTType     : TTokenType;
    fFunc      : TOwnerFunction;
    fTText     : String;
    fTokens    : Array[0..MAX_TOKENS] of TToken;
    fTokenCount: Integer;
    fEngine    : TMissionSkriptEngine;
    AktToken   : PToken;
    fParsePos  : PChar;
    fTokenLen  : Integer;
    procedure ParseLine;
    procedure ReadToken;
    function GetTokenType: TTokenType;
    function GetTokenText: String;

  public
    constructor Create(Line: String;Engine: TMissionSkriptEngine; Func: TOwnerFunction);
    destructor Destroy;override;
    procedure CreateAusdruck(var Value: TValue);
    procedure CreateValue(var Value: TValue; CanNoType: Boolean = false);
    procedure NextToken;
    procedure Restart;
    procedure SetFunction(Func: TOwnerFunction);
    procedure CheckToken(NeedToken: TTokenType);
    property TokenType  : TTokenType read GetTokenType;
    property TokenText  : String read GetTokenText;
    property SourceLine : Integer read fSLine;
    property TokenCount : Integer read fTokenCount;
  end;

  TAusdruckParser = class(TObject)
  private
    fValues    : Array of TAusdruck;
    AktToken   : PAusdruck;
    Last       : PAusdruck;
    fSLine     : Integer;
    fEngine    : TMissionSkriptEngine;
    fFunc      : TOwnerFunction;
    fOper      : TOperation;
    fVal       : TValue;
  public
    constructor Create(Engine: TMissionSkriptEngine; Func: TOwnerFunction);
    destructor Destroy;override;
    procedure AddValue(Value: TValue;Operator: TOperation;XPos: Integer);
    procedure Restart;
    procedure SetToStartPos;
    function NextToken: boolean;
    property Operator   : TOperation read fOper;
    property Value      : TValue read fVal;
    property SourceLine : Integer read fSLine write fSLine;
  end;

  TCommand = class(TObject)
  private
    fEngine : TMissionSkriptEngine;
    fSkript : TSkript;
    fParams : TParameterList;
    fSetOp  : boolean;
    fEmpf   : TValue;
    Value   : TValue;
    fSLine  : Integer;
    fFunc   : TOwnerFunction;
  public
    constructor Create(Skript : TSkript; Func: TOwnerFunction);
    destructor destroy;override;
    procedure ErstelleCommando(Parser: TLineParser);
    procedure InitVariable(Variable: TVariable);
    procedure Start;virtual;
    procedure Reset;virtual;
    procedure NewStart;virtual;
    function IsCommand(Line: Integer): boolean;virtual;
    function MakeSingleStep: TCommand;virtual;
    function GetFirst: TCommand;virtual;
    property Line: Integer read fSLine write fSLine;
  end;

  TCommandList = class(TCommand)
  private
    fList  : TList;
    fLine  : Integer;
  public
    destructor destroy;override;
    procedure ErstelleListe(List: TStringList);
    procedure Reset;override;
    procedure NewStart;override;
    procedure Start;override;
    function IsCommand(Line: Integer): boolean;override;
    function GetFirst: TCommand;override;
    function MakeSingleStep: TCommand;override;
  end;

  TForBlock = class(TCommand)
  private
    fCommand : TCommand;
    fFrom    : TValue;
    fTo      : TValue;
    fVar     : TValueVar;
    fCount   : Integer;
    fDown    : boolean;
  public
    procedure ErstelleListe(List: TStringList);
    procedure Reset;override;
    procedure NewStart;override;
    procedure Start;override;
    function IsCommand(Line: Integer): boolean;override;
    function MakeSingleStep: TCommand;override;
  end;

  TWhileBlock = class(TCommand)
  private
    fCommand    : TCommand;
    fBedingung  : TValue;
    fInCommands : Boolean;
  public
    procedure ErstelleListe(List: TStringList);
    procedure Reset;override;
    procedure NewStart;override;
    procedure Start;override;
    function IsCommand(Line: Integer): boolean;override;
    function MakeSingleStep: TCommand;override;
  end;

  TIfBlock = class(TCommand)
  private
    fPaths     : Array of TIfPath;
    fLine      : Integer;
    fPath      : Integer;
    fElse      : TCommand;
    fDebugComm : TCommand;
  public
    procedure ErstelleListe(List: TStringList);
    procedure Reset;override;
    procedure NewStart;override;
    procedure Start;override;
    function IsCommand(Line: Integer): boolean;override;
    function MakeSingleStep: TCommand;override;
  end;

  TBasisObject = class(TObject)
  protected
    fName       : String;
    fHashValue  : Cardinal;
    fObjektType : TObjectType;
    fEngine     : TMissionSkriptEngine;
    fLine       : Integer;
    fDefault    : boolean;
    fPublic     : boolean;
    fSkript     : TSkript;
    procedure SetName(const Value: String);virtual;
  public
    constructor Create(Engine: TMissionSkriptEngine; Skript: TSkript);virtual;
    destructor Destroy;override;
    function IsCommand(Line: Integer): boolean;virtual;
    property Name            : String read fName write SetName;
    property ObjektType      : TObjectType read fObjektType write fObjektType;
    property LineDeklaration : Integer read fLine write fLine;
    property IsDefault       : boolean read fDefault write fDefault;
    property IsPublic        : boolean read fPublic write fPublic;
  end;

  TFunction = class(TBasisObject)
  private
    fParamCount : Integer;
    fReturn     : TValueType;
    fValue      : TValueConst;
    fRecordTyp  : TRecordDiscriptor;
    fExternalTyp: TRecordDiscriptor;
    fParamInfo  : String;
    procedure SetHasReturn(const Value: TValueType);
    procedure SetRecordTyp(const Value: TRecordDiscriptor);
    function GetDeklaration: String;
  protected
    procedure SetName(const Value: String);override;
  public
    constructor Create(Engine: TMissionSkriptEngine; Skript: TSkript);override;
    procedure Start(Skript: TSkript; ParamList: TParameterList);
    procedure SetMethod(Typ: TRecordDiscriptor);
    function AsString    : String;
    function AsZahl      : Integer;
    function AsRecord    : TRecordInstance;
    function AsBoolean   : Boolean;
    function GetNoType   : String;
    property ParamCount  : Integer read fParamCount write fParamCount;
    property Return      : TValueType read fReturn write SetHasReturn;
    property RecordTyp   : TRecordDiscriptor read fRecordTyp write SetRecordTyp;
    property ParamInfo   : String read fParamInfo;
    property Deklaration : String read GetDeklaration;
  end;

  TOwnerFunction = class(TBasisObject)
  private
    fList          : TList;
    fVariables     : Array of TVariable;
    fEndLine       : Integer;
    fHasVariables  : boolean;
    function GetVarCount: Integer;
    function GetVariable(Index: Integer): TVariable;
  public
    constructor Create(Engine: TMissionSkriptEngine; Skript: TSkript);override;
    destructor destroy;override;
    procedure AddVariable(Parser: TLineParser);
    procedure GeneriereFunction(List: TStringList);
    procedure Start;
    procedure NewStart;
    function FindObject(Text: String): TBasisObject;
    function IsDefine(Text: String): Boolean;
    function GetFirstCommand: TCommand;
    function MakeSingleStep: TCommand;
    function IsCommand(Line: Integer): boolean;override;
    property LastLine                   : Integer read fEndLine write fEndLine;
    property VarCount                   : Integer read GetVarCount;
    property Variables[Index: Integer]  : TVariable read GetVariable;
    property Commands                   : TList read fList;
  end;

  TMemberEvent   = procedure(Skript: TSkript; Instance: TRecordInstance;Member: String;Value: TValue) of object;
  TGetSaveString = procedure(Skript: TSkript; Instance: TRecordInstance;var SaveString: String) of object;

  TRecordDiscriptor = class(TBasisObject)
  private
    fMembers          : Array of TRecordMember;
    fInstances        : TList;
    fOnWriteMember    : TMemberEvent;
    fOnReadMember     : TMemberEvent;
    fOnMethod         : TCallExternal;
    fInstanceSize     : Integer;
    fOnNeedSaveString : TGetSaveString;
    fOnSetSaveString  : TGetSaveString;
    function GetMemberCount: Integer;
    function GetRecordMember(Index: Integer): TRecordMember;
  protected
    procedure AddInstance(Instance: TRecordInstance);
    procedure RemoveInstance(Instance: TRecordInstance);
  public
    constructor Create(Engine: TMissionSkriptEngine; Skript: TSkript);override;
    destructor Destroy;override;
    procedure NotifyInstances;
    procedure AddMember(Name: String; Return: TValueType;const MemberSource: TMemberSource; ReadOnly: Boolean = false; Typ: TRecordDiscriptor = nil);
    procedure AddEvent(Name: String; const MemberSource: TMemberSource);
    procedure AddMethod(Name: String; Params: Integer;Return: TValueType; Typ: TRecordDiscriptor = nil);

    function IsMember(Name: String): Boolean;

    property MemberCount: Integer read GetMemberCount;
    property Members[Index: Integer]: TRecordMember read GetRecordMember;
    property OnReadMember      : TMemberEvent read fOnReadMember write fOnReadMember;
    property OnWriteMember     : TMemberEvent read fOnWriteMember write fOnWriteMember;
    property OnCallMethod      : TCallExternal read fOnMethod write fOnMethod;
    property ExternalSize      : Integer read fInstanceSize write fInstanceSize;
    property OnNeedSaveString  : TGetSaveString read fOnNeedSaveString write fOnNeedSaveString;
    property OnSetSaveString   : TGetSaveString read fOnSetSaveString write fOnSetSaveString;
  end;

  TRecordFields = record
    Name     : String;
    HashValue: Cardinal;
    Typ      : TValueType;
    Instance : TRecordInstance;
    Accessor : TValueMemberAccess;
    Func     : TFunction;
    ReadOnly : Boolean;
    Source   : TMemberSource;
  end;

  TRecordInstance = class(TBasisObject)
  private
    fDiscriptor       : TRecordDiscriptor;
    fDataField        : Array of TRecordFields;
    fExternalData     : Pointer;
    fMemberChange     : boolean;
    fIsOwnerData      : Boolean;
    procedure Clear;
    procedure SetDiscriptor(const Value: TRecordDiscriptor);
    procedure MembersChange;
    function GetMemberCount: Integer;
    function GetMembers(Index: Integer): TRecordFields;
    procedure SetExternalData(const Value: Pointer);
    function GetAsString(Member: String): String;
    procedure SetAsString(Member: String; const Value: String);
    function GetAsZahl(Member: String): Integer;
    procedure SetAsZahl(Member: String; const Value: Integer);
  public
    constructor Create(Engine: TMissionSkriptEngine; Skript: TSkript);override;
    destructor Destroy;override;
    procedure Assign(Instance: TRecordInstance);
    function ParseMember(Parser: TLineParser; LastValue: TValue = nil): TValue;
    function GetMemberInfo(Member: String): TRecordFields;
    function GetSavedString: String;
    procedure SetSavedString(SavedString: String);
    property RecordTyp     : TRecordDiscriptor read fDiscriptor write SetDiscriptor;
    property ExternalData  : Pointer read fExternalData write SetExternalData;
    property AsString [Member: String] : String read GetAsString write SetAsString;
    property AsZahl   [Member: String] : Integer read GetAsZahl write SetAsZahl;

    property MemberCount: Integer read GetMemberCount;
    property Members[Index: Integer]: TRecordFields read GetMembers;
  end;

  TVariable = class(TBasisObject)
  private
    fText      : String;
    fInteger   : Integer;
    fBoolean   : Boolean;
    fInstance  : TRecordInstance;

    fReadOnly  : boolean;
    fValueType : TValueType;
    fKat       : String;
    fProtected: Boolean;
    function GetString: String;
    function GetZahl: Integer;
    procedure SetString(const Value: String);
    procedure SetZahl(const Value: Integer);
    procedure Change;
    function GetBoolean: Boolean;
    procedure SetBoolean(const Value: Boolean);
    function GetRecord: TRecordInstance;
    procedure SetRecord(const Value: TRecordInstance);
  public
    constructor Create(Engine: TMissionSkriptEngine; Skript: TSkript);override;
    procedure ReadLine(Parser: TLineParser;Func: TOwnerFunction);
    procedure AssignRecord(Instance: TRecordInstance);
    procedure Protect(Protect: Boolean);
    function GetNoType: String;
    property AsZahl    : Integer read GetZahl write SetZahl;
    property AsString  : String read GetString write SetString;
    property AsBoolean : Boolean read GetBoolean write SetBoolean;
    property AsRecord  : TRecordInstance read GetRecord write SetRecord;
    property ValueType : TValueType read fValueType;
    property IsProtect : Boolean read fProtected;
    property Kategorie : String read fKat write fKat;
    property ReadOnly  : boolean read fReadOnly write fReadOnly;
  end;

  TValue = class(TObject)
  protected
    fEngine       : TMissionSkriptEngine;
    fFunc         : TOwnerFunction;
    fValueType    : TValueType;
    fDiscriptor   : TRecordDiscriptor;
    fDefault      : Boolean;
    fSkript       : TSkript;
    function GetBoolean: boolean;virtual;
    function GetString: String;virtual;
    function GetZahl: Integer;virtual;
    function GetRecord: TRecordInstance;virtual;
    function GetDiscriptor: TRecordDiscriptor;virtual;
    function GetKategorie: String;virtual;
    procedure SetDiscriptor(const Value: TRecordDiscriptor);virtual;
    procedure SetRecord(const Value: TRecordInstance);virtual;
    procedure SetBoolean(const Value: boolean);virtual;
    procedure SetString(const Value: String);virtual;
    procedure SetZahl(const Value: Integer);virtual;
    function GetValueType: TValueType;virtual;
    function GetReadOnly: Boolean;virtual;
    function GetCanCall: Boolean;virtual;
  public
    constructor Create(Engine: TMissionSkriptEngine;Func: TOwnerFunction);virtual;
    destructor Destroy;override;
    function GetNoType: String;virtual;
    function IsConstant: Boolean;virtual;
    procedure Reset;virtual;
    procedure Start;virtual;

    procedure SetSkript(Skript: TSkript);virtual;

    property AsZahl    : Integer read GetZahl write SetZahl;
    property AsString  : String read GetString write SetString;
    property AsBoolean : boolean read GetBoolean write SetBoolean;
    property AsRecord  : TRecordInstance read GetRecord write SetRecord;
    property ValueTyp  : TValueType read GetValueType write fValueType;
    property ReadOnly  : Boolean read GetReadOnly;
    property IsDefault : Boolean read fDefault write fDefault;
    property CanCall   : Boolean read GetCanCall;

    property RecordTyp : TRecordDiscriptor read GetDiscriptor write SetDiscriptor;
  end;

  TValueVar = class(TValue)
  protected
    fVaria     : TVariable;
    fProtected : Boolean;
    function GetBoolean: boolean;override;
    function GetString: String;override;
    function GetZahl: Integer;override;
    function GetRecord: TRecordInstance;override;
    function GetReadOnly: Boolean;override;
    procedure SetRecord(const Value: TRecordInstance);override;
    procedure SetBoolean(const Value: boolean);override;
    procedure SetString(const Value: String);override;
    procedure SetZahl(const Value: Integer);override;
  public
    constructor Create(Engine: TMissionSkriptEngine;Func: TOwnerFunction);override;
    function GetValueType: TValueType;override;
    function IsConstant: Boolean;override;
    procedure SetVariable(Varia: TVariable);
    procedure Protect(Protect: Boolean);
  end;

  TValueConst = class(TValue)
  protected
    fText      : String;
    fZahl      : Integer;
    fBoolean   : Boolean;
    fInstance  : TRecordInstance;
    function GetBoolean: boolean;override;
    function GetString: String;override;
    function GetZahl: Integer;override;
    function GetRecord: TRecordInstance;override;
    function GetReadOnly: Boolean;override;
    procedure SetDiscriptor(const Value: TRecordDiscriptor);override;
    procedure SetRecord(const Value: TRecordInstance);override;
    procedure SetBoolean(const Value: boolean);override;
    procedure SetString(const Value: String);override;
    procedure SetZahl(const Value: Integer);override;
  public
    function IsConstant: Boolean;override;
  end;

  TValueAusdruck = class(TValue)
  protected
    fInteger   : Integer;
    fBoolean   : Boolean;
    fText      : String;
    fWasStart  : boolean;
    fParser    : TAusdruckParser;
    fRecord    : TRecordInstance;
    function GetBoolean: boolean;override;
    function GetString: String;override;
    function GetZahl: Integer;override;
    function GetRecord: TRecordInstance;override;
    procedure SetRecord(const Value: TRecordInstance);override;
    procedure SetBoolean(const Value: boolean);override;
    procedure SetString(const Value: String);override;
    procedure SetZahl(const Value: Integer);override;
    procedure SetParser(Parser: TAusdruckParser);
  public
    function GetNoType: String;override;
    procedure Start;override;
    procedure SetFunc(Func: TOwnerFunction);
    procedure Reset;override;
    function IsConstant: Boolean;override;
  end;

  TValueFunc = class(TValue)
  protected
    fFunction  : TFunction;
    fParamList : TParameterList;
    fWasStart  : boolean;
    function GetBoolean: boolean;override;
    function GetString: String;override;
    function GetZahl: Integer;override;
    function GetRecord: TRecordInstance;override;
    procedure SetBoolean(const Value: boolean);override;
    procedure SetString(const Value: String);override;
    procedure SetZahl(const Value: Integer);override;
    function GetValueType: TValueType;override;
    function GetCanCall: Boolean;override;
  public
    function GetNoType: String;override;
    procedure Start;override;
    procedure SetFunction(Funct: TFunction;Params: TParameterList);
    procedure Reset;override;
  end;

  TValueMemberAccess = class(TValue)
  protected
    fInstance : TRecordInstance;
    fInstVal  : TValue;
    fMember   : TRecordFields;
    fVar      : TValue;
    fWasStart : Boolean;
    fParamList: TParameterList;
    fMethod   : Boolean;
    function GetBoolean: boolean;override;
    function GetString: String;override;
    function GetZahl: Integer;override;
    function GetRecord: TRecordInstance;override;
    function GetValueType: TValueType;override;
    function GetReadOnly: Boolean;override;
    function GetKategorie: String;override;
    procedure SetRecord(const Value: TRecordInstance);override;
    procedure SetBoolean(const Value: boolean);override;
    procedure SetString(const Value: String);override;
    procedure SetZahl(const Value: Integer);override;
    function GetCanCall: Boolean;override;
  public
    constructor Create(Engine: TMissionSkriptEngine;Func: TOwnerFunction);override;
    procedure Start;override;
    procedure Reset;override;
    procedure SetMember(Member: String; Instance: TRecordInstance);
    procedure SetInstanceValue(Value: TValue);
    property Kategorie : String read GetKategorie;
  end;

var
  ParsingLine    : Integer;
  ParsingPos     : Integer;
  Parsers        : TList = nil;
  IdentTable     : Array[Char] of TTokenType;

  // Alle Values werden diesem Skript zugeordnet
  ParsingSkript  : TSkript;

  // Das Skript, dass zur Zeit ausgef�hrt wird
  RunningSkript  : TSkript;


function NameOfToken(Token: TTokenType; Text: String; SkriptBeginn: String): String;
function NameOfType(Typ: TValueType): String;
procedure CheckTyp(NeedTyp: TValueType; IsTyp: TValueType);
function ToScriptString(Text: String): String;
function InterpretiereCommand(Befehle: TStringList; Skript: TSkript; Func: TOwnerFunction): TCommand;
function Compare(Val1,Val2: TValue; Art: TOperation): boolean;
function StringToBoolean(Str: String): boolean;
function BooleanToString(Bol: boolean): String;
procedure FreeandNil(var Obj: TObject);
function GetPosText: String;
function BuildMemberSource(Typ: TMemberSourceTyp; Offset: Integer = 0; MaxLen: Integer = 0): TMemberSource;overload;
function BuildMemberSource(Typ: TMemberSourceTyp; ValueKat : String; Offset: Integer = 0): TMemberSource;overload;

var
  ValuesCreate    : Integer = 0;
  ValuesDestroy   : Integer = 0;
  CommandsCreate  : Integer = 0;
  CommandsDestroy : Integer = 0;
  ObjektsCreate   : Integer = 0;
  ObjektsDestroy  : Integer = 0;
  ParsersCreate   : Integer = 0;
  ParsersDestroy  : Integer = 0;

implementation

uses SkriptUtils;

function BuildMemberSource(Typ: TMemberSourceTyp; ValueKat : String; Offset: Integer = 0): TMemberSource;overload;
begin
  result:=BuildMemberSource(Typ,OffSet,0);
  result.ValueKat:=ValueKat;
end;

function BuildMemberSource(Typ: TMemberSourceTyp; Offset: Integer; MaxLen: Integer): TMemberSource;
begin
  result.Typ:=Typ;
  result.OffSet:=OffSet;
  result.MaxLen:=MaxLen;
end;

procedure ClearParsers;
begin
  while Parsers.Count>0 do
    TLineParser(Parsers[0]).Free;
  Parsers.Clear;
end;

function GetPosText: String;
begin
  result:=Format('Position: (%d,%d)',[ParsingLine,ParsingPos]);
end;

function NameOfToken(Token: TTokenType; Text: String; SkriptBeginn: String): String;
begin
  result:='';
  case Token of
    ttBezeichner     : result:='Bezeichner '''+Text+'''';
    ttString         : result:='String '''+Text+'''';
    ttZahl           : result:='Zahl '''+Text+'''';
    ttKomma          : result:=',';
    ttBlockBegin     : result:='begin';
    ttBlockEnd       : result:='end';
    ttThen           : result:='then';
    ttKlammerAuf     : result:='(';
    ttKlammerZu      : result:=')';
    ttLineEnd        : result:='Zeilenende';
    ttMission        : result:=SkriptBeginn;
    ttBei            : result:='event';
    ttEndMission     : result:='end'+SkriptBeginn;
    ttVar            : result:='var';
    ttNicht          : result:='not';
    ttFor            : result:='for';
    ttTo             : result:='to';
    ttDownTo         : result:='downto';
    ttWhile          : result:='while';
    ttDo             : result:='do';
    ttwenn           : result:='if';
    ttansonsten      : result:='else';
    ttUnd            : result:='and';
    ttOder           : result:='or';
    ttDivide         : result:='/';
    ttAdd            : result:='+';
    ttSubtract       : result:='-';
    ttMultiply       : result:='*';
    ttAssign         : result:=':=';
    ttEqual          : result:='=';
    ttGreater        : result:='>';
    ttGreaterEqual   : result:='>=';
    ttSmaller        : result:='<';
    ttSmallerEqual   : result:='<=';
    ttGreaterSmaller : result:='<>';
    ttWahr           : result:='true';
    ttFalsch         : result:='false';
    ttPoint          : result:='.';
    ttDoublePoint    : result:=':';
  end;
end;

function NameOfType(Typ: TValueType): String;
begin
  case Typ of
    vtString  : result:='String';
    vtZahl    : result:='Zahl';
    vtBoolean : result:='Wahrheitswert';
    vtRecord  : result:='Objekt';
    vtNone    : result:='untypisiert';
  end;
end;

procedure CheckTyp(NeedTyp: TValueType; IsTyp: TValueType);
begin
  if NeedTyp<>IsTyp then
    raise EParseError.Create('[000] '+GetPosText+' Inkompatible Typen: '''+NameOfType(NeedTyp)+''' und '''+NameOfType(IsTyp)+'''');
end;

function StringToBoolean(Str: String): boolean;
begin
  if StrIComp(PChar(Str),'true')=0 then result:=true
  else if StrIComp(PChar(Str),'false')=0 then result:=false
  else
    raise Exception.Create('[002] '+GetPosText+' Kein boolescher Ausdruck.');
end;

function BooleanToString(Bol: boolean): String;
begin
  if Bol then result:='true' else result:='false';
end;

function ToScriptString(Text: String): String;
var
  Dummy: Integer;
begin
  result:='''';
  for Dummy:=1 to length(text) do
  begin
    case Text[Dummy] of
      '''' : result:=result+'\''';
      #10  : result:=result+'\n';
      else
        result:=result+Text[Dummy];
    end;
  end;
  result:=result+'''';
end;

function InterpretiereCommand(Befehle: TStringList; Skript: TSkript;Func: TOwnerFunction): TCommand;
var
  Command  : TCommand;
  Parser   : TLineParser;
begin
  try
    Parser:=TLineParser(Befehle.Objects[0]);
    ParsingLine:=Parser.SourceLine;
    Parser.Restart;
    Parser.SetFunction(Func);
    Parser.NextToken;
    if Parser.Tokentype=ttFor then
    begin
      Command:=TForBlock.Create(Skript,Func);
      Command.Line:=Parser.SourceLine;
      TForBlock(Command).ErstelleListe(Befehle);
    end
    else if Parser.Tokentype=ttWhile then
    begin
      Command:=TWhileBlock.Create(Skript,Func);
      Command.Line:=Parser.SourceLine;
      TWhileBlock(Command).ErstelleListe(Befehle);
    end
    else if Parser.TokenType=ttWenn then
    begin
      Parser.Restart;
      Command:=TIfBlock.Create(Skript,Func);
      Command.Line:=Parser.SourceLine;
      TIfBlock(Command).ErstelleListe(Befehle);
    end
    else if Parser.TokenType=ttBlockBegin then
    begin
      Command:=TCommandList.Create(Skript,Func);
      Command.Line:=Parser.SourceLine;
      TCommandList(Command).ErstelleListe(Befehle);
    end
    else
    begin
      Command:=TCommand.Create(Skript,Func);
      Command.Line:=Parser.SourceLine;
      Command.ErstelleCommando(Parser);
      Befehle.Delete(0);
    end;
    result:=Command;
  except
    on E: Exception do
    begin
      Skript.Error(E.Message);
      Befehle.Delete(0);
      result:=nil;
      if Assigned(Skript.Engine.OnError) then
        Skript.Engine.OnError(E.Message,Skript);
    end;
  end;
end;

procedure FreeandNil(var Obj: TObject);
begin
  if Obj<>nil then
  begin
    Obj.Free;
    Obj:=nil;
  end;
end;

function Compare(Val1,Val2: TValue;Art: TOperation): boolean;
begin
  result:=false;
  CheckTyp(Val1.ValueTyp,Val2.ValueTyp);
  case Val1.ValueTyp of
    vtString:
    begin
      case Art of
        opEqual          : result:=Val1.AsString=Val2.AsString;
        opSmaller        : result:=Val1.AsString<Val2.AsString;
        opHigher         : result:=Val1.AsString>Val2.AsString;
        opGreaterEqual   : result:=Val1.AsString>=Val2.AsString;
        opSmallerEqual   : result:=Val1.AsString<=Val2.AsString;
        opGreaterSmaller : result:=Val1.AsString<>Val2.AsString;
      end;
    end;
    vtZahl:
    begin
      case Art of
        opEqual          : result:=Val1.AsZahl=Val2.AsZahl;
        opSmaller        : result:=Val1.AsZahl<Val2.AsZahl;
        opHigher         : result:=Val1.AsZahl>Val2.AsZahl;
        opGreaterEqual   : result:=Val1.AsZahl>=Val2.AsZahl;
        opSmallerEqual   : result:=Val1.AsZahl<=Val2.AsZahl;
        opGreaterSmaller : result:=Val1.AsZahl<>Val2.AsZahl;
      end;
    end;
    vtBoolean:
    begin
      case Art of
        opEqual          : result:=Val1.AsBoolean=Val2.AsBoolean;
        opSmaller        : result:=Val1.AsBoolean<Val2.AsBoolean;
        opHigher         : result:=Val1.AsBoolean>Val2.AsBoolean;
        opGreaterEqual   : result:=Val1.AsBoolean>=Val2.AsBoolean;
        opSmallerEqual   : result:=Val1.AsBoolean<=Val2.AsBoolean;
        opGreaterSmaller : result:=Val1.AsBoolean<>Val2.AsBoolean;
      end;
    end;
    vtRecord:
      raise Exception.Create('[003] '+GetPosText+' Records sind nicht bei erlaubt');
  end;
end;

{ TMissionSkriptEngine }

procedure TMissionSkriptEngine.AddFunc(Name: String; ParamCount: Integer ;Return: TValueType; ObjektTyp: TRecordDiscriptor);
var
  Func: TFunction;
begin
  Func:=TFunction.Create(Self,nil);
  fObjekts.Add(Func);
  Func.Name:=Name;
  Func.ParamCount:=ParamCount;
  Func.Return:=Return;
  Func.RecordTyp:=ObjektTyp;
  Func.IsDefault:=true;
end;

procedure TMissionSkriptEngine.AddVariable(Name, Value: String; Typ: TValueType;Kat: String; Rec: TRecordDiscriptor);
var
  Vari     : TVariable;
  Instance : TRecordInstance;
begin
  Vari:=TVariable.Create(Self,nil);
  Vari.Name:=Name;
  Vari.Kategorie:=Kat;
  Vari.IsDefault:=true;
  fObjekts.Add(Vari);
  if Rec=nil then
  begin
    case Typ of
      vtZahl   : Vari.SetZahl(StrToInt(Value));
      vtString : Vari.SetString(Value);
      vtBoolean: Vari.SetBoolean(StringToBoolean(Value));
    end;
  end
  else
  begin
    Instance:=TRecordInstance.Create(Self,nil);
    Instance.RecordTyp:=Rec;
    Instance.Name:=Name;
    Vari.AsRecord:=Instance;
  end;
  Vari.ReadOnly:=true;
end;

procedure TMissionSkriptEngine.Clear;
var
  Dummy: Integer;
begin
  for Dummy:=fObjekts.Count-1 downto 0 do
  begin
    if not TBasisObject(fObjekts[Dummy]).IsDefault then
    begin
      TBasisObject(fObjekts[Dummy]).Free;
      fObjekts.Delete(Dummy);
    end;
  end;

  Dummy:=1;
  while fValuesList.Count>Dummy-1 do
  begin
    if TValue(fValuesList[fValuesList.Count-Dummy]).IsDefault then
      inc(Dummy)
    else
      TValue(fValuesList[fValuesList.Count-Dummy]).Free;
  end;

  Dummy:=1;
  while fAllObjekts.Count>Dummy-1 do
  begin
    if TBasisObject(fAllObjekts[fAllObjekts.Count-Dummy]).IsDefault then
      inc(Dummy)
    else
      TBasisObject(fAllObjekts[fAllObjekts.Count-Dummy]).Free;
  end;

end;

constructor TMissionSkriptEngine.Create;
begin
  fObjekts:=TList.Create;
  fValuesList:=TList.Create;
  fValuesDefault:=true;
  fAllObjekts:=TList.Create;
  fAParsers:=TList.Create;
  fParsing:=false;
  fSkriptBeginn:='mission';
//  InitDefaultFunctions;
end;

destructor TMissionSkriptEngine.destroy;
begin
  Clear;
  while fAllObjekts.Count>0 do
    TBasisObject(fAllObjekts[0]).Free;

  while fValuesList.Count>0 do
  begin
    TValue(fValuesList[0]).Free;
  end;

  while fAParsers.Count>0 do
  begin
    TAusdruckParser(fAParsers[0]).Free;
  end;

  FreeAndNil(TObject(fObjekts));
  fAParsers.Free;
  fValuesList.Free;
  fAllObjekts.Free;
  inherited;
end;

function TMissionSkriptEngine.FindObjectFromName(Text: String; All: Boolean): TBasisObject;
var
  Dummy: Integer;
  Hash : Cardinal;
begin
  result:=nil;
  Hash:=HashCommandName(Text);
  for Dummy:=0 to fObjekts.Count-1 do
  begin
    if TBasisObject(fObjekts[Dummy]).fHashValue=Hash then
    begin
      result:=TBasisObject(fObjekts[Dummy]);
      if (not All) and (not result.IsPublic) then
        result:=nil;
      exit;
    end;
  end;
end;

function TMissionSkriptEngine.IsDefine(Text: String): boolean;
var
  Dummy: Integer;
  Hash : Cardinal;
begin
  result:=false;
  Hash:=HashCommandName(Text);
  for Dummy:=0 to fObjekts.Count-1 do
  begin
    if TBasisObject(fObjekts[Dummy]).fHashValue=Hash then
    begin
      result:=true;
      exit;
    end;
  end;
end;

function TMissionSkriptEngine.LoadSkript(FileName: String): TSkript;
var
  LoadList: TStringList;
begin
  LoadList:=TStringList.Create;
  LoadList.LoadFromFile(FileName);
  result:=LoadSkript(LoadList);
  FreeAndNil(TObject(LoadList));
end;

function TMissionSkriptEngine.LoadSkript(Strings: TStrings): TSkript;
begin
  result:=TSkript.Create(Self);
  result.Parse(Strings);
end;

function TMissionSkriptEngine.NewRecord(Name: String; Publ: boolean): TRecordDiscriptor;
begin
  result:=TRecordDiscriptor.Create(Self,nil);
  result.Name:=Name;
  result.IsDefault:=true;
  result.IsPublic:=Publ;
  fObjekts.Add(result);
end;


{ TOwnerFunction }

procedure TOwnerFunction.AddVariable(Parser: TLineParser);
var
  Index    : Integer;
  Command  : TCommand;
begin
  Index:=length(fVariables);
  SetLength(fVariables,Index+1);
  fVariables[Index]:=TVariable.Create(fEngine,fSkript);
  fVariables[Index].ReadLine(Parser,Self);
  fVariables[Index].LineDeklaration:=ParsingLine;
  if fVariables[Index].ValueType<>vtRecord then
  begin
    Command:=TCommand.Create(fSkript,Self);
    Command.Line:=ParsingLine;
    Command.InitVariable(fVariables[Index]);
    fList.Add(Command);
  end;
end;

constructor TOwnerFunction.Create(Engine: TMissionSkriptEngine; Skript: TSkript);
begin
  inherited;
  fList:=TList.Create;
  fObjektType:=otOwnerFunction;
  fPublic:=true;
  fDefault:=false;
end;

destructor TOwnerFunction.destroy;
begin
//  Clear;
  FreeAndNil(TObject(fList));
  inherited;
end;

function TOwnerFunction.FindObject(Text: String): TBasisObject;
var
  Dummy: integer;
begin
  // Variablen pr�fen
  for Dummy:=0 to high(fVariables) do
  begin
    if StrIComp(PChar(fVariables[Dummy].Name),PChar(Text))=0 then
    begin
      result:=fVariables[Dummy];
      exit;
    end;
  end;

  result:=fSkript.FindObjectFromName(Text);
  if result=nil then
    result:=fEngine.FindObjectFromName(Text);
end;

procedure TOwnerFunction.GeneriereFunction(List: TStringList);
var
  Parser       : TLineParser;
  FunkEnde     : boolean;
  fInVariables : boolean;
  fInBlock     : Boolean;
  fBlocks      : Integer;
begin
  fHasVariables:=false;
  fLine:=ParsingLine;
  fList:=nil;
  Parser:=TLineParser(List.Objects[0]);
  Parser.SetFunction(Self);
  Parser.NextToken;
  Parser.CheckToken(ttBezeichner);
  Name:=Parser.TokenText;
  Parser.NextToken;
  Parser.CheckToken(ttLineEnd);
  List.Delete(0);
  fList:=TList.Create;
  FunkEnde:=false;
  fInVariables:=false;
  fInBlock:=false;
  fBlocks:=0;
  while (List.Count>0) and (not FunkEnde) do
  begin
    Parser:=TLineParser(List.Objects[0]);
    ParsingLine:=Parser.SourceLine;
    Parser.SetFunction(Self);
    Parser.NextToken;
    case Parser.TokenType of
      ttBlockBegin:
      begin
        if fSkript.fSetNoCommands then
        begin
          inc(fBlocks);
          List.Delete(0);
        end
        else
        begin
          if fInBlock then
          begin
            fList.Add(InterpretiereCommand(List,fSkript,Self));
          end
          else
          begin
            fInBlock:=true;
            Parser.NextToken;
            Parser.CheckToken(ttLineEnd);
            List.Delete(0);
          end;
        end;
        fInVariables:=false;
      end;
      ttBlockEnd:
      begin
        if fSkript.fSetNoCommands then
        begin
          dec(fBlocks);
          if fBlocks=0 then
            FunkEnde:=true
          else
            List.Delete(0);
        end
        else
        begin
          if not fInBlock then
            raise EParseError.Create('[012] '+GetPosText+' ''begin'' fehlt');
          Parser.NextToken;
          Parser.CheckToken(ttLineEnd);
          FunkEnde:=true;
        end;
      end;
      ttVar:
      begin
        if fHasVariables then
          raise EParseError.Create('[013] '+GetPosText+' Variablendeklaration k�nnen nur am Anfang eines Ereignisses stehen.');
        fInVariables:=true;
        Parser.NextToken;
        Parser.CheckToken(ttLineEnd);
        List.Delete(0);
      end;
      ttBei:
      begin
        if fSkript.fSetNoCommands then
        begin
          Parser.Restart;
          exit;
        end;
      end;
      else
      begin
        if fInVariables then
        begin
          AddVariable(Parser);
          List.Delete(0);
        end
        else
        begin
          if fSkript.fSetNoCommands then
            List.Delete(0)
          else
            fList.Add(InterpretiereCommand(List,fSkript,Self));
        end;
      end;
    end;
  end;
  if (List.Count=0) then
    raise Exception.Create('[014] '+GetPosText+' end erwartet, aber Dateiende erreicht');
  fEndLine:=ParsingLine;
end;

function TOwnerFunction.GetFirstCommand: TCommand;
begin
  result:=nil;
  if fList.Count=0 then exit;
  result:=TCommand(fList[0]);
end;

function TOwnerFunction.GetVarCount: Integer;
begin
  result:=Length(fVariables);
end;

function TOwnerFunction.GetVariable(Index: Integer): TVariable;
begin
  if (Index>=0) and (Index<VarCount) then
    result:=fVariables[Index]
  else
    raise Exception.Create('[015] '+GetPosText+' Ung�ltige Variable');
end;

function TOwnerFunction.IsCommand(Line: Integer): boolean;
var
  Dummy: Integer;
begin
  result:=false;
  for Dummy:=0 to fList.Count-1 do
  begin
    if TCommand(fList[Dummy]).IsCommand(Line) then
    begin
      result:=true;
      exit;
    end;
  end;
end;

function TOwnerFunction.IsDefine(Text: String): Boolean;
var
  Dummy: Integer;
begin
  for Dummy:=0 to High(fVariables) do
  begin
    if StrIComp(PChar(fVariables[Dummy].Name),PChar(Text))=0 then
    begin
      result:=true;
      exit;
    end;
  end;
  result:=fEngine.IsDefine(Text);
end;

function TOwnerFunction.MakeSingleStep: TCommand;
begin
  RunningSkript:=fSkript;
  result:=nil;
  if fList.Count=0 then exit;
  result:=TCommand(fList[fLine]).MakeSingleStep;
  if result=nil then
  begin
    inc(fLine);
    if fLine<fList.Count then
      result:=TCommand(fList[fLine]).GetFirst;
  end;
  RunningSkript:=nil;
end;

procedure TOwnerFunction.NewStart;
var
  Dummy: Integer;
begin
  fLine:=0;
  for Dummy:=0 to fList.Count-1 do
  begin
    TCommand(fList[Dummy]).NewStart;
  end;
end;

procedure TOwnerFunction.Start;
var
  Dummy: integer;
begin
  RunningSkript:=fSkript;
  for Dummy:=0 to fList.Count-1 do
  begin
    TCommand(fList[Dummy]).Reset;
    TCommand(fList[Dummy]).Start;
  end;
  RunningSkript:=nil;
end;

{ TBasisObject }

constructor TBasisObject.Create(Engine: TMissionSkriptEngine; Skript: TSkript);
begin
  fEngine:=Engine;
  fDefault:=fEngine.fValuesDefault;
  fPublic:=true;
  if Skript=nil then
    fEngine.fAllObjekts.Add(Self)
  else
    Skript.AddObject(Self);
  fSkript:=Skript;
  inc(ObjektsCreate);
end;

destructor TBasisObject.Destroy;
begin
  if fSkript=nil then
    fEngine.fAllObjekts.Remove(Self)
  else
    fSkript.RemoveObject(Self);
  inc(ObjektsDestroy);
  inherited;
end;

function TBasisObject.IsCommand(Line: Integer): boolean;
begin
  result:=false;
end;

procedure TBasisObject.SetName(const Value: String);
begin
  fName := Value;
  fHashValue:=HashCommandName(Value);
end;

{ TCommand }

constructor TCommand.Create(Skript : TSkript; Func: TOwnerFunction);
begin
  fEngine:=Skript.Engine;
  fSkript:=Skript;

  fSkript.fCommands.Add(Self);
  fFunc:=Func;
  inc(CommandsCreate);
end;

destructor TCommand.destroy;
begin
  fSkript.fCommands.Remove(Self);
  inc(CommandsDestroy);
  if fParams<>nil then
    fParams.Free;
  inherited;
end;

procedure TCommand.ErstelleCommando(Parser: TLineParser);
var
  Val     : TValue;
begin
  fParams:=nil;
  Val:=nil;
  Value:=nil;
  Parser.Restart;
  Parser.SetFunction(fFunc);
  Parser.NextToken;
  Parser.CheckToken(ttBezeichner);
  Parser.CreateValue(Val,true);
  if Val.CanCall then
  begin
    Value:=Val;
    Parser.NextToken;
  end
  else
  begin
    Parser.CheckToken(ttAssign);
    fSetOp:=true;
    fEmpf:=Val;
    if fEmpf.ReadOnly then
    begin
      if fEmpf is TValueMemberAccess then
        raise EParseError.Create('[019] '+GetPosText+' Das Feld '+TValueMemberAccess(fEmpf).fMember.Name+' ist schreibgesch�tzt.')
      else
        raise EParseError.Create('[020] '+GetPosText+' Das Feld '+fEmpf.AsRecord.Name+' ist schreibgesch�tzt.');
    end;
    Parser.NextToken;
    Parser.CreateAusdruck(Value);
    // Typpr�fung
    if (fEmpf.ValueTyp=vtRecord) or (Value.ValueTyp=vtRecord) then
    begin
      CheckTyp(Value.ValueTyp,fEmpf.ValueTyp);
      if not (fEmpf.RecordTyp=Value.RecordTyp) then
        raise EParseError.Create('[021] '+GetPosText+' Inkompatible Objekttypen '+fEmpf.RecordTyp.name+' und '+Value.RecordTyp.Name);
    end;
  end;
  Parser.CheckToken(ttLineEnd);
end;

function TCommand.GetFirst: TCommand;
begin
  result:=Self;
end;

procedure TCommand.InitVariable(Variable: TVariable);
begin
  fEmpf:=TValueVar.Create(fEngine,fFunc);
  TValueVar(fEmpf).SetVariable(TVariable(Variable));
  Value:=TValueConst.Create(fEngine,fFunc);
  case Variable.ValueType of
    vtString : TValueConst(Value).AsString:=Variable.AsString;
    vtZahl   : TValueConst(Value).AsZahl:=Variable.AsZahl;
    vtBoolean: TValueConst(Value).AsBoolean:=Variable.AsBoolean;
  end;
  TValueConst(Value).ValueTyp:=Variable.ValueType;
  fSetOp:=true;
end;

function TCommand.IsCommand(Line: Integer): boolean;
begin
  result:=Line=fSLine;
end;

function TCommand.MakeSingleStep: TCommand;
begin
  Reset;
  Start;
  result:=nil;
end;

procedure TCommand.NewStart;
begin
end;

procedure TCommand.Reset;
begin
  if Value<>nil then
    Value.Reset;
  if fParams<>nil then
  begin
    fParams.Reset;
  end;
end;

procedure TCommand.Start;
begin
  ParsingLine:=fSLine;
  if fSetOp then
  begin
    Value.Reset;
    Value.Start;
    case Value.ValueTyp of
      vtString  : fEmpf.SetString(Value.AsString);
      vtZahl    : fEmpf.SetZahl(Value.AsZahl);
      vtBoolean : fEmpf.SetBoolean(Value.AsBoolean);
      vtRecord  : fEmpf.SetRecord(Value.AsRecord);
    end;
  end
  else
  begin
    Value.Reset;
    Value.Start;
{    case fObjekt.fObjektType of
      otOwnerFunction: TOwnerFunction(fObjekt).Start;
      otFunction     : TFunction(fObjekt).Start(fParams);
      otVariable     : Value.Start;  // Nur m�glich wenn es ein Objekt ist
    end;}
  end;
end;

{ TFunction }

function TFunction.AsBoolean: Boolean;
begin
  if fValue=nil then
    raise EParseError.Create('[022] '+GetPosText+' Kein R�ckgabewert der Funktion :'+Name);
  result:=fValue.AsBoolean;
end;

function TFunction.AsRecord: TRecordInstance;
begin
  if fValue=nil then
    raise EParseError.Create('[022] '+GetPosText+' Kein R�ckgabewert der Funktion :'+Name);
  result:=fValue.AsRecord;
end;

function TFunction.AsString: String;
begin
  if fValue=nil then
    raise EParseError.Create('[023] '+GetPosText+' Kein R�ckgabewert der Funktion :'+Name);
  result:=fValue.AsString;
end;

function TFunction.AsZahl: Integer;
begin
  if fValue=nil then
    raise EParseError.Create('[024] '+GetPosText+' Kein R�ckgabewert der Funktion :'+Name);
  result:=fValue.AsZahl;
end;

constructor TFunction.Create(Engine: TMissionSkriptEngine; Skript: TSkript);
begin
  fObjektType:=otFunction;
  fValue:=nil;
  inherited;
end;

function TFunction.GetDeklaration: String;
begin
  result:=Name+'|';
  if fParamInfo<>EmptyStr then
    result:=result+'('+ParamInfo+') ';
  case Return of
    vtNone   : ;
    vtRecord : result:=Result+': '+RecordTyp.Name;
    else
      result:=Result+': '+NameOfType(Return);
  end;
  result:=Trim(Result);
end;

function TFunction.GetNoType: String;
begin
  if fValue=nil then
    raise EParseError.Create('[025] '+GetPosText+' Kein R�ckgabewert der Funktion :'+Name);
  result:=fValue.GetNoType;
end;

procedure TFunction.SetHasReturn(const Value: TValueType);
begin
  fValue:=nil;
  fReturn := Value;
  if Value<>vtNone then
  begin
    fValue:=TValueConst.Create(fEngine,nil);
    fValue.ValueTyp:=Value;
    if Value=vtRecord then
      fValue.RecordTyp:=RecordTyp;
  end;
end;

procedure TFunction.SetMethod(Typ: TRecordDiscriptor);
begin
  fExternalTyp:=Typ;
end;

procedure TFunction.SetName(const Value: String);
var
  Name: String;
begin
  if Pos('|',Value)>0 then
  begin
    Name:=GetShortHint(Value);
    fParamInfo:=GetLongHint(Value);
  end
  else
    Name:=Value;
  inherited SetName(Name);
end;

procedure TFunction.SetRecordTyp(const Value: TRecordDiscriptor);
begin
  if Value=nil then exit;
  fRecordTyp := Value;
  fReturn := vtRecord;
  if fValue<>nil then
    fValue.RecordTyp:=Value;
end;

procedure TFunction.Start(Skript: TSkript; ParamList: TParameterList);
begin
  ParamList.Reset;
  if fEngine.fParsing then
    exit;
    
  if (fExternalTyp=nil)  then
  begin
    if Assigned(fEngine.OnCallExternal) then
      fEngine.OnCallExternal(RunningSkript,fName,ParamList,fValue)
    else
      raise ERunTimeError.Create('[026] '+GetPosText+' '+'Funktion '+fName+' konnte nicht gestartet werden.'#13#10'Keine Verarbeitungsprozedur gesetzt.');
  end
  else
  begin
    if Assigned(fExternalTyp.OnCallMethod) then
      fExternalTyp.OnCallMethod(RunningSkript,fName,ParamList,fValue)
    else
      raise ERunTimeError.Create('[026] '+GetPosText+' '+'Methode '+fExternalTyp.Name+'.'+fName+' konnte nicht gestartet werden.'#13#10'Keine Verarbeitungsprozedur gesetzt.');
  end;
  if fReturn<>vtNone then
  begin
    CheckTyp(fValue.ValueTyp,fReturn);
    if fReturn=vtRecord then
    begin
      if not (fValue.GetRecord.RecordTyp=fRecordTyp) then
        raise Exception.Create('[026] '+GetPosText+' R�ckgabe der Funktion '+Name+' entspricht nicht dem vereinbartem Typ');
    end;
  end;
end;

{ TVariable }

procedure TVariable.AssignRecord(Instance: TRecordInstance);
begin
  CheckTyp(vtRecord,ValueType);
  if Instance.fDiscriptor<>AsRecord.fDiscriptor then
    raise Exception.Create('[027] '+GetPosText+' Inkompatible Objekttypen '+Instance.fDiscriptor.name+' und '+AsRecord.fDiscriptor.Name);
  AsRecord.Assign(Instance);
end;

procedure TVariable.Change;
begin
  if fEngine=nil then exit;
  if Assigned(fEngine.OnChangeVar) then
    fEngine.OnChangeVar(Self);
end;

constructor TVariable.Create(Engine: TMissionSkriptEngine; Skript: TSkript);
begin
  inherited;
  fObjektType:=otVariable;
end;

function TVariable.GetBoolean: Boolean;
begin
  CheckTyp(vtBoolean,ValueType);
  result:=fBoolean;
end;

function TVariable.GetNoType: String;
begin
  case fValueType of
    vtString          : result:=fText;
    vtZahl            : result:=IntToStr(fInteger);
    vtBoolean         : result:=BooleanToString(fBoolean);
    vtRecord          : result:='No Type bei Records wird nicht unterst�tzt';
  end;
end;

function TVariable.GetRecord: TRecordInstance;
begin
  CheckTyp(vtRecord,ValueType);
  result:=fInstance;
end;

function TVariable.GetString: String;
begin
  CheckTyp(vtString,ValueType);
  result:=fText;
end;

function TVariable.GetZahl: Integer;
begin
  CheckTyp(vtZahl,ValueType);
  result:=fInteger;
end;

procedure TVariable.Protect(Protect: Boolean);
begin
  fProtected:=Protect;
  ReadOnly:=Protect;
end;

procedure TVariable.ReadLine(Parser: TLineParser;Func: TOwnerFunction);
var
  Exist      : boolean;
  Typ        : String;
  Obj        : TBasisObject;
  Instance   : TRecordInstance;
  Val        : TValue;
begin
  Parser.SetFunction(Func);
  Parser.CheckToken(ttBezeichner);
  if Func=nil then
    Exist:=fEngine.IsDefine(Parser.TokenText)
  else
    Exist:=Func.IsDefine(Parser.TokenText);
  if Exist then
  begin
    raise EParseError.Create('[028] '+GetPosText+' Bezeichner redefiniert: '+Parser.TokenText);
  end;
  Name:=Parser.TokenText;
  Parser.NextToken;
  if Parser.TokenType=ttEqual then
  begin
    Parser.NextToken;
    Parser.CreateAusdruck(Val);
    if not Val.IsConstant then
      raise EParseError.Create('[002] '+GetPosText+' Konstanter Ausdruck erwartet');
    case Val.ValueTyp of
      vtString   : AsString:=Val.AsString;
      vtZahl     : AsZahl:=Val.AsZahl;
      vtBoolean  : AsBoolean:=Val.AsBoolean;
      else
        raise EParseError.Create('[001] '+GetPosText+' Konstanter Ausdruck erwartet');
    end;
  end
  else if Parser.TokenType=ttDoublePoint then
  begin
    Parser.NextToken;
    Parser.CheckToken(ttBezeichner);
    Typ:=Parser.TokenText;
    if Func<>nil then
      Obj:=Func.FindObject(Typ)
    else
      Obj:=fEngine.FindObjectFromName(Typ);
    if (Obj=nil) or not (Obj is TRecordDiscriptor) then
      raise EParseError.Create('[029] '+GetPosText+' '+Typ+' ist kein g�ltiger Typenbezeichner');
    Instance:=TRecordInstance.Create(fEngine,fSkript);
    Instance.RecordTyp:=TRecordDiscriptor(Obj);
    Instance.Name:=Name;
    AsRecord:=Instance;
    Parser.NextToken;
    if Parser.TokenType=ttEqual then
    begin
      Parser.NextToken;
      Parser.CheckToken(ttString);
      Instance.SetSavedString(Parser.TokenText);
      Parser.NextToken;
    end;
  end;
  Parser.CheckToken(ttLineEnd);
end;

procedure TVariable.SetBoolean(const Value: Boolean);
begin
  if fReadOnly and (not fProtected) then
    raise ERunTimeError.Create('[030] '+GetPosText+' Variable '+fName+' kann kein Wert zugewiesen werden');
  fValueType:=vtBoolean;
  fBoolean:=Value;
  Change;
end;

procedure TVariable.SetRecord(const Value: TRecordInstance);
begin
  if fReadOnly and (not fProtected) then
    raise ERunTimeError.Create('[031] '+GetPosText+' Variable '+fName+' kann kein Wert zugewiesen werden');
  fValueType:=vtRecord;
  fInstance:=Value;
  Change;
end;

procedure TVariable.SetString(const Value: String);
begin
  if fReadOnly and (not fProtected) then
    raise ERunTimeError.Create('[031] '+GetPosText+' Variable '+fName+' kann kein Wert zugewiesen werden');
  fValueType:=vtString;
  fText:=Value;
  Change;
end;

procedure TVariable.SetZahl(const Value: Integer);
begin
  if fReadOnly and (not fProtected) then
    raise ERunTimeError.Create('[032] '+GetPosText+' Variable '+fName+' kann kein Wert zugewiesen werden');
  fValueType:=vtZahl;
  fInteger:=Value;
  Change;
end;

{ TValue }

constructor TValue.Create(Engine: TMissionSkriptEngine;Func: TOwnerFunction);
begin
  fEngine:=Engine;
  fFunc:=Func;
  fDiscriptor:=nil;
  inc(ValuesCreate);
  IsDefault:=Engine.fValuesDefault;
  if Engine<>nil then
    Engine.fValuesList.Add(Self);

  SetSkript(ParsingSkript);
end;

function TValue.GetBoolean: boolean;
begin
  CheckTyp(vtBoolean,ValueTyp);
end;

function TValue.GetRecord: TRecordInstance;
begin
  CheckTyp(vtRecord,ValueTyp);
end;

function TValue.GetNoType: String;
begin
  case GetValueType of
    vtString : result:=AsString;
    vtZahl   : result:=IntToStr(AsZahl);
    vtBoolean: result:=BooleanToString(AsBoolean);
  end;
end;

function TValue.GetString: String;
begin
  CheckTyp(vtString,ValueTyp);
end;

function TValue.GetValueType: TValueType;
begin
  Result := fValueType;
end;

function TValue.GetZahl: Integer;
begin
  CheckTyp(vtZahl,ValueTyp);
end;

procedure TValue.Reset;
begin
end;

procedure TValue.SetBoolean(const Value: boolean);
begin
  fValueType:=vtBoolean;
end;

procedure TValue.SetRecord(const Value: TRecordInstance);
begin
  fValueType:=vtRecord;
end;

procedure TValue.SetString(const Value: String);
begin
  fValueType:=vtString;
end;

procedure TValue.SetZahl(const Value: Integer);
begin
  fValueType:=vtZahl;
end;

function TValue.GetDiscriptor: TRecordDiscriptor;
begin
  result:=fDiscriptor;
end;

procedure TValue.SetDiscriptor(const Value: TRecordDiscriptor);
begin
  fValueType:=vtRecord;
  fDiscriptor:=Value;
end;

procedure TValue.Start;
begin
end;

function TValue.GetReadOnly: Boolean;
begin
  result:=false;
end;

destructor TValue.Destroy;
begin
  if fEngine<>nil then
  begin
    fEngine.fValuesList.Remove(Self);
    fEngine:=nil;
  end;
  if fSkript<>nil then
    fSkript.RemoveObject(Self);
  inc(ValuesDestroy);
  inherited;
end;

function TValue.GetKategorie: String;
begin
  result:='';
end;

function TValue.IsConstant: Boolean;
begin
  result:=false;
end;

function TValue.GetCanCall: Boolean;
begin
  result:=false;
end;

procedure TValue.SetSkript(Skript: TSkript);
begin
  if fSkript<>nil then
    fSkript.RemoveObject(Self);

  fSkript:=Skript;
  
  if fSkript<>nil then
    fSkript.AddObject(Self);
end;

{ TValueVar }

constructor TValueVar.Create(Engine: TMissionSkriptEngine;
  Func: TOwnerFunction);
begin
  inherited;
  fProtected:=false;
end;

function TValueVar.GetBoolean: boolean;
begin
  result:=fVaria.GetBoolean;
end;

function TValueVar.GetReadOnly: Boolean;
begin
  result:=fVaria.ReadOnly;
end;

function TValueVar.GetRecord: TRecordInstance;
begin
  result:=fVaria.AsRecord;
end;

function TValueVar.GetString: String;
begin
  result:=fVaria.AsString;
end;

function TValueVar.GetValueType: TValueType;
begin
  result:=fVaria.ValueType;
end;

function TValueVar.GetZahl: Integer;
begin
  result:=fVaria.AsZahl;
end;

function TValueVar.IsConstant: Boolean;
begin
  result:=fVaria.ReadOnly and (not fVaria.IsProtect);
end;

procedure TValueVar.Protect(Protect: Boolean);
begin
  fVaria.Protect(Protect);
end;

procedure TValueVar.SetBoolean(const Value: boolean);
begin
  fVaria.SetBoolean(Value);
end;

procedure TValueVar.SetRecord(const Value: TRecordInstance);
begin
  fVaria.AssignRecord(Value);
end;

procedure TValueVar.SetString(const Value: String);
begin
  fVaria.SetString(Value);
end;

procedure TValueVar.SetVariable(Varia: TVariable);
begin
  fVaria:=Varia;
  if Varia.ValueType=vtRecord then
    RecordTyp:=Varia.AsRecord.RecordTyp;
end;

procedure TValueVar.SetZahl(const Value: Integer);
begin
  fVaria.SetZahl(Value);
end;

{ TValueConst }

function TValueConst.GetBoolean: boolean;
begin
  CheckTyp(fValueType,vtBoolean);
  result:=fBoolean;
end;

function TValueConst.GetReadOnly: Boolean;
begin
  result:=true;
end;

function TValueConst.GetRecord: TRecordInstance;
begin
  CheckTyp(fValueType,vtRecord);
  result:=fInstance;
end;

function TValueConst.GetString: String;
begin
  CheckTyp(fValueType,vtString);
  result:=fText;
end;                      

function TValueConst.GetZahl: Integer;
begin
  CheckTyp(fValueType,vtZahl);
  result:=fZahl;
end;

function TValueConst.IsConstant: Boolean;
begin
  result:=true;
end;

procedure TValueConst.SetBoolean(const Value: boolean);
begin
  fValueType:=vtBoolean;
  fBoolean:=Value;
end;

procedure TValueConst.SetDiscriptor(const Value: TRecordDiscriptor);
begin
  inherited;
  if fInstance<>nil then
  begin
    fInstance.Free;
    fInstance:=nil;
  end;
  if Value<>nil then
  begin
    if fFunc<>nil then
      fInstance:=TRecordInstance.Create(fEngine,fFunc.fSkript)
    else
      fInstance:=TRecordInstance.Create(fEngine,nil);

    fInstance.SetDiscriptor(Value);
  end;
end;

procedure TValueConst.SetRecord(const Value: TRecordInstance);
begin
  fValueType:=vtRecord;
  fInstance:=Value;
end;

procedure TValueConst.SetString(const Value: String);
begin
  fValueType:=vtString;
  fText:=Value;
end;

procedure TValueConst.SetZahl(const Value: Integer);
begin
  fValueType:=vtZahl;
  fZahl:=Value;
end;

{ TValueAusdruck }

function TValueAusdruck.GetBoolean: boolean;
begin
  Start;
  CheckTyp(fValueType,vtBoolean);
  result:=fBoolean;
end;

function TValueAusdruck.GetNoType: String;
begin
  Start;
  case fValueType of
    vtString  : result:=fText;
    vtZahl    : result:=IntToStr(fInteger);
    vtBoolean : result:=BooleanToString(fBoolean);
    vtRecord  :
      raise Exception.Create('[034] '+GetPosText+' Notype bei Records nicht erlaubt');
  end;
end;

function TValueAusdruck.GetRecord: TRecordInstance;
begin
  Start;
  CheckTyp(fValueType,vtRecord);
  result:=fRecord;
end;

function TValueAusdruck.GetString: String;
begin
  Start;
  CheckTyp(fValueType,vtString);
  result:=fText;
end;

function TValueAusdruck.GetZahl: Integer;
begin
  Start;
  CheckTyp(fValueType,vtZahl);
  result:=fInteger;
end;

function TValueAusdruck.IsConstant: Boolean;
var
  Dummy: Integer;
begin
  result:=true;
  for Dummy:=0 to High(fParser.fValues) do
  begin
    if not fParser.fValues[Dummy].Value.IsConstant then
    begin
      result:=false;
      exit;
    end;
  end;
end;

procedure TValueAusdruck.Reset;
begin
  fWasStart:=false;
end;

procedure TValueAusdruck.SetBoolean(const Value: boolean);
begin
  raise ERunTimeError.Create('[035] '+GetPosText+' �ndern der Werte nicht erlaubt');
end;

procedure TValueAusdruck.SetFunc(Func: TOwnerFunction);
begin
  fFunc:=Func;
end;

procedure TValueAusdruck.SetParser(Parser: TAusdruckParser);
begin
  fParser:=Parser;
end;

procedure TValueAusdruck.SetRecord(const Value: TRecordInstance);
begin
  raise ERunTimeError.Create('[036] '+GetPosText+' �ndern der Werte nicht erlaubt');
end;

procedure TValueAusdruck.SetString(const Value: String);
begin
  raise ERunTimeError.Create('[037] '+GetPosText+' �ndern der Werte nicht erlaubt');
end;

procedure TValueAusdruck.SetZahl(const Value: Integer);
begin
  raise ERunTimeError.Create('[038] '+GetPosText+' �ndern der Werte nicht erlaubt');
end;

procedure TValueAusdruck.Start;
var
  Val       : TValue;
  Operation : TOperation;
begin
  if fWasStart then exit;
  fParser.SetToStartPos;
  fWasStart:=true;
  ValueTyp:=vtNone;
  ParsingLine:=fParser.SourceLine;
  fParser.NextToken;
  repeat
    Operation:=fParser.Operator;
    Val:=fParser.Value;
    Val.Reset;
    Val.Start;
    case Operation of
      opNone        :
        begin
          case Val.ValueTyp of
            vtString  : begin fText:=Val.AsString    ;ValueTyp:=vtString; end;
            vtZahl    : begin fInteger:=Val.AsZahl   ;ValueTyp:=vtZahl; end;
            vtBoolean : begin fBoolean:=Val.AsBoolean;ValueTyp:=vtBoolean; end;
            vtRecord  : begin fRecord:=Val.AsRecord  ;ValueTyp:=vtRecord; end;
          end;
        end;
{      opConcat      :
        begin
          if ValueTyp<>vtRecord then
          begin
            fText:=GetNoType+val.GetNoType;
            ValueTyp:=vtString;
          end
          else
            raise ETypeMisMatch.Create('[039] '+GetPosText+' Objekte sind bei Stringverkn�pfungen nicht erlaubt.');
        end;}
      opAdd         :
      begin
        if (ValueTyp=vtZahl) and (Val.ValueTyp=vtZahl) then
          fInteger:=fInteger+val.AsZahl
        else
        begin
          if (ValueTyp<>vtRecord) and (Val.ValueTyp<>vtRecord) then
          begin
            fText:=GetNoType+val.GetNoType;
            ValueTyp:=vtString;
          end
          else
            raise ETypeMisMatch.Create('[039] '+GetPosText+' Objekte sind bei Stringverkn�pfungen nicht erlaubt.');
        end;
      end;
      opSubtract    : begin CheckTyp(vtZahl,ValueTyp);fInteger:=fInteger-val.AsZahl; end;
      opMultiply    : begin CheckTyp(vtZahl,ValueTyp);fInteger:=fInteger*val.AsZahl; end;
      opDivision    : begin CheckTyp(vtZahl,ValueTyp);fInteger:=fInteger div val.AsZahl; end;
      opEqual..opHigher,opGreaterEqual..opGreaterSmaller:
        begin
          fBoolean:=Compare(Self,val,Operation);
          ValueTyp:=vtBoolean;
        end;
      opAnd         : begin CheckTyp(vtboolean,ValueTyp);fBoolean:=fBoolean and val.AsBoolean; end;
      opOr          : begin CheckTyp(vtboolean,ValueTyp);fBoolean:=fBoolean or val.AsBoolean; end;
      opMinus       : begin fInteger:=-Val.AsZahl;ValueTyp:=vtZahl;end;
      opNot         : begin fBoolean:=not Val.AsBoolean;ValueTyp:=vtBoolean;end;
    end;
  until (not fParser.NextToken);
end;

{ TValueFunc }

function TValueFunc.GetBoolean: boolean;
begin
  Start;
  CheckTyp(fValueType,vtBoolean);
  result:=fFunction.AsBoolean;
end;

function TValueFunc.GetCanCall: Boolean;
begin
  result:=true;
end;

function TValueFunc.GetNoType: String;
begin
  Start;
  result:=fFunction.GetNoType;
end;

function TValueFunc.GetRecord: TRecordInstance;
begin
  Start;
  CheckTyp(fValueType,vtRecord);
  result:=fFunction.AsRecord;
end;

function TValueFunc.GetString: String;
begin
  Start;
  CheckTyp(fValueType,vtString);
  result:=fFunction.AsString;
end;

function TValueFunc.GetValueType: TValueType;
begin
  result:=fFunction.fReturn;
end;

function TValueFunc.GetZahl: Integer;
begin
  Start;
  CheckTyp(fValueType,vtZahl);
  result:=fFunction.AsZahl;
end;

procedure TValueFunc.Reset;
var
  Dummy: Integer;
begin
  fWasStart:=false;
  fParamList.Reset;
end;

procedure TValueFunc.SetBoolean(const Value: boolean);
begin
  raise ERunTimeError.Create('[042] '+GetPosText+' �ndern der Werte nicht erlaubt');
end;

procedure TValueFunc.SetFunction(Funct: TFunction; Params: TParameterList);
begin
  fFunction:=Funct;
  fParamList:=Params;
  fValueType:=Funct.Return;
  if Funct.Return=vtRecord then
    RecordTyp:=Funct.RecordTyp;
end;

procedure TValueFunc.SetString(const Value: String);
begin
  raise ERunTimeError.Create('[043] '+GetPosText+' �ndern der Werte nicht erlaubt');
end;

procedure TValueFunc.SetZahl(const Value: Integer);
begin
  raise ERunTimeError.Create('[044] '+GetPosText+' �ndern der Werte nicht erlaubt');
end;

procedure TValueFunc.Start;
begin
  if fWasStart then exit;
  Assert(fFunc<>nil);
  fFunction.Start(fFunc.fSkript,fParamList);
  fWasStart:=true;
end;

{ TCommandBlock }

procedure TForBlock.ErstelleListe(List: TStringList);
var
  Parser  : TLineParser;
  Value   : TValue;
begin
  Parser:=TLineParser(List.Objects[0]);
  ParsingLine:=Parser.SourceLine;
  Line:=Parser.SourceLine;
  Parser.SetFunction(fFunc);
  Parser.NextToken;
  Parser.CreateValue(Value);
  if (not (Value is TValueVar)) or Value.ReadOnly or (Value.ValueTyp=vtRecord) then
  begin
    raise EParseError.Create('[045] '+GetPosText+' einfache Lokale Variable erwartet');
  end;
  fVar:=TValueVar(Value);
  Parser.CheckToken(ttAssign);
  Parser.NextToken;
  Parser.CreateAusdruck(Value);
  if not (Value.ValueTyp in [vtNone,vtZahl]) then
    CheckTyp(Value.ValueTyp,vtZahl);
  fFrom:=Value;
  if Parser.TokenType=ttTo then
    fDown:=false
  else if Parser.TokenType=ttDownTo then
    fDown:=true
  else
    raise EParseError.Create('[045] '+GetPosText+' to oder downto erwartet, aber '''+Parser.TokenText+''' gefunden.');
  Parser.NextToken;
  Parser.CreateAusdruck(Value);
  if not (Value.ValueTyp in [vtNone,vtZahl]) then
    CheckTyp(Value.ValueTyp,vtZahl);
  fTo:=Value;
  Parser.CheckToken(ttDo);
  Parser.NextToken;
  Parser.CheckToken(ttLineEnd);
  List.Delete(0);
  fVar.Protect(true);
  fCommand:=InterpretiereCommand(List,fSkript,fFunc);
  fVar.Protect(false);
  if (List.Count=0) then
    raise EParseError.Create('[045] '+GetPosText+' ''end'' erwartet, aber Dateiende erreicht');
  ParsingLine:=TLineParser(List.Objects[0]).SourceLine;
end;

function TForBlock.IsCommand(Line: Integer): boolean;
begin
  if Self.Line=Line then
  begin
    result:=true;
    exit;
  end;
  result:=fCommand.IsCommand(Line);
end;

function TForBlock.MakeSingleStep: TCommand;
begin
  if fCount=-1 then
  begin
    ParsingLine:=fSLine;
    fFrom.Start;
    fTo.Start;
    fVar.AsZahl:=fFrom.AsZahl;
    fVar.Protect(true);
    if fDown then
      fCount:=fFrom.AsZahl-fTo.AsZahl
    else
      fCount:=fTo.AsZahl-fFrom.AsZahl;
    if fCount>=0 then
      result:=fCommand.GetFirst
    else
      result:=nil;
    exit;
  end;

  result:=fCommand.MakeSingleStep;
  if result=nil then
  begin
    dec(fCount);
    if fCount>=0 then
    begin
      if fDown then
        fVar.AsZahl:=fVar.AsZahl-1
      else
        fVar.AsZahl:=fVar.AsZahl+1;
      result:=fCommand.GetFirst;
      fCommand.Reset;
      fCommand.NewStart;
    end
    else
    begin
      result:=nil;
      fVar.Protect(false);
    end;
  end;
end;

procedure TForBlock.NewStart;
begin
  fCount:=-1;
  fCommand.NewStart;
end;

procedure TForBlock.Reset;
begin
  if fFrom<>nil then
    fFrom.Reset;
  if fTo<>nil then
    fTo.Reset;
  fCommand.Reset;
end;

procedure TForBlock.Start;
var
  Count: Integer;
begin
  ParsingLine:=fSLine;
  fFrom.Start;
  fTo.Start;
  fVar.AsZahl:=fFrom.AsZahl;
  if fDown then
    Count:=fFrom.AsZahl-fTo.AsZahl
  else
    Count:=fTo.AsZahl-fFrom.AsZahl;
  while Count>=0 do
  begin
    fCommand.Reset;
    fCommand.Start;
    if fDown then
      fVar.AsZahl:=fVar.AsZahl-1
    else
      fVar.AsZahl:=fVar.AsZahl+1;
    dec(Count);
  end;
end;

{ TIfBlock }

procedure TIfBlock.ErstelleListe(List: TStringList);
var
  Parser       : TLineParser;
  Value        : TValue;
  wasAnsonsten : boolean;
begin
  Parser:=TLineParser(List.Objects[0]);
  Line:=Parser.SourceLine;
  Parser.NextToken;
  wasAnsonsten:=false;
  while (List.Count>0) and (Parser.TokenType<>ttBlockEnd) do
  begin
    Parser:=TLineParser(List.Objects[0]);
    ParsingLine:=Parser.SourceLine;
    Parser.SetFunction(fFunc);
    if Parser.TokenType=ttansonsten then
      Parser.NextToken;
    Parser.CheckToken(ttWenn);
    if wasAnsonsten then
      raise Exception.Create('[048] '+GetPosText+' ''if'' nach einem ''else'' nicht mehr erlaubt.');
    Parser.NextToken;
    Parser.CreateAusdruck(Value);
    Value.Start;
    CheckTyp(Value.ValueTyp,vtBoolean);
    Parser.CheckToken(ttThen);
    Parser.NextToken;
    Parser.CheckToken(ttLineEnd);
    SetLength(fPaths,length(fPaths)+1);
    List.Delete(0);
    with fPaths[length(fPaths)-1] do
    begin
      Bedingung:=Value;
      Line:=ParsingLine;
      Command:=InterpretiereCommand(List,fSkript,fFunc);
    end;
    Parser:=TLineParser(List.Objects[0]);
    ParsingLine:=Parser.SourceLine;
    Parser.SetFunction(fFunc);
    Parser.NextToken;
    if Parser.TokenType=ttansonsten then
    begin
      Parser.NextToken;
      if Parser.TokenType=ttWenn then
        continue
      else if Parser.TokenType=ttLineEnd then
      begin
        List.Delete(0);
        fElse:=InterpretiereCommand(List,fSkript,fFunc);
        break;
      end
      else
      begin
        raise Exception.Create('[050] '+GetPosText+' Zeilenende erwartet, aber '''+Parser.TokenText+''' gefunden.');
      end
    end
    else
      break;
  end;
  if (List.Count=0) then
    raise Exception.Create('[051] '+GetPosText+' ''end'' erwartet, aber Dateiende erreicht');
  TLineParser(List.Objects[0]).Restart;
end;

function TIfBlock.IsCommand(Line: Integer): boolean;
var
  Dummy : Integer;

  function CheckList(List: TList): boolean;
  var
    Dummy  : Integer;
  begin
    result:=false;
    for Dummy:=0 to List.Count-1 do
    begin
      if TCommand(List[Dummy]).IsCommand(Line) then
      begin
        result:=true;
        exit;
      end;
    end;
  end;

begin
  result:=false;
  for Dummy:=0 to High(fPaths) do
  begin
    if (fPaths[Dummy].Line=Line) or fPaths[Dummy].Command.IsCommand(Line) then
    begin
      result:=true;
      exit;
    end;
  end;
  if fElse<>nil then
    result:=fElse.IsCommand(Line);
end;

function TIfBlock.MakeSingleStep: TCommand;
begin
  result:=nil;
  if fLine=-1 then
  begin
    inc(fPath);
    fDebugComm:=nil;
    if fPath<length(fPaths) then
    begin
      fPaths[fPath].Bedingung.Start;
      if fPaths[fPath].Bedingung.AsBoolean then
      begin
        fDebugComm:=fPaths[fPath].Command;
      end
      else
      begin
        if fPath=High(fPaths) then
          fDebugComm:=fElse;
      end;
    end
    else
      fDebugComm:=fElse;
    if fDebugComm<>nil then
    begin
      fLine:=0;
      result:=TCommand(fDebugComm).GetFirst;
    end
    else
    begin
      if fPath=High(fPaths) then exit;
      result:=Self;
      fSLine:=fPaths[fPath+1].Line;
    end;
  end
  else
  begin
    TCommand(fDebugComm).Reset;
    result:=TCommand(fDebugComm).MakeSingleStep;
  end;
end;

procedure TIfBlock.NewStart;
var
  Dummy   : Integer;
begin
  fLine:=-1;
  fSLine:=fPaths[0].Line;
  fPath:=-1;
  fDebugComm:=nil;
  for Dummy:=0 to High(fPaths) do
  begin
    fPaths[Dummy].Bedingung.Reset;
    fPaths[Dummy].Command.NewStart
  end;
  if fElse<>Nil then
  begin
    fElse.NewStart;
  end;
end;

procedure TIfBlock.Reset;
var
  Dummy  : Integer;
begin
  for Dummy:=0 to High(fPaths) do
  begin
    with fPaths[Dummy] do
    begin
      Bedingung.Reset;
      Command.NewStart;
    end;
  end;
  if fElse<>nil then
  begin
    fElse.Reset;
  end;
end;

procedure TIfBlock.Start;
var
  Dummy   : Integer;
  AktList : TCommand;
begin
  AktList:=nil;
  ParsingLine:=fSLine;
  for Dummy:=0 to High(fPaths) do
  begin
    ParsingLine:=fPaths[Dummy].Line;
    fPaths[Dummy].Bedingung.Start;
    if fPaths[Dummy].Bedingung.AsBoolean then
    begin
      AktList:=fPaths[Dummy].Command;
      break;
    end;
  end;
  if AktList=nil then AktList:=fElse;
  if AktList=nil then exit;
  AktList.Start;
end;

{ TLineParser }

constructor TLineParser.Create(Line: String; Engine: TMissionSkriptEngine;
  Func: TOwnerFunction);
begin
  fLine:=Line;
  fSLine:=ParsingLine;
  fFunc:=Func;
  fGesamt:=fLine;
  fEngine:=Engine;
  fTType:=ttNone;
  ParseLine;
  Parsers.Add(Self);
end;

procedure TLineParser.CreateAusdruck(var Value: TValue);
var
  fTParser    : TAusdruckParser;
  Val         : TValue;
  TempVal     : TValue;
  TempOp      : TOperation;
  TempPos     : Integer;
  AusdruckEnd : boolean;
  Operation   : TOperation;
  Pos         : Integer;
  Tokens      : Array of TAusdruck;
  Index       : Integer;
  ValueType   : TValueType;

  function GetPrior(Oper: TOperation): Integer;
  begin
    case Oper of
      opNone : result:=10;
      opAnd,opOr       : result:=8;
      opGreaterEqual..opGreaterSmaller,
      opEqual..opHigher: result:=6;
      opAdd,opSubtract : result:=2;
      opMultiply,opDivision : result:=0;
      else
        ShowMessage(GetEnumName(TypeInfo(TOperation),Integer(Oper)));
    end;
  end;

  function CreateValues(LastPrior: Integer): TValueAusdruck;
  var
    Val      : TValueAusdruck;
    TempVal  : TValue;
    fAParser : TAusdruckParser;
    Oper     : TOperation;
  begin
    Val:=TValueAusdruck.Create(fEngine,fFunc);
    fAParser:=TAusdruckParser.Create(fEngine,fFunc);
    fAParser.SourceLine:=ParsingLine;
    Val.SetParser(fAParser);
    repeat
      if Index<high(Tokens) then
      begin
        if GetPrior(Tokens[Index+1].Oper)>LastPrior then
        begin
          fAParser.AddValue(Tokens[Index].Value,Tokens[Index].Oper,Tokens[Index].XPos);
          break;
        end
        else if GetPrior(Tokens[Index+1].Oper)<LastPrior then
        begin
          Oper:=Tokens[Index].Oper;
          Tokens[Index].Oper:=opNone;
          TempVal:=CreateValues(GetPrior(Tokens[Index+1].Oper));
          Tokens[Index].Value:=TempVal;
          Tokens[Index].Oper:=Oper;
        end
        else
        begin
          fAParser.AddValue(Tokens[Index].Value,Tokens[Index].Oper,Tokens[Index].XPos);
          inc(Index);
        end;
      end
      else
      begin
        fAParser.AddValue(Tokens[Index].Value,Tokens[Index].Oper,Tokens[Index].XPos);
        break;
      end;
    until false;
    result:=Val;
  end;

begin
//  Value:=TValueAusdruck.Create(fEngine,fFunc);
//  fAParser:=TAusdruckParser.Create(fEngine,fFunc);
//  fAParser.SourceLine:=ParsingLine;
//  TValueAusdruck(Value).SetParser(fAParser);
  Operation:=opNone;
  AusdruckEnd:=false;
  Index:=0;
  ValueType:=vtNone;
  while not AusdruckEnd do
  begin
    if (TokenType=ttSubtract) or (TokenType=ttNicht) then
    begin
      TempPos:=ParsingPos;
      case TokenType of
        ttSubtract : TempOp:=opMinus;
        ttNicht    : TempOp:=opNot;
      end;
      fTParser:=TAusdruckParser.Create(fEngine,fFunc);
      fTParser.SourceLine:=ParsingLine;
      Val:=TValueAusdruck.Create(fEngine,fFunc);
      TValueAusdruck(Val).SetParser(fTParser);
      NextToken;
      CreateValue(TempVal);

      fTParser.AddValue(TempVal,TempOp,TempPos);
      Pos:=TempPos;
    end
    else
    begin
      Pos:=ParsingPos;
      CreateValue(Val);
    end;
    SetLength(Tokens,Index+1);
    Tokens[Index].Value:=Val;
    Tokens[Index].Oper:=Operation;
    Tokens[Index].XPos:=Pos;
    inc(Index);
    if Val.ValueTyp=vtRecord then
      ValueType:=vtRecord;
    if not (TokenType in Operatoren) then
      AusdruckEnd:=true
    else
    begin
      if Val.ValueTyp=vtRecord then
        raise Exception.Create('[053] '+GetPosText+' Operatoren k�nnen auf Objekte nicht angewendet werden');
      case TokenType of
        ttUnd            : Operation:=opAnd;
        ttOder           : Operation:=opOr;
        ttAdd            : Operation:=opAdd;
        ttSubtract       : Operation:=opSubtract;
        ttDivide         : Operation:=opDivision;
        ttMultiply       : Operation:=opMultiply;
        ttEqual          : Operation:=opEqual;
        ttGreater        : Operation:=opHigher;
        ttGreaterEqual   : Operation:=opGreaterEqual;
        ttSmaller        : Operation:=opSmaller;
        ttSmallerEqual   : Operation:=opSmallerEqual;
        ttGreaterSmaller : Operation:=opGreaterSmaller;
      end;
      NextToken;
    end;
  end;
  Index:=0;
  if length(Tokens)>1 then
  begin
    Value:=CreateValues(10);
    if Value.IsConstant then
    begin
      Value.Reset;
      Value.Start;
      TempVal:=TValueConst.Create(fEngine,fFunc);
      case Value.ValueTyp of
        vtString : TempVal.AsString:=Value.AsString;
        vtZahl   : TempVal.AsZahl:=Value.AsZahl;
        vtBoolean: TempVal.AsBoolean:=Value.AsBoolean;
        vtRecord :
        begin
          TempVal.AsRecord:=Value.AsRecord;
          TempVal.RecordTyp:=Value.RecordTyp;
        end;
      end;
      Value.Free;
      Value:=TempVal;
    end;
  end
  else if length(Tokens)=1 then
  begin
    if Tokens[0].Oper=opNone then
      Value:=Tokens[0].Value
    else
      Value:=CreateValues(10);
  end
  else
    Value:=nil;

end;

procedure TLineParser.Restart;
begin
  AktToken:=nil;
end;

function TLineParser.GetTokenText: String;
begin
  if AktToken=nil then
    raise Exception.Create('[054] '+GetPosText+' TokenType erst nach ReadToken oder FirstToken erlaubt');
  result:=AktToken.Text;
end;

function TLineParser.GetTokenType: TTokenType;
begin
  if AktToken=nil then
    raise Exception.Create('[055] '+GetPosText+' TokenType erst nach ReadToken oder FirstToken erlaubt');
  result:=AktToken.Typ;
end;

procedure TLineParser.NextToken;
begin
  if AktToken=nil then
    AktToken:=Addr(fTokens[0])
  else if AktToken.Typ<>ttLineEnd then
  begin
    inc(AktToken);
    if AktToken.Typ=ttInvalid then
    begin
      ParsingPos:=AktToken.XPos;
      raise EParseError.Create('[057] '+GetPosText+' Ung�ltiges Zeichen in der Datei '+fParsePos^+'($'+IntToHex(Integer(fParsePos^),2)+')');
    end;
  end;
  ParsingPos:=AktToken.XPos;
end;

procedure TLineParser.ParseLine;
begin
  fTokenCount:=0;
  if length(fLine)=0 then
  begin
    fTokens[0].Typ:=ttLineEnd;
    exit;
  end;
  fParsePos:=Addr(fLine[1]);
  while fParsePos^=' ' do
    inc(fParsePos);
  AktToken:=Addr(fTokens[0]);
  ParsingPos:=fParsePos-Addr(fLine[1])+1;
  repeat
    ReadToken;
    if fTokenCount=MAX_TOKENS then
      raise Exception.Create('[056] '+GetPosText+' Maximale Tokens pro Zeile �berschritten');
    with AktToken^ do
    begin
      Typ:=fTType;
      XPos:=ParsingPos;
      Text:=fTText;
    end;
    inc(AktToken);
    inc(fTokenCount);
    inc(ParsingPos,fTokenLen);
  until (fTType=ttLineEnd);
  Restart;
end;

procedure TLineParser.ReadToken;
var
  fBegin   : PChar;
  StrConst : boolean;
  Len      : Integer;
  AktCh    : PChar;
begin
  fTText:='';
  fBegin:=fParsePos;
  while fParsePos^=' ' do
    inc(fParsePos);
  if fParsePos^=#0 then
  begin
    fTType:=ttLineEnd;
    exit;
  end;
  inc(ParsingPos,fParsePos-fBegin);
  fTType:=IdentTable[fParsePos^];
  fTText:='';
  Len:=0;
//  LineLen:=Length(fLine);
  case fTType of
    ttDivide:
    begin
      if (fParsePos+1)^='/' then
      begin
        fTType:=ttLineEnd;
        exit;
      end;
      Len:=1;
    end;
    ttString:
    begin
      StrConst:=true;
      AktCh:=fParsePos;      
      inc(AktCh);
      while (AktCh^<>#0) and (StrConst) do
      begin
        if Char(AktCh^)='\' then
        begin
          if (AktCh^<>#0) then
          begin
            if Char((AktCh+1)^)='''' then
            begin
              fTText:=fTText+'''';
              inc(AktCh);
            end
            else if Char((AktCh+1)^)='n' then
            begin
              fTText:=fTText+#10;
              inc(AktCh);
            end
            else
              fTText:=fTText+'\';
          end
          else
            fTText:=fTText+'\';
        end
        else if Char(AktCh^)='''' then
          StrConst:=false
        else
        begin
          fTText:=fTText+Char(AktCh^);
        end;
        inc(AktCh);
      end;
      Len:=Integer(AktCh)-Integer(fParsePos);
      fParsePos:=AktCh;
    end;
    ttKlammerAuf,ttKlammerZu,ttKomma: Len:=1;
    ttBezeichner:
    begin
      AktCh:=fParsePos+1;
      while (AktCh^<>#0) and ((IdentTable[Char(AktCh^)]=ttZahl) or (IdentTable[Char(AktCh^)]=ttBezeichner)) do
        inc(AktCh);
      Len:=Integer(AktCh)-Integer(fParsePos);
    end;
    ttZahl:
    begin
      AktCh:=fParsePos+1;
      while (AktCh<>#0) and (IdentTable[Char(AktCh^)]=ttZahl) do
        inc(AktCh);
      Len:=Integer(AktCh)-Integer(fParsePos);
    end;
    ttAdd..ttEqual,ttPoint: Len:=1;
    ttGreater:
    begin
      Len:=1;
      if (fParsePos+1)^='=' then
      begin
        Len:=2;
        fTType:=ttGreaterEqual;
      end;
    end;
    ttSmaller:
    begin
      Len:=1;
      case (fParsePos+1)^ of
        '=' :
        begin
          Len:=2;
          fTType:=ttSmallerEqual;
        end;
        '>' :
        begin
          Len:=2;
          fTType:=ttGreaterSmaller;
        end;
      end;
    end;
    ttDoublePoint:
    begin
      Len:=1;
      if (fParsePos+1)^='=' then
      begin
        fTType:=ttAssign;
        Len:=2;
      end;
    end;
    ttInvalid:
    begin
      fTType:=ttInvalid;
      Len:=1;
//      raise EParseError.Create('[057] '+GetPosText+' Ung�ltiges Zeichen in der Datei '+fParsePos^+'($'+IntToHex(Integer(fParsePos^),2)+')');
    end;
  end;
  fTokenLen:=Len;
  if fTType<>ttString then
  begin
    SetLength(fTText,Len);
    AktCh:=Addr(fTText[1]);
    while Len>0 do
    begin
      AktCh^:=fParsePos^;
      inc(fParsePos);
      inc(AktCh);
      dec(Len);
    end;
  end;
  if fTType=ttBezeichner then
  begin
    case HashCommandName(fTText) of
      $1DC86E8F : fTType:=ttUnd;          { and }
      $B8959E4E : fTType:=ttOder;         { or }
      $A4EF4476 : fTType:=ttNicht;        { not }
      $9CDB1E84 : fTType:=ttwahr;         { true }
      $E6BD9878 : fTType:=ttfalsch;       { false }
      $95B3FED8 : fTType:=ttBlockBegin;   { begin }
      $39A9D8DD : fTType:=ttThen;         { then }
      $F41C6C09 : fTType:=ttBlockEnd;     { end }
      $2E29382D : fTType:=ttBei;          { event }
      $52CF9E21 : fTType:=ttwenn;         { if }
      $6D2CB842 : fTType:=ttfor;          { for }
      $D9AA1BBC : fTType:=ttVar;          { var }
      $1A7FF2AE : fTType:=ttansonsten;    { else }
      $A0A70BF1 : fTType:=ttTo;           { to }
      $DB6162B4 : fTType:=ttDownTo;       { downto }
      $58E2CC71 : fTType:=ttDo;           { do }
      $2AEFDE7C : fTType:=ttWhile;        { while }
    end;

    if LowerCase(fTText)=fEngine.fSkriptBeginn then
      fTType:=ttMission
    else if LowerCase(fTText)='end'+fEngine.fSkriptBeginn then
      fTType:=ttEndMission;
  end;
end;

procedure TLineParser.SetFunction(Func: TOwnerFunction);
var
  Dummy: Integer;
begin
  fFunc:=Func;
end;

procedure TLineParser.CheckToken(NeedToken: TTokenType);
var
  Text: String;
begin
  if NeedToken<>TokenType then
  begin
    Text:=Trim(TokenText);
    raise EParseError.Create('[058] '+GetPosText+' '+NameOfToken(NeedToken,'',fEngine.SkriptBeginn)+' erwartet, aber '+NameOfToken(TokenType,Text,fEngine.SkriptBeginn)+' gefunden')
  end;
end;

procedure TLineParser.CreateValue(var Value: TValue; CanNoType: Boolean);
Var
  Varia       : TBasisObject;
  fParams     : TParameterList;
  tmp         : TValue;
  Start       : Integer;
begin
  Start:=ParsingPos;
  Value:=nil;
  if TokenType in [ttString,ttZahl,ttWahr,ttFalsch] then
  begin
    Value:=TValueConst.Create(fEngine,fFunc);
    case TokenType of
      ttString        : TValueConst(Value).AsString:=TokenText;
      ttZahl          : TValueConst(Value).AsZahl:=StrToInt(TokenText);
      ttWahr,ttFalsch : TValueConst(Value).AsBoolean:=StringToBoolean(TokenText);
    end;
    NextToken;
    exit;
  end
  else if TokenType=ttBezeichner then
  begin
    if fEngine=nil then
      raise EParseError.Create('[060] '+GetPosText+' Konstante erwartet, aber '''+TokenText+''' gefunden.');
    if fFunc<>nil then
      Varia:=fFunc.FindObject(TokenText)
    else
      Varia:=fEngine.FindObjectFromName(TokenText);
    if Varia=nil then
      raise EParseError.Create('[061] '+GetPosText+' Undefinierter Bezeichner: '''+TokenText+'''.');
    if Varia.ObjektType=otFunction then
    else if Varia.ObjektType=otVariable then
    begin
      if TVariable(Varia).ValueType=vtRecord then
        Value:=TVariable(Varia).AsRecord.ParseMember(Self)
      else
        NextToken;
      if Value=nil then
      begin
        Value:=TValueVar.Create(fEngine,fFunc);
        TValueVar(Value).SetVariable(TVariable(Varia));
        Value.ValueTyp:=TVariable(Varia).ValueType;
      end;
      exit;
    end
    else
      raise EParseError.Create('[062] '+GetPosText+' Ausdruck erwartet, aber '''+NameOfToken(TokenType,TokenText,fEngine.SkriptBeginn)+''' gefunden.');
    fParams:=TParameterList.Create;
    fParams.ParseParams(Self,TFunction(Varia).ParamCount);
    Value:=TValueFunc.Create(fEngine,fFunc);
    TValueFunc(Value).SetFunction(TFunction(Varia),fParams);
    if Value.ValueTyp=vtRecord then
    begin
      tmp:=Value.AsRecord.ParseMember(Self,Value);
      if tmp<>nil then
        Value:=tmp;
    end;
//    else
//      NextToken;
    if (not CanNoType) and (Value.ValueTyp=vtNone) then
    begin
      ParsingPos:=Start;
      raise EParseError.Create('[063] '+GetPosText+' Funktionen muss einen Wert zur�ckgeben');
    end;
  end
  else if TokenType=ttKlammerAuf then
  begin
    NextToken;
    CreateAusdruck(Value);
    CheckToken(ttKlammerZu);
    NextToken;
    exit;
  end
  else
  begin
    raise Exception.Create('[064] '+GetPosText+' Ausdruck erwartet, aber '+NameOfToken(TokenType,TokenText,fEngine.SkriptBeginn)+' gefunden');
  end;
end;


destructor TLineParser.Destroy;
begin
  Parsers.Remove(Self);
  inherited;
end;

{ TAusdruckParser }

procedure TAusdruckParser.AddValue(Value: TValue; Operator: TOperation;
  XPos: Integer);
var
  Index: Integer;
begin
  Index:=length(fValues);
  SetLength(fValues,Index+1);
  fValues[Index].Value:=Value;
  fValues[Index].Oper:=Operator;
  fValues[Index].XPos:=XPos;
end;

constructor TAusdruckParser.Create(Engine: TMissionSkriptEngine;
  Func: TOwnerFunction);
begin
  fEngine:=Engine;
  if fEngine<>nil then
    fEngine.fAParsers.Add(Self);
  fFunc:=Func;
  SetLength(fValues,0);
end;

destructor TAusdruckParser.Destroy;
begin
  if fEngine<>nil then
    fEngine.fAParsers.Remove(Self);
  inherited;
end;

function TAusdruckParser.NextToken: boolean;
begin
  if AktToken=Last then
  begin
    result:=false;
    ParsingPos:=AktToken.XPos;
  end
  else
  begin
    inc(AktToken);
    ParsingPos:=AktToken.XPos;
    fVal:=AktToken.Value;
    fOper:=AktToken.Oper;
    result:=true;
  end;
end;

procedure TAusdruckParser.Restart;
var
  Dummy: Integer;
begin
  AktToken:=Addr(fValues[high(fValues)]);
  for Dummy:=High(fValues) downto 0 do
  begin
    AktToken.Value.Reset;
    dec(AktToken);
  end;
end;

var
  ChDummy: Char;
  TType  : TTokenType;

procedure TAusdruckParser.SetToStartPos;
begin
  AktToken:=Addr(fValues[0]);
  Last:=Addr(fValues[high(fValues)]);
  dec(AktToken);
end;

{ TRecordDiscriptor }

procedure TRecordDiscriptor.AddEvent(Name: String; const MemberSource: TMemberSource);
var
  Member: TRecordMember;
begin
  if IsMember(Name) then
    raise Exception.Create('Feld bereits mit gleichem Hashwert vorhanden');
  SetLength(fMembers,Length(fMembers)+1);
  Member.Name:=Name;
  Member.HashValue:=HashCommandName(Name);
  Member.Typ:=mtMember;
  Member.ValueType:=vtString;
  Member.Discriptor:=nil;
  Member.ReadOnly:=false;
  Member.Source:=MemberSource;
  Member.isEvent:=true;

  fMembers[High(fMembers)]:=Member;
  NotifyInstances;
end;

procedure TRecordDiscriptor.AddInstance(Instance: TRecordInstance);
begin
  fInstances.Add(Instance);
end;

procedure TRecordDiscriptor.AddMember(Name: String; Return: TValueType; const MemberSource: TMemberSource;
  ReadOnly: Boolean; Typ: TRecordDiscriptor);
var
  Member: TRecordMember;
begin
  if IsMember(Name) then
    raise Exception.Create('Feld bereits mit gleichem Hashwert vorhanden');
  SetLength(fMembers,Length(fMembers)+1);
  Member.Name:=Name;
  Member.HashValue:=HashCommandName(Name);
  Member.Typ:=mtMember;
  Member.ValueType:=Return;
  Member.Discriptor:=Typ;
  Member.ReadOnly:=ReadOnly;
  Member.Source:=MemberSource;
  Member.isEvent:=false;

  fMembers[High(fMembers)]:=Member;
  NotifyInstances;
end;

procedure TRecordDiscriptor.AddMethod(Name: String; Params: Integer;
  Return: TValueType; Typ: TRecordDiscriptor = nil);
var
  Member: TRecordMember;
  Func  : TFunction;
  MName : String;
begin
  if IsMember(Name) then
    raise Exception.Create('Feld bereits mit gleichem Hashwert vorhanden');
  SetLength(fMembers,Length(fMembers)+1);
  if Pos('|',Name)>0 then
    MName:=GetShortHint(Name)
  else
    MName:=Name;
  Member.Name:=MName;
  Member.Typ:=mtMethod;
  Member.ValueType:=Return;
  Member.Discriptor:=Typ;
  Member.ReadOnly:=true;
  Member.HashValue:=HashCommandName(MName);
  Member.isEvent:=false;

  Func:=TFunction.Create(fEngine,nil);
  Func.Name:=Name;
  Func.ParamCount:=Params;
  Func.Return:=Return;
  Func.RecordTyp:=Typ;
  Func.IsDefault:=true;
  Func.SetMethod(Self);
  Member.Func:=Func;
  fMembers[High(fMembers)]:=Member;
  NotifyInstances;
end;

constructor TRecordDiscriptor.Create(Engine: TMissionSkriptEngine; Skript: TSkript);
begin
  inherited;
  ObjektType:=otRecord;
  fInstances:=TList.Create;
end;

destructor TRecordDiscriptor.Destroy;
begin
  while fInstances.Count>0 do
    TRecordInstance(fInstances[0]).RecordTyp:=nil;
  fInstances.Free;
  inherited;
end;

function TRecordDiscriptor.GetMemberCount: Integer;
begin
  result:=Length(fMembers);
end;

function TRecordDiscriptor.GetRecordMember(Index: Integer): TRecordMember;
begin
  result:=fMembers[Index];
end;

function TRecordDiscriptor.IsMember(Name: String): Boolean;
var
  Dummy: Integer;
  Hash : Cardinal;
begin
  result:=false;
  Hash:=HashCommandName(Name);
  for Dummy:=0 to high(fMembers) do
  begin
    if fMembers[Dummy].HashValue=Hash then
    begin
      result:=true;
      exit;
    end;
  end;
end;

procedure TRecordDiscriptor.NotifyInstances;
var
  Dummy: Integer;
begin
  for Dummy:=0 to fInstances.Count-1 do
  begin
    TRecordInstance(fInstances[Dummy]).MembersChange;
  end;
end;

procedure TRecordDiscriptor.RemoveInstance(Instance: TRecordInstance);
begin
  fInstances.Remove(Instance);
end;

{ TRecordInstance }

procedure TRecordInstance.Assign(Instance: TRecordInstance);
var
  Dummy: Integer;
begin
  if Instance.fIsOwnerData then
    ExternalData:=Instance.ExternalData
  else
    CopyMemory(ExternalData,Instance.ExternalData,fDiscriptor.ExternalSize);
    
  for Dummy:=0 to MemberCount-1 do
  begin
    if fDataField[Dummy].Instance<>nil then
    begin
      fDataField[Dummy].Instance.Assign(Instance.Members[Dummy].Instance);
    end;
  end;
end;

procedure TRecordInstance.Clear;
begin
  if fDiscriptor<>nil then
  begin
    if (fExternalData<>nil) and (not fIsOwnerData) then
    begin
      FreeMem(fExternalData);
      fExternalData:=nil;
    end;
    if not fMemberChange then
    begin
      fDiscriptor.RemoveInstance(Self);
      fDiscriptor:=nil;
    end;
{    for Dummy:=0 to high(fDataField) do
    begin
      if fDataField[Dummy].Instance<>nil then fDataField[Dummy].Instance.Free;
    end;}
    SetLength(fDataField,0);
  end;
end;

constructor TRecordInstance.Create(Engine: TMissionSkriptEngine; Skript: TSkript);
begin
  inherited;
  fObjektType:=otObject;
  fMemberChange:=false;
  fExternalData:=nil;
  fIsOwnerData:=false;
end;

destructor TRecordInstance.Destroy;
begin
  Clear;
  inherited;
end;

function TRecordInstance.GetAsString(Member: String): String;
var
  Info: TRecordFields;
begin
  Info:=GetMemberInfo(Member);
  if Info.Func=nil then
    result:=GetMemberInfo(Member).Accessor.AsString
  else
    raise EParseError.Create(Member+' ist eine Funktion');
end;

function TRecordInstance.GetAsZahl(Member: String): Integer;
var
  Info: TRecordFields;
begin
  Info:=GetMemberInfo(Member);
  if Info.Func=nil then
    result:=GetMemberInfo(Member).Accessor.AsZahl
  else
    raise EParseError.Create(Member+' ist eine Funktion');
end;

function TRecordInstance.GetMemberCount: Integer;
begin
  result:=length(fDataField);
end;

function TRecordInstance.GetMemberInfo(Member: String): TRecordFields;
var
  Hash : Cardinal;
  Dummy: Integer;
begin
  result.Name:='';
  Hash:=HashCommandName(Member);
  for Dummy:=0 to high(fDataField) do
  begin
    if fDataField[Dummy].HashValue=Hash then
    begin
      result:=fDataField[Dummy];
      exit;
    end;
  end;
  raise Exception.Create('Member '+Member+' bei '+fDiscriptor.Name+' nicht definiert');
end;

function TRecordInstance.GetMembers(Index: Integer): TRecordFields;
begin
  result:=fDataField[Index];
end;

function TRecordInstance.GetSavedString: String;
var
  Dummy : Integer;
  Poi   : PByte;
begin
  result:='';
  if ExternalData=nil then exit;
  if not Assigned(fDiscriptor.fOnNeedSaveString) then
  begin
    result:=ToHexString(ExternalData,fDiscriptor.ExternalSize);
  end
  else
    fDiscriptor.fOnNeedSaveString(fSkript,Self,result);
end;

procedure TRecordInstance.MembersChange;
begin
  fMemberChange:=true;
  SetDiscriptor(fDiscriptor);
  fMemberChange:=false;
end;

function TRecordInstance.ParseMember(Parser: TLineParser;LastValue: TValue): TValue;
var
  Info: TRecordFields;
  tmp : TValue;
  Next: TValue;
begin
  result:=nil;
  Parser.NextToken;
  Next:=nil;
  while Parser.TokenType=ttPoint do
  begin
    Parser.NextToken;
    Parser.CheckToken(ttBezeichner);
    try
      Info:=GetMemberInfo(Parser.TokenText);
    except
      raise EParseError.Create('[065] '+GetPosText+' '+'Bezeichner '+Parser.TokenText+' bei '+fDiscriptor.Name+' nicht definiert');
    end;
    if Info.Func<>nil then
    begin
      result:=TValueMemberAccess.Create(fEngine,nil);
      TValueMemberAccess(result).SetMember(Info.Name,Self);
      TValueMemberAccess(result).fParamList.ParseParams(Parser,Info.Func.ParamCount);
      TValueMemberAccess(result).SetInstanceValue(LastValue);
      if Info.Typ=vtRecord then
      begin
        tmp:=Info.Func.fValue.AsRecord.ParseMember(Parser,result);
        if tmp<>nil then
          result:=tmp;
      end;
      exit;
    end
    else if LastValue=nil then
      result:=Info.Accessor
    else
    begin
      result:=TValueMemberAccess.Create(fEngine,nil);
      TValueMemberAccess(result).SetMember(Info.Name,Self);
      TValueMemberAccess(result).SetInstanceValue(LastValue);
      Next:=result;
    end;
    if Info.Instance<>nil then
    begin
      tmp:=Info.Instance.ParseMember(Parser,Next);
      if tmp<>nil then
        result:=tmp;
    end
    else
      Parser.NextToken;
  end;
end;

procedure TRecordInstance.SetAsString(Member: String; const Value: String);
var
  Info: TRecordFields;
begin
  Info:=GetMemberInfo(Member);
  if Info.Func=nil then
    GetMemberInfo(Member).Accessor.AsString:=Value
  else
    raise EParseError.Create(Member+' ist eine Funktion');
end;

procedure TRecordInstance.SetAsZahl(Member: String; const Value: Integer);
var
  Info: TRecordFields;
begin
  Info:=GetMemberInfo(Member);
  if Info.Func=nil then
    GetMemberInfo(Member).Accessor.AsZahl:=Value
  else
    raise EParseError.Create(Member+' ist eine Funktion');
end;

procedure TRecordInstance.SetDiscriptor(const Value: TRecordDiscriptor);
var
  Dummy: Integer;
begin
  Clear;
  fDiscriptor := Value;
  if Value=nil then
    exit;
  if not fMemberChange then
    fDiscriptor.AddInstance(Self);
  SetLength(fDataField,Value.MemberCount);
  for Dummy:=0 to high(fDataField) do
  begin
    with Value.Members[Dummy] do
    begin
      fDataField[Dummy].ReadOnly:=ReadOnly;
      fDataField[Dummy].Name:=Name;
      fDataField[Dummy].HashValue:=HashValue;
      fDataField[Dummy].Typ:=ValueType;
      fDataField[Dummy].Source:=Source;
      if Typ=mtMember then
      begin
        if Discriptor<>nil then
        begin
          fDataField[Dummy].Instance:=TRecordInstance.Create(fEngine,fSkript);
          fDataField[Dummy].Instance.SetDiscriptor(Discriptor);
          fDataField[Dummy].Instance.Name:=Name;
        end;
        fDataField[Dummy].Accessor:=TValueMemberAccess.Create(fEngine,nil);
        fDataField[Dummy].Accessor.SetMember(Name,Self);
        fDataField[Dummy].Func:=nil;
      end
      else if Typ=mtMethod then
      begin
        fDataField[Dummy].Func:=Func;
      end;
    end;
  end;
  GetMem(fExternalData,fDiscriptor.ExternalSize);
  FillChar(fExternalData^,fDiscriptor.ExternalSize,#0);
  fIsOwnerData:=false;
end;

procedure TRecordInstance.SetExternalData(const Value: Pointer);
begin
  if not fIsOwnerData then
    FreeMem(fExternalData);
  if Value<>nil then
  begin
    fExternalData:=Value;
    fIsOwnerData:=true;
  end
  else
  begin
    GetMem(fExternalData,fDiscriptor.ExternalSize);
    FillChar(fExternalData^,fDiscriptor.ExternalSize,#0);
    fIsOwnerData:=false;
  end
end;

procedure TRecordInstance.SetSavedString(SavedString: String);
var
  Dummy: Integer;
  Poi  : PByte;
begin
  if not Assigned(fDiscriptor.fOnSetSaveString) then
  begin
    HexToMem(SavedString,ExternalData,fDiscriptor.ExternalSize);
  end
  else
  begin
    fDiscriptor.fOnSetSaveString(fSkript,Self,SavedString);
  end;
end;

{ TValueMemberAccess }

constructor TValueMemberAccess.Create(Engine: TMissionSkriptEngine;
  Func: TOwnerFunction);
begin
  inherited;
  fVar:=TValueConst.Create(Engine,nil);
  fInstVal:=nil;
end;

function TValueMemberAccess.GetBoolean: boolean;
begin
  Start;
  if fVar<>nil then
    CheckTyp(fVar.ValueTyp,vtBoolean)
  else
    CheckTyp(vtNone,vtBoolean);
  result:=fVar.AsBoolean;
end;

function TValueMemberAccess.GetCanCall: Boolean;
begin
  result:=fMethod;
end;

function TValueMemberAccess.GetKategorie: String;
begin
  result:=fMember.Source.ValueKat;
end;

function TValueMemberAccess.GetReadOnly: Boolean;
begin
  result:=fMember.ReadOnly;
end;

function TValueMemberAccess.GetRecord: TRecordInstance;
begin
  Start;
  if fVar<>nil then
    CheckTyp(fVar.ValueTyp,vtRecord)
  else
    CheckTyp(vtNone,vtRecord);
  result:=fVar.AsRecord;
end;

function TValueMemberAccess.GetString: String;
begin
  Start;
  if fVar<>nil then
    CheckTyp(fVar.ValueTyp,vtString)
  else
    CheckTyp(vtNone,vtString);
  result:=fVar.AsString;
end;

function TValueMemberAccess.GetValueType: TValueType;
begin
  result:=fMember.Typ;
end;

function TValueMemberAccess.GetZahl: Integer;
begin
  Start;
  if fVar<>nil then
    CheckTyp(fVar.ValueTyp,vtZahl)
  else
    CheckTyp(vtNone,vtZahl);
  result:=fVar.AsZahl;
end;

procedure TValueMemberAccess.Reset;
begin
  fWasStart:=false;
  if fVar<>nil then
    fVar.Reset;
  if fInstVal<>nil then
    fInstVal.Reset;
end;

procedure TValueMemberAccess.SetBoolean(const Value: boolean);
var
  Inst: TRecordInstance;
begin
  CheckTyp(fMember.Typ,vtBoolean);
  if fEngine.fParsing then
    exit;
    
  if fInstVal=nil then
    Inst:=fInstance
  else
  begin
    fInstVal.Reset;
    fInstVal.Start;
    Inst:=fInstVal.AsRecord;
  end;
  if fMember.Source.Typ=mstMethodCall then
  begin
    fVar.SetBoolean(Value);
    if Assigned(fInstance.RecordTyp.OnWriteMember) then
      Inst.RecordTyp.OnWriteMember(RunningSkript,Inst,fMember.Name,fVar)
    else
      raise ERunTimeError.Create('[026] '+GetPosText+' '+'Zugriff auf '+fInstance.RecordTyp.Name+'.'+fMember.Name+' fehlgeschlagen.'#13#10'Keine Verarbeitungsprozedur gesetzt.');
  end
  else
  begin
    case fMember.Source.Typ of
      mstBoolean : PBoolean(Integer(Inst.ExternalData)+fMember.Source.OffSet)^:=Value;
      else
        Assert(false,'Ung�ltiger SourceTyp f�r Booleans');
    end;
  end;
end;

procedure TValueMemberAccess.SetInstanceValue(Value: TValue);
begin
  if fMethod then
  begin
    if Value=nil then
    begin
      Value:=TValueConst.Create(fEngine,fFunc);
      Value.RecordTyp:=fInstance.RecordTyp;
      Value.AsRecord:=fInstance;
    end;
    fParamList.fList.Insert(0,Value);
    fInstVal:=Value;
  end
  else
  begin
    fInstVal:=Value;
  end;
end;

procedure TValueMemberAccess.SetMember(Member: String;
  Instance: TRecordInstance);
begin
  fMember:=Instance.GetMemberInfo(Member);
  fInstance:=Instance;
  if fMember.Func<>nil then
  begin
    fParamList:=TParameterList.Create;
    fVar:=fMember.Func.fValue;
    fMethod:=true;
    RecordTyp:=fMember.Func.RecordTyp;
  end
  else
  begin
    fMethod:=false;
    if fMember.Instance<>nil then
    begin
      fVar.AsRecord:=fMember.Instance;
      RecordTyp:=fMember.Instance.RecordTyp;
    end;
  end;
end;

procedure TValueMemberAccess.SetRecord(const Value: TRecordInstance);
var
  Inst: TRecordInstance;
begin
  CheckTyp(fMember.Typ,vtRecord);
  if fEngine.fParsing then
    exit;

  fVar.SetRecord(Value);
  if fInstVal=nil then
    Inst:=fInstance
  else
  begin
    fInstVal.Reset;
    fInstVal.Start;
    Inst:=fInstVal.AsRecord;
  end;
  if Assigned(fInstance.RecordTyp.OnWriteMember) then
    Inst.RecordTyp.OnWriteMember(RunningSkript,Inst,fMember.Name,fVar)
  else
    raise ERunTimeError.Create('[026] '+GetPosText+' '+'Zugriff auf '+fInstance.RecordTyp.Name+'.'+fMember.Name+' fehlgeschlagen.'#13#10'Keine Verarbeitungsprozedur gesetzt.');
end;

procedure TValueMemberAccess.SetString(const Value: String);
var
  Inst: TRecordInstance;
  Str : String;
begin
  CheckTyp(fMember.Typ,vtString);
  if fEngine.fParsing then
    exit;

  if fInstVal=nil then
    Inst:=fInstance
  else
  begin
    fInstVal.Reset;
    fInstVal.Start;
    Inst:=fInstVal.AsRecord;
  end;
  if fMember.Source.Typ=mstMethodCall then
  begin
    fVar.SetString(Value);
    if Assigned(fInstance.RecordTyp.OnWriteMember) then
      Inst.RecordTyp.OnWriteMember(RunningSkript,Inst,fMember.Name,fVar)
    else
      raise ERunTimeError.Create('[026] '+GetPosText+' '+'Zugriff auf '+fInstance.RecordTyp.Name+'.'+fMember.Name+' fehlgeschlagen.'#13#10'Keine Verarbeitungsprozedur gesetzt.');
  end
  else
  begin
    case fMember.Source.Typ of
      mstLongString   :
      begin
        string(Pointer(Integer(Inst.ExternalData)+fMember.Source.OffSet)^):=Value;
      end;
      mstShortString  :
      begin
        Str:=Value;
        if (Length(Str)>fMember.Source.MaxLen) then
          SetLength(Str,fMember.Source.MaxLen);
        PShortString(Integer(Inst.ExternalData)+fMember.Source.OffSet)^:=Str;
      end;
      else
        Assert(false,'Ung�ltiger SourceTyp f�r Strings');
    end;
  end;
end;

procedure TValueMemberAccess.SetZahl(const Value: Integer);
var
  Inst: TRecordInstance;
begin
  CheckTyp(fMember.Typ,vtZahl);
  if fEngine.fParsing then
    exit;

  if fInstVal=nil then
    Inst:=fInstance
  else
  begin
    fInstVal.Reset;
    fInstVal.Start;
    Inst:=fInstVal.AsRecord;
  end;
  if fMember.Source.Typ=mstMethodCall then
  begin
    fVar.SetZahl(Value);
    if Assigned(fInstance.RecordTyp.OnWriteMember) then
      Inst.RecordTyp.OnWriteMember(RunningSkript,Inst,fMember.Name,fVar)
    else
      raise ERunTimeError.Create('[026] '+GetPosText+' '+'Zugriff auf '+fInstance.RecordTyp.Name+'.'+fMember.Name+' fehlgeschlagen.'#13#10'Keine Verarbeitungsprozedur gesetzt.');
  end
  else
  begin
    case fMember.Source.Typ of
      mstByte    : PByte(Integer(Inst.ExternalData)+fMember.Source.OffSet)^:=Value;
      mstWord    : PWord(Integer(Inst.ExternalData)+fMember.Source.OffSet)^:=Value;
      mstInteger : PInteger(Integer(Inst.ExternalData)+fMember.Source.OffSet)^:=Value;
      else
        Assert(false,'Ung�ltiger SourceTyp f�r Zahlwerte');
    end;
  end;
end;

procedure TValueMemberAccess.Start;
var
  Inst  : TRecordInstance;
begin
  if fWasStart then exit;
  if fEngine.fParsing then
  begin
    if fVar<>nil then
      fVar.ValueTyp:=fMember.Typ;
    exit;
  end;

  fWasStart:=true;
  if fMethod then
    fMember.Func.Start(fSkript,fParamList)
  else
  begin
    if fMember.Instance=nil then
    begin
      if fInstVal=nil then
        Inst:=fInstance
      else
      begin
        fInstVal.Reset;
        fInstVal.Start;
        Inst:=fInstVal.AsRecord;
      end;

      if fMember.Source.Typ=mstMethodCall then
      begin
        if Assigned(fInstance.RecordTyp.OnReadMember) then
          fInstance.RecordTyp.OnReadMember(RunningSkript,Inst,fMember.Name,fVar)
        else
          raise ERunTimeError.Create('[026] '+GetPosText+' '+'Zugriff auf '+fInstance.RecordTyp.Name+'.'+fMember.Name+' fehlgeschlagen.'#13#10'Keine Verarbeitungsprozedur gesetzt.');
      end
      else
      begin
        case fMember.Source.Typ of
          mstShortString : fVar.AsString:=PShortString(Integer(Inst.ExternalData)+fMember.Source.OffSet)^;
          mstLongString  :
          begin
            if PInteger(Integer(Inst.ExternalData)+fMember.Source.OffSet)^=0 then
              fVar.AsString:=''
            else
              fVar.AsString:=string(Pointer(Integer(Inst.ExternalData)+fMember.Source.OffSet)^);
          end;
          mstByte        : fVar.AsZahl:=PByte(Integer(Inst.ExternalData)+fMember.Source.OffSet)^;
          mstWord        : fVar.AsZahl:=PWord(Integer(Inst.ExternalData)+fMember.Source.OffSet)^;
          mstInteger     : fVar.AsZahl:=PInteger(Integer(Inst.ExternalData)+fMember.Source.OffSet)^;
          mstBoolean     : fVar.AsBoolean:=PBoolean(Integer(Inst.ExternalData)+fMember.Source.OffSet)^;
        end;
      end;
    end
    else
    begin
      if fInstVal<>nil then
      begin
        fVar.AsRecord:=fInstVal.AsRecord.GetMemberInfo(fMember.Name).Instance;
      end;
    end;
  end;
end;

{ TParameterList }

constructor TParameterList.Create;
begin
  fList:=TList.Create;
end;

destructor TParameterList.Destroy;
begin
  fList.Free;
  inherited;
end;

function TParameterList.GetParamCount: Integer;
begin
  result:=fList.Count;
end;

function TParameterList.GetParameter(Index: Integer): TValue;
begin
  if (Index<0) or (Index>=fList.Count) then
    raise ERunTimeError.Create('[066] '+GetPosText+' Parameter '+IntToStr(Index)+' nicht vergeben');
  result:=TValue(fList[Index]);
end;

function TParameterList.HasParameter(Index: Integer): boolean;
begin
  result:=(Index>=0) and (Index<fList.Count);
end;

procedure TParameterList.ParseParams(Parser: TLineParser;ParamCount: Integer);
var
  Val: TValue;
begin
  fList.Clear;
  Parser.NextToken;
  if ParamCount=0 then
  begin
    if Parser.TokenType=ttKlammerAuf then
    begin
      Parser.NextToken;
      Parser.CheckToken(ttKlammerZu);
      Parser.NextToken;
    end;
    exit;
  end;
  Parser.CheckToken(ttKlammerAuf);
  while Parser.TokenType<>ttKlammerZu do
  begin
    Parser.NextToken;
    if Parser.TokenType=ttKlammerZu then break;
    Parser.CreateAusdruck(Val);
    fList.Add(Val);
    Val:=nil;
    if ((Parser.TokenType<>ttKomma) and (Parser.TokenType<>ttKlammerZu)) then
      raise EParseError.Create('[067] '+GetPosText+' '','' oder '')'' erwartet, aber '''+Parser.TokenText+''' gefunden.');
  end;
  Parser.NextToken;
  if fList.Count=ParamCount then exit;
  if fList.Count>ParamCount then
    raise EParseError.Create('[068] '+GetPosText+' Zu Viele Parameter')
  else if fList.Count<ParamCount then
    raise EParseError.Create('[069] '+GetPosText+' Zu wenig Parameter');
end;

procedure TParameterList.Reset;
var
  Dummy: Integer;
begin
  for Dummy:=0 to fList.Count-1 do
  begin
    TValue(fList[Dummy]).Reset;
  end;
end;

{ TCommandList }

destructor TCommandList.destroy;
begin
  FreeAndNil(TObject(fList));
  inherited;
end;

procedure TCommandList.ErstelleListe(List: TStringList);
var
  Parser   : TLineParser;
begin
  fList:=TList.Create;
  Parser:=TLineParser(List.Objects[0]);
  Parser.CheckToken(ttBlockBegin);
  Parser.NextToken;
  Parser.CheckToken(ttLineEnd);
  List.Delete(0);
  while (List.Count>0) do
  begin
    Parser:=TLineParser(List.Objects[0]);
    ParsingLine:=Parser.SourceLine;
    Parser.SetFunction(fFunc);
    Parser.NextToken;
    if Parser.TokenType=ttBlockEnd then
    begin
      Parser.NextToken;
      Parser.CheckToken(ttLineEnd);
      break;
    end;
    Parser.Restart;
    fList.Add(InterpretiereCommand(List,fSkript,fFunc));
  end;
  List.Delete(0);
end;

function TCommandList.GetFirst: TCommand;
begin
  if fList.Count>0 then
    result:=TCommand(fList[0]).GetFirst
  else
    result:=nil;
end;

function TCommandList.IsCommand(Line: Integer): boolean;
var
  Dummy: Integer;
begin
  result:=false;
  for Dummy:=0 to fList.Count-1 do
  begin
    if TCommand(fList[Dummy]).IsCommand(Line) then
    begin
      result:=true;
      exit;
    end;
  end;
end;

function TCommandList.MakeSingleStep: TCommand;
begin
  result:=nil;
  if fList.Count=0 then exit;
  result:=TCommand(fList[fLine]).MakeSingleStep;
  if result=nil then
  begin
    inc(fLine);
    if fLine<fList.Count then
      result:=TCommand(fList[fLine]).GetFirst;
  end;
end;

procedure TCommandList.NewStart;
var
  Dummy : Integer;
begin
  fLine:=0;
  for Dummy:=0 to fList.Count-1 do
  begin
    TCommand(fList[Dummy]).NewStart;
  end;
end;

procedure TCommandList.Reset;
var
  Dummy: Integer;
begin
  for Dummy:=0 to fList.Count-1 do
    TCommand(fList[Dummy]).Reset;
end;

procedure TCommandList.Start;
var
  Dummy: Integer;
begin
  for Dummy:=0 to fList.Count-1 do
  begin
    TCommand(fList[Dummy]).Reset;
    TCommand(fList[Dummy]).Start;
  end;
end;

{ TWhileBlock }

procedure TWhileBlock.ErstelleListe(List: TStringList);
var
  Parser  : TLineParser;
begin
  Parser:=TLineParser(List.Objects[0]);
  ParsingLine:=Parser.SourceLine;
  Line:=Parser.SourceLine;
  Parser.SetFunction(fFunc);
  Parser.NextToken;
  Parser.CreateAusdruck(fBedingung);
  if fBedingung.IsConstant then
  begin
    raise EParseError.Create('[045] '+GetPosText+' Keine Konstanten Werte erlaubt');
  end;
  Parser.CheckToken(ttDo);
  Parser.NextToken;
  Parser.CheckToken(ttLineEnd);
  List.Delete(0);
  fCommand:=InterpretiereCommand(List,fSkript,fFunc);
  if (List.Count=0) then
    raise EParseError.Create('[045] '+GetPosText+' ''end'' erwartet, aber Dateiende erreicht');
  ParsingLine:=TLineParser(List.Objects[0]).SourceLine;
end;

function TWhileBlock.IsCommand(Line: Integer): boolean;
begin
  if Self.Line=Line then
  begin
    result:=true;
    exit;
  end;
  result:=fCommand.IsCommand(Line);
end;

function TWhileBlock.MakeSingleStep: TCommand;
begin
  if not fInCommands then
  begin
    ParsingLine:=fSLine;
    fBedingung.Start;
    if fBedingung.AsBoolean then
      result:=fCommand.GetFirst
    else
      result:=nil;
    fInCommands:=true;
    exit;
  end;

  result:=fCommand.MakeSingleStep;
  if result=nil then
  begin
    fInCommands:=false;
    result:=Self;
    fBedingung.Reset;
    fCOmmand.NewStart;
  end;
end;

procedure TWhileBlock.NewStart;
begin
  fInCommands:=false;
  fCommand.NewStart;
end;

procedure TWhileBlock.Reset;
begin
  fBedingung.Reset;
  fCommand.Reset;
end;

procedure TWhileBlock.Start;
begin
  ParsingLine:=fSLine;
  fBedingung.Reset;
  fBedingung.Start;
  while fBedingung.AsBoolean do
  begin
    fCommand.Reset;
    fCommand.Start;
    fBedingung.Reset;
    fBedingung.Start;
  end;
end;

{ TSkript }

procedure TSkript.AddNamedObject(Obj: TBasisObject);
begin
  fObjekts.Add(Obj);
end;

procedure TSkript.AddObject(Obj: TObject);
begin
  fObjectList.Add(Obj);
end;

procedure TSkript.Call(Text: String);
var
  Dummy: Integer;
  Hash : Cardinal;
begin
  Hash:=HashCommandName(Text);
  for Dummy:=0 to fObjekts.Count-1 do
  begin
    if (TBasisObject(fObjekts[Dummy]).ObjektType=otOwnerFunction) and (TBasisObject(fObjekts[Dummy]).fHashValue=Hash) then
    begin
      TOwnerFunction(fObjekts[Dummy]).Start;
      exit;
    end;
  end;
end;

procedure TSkript.CheckHeadLine;
var
  Parser   : TLineParser;
begin
  while not fHasMission do
  begin
    if List.Count=0 then
      raise Exception.Create('[004] '+GetPosText+' '''+fEngine.SkriptBeginn+''' erwartet, aber Dateiende erwartet');
    Parser:=TLineParser(List.Objects[0]);
    ParsingLine:=Parser.SourceLine;
    Parser.NextToken;
    if Parser.TokenType=ttMission then
    begin
      Parser.NextToken;
      Parser.CheckToken(ttString);
      fSkriptName:=Parser.TokenText;
      Parser.NextToken;
      Parser.CheckToken(ttLineEnd);
      fHasMission:=true;
    end
    else
    begin
      raise EParseError.Create('[005] '+GetPosText+' '''+fEngine.SkriptBeginn+''' erwartet, aber '''+Parser.TokenText+''' gefunden.');
    end;
    List.Delete(0);
  end;
end;

procedure TSkript.Clear;
var
  Dummy: Integer;
begin
  List.Clear;
  while fObjectList.Count>0 do
    TObject(fObjectList[0]).Free;

  while fCommands.Count>0 do
    TObject(fCommands[0]).Free;

  fObjectList.Clear;
  fCommands.Clear;
  fObjekts.Clear;
end;

constructor TSkript.Create(Engine: TMissionSkriptEngine);
begin
  fEngine:=Engine;
  List:=TStringList.Create;
  fObjectList:=TList.Create;
  fObjekts:=TList.Create;
  fCommands:=TList.Create;
  fSetNoCommands:=false;
end;

destructor TSkript.Destroy;
begin
  Clear;

  FreeAndNil(TObject(List));
  FreeAndNil(TObject(fObjekts));
  FreeAndNil(TObject(fObjectList));
  FreeAndNil(TObject(fCommands));
  inherited;
end;

procedure TSkript.Error(Error: String);
begin
  fErrors:=true;
  fErrorMsg:=Error;
end;

function TSkript.FindObjectFromName(Text: String): TBasisObject;
var
  Dummy: Integer;
  Hash : Cardinal;
begin
  result:=nil;
  Hash:=HashCommandName(Text);
  for Dummy:=0 to fObjekts.Count-1 do
  begin
    if TBasisObject(fObjekts[Dummy]).fHashValue=Hash then
    begin
      result:=TBasisObject(fObjekts[Dummy]);
      exit;
    end;
  end;
  result:=fEngine.FindObjectFromName(Text,false);
end;

function TSkript.IsCommand(Line: Integer): boolean;
var
  Dummy: Integer;
begin
  result:=false;
  for Dummy:=0 to fObjekts.Count-1 do
  begin
    if TBasisObject(fObjekts[Dummy]).IsCommand(Line) then
    begin
      result:=true;
      exit;
    end;
  end;
end;

procedure TSkript.Parse(Strings: TStrings);
var
  Dummy: Integer;
begin
  List.Assign(Strings);
  fValuesDefault:=false;
  fHasEndMission:=false;
  fInVariables:=false;
  fHasMission:=false;
  fHasVariables:=false;
  Clear;
  List.AddStrings(Strings);

  ParsingSkript:=Self;
  fEngine.fParsing:=true;
  try
    for Dummy:=List.Count-1 downto 0 do
    begin
      ParsingLine:=Dummy+1;
      List.Objects[Dummy]:=TLineParser.Create(List[Dummy],fEngine,nil);
      if TLineParser(List.Objects[Dummy]).TokenCount<=1 then
        List.Delete(Dummy);
    end;

    if List.Count=0 then
      raise Exception.Create('[006] '+GetPosText+' ''mission'' erwartet, aber Dateiende gefunden');

    fErrors:=false;
    ParsingLine:=0;
    CheckHeadLine;
    while List.Count>0 do
    begin
      ParsingLine:=TLineParser(List.Objects[0]).SourceLine;
      ParseLine;
      if List.Count>0 then
        List.Delete(0);
    end;
    ParsingPos:=1;

    if fErrors then
      raise EParseError.Create('[Fataler Fehler] Skript enth�lt Fehler');

    if not fHasEndMission then
      raise EParseError.Create('[007] '+GetPosText+' ''endmission'' erwartet, aber Dateiende gefunden.');

    ClearParsers;
  finally
    ParsingSkript:=nil;
    fEngine.fParsing:=false;
  end;
end;

procedure TSkript.Parse(FileName: String);
var
  LoadList: TStringList;
begin
  LoadList:=TStringList.Create;
  LoadList.LoadFromFile(FileName);
  Parse(LoadList);
  FreeAndNil(TObject(LoadList));
end;

procedure TSkript.ParseLine;
var
  Parser       : TLineParser;
  Funct        : TOwnerFunction;
  Varia        : TVariable;
begin
  if List.Count=0 then
    raise EParseError.Create('[008] '+GetPosText+' ''endmission'' erwartet, aber Dateiende gefunden.');
  Parser:=TLineParser(List.Objects[0]);
  Parser.NextToken;
  if fHasEndMission then
  begin
    raise EParseError.Create('[009] '+GetPosText+' Dateiende erwartet aber '''+Parser.TokenText+''' gefunden.');
  end
  else if Parser.TokenType=ttBei then
  begin
    fHasVariables:=true;
    fInVariables:=false;
    Funct:=TOwnerFunction.Create(fEngine,Self);
    Funct.GeneriereFunction(List);
    fObjekts.Add(Funct);
    exit;
  end
  else if Parser.TokenType=ttendMission then
  begin
    Parser.NextToken;
    Parser.CheckToken(ttLineEnd);
    fHasEndMission:=true;
    Exit;
  end
  else if Parser.TokenType=ttVar then
  begin
    if fHasVariables then
      raise EParseError.Create('[010] '+GetPosText+' Variablendeklaration k�nnen nur vor dem ersten Ereigniss stehen.');
    fInVariables:=true;
    Parser.NextToken;
    Parser.CheckToken(ttLineEnd);
    exit;
  end
  else if fInVariables then
  begin
    Varia:=TVariable.Create(fEngine,Self);
    Varia.LineDeklaration:=ParsingLine;
    Varia.ReadLine(Parser,nil);
    fObjekts.Add(Varia);
  end
  else
  begin
    raise EParseError.Create('[011] '+GetPosText+' Deklaration erwartet, aber '''+Parser.TokenText+''' gefunden.');
  end;
end;

procedure TSkript.ParseOnlyDeklarations(Strings: TStrings);
begin
  fSetNoCommands:=true;
  Parse(Strings);
  fSetNoCommands:=false;
end;

procedure TSkript.RemoveObject(Obj: TObject);
begin
  fObjectList.Remove(Obj);
  fObjekts.Remove(Obj);
end;

initialization

  Parsers:=TList.Create;
  for ChDummy:=#0 to #255 do
  begin
    case ChDummy of
      'A'..'Z','a'..'z','�','�','�','�','�','�': TType:=ttBezeichner;
      ''''                                     : TType:=ttString;
      '0'..'9'                                 : TType:=ttZahl;
      ','                                      : TType:=ttKomma;
      '('                                      : TType:=ttKlammerAuf;
      ')'                                      : TType:=ttKlammerZu;
      '+'                                      : TType:=ttAdd;
      '-'                                      : TType:=ttSubtract;
      '*'                                      : TType:=ttMultiply;
      '/'                                      : TType:=ttDivide;
      '>'                                      : TType:=ttGreater;
      '<'                                      : TType:=ttSmaller;
      '='                                      : TType:=ttEqual;
      '.'                                      : TType:=ttPoint;
      ':'                                      : TType:=ttDoublePoint;
      else
        TType:=ttInvalid;
    end;
    IdentTable[ChDummy]:=TType;
  end;

finalization
  GlobalFile.Write('Values erstellt',ValuesCreate);
  GlobalFile.Write('Values zerst�rt',ValuesDestroy);
  GlobalFile.Write('Commandos erstellt',CommandsCreate);
  GlobalFile.Write('Commandos zerst�rt',CommandsDestroy);
  GlobalFile.Write('Objekte erstellt',ObjektsCreate);
  GlobalFile.Write('Objekte zerst�rt',ObjektsDestroy);
  ClearParsers;
  Parsers.Free;
end.
