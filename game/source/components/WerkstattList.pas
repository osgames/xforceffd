{********************************************************************************
*										*
* Copyright (C) 2004  X-Force Team - jim_raynor@web.de				*
*										*
* This file is part of X-Force: Fight For Destiny (X-Force).			*
*										*
* X-Force is free software; you can redistribute it and/or modify		*
* it under the terms of the GNU General Public License as published by		*
* the Free Software Foundation; either version 2 of the License, or		*
* (at your option) any later version.						*
*										*
* X-Force is distributed in the hope that it will be useful,			*
* but WITHOUT ANY WARRANTY; without even the implied warranty of		*
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the			*
* GNU General Public License for more details.					*
*										*
* You should have received a copy of the GNU General Public License		*
* along with X-Force; if not, write to the Free Software			*
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA	*
*										*
*********************************************************************************
*										*
* TWerkstattList verwaltet alles rund am die Werkstatt. Dazu z�hlen zum einen   *
* die aktuellen Herstellungsprojekte und zum anderen die eigenen Techniker und  *
* Techniker auf dem Arbeitsmarkt. Hier wird auch die �berwachung des    	*
* Arbeitsmarktes f�r Techniker geregelt.					*
*										*
********************************************************************************}

{$I ../Settings.inc}

unit WerkstattList;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  XForce_types, math, DXClass, DXDraws, KD4Utils, Defines, Loader, Blending,
  DirectDraw, NotifyList, StringConst;

const
  EVENT_ONCHANGEALPHATRONMINING  : TEventID = 0;

type
  TTechnikerArray  = Array of TTechniker;
  TProduktionArray = Array of TProduktion;

  TWerkstattList = class(TObject)
  private
    fTechniker       : TTechnikerArray;
    fKaufList        : TTechnikerArray;
    fProjekte        : TProduktionArray;

    fTime            : Integer;

    fWatchs          : TWatchMarktArray;
    fSortKauf        : Boolean;
    fNotSort         : Boolean;

    // Events
    fNotifyList      : TNotifyList;

    function GetTechniker(Index: Integer): TTechniker;
    function GetCount: Integer;
    function GetKaufCount: Integer;
    function GetKTechniker(Index: Integer): TTechniker;
    
    function GetProjektCount: Integer;
    function GetProjekt(Index: Integer): TProduktion;

    procedure Sort;
    function SortFunc(KTechniker1,KTechniker2: Integer;Typ: TFunctionType): Integer;

    procedure CheckWatchTechniker(Index: Integer);

    function GenerateTechnikerID: Cardinal;
    { Private-Deklarationen }
  protected
    function RandomTechniker(GeniusErlaubt: boolean): TTechniker;
    procedure DeleteProjekt(Projekt: Integer);

    procedure NewGameHandler(Sender: TObject);
    { Protected-Deklarationen }
  public
    constructor Create;
    destructor destroy;override;

    procedure Clear;
    procedure CreateTechniker(Anzahl: Integer);
    procedure CreateKaufList;

    procedure DrawDXTechniker(Index: integer;Surface: TDirectDrawSurface;const Mem: TDDSurfaceDesc; x,y: Integer);
    procedure DrawDXKaufTechniker(Index: integer;Surface: TDirectDrawSurface;const Mem: TDDSurfaceDesc; x,y: Integer);

    procedure SaveToStream(Stream: TStream);
    procedure LoadFromStream(Stream: TStream);

    procedure NextDay;
    procedure NextRound(Minuten: Integer);

    procedure SetTraining(Index: Integer);
    procedure ChangeTraining(Index: Integer);
    procedure SetProjekt(Techniker: Integer; Projekt: Integer);
    procedure FreeTechniker(Index: Integer);
    procedure SetMining(Index: Integer);
    procedure SetRepair(Index: Integer);

    function CheckWerkRoom(BaseID: Cardinal): String;

    function IsTechnikerFree(Index: Integer): Boolean;

    procedure BuyTechniker(Index: Integer;BasisID: Cardinal=0);
    procedure SellTechniker(Index: Integer);
    procedure DeleteTechniker(Index: Integer; FreeWohnRaum: Boolean = true);
    procedure Rename(Index: Integer;Name: String);
    function InSelectedBasis(Index: Integer): boolean;

    procedure AddProjekt(const Item: TLagerItem; Anzahl: Integer);
    procedure ChangeProjekt(ID: Cardinal;Anzahl: Integer);
    procedure CancelProjekt(Projekt: Integer;PayAlphatron: boolean; BuildNext: Boolean = false);
    function GetProjektOfID(ID: Cardinal): Integer;
    function ProjektStrength(Index: Integer): Integer;
    function PercentDone(Index: Integer): real;
    function ProjektInSelectedBasis(Index: Integer): boolean;

    function CalculateWeeklySold: Integer; overload;
    function CalculateWeeklySold(BasisID: Cardinal): Integer; overload;
    { Steuerung zur �berwachung der Liste }
    procedure CancelWatch(BasisID: Cardinal);
    procedure SetWatch(Basis: Cardinal;minStrength: Integer;WType: TWatchType);
    function GetWatch(BasisID: Cardinal;var Watch: TWatchMarktSettings): Boolean;

    function CountInBase(BasisID: Cardinal): Integer;

    property Techniker[Index: Integer]     : TTechniker read GetTechniker;default;
    property KaufTechniker[Index: Integer] : TTechniker read GetKTechniker;
    property Projekte[Index: Integer]      : TProduktion read GetProjekt;
    property Count            : Integer read GetCount;
    property KaufCount        : Integer read GetKaufCount;
    property ProjektCount     : Integer read GetProjektCount;
    property ProjektTime      : Integer read fTime write fTime;

    property NotifyList       : TNotifyList read fNotifyList;
  end;

implementation

uses
  array_utils, basis_api, savegame_api, person_api, lager_api, werkstatt_api,
  ExtRecord, ConvertRecords;

var
  TWerkstattListRecord : TExtRecordDefinition;

{ TWerkstattList }

procedure TWerkstattList.AddProjekt(const Item: TLagerItem; Anzahl: Integer);
var
  PIndex   : Integer;
  Kost     : Integer;
  Dummy    : Integer;
  Become   : Integer;
  BaseID   : Cardinal;
begin
  if not Item.Useable then
    raise Exception.Create(ST0310250002);

  if Item.AlienItem then
    raise Exception.Create(MK0504160002);

  if not Item.HerstellBar then
    raise Exception.Create(Format(ENoConstruction,[Item.Name]));

  BaseID:=basis_api_GetSelectedBasis.ID;

  for dummy:=0 to ProjektCount-1 do
  begin
    if (fProjekte[Dummy].ItemID=Item.ID) and
       (fProjekte[Dummy].BasisID=BaseID) then
    begin
      if fProjekte[Dummy].Anzahl=101 then
        raise EProjektExists.Create(Format(EEndItemInProd,[Item.Name]))
      else
        raise EProjektExists.Create(Format(EItemInProd,[fProjekte[Dummy].Anzahl,Item.Name]));
    end;
  end;

  if not basis_api_NeedLagerRaum(Item.LagerV,BaseID,false) then
    raise ENotEnoughRoom.Create(Format(ENoLagerToProd,[Item.Name]));

  Kost:=Item.ManuAlphatron;
  Become:=savegame_api_NeedAlphatron(Kost);

  SetLength(fProjekte,ProjektCount+1);
  PIndex:=ProjektCount-1;
  fProjekte[PIndex].LagerV:=Item.LagerV;
  fProjekte[PIndex].ID:=random(High(LongWord));
  fProjekte[PIndex].ItemID:=Item.ID;
  fProjekte[PIndex].Kost:=Kost;
  fProjekte[PIndex].Reserved:=Become;
  fProjekte[PIndex].Anzahl:=Anzahl;
  fProjekte[PIndex].Gesamt:=round(Item.ProdTime);
  fProjekte[PIndex].Hour:=round(Item.ProdTime);
  fProjekte[PIndex].Product:=fProjekte[PIndex].Reserved=Kost;
  fProjekte[PIndex].BasisID:=BaseID;

end;

procedure TWerkstattList.BuyTechniker(Index: Integer;BasisID: Cardinal);
begin
  basis_api_NeedWohnRaum(BasisID);

  if not savegame_api_NeedMoney(round(fKaufList[Index].Strength*TechnikerBuy),kbHK) then
  begin
    basis_api_FreeWohnRaum(BasisID);

    raise ENotEnoughMoney.Create(ENoMoney);
  end;

  SetLength(fTechniker,Count+1);
  fTechniker[high(fTechniker)]:=fKaufList[Index];
  fTechniker[high(fTechniker)].BasisID:=basis_api_GetBasisFromID(BasisID).ID;
  with fTechniker[high(fTechniker)] do
  begin
    ProjektID:=0;
    Ausbil:=false;
    AlphatronMining:=false;
    Repair:=false;
  end;

  DeleteArray(Addr(fKaufList),TypeInfo(TTechnikerArray),Index);

  Sort;
end;

procedure TWerkstattList.CancelProjekt(Projekt: Integer;PayAlphatron: boolean; BuildNext: Boolean);
var
  Dummy      : Integer;
  Zaehler    : Integer;
  ID         : Cardinal;
  NextIndex  : Integer;
  OK         : Boolean;

  procedure GotoNextProject;
  begin
    // N�chstes freies Projekt ermitteln
    NextIndex:=-1;
    repeat
      Zaehler:=(Zaehler+1) mod ProjektCount;
      if (ProjektStrength(Zaehler)=0) then
        NextIndex:=Zaehler;
    until (NextIndex<>-1) or (Zaehler=Projekt);
  end;

begin
  if (Projekt<0) or (Projekt>ProjektCount-1) then
    exit;

  if PayAlphatron then
    savegame_api_FreeAlphatron(fProjekte[Projekt].Reserved,fProjekte[Projekt].BasisID);

  basis_api_FreeLagerRaum(fProjekte[Projekt].LagerV,fProjekte[Projekt].BasisID);

  if BuildNext then
  begin
    Zaehler:=Projekt;
    ID:=fProjekte[Projekt].ID;

    GotoNextProject;

    // Forscher dem n�chsten Projekt zuweisen
    if NextIndex<>-1 then
    begin
      for Dummy:=0 to Count-1 do
      begin
        if fTechniker[Dummy].ProjektID=ID then
        begin
          OK:=false;
          while not OK do
          begin
            try
              SetProjekt(Dummy,NextIndex);
              OK:=true;
            except
              GotoNextProject;
              if NextIndex=-1 then
                break;
            end;
          end;
        end;
        if NextIndex=-1 then
          break;
      end;
    end;
  end;

  DeleteProjekt(Projekt);
end;

procedure TWerkstattList.Clear;
begin
  SetLength(fTechniker,0);
  SetLength(fKaufList,0);
  SetLength(fProjekte,0);
  SetLength(fWatchs,0);
end;

constructor TWerkstattList.Create;
begin
  fNotifyList:=TNotifyList.Create;

  randomize;
  fNotSort:=false;

  werkstatt_api_init(Self);

  savegame_api_RegisterCustomSaveHandler(SaveToStream,LoadFromStream,nil,$303974A2);
  savegame_api_RegisterNewGameHandler(NewGameHandler);
end;

procedure TWerkstattList.CreateKaufList;
var
  Dummy         : Integer;
begin
  { Aktualisiert die Liste von Forschern auf dem Arbeitsmarkt }
  { dabei werden zuf�llig neue erstellt oder gel�scht         }
  { Zuerst werden alle Forscher durchgangen und falls eine    }
  { Zufallszahl bis 100 kleiner als 15 ist wird er gel�scht   }
  Dummy:=0;
  while (Dummy<KaufCount) do
  begin
    if random(100)<=15 then
      DeleteArray(Addr(fKaufList),TypeInfo(TTechnikerArray),Dummy)
    else
      inc(Dummy);
  end;
  { Es werden f�nf Durchl�ufe get�tigt, bei jedem Durchlauf   }
  { wird eine Zufallszahl erstellt. Wenn diese kleiner als    }
  { 30 ist wird ein neuer Forscher erstellt.Es k�nnen max. 20 }
  { Forscher auf dem Arbeitsmarkt sein                        }
  for Dummy:=1 to 5 do
  begin
    if not (KaufCount=20) then
    begin
      if Random(100)<=30 then
      begin
        SetLength(fKaufList,KaufCount+1);
        fKaufList[KaufCount-1]:=RandomTechniker(false);
        CheckWatchTechniker(high(fKaufList));
      end;
    end;
  end;
  Sort;
end;

procedure TWerkstattList.CreateTechniker(Anzahl: Integer);
var
  Dummy: Integer;
  Index: Integer;
begin
  Index:=Count;
  for Dummy:=1 to Anzahl do
  begin
    basis_api_NeedWohnRaum(basis_api_GetMainBasis.ID);

    SetLength(fTechniker,Count+1);
    fTechniker[Index]:=RandomTechniker(True);
    fTechniker[Index].BasisID:=basis_api_GetMainBasis.ID;
    inc(Index);
  end;
  Sort;
end;

procedure TWerkstattList.DeleteTechniker(Index: Integer; FreeWohnRaum: Boolean);
begin
  if (Index>=0) and (Index<Count) then
  begin
    FreeTechniker(Index);

    if FreeWohnRaum then
      basis_api_FreeWohnRaum(fTechniker[Index].BasisID);

    SetLength(fKaufList,Length(fKaufList)+1);
    fKaufList[high(fKaufList)]:=fTechniker[Index];

    DeleteArray(Addr(fTechniker),TypeInfo(TTechnikerArray),Index);

    Sort;
  end;
end;

procedure TWerkstattList.DrawDXKaufTechniker(Index: integer;
  Surface: TDirectDrawSurface;const Mem: TDDSurfaceDesc; x, y: Integer);
begin
  if (Index<0) or (Index>KaufCount-1) then
    exit;
  person_api_DrawFace(Surface,x,y,KaufTechniker[Index].BitNr);
end;

procedure TWerkstattList.DrawDXTechniker(Index: integer; Surface: TDirectDrawSurface;
  const Mem: TDDSurfaceDesc; x, y: Integer);
begin
  if (Index<0) or (Index>Count-1) then exit;
  person_api_DrawFace(Surface,x,y,Techniker[Index].BitNr);
end;

procedure TWerkstattList.DeleteProjekt(Projekt: Integer);
var
  Dummy: Integer;
  ID   : Cardinal;
begin
  ID:=fProjekte[Projekt].ID;
  for Dummy:=0 to Count-1 do
  begin
    if fTechniker[Dummy].ProjektID=ID then
      FreeTechniker(Dummy);
  end;

  DeleteArray(Addr(fProjekte),TypeInfo(TProduktionArray),Projekt);
end;

procedure TWerkstattList.FreeTechniker(Index: Integer);
begin
  if fTechniker[Index].ProjektID<>0 then
  begin
    fTechniker[Index].ProjektID:=0;
    // Raum in der Basis freigeben
    basis_api_FreeWerkRaum(fTechniker[Index].BasisID);
  end;

  fTechniker[Index].Ausbil:=false;
  fTechniker[Index].Repair:=false;

  if fTechniker[Index].AlphatronMining then
  begin
    fTechniker[Index].AlphatronMining:=false;

    fNotifyList.CallEvents(EVENT_ONCHANGEALPHATRONMINING,Self);
  end;
end;

function TWerkstattList.GetCount: Integer;
begin
  result:=length(fTechniker);
end;

function TWerkstattList.CountInBase(BasisID: Cardinal): Integer;
var
  Dummy: Integer;
begin
  Result := 0;
  for Dummy:=0 to High(fTechniker) do
    if fTechniker[Dummy].BasisID=BasisID then
      Inc(result);
end;

function TWerkstattList.GetKaufCount: Integer;
begin
  result:=length(fKaufList);
end;

function TWerkstattList.GetKTechniker(Index: Integer): TTechniker;
begin
  if (Index<0) or (Index>KaufCount-1) then exit;
  result:=fKaufList[Index];
end;

function TWerkstattList.GetProjekt(Index: Integer): TProduktion;
begin
  if (Index<0) or (Index>ProjektCount-1) then exit;
  result:=fProjekte[Index];
end;

function TWerkstattList.GetProjektCount: Integer;
begin
  result:=length(fProjekte);
end;

function TWerkstattList.GetProjektOfID(ID: Cardinal): Integer;
var
  Dummy: Integer;
begin
  result:=-1;
  for Dummy:=0 to ProjektCount-1 do
  begin
    if fProjekte[Dummy].ID=ID then
      result:=Dummy;
  end;
end;

function TWerkstattList.GetTechniker(Index: Integer): TTechniker;
begin
  if (Index<0) or (Index>Count-1) then exit;
  result:=fTechniker[Index];
end;

procedure TWerkstattList.LoadFromStream(Stream: TStream);
var
  Dummy  : Integer;
  Rec    : TExtRecord;
begin
  Clear;

  // Eigene Techniker speichern
  Rec:=TExtRecord.Create(TWerkstattListRecord);
  Rec.LoadFromStream(Stream);

  with Rec.GetRecordList('Techniker') do
  begin
    SetLength(fTechniker,Count);
    for Dummy:=0 to Count-1 do
      fTechniker[Dummy]:=RecordToTechniker(Item[Dummy]);
  end;

  // Arbeitsmarkteinstellungen speichern
  with Rec.GetRecordList('WatchMarkt') do
  begin
    SetLength(fWatchs,Count);
    for Dummy:=0 to Count-1 do
      fWatchs[Dummy]:=RecordToWatchMarktSettings(Item[Dummy]);
  end;


  // Techniker auf dem Arbeitsmarkt speichern
  with Rec.GetRecordList('KaufList') do
  begin
    SetLength(fKaufList,Count);
    for Dummy:=0 to Count-1 do
      fKaufList[Dummy]:=RecordToTechniker(Item[Dummy]);
  end;

  // Projekte speichern
  with Rec.GetRecordList('Produktion') do
  begin
    SetLength(fProjekte,Count);
    for Dummy:=0 to Count-1 do
      fProjekte[Dummy]:=RecordToProduktion(Item[Dummy]);
  end;

  Rec.Free;

  Sort;
end;

procedure TWerkstattList.NextDay;
var
  Dummy     : integer;
  Pension   : boolean;
  TrainTime : Double;
  ProdTime  : Double;

  procedure Train(TrainRatio: Double);
  begin
    fTechniker[Dummy].Strength:=MinReal(fTechniker[Dummy].Strength+(TrainRatio/MaxReal(1,fTechniker[Dummy].Strength)),100);
  end;

begin
  CreateKaufList;
  Dummy:=0;
  while (Dummy<Count) do
  begin
    inc(fTechniker[Dummy].Days);
    Pension:=false;
    if fTechniker[Dummy].Days>1500 then
    begin
      if fTechniker[Dummy].Days>2500 then
        Pension:=(random(10)=5)
      else
        Pension:=(random(30)=random(30));
    end;
    if Pension then
    begin
      savegame_api_FreeMoney(round(fTechniker[Dummy].Strength*TechnikerBuy),kbHEn);
      savegame_api_Message(Format(HPension,[STechniker,fTechniker[Dummy].Name,fTechniker[Dummy].Days,round(fTechniker[Dummy].Strength*TechnikerBuy)]),lmPension);

      FreeTechniker(Dummy);
      basis_api_FreeWohnRaum(fTechniker[Dummy].BasisID);

      DeleteArray(Addr(fTechniker),TypeInfo(TTechnikerArray),Dummy);

      dec(Dummy);
    end
    else
    begin
      if fTechniker[Dummy].TrainTime + fTechniker[Dummy].ProdTime>0 then
      begin
        if fTechniker[Dummy].Strength<100 then
        begin
          try
            TrainTime := fTechniker[Dummy].TrainTime/1440;
            ProdTime := fTechniker[Dummy].ProdTime/1440;

            if fTechniker[Dummy].ProdTime>0 then
              Train(3*ProdTime);

            if fTechniker[Dummy].TrainTime>0 then
            begin
              savegame_api_NeedMoney(Round(TrainKost*TrainTime),kbTK,true);
              Train(20*TrainTime);
            end;

            if fTechniker[Dummy].Ausbil and (fTechniker[Dummy].Strength=100) then
            begin
              fTechniker[Dummy].Ausbil:=false;
              savegame_api_Message(Format(MTrainEnd,[STechniker,fTechniker[Dummy].Name]),lmTrainEnd);
            end;
          except
            on ENotEnoughMoney do
            begin
              savegame_api_Message(Format(ETrainCancel,[fTechniker[Dummy].Name]),lmTrainCancel);
              fTechniker[Dummy].Ausbil:=false;
            end;
          end;
        end
        else fTechniker[Dummy].Ausbil:=false;
        
        fTechniker[Dummy].TrainTime := 0;
        fTechniker[Dummy].ProdTime := 0;
      end;
    end;
    inc(Dummy);
  end;
end;

procedure TWerkstattList.NextRound(Minuten: Integer);
var
  Dummy     : Integer;
  Index     : Integer;
  ItemName  : String;
  Become    : Integer;
begin
  for Dummy:=0 to ProjektCount-1 do
  begin
    if not fProjekte[Dummy].Product then
    begin
      Become:=savegame_api_NeedAlphatron(fProjekte[Dummy].Kost-fProjekte[Dummy].Reserved,fProjekte[Dummy].BasisID);

      fProjekte[Dummy].Reserved:=fProjekte[Dummy].Reserved+Become;
      fProjekte[Dummy].Product:=fProjekte[Dummy].Reserved=fProjekte[Dummy].Kost;
    end;
  end;

  for Dummy:=0 to Count-1 do
  begin
    Index:=GetProjektOfID(fTechniker[Dummy].ProjektID);
    if not (Index=-1) then
    begin
      if fProjekte[Index].Product then
      begin
        //Dies stimmt nicht ganz, da die Runde l�nger dauern kann, als Projektrestzeit
        Inc(fTechniker[Dummy].ProdTime, Minuten);

        // Ein Techniker schafft mit Faehigkeit 80 eine Techniker Stunde
        fProjekte[Index].Hour:=fProjekte[Index].Hour-((fTechniker[Dummy].Strength/80)*(Minuten/60));
        if fProjekte[Index].Hour<=0 then
        begin
          fProjekte[Index].Reserved:=0;
          fProjekte[Index].Hour:=fProjekte[Index].Gesamt;
          if not (fProjekte[Index].Anzahl=101) then
            dec(fProjekte[Index].Anzahl);

          lager_api_PutItemDirekt(fProjekte[Index].BasisID,fProjekte[Index].ItemID);

          ItemName:=lager_api_GetItem(fProjekte[Index].ItemID).Name;

          if fProjekte[Index].Anzahl=0 then
          begin
            fProjekte[Index].LagerV:=0;

            savegame_api_Message(Format(MProduktionEnd,[ItemName]),lmProduktionEnd);
            CancelProjekt(Index,true,true);
            continue;
          end
          else
          begin
            // Nachricht senden
            savegame_api_Message(Format(ST0501230001,[ItemName]),lmProduktionEnd);
          end;

          if not basis_api_NeedLagerRaum(fProjekte[Index].LagerV,fProjekte[Index].BasisID,false) then
          begin
            // Muss auf null gesetzt werden, um nicht noch nochmal den Lagerraum freizugeben
            fProjekte[Index].LagerV:=0;
            ItemName:=lager_api_GetItem(fProjekte[Index].ItemID).Name;

            savegame_api_Message(Format(ENoLagerToProd,[ItemName])+EProdCancel,lmProdCancel);
            CancelProjekt(Index,true);
            continue;
          end;
          Become:=savegame_api_NeedAlphatron(fProjekte[Index].Kost);
          fProjekte[Index].Reserved:=become;
          fProjekte[Index].Product:=fProjekte[Index].Reserved=fProjekte[Index].Kost
        end;
      end;
    end;

    if fTechniker[Dummy].Ausbil then
      Inc(fTechniker[Dummy].TrainTime, Minuten);
  end;
end;

function TWerkstattList.ProjektStrength(Index: Integer): Integer;
var
  ID: Cardinal;
  Dummy: Integer;
begin
  result:=0;
  if (Index<0) or (Index>ProjektCount-1) then exit;
  ID:=fProjekte[Index].ID;
  for Dummy:=0 to Count-1 do
  begin
    if ID=fTechniker[Dummy].ProjektID then
    begin
      inc(result,round(fTechniker[Dummy].Strength));
    end;
  end;
end;

function TWerkstattList.RandomTechniker(GeniusErlaubt: boolean): TTechniker;
var
  Dummy: Integer;
begin
  FillChar(result,sizeof(result),#0);
  result.TechnikerID:=GenerateTechnikerID;

  if GeniusErlaubt then
    result.Strength:=random(60)+40 //40..99
  else
  begin
    Result.Strength := random(50)+ 25; //25..74
    Dummy := Random(25);  //bis zu 24 mal Gl�ck probieren (bis zu +24 St�rke)
    while Dummy>0 do
    begin
      if Random(100)>95 then //wenn Gl�ck gehabt, ein Punkt dazu
        Result.Strength := Result.Strength + 1;
      dec(Dummy);
    end;
  end;

  result.Name:=person_api_GenerateName;
  result.BitNr:=person_api_GenerateFace;
  result.ProjektID:=0;
  result.days:=0;
  result.Ausbil:=false;
  result.AlphatronMining:=false;
  result.Repair:=false;
  result.TrainTime := 0;
  result.ProdTime := 0;
end;

procedure TWerkstattList.SaveToStream(Stream: TStream);
var
  Dummy  : Integer;
  Rec    : TExtRecord;
begin
  // Eigene Techniker speichern
  Rec:=TExtRecord.Create(TWerkstattListRecord);

  with Rec.GetRecordList('Techniker') do
  begin
    for Dummy:=0 to high(fTechniker) do
      Add(TechnikerToRecord(fTechniker[Dummy]));
  end;

  // Arbeitsmarkteinstellungen speichern
  with Rec.GetRecordList('WatchMarkt') do
  begin
    for Dummy:=0 to high(fWatchs) do
      Add(WatchMarktSettingsToRecord(fWatchs[Dummy]));
  end;


  // Techniker auf dem Arbeitsmarkt speichern
  with Rec.GetRecordList('KaufList') do
  begin
    for Dummy:=0 to high(fKaufList) do
      Add(TechnikerToRecord(fKaufList[Dummy]));
  end;

  // Projekte speichern
  with Rec.GetRecordList('Produktion') do
  begin
    for Dummy:=0 to high(fProjekte) do
      Add(ProduktionToRecord(fProjekte[Dummy]));
  end;

  Rec.SaveToStream(Stream);
  Rec.Free;
end;

procedure TWerkstattList.SellTechniker(Index: Integer);
begin
  Assert((Index>=0) and (Index<Count));

  savegame_api_FreeMoney(round(fTechniker[Index].Strength*TechnikerSell),kbHVK);
  DeleteTechniker(Index);
end;

procedure TWerkstattList.SetProjekt(Techniker, Projekt: Integer);
begin
  if (Techniker<0) or (Techniker>Count-1) then exit;
  if (Projekt<0) or (Projekt>ProjektCount-1) then exit;

  FreeTechniker(Techniker);

  basis_api_NeedWerkRaum(fProjekte[Projekt].BasisID);

  fTechniker[Techniker].ProjektID:=fProjekte[Projekt].ID;

  if fTechniker[Techniker].AlphatronMining then
  begin
    fTechniker[Techniker].AlphatronMining:=false;
    fNotifyList.CallEvents(EVENT_ONCHANGEALPHATRONMINING,Self);
  end;
end;

procedure TWerkstattList.ChangeTraining(Index: Integer);
begin
  Assert((Index>=0) and (Index<Count));
  if fTechniker[Index].Strength=100 then
    raise Exception.Create(Format(ENoTrainNeed,[STechniker,fTechniker[Index].Name]));

  if fTechniker[Index].Ausbil then
    fTechniker[Index].Ausbil:=false
  else
  begin
    FreeTechniker(Index);
    fTechniker[Index].Ausbil:=true;
  end;
end;

procedure TWerkstattList.SetTraining(Index: Integer);
begin
  Assert((Index>=0) and (Index<Count));
  if fTechniker[Index].Strength=100 then
    raise Exception.Create(Format(ENoTrainNeed,[STechniker,fTechniker[Index].Name]));

  FreeTechniker(Index);
  fTechniker[Index].Ausbil:=true;
end;

function TWerkstattList.PercentDone(Index: Integer): real;
begin
  result:=0;
  Assert((Index>=0) and (Index<ProjektCount));
  if Projekte[Index].Gesamt=0 then exit;
  result:=100-((Projekte[Index].Hour/Projekte[Index].Gesamt)*100);
end;

procedure TWerkstattList.ChangeProjekt(ID:Cardinal; Anzahl: Integer);
var
  Dummy  : Integer;
  BaseID : Cardinal;
begin
  BaseID:=basis_api_GetSelectedBasis.ID;
  for Dummy:=0 to ProjektCount-1 do
  begin
    if (fProjekte[Dummy].ItemID=ID) and
       (fProjekte[Dummy].BasisID=BaseID) then
      fProjekte[Dummy].Anzahl:=Anzahl;
  end;
end;

procedure TWerkstattList.Rename(Index: Integer; Name: String);
begin
  fTechniker[Index].Name:=Name;
end;

{ Diese Funktionen gibt true zur�ck, wenn der Techniker Index
  die gleiche BasisID hat, wie die ausgew�hlte Basis }
function TWerkstattList.InSelectedBasis(Index: Integer): boolean;
begin
  result:=fTechniker[Index].BasisID=basis_api_GetSelectedBasis.ID;
end;

function TWerkstattList.ProjektInSelectedBasis(Index: Integer): boolean;
begin
  result:=fProjekte[Index].BasisID=basis_api_GetSelectedBasis.ID;
end;

procedure TWerkstattList.CancelWatch(BasisID: Cardinal);
var
  Dummy: Integer;
begin
  for Dummy:=0 to high(fWatchs) do
  begin
    if fWatchs[Dummy].Active and (fWatchs[Dummy].BasisID=BasisID) then
    begin
      DeleteArray(Addr(fWatchs),TypeInfo(TWatchMarktArray),Dummy);
      exit;
    end;
  end;
end;

function TWerkstattList.GetWatch(BasisID: Cardinal;
  var Watch: TWatchMarktSettings): Boolean;
var
  Dummy: Integer;
begin
  result:=false;
  for Dummy:=0 to high(fWatchs) do
  begin
    if fWatchs[Dummy].Active and (fWatchs[Dummy].BasisID=BasisID) then
    begin
      Watch:=fWatchs[Dummy];
      result:=true;
      exit;
    end;
  end;
end;

procedure TWerkstattList.SetWatch(Basis: Cardinal; minStrength: Integer;
  WType: TWatchType);
begin
  CancelWatch(Basis);

  SetLength(fWatchs,length(fWatchs)+1);

  with fWatchs[high(fWatchs)] do
  begin
    BasisID:=Basis;
    Active:=true;
    minFaehigkeiten:=minStrength;
    WatchType:=WType;
  end;
end;

procedure TWerkstattList.Sort;
begin
  if fNotSort then
    exit;
  fSortKauf:=true;
  QuickSort(0,High(fKaufList),SortFunc);
  fSortKauf:=false;
  QuickSort(0,High(fTechniker),SortFunc);
end;

function TWerkstattList.SortFunc(KTechniker1, KTechniker2: Integer;
  Typ: TFunctionType): Integer;
var
  Techniker: TTechniker;
  List     : TTechnikerArray;
begin
  result:=0;
  if fSortKauf then
    List:=fKaufList
  else
    List:=fTechniker;
  case Typ of
    ftCompare  : result:=round((List[KTechniker2].Strength-List[KTechniker1].Strength)*100);
    ftExchange :
    begin
      Techniker:=List[KTechniker2];
      List[KTechniker2]:=List[KTechniker1];
      List[KTechniker1]:=Techniker;
    end;
  end;
end;

procedure TWerkstattList.CheckWatchTechniker(Index: Integer);
var
  Dummy             : Integer;
  DefMessage        : String;
  Techniker         : TTechniker;

  function SellWeakTechniker(BasisID: Cardinal): TTechniker;
  var
    Dummy: Integer;
    Index: Integer;
  begin
    result.Name:='';
    if length(fTechniker)=0 then
      exit;

    Index:=-1;
    for Dummy:=0 to high(fTechniker) do
    begin
      if (Index=-1) and (fTechniker[Dummy].BasisID=BasisID) then
      begin
        result:=fTechniker[Dummy];
        Index:=Dummy;
      end
      else if (result.Strength>fTechniker[Dummy].Strength) and (fTechniker[Dummy].BasisID=BasisID) then
      begin
        result:=fTechniker[Dummy];
        Index:=Dummy;
      end;
    end;
    SellTechniker(Index);
  end;

  function AllTechnikerBetter(BasisID: Cardinal; Faehigkeit: single): boolean;
  var
    Dummy: Integer;
  begin
    result:=true;
    for Dummy:=0 to high(fTechniker) do
    begin
      if (fTechniker[Dummy].BasisID=BasisID) and (fTechniker[Dummy].Strength<Faehigkeit) then
      begin
        result:=false;
        exit;
      end;
    end;
  end;

begin
  fNotSort:=true;
  for Dummy:=0 to high(fWatchs) do
  begin
    if (not fWatchs[Dummy].Active) or (fKaufList[Index].Strength<fWatchs[Dummy].minFaehigkeiten) then
      continue;

    if basis_api_GetBasisCount>1 then
      DefMessage:=Format(ST0309270034,[fKaufList[Index].Name,basis_api_GetBasisFromID(fWatchs[Dummy].BasisID).Name])
    else
      DefMessage:=Format(ST0309270035,[fKaufList[Index].Name]);

    if fWatchs[Dummy].WatchType=wtMessage then
    begin
      savegame_api_Message(DefMessage,lmFindPerson);
      break;
    end;

    try
      Techniker.Name:='';
      if fWatchs[Dummy].WatchType=wtExchange then
      begin
        if AllTechnikerBetter(fWatchs[Dummy].BasisID,fKaufList[KaufCount-1].Strength) then
        begin
          savegame_api_Message(DefMessage+#10+Format(ST0311210002,[STechniker]),lmFindPerson);
          break;
        end;
        // Schw�chsten Techniker entlassen
        Techniker:=SellWeakTechniker(fWatchs[Dummy].BasisID);
      end;
      BuyTechniker(Index,fWatchs[Dummy].BasisID);
      DefMessage:=DefMessage+#10+ST0309270036;

      if Techniker.Name<>'' then
      begin
        // Ein anderer Techniker wurde entlassen (wtExchange)
        DefMessage:=DefMessage+#10+Format(ST0311210001,[Techniker.Name]);
        // Neuen Techniker das Projekt weiter erforschen lassen
        SetProjekt(high(fTechniker),GetProjektOfID(Techniker.ProjektID));
      end;

      savegame_api_Message(DefMessage,lmFindPerson);
    except
      on E: ENotEnoughMoney do
        savegame_api_Message(DefMessage+#10+ST0309270037,lmReserved);
      on E: ENotEnoughRoom do
        savegame_api_Message(DefMessage+#10+ST0309270038,lmReserved);
    end;
  end;
  fNotSort:=false;
end;

function TWerkstattList.CalculateWeeklySold: Integer;
var
  Dummy: Integer;
begin
  result:=0;
  for Dummy:=0 to Count-1 do
    inc(result,round(fTechniker[Dummy].Strength*TechnikerBuy/100));
end;

function TWerkstattList.CalculateWeeklySold(BasisID: Cardinal): Integer;
var
  Dummy: Integer;
begin
  result:=0;
  for Dummy:=0 to Count-1 do
    if fTechniker[Dummy].BasisID=BasisID then
      inc(result,round(fTechniker[Dummy].Strength*TechnikerBuy/100));
end;

procedure TWerkstattList.NewGameHandler(Sender: TObject);
begin
  Clear;

  CreateTechniker(savegame_api_GetNewGameData.Techniker);
  CreateKaufList;
end;

procedure TWerkstattList.SetMining(Index: Integer);
begin
  if fTechniker[Index].AlphatronMining then
    exit;

  FreeTechniker(Index);

  fTechniker[Index].AlphatronMining:=true;

  fNotifyList.CallEvents(EVENT_ONCHANGEALPHATRONMINING,Self);
end;

destructor TWerkstattList.destroy;
begin
  fNotifyList.Free;
  inherited;
end;

function TWerkstattList.GenerateTechnikerID: Cardinal;
var
  ID     : Cardinal;
  IDOk   : Boolean;
  Dummy  : Integer;
begin
  repeat
    IDOk:=true;
    ID:=random(high(Cardinal));

    for Dummy:=0 to high(fTechniker) do
    begin
      if ID=fTechniker[Dummy].TechnikerID then
      begin
        IDOk:=false;
        continue;
      end;
    end;

    for Dummy:=0 to high(fKaufList) do
    begin
      if ID=fKaufList[Dummy].TechnikerID then
      begin
        IDOk:=false;
        continue;
      end;
    end;
  until IDOk;
  result:=ID;
end;

procedure TWerkstattList.SetRepair(Index: Integer);
begin
  FreeTechniker(Index);

  fTechniker[Index].Repair:=true;
end;

function TWerkstattList.IsTechnikerFree(Index: Integer): Boolean;
begin
  Assert((Index>=0) and (Index<Count));

  result:=(fTechniker[Index].ProjektID=0) and (not fTechniker[Index].Ausbil) and
          (not fTechniker[Index].AlphatronMining) and (not fTechniker[Index].Repair);
end;

function TWerkstattList.CheckWerkRoom(BaseID: Cardinal): String;
var
  Dummy: Integer;
  Count: Integer;
  Res  : Integer;
begin
  Count:=0;
  for Dummy:=0 to high(fTechniker) do
  begin
    if (fTechniker[Dummy].BasisID=BaseID) and (fTechniker[Dummy].ProjektID<>0) then
      inc(Count);
  end;

  Assert(Count>=basis_api_FreierWerkRaum(BaseID));
  Res:=0;
  Dummy:=high(fTechniker);
  while (Dummy>=0) and (Count>basis_api_FreierWerkRaum(BaseID)) do
  begin
    if (fTechniker[Dummy].BasisID=BaseID) and (fTechniker[Dummy].ProjektID<>0) then
    begin
      FreeTechniker(Dummy);
      dec(Count);
      inc(Res);
    end;
    dec(Dummy);
  end;
  if Res>0 then
    result:=Format(ST0503100001,[Res,STechniker])
  else
    result:='';
end;


initialization

  TWerkstattListRecord:=TExtRecordDefinition.Create('TWerkstattListRecord');
  TWerkstattListRecord.AddRecordList('Techniker',RecordTechniker);
  TWerkstattListRecord.AddRecordList('KaufList',RecordTechniker);
  TWerkstattListRecord.AddRecordList('Produktion',RecordProduktion);
  TWerkstattListRecord.AddRecordList('WatchMarkt',RecordWatchMarktSettings);

end.
