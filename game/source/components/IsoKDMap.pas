{********************************************************************************
*										*
* Copyright (C) 2004  X-Force Team - jim_raynor@web.de				*
*										*
* This file is part of X-Force: Fight For Destiny (X-Force).			*
*										*
* X-Force is free software; you can redistribute it and/or modify		*
* it under the terms of the GNU General Public License as published by		*
* the Free Software Foundation; either version 2 of the License, or		*
* (at your option) any later version.						*
*										*
* X-Force is distributed in the hope that it will be useful,			*
* but WITHOUT ANY WARRANTY; without even the implied warranty of		*
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the			*
* GNU General Public License for more details.					*
*										*
* You should have received a copy of the GNU General Public License		*
* along with X-Force; if not, write to the Free Software			*
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA	*
*										*
*********************************************************************************
*										*
* Nur f�r den Karteneditor							*
*										*
********************************************************************************}

{$I ../Settings.inc}

unit IsoKDMap;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  DXDraws, XForce_types, math, TileGroup,DIB, extctrls, Defines;

type
  TDirection = (sdLeft,sdRight,sdUp,sdDown);
  TDirections= set of TDirection;

{.$DEFINE DEBUG }
type
  TIsoKDMap = class(TDXDraw)
  private                                        
    fOnChange  : TNotifyEvent;
    fOnTileMove: TTileEvent;
    SystemList : TDXImageList;
    fWidth     : Integer;
    fHeight    : Integer;
    fMouseMap  : TBitmap;
    fDirect    : TDirections;
    fTimer     : TTimer;
    LastX,LastY: Integer;
    fmark      : Tpoint;
    fBackMark  : TBitmap;
    fForeMark  : TBitmap;
    fBackSelect: TBitmap;
    fForeSelect: TBitmap;
    {$IFDEF DEBUG}
      fDrawx     : Integer;
      LastXPos   : Integer;
    {$ENDIF}
    fModify    : boolean;
    fSelect    : Array of Array of TSelectTile;
    fTileGroup  : TTileGroup;
    MarkStart  : TPoint;
    MarkReg    : boolean;
    ShiftSelect: boolean;
    fxoff,fyoff: Integer;
    fMapName   : NameString;
    fMapOpen   : boolean;
    fFile      : String;
    fOnFileChange: TNotifyEvent;
    fAutoScroll: boolean;
    fLoaded    : boolean;
    fPercentZufall: Integer;
    fShowWalls : Boolean;
    fPaintStruct: TSetTile;
    fDeleteRight: TBitmap;
    fDeleteLeft: TBitmap;
    fShowGrid: Boolean;
    fGrid: TBitmap;
    fFriendlyHotSpot: TBitmap;
    fEnemyHotSpot: TBitmap;

    procedure SetHeight(const Value: Integer);
    procedure SetWidth(const Value: Integer);
    function TileAtPos(x,y: Integer): TPoint;
    procedure SetBackMark(const Value: TBitmap);
    procedure SetForeMark(const Value: TBitmap);
    procedure SetBackSelect(const Value: TBitmap);
    procedure SetForeSelect(const Value: TBitmap);
    procedure RestoreRect(FromPoint,ToPoint: TPoint);
    procedure SelectRect(FromPoint,ToPoint: TPoint);
    procedure SaveSelection;
    function GetMaxYOffSet: Integer;
    function GetMaxXOffSet: Integer;
    function GetMinXOffSet: Integer;
    procedure UpdateScrollBar;
    procedure SetXOff(const Value: Integer);
    procedure SetyOff(const Value: Integer);
    procedure SetMouseMap(const Value: TBitmap);
    function GetTileGroup: TTileGroup;
    procedure SetModify(const Value: boolean);
    procedure SetMark(const Value: TPoint);
    procedure SetMapName(const Value: NameString);
    procedure SetFile(const Value: String);
    procedure SetAutoScroll(const Value: boolean);
    procedure SetDeleteLeft(const Value: TBitmap);
    procedure SetDeleteRight(const Value: TBitmap);
    procedure SetGrid(const Value: TBitmap);
    procedure SetEnemyHotSpot(const Value: TBitmap);
    procedure SetFriendlyHotSpot(const Value: TBitmap);
    { Private-Deklarationen }
  protected
    procedure WMMouseMove(var Msg: TWMMouseMove);message WM_MOUSEMOVE;
    procedure WMNCMouseMove(var Msg: TWMNCMouseMove);message WM_NCMOUSEMOVE;
    procedure WMLDown(var Msg: TWMLButtonDown);message WM_LBUTTONDOWN;
    procedure WMLNCUp(var Msg: TWMLButtonUp);message WM_NCLBUTTONUP;
    procedure WMLUp(var Msg: TWMLButtonUp);message WM_LBUTTONUP;
    procedure WMSize(var Msg: TWMSize);message WM_SIZE;
    procedure WMVScroll(var Message: TWMScroll); message WM_VSCROLL;
    procedure WMHScroll(var Message: TWMScroll); message WM_HSCROLL;
    procedure CMMouseLeave(var Msg: TMessage);message CM_MOUSELEAVE;
    procedure ScrollTimer(Sender: TObject);
    procedure BackMarkChange(Sender: TObject);
    procedure CreateParams(var Params: TCreateParams); override;
    { Protected-Deklarationen }
  public
    Tiles        : Array of Array of TMapTileEntry;
    procedure ClearTileSet;
    constructor Create(AOwner: TComponent);override;
    destructor Destroy;override;
    procedure LoadTile(FileName: String);
    procedure SetDimension(NewWidth,NewHeight: Integer);
    property TileGroup : TTileGroup read GetTileGroup;
    procedure SetTile(X,Y: Integer;TileStruct: TSetTile);
    procedure Draw;

    procedure PaintTile;

    procedure SetPaintStruct(Struct: TSetTile);

    { Public-Deklarationen }
  published
    property xOff           : Integer read fxOff write SetXOff;
    property yOff           : Integer read fyOff write SetyOff;
    property MapWidth       : Integer read fWidth write SetWidth;
    property MapHeight      : Integer read fHeight write SetHeight;

    property BackMark       : TBitmap read fBackMark write SetBackMark;
    property ForeMark       : TBitmap read fForeMark write SetForeMark;
    property BackSelect     : TBitmap read fBackSelect write SetBackSelect;
    property ForeSelect     : TBitmap read fForeSelect write SetForeSelect;
    property DeleteWallLeft : TBitmap read fDeleteLeft write SetDeleteLeft;
    property DeleteWallRight: TBitmap read fDeleteRight write SetDeleteRight;
    property GridBitmap     : TBitmap read fGrid write SetGrid;

    property FriendlyHotSpot: TBitmap read fFriendlyHotSpot write SetFriendlyHotSpot;
    property EnemyHotSpot   : TBitmap read fEnemyHotSpot write SetEnemyHotSpot;

    property MaxYOffSet     : Integer read GetMaxYOffSet;
    property MinXOffSet     : Integer read GetMinXOffSet;
    property MaxXOffSet     : Integer read GetMaxXOffSet;
    property MouseMap       : TBitmap read fMouseMap write SetMouseMap;
    property MapName        : NameString read fMapName write SetMapName;
    property PercentZufall  : Integer read fPercentZufall write fPercentZufall;
    property Modify         : boolean read fModify write SetModify;
    property OnChange       : TNotifyEvent read fOnChange write fOnChange;
    property OnTileMove     : TTileEvent   read fOnTileMove write fOnTileMove;
    property Mark           : TPoint       read fMark write SetMark;
    property FileName       : String       read fFile write SetFile;
    property OnFileChange   : TNotifyEvent read fOnFileChange write fOnFileChange;
    property AutoScroll     : boolean      read fAutoScroll write SetAutoScroll;

    property ShowWall       : Boolean      read fShowWalls write fShowWalls;
    property Grid           : Boolean      read fShowGrid write fShowGrid;
    { Published-Deklarationen }
  end;

procedure Register;

implementation

function RandomChance(Percent: Integer): boolean;
begin
  result:=false;
  if random(100)<Percent+1 then
  begin
    result:=true;
  end;
end;

procedure Register;
begin
  RegisterComponents('KD4 Tools', [TIsoKDMap]);
end;

{ TIsoKDMap }

procedure TIsoKDMap.BackMarkChange(Sender: TObject);
begin
  SystemList.Items[0].Picture.Assign(fBackMark);
  SystemList.Items[0].Transparent:=true;
  SystemList.Items[0].TransparentColor:=clWhite;

  SystemList.Items[1].Picture.Assign(fForeMark);
  SystemList.Items[1].Transparent:=true;
  SystemList.Items[1].TransparentColor:=clWhite;

  SystemList.Items[2].Picture.Assign(fBackSelect);
  SystemList.Items[2].Transparent:=true;
  SystemList.Items[2].TransparentColor:=clWhite;

  SystemList.Items[3].Picture.Assign(fForeSelect);
  SystemList.Items[3].Transparent:=true;
  SystemList.Items[3].TransparentColor:=clWhite;

  SystemList.Items[4].Picture.Assign(fDeleteLeft);
  SystemList.Items[4].Transparent:=true;
  SystemList.Items[4].TransparentColor:=clWhite;

  SystemList.Items[5].Picture.Assign(fDeleteRight);
  SystemList.Items[5].Transparent:=true;
  SystemList.Items[5].TransparentColor:=clWhite;

  SystemList.Items[6].Picture.Assign(fGrid);
  SystemList.Items[6].Transparent:=true;
  SystemList.Items[6].TransparentColor:=clWhite;

  SystemList.Items[7].Picture.Assign(fFriendlyHotSpot);
  SystemList.Items[7].Transparent:=true;
  SystemList.Items[7].TransparentColor:=clFuchsia;

  SystemList.Items[8].Picture.Assign(fEnemyHotSpot);
  SystemList.Items[8].Transparent:=true;
  SystemList.Items[8].TransparentColor:=clFuchsia;
end;

procedure TIsoKDMap.ClearTileSet;
begin
  if fTileGroup<>nil then
    TileGroup.Free;
  fTileGroup:=TTileGroup.Create(Self);
  fTileGroup.DXDraw:=Self;
end;

procedure TIsoKDMap.CMMouseLeave(var Msg: TMessage);
begin
  fDirect:=[];
end;

constructor TIsoKDMap.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
  fLoaded:=false;
  fMapOpen:=false;

  fMouseMap:=TBitmap.Create;
  fBackMark:=TBitmap.Create;
  fForeMark:=TBitmap.Create;
  fBackSelect:=TBitmap.Create;
  fForeSelect:=TBitmap.Create;
  fDeleteLeft:=TBitmap.Create;
  fDeleteRight:=TBitmap.Create;

  fFriendlyHotSpot:=TBitmap.Create;
  fEnemyHotSpot:=TBitmap.Create;

  fGrid:=TBitmap.Create;
  fTImer:=TTimer.Create(Self);
  fTimer.Interval:=75;
  fTimer.OnTimer:=ScrollTimer;
  fTimer.Enabled:=fAutoScroll;
  fLoaded:=true;
  fBackMark.OnChange:=BackMarkChange;
  fWidth:=0;
  fHeight:=0;
  UpdateScrollBar;

  fShowWalls:=true;

  fForeMark.OnChange:=BackMarkChange;
  fBackSelect.OnChange:=BackMarkChange;
  fForeSelect.OnChange:=BackMarkChange;
  fDeleteLeft.OnChange:=BackMarkChange;
  fDeleteRight.OnChange:=BackMarkChange;
  fGrid.OnChange:=BackMarkChange;
  fFriendlyHotSpot.OnChange:=BackMarkChange;
  fEnemyHotSpot.OnChange:=BackMarkChange;
  SystemList:=TDXImageList.Create(Self);
  SystemList.Items.Add;
  SystemList.Items.Add;
  SystemList.Items.Add;
  SystemList.Items.Add;
  SystemList.Items.Add;
  SystemList.Items.Add;
  SystemList.Items.Add;
  SystemList.Items.Add;
  SystemList.Items.Add;
  SystemList.DXDraw:=self;
  SetDimension(0,0);
end;

procedure TIsoKDMap.CreateParams(var Params: TCreateParams);
begin
  inherited CreateParams(Params);
  with Params do
  begin
    Style:=Style or WS_HSCROLL or WS_VSCROLL;
    Style := Style and not WS_BORDER;
    ExStyle := ExStyle and (not WS_EX_CLIENTEDGE);
    WindowClass.style := WindowClass.style and not (CS_HREDRAW or CS_VREDRAW);
  end;
end;

destructor TIsoKDMap.Destroy;
begin
  fTimer.Free;
  fMouseMap.Free;
  fBackMark.Free;
  fForeMark.Free;
  fBackSelect.Free;
  fForeSelect.Free;
  if fTileGroup<>nil then
    fTileGroup.Free;
  inherited;
end;

procedure TIsoKDMap.Draw;
var
  X,Y: integer;
  XPos,YPos :integer;
begin
  if not CanDraw then exit;
  Surface.Canvas.Release;
  Surface.Fill(clBlack);

  YPos:=0;
  y:=0;
  while (YPos<fHeight) and (y+yoff<=Height) do
  begin
    x:=((Width div 2)-(YPos*HalfTileWidth));
    XPos:=0;
    while (XPos<fWidth) and (x+xoff<=Width) do
    begin
      if (x+xoff>-TileWidth) and (y+yoff>-TileHeight) then
      begin
        TileGroup.DrawFloorDirect(x+Xoff,y+yoff,Tiles[xPos,yPos].Floor);

        if (fPaintStruct.Floor.SetTile) and (xPos=mark.x) and (ypos=mark.y) then
          TileGroup.DrawFloorDirect(x+Xoff,y+yoff,fPaintStruct.Floor.Index,150);

        if fShowGrid then
         SystemList.Items[6].Draw(Surface,x+Xoff,y+yoff,0)

      end;
      x:=x+HalfTileWidth;
      y:=y+HalfTileHeight;
      inc(XPos);
    end;
    inc(ypos);
    y:=(YPOS*HalfTileHeight);
  end;

  // mauern zeichnen folgt noch

  YPos:=0;
  y:=0;
  while (YPos<fHeight) and (y+yoff<=Height) do
  begin
    x:=((Width div 2)-(YPos*HalfTileWidth));
    XPos:=0;
    while (XPos<fWidth) and (x+xoff<=Width) do
    begin
      if (x+xoff>-TileWidth) and (y+yoff>-TileHeight) then
      begin
{        if fSelect[xPos,yPos].Select then
          SystemList.Items[2].Draw(Surface,x+xoff,y-ZOffSet+yoff,0);

        if (xPos=mark.x) and (ypos=mark.y) then
          SystemList.Items[0].Draw(Surface,x+xoff,y-ZOffSet+yoff,0);
}
        if fShowWalls then
        begin
          TileGroup.DrawWallLeftDirect(x+Xoff,y-(WallHeight-(Wall3DEffekt div 2)-HalfTileHeight)+yoff,Tiles[xPos,yPos].TLeft);
          TileGroup.DrawWallRightDirect(x+(HalfTileWidth-(Wall3DEffekt))+Xoff,y-(WallHeight-(Wall3DEffekt div 2)-HalfTileHeight)+yoff,Tiles[xPos,yPos].TRight);
        end;

        if (xPos=mark.x) and (ypos=mark.y) and (fPaintStruct.TRight.SetTile) then
        begin
          if fPaintStruct.TRight.Index=-1 then
            SystemList.Items[5].Draw(Surface,x+(HalfTileWidth-(Wall3DEffekt))+Xoff,y-(WallHeight-(Wall3DEffekt div 2)-HalfTileHeight)+yoff,0)
          else
            TileGroup.DrawWallRightDirect(x+(HalfTileWidth-(Wall3DEffekt))+Xoff,y-(WallHeight-(Wall3DEffekt div 2)-HalfTileHeight)+yoff,fPaintStruct.TRight.Index,150);
        end;

        if (xPos=mark.x) and (ypos=mark.y) and (fPaintStruct.TLeft.SetTile) then
        begin
          if fPaintStruct.TLeft.Index=-1 then
            SystemList.Items[4].Draw(Surface,x+Xoff,y-(WallHeight-(Wall3DEffekt div 2)-HalfTileHeight)+yoff,0)
          else
            TileGroup.DrawWallLeftDirect(x+Xoff,y-(WallHeight-(Wall3DEffekt div 2)-HalfTileHeight)+yoff,fPaintStruct.TLeft.Index,150);
        end;

        TileGroup.DrawObjectDirect(x+Xoff,y-42+yoff,Tiles[xPos,yPos].Obj);

        if (xPos=mark.x) and (ypos=mark.y) and fPaintStruct.HotSpot.SetTile then
        begin
          if fPaintStruct.HotSpot.Index = 1 then
            SystemList.Items[7].DrawAlpha(Surface,Bounds(x+Xoff-11,y+yoff-TileHeight-11,85,85),0,150)
          else if fPaintStruct.HotSpot.Index = 2 then
            SystemList.Items[8].DrawAlpha(Surface,Bounds(x+Xoff-11,y+yoff-TileHeight-11,85,85),0,150);
        end
        else
        begin
          if Tiles[xPos,yPos].HotSpot = 1 then
            SystemList.Items[7].Draw(Surface,x+Xoff-11,y+yoff-TileHeight-11,0)
          else if Tiles[xPos,yPos].HotSpot = 2 then
            SystemList.Items[8].Draw(Surface,x+Xoff-11,y+yoff-TileHeight-11,0);
        end;

        if fShowWalls then
        begin
          TileGroup.DrawWallLeftDirect(x+(HalfTileWidth-(Wall3DEffekt))+Xoff,y-(WallHeight-TileHeight)+yoff,Tiles[xPos,yPos].Bright);
          TileGroup.DrawWallRightDirect(x+Xoff,y-(WallHeight-TileHeight)+yoff,Tiles[xPos,yPos].BLeft);
        end;

        if (xPos=mark.x) and (ypos=mark.y) and (fPaintStruct.BRight.SetTile) then
        begin
          if fPaintStruct.BRight.Index=-1 then
            SystemList.Items[4].Draw(Surface,x+(HalfTileWidth-(Wall3DEffekt))+Xoff,y-(WallHeight-TileHeight)+yoff,0)
          else
            TileGroup.DrawWallLeftDirect(x+(HalfTileWidth-(Wall3DEffekt))+Xoff,y-(WallHeight-TileHeight)+yoff,fPaintStruct.BRight.Index,150);
        end;

        if (xPos=mark.x) and (ypos=mark.y) and (fPaintStruct.BLeft.SetTile) then
        begin
          if fPaintStruct.BLeft.Index=-1 then
            SystemList.Items[5].Draw(Surface,x+Xoff,y-(WallHeight-TileHeight)+yoff,0)
          else
            TileGroup.DrawWallRightDirect(x+Xoff,y-(WallHeight-TileHeight)+yoff,fPaintStruct.BLeft.Index,150);
        end;

{        if fSelect[xPos,yPos].Select then
          SystemList.Items[3].Draw(Surface,x+xoff,y-ZOffSet+yoff,0);

        if (xPos=mark.x) and (ypos=mark.y) then
          SystemList.Items[1].Draw(Surface,x+xoff,y-ZOffSet+yoff,0);
}
      end;
      x:=x+HalfTileWidth;
      y:=y+HalfTileHeight;
      inc(XPos);
    end;
    inc(ypos);
    y:=(YPOS*HalfTileHeight);
  end;
  Surface.Canvas.Release;
  Flip;
end;

function TIsoKDMap.GetMaxXOffSet: Integer;
var
  xTemp: Integer;
begin
  xTemp:=MapWidth;
  result:=(xTemp*HalfTileWidth)+TileWidth-(Width div 2);
end;

function TIsoKDMap.GetMaxYOffSet: Integer;
var
  yTemp: Integer;
begin
  yTemp:=Ceil((MapWidth+MapHeight)/2);
  result:=(yTemp*NormalTileHeight)-Height+21;
end;

function TIsoKDMap.GetMinXOffSet: Integer;
var
  xTemp: Integer;
begin
  xTemp:=MapHeight;
  result:=-(xTemp*HalfTileWidth)+(Width div 2);
end;

function TIsoKDMap.GetTileGroup: TTileGroup;
begin
  if fTileGroup=nil then
  begin
    fTileGroup:=TTileGroup.Create(Self);
    fTileGroup.DXDraw:=Self;
  end;
  result:=fTileGroup;
end;

procedure TIsoKDMap.LoadTile(FileName: String);
begin
  if TileGroup<>nil then TileGroup.Free;
  fTileGroup:=TTileGroup.Create(Self);
  fTileGRoup.LoadFromFile(FileName);
  fTileGroup.DXDraw:=Self;
end;

procedure TIsoKDMap.PaintTile;
begin
  SetTile(Mark.X,Mark.Y,fPaintStruct);
end;

procedure TIsoKDMap.RestoreRect(FromPoint, ToPoint: TPoint);
var
  x,y: Integer;
begin
  if PtInRect(Rect(0,0,MapWidth,MapHeight),FromPoint) and PtInRect(Rect(0,0,MapWidth,MapHeight),ToPoint) then
  begin
    for x:=min(FromPoint.x,ToPoint.x) to max(FromPoint.x,ToPoint.x) do
    begin
      for y:=min(FromPoint.y,ToPoint.y) to max(FromPoint.y,ToPoint.y) do
      begin
        fSelect[x,y].Select:=fSelect[x,y].Temp;
      end;
    end;
  end;
end;

procedure TIsoKDMap.SaveSelection;
var
  x,y: Integer;
begin
  for x:=0 to MapWidth-1 do
  begin
    for y:=0 to MapHeight-1 do
    begin
      fSelect[x,y].Temp:=fSelect[x,y].Select;
    end;
  end;
end;

procedure TIsoKDMap.ScrollTimer(Sender: TObject);
var
  NewMark: TPoint;
  Aktu: boolean;
begin
  Aktu:=false;
  if sdLeft in fdirect then
  begin
    xOff:=min(XOff+48,-MinXOffSet);
    Aktu:=true;
  end;
  if sdRight in fdirect then
  begin
    xOff:=max(XOff-48,-MaxXOffSet);
    Aktu:=true;
  end;
  if sdUp in fdirect then
  begin
    YOff:=min(Yoff+24,60);
    Aktu:=true;
  end;
  if sdDown in fdirect then
  begin
    yOff:=max(yOff-24,-MaxYOffSet);
    Aktu:=true;
  end;
  if ((NewMark.x<>mark.x) or (NewMark.y<>mark.y)) and MarkReg then
  begin
    Newmark:=TileAtPos(LastX,LastY);
    RestoreRect(MarkStart,Mark);
    SelectRect(MarkStart,NewMark);
    mark:=NewMark;
    Aktu:=true;
  end;
  if Aktu then Draw;
end;

procedure TIsoKDMap.SelectRect(FromPoint, ToPoint: TPoint);
var
  x,y: Integer;
begin
  if PtInRect(Rect(0,0,MapWidth,MapHeight),FromPoint) and PtInRect(Rect(0,0,MapWidth,MapHeight),ToPoint) then
  begin
    for x:=min(FromPoint.x,ToPoint.x) to max(FromPoint.x,ToPoint.x) do
    begin
      for y:=min(FromPoint.y,ToPoint.y) to max(FromPoint.y,ToPoint.y) do
      begin
        fSelect[x,y].Select:=true;
      end;
    end;
  end;
end;

procedure TIsoKDMap.SetAutoScroll(const Value: boolean);
begin
  fAutoScroll := Value;
  if fLoaded then fTimer.Enabled:=Value;
end;

procedure TIsoKDMap.SetBackMark(const Value: TBitmap);
begin
  fBackMark.Assign(Value);
  BackMarkChange(Self);
end;

procedure TIsoKDMap.SetBackSelect(const Value: TBitmap);
begin
  fBackSelect.Assign(Value);
  BackMarkChange(Self);
end;

procedure TIsoKDMap.SetDeleteLeft(const Value: TBitmap);
begin
  fDeleteLeft.Assign(Value);
  BackMarkChange(Self);
end;

procedure TIsoKDMap.SetDeleteRight(const Value: TBitmap);
begin
  fDeleteRight.Assign(Value);
  BackMarkChange(Self);
end;

procedure TIsoKDMap.SetDimension(NewWidth, NewHeight: Integer);
var
  x,y: integer;
begin
  SetLength(Tiles,NewWidth,NewHeight);
  SetLength(fSelect,NewWidth,NewHeight);
  for x:=0 to NewWidth-1 do
  begin
    for y:=0 to NewHeight-1 do
    begin
      if (x>fWidth-1) or (y>fHeight-1) then
      begin
        FillChar(Tiles[x,y],SizeOf(TMapTileEntry),#0);
        Tiles[x,y].Floor:=0;
        Tiles[x,y].TLeft:=-1;
        Tiles[x,y].BLeft:=-1;
        Tiles[x,y].Bright:=-1;
        Tiles[x,y].TRight:=-1;
        Tiles[x,y].Obj:=-1;
      end;
    end;
  end;
  fWidth:=NewWidth;
  fHeight:=NewHeight;
  fXOff:=0;
  fYOff:=100;
  UpdateScrollBar;
  Modify:=true;
end;

procedure TIsoKDMap.SetEnemyHotSpot(const Value: TBitmap);
begin
  fEnemyHotSpot.Assign(Value);
  BackMarkChange(Self);
end;

procedure TIsoKDMap.SetFile(const Value: String);
begin
  fFile := Value;
  if Assigned(fOnFileChange) then fOnFileChange(Self);
end;

procedure TIsoKDMap.SetForeMark(const Value: TBitmap);
begin
  fForeMark.Assign(Value);
  BackMarkChange(Self);
end;

procedure TIsoKDMap.SetForeSelect(const Value: TBitmap);
begin
  fForeSelect.Assign(Value);
  BackMarkChange(Self);
end;

procedure TIsoKDMap.SetFriendlyHotSpot(const Value: TBitmap);
begin
  fFriendlyHotSpot.Assign(Value);
  BackMarkChange(Self);
end;

procedure TIsoKDMap.SetGrid(const Value: TBitmap);
begin
  fGrid.Assign(Value);
  BackMarkChange(Self);
end;

procedure TIsoKDMap.SetHeight(const Value: Integer);
begin
  fHeight := Value;
  SetDimension(fWidth,fHeight);
end;

procedure TIsoKDMap.SetMapName(const Value: NameString);
begin
  fMapName := Value;
  Modify:=true;
end;

procedure TIsoKDMap.SetMark(const Value: TPoint);
begin
  fMark:=Value;
  if Assigned(fOnTileMove) then fOnTileMove(Self,fMark.x,fMark.y,mbLeft);
end;

procedure TIsoKDMap.SetModify(const Value: boolean);
begin
  fModify := Value;
  if Assigned(fOnChange) then fOnChange(Self);
end;

procedure TIsoKDMap.SetMouseMap(const Value: TBitmap);
begin
  fMouseMap.Assign(Value);
end;

procedure TIsoKDMap.SetPaintStruct(Struct: TSetTile);
begin
  fPaintStruct:=Struct;
  Draw;
end;

procedure TIsoKDMap.SetTile(X,Y: Integer;TileStruct: TSetTile);
begin
  if TileStruct.Floor.SetTile then
  begin
    Tiles[x,y].Floor:=TileStruct.Floor.Index;
    Modify:=true;
  end;
  if TileStruct.BLeft.SetTile then
  begin
    Tiles[x,y].BLeft:=TileStruct.BLeft.Index;
    Modify:=true;
  end;
  if TileStruct.BRight.SetTile then
  begin
    Tiles[x,y].Bright:=TileStruct.Bright.Index;
    Modify:=true;
  end;
  if TileStruct.TLeft.SetTile then
  begin
    Tiles[x,y].TLeft:=TileStruct.TLeft.Index;
    Modify:=true;
  end;
  if TileStruct.TRight.SetTile then
  begin
    Tiles[x,y].TRight:=TileStruct.TRight.Index;
    Modify:=true;
  end;
  if TileStruct.Obj.SetTile then
  begin
    Tiles[x,y].Obj:=TileStruct.Obj.Index;
    Modify:=true;
  end;
  if TileStruct.HotSpot.SetTile then
  begin
    Tiles[x,y].HotSpot:=TileStruct.HotSpot.Index;
    Modify:=true;
  end;
  Draw;
end;

procedure TIsoKDMap.SetWidth(const Value: Integer);
begin
  fWidth := Value;
  SetDimension(fWidth,fHeight);
end;

procedure TIsoKDMap.SetXOff(const Value: Integer);
begin
  fxOff := min(max(Value,-MaxXOffSet),-MinXOffSet);
//  fXOff:=Value;
  UpdateScrollBar;
end;

procedure TIsoKDMap.SetyOff(const Value: Integer);
begin
  fyOff := min(max(Value,-MaxYOffSet),100);
//  fYOff:=Value;
  UpdateScrollBar;
end;

function TIsoKDMap.TileAtPos(x, y: Integer): TPoint;
var
  xMap,yMap: Integer;
  xTemp,yTemp: Integer;
  xPos,yPos: Integer;
begin
  xMap:=(x-(Width div 2)-xOff) mod NormalTileWidth;
  {$IFDEF DEBUG}
    fDrawx:=(x-(Width div 2)-xOff);
    Draw;
  {$ENDIF}
  yMap:=(y-yoff) mod NormalTileHeight;
  xTemp:=(x-(Width div 2)-xOff) div NormalTileWidth;
  ytemp:=(y-yoff) div NormalTileHeight;
  if xMap<0 then dec(xTemp);
  if yMap<0 then dec(yTemp);
  yPos:=yTemp-xTemp;
  xPos:=yTemp+xTemp;
  if xMap<0 then inc(xMap,NormalTileWidth);
  if yMap<0 then inc(yMap,NormalTileHeight);

  if fMouseMap.Canvas.Pixels[xMap,yMap]=clGreen then inc(YPos);
  if fMouseMap.Canvas.Pixels[xMap,yMap]=clTeal  then inc(XPos);
  if fMouseMap.Canvas.Pixels[xMap,yMap]=clOlive then dec(YPos);
  if fMouseMap.Canvas.Pixels[xMap,yMap]=clMaroon then dec(XPos);
  
  xPos:=min(max(xPos,0),fWidth-1);
  yPos:=min(max(yPos,0),fHeight-1);
  result:=Point(xPos,yPos);
end;

procedure TIsoKDMap.UpdateScrollBar;
var
  ScrollInfo: TScrollInfo;
begin
  ScrollInfo.cbSize := SizeOf(ScrollInfo);
  if Length(Tiles)=0 then
  begin
    ScrollInfo.fMask := SIF_ALL;
    ScrollInfo.nPage:=0;
    ScrollInfo.nPos :=0;
    ScrollInfo.nMax :=0;
    ScrollInfo.nMin :=0;
    SetScrollInfo(Handle, SB_HORZ, ScrollInfo, True);
    ScrollInfo.nMax :=0;
    ScrollInfo.nMin :=0;
    ScrollInfo.nPage:=0;
    ScrollInfo.nPos :=0;
    SetScrollInfo(Handle, SB_VERT, ScrollInfo, True);
    exit;
  end;

  ScrollInfo.fMask := SIF_ALL;
  ScrollInfo.nMin := MinXOffSet;
  ScrollInfo.nMax := MaxXOffSet+Width;
  ScrollInfo.nPage:=Width;
  ScrollInfo.nPos := -xOff;
  ScrollInfo.nTrackPos:=0;
  SetScrollInfo(Handle, SB_HORZ, ScrollInfo, True);

  ScrollInfo.nMin := -100;
  ScrollInfo.nMax := MaxYOffSet+Height;
  ScrollInfo.nPage:=Height;
  ScrollInfo.nPos := -yOff;
  ScrollInfo.nTrackPos:=0;
  SetScrollInfo(Handle, SB_VERT, ScrollInfo, True);
end;

procedure TIsoKDMap.WMHScroll(var Message: TWMScroll);
begin
//  ShowMessage(IntToStr(xOff+48)+'/'+IntToStr(-MinXOffSet));
  case Message.ScrollCode of
    SB_TOP: xOff := MinXOffSet;
    SB_BOTTOM: xoff := MaxXOffSet;
    SB_LINEDOWN: xOff := max(xOff-48,-MaxXOffSet-Width);
    SB_LINEUP: xoff := min(xOff+48,-MinXOffSet);
    SB_PAGEDOWN: xOff := xOff-Width;
    SB_PAGEUP: xoff := xoff+Width;
    SB_THUMBPOSITION,
    SB_THUMBTRACK: xOff := -Message.Pos;
  end;
  Draw;
end;

procedure TIsoKDMap.WMLDown(var Msg: TWMLButtonDown);
var
  x,y: Integer;
begin
  if (Mark.x<0) or (Mark.x>MapWidth-1) then exit;
  if (Mark.y<0) or (Mark.y>MapHeight-1) then exit;

  PaintTile;
  Draw;
end;

procedure TIsoKDMap.WMLNCUp(var Msg: TWMLButtonUp);
begin
  MarkReg:=false;
  ReleaseCapture;
end;

procedure TIsoKDMap.WMLUp(var Msg: TWMLButtonUp);
begin
  MarkReg:=false;
  ReleaseCapture;
end;

procedure TIsoKDMap.WMMouseMove(var Msg: TWMMouseMove);
begin
(*  Aktu:=false;
  {$IFDEF DEBUG}
    LastXPos:=Msg.XPos;
  {$ENDIF}
  if ((NewMark.x<>mark.x) or (NewMark.y<>mark.y)) and MarkReg then
  begin
    RestoreRect(MarkStart,Mark);
    SelectRect(MarkStart,NewMark);
    Aktu:=true;
  end;
  if (NewMark.x<>mark.x) or (NewMark.y<>mark.y) then
  begin
    mark:=NewMark;
    Aktu:=true;
  end;
  LastX:=Msg.XPos;
  LastY:=Msg.YPos;
  if Msg.YPos>Height-40 then
  begin
    include(fDirect,sdDown);
    if fAutoScroll then
    begin
      YOff:=max(Yoff-24,-MaxYOffSet);
      Aktu:=true;
    end;
  end
  else exclude(fDirect,sdDown);
  if Msg.YPos<40 then
  begin
    include(fDirect,sdUp);
    if fAutoScroll then
    begin
      YOff:=min(Yoff+24,60);
      Aktu:=true;
    end;
  end
  else exclude(fDirect,sdUp);
  if Msg.XPos<40 then
  begin
    include(fDirect,sdleft);
    if fAutoScroll then
    begin
      XOff:=min(XOff+48,-MinXOffSet);
      Aktu:=true;
    end;
  end
  else exclude(fDirect,sdLeft);
  if Msg.XPos>Width-40 then
  begin
    include(fDirect,sdright);
    if fAutoScroll then
    begin
      XOff:=max(XOff-48,-MaxXOffSet);
      Aktu:=true;
    end;
  end
  else exclude(fDirect,sdRight);
  if Aktu then Draw;*)
  Mark:=TileAtPos(Msg.XPos,Msg.YPos);

  if (Msg.Keys and MK_LBUTTON)<>0 then
    PaintTile;

  Draw;
end;

procedure TIsoKDMap.WMNCMouseMove(var Msg: TWMNCMouseMove);
begin
  if (Msg.HitTest=HTVSCROLL) or (Msg.HitTest=HTHSCROLL) then
    fDirect:=[];
end;

procedure TIsoKDMap.WMSize(var Msg: TWMSize);
begin
  UpdateScrollBar;
  XOff:=Xoff;
  yOff:=yOff;
  Draw;
end;

procedure TIsoKDMap.WMVScroll(var Message: TWMScroll);
begin
  case Message.ScrollCode of
    SB_TOP: YOff := 60;
    SB_BOTTOM: Yoff := MaxYOffSet;
    SB_LINEDOWN: yOff := max(yOff-24,-MaxYOffSet);
    SB_LINEUP: Yoff := min(Yoff+24,60);
    SB_PAGEDOWN: yOff := max(yOff-Height,-MaxYOffSet);
    SB_PAGEUP: Yoff := min(Yoff+Height,60);
    SB_THUMBPOSITION,
    SB_THUMBTRACK: yOff := -Message.Pos;
  end;
  Draw;
end;

end.
