{********************************************************************************
*										*
* Copyright (C) 2004  X-Force Team - jim_raynor@web.de				*
*										*
* This file is part of X-Force: Fight For Destiny (X-Force).			*
*										*
* X-Force is free software; you can redistribute it and/or modify		*
* it under the terms of the GNU General Public License as published by		*
* the Free Software Foundation; either version 2 of the License, or		*
* (at your option) any later version.						*
*										*
* X-Force is distributed in the hope that it will be useful,			*
* but WITHOUT ANY WARRANTY; without even the implied warranty of		*
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the			*
* GNU General Public License for more details.					*
*										*
* You should have received a copy of the GNU General Public License		*
* along with X-Force; if not, write to the Free Software			*
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA	*
*										*
*********************************************************************************
*										*
* Rendert die Erde im Geoscape in ein Surface					*
*										*
********************************************************************************}

{$I ../Settings.inc}

unit GlobeRenderer;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  DirectDraw, DXDraws, Direct3D, Blending, D3DUtils, math, D3DRender,
  DXContainer, TraceFile, ArchivFile, Defines, XForce_Types;

const
  NR_OF_BANDS =  50;
  NR_OF_STEPS =  50;

  // Gradnetz konstante
  XGRAD_STEP  =  20;
  YGRAD_STEP  =  20;

  PRECISION   =   4;

  D3DFVF_DIFFUSEVERTEX = D3DFVF_XYZ or D3DFVF_DIFFUSE;

type
  PDiffuseVertex = ^TDiffuseVertex;
  TDiffuseVertex = packed record
    x: TD3DValue;             (* Homogeneous coordinates *)
    y: TD3DValue;
    z: TD3DValue;
    Col : TD3DColor;
  end;

  PCustomVertex = ^TCustomVertex;
  TCustomVertex = packed record
    x: TD3DValue;             (* Homogeneous coordinates *)
    y: TD3DValue;
    z: TD3DValue;
    Col : TD3DColor;
    tu : TD3DValue;
    tv : TD3DValue;
  end;

  TDiffuseVertexArray = Array of TDiffuseVertex;

  TSensorField = record
    Pos        : TFloatPoint;
    Range      : Integer;
    Color      : TColor;
    Visible    : Boolean;
    WithAlpha  : Boolean;
    Vertizes   : TDiffuseVertexArray;
  end;

  TSensorFieldArray = Array of TSensorField;

  TGlobeRenderer = class(TObject)
  private
    fDXDraw      : TDXContainer;
    fSurface     : TDirectDrawSurface;

    fTextur      : TDirect3DTexture2;

    fMatrix      : TD3DMatrix;
    fCompatible  : Boolean;
    fCopySystem  : Boolean;
    fCompSurface : TDirectDrawSurface;

    fEarthSurface: TDirectDrawSurface;

    // Vertex der Kugel
    fVertices        : array[0..NR_OF_BANDS-1,0..(NR_OF_STEPS*2)+1] of TCustomVertex;
    fXGradVertices   : array[0..((360 div XGRAD_STEP)*(180 div PRECISION))] of TDiffuseVertex;
    fYGradVertices   : array[0..(180 div YGRAD_STEP),0..(360 div PRECISION)] of TDiffuseVertex;

    fSensorFields    : TSensorFieldArray;

    function GetTextur: TDirectDrawSurface;
    function GetSurface: TDirectDrawSurface;
    function GetSystemSurface: TDirectDrawSurface;
    { Private-Deklarationen }
  protected
    procedure RestoreDirectX;
    procedure LoadTexture;

    procedure CreateSphere;
    procedure CreateGradNetz;

    { Protected-Deklarationen }
  public
    constructor Create(DXDraw: TDXContainer);
    destructor Destroy;override;

    procedure SetSize(Width,Height: Integer);

    procedure SetTextur(Bitmap: TBitmap);

    procedure SetMatrix(const Mat: TD3DMatrix;Zoom: double);
    procedure RenderEarth;

    procedure ClearSensorFields;
    procedure CreateSensorField(Pos: TFloatPoint; Range: Integer; Col: TColor);

    procedure SetCompatibleMode(Compatible: Boolean);

    property EarthSurface   : TDirectDrawSurface read GetSurface;
    property SystemSurface  : TDirectDrawSurface read GetSystemSurface;

    property Textur : TDirectDrawSurface read GetTextur;
    { Public-Deklarationen }
  end;

var
  GradNetz      : Boolean = true;
  ShowSensorMap : Boolean = true;

const
  EarthSize = 120;
implementation

uses
  KD4Utils, array_utils;

{ TGlobeRenderer }

procedure TGlobeRenderer.ClearSensorFields;
var
  Dummy: Integer;
begin
  for Dummy:=high(fSensorFields) downto 0 do
  begin
    if not fSensorFields[Dummy].Visible then
      DeleteArray(Addr(fSensorFields),TypeInfo(TSensorFieldArray),Dummy)
    else
      fSensorFields[Dummy].Visible:=false;
  end;
end;

constructor TGlobeRenderer.Create(DXDraw: TDXContainer);
begin
  fDXDraw:=DXDraw;
  fSurface:=nil;
  fDXDraw.AddRestoreFunction(RestoreDirectX);
  CreateSphere;
end;

procedure TGlobeRenderer.CreateGradNetz;
var
  YGrad          : double;
  XGrad          : double;
  I,j            : Integer;
  Ptr            : PDiffuseVertex;
  XGradStep      : double;
  YGradStep      : double;

  procedure SetVertex(RadX,RadY: double);
  begin
    RadY:=-max((-Pi/2),min(RadY,(Pi/2)));
    Ptr.x:=System.cos(RadX)*System.Cos(RadY);
    Ptr.y:=System.sin(RadY);
    Ptr.z:=System.sin(RadX)*System.Cos(RadY);
//    Ptr.Col:=D3DRGB(max(0.1,Ptr.z),max(0.1,Ptr.z),max(0.1,Ptr.z));
    Ptr.Col:=$400080FF;
    inc(Ptr);
  end;

begin
  // Netz f�r Breitengrade
  YGrad:=-Pi/2;
  YGradStep:= PRECISION * g_DEGTORAD;
  XGradStep:= XGRAD_STEP * g_DEGTORAD;
  XGrad:=0;
  Ptr:=Addr(fXGradVertices[0]);
  for i:=0 to (360 div XGRAD_STEP)-1 do
  begin
    for j:=0 to (180 div PRECISION)-1 do
    begin
      SetVertex(XGrad,YGrad);
      YGrad:=YGrad+YGradStep;
    end;
    XGrad:=XGrad+XGradStep;
    YGradStep:=-YGradStep;
  end;
  SetVertex(XGrad,YGrad);

  // Netz f�r L�ngengrade
  YGrad:=-Pi/2;
  XGradStep:= PRECISION * g_DEGTORAD;
  YGradStep:= YGRAD_STEP * g_DEGTORAD;
  for i:=0 to (180 div YGRAD_STEP)-1 do
  begin
    Ptr:=Addr(fYGradVertices[i]);
    XGrad:=0;
    for j:=0 to (360 div PRECISION)-1 do
    begin
      SetVertex(XGrad,YGrad);
      XGrad:=XGrad+XGradStep;
    end;
    SetVertex(XGrad,YGrad);
    YGrad:=YGrad+YGradStep;
  end;
end;

procedure TGlobeRenderer.CreateSensorField(Pos: TFloatPoint; Range: Integer;
  Col: TColor);
var
  YGrad          : double;
  YGradMax       : double;
  YGradStep      : double;
  XGradStep      : double;
  XGrad          : double;
  YTop           : double;
  I,j            : Integer;
  Ptr            : PDiffuseVertex;
  Vertizes       : TDiffuseVertexArray;
  XMatrix,YMatrix: TD3DMatrix;
  RotMatrix      : TD3DMatrix;
  Color          : TColor;
  Dummy          : Integer;

  Steps          : Integer;

  procedure SetVertex(RadX,RadY: double);
  var
    Vektor: TD3DVector;
  begin
    RadY:=-max((-Pi/2),min(RadY,(Pi/2)));

    Vektor.x:=System.cos(RadX)*System.Cos(RadY);
    Vektor.y:=System.sin(RadY);
    Vektor.z:=System.sin(RadX)*System.Cos(RadY);

    D3DMath_VectorMatrixMultiply(Vektor,Vektor,YMatrix);
    D3DMath_VectorMatrixMultiply(Vektor,Vektor,XMatrix);

    Ptr.X:=Vektor.X;
    Ptr.Y:=Vektor.Y;
    Ptr.Z:=Vektor.Z;

//    Ptr.Col:=D3DRGB(max(0.1,Ptr.z),max(0.1,Ptr.z),max(0.1,Ptr.z));
    Ptr.Col:=Color;
    inc(Ptr);
  end;

begin
  for Dummy:=0 to high(fSensorFields) do
  begin
    if (fSensorFields[Dummy].Pos.X=Pos.X) and (fSensorFields[Dummy].Pos.Y=Pos.Y) and
       (fSensorFields[Dummy].Range=Range) and (fSensorFields[Dummy].Color=Col) and
       (fSensorFields[Dummy].WithAlpha=Enabled3DAlphaBlend) then
    begin
      fSensorFields[Dummy].Visible:=true;
      exit;
    end;
  end;

  Color:=$40000000 or ($00FFFFFF and D3DRGB(GetRValue(Col)/255,GetGValue(Col)/255,GetBValue(Col)/255));
  D3DUtil_SetRotateZMatrix(YMatrix,DegToRad(Pos.Y));
  D3DUtil_SetRotateYMatrix(XMatrix,DegToRad(Pos.X-180));

  D3DMath_MatrixMultiply(RotMatrix,YMatrix,XMatrix);

  // Netz f�r L�ngengrade
  if Enabled3DAlphaBlend then
  begin
    YGradMax:=DegToRad(Range/20005.5767*180);

    Steps:=max(1,round(NR_OF_BANDS / 180*RadToDeg(YGradMax)));

    YGradStep:=YGradMax/Steps;
    XGradStep:=360 / NR_OF_STEPS * g_DEGTORAD;

    SetLength(Vertizes,Steps*((NR_OF_STEPS+1)*2));

    Ptr:=Addr(Vertizes[0]);
    YGrad:=-Pi/2;
    for i:=0 to Steps-1 do
    begin
      YTop:=YGrad;
      YGrad:=YGrad+YGradStep;
      SetVertex(0,YTop);
      XGrad:=0;
      for j:=0 to (NR_OF_STEPS)-1 do
      begin
        SetVertex(XGrad,YGrad);
        XGrad:=XGrad+XGradStep;
        SetVertex(XGrad,YTop);
      end;
      SetVertex(XGrad,YGrad);
    end;
  end
  else
  begin
    SetLength(Vertizes,(360 div PRECISION)+1);

    // Nur einen Kreis
    YGrad:=DegToRad(Range/20005.5767*180)-Pi/2;
    XGradStep:=PRECISION * g_DEGTORAD;
    YGradStep:=YGRAD_STEP * g_DEGTORAD;

    Ptr:=Addr(Vertizes[0]);
    XGrad:=0;
    for j:=0 to (360 div PRECISION)-1 do
    begin
      SetVertex(XGrad,YGrad);
      XGrad:=XGrad+XGradStep;
    end;
    SetVertex(XGrad,YGrad);
  end;

  SetLength(fSensorFields,length(fSensorFields)+1);

  fSensorFields[high(fSensorFields)].Pos:=Pos;
  fSensorFields[high(fSensorFields)].Range:=Range;
  fSensorFields[high(fSensorFields)].Color:=Col;
  fSensorFields[high(fSensorFields)].Visible:=true;
  fSensorFields[high(fSensorFields)].WithAlpha:=Enabled3DAlphaBlend;
  fSensorFields[high(fSensorFields)].Vertizes:=Vertizes;
end;

procedure TGlobeRenderer.CreateSphere;
const
  YGradStep = 180 / (NR_OF_BANDS-1) * g_DEGTORAD;
  XGradStep = 360 / NR_OF_STEPS * g_DEGTORAD;
var
  YGrad          : double;
  XGrad          : double;
  I,j            : Integer;
  YTop,YBottom   : double;
  Ptr            : PCustomVertex;

  procedure SetVertex(RadX,RadY: double);
  begin
    RadY:=-max(-Pi/2,min(RadY,Pi/2));
    Ptr.x:=System.cos(RadX)*System.Cos(RadY);
    Ptr.y:=System.sin(RadY);
    Ptr.z:=System.sin(RadX)*System.Cos(RadY);
//    Ptr.Col:=D3DRGB(max(0.1,Ptr.z),max(0.1,Ptr.z),max(0.1,Ptr.z));
    Ptr.Col:=$40FFFFFF;
    Ptr.tu:=-RadX/(Pi*2);

    Ptr.tv:=-(RadY+(Pi/2))/Pi;
    inc(Ptr);
  end;

begin
  YGrad:=-Pi/2;
  for i:=0 to NR_OF_BANDS-1 do
  begin
    Ptr:=Addr(fVertices[I,0]);
    YTop:=YGrad;
    YBottom:=YGrad+YGradStep;
    SetVertex(0,YTop);
    XGrad:=0;
    for j:=0 to (NR_OF_STEPS)-1 do
    begin
      SetVertex(XGrad,YBottom);
      XGrad:=XGrad+XGradStep;
      SetVertex(XGrad,YTop);
    end;
    SetVertex(Pi*2,YBottom);
    YGrad:=YGrad+YGradStep;
  end;

  CreateGradNetz;
end;

destructor TGlobeRenderer.Destroy;
begin
  if fTextur<>nil then
    fTextur.Free;

  inherited;
end;

function TGlobeRenderer.GetSurface: TDirectDrawSurface;
begin
  if fCompatible then
    result:=fCompSurface
  else
    result:=fSurface;
end;

function TGlobeRenderer.GetSystemSurface: TDirectDrawSurface;
begin
  if fCopySystem then
  begin
    fCompSurface.Draw(0,0,fSurface,false);
    fCopySystem:=false;
  end;

  result:=fCompSurface;
end;

function TGlobeRenderer.GetTextur: TDirectDrawSurface;
begin
  // Ist das Texturformat ein anderes, dann muss ein neues Surface erzeugt werden.
  if fEarthSurface=nil then
  begin
    fEarthSurface:=TDirectDrawSurface.Create(fDXDraw.DDraw);
    fEarthSurface.SystemMemory:=true;
    fEarthSurface.SetSize(fTextur.Surface.Width,fTextur.Surface.Height);
    fEarthSurface.Fill(0);
    fEarthSurface.Canvas.CopyRect(fEarthSurface.ClientRect,fTextur.Surface.Canvas,fTextur.Surface.ClientRect);
    fEarthSurface.Canvas.Release;
    fTextur.Surface.Canvas.Release;
  end;

  result:=fEarthSurface;
end;

procedure TGlobeRenderer.LoadTexture;
var
  Archiv: TArchivFile;
  Bitmap: TBitmap;
begin
  // Textur f�r die Erde laden
  Archiv:=TArchivFile.Create;
  Archiv.OpenArchiv(FDataFile,true);
  Archiv.OpenRessource(RNEarthShem);
  Bitmap:=TBitmap.Create;
  Bitmap.LoadFromStream(Archiv.Stream);
  SetTextur(Bitmap);
  Archiv.Free;

  Bitmap.Free;
end;

procedure TGlobeRenderer.RenderEarth;
var
  r        : TD3DRect;
  Dummy    : Integer;
  NoTarget : Boolean;
begin
  if (fTextur=nil) or (fSurface=nil) then
    exit;

  {  Clear Screen  }
  r.x1 := 0;
  r.y1 := 0;
  r.x2 := fSurface.Width;
  r.y2 := fSurface.Height;

  D3DDisabledAlpha(fDXDraw);
  NoTarget:=false;
  if not (fDXDraw.D3DDevice7.SetRenderTarget(fSurface.ISurface7,0)=DD_OK) then
    NoTarget:=true;

//  fDXDraw.D3DDevice7.SetRenderState(D3DRENDERSTATE_ZENABLE, Ord(FALSE));
//  fDXDraw.D3DDevice7.SetRenderState(D3DRENDERSTATE_FILLMODE, Ord(D3DFILL_WIREFRAME));

  CheckDirectXResult(fDXDraw.D3DDevice7.BeginScene);

  if fDXDraw.ZBuffer<>nil then
    CheckDirectXResult(fDXDraw.D3DDevice7.Clear(1, @r, D3DCLEAR_TARGET or D3DCLEAR_ZBUFFER, $000000, 1, 0))
  else
    CheckDirectXResult(fDXDraw.D3DDevice7.Clear(1, @r, D3DCLEAR_TARGET, $000000, 1, 0));

  // Textur
  CheckDirectXResult(fDXDraw.D3DDevice7.SetTexture( 0, fTextur.Surface.IDDSurface7 ));

  // Anti-Aliasing
  fDXDraw.D3DDevice7.SetTextureStageState(0, D3DTSS_MAGFILTER, DWord(D3DTFG_LINEAR));
  fDXDraw.D3DDevice7.SetTextureStageState(0, D3DTSS_MINFILTER, DWord(D3DTFN_LINEAR));
  fDXDraw.D3DDevice7.SetTextureStageState(0, D3DTSS_MIPFILTER, DWord(D3DTFN_LINEAR));

  fDXDraw.D3DDevice7.SetRenderState(D3DRENDERSTATE_CULLMODE, Ord(D3DCULL_CCW));
  fDXDraw.D3DDevice7.SetRenderState(D3DRENDERSTATE_LIGHTING, Cardinal( FALSE ) );
  fDXDraw.D3DDevice7.SetRenderState(D3DRENDERSTATE_COLORKEYENABLE, Ord(FALSE));

  CheckDirectXResult(fDXDraw.D3DDevice7.DrawPrimitive(D3DPT_TRIANGLESTRIP, D3DFVF_XYZ or D3DFVF_DIFFUSE or D3DFVF_TEX1, fVertices[0],NR_OF_BANDS*((NR_OF_STEPS*2)+2),D3DDP_WAIT));

  if GradNetz or ShowSensorMap then
  begin
    CheckDirectXResult(fDXDraw.D3DDevice7.SetTexture(0,nil));

    D3DEnabledAlpha(fDXDraw);

    if GradNetz then
    begin
      CheckDirectXResult(fDXDraw.D3DDevice7.DrawPrimitive(D3DPT_LINESTRIP,D3DFVF_DIFFUSEVERTEX,fXGradVertices,length(fXGradVertices),0));

      for Dummy:=0 to high(fYGradVertices) do
        CheckDirectXResult(fDXDraw.D3DDevice7.DrawPrimitive(D3DPT_LINESTRIP,D3DFVF_DIFFUSEVERTEX,fYGradVertices[Dummy],length(fYGradVertices[Dummy]),0));
    end;

    if ShowSensorMap then
    begin
      for Dummy:=0 to high(fSensorFields) do
      begin
        if fSensorFields[Dummy].Visible then
        begin
          if Enabled3DAlphaBlend then
            fDXDraw.D3DDevice7.DrawPrimitive(D3DPT_TRIANGLESTRIP,D3DFVF_DIFFUSEVERTEX,fSensorFields[Dummy].Vertizes[0],length(fSensorFields[Dummy].Vertizes),0)
          else
            fDXDraw.D3DDevice7.DrawPrimitive(D3DPT_LINESTRIP,D3DFVF_DIFFUSEVERTEX,fSensorFields[Dummy].Vertizes[0],length(fSensorFields[Dummy].Vertizes),0);
        end;
      end;
    end;

    D3DDisabledAlpha(fDXDraw);
  end;

  // Anti-Aliasing abschalten
  fDXDraw.D3DDevice7.SetTextureStageState(0, D3DTSS_MAGFILTER, DWord(D3DTFG_POINT));
  fDXDraw.D3DDevice7.SetTextureStageState(0, D3DTSS_MINFILTER, DWord(D3DTFN_POINT));
  fDXDraw.D3DDevice7.SetTextureStageState(0, D3DTSS_MIPFILTER, DWord(D3DTFN_POINT));

  fDXDraw.D3DDevice7.EndScene;

  if NoTarget then
  begin
    fSurface.Draw(0,0,fDXDraw.Surface,false);
    fDXDraw.Surface.Fill(0);
  end;

  if fCompatible then
  begin
    fCompSurface.Draw(0,0,fSurface);
    fCopySystem:=false;
  end
  else
    fCopySystem:=true;
end;

procedure TGlobeRenderer.RestoreDirectX;
var
  Width, height: Integer;
begin
  if fSurface<>nil then
  begin
    Width:=fSurface.Width;
    Height:=fSurface.Height;
    fSurface.Free;
    fSurface:=nil;

    SetSize(Width,height);
  end;

  if fEarthSurface<>nil then
    FreeAndNil(fEarthSurface);
end;

procedure TGlobeRenderer.SetCompatibleMode(Compatible: Boolean);
begin
  fCompSurface:=TDirectDrawSurface.Create(fDXDraw.DDraw);
  fCompSurface.SystemMemory:=true;
  fCompSurface.SetSize(fSurface.Width,fSurface.Height);

  fCompatible:=Compatible;
end;

procedure TGlobeRenderer.SetMatrix(const Mat: TD3DMatrix;Zoom: double);
var
  vp      : TD3DViewport7;
  matProj : TD3DMatrix;
  matView : TD3DMatrix;
begin
  fMatrix:=Mat;

  // Viewport setzen
  FillChar(vp, SizeOf(vp), 0);
  vp.dwX := 0;
  vp.dwY := 0;
  vp.dwWidth := fSurface.Width;
  vp.dwHeight := fSurface.Height;
  vp.dvMinZ := 0;
  vp.dvMaxZ := 10;

  fDXDraw.D3DDevice7.SetViewport(vp);

  // Projectionsmatrix
  FilLChar(matProj, SizeOf(matProj), 0);
  matProj._11 :=    1;
  matProj._22 :=    1;
  matProj._33 :=    0.1;
  matProj._44 :=    1;
  fDXDraw.D3DDevice7.SetTransform( D3DTRANSFORMSTATE_PROJECTION, matProj );

  // Viewmatrix setzen
  FilLChar(matView, SizeOf(matView), 0);
  matView._11 := fSurface.Height/fSurface.Width*Zoom;
  matView._22 := Zoom;
  matView._33 := Zoom;
  matView._41 := -0.2;
  matView._44 := 1;
  fDXDraw.D3DDevice7.SetTransform( D3DTRANSFORMSTATE_VIEW, matView);

  // Rotationsmatrix
  fDXDraw.D3DDevice7.SetTransform( D3DTRANSFORMSTATE_WORLD, fMatrix);

  {  Material  }
{  FillChar(mtrl, SizeOf(mtrl), 0);
  mtrl.ambient.r := 1.0;
  mtrl.ambient.g := 1.0;
  mtrl.ambient.b := 1.0;
  mtrl.diffuse.r := 1.0;
  mtrl.diffuse.g := 0.0;
  mtrl.diffuse.b := 1.0;
  fDXDraw.D3DDevice7.SetMaterial(mtrl);
  fDXDraw.D3DDevice7.SetRenderState(D3DRENDERSTATE_AMBIENT, $00FFFFFF);}

end;

procedure TGlobeRenderer.SetSize(Width, Height: Integer);
begin
  if fSurface=nil then
  begin
    fSurface:=TDirectDrawSurface.Create(fDXDraw.DDraw);
    fSurface.SystemMemory:=false;
    fSurface.Do3DStuff:=true;
  end;
  fSurface.SetSize(Width,Height);
  fSurface.Fill(0);
  LoadTexture;
end;

procedure TGlobeRenderer.SetTextur(Bitmap: TBitmap);
begin
  if fTextur<>nil then
    fTextur.Free;

  fTextur:=TDirect3DTexture2.Create(fDXDraw,Bitmap,false);

end;

end.
