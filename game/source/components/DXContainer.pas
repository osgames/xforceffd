{********************************************************************************
*										*
* Copyright (C) 2004  X-Force Team - jim_raynor@web.de				*
*										*
* This file is part of X-Force: Fight For Destiny (X-Force).			*
*										*
* X-Force is free software; you can redistribute it and/or modify		*
* it under the terms of the GNU General Public License as published by		*
* the Free Software Foundation; either version 2 of the License, or		*
* (at your option) any later version.						*
*										*
* X-Force is distributed in the hope that it will be useful,			*
* but WITHOUT ANY WARRANTY; without even the implied warranty of		*
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the			*
* GNU General Public License for more details.					*
*										*
* You should have received a copy of the GNU General Public License		*
* along with X-Force; if not, write to the Free Software			*
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA	*
*										*
*********************************************************************************
*										*
* TDXContainer 	- Basiskomponente f�r die Oberfl�chenverwaltung			*
* TDXPage	- Basiskomponente f�r die verschiedenen Ansichten		*
* TDXComponent	- Basiskomponente f�r visuelle Elemente in X-Force		*
*										*
********************************************************************************}

{$I ../Settings.inc}

unit DXContainer;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  DXDraws, Dib, math, Defines, XForce_types,DirectDraw, TraceFile, DXClass, Blending,
  contnrs, PowerDraw, BassPlayer, ArchivFile, MPlayer, MusikList;

const
  ScrollTempo : Integer = 10;
  AnimTime    = 25;
type

{$IFDEF DEBUGMODE}
  {.$DEFINE FRAMECOMPONENT}
{$ENDIF}
  TShowMessageBox         = procedure(Text, Caption: String;Question: boolean;var Result: TModalResult) of Object;
  TShowSpecialMessageBox  = procedure(Text, Caption,FirstButton,SecondButton: String;var Result: TModalResult) of Object;
  TDXPage                 = class;
  TDXContainer            = class;
  TPageAnimation          = (paNone,paLeftToRight,paRightToLeft,paUpToBottom,
                             paBottomToUp);

  TPageChangeEvent        = procedure(Sender: TDXPage;Page: Integer) of object;
  TScrollDirection        = (sdUp,sdDown,sdLeft,sdRight);

  TDXComponent = class(TObject)
  private
    fHeight         : Integer;
    fTop            : Integer;
    fWidth          : Integer;
    fLeft           : Integer;
    fOnMouseOver    : TNotifyEvent;
    fOnMouseLeave   : TNotifyEvent;
    fOnMouseMove    : TMouseMoveEvent;
    fOnMouseDown    : TMouseEvent;
    fOnMouseUp      : TMouseEvent;
    fOnClick        : TNotifyEvent;
    fVisible        : boolean;
    fEnabled        : boolean;
    fTempEnabled    : boolean;
    fDeactivate     : boolean;
    fDXPage         : TDXPage;
    fContainer      : TDXContainer;
    fTabStop        : boolean;
    fOnKeyPress     : TKeyEvent;
    fHint           : String;
    fOnKeyDown      : TKeyEvent;
    fFont           : TFont;
    fHintShow       : boolean;
    fOnKeyUp        : TKeyEvent;
    fOnDblClick     : TMouseEvent;
    fTag            : Integer;
    fDirectDraw     : boolean;
    procedure SetHeight(const Value: Integer);
    procedure SetLeft(const Value: Integer);
    procedure SetTop(const Value: Integer);
    procedure SetWidth(const Value: Integer);
    procedure SetVisible(const Value: boolean);
    procedure SetEnabled(const Value: boolean);
    procedure SetFont(const Value: TFont);
    function GetClientRect: TRect;
    function GetBottom: Integer;
    function GetRight: Integer;
    function GetHasFocus: boolean;
  protected
    fMouseRegion: HRGN;
    procedure SetComponentHint(const Value: String);virtual;
    procedure CreateMouseRegion;virtual;
    procedure Restore;virtual;
    procedure DoScroll(Direction: TScrollDirection;Pos: TPoint);virtual;
    procedure KillFocus;virtual;
    procedure GetFocus;virtual;
    procedure FontChange(Sender: TObject);virtual;
    function DrawEnabled: boolean;
  public
    constructor Create(Page: TDXPage);virtual;
    destructor Destroy;override;
    procedure Draw(Surface: TDirectDrawSurface ;var Mem: TDDSurfaceDesc);virtual;
    procedure ReDrawRect(DrawRect: TRect;Surface: TDirectDrawSurface ;var Mem: TDDSurfaceDesc);virtual;
    procedure MouseMove(X,Y: Integer);virtual;
    procedure MouseOver;virtual;
    procedure MouseLeave;virtual;
    procedure MouseDown(Button: TMouseButton;X,Y: Integer);virtual;
    procedure DblClick(Button: TMouseButton;x,y: Integer);virtual;
    procedure MouseUp(Button: TMouseButton;X,Y: Integer);virtual;
    procedure KeyPress(var Key: Word;State: TShiftState);virtual;
    procedure KeyDown(var Key: Word;State: TShiftState);virtual;
    procedure KeyUp(var Key: Word;State: TShiftState);virtual;
    procedure SetFocus;
    procedure DoClick;virtual;
    procedure DeactiveControl;virtual;
    procedure ReactiveControl;
    procedure Resize(var NewLeft,NewTop,NewWidth,NewHeight: Integer);virtual;
    procedure VisibleChange;virtual;
    procedure EnableChange;virtual;
    procedure Redraw;virtual;
    procedure RecreateMouseRegion;
    procedure SetRect(Left,Top,Width,Height: Integer);
    procedure RestoreFontChange;
    function IsCaptured: boolean;
    property Left            : Integer read fLeft write SetLeft;
    property Top             : Integer read fTop write SetTop;
    property Width           : Integer read fWidth write SetWidth;
    property Height          : Integer read fHeight write SetHeight;
    property Right           : Integer read GetRight;
    property Bottom          : Integer read GetBottom;
    property Parent          : TDXPage read fDXPage;
    property Container       : TDXContainer read fContainer;
    property ClientRect      : TRect read GetClientRect;
    property MouseRegion     : HRGN read fMouseRegion;
    property Hint            : String read fHint write SetComponentHint;
    property OnMouseOver     : TNotifyEvent read fOnMouseOver write fOnMouseOver;
    property OnMouseLeave    : TNotifyEvent read fOnMouseLeave write fOnMouseLeave;
    property OnMouseMove     : TMouseMoveEvent read fOnMouseMove write fOnMouseMove;
    property OnMouseDown     : TMouseEvent read fOnMouseDown write fOnMouseDown;
    property OnMouseUp       : TMouseEvent read fOnMouseUp write fOnMouseUp;
    property OnDblClick      : TMouseEvent read fOnDblClick write fOnDblClick;
    property OnKeyPress     : TKeyEvent read fOnKeyPress write fOnKeyPress;
    property OnKeyDown      : TKeyEvent read fOnKeyDown write fOnKeyDown;
    property OnKeyUp        : TKeyEvent read fOnKeyUp write fOnKeyUp;
    property OnClick        : TNotifyEvent read fOnClick write fOnClick;
    property Visible        : boolean read fVisible write SetVisible;
    property Enabled        : boolean read fEnabled write SetEnabled;
    property TabStop        : boolean read fTabStop write fTabStop;
    property HasFocus       : boolean read GetHasFocus;
    property DirectDraw     : boolean read fDirectDraw write fDirectDraw;
    property Font           : TFont read fFont write SetFont;
    property Tag            : Integer read fTag write fTag;
  end;

  THotKeyEntry = record
    Zeichen   : Char;
    Component : TDXComponent;
    Event     : TNotifyEvent;
  end;

  TSystemKeyFunction = procedure(Data: Integer) of object;
  TSystemKeyEntry = record
    Zeichen : Char;
    Proc    : TSystemKeyFunction;
    Data    : Integer;
  end;

  TDXPage = class(TObject)
  private
    fControls             : TList;
    fBackground           : TBitmap;
    fContainer            : TDXContainer;
    fOnKeyPress           : TKeyEvent;
    fHotKeys              : Array of THotKeyEntry;
    fSystemKeys           : Array of TSystemKeyEntry;
    fAnimation            : TPageAnimation;
    fHint                 : String;
    fCaption              : String;
    fPageIndex            : Integer;
    fBackGroundName       : String;
    fShowSpecialMessage   : TShowSpecialMessageBox;
    fShowPageModal        : Boolean;
    procedure SetPageHint(const Value: String);
    function GetBack: TBitmap;
    procedure SetBackGroundName(const Value: String);

    procedure ReadBackGround;
  protected
    procedure Restore;virtual;
    procedure PerformSystemKey(Key: Char);
    procedure HandleFKey(FKey: Byte);virtual;
  public
    constructor Create(Container: TDXContainer);virtual;
    destructor Destroy;override;
    procedure AddComponent(Component: TDXComponent);
    procedure RemoveComponent(Component: TDXComponent);
    procedure Redraw(Surface: TDirectDrawSurface);
    procedure Activate(LoadDefaults: boolean=true);
    procedure KeyPress(var Key: Word;State: TShiftState);virtual;
    procedure DeactivatePage;virtual;
    function CloseModal: boolean;
    procedure SetHint(Hint: String);virtual;
    procedure SetDefaults;virtual;
    procedure LeaveModal;virtual;
    procedure SetFont(Font: TFont);
    procedure SetButtonFont(Font: TFont);
    procedure PageShown;virtual;
    procedure ChangePage(Page: Integer);
    procedure RegisterHotKey(Taste: Char;Control: TDXComponent; ClickEvent: TNotifyEvent = nil);
    procedure DeleteHotKey(Zeichen: Char;Control: TDXComponent; ClickEvent: TNotifyEvent = nil);
    procedure RegisterSystemKey(Taste: Char;Func: TSystemKeyFunction;Data: Integer);
    procedure DeleteSystemKey(Taste: Char;Func: TSystemKeyFunction;Data: Integer);
    function ReshowPage: boolean;virtual;
    property NeedSpMessageBox : TShowSpecialMessageBox read fShowSpecialMessage write fShowSpecialMessage;
    property Container        : TDXContainer read fContainer;
    property BackGround       : TBitmap read GetBack;
    property BackGroundName   : String read fBackGroundName write SetBackGroundName;
    property OnKeyPress       : TKeyEvent read fOnKeyPress write fOnKeyPress;
    property Animation        : TPageAnimation read fAnimation write fAnimation;
    property Hint             : String read fHint write SetPageHint;
    property Caption          : String read fCaption write fCaption;

    property PageIndex        : Integer read fPageIndex write fPageIndex;
    property ShowPageModal    : Boolean read fShowPageModal write fShowPageModal;
  end;

  TDXContainerStatus = (csReady, csDeactivated);

  TDXContainer = class(TDXDraw)
  private
    fSystemList           : TDXImageList;
    fActivePage           : Integer;
    fControls             : TList;
    fPages                : TList;
    fOverControl          : TDXComponent;
    fSavedRect            : TRect;
    fLock                 : Integer;
    fCaptureControl       : TDXComponent;
    fFocusControl         : TDXComponent;
    fTimer                : TDXTimer;
    fShowCursor           : Integer;
    fPageAnim             : boolean;
    fMessageLock          : Integer;
    fLoadGame             : Integer;
    fMouseMove            : boolean;
    fTempSurface          : TDirectDrawSurface;
    fBackGround           : TDirectDrawSurface;
    fMVolume              : Integer;
    fImageList            : TDXImageList;
    fFrameTime            : Integer;

    fCursor               : TDXMouseCursor;
    fFrame                : Integer;

    fLastFrame            : TLargeInteger;
    fFrameFunc            : Array of TFrameProcRecord;
    fFrameLock            : Boolean;
    fScrolling            : boolean;
    fMouseAnim            : boolean;
    fMouseShadow          : boolean;
    fStop                 : boolean;
    fSound                : boolean;
    fPageStack            : TObjectStack;
    fClearStack           : boolean;
    fPowerGraph           : TPowerGraph;
    fBassPlayer           : TBASSPlayer;

    fMediaPlayer          : TMediaPlayer;
    fPlayingMusic         : boolean;
    fLooping              : boolean;
    fMusicCategory        : String;

    fGameSetSounds        : TStringList;

    fStatus               : TDXContainerStatus;

    fGamma                : Integer;
    fOrgGammaRamp         : TDDGammaRamp;
    procedure SetSystemBitmap(const Index: Integer; const Value: TPicture);
    procedure SetMousePos(const Value: TPoint);
    procedure SetPage(const Value: TDXPage);
    procedure SetVolume(const Value: Integer);
    procedure SaveMouseRect(Pos: TPoint;Surface: TDirectDrawSurface);
    procedure RestoreMouseRect(Surface: TDirectDrawSurface);
    procedure SetMVolume(const Value: Integer);
    procedure RestoreImages(Sender: TObject);
    procedure PerformFrameTime;
    procedure KillFocus;
    procedure SetMouseAnim(const Value: boolean);
    procedure CorrectFrameArray;
    function GetSystemBitmap(const Index: Integer): TPicture;
    function GetPage: TDXPage;
    function GetMessage: boolean;
    function GetVolume: Integer;
    function IndexOfFrameFunc(Func: TFrameFunction;Sender: TObject): Integer;
    function GetIsLocked: boolean;
    function GetLoadGame: boolean;
    procedure SetMouseShadow(const Value: boolean);
    procedure SetBackground(const Value: TBitmap);

    procedure OnMusicEnd(Sender: TObject);
    procedure RequestNextMusicFile(var FileName: String; var Loop: Boolean);
    procedure SetMediaPlayer(const Value: TMediaPlayer);
    procedure SetGamma(Value: Integer);
    { Private-Deklarationen }
  protected
    fMousePos             : TPoint;
    StackCount            : Integer;
    fTempLock             : Integer;
    fTempLoadGame         : Integer;
    fLButtonUp            : boolean;
    fRButtonUp            : boolean;
    fDblClick             : boolean;
    fRestoreList          : Array of TThreadMethod;
    procedure WMEraseBKND(var Msg: TWMEraseBkgnd);message WM_ERASEBKGND;
    procedure WMMouseMove(var Msg: TWMMouseMove);message WM_MOUSEMOVE;
    procedure WMLButtonDown(var Msg: TWMLButtonDown);message WM_LBUTTONDOWN;
    procedure WMDBlClick(var Msg: TWMLButtonDblClk);message WM_LBUTTONDBLCLK;
    procedure WMLButtonUp(var Msg: TWMLButtonUp);message WM_LBUTTONUP;
    procedure WMRButtonDown(var Msg: TWMRButtonDown);message WM_RBUTTONDOWN;
    procedure WMRButtonUp(var Msg: TWMRButtonUp);message WM_RBUTTONUP;
    procedure WMChar(var Msg: TWMChar);message WM_CHAR;
    procedure WMKeyDown(var Msg: TWMKeyDown);message WM_KEYDOWN;
    procedure WMKeyUp(var Msg: TWMKeyUp);message WM_KEYUP;
    procedure RedrawCursor(OldPos,NewPos: TPoint);

    procedure DoInitialize; override;
    procedure DoFinalize; override;
    procedure DoRestoreSurface; override;
    procedure DoLostFocus; override;

    procedure LeaveObject(CurrentObject: TDXComponent);
    procedure NextFrame(Sender: TObject;LagCount: Integer);
    procedure CreateParams(var Params: TCreateParams); override;

    function IsReady: Boolean;

    {$IFNDEF MUSIKTEST}
    procedure ReadMusikFiles;
    {$ENDIF}
    { Protected-Deklarationen }
  public
    IsDraw                : boolean;
    LeaveModal            : boolean;
    constructor Create(AOwner: TComponent);override;
    destructor destroy;override;
    procedure Stop(Stoppen: boolean = true);
    procedure MessageLock(Lock: boolean);
    procedure UnLockAll;
    procedure LockAll;
    procedure ShowCursor(Show: boolean);
    procedure ReleaseCapture;
    procedure SetCapture(Component: TDXComponent);
    procedure SetFocusControl(Component: TDXComponent);

    procedure AddComponent(Component: TDXComponent);
    procedure RemoveComponent(Component: TDXComponent);
    procedure BringToTop(Component: TDXComponent);

    procedure RemovePage(Page: TDXPage);
    procedure AddPage(Page: TDXPage);

    procedure ShowPageModal(Page: TDXPage);
    procedure MouseLeave;
    procedure AddFrameFunction(proc: TFrameFunction;Sender: TObject;MilliSec: Integer);
    procedure DeleteFrameFunction(proc: TFrameFunction;Sender: TObject);
    procedure ReactiveFrameFunctions;
    procedure DoScrollMessage(Pos: TPoint; Direction: TScrollDirection);

    procedure PlayMusicCategory(Category: String);
    procedure PlayMusic(FileName: String;Loop: Boolean = false);
    procedure StopMusic;

    procedure PlaySound(Sound: String);
    procedure StopSound(Sound: String);
    procedure ReadSounds(FromArchiv: String; ResLoc: String = 'default');

    procedure Lock;
    procedure UnLock;
    procedure DoMouseMessage;
    procedure IncLock;
    procedure decLock;
    procedure DoFlip;
    procedure LoadGame(Load: boolean);

    function CloseModal: boolean;
    procedure ShowPage(PageIndex: Integer);

{ ******************************** Funktionen f�r Zeichnen **************************** }
    procedure DrawContainer(Surface: TDirectDrawSurface);
    procedure ReDraw(Surface: TDirectDrawSurface);
    procedure RedrawControl(Control: TDXComponent;Surface: TDirectDrawSurface;Flip: boolean=true);
    procedure RedrawDirect(Control: TDXComponent;Surface: TDirectDrawSurface;Flip: boolean=true);
    procedure RedrawBlackControl(Control: TDXComponent;Surface: TDirectDrawSurface; TempSurface: Boolean = true);
    procedure RedrawArea(Rect: TRect;Surface: TDirectDrawSurface;Flip: boolean=true);
    procedure DrawCursor(Surface: TDirectDrawSurface);
{ ******************************** Zeichenfunktionen ende **************************** }

    procedure DeactiveAll;
    procedure ReactiveAll;
    procedure SetCursor(Cursor: TDXMouseCursor);
    procedure AddRestoreFunction(Func: TThreadMethod);
    procedure DeleteRestoreFunction(Func: TThreadMethod);
    function IsMessageActiv(Component: TDXComponent): boolean;
    function GetTempSurface: TDirectDrawSurface;
    function GetPageOfIndex(Index: Integer): TDXPage;

    property ActivePage: TDXPage read GetPage write SetPage;
    property IsLocked      : boolean read GetIsLocked;
    property IsLoadGame    : boolean read GetLoadGame;

    procedure PlayMovie(FileName: String; Rect: TRect);
    { Public-Deklarationen }
  published
    property Mouse           : TPicture index 0 read GetSystemBitmap write SetSystemBitmap;
    property MousePos        : TPoint read fMousePos write SetMousePos;
    property Background      : TBitmap write SetBackground;
    property PageAnimation   : boolean read fPageAnim write fPageAnim;
    property FocusControl    : TDXComponent read fFocusControl write SetFocusControl;
    property CanMessage      : boolean read GetMessage;
    property Volume          : Integer read GetVolume write SetVolume;
    property MoveMessage     : boolean read fMouseMove write fMouseMove;
    property MusicVolume     : Integer read fMVolume write SetMVolume;
    property Gamma           : Integer read fGamma write SetGamma;
    property ImageList       : TDXImageList read fImageList write fImageList;
    property Scrolling       : boolean read fScrolling write fScrolling;
    property MouseAnimation  : boolean read fMouseAnim write SetMouseAnim;
    property MouseShadow     : boolean read fMouseShadow write SetMouseShadow;
    property Sound           : boolean read fSound write fSound;

    property PowerDraw      : TPowerGraph read fPowerGraph;
    property MediaPlayer    : TMediaPlayer read fMediaPlayer write SetMediaPlayer;
  end;

var
  CounterFrequency : TLargeInteger;

procedure Register;

implementation

uses KD4Utils;

{$IFNDEF MUSIKTEST}
var
  gMusikList: TMusikList;
{$ENDIF}

procedure Register;
begin
  RegisterComponents('KD4 Tools', [TDXContainer]);
end;

{ TDXContainer }

procedure TDXContainer.AddComponent(Component: TDXComponent);
begin
  if fControls.IndexOf(Component)<>-1 then exit;
  fControls.Add(Component);
  if (fFocusControl=nil) and Component.TabStop and Component.Visible and Component.Enabled then
  begin
    fFocusControl:=Component;
    fFocusControl.GetFocus;
  end;
end;

procedure TDXContainer.AddPage(Page: TDXPage);
begin
  fPages.Add(Page);
end;

constructor TDXContainer.Create(AOwner: TComponent);
var
  Item: TPictureCollectionItem;
begin
  inherited;
  if not (csDesigning in ComponentState) then Windows.ShowCursor(false);
  fSound:=false;
  fTempSurface:=nil;
  fClearStack:=true;
  fDblClick:=false;
  fLButtonUp:=true;
  fRButtonUp:=true;
  fStop:=False;

  fStatus:=csReady;

  fFrameTime:=AnimTime;
  fFrame:=0;
  fFrameLock:=false;
  {$IFDEF ASSERT_QUERYPERFORMANCECOUNTER}
  Assert(QueryPerformanceCounter(fLastFrame),'QueryPerformanceCounter fehlgeschlagen');
  {$ELSE}
  QueryPerformanceCounter(fLastFrame);
  {$ENDIF}
  fTimer:=TDXTimer.Create(Self);
  fTimer.Interval:=AnimTime div 2;
  fTimer.OnTimer:=NextFrame;
  fTimer.Enabled:=false;
  fActivePage:=-1;

  fPageStack:=TObjectStack.Create;

  fShowCursor:=0;

  fControls:=TList.Create;
  fPages:=TList.Create;

  fMouseMove:=true;
  IsDraw:=false;

  fPlayingMusic:=false;

  fSystemList:=TDXImageList.Create(Self);
  fSystemList.DXDraw:=Self;

  Item := TPictureCollectionItem.Create(fSystemList.Items);
  with Item do
  begin
    Transparent:=true;
    TransparentColor:=clWhite;
    SystemMemory:=false;
    PatternHeight:=32;
    PatternWidth:=32;
    Picture.OnChange:=RestoreImages;
  end;

  Item := TPictureCollectionItem.Create(fSystemList.Items);
  Item.SystemMemory:=true;
  Item.Picture.OnChange:=RestoreImages;
  Item.Picture.Bitmap.Width:=35;
  Item.Picture.Bitmap.Height:=35;

  fLock:=0;

  fOverControl:=nil;
  fCaptureControl:=nil;
  fFocusControl:=nil;

  fGameSetSounds:=TStringList.Create;

  // Powerdraw
  fPowerGraph:=nil;

  // Musikdateien einlesen
  ReadMusikFiles;

  {$IFDEF ASSERT_QUERYPERFORMANCECOUNTER}
  Assert(QueryPerformanceFrequency(CounterFrequency),'QueryPerformanceFrequency fehlgeschlagen');
  {$ELSE}
  QueryPerformanceFrequency(CounterFrequency);
  {$ENDIF}
end;

procedure TDXContainer.DeactiveAll;
var
  Dummy: Integer;
begin
  Dummy:=fControls.Count-1;
  while (Dummy>-1) do
  begin
    TDXComponent(fControls[Dummy]).DeactiveControl;
    dec(Dummy);
  end;
end;

procedure TDXContainer.decLock;
begin
  if fLock>0 then dec(fLock);
end;

destructor TDXContainer.destroy;
var
  Control: TDXPage;
begin
  fOverControl:=nil;
  fFocusControl:=nil;
  fControls.Clear;
  ReleaseCapture;
  while (fPages.Count>0) do
  begin
    Control:=TDXPage(fPages[0]);
    Control.Free;
  end;
  StopMusic;
  fTimer.Free;
  fTimer:=nil;
  fPages.Free;
  fControls.Free;
  fSystemList.Free;
  fPageStack.Free;
  fGameSetSounds.Free;
  inherited;
end;

procedure TDXContainer.DrawContainer(Surface: TDirectDrawSurface);
var        
  Dummy   : Integer;
  Control : TDXComponent;
  Count   : Integer;
  fMem    : TDDSurfaceDesc;
begin
  if not CanDraw then exit;
  Surface.Canvas.Release;
  fTempSurface.Canvas.Release;
  if fBackGround.Width=0 then
    fTempSurface.Fill(0)
  else
    fTempSurface.Draw(0,0,fBackGround,false);
    
  Dummy:=0;
  Count:=fControls.Count;
  fTempSurface.Lock(fMem);
  fTempSurface.Unlock;
  while (Dummy<Count) do
  begin
    Control:=TDXComponent(fControls.Items[Dummy]);
    if Control.Visible then
    begin
      with fTempSurface.Canvas do
      begin
        Font:=Control.Font;
        Control.Draw(fTempSurface,fMem);
      {$IFDEF FRAMECOMPONENT}
        Pen.Color:=clWhite;
        Brush.Style:=bsClear;
        Rectangle(Control.ClientRect);
      {$ENDIF}
        Release;
      end;
    end;
    inc(Dummy);
  end;
  SaveMouseRect(fMousePos,fTempSurface);
  Surface.Draw(0,0,fTempSurface,false);
end;

procedure TDXContainer.DrawCursor(Surface: TDirectDrawSurface);
var
  MemSur            : TDDSurfaceDesc;
  MemCurBil         : TDDSurfaceDesc;
  PoiSur            : Cardinal;
  PoiCurBil         : Cardinal;
  CurBil            : TDirectDrawSurface;
  relX,relY         : Integer;
  absX,absY         : Integer;
  CurX,CurY         : Integer;
begin
  if (not CanDraw) then
    exit;

  SaveMouseRect(Point(fMousePos.X-fCursor.HotSpot.X,fMousePos.Y-fCursor.HotSpot.Y),Surface);

  if fShowCursor<>0 then
    exit;

  CurBil:=fSystemList.Items[0].PatternSurfaces[fFrame];
  { Schatten f�r Cursor zeichnen }
  if fMouseShadow then
  begin
    CurBil.Lock(PRect(nil)^,MemCurBil);
    Surface.Lock(PRect(nil)^,MemSur);
    AbsY:=fMousePos.Y+3-fCursor.HotSpot.Y;
    CurY:=fSystemList.Items[0].PatternRects[fFrame].Top;
    relY:=0;
    if Mode32Bit then
    begin
      while (AbsY<Surface.Height) and (relY<32) do
      begin
        relX:=0;
        AbsX:=fMousePos.X+3-fCursor.HotSpot.X;
        if AbsY>=0 then
        begin
          CurX:=fSystemList.Items[0].PatternRects[fFrame].Left;
          PoiCurBil:=Integer(MemCurBil.lpSurface)+(CurY*MemCurBil.lPitch)+(CurX shl 2);
          PoiSur:=Integer(MemSur.lpSurface)+(AbsY*MemSur.lPitch)+(AbsX shl 2);
          while (AbsX<Surface.Width) and (relX<32) do
          begin
            if AbsX>=0 then
            begin
              if (PLongWord(PoiCurBil)^ and $00FFFFFF)<>$00FFFFFF then
              begin
                if AlphaElements then
                begin
                  PLongWord(PoiSur)^:=((PLongWord(PoiSur)^ and Mask) shr 1);
                end
                else PLongWord(PoiSur)^:=0;
              end;
            end;
            inc(PoiSur,4);
            inc(PoiCurBil,4);
            inc(relX);
            inc(absX);
          end;
        end;
        inc(relY);
        inc(AbsY);
        inc(CurY);
        if CurY>=MemCurBil.dwHeight then
          break;
      end;
    end
    else
    begin
      while (AbsY<Surface.Height) and (relY<32) do
      begin
        relX:=0;
        AbsX:=fMousePos.X+3-fCursor.HotSpot.X;
        if AbsY>=0 then
        begin
          CurX:=fSystemList.Items[0].PatternRects[fFrame].Left;
          PoiCurBil:=Integer(MemCurBil.lpSurface)+(CurY*MemCurBil.lPitch)+(CurX shl 1);
          PoiSur:=Integer(MemSur.lpSurface)+(AbsY*MemSur.lPitch)+(AbsX shl 1);
          while (AbsX<Surface.Width) and (relX<32) do
          begin
            if AbsX>=0 then
            begin
              if PWord(PoiCurBil)^<>bcWhite then
              begin
                if AlphaElements then
                begin
                  PWord(PoiSur)^:=(PWord(PoiSur)^ and Mask) shr 1;
                end
                else PWord(PoiSur)^:=0;
              end;
            end;
            inc(PoiSur,2);
            inc(PoiCurBil,2);
            inc(relX);
            inc(absX);
          end;
        end;
        inc(relY);
        inc(AbsY);
        inc(CurY);
        if CurY>=MemCurBil.dwHeight then
          break;
      end;
    end;
    CurBil.UnLock;
    Surface.UnLock;
  end;
  Surface.Draw(fMousePos.x-fCursor.HotSpot.X,fMousePos.y-fCursor.HotSpot.Y,fSystemList.Items[0].PatternRects[fFrame],CurBil,true);
end;

function TDXContainer.GetPage: TDXPage;
begin
  if (fActivePage<0) or (fActivePage>=fPages.Count) then
    result:=nil
  else
    result:=TDXPage(fPages[fActivePage]);
end;

function TDXContainer.GetSystemBitmap(const Index: Integer): TPicture;
begin
  if fSystemList.Items.Count-1<Index then
  begin
    result:=nil;
    exit;
  end;
  result:=fSystemList.Items[Index].Picture;
end;

procedure TDXContainer.IncLock;
begin
  inc(fLock);
end;

function TDXContainer.IsMessageActiv(Component: TDXComponent): boolean;
begin
  result:=false;
  if not (Component.Enabled and Component.Visible) then exit;
  if fCaptureControl=nil then
  begin
    result:=true;
    exit;
  end;
  if fCaptureControl=Component then result:=true;
end;

procedure TDXContainer.PlaySound(Sound: String);
begin
  if fBassPlayer=nil then
    exit;

  if Pos('\',Sound)=0 then
    Sound:='default\'+Sound;

  fBassPlayer.PlaySample(HashString(Sound));
//    raise Exception.CreateFmt('Sound %s nicht gefunden',[Sound]); 
end;

procedure TDXContainer.ReactiveAll;
var
  Dummy: Integer;
begin
  Dummy:=fControls.Count-1;
  while (Dummy>-1) do
  begin
    TDXComponent(fControls[Dummy]).ReactiveControl;
    dec(Dummy);
  end;
end;

procedure TDXContainer.ReDraw(Surface: TDirectDrawSurface);
begin
  if fLoadGame<>0 then exit;
  if (not CanDraw) or (not Visible) then
  begin
    exit;
  end;
  IncLock;
  PerformFrameTime;
  DecLock;
  Surface.Canvas.Release;
  DrawContainer(Surface);
  DrawCursor(Surface);
  if fLock=0 then Flip;
end;

procedure TDXContainer.RedrawControl(Control: TDXComponent;Surface: TDirectDrawSurface;Flip: boolean=true);
begin
  if (not CanDraw) then exit;
  if fLoadGame<>0 then exit;
  IncLock;
  PerformFrameTime;
  DecLock;
  if fControls.IndexOf(Control)=-1 then exit;
  if not Control.Visible then exit;
  if Control.DirectDraw then
    RedrawDirect(Control,Surface,Flip)
  else
    RedrawArea(Control.ClientRect,Surface,Flip);
end;

procedure TDXContainer.RedrawCursor(OldPos, NewPos: TPoint);
begin
  if (not CanDraw) then exit;
  if not IsDraw then exit;
  if (fLock<>0) or (fLoadGame<>0) then exit;
  Surface.Canvas.Release;
  RestoreMouseRect(Surface);
  DrawCursor(Surface);
  Flip;
end;

procedure TDXContainer.ReleaseCapture;
begin
  fCaptureControl:=nil;
end;

procedure TDXContainer.RemoveComponent(Component: TDXComponent);
var
  Index: Integer;
begin
  if Component=nil then
    exit;
  Index:=fControls.IndexOf(Component);
  if Index=-1 then exit;
  if Component=fFocusControl then
  begin
    fFocusControl.KillFocus;
    fFocusControl:=nil;
  end;
  if Component=fOverControl then
  begin
    fOverControl:=nil;
    Component.MouseLeave;
  end;
  fControls.Delete(Index);
end;

procedure TDXContainer.RemovePage(Page: TDXPage);
var
  Index: Integer;
begin
  Index:=fPages.IndexOf(Page);
  if Index=-1 then exit;
  fPages.Delete(Index);
  if Page=ActivePage then
  begin
    fFocusControl:=nil;
    fOverControl:=nil;
  end;
end;

procedure TDXContainer.SetCapture(Component: TDXComponent);
begin
  if fControls.IndexOf(Component)<>-1 then
  fCaptureControl:=Component;
end;

procedure TDXContainer.SetFocusControl(Component: TDXComponent);
var
  Old: TDXComponent;
begin
  if fFocusControl=Component then exit;
  Old:=fFocusControl;
  fFocusControl:=Component;
  IncLock;
  if Old<>nil then
  begin
    Old.KillFocus;
    Old.Redraw;
  end;
  DecLock;
  if fFocusControl<>nil then
  begin
    fFocusControl.GetFocus;
    fFocusControl.Redraw;
  end;
end;

procedure TDXContainer.SetMousePos(const Value: TPoint);
begin
  RestoreMouseRect(Surface);
  fMousePos := Value;
  DrawCursor(Surface);
  if fLock<>0 then Flip;
end;

procedure TDXContainer.SetPage(const Value: TDXPage);
var
  Index: Integer;

  procedure ClearStack;
  begin
    if fClearStack then
      while fPageStack.Count>0 do fPageStack.Pop;
  end;

begin
  ClearStack;
  Lock;
  StackCount:=-1;
  if ActivePage<>nil then
    ActivePage.DeactivatePage;
  Index:=fPages.IndexOf(Value);
  if Index<>-1 then
    fActivePage:=Index
  else
    fActivePage:=fPages.Add(Value);
  fPageStack.Push(Value);
  fFocusControl:=nil;
  ReleaseCapture;
  fControls.Clear;
  UnLock;
  ActivePage.Activate;
  fLock:=0;
  fLoadGame:=0;
  fFrameLock:=false;
end;

procedure TDXContainer.SetSystemBitmap(const Index: Integer;
  const Value: TPicture);
begin
  if fSystemList.Items.Count-1<Index then
  begin
    exit;
  end;
  fSystemList.Items[Index].Picture.Assign(Value);
end;

procedure TDXContainer.ShowCursor(Show: boolean);
var
  NewPoint: TPoint;
begin
  if Show then
  begin
    if fShowCursor>0 then
    begin
      dec(fShowCursor);
      GetCursorPos(NewPoint);
      fMousePos:=NewPoint;
      fTimer.Enabled:=true;
    end;
  end
  else
  begin
    inc(fShowCursor);
  end;
end;

procedure TDXContainer.WMChar(var Msg: TWMChar);
var
  State: TShiftState;
begin
  if GetKeyState(VK_SHIFT)<0 then Include(State,ssShift);
  if GetKeyState(VK_MENU)<0 then Include(State,ssAlt);
  if GetKeyState(VK_Control)<0 then Include(State,ssCtrl);
  if GetKeyState(VK_LButton)<0 then Include(State,ssLeft);
  if GetKeyState(VK_MButton)<0 then Include(State,ssMiddle);
  if GetKeyState(VK_RButton)<0 then Include(State,ssRight);
  if fFocusControl<>nil then fFocusControl.KeyPress(Msg.CharCode,State);
  if Msg.CharCode<>0 then
    if ActivePage<>nil then ActivePage.KeyPress(Msg.CharCode,State);
end;

procedure TDXContainer.WMEraseBKND(var Msg: TWMEraseBkgnd);
begin
  // zur Laufzeit Komponente Transparent
  if (csDesigning in ComponentState) then Canvas.Rectangle(ClientRect);
end;

procedure TDXContainer.WMKeyDown(var Msg: TWMKeyDown);
var
  State: TShiftState;
  Dummy: Integer;
  Beg: Integer;
begin
  if ActivePage=nil then
    exit;
  if (Msg.CharCode=9) and (fControls.Count>0) then
  begin
    if GetKeyState(VK_SHIFT)<0 then
    begin
      Dummy:=fControls.IndexOf(fFocusControl);
      if Dummy=-1 then Dummy:=0;
      Beg:=Dummy;
      repeat
        dec(Dummy);
        if Dummy<0 then Dummy:=fControls.Count-1;
        if Beg=Dummy then exit;
      until (TDXComponent(fControls[Dummy]).TabStop) and (TDXComponent(fControls[Dummy]).Enabled) and (TDXComponent(fControls[Dummy]).Visible);
      FocusControl:=fControls[Dummy];
    end
    else
    begin
      Dummy:=fControls.IndexOf(fFocusControl);
      if Dummy=-1 then Dummy:=0;
      Beg:=Dummy;
      repeat
        inc(Dummy);
        if Dummy>fControls.Count-1 then Dummy:=0;
        if Beg=Dummy then exit;
      until (TDXComponent(fControls[Dummy]).TabStop) and (TDXComponent(fControls[Dummy]).Enabled) and (TDXComponent(fControls[Dummy]).Visible);
      FocusControl:=fControls[Dummy];
    end;
  end;
  State:=[];
  if GetKeyState(VK_SHIFT)<0 then Include(State,ssShift);
  if GetKeyState(VK_MENU)<0 then Include(State,ssAlt);
  if GetKeyState(VK_Control)<0 then Include(State,ssCtrl);
  if GetKeyState(VK_LButton)<0 then Include(State,ssLeft);
  if GetKeyState(VK_MButton)<0 then Include(State,ssMiddle);
  if GetKeyState(VK_RButton)<0 then Include(State,ssRight);
  if (ssAlt in State) then
  begin
    Assert(ActivePage<>nil);
    ActivePage.PerformSystemKey(Char(Msg.CharCode));
    exit;
  end;
  if (fMessageLock=0) and (Msg.CharCode>=VK_F1) and (Msg.CharCode<=VK_F12) then
  begin
    ActivePage.HandleFKey(Msg.CharCode-VK_F1+1);
    exit;
  end;
  if fFocusControl<>nil then fFocusControl.KeyDown(Msg.CharCode,State);
end;

procedure TDXContainer.WMLButtonDown(var Msg: TWMLButtonDown);
var
  Control: TDXComponent;
  Dummy: Integer;
begin
  if (not fLButtonUp) or fDblClick then exit;
  fLButtonUp:=false;
  if fControls.Count=0 then exit;
  Dummy:=fControls.Count-1;
  while Dummy>-1 do
  begin
    Control:=TDXComponent(fControls.Items[Dummy]);
    if PtInRegion(Control.MouseRegion,fMousePos.X,fMousePos.Y) then
    begin
      if IsMessageActiv(Control) then
      begin
        Control.MouseDown(mbLeft,fMousePos.X-Control.Left,fMousePos.Y-Control.Top);
        SetCapture(Control);
        exit;
      end;
    end;
    dec(Dummy);
  end;
end;

procedure TDXContainer.WMLButtonUp(var Msg: TWMLButtonUp);
var
  Dummy: Integer;
  LeaveLoop: boolean;
  Control: TDXComponent;
begin
  if fLButtonUp or (fDblClick) then
  begin
    fDblClick:=false;
    exit;
  end;
  fLButtonUp:=true;
  if fControls.Count=0 then exit;
  if fCaptureControl<>nil then
  begin
    fCaptureControl.MouseUp(mbLeft,fMousePos.X-fCaptureControl.Left,fMousePos.Y-fCaptureControl.Top);
    if fCaptureControl<>nil then
      if PtInRect(fCaptureControl.ClientRect,fMousePos) then
        fCaptureControl.DoClick
      else
        fCaptureControl.MouseLeave;
  end
  else
  begin
    Dummy:=fControls.Count-1;
    LeaveLoop:=false;
    if fControls.Count>0 then
    begin
      while not LeaveLoop do
      begin
        Control:=TDXComponent(fCOntrols.Items[Dummy]);
        if PtInRegion(Control.MouseRegion,fMousePos.X,fMousePos.Y) then
        begin
          if IsMessageActiv(Control) then
          begin
            Control.MouseUp(mbLeft,fMousePos.X-Control.Left,fMousePos.Y-Control.Top);
            Control.DoClick;
          end;
          LeaveLoop:=true;
        end;
        dec(Dummy);
        if Dummy=-1 then LeaveLoop:=true;
      end;
    end;
  end;
  ReleaseCapture;
  Windows.ReleaseCapture;
end;

procedure TDXContainer.WMMouseMove(var Msg: TWMMouseMove);
var
  Control       : TDXComponent;
  Dummy         : Integer;
  IsOverControl : boolean;
  OldPos        : TPoint;
  PerformLoop     : boolean;
begin
  if not IsReady then
    exit;
    
  if not fMouseMove then
    exit;

  IncLock;
  PerformFrameTime;
  DecLock;
  OldPos:=fMousePos;
  fMousePos.X:=Msg.XPos;
  fMousePos.Y:=Msg.YPos;
  IsOverControl:=false;
  PerformLoop:=true;
  if (fControls.Count>0) and (not fDblClick) then
  begin
    incLock;
    Dummy:=fControls.Count-1;
    if fCaptureControl=nil then
    begin
      while PerformLoop do
      begin
        Control:=TDXComponent(fControls.Items[Dummy]);
        if PtInRegion(Control.MouseRegion,fMousePos.X,fMousePos.Y) then
        begin
          if IsMessageActiv(Control) then
          begin
            Control.MouseMove(fMousePos.X-Control.Left,fMousePos.Y-Control.Top);
            IsOverControl:=true;
            if fOverControl<>Control then
            begin
              if fOverControl<>nil then fOverControl.MouseLeave;
              fOverControl:=Control;
              Control.MouseOver;
            end;
            PerformLoop:=false;
          end;
        end;
        dec(Dummy);
        if Dummy=-1 then PerformLoop:=false;
      end;
      if (not IsOverControl) and (fOverControl<>nil) then
      begin
        fOverControl.MouseLeave;
        fOverControl:=nil;
      end;
    end
    else
      fCaptureControl.MouseMove(fMousePos.X-fCaptureControl.Left,fMousePos.Y-fCaptureControl.Top);
    DecLock;
  end;
  RedrawCursor(OldPos,fMousePos);
end;

procedure TDXContainer.WMKeyUp(var Msg: TWMKeyUp);
var
  State: TShiftState;
begin
  if GetKeyState(VK_SHIFT)<0 then Include(State,ssShift);
  if GetKeyState(VK_MENU)<0 then Include(State,ssAlt);
  if GetKeyState(VK_Control)<0 then Include(State,ssCtrl);
  if GetKeyState(VK_LButton)<0 then Include(State,ssLeft);
  if GetKeyState(VK_MButton)<0 then Include(State,ssMiddle);
  if GetKeyState(VK_RButton)<0 then Include(State,ssRight);
  if fFocusControl<>nil then fFocusControl.KeyUp(Msg.CharCode,State);
end;

function TDXContainer.GetVolume: Integer;
begin
  result:=0;
  if fBassPlayer=nil then
    exit;
  result:=fBassPlayer.SampleVolume;
end;

procedure TDXContainer.SetVolume(const Value: Integer);
begin
  fBassPlayer.SampleVolume:=Value;
end;

procedure TDXContainer.WMRButtonDown(var Msg: TWMRButtonDown);
var
  Control: TDXComponent;
  Dummy: Integer;
  EndLoop: boolean;
begin
  if not fRButtonUp then exit;
  fRButtonUp:=false;
  if fControls.Count=0 then exit;
  EndLoop:=false;
  Dummy:=fControls.Count-1;
  while (Dummy<>0) and not EndLoop do
  begin
    Control:=TDXComponent(fControls.Items[Dummy]);
    if PtInRegion(Control.MouseRegion,fMousePos.X,fMousePos.Y) then
    begin
      if IsMessageActiv(Control) then
      begin
        Control.MouseDown(mbRight,fMousePos.X-Control.Left,fMousePos.Y-Control.Top);
        EndLoop:=true;
      end;
    end;
    dec(Dummy);
  end;
end;

procedure TDXContainer.WMRButtonUp(var Msg: TWMRButtonUp);
var
  Dummy: Integer;
  LeaveLoop: boolean;
  Control: TDXComponent;
begin
  if fRButtonUp then exit;
  fRButtonUp:=true;
  if fControls.Count=0 then exit;
  RestoreMouseRect(Surface);
  if fCaptureControl<>nil then
    fCaptureControl.MouseUp(mbRight,fMousePos.X-fCaptureControl.Left,fMousePos.Y-fCaptureControl.Top)
  else
  begin
    Dummy:=fControls.Count-1;
    LeaveLoop:=false;
    if fControls.Count>0 then
    begin
      while not LeaveLoop do
      begin
        Control:=TDXComponent(fCOntrols.Items[Dummy]);
        if PtInRegion(Control.MouseRegion,fMousePos.X,fMousePos.Y) then
        begin
          if IsMessageActiv(Control) then
          begin
            Control.MouseUp(mbRight,fMousePos.X-Control.Left,fMousePos.Y-Control.Top);
            LeaveLoop:=true;
          end;
        end;
        dec(Dummy);
        if Dummy=-1 then LeaveLoop:=true;
      end;
    end;
  end;
  ReleaseCapture;
  Windows.ReleaseCapture;
end;

procedure TDXContainer.ShowPageModal(Page: TDXPage);
var
  SaveStack : Integer;
begin
  fFrameLock:=false;
  fClearStack:=false;
  LeaveModal:=false;
  ActivePage:=Page;
  if LeaveModal then
  begin
    CloseModal;
    fClearStack:=true;
    exit;
  end;
  fClearStack:=true;
  SaveStack:=fPageStack.Count;
  while (fPageStack.Count>=SaveStack) do
  begin
    Application.HandleMessage;
    if Application.Terminated then exit;
  end;
end;

function TDXContainer.CloseModal: boolean;
var
  Page      : TDXPage;
  Index     : Integer;
begin
  result:=false;
  if Application.Terminated then
    exit;
  if fPageStack.Count>1 then
    fPageStack.Pop
  else if fPageStack.Count=1 then
  begin
    // Fallback zum Geoscape, wenn kein Stack vorhanden ist
    ShowPage(PageGameMenu);
    exit;
  end;
  result:=true;
  fClearStack:=false;
  while fPageStack.Count>1 do
  begin
    Page:=TDXPage(fPageStack.Pop);
    if Page.ReshowPage then
    begin
      IncLock;
      Index:=fPages.IndexOf(Page);
      if Index<>-1 then
        fActivePage:=Index
      else
        fActivePage:=fPages.Add(Page);
      fFocusControl:=nil;
      ReleaseCapture;
      fControls.Clear;
      DecLock;
      ActivePage.Activate(false);
      fPageStack.Push(ActivePage);
      fLock:=0;
      fLoadGame:=0;
      fClearStack:=true;
      exit;
    end;
  end;
  fClearStack:=true;
  ActivePage:=TDXPage(fPageStack.Peek);
end;

procedure TDXContainer.StopSound(Sound: String);
begin
  if fBassPlayer=nil then
    exit;
  fBassPlayer.StopSample(HashString(Sound));
end;

procedure TDXContainer.LoadGame(Load: boolean);
begin
  if Load then
  begin
    inc(fLoadGame);
  end
  else if fLoadGame>0 then
  begin
    dec(fLoadGame);
  end;
end;

procedure TDXContainer.RestoreMouseRect(Surface: TDirectDrawSurface);
var
  MouseRect: TRect;
begin
  MouseRect:=fSavedRect;
  OffsetRect(MouseRect,-fSavedRect.Left,-fSavedRect.Top);
  Surface.Draw(fSavedRect.Left,fSavedRect.Top,MouseRect,fSystemList.Items[1].PatternSurfaces[0],false);
end;

procedure TDXContainer.SaveMouseRect(Pos: TPoint;
  Surface: TDirectDrawSurface);
begin
  fSavedRect:=Rect(Pos.x,Pos.y,Pos.x+35,Pos.y+35);
  CorrectRect(fSavedRect);
  fSystemList.Items[1].PatternSurfaces[0].BltFast(0,0,fSavedRect,0,Surface);
end;

procedure TDXContainer.WMDBlClick(var Msg: TWMLButtonDblClk);
var
  Control : TDXComponent;
  Dummy   : Integer;
  EndLoop : boolean;
begin
  if fControls.Count=0 then exit;
  fDblClick:=true;
  EndLoop:=false;
  Dummy:=fControls.Count-1;
  while (Dummy<>0) and not EndLoop do
  begin
    Control:=TDXComponent(fControls.Items[Dummy]);
    if PtInRegion(Control.MouseRegion,fMousePos.X,fMousePos.Y) then
    begin
      if IsMessageActiv(Control) then
      begin
        Control.DblClick(mbLeft,fMousePos.X-Control.Left,fMousePos.Y-Control.Top);
        EndLoop:=true;
      end;
    end;
    dec(Dummy);
  end;
end;

procedure TDXContainer.PlayMusic(FileName: String;Loop: Boolean);
begin
  if fBassPlayer=nil then exit;
  if fBassPlayer.Volume=0 then
    exit;
  if not FileExists(Filename) then
  begin
    GlobalFile.Write('Musikfile nicht gefunden',FileName);
    exit;
  end;

  if Loop then
    GlobalFile.Write('Now Looping',Filename)
  else
    GlobalFile.Write('Now Playing',Filename);

  if fPlayingMusic then
    fBassPlayer.FadeTo(FileName,Loop)
  else
  begin
    fBassPlayer.Open(FileName);
    fBassPlayer.Play(Loop);
  end;
  fLooping:=Loop;
  fPlayingMusic:=true;
end;

procedure TDXContainer.StopMusic;
begin
  if fPlayingMusic then
  begin
    fBassPlayer.Stop;
    fPlayingMusic:=false;
  end;
end;

procedure TDXContainer.SetMVolume(const Value: Integer);
begin
  fMVolume := Value;
  if fBassPlayer<>nil then
    fBassPlayer.Volume:=Value;
end;

procedure TDXContainer.DoMouseMessage;
var
  Msg: TWMMouseMove;
begin
  Msg.Msg:=WM_MOUSEMOVE;
  Msg.Keys:=0;
  Msg.XPos:=fMousePos.x;
  Msg.YPos:=fMousePos.y;
  Msg.Pos:=PointToSmallPoint(fMousePos);
  Msg.Result:=0;
  WMMouseMove(Msg);
end;

procedure TDXContainer.RestoreImages(Sender: TObject);
var
  Dummy: Integer;
begin
  for Dummy:=0 to fSystemList.Items.Count-1 do
  begin
    fSystemList.Items[Dummy].Restore;
  end;
end;

procedure TDXContainer.DoInitialize;
var
  MemSur            : TDDSurfaceDesc;
  RMask,BMask,GMask : Cardinal;
begin
  inherited;
  if fTempSurface=nil then
  begin
    fTempSurface:=TDirectDrawSurface.Create(DDraw);
    fTempSurface.SystemMemory:=true;
    fTempSurface.SetSize(ScreenWidth,ScreenHeight);
    MemSur:=fTempSurface.SurfaceDesc;
    RMask:=MemSur.ddpfPixelFormat.dwRBitMask;
    BMask:=MemSur.ddpfPixelFormat.dwBBitMask;
    GMask:=MemSur.ddpfPixelFormat.dwGBitMask;
    RMask:=RMask and (RMask shl 1);
    BMask:=BMask and (BMask shl 1);
    GMask:=GMask and (GMask shl 1);
    Mask:=RMask or BMask or GMask;
  end;
  if fBackGround=nil then
  begin
    fBackGround:=TDirectDrawSurface.Create(Self.DDraw);
    fBackGround.SystemMemory:=true;
  end;

  if fPowerGraph<>nil then
    fPowerGraph.Free;
  fPowerGraph:=TPowerGraph.Create(Self,false);
  fPowerGraph.AntiAliasing:=false;

  fLock:=0;
  fLoadGame:=0;
  fTimer.Enabled:=true;

  if fBassPlayer=nil then
  begin
    fBassPlayer:=TBassPlayer.Create(Self);
    if (fBassPlayer.BASSDLLVer = '') or (not fBassPlayer.PlayerReady) then
    begin
      fBassPlayer.Free;
      fBassPlayer:=nil;
    end
    else
    begin
      fBassPlayer.OnPlayEnd:=OnMusicEnd;
      fBassPlayer.OnRequestNextFile:=RequestNextMusicFile;
      {$IFNDEF MUSICTEST}
  //    PlayMusicCategory('Hauptmen�');
      {$ENDIF}
    end;
  end;

  if Primary.GammaControl<>nil then
    Primary.GammaControl.GetGammaRamp(0,fOrgGammaRamp);
//  Redraw(Surface);
end;

procedure TDXContainer.DoFinalize;
begin
  inherited;
  fLock:=0;
  fLoadGame:=0;
  if fTimer<>nil then
    fTimer.Enabled:=false;

  if fPowerGraph<>nil then
  begin
    fPowerGraph.Free;
    fPowerGraph:=nil;
  end;
end;

procedure TDXContainer.RedrawArea(Rect: TRect; Surface: TDirectDrawSurface;
  Flip: boolean);
var
  Dummy   : Integer;
  ConTemp : TDXComponent;
  Count   : Integer;
  fMem    : TDDSurfaceDesc;
begin
  if (fLoadGame<>0) or (not CanDraw) then exit;
  IncLock;
  PerformFrameTime;
  DecLock;
  CorrectRect(Rect);
  Surface.Canvas.Release;
  fTempSurface.Canvas.Release;
  if fBackground.Width=0 then
    fTempSurface.FillRect(Rect,0)
  else
    fTempSurface.BltFast(Rect.Left,Rect.Top,Rect,DDBLTFAST_NOCOLORKEY,fBackGround);

  RestoreMouseRect(Surface);
  fTempSurface.Lock(fMem);
  fTempSurface.Unlock;
  with fTempSurface.Canvas do
  begin
    Dummy:=0;
    Count:=fControls.Count;
    While (Dummy<Count) do
    begin
      ConTemp:=fControls[Dummy];
      if ConTemp.Visible then
      begin
        if OverlapRect(ConTemp.ClientRect,Rect) then
        begin
          Font:=ConTemp.Font;
          ConTemp.ReDrawRect(Rect,fTempSurface,fMem);
          Release;
        end;
        {$IFDEF FRAMECOMPONENT}
          Surface.Canvas.Pen.Color:=clWhite;
          Surface.Canvas.Brush.Style:=bsClear;
          Surface.Canvas.Rectangle(ConTemp.ClientRect);
          Surface.Canvas.Release;
        {$ENDIF}
      end;
      inc(Dummy);
    end;
  end;
  Surface.Canvas.Release;
  Surface.BltFast(Rect.Left,Rect.Top,Rect,0,fTempSurface);
  DrawCursor(Surface);
  if Flip and (fLock=0) then Self.Flip;
end;

function TDXContainer.GetIsLocked: boolean;
begin
  result:=fLock>0;
end;

function TDXContainer.GetLoadGame: boolean;
begin
  result:=fLoadGame>0;
end;

procedure TDXContainer.DoRestoreSurface;
var
  Dummy: Integer;
begin
  fStatus:=csReady;
  if ActivePage<>nil then ActivePage.Restore;
  fImageList.Items[0].Restore;
  fImageList.Items[1].Restore;
  for Dummy:=0 to fControls.Count-1 do
    TDXComponent(fControls.List[Dummy]).Restore;

  for Dummy:=High(fRestoreList) downto 0 do
    fRestoreList[Dummy];

  fLock:=0;
  fLoadGame:=0;
  inherited;
  Redraw(Surface);
  DoFlip;
end;

procedure TDXContainer.Lock;
begin
  inc(fLock);
  inc(fLoadGame);
end;

procedure TDXContainer.UnLock;
begin
  if fLock>0 then dec(fLock);
  if fLoadGame>0 then dec(fLoadGame);
end;

procedure TDXContainer.LeaveObject(CurrentObject: TDXComponent);
begin
  if (fOverControl<>CurrentObject) or (fOverControl=nil) then exit;
  fOverControl.MouseLeave;
  fOverControl:=nil;
end;

procedure TDXContainer.RedrawBlackControl(Control: TDXComponent;
  Surface: TDirectDrawSurface;TempSurface: Boolean);
var
  Mem: TDDSurfaceDesc;
begin
  if not Control.Visible then
  begin
    exit;
  end;
  if fLoadGame<>0 then exit;
  if not CanDraw then exit;
  IncLock;
  PerformFrameTime;
  DecLock;
  Surface.Canvas.Release;
  RestoreMouseRect(Surface);
  if TempSurface then
  begin
    fTempSurface.Canvas.Release;
    fTempSurface.Lock(Mem);
    fTempSurface.Unlock;
    Control.Draw(fTempSurface,Mem);
    fTempSurface.Canvas.Release;
    Surface.BltFast(Control.Left,Control.Top,Control.ClientRect,0,fTempSurface);
  end
  else
  begin
    Surface.Lock(Mem);
    Surface.Unlock;
    Control.Draw(Surface,Mem);
    Surface.Canvas.Release;
  end;
  DrawCursor(Surface);
  if (fLock=0) then Self.Flip;
end;

procedure TDXContainer.NextFrame(Sender: TObject;LagCount: Integer);
begin
  PerformFrameTime;
end;

procedure TDXContainer.PerformFrameTime;
var
  OldFrame    : Integer;
  Frames      : Integer;
  Time        : Integer;
  Dummy       : Integer;
  NeedFlip    : boolean;
  FoundDelete : boolean;
  NewFrame    : TLargeInteger;
begin
  if fStop then exit;
  if fFrameLock then exit;

  {$IFDEF ASSERT_QUERYPERFORMANCECOUNTER}
  Assert(QueryPerformanceCounter(NewFrame),'QueryPerformanceCounter fehlgeschlagen');
  {$ELSE}
  QueryPerformanceCounter(NewFrame);
  {$ENDIF}
  Time:=round(((NewFrame-fLastFrame)/CounterFrequency)*1000);
  if Time<1 then
    exit;
//  if Time<AnimTime then exit;
  IncLock;
  NeedFlip:=false;
  OldFrame:=fFrame;
  fLastFrame:=NewFrame;
  FoundDelete:=false;
  if (fShowCursor=0) and (MouseAnimation) then
  begin
    dec(fFrameTime,Time);
    while fFrameTime<0 do
    begin
      inc(fFrame);
      if fFrame>fCursor.EndFrame then
        fFrame:=fCursor.BeginFrame;
        
      inc(fFrameTime,fCursor.AnimTime);
    end;
  end;
  Dummy:=0;
  while Dummy<length(fFrameFunc) do
  begin
    with fFrameFunc[Dummy] do
    begin
      if not Delete then
      begin
        if Aktiv then
        begin
          fFrameLock:=true;
          inc(MSecs,Time);
          if MSecs>=Interval then
          begin
            Frames:=(MSecs div Interval);
            MSecs:=(MSecs mod Interval);

            if Func(Parameter,Frames) then
              NeedFlip:=true;
          end;
          fFrameLock:=false;
        end;
      end
      else
      begin
        FoundDelete:=true;
      end;
    end;
    inc(Dummy);
  end;
  if FoundDelete then CorrectFrameArray;
  DecLock;
  if (fLock<>0) or (fLoadGame<>0) then exit;
  if OldFrame<>fFrame then
  begin
    ReDrawCursor(fMousePos,fMousePos);
    NeedFlip:=false;
  end;
  if NeedFlip then Flip;
end;

procedure TDXContainer.SetCursor(Cursor: TDXMouseCursor);
begin
  if (fCursor.BeginFrame=Cursor.BeginFrame) and (fCursor.EndFrame=Cursor.EndFrame) then
    exit;

  fCursor:=Cursor;

  fFrame:=Cursor.BeginFrame;

  RedrawCursor(fMousePos,fMousePos);
end;

procedure TDXContainer.KillFocus;
var
  Dummy : Integer;
  Beg   : Integer;
begin
  Dummy:=fControls.IndexOf(fFocusControl);
  Beg:=Dummy;
  repeat
    inc(Dummy);
    if Dummy>fControls.Count-1 then Dummy:=0;
    if Beg=Dummy then exit;
  until (TDXComponent(fControls[Dummy]).TabStop) and (TDXComponent(fControls[Dummy]).Enabled) and (TDXComponent(fControls[Dummy]).Visible);
  FocusControl:=fControls[Dummy];
end;

procedure TDXContainer.RedrawDirect(Control: TDXComponent;
  Surface: TDirectDrawSurface; Flip: boolean);
var
  Mem: TDDSurfaceDesc;
begin
  if fLoadGame<>0 then
  begin
    exit;
  end;
  if not CanDraw then exit;
  IncLock;
  PerformFrameTime;
  DecLock;
  Surface.Canvas.Release;
  fTempSurface.Canvas.Release;
  if fBackground.Width=0 then
    fTempSurface.FillRect(Control.ClientRect,0)
  else
    fTempSurface.BltFast(Control.Left,Control.Top,Control.ClientRect,0,fBackGround);

  RestoreMouseRect(Surface);
  if Control.Visible then
  begin
    fTempSurface.Lock(Mem);
    fTempSurface.Unlock;
    Control.Draw(fTempSurface,Mem);
  end;
  fTempSurface.Canvas.Release;
  Surface.Canvas.Release;
  Surface.BltFast(Control.ClientRect.Left,Control.ClientRect.Top,Control.ClientRect,0,fTempSurface);
  DrawCursor(Surface);
  if (fLock=0) and Flip then Self.Flip;
end;

procedure TDXContainer.SetMouseAnim(const Value: boolean);
begin
  fMouseAnim := Value;
  if not Value then
  begin
    fFrame:=fCursor.BeginFrame;
    RedrawCursor(fMousePos,fMousePos);
  end;
end;

procedure TDXContainer.AddFrameFunction(proc: TFrameFunction;Sender: TObject;
  MilliSec: Integer);
begin
  if IndexOfFrameFunc(proc,Sender)<>-1 then
    exit;
    
  SetLength(fFrameFunc,length(fFrameFunc)+1);
  with fFrameFunc[length(fFrameFunc)-1] do
  begin
    Func:=proc;
    Parameter:=Sender;
    Interval:=MilliSec;
    MSecs:=0;
    Aktiv:=true;
    Delete:=false;
  end;
  {$IFDEF ASSERT_QUERYPERFORMANCECOUNTER}
  Assert(QueryPerformanceCounter(fLastFrame),'QueryPerformanceCounter fehlgeschlagen');
  {$ELSE}
  QueryPerformanceCounter(fLastFrame);
  {$ENDIF}
end;

procedure TDXContainer.DeleteFrameFunction(proc: TFrameFunction;Sender: TObject);
var
  FoundIndex : Integer;
begin
  FoundIndex:=IndexOfFrameFunc(proc,Sender);
  if FoundIndex<>-1 then
  begin
    fFrameFunc[FoundIndex].Delete:=true;
    if not fFrameLock then CorrectFrameArray;
  end;
//  fFrameLock:=false;
end;


function TDXContainer.IndexOfFrameFunc(Func: TFrameFunction;Sender: TObject): Integer;
var
  Dummy      : Integer;
begin
  result:=-1;
  for Dummy:=0 to length(fFrameFunc)-1 do
  begin
    if (not fFrameFunc[Dummy].Delete) and (@fFrameFunc[Dummy].Func=@Func) and (fFrameFunc[Dummy].Parameter=Sender) then
    begin
      result:=Dummy;
      exit;
    end;
  end;
end;

procedure TDXContainer.CorrectFrameArray;
var
  Dummy      : Integer;
  Dummy2     : Integer;
  Deleted    : Integer;
  Count      : Integer;
begin
  Deleted:=0;
  Dummy:=0;
  Count:=length(fFrameFunc);
  while (Dummy<Count) do
  begin
    if fFrameFunc[Dummy].Delete then
    begin
      inc(deleted);
      for Dummy2:=Dummy to Count-2 do
      begin
        fFrameFunc[Dummy2]:=fFrameFunc[Dummy2+1];
      end;
      dec(Count);
    end
    else inc(Dummy);
  end;
  SetLength(fFrameFunc,length(fFrameFunc)-Deleted);
end;

procedure TDXContainer.LockAll;
begin
  fLock:=fTempLock;
  fLoadGame:=fTempLoadGame;
end;

procedure TDXContainer.UnLockAll;
begin
  fTempLock:=fLock;
  fTempLoadGame:=fLoadGame;
  fLock:=0;
  fLoadGame:=0;
end;

procedure TDXContainer.SetMouseShadow(const Value: boolean);
begin
  fMouseShadow := Value;
  RedrawCursor(fMousePos,fMousePos);
end;

procedure TDXContainer.Stop(Stoppen: boolean);
begin
  fTimer.Enabled:=not Stoppen;
  fStop:=Stoppen;
  if not Stoppen then
    {$IFDEF ASSERT_QUERYPERFORMANCECOUNTER}
    Assert(QueryPerformanceCounter(fLastFrame),'QueryPerformanceCounter fehlgeschlagen');
    {$ELSE}
    QueryPerformanceCounter(fLastFrame);
    {$ENDIF}
end;

procedure TDXContainer.DoScrollMessage(Pos: TPoint; Direction: TScrollDirection);
var
  Dummy     : Integer;
  LeaveLoop : boolean;
  Control   : TDXComponent;
begin
  if fControls.Count=0 then
    exit;

  Pos:=ScreenToClient(Pos);
  Dummy:=fControls.Count-1;
  LeaveLoop:=false;
  if fControls.Count>0 then
  begin
    while not LeaveLoop do
    begin
      Control:=TDXComponent(fCOntrols.Items[Dummy]);
      if PtInRegion(Control.MouseRegion,Pos.X,Pos.Y) then
      begin
        if IsMessageActiv(Control) then
        begin
          Control.DoScroll(Direction,Pos);
          LeaveLoop:=true;
        end;
      end;
      dec(Dummy);
      if Dummy=-1 then LeaveLoop:=true;
    end;
  end;
end;

procedure TDXContainer.DoFlip;
begin
  if (fLock=0) and (fLoadGame=0) then Flip;
end;

procedure TDXContainer.AddRestoreFunction(Func: TThreadMethod);
var
  Index: Integer;
begin
  DeleteRestoreFunction(Func);
  Index:=Length(fRestoreList);
  SetLength(fRestoreList,Index+1);
  fRestoreList[Index]:=Func;
end;

type
  PMethodPointer = ^TMethodPointer;
  TMethodPointer = packed record
    pMethod: Pointer;
    pObject: TObject;
  end;

procedure TDXContainer.DeleteRestoreFunction(Func: TThreadMethod);
var
  Dummy  : Integer;
  Index  : Integer;
begin
  Index:=-1;
  for Dummy:=0 to Length(fRestoreList)-1 do
  begin
    if (TMethodPointer(fRestoreList[Dummy]).pMethod=TMethodPointer(Func).pMethod) and
       (TMethodPointer(fRestoreList[Dummy]).pObject=TMethodPointer(Func).pObject) then
      Index:=Dummy;
  end;
  if Index<>-1 then
  begin
    fRestoreList[Index]:=fRestoreList[length(fRestoreList)-1];
    SetLength(fRestoreList,length(fRestoreList)-1);
  end;
end;


function TDXContainer.GetTempSurface: TDirectDrawSurface;
begin
  result:=fTempSurface;
end;

procedure TDXContainer.MouseLeave;
begin
  if fOverControl<>nil then
  begin
    fOverControl.MouseLeave;
    fOverControl:=nil;
  end;
end;

procedure TDXContainer.SetBackground(const Value: TBitmap);
begin
  if Value=nil then
    fBackGround.SetSize(0,0)
  else
    fBackGround.LoadFromGraphic(Value);
end;

procedure TDXContainer.ReactiveFrameFunctions;
var
  Dummy : Integer;
begin
  {$IFDEF ASSERT_QUERYPERFORMANCECOUNTER}
  Assert(QueryPerformanceCounter(fLastFrame),'QueryPerformanceCounter fehlgeschlagen');
  {$ELSE}
  QueryPerformanceCounter(fLastFrame);
  {$ENDIF}
  for Dummy:=0 to high(fFrameFunc) do
    fFrameFunc[Dummy].Aktiv:=true;
end;

function TDXContainer.GetPageOfIndex(Index: Integer): TDXPage;
var
  Dummy: Integer;
begin
  result:=nil;
  for Dummy:=0 to fPages.Count-1 do
  begin
    if TDXPage(fPages[Dummy]).PageIndex=Index then
    begin
      result:=TDXPage(fPages[Dummy]);
      exit;
    end;
  end;
end;

procedure TDXContainer.OnMusicEnd(Sender: TObject);
begin
  fPlayingMusic:=false;
end;

{$IFNDEF MUSIKTEST}
procedure TDXContainer.ReadMusikFiles;
var
  Path: String;
begin
  Path:=ExtractFilePath(Application.ExeName)+'musik\';
  if FileExists(Path+'musik.ini') then
    gMusikList.ReadIniFile(Path+'musik.ini',Path)
  else
    gMusikList.ReadAllFiles(Path);
end;
{$ENDIF}

procedure TDXContainer.PlayMovie(FileName: String; Rect: TRect);
begin
  if Mediaplayer=nil then
    exit;
  if not FileExists(FileName) then
    exit;

  MediaPlayer.FileName:=FileName;
  MediaPlayer.Open;
  MediaPlayer.DisplayRect:=Rect;
  Stop(true);
  fMouseMove:=false;
  MediaPlayer.Play;

  while (not Application.Terminated) and (MediaPlayer.Position<MediaPlayer.Length) and (GetKeyState(VK_ESCAPE)>=0) do
  begin
    Application.ProcessMessages;
  end;
  MediaPlayer.Stop;
  fMouseMove:=true;
  Stop(false);
end;

procedure TDXContainer.SetMediaPlayer(const Value: TMediaPlayer);
begin
  fMediaPlayer := Value;
  fMediaPlayer.Display:=Self;
end;

procedure TDXContainer.PlayMusicCategory(Category: String);
begin
  if Category=fMusicCategory then
    exit;

  if gMusikList.SetCategory(Category) then
  begin
    fMusicCategory:=Category;
    PlayMusic(gMusikList.GetNextFile,gMusikList.CategoryFileCount=1 {loop})
  end;
end;

procedure TDXContainer.ReadSounds(FromArchiv: String; ResLoc: String);
var
  Archiv: TArchivFile;
  Dummy: Integer;
begin
  if fBassPlayer=nil then
    exit;

  Archiv:=TArchivFile.Create;
  Archiv.OpenArchiv(FromArchiv,true);
  GlobalFile.WriteLine;
  GlobalFile.Write('Sounds laden aus',FromArchiv);

  ResLoc:=lowerCase(ResLoc);

  if ResLoc = 'user' then
  begin
    // Alte Sounds aus dem Spielsatz freigeben
    for Dummy:=0 to fGameSetSounds.Count-1 do
      fBassPlayer.RemoveSample(HashString(fGameSetSounds[Dummy]));

    fGameSetSounds.Clear;
  end;

  for Dummy:=0 to Archiv.Count-1 do
  begin
    Archiv.OpenRessource(Dummy);
    if fBassPlayer.AddSample(HashString(ResLoc+'\'+Archiv.Files[Dummy].Name),Archiv.Stream) then
    begin
      GlobalFile.Write('Soundfile geladen',ResLoc+'\'+Archiv.Files[Dummy].Name);

      if ResLoc='user' then
        fGameSetSounds.Add(ResLoc+'\'+Archiv.Files[Dummy].Name);
    end;
  end;

  GlobalFile.WriteLine;
  Archiv.Free;
end;

procedure TDXContainer.RequestNextMusicFile(var FileName: String;
  var Loop: Boolean);
begin
  FileName:=gMusikList.GetNextFile;
  Loop:=false;
end;

procedure TDXContainer.ShowPage(PageIndex: Integer);
var
  Page: TDXPage;
begin
  Page:=GetPageOfIndex(PageIndex);
  if (Page<>nil) and (ActivePage<>Page) then
  begin
    if Page.ShowPageModal then
      ShowPageModal(Page)
    else
      ActivePage:=Page;
  end;
end;

procedure TDXContainer.BringToTop(Component: TDXComponent);
begin
  if fControls.IndexOf(Component)=-1 then
    exit;

  RemoveComponent(Component);
  AddComponent(Component);
end;

procedure TDXContainer.SetGamma(Value: Integer);
var
  Ramp  : TDDGammaRamp;
  Dummy : Integer;
begin
  fGamma := Value;
  if Primary.GammaControl<>nil then
  begin
    if Value>0 then
    begin
      Value:=round(Value*1.333333333);
      for Dummy:=0 to 255 do
      begin
        Ramp.red[Dummy]:=min(65535,round(fOrgGammaRamp.Red[Dummy]+fOrgGammaRamp.Red[Dummy]*Value/100));
        Ramp.green[Dummy]:=min(65535,round(fOrgGammaRamp.Green[Dummy]+fOrgGammaRamp.Green[Dummy]*Value/100));
        Ramp.blue[Dummy]:=min(65535,round(fOrgGammaRamp.Blue[Dummy]+fOrgGammaRamp.Blue[Dummy]*Value/100));
      end;
    end
    else
    begin
      Value:=100+Value;
      for Dummy:=0 to 255 do
      begin
        Ramp.red[Dummy]:=round(fOrgGammaRamp.Red[Dummy]*Value/100);
        Ramp.green[Dummy]:=round(fOrgGammaRamp.Green[Dummy]*Value/100);
        Ramp.blue[Dummy]:=round(fOrgGammaRamp.Blue[Dummy]*Value/100);
      end;
    end;
    Primary.GammaControl.SetGammaRamp(0,Ramp);
  end;

end;

function TDXContainer.IsReady: Boolean;
begin
  result:=fStatus=csReady;
end;

procedure TDXContainer.DoLostFocus;
begin
  inherited;

  fStatus:=csDeactivated;
end;

{ TDXComponent }

constructor TDXComponent.Create(Page: TDXPage);
begin
  Assert(Page<>nil);
  Page.AddComponent(Self);
  fContainer:=Page.fContainer;

  fFont:=TFont.Create;
  fFont.OnChange:=FontChange;

  fDXPage:=Page;
  fVisible:=true;
  fDeactivate:=false;
  fEnabled:=true;
  fDirectDraw:=false;
  fTabStop:=False;
end;

procedure TDXContainer.CreateParams(var Params: TCreateParams);
begin
  inherited;
  with Params do
  begin
    Style:=Style or WS_HSCROLL or WS_VSCROLL;
    Style := Style and not WS_BORDER;
    WindowClass.style := WindowClass.style and not (CS_HREDRAW or CS_VREDRAW);
  end;
end;

procedure TDXComponent.CreateMouseRegion;
begin
  fMouseRegion:=CreateRectRgn(fLeft,fTop,Right,Bottom);
end;

procedure TDXComponent.DblClick(Button: TMouseButton; x, y: Integer);
var
  State: TShiftState;
begin
  if Assigned(fOnDblClick) then
  begin
    if GetKeyState(VK_SHIFT)<0 then Include(State,ssShift);
    if GetKeyState(VK_MENU)<0 then Include(State,ssAlt);
    if GetKeyState(VK_Control)<0 then Include(State,ssCtrl);
    if GetKeyState(VK_LButton)<0 then Include(State,ssLeft);
    if GetKeyState(VK_MButton)<0 then Include(State,ssMiddle);
    if GetKeyState(VK_RButton)<0 then Include(State,ssRight);
    fOnDblClick(Self,Button,State,x,y);
  end;
end;

procedure TDXComponent.DeactiveControl;
begin
  if fDeactivate then exit;
  fTempEnabled:=fEnabled;
  fEnabled:=false;
  fDeactivate:=true;
  Container.LeaveObject(Self);
end;

destructor TDXComponent.Destroy;
begin
  if Parent<>nil then
    Parent.RemoveComponent(Self);
    
  DeleteObject(fMouseRegion);
  fFont.Free;
  inherited;
end;

procedure TDXComponent.DoClick;
begin
  if Assigned(fOnClick) then fOnClick(Self);
end;

procedure TDXComponent.DoScroll(Direction: TScrollDirection; Pos: TPoint);
begin
end;

procedure TDXComponent.Draw(Surface: TDirectDrawSurface;var Mem: TDDSurfaceDesc);
begin
end;

function TDXComponent.DrawEnabled: boolean;
begin
  result:=(fEnabled or (fDeactivate and fTempEnabled));
end;

procedure TDXComponent.EnableChange;
begin
end;

function TDXComponent.GetBottom: Integer;
begin
  result:=Top+Height;
end;

function TDXComponent.GetClientRect: TRect;
begin
  result:=Rect(Left,Top,Left+Width,Top+Height);
end;

function TDXComponent.GetHasFocus: boolean;
begin
  result:=(Container.FocusControl=Self);
end;

function TDXContainer.GetMessage: boolean;
begin
  result:=fMessageLock=0;
end;

procedure TDXComponent.GetFocus;
begin
end;

function TDXComponent.GetRight: Integer;
begin
  result:=Left+Width;
end;

procedure TDXComponent.KeyDown(var Key: Word; State: TShiftState);
begin
  if Assigned(fOnKeyDown) then fOnKeyDown(Self,Key,State);
end;

procedure TDXComponent.KeyPress(var Key: Word; State: TShiftState);
begin
  if Assigned(fOnKeyPress) then fOnKeyPress(Self,Key,State);
end;

procedure TDXContainer.MessageLock(Lock: boolean);
var
  Dummy: Integer;
begin
  if Lock then inc(fMessageLock) else if fMessageLock>0 then dec(fMessageLock);
  for Dummy:=0 to High(fFrameFunc) do
    fFrameFunc[Dummy].Aktiv:=not Lock;
  {$IFDEF ASSERT_QUERYPERFORMANCECOUNTER}
  Assert(QueryPerformanceCounter(fLastFrame),'QueryPerformanceCounter fehlgeschlagen');
  {$ELSE}
  QueryPerformanceCounter(fLastFrame);
  {$ENDIF}
end;

procedure TDXComponent.KeyUp(var Key: Word; State: TShiftState);
begin
  if Assigned(fOnKeyUp) then fOnKeyUp(Self,Key,State);
end;

procedure TDXComponent.KillFocus;
begin
end;

procedure TDXComponent.MouseDown(Button: TMouseButton;X, Y: Integer);
begin
  if TabStop then Container.FocusControl:=Self;
  if Assigned(fOnMouseDown) then fOnMouseDown(Self,Button,[],X,Y);
end;

procedure TDXComponent.MouseLeave;
begin
  Parent.SetHint(EmptyStr);
  fHintShow:=false;
  if Assigned(fOnMouseLeave) then fOnMouseLeave(Self);
end;

procedure TDXComponent.MouseMove(X, Y: Integer);
begin
  if Assigned(fOnMouseMove) then fOnMouseMove(Self,[],x,y);
end;

procedure TDXComponent.MouseOver;
begin
  Parent.SetHint(fHint);
  fHintShow:=true;
  if Assigned(fOnMouseOver) then fOnMouseOver(Self);
end;

procedure TDXComponent.MouseUp(Button: TMouseButton; X, Y: Integer);
begin
  if Assigned(fOnMouseUp) then fOnMouseUp(Self,Button,[],x,y);
end;

procedure TDXComponent.ReactiveControl;
begin
  Enabled:=fTempEnabled;
  fDeactivate:=false;
end;

procedure TDXComponent.RecreateMouseRegion;
begin
  if fMouseRegion<>0 then
    DeleteObject(fMouseRegion);
  CreateMouseRegion;
end;

procedure TDXComponent.Redraw;
begin
  if (not Visible) then exit;
  Container.RedrawControl(Self,Container.Surface);
end;

procedure TDXComponent.ReDrawRect(DrawRect: TRect;Surface: TDirectDrawSurface ;var Mem: TDDSurfaceDesc);
begin
  Draw(Surface,Mem);
end;

procedure TDXComponent.Resize(var NewLeft, NewTop, NewWidth,
  NewHeight: Integer);
begin                      
end;

procedure TDXComponent.Restore;
begin

end;

procedure TDXComponent.SetComponentHint(const Value: String);
begin
  fHint := Value;
  if fHintShow then Parent.SetHint(Value);
end;

procedure TDXComponent.SetEnabled(const Value: boolean);
begin
  if fEnabled=Value then exit;
  fEnabled := Value;
  Container.IncLock;
  Container.LeaveObject(Self);
  EnableChange;
  Container.DecLock;
  Redraw;
  fTempEnabled := Value;
end;

procedure TDXComponent.SetFocus;
begin
  Container.FocusControl:=Self;
end;

procedure TDXComponent.SetFont(const Value: TFont);
begin
  fFont.Assign(Value);
  Redraw;
end;

procedure TDXComponent.SetHeight(const Value: Integer);
begin
  fHeight := Value;
  Resize(fLeft,fTop,fWidth,fHeight);
  RecreateMouseRegion;
  if Visible then fDXPage.Redraw(fContainer.Surface);
end;

procedure TDXComponent.SetLeft(const Value: Integer);
begin
  fLeft := Value;
  Resize(fLeft,fTop,fWidth,fHeight);
  RecreateMouseRegion;
  if Visible then fDXPage.Redraw(fContainer.Surface);
end;

procedure TDXComponent.SetRect(Left, Top, Width, Height: Integer);
var
  RedrawRect: TRect;
begin
  RedrawRect:=Rect(fLeft,fTop,Right,Bottom);
  fLeft:=Left;
  fTop:=Top;
  fWidth:=Width;
  fHeight:=Height;
  UnionRect(RedrawRect,RedrawRect,Rect(Left,Top,Right,Bottom));
  Resize(fLeft,fTop,fWidth,fHeight);
  RecreateMouseRegion;
  Container.RedrawArea(RedrawRect,Container.Surface);
end;

procedure TDXComponent.SetTop(const Value: Integer);
begin
  fTop := Value;
  Resize(fLeft,fTop,fWidth,fHeight);
  RecreateMouseRegion;
  if Visible then fDXPage.Redraw(fContainer.Surface);
end;

procedure TDXComponent.SetVisible(const Value: boolean);
begin
  if fVisible=Value then exit;
  fVisible := Value;
  if (not Value) and HasFocus then Container.KillFocus;
  if Value and TabStop and (Container.FocusControl=nil) then Container.FocusControl:=Self;
  VisibleChange;
  Container.RedrawArea(ClientRect,Container.Surface);
end;

procedure TDXComponent.SetWidth(const Value: Integer);
begin
  fWidth := Value;
  Resize(fLeft,fTop,fWidth,fHeight);
  RecreateMouseRegion;
  if Visible then fDXPage.Redraw(fContainer.Surface);
end;


procedure TDXComponent.VisibleChange;
begin
end;


procedure TDXComponent.FontChange(Sender: TObject);
begin
end;

procedure TDXComponent.RestoreFontChange;
begin
  Font.OnChange:=FontChange;
end;

function TDXComponent.IsCaptured: boolean;
begin
  result:=fContainer.fCaptureControl=Self;
end;

{ TDXPage }

procedure TDXPage.Activate(LoadDefaults: boolean=true);
var
  Dummy   : Integer;
  Count   : Integer;
  TempSrc : TDirectDrawSurface;
  Surface : TDirectDrawSurface;
  Mem     : TDDSurfaceDesc;
  Mem2    : TDDSurfaceDesc;
  x,y     : Integer;
  Time    : Cardinal;
  Mask32  : Cardinal;
  Pointer1: Cardinal;
  Pointer2: Cardinal;
  TR,TG,TB: Cardinal;

  procedure Wait(Time: Cardinal;Milli: Cardinal=5);
  begin
    repeat
    until GetTickCount-Time>Milli;
  end;

begin
  if not Container.Scrolling then
  begin
    Mask32:=Mask or (Mask shl 16);
    if (Container.PageAnimation) and ((doFullScreen in Container.Options)) then
    begin
      Surface:=Container.fTempSurface;
      Surface.Draw(0,0,Container.Primary,false);
      Container.DrawCursor(Surface);
      Surface.Canvas.Release;
      Surface.Lock(PRect(nil)^,Mem);
      if Mode32Bit then
      begin
        for Dummy:=0 to 7 do
        begin
          Time:=GetTickCount;
          for Y:=0 to ScreenHeight-1 do
          begin
            Pointer1:=Integer(Mem.lpSurface)+(Y*Mem.lPitch);
            for X:=0 to ScreenWidth-1 do
            begin
              PInteger(Pointer1)^:=((PInteger(Pointer1)^ and Mask) shr 1);
              inc(Pointer1,4);
            end;
          end;
          Surface.Unlock;
          Container.Surface.Draw(0,0,Surface,false);
          Container.Flip;
          Wait(Time,25);
        end;
      end
      else
      begin
        for Dummy:=0 to 4 do
        begin
          Time:=GetTickCount;
          for Y:=0 to ScreenHeight-1 do
          begin
            Pointer1:=Integer(Mem.lpSurface)+(Y*Mem.lPitch);
            for X:=0 to ((ScreenWidth-1) div 2) do
            begin
              PInteger(Pointer1)^:=(PInteger(Pointer1)^ and Mask32) shr 1;
              inc(Pointer1,4);
            end;
          end;
          Surface.Unlock;
          Container.Surface.Draw(0,0,Surface,false);
          Container.Flip;
          Wait(Time,50);
        end;
      end;
      Container.Surface.Fill(0);
      Container.Flip;
    end;
  end;
  Container.Lock;
  if LoadDefaults then SetDefaults else LeaveModal;
  Dummy:=0;
  Count:=fControls.Count;
  while (Dummy<Count) do
  begin
    fContainer.AddComponent(TDXComponent(fControls[Dummy]));
    inc(Dummy);
  end;
  ReadBackGround;
  Container.Background:=fBackGround;
  Container.DoMouseMessage;
  Container.UnLock;
  Container.DrawContainer(Container.Surface);
  Container.Surface.Canvas.Release;
  if ((Animation=paNone) and Container.Scrolling) or (not Container.PageAnimation) or (not (doFullScreen in Container.Options)) then
  begin
    Container.DrawCursor(Container.Surface);
    Container.Flip;
    PageShown;
    exit;
  end;
  if not Container.Scrolling then
  begin
    Surface:=Container.Surface;
    TempSrc:=Container.fTempSurface;
//    TempSrc.SystemMemory:=true;
//    TempSrc.SetSize(ScreenWidth,ScreenHeight);
//
    TempSrc.Draw(0,0,Rect(0,0,ScreenWidth,ScreenHeight),Container.Surface,false);
    Container.DrawCursor(TempSrc);
    TempSrc.Lock(PRect(nil)^,Mem2);
    Surface.Canvas.Release;
    if Mode32Bit then
    begin
      for Dummy:=7 downto 1 do
      begin
        TR:=Mem2.ddpfPixelFormat.dwRBitMask;
        TG:=Mem2.ddpfPixelFormat.dwGBitMask;
        TB:=Mem2.ddpfPixelFormat.dwBBitMask;
        Mask32:=(TR and (TR shl Dummy)) or (TG and (TG shl Dummy)) or (TB and (TB shl Dummy));
        Time:=GetTickCount;
        Surface.Lock(PRect(nil)^,Mem);
        for Y:=0 to ScreenHeight-1 do
        begin
          Pointer1:=Integer(Mem.lpSurface)+(Y*Mem.lPitch);
          Pointer2:=Integer(Mem2.lpSurface)+(Y*Mem2.lPitch);
          for X:=0 to ScreenWidth-1 do
          begin
            PInteger(Pointer1)^:=((PInteger(Pointer2)^ and Mask32) shr Dummy);
            inc(Pointer1,4);
            inc(Pointer2,4);
          end;
        end;
        Surface.UnLock;
        Container.Flip;
        Wait(Time,25);
      end;
    end
    else
    begin
      for Dummy:=5 downto 1 do
      begin
        TR:=Mem2.ddpfPixelFormat.dwRBitMask;
        TG:=Mem2.ddpfPixelFormat.dwGBitMask;
        TB:=Mem2.ddpfPixelFormat.dwBBitMask;
        Mask32:=(TR and (TR shl Dummy)) or (TG and (TG shl Dummy)) or (TB and (TB shl Dummy));
        Mask32:=Mask32 or (Mask32 shl 16);
        Time:=GetTickCount;
        Surface.Lock(PRect(nil)^,Mem);
        for Y:=0 to ScreenHeight-1 do
        begin
          Pointer1:=Integer(Mem.lpSurface)+(Y*Mem.lPitch);
          Pointer2:=Integer(Mem2.lpSurface)+(Y*Mem2.lPitch);
          for X:=0 to ((ScreenWidth-1) div 2) do
          begin
            PInteger(Pointer1)^:=(PInteger(Pointer2)^ and Mask32) shr Dummy;
            inc(Pointer1,4);
            inc(Pointer2,4);
          end;
        end;
        Surface.UnLock;
        Container.Flip;
        Wait(Time,50);
      end;
    end;
    TempSrc.UnLock;
    Container.Surface.Draw(0,0,Rect(0,0,ScreenWidth,ScreenHeight),TempSrc,false);
    Container.Flip;
//    TempSrc.Free;
  end
  else
  begin
    if Animation=paLeftToRight then
    begin
      x:=0;
      Container.PlaySound(SPageIn);
      while (x<=ScreenWidth) do
      begin
        Time:=GetTickCount;
        Container.Primary.Draw(0,0,rect(ScreenWidth-x,0,ScreenWidth,ScreenHeight),Container.Surface,false);
        inc(x,ScrollTempo);
        Wait(Time);
      end;
    end
    else if Animation=paRightToLeft then
    begin
      x:=ScreenWidth;
      Container.PlaySound(SPageIn);
      while (x>=0) do
      begin
        Time:=GetTickCount;
        Container.Primary.Draw(x,0,rect(0,0,ScreenWidth,ScreenHeight),Container.Surface,false);
        dec(x,ScrollTempo);
        Wait(Time);
      end;
    end
    else if Animation=paUpToBottom then
    begin
      y:=0;
      Container.PlaySound(SPageIn);
      while (y<=ScreenHeight) do
      begin
        Time:=GetTickCount;
        Container.Primary.Draw(0,0,rect(0,ScreenHeight-y,ScreenWidth,ScreenHeight),Container.Surface,false);
        inc(y,ScrollTempo);
        Wait(Time);
      end;
    end
    else if Animation=paBottomToUp then
    begin
      y:=ScreenHeight;
      Container.PlaySound(SPageIn);
      while (y>=0) do
      begin
        Time:=GetTickCount;
        Container.Primary.Draw(0,y,rect(0,0,ScreenWidth,ScreenHeight),Container.Surface,false);
        dec(y,ScrollTempo);
        Wait(Time);
      end;
    end;
    Container.DrawCursor(Container.Primary);
  end;
  PageShown;
end;

procedure TDXPage.AddComponent(Component: TDXComponent);
begin
  if fControls.IndexOf(Component)<>-1 then exit;
  fControls.Add(Component);
  Redraw(Container.Surface);
end;

procedure TDXPage.ChangePage(Page: Integer);
begin
  Container.ShowPage(Page);
end;

function TDXPage.CloseModal: Boolean;
begin
  result:=fContainer.CloseModal;
end;

constructor TDXPage.Create(Container: TDXContainer);
begin
  Animation:=paNone;
  fControls:=TList.Create;
  fContainer:=Container;
  Container.AddPage(Self);

  SetLength(fSystemKeys,0);
end;

procedure TDXPage.DeactivatePage;
begin
  {$IFNDEF PUFFERBACKGROUND}
  fBackground.Free;
  fBackground:=nil;
  {$ENDIF}
end;

procedure TDXPage.DeleteHotKey(Zeichen: Char; Control: TDXComponent; ClickEvent: TNotifyEvent = nil);
var
  Dummy: Integer;
begin
  Zeichen:=LowerChar(Zeichen);
  for Dummy:=0 to length(fHotKeys)-1 do
  begin
    if (fHotKeys[Dummy].Zeichen=Zeichen) and (fHotKeys[Dummy].Component=Control) and (@fHotKeys[Dummy].Event=@ClickEvent) then
    begin
      if length(fHotKeys)>0 then
        fHotKeys[Dummy]:=fHotKeys[High(fHotKeys)];
      SetLength(fHotKeys,High(fHotKeys));
      exit;
    end;
  end;
end;

procedure TDXPage.DeleteSystemKey(Taste: Char; Func: TSystemKeyFunction;
  Data: Integer);
var
  Dummy: Integer;
begin
  for Dummy:=0 to length(fSystemKeys)-1 do
  begin
    if (fSystemKeys[Dummy].Zeichen=Taste) and (@fSystemKeys[Dummy].Proc=@Func) then
    begin
      if length(fSystemKeys)>0 then
        fSystemKeys[Dummy]:=fSystemKeys[length(fHotKeys)-1];
      SetLength(fSystemKeys,Length(fSystemKeys)-1);
      exit;
    end;
  end;
end;

destructor TDXPage.Destroy;
begin
  fContainer.RemovePage(Self);
  while (fControls.Count>0) do
  begin
    TDXComponent(fControls[0]).Free;
  end;
  fControls.Free;
  fBackground.Free;
  inherited;
end;

function TDXPage.GetBack: TBitmap;
begin
  result:=fBackGround;
end;

procedure TDXPage.HandleFKey(FKey: Byte);
begin
end;

procedure TDXPage.KeyPress(var Key: Word; State: TShiftState);
var
  Count : Integer;
  Dummy : Integer;
  Ch    : Char;
  Str   : String;
begin
  if Assigned(fOnKeyPress) then
    fOnKeyPress(Self,Key,State);

  Ch:=LowerChar(Char(Key));
  Str:=Char(Key);
  Count:=length(fHotKeys);
  Dummy:=0;
  while Dummy<Count do
  begin
    if Ch=fHotKeys[Dummy].Zeichen then
    begin
      if Assigned(fHotKeys[Dummy].Event) then
      begin
        fHotKeys[Dummy].Event(Self);
        exit;
      end
      else if fHotKeys[Dummy].Component.Visible and fHotKeys[Dummy].Component.Enabled then
      begin
        fHotKeys[Dummy].Component.DoClick;
        exit;
      end;
    end;
    inc(Dummy);
  end;
end;

procedure TDXPage.LeaveModal;
begin
end;

procedure TDXPage.PageShown;
begin
end;

procedure TDXPage.PerformSystemKey(Key: Char);
var
  Count : Integer;
  Dummy : Integer;
  Ch    : Char;
begin
  Ch:=LowerChar(Key);
  Count:=length(fSystemKeys);
  Dummy:=0;
  while Dummy<Count do
  begin
    if Ch=fSystemKeys[Dummy].Zeichen then
    begin
      fSystemKeys[Dummy].Proc(fSystemKeys[Dummy].Data);
      exit;
    end;
    inc(Dummy);
  end;
end;

procedure TDXPage.ReadBackGround;
var
  Archiv: TArchivFile;
begin
  if (fBackGroundName<>'') then
  begin
    if fBackground=nil then
    begin
      fBackground:=TBitmap.Create;
    end;
    Archiv:=TArchivFile.Create;
    Archiv.OpenArchiv(FDataFile,true);
    Archiv.OpenRessource(fBackgroundName);
    fBackground.LoadFromStream(Archiv.Stream);
    Archiv.Free;
  end
  else
  begin
    FreeAndNil(fBackground);
  end;
end;

procedure TDXPage.Redraw;
begin
  fContainer.Redraw(fContainer.Surface);
end;

procedure TDXPage.RegisterHotKey(Taste: Char; Control: TDXComponent; ClickEvent: TNotifyEvent);
var
  Index: Integer;
begin
  Assert((Control<>nil) or Assigned(ClickEvent));
  Index:=length(fHotKeys);
  SetLength(fHotKeys,Index+1);
  with fHotKeys[Index] do
  begin
    Zeichen:=LowerChar(Taste);
    Component:=Control;
    Event:=ClickEvent;
  end;
end;

procedure TDXPage.RegisterSystemKey(Taste: Char; Func: TSystemKeyFunction;
  Data: Integer);
var
  Index: Integer;
begin
  Index:=length(fSystemKeys);
  SetLength(fSystemKeys,Index+1);
  with fSystemKeys[Index] do
  begin
    Zeichen:=Taste;
    Proc:=Func;
  end;
  fSystemKeys[Index].Data:=Data;
end;

procedure TDXPage.RemoveComponent(Component: TDXComponent);
var
  Index: Integer;
begin
  Index:=fControls.IndexOf(Component);
  if Index=-1 then exit;
  fContainer.RemoveComponent(Component);
  fControls.Delete(Index);
end;

function TDXPage.ReshowPage: boolean;
begin
  result:=true;
end;

procedure TDXPage.Restore;
begin
end;

procedure TDXPage.SetBackGroundName(const Value: String);
begin
  fBackGroundName := Value;
  {$IFDEF PUFFERBACKGROUND}
  ReadBackGround;
  {$ENDIF}
end;

procedure TDXPage.SetButtonFont(Font: TFont);
var
  Chan: TNotifyEvent;
begin
  Chan:=Font.OnChange;
  Font.OnChange:=nil;
  Font.Name:=coFontName;
  Font.Size:=coFontSize;
  Font.OnChange:=Chan;
  Font.Color:=coButtonColor;
end;

procedure TDXPage.SetDefaults;
begin
end;

procedure TDXPage.SetFont(Font: TFont);
var
  Chan: TNotifyEvent;
begin
  Chan:=Font.OnChange;
  Font.OnChange:=nil;
  Font.Name:=coFontName;
  Font.Size:=coFontSize;
  Font.OnChange:=Chan;
  Font.Color:=coFontColor;
end;

procedure TDXPage.SetHint(Hint: String);
begin
end;

procedure TDXPage.SetPageHint(const Value: String);
begin
  fHint := Value;
  SetHint(Value);
end;

{$IFNDEF MUSIKTEST}
initialization
  gMusikList:=TMusikList.Create;

finalization
  gMusikList.Free;
{$ENDIF}

end.


