{********************************************************************************
*										*
* Copyright (C) 2004  X-Force Team - jim_raynor@web.de				*
*										*
* This file is part of X-Force: Fight For Destiny (X-Force).			*
*										*
* X-Force is free software; you can redistribute it and/or modify		*
* it under the terms of the GNU General Public License as published by		*
* the Free Software Foundation; either version 2 of the License, or		*
* (at your option) any later version.						*
*										*
* X-Force is distributed in the hope that it will be useful,			*
* but WITHOUT ANY WARRANTY; without even the implied warranty of		*
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the			*
* GNU General Public License for more details.					*
*										*
* You should have received a copy of the GNU General Public License		*
* along with X-Force; if not, write to the Free Software			*
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA	*
*										*
*********************************************************************************
*										*
* Berechnet die Simulation des Raumschiffkampfes.				*
*										*
********************************************************************************}

{$I ../Settings.inc}

unit Simulator;

interface

uses
  UFOList, RaumschiffList, XForce_types, Forms, Windows, KD4Utils, SysUtils,
  Defines, TraceFile, StringConst;

type
  TSimulationResult = (srSchiffWin, srUFOWin);
  TSimulatorMessage = procedure(Text: String) of object;

  TSimulatorObject  = (soUFO,soRaumschiff);
  PWeaponEntry      = ^TWeaponEntry;
  TWeaponEntry      = record
                        Objekt : TSimulatorObject;
                        Zelle  : Integer;
                        Time   : Integer;
                        Next   : PWeaponEntry;
                      end;
  TSimulator = class
  private
    fOnMessage  : TSimulatorMessage;
    fNextEntry  : PWeaponEntry;
    fRaumschiff : TRaumschiff;
    fUFO        : TUFO;
    procedure SendMessage(Text: String;mSecs: Integer);
    procedure Clear;
    procedure InsertShoot(Objekt: TSimulatorObject; Zelle: Integer; Time: Integer);
    procedure RemoveFirst;

    procedure ReduceTime(Time: Integer);
  public
    destructor Destroy;override;
    function Simulate(Raumschiff: TRaumschiff; UFO: TUFO): TSimulationResult;

    property OnMessage: TSimulatorMessage read fOnMessage write fOnMessage;
  end;

implementation

{ TSimulator }

procedure TSimulator.Clear;
var
  Next: PWeaponEntry;
  tmp : PWeaponEntry;
begin
  Next:=fNextEntry;
  while Next<>nil do
  begin
    tmp:=Next;
    Next:=Next.Next;
    FreeMem(tmp);
  end;
end;

destructor TSimulator.Destroy;
begin
  Clear;
  inherited;
end;

procedure TSimulator.InsertShoot(Objekt: TSimulatorObject; Zelle,
  Time: Integer);
var
  Next: PWeaponEntry;
  Last: PWeaponEntry;
  tmp : PWeaponEntry;

  function CreateShoot: PWeaponEntry;
  begin
    GetMem(result,SizeOf(TWeaponEntry));
    result.Objekt:=Objekt;
    result.Zelle:=Zelle;
    Result.Time:=Time;
    result.Next:=nil;
  end;

begin
  if fNextEntry=nil then
    fNextEntry:=CreateShoot
  else
  begin
    if fNextEntry.Time>Time then
    begin
      tmp:=fNextEntry;
      fNextEntry:=CreateShoot;
      fNextEntry.Next:=tmp;
    end
    else
    begin
      Last:=fNextEntry;
      Next:=fNextEntry.Next;
      while Next<>nil do
      begin
        if Next.Time>Time then
        begin
          tmp:=CreateShoot;
          Last.Next:=tmp;
          tmp.Next:=Next;
          exit;
	end;
        Last:=Next;
        Next:=Next.Next;
      end;
      // Hinten anf�gen
      Last.Next:=CreateShoot;
    end;
  end;
end;

procedure TSimulator.ReduceTime(Time: Integer);
var
  Next: PWeaponEntry;
begin
  fRaumschiff.RechargeShield(Time);
  fUFO.RechargeShield(Time);
  Next:=fNextEntry;
  while (Next<>nil) do
  begin
    dec(Next.Time,Time);
    Next:=Next.Next;
  end;
end;

procedure TSimulator.RemoveFirst;
var
  tmp: PWeaponEntry;
begin
  tmp:=fNextEntry.Next;
  FreeMem(fNextEntry);
  fNextEntry:=tmp;
end;

procedure TSimulator.SendMessage(Text: String;mSecs: Integer);
var
  Time: Cardinal;
begin
  if Assigned(fOnMessage) then
    fOnMessage(Text);

  Time:=GetTickCount;
  repeat
    Application.ProcessMessages;
  until GetTickCount-Time>mSecs;
end;

function TSimulator.Simulate(Raumschiff: TRaumschiff;
  UFO: TUFO): TSimulationResult;
var
  Absorb    : Integer;
  Dummy     : Integer;
  CanShoot  : Boolean;
  Zelle     : TWaffenZelle;
begin
  fRaumschiff:=Raumschiff;
  fUFO:=UFO;
  fRaumschiff.RechargeShield(0,true);
  fUFO.RechargeShield(0,true);
  Clear;
  // Raumschiffwaffen einf�gen
  for Dummy:=0 to Raumschiff.WaffenZellen-1 do
    with Raumschiff.WaffenZelle[Dummy] do
      if Installiert then
        InsertShoot(soRaumschiff,Dummy,Laden);

  // UFO einf�gen
  InsertShoot(soUFO,0,UFOShootTime);
  repeat
    ReduceTime(fNextEntry.Time);
    case fNextEntry.Objekt of
      soRaumschiff :
      begin
        // Pr�fen, ob f�r diesen Schuss noch genug Munition zur Verf�gung steht
        CanShoot:=true;
        if fRaumschiff.WaffenZelle[fNextEntry.Zelle].Munition=0 then
        begin
          if fRaumschiff.WaffenZelle[fNextEntry.Zelle].WaffType=wtLaser then
          begin
            Zelle:=fRaumschiff.WaffenZelle[fNextEntry.Zelle];
            inc(Zelle.Munition);
            fRaumschiff.WaffenZelle[fNextEntry.Zelle]:=Zelle;
            CanShoot:=false;
            InsertShoot(soRaumschiff,fNextEntry.Zelle,(fRaumschiff.WaffenZelle[fNextEntry.Zelle].Strength*2)-fRaumschiff.WaffenZelle[fNextEntry.Zelle].Laden);
	  end
          else
          begin
            CanShoot:=false;
            SendMessage(Format(ST0312160001,[fNextEntry.Zelle+1]),1000);
          end;
        end
        else
        begin
          Zelle:=fRaumschiff.WaffenZelle[fNextEntry.Zelle];
          dec(Zelle.Munition);
          fRaumschiff.WaffenZelle[fNextEntry.Zelle]:=Zelle;
        end;
        if CanShoot then
        begin
          SendMessage(Format(ST0311080001,[fNextEntry.Zelle+1,Raumschiff.WaffenZelle[fNextEntry.Zelle].Strength]), 1000);
          if RandomChance(60) then
          begin
            if UFO.DoTreffer(Raumschiff.WaffenZelle[fNextEntry.Zelle].Strength,Raumschiff.WaffenZelle[fNextEntry.Zelle].WaffType,Absorb) then
            begin
              UFO:=nil;
              SendMessage(ST0311080002, 3000);
            end
            else
              SendMessage(Format(ST0311080003,[UFO.Hitpoints,UFO.ShieldPoints]), 750);
          end
          else
          begin
            SendMessage(ST0311080004, 500);
          end;
          // Schuss wieder einf�gen
          InsertShoot(soRaumschiff,fNextEntry.Zelle,round(Raumschiff.WaffenZelle[fNextEntry.Zelle].Laden));
        end;
      end;
      soUFO :
      begin
        SendMessage(ST0501080006, 1000);
        if RandomChance(UFO.Model.Treffsicherheit) then
        begin
          if Raumschiff.DoTreffer(UFO.Model.Angriff,Absorb,UFO) then
          begin
            SendMessage(ST0501080007, 3000);
            Raumschiff:=nil;
	  end
          else
            SendMessage(Format(ST0501080008,[Raumschiff.AktHitPoints,Raumschiff.ShieldPoints]), 750);
	end
        else
          SendMessage(ST0311080004, 500);

        InsertShoot(soUFO,0,round(UFOShootTime));
      end;
    end;

    RemoveFirst;
    if fNextEntry=nil then
      exit;
  until (UFO=nil) or (Raumschiff=nil);
  if UFO=nil then
  begin
    result:=srSchiffWin;
    Raumschiff.DoAbschuss;
  end
  else if Raumschiff=nil then
    result:=srUFOWin;
end;

end.
