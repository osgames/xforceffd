{********************************************************************************
*										*
* Copyright (C) 2004  X-Force Team - jim_raynor@web.de				*
*										*
* This file is part of X-Force: Fight For Destiny (X-Force).			*
*										*
* X-Force is free software; you can redistribute it and/or modify		*
* it under the terms of the GNU General Public License as published by		*
* the Free Software Foundation; either version 2 of the License, or		*
* (at your option) any later version.						*
*										*
* X-Force is distributed in the hope that it will be useful,			*
* but WITHOUT ANY WARRANTY; without even the implied warranty of		*
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the			*
* GNU General Public License for more details.					*
*										*
* You should have received a copy of the GNU General Public License		*
* along with X-Force; if not, write to the Free Software			*
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA	*
*										*
*********************************************************************************
*										*
* Stellt das User-Interface (untere Soldatenliste und Linkes User-Interface)	*
* zur Verf�gung.								*
*										*
********************************************************************************}

{$I ../Settings.inc}

unit DXUIBodenEinsatz;

interface

{$DEFINE MULTISELECT}
uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  DXContainer,DXDraws, DXISOEngine, DirectDraw, Blending, SoldatenList,
  DirectFont, DXBitmapButton,Defines, XForce_types, KD4Page, DXClass, KD4Utils,
  DXISOMiniMap, GameFigureManager, GameFigure, VerSoldatEinsatz, ISOMessages,
  TraceFile, NGTypes, PathFinder, Menus, DXInput, StringConst, DXSpinEdit,
  ISOObjects, NotifyList;

type
  TDXUISoldatenList = class;

  TUIPage = (uipNone,uipSoldaten,{$IFNDEF DISABLEBEAM}uipBeaming,uipGroupBeaming,{$ENDIF}uipSettings);

  PButtonInfo = ^TButtonInfo;
  TButtonInfo = record
    ID          : Integer;
    Draw        : boolean;
    Rect        : TRect;
    ImageRect   : TRect;
    DrawFunc    : TDrawIconEvent;
    Status      : DXBitmapButton.TButtonState;
    Click       : TNotifyEvent;
    ShortHint   : String;
    LongHint    : String; //Gef�llt in CalculateShortHintRect
    Key         : TShortCut;
    ShowKey     : boolean;
    HintRect    : TRect;
    Data        : Integer;
    Visible     : boolean;
    Highlight   : boolean;
    Enabled     : boolean;
    ImageAlign  : TAlignment;
    ImageOffset : Integer;
    DefaultPage : TUIPage;
    TimeUnits   : Integer;
  end;

  TDXUIBodenEinsatz = class(TDXComponent)
  private
    fFigure            : TGameFigure;
    fISOEngine         : TDXISOEngine;
    fEchtzeit          : boolean;
    fButtons           : Array of TButtonInfo;
    fUISol             : TDXUISoldatenList;
    fOverID            : Integer;
    fMiniMap           : TDXISOMiniMap;
    fUIPage            : TUIPage;
    {$IFNDEF DISABLEBEAM}
    fBeamUnit          : TGameFigure;
    fNextBeam          : Integer;
    {$ENDIF}
    fMerkeRedraw       : boolean;
    fDefaultButtons    : Integer;
    fOverButton        : PButtonInfo;
    fUIImages          : TDirectDrawSurface;

    fSpinEdit          : TDXSpinEdit;

    {$IFNDEF DISABLEFORMATION}
    fFormations        : TList;
    fFormation         : TGroupFormation;
    fFormationButton   : Integer;

    fAllFormations     : boolean;
    {$ENDIF}

    fTakeAllButton     : Integer;

    fChangeSpeedButton : Integer;

    fFigureDestroy     : TEventHandle;

    // Informationen zum Objekt, dass geworfen werden soll
    fThrowInformation  : record
                           ThrowMode : boolean;
                           FromUnit  : TGameFigure;
                           FromSlot  : TSoldatConfigSlot;
                           FromData  : Integer;
                           Button    : PButtonInfo;
                         end;

    procedure SetFigure(const Value: TGameFigure);
    procedure SetEchtzeit(const Value: boolean);
    procedure SetUISol(const Value: TDXUISoldatenList);
    procedure SetMiniMap(const Value: TDXISOMiniMap);
    procedure CalculateShortHintRect(var Hint: TButtonInfo);
    procedure SetUIPage(const Value: TUIPage);

    procedure FigureDestroy(Sender: TObject);

    procedure AktuButtons;

    {$IFNDEF DISABLEFORMATION}
    procedure AktuFormationsButtons;
    procedure SystemClickFormation(Data: Integer);
    {$ENDIF}

    procedure AktuTakeItemsTU;

    {$IFNDEF DISABLEBEAM}
    procedure StartBeamTimer;
    function  SekTimer(Sender: TObject;Frames: Integer): boolean;
    {$ENDIF}

    procedure ChangeTileGrid(Sender: TObject);

    {$IFNDEF DISABLEBEAM}
    procedure BeamCancel(Sender: TObject);
    procedure BeamDown(Sender: TObject);
    procedure BeamUp(Sender: TObject);
    procedure BeamUnitDown(Sender: TObject;X,Y: Integer; Button: TMouseButton);
    procedure BeamGroupDown(Sender: TObject;X,Y: Integer; Button: TMouseButton);
    {$ENDIF}

    procedure Einheitausrusten(Sender: TObject);
    procedure TakeItems(Sender: TObject);
    procedure EscClick(Sender: TObject);
    procedure SelectUnit(Sender: TObject);
    procedure ChangeUnit(Sender: TObject);
    procedure CameraClick(Sender: TObject);

    function SetThrowMode(Figure: TGameFigure; Slot: TSoldatConfigSlot; Data: Integer): boolean;
    procedure CancelThrowMode;
    procedure ChooseThrowTarget(Sender: TObject;X,Y: Integer; Button: TMouseButton);

    // Zeichenfunktionen
    procedure DrawWaffe(Surface: TDirectDrawSurface;const Mem: TDDSurfaceDesc; var DrawTop: Integer; const Waffe: TSoldatWaffe);
    procedure DrawCaption(Surface: TDirectDrawSurface;const Mem: TDDSurfaceDesc;Text: String; Top: Integer; OffSet: Integer = 4);
    procedure DrawStatus(Surface: TDirectDrawSurface; const Mem: TDDSurfacedesc;const ImageRect: TRect; Text: String; Top: Integer);

    procedure BuildWaffenMenue(const Waffe: TSoldatWaffe;var TopPos: Integer; Slot: TSoldatConfigSlot);
    procedure BuildGuertelMenue;

    procedure SetSchussArt(SchussArt: TSchussType; Slot: TSoldatConfigSlot; All: Boolean);

    procedure ClickSchussArt(Sender: TObject);

    procedure UseObject(Sender: TObject);
    procedure ThrowObject(Sender: TObject);

    procedure UseObjectFast(ObjectType: TProjektType);

    procedure UseGreanadeFast(Sender: TObject);
    procedure UseSensorFast(Sender: TObject);
    procedure UseMineFast(Sender: TObject);

    function AddNewButton: PButtonInfo;

    function CanFriendsMove: boolean;

    { Private-Deklarationen }
  protected
    procedure ChangeFigure(Sender: TObject);
    procedure ChangePause(Sender: TObject);
    procedure NextRoundClick(Sender: TObject);
    procedure ChangeMiniMap(Sender: TObject);
    procedure ShowMiniMap(Sender: TObject);
    procedure HideMiniMap(Sender: TObject);
    procedure ShowUnitSettings(Sender: TObject);
    procedure ShowUnits(Sender: TObject);

    {$IFNDEF DISABLEFORMATION}
    procedure ClickFormation(Sender: TObject);
    procedure ClickAllFormations(Sender: TObject);
    {$ENDIF}

    procedure InitButtons;

    function GetImageRect(Image: Integer): TRect;
    procedure Restore;override;
    { Protected-Deklarationen }
  public
    constructor Create(Page: TDXPage);override;
    destructor destroy;override;
    procedure MouseMove(X,Y: Integer);override;
    procedure MouseLeave;override;
    procedure MouseDown(Button: TMouseButton;X,Y: Integer);override;
    procedure MouseUp(Button: TMouseButton;X,Y: Integer);override;
    procedure Redraw;override;
    procedure Draw(Surface: TDirectDrawSurface;var Mem: TDDSurfaceDesc);override;
    procedure RedrawRect(DrawRect: TRect;Surface: TDirectDrawSurface;var Mem: TDDSurfaceDesc);override;
    procedure Resize(var NewLeft,NewTop,NewWidth,NewHeight: Integer);override;
    procedure ChangeHotKeys(Registration: boolean);

    {$IFNDEF DISABLEFORMATION}
    procedure RegisterFormation(Formation: TGroupFormation);
    {$ENDIF}

    procedure SetStartState;
    procedure RoundEnd;

    procedure SetNextFigure(Next: boolean);
    {$IFNDEF DISABLEBEAM}
    procedure ShowBeamMenu(Figure: TGameFigure);
    {$ENDIF}

    function NotifyMessage(MessageType: TNotifyUIType; Figure: TGameFigure): boolean;

    property Figure         : TGameFigure read fFigure write SetFigure;
    property Echtzeit       : boolean read fEchtzeit write SetEchtzeit;
    property ISOEngine      : TDXISOEngine read fISOEngine write fISOEngine;
    property ISOMiniMap     : TDXISOMiniMap read fMiniMap write SetMiniMap;
    property Soldaten       : TDXUISoldatenList read fUISol write SetUISol;
    property Page           : TUIPage read fUIPage write SetUIPage;

    {$IFNDEF DISABLEFORMATION}
    property Formation      : TGroupFormation read fFormation;
    {$ENDIF}
    { Public-Deklarationen }
  end;

  TDXUISoldatenList = class(TDXComponent)
  private
    fUI            : TDXUIBodenEinsatz;
    Figur          : TGameFigure;
    FigurSeeRect   : TGameFigure;
    fSelected      : TGameFigure;
    fHintText      : String;
    fHintRect      : TRect;
    fHintSeeCount  : Integer;
    fMustRedraw    : boolean;

    procedure SetSelected(const Value: TGameFigure);
    procedure SetUI(const Value: TDXUIBodenEinsatz);
    function AktuHint: boolean;
    procedure RecalcHintRect(X,Y: Integer);
  protected
    function GetSolAtPos(Pos: TPoint;var X: Integer): TGameFigure;
    function GetAlienCountRect(Pos: TPoint; var X: Integer): TGameFigure;
  public
    constructor Create(Page: TDXPage);override;
    procedure MouseMove(X,Y: Integer);override;
    procedure MouseDown(Button: TMouseButton;X,Y: Integer);override;
    procedure DblClick(Button: TMouseButton;x,y: Integer);override;
    procedure MouseLeave;override;
    procedure Redraw;override;
    procedure Draw(Surface: TDirectDrawSurface;var Mem: TDDSurfaceDesc);override;
    procedure RedrawRect(DrawRect: TRect;Surface: TDirectDrawSurface;var Mem: TDDSurfaceDesc);override;
    procedure CheckRedraw;

    property UserInterface : TDXUIBodenEinsatz read fUI write SetUI;
    property Selected      : TGameFigure read fSelected write SetSelected;
  end;

  const
    {$IFNDEF DISABLEBEAM}
    BeamDownRect   : TRect = (Left:   0;Top: 22;Right:   6; Bottom: 36);
    BeamUpRect     : TRect = (Left:   7;Top: 22;Right:  13; Bottom: 36);
    {$ENDIF}
    CancelRect     : TRect = (Left: 128;Top:104;Right: 149; Bottom:125);
    FigureRect     : TRect = (Left: 140;Top: 20;Right: 144; Bottom: 24);
    LeftRect       : TRect = (Left: 140;Top: 35;Right: 154; Bottom: 47);
    RightRect      : TRect = (Left: 155;Top: 35;Right: 169; Bottom: 47);

    SpontanRect    : TRect = (Left: 139;Top: 49;Right: 155; Bottom: 65);
    GezieltRect    : TRect = (Left: 155;Top: 49;Right: 171; Bottom: 65);
    AutoRect       : TRect = (Left: 171;Top: 49;Right: 187; Bottom: 65);
    NoShootRect    : TRect = (Left: 139;Top: 65;Right: 155; Bottom: 81);
    SchlagRect     : TRect = (Left: 124;Top: 65;Right: 140; Bottom: 81);

    ConfigRect     : TRect = (Left: 179;Top: 34;Right: 186; Bottom: 49);
    TakeRect       : TRect = (Left: 163;Top: 65;Right: 173; Bottom: 81);
    UseRect        : TRect = (Left: 155;Top: 65;Right: 163; Bottom: 81);
    ThrowRect      : TRect = (Left: 174;Top: 65;Right: 187; Bottom: 82);
    HealthRect     : TRect = (Left:  28;Top: 20;Right:  43; Bottom: 35);
    TimeUnitsRect  : TRect = (Left:  43;Top: 20;Right:  58; Bottom: 35);
    HalfSpeedRect  : TRect = (Left: 233;Top:  0;Right: 256; Bottom: 20);

    {$IFNDEF DISABLEBEAM}
    BeamTime        = 149;  // in zehntel Sekunden
    {$ENDIF}
    WaffenMenueSize = 90;

    {$IFNDEF DISABLEFORMATION}
    UnitFacePos     : TPoint = (x: 1; y: 137);
    {$ELSE}
    UnitFacePos     : TPoint = (x: 1; y: 42);
    {$ENDIF}

    WaffenMenueTop  : Integer = 133;
    UnitOrdersTop   : Integer = 71;

implementation

uses
  lager_api, draw_utils, person_api, typinfo;

var
  SchussArten : Array[TSchussType] of record
                  SymbolRect  : TRect;
                  Hint        : String;
                  TimeUnits   : Integer;
                  TUMulti     : double;     // Wenn TimeUnits=0 ist wird die WaffenZeiteinheiten mit diesem Multiplikator multipliziert
                end;

{ TDXUIBodenEinsatz }

procedure TDXUIBodenEinsatz.AktuButtons;
var
  Dummy     : Integer;
  YTop      : Integer;
  InSchiff  : boolean;
  Button    : PButtonInfo;
begin
  { Einstellungen zur�cksetzen }
  Parent.DeleteHotKey(#27,nil,EscClick);
  {$IFDEF DONTRESETTHROWMODE}
  if not fThrowInformation.ThrowMode then
  begin
  {$ENDIF}
  ISOEngine.OnTileClick:=nil;
  fThrowInformation.Button:=nil;
  fThrowInformation.ThrowMode:=false;
  {$IFDEF DONTRESETTHROWMODE}
  end;
  {$ENDIF}

  { Buttons sichtbar/unsichtbar machen }

  for Dummy:=0 to high(fButtons) do
  begin
    with fButtons[Dummy] do
    begin
      if DefaultPage<>uipNone then
        Visible:=(fUIPage=DefaultPage);
    end;
  end;

  SetLength(fButtons,fDefaultButtons);

  fOverButton:=nil;
  fOverID:=-1;

  fTakeAllButton:=-1;
  fSpinEdit.Visible:=false;
  case fUIPage of
    uipSettings            :
    begin
      {$IFNDEF DISABLEFORMATION}
      AktuFormationsButtons;
      {$ENDIF}
      if {$IFNDEF DISABLEFORMATION}(not fAllFormations) and {$ENDIF} (fFigure<>nil) then
      begin
        Assert(fFigure.Manager<>nil);
        if (ISOEngine.Figures.SelectedCount>1) or ((not Echtzeit) and (ISOEngine.Figures.Count>1)) then
        begin
          Button:=AddNewButton;
          with Button^ do
          begin
            Click:=ChangeUnit;
            Rect:=Classes.Rect(Left,Top+UnitFacePos.y,Left+20,Top+UnitFacePos.y+19);
            ImageRect:=LeftRect;
            ShortHint:=ST0309220053;
            Key:=ShortCut(Word(KeyConfiguration.BodenKeys.PrevSoldat),[]);
            Data:=Integer(false);
          end;
          CalculateShortHintRect(Button^);
          Button:=AddNewButton;
          with Button^ do
          begin
            Click:=ChangeUnit;
            Rect:=Classes.Rect(Right-19,Top+UnitFacePos.y,Right-1,Top+UnitFacePos.y+19);
            ImageRect:=RightRect;
            ShortHint:=ST0309220054;
            Key:=ShortCut(Word(KeyConfiguration.BodenKeys.NextSoldat),[]);
            Data:=Integer(true);
          end;
          CalculateShortHintRect(Button^);
        end;

        {$IFNDEF DISABLEBEAM}
        Button:=AddNewButton;
        with Button^ do
        begin
          ImageRect:=BeamUpRect;
          Click:=BeamUp;
          Rect:=Bounds(Left+22,Top+UnitOrdersTop,11,21);
          ShortHint:=ST0309220055;
          Data:=Integer(fFigure);
          TimeUnits:=TUBeam;
          Key:=ShortCut(Word(KeyConfiguration.BodenKeys.BeamUp),[]);
        end;
        CalculateShortHintRect(Button^);
        {$ENDIF}

        Button:=AddNewButton;
        with Button^ do
        begin
          Rect:=Bounds(Left+2,Top+UnitOrdersTop,19,21);
          ImageRect:=ConfigRect;
          Click:=Einheitausrusten;
          ShortHint:=ST0309220056;
          Data:=Integer(fFigure);
          ImageAlign:=taCenter;
          Key:=ShortCut(Word(KeyConfiguration.BodenKeys.Ausruesten),[]);
        end;
        CalculateShortHintRect(Button^);

        Button:=AddNewButton;
        with Button^ do
        begin
          Rect:=Bounds(Left+34,Top+UnitOrdersTop,19,21);
          ImageRect:=TakeRect;
          Click:=TakeItems;
          ShortHint:=ST0309220057;
          Data:=Integer(fFigure);
          ImageAlign:=taCenter;
          Key:=ShortCut(Word(KeyConfiguration.BodenKeys.TakeAll),[]);
        end;
        fTakeAllButton:=Button.ID;
        CalculateShortHintRect(Button^);

        AktuTakeItemsTU;

        YTop:=Top+WaffenMenueTop;
        BuildWaffenMenue(fFigure.Manager.GetAddrOfSlot(scsLinkeHand)^,YTop,scsLinkeHand);
        BuildWaffenMenue(fFigure.Manager.GetAddrOfSlot(scsRechteHand)^,YTop,scsRechteHand);

        BuildGuertelMenue;
      end;
    end;
    {$IFNDEF DISABLEBEAM}
    uipGroupBeaming        :
    begin
      Assert(high(fButtons)>=4);
      fButtons[4].Visible:=true;
      fButtons[4].Key:=ShortCut(0,[]);
      CalculateShortHintRect(fButtons[4]);
      temp:=fAllFormations;
      fAllFormations:=true;
      AktuFormationsButtons;
      fAllFormations:=Temp;
      ISOEngine.OnTileClick:=BeamGroupDown;
      Count:=0;
      for Dummy:=0 to ISOEngine.Figures.Count-1 do
      begin
        if ISOEngine.Figures[Dummy].FigureStatus=fsFriendly then
          inc(Count);
      end;
      fSpinEdit.Max:=Count;
      fSpinEdit.Visible:=true;
      fSpinEdit.SetRect(Left+20,Top+300,100,20);
      exit;
    end;
    uipBeaming             :
    begin
      Assert(high(fButtons)>=4);
      Parent.RegisterHotKey(#27,nil,EscClick);
      fButtons[4].Key:=ShortCut(VK_ESCAPE,[]);
      CalculateShortHintRect(fButtons[4]);
      ISOEngine.OnTileClick:=BeamUnitDown;
      exit;
    end;
    {$ENDIF}
    uipSoldaten            :
    begin
      with ISOEngine do
      begin
        YTop:=Self.Top+83;
        for Inschiff:=true downto false do
        begin
          for Dummy:=0 to Figures.Count-1 do
          begin
            if (Figures[Dummy].ImRaumschiff=InSchiff) and (Figures[Dummy].FigureStatus=fsFriendly) then
            begin
              if not Figures[Dummy].IsDead then
              begin
                if not InSchiff then
                begin
                  with AddNewButton^ do
                  begin
                    Draw:=false;
                    Rect:=Classes.Rect(Self.Left+34,YTop,Self.Right-2,YTop+21);
                    Click:=SelectUnit;
                    Data:=Integer(Figures[Dummy]);
                  end;
                end;

                {$IFNDEF DISABLEBEAM}
                Button:=AddNewButton;
                with Button^ do
                begin
                  Rect:=Bounds(Self.Left+22,YTop,11,21);
                  if Figures[Dummy].ImRaumschiff then
                  begin
                    ImageRect:=BeamDownRect;
                    Click:=BeamDown;
                  end
                  else
                  begin
                    ImageRect:=BeamUpRect;
                    Click:=BeamUp;
                  end;

                  ShortHint:=ST0309220055;
                  Data:=Integer(Figures[Dummy]);
                  Enabled:=Figures[Dummy].CanBeam;
                  TimeUnits:=TUBeam;
                end;
                CalculateShortHintRect(Button^);
                {$ENDIF}

                Button:=AddNewButton;
                with Button^ do
                begin
                  Rect:=Bounds(Self.Left+2,YTop,19,21);
                  ImageRect:=ConfigRect;
                  Click:=Einheitausrusten;
                  ShortHint:=ST0309220056;
                  Data:=Integer(Figures[Dummy]);
                  ImageAlign:=taCenter;
                end;
                CalculateShortHintRect(Button^);
              end;
              inc(YTop,22);
            end;
          end;
          inc(YTop,20);
        end;
      end;
      fOverID:=-1;
      fOverButton:=nil;
    end;
  end;
end;

{$IFNDEF DISABLEBEAM}
procedure TDXUIBodenEinsatz.BeamCancel(Sender: TObject);
begin
  Page:=uipSoldaten;
end;

procedure TDXUIBodenEinsatz.BeamDown(Sender: TObject);
begin
  ShowBeamMenu(TGameFigure(fOverButton.Data));
end;

procedure TDXUIBodenEinsatz.BeamUnitDown(Sender: TObject; X, Y: Integer; Button: TMouseButton);
begin
  if Button<>mbLeft then
    exit;

  if not CanFriendsMove then
    exit;

  if (fNextBeam<>0) and not (fBeamUnit.IsFirstBeam) then
  begin
    Rec.Text:=ST0309220058;
    SendVMessage(vmTextMessage);
    exit;
  end;
  Container.IncLock;
  if not fBeamUnit.BeamTo(X,Y) then
  begin
    Container.DecLock;
    exit;
  end;
  StartBeamTimer;
  Page:=uipSoldaten;
  Container.decLock;
  Container.DoFlip;
end;

procedure TDXUIBodenEinsatz.BeamUp(Sender: TObject);
var
  Figure: TGameFigure;
begin
  if (not CanFriendsMove) or (Self.Figure=nil) then
    exit;

  if fNextBeam<>0 then
  begin
    Rec.Text:=ST0309220058;
    SendVMessage(vmTextMessage);
    exit;
  end;

  // Sender=Self bedeutet, dass der Button gedr�ckt wurde. Ist dies nicht der Fall
  // so wurde �ber den Shortcut die Funktion aktiviert
  if Sender=Self then
  begin
    Assert(fOverButton<>nil);
    Figure:=TGameFigure(fOverButton.Data);
  end
  else
    Figure:=fFigure;

  Assert(Figure<>nil);

  Container.IncLock;
  if not Figure.BeamUp then
  begin
    Container.DecLock;
    exit;
  end;

  AktuButtons;
  StartBeamTimer;
  Container.DoMouseMessage;
  ISOEngine.Redraw;
  Redraw;
  Container.DecLock;
end;
{$ENDIF}

procedure TDXUIBodenEinsatz.CalculateShortHintRect(var Hint: TButtonInfo);
var
  Wid: Integer;
  Height: Integer;
begin
  with Hint do
  begin
    if (not Echtzeit) and (TimeUnits<>0) then
      Height:=40
    else
      Height:=20;
    if Rect.Bottom+Height>Bottom then
    begin
      HintRect.Top:=Rect.Top-Height;
      HintRect.Bottom:=Rect.Top-1;
    end
    else
    begin
      HintRect.Top:=Rect.Bottom+1;
      HintRect.Bottom:=Rect.Bottom+Height;
    end;
    ShowKey:=WordRec(Key).Lo<>0;
    LongHint:=ShortHint;
    if ShowKey then
      LongHint:=LongHint+' '#2'('+ShortCutToText(Key)+')';

    Wid:=WhiteStdFont.TextWidth(LongHint)+4;

    if Wid+Rect.Left>Right then
    begin
      HintRect.Right:=Rect.Right;
      HintRect.Left:=Rect.Right-Wid;
    end
    else
    begin
      HintRect.Left:=Rect.Left;
      HintRect.Right:=Rect.Left+Wid;
    end;
    if HintRect.Left<Left then
      OffsetRect(HintRect,Left-HintRect.Left+2,0);
  end;
end;

procedure TDXUIBodenEinsatz.ChangeFigure(Sender: TObject);
begin
  Redraw;
end;

procedure TDXUIBodenEinsatz.ChangeHotKeys(Registration: boolean);
var
  Dummy   : Integer;
  TmpPage : TUIPage;
begin
  if Registration then
  begin
    Parent.RegisterHotKey(KeyConfiguration.BodenKeys.Karte,nil,ChangeMiniMap);
    Parent.RegisterHotKey(KeyConfiguration.BodenKeys.Camera,nil,CameraClick);
    Parent.RegisterHotKey(KeyConfiguration.BodenKeys.Ausruesten,nil,Einheitausrusten);
    Parent.RegisterHotKey(KeyConfiguration.BodenKeys.TakeAll,nil,TakeItems);
    Parent.RegisterHotKey(KeyConfiguration.BodenKeys.Pause,nil,ChangePause);
    {$IFNDEF DISABLEBEAM}
    Parent.RegisterHotKey(KeyConfiguration.BodenKeys.BeamUp,nil,BeamUp);
    {$ENDIF}

    Parent.RegisterHotKey(KeyConfiguration.BodenKeys.ShowGrid,nil,ChangeTileGrid);

    Parent.RegisterHotKey(KeyConfiguration.BodenKeys.UseGrenade,nil,UseGreanadeFast);
    Parent.RegisterHotKey(KeyConfiguration.BodenKeys.UseSensor,nil,UseSensorFast);
    Parent.RegisterHotKey(KeyConfiguration.BodenKeys.UseMine,nil,UseMineFast);

    if not Echtzeit then
      Parent.RegisterHotKey(KeyConfiguration.BodenKeys.RundenEnde,nil,NextRoundClick);
  end
  else
  begin
    Parent.DeleteHotKey(KeyConfiguration.BodenKeys.Karte,nil,ChangeMiniMap);
    Parent.DeleteHotKey(KeyConfiguration.BodenKeys.Camera,nil,CameraClick);
    Parent.DeleteHotKey(KeyConfiguration.BodenKeys.Ausruesten,nil,Einheitausrusten);
    Parent.DeleteHotKey(KeyConfiguration.BodenKeys.TakeAll,nil,TakeItems);
    Parent.DeleteHotKey(KeyConfiguration.BodenKeys.Pause,nil,ChangePause);
    {$IFNDEF DISABLEBEAM}
    Parent.DeleteHotKey(KeyConfiguration.BodenKeys.BeamUp,nil,BeamUp);
    {$ENDIF}

    Parent.DeleteHotKey(KeyConfiguration.BodenKeys.ShowGrid,nil,ChangeTileGrid);

    Parent.DeleteHotKey(KeyConfiguration.BodenKeys.UseGrenade,nil,UseGreanadeFast);
    Parent.DeleteHotKey(KeyConfiguration.BodenKeys.UseSensor,nil,UseSensorFast);
    Parent.DeleteHotKey(KeyConfiguration.BodenKeys.UseMine,nil,UseMineFast);

    if not fEchtzeit then
      Parent.DeleteHotKey(KeyConfiguration.BodenKeys.RundenEnde,nil,NextRoundClick);
  end;
  Assert(high(fButtons)>=5);
  fButtons[0].Key:=ShortCut(Word(KeyConfiguration.BodenKeys.Karte),[]);
  fButtons[5].Key:=ShortCut(Word(KeyConfiguration.BodenKeys.Camera),[]);
  TmpPage:=fUIPage;
  fUIPage:=uipNone;
  Page:=TmpPage;
  for Dummy:=0 to High(fButtons) do
    CalculateShortHintRect(fButtons[Dummy]);
end;

procedure TDXUIBodenEinsatz.ChangeMiniMap(Sender: TObject);
begin
  if not CanFriendsMove then
    exit;

  Assert(high(fButtons)>=0);

  if fButtons[0].Data=0 then
    fMiniMap.Show
  else
    fMiniMap.Hide;
end;

procedure TDXUIBodenEinsatz.ChangePause(Sender: TObject);
begin
  if fISOEngine.UserSperre>0 then
  begin
    Rec.Bool:=true;
    SendVMessage(vmGoOnAfterMessage);
    exit;
  end;

  if not fEchtzeit then
    exit;

  Assert(high(fButtons)>=1);

  if fButtons[1].Data=0 then
    fISOEngine.Pause:=true
  else
    fISOEngine.Pause:=false;
end;

constructor TDXUIBodenEinsatz.Create(Page: TDXPage);
var
  SchussType  : TSchussType;
begin
  inherited;
  fChangeSpeedButton:=-1;
  fDefaultButtons:=5;
  fTakeAllButton:=-1;
  InitButtons;
  fOverID:=-1;
  fOverButton:=nil;

  {$IFNDEF DISABLEFORMATION}
  fFormation:=nil;
  fFormations:=TList.Create;

  for Dummy:=1 to 9 do
  begin
    Parent.RegisterSystemKey(Char(Dummy+Ord('0')),SystemClickFormation,Dummy+1);
  end;
  {$ENDIF}

  // Schussarten f�llen

  // Nicht Schiessen
  SchussArten[stNichtSchiessen].SymbolRect:=NoShootRect;
  SchussArten[stNichtSchiessen].Hint:=ST0309220084;

  // Gezielt
  SchussArten[stGezielt].SymbolRect:=GezieltRect;
  SchussArten[stGezielt].Hint:=ST0309220085;

  // Spontan
  SchussArten[stSpontan].SymbolRect:=SpontanRect;
  SchussArten[stSpontan].Hint:=ST0309220086;

  // Automatisch
  SchussArten[stAuto].SymbolRect:=AutoRect;
  SchussArten[stAuto].Hint:=ST0309220087;

  // Schlagen
  SchussArten[stSchlag].SymbolRect:=SchlagRect;
  SchussArten[stSchlag].Hint:=ST0402140001;

  // Stossen
  SchussArten[stStossen].SymbolRect:=SchlagRect;
  SchussArten[stStossen].Hint:=CR0504280001;

  // Schwingen
  SchussArten[stSchwingen].SymbolRect:=SchlagRect;
  SchussArten[stSchwingen].Hint:=CR0504280002;

  // Benutzen
  SchussArten[stBenutzen].SymbolRect:=UseRect;
  SchussArten[stBenutzen].Hint:=ST0309220088;
  SchussArten[stBenutzen].TimeUnits:=TUUseItem+TUTakeHand;

  // Werfen
  SchussArten[stWerfen].SymbolRect:=ThrowRect;
  SchussArten[stWerfen].Hint:=ST0412270001;
  SchussArten[stWerfen].TimeUnits:=TUThrowItem+TUTakeHand;

  // Pr�fen, ob alle Schussarten festgelegt sind und die Zeiteinheiten setzen
  for SchussType:=low(TSchussType) to high(TSchussType) do
  begin
    Assert(SchussArten[SchussType].Hint<>'','Vermutlich keine Button-Informationen f�r '+GetEnumName(TypeInfo(TSchussType),ord(SchussType)));
    SchussArten[SchussType].TUMulti:=TimeUnitsMultiplikator[SchussType];
  end;

  fSpinEdit:=TDXSpinEdit.Create(Page);
  with fSpinEdit do
  begin
    TabStop:=false;
    Visible:=false;
    Min:=1;
    Value:=4;
    Font.Color:=clWhite;
    Interval:=200;
    BorderColor:=coFirstColor;
    RoundCorners:=rcNone;
    FocusColor:=coSecondColor;
  end;

  // Zuweisen des Surfaces zum leichten Zugriff auf die UI-Bilder
  Container.AddRestoreFunction(Restore);
  Restore;
end;

destructor TDXUIBodenEinsatz.destroy;
begin
  fSpinEdit.Free;

  {$IFNDEF DISABLEFORMATION}
  fFormations.Free;
  fFormations:=nil;
  {$ENDIF}
  
  inherited;
end;

procedure TDXUIBodenEinsatz.Draw(Surface: TDirectDrawSurface;var Mem: TDDSurfaceDesc);
begin
  RedrawRect(ClientRect,Surface,Mem);
end;

procedure TDXUIBodenEinsatz.Einheitausrusten(Sender: TObject);
var
  OldPause  : boolean;
  ManagList : TList;
  Dummy     : Integer;
begin
  if not CanFriendsMove then
    exit;

  Container.DeleteFrameFunction(ISOEngine.PerformFrame,ISOEngine);
  OldPause:=ISOEngine.Pause;
  ISOEngine.Pause:=true;
  ManagList:=TList.Create;
  for Dummy:=0 to ISOEngine.Figures.Count-1 do
  begin
    ManagList.Add(ISOEngine.Figures[Dummy].Manager);
  end;
  Managers:=ManagList;

  // Sender=Self bedeutet, dass der Button gedr�ckt wurde. Ist dies nicht der Fall
  // so wurde �ber den Shortcut die Funktion aktiviert
  if Sender=Self then
  begin
    Assert(fOverButton<>nil);
    ActiveManager:=TGameFigure(fOverButton.Data).Manager;
  end
  else
  begin
    if fFigure<>nil then
      ActiveManager:=TGameFigureManager(fFigure.Manager)
    else
      ActiveManager:=TGameFigureManager(ManagList[0]);
  end;

  VerSoldatEinsatz.UseTimeUnits:=not Echtzeit;
  ISOComun:=fISOEngine.SoldatConfigComun;
  Parent.ChangePage(PageSoldatConfig);
  ISOComun:=nil;

  SendVMessage(vmReleaseEscButton);

  ManagList.Free;
  ISOEngine.Pause:=OldPause;
  Container.AddFrameFunction(ISOEngine.PerformFrame,ISOEngine,25);
end;

function TDXUIBodenEinsatz.GetImageRect(Image: Integer): TRect;
begin
  result.Top:=0;
  result.Bottom:=20;
  result.Left:=Image*38;
  result.Right:=(Image+1)*38;
end;

procedure TDXUIBodenEinsatz.HideMiniMap(Sender: TObject);
begin
  Assert(high(fButtons)>=0);
  fButtons[0].Data:=0;
  fButtons[0].ImageRect:=GetImageRect(0);
  Redraw;
end;

procedure TDXUIBodenEinsatz.InitButtons;
begin
  with AddNewButton^ do
  begin
    ImageRect:=GetImageRect(0);
    Click:=ChangeMiniMap;
    ShortHint:=ST0309220059;
    Key:=ShortCut(Word('M'),[]);
  end;
  with AddNewButton^ do
  begin
    ImageRect:=GetImageRect(2);
  end;
  with AddNewButton^ do
  begin
    ImageRect:=Bounds(0,104,32,32);
    Click:=ShowUnits;
    ShortHint:=ST0309220060;
  end;
  with AddNewButton^ do
  begin
    Click:=ShowUnitSettings;
    ShortHint:=ST0309220061;
  end;

  with AddNewButton^ do
  begin
    ImageRect:=CancelRect;
    ShortHint:=ST0309220062;
    Key:=ShortCut(27,[]);
    {$IFNDEF DISABLEBEAM}
    Click:=BeamCancel;
    {$ENDIF}
    ImageAlign:=taCenter;
    DefaultPage:=uipNone;
    Visible:=false;
  end;

  with AddNewButton^ do
  begin
    ImageRect:=GetImageRect(4);
    ShortHint:=ST0309220063;
    Click:=CameraClick;
    ImageAlign:=taCenter;
    DefaultPage:=uipSettings;
    Visible:=false;
  end;
  fDefaultButtons:=length(fButtons);
end;

procedure TDXUIBodenEinsatz.MouseLeave;
begin
  if fOverID<>-1 then
  begin
    Assert(high(fButtons)>=fOverID);
    fButtons[fOverID].Status:=bsNormal;
    fOverButton:=nil;
    fOverID:=-1;
    Redraw;
  end;
end;

procedure TDXUIBodenEinsatz.MouseMove(X, Y: Integer);
var
  Dummy   : Integer;
  MousePt : TPoint;
begin
  MousePt:=Point(X+Left,Y+Top);
  for Dummy:=0 to High(fButtons) do
  begin
    with fButtons[Dummy] do
    begin
      if Visible and Enabled and PtInRect(Rect,MousePt) then
      begin
        if fOverID<>ID then
        begin
          if fOverButton<>nil then
            fOverButton.Status:=bsNormal;
          fOverID:=ID;
          Status:=bsOver;
          fOverButton:=Addr(fButtons[fOverID]);
          Redraw;
        end;
        exit;
      end;
    end;
  end;
  if fOverID<>-1 then
  begin
    fOverButton.Status:=bsNormal;
    fOverID:=-1;
    fOverButton:=nil;
    Redraw;
  end;
end;

procedure TDXUIBodenEinsatz.MouseUp(Button: TMouseButton; X, Y: Integer);
begin
  if (fOverID=-1) or (Button<>mbLeft) then exit;

  SendVMessage(vmGetUserInterfaceSperre);
  if (Rec.Int=0) and (Assigned(fOverButton.Click)) then
    fOverButton.Click(Self);
end;

procedure TDXUIBodenEinsatz.NextRoundClick(Sender: TObject);
begin
  if fMiniMap.Visible then
    exit;

  if CanFriendsMove and (fISOEngine.UserSperre=0) then
  begin
    rec.ZugSeite:=zsFriend;
    SendVMessage(vmRoundEnd);
  end;
end;

function TDXUIBodenEinsatz.NotifyMessage(MessageType: TNotifyUIType; Figure: TGameFigure): boolean;
var
  Dummy: Integer;
begin
  result:=false;
  case MessageType of
    {$IFNDEF DISABLEBEAM}nuitBeamReady,{$ENDIF} nuitUnitDeath :
    begin
      if Page=uipSoldaten then
      begin
        Container.Lock;
        AktuButtons;
        Container.Unlock;
        Redraw;
        result:=fMerkeRedraw;
      end;
    end;
    nuitCheckMerkeRedraw:
    begin
      result:=fMerkeRedraw;
      if fMerkeRedraw then
        Redraw;
    end;
    nuitSelectionChange:
    begin
      // Ausgew�hlte Einheit suchen und setzen
      if not fEchtzeit then
      begin
        for Dummy:=0 to ISOEngine.Figures.Count-1 do
        begin
          if ISOEngine.Figures[Dummy].Selected then
          begin
            Self.Figure:=ISOEngine.Figures[Dummy];
            break;
          end;
        end;
      end;

      if (Echtzeit) and ((fFigure=nil) or (not fFigure.Selected)) then
        SetNextFigure(true);

      if fUIPage=uipSettings then
      begin
        fUIPage:=uipNone;
        Page:=uipSettings;
      end;
      if Page=uipSoldaten then
      begin
        if Container.IsLoadGame then
          fMerkeRedraw:=true
        else
          Redraw;
        result:=fMerkeRedraw;
      end;
    end;
    nuitPauseChange:
    begin
      if not fEchtzeit then
        exit;

      Assert(high(fButtons)>=1);

      if rec.Bool then
      begin
        fButtons[1].ImageRect:=GetImageRect(2);
        fButtons[1].Data:=1;
        Redraw;
      end
      else
      begin
        fButtons[1].ImageRect:=GetImageRect(3);
        fButtons[1].Data:=0;
        Redraw;
      end;
    end;
    nuitMunitionChange,nuitTimeUnitChange,nuitHitPointsChange:
    begin
      if (fUIPage=uipSettings) and (fFigure=Figure) then
        Redraw;
    end;
    nuitNextRound:
    begin
      // Pr�fen, ob Friend am Zug ist
      SendVMessage(vmCanFriendMove);
      Enabled:=Rec.Result;

      {$IFNDEF DISABLEBEAM}
      fNextBeam:=0;
      {$ENDIF}
      Redraw;
    end;
  end;
end;

procedure TDXUIBodenEinsatz.Redraw;
begin
  if Parent<>Container.ActivePage then
    exit;
    
  if Container.IsLoadGame then
  begin
    fMerkeRedraw:=true;
    exit;
  end;
  fMerkeRedraw:=false;
  Container.Inclock;
  Container.RedrawBlackControl(Self,Container.Surface);
  if fSpinEdit.Visible then
    Container.RedrawBlackControl(fSpinEdit,Container.Surface);
  Container.Declock;
end;

procedure TDXUIBodenEinsatz.DrawCaption(Surface: TDirectDrawSurface; const Mem: TDDSurfaceDesc; Text: String;Top: Integer; OffSet : Integer);
begin
  //BlendRectangle(Rect(Left+1,Top+1,Right-1,Top+17),100,bcMaroon,Surface,Mem);
  Surface.FillRect(Rect(Left+1,Top+1,Right-1,Top+18),draw_utils_GetAlphaColor(Mem,bcMaroon,100));
  HLine(Surface,Mem,Left+1,Right-1,Top,bcMaroon);
  HLine(Surface,Mem,Left+1,Right-1,Top+18,bcMaroon);
  YellowStdFont.Draw(Surface,Left+OffSet,Top+3,Text);
end;

procedure TDXUIBodenEinsatz.RedrawRect(DrawRect: TRect;
  Surface: TDirectDrawSurface; var Mem: TDDSurfaceDesc);
var
  Dummy       : Integer;
  InSchiff    : boolean;
  YTop        : Integer;
  Text        : String;
  Sperre      : Integer;

  procedure DrawHint(var Button: TButtonInfo);
  begin
    with Button do
    begin
      if LongHint=EmptyStr then exit;
      BlendRectangle(ResizeRect(HintRect,1),200,bcDisabled,Surface,Mem);
      Rectangle(Surface,Mem,HintRect,bcDisabled);
      FontEngine.DynamicText(Surface,HintRect.Left+2,HintRect.Top+2,LongHint,[WhiteStdFont,YellowStdFont]);
      if (not Echtzeit) and (TimeUnits<>0) then
      begin
        Surface.Draw(HintRect.Left+2,HintRect.Top+20,TimeUnitsRect,fUIImages);
        YellowStdFont.Draw(Surface,HintRect.Left+22,HintRect.Top+22,IntToStr(TimeUnits));
      end;
//      WhiteStdFont.Draw(Surface,HintRect.Left+2,HintRect.Top+2,ShortHint);

//      if ShowKey then
  //      YellowStdFont.Draw(Surface,HintRect.Left+2+YellowStdFont.TextWidth(ShortHint+' '),HintRect.Top+2,'('+ShortCutToText(Key)+')');
    end;
  end;

  procedure DrawFigure(Figure: TGameFigure; Top: Integer);
  const
    LeftMargin = {$IFDEF DISABLEBEAM}22{$ELSE}34{$ENDIF};
  var
    p  : Integer;
  begin
    if Figure.IsDead then
      Surface.FillRect(Rect(Left+LeftMargin,Top,Right-2,Top+21),draw_utils_GetAlphaColor(Mem,bcGray,100))
      //BlendRectangle(Rect(Left+LeftMargin,Top,Right-2,Top+20),100,bcGray,Surface,Mem)
    else
    begin
      p:=max(0,(Figure.Manager.Gesundheit*((Right-2)-(Left+LeftMargin))) div Figure.Manager.MaxGesundheit);
      //BlendRectangle(Classes.Rect((Left+LeftMargin),Top,(Left+LeftMargin)+p,Top+20),150,bcMaroon,Surface,Mem);
      Surface.FillRect(Classes.Rect((Left+LeftMargin),Top,(Left+LeftMargin)+p,Top+21),draw_utils_GetAlphaColor(Mem,bcMaroon,150));
      //BlendRectangle(Classes.Rect((Left+LeftMargin)+p,Top,Right-2,Top+20),150,bcDisabled,Surface,Mem);
      Surface.FillRect(Classes.Rect((Left+LeftMargin)+p,Top,Right-2,Top+21),draw_utils_GetAlphaColor(Mem,bcDisabled,150));
    end;
    if Figure.Selected then
    begin
      YellowStdFont.Draw(Surface,Left+LeftMargin+2,Top+4,Figure.Name);
      Rectangle(Surface,Mem,Rect(Left+LeftMargin,Top,Right-2,Top+21),bcGray);
    end
    else
      WhiteStdFont.Draw(Surface,Left+LeftMargin,Top+4,Figure.Name);
  end;

  {$IFNDEF DISABLEBEAM}
  procedure DrawBeamTime(Top: Integer);
  var
    Text: String;
    Wid : Integer;
  begin
//    RedStdFont.Draw(Surface,Left+2,Top,'Teleporter nicht bereit');
    Rectangle(Surface,Mem,Left+2,Top,Right-2,Top+20,bcMaroon);
    Wid:=((Width-8)*fNextBeam) div BeamTime;
    //BlendRectangle(Rect(Left+4,Top+2,Left+4+Wid,Top+17),150,bcMaroon,Surface,Mem);
    Surface.FillRect(Rect(Left+4,Top+2,Left+4+Wid,Top+18),draw_utils_GetAlphaColor(Mem,bcMaroon,150));
    if Echtzeit then
      Text:=Format(ST0309220064,[(fNextBeam div 10)+1])
    else
      Text:=ST0309220065;
    RedStdFont.Draw(Surface,Left+(Width div 2)-(RedStdFont.TextWidth(Text) div 2),Top+3,Text);
  end;
  {$ENDIF}

begin
  fMerkeRedraw:=false;
  Container.IncLock;
  IntersectRect(DrawRect,DrawRect,ClientRect);
  Surface.FillRect(DrawRect,bcBlack);
  Surface.Lock(Mem);
  Surface.Unlock;
  Rectangle(Surface,Mem,ClientRect,bcMaroon);

  case fUIPage of
    uipSoldaten :
    begin
      with ISOEngine do
      begin
        YTop:=63;
        Text:=ST0309220028;
        for InSchiff:=true downto false do
        begin
          DrawCaption(Surface,Mem,Text,YTop);
          inc(YTop,20);
          for Dummy:=0 to Figures.Count-1 do
          begin
            if (Figures[Dummy].ImRaumschiff=InSchiff) and (Figures[Dummy].FigureStatus=fsFriendly) then
            begin
              DrawFigure(Figures[Dummy],YTop);
              inc(YTop,22);
            end;
          end;
          Text:=ST0309220027;
        end;
      end;
      {$IFNDEF DISABLEBEAM}
      if (fNextBeam<>0) then
        DrawBeamTime(Top+42)
      else
        WhiteStdFont.Draw(Surface,Left+2,Top+45,ST0309220066);
      {$ENDIF}
    end;
    {$IFNDEF DISABLEBEAM}
    uipGroupBeaming:
    begin
      DrawCaption(Surface,Mem,ST0309220067,Top+42);
      YTop:=Top+(((fFormations.Count-1) div 4)*37)+100;
      DrawCaption(Surface,Mem,ST0309220068,YTop);
      WhiteStdFont.Draw(Surface,Left+2,YTop+25,ST0309220069);
      WhiteStdFont.Draw(Surface,Left+2,YTop+43,ST0309220070);
      WhiteBStdFont.Draw(Surface,Left+52,YTop+65,ST0309220071);
      WhiteStdFont.Draw(Surface,Left+2,YTop+87,ST0309220072);
      WhiteBStdFont.Draw(Surface,Left+2,YTop+107,ST0309220073);
    end;
    uipBeaming:
    begin
      DrawCaption(Surface,Mem,ST0309220074,Top+42);
      WhiteStdFont.Draw(Surface,Left+2,Top+80,fBeamUnit.Name);
      if (not fBeamUnit.IsFirstBeam) and (fNextBeam<>0) then
        DrawBeamTime(Top+100)
      else
        WhiteBStdFont.Draw(Surface,Left+2,Top+100,ST0309220075);
    end;
    {$ENDIF}
    uipSettings:
    begin
      {$IFNDEF DISABLEFORMATION}
      DrawCaption(Surface,Mem,ST0309220076,Top+42);
      if not fAllFormations then
      begin
      {$ENDIF}
        if (fFigure<>nil) and (fFigure.Manager<>nil) then
        begin
          DrawCaption(Surface,Mem,fFigure.Name,Top+UnitFacePos.y,24);

          fFigure.Manager.DrawGesicht(Surface,Mem,Left+UnitFacePos.x,Top+UnitFacePos.y+19,false);

          DrawStatus(Surface,Mem,HealthRect,Format('%d / %d',[fFigure.Manager.Gesundheit,fFigure.Manager.MaxGesundheit]),Top+UnitFacePos.y+20);
          if not fEchtzeit then
            DrawStatus(Surface,Mem,TimeUnitsRect,Format('%d / %d',[fFigure.Zeiteinheiten,Figure.Manager.MaxZeiteinheiten]),Top+UnitFacePos.y+42);
          YTop:=Top+WaffenMenueTop;
          DrawWaffe(Surface,Mem,YTop,fFigure.Manager.LeftHandWaffe);
          DrawWaffe(Surface,Mem,YTop,fFigure.Manager.RightHandWaffe);
        end;
      {$IFNDEF DISABLEFORMATION}
      end;
      {$ENDIF}
    end;
  end;

  { Buttons und Hint zeichnen }
  SendVMessage(vmGetUserInterfaceSperre);
  Sperre:=Rec.Int;
  for Dummy:=0 to High(fButtons) do
  begin
    with fButtons[Dummy] do
    begin
      if Visible and Draw then
      begin
        if Enabled and (Self.Enabled) and (Sperre=0) then
        begin
          if Highlight then
          begin
            Rectangle(Surface,Mem,Rect,bcRed);
            if Status=bsNormal then
              //BlendRectangle(ResizeRect(Rect,1),100,bcRed,Surface,Mem)
              Surface.FillRect(ResizeRect(Rect,1),draw_utils_GetAlphaColor(Mem,bcRed,100))
            else
              //BlendRectangle(ResizeRect(Rect,1),150,bcRed,Surface,Mem);
              Surface.FillRect(ResizeRect(Rect,1),draw_utils_GetAlphaColor(Mem,bcRed,150));
          end
          else
          begin
            Rectangle(Surface,Mem,Rect,bcMaroon);
            if Status=bsNormal then
              //BlendRectangle(ResizeRect(Rect,1),100,bcMaroon,Surface,Mem)
              Surface.FillRect(ResizeRect(Rect,1),draw_utils_GetAlphaColor(Mem,bcMaroon,100))
            else
              //BlendRectangle(ResizeRect(Rect,1),150,bcMaroon,Surface,Mem);
              Surface.FillRect(ResizeRect(Rect,1),draw_utils_GetAlphaColor(Mem,bcMaroon,150));
          end;
        end
        else
        begin
          Rectangle(Surface,Mem,Rect,bcDisabled);
          //BlendRectangle(ResizeRect(Rect,1),100,bcDisabled,Surface,Mem)
          Surface.FillRect(ResizeRect(Rect,1),draw_utils_GetAlphaColor(Mem,bcDisabled,100));
        end;
        if not Assigned(DrawFunc) then
        begin
          case ImageAlign of
            taLeftJustify  : Surface.Draw(Rect.Left+ImageOffset,Rect.Top+ImageOffset,ImageRect,fUIImages);
            taRightJustify : Surface.Draw(Rect.Right-ImageOffset-(ImageRect.Right-ImageRect.Left),Rect.Top+ImageOffset,fUIImages);
            taCenter       : Surface.Draw(Rect.Left+((Rect.Right-Rect.Left) div 2)-((ImageRect.Right-ImageRect.Left) div 2),Rect.Top+ImageOffset,ImageRect,fUIImages);
          end;
        end
        else
          DrawFunc(Surface,Rect.Left+2,Rect.Top+2);
      end;
    end;
  end;

  HLine(Surface,Mem,Left+1,Right-1,Bottom-28,bcMaroon);
  if fOverId<>-1 then
  begin
    Assert(fOverButton<>nil,'Overbutton zeigt auf nil');
    DrawHint(fOverButton^);
  end;
  Container.DecLock;
end;

procedure TDXUIBodenEinsatz.Resize(var NewLeft, NewTop, NewWidth,
  NewHeight: Integer);
var
  Dummy: Integer;
begin
  Assert(high(fButtons)>=5);
  fButtons[0].Rect:=Bounds(NewLeft+2,NewTop+NewHeight-26,42,24);
  fButtons[1].Rect:=Bounds(NewLeft+NewWidth-44,NewTop+NewHeight-26,42,24);
  fButtons[2].Rect:=Bounds(NewLeft+2,NewTop+2,36,36);
  fButtons[3].Rect:=Bounds(NewLeft+40,NewTop+2,36,36);
  fButtons[4].Rect:=Bounds(Right-44,Bottom-54,42,25);
  fButtons[5].Rect:=Bounds(NewLeft+45,NewTop+NewHeight-26,30,24);
  for Dummy:=0 to high(fButtons) do
  begin
    CalculateShortHintRect(fButtons[Dummy]);
  end;
end;

{$IFNDEF DISABLEBEAM}
function TDXUIBodenEinsatz.SekTimer(Sender: TObject;
  Frames: Integer): boolean;
begin
  result:=false;
  if Parent<>Container.ActivePage then exit;

  SendVMessage(vmGetPause);
  if rec.Result then exit;

  dec(fNextBeam);
  if fNextBeam<=0 then
  begin
    fNextBeam:=0;
    Container.DeleteFrameFunction(SekTimer,Self);
  end;
  if Page in [uipBeaming,uipSoldaten] then
  begin
    Redraw;
    result:=true;
  end;
end;
{$ENDIF}

procedure TDXUIBodenEinsatz.SetEchtzeit(const Value: boolean);
var
  Button: PButtonInfo;
begin

  Assert(high(fButtons)>=1);
  
  fEchtzeit := Value;
  fButtons[0].Data:=0;
  if Value then
  begin
    fButtons[1].ImageRect:=GetImageRect(3);
    fButtons[1].Click:=ChangePause;
    fButtons[1].ShortHint:=ST0309220077;
    fButtons[1].Key:=ShortCut(Word(KeyConfiguration.BodenKeys.Pause),[]);
    fButtons[1].Data:=0;
  end
  else
  begin
    fButtons[1].ImageRect:=GetImageRect(5);
    fButtons[1].Click:=NextRoundClick;
    fButtons[1].ShortHint:=ST0309220078;
    fButtons[1].Key:=ShortCut(Word(KeyConfiguration.BodenKeys.RundenEnde),[]);
  end;
  CalculateShortHintRect(fButtons[1]);
end;

procedure TDXUIBodenEinsatz.SetFigure(const Value: TGameFigure);
begin
  if fFigure=Value then
    exit;

  if fFigure<>nil then
    fFigure.NotifyList.RemoveEvent(fFigureDestroy);

  fFigure := Value;

  if fFigure<>nil then
    fFigureDestroy:=fFigure.NotifyList.RegisterEvent(EVENT_FIGURE_ONDESTROY,FigureDestroy);

  // Aktualisieren der Waffenbuttons
  Assert(high(fButtons)>=5);

  fButtons[5].Enabled:=Value<>nil;
  if fUIPage=uipSettings then
  begin
    fUIPage:=uipNone;
    Page:=uipSettings;
  end;

  // Kameraverfolgung
  if fButtons[5].Highlight then
  begin
    Rec.Figure:=fFigure;
    SendVMessage(vmKameraVerfolgung);
  end;
  SendVMessage(vmRedrawSolList);
end;

procedure TDXUIBodenEinsatz.SetMiniMap(const Value: TDXISOMiniMap);
begin
  fMiniMap := Value;
  fMiniMap.OnShow:=ShowMiniMap;
  fMiniMap.OnHide:=HideMiniMap;
end;

procedure TDXUIBodenEinsatz.SetUIPage(const Value: TUIPage);
begin
  if fUIPage=Value then exit;
  fUIPage := Value;

  Assert(high(fButtons)>=3);

  if fUIPage in [uipSoldaten,uipSettings] then
  begin
    fButtons[2].Highlight:=fUIPage=uipSoldaten;
    fButtons[3].Highlight:=fUIPage=uipSettings;
  end;
  AktuButtons;
  Container.Lock;
  Container.DoMouseMessage;
  Container.UnLock;
  Redraw;
end;

procedure TDXUIBodenEinsatz.SetUISol(const Value: TDXUISoldatenList);
begin
  fUISol := Value;
  fUISol.UserInterface:=Self;
end;

procedure TDXUIBodenEinsatz.ShowUnits(Sender: TObject);
begin
  if not CanFriendsMove then
    exit;
  Page:=uipSoldaten;
end;

procedure TDXUIBodenEinsatz.ShowMiniMap(Sender: TObject);
begin
  Assert(high(fButtons)>=0);

  fButtons[0].Data:=1;
  fButtons[0].ImageRect:=GetImageRect(1);
  Redraw;
end;

procedure TDXUIBodenEinsatz.ShowUnitSettings(Sender: TObject);
begin
  if not CanFriendsMove then
    exit;
  Page:=uipSettings;
end;

{$IFNDEF DISABLEBEAM}
procedure TDXUIBodenEinsatz.StartBeamTimer;
begin
(*  fNextBeam:=BeamTime;
  if Echtzeit then
  begin
    Container.DeleteFrameFunction(SekTimer,Self);
    Container.AddFrameFunction(SekTimer,Self,100);
  end;*)
end;
{$ENDIF}

{$IFNDEF DISABLEFORMATION}
procedure TDXUIBodenEinsatz.RegisterFormation(Formation: TGroupFormation);
const // typisierte Konstanten k�nnen in Delphi wie statische Variablen verwendet werden
  FormationCount: Integer = 0;
var
  Button: PButtonInfo;
begin
  inc(FormationCount);
  if FormationCount=9 then
  begin
    inc(fDefaultButtons);
    Button:=AddNewButton;
    with Button^ do
    begin
      DefaultPage:=uipSettings;
      ImageRect:=Classes.Rect(144,20,155,35);
      ShortHint:=ST0309220079;
      Rect:=Bounds(Right-19,Top+42,19,19);
      ShowKey:=false;
      Click:=ClickAllFormations;
      Data:=0;
      ImageAlign:=taCenter;
    end;
    CalculateShortHintRect(Button^);
  end;
  Assert(fFormations<>nil);
  fFormations.Add(Formation);
  if FormationCount=1 then
  begin
    fFormation:=Formation;
  end;
end;

procedure TDXUIBodenEinsatz.ClickFormation(Sender: TObject);
begin
  if fOverId<0 then exit;
  if fFormationButton<>-1 then
  begin
    Assert(high(fButtons)>=fFormationButton);
    fButtons[fFormationButton].Highlight:=false;
  end;

  if fAllFormations and (fFormations.IndexOf(Pointer(fOverButton.Data))>7) then
  begin
    fFormation:=TGroupFormation(fOverButton.Data);
    fFormations.Remove(Pointer(fOverButton.Data));
    fFormations.Insert(0,TGroupFormation(fOverButton.Data));
    AktuButtons;
  end
  else
  begin
    fOverButton.Highlight:=true;
    fFormation:=TGroupFormation(fOverButton.Data);
    fFormationButton:=fOverID;
  end;
  Redraw;
end;

procedure TDXUIBodenEinsatz.ClickAllFormations(Sender: TObject);
begin
  with fOverButton^ do
  begin
    if Data=0 then
    begin
      ImageRect:=Classes.Rect(156,20,167,35);
      Data:=1;
      ShortHint:=ST0309220080;
      fAllFormations:=true;
    end
    else
    begin
      ImageRect:=Classes.Rect(144,20,155,35);
      Data:=0;
      ShortHint:=ST0309220079;
      fAllFormations:=false;
    end;
  end;
  CalculateShortHintRect(fOverButton^);
  fUIPage:=uipNone;
  Page:=uipSettings;
end;

procedure TDXUIBodenEinsatz.AktuFormationsButtons;
var
  Dummy     : Integer;
  Count     : Integer;
  Button    : PButtonInfo;
  Formation : TGroupFormation;
begin
  if fAllFormations then
    Count:=fFormations.Count
  else
    Count:=8;
  fFormationButton:=-1;
  for Dummy:=0 to Count-1 do
  begin
    Formation:=TGroupFormation(fFormations[Dummy]);
    Button:=AddNewButton;
    with Button^ do
    begin
      DrawFunc:=Formation.DrawSymbol;
      ShortHint:=Formation.FormationName;
      Rect:=Bounds(Left+2+((Dummy mod 4)*37),Top+63+((Dummy div 4)*37),35,35);
      ShowKey:=false;
      Data:=Integer(Formation);
      Click:=ClickFormation;
      if Formation=fFormation then
      begin
        fFormationButton:=Button.ID;
        Highlight:=true;
      end
      else
        Highlight:=false;
      if Dummy<8 then
        Key:=ShortCut(Dummy+Ord('1'),[ssAlt]);
    end;
    CalculateShortHintRect(Button^);
  end;
end;

procedure TDXUIBodenEinsatz.SystemClickFormation(Data: Integer);
var
  temp: Boolean;
begin
  fFormation:=TGroupFormation(fFormations[Data-1]);
  if Page=uipSettings then
  begin
    AktuFormationsButtons;
    Redraw;
  end
  {$IFNDEF DISABLEBEAM}
  else if Page=uipGroupBeaming then
  begin
    temp:=fAllFormations;
    fAllFormations:=true;
    AktuFormationsButtons;
    fAllFormations:=Temp;
    Redraw;
  end;
  {$ENDIF}
end;
{$ENDIF}

procedure TDXUIBodenEinsatz.EscClick(Sender: TObject);
begin
  if fISOEngine.UserSperre>0 then exit;

  SendVMessage(vmReleaseEscButton);

  {$IFNDEF DISABLEBEAM}
  BeamCancel(Self);
  {$ENDIF}
end;

procedure TDXUIBodenEinsatz.SetStartState;
begin
  SetFigure(nil);

  {$IFNDEF DISABLEBEAM}
  fNextBeam:=0;
  if AutoPlace then
    SetUIPage(uipSoldaten)
  else
    SetUIPage(uipGroupBeaming);
  {$ELSE}
    SetUIPage(uipSettings);
  {$ENDIF}

  {$IFNDEF DISABLEFORMATION}
  fAllFormations:=false;
  {$ENDIF}

  Assert(high(fButtons)>=5);
  fButtons[5].Enabled:=false;
  fButtons[5].HighLight:=false;
end;

{$IFNDEF DISABLEBEAM}
procedure TDXUIBodenEinsatz.BeamGroupDown(Sender: TObject; X, Y: Integer; Button: TMouseButton);
var
  Dummy       : Integer;
  Count       : Integer;
  AllBeamDown : Boolean;
  Group       : Integer;
begin
  if Button<>mbLeft then
    exit;

  if not CanFriendsMove then
    exit;
  // N�chste Freie Gruppe ermitteln (angefangen bei 1)
  Group:=1;
  while (Group<10) and (ISOEngine.Groups[Group].UnitCount>0) do
    inc(Group);
  Rec.Point:=Point(X,Y);
  SendVMessage(vmCanBeamTo);
  if not Rec.Result then
    exit;
  Formation.SetzePosition(Point(X-1,Y-1),Point(X,Y),TSize(Point(fISOEngine.ISOMap.Width,fISOEngine.ISOMap.Height)));
  Count:=0;
  for Dummy:=0 to ISOEngine.Figures.Count-1 do
  begin
    if (ISOEngine.Figures[Dummy].FigureStatus=fsFriendly) and ISOEngine.Figures[Dummy].ImRaumschiff then
    begin
      repeat
        Rec.Point:=Formation.GetNextZiel;
        if Rec.Point.X=-1 then
          break;
        SendVMessage(vmCanBeamTo);
      until Rec.Result;
      if Rec.Point.X<>-1 then
      begin
        ISOEngine.Figures[Dummy].BeamTo(Rec.Point.X,Rec.Point.Y);
        if Group<10 then
          ISOEngine.Groups[Group].AddUnit(ISOEngine.Figures[Dummy]);
        inc(Count);
        if Count=fSpinEdit.Value then
          break;
      end;
    end;
  end;
  StartBeamTimer;
  // Wenn alle EInheiten runtergebeamt wurden, dann Gruppentransport beenden
  AllBeamDown:=true;
  for Dummy:=0 to ISOEngine.Figures.Count-1 do
  begin
    if (ISOEngine.Figures[Dummy].FigureStatus=fsFriendly) and ISOEngine.Figures[Dummy].ImRaumschiff then
    begin
      AllBeamDown:=false;
      break;
    end;
  end;

  if AllBeamDown then
    Page:=uipSoldaten;
  Container.decLock;
  Container.DoFlip;
end;
{$ENDIF}

procedure TDXUIBodenEinsatz.RoundEnd;
begin
end;

procedure TDXUIBodenEinsatz.SelectUnit(Sender: TObject);
var
  Fig: TGameFigure;
begin
  if not CanFriendsMove then
    exit;
    
  Fig:=TGameFigure(fOverButton.Data);
  if GetKeyState(VK_SHIFT)<0 then
    Fig.Selected:=not Fig.Selected
  else
    Fig.Select;
end;

procedure TDXUIBodenEinsatz.SetNextFigure(Next: Boolean);
var
  Dummy: Integer;
  Index: Integer;
  First: Integer;
begin
  Index:=0;
  if Echtzeit then
  begin
    if (Figure=nil) or (not Figure.Selected) or (Figure.IsDead) then
      Figure:=nil
    else
    begin
      for Dummy:=0 to ISOEngine.Figures.Count-1 do
      begin
        if ISOEngine.Figures[Dummy]=Figure then
        begin
          Index:=Dummy;
          break;
        end;
      end;
    end;
    First:=Index;
    repeat
      if Next then
        Index:=(Index+1) mod ISOEngine.Figures.Count
      else
      begin
        dec(Index);
        if Index<0 then
          Index:=ISOEngine.Figures.Count-1;
      end;
      if ISOEngine.Figures[Index].Selected then
      begin
        Figure:=ISOEngine.Figures[Index];
        exit;
      end;
    until (Index=First);
  end
  else
  begin
    // Auswahl der n�chsten Einheit im Rundenbasierten Modus
    for Dummy:=0 to ISOEngine.Figures.Count-1 do
    begin
      if (ISOEngine.Figures[Dummy].Selected) then
      begin
        Index:=Dummy;
        break;
      end;
    end;

    First:=Index;
    repeat
      if Next then
        Index:=(Index+1) mod ISOEngine.Figures.Count
      else
      begin
        dec(Index);
        if Index<0 then
          Index:=ISOEngine.Figures.Count-1;
      end;
      with ISOEngine.Figures[Index] do
      begin
        if (FigureStatus=fsFriendly) and (not IsDead) and (not ImRaumschiff) and (ISOEngine.CanUnitMove(ISOEngine.Figures[Index])) then
        begin
          Select;
          exit;
        end;
      end;
    until (Index=First);
  end;

  SetFigure(nil);
end;

procedure TDXUIBodenEinsatz.ChangeUnit(Sender: TObject);
begin
  if not CanFriendsMove then
    exit;

  SetNextFigure(Boolean(fOverButton.Data));
end;

procedure TDXUIBodenEinsatz.DrawWaffe(Surface: TDirectDrawSurface;
  const Mem: TDDSurfaceDesc; var DrawTop: Integer; const Waffe: TSoldatWaffe);

  procedure DrawEntry(DrawTop: Integer; Text1, Text2: String);
  begin
    Surface.FillRect(Rect(Left+40,DrawTop,Right-2,DrawTop+19),draw_utils_GetAlphaColor(Mem,bcMaroon,100));
    WhiteStdFont.Draw(Surface,Left+42,DrawTop+2,Text1);
    WhiteStdFont.Draw(Surface,Right-4-WhiteStdFont.TextWidth(Text2),DrawTop+2,Text2);
  end;

var
  Item: TLagerItem;
begin
  if Waffe.Gesetzt then
  begin
    DrawCaption(Surface,Mem,Waffe.Name,DrawTop);
    Surface.FillRect(Rect(Left+2,DrawTop+20,Left+39,DrawTop+59),draw_utils_GetAlphaColor(Mem,bcMaroon,100));
    Item:=lager_api_GetItem(Waffe.ID)^;
    lager_api_DrawItem(Waffe.Image,Surface,Left+5,DrawTop+25);
    case Item.TypeId of
      ptWaffe :
      begin
        if Waffe.WaffenArt<>wtShortRange then
          DrawEntry(DrawTop+20,ST0309220082,Format('%d / %d',[Waffe.Schuesse,Waffe.SchussMax]))
        else
          DrawEntry(DrawTop+20,ST0401120001,'');  // Nahkampf
          
        DrawEntry(DrawTop+40,ST0309220081,IntToStr(Waffe.Strength));
      end;
      ptSensor :
        DrawEntry(DrawTop+20,IARange,IntToStr(Waffe.Strength));
      ptMine,ptGranate :
      begin
        DrawEntry(DrawTop+20,ST0309220081,IntToStr(Waffe.Strength));
        DrawEntry(DrawTop+40,IARange,IntToStr(Item.Reichweite));
      end;
    end;
  end
  else
  begin
    DrawCaption(Surface,Mem,ST0309220083,DrawTop);
  end;
  inc(DrawTop,WaffenMenueSize);
end;

procedure TDXUIBodenEinsatz.BuildWaffenMenue(const Waffe: TSoldatWaffe; var TopPos: Integer; Slot: TSoldatConfigSlot);
const
  // Hier werden die Prozeduren festgelegt, die bei einem Klick auf einen Button aufgerufen werden. Beim Aufbau des
  // TBUttonInfo wird daraus ein normaler Methodenzeiger gebastelt
  ClickProcedures : Array[TSchussType] of Pointer = (@TDXUIBodenEinsatz.ClickSchussArt,   {stNichtSchiessen}
                                                     @TDXUIBodenEinsatz.ClickSchussArt,   {stGezielt}
                                                     @TDXUIBodenEinsatz.ClickSchussArt,   {stSpontan}
                                                     @TDXUIBodenEinsatz.ClickSchussArt,   {stAuto}
                                                     @TDXUIBodenEinsatz.ThrowObject,      {stWerfen}
                                                     @TDXUIBodenEinsatz.UseObject,        {stBenutzen}
                                                     @TDXUIBodenEinsatz.ClickSchussArt,   {stSchlag}
                                                     @TDXUIBodenEinsatz.ClickSchussArt,   {stSchwingen}
                                                     @TDXUIBodenEinsatz.ClickSchussArt);  {stStossen}

var
  Button : PButtonInfo;
  Item   : TLagerItem;
  X      : Integer;
  Y      : Integer;
  Dummy  : TSchussType;
  Meth   : TMethod;
begin
  if Waffe.Gesetzt then
  begin
    Item:=lager_api_GetItem(Waffe.ID)^;
    X:=Left+5;
    Y:=TopPos+65;

    for Dummy:=low(TSchussType) to high(TSchussType) do
    begin
      with SchussArten[Dummy] do
      begin
        if (Dummy in Item.Schuss) then
        begin
          Button:=AddNewButton;
          with Button^ do
          begin
            Rect:=Bounds(X,Y,20,20);
            ImageRect:=SymbolRect;

            if Dummy=stWerfen then
            begin
              if Item.TypeID=ptGranate then
                ShortHint:=ST0309220090
              else
                ShortHint:=Hint;
            end
            else if Dummy=stBenutzen then
            begin
              if Item.TypeID=ptSensor then
                ShortHint:=Hint
              else
                ShortHint:=ST0309220089;
            end;

            ShortHint:=Hint;

            HighLight:=Waffe.SchussArt=Dummy;
            Data:=MakeLong(Word(Slot),Word(Dummy));

            // Clickprozedur zusammenbauen
            Meth.Code:=ClickProcedures[Dummy];
            Meth.Data:=Self;
            Click:=TNotifyEvent(Meth);

            ImageAlign:=taCenter;
            if TimeUnits=0 then
              TimeUnits:=round(TUMulti*Waffe.TimeUnits);
          end;
          CalculateShortHintRect(Button^);
          inc(X,21);
        end;
      end;
    end;
  end;

  inc(TopPos,WaffenMenueSize);
end;

function TDXUIBodenEinsatz.AddNewButton: PButtonInfo;
begin
  SetLength(fButtons,length(fButtons)+1);
  result:=Addr(fButtons[High(fButtons)]);

  // Da nach einem SetLength das in einen Anderen Bereich verlegt wird
  // muss OverButton gel�scht werden
  fOverID:=-1;
  fOverButton:=nil;
  // Standardeinstellungen setzen
  result.ID:=High(fButtons);
  result.Draw:=true;
  result.Status:=bsNormal;
  result.Visible:=true;
  result.Enabled:=true;
  result.ImageAlign:=taLeftJustify;
  result.DefaultPage:=uipNone;
  result.Highlight:=false;
  result.Key:=ShortCut(0,[]);
  result.ImageOffset:=2;
  result.TimeUnits:=0;

end;

procedure TDXUIBodenEinsatz.SetSchussArt(SchussArt: TSchussType; Slot: TSoldatConfigSlot; All: Boolean);
var
  Dummy: Integer;
  ID   : Cardinal;
  Waffe: PSoldatWaffe;
begin
  Assert(fFigure<>nil);
  if not All then
  begin
    fFigure.Manager.GetAddrOfSlot(Slot).SchussArt:=SchussArt;
  end
  else
  begin
    ID:=fFigure.Manager.GetAddrOfSlot(Slot).ID;
    with ISOEngine.Figures do
    begin
      for Dummy:=0 to Count-1 do
      begin
        if (Figure[Dummy].FigureStatus=fsFriendly) then
        begin
          Waffe:=Figure[Dummy].Manager.GetAddrOfSlot(Slot);
          if Waffe.ID=ID then
            Waffe.SchussArt:=SchussArt;
        end;
      end;
    end;
  end;
  fUIPage:=uipNone;
  Page:=uipSettings;
end;

procedure TDXUIBodenEinsatz.CameraClick(Sender: TObject);
begin
  // Wenn die UserSperre aktiv ist, wird wegen einer Nachricht der Kampf unterbrochen
  // In diesem Fall kann nur �ber die Funktion nur �ber einen Hotkey aufgerufen werden
  // Es wird so getan als ob der Zentrieren Button gedr�ckt wurde
  if fISOEngine.UserSperre>0 then
  begin
    SendVMessage(vmCenterToPauseObject);
    exit;
  end;

  // Sicherheit bei Aufruf �ber Hotkey
  Assert(high(fButtons)>=5);

  if not fButtons[5].Enabled then exit;
  fButtons[5].HighLight:=not fButtons[5].Highlight;
  if fButtons[5].HighLight then
  begin
    Rec.Figure:=fFigure;
    fButtons[5].ShortHint:=ST0309220091;
  end
  else
  begin
    Rec.Figure:=nil;
    fButtons[5].ShortHint:=ST0309220092;
  end;
  SendVMessage(vmKameraVerfolgung);
  CalculateShortHintRect(fButtons[5]);
  if Page=uipSettings then
    Redraw;                  
end;

procedure TDXUIBodenEinsatz.UseObject(Sender: TObject);
begin
  if (not CanFriendsMove) or (Self.Figure=nil) then
    exit;

  Assert(fOverButton<>nil);

  Rec.Slot:=TSoldatConfigSlot(LoWord(fOverButton.Data));
  Rec.SlotIndex:=HiWord(fOverButton.Data);
  Rec.Figure:=fFigure;
  SendVMessage(vmUseObject);
end;

procedure TDXUIBodenEinsatz.TakeItems(Sender: TObject);
var
  Figure: TGameFigure;
  Dummy : Integer;
  Error : TDragActionError;
begin
  if (not CanFriendsMove) or (Self.Figure=nil) then
    exit;

  SendVMessage(vmContainerLock);
  Figure:=Self.Figure;

  for Dummy:=fISOEngine.ItemList.Count-1 downto 0 do
  begin
    if TISOItem(fISOEngine.ItemList[Dummy]).InRange(Figure) then
    begin
      // Ausr�stung aufnehmen
      Figure.Manager.BeginnEquip;
      Figure.TakeItem(fISOEngine.ItemList[Dummy]);
      Error:=Figure.Manager.EndEquip;
      if Error<>daeNone then
      begin
        Rec.Text:=DropErrors[Error];
        SendVMessage(vmTextMessage);
      end;
    end;
  end;

  if Page=uipSettings then
    AktuButtons;
  SendVMessage(vmContainerUnLock);
  Redraw;
  SendVMessage(vmEngineRedraw);
end;

procedure TDXUIBodenEinsatz.ThrowObject(Sender: TObject);
begin
  if (not CanFriendsMove) or (Self.Figure=nil) then
    exit;

  if SetThrowMode(fFigure,TSoldatConfigSlot(LoWord(fOverButton.Data)),HiWord(fOverButton.Data)) then
  begin
    fOverButton.Highlight:=true;
    fThrowInformation.Button:=fOverButton;
  end;

  Redraw;
end;

procedure TDXUIBodenEinsatz.ChooseThrowTarget(Sender: TObject; X,
  Y: Integer; Button: TMouseButton);
begin
  if Button=mbRight then
  begin
    CancelThrowMode;
    exit;
  end;
  
  if fThrowInformation.Button<>nil then
  begin
    with fThrowInformation do
    begin
      // Sicherstellen, dass nicht auf das Feld der Einheit geworfen wird
      if (FromUnit.XPos=X) and (FromUnit.YPos=Y) then
      begin
        CancelThrowMode;
        exit;
      end;

      Rec.Figure:=FromUnit;
      Rec.Slot:=FromSlot;
      Rec.Point:=Point(X,Y);
      Rec.SlotIndex:=FromData;
      SendVMessage(vmThrowObject);
    end;
    {$IFNDEF DONTRESETTHROWMODE}
    CancelThrowMode;
    {$ENDIF}

    exit;
  end;
end;

function TDXUIBodenEinsatz.SetThrowMode(Figure: TGameFigure; Slot: TSoldatConfigSlot;Data: Integer): boolean;
begin
  if not fThrowInformation.ThrowMode then
  begin
    fThrowInformation.FromUnit:=Figure;
    fThrowInformation.FromSlot:=Slot;
    fThrowInformation.FromData:=Data;
    fThrowInformation.Button:=nil;
    ISOEngine.OnTileClick:=ChooseThrowTarget;

    Container.SetCursor(CAuswahl);
    result:=true;
  end
  else
  begin
    CancelThrowMode;
    result:=false;
  end;
  fThrowInformation.ThrowMode:=result;
end;

function TDXUIBodenEinsatz.CanFriendsMove: boolean;
begin
  SendVMessage(vmCanFriendMove);
  result:=Rec.Result;
end;

procedure TDXUIBodenEinsatz.DrawStatus(Surface: TDirectDrawSurface;  const Mem: TDDSurfacedesc; const ImageRect: TRect;
  Text: String; Top: Integer);
begin
  Surface.FillRect(Bounds(Left+41,Top,21,21),draw_utils_GetAlphaColor(Mem,bcMaroon,100));
  Surface.FillRect(Rect(Left+63,Top,Right-2,Top+21),draw_utils_GetAlphaColor(Mem,bcMaroon,100));

  Surface.Draw(Left+43,Top+3,ImageRect,fUIImages,true);

  WhiteStdFont.Draw(Surface,Left+65,Top+3,Text);
end;

procedure TDXUIBodenEinsatz.Restore;
begin
  fUIImages:=Container.ImageList.Items[1].PatternSurfaces[0];
end;

procedure TDXUIBodenEinsatz.AktuTakeItemsTU;
var
  TUs    : Integer;
  Dummy  : Integer;
begin
  if Echtzeit or (fTakeAllButton=-1) then
    exit;

  TUs:=0;
  for Dummy:=fISOEngine.ItemList.Count-1 downto 0 do
  begin
    if TISOItem(fISOEngine.ItemList[Dummy]).InRange(Figure) then
      inc(TUs,TUTakeBoden);
  end;

  Assert(high(fButtons)>=fTakeAllButton);

  fButtons[fTakeAllButton].TimeUnits:=TUs;
  CalculateShortHintRect(fButtons[fTakeAllButton]);
end;

procedure TDXUIBodenEinsatz.CancelThrowMode;
begin
  ISOEngine.OnTileClick:=nil;

  if fThrowInformation.Button<>nil then
    fThrowInformation.Button.Highlight:=false;

  fThrowInformation.Button:=nil;
  fThrowInformation.ThrowMode:=false;

  Container.SetCursor(CZeiger);

  Redraw;
end;

procedure TDXUIBodenEinsatz.BuildGuertelMenue;
var
  Dummy     : Integer;
  GItem     : TBackPackItem;
  Item      : TLagerItem;
  Button    : PButtonInfo;
  XPos      : Integer;
  EnumRec   : TEnumRuckSackItemRecord;
  Buttons   : Array of record
                ItemID    : Cardinal;
                TypeID    : TProjektType;
                Name      : String;
                TimeUnits : Integer;
                Slot      : TSoldatConfigSlot;
                Index     : Integer;
                Count     : Integer;
  end;

  procedure AddButton(ConfigSlot: TSoldatConfigSlot; SlotIndex: Integer; TypeID: TProjektType);
  var
    Dummy : Integer;
  begin
    // Pr�fen, ob das Item in der Liste bereits steht
    for Dummy:=0 to high(Buttons) do
    begin
      if Item.ID=Buttons[Dummy].ItemID then
      begin
        inc(Buttons[Dummy].Count);
        exit;
      end;
    end;

    // Item steht noch nicht in der Liste => neu eintragen
    SetLength(Buttons,length(Buttons)+1);

    with Buttons[high(Buttons)] do
    begin
      ItemID:=Item.ID;
      TypeID:=Item.TypeID;
      Name:=Item.Name;
      Count:=1;
      Slot:=ConfigSlot;
      Index:=SlotIndex;

      // Zeiteinheiten berechnen
      case TypeID of
        ptGranate        : TimeUnits:=TUThrowItem;
        ptMine,ptSensor  : TimeUnits:=TUUseItem;
      end;

      case Slot of
        scsRucksack      : inc(TimeUnits,TUTakeRucksack);
        scsMunition      : inc(TimeUnits,TUTakeFromGuertel);
      end;
    end;
  end;

begin
  SetLength(Buttons,0);

  // Erstmal die Buttons erstellen
  with fFigure.Manager do
  begin
    for Dummy:=0 to GuertelSlots-1 do
    begin
      if GetGuertelItem(Dummy,GItem) then
      begin
        Item:=lager_api_GetItem(GItem.ID)^;
        if Item.TypeID in [ptGranate,ptMine,ptSensor] then
          AddButton(scsMunition,Dummy,Item.TypeID)
      end;
    end;
    EnumRec.Index:=0;
    repeat
      EnumRucksackItems(EnumRec);
      if (EnumRec.Item.ID<>0) then
      begin
        Item:=lager_api_GetItem(EnumRec.Item.ID)^;
        if Item.TypeID in [ptGranate,ptMine,ptSensor] then
          AddButton(scsRucksack,EnumRec.Index-1,Item.TypeID);
      end;
    until (EnumRec.Item.ID=0);
  end;

  // Zugeh�rige Buttons erstellen
  XPos:=Left+2;
  for Dummy:=0 to min(5,high(Buttons)) do
  begin
    Button:=AddNewButton;
    with Buttons[Dummy] do
    begin
      case TypeID of
        ptGranate:
        begin
          Button.ImageRect:=ThrowRect;
          Button.Click:=ThrowObject;
        end;
        ptMine,ptSensor:
        begin
          Button.ImageRect:=UseRect;
          Button.Click:=UseObject;
        end;
      end;
      if Count>1 then
        Button.ShortHint:=Format('(%1:d) %0:s',[Name,Count])
      else
        Button.ShortHint:=Name;

      Button.TimeUnits:=TimeUnits;
      Button.ImageAlign:=taCenter;
      Button.Rect:=Bounds(XPos,Top+WaffenMenueTop-23,21,21);
      Button.Data:=MakeLong(Word(Slot),Index);

      CalculateShortHintRect(Button^);
      inc(XPos,22)
    end;
  end;
end;

procedure TDXUIBodenEinsatz.ClickSchussArt(Sender: TObject);
begin
  Assert(fOverButton<>nil);
  SetSchussArt(TSchussType(HiWord(fOverButton.Data)),TSoldatConfigSlot(LoWord(fOverButton.Data)),GetKeyState(VK_SHIFT)<0);
end;

{$IFNDEF DISABLEBEAM}
procedure TDXUIBodenEinsatz.ShowBeamMenu(Figure: TGameFigure);
begin
  fBeamUnit:=Figure;
  Page:=uipBeaming;
  Redraw;
end;
{$ENDIF}

procedure TDXUIBodenEinsatz.FigureDestroy(Sender: TObject);
begin
  if Sender=fFigure then
    SetFigure(nil);
end;

procedure TDXUIBodenEinsatz.ChangeTileGrid(Sender: TObject);
begin
  fISOEngine.ISOMap.ShowGrid:=not fISOEngine.ISOMap.ShowGrid;
end;

procedure TDXUIBodenEinsatz.UseGreanadeFast(Sender: TObject);
var
  Slot  : TSoldatConfigSlot;
  Index : Integer;
  Dummy : Integer;

  procedure CheckSlot(CheckSlot: TSoldatConfigSlot);
  var
    Item: TLagerItem;
  begin
    if fFigure.Manager.GetItemInSlot(CheckSlot,Index,Item) then
    begin
      if Item.TypeID=ptGranate then
      begin
        Slot:=CheckSlot;
        Index:=0;
      end;
    end;
  end;

  procedure CheckBackPack(CheckSlot: TSoldatConfigSlot);
  var
    BackPack: TBackPackArray;
    Dummy   : Integer;
    Item    : PLagerItem;
  begin
    BackPack:=fFigure.Manager.GetBackPack(CheckSlot);

    for Dummy:=0 to high(BackPack) do
    begin
      if BackPack[Dummy].IsSet then
      begin
        Item:=lager_api_GetItem(BackPack[Dummy].ID);
        if Item.TypeID=ptGranate then
        begin
          Slot:=CheckSlot;
          Index:=Dummy;
          break;
        end;
      end;
    end;
  end;

begin
  if (not CanFriendsMove) or (Self.Figure=nil) then
    exit;

  Assert(fFigure.Manager<>nil);

  Slot:=scsNone;

  CheckSlot(scsLinkeHand);

  if Slot=scsNone then
    CheckSlot(scsRechteHand);

  if Slot=scsNone then
    CheckBackPack(scsMunition);

  if Slot=scsNone then
    CheckBackPack(scsRucksack);

  if Slot<>scsNone then
  begin
    SetThrowMode(fFigure,Slot,Index);
    for Dummy:=0 to high(fButtons) do
    begin
      if @fButtons[Dummy].Click=@TDXUIBodenEinsatz.ThrowObject then
      begin
        if (TSoldatConfigSlot(LoWord(fButtons[Dummy].Data))=Slot) and (HiWord(fButtons[Dummy].Data)=Index) then
        begin
          fButtons[Dummy].Highlight:=true;
          fThrowInformation.Button:=Addr(fButtons[Dummy]);
          Redraw;
        end;
        exit;
      end;
    end;
  end;
end;

procedure TDXUIBodenEinsatz.UseMineFast(Sender: TObject);
begin
  UseObjectFast(ptMine);
end;

procedure TDXUIBodenEinsatz.UseSensorFast(Sender: TObject);
begin
  UseObjectFast(ptSensor);
end;

procedure TDXUIBodenEinsatz.UseObjectFast(ObjectType: TProjektType);

  function CheckSlot(Slot: TSoldatConfigSlot): Boolean;
  var
    Item: TLagerItem;
  begin
    result:=false;
    if fFigure.Manager.GetItemInSlot(Slot,0,Item) then
    begin
      if Item.TypeID=ObjectType then
      begin
        Rec.Slot:=Slot;
        Rec.SlotIndex:=-1;
        Rec.Figure:=fFigure;
        SendVMessage(vmUseObject);
        result:=true;
      end;
    end;
  end;

  function CheckBackPack(Slot: TSoldatConfigSlot): Boolean;
  var
    BackPack: TBackPackArray;
    Dummy   : Integer;
    Item    : PLagerItem;
  begin
    result:=false;
    BackPack:=fFigure.Manager.GetBackPack(Slot);

    for Dummy:=0 to high(BackPack) do
    begin
      if BackPack[Dummy].IsSet then
      begin
        Item:=lager_api_GetItem(BackPack[Dummy].ID);
        if Item.TypeID=ObjectType then
        begin
          Rec.Slot:=Slot;
          Rec.SlotIndex:=Dummy;
          Rec.Figure:=fFigure;
          SendVMessage(vmUseObject);
          result:=true;
          break;
        end;
      end;
    end;
  end;

begin
  if (not CanFriendsMove) or (Self.Figure=nil) then
    exit;

  Assert(fFigure.Manager<>nil);

  if CheckSlot(scsLinkeHand) then
    exit;

  if CheckSlot(scsRechteHand) then
    exit;

  if CheckBackPack(scsMunition) then
    exit;

  if CheckBackPack(scsRucksack) then
    exit;
end;

procedure TDXUIBodenEinsatz.MouseDown(Button: TMouseButton; X, Y: Integer);
begin
  inherited;
  if (Button=mbLeft) and (Page=uipSettings) then
  begin
    if fFigure<>nil then
    begin
      // Pr�fen, ob auf das Soldatensymbol geklickt wurde
      if PtInRect(Bounds(UnitFacePos.X,UnitFacePos.Y,PersonImageWidth,PersonImageHeight),Point(x,Y)) then
        fISOEngine.ISOMap.CenterTo(fFigure);
    end;
  end;
end;

{ TDXUISoldatenList }

function TDXUISoldatenList.AktuHint: boolean;
var
  Old          : TGameFigure;
  OldSee       : TGameFigure;
  MPoint       : TPoint;
  HasChange    : boolean;
  FigX         : Integer;
  SeeX         : Integer;
begin
  result:=false;
  Old:=Figur;
  OldSee:=FigurSeeRect;

  MPoint:=Point(Container.MousePos.X-Left,Container.MousePos.Y-Top);
  Figur:=GetSolAtPos(MPoint,FigX);
  FigurSeeRect:=GetAlienCountRect(MPoint,SeeX);
  HasChange:=false;

  if (OldSee<>FigurSeeRect) or (Old<>Figur) then
    HasChange:=true;

  if (not HasChange) and (OldSee=FigurSeeRect) then
  begin
    if FigurSeeRect<>nil then
      HasChange:=FigurSeeRect.SeeCount<>fHintSeeCount;
  end;
  if (not HasChange) then exit;

  if Figur=nil then
  begin
    if (FigurSeeRect<>nil) then
    begin
      if FigurSeeRect.SeeCount>0 then
      begin
        fHintText:=ZahlString(ST0309220093,ST0309220094,FigurSeeRect.SeeCount);
        RecalcHintRect(SeeX,Top+2);
      end
      else
        fHintText:='';
      fHintSeeCount:=FigurSeeRect.SeeCount;
    end
    else
      fHintText:='';
  end
  else
  begin
    fHintText:=Figur.Name;
    RecalcHintRect(FigX,Top+16);
  end;

  result:=true;
end;

procedure TDXUISoldatenList.CheckRedraw;
begin
  if fMustRedraw then
  begin
    Redraw;
    fMustRedraw:=false;
  end;
end;

constructor TDXUISoldatenList.Create(Page: TDXPage);
begin
  inherited;
  Figur:=nil;
  fMustRedraw:=false;
end;

procedure TDXUISoldatenList.DblClick(Button: TMouseButton; x, y: Integer);
var
  Fig: TGameFigure;
begin
  inherited;
  if Button<>mbLeft then exit;
  Fig:=GetSolAtPos(Point(X,Y),X);
  if Fig<>nil then
  begin
    if Fig.IsOnField then
      fUI.ISOEngine.ISOMap.CenterTo(Point(Fig.XPos,Fig.YPos));
  end;
end;

procedure TDXUISoldatenList.Draw(Surface: TDirectDrawSurface;
  var Mem: TDDSurfaceDesc);
begin
  RedrawRect(ClientRect,Surface,Mem);
end;

function TDXUISoldatenList.GetAlienCountRect(Pos: TPoint; var X: Integer): TGameFigure;
var
  Dummy  : Integer;
  Li     : TGameFigureList;
  Rec    : TRect;
begin
  result:=nil;
  Li:=fUI.fISOEngine.Figures;
  Rec:=Bounds(2,2,25,14);
  for Dummy:=0 to Li.Count-1 do
  begin
    if TGameFigure(Li[Dummy]).FigureStatus=fsFriendly then
    begin
      if PtInRect(Rec,Pos) then
      begin
        result:=Li[Dummy];
        X:=Rec.Right+1;
        exit;
      end;
      OffsetRect(Rec,28,0);
    end;
  end;
end;

function TDXUISoldatenList.GetSolAtPos(Pos: TPoint; var X: Integer): TGameFigure;
var
  Dummy  : Integer;
  Li     : TGameFigureList;
  Rec    : TRect;
begin
  result:=nil;
  Li:=fUI.fISOEngine.Figures;
  if (Pos.y<14) or (Pos.Y>=43) then exit;
  Rec:=Bounds(3,14,25,43);
  for Dummy:=0 to Li.Count-1 do
  begin
    if TGameFigure(Li[Dummy]).FigureStatus=fsFriendly then
    begin
      if (Pos.X>=Rec.Left) and (Pos.X<Rec.Right) then
      begin
        result:=Li[Dummy];
        X:=Rec.Right+1;
        exit;
      end;
      OffsetRect(Rec,28,0);
    end;
  end;
end;

procedure TDXUISoldatenList.MouseDown(Button: TMouseButton;X, Y: Integer);
var
  Fig   : TGameFigure;
  TempX : Integer;
begin
  inherited;
  if Button<>mbLeft then exit;
  Fig:=GetSolAtPos(Point(X,Y),TempX);
  if Fig<>nil then
  begin
    {$IFNDEF DISABLEBEAM}
    // Einheit noch im Raumschiff ? -> Beammen� anzeigen
    if Fig.ImRaumschiff then
    begin
      Rec.Figure:=Fig;
      SendVMessage(vmShowBeamMenue);
    end
    else
    {$ENDIF}
    begin
      Fig.Select;
    end;
  end;
  Fig:=GetAlienCountRect(Point(X,Y),TempX);
  if Fig<>nil then
    Fig.CenterToNextAlien;
end;

procedure TDXUISoldatenList.MouseLeave;
begin
  inherited;
  Figur:=nil;
  FigurSeeRect:=nil;

  if fHintText='' then
    exit;

  fHintText:='';
  
  Redraw;
end;

procedure TDXUISoldatenList.MouseMove(X, Y: Integer);
begin
  inherited;
  if AktuHint then Redraw;
end;

procedure TDXUISoldatenList.RecalcHintRect(X, Y: Integer);
begin
  if fHintText='' then exit;
  fHintRect.Left:=X;
  fHintRect.Top:=Y;
  fHintRect.Right:=X+4+WhiteStdFont.TextWidth(fHintText);
  fHintRect.Bottom:=Y+18;
end;

procedure TDXUISoldatenList.Redraw;
begin
  if Container.IsLoadGame then
  begin
    fMustRedraw:=true;
    exit;
  end;
  inherited;
end;

procedure TDXUISoldatenList.RedrawRect(DrawRect: TRect;
  Surface: TDirectDrawSurface; var Mem: TDDSurfaceDesc);
var
  Dummy   : Integer;
  Aliens  : Integer;
  List    : TGameFigureList;
  XPos    : Integer;
  Fig     : TGameFigure;
  X,Y     : Integer;
  ACount  : Integer;
  XLeft   : Integer;
  XRight  : Integer;
  Text    : String;

  function DrawMunitionBalken(const Waffe: TSoldatWaffe; X: Integer): boolean;
  begin
    result:=false;
    if (Waffe.Gesetzt) and ((Waffe.MunGesetzt) or (Waffe.WaffenArt=wtLaser)) then
    begin
      if Waffe.SchussMax=0 then

      else
      begin
        Assert(Waffe.Schuesse<=Waffe.SchussMax,'Sch�sse gr��er als Maxsch�sse');
        Surface.FillRect(Rect(X,Top+14+(29-round(29*(Waffe.Schuesse/Waffe.SchussMax))),X+2,Top+43),bcSilver);
      end;
      result:=true;
    end;
  end;

begin
  // Hint aktualisieren
  if (FigurSeeRect<>nil) and (fHintSeeCount<>FigurSeeRect.SeeCount) then
    AktuHint;
  IntersectRect(DrawRect,DrawRect,ClientRect);
  Surface.FillRect(DrawRect,bcBlack);
  Surface.Lock(Mem);
  Surface.Unlock;
  Rectangle(Surface,Mem,ClientRect,bcMaroon);
  List:=fUI.fISOEngine.Figures;
  XPos:=Left+3;
  Text:=EmptyStr;
  for Dummy:=0 to List.Count-1 do
  begin
    Fig:=TGameFigure(List[Dummy]);
    if Fig.FigureStatus=fsFriendly then
    begin
      ACount:=Fig.SeeCount;
      if ACount>0 then
      begin
        // Code zum Zeichnen der gesichteten Aliens
        Rectangle(Surface,Mem,XPos,Top+2,XPos+25,Top+15,bcMaroon);
        if ACount<5 then
          Y:=Top+6
        else
          Y:=Top+4;
        X:=XPos+3;
        for Aliens:=1 to min(8,ACount) do
        begin
          if Aliens=5 then
          begin
            X:=XPos+3;
            inc(Y,5);
          end;
          Surface.Draw(X,Y,FigureRect,fUI.fUIImages);
          inc(X,5);
        end;
      end;
      Fig.Manager.DrawSmallGesicht(Surface,Mem,XPos,Top+14,Fig.Zeiteinheiten);

      rec.Figure:=Fig;
      SendVMessage(vmCanUnitDoAction);

      if not rec.result then
        BlendRectangle(Bounds(Xpos,Top+14,PersonSmallImageWidth,PersonSmallImageHeight),100,bcMaroon,Surface,Mem);

      if not Fig.IsDead then
      begin
        if DrawMunitionBalken(Fig.Manager.RightHandWaffe,XPos-1) then
          XLeft:=XPos+1
        else
          XLeft:=XPos;
        if DrawMunitionBalken(Fig.Manager.LeftHandWaffe,XPos+24) then
          XRight:=XPos+24
        else
          XRight:=XPos+25;

        if Fig.Selected then
          Rectangle(Surface,Mem,XLeft,Top+14,XRight,Top+43,bcGray);
        if fUI.Figure=Fig then
          Rectangle(Surface,Mem,XLeft+1,Top+15,XRight-1,Top+42,bcYellow);
      end;

      inc(XPos,28);
    end;
  end;
  if fHintText<>EmptyStr then
  begin
    BlendRectangle(CorrectBottomOfRect(fHintRect),150,bcDisabled,Surface,Mem);
    Rectangle(Surface,Mem,fHintRect,bcDisabled);
    WhiteStdFont.Draw(Surface,fHintRect.Left+2,fHintRect.Top+2,fHintText);
  end;
end;

procedure TDXUISoldatenList.SetSelected(const Value: TGameFigure);
begin
  fSelected := Value;
  Redraw;
end;

procedure TDXUISoldatenList.SetUI(const Value: TDXUIBodenEinsatz);
begin
  fUI := Value;
end;

initialization

  UnitOrdersTop:=UnitFacePos.y+UnitOrdersTop;
  WaffenMenueTop:=UnitFacePos.y+WaffenMenueTop;

end.


