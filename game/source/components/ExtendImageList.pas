{********************************************************************************
*										*
* Copyright (C) 2004  X-Force Team - jim_raynor@web.de				*
*										*
* This file is part of X-Force: Fight For Destiny (X-Force).			*
*										*
* X-Force is free software; you can redistribute it and/or modify		*
* it under the terms of the GNU General Public License as published by		*
* the Free Software Foundation; either version 2 of the License, or		*
* (at your option) any later version.						*
*										*
* X-Force is distributed in the hope that it will be useful,			*
* but WITHOUT ANY WARRANTY; without even the implied warranty of		*
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the			*
* GNU General Public License for more details.					*
*										*
* You should have received a copy of the GNU General Public License		*
* along with X-Force; if not, write to the Free Software			*
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA	*
*										*
*********************************************************************************
*										*
* Verwaltet die Icons f�r Ausr�stung bestehend aus dem Globalen Bilder		*
* die in allen Spiels�tzen zur Verf�gung stehen und in data.pak definiert sind. *
* Zus�tzlich werden die Spielsatz spezifischen Symbole angef�gt			*
*										*
********************************************************************************}

{$I ../Settings.inc}

unit ExtendImageList;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ImgList;

type
  TExtendImageList = class(TImageList)
  private
    fBasisImages : Integer;
    fImageList   : TImageList;
    fImages      : Integer;
    { Private-Deklarationen }
  protected
    { Protected-Deklarationen }
  public
    procedure AktuSmallImages;
    procedure SetBasisImage(Bitmap: TBitmap);
    procedure SetLokalImage(Bitmap: TBitmap);
    procedure AddBitmap(Bitmap: TBitmap);
    procedure DeleteBitmap(Index: Integer);
    procedure ReplaceBitmap(Bitmap: TBitmap; Index: Integer);
    procedure Draw(Canvas: TCanvas;X,Y: Integer;Index: Integer);
    procedure SaveLokalToStream(Stream: TStream);
    constructor Create(AOwner: TComponent);override;
    destructor Destroy;override;
    { Public-Deklarationen }
  published
    property BasisImages     : Integer read fBasisImages;
    property LokalImages     : Integer read fImages;
    property SmallImageList  : TImageList read fImageList;
    { Published-Deklarationen }
  end;

procedure Register;

implementation

procedure Register;
begin
  RegisterComponents('KD4 Tools', [TExtendImageList]);
end;

{ TExtendImageList }

procedure TExtendImageList.AddBitmap(Bitmap: TBitmap);
begin
  if (Bitmap.Height<>32) or (Bitmap.Width<>32) then
    raise EInvalidGraphic.Create('Die Grafik muss 32x32 Pixels gro� sein!');
  Add(Bitmap,nil);
  AktuSmallImages;
end;

procedure TExtendImageList.AktuSmallImages;
var
  Dummy : Integer;
  Orig  : TBitmap;
  Bitmap: TBitmap;
begin
  fImageList.Clear;
  Bitmap:=TBitmap.Create;
  Orig:=TBitmap.Create;
  Bitmap.Width:=16;
  Bitmap.Height:=16;
  for Dummy:=0 to Count-1 do
  begin
    GetBitmap(Dummy,Orig);
    Bitmap.Canvas.StretchDraw(Rect(0,0,16,16),Orig);
    fImageList.AddMasked(Bitmap,Orig.Canvas.Pixels[0,0]);
  end;
  Bitmap.Free;
  Orig.Free;
end;

constructor TExtendImageList.Create(AOwner: TComponent);
begin
  inherited;
  Masked:=false;
  fImages:=0;
  fImageList:=TImageList.CreateSize(16,16);
end;

procedure TExtendImageList.DeleteBitmap(Index: Integer);
begin
  Delete(Index);
  AktuSmallImages;
end;

destructor TExtendImageList.Destroy;
begin
  fImageList.Free;
  inherited;
end;

procedure TExtendImageList.Draw(Canvas: TCanvas; X, Y, Index: Integer);
var
  Bitmap: TBitmap;
begin
  Bitmap:=TBitmap.Create;
  GetBitmap(Index,Bitmap);
  Bitmap.TransparentColor:=Bitmap.Canvas.Pixels[0,0];
  Bitmap.Transparent:=true;
  Canvas.Draw(X,Y,Bitmap);
  Bitmap.Free;
end;

procedure TExtendImageList.ReplaceBitmap(Bitmap: TBitmap; Index: Integer);
begin
  if (Bitmap.Height<>32) or (Bitmap.Width<>32) then
    raise EInvalidGraphic.Create('Die Grafik muss 32x32 Pixels gro� sein!');
  Replace(Index,Bitmap,nil);
  AktuSmallImages;
end;

procedure TExtendImageList.SaveLokalToStream(Stream: TStream);
var
  Bitmap: TBitmap;
  Dummy : Integer;
  XPos  : Integer;
begin
  Bitmap:=TBitmap.Create;
  Bitmap.Width:=(Count-fBasisImages)*32;
  Bitmap.Height:=32;
  Xpos:=0;
  for Dummy:=fBasisImages to Count-1 do
  begin
    Draw(Bitmap.Canvas,Xpos,0,Dummy);
    inc(XPos,32);
  end;
  Bitmap.SaveToStream(Stream);
  Bitmap.Free;
end;

procedure TExtendImageList.SetBasisImage(Bitmap: TBitmap);
begin
  if Bitmap.Width=0 then exit;
  Clear;
  Add(Bitmap,nil);
  fBasisImages:=Count;
  AktuSmallImages;
end;

procedure TExtendImageList.SetLokalImage(Bitmap: TBitmap);
begin
  while Count>fBasisImages do
    Delete(fBasisImages);

  if Bitmap<>nil then
    Add(Bitmap,nil);

  fImages:=Count-BasisImages;
  AktuSmallImages;
end;

end.
