{********************************************************************************
*										*
* Copyright (C) 2004  X-Force Team - jim_raynor@web.de				*
*										*
* This file is part of X-Force: Fight For Destiny (X-Force).			*
*										*
* X-Force is free software; you can redistribute it and/or modify		*
* it under the terms of the GNU General Public License as published by		*
* the Free Software Foundation; either version 2 of the License, or		*
* (at your option) any later version.						*
*										*
* X-Force is distributed in the hope that it will be useful,			*
* but WITHOUT ANY WARRANTY; without even the implied warranty of		*
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the			*
* GNU General Public License for more details.					*
*										*
* You should have received a copy of the GNU General Public License		*
* along with X-Force; if not, write to the Free Software			*
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA	*
*										*
*********************************************************************************
*										*
* TMissionList verwaltet die Mission (nicht zu verwechseln mit Bodeneins�tzen)  *
* und stellt die Schnittstelle zur Skriptsprache dar.				*
*										*
********************************************************************************}

{$I ../Settings.inc}

unit MissionList;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  TraceFile, XForce_types, UFOList, KD4Utils, RaumschiffList, EinsatzListe,
  ExtRecord, ConvertRecords, uPSCompiler, string_utils, uPSDebugger,
  uPSRuntime, script_utils, uPSPreProcessor, NotifyList, ObjektRecords, StringConst;


const
  EVENT_MISSIONDESTROY : TEventID = 0;

type
  TWinMode   = (mzUFOs,mzBenutzer);
  TMissionState = (msNone,msWin,msLoose,msAbort);

  TSendDebugMessage = procedure(Message: String) of object;
  TOnEvent          = procedure(SkriptEvent: String;Params : Array of const) of object;

  {$M+}
  TEvent = class
  private
    fID           : Cardinal;
    fAktiv        : boolean;
    fSkriptEvent  : String;
    fInterval     : Integer;
    fRepeated     : Boolean;
    fMinutesDone  : Integer;
    fOnEvent      : TOnEvent;
  public
    constructor Create(Event: String; Interval: Integer);
    destructor Destroy;override;

    procedure DoInterval(Minutes: Integer);

    function GenerateExtRecord: TExtRecord;
    procedure ReadFromRecord(Rec: TExtRecord);

    property OnEvent  : TOnEvent read fOnEvent write fOnEvent;
  published
    function WriteToScriptString: String;
    class function ReadFromScriptString(ObjStr: String): TObject;

    property Interval      : Integer read fInterval write fInterval;
    property Active        : Boolean read fAktiv write fAktiv;
    property CalledScript  : String read fSkriptEvent;
    property Repeated      : Boolean read fRepeated write fRepeated;
  end;
  {$M-}

  TEventArray       = Array of TEvent;
  TUFOArray         = Array of TMissionUFO;
  TEinsatzArray     = Array of TMissionEinsatz;
  TRaumschiffArray  = Array of TMissionRaumschiff;

  TMission = class;
  TMissionList = class(TObject)
  private
    fMissions    : TList;
    fDebugMode   : Boolean;
    fMessage     : TSendDebugMessage;
    fCompiler    : TPSPascalCompiler;
    fMissionData : Array of TMissionData;

    // nur f�r den missionstest
    fMissFiles   : Array of TMissionFile;
    procedure AddMission(Mission: TMission);
    procedure DeleteMission(Mission: TMission);
    procedure Clear;
//    procedure SkriptError(Error: String; Skript: TSkript);
    function GetMission(Index: Integer): TMission;
    function GetMissionCount: Integer;
    function GetFileCount: Integer;
    function GetMissionFile(Index: Integer): TMissionFile;
    function SortFunc(Index1,Index2: Integer;Typ: TFunctionType): Integer;

    procedure ReadGameSetMissions;

    procedure ClearSingleTriggers(TrigTyp: TTriggerTyp; CarParam1: Cardinal = 0; IntParam1: Integer = 0;IntParam2: Integer = 0; StringParam1: String = '');

    // Behandlungsroutinen, um auf m�gliche Ereignisse zu reagieren
    procedure ForschProjektEnd(Sender: TObject);
  protected
    procedure NewGameHandler(Sender: TObject);
    function GetMissionDataIndex(SkriptName: String): Integer;
  public
    constructor Create;
    destructor destroy;override;
    procedure SearchForMissions;

    procedure StartMission(Index: Integer);
    procedure StartMissionFromSkriptName(SkriptName: String);
    
    function CreateMission(Script: String;FileName: String = ''; AdditonalDefines: TStringList = nil): TMission;

    function CompileScript(Script: String;FileName: String = ''; AdditonalDefines: TStringList = nil): Boolean;

    procedure NextRound(Minutes: Integer);
    procedure SaveToStream(Stream: TStream);
    procedure LoadFromStream(Stream: TStream);

    procedure SetSkriptStatus(Name: String; Active: Boolean);
    procedure ClearUserTrigger(Switch: String);

    procedure SendDebugMessage(Mess: String);

    property Count                       : Integer read GetMissionCount;
    property DebugMode                   : Boolean read fDebugMode write fDebugMode;
    property OnDebugMessage              : TSendDebugMessage read fMessage write fMessage;
    property Files                       : Integer read GetFileCount;
    property MissionsFile[Index: Integer]: TMissionFile read GetMissionFile;
    property Missions[Index: Integer]    : TMission read GetMission;

    property Compiler                    : TPSPascalCompiler read fCompiler;
  end;

  TMission    = class(TObject)
  private
    fList           : TMissionList;
    fUFOs           : TUFOArray;
    fEinsaetze      : TEinsatzArray;
    fSchiffe        : TRaumschiffArray;
    fEvents         : TEventArray;
    fWinMode        : TWinMode;

    fScriptRunTime  : TPSDebugExec;
    fNotifyList     : TNotifyList;
    fImporter       : TPSRuntimeClassImporter;

    fState          : TMissionState;
    fMissionEnd     : Boolean;
    fMissionName    : String;

    // Compiled Script
    fScript         : String;
    fDebugInfo      : String;
  protected
    procedure RaumschiffShootDown(Sender: TObject);
    procedure RaumschiffReachedDest(Sender: TObject);

    procedure EinsatzWin(Sender: TObject);
    procedure EinsatzTimeUp(Sender: TObject);

    procedure UFOShootDown(Sender: TObject);
    procedure UFOEscape(Sender: TObject);
    procedure UFODiscovered(Sender: TObject);

    procedure ReadingConfiguration;
    procedure CheckMissionsZiel;

    function GetMissionEnd: Boolean;

    function NewMissionUFO(UFO: TUFO): Integer;
    function GetMissionUFO(ID: Cardinal): Integer;

    function NewEreigniss(SkriptEvent: String; Interval: Integer): TEvent;

    function NewMissionSchiff(Raumschiff: TRaumschiff): Integer;
    function GetMissionSchiff(ID: Integer): Integer;

    function NewEinsatz(Einsatz: TEinsatz): Integer;overload;
    procedure NewEinsatz(Einsatz: TMissionEinsatz);overload;
    function GetEinsatzInfo(ID: Cardinal): Integer;

    procedure CallEvent(Event: String;Params : Array of const);

    procedure RunTimeException(E: Exception);

    procedure LoadCompiledScript(Script: String; DebugInfo: String);
    procedure ReadDataFromScriptVars;

    {$IFDEF TRACEVARS}
    procedure TraceGlobalVars;
    {$ENDIF}
  public
    constructor Create(List: TMissionList);
    destructor destroy;override;

    procedure SaveToStream(Stream: TStream);
    procedure LoadFromStream(Stream: TStream);
    procedure ReadFromRecord(Rec: TExtRecord);
    procedure ReadGlobalVarsFromRecord(Rec: TExtRecord);

    procedure NextRound(Minutes: Integer);
    procedure SetSettings(Mode: TWinMode);

    // Aufruf nur erforderlich, wenn Mission �ber CreateMission und nicht �ber StartMission
    // erstellt wurde
    procedure Start;

    function RegisterEvent(Event: TSkriptProcedure; Minutes: Integer): TEvent;stdcall;
    function EventCount: Integer;
    function GetEvent(Index: Integer): TEvent;

    // Registrierung von UFOs
    procedure RegisterUFO(UFO: TUFO);
    procedure RegisterUFOShootDown(ShootDownEvent: TSkriptProcedure; UFO: TUFO);stdcall;
    procedure RegisterUFOEscape(EscapeEvent: TSkriptProcedure; UFO: TUFO);stdcall;
    procedure RegisterUFODiscovered(DiscoveredEvent: TSkriptProcedure; UFO: TUFO);stdcall;
    function RegisteredMissionUFOs: Integer;
    function GetRegisteredMissionUFO(Index: Integer): TMissionUFO;

    // Registrierung von Eins�tzen
    procedure RegisterEinsatzWin(EinsatzWinEvent: TSkriptProcedure; Einsatz: TEinsatz);stdcall;
    procedure RegisterEinsatzTimeUp(EinsatzTimeUpEvent: TSkriptProcedure; Einsatz: TEinsatz);stdcall;
    function RegisteredMissionEinsaetze: Integer;
    function GetRegisteredMissionEinsatz(Index: Integer): TMissionEinsatz;

    // Registrierung von UFOs
    procedure RegisterRaumschiffShootDown(ShootDownEvent: TSkriptProcedure; Schiff: TRaumschiff);stdcall;
    procedure RegisterRaumschiffReachedDest(ReachedEvent: TSkriptProcedure; Schiff: TRaumschiff);stdcall;
    function RegisteredMissionSchiffs: Integer;
    function GetRegisteredMissionSchiff(Index: Integer): TMissionRaumschiff;

    procedure MissionAbort;
    procedure MissionLoose;
    procedure MissionWin;

    function WaitForEvent: Boolean;

    property ScriptEngine : TPSDebugExec read fScriptRunTime;
    property NotifyList   : TNotifyList read fNotifyList;

    property MissionEnd   : Boolean read GetMissionEnd;

    property MissionList  : TMissionList read fList;
//    property UFOIDs   : TList read fUFOs;
  end;


implementation

uses
  KD4SaveGame, Loader, BasisListe, savegame_api, ufo_api, lager_api,
  country_api, basis_api, raumschiff_api, einsatz_api, alien_api, game_api,
  register_scripttypes, uPSUtils, array_utils, Defines, file_utils,
  record_utils, forsch_api;

var
  EventList           : TList = nil;

  TMissionListRecord  : TExtRecordDefinition;
  TMissionRecord      : TExtRecordDefinition;

// In dieser Funktion geh�ren die Pr�fungen, ob die Funktionsk�pfe f�r
// spezielle Skriptfunktionen stimmen
function CheckExports(Sender: TPSPascalCompiler; Proc: TPSInternalProcedure; const ProcDecl: string): Boolean;

  function CheckScriptFunktion(Params: Array of TPSBaseType; NeededDecl: String): Boolean;
  begin
    result:=true;
    if not ExportCheck(Sender, Proc, Params, []) then
    begin
      Sender.MakeError('', ecCustomError, 'Funktionskopf f�r '+Proc.Name+' muss  '+NeededDecl+' entsprechen.');
      result:=false;
    end
  end;

begin
  result:=true;
  if Proc.Name='STARTMISSION' then
    result:=CheckScriptFunktion([0],'procedure StartMission;')
  else if Proc.Name='ONMISSIONWIN' then
    result:=CheckScriptFunktion([0],'procedure OnMissionWin;')
  else if Proc.Name='ONMISSIONLOOSE' then
    result:=CheckScriptFunktion([0],'procedure OnMissionLoose;')
end;

procedure MissionRunTimException(Sender: TPSExec; ExError: TPSError; const ExParam: string; ExObject: TObject; ProcNo, Position: Cardinal);
begin
  (TObject(Sender.ID) as TMission).RunTimeException(ExObject as Exception);
end;

{ TMissionList }

procedure TMissionList.AddMission(Mission: TMission);
begin
  fMissions.Add(Mission);
end;

procedure TMissionList.Clear;
begin
  while fMissions.Count>0 do
    TObject(fMissions[0]).Free;
end;

function TMissionList.CompileScript(Script: String;FileName: String; AdditonalDefines: TStringList): Boolean;
var
  PreProcessor: TPSPreProcessor;
begin
  PreProcessor:=TPSPreProcessor.Create;

  PreProcessor.Defines.Add(Language);
  if AdditonalDefines<>nil then
    PreProcessor.Defines.AddStrings(AdditonalDefines);

  PreProcessor.MainFile:=Script;
  PreProcessor.MainFileName:=ExtractFileName(FileName);
  PreProcessor.PreProcess(ExtractFileName(FileName),Script);
  result:=fCompiler.Compile(Script);

//  PreProcessor.AdjustMessages(fCompiler);

  PreProcessor.Free;
end;

constructor TMissionList.Create;
begin
  if TMissionListRecord=nil then
  begin
    TMissionListRecord:=TExtRecordDefinition.Create('TMissionListRecord');
    TMissionListRecord.AddInteger('MissionCount',0,high(Integer));
    TMissionListRecord.AddRecordList('MissionsData',RecordMissionSkript);

    TMissionRecord:=TExtRecordDefinition.Create('TMission');
    with TMissionRecord do
    begin
      AddInteger('WinMode',0,high(Integer));
      AddString('ScriptString',100000);
      AddString('DebugString',100000);
      AddCardinal('ScriptEngineVersion',0,high(Cardinal));

      AddRecordList('MissionUFOs',RecordMissionUFOs);
      AddRecordList('MissionEinsatz',RecordMissionEinsatz);
      AddRecordList('MissionSchiff',RecordMissionSchiff);
      AddRecordList('MissionEvent',RecordMissionEvent);
    end;
  end;

  fMissions:=TList.Create;

  fCompiler:=TPSPascalCompiler.Create;
  fCompiler.AllowNoBegin:=true;
  fCompiler.BooleanShortCircuit:=true;
  fCompiler.OnUses:=RegisterCompiler;
  fCompiler.OnExportCheck:=CheckExports;

  savegame_api_RegisterCustomSaveHandler(SaveToStream,LoadFromStream,nil,$7B6AB135);
  savegame_api_RegisterNewGameHandler(NewGameHandler);

  forsch_api_RegisterProjektEndHandler(ForschProjektEnd);
end;

procedure TMissionList.DeleteMission(Mission: TMission);
var
  Index: Integer;
begin
  Index:=fMissions.IndexOf(Mission);
  if Index<>-1 then fMissions.Delete(Index);
end;

destructor TMissionList.destroy;
begin
  Clear;
  fMissions.Free;
  fCompiler.Free;
  inherited;
end;

function TMissionList.GetFileCount: Integer;
begin
  result:=length(fMissFiles);
end;

function TMissionList.GetMission(Index: Integer): TMission;
begin
  result:=TMission(fMissions[Index]);
end;

function TMissionList.GetMissionCount: Integer;
begin
  result:=fMissions.Count;
end;

function TMissionList.GetMissionFile(Index: Integer): TMissionFile;
begin
  result:=fMissFiles[Index];
end;

procedure TMissionList.LoadFromStream(Stream: TStream);
var
  Rec    : TExtRecord;
  Anzahl : Integer;
  Dummy  : Integer;
begin
  Clear;
  Rec:=TExtRecord.Create(TMissionListRecord);
  Rec.LoadFromStream(Stream);

  Anzahl:=Rec.GetInteger('MissionCount');

  SetLength(fMissionData,Rec.GetRecordList('MissionsData').Count);

  for Dummy:=0 to high(fMissionData) do
  begin
    fMissionData[Dummy]:=RecordToMissionData(Rec.GetRecordList('MissionsData').Item[Dummy]);
  end;

  Rec.Free;

  while Anzahl>0 do
  begin
    with TMission.Create(Self) do
    begin
      LoadFromStream(Stream);
      {$IFDEF NOLOADOFMISSION}
      if Count>25 then
        Free;
      {$ENDIF}
    end;
    dec(Anzahl);
  end;
end;

procedure TMissionList.NewGameHandler(Sender: TObject);
begin
  Clear;

  ReadGameSetMissions;

  ClearSingleTriggers(ttStartGame);
end;

procedure TMissionList.NextRound(Minutes: Integer);
var
  Dummy  : Integer;
  Date   : TKD4Date;
begin
  for Dummy:=Count-1 downto 0 do
  begin
    Missions[Dummy].NextRound(Minutes);
    if Missions[Dummy].MissionEnd then
      Missions[Dummy].Free;
  end;

  for Dummy:=Count-1 downto 0 do
  begin
    if Missions[Dummy].MissionEnd then
      Missions[Dummy].Free;
  end;
  
  if (not DebugMode) and (length(fMissionData)>0) then
  begin
    for Dummy:=0 to high(fMissionData) do
    begin
      if (fMissionData[Dummy].Active) and (fMissionData[Dummy].Typ=mstRandom) and RandomChance(Minutes/{$IFDEF LOTSOFMISSIONS}200{$ELSE}6000{$ENDIF}*100) then
        StartMission(Dummy);
    end;
  end;

  Date:=savegame_api_GetDate;

  ClearSingleTriggers(ttDate,0,Integer(round(EncodeDate(Date.Year,Date.Month,Date.Day))),(Date.Hour*60)+Date.Minute);
end;

procedure TMissionList.SaveToStream(Stream: TStream);
var
  Dummy  : Integer;
  Rec    : TExtRecord;
begin
  Rec:=TExtRecord.Create(TMissionListRecord);

  Rec.SetInteger('MissionCount',Count);

  for Dummy:=0 to high(fMissionData) do
  begin
    Rec.GetRecordList('MissionsData').Add(MissionDataToRecord(fMissionData[Dummy]));
  end;

  Rec.SaveToStream(Stream);
  Rec.Free;

  for Dummy:=0 to Count-1 do
    Missions[Dummy].SaveToStream(Stream);
end;

procedure TMissionList.SearchForMissions;
var
  Files  : TStringArray;
  Dummy  : Integer;
  Index  : Integer;
begin
  { Missionen ermitteln }
  SetLength(fMissFiles,0);
  Index:=0;
  Files:=file_utils_ReadDirectory('data\scripts\missions\','*.xms',false);
  for Dummy:=0 to high(Files) do
  begin
    GlobalFile.WriteLine;
    GlobalFile.Write('Pr�fe Skript',Files[Dummy]);
    SetLength(fMissFiles,Index+1);
    fMissFiles[Index].FileName:=Files[Dummy];

    if not CompileScript(string_utils_LoadFromFile(fMissFiles[Index].FileName),fMissFiles[Index].FileName) then
    begin
      if fCompiler.MsgCount=0 then
      begin
        fMissFiles[Index].Error:='Unknown Error';
      end
      else
      begin
        fMissFiles[Index].Error:=fCompiler.Msg[0].MessageToString;
        GlobalFIle.Write(fCompiler.Msg[0].MessageToString);
        GlobalFIle.WriteLine;
      end;
    end;
//    fMissFiles[Index].Name:=Skript.SkriptName;
    inc(Index);
    GlobalFile.WriteLine;
  end;
  QuickSort(0,Index-1,SortFunc);
end;

procedure TMissionList.SendDebugMessage(Mess: String);
begin
  if fDebugMode then
    if Assigned(fMessage) then fMessage(Mess);
end;

function TMissionList.SortFunc(Index1, Index2: Integer;
  Typ: TFunctionType): Integer;
var
  Temp      : TMissionFile;
begin
  case Typ of
    ftCompare: result:=StrIComp(PChar(fMissFiles[Index1].Name),PChar(fMissFiles[Index2].Name));
    ftExchange:
    begin
      Temp:=fMissFiles[Index1];
      fMissFiles[Index1]:=fMissFiles[Index2];
      fMissFiles[Index2]:=Temp;
    end;
  end;
end;

function TMissionList.CreateMission(Script: String;FileName: String; AdditonalDefines: TStringList): TMission;
var
  compscript: String;
  compdebug : String;
begin
  if not CompileScript(Script,FileName,AdditonalDefines) then
  begin
    result:=nil;

    exit;
  end;
  result:=TMission.Create(Self);
  
  Assert(fCompiler.GetOutput(compscript));
  Assert(fCompiler.GetDebugOutput(compdebug));

  Result.LoadCompiledScript(compscript,compdebug);

  result.ReadingConfiguration;
end;

procedure TMissionList.StartMission(Index: Integer);
var
  Mission: TMission;
begin
  Assert((Index>=0) and (Index<length(fMissionData)));
  SendDebugMessage(Format(ST0309270001,[fMissionData[Index].Name]));

  Mission:=CreateMission(fMissionData[Index].Skript);
  if Mission<>nil then
    Mission.Start
  else
    game_api_MessageBox(Format('Der Versuch das Skript ''%s'' zu starten ist gescheitert. Grund: '#13#10'%s',[fMissionData[Index].Name,fCompiler.Msg[0].MessageToString]),CError);

  if fMissionData[Index].Typ=mstSingle then
    fMissionData[Index].Active:=false;
end;

procedure TMissionList.ReadGameSetMissions;
var
  Skripts: TRecordArray;
  Dummy  : Integer;
  TrigDum: Integer;
begin
  Skripts:=savegame_api_GameSetGetSkripts;
  SetLength(fMissionData,length(Skripts));
  for Dummy:=0 to high(Skripts) do
  begin
    with fMissionData[Dummy],Skripts[Dummy] do
    begin
      ID:=GetCardinal('ID');
      Typ:=TMissionSkriptTyp(GetInteger('Typ'));
      Name:=GetString('Name');
      Skript:=GetString('Skript');
      Active:=(Typ<>mstRandom) or GetBoolean('Active');

      with GetRecordList('Triggers') do
      begin
        SetLength(Triggers,Count);
        for TrigDum:=0 to Count-1 do
        begin
          Triggers[TrigDum].TriggerTyp:=TTriggerTyp(Item[TrigDum].GetInteger('TriggerTyp'));
          Triggers[TrigDum].Negation:=Item[TrigDum].GetBoolean('Negation');
          Triggers[TrigDum].CarParam1:=Item[TrigDum].GetCardinal('CarParam1');
          Triggers[TrigDum].IntParam1:=Item[TrigDum].GetInteger('IntParam1');
          Triggers[TrigDum].IntParam2:=Item[TrigDum].GetInteger('IntParam2');
          Triggers[TrigDum].StringParam1:=Item[TrigDum].GetString('StringParam1');
          Triggers[TrigDum].Cleared:=false;
        end;
      end;

    end;
  end;

  ClearSingleTriggers(ttNone);
end;

procedure TMissionList.ForschProjektEnd(Sender: TObject);
begin
  ClearSingleTriggers(ttProjectReady,forsch_api_GetLastForschprojekt.ID);
end;

procedure TMissionList.ClearSingleTriggers(TrigTyp: TTriggerTyp;
  CarParam1: Cardinal;IntParam1, IntParam2: Integer; StringParam1: String);
var
  Dummy     : Integer;
  TrigDum   : Integer;
  Start     : Boolean;
  Clear     : Boolean;
begin
  for Dummy:=0 to high(fMissionData) do
  begin
    with fMissionData[Dummy] do
    begin
      if (Typ=mstSingle) and (Active) then
      begin
        Start:=true;
        for TrigDum:=0 to high(Triggers) do
        begin
          Clear:=false;
          if Triggers[TrigDum].TriggerTyp=TrigTyp then
          begin
            //---------------------------------------------------------------------
            // Hier folgt die Pr�fung, ob eine Bedingung erf�llt wurde
            case TrigTyp of
              ttNone         : Clear:=true;
              ttProjectReady : Clear:=Triggers[TrigDum].CarParam1=CarParam1;
              ttDate         : Clear:=(Triggers[TrigDum].IntParam1<IntParam1) or // Tag vorbei
                                      ((Triggers[TrigDum].IntParam1=IntParam1) and // gleicher Tag aber Uhrzeit verstrichen
                                       (Triggers[TrigDum].IntParam2<=IntParam2));
              ttStartGame    : Clear:=true;
              ttUserDefined  : Clear:=Triggers[TrigDum].StringParam1=StringParam1;
              else
                Assert(false,'ClearTyp wird nicht unterst�tzt');
            end;
            //---------------------------------------------------------------------

            if Clear then
              Triggers[TrigDum].Cleared:=true;
          end;

          with Triggers[TrigDum] do
          begin
            // Wenn Negation true ist, darf die Bedingung nicht erf�llt (Cleared=false) sein
            // Wenn Negation false ist, muss die Bedingung erf�llt sein (Cleared=true)
            // langform: if ((not Cleared) and (not Negation)) or (Cleared and Negation) then
            if Cleared=Negation then
              Start:=false;
          end;
        end;
        if Start=true then
        begin
          // Alle Bedingungen sind erf�llt => Mission starten
          StartMission(Dummy);
        end;
      end;
    end;
  end;
end;

procedure TMissionList.SetSkriptStatus(Name: String; Active: Boolean);
var
  Index: Integer;
begin
  Index:=GetMissionDataIndex(Name);
  if (Index<>-1) and (fMissionData[Index].Typ=mstRandom) then
    fMissionData[Index].Active:=Active;
end;

function TMissionList.GetMissionDataIndex(SkriptName: String): Integer;
var
  Dummy: Integer;
begin
  result:=-1;
  for Dummy:=0 to high(fMissionData) do
  begin
    if fMissionData[Dummy].Name=SkriptName then
    begin
      result:=Dummy;
      exit;
    end;
  end;

  Assert(false,'Skript nicht im Spielsatz gefunden');
end;

procedure TMissionList.StartMissionFromSkriptName(SkriptName: String);
var
  Index: Integer;
begin
  Index:=GetMissionDataIndex(SkriptName);
  if (Index<>-1) then
    StartMission(Index);
end;

procedure TMissionList.ClearUserTrigger(Switch: String);
begin
  ClearSingleTriggers(ttUserDefined,0,0,0,Switch);
end;

{ TMission }

function TMission.NewEreigniss(SkriptEvent: String; Interval: Integer): TEvent;
begin
  result:=TEvent.Create(SkriptEvent,Interval);
  result.OnEvent:=CallEvent;

  SetLength(fEvents,length(fEvents)+1);
  fEvents[high(fEvents)]:=result;
end;

procedure TMission.CallEvent(Event: String;Params : Array of const);
var
  List: TPSList;
  Proc: Cardinal;
begin
  if Event='' then
  begin
    CheckMissionsZiel;
    exit;
  end;

  Proc:=fScriptRuntime.GetProc(Event);
  if Proc=InvalidVal then
  begin
    exit;
  end;

  List:=script_utils_constArrayToPSList(fScriptRunTime, fScriptRuntime.GetProcNo(Proc) as TPSInternalProcRec,Params);

  try
    fScriptRunTime.RunProc(List,Proc);
  finally
    script_utils_ClearPSList(List);
    List.Free;
  end;

  CheckMissionsZiel;
end;

procedure TMission.CheckMissionsZiel;
var
  Dummy      : Integer;
  UFOsLived  : Boolean;
begin
  if fMissionEnd then
    exit;

  // Pr�fen ob alle UFOs abgeschossen sind
  if (fWinMode=mzUFOs) and (fState=msNone) then
  begin
    UFOsLived:=false;
    for Dummy:=0 to High(fUFOs) do
    begin
      if not fUFOs[Dummy].Delete then
      begin
        UFOsLived:=true;
        break;
      end;
    end;

    if not UFOsLived then
      fState:=msWin;
  end;

  if fState<>msNone then
  begin
    fMissionEnd:=true;
    case fState of
      msWin:
      begin
        fList.SendDebugMessage(ST0309210015);
        // Einsatz gewonnen
        CallEvent('OnMissionWin',[]);
      end;

      msLoose:
      begin
        fList.SendDebugMessage(ST0309210016);
        // Einsatz verloren
        CallEvent('OnMissionLoose',[]);
      end;

      msAbort:
      // Einsatz abgebrochen
      begin
        fList.SendDebugMessage(ST0309270003);
      end;
    end;
  end;
end;

constructor TMission.Create(List: TMissionList);
begin
//  Assert(false,'Auskommentiert TMission.Create');
  fList:=List;
  fList.AddMission(Self);

  SetLength(fUFOs,0);
  fWinMode:=mzUFOs;

  fScriptRunTime:=TPSDebugExec.Create;
  fScriptRunTime.DebugEnabled:=false;

  // ID darf nach dem erfolgreichen Compilieren ge�ndert werden, sofern
  // OnException ge�ndert wird
  fScriptRunTime.Id:=Self;
  fImporter:=RegisterRunTimeEngine(fScriptRunTime);

  fScriptRunTime.OnException:=MissionRunTimException;

  fState:=msNone;
  fMissionEnd:=false;

  fNotifyList:=TNotifyList.Create;
end;

destructor TMission.destroy;
var
  Dummy     : Integer;
  UFO       : TUFO;
  Einsatz   : TEinsatz;
  Schiff    : TRaumschiff;
begin
  fNotifyList.CallEvents(EVENT_MISSIONDESTROY,Self);

  // UFOs freigeben
  for Dummy:=0 to high(fUFOs) do
  begin
    UFO:=ufo_api_GetUFO(fUFOs[Dummy].UFOID);
    if UFO<>nil then
    begin
      UFO.NotifyList.RemoveEvent(fUFOs[Dummy].EventOnShootDown);
      UFO.NotifyList.RemoveEvent(fUFOs[Dummy].EventOnEscape);
      UFO.NotifyList.RemoveEvent(fUFOs[Dummy].EventOnDiscovered);
    end;
  end;
  SetLength(fUFOs,0);

  // Eins�tze freigeben
  for Dummy:=0 to high(fEinsaetze) do
  begin
    Einsatz:=einsatz_api_GetEinsatz(fEinsaetze[Dummy].ID);
    if (Einsatz<>nil) then
    begin
      Einsatz.NotifyList.RemoveEvent(fEinsaetze[Dummy].NotifyHandleWin);
      Einsatz.NotifyList.RemoveEvent(fEinsaetze[Dummy].NotifyHandleTimeUp);
    end;
  end;
  SetLength(fEinsaetze,0);

  // Raumschiff freigeben
  for Dummy:=0 to high(fSchiffe) do
  begin
    Schiff:=raumschiff_api_GetRaumschiff(fSchiffe[Dummy].SchiffID);
    if (Schiff<>nil) then
    begin
      Schiff.NotifyList.RemoveEvent(fSchiffe[Dummy].EventOnReached);
      Schiff.NotifyList.RemoveEvent(fSchiffe[Dummy].EventOnDestroy);
    end;
  end;

  // Ereignisse freigeben
  for Dummy:=0 to High(fEvents) do
    fEvents[Dummy].Free;

  fList.DeleteMission(Self);

  fNotifyList.Free;
  fScriptRunTime.Free;

  fImporter.Free;

  inherited;
end;

procedure TMission.LoadFromStream(Stream: TStream);
var
  Rec  : TExtRecord;
  Def  : TExtRecordDefinition;
  Err  : Boolean;
begin
  Rec:=TExtRecord.Create(TMissionRecord);
  Rec.LoadFromStream(Stream);
  Err:=false;
  try
    ReadFromRecord(Rec);
  except
    Err:=true;
  end;

  // Pr�fen, ob die Mission in einer kompatiblen Version gespeichert wurde
  if Rec.GetCardinal('ScriptEngineVersion')<>ScriptEngineVersion then
  begin
    Err:=true;
  end;

  Rec.Free;

  // Globale Variablen laden
  Def:=TExtRecordDefinition.Create('GlobalVars');
  Def.LoadFromStream(Stream);

  Rec:=TExtRecord.Create(Def);
  Rec.LoadFromStream(Stream);

  if not Err then
    ReadGlobalVarsFromRecord(Rec);

  Rec.Free;
  Def.Free;

  fState:=msNone;

  // Trat ein Fehler auf,
  if Err then
  begin
    game_api_MessageBox(Format(CR0503310001,[fMissionName]),CError);
    Free;
  end;
end;

procedure TMission.NextRound(Minutes: Integer);
var
  Dummy     : Integer;
begin
  for Dummy:=high(fEvents) downto 0 do
  begin
    fEvents[Dummy].DoInterval(Minutes);
  end;
end;

function TMission.GetMissionEnd: Boolean;
begin
  result:=fState<>msNone;
end;

procedure TMission.SaveToStream(Stream: TStream);
var
  Rec     : TExtRecord;
  Rec2    : TExtRecord;
  Dummy   : Integer;
  Def     : TExtRecordDefinition;

  GlobVar : PPSVariant;
  GlobName: String;
begin
//  SaveInitVariables;
  Rec:=TExtRecord.Create(TMissionRecord);

  Rec.SetInteger('WinMode',Integer(fWinMode));

  Rec.SetString('ScriptString',fScript);
  Rec.SetString('DebugString',fDebugInfo);
  Rec.SetCardinal('ScriptEngineVersion',ScriptEngineVersion);

  for Dummy:=0 to high(fUFOs) do
  begin
    Rec2:=MissionUFOToRecord(fUFOs[Dummy]);
    Rec.GetRecordList('MissionUFOs').Add(Rec2);
  end;

  for Dummy:=0 to high(fEinsaetze) do
  begin
    Rec2:=MissionEinsatzToRecord(fEinsaetze[Dummy]);
    Rec.GetRecordList('MissionEinsatz').Add(Rec2);
  end;

  for Dummy:=0 to high(fSchiffe) do
  begin
    Rec2:=MissionSchiffToRecord(fSchiffe[Dummy]);
    Rec.GetRecordList('MissionSchiff').Add(Rec2);
  end;

  for Dummy:=0 to high(fEvents) do
  begin
    Rec.GetRecordList('MissionEvent').Add(fEvents[Dummy].GenerateExtRecord);
  end;

  Rec.SaveToStream(Stream);
  Rec.Free;

  // Globale Variablen speichern
  Def:=TExtRecordDefinition.Create('GlobalVars');
  for Dummy:=0 to fScriptRunTime.GlobalVarNames.Count-1 do
  begin
    GlobVar:=fScriptRunTime.GetGlobalVar(Dummy);
    GlobName:=fScriptRunTime.GlobalVarNames[Dummy];

    script_utils_AddTypeToExtRecord(GlobName,GlobVar.FType,Def);
  end;

  Def.SaveToStream(Stream);

  Rec:=TExtRecord.Create(Def);
  // Daten f�r Globale Variablen �bernehmen
  for Dummy:=0 to fScriptRunTime.GlobalVarNames.Count-1 do
  begin
    GlobVar:=fScriptRunTime.GetGlobalVar(Dummy);
    GlobName:=fScriptRunTime.GlobalVarNames[Dummy];

    script_utils_SetVarToExtRecord(GlobName,GlobVar,Rec);
  end;

  Rec.DumpData;
  Rec.SaveToStream(Stream);

  Rec.Free;
  Def.Free;
end;

procedure TMission.SetSettings(Mode: TWinMode);
begin
  fWinMode:=Mode;
end;

procedure TMission.ReadingConfiguration;
begin
  fScriptRunTime.RunScript;

  ReadDataFromScriptVars;
end;

function TMission.NewMissionSchiff(Raumschiff: TRaumschiff): Integer;
begin
  result:=GetMissionSchiff(Raumschiff.ID);
  if result=-1 then
  begin
    setLength(fSchiffe,length(fSchiffe)+1);
    result:=high(fSchiffe);

    FillChar(fSchiffe[result],sizeOf(TMissionRaumschiff),#0);

    fSchiffe[result].SchiffID:=Raumschiff.ID;
    fSchiffe[result].EventOnReached:=Raumschiff.NotifyList.RegisterEvent(EVENT_SCHIFFREACHEDDEST,RaumschiffReachedDest);
    fSchiffe[result].EventOnDestroy:=Raumschiff.NotifyList.RegisterEvent(EVENT_SCHIFFSHOOTDOWN,RaumschiffShootDown);
  end;
end;

function TMission.NewEinsatz(Einsatz: TEinsatz): Integer;
begin
  result:=GetEinsatzInfo(Einsatz.ID);
  if result=-1 then
  begin
    SetLength(fEinsaetze,length(fEinsaetze)+1);
    result:=high(fEinsaetze);

    FillChar(fEinsaetze[result],sizeOf(TMissionEinsatz),#0);

    fEinsaetze[result].ID:=Einsatz.ID;
    fEinsaetze[result].NotifyHandleWin:=Einsatz.NotifyList.RegisterEvent(EVENT_EINSATZONWIN,EinsatzWin);
    fEinsaetze[result].NotifyHandleTimeUp:=Einsatz.NotifyList.RegisterEvent(EVENT_EINSATZONTIMEUP,EinsatzTimeUp);
  end;
end;

procedure TMission.EinsatzWin(Sender: TObject);
var
  MEinsa: Integer;
begin
  MEinsa:=GetEinsatzInfo(TEinsatz(Sender as TEinsatz).ID);
  if MEinsa=-1 then
    exit;

  if fEinsaetze[MEinsa].OnWin<>'' then
    CallEvent(fEinsaetze[MEinsa].OnWin,[einsatz_api_GetEinsatz(fEinsaetze[MEinsa].ID,true)]);
end;

function TMission.GetEinsatzInfo(ID: Cardinal): Integer;
var
  Dummy: Integer;
begin
  result:=-1;
  for Dummy:=0 to high(fEinsaetze) do
  begin
    if fEinsaetze[Dummy].ID=ID then
    begin
      result:=Dummy;
      exit;
    end;
  end;
end;

procedure TMission.NewEinsatz(Einsatz: TMissionEinsatz);
var
  EinsatzObj: TEinsatz;
begin
  SetLength(fMissions,length(fMissions)+1);
  fEinsaetze[high(fEinsaetze)]:=Einsatz;

  EinsatzObj:=einsatz_api_GetEinsatz(Einsatz.ID);
  if EinsatzObj<>nil then
  begin
    with fEinsaetze[high(fEinsaetze)] do
    begin
      NotifyHandleWin:=EinsatzObj.NotifyList.RegisterEvent(EVENT_EINSATZONWIN,EinsatzWin);
      NotifyHandleTimeUp:=EinsatzObj.NotifyList.RegisterEvent(EVENT_EINSATZONTIMEUP,EinsatzTimeUp);
    end;
  end;
end;

procedure TMission.EinsatzTimeUp(Sender: TObject);
var
  MEinsa: Integer;
begin
  MEinsa:=GetEinsatzInfo(TEinsatz(Sender).ID);
  if MEinsa=-1 then
    exit;
  if fEinsaetze[MEinsa].OnTimeUp<>'' then
    CallEvent(fEinsaetze[MEinsa].OnTimeUp,[einsatz_api_GetEinsatz(fEinsaetze[MEinsa].ID,true)]);
end;

procedure TMission.Start;
begin
  CallEvent('StartMission',[]);
end;

function TMission.RegisterEvent(Event: TSkriptProcedure; Minutes: Integer): TEvent;stdcall;
begin
//  Proc;
  result:=NewEreigniss(script_utils_GetFunctionName(fScriptRunTime,Event),Minutes);
end;

function TMission.NewMissionUFO(UFO: TUFO): Integer;
var
  Index: Integer;
  Dummy: Integer;
begin
  Index:=-1;
  for Dummy:=0 to high(fUFOs) do
  begin
    if fUFOs[Dummy].UFOID=UFO.ID then
    begin
      Index:=Dummy;
      break;
    end;
  end;

  if Index=-1 then
  begin
    SetLength(fUFOs,length(fUFOs)+1);
    with fUFOs[high(fUFOs)] do
    begin
      UFOID:=UFO.ID;
      Delete:=false;
    end;
    Index:=high(fUFOs);

    fUFOs[Index].EventOnShootDown:=UFO.NotifyList.RegisterEvent(EVENT_ONUFOSHOOTDOWN,UFOShootDown);
    fUFOs[Index].EventOnEscape:=UFO.NotifyList.RegisterEvent(EVENT_ONUFOESCAPE,UFOEscape);
    fUFOs[Index].EventOnDiscovered:=UFO.NotifyList.RegisterEvent(EVENT_ONUFODISCOVERED,UFODiscovered);
  end;
  result:=Index;
end;

procedure TMission.RegisterUFOShootDown(ShootDownEvent: TSkriptProcedure; UFO: TUFO);stdcall;
begin
  fUFOs[NewMissionUFO(UFO)].OnShootDown:=script_utils_GetFunctionName(fScriptRunTime,ShootDownEvent);
end;

function TMission.WaitForEvent: Boolean;                       
begin
  result:=fScriptRunTime.GetCurrentProcNo=0;
end;

function TMission.EventCount: Integer;
begin
  result:=length(fEvents);
end;

function TMission.GetEvent(Index: Integer): TEvent;
begin
  if (Index<0) or (Index>high(fEvents)) then
    raise Exception.Create('Ung�ltiger Event-Index');

  result:=fEvents[Index];
end;

procedure TMission.MissionAbort;
begin
  fState:=msAbort;
  fScriptRunTime.Stop;
end;

procedure TMission.MissionLoose;
begin
  fState:=msLoose;
  fScriptRunTime.Stop;
end;

procedure TMission.MissionWin;
begin
  fState:=msWin;
  fScriptRunTime.Stop;
end;

procedure TMission.UFOShootDown(Sender: TObject);
var
  MUFO: Integer;
begin
  MUFO:=GetMissionUFO((Sender as TUFO).ID);
  if MUFO=-1 then
    exit;

  if ufo_api_GetUFO(fUFOs[MUFO].UFOID)<>nil then
  begin
    fUFOs[MUFO].Delete:=true;
    CallEvent(fUFOs[MUFO].OnShootDown,[ufo_api_GetUFO(fUFOs[MUFO].UFOID)]);
  end;
end;

procedure TMission.UFOEscape(Sender: TObject);
var
  MUFO: Integer;
begin
  MUFO:=GetMissionUFO((Sender as TUFO).ID);
  if MUFO=-1 then
    exit;

  if ufo_api_GetUFO(fUFOs[MUFO].UFOID)<>nil then
  begin
    fUFOs[MUFO].Delete:=true;
    CallEvent(fUFOs[MUFO].OnEscape,[ufo_api_GetUFO(fUFOs[MUFO].UFOID)]);
  end;
end;

function TMission.RegisteredMissionUFOs: Integer;
begin
  result:=length(fUFOs);
end;

function TMission.GetRegisteredMissionUFO(Index: Integer): TMissionUFO;
begin
  if (Index<0) or (Index>high(fUFOs)) then
    raise Exception.Create('Ung�ltiger UFO-Index');

  result:=fUFOs[Index];
end;

function TMission.GetRegisteredMissionEinsatz(Index: Integer): TMissionEinsatz;
begin
  if (Index<0) or (Index>high(fEinsaetze)) then
    raise Exception.Create('Ung�ltiger Einsatz-Index');

  result:=fEinsaetze[Index];
end;

function TMission.RegisteredMissionEinsaetze: Integer;
begin
  result:=length(fEinsaetze);
end;

procedure TMission.RegisterUFO(UFO: TUFO);
begin
  NewMissionUFO(UFO);
end;

procedure TMission.RegisterEinsatzTimeUp(EinsatzTimeUpEvent: TSkriptProcedure; Einsatz: TEinsatz);
begin
  fEinsaetze[NewEinsatz(Einsatz)].OnTimeUp:=script_utils_GetFunctionName(fScriptRunTime,EinsatzTimeUpEvent);
end;

procedure TMission.RegisterEinsatzWin(EinsatzWinEvent: TSkriptProcedure;
  Einsatz: TEinsatz);
begin
  fEinsaetze[NewEinsatz(Einsatz)].OnWin:=script_utils_GetFunctionName(fScriptRunTime,EinsatzWinEvent);
end;

procedure TMission.RegisterUFOEscape(EscapeEvent: TSkriptProcedure;
  UFO: TUFO);
begin
  fUFOs[NewMissionUFO(UFO)].OnEscape:=script_utils_GetFunctionName(fScriptRunTime,EscapeEvent);
end;

procedure TMission.RunTimeException(E: Exception);
begin
  fState:=msAbort;
  fScriptRunTime.Stop;

  if E is ELooseGame then
    raise E;
end;

function TMission.GetMissionSchiff(ID: Integer): Integer;
var
  Dummy: Integer;
begin
  result:=-1;
  for Dummy:=0 to high(fSchiffe) do
  begin
    if fSchiffe[Dummy].SchiffID=ID then
    begin
      result:=Dummy;
      break;
    end;
  end;
end;

procedure TMission.RaumschiffReachedDest(Sender: TObject);
var
  MSchiff: Integer;
begin
  MSchiff:=GetMissionSchiff(TRaumschiff(Sender).ID);
  if MSchiff=-1 then
    exit;
  if fSchiffe[MSchiff].OnReached<>'' then
    CallEvent(fSchiffe[MSchiff].OnReached,[raumschiff_api_GetRaumschiff(fSchiffe[MSchiff].SchiffID,true)]);
end;

procedure TMission.RaumschiffShootDown(Sender: TObject);
var
  MSchiff: Integer;
begin
  MSchiff:=GetMissionSchiff(TRaumschiff(Sender).ID);
  if MSchiff=-1 then
    exit;
  if fSchiffe[MSchiff].OnDestroy<>'' then
    CallEvent(fSchiffe[MSchiff].OnDestroy,[raumschiff_api_GetRaumschiff(fSchiffe[MSchiff].SchiffID,true)]);
end;

function TMission.GetRegisteredMissionSchiff(
  Index: Integer): TMissionRaumschiff;
begin
  if (Index<0) or (Index>high(fSchiffe)) then
    raise Exception.Create('Ung�ltiger Schiffe-Index');

  result:=fSchiffe[Index];
end;

function TMission.RegisteredMissionSchiffs: Integer;
begin
  result:=length(fSchiffe);
end;

procedure TMission.RegisterRaumschiffReachedDest(
  ReachedEvent: TSkriptProcedure; Schiff: TRaumschiff);
begin
  fSchiffe[NewMissionSchiff(Schiff)].OnReached:=script_utils_GetFunctionName(fScriptRunTime,ReachedEvent);
end;

procedure TMission.RegisterRaumschiffShootDown(
  ShootDownEvent: TSkriptProcedure; Schiff: TRaumschiff);
begin
  fSchiffe[NewMissionSchiff(Schiff)].OnDestroy:=script_utils_GetFunctionName(fScriptRunTime,ShootDownEvent);
end;

function TMission.GetMissionUFO(ID: Cardinal): Integer;
var
  Dummy: Integer;
begin
  result:=-1;
  for Dummy:=0 to high(fUFOs) do
  begin
    if fUFOs[Dummy].UFOID=ID then           
    begin
      result:=Dummy;
      break;
    end;
  end;
end;

procedure TMission.LoadCompiledScript(Script, DebugInfo: String);
begin
  Assert(fScriptRunTime.LoadData(Script));
  fScriptRunTime.LoadDebugData(DebugInfo);

  fScript:=Script;
  fDebugInfo:=DebugInfo;
end;

procedure TMission.ReadFromRecord(Rec: TExtRecord);
var
  Dummy   : Integer;
  Index   : Integer;
begin
  Assert(Rec.RecordDefinition=TMissionRecord);

  fWinMode:=TWinMode(Rec.GetInteger('WinMode'));

  LoadCompiledScript(Rec.GetString('ScriptString'),Rec.GetString('DebugString'));

  // UFOs laden
  with Rec.GetRecordList('MissionUFOs') do
  begin
    for Dummy:=0 to Count-1 do
    begin
      if ufo_api_GetUFO(Item[Dummy].GetCardinal('ID'))<>nil then
      begin
        Index:=NewMissionUFO(ufo_api_GetUFO(Item[Dummy].GetCardinal('ID')));
        fUFOs[Index].Delete:=false;
        fUFOs[Index].OnShootDown:=Item[Dummy].GetString('OnShootDown');
        fUFOs[Index].OnEscape:=Item[Dummy].GetString('OnEscape');
      end;
    end;
  end;

  // Eins�tze laden
  with Rec.GetRecordList('MissionEinsatz') do
  begin
    for Dummy:=0 to Count-1 do
    begin
      if einsatz_api_GetEinsatz(Item[Dummy].GetCardinal('ID'))<>nil then
      begin
        Index:=NewEinsatz(einsatz_api_GetEinsatz(Item[Dummy].GetCardinal('ID')));
        fEinsaetze[Index].OnWin:=Item[Dummy].GetString('OnWin');
        fEinsaetze[Index].OnTimeUp:=Item[Dummy].GetString('OnTimeUp');
      end;
    end;
  end;

  // Raumschiffe laden
  with Rec.GetRecordList('MissionSchiff') do
  begin
    for Dummy:=0 to Count-1 do
    begin
      if raumschiff_api_GetRaumschiff(Item[Dummy].GetInteger('SchiffID'))<>nil then
      begin
        Index:=NewMissionSchiff(raumschiff_api_GetRaumschiff(Item[Dummy].GetInteger('SchiffID')));
        fSchiffe[Index].OnDestroy:=Item[Dummy].GetString('OnDestroy');
        fSchiffe[Index].OnReached:=Item[Dummy].GetString('OnReached');
      end;
    end;
  end;

  // Events laden
  // Raumschiffe laden
  with Rec.GetRecordList('MissionEvent') do
  begin
    for Dummy:=0 to Count-1 do
    begin
      NewEreigniss('',0).ReadFromRecord(Item[Dummy]);
    end;
  end;
end;

procedure TMission.ReadGlobalVarsFromRecord(Rec: TExtRecord);
var
  Dummy   : Integer;
  GlobVar : PPSVariant;
  GlobName: String;
begin
  // Daten f�r Globale Variablen �bernehmen
  for Dummy:=0 to fScriptRunTime.GlobalVarNames.Count-1 do
  begin
    GlobVar:=fScriptRunTime.GetGlobalVar(Dummy);
    GlobName:=fScriptRunTime.GlobalVarNames[Dummy];

    script_utils_GetVarFromExtRecord(GlobName,Addr(PPSVariantData(GlobVar).Data[0]),GlobVar.FType,Rec);
  end;

  ReadDataFromScriptVars;
  
  {$IFDEF TRACEVARS}
  TraceGlobalVars;
  {$ENDIF}
end;

{$IFDEF TRACEVARS}
procedure TMission.TraceGlobalVars;
var
  Dummy : Integer;
  Name  : String;
  pv    : PIFVariant;

  procedure AddItem(Text: String);
  begin
    GlobalFile.Write(Text);
  end;

  procedure VariantToStr(Variant: TPSVariantIFC; Indent: String = ''; First: String = '');
  var
    Entrys    : Integer;
    Dummy     : Integer;
    Text      : String;
    NewVar    : TPSVariantIFC;
    OffS      : Integer;
  begin
    case Variant.aType.BaseType of
      btArray:
      begin
        Entrys:=PSDynArrayGetLength(Pointer(Variant.dta^),Variant.aType);
        AddItem(Indent+First+Name+' Array = (');
        for Dummy:=0 to Entrys-1 do
        begin
          VariantToStr(PSGetArrayField(Variant,Dummy),Indent+#9,Format('[%d] ',[Dummy]));
        end;
        if Entrys=0 then
          AddItem(First+Indent+#9'Leer');

        AddItem(Indent+')');
      end;
      btStaticArray:
      begin
        Entrys:=TPSTypeRec_StaticArray(Variant.aType).Size;
        AddItem(Indent+First+Name+' Array = (');
        NewVar:=Variant;
        for Dummy:=0 to Entrys-1 do
        begin
          Offs := TPSTypeRec_StaticArray(Variant.aType).ArrayType.RealSize * Cardinal(Dummy);
          NewVar.aType := TPSTypeRec_StaticArray(Variant.aType).ArrayType;
          NewVar.Dta := Pointer(IPointer(Variant.dta) + Offs);
          VariantToStr(NewVar,Indent+#9,Format('[%d] ',[Dummy]));
        end;
        if Entrys=0 then
          AddItem(First+Indent+#9'Leer');

        AddItem(Indent+')');
      end;
      btRecord:
      begin
        Entrys:=TPSTypeRec_Record(Variant.aType).FieldTypes.Count;
        AddItem(Indent+First+Name+' record = (');
        for Dummy:=0 to Entrys-1 do
        begin
          VariantToStr(PSGetRecField(Variant,Dummy),Indent+#9,Format('[%d] ',[Dummy]));
        end;
        if Entrys=0 then
          AddItem(First+Indent+#9'Leer');

        AddItem(Indent+')');
      end;
      else
      begin
        Text:=PSVariantToString(Variant, '');
        AddItem(Indent+First+Name+': '+Variant.aType.ExportName +' = '+Text+';');
      end;
    end;
  end;

begin
  for Dummy:=0 to fScriptRunTime.GlobalVarNames.Count-1 do
  begin
    Name:=fScriptRunTime.GlobalVarNames[Dummy];
    pv:=fScriptRunTime.GetGlobalVar(Dummy);
    VariantToStr(NewTPSVariantIFC(pv, False));
  end;

end;
{$ENDIF}

procedure TMission.RegisterUFODiscovered(DiscoveredEvent: TSkriptProcedure;
  UFO: TUFO);
begin
  fUFOs[NewMissionUFO(UFO)].OnDiscovered:=script_utils_GetFunctionName(fScriptRunTime,DiscoveredEvent);
end;

procedure TMission.UFODiscovered(Sender: TObject);
var
  MUFO: Integer;
begin
  MUFO:=GetMissionUFO((Sender as TUFO).ID);
  if MUFO=-1 then
    exit;

  if ufo_api_GetUFO(fUFOs[MUFO].UFOID)<>nil then
  begin
    fUFOs[MUFO].Delete:=true;
    CallEvent(fUFOs[MUFO].OnDiscovered,[ufo_api_GetUFO(fUFOs[MUFO].UFOID)]);
  end;
end;

procedure TMission.ReadDataFromScriptVars;
begin
  fWinMode:=TWinMode(VGetInt(fScriptRunTime.GetVar2('MissionType')));
  fMissionName:=VGetString(fScriptRunTime.GetVar2('MissionName'));
end;

{ TEvent }

constructor TEvent.Create(Event: String; Interval: Integer);
begin
  fSkriptEvent:=Event;
  fInterval:=Interval;
  fID:=random(High(Cardinal)-1)+1;
  fMinutesDone:=0;
  fAktiv:=true;

  if EventList=nil then
    EventList:=TList.Create;

  EventList.Add(Self);
end;

destructor TEvent.Destroy;
begin
  EventList.Remove(Self);
  inherited;
end;

procedure TEvent.DoInterval(Minutes: Integer);
begin
  if fAktiv then
  begin
    inc(fMinutesDone,Minutes);
    if fMinutesDone>=fInterval then
    begin
      if Assigned(fOnEvent) then
        fOnEvent(fSkriptEvent,[]);

      if not fRepeated then
        fAktiv:=false
      else
        dec(fMinutesDone,fInterval);
    end;
  end;
end;

function TEvent.GenerateExtRecord: TExtRecord;
begin
  result:=TExtRecord.Create(RecordMissionEvent);
  result.SetCardinal('ID',fID);
  result.SetBoolean('Aktiv',fAktiv);
  result.SetString('SkriptEvent',fSkriptEvent);
  result.SetInteger('Interval',fInterval);
  result.SetBoolean('Repeated',fRepeated);
  result.SetInteger('MinutesDone',fMinutesDone);
end;

procedure TEvent.ReadFromRecord(Rec: TExtRecord);
begin
  Assert(Rec.RecordDefinition=RecordMissionEvent);

  fID:=Rec.GetCardinal('ID');
  fAktiv:=Rec.GetBoolean('Aktiv');
  fSkriptEvent:=Rec.GetString('SkriptEvent');
  fInterval:=Rec.GetInteger('Interval');
  fRepeated:=Rec.GetBoolean('Repeated');
  fMinutesDone:=Rec.GetInteger('MinutesDone');
end;

class function TEvent.ReadFromScriptString(ObjStr: String): TObject;
var
  I    : Int64;
  ID   : Cardinal;
  Dummy: Integer;
begin
  I:=StrToInt64(ObjStr);
  ID:=I;

  for Dummy:=0 to EventList.Count-1 do
  begin
    if TEvent(EventList[Dummy]).fID=ID then
    begin
      result:=EventList[Dummy];
      exit;
    end;
  end;
end;

function TEvent.WriteToScriptString: String;
var
  I: Int64;
begin
  I:=fID;
  result:=SysUtils.IntToStr(I);
end;

initialization

finalization
  if EventList<>nil then
    EventList.Free;

  FreeAndNil(TMissionListRecord);

  FreeAndNil(TMissionRecord);
end.
