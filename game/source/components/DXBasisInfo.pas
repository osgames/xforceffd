{********************************************************************************
*										*
* Copyright (C) 2004  X-Force Team - jim_raynor@web.de				*
*										*
* This file is part of X-Force: Fight For Destiny (X-Force).			*
*										*
* X-Force is free software; you can redistribute it and/or modify		*
* it under the terms of the GNU General Public License as published by		*
* the Free Software Foundation; either version 2 of the License, or		*
* (at your option) any later version.						*
*										*
* X-Force is distributed in the hope that it will be useful,			*
* but WITHOUT ANY WARRANTY; without even the implied warranty of		*
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the			*
* GNU General Public License for more details.					*
*										*
* You should have received a copy of the GNU General Public License		*
* along with X-Force; if not, write to the Free Software			*
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA	*
*										*
*********************************************************************************
*										*
* Zeigt generelle Informationen (Gehälter, Basiskosten) in der Ansicht Basis-	*
* informationen an								*
*										*
********************************************************************************}

{$I ../Settings.inc}

unit DXBasisInfo;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  DXContainer, DXDraws, KD4SaveGame, Blending, XForce_types, StringConst, Defines,
  DirectDraw, DirectFont;

type
  TDXBasisInfo = class(TDXComponent)
  private
    fBorderColor  : TColor;
    fDXBorderCol  : Cardinal;
    fCorners      : TRoundCorners;
    fCorner       : TCorners;
    procedure SetBorderColor(const Value: TColor);
    procedure SetCorners(const Value: TRoundCorners);
    function GetFrameRect: TRect;
    procedure Clear;
    { Private-Deklarationen }
  protected
    fBasisKost  : Integer;
    fSolGehalt  : Integer;
    fWisGehalt  : Integer;
    fTecGehalt  : Integer;
    fRauKosten  : Integer;
    fSolCount   : Integer;
    fForCount   : Integer;
    fForFaehig  : Integer;
    fTecCount   : Integer;
    fTecFaehig  : Integer;
    fEinCount   : Integer;
    fAlphatron  : double;
    fGesamt     : Integer;
    procedure DrawCaption(var DrawTop: Integer;Caption: String;Surface: TDirectDrawSurface);
    procedure DrawEntry(var DrawTop: Integer;Text1,Text2: String;Surface: TDirectDrawSurface);
    procedure DrawLine(var DrawTop: Integer; Surface: TDirectDrawSurface; var Mem: TDDSurfaceDesc);
    { Protected-Deklarationen }
  public
    { Public-Deklarationen }
    constructor Create(Page: TDXPage);override;
    procedure Draw(Surface: TDirectDrawSurface;var Mem: TDDSurfaceDesc);override;
    procedure Update;
    property BorderColor    : TColor read fBorderColor write SetBorderColor;
    property RoundCorners   : TRoundCorners read fCorners write SetCorners;
  end;

implementation

uses
  basis_api, soldaten_api, forsch_api, werkstatt_api, raumschiff_api;

{ TDXBasisInfo }

procedure TDXBasisInfo.Clear;
begin
  fBasisKost:=0;
  fSolGehalt:=0;
  fWisGehalt:=0;
  fTecGehalt:=0;
  fRauKosten:=0;
  fSolCount:=0;
  fForCount:=0;
  fForFaehig:=0;
  fTecCount:=0;
  fTecFaehig:=0;
  fEinCount:=0;
  fGesamt:=0;
  fAlphatron:=0;
end;

constructor TDXBasisInfo.Create(Page: TDXPage);
begin
  inherited;
  Clear;
end;

procedure TDXBasisInfo.Draw(Surface: TDirectDrawSurface;var Mem: TDDSurfaceDesc);
var
  TempRect : TRect;
  DrawTop  : Integer;
begin
  BlendRoundRect(ClientRect,125,bcMaroon,Surface,Mem,8,fCorner,ClientRect);

  DrawTop:=Top;
  DrawCaption(DrawTop,LBMonthKost,Surface);
  DrawEntry(DrawTop,LBBasisKost,Format(FCredits,[fBasisKost/1]),Surface);
  DrawEntry(DrawTop,LSolGehalt,Format(FCredits,[fSolGehalt/1]),Surface);
  DrawEntry(DrawTop,LForGehalt,Format(FCredits,[fwisGehalt/1]),Surface);
  DrawEntry(DrawTop,LHerGehalt,Format(FCredits,[fTecGehalt/1]),Surface);
  DrawEntry(DrawTop,LRauBau,Format(FCredits,[fRauKosten/1]),Surface);

  DrawLine(DrawTop,Surface,Mem);
  DrawEntry(DrawTop,LBMonthKost,Format(FCredits,[fGesamt/1]),Surface);
  DrawCaption(DrawTop,LBasisInfos,Surface);
  DrawEntry(DrawTop,IASoldaten,Format(FInteger,[fSolCount]),Surface);
  DrawEntry(DrawTop,LWissenschaf,Format(FIntFahig,[fForCount,fForFaehig/1]),Surface);
  DrawEntry(DrawTop,IATechniker,Format(FIntFahig,[fTecCount,fTecFaehig/1]),Surface);
  DrawEntry(DrawTop,LEinrichtungen,Format(FInteger,[fEinCount]),Surface);
  DrawEntry(DrawTop,LMiningAlphatron, Format(FDezimal2, [fAlphatron]), Surface);
end;

procedure TDXBasisInfo.DrawCaption(var DrawTop: Integer; Caption: String;
  Surface: TDirectDrawSurface);
var
  ClipRect : TRect;
  TempRect : TRect;
  Region   : HRGN;
begin
  ClipRect:=Rect(Left,DrawTop,Right-1,DrawTop+23);
  if AlphaElements then
    BlendFrameRoundRect(GetFrameRect,150,bcMaroon,Surface,21,ClipRect,ClipRect,fBorderColor)
  else
  begin
    TempRect:=GetFrameRect;
    Region:=CreateRoundRectRgn(TempRect.Left,TempRect.Top,TempRect.Right,TempRect.Bottom,20,22);
    FrameRegion(Region,ClipRect,fBorderColor,Surface);
    DeleteObject(Region);
  end;
  WhiteBStdFont.Draw(Surface,Left+6,DrawTop+4,Caption);
  inc(DrawTop,27);
end;

procedure TDXBasisInfo.DrawEntry(var DrawTop: Integer; Text1,
  Text2: String;Surface: TDirectDrawSurface);
begin
  YellowStdFont.Draw(Surface,Left+8,DrawTop,Text1);
  WhiteStdFont.Draw(Surface,Right-8-WhiteStdFont.TextWidth(Text2),DrawTop,Text2);
  Inc(DrawTop,20);
end;

procedure TDXBasisInfo.DrawLine(var DrawTop: Integer; Surface: TDirectDrawSurface;
                                var Mem: TDDSurfaceDesc);
begin
  HLine(Surface,Mem,Left+1,Right-2,DrawTop, fDXBorderCol);
  inc(DrawTop,5);
end;

function TDXBasisInfo.GetFrameRect: TRect;
begin
  result:=ClientRect;
  if fCorners in [rcLeft,rcLeftTop,rcLeftBottom,rcNone] then inc(result.Right,20);
  if fCorners in [rcTop,rcLeftTop,rcRightTop,rcNone] then inc(result.Bottom,20);
  if fCorners in [rcRight,rcRightTop,rcRightBottom,rcNone] then dec(result.Left,20);
  if fCorners in [rcBottom,rcLeftBottom,rcRightBottom,rcNone] then dec(result.Top,20);
end;

procedure TDXBasisInfo.SetBorderColor(const Value: TColor);
begin
  fBorderColor := Value;
  fDXBorderCol:=Container.Primary.ColorMatch(Value);
  Redraw;
end;

procedure TDXBasisInfo.SetCorners(const Value: TRoundCorners);
begin
  fCorners := Value;
  fCorner:=[];
  if fCorners in [rcLeft,rcTop,rcLeftTop,rcAll] then include(fCorner,cLeftTop);
  if fCorners in [rcRight,rcTop,rcRightTop,rcAll] then include(fCorner,cRightTop);
  if fCorners in [rcLeft,rcBottom,rcLeftBottom,rcAll] then include(fCorner,cLeftBottom);
  if fCorners in [rcRight,rcBottom,rcRightBottom,rcAll] then include(fCorner,cRightBottom);
  RecreateMouseRegion;
  Redraw;
end;

procedure TDXBasisInfo.Update;
var
  Dummy  : Integer;
begin
  Clear;
  fBasisKost:=basis_api_GetWeeklyKost;
  fGesamt:=0;

  // Informationen zu Soldaten ermitteln
  fSolGehalt:=soldaten_api_GetWeeklySold;
  fSolCount:=soldaten_api_GetSoldatenCount;

  // Informationen zu Wissenschaftler ermitteln
  fForCount:=forsch_api_GetForscherCount;
  fWisGehalt:=forsch_api_GetWeeklySold;
  fForFaehig:=forsch_api_GetAllStrength;

  // Informationen zu Technikern
  fTecCount:=werkstatt_api_GetTechnikerCount;
  fTecGehalt:=werkstatt_api_GetWeeklySold;
  fTecFaehig:=werkstatt_api_GetAllStrength;

  // Informationen zu den Basen ermitteln
  fEinCount:=0;
  for Dummy:=0 to basis_api_GetBasisCount-1 do
    inc(fEinCount,basis_api_GetBasisFromIndex(Dummy).Count);

  // Informationen zu Raumschiffen ermitteln
  fRauKosten:=raumschiff_api_GetWeeklyCost;

  // Gesamtkosten berechnen
  inc(fGesamt,fRauKosten);
  inc(fGesamt,fTecGehalt);
  inc(fGesamt,fWisGehalt);
  inc(fGesamt,fSolGehalt);
  inc(fGesamt,fBasisKost);

  // Alphatronförderung pro Stunde (max)
  fAlphatron:=basis_api_GetAlphatronperHour;
end;

end.
