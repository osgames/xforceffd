{********************************************************************************
*										*
* Copyright (C) 2004  X-Force Team - jim_raynor@web.de				*
*										*
* This file is part of X-Force: Fight For Destiny (X-Force).			*
*										*
* X-Force is free software; you can redistribute it and/or modify		*
* it under the terms of the GNU General Public License as published by		*
* the Free Software Foundation; either version 2 of the License, or		*
* (at your option) any later version.						*
*										*
* X-Force is distributed in the hope that it will be useful,			*
* but WITHOUT ANY WARRANTY; without even the implied warranty of		*
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the			*
* GNU General Public License for more details.					*
*										*
* You should have received a copy of the GNU General Public License		*
* along with X-Force; if not, write to the Free Software			*
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA	*
*										*
*********************************************************************************
*										*
* TWaffenObject bildet die Basisklasse f�r Projektile im Bodeneinsatz		*
*										*
********************************************************************************}

{$I ../Settings.inc}

unit ISOWaffenObjects;

interface

uses
  Classes, Windows, DXISOEngine, GameFigure, DXDraws, XForce_types, ISOTools, SysUtils,
  Blending, DirectDraw, ISOMessages, KD4Utils, math, TraceFile,DirectFont,
  NGTypes, AGFUnit, Graphics, Defines;

type
  TWaffenObject = class(TISOObject)
  private
    fMoveOffX,fMoveOffY    : real;
    fMoveOffZ              : real;
    fOffSetX,fOffSetY      : real;
    fSpeed                 : Integer;
    fZAxis                 : double;
    fStrength              : Integer;
    fPower                 : Integer;
    fWasSeen               : Boolean;
    fWinkel                : Integer;
    fCountTreffer          : Boolean;

    fZielPos               : TPoint;
    procedure AktuObjectRect;
    procedure TestPos;
  protected
    fExplosionSize         : Integer;

    function PerformFrame: boolean;override;
    procedure GetBoundRect(var Rect: TRect);virtual;

    function FlyThroughWall(NeedPower: Integer): boolean;virtual;

    procedure CalculateFlyingWay(From, Ziel: TPoint; Infos: TWaffenParamInfos);virtual;
    procedure CalculateZOffSetMove(From, Ziel: TPoint; TargetHeight: Integer);

    function DoExplode: Boolean;virtual;

  public
    constructor Create(Engine: TDXISOEngine);override;
    procedure SetParams(From: TPoint; Ziel: TPoint; ShootInfos: TWaffenParamInfos;Status: TFigureStatus;Schuetze: TGameFigure);virtual;
    procedure SetWaffenParams(Strength: Integer;Power: Integer);
    procedure SetZAxis(Z: Integer);virtual;

    procedure SetExplosionSize(Size: Integer);

    procedure CountTreffer;

    function GetAttackStrength: Integer;override;
    function GetAttackPower   : Integer;override;
    function GetSchadensTyp   : TSchadensTyp;override;
    procedure GetPositionVektor(out X,Y,Z: Integer);override;

    property WasSeen          : Boolean read fWasSeen write fWasSeen;
  end;

  TLaserWaffe = class(TWaffenObject)
  private
  protected
    function FlyThroughWall(NeedPower: Integer): boolean;override;
  public
    procedure Draw(Surface : TDirectDrawSurface;X,Y: Integer);override;
  end;

  TShortRangeWaffe = class(TWaffenObject)
  private
    fZiel: TPoint;
  protected
    function PerformFrame: boolean;override;
  public
    procedure SetParams(From: TPoint; Ziel: TPoint; ShootInfos: TWaffenParamInfos;Status: TFigureStatus;Schuetze: TGameFigure);override;
  end;

  TRaketenWaffe = class(TWaffenObject)
  private
    fImage : TDirectDrawSurface;
  protected
    function PerformFrame: boolean;override;

    function FlyThroughWall(NeedPower: Integer): boolean;override;

    procedure CalculateFlyingWay(From, Ziel: TPoint; Infos: TWaffenParamInfos);override;
  public
    constructor Create(Engine: TDXISOEngine);override;
    procedure Draw(Surface : TDirectDrawSurface;X,Y: Integer);override;
  end;

  TRaketenSchweif = class(TISOObject)
  private
    fImage : TAGFImage;
    fFrame : Integer;
    fOffSet: TPoint;
  protected
    function PerformFrame: boolean;override;
  public
    constructor Create(Engine: TDXISOEngine);override;
    procedure Draw(Surface : TDirectDrawSurface;X,Y: Integer);override;

    procedure SetOffSet(OffSet: TPoint);
  end;

const
  RocketRect : TRect = (Left: 30;Top: 0;Right: 39;Bottom: 17);
  LaserRect  : TRect = (Left: 43;Top: 0;Right: 46;Bottom: 17);

implementation

{ TWaffenObject }

procedure TWaffenObject.AktuObjectRect;
begin
  Rec.Point:=Point(X,Y);
  SendVMessage(vmGetTileRect);
  SendVMessage(vmAktuTempSurfaceRect);
end;

procedure TWaffenObject.CalculateFlyingWay(From, Ziel: TPoint; Infos: TWaffenParamInfos);
var
  Winkel         : Integer;
  Rad            : double;
  Abweichung     : Integer;
  ToPos,FromPos  : TPoint;
  FlyWay         : Integer;
begin
  // Flugwinkel berechnen (-135 weil die Winkel in die entsprechende Ansicht gedreht werden m�ssen)
  Winkel:=CalculateWinkel(Point(X,Y),Ziel)-135;

  FromPos:=Engine.GetTileRect(Self,From).TopLeft;
  ToPos:=Engine.GetTileRect(Self,Ziel).TopLeft;

  FlyWay:=CalculateEntfern(FromPos,ToPos);

  // Abweichen, wenn der Schuss daneben gehen soll.
  if not RandomChance(Infos.Treff) then
  begin
//    Abweichung:=random(random((120-Infos.Treff) div 4));
    Abweichung:=round(RadToDeg((15/FlyWay)+((((100-Infos.Treff) div 4)/FlyWay)*random)));
  end
  else
  begin
    Abweichung:=round(RadToDeg(1/FlyWay)*random);
  end;

  if random(200)>100 then
    Abweichung:=-Abweichung;

  inc(Winkel,Abweichung);

  Rad:=DegToRad(Winkel);

  // Fluggeschwindigkeit berechnen
  fMoveOffX:=sin(Rad);
  fMoveOffY:=-cos(Rad)*0.5;

  // Winkel auf der Karte ermitteln, dazu muss die Absolute Position vom Ausgangs-
  // punkt und von Zielpunkt genommen werden. Zielpunkt ist dabei ein Beliebiger
  // Punkt auf der Flugbahn

  Assert(Engine<>nil);
  FromPos:=Engine.GetTileRect(Self,From).TopLeft;
  ToPos:=Point(round(FromPos.X+(fMoveOffX*100)),round(FromPos.Y+(fMoveOffY*50)));

  fWinkel:=(CalculateWinkel(ToPos,FromPos)*256 div 360);

  CalculateZOffSetMove(From,Ziel,Infos.TargetHeight);
end;

procedure TWaffenObject.CalculateZOffSetMove(From, Ziel: TPoint;
  TargetHeight: Integer);
var
  FromPos     : TPoint;
  ToPos       : TPoint;
  FlyWay      : double;
  pixperMove  : double;
begin
  // Z-Offset so berechnen, dass das Projektil im Ziel auf den Boden trifft
  // und dadurch zerst�rt wird

  FromPos:=Engine.GetTileRect(Self,Position).TopLeft;
  ToPos:=Engine.GetTileRect(Self,fZielPos).TopLeft;

  FlyWay:=CalculateEntfern(FromPos,ToPos);

  pixperMove:=Sqrt(sqr(fMoveOffX)+sqr(fMoveOffY));

  fMoveOffZ:=(TargetHeight-fZAxis)*(pixperMove/FlyWay);
end;

procedure TWaffenObject.CountTreffer;
begin
  if not fCountTreffer then
  begin
    // F�r die Statistik: Eigene Treffer z�hlen
    if (fFigure<>nil) and (fFigure.FigureStatus=fsFriendly)  then
    begin
      SendVMessage(vmFriendTreffer);
      fCountTreffer:=true;
    end;
  end;
end;

constructor TWaffenObject.Create(Engine: TDXISOEngine);
begin
  inherited;
  fWasSeen:=false;
  AktuObjectRect;
  fExplosionSize:=0;
  fCountTreffer:=false;
  // Erzwingt, dass Bodeneinsatz solange weitergeht, bis letztes Projektil
  // verschwunden ist
  BlockEndGame:=true;

  {$IFDEF SHOOTDEBUG}
  fSpeed:=1;
  {$ELSE}
  fSpeed:=25;
  {$ENDIF}
end;

function TWaffenObject.DoExplode: boolean;
begin
  if fExplosionSize=0 then
    result:=false
  else
  begin
    Engine.CreateExplosion(Figure,Position,fExplosionSize,GetAttackStrength,GetAttackPower);
    result:=true;
  end;
  // Projektil trifft auf ein Hindernis
end;

function TWaffenObject.FlyThroughWall(NeedPower: Integer): boolean;
begin
  // Das Projektil kann nicht durch die Mauer fliegen und wird zerst�rt
  DoExplode;
  fDeleted:=true;
  result:=true;
end;

function TWaffenObject.GetAttackPower: Integer;
begin
  result:=fPower;
end;

function TWaffenObject.GetAttackStrength: Integer;
begin
  result:=fStrength;
end;

procedure TWaffenObject.GetBoundRect(var Rect: TRect);
begin
  rec.Point:=Position;
  SendVMessage(vmGetTileRect);
  Rect:=rec.Rect;
end;

procedure TWaffenObject.GetPositionVektor(out X, Y, Z: Integer);
begin
  X:=round(fOffSetX);
  Y:=round(fOffSetY);
  Z:=round(fZAxis);
end;

function TWaffenObject.GetSchadensTyp: TSchadensTyp;
begin
  result:=stLokal;
end;

function TWaffenObject.PerformFrame: boolean;
var
  NewRect   : TRect;
  OldRect   : TRect;
  Dummy     : Integer;
begin
  GetBoundRect(OldRect);

  for Dummy:=1 to fSpeed do
  begin
    if fDeleted then
      break;

    fOffSetX:=fOffSetX+fMoveOffX;
    fOffSetY:=fOffSetY+fMoveOffY;
    fZAxis:=fZAxis+fMoveOffZ;

    // Position anpassen
    TestPos;
  end;

  GetBoundRect(NewRect);

  // Neu zeichnen
  UnionRect(Rec.Rect,OldRect,NewRect);
  InflateRect(Rec.Rect,fSpeed*2,fSpeed*2);
  SendVMessage(vmAktuTempSurfaceRect);
  result:=true;
end;

procedure TWaffenObject.SetExplosionSize(Size: Integer);
begin
  fExplosionSize:=Size;
end;

procedure TWaffenObject.SetParams(From, Ziel: TPoint; ShootInfos: TWaffenParamInfos;Status: TFigureStatus;Schuetze: TGameFigure);
begin
  SetPos(From);

  SetZAxis(ShootInfos.ZAxisStart);

  SetWaffenParams(ShootInfos.Strength,ShootInfos.Power);

  fZielPos:=Ziel;

  fOffSetX:=HalfTileWidth;
  fOffSetY:=HalfTileHeight;

  fFigure:=Schuetze;

  CalculateFlyingWay(From,Ziel,ShootInfos);
end;

procedure TWaffenObject.SetWaffenParams(Strength: Integer; Power: Integer);
begin
  fStrength:=Strength;
  fPower:=Power;
end;

procedure TWaffenObject.SetZAxis(Z: Integer);
begin
  fZAxis:=Z;
end;

procedure TWaffenObject.TestPos;
var
  NewTile  : TPoint;
  NewOff   : TFloatPoint;

  Bloc     : TWayFreeResult;
  Figure   : TGameFigure;
  NeedPower: Integer;
begin
  if (fZAxis<0) or (fZAxis>ObjektHeight) then
  begin
    // Projektil schl�gt auf dem Boden ein
    if (fZAxis<0) then
      DoExplode;
      
    fDeleted:=true;
    exit;
  end;

  NewTile:=CheckOffset(FloatPoint(fOffSetX,fOffSetY),NewOff);
  if (NewTile.Y<>Y) or (NewTile.X<>X) then
  begin

    ISOMap.IsWayFree(Point(X,Y),NewTile,Bloc);

    if (Bloc.WayBlocked=wbObject) or (Bloc.WayBlocked in [wbWall,wbTransparentWall,wbDoor]) then
    begin
      // Pr�fen, ob das Projektil die Mauer durchschl�gt
      NeedPower:=CheckWay(NewOff,round(fZAxis),NewTile,1,fStrength);

      if NeedPower>0 then
      begin
        if FlyThroughWall(NeedPower) then
          exit;
      end;
    end;
    SetPos(NewTile);

    // Projektil hat das Spielfeld verlassen
    if fDeleted then
      exit;

    fOffSetX:=NewOff.X;
    fOffSetY:=NewOff.Y;

    Visible:=ISOMap.TileEntry(X,Y).AlphaV.All>1;
    if (not fWasSeen) then
    begin
      Rec.Figure:=Self;
      SendVMessage(vmWaffePosChanged);
    end;
    if fFreed then exit;
  end;


  if not fDeleted then
  begin
    Figure:=ISOMap.Figures[X,Y].Figure;
    if (Figure<>nil) then
    begin
      if (Figure.FigureStatus<>Self.Figure.FigureStatus) and (not Figure.IsDead) then
      begin
        if Figure.GetTrefferZone(Self)<>tzNone then
        begin
          Assert(Figure<>nil,'Figure nicht angegeben');

          // DoExplode erzeugt die Explosion beim Raketenwerfen und gibt true
          // zur�ck. Durch diese Explosion erh�lt der Gegner bereits den Schaden
          // und so muss nicht DoTreffer aufgerufen werden
          if not DoExplode then
            Figure.DoTreffer(Self);

          CountTreffer;
          fDeleted:=true;
        end;
      end;
    end;
  end;
end;

{ TRaketenWaffe }

procedure TRaketenWaffe.CalculateFlyingWay(From, Ziel: TPoint; Infos: TWaffenParamInfos);
begin
  inherited;

  CalculateZOffSetMove(From,Ziel,0);
end;

constructor TRaketenWaffe.Create(Engine: TDXISOEngine);
begin
  inherited;
  fImage:=Engine.GetSurface('Symbole');
end;

procedure TRaketenWaffe.Draw(Surface: TDirectDrawSurface; X, Y: Integer);
begin
  assert(fImage<>nil);
  Surface.DrawRotate(X+round(fOffSetX),Y+round(fOffSetY)+39-round(fZAxis),10,17,RocketRect,fImage,0.5,1,true,fWinkel);
//  Surface.DrawRotate(X+round(fOffSetX),Y+round(fOffSetY)+39-fZAxis,3,40,LaserRect,fImage,0.5,0.5,true,fWinkel);
//  Surface.FillRect(Bounds(-fZAxis,5,5),bcBlack);
//  Surface.Draw(X+round(fOffSetX),Y+round(fOffSetY)+39,fImage);
end;

function TRaketenWaffe.FlyThroughWall(NeedPower: Integer): boolean;
begin
  // Raketen brauchen doppelt soviel kraft um eine Mauer zu durchschlagen.
  // Dadurch wird verhindert, dass Raketen durch Beton oder Stahlmauern fliegen
  // k�nnen
  if NeedPower*2>fPower then
    result:=inherited FlyThroughWall(NeedPower)
  else
    result:=false;
end;

function TRaketenWaffe.PerformFrame: boolean;
begin
  result:=inherited PerformFrame;

  // Waffenobjekt ist immer noch unterwegs
  if not fDeleted then
  begin
    with TRaketenSchweif.Create(Engine) do
    begin
      SetPos(Self.X,Self.Y);
      SetOffSet(point(round(fOffSetX),round(fOffSetY)+39-round(fZAxis)));
    end;
  end;
end;

{ TRaketenSchweif }

constructor TRaketenSchweif.Create(Engine: TDXISOEngine);
begin
  inherited;
  fImage:=Engine.GetAGFImage('RaketenSchweif');
  fFrame:=0;
end;

procedure TRaketenSchweif.Draw(Surface: TDirectDrawSurface; X, Y: Integer);
begin
  Assert(Engine.Container.PowerDraw.Lock(Surface),'Surface wurde nicht gelockt');
  Engine.Container.PowerDraw.RenderAdd(fImage,X+fOffSet.X,Y+fOffSet.Y,fFrame);
  Engine.Container.PowerDraw.UnLock;
end;

function TRaketenSchweif.PerformFrame: boolean;
begin
  inc(fFrame);
  if fFrame>=fImage.Pattern then
  begin
    Deleted:=true;
    result:=false;
  end
  else
  begin
    Rec.Point:=Point(X,Y);
    SendVMessage(vmGetTileRect);
    SendVMessage(vmAktuTempSurfaceRect);
    result:=true;
  end;
end;

procedure TRaketenSchweif.SetOffSet(OffSet: TPoint);
begin
  fOffSet:=OffSet;
  dec(fOffSet.X,fImage.PatternWidth div 2);
  dec(fOffSet.Y,fImage.PatternHeight div 2);
end;

{ TProjektilWaffe }

procedure TLaserWaffe.Draw(Surface: TDirectDrawSurface; X, Y: Integer);
var
  Mem: TDDSurfaceDesc;
begin
  Surface.Lock(Mem);
  {$IFDEF SHOOTDEBUG}
  AALine(Surface,Mem,X+round(fOffSetX),Y+39+round(fOffSetY)-round(fZAxis),X+round(fOffSetX)-round(fMoveOffX*(fSpeed*20)),Y+39-round(fZAxis)+round(fOffSetY)-round(fMoveOffY*(fSpeed*20)),bcBlue);
  {$ELSE}
  AALine(Surface,Mem,X+round(fOffSetX),Y+39+round(fOffSetY)-round(fZAxis),X+round(fOffSetX)-round(fMoveOffX*(fSpeed/2)),Y+39-round(fZAxis)+round(fOffSetY)-round(fMoveOffY*(fSpeed/2)),bcBlue);
  {$ENDIF}
  Surface.Unlock;
end;

{ TNahKampfWaffe }

function TShortRangeWaffe.PerformFrame: boolean;
begin
  SetPos(fZiel);
  TestPos;
  
  // L�schen, falls Keine Einheit auf dem Feld steht
  Deleted:=true;
  result:=true;
end;

procedure TShortRangeWaffe.SetParams(From, Ziel: TPoint;  ShootInfos: TWaffenParamInfos;
  Status: TFigureStatus; Schuetze: TGameFigure);
begin
  inherited;
  fZiel:=Ziel;
end;

function TLaserWaffe.FlyThroughWall(NeedPower: Integer): boolean;
begin
  dec(fPower,NeedPower);
  if fPower<=0 then
    // Projektil kann die Mauer nicht durchschlagen
    result:=inherited FlyThroughWall(NeedPower)
  else
    result:=false;
end;

end.
