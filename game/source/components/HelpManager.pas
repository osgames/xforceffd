{********************************************************************************
*										*
* Copyright (C) 2004  X-Force Team - jim_raynor@web.de				*
*										*
* This file is part of X-Force: Fight For Destiny (X-Force).			*
*										*
* X-Force is free software; you can redistribute it and/or modify		*
* it under the terms of the GNU General Public License as published by		*
* the Free Software Foundation; either version 2 of the License, or		*
* (at your option) any later version.						*
*										*
* X-Force is distributed in the hope that it will be useful,			*
* but WITHOUT ANY WARRANTY; without even the implied warranty of		*
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the			*
* GNU General Public License for more details.					*
*										*
* You should have received a copy of the GNU General Public License		*
* along with X-Force; if not, write to the Free Software			*
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA	*
*										*
*********************************************************************************
*										*
* Verwaltet die Tipps und Tricks auf der Startseite				*
*										*
********************************************************************************}

{$I ../Settings.inc}

unit HelpManager;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Sequenzer;

type

  TTipp = record
    Kategorie : String;
    Titel     : String;
    Tipp      : String;
  end;

  THelpManager = class(TObject)
  private
    fSequenzer : TSequenzer;
    fNumber    : Integer;
    fKategorie : String;

    function ReadString(Stream: TStream): String;
    procedure WriteString(Stream: TStream; Str: String);
    function GetPTippArray(Index: Integer): TTipp;
    function GetPTippAnzahl: Integer;

  protected
    TippArray : Array of TTipp;

  public
    constructor Create;
    destructor Destroy;override;

    procedure Save(SaveStream: TStream);
    procedure Load(LoadStream: TStream);
    procedure LoadFromTextFile(FileName: TFileName);
    procedure Delete(Index: Integer);

    function New(NTipp: TTipp): TTipp;
    function Change(Bearbeiten: TTipp; Index: Integer): TTipp;
    function RandomTipp: String;
    function NextTipp: String;
    function LastTipp: String;

    property PTippArray[Index: Integer]: TTipp read GetPTippArray;
    property PTippAnzahl : Integer read GetPTippAnzahl;
    property Kategorie : String read fKategorie write fKategorie;
  published
  end;

implementation

uses
  ReadLanguage, UnicodeHelper;

const
  SPLITTER = '|';

{ THelpManager }

function THelpManager.Change(Bearbeiten: TTipp; Index: Integer): TTipp;
begin
  TippArray[Index]:=Bearbeiten;
end;

procedure THelpManager.Delete(Index: Integer);
var
  Zaehler : Integer;
begin
  for Zaehler:=Index to High(TippArray)-1 do
  begin
    TippArray[Zaehler]:=TippArray[Zaehler+1];
  end;
  SetLength(TippArray, Length(TippArray) - 1)
end;

procedure THelpManager.Load(LoadStream: TStream);
var
  Zaehler  : Integer;
  Count    : Integer;
begin
  LoadStream.Read(Count,sizeof(Integer));
  SetLength(TippArray,Count);

  for Zaehler:=0 to High(TippArray) do
  begin
    with TippArray[Zaehler] do
    begin
      Kategorie:=ReadString(LoadStream);
      Titel:=ReadString(LoadStream);
      Tipp:=ReadString(LoadStream);
    end;
  end;

  fSequenzer.SetHighest(PTippAnzahl-1);
end;

procedure THelpManager.LoadFromTextFile(FileName: TFileName);
var
  i: Integer;
  SplitterPosition: Integer;
  StringList: TStringList;
begin
  StringList := TStringList.Create;
  try
    FileToStringList(FileName, StringList);
    SetLength(TippArray, StringList.Count);
    
    for i := 0 to StringList.Count - 1 do begin
      SplitterPosition := Pos(SPLITTER, StringList.Names[i]);
      TippArray[i].Kategorie := Copy(StringList.Names[i], 1, SplitterPosition-1);
      TippArray[i].Titel := Copy(StringList.Names[i], SplitterPosition+1, MaxInt);
      TippArray[i].Tipp := ReplaceTags(Copy(StringList[i], Length(StringList.Names[i])+2, MaxInt));
    end;

  finally
    StringList.Free;
  end;
  fSequenzer.SetHighest(PTippAnzahl-1);
end;

function THelpManager.New(NTipp: TTipp): TTipp;
begin
  SetLength(TippArray,Length(TippArray)+1);
  TippArray[High(TippArray)]:=NTipp;
end;

procedure THelpManager.Save(SaveStream: TStream);
var
  Zaehler : Integer;
  Count   : Integer;
begin
  Count:=Length(TippArray);
  SaveStream.Write(Count,sizeof(Count));

  for Zaehler:=0 to High(TippArray) do
  begin
    WriteString(SaveStream,TippArray[Zaehler].Kategorie);
    WriteString(SaveStream,TippArray[Zaehler].Titel);
    WriteString(SaveStream,TippArray[Zaehler].Tipp);
  end;
end;

function THelpManager.ReadString(Stream: TStream): String;
var
  Len : Integer;
begin

  Stream.Read(Len,SizeOf(Len));
  SetString(result, nil, Len);
  Stream.Read(Pointer(result)^,Len);
end;

procedure THelpManager.WriteString(Stream: TStream; Str: String);
var
  Len : Integer;
begin
  Len:=Length(Str);
  Stream.Write(Len,SizeOf(Len));
  Stream.Write(Pointer(Str)^,Len);
end;


function THelpManager.GetPTippArray(Index: Integer): TTipp;
begin
  result:=TippArray[Index];
end;

function THelpManager.GetPTippAnzahl: Integer;
begin
  result:=Length(TippArray);
end;

function THelpManager.RandomTipp: String;
begin
  repeat
    fNumber:=fSequenzer.GetNumber;
  until Kategorie=TippArray[fNumber].Kategorie;
  result:=TippArray[fNumber].Tipp;
end;

function THelpManager.LastTipp: String;
begin
  repeat
    if fNumber=0 then
    begin
      fNumber:=PTippAnzahl-1;
    end
    else
    begin
      dec(fNumber);
    end;
  until Kategorie=TippArray[fNumber].Kategorie;
  result:=TippArray[fNumber].Tipp;
end;

function THelpManager.NextTipp: String;
begin
  repeat
    if fNumber=PTippAnzahl-1 then
    begin
      fNumber:=0;
    end
    else
    begin
      inc(fNumber);
    end;
  until Kategorie=TippArray[fNumber].Kategorie;
  result:=TippArray[fNumber].Tipp;
end;

constructor THelpManager.Create;
begin
  fSequenzer:=TSequenzer.Create;
end;

destructor THelpManager.Destroy;
begin
  fSequenzer.Free;
  inherited;
end;

end.
