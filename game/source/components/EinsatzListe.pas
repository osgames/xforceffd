{********************************************************************************
*										*
* Copyright (C) 2004  X-Force Team - jim_raynor@web.de				*
*										*
* This file is part of X-Force: Fight For Destiny (X-Force).			*
*										*
* X-Force is free software; you can redistribute it and/or modify		*
* it under the terms of the GNU General Public License as published by		*
* the Free Software Foundation; either version 2 of the License, or		*
* (at your option) any later version.						*
*										*
* X-Force is distributed in the hope that it will be useful,			*
* but WITHOUT ANY WARRANTY; without even the implied warranty of		*
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the			*
* GNU General Public License for more details.					*
*										*
* You should have received a copy of the GNU General Public License		*
* along with X-Force; if not, write to the Free Software			*
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA	*
*										*
*********************************************************************************
*										*
* Verwaltet die Bodeneinsätze im Spielsatz					*
*										*
********************************************************************************}

{$I ../Settings.inc}

unit EinsatzListe;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  XForce_types, KD4Utils, TraceFile, NotifyList, ExtRecord, OrganisationList,
  string_utils, StringConst;

const
  EVENT_EINSATZONWIN     = 0;
  EVENT_EINSATZONDESTROY = 1;
  EVENT_EINSATZONTIMEUP  = 2;

type
  {$M+}
  TEinsatz = class;
  {$M-}

  TEinsatzListe = class(TObject)
  private
    fList         : TList;
    fNumber       : Cardinal;
    fMapList      : TStringArray;
    function GetCount: Integer;
    function GetEinsatz(Index: Integer): TEinsatz;
    { Private-Deklarationen }
  protected
    procedure Clear;
    procedure AddEinsatz(Einsatz: TEinsatz);
    procedure DeleteEinsatz(Einsatz: TEinsatz);

    procedure NewGameHandler(Sender: TObject);

    procedure ReadMaps;
    function GetRandomMap: String;

    { Protected-Deklarationen }
  public
    constructor Create;
    destructor destroy;override;
    procedure LoadFromStream(Stream: TStream);
    procedure SaveToStream(Stream: TStream);

    function NewEinsatz(Pos: TFloatPoint): TEinsatz;overload;
    function NewEinsatz: TEinsatz;overload;

    procedure NextRound(Minuten: Integer);

    function IndexOfID(ID: Cardinal): TEinsatz;

    property Count                  : Integer read GetCount;
    property Einsatz[Index: Integer]: TEinsatz read GetEinsatz;default;
    { Public-Deklarationen }
  end;

  TEinsatz = class(TObject)
  private
    fList          : TEinsatzListe;
    fAliens        : Array of TAlien;
    fID            : Cardinal;
    fPosition      : TFloatPoint;
    fName          : String;
    fMAvaible      : Boolean;
    fNotifyList    : TNotifyList;
    fDescription   : String;
    fObjectives    : String;
    fRemainTime    : Integer;
    fMapName       : String;
    function GetAliens: Integer;
    function GetAlien(Index: Integer): TAlien;
    function GetCountry: Integer;
  protected
  public
    constructor Create(List: TEinsatzListe);
    destructor destroy;override;
    procedure LoadFromStream(Stream: TStream);
    procedure SaveToStream(Stream: TStream);
    procedure SetNewParams(Number: Integer;Pos: TFloatPoint);
    procedure AddAlien(const Alien: TAlien);overload;
    function CheckEinsatz: Boolean;
    procedure AktuAlienList;

    procedure NextMinute(Minuten: Integer);

    function GetMissionInfo: String;

    procedure Win;
    procedure TimeUp;

    function AddrOfAlien(Index: Integer): PAlien;
    procedure GenerateNewMap;

    property ID           : Cardinal read fID;
    property Alien[Index: Integer] : TAlien read GetAlien;
    property AlienCount   : Integer read GetAliens;
    property NotifyList   : TNotifyList read fNotifyList write fNotifyList;

  published
    function WriteToScriptString: String;
    class function ReadFromScriptString(ObjStr: String): TObject;

    procedure AddAlien(Alien: PAlien);overload;

    property Description  : String read fDescription write fDescription;
    property Objectives   : String read fObjectives write fObjectives;
    property Name         : String read fName write fName;
    property RemainTime   : Integer read fRemainTime write fRemainTime;
    property Country      : Integer read GetCountry;
    property MapName      : String read fMapName write fMapName;

    property Position     : TFloatPoint read fPosition write fPosition;
  end;

implementation

uses Loader, UFOList, alien_api, savegame_api, country_api, einsatz_api,
  game_utils, file_utils, registry, earth_api;

var
  TEinsatzListRecord : TExtRecordDefinition;
  TEinsatzRecord     : TExtRecordDefinition;

function ValidVersion(Version: Cardinal): boolean;
begin
  result:=false;
  case Version of
    $30F55EBB: result:=true;
    $74B9509D: result:=true;
    $26090196: result:=true;
    $19D7F5EB: result:=true;
    $423D056D: result:=true;
  end;
end;

{ TEinsatzListe }

procedure TEinsatzListe.AddEinsatz(Einsatz: TEinsatz);
begin
  if fList.IndexOf(Einsatz)=-1 then fList.Add(Einsatz);
end;

procedure TEinsatzListe.Clear;
begin
  while fList.Count>0 do
    TEinsatz(fList[0]).Free;

  // Ermitteln der Karten
  ReadMaps;
end;

constructor TEinsatzListe.Create;
begin
  einsatz_api_init(Self);

  fList:=TList.Create;

  savegame_api_RegisterCustomSaveHandler(SaveToStream,LoadFromStream,nil,$11A0AB2A);
  savegame_api_RegisterNewGameHandler(NewGameHandler);
end;

procedure TEinsatzListe.DeleteEinsatz(Einsatz: TEinsatz);
var
  Index: Integer;
begin
  Index:=fList.IndexOf(Einsatz);
  if Index<>-1 then fList.Delete(Index);
end;

destructor TEinsatzListe.destroy;
begin
  Clear;
  fList.Free;
  inherited;
end;

function TEinsatzListe.GetCount: Integer;
begin
  result:=fList.Count;
end;

function TEinsatzListe.GetEinsatz(Index: Integer): TEinsatz;
begin
  result:=nil;
  if (Index<0) or (Index>=Count) then
    exit;
  result:=TEinsatz(fList[Index]);
end;

function TEinsatzListe.GetRandomMap: String;
begin
  Assert(length(fMapList)>0,'Keine Karten gefunden (GetRandomMap)');
  result:=fMapList[random(length(fMapList))];
end;

function TEinsatzListe.IndexOfID(ID: Cardinal): TEinsatz;
var
  Dummy: Integer;
begin
  result:=nil;
  for Dummy:=0 to fList.Count-1 do
  begin
    if TEinsatz(fList[Dummy]).ID=ID then
    begin
      result:=TEinsatz(fList[Dummy]);
      exit;
    end;
  end;
end;

procedure TEinsatzListe.LoadFromStream(Stream: TStream);
var
  Count : Integer;
  Rec   : TExtRecord;
begin
  Clear;

  Rec:=TExtRecord.Create(TEinsatzListRecord);
  Rec.LoadFromStream(Stream);

  fNumber:=Rec.GetInteger('Number');
  Count:=Rec.GetInteger('EinsatzCount');

  Rec.Free;

  while (Count>0) do
  begin
    with TEinsatz.Create(Self) do
    begin
      LoadFromStream(Stream);
      if MapName='' then
        MapName:=GetRandomMap;
    end;

    dec(Count);
  end;
end;

function TEinsatzListe.NewEinsatz(Pos: TFloatPoint): TEinsatz;
begin
  result:=TEinsatz.Create(Self);
  result.SetNewParams(fNumber,Pos);

  result.MapName:=GetRandomMap;
  inc(fNumber);
end;

function TEinsatzListe.NewEinsatz: TEinsatz;
begin
  result:=NewEinsatz(earth_api_RandomEarthPoint(true));
end;

procedure TEinsatzListe.NewGameHandler(Sender: TObject);
begin
  Clear;
  fNumber:=1;
end;

procedure TEinsatzListe.NextRound(Minuten: Integer);
var
  Dummy: Integer;
begin
  for Dummy:=fList.Count-1 downto 0 do
    TEinsatz(fList[Dummy]).NextMinute(Minuten);
end;

procedure TEinsatzListe.ReadMaps;
begin
  fMapList:=file_utils_ReadDirectory('data\maps\','*.m3d',false);

//  Assert(length(fMapList)>0,'Keine Maps im maps Verzeichnis gefunden');
end;

procedure TEinsatzListe.SaveToStream(Stream: TStream);
var
  Dummy : Integer;
  Rec   : TExtRecord;
begin
  Rec:=TExtRecord.Create(TEinsatzListRecord);

  Rec.SetInteger('EinsatzCount',Count);
  Rec.SetInteger('Number',fNumber);

  Rec.SaveToStream(Stream);
  Rec.Free;

  for Dummy:=0 to fList.Count-1 do
    TEinsatz(fList[Dummy]).SaveToStream(Stream);
end;

{ TEinsatz }

procedure TEinsatz.AddAlien(Alien: PAlien);
begin
  SetLength(fAliens,AlienCount+1);
  fAliens[AlienCount-1]:=Alien^;
  fAliens[AlienCount-1].Info:='';
end;

procedure TEinsatz.AddAlien(const Alien: TAlien);
begin
  AddAlien(Addr(Alien));
end;

function TEinsatz.AddrOfAlien(Index: Integer): PAlien;
begin
  result:=Addr(fAliens[Index]);
end;

procedure TEinsatz.AktuAlienList;
var
  Dummy: Integer;
begin
  for Dummy:=High(fAliens) downto 0 do
  begin
    if fAliens[Dummy].Ges<=0 then
    begin
      if AlienCount>0 then
        fAliens[Dummy]:=fAliens[High(fAliens)];
      SetLength(fAliens,Length(fAliens)-1);
    end;
  end;
end;

function TEinsatz.CheckEinsatz: Boolean;
begin
  if AlienCount=0 then
  begin
    result:=false;
    Free;
    exit;
  end;

  result:=true;
  if not fMAvaible then
  begin
    fMAvaible:=true;
    savegame_api_Message(ST0311060002,lmEinsaetze,Self);
  end;
end;

constructor TEinsatz.Create(List: TEinsatzListe);
begin
  fList:=List;
  fList.AddEinsatz(Self);
  fMAvaible:=false;
  fNotifyList:=TNotifyList.Create;

  fDescription:=ClearMission;
  fObjectives:=LVernichtBastard;
end;

destructor TEinsatz.destroy;
begin
  fNotifyList.CallEvents(EVENT_EINSATZONDESTROY,Self);
  fNotifyList.Free;
  fList.DeleteEinsatz(Self);
  SetLength(fAliens,0);
  inherited;
end;

function TEinsatz.GetAlien(Index: Integer): TAlien;
begin
  result:=fAliens[Index];
end;

function TEinsatz.GetAliens: Integer;
begin
  result:=length(fAliens);
end;

function TEinsatz.GetCountry: Integer;
begin
  Result:=country_api_LandNrOverPos(fPosition);
end;

function TEinsatz.GetMissionInfo: String;
begin
  result:=fDescription+
          #10#10+Format(StringConst.AlienCount,[game_utils_IntToText(AlienCount)])+
          #10#10'<yellowbold>'+ST0312260001+#13#10'<yellow>'+fObjectives;
end;

procedure TEinsatz.LoadFromStream(Stream: TStream);
var
  Rec     : TExtRecord;
  Dummy   : Integer;
  ACount  : Integer;
begin
  Rec:=TExtRecord.Create(TEinsatzRecord);
  Rec.LoadFromStream(Stream);

  fName:=Rec.GetString('Name');
  fID:=Rec.GetCardinal('ID');
  fPosition.X:=Rec.GetDouble('PositionX');
  fPosition.Y:=Rec.GetDouble('PositionY');
  ACount:=Rec.GetInteger('AlienCount');
  Description:=Rec.GetString('Description');
  Objectives:=Rec.GetString('Objectives');
  fMapName:=Rec.GetString('MapName');
  RemainTime:=Rec.GetInteger('RemainTime');
    
  SetLength(fAliens,ACount);

  for Dummy:=0 to ACount-1 do
  begin
    with Rec.GetRecordList('Aliens').Item[Dummy] do
    begin
      FillChar(fAliens[Dummy],SizeOf(TAlien),#0);
      fAliens[Dummy]:=alien_api_GetAlienInfo(GetCardinal('ID'))^;
      fAliens[Dummy].Ges:=GetInteger('Ges');
    end;
  end;

  fMAvaible:=true;

  Rec.Free;
end;

procedure TEinsatz.NextMinute(Minuten: Integer);
begin
  if fRemainTime=0 then
    exit;
  dec(fRemainTime,Minuten);
  if fRemainTime<=0 then
  begin
    TimeUp;
    exit;
  end;
end;

class function TEinsatz.ReadFromScriptString(ObjStr: String): TObject;
begin
  result:=einsatz_api_GetEinsatz(StrToInt(ObjStr));
end;

procedure TEinsatz.SaveToStream(Stream: TStream);
var
  Dummy : Integer;
  Rec   : TExtRecord;
begin
  Rec:=TExtRecord.Create(TEinsatzRecord);
  Rec.SetInteger('AlienCount',AlienCount);
  Rec.SetDouble('PositionX',fPosition.x);
  Rec.SetDouble('PositionY',fPosition.y);
  Rec.SetCardinal('ID',fID);
  Rec.SetString('Name',fName);
  Rec.SetString('Description',fDescription);
  Rec.SetString('Objectives',fObjectives);
  Rec.SetString('MapName',fMapName);
  Rec.SetInteger('RemainTime',fRemainTime);

  for Dummy:=0 to High(fAliens) do
  begin
    with Rec.GetRecordList('Aliens').Add do
    begin
      SetCardinal('ID',fAliens[Dummy].ID);
      SetInteger('Ges',fAliens[Dummy].Ges);
    end;
  end;

  Rec.SaveToStream(Stream);
  Rec.Free;
end;

procedure TEinsatz.SetNewParams(Number: Integer;Pos: TFloatPoint);
var
  IDOK : boolean;
  Dummy: Integer;
  NewID: Cardinal;
begin
  fPosition:=Pos;
  fName:=Format(FEinsatzName,[Number]);
  IDOK:=false;
  NewID:=0;
  while not IDOK do
  begin
    NewID:=random(High(Cardinal));
    IDOK:=true;
    for Dummy:=0 to fList.Count-1 do
    begin
      if fList[Dummy].ID=NewID then
      begin
        IDOK:=false;
        break;
      end;
    end;
  end;
  fID:=NewID;
end;

procedure TEinsatz.TimeUp;
begin
  fNotifyList.CallEvents(EVENT_EINSATZONTIMEUP,Self);
  Free;
end;

procedure TEinsatz.Win;
begin
  fNotifyList.CallEvents(EVENT_EINSATZONWIN,Self);
  SetLength(fAliens,0);
  CheckEinsatz;
end;

function TEinsatz.WriteToScriptString: String;
begin
  result:=IntToStr(Int64(fID));
end;

procedure TEinsatz.GenerateNewMap;
begin
  MapName:=fList.GetRandomMap;
end;

var
  Temp1: TExtRecordDefinition;

initialization

  TEinsatzListRecord:=TExtRecordDefinition.Create('TEinsatzListRecord');
  TEinsatzListRecord.AddInteger('EinsatzCount',0,high(Integer));
  TEinsatzListRecord.AddInteger('Number',0,high(Integer));

  Temp1:=TExtRecordDefinition.Create('EinsatzAliens');
  Temp1.AddCardinal('ID',0,high(Cardinal));
  Temp1.AddInteger('Ges',low(Integer),high(Integer));

  TEinsatzRecord:=TExtRecordDefinition.Create('TEinsatzRecord');
  with TEinsatzRecord do
  begin
    AddInteger('AlienCount',0,high(Integer));
    AddDouble('PositionX',-10000,10000,10);
    AddDouble('PositionY',-10000,10000,10);
    AddCardinal('ID',0,high(Cardinal));
    AddString('Name',50);
    AddString('Description',10000);
    AddString('Objectives',10000);
    AddString('MapName',255);
    AddInteger('RemainTime',0,high(Integer));
    AddRecordList('Aliens',Temp1);
  end;

finalization
  TEinsatzListRecord.Free;
  TEinsatzRecord.Free;
  Temp1.Free;
end.
