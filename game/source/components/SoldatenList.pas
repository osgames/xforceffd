{********************************************************************************
*										*
* Copyright (C) 2004  X-Force Team - jim_raynor@web.de				*
*										*
* This file is part of X-Force: Fight For Destiny (X-Force).			*
*										*
* X-Force is free software; you can redistribute it and/or modify		*
* it under the terms of the GNU General Public License as published by		*
* the Free Software Foundation; either version 2 of the License, or		*
* (at your option) any later version.						*
*										*
* X-Force is distributed in the hope that it will be useful,			*
* but WITHOUT ANY WARRANTY; without even the implied warranty of		*
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the			*
* GNU General Public License for more details.					*
*										*
* You should have received a copy of the GNU General Public License		*
* along with X-Force; if not, write to the Free Software			*
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA	*
*										*
*********************************************************************************
*										*
* basis_api bildet die Schnittstelle zu einer Basisliste, die bis zu 8 Basen	*
* verwaltet									*
*										*
* WICHTIG!! Bevor die Funktionen genutzt werden k�nnen muss �ber 		*
*           basis_api_init die Basisliste gesetzt werden. Sollte durch 		*
*           savegame_api_init geschehen sein.					*
*										*
********************************************************************************}

{$I ../Settings.inc}

unit SoldatenList;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  XForce_types, imgList,math,DXDraws, DXClass, KD4Utils, Blending, Defines, DXContainer,
  DirectDraw, TraceFile, BasisListe, Loader, NGTypes, GameFigureManager,
  ISOMessages, ConvertRecords, ExtRecord, StringConst;

const
  AlphaValues: Array[boolean,0..4] of Integer = ((128,255,255,255,128),
                                                  (128,96,64,96,128));


type
  TSoldatenArray     = Array of TSoldatInfo;
  TManagerArray      = Array of TSoldatManager;

  TSoldatenList = class(TObject)
  private
    fSoldaten        : TSoldatenArray;
    fFigureManagers  : TManagerArray;

    fManagers        : Boolean;
    fWatchs          : TWatchMarktArray;
    fFindPerson      : TFindPersonEvent;
    procedure DrawGesundheit(Surface: TDirectDrawSurface;const Mem: TDDSurfaceDesc;X,Y: Integer; Prozent: Integer);
    function GetCount: Integer;
    function GetSoldat(Index: Integer): TSoldatInfo;
    function GetSoldatenWert(Index: Integer): Integer;
    function CreateManager(Index: Integer): TSoldatManager;
    function GetManager(Index: Integer): TSoldatManager;

    procedure AktuFigureRefs;

    procedure SetSoldatenKlassenProperty(var Soldat: TSoldatInfo);

    procedure ChangeSoldatenBasis(Soldat: Integer; BasisID: Cardinal);
  protected
    procedure CheckWatchForSoldat(Index: Integer);

    function CanTrain(Index: Integer): boolean;

    procedure NewGame(Sender: TObject);
  public
    constructor Create;
    destructor Destroy;override;

    procedure DrawDXSoldatenBitmap(const Soldat: Integer;Surface: TDirectDrawSurface;const Mem: TDDSurfaceDesc;x,y: Integer;Ges: boolean=true);
    procedure DrawDXIndexBitmap(Index: Integer;Surface: TDirectDrawSurface;const Mem: TDDSurfaceDesc;x,y: Integer);
    procedure DrawDXSoldat(const Soldat: TSoldatInfo;Surface: TDirectDrawSurface;const Mem: TDDSurfaceDesc; x, y: Integer;Ges: boolean=true);
    procedure DrawSmallSoldat(const Soldat: TSoldatInfo;Surface: TDirectDrawSurface;const Mem: TDDSurfaceDesc; x, y: Integer;ZeitEinh: Integer; Ges: boolean = true; MaxZeit: Integer = 0);
    procedure CreateSoldaten(Anzahl: Integer);
    procedure LoadFromStream(Stream: TStream);
    procedure SaveToStream(Stream: TStream);

    procedure AddSoldat(const Soldat: TSoldatInfo; ToBasis: Cardinal = 0);
    procedure RenameSoldat(Soldat: Integer;Name: String);
    procedure DeleteSoldat(Index: Integer);

    procedure ChangeBase(Soldat: Integer; NewBase: Cardinal);

    procedure Clear;
    procedure NextDay;
    procedure NextRound(Minuten: Integer);
    procedure SetTraining(Index: Integer;Change: boolean=true);
    procedure SetRaumschiff(Index, RaumschiffID: Integer;Slot: Byte);
    procedure CheckSoldaten;
    procedure UpgradeID(ID: TObject);

    function IsSoldatUnterwegs(Index: Integer): boolean;
    function GetRaumschiffName(Index: Integer): String;
    function InSelectedBasis(Index: Integer): boolean;
    function GetSoldatInRaumschiff(RaumID: Integer; Slot: Integer; var Soldat: PSoldatInfo): Integer;

    function GetSoldatAddr(Index: Integer): PSoldatInfo;

    // Berechnet das W�chentliche Gehalt f�r alle Soldaten
    function CalculateWeeklySold: Integer; overload;
    // Berechnet das W�chentliche Gehalt f�r alle Soldaten in angegebener Basis
    function CalculateWeeklySold(BasisID: Cardinal): Integer; overload;

    { Steuerung zur �berwachung der Liste }
    procedure CancelWatch(BasisID: Cardinal);
    procedure SetWatch(Basis: Cardinal;minStrength: Integer;WType: TWatchType);
    function GetWatch(BasisID: Cardinal;var Watch: TWatchMarktSettings): Boolean;

    function CountInBase(BasisID: Cardinal): Integer;

    property Soldaten          [Index: Integer]: TSoldatInfo read GetSoldat;default;
    property Managers          [Index: Integer]: TSoldatManager read GetManager;
    property Wert              [Index: Integer]: Integer read GetSoldatenWert;
    property Count             : Integer read GetCount;
    property ManagePlayers     : Boolean read fManagers write fManagers;
    property OnFindPerson      : TFindPersonEvent read fFindPerson write fFindPerson;
  end;

implementation

{ TSoldatenList }
uses
  RaumschiffList, LagerListe, ArchivFile, savegame_api, array_utils, basis_api,
  person_api, raumschiff_api, lager_api, forsch_api, soldaten_api;

var
  ReadClasses           : boolean = false;
  Klassen               : Array of TSoldatKlasse;
  TSoldatenListRecord   : TExtRecordDefinition;


procedure ParseClassIniFile(List: TStringList);
var
  Dummy  : Integer;
  Klasse : String;
  Index  : Integer;
  prop   : String;
  val    : String;

  procedure SplitProperty(var Prop: TSoldatKlasseProperty; Value: String);

    procedure SplitRange(var min,max: Byte;val: String);
    begin
      min:=StrToInt(Copy(Val,1,Pos('-',Value)-1));
      max:=StrToInt(Copy(Val,Pos('-',Value)+1,3));
    end;

  begin
    SplitRange(Prop.StartMin,Prop.StartMax,Copy(Value,1,Pos(',',Value)-1));
    SplitRange(Prop.FaehigMin,Prop.FaehigMax,Copy(Value,Pos(',',Value)+1,20));
  end;

begin
  for Dummy:=0 to List.Count-1 do
  begin
    if List[Dummy][1]='[' then
    begin
      Klasse:=trim(Copy(List[Dummy],2,Pos(']',List[Dummy])-2));
      SetLength(Klassen,length(Klassen)+1);
      Index:=high(Klassen);
      Klassen[Index].Name:=Klasse;
      GlobalFile.Write('Soldatenklasse',Klassen[Index].Name);
    end
    else
    begin
      prop:=uppercase(Copy(List[Dummy],1,pos('=',List[Dummy])-1));
      val:=Copy(List[Dummy],pos('=',List[Dummy])+1,20);
      if prop='GES' then SplitProperty(Klassen[Index].Ges,val)
      else if prop='TREFF' then SplitProperty(Klassen[Index].Treff,val)
      else if prop='ZEIT' then SplitProperty(Klassen[Index].Zeit,val)
      else if prop='KRAFT' then SplitProperty(Klassen[Index].Kraft,val)
      else if prop='SICHT' then SplitProperty(Klassen[Index].Sicht,val)
      else if prop='PSIAN' then SplitProperty(Klassen[Index].PSIAn,val)
      else if prop='PSIAB' then SplitProperty(Klassen[Index].PSIAb,val)
      else if prop='IQ' then SplitProperty(Klassen[Index].IQ,val)
      else if prop='REACT' then SplitProperty(Klassen[Index].React,val);
    end;
  end;
end;

constructor TSoldatenList.Create;
// const in Delphi verhalten sich wie statische Variablen
const
  BucketID : Cardinal = $5C08F3B2;

var
  Archiv: TArchivFile;
  List  : TStringList;
begin
  soldaten_api_init(Self);

  randomize;
  fManagers:=true;

  if not ReadClasses then
  begin
    ReadClasses:=true;
    Archiv:=TArchivFile.Create;
    if not FileExists(FGameDataFile) then
      exit;

    // Classen einlesen
    Archiv.OpenArchiv(FGameDataFile);
    Archiv.OpenRessource(RNSoldatClasses);
    List:=TStringList.Create;
    List.LoadFromStream(Archiv.Stream);
    CleanStringList(List);
    ParseClassIniFile(List);
    List.Free;
    Archiv.Free;
  end;

  forsch_api_RegisterUpgradeEndHandler(UpgradeID);

  savegame_api_RegisterCustomSaveHandler(SaveToStream,LoadFromStream,nil,BucketID);
  // BucketID f�r die N�chste Soldatenliste �ndern
  inc(BucketID,$32C99A8A);

  savegame_api_RegisterNewGameHandler(NewGame);
end;

procedure TSoldatenList.CreateSoldaten(Anzahl: Integer);
var
  Dummy: Integer;
  Index: Integer;
begin
  SetLength(fSoldaten,Count+Anzahl);
  if fManagers then
    SetLength(fFigureManagers,length(fSoldaten));
  Index:=high(fSoldaten);
  for Dummy:=1 to Anzahl do
  begin
    with fSoldaten[Index] do
    begin
      Name:=person_api_GetFixedName;
      SetSoldatenKlassenProperty(fSoldaten[Index]);

      Image:=person_api_GenerateFace;
      Tag:=0;
      Train:=false;
      Einsatz:=0;
      Treffer:=0;
      Raum:=0;
      LeftHand:=(random(4)=1);
      BasisID:=basis_api_GetSelectedBasis.ID;

    end;
    if fManagers then
    begin
      fFigureManagers[Index]:=CreateManager(Index);
    end;
    CheckWatchForSoldat(Index);
    dec(Index);
  end;
  if fManagers then
    AktuFigureRefs;
end;

function TSoldatenList.GetCount: Integer;
begin
  result:=length(fSoldaten);
end;

function TSoldatenList.GetSoldat(Index: Integer): TSoldatInfo;
begin
  if (Index>=0) and (Index<Count) then
    Result:=fSoldaten[Index];
end;

procedure TSoldatenList.LoadFromStream(Stream: TStream);
var
  Dummy   : Integer;
  Rec     : TExtRecord;
  SCount  : Integer;
begin
  Clear;

  Rec:=TExtRecord.Create(TSoldatenListRecord);
  Rec.LoadFromStream(Stream);

  // Arbeitsmarkteinstellungen speichern
  with Rec.GetRecordList('WatchMarkt') do
  begin
    SetLength(fWatchs,Count);
    for Dummy:=0 to high(fWatchs) do
      fWatchs[Dummy]:=RecordToWatchMarktSettings(Item[Dummy]);
  end;

  SCount:=Rec.GetInteger('SoldatenCount');

  SetLength(fSoldaten,SCount);

  if fManagers then
    SetLength(fFigureManagers,SCount);

  with Rec.GetRecordList('Soldaten') do
  begin
    for Dummy:=0 to SCount-1 do
    begin
      if fManagers then
        fFigureManagers[Dummy]:=CreateManager(Dummy);

      fSoldaten[Dummy]:=RecordToSoldatInfo(Item[Dummy]);
    end;
  end;

  if fManagers then
  begin
    for Dummy:=0 to SCount-1 do
      Managers[Dummy].LoadFromStream(Stream);
  end;

  Rec.Free;
end;

procedure TSoldatenList.SaveToStream(Stream: TStream);
var
  Dummy  : Integer;
  Rec    : TExtRecord;
begin
  Rec:=TExtRecord.Create(TSoldatenListRecord);

  Rec.SetInteger('SoldatenCount',Count);

  with Rec.GetRecordList('Soldaten') do
  begin
    for Dummy:=0 to high(fSoldaten) do
      Add(SoldatInfoToRecord(fSoldaten[Dummy]));
  end;

  // Arbeitsmarkteinstellungen speichern
  with Rec.GetRecordList('WatchMarkt') do
  begin
    for Dummy:=0 to high(fWatchs) do
      Add(WatchMarktSettingsToRecord(fWatchs[Dummy]));
  end;

  Rec.SaveToStream(Stream);
  Rec.Free;

  if fManagers then
  begin
    for Dummy:=0 to Count-1 do
      Managers[Dummy].SaveToStream(Stream);
  end;
end;

procedure TSoldatenList.Clear;
var
  Dummy: Integer;
begin
  SetLength(fSoldaten,0);
  if fManagers then
  begin
    for Dummy:=0 to high(fFigureManagers) do
    begin
      fFigureManagers[Dummy].Free;
    end;
    SetLength(fFigureManagers,0);
  end;

  SetLength(fWatchs,0);
end;

function TSoldatenList.GetSoldatenWert(Index: Integer): Integer;
begin
  result:=KD4Utils.GetSoldatWert(Soldaten[Index]);
end;

procedure TSoldatenList.NextDay;
var
  Dummy      : integer;
  Pension    : boolean;
  Soldat     : PSoldatInfo;
  TrainTime  : Double;

  function Kosten: boolean;
  begin
    result:=savegame_api_NeedMoney(round(25*TrainTime),kbTK);
  end;

  procedure TrainProperty(var Prop: Single);
  begin
    if (Prop<100) and Kosten then
    begin
      if Prop=0 then
        Prop:=1;
      Prop:=minReal(Prop+((20/Prop)*TrainTime),100);
    end;
  end;

begin
  Dummy:=0;
  GlobalFile.WriteLine;
  while (Dummy<Count) do
  begin
    Soldat:=Addr(fSoldaten[Dummy]);
    inc(Soldat.Tag);
    Pension:=false;
    if Soldat.Tag>1500 then
    begin
      if Soldat.Tag>2500 then
        Pension:=(random(10)=5)
      else
        Pension:=(random(30)=random(30));
    end;
    if Pension then
    begin
      savegame_api_FreeMoney(GetSoldatenWert(Dummy),kbSEn);
      savegame_api_Message(Format(HPension,[SSoldat,Soldat.Name,Soldat.Tag,GetSoldatenWert(Dummy)]),lmPension);
      basis_api_FreeWohnRaum(Soldat.BasisID);
      DeleteSoldat(Dummy);
      dec(Dummy);
    end
    else
    begin
      TrainTime:=Soldat.TrainTime/1440;
      if (Soldat.MaxGes<=Soldat.Ges) then
      begin
        if Soldat.Train then
        begin
          { Ausbildung der Soldaten }
          TrainProperty(Soldat.MaxGes);
          TrainProperty(Soldat.PSIAn);
          TrainProperty(Soldat.PSIAb);
          TrainProperty(Soldat.Zeit);
          TrainProperty(Soldat.Treff);
          TrainProperty(Soldat.Kraft);
          TrainProperty(Soldat.Sicht);
          TrainProperty(Soldat.IQ);
          TrainProperty(Soldat.React);
          Soldat.Ges:=Soldat.MaxGes;

          if not CanTrain(Dummy) then
          begin
            Soldat.Train:=false;
            savegame_api_Message(Format(MTrainEnd,[SSoldat,Soldat.Name]),lmTrainEnd);
          end;
        end;
      end;
      // Ausbildungszeit des Tages zur�cksetzen
      Soldat.TrainTime:=0;
    end;
    inc(Dummy);
  end;
end;

procedure TSoldatenList.DeleteSoldat(Index: Integer);
begin
  if (Index>=0) and (Index<Count) then
  begin
    if fManagers then
      fFigureManagers[Index].Free;

    DeleteArray(Addr(fSoldaten),TypeInfo(TSoldatenArray),Index);

    if fManagers then
      DeleteArray(Addr(fFigureManagers),TypeInfo(TManagerArray),Index);

    AktuFigureRefs;
  end;
end;

procedure TSoldatenList.AddSoldat(const Soldat: TSoldatInfo; ToBasis: Cardinal);
begin
  SetLength(fSoldaten,Count+1);
  fSoldaten[Count-1]:=Soldat;
  with fSoldaten[Count-1] do
  begin
    Train:=false;
    Raum:=0;
    Slot:=30;
    if ToBasis=0 then
    begin
      BasisID:=basis_api_GetSelectedBasis.ID;
    end
    else
      BasisID:=ToBasis;
  end;
  if fManagers then
  begin
    SetLength(fFigureManagers,Count);
    fFigureManagers[Count-1]:=CreateManager(Count-1);
    AktuFigureRefs;
  end;
  CheckWatchForSoldat(high(fSoldaten));
end;

procedure TSoldatenList.DrawDXSoldatenBitmap(const Soldat: Integer;
  Surface: TDirectDrawSurface;const Mem: TDDSurfaceDesc; x, y: Integer; Ges: boolean);
begin
  DrawDXSoldat(fSoldaten[Soldat],Surface,Mem,x,y,ges);
end;

procedure TSoldatenList.SetTraining(Index: Integer;Change: boolean=true);
begin
  if (Index>=0) and (Index<Count) then
  begin
    if fSoldaten[Index].Train=false then
    begin
      if not CanTrain(Index) then
        raise Exception.Create(Format(ENoTrainNeed,[SSoldat,fSoldaten[Index].Name]));
    end;
    if Change then
      fSoldaten[Index].Train:=not fSoldaten[Index].Train
    else
      fSoldaten[Index].Train:=true;
  end;
end;

procedure TSoldatenList.DrawDXIndexBitmap(Index: Integer;
  Surface: TDirectDrawSurface;const Mem: TDDSurfaceDesc; x, y: Integer);
var
  SolRect: TRect;
begin
  person_api_DrawFace(Surface,x,y,Index);
  SolRect:=Rect(X,Y,40+x,46+y);
  IntersectRect(SolRect,SolRect,Surface.ClippingRect);
  if IsRectEmpty(SolRect) then
    exit;
  Rectangle(Surface,Mem,SolRect,bcBlack);
end;

function TSoldatenList.CanTrain(Index: Integer): boolean;
var
  Temp: PSoldatInfo;
begin
  Temp:=Addr(fSoldaten[Index]);
  result:=not ((Temp.MaxGes>=100) and
               (Temp.PSIAn>=100) and
               (Temp.PSIAb>=100) and
               (Temp.Zeit>=100) and
               (Temp.Treff>=100) and
               (Temp.Kraft>=100) and
               (Temp.IQ>=100) and
               (Temp.React>=100));
end;

procedure TSoldatenList.RenameSoldat(Soldat: Integer; Name: String);
begin
  fSoldaten[Soldat].Name:=Name;
end;

procedure TSoldatenList.DrawDXSoldat(const Soldat: TSoldatInfo;
  Surface: TDirectDrawSurface;const Mem: TDDSurfaceDesc; x, y: Integer; Ges: boolean);
var
  SolRect: TRect;
begin
  person_api_DrawFace(Surface,x,y,Soldat.Image);

  if Ges then
  begin
    { Zeichnen des Gesundheitsbalken }
    if trunc(Soldat.MaxGes)>0 then
      DrawGesundheit(Surface,Mem,x+34,y+1,round(Soldat.Ges/trunc(Soldat.MaxGes)*100))
    else
      DrawGesundheit(Surface,Mem,x+34,y+1,0)
  end;
end;

{ Diese Routine zeichnet den Gesundheitsbalken      }
{ Surface - Surface auf dem Gezeichnet werden soll  }
{ X,Y     - Anfangsposition des Balkens             }
{ Prozent - Angabe wieviel Prozent hell sein sollen }
procedure TSoldatenList.DrawGesundheit(Surface: TDirectDrawSurface;const Mem: TDDSurfaceDesc;
  X, Y, Prozent: Integer);
var
  Links  : Integer;
  Oben   : Integer;
  Zeiger : Cardinal;
  Full   : boolean;
  RMask  : Cardinal;
  BMask  : Cardinal;
  GMask  : Cardinal;
  RValue : Cardinal;
  BValue : Cardinal;
  GValue : Cardinal;
  Farbe  : Cardinal;
  Beginn : Integer;
  Ende   : Integer;
  RWert  : Cardinal;
  BWert  : Cardinal;
  GWert  : Cardinal;
  HalfAb : Integer;

  function GeneriereFarbStufe(Alpha: Cardinal): TBlendColor;
  begin
    RWert:=((RValue*Alpha) shr 8) and RMask;
    BWert:=((BValue*Alpha) shr 8) and BMask;
    GWert:=((GValue*Alpha) shr 8) and GMask;
    result:=RWert or BWert or GWert;
  end;

begin
  Prozent:=100-Prozent;
  Beginn:=0;
  Ende:=43;
  if Surface.ClippingRect.Top>Y then Beginn:=Surface.ClippingRect.Top-Y;
  if Surface.ClippingRect.Bottom<Y+43 then Ende:=min(43,Surface.ClippingRect.Bottom-Y-1);
  RMask:=Mem.ddpfPixelFormat.dwRBitMask;
  BMask:=Mem.ddpfPixelFormat.dwBBitMask;
  GMask:=Mem.ddpfPixelFormat.dwGBitMask;
  HalfAb:=Prozent*43 div 100;
  if Mode32Bit then
  begin
    for Oben:=Beginn to Ende do
    begin
      Farbe:=LineColorTable[round(Oben/43*100)];
      Full:=HalfAb>Oben;
      if not Full then
      begin
        RValue:=RMask and Farbe;
        BValue:=BMask and Farbe;
        GValue:=GMask and Farbe;
      end
      else
      begin
        RValue:=RMask and bcMaroon;
        BValue:=BMask and bcMaroon;
        GValue:=GMask and bcMaroon;
      end;
      Zeiger:=Integer(Mem.lpSurface)+((Y+Oben)*Mem.lPitch)+(X shl 2);
      for Links:=0 to 4 do
      begin
        PInteger(Zeiger)^:=GeneriereFarbStufe(AlphaValues[Full][Links]);
        inc(Zeiger,4);
      end;
    end;
  end
  else
  begin
    for Oben:=Beginn to Ende do
    begin
      Farbe:=LineColorTable[round(Oben/43*100)];
      Full:=HalfAb>Oben;
      if not Full then
      begin
        RValue:=RMask and Farbe;
        BValue:=BMask and Farbe;
        GValue:=GMask and Farbe;
      end
      else
      begin
        RValue:=RMask and bcMaroon;
        BValue:=BMask and bcMaroon;
        GValue:=GMask and bcMaroon;
      end;
      Zeiger:=Integer(Mem.lpSurface)+((Y+Oben)*Mem.lPitch)+(X shl 1);
      for Links:=0 to 4 do
      begin
        PWord(Zeiger)^:=GeneriereFarbStufe(AlphaValues[Full][Links]);
        inc(Zeiger,2);
      end;
    end;
  end;
end;

procedure TSoldatenList.SetRaumschiff(Index,RaumschiffID: Integer; Slot: Byte);
var
  RSchiff: TRaumschiff;
begin
  // Pr�fen, ob der Soldat in ein anderes Raumschiff verlegt werden kann
  if fSoldaten[Index].Raum<>RaumschiffID then
  begin
    if IsSoldatUnterwegs(Index) then
      raise Exception.Create(ESoldatEinsatz);
  end;

  RSchiff:=raumschiff_api_GetRaumschiff(fSoldaten[Index].Raum);
  // Slot 30 bedeutet, dass der Soldat rausgenommen wird
  if (RSchiff<>nil) then
  begin
    // Soldat wird in einer neuen Basis rausgelassen
    if (RSchiff.GetHomeBasis.ID<>fSoldaten[Index].BasisID) then
    begin
      if (sabEquipt in RSchiff.Abilities) then
        ChangeSoldatenBasis(Index,RSchiff.GetHomeBasis.ID)
      else
        raise Exception.Create(ESoldatEinsatz);
    end;
  end;

  fSoldaten[Index].Raum:=RaumschiffID;
  fSoldaten[Index].Slot:=Slot;
end;

function TSoldatenList.GetRaumschiffName(Index: Integer): String;
var
  Raumschiff: TRaumschiff;
begin
  Raumschiff:=soldaten_api_GetRaumschiff(fSoldaten[Index]);
  if Raumschiff=nil then
    result:=''
  else
    result:=Raumschiff.Name;
end;

function TSoldatenList.InSelectedBasis(Index: Integer): boolean;
begin
  result:=basis_api_GetSelectedBasis.ID=fSoldaten[Index].BasisID;
end;

procedure TSoldatenList.DrawSmallSoldat(const Soldat: TSoldatInfo;
  Surface: TDirectDrawSurface; const Mem: TDDSurfaceDesc; x, y: Integer;
  Zeiteinh: Integer; Ges: boolean; MaxZeit: Integer);
begin
  if Soldat.Ges>0 then
  begin
    person_api_DrawSmallFace(Surface,x,y,Soldat.Image);
    if Ges then
    begin
      Rectangle(Surface,Mem,x,y+30,x+25,y+32,bcMaroon);
      Rectangle(Surface,Mem,x,y+30,x+round(25*Soldat.Ges/trunc(Soldat.MaxGes)),y+32,bcRed);
      if ZeitEinh<>-1 then
      begin
        Rectangle(Surface,Mem,x,y+33,x+25,y+35,bcGreen);
        Rectangle(Surface,Mem,x,y+33,x+round(25*ZeitEinh/MaxZeit),y+35,bcLime);
      end;
    end;
  end
  else
  begin
    person_api_DrawSmallFace(Surface,x,y,Soldat.Image+person_api_GetFaceCount);
  end;
end;

function TSoldatenList.GetSoldatInRaumschiff(RaumID, Slot: Integer;
  var Soldat: PSoldatInfo): Integer;
var
  Dummy: Integer;
begin
  result:=-1;
  for Dummy:=0 to Count-1 do
  begin
    if (fSoldaten[Dummy].Raum=RaumID) and (fSoldaten[Dummy].Slot=Slot) then
    begin
      result:=Dummy;
      Soldat:=Addr(fSoldaten[Dummy]);
      exit;
    end;
  end;
end;

function TSoldatenList.IsSoldatUnterwegs(Index: Integer): boolean;
var
  RSchiff: TRaumschiff;
begin
  result:=false;
  RSchiff:=raumschiff_api_GetRaumschiff(fSoldaten[Index].Raum);
  if RSchiff=nil then
    exit;

  result:=true;
  if not (sabEquipt in RSchiff.Abilities) then
    exit;

  if RSchiff.GetHomeBasis.ID<>fSoldaten[Index].BasisID then
    exit;

  result:=false;
end;

procedure TSoldatenList.CheckSoldaten;
var
  Dummy: Integer;
begin
  for Dummy:=Count-1 downto 0 do
  begin
    if (Soldaten[Dummy].Ges<=0) then
    begin
      basis_api_FreeWohnRaum(Soldaten[Dummy].BasisID);
      DeleteSoldat(Dummy);
    end;
  end;
end;

function TSoldatenList.CreateManager(Index: Integer): TSoldatManager;
begin
  result:=TSoldatManager.Create(Addr(fSoldaten[Index]));
  with result do
  begin
    FigureStatus:=fsFriendly;
    FigureMap:=0;
    OnDrawGesicht:=DrawDXSoldat;
    OnDrawSmallGeischt:=DrawSmallSoldat;
  end;
end;

function TSoldatenList.GetManager(Index: Integer): TSoldatManager;
begin
  if not fManagers then
    raise Exception.Create('Liste verwaltet keine TSoldatManagers');

  Assert((Index>=0) and (Index<=high(fSoldaten)),Format('Invalid Soldat: %d',[Index]));

  result:=fFigureManagers[Index];
end;

procedure TSoldatenList.AktuFigureRefs;
var
  Dummy:  Integer;
begin
  if fManagers then
  begin
    // Dadurch das TGameFigure Pointer verwaltet und durch SetLength
    // die Records in andere Adressr�ume gepackt werden, m�ssen die
    // Pointer zu den Manager aktualisiert werden
    for Dummy:=0 to high(fFigureManagers) do
      fFigureManagers[Dummy].SetRefPointer(Addr(fSoldaten[Dummy]));
  end;
end;

procedure TSoldatenList.SetWatch(Basis: Cardinal; minStrength: Integer; WType: TWatchType);
var
  Dummy: Integer;
begin
  CancelWatch(Basis);

  SetLength(fWatchs,length(fWatchs)+1);

  with fWatchs[high(fWatchs)] do
  begin
    BasisID:=Basis;
    Active:=true;
    minFaehigkeiten:=minStrength;
    WatchType:=WType;
  end;
end;

procedure TSoldatenList.CancelWatch(BasisID: Cardinal);
var
  Dummy: Integer;
begin
  for Dummy:=0 to high(fWatchs) do
  begin
    if fWatchs[Dummy].Active and (fWatchs[Dummy].BasisID=BasisID) then
    begin
      DeleteArray(Addr(fWatchs),TypeInfo(TWatchMarktArray),Dummy);
      exit;
    end;
  end;
end;

function TSoldatenList.GetWatch(BasisID: Cardinal; var Watch: TWatchMarktSettings): Boolean;
var
  Dummy: Integer;
begin
  result:=false;
  for Dummy:=0 to high(fWatchs) do
  begin
    if fWatchs[Dummy].Active and (fWatchs[Dummy].BasisID=BasisID) then
    begin
      Watch:=fWatchs[Dummy];
      result:=true;
      exit;
    end;
  end;
end;

procedure TSoldatenList.CheckWatchForSoldat(Index: Integer);
var
  Dummy      : Integer;
  Faehigkeit : Integer;
  OldCount   : Integer;
begin
  with fSoldaten[Index] do
    Faehigkeit:=round((MaxGes+PSIAn+PSIAb+Zeit+Treff+Kraft+IQ)/3.1);

  OldCount:=length(FSoldaten);
  for Dummy:=0 to high(fWatchs) do
  begin
    if fWatchs[Dummy].Active then
    begin
      if Faehigkeit>=fWatchs[Dummy].minFaehigkeiten then
      begin
        if Assigned(fFindPerson) then
        begin
          fFindPerson(Index,fWatchs[Dummy].BasisID,fWatchs[Dummy].WatchType);

          // Wenn es weniger Soldaten geworden sind, kann davon ausgegangen
          // werden, dass der Soldat angeheuert wurde
          if OldCount>length(fSoldaten) then
            exit;
        end;
      end;
    end;
  end;
end;

procedure TSoldatenList.UpgradeID(ID: TObject);
var
  Dummy: Integer;
begin
  if fManagers then
  begin
    for Dummy:=0 to high(fFigureManagers) do
    begin
      fFigureManagers[Dummy].UpgradeID(Cardinal(ID));
    end;
  end;
end;

procedure TSoldatenList.NextRound(Minuten: Integer);
var
  Dummy   : Integer;
  Soldat  : PSoldatInfo;
begin
  for Dummy:=0 to Count-1 do
  begin
    Soldat:=Addr(fSoldaten[Dummy]);
    // Heilen und Ausbildung darf nur durchgef�hrt werden wenn der Soldat in der
    // ist
    if not IsSoldatUnterwegs(Dummy) then
    begin
      if (Soldat.Ges<Soldat.MaxGes) then
      begin
        // Heilen des Soldaten (20 Prozent pro Tag;runtergerechnet auf die vergangenen Minuten
        Soldat.Ges:=Math.min(Soldat.Ges+(Soldat.MaxGes*0.2)/1440*Minuten,Soldat.MaxGes);

        if Soldat.Ges=Soldat.MaxGes then
        begin
          // Nachricht versenden
          savegame_api_Message(Format(ST0501230002,[Soldat.Name]),lmSoldierCured);
        end;
      end
      else
      begin
        if Soldat.Train then
        begin
          // Dem Soldaten die Zeiten in der Basis f�r die Ausbildung anrechnen
          // Am Tagesende wird dann die Gesamte Ausbildung f�r den Tag berechnet
          inc(Soldat.TrainTime,Minuten);
        end;
      end;
    end;
  end;
end;

function TSoldatenList.GetSoldatAddr(Index: Integer): PSoldatInfo;
begin
  if (Index<0) or (Index>=Count) then
    raise Exception.CreateFmt('TSoldatenList.GetSoldatAddr: Ung�ltiger Index (%d)',[Index])
  else
    result:=Addr(fSoldaten[Index]);
end;

procedure TSoldatenList.SetSoldatenKlassenProperty(var Soldat: TSoldatInfo);
var
  Klasse: TSoldatKlasse;
begin
  if length(Klassen)=0 then
    exit;

  Klasse:=Klassen[random(length(Klassen))];
  with Soldat do
  begin
    ClassName:=Klasse.Name;
    FaehigGes:=randomMinMax(Klasse.Ges.FaehigMin,Klasse.Ges.FaehigMax);
    FaehigZeit:=randomMinMax(Klasse.Zeit.FaehigMin,Klasse.Zeit.FaehigMax);
    FaehigPSIAn:=randomMinMax(Klasse.PSIAn.FaehigMin,Klasse.PSIAn.FaehigMax);
    FaehigPSIAb:=randomMinMax(Klasse.PSIAb.FaehigMin,Klasse.PSIAb.FaehigMax);
    FaehigKraft:=randomMinMax(Klasse.Kraft.FaehigMin,Klasse.Kraft.FaehigMax);
    FaehigSicht:=randomMinMax(Klasse.Sicht.FaehigMin,Klasse.Sicht.FaehigMax);
    FaehigTreff:=randomMinMax(Klasse.Treff.FaehigMin,Klasse.Treff.FaehigMax);
    FaehigIQ:=randomMinMax(Klasse.IQ.FaehigMin,Klasse.IQ.FaehigMax);
    FaehigReact:=randomMinMax(Klasse.React.FaehigMin,Klasse.React.FaehigMax);

    Ges:=randomMinMax(Klasse.Ges.StartMin,Klasse.Ges.StartMax);
    MaxGes:=Ges;
    PSIAn:=randomMinMax(Klasse.PSIAn.StartMin,Klasse.PSIAn.StartMax);
    PSIAb:=randomMinMax(Klasse.PSIAb.StartMin,Klasse.PSIAb.StartMax);
    Zeit:=randomMinMax(Klasse.Zeit.StartMin,Klasse.Zeit.StartMax);
    Kraft:=randomMinMax(Klasse.Kraft.StartMin,Klasse.Kraft.StartMax);
    Treff:=randomMinMax(Klasse.Treff.StartMin,Klasse.Treff.StartMax);
    Sicht:=randomMinMax(Klasse.Sicht.StartMin,Klasse.Sicht.StartMax);
    IQ:=randomMinMax(Klasse.IQ.StartMin,Klasse.IQ.StartMax);
    React:=randomMinMax(Klasse.React.StartMin,Klasse.React.StartMax);

    LastMaxGes:=MaxGes;
    LastPSIAn:=PSIAn;
    LastPSIAb:=PSIAb;
    LastZeit:=Zeit;
    LastTreff:=Treff;
    LastKraft:=Kraft;
    LastSicht:=Sicht;
    LastIQ:=IQ;
    LastReact:=React;

  end;
end;


procedure TSoldatenList.ChangeSoldatenBasis(Soldat: Integer;
  BasisID: Cardinal);
begin
  if fSoldaten[Soldat].BasisID<>0 then
    basis_api_FreeWohnRaum(fSoldaten[Soldat].BasisID);

  if BasisID<>0 then
    basis_api_NeedWohnRaum(BasisID);

  fSoldaten[Soldat].BasisID:=BasisID;
end;

function TSoldatenList.CalculateWeeklySold: Integer;
var
  Dummy: Integer;
begin
  result:=0;
  for Dummy:=0 to Count-1 do
    inc(result,round(Wert[Dummy]/100));
end;

function TSoldatenList.CalculateWeeklySold(BasisID: Cardinal): Integer;
var
  Dummy: Integer;
begin
  result:=0;
  for Dummy:=0 to Count-1 do
    if Soldaten[Dummy].BasisID=BasisID then
      inc(result,round(Wert[Dummy]/100));
end;

procedure TSoldatenList.NewGame(Sender: TObject);
begin
  Clear;
end;

procedure TSoldatenList.ChangeBase(Soldat: Integer;
  NewBase: Cardinal);
begin
  Assert((Soldat>=0) and (Soldat<=high(fSoldaten)),Format('Invalid Soldat: %d',[Soldat]));

  basis_api_FreeWohnRaum(fSoldaten[Soldat].BasisID);
  basis_api_NeedWohnRaum(NewBase,true);
  fSoldaten[Soldat].BasisID:=NewBase;
end;

destructor TSoldatenList.Destroy;
begin
  Clear;
  inherited;
end;

function TSoldatenList.CountInBase(BasisID: Cardinal): Integer;
var
  Dummy: Integer;
begin
  Result := 0;
  for Dummy:=0 to High(fSoldaten) do
    if fSoldaten[Dummy].BasisID=BasisID then
      Inc(Result);
end;

initialization

  TSoldatenListRecord:=TExtRecordDefinition.Create('TSoldatenListRecord');
  TSoldatenListRecord.AddInteger('SoldatenCount',0,high(Integer));
  TSoldatenListRecord.AddRecordList('Soldaten',RecordSoldatInfo);
  TSoldatenListRecord.AddRecordList('WatchMarkt',RecordWatchMarktSettings);

finalization

  TSoldatenListRecord.Free;

end.
