{********************************************************************************
*										*
* Copyright (C) 2004  X-Force Team - jim_raynor@web.de				*
*										*
* This file is part of X-Force: Fight For Destiny (X-Force).			*
*										*
* X-Force is free software; you can redistribute it and/or modify		*
* it under the terms of the GNU General Public License as published by		*
* the Free Software Foundation; either version 2 of the License, or		*
* (at your option) any later version.						*
*										*
* X-Force is distributed in the hope that it will be useful,			*
* but WITHOUT ANY WARRANTY; without even the implied warranty of		*
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the			*
* GNU General Public License for more details.					*
*										*
* You should have received a copy of the GNU General Public License		*
* along with X-Force; if not, write to the Free Software			*
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA	*
*										*
*********************************************************************************
*										*
* Dialog um Einstellungen f�r die automatische Auftragsannahme zu t�tigen	*
*										*
********************************************************************************}

{$I ../Settings.inc}

unit AuftragWatch;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  DXContainer, ModalDialog, DXGroupCaption, Blending, XForce_types, DXBitmapButton,
  DXCheckBox, DXSpinEdit, StringConst;

type
  TWatchPage    = (wpKauf,wpVerkauf);
  TAuftragWatch = class(TModalDialog)
  private
    GroupHeader       : TDXGroupCaption;
    KaufButton        : TDXBitmapButton;
    VerkaufButton     : TDXBitmapButton;
    AktivBox          : TDXCheckBox;
    PreisCaption      : TDXGroupCaption;
    PreisSpinBox      : TDXSpinEdit;
    ActionCaption     : TDXGroupCaption;
    MessageBox        : TDXCheckBox;
    AcceptBox         : TDXCheckBox;
    OKButton          : TDXBitmapButton;
    CancelButton      : TDXBitmapButton;

    fSettings         : TWatchAuftragSettings;
    fAktSettings      : TWatchAuftragSettings;
    fPage             : TWatchPage;


    { Private-Deklarationen }
  protected
    procedure BeforeShow;override;
    procedure SetPage(Page: TWatchPage);
    procedure AktivChange(Sender: TObject);
    procedure PreisChange(Sender: TObject);
    procedure SetEnabled(Enabled: Boolean);
    procedure ButtonClick(Sender: TObject);
    procedure SpinEditKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure ActionChange(Sender: TObject);
    procedure NeedPreisPercent(Value: Integer;var Text: String);
  public
    constructor Create(Page: TDXPage);override;

    property Settings : TWatchAuftragSettings read fSettings write fSettings;
    { Public-Deklarationen }
  end;

implementation

{ TAuftragWatch }

procedure TAuftragWatch.ActionChange(Sender: TObject);
begin
  AktivBox.Container.IncLock;
  AcceptBox.Checked:=Sender=AcceptBox;
  MessageBox.Checked:=Sender=MessageBox;
  if fPage=wpKauf then
  begin
    if AcceptBox.Checked then
      fAktSettings.KaufType:=wtAccept
    else
      fAktSettings.KaufType:=wtMessage;
  end
  else
  begin
    if AcceptBox.Checked then
      fAktSettings.VerKaufType:=wtAccept
    else
      fAktSettings.VerKaufType:=wtMessage;
  end;
  AktivBox.Container.DecLock;
end;

procedure TAuftragWatch.AktivChange(Sender: TObject);
begin
  SetEnabled(AktivBox.Checked);
  if fPage=wpKauf then
  begin
    if AktivBox.Checked then
      fAktSettings.KaufType:=wtMessage
    else
      fAktSettings.KaufType:=wtNone;
  end
  else
  begin
    if AktivBox.Checked then
      fAktSettings.VerKaufType:=wtMessage
    else
      fAktSettings.VerKaufType:=wtNone;
  end;

  PreisChange(Self);
end;

procedure TAuftragWatch.BeforeShow;
begin
  fAktSettings:=fSettings;
  GroupHeader.Font.Style:=[fsBold];
  GroupHeader.Font.Size:=Font.Size+4;
  GroupHeader.FontColor:=clYellow;
  GroupHeader.Caption:=ST0309210001;

  PreisCaption.Caption:=ST0309210002;
  ActionCaption.Caption:=ST0309210003;

  SetPage(wpKauf);
end;

procedure TAuftragWatch.ButtonClick(Sender: TObject);
begin
  case (Sender as TDXBitmapButton).Tag of
    0: SetPage(wpKauf);
    1: SetPage(wpVerkauf);
    2:
    begin
      fSettings:=fAktSettings;
      ReadyShow;
    end;
    3: ReadyShow;
  end;
end;

constructor TAuftragWatch.Create(Page: TDXPage);
begin
  inherited;

  { �berschrift }
  GroupHeader:=TDXGroupCaption.Create(Page);
  with GroupHeader do
  begin
    Visible:=false;
    BorderColor:=$00400000;
    BlendColor:=bcDarkNavy;
    RoundCorners:=rcTop;
    AlphaValue:=200;
    Width:=323;
    Height:=25;
    Alignment:=taCenter;
  end;
  AddComponent(GroupHeader);

  { Einkaufbutton }
  KaufButton:=TDXBitmapButton.Create(Page);
  with KaufButton do
  begin
    Visible:=false;
    OnClick:=ButtonClick;
    Text:=ST0309210004;
    Width:=GroupHeader.Width div 2;
    Top:=GroupHeader.Bottom+1;
    Font:=Font;
    Highlight:=true;
    RoundCorners:=rcNone;
    Height:=28;
    BlendAlpha:=150;
    BlendColor:=bcDarkNavy;
    FirstColor:=$00400000;
    SecondColor:=clBlue;
    AccerlateColor:=clYellow;
    Tag:=0;
  end;
  AddComponent(KaufButton);

  { Einstellungen f�r den Abbrechen Button }
  VerkaufButton:=TDXBitmapButton.Create(Page);
  with VerkaufButton do
  begin
    Visible:=false;
    Text:=ST0309210005;
    Top:=GroupHeader.Bottom+1;
    Width:=GroupHeader.Width div 2;
    Left:=KaufButton.Right+1;
    Font:=Font;
    Height:=28;
    RoundCorners:=rcNone;
    OnClick:=ButtonClick;
    BlendAlpha:=150;
    FirstColor:=$00400000;
    BlendColor:=bcDarkNavy;
    SecondColor:=clBlue;
    AccerlateColor:=clYellow;
    Tag:=1;
  end;
  AddComponent(VerkaufButton);

  { Aktivieren/deaktivieren Checkbox }
  AktivBox:=TDXCheckbox.Create(Page);
  with AktivBox do
  begin
    Width:=GroupHeader.Width;
    Height:=23;
    Top:=KaufButton.Bottom+1;
    BorderColor:=$00400000;
    BlendColor:=bcDarkNavy;
    Caption:=ST0309210006;
    RoundCorners:=rcNone;
    LeftMargin:=6;
    OnChange:=AktivChange;
  end;
  AddComponent(AktivBox);

  { Preiseinstellung }
  PreisSpinBox:=TDXSpinEdit.Create(Page);
  with PreisSpinBox do
  begin
    Width:=130;
    Height:=20;
    Left:=GroupHeader.Width-Width;
    Top:=AktivBox.Bottom+1;
    Max:=1100;
    Min:=900;
    SmallStepButton:=true;
    SmallStep:=1;
    Step:=10;
    FormatString:='';
    BlendColor:=bcDarkNavy;
    BorderColor:=$00400000;
    RoundCorners:=rcNone;
    FocusColor:=clNavy;
    TopMargin:=3;
    BlendValue:=175;
    OnKeyPress:=SpinEditKeyDown;
    GetValueText:=NeedPreisPercent;
    OnIncrement:=PreisChange;
    OnDecrement:=PreisChange;
  end;
  AddComponent(PreisSpinBox);

  { �berschrift }
  PreisCaption:=TDXGroupCaption.Create(Page);
  with PreisCaption do
  begin
    Visible:=false;
    BorderColor:=$00400000;
    BlendColor:=bcDarkNavy;
    RoundCorners:=rcNone;
    Width:=GroupHeader.Width-PreisSpinBox.Width-1;
    Height:=PreisSpinBox.Height;
    Top:=AktivBox.Bottom+1;
  end;
  AddComponent(PreisCaption);

  { �berschrift }
  ActionCaption:=TDXGroupCaption.Create(Page);
  with ActionCaption do
  begin
    Visible:=false;
    BorderColor:=$00400000;
    BlendColor:=bcDarkNavy;
    RoundCorners:=rcNone;
    Width:=GroupHeader.Width;
    Height:=PreisSpinBox.Height;
    Top:=PreisSpinBox.Bottom+1;
  end;
  AddComponent(ActionCaption);

  { Aktivieren/deaktivieren Checkbox }
  MessageBox:=TDXCheckbox.Create(Page);
  with MessageBox do
  begin
    Width:=GroupHeader.Width;
    Height:=23;
    Top:=ActionCaption.Bottom+1;
    BorderColor:=$00400000;
    BlendColor:=bcDarkNavy;
    Caption:=ST0309210007;
    RoundCorners:=rcNone;
    LeftMargin:=6;
    OnChange:=ActionChange;
    RadioButton:=true;
  end;
  AddComponent(MessageBox);

  { Aktivieren/deaktivieren Checkbox }
  AcceptBox:=TDXCheckbox.Create(Page);
  with AcceptBox do
  begin
    Width:=GroupHeader.Width;
    Height:=23;
    Top:=MessageBox.Bottom+1;
    BorderColor:=$00400000;
    BlendColor:=bcDarkNavy;
    RoundCorners:=rcNone;
    Caption:=ST0309210008;
    LeftMargin:=6;
    OnChange:=ActionChange;
    RadioButton:=true;
  end;
  AddComponent(AcceptBox);

  { Einstellungen f�r den OK Button }
  OKButton:=TDXBitmapButton.Create(Page);
  with OKButton do
  begin
    Visible:=false;
    OnClick:=ButtonClick;
    Text:=BOK;
    Width:=GroupHeader.Width div 2;
    Top:=AcceptBox.Bottom+1;
    Font:=Font;
    RoundCorners:=rcLeftBottom;
    Height:=28;
    BlendAlpha:=150;
    BlendColor:=bcDarkNavy;
    FirstColor:=$00400000;
    SecondColor:=clBlue;
    AccerlateColor:=clYellow;
    Tag:=2;
  end;
  AddComponent(OKButton);

  { Einstellungen f�r den Abbrechen Button }
  CancelButton:=TDXBitmapButton.Create(Page);
  with CancelButton do
  begin
    Visible:=false;
    Text:=BCancel;
    Top:=AcceptBox.Bottom+1;
    Width:=GroupHeader.Width div 2;
    Left:=OKButton.Right+1;
    Font:=Font;
    Height:=28;
    RoundCorners:=rcRightBottom;
    OnClick:=ButtonClick;
    BlendAlpha:=150;
    FirstColor:=$00400000;
    BlendColor:=bcDarkNavy;
    SecondColor:=clBlue;
    AccerlateColor:=clYellow;
    Tag:=3;
  end;
  AddComponent(CancelButton);
end;

procedure TAuftragWatch.NeedPreisPercent(Value: Integer; var Text: String);
begin
  Text:=Format('%.1n %%',[Value/10]);
end;

procedure TAuftragWatch.PreisChange(Sender: TObject);
begin
  if fPage=wpKauf then
    fAktSettings.KaufPreis:=PreisSpinBox.Value
  else
    fAktSettings.VerKaufPreis:=PreisSpinBox.Value
end;

procedure TAuftragWatch.SetEnabled(Enabled: Boolean);
begin
  PreisSpinBox.Container.IncLock;
  AktivBox.Checked:=Enabled;
  PreisSpinBox.Enabled:=Enabled;
  MessageBox.Enabled:=Enabled;
  AcceptBox.Enabled:=Enabled;
  AcceptBox.Checked:=false;
  MessageBox.Checked:=Enabled;
  PreisSpinBox.Container.DecLock;
end;

procedure TAuftragWatch.SetPage(Page: TWatchPage);
begin
  fPage:=Page;
  KaufButton.Container.Lock;
  KaufButton.Highlight:=Page=wpKauf;
  VerkaufButton.Highlight:=Page=wpVerkauf;
  if Page=wpKauf then
  begin
    PreisCaption.Caption:=ST0309210002;
    PreisSpinBox.Value:=fAktSettings.KaufPreis;
    SetEnabled(fAktSettings.KaufType<>wtNone);
    AcceptBox.Checked:=fAktSettings.KaufType=wtAccept;
    MessageBox.Checked:=fAktSettings.KaufType=wtMessage;
  end
  else
  begin
    PreisCaption.Caption:=ST0309210009;
    PreisSpinBox.Value:=fAktSettings.VerKaufPreis;
    SetEnabled(fAktSettings.VerKaufType<>wtNone);
    AcceptBox.Checked:=fAktSettings.VerkaufType=wtAccept;
    MessageBox.Checked:=fAktSettings.VerkaufType=wtMessage;
  end;
  KaufButton.Container.Unlock;
  KaufButton.Container.RedrawArea(Rect(KaufButton.Left,KaufButton.Top,AcceptBox.Right,AcceptBox.Bottom),KaufButton.Container.Surface);
end;

procedure TAuftragWatch.SpinEditKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key=13 then OKButton.Doclick
  else if Key=27 then CancelButton.Doclick;
end;

end.

