{********************************************************************************
*										*
* Copyright (C) 2004  X-Force Team - jim_raynor@web.de				*
*										*
* This file is part of X-Force: Fight For Destiny (X-Force).			*
*										*
* X-Force is free software; you can redistribute it and/or modify		*
* it under the terms of the GNU General Public License as published by		*
* the Free Software Foundation; either version 2 of the License, or		*
* (at your option) any later version.						*
*										*
* X-Force is distributed in the hope that it will be useful,			*
* but WITHOUT ANY WARRANTY; without even the implied warranty of		*
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the			*
* GNU General Public License for more details.					*
*										*
* You should have received a copy of the GNU General Public License		*
* along with X-Force; if not, write to the Free Software			*
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA	*
*										*
*********************************************************************************
*										*
* Anzeige des kleinen Nachrichten-Bereiches im Bodeneinsatz �ber der Soldaten-	*
* leiste									*
*										*
********************************************************************************}

{$I ../Settings.inc}

unit DXEinsatzPager;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  DXContainer, DXDraws, Blending, DirectDraw, DirectFont, KD4Utils, TraceFile,
  DXScrollBar, XForce_types, Defines, ISOMessages, DXCheckBox, GameFigure,
  StringConst;

const
  ImageRect   : TRect = (Left: 144;Top: 20;Right: 155;Bottom: 35);
  FlippedRect : TRect = (Left: 156;Top: 20;Right: 167;Bottom: 35);
  CenterRect  : TRect = (Left: 175;Top: 20;Right: 188;Bottom: 33);
  PlayRect    : TRect = (Left:  92;Top:  2;Right: 100;Bottom: 18);
  PauseRect   : TRect = (Left: 125;Top:  2;Right: 140;Bottom: 18);

type
  TPagerButton    = (pbNone,pbPause,pbPlay);

  PMessageInfo    = ^TMessageInfo;
  TMessageInfo    = record
    Alpha         : Integer;
    MessageType   : TEinsatzMessage;
    Point         : TPoint;
    Figure        : TGameFigure;
  end;

  TDXEinsatzPager = class(TDXComponent)
  private
    fShowTime     : Integer;
    fCenterButton : Integer;
    fPagerButton  : TPagerButton;
    fOverFull     : boolean;
    fTop          : Integer;
    fMessages     : TStringList;
    fMessage      : Integer;
    fCollaps      : boolean;
    fScrollBar    : TDXScrollBar;
    fCheckBox     : TDXCheckBox;
    fTextWidth    : Integer;
    fNeedRedraw   : boolean;
    fFullMessage  : Integer;
    fLastType     : TEinsatzMessage;
    fPausedTypes  : TEinsatzMessages;
    fEchtzeit     : boolean;
    function OverCollapsButton(Point: TPoint): boolean;
    function OverPagerButton(Point: TPoint): TPagerButton;
    function GetCenterButton(Point: TPoint): Integer;
    procedure OnScroll(Sender: TObject);
    procedure ClearMessages;
    procedure SetCollaps(const Value: Boolean);

    procedure GoOnWithPause(Sender: TObject);
    procedure GoOnWithPlay(Sender: TObject);

    // Benutzerdefinierte Daten in den Spielstand speichern
    procedure SaveCustomSaveData(Stream: TStream);
    procedure LoadCustomSaveData(Stream: TStream);

    procedure NewGameHandler(Sender: TObject);
  protected
    function PerformFrame(Sender: TObject;Frames: Integer): boolean;
    procedure DoScroll(Direction: TScrollDirection;Pos: TPoint);override;
  public
    constructor Create(Page: TDXPage);override;
    destructor Destroy;override;
    procedure Redraw;override;
    procedure Draw(Surface: TDirectDrawSurface;var Mem: TDDSurfaceDesc);override;
    procedure RedrawRect(DrawRect: TRect;Surface: TDirectDrawSurface;var Mem: TDDSurfaceDesc);override;
    procedure Resize(var NewLeft,NewTop,NewWidth,NewHeight: Integer);override;
    procedure CenterToPausedMessage;
    procedure EndMessage(Play: Boolean);

    procedure MouseMove(X,Y: Integer);override;
    procedure MouseDown(Button: TMouseButton;X,Y: Integer);override;
    procedure MouseLeave;override;

    procedure SendMessage(Text: String; Point: TPoint; MessageType: TEinsatzMessage; Figure: TGameFigure);
    procedure StartGame(Echtzeit: Boolean);
    procedure EndGame;

    property Collaps    : Boolean read fCollaps write SetCollaps;
    property PauseBei   : TEinsatzMessages read fPausedTypes write fPausedTypes;
  end;

implementation

uses
  savegame_api, ui_utils, draw_utils;

{ TDXEinsatzPager }

procedure TDXEinsatzPager.CenterToPausedMessage;
var
  Info : PMessageInfo;
begin
  if fFullMessage=-1 then
    exit;
  Info:=PMessageInfo(fMessages.Objects[fFullMessage]);

  if Info=nil then
    exit;

  if Info.MessageType in [emReloadWeapon,emNoMunition,emUnitHeavyInjured] then
    Rec.Point:=Point(Info.Figure.XPos,Info.Figure.YPos)
  else
    Rec.Point:=Info.Point;
  SendVMessage(vmCenterPoint);
end;

procedure TDXEinsatzPager.ClearMessages;
var
  Dummy: Integer;
begin
  for Dummy:=0 to fMessages.Count-1 do
  begin
    if fMessages.Objects[Dummy]<>nil then
      FreeMem(PMessageInfo(fMessages.Objects[Dummy]));
  end;
  fMessages.Clear;
end;

constructor TDXEinsatzPager.Create(Page: TDXPage);
begin
  inherited;
  fMessages:=TStringList.Create;
  fOverFull:=False;
  fMessage:=-1;

  fScrollBar:=TDXScrollBar.Create(Page);
  fScrollBar.Visible:=false;
  fScrollBar.Kind:=sbVertical;
  fScrollBar.RoundCorners:=rcNone;
  fScrollBar.FirstColor:=coScrollBrush;
  fScrollBar.SecondColor:=coScrollPen;
  fScrollBar.SmallChange:=18;
  fScrollBar.OnChange:=OnScroll;
  fScrollBar.LargeChange:=18*4;

  fCheckBox:=TDXCheckBox.Create(Page);
  fCheckBox.Caption:=ST0309210017;
  fCheckBox.Visible:=false;
  fCheckBox.BorderColor:=clMaroon;
  fCheckBox.BlendColor:=bcMaroon;
  fCheckBox.LeftMargin:=4;
  fCheckBox.Font.Name:=coFontName;
  fCheckBox.Font.Color:=clWhite;
  fCheckBox.Font.Style:=[fsBold];
  fCheckBox.RoundCorners:=rcAll;
  fCheckBox.Font.Size:=coFontSize;
  fCheckBox.SetRect(0,0,0,0);

  fFullMessage:=-1;
  fCollaps:=true;
  Collaps:=false;

  savegame_api_RegisterCustomSaveHandler(SaveCustomSaveData,LoadCustomSaveData,nil,$33BA32BE);

  savegame_api_RegisterNewGameHandler(NewGameHandler);
end;

destructor TDXEinsatzPager.Destroy;
begin
  ClearMessages;
  fMessages.Free;
  inherited;
end;

procedure TDXEinsatzPager.DoScroll(Direction: TScrollDirection;
  Pos: TPoint);
begin
  inherited;
  Container.IncLock;
  if Direction=sdUp then
    fScrollBar.Value:=fScrollBar.Value-18
  else if Direction=sdDown then
    fScrollBar.Value:=fScrollBar.Value+18;
  Container.DoMouseMessage;
  Container.DecLock;
end;

procedure TDXEinsatzPager.Draw(Surface: TDirectDrawSurface;
  var Mem: TDDSurfaceDesc);
begin
  RedrawRect(ClientRect,Surface,Mem);
end;

procedure TDXEinsatzPager.EndGame;
begin
  Container.DeleteFrameFunction(PerformFrame,Self);
end;

procedure TDXEinsatzPager.EndMessage(Play: Boolean);
begin
  if fFullMessage=-1 then
    exit;

  if not fCheckBox.Checked then
    Exclude(fPausedTypes,fLastType);

  Parent.DeleteHotKey(#27,nil,GoOnWithPause);
  Parent.DeleteHotKey(' ',nil,GoOnWithPlay);

  Container.IncLock;
  Container.Lock;
  fCheckBox.Visible:=false;
  Container.Unlock;
//  Container.ReactiveAll;
  fFullMessage:=-1;
  Rec.Bool:=false;
  SendVMessage(vmSetUserInterfaceSperre);
  fCollaps:=false;

  // Pause aufheben
  Rec.Bool:=(not Play) and (fEchtZeit);
  SendVMessage(vmSetPause);

  Height:=20;
  Container.DoMouseMessage;
  Container.DecLock;
  Container.DoFlip;
end;

function TDXEinsatzPager.GetCenterButton(Point: TPoint): Integer;
var
  Index: Integer;
begin
  result:=-1;
  if fCollaps and (fFullMessage=-1) then
  begin
    if (Point.X>1) and (Point.X<19) then
    begin
      Index:=fMessages.Count-((fScrollBar.Value+Point.Y-2) div 18)-1;
      Point.Y:=(fScrollBar.Value+Point.Y-2) mod 18;
      if (Point.Y<17) and (Index>=0) then
      begin
        if fMessages.Objects[Index]<>nil then
          result:=Index;
      end;
    end;
  end;
  if (fFullMessage<>-1) then
  begin
    if PtInRect(Rect(2,4,17,19),Point) then
      result:=fFullMessage;
  end;
end;

procedure TDXEinsatzPager.GoOnWithPause(Sender: TObject);
begin
  SendVMessage(vmReleaseEscButton);

  EndMessage(false);
end;

procedure TDXEinsatzPager.GoOnWithPlay(Sender: TObject);
begin
  EndMessage(true);
end;

procedure TDXEinsatzPager.LoadCustomSaveData(Stream: TStream);
var
  MTypes : TEinsatzMessages;
begin
  Stream.Read(MTypes,SizeOf(MTypes));
  PauseBei:=MTypes;
end;

procedure TDXEinsatzPager.MouseDown(Button: TMouseButton; X, Y: Integer);
var
  CenterButton: Integer;
  Info        : PMessageInfo;
  PagerButton : TPagerButton;
begin
  inherited;
  if fFullMessage<>-1 then
  begin
    PagerButton:=OverPagerButton(Point(X,Y));
    if PagerButton<>pbNone then
    begin
      Rec.Bool:=PagerButton=pbPlay;
      SendVMessage(vmGoOnAfterMessage);
      exit;
    end;
  end
  else if OverCollapsButton(Point(X,Y)) then
  begin
    Collaps:=not Collaps;
    exit;
  end;

  CenterButton:=GetCenterButton(Point(X,Y));
  if CenterButton<>-1 then
  begin
    Info:=PMessageInfo(fMessages.Objects[CenterButton]);
    if Info.MessageType in [emReloadWeapon,emNoMunition] then
      Rec.Point:=Point(Info.Figure.XPos,Info.Figure.YPos)
    else
      Rec.Point:=Info.Point;
    SendVMessage(vmCenterPoint);
  end;
end;

procedure TDXEinsatzPager.MouseLeave;
begin
  inherited;
  if fOverFull or (fCenterButton<>-1) or (fPagerButton<>pbNone) then
  begin
    fOverFull:=false;
    fPagerButton:=pbNone;
    fCenterButton:=-1;
    Redraw;
  end;
end;

procedure TDXEinsatzPager.MouseMove(X, Y: Integer);
var
  OldCenter  : Integer;
  OldCollaps : Boolean;
  OldButton  : TPagerButton;
  Change     : Boolean;
begin
  OldCollaps:=fOverFull;
  if fFullMessage<>-1 then
    fOverFull:=false
  else
    fOverFull:=OverCollapsButton(Point(X,Y));
  Change:=(fOverFull<>OldCollaps);

  OldCenter:=fCenterButton;
  fCenterButton:=GetCenterButton(Point(X,Y));
  Change:=Change or (OldCenter<>fCenterButton);

  OldButton:=fPagerButton;
  fPagerButton:=OverPagerButton(Point(X,Y));
  Change:=Change or (OldButton<>fPagerButton);

  if Change then
    Redraw;
end;

procedure TDXEinsatzPager.NewGameHandler(Sender: TObject);
begin
  PauseBei:=[low(TEinsatzMessage)..high(TEinsatzMessage)]
end;

procedure TDXEinsatzPager.OnScroll(Sender: TObject);
begin
  Redraw;
end;

function TDXEinsatzPager.OverCollapsButton(Point: TPoint): boolean;
begin
  result:=PtInRect(Rect(Left+fTextWidth-30,0,Left+fTextWidth,20),Point);
end;

function TDXEinsatzPager.OverPagerButton(Point: TPoint): TPagerButton;
begin
  result:=pbNone;
  if fFullMessage=-1 then exit;
  if fEchtzeit then
  begin
    if PtInRect(Rect(Right-32,2,Right-2,23),Point) then
      result:=pbPause
    else if PtInRect(Rect(Right-62,2,Right-32,23),Point) then
      result:=pbPlay;
  end
  else
  begin
    if PtInRect(Rect(Right-32,2,Right-2,23),Point) then
      result:=pbPlay;
  end;
end;

function TDXEinsatzPager.PerformFrame(Sender: TObject;
  Frames: Integer): boolean;
var
  Dummy: Integer;
  Info : PMessageInfo;
begin
  result:=false;
  for Dummy:=0 to fMessages.Count-1 do
  begin
    Info:=PMessageInfo(fMessages.Objects[Dummy]);
    if Info.Alpha>75 then
    begin
      dec(Info.Alpha,Frames*2);
      fNeedRedraw:=true;
    end;
  end;
  if (Parent<>Container.ActivePage) or (fFullMessage<>-1) then exit;
  if fMessage=-1 then
  begin
    if fNeedRedraw then
      Redraw;
    result:=true;
    exit;
  end;
  if fTop<>0 then
  begin
    dec(fTop,2);
    if fTop=-20 then
    begin
      fShowTime:=-1;
      inc(fMessage);
      if fMessage=fMessages.Count then
        fMessage:=-1
      else
      begin
        fShowTime:=(length(fMessages[fMessage])*10);
        fTop:=20;
      end;
    end;
    if not fCollaps then
    begin
      Redraw;
      result:=true;
    end;
  end
  else
  begin
    if fShowTime>0 then dec(fShowTime,50);
    if fShowTime<=0 then
    begin
      dec(fTop,2);
    end;
  end;
  if fNeedRedraw then
  begin
    Redraw;
    result:=true;
  end;
end;

procedure TDXEinsatzPager.Redraw;
begin
  if not Visible then exit;
  if Container.IsLoadGame then
  begin
    fNeedRedraw:=true;
    exit;
  end;
  Container.Surface.ClearClipRect;
  fNeedRedraw:=false;
  Container.RedrawBlackControl(Self,Container.Surface);
  if fScrollBar.Visible then Container.RedrawBlackControl(fScrollBar,Container.Surface);
  if fCheckBox.Visible then Container.RedrawBlackControl(fCheckBox,Container.Surface);
end;

procedure TDXEinsatzPager.RedrawRect(DrawRect: TRect;
  Surface: TDirectDrawSurface; var Mem: TDDSurfaceDesc);
var
  Text      : string;
  Y         : Integer;
  Dummy     : Integer;
  Info      : PMessageInfo;
  BRight    : Integer;

  procedure DrawHint(Pos: TPoint; Text: String; Right: Boolean = false; HotKey : Char = #0);
  var
    TxtWidth: Integer;
  begin
    if HotKey<>#0 then
      TxtWidth:=WhiteStdFont.TextWidth(Text+' ()'+ui_utils_GetText(HotKey))
    else
      TxtWidth:=WhiteStdFont.TextWidth(Text);

    if Right then
      dec(Pos.X,TxtWidth+3);

    BlendRectangle(Rect(Pos.X+1,Pos.Y+1,Pos.X+TxtWidth+4,Pos.Y+15),200,bcDisabled,Surface,Mem);
    Rectangle(Surface,Mem,Rect(Pos.X,Pos.Y,Pos.X+TxtWidth+5,Pos.Y+16),bcDisabled);
    WhiteStdFont.Draw(Surface,Pos.X+2,Pos.Y,Text);
    if HotKey<>#0 then
      YellowStdFont.Draw(Surface,Pos.X+2+WhiteStdFont.TextWidth(Text),Pos.Y,' ('+ui_utils_GetText(HotKey)+')');
  end;

begin
  Surface.FillRect(DrawRect,bcBlack);
  Surface.Lock(Mem);
  Surface.Unlock;
  Rectangle(Surface,Mem,ClientRect,bcMaroon);
  if fFullMessage<>-1 then
  begin
    Dummy:=fFullMessage;
    YellowBStdFont.Draw(Surface,Left+20,Top+5,fMessages[Dummy]);
    // Pausebutton
    if fEchtzeit then
    begin
      if fPagerButton=pbPause then
      begin
        BlendRectangle(Rect(Right-31,Top+3,Right-3,Top+21),100,bcMaroon,Surface,Mem);
        Rectangle(Surface,Mem,Right-32,Top+2,Right-2,Top+23,bcMaroon);
      end;
      Surface.Draw(Right-24,Top+5,PauseRect,Container.ImageList.Items[1].PatternSurfaces[0]);
      BRight:=Right-61;
    end
    else
      BRight:=Right-31;

    // Weiterspielen
    if fPagerButton=pbPlay then
    begin
      BlendRectangle(Bounds(BRight,Top+3,28,18),100,bcMaroon,Surface,Mem);
      Rectangle(Surface,Mem,BRight-1,Top+2,BRight+29,Top+23,bcMaroon);
    end;
    Surface.Draw(BRight+12,Top+5,PlayRect,Container.ImageList.Items[1].PatternSurfaces[0]);

    if fMessages.Objects[Dummy]<>nil then
    begin
      if fCenterButton=Dummy then
      begin
        BlendRectangle(Rect(Left+3,Top+5,Left+16,Top+18),150,bcMaroon,Surface,Mem);
        Rectangle(Surface,Mem,Left+2,Top+4,Left+17,Top+19,bcMaroon);
        DrawHint(Point(Left+20,Top+5),ST0309210018,false,KeyConfiguration.BodenKeys.Camera);
      end;
      Surface.Draw(Left+3,Top+5,CenterRect,Container.ImageList.Items[1].PatternSurfaces[0]);
    end;

    HLine(Surface,Mem,Left+1,Right-1,Top+24,bcRed);

    // Zeichnen der Hints
    case fPagerButton of
      pbPause : DrawHint(Point(Right-35,Top+4),ST0309210019,true,#27);
      pbPlay  : DrawHint(Point(BRight-4,Top+4),ST0309210020,true,KeyConfiguration.BodenKeys.Pause);
    end;

    if fCenterButton=Dummy then
      DrawHint(Point(Left+20,Top+5),ST0309210018,false,KeyConfiguration.BodenKeys.Camera);

    exit;
  end
  else if fCollaps then
  begin
    Y:=Top+1;
    if fScrollBar.Value>0 then
      dec(Y,fScrollBar.Value);
    Surface.ClippingRect:=ResizeRect(ClientRect,1);
    for Dummy:=fMessages.Count-1 downto 0 do
    begin
      Info:=PMessageInfo(fMessages.Objects[Dummy]);

      Surface.FillRect(Rect(Left+20,Y+1,Left+fTextWidth,Y+18),draw_utils_GetAlphaColor(Mem,bcMaroon,Info.Alpha));
      Surface.FillRect(Rect(Left+2,Y+1,Left+19,Y+18),draw_utils_GetAlphaColor(Mem,bcMaroon,Info.Alpha));

      WhiteStdFont.Draw(Surface,Left+20,Y+2,fMessages[Dummy]);
      if fMessages.Objects[Dummy]<>nil then
      begin
        if fCenterButton=Dummy then
        begin
//          BlendRectangle(Rect(Left+3,Y+1,Left+16,Y+14),150,bcMaroon,Surface,Mem);
          Rectangle(Surface,Mem,Left+2,Y+1,Left+19,Y+18,bcRed);
          DrawHint(Point(Left+20,Y+2),ST0309210018);
        end;
        Surface.Draw(Left+4,Y+3,CenterRect,Container.ImageList.Items[1].PatternSurfaces[0]);
      end;
      inc(Y,18);
      if Y>ScreenHeight then break;
    end;
    Surface.ClearClipRect;
  end
  else if fMessage>-1 then
    WhiteStdFont.DrawInRect(Surface,ResizeRect(ClientRect,1),Left+2,Top+3+fTop,fMessages[fMessage]);
  if fOverFull then
  begin
    BlendRectangle(Rect(Left+fTextWidth-29,Top+1,Left+fTextWidth-1,Top+19),100,bcMaroon,Surface,Mem);
    VLine(Surface,Mem,Top,Top+19,Left+fTextWidth-30,bcMaroon);
    if fCollaps then
    begin
      HLine(Surface,Mem,Left+fTextWidth-30,Left+fTextWidth,Top+19,bcMaroon);
      Text:=ST0309210021;
    end
    else
      Text:=ST0309210022;
    DrawHint(Point(Left+fTextWidth-33,Top+2),Text,true);
  end;
  if fCollaps then
    Surface.Draw(Left+fTextWidth-20,Top+3,FlippedRect,Container.ImageList.Items[1].PatternSurfaces[0])
  else
    Surface.Draw(Left+fTextWidth-20,Top+3,ImageRect,Container.ImageList.Items[1].PatternSurfaces[0]);
end;

procedure TDXEinsatzPager.Resize(var NewLeft, NewTop, NewWidth,
  NewHeight: Integer);
begin
  inherited;
  fScrollBar.SetRect((NewLeft+NewWidth)-17,NewTop,17,NewHeight);
end;

procedure TDXEinsatzPager.SaveCustomSaveData(Stream: TStream);
var
  MTypes : TEinsatzMessages;
begin
  MTypes:=PauseBei;
  Stream.Write(MTypes,SizeOf(MTypes))
end;

procedure TDXEinsatzPager.SendMessage(Text: String; Point: TPoint; MessageType: TEinsatzMessage; Figure: TGameFigure);
var
  SPoint: PMessageInfo;
begin
  if Point.X<>-1 then
  begin
    GetMem(SPoint,SizeOf(TMessageInfo));
    SPoint.MessageType:=MessageType;
    SPoint.Point:=Point;
    SPoint.Figure:=Figure;
    SPoint.Alpha:=225;
  end
  else
    SPoint:=nil;

  fMessages.AddObject(TimeToStr(Time)+' '+Text,TObject(SPoint));

  // Pr�fen, ob das eine Nachricht ist bei der das Spiel angehalten werden soll
  if (MessageType in fPausedTypes) and (fFullMessage=-1) then
  begin
    fFullMessage:=fMessages.Count-1;

    fLastType:=MessageType;
    Rec.Text:='MSG in';
    SendVMessage(vmPlaySound);

    Rec.Bool:=true;
    SendVMessage(vmSetPause);
    SendVMessage(vmSetUserInterfaceSperre);

//    Container.DeactiveAll;
//    ReactiveControl;
    fCheckBox.SetRect(Left+10,Top+38,250,23);
    fCheckBox.Checked:=true;
    fCheckBox.Visible:=true;
//    fCheckBox.ReactiveControl;
    Container.UnlockAll;
    Container.IncLock;
    Collaps:=false;
    Height:=ScreenHeight-Top;
    Container.DoMouseMessage;
    Container.DecLock;
    Redraw;
    Container.LockAll;
    fCollaps:=true;

    // Registrierung des Hotkeys Escape um das Spiel im Pausemodus fortzusetzen

    // Hotkey zur normalen Fortsetzung braucht nicht Registriert zu werden
    // Die Verarbeitung �bernimmt DXUIBodeneinsatz (normale Verarbeitung Pause-Hotkey)

    Parent.RegisterHotKey(#27,nil,GoOnWithPause);
    Parent.RegisterHotKey(' ',nil,GoOnWithPlay);

{    if Wait then
    begin
      Container.UnlockAll;
      while fFullMessage<>-1 do
        Application.HandleMessage;
    end;}
  end
  else if fShowTime=-1 then
  begin
    fMessage:=fMessages.Count-1;
    fShowTime:=(length(Text)*10);
    fTop:=20;
  end;
  if fCollaps and (fFullMessage=-1) then
  begin
    Container.Lock;
    fScrollBar.Visible:=(fMessages.Count>4);
    if fScrollBar.Visible then
    begin
      fScrollBar.Max:=((fMessages.Count-4)*18)+2;
      fScrollBar.Value:=0;
      fTextWidth:=Width-fScrollBar.Width;
    end;
    Container.Unlock;
    Redraw;
  end;
end;

procedure TDXEinsatzPager.SetCollaps(const Value: Boolean);
begin
  if fCollaps=Value then exit;
  Container.IncLock;
  fCollaps:=Value;
  Container.Lock;
  fScrollBar.Max:=((fMessages.Count-4)*18)+3;
  if fScrollBar.Value<0 then
    fScrollBar.Value:=0;
  Container.Unlock;
  fScrollBar.Visible:=fCollaps and (fMessages.Count>4);
  if fScrollBar.Visible then
    fTextWidth:=Width-fScrollBar.Width
  else
    fTextWidth:=Width;

  if fCollaps then
    Height:=ScreenHeight-Top
  else
    Height:=20;
  Container.Declock;
end;

procedure TDXEinsatzPager.StartGame(Echtzeit: Boolean);
begin
  Container.Lock;
  fEchtzeit:=Echtzeit;
  fShowTime:=-1;
  fScrollBar.Value:=0;
  fCenterButton:=-1;
  ClearMessages;
  fMessage:=-1;
  Container.DeleteFrameFunction(PerformFrame,Self);
  Container.AddFrameFunction(PerformFrame,Self,50);
  fCollaps:=true;
  fFullMessage:=-1;
  fPagerButton:=pbNone;
  Collaps:=false;
  fCheckBox.Visible:=false;
  Container.Unlock;
  Container.Inclock;
  Height:=20;
  Container.DecLock;
end;

end.
