{********************************************************************************
*										*
* Copyright (C) 2004  X-Force Team - jim_raynor@web.de				*
*										*
* This file is part of X-Force: Fight For Destiny (X-Force).			*
*										*
* X-Force is free software; you can redistribute it and/or modify		*
* it under the terms of the GNU General Public License as published by		*
* the Free Software Foundation; either version 2 of the License, or		*
* (at your option) any later version.						*
*										*
* X-Force is distributed in the hope that it will be useful,			*
* but WITHOUT ANY WARRANTY; without even the implied warranty of		*
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the			*
* GNU General Public License for more details.					*
*										*
* You should have received a copy of the GNU General Public License		*
* along with X-Force; if not, write to the Free Software			*
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA	*
*										*
*********************************************************************************
*										*
* VERALTET. Stellte fr�her die Anzeige der Nachrichten im Hauptmen� dar		*
*										*
********************************************************************************}

{$I ../Settings.inc}

unit DXPager;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  DXListBox, DXContainer,DXDraws, NGTypes, math, Blending, KD4Utils, DirectDraw,
  DirectFont, TraceFile, StringConst;

type
  TDXPager = class(TDXListBox)
  private
    fMaxMessage      : Integer;
    fLineColor       : TColor;
    fShowMessage     : TShowMessageBox;
    fSave            : boolean;
    fRound           : Integer;
    procedure SetMaxMessage(const Value: Integer);
    function GetCountMessage: Integer;
    procedure SetLineColor(const Value: TColor);
    procedure CorrectMessages;
    function NewMessage(Index: Integer): boolean;
    function IsMessageText(Index: Integer): boolean;
    { Private-Deklarationen }
  protected
    procedure DrawItem(Surface: TDirectDrawSurface;const Mem: TDDSurfaceDesc; Rect: TRect; Index: Integer);override;
    procedure DrawHeadRow(Surface: TDirectDrawSurface;const Mem: TDDSurfaceDesc);override;
    { Protected-Deklarationen }
  public
    constructor Create(Page: TDXPage);override;
    function NextRound(Minuten: Integer): boolean;
    procedure AddMessage(Text: String;Date: String);
    procedure MouseDown(Button: TMouseButton;X,Y: Integer);override;
    procedure DeleteMessage(Index: Integer);
    function SelectedMessage: Integer;
    function ExtractMessageText(Index: Integer): String;
    property MaxMessages  : Integer read fMaxMessage write SetMaxMessage;
    property MessageCount : Integer read GetCountMessage;
    property LineColor    : TColor read fLineColor write SetLineColor;
    property ShowMessage  : TShowMessageBox read fShowMessage write fShowMessage;
    property SaveDelete   : boolean read fSave write fSave;
    property Round        : Integer read fRound write fRound;
  end;

implementation

{ TDXPager }

procedure TDXPager.AddMessage(Text, Date: String);
var
  Dummy       : Integer;
  Line        : String;
  Word        : String;
  Ch          : Char;
  LineInsert  : Integer;
  LineWidth   : Integer;
  Added       : boolean;
  Bis         : Integer;

  procedure InsertLine(Index: Integer;Line: String);
  begin
    if Line='' then exit;
    if Line=#10 then
    begin
      Items[Index-1]:=Items[Index-1]+#10;
      exit;
    end;
    if Items.Count=1 then
      Items.Add(Line)
    else
      Items.Insert(Index,Line);
    Items.Objects[Index]:=Pointer(fRound);
    if Items.Count=0 then LineInsert:=0;
    inc(LineInsert);
  end;

begin
  LineInsert:=-1;
  if Items.Count<>0 then InsertLine(0,#255);
  ChangeItem(true);
  Container.IncLock;
  InsertLine(0,Date);
  Added:=false;
  LineWidth:=0;
  Bis:=length(Text);
  for Dummy:=1 to Bis do
  begin
    Ch:=Text[Dummy];
    if ch=#10 then
    begin
      Line:=Line+Word+#10;
      Word:='';
    end
    else Word:=Word+Ch;
    Added:=false;
    if (Ch=#10) or (Ch=' ') then
    begin
      LineWidth:=LineWidth+WhiteStdFont.TextWidth(Word);
      if (Ch=#10) or (LineWidth>Width-50) then
      begin
        InsertLine(LineInsert,Line);
        LineWidth:=WhiteStdFont.TextWidth(Word);
        Line:='';
        Added:=true;
      end;
      Line:=Line+Word;
      Word:='';
    end;
  end;
  if not Added then
  begin
    LineWidth:=LineWidth+WhiteStdFont.TextWidth(Word);
    if LineWidth>Width-22 then
    begin
      InsertLine(LineInsert,Line);
      Line:='';
    end;
    Line:=Line+Word;
    InsertLine(LineInsert,Line);
  end;
  CorrectMessages;
  Container.DecLock;
  ChangeItem(false);
  ItemsChange(Self);
  ItemIndex:=0;
end;

procedure TDXPager.CorrectMessages;
var
 Ch: Char;
begin
  if Items.Count=0 then exit;
  while MessageCount>MaxMessages do
  begin
    repeat
      Ch:=Items[Items.Count-1][1];
      Items.Delete(Items.Count-1);
    until Ch=#255;
  end;
end;

constructor TDXPager.Create(Page: TDXPage);
begin
  inherited;
  fMaxMessage:=15;
  HeadData:=MakeHeadData(LMessages,FMessage,FMessages);
  AlwaysChange:=true;
  fRound:=0;
end;

procedure TDXPager.DeleteMessage(Index: Integer);
var
  Dummy: Integer;
  AktMessage: Integer;
begin
  if Items.Count=0 then exit;
  Container.IncLock;
  Container.LoadGame(true);
  AktMessage:=1;
  Dummy:=0;
  repeat
    if Items[Dummy][1]=#255 then inc(AktMessage);
    inc(Dummy);
  until (Dummy>Items.Count-1) or (AktMessage=Index);
  dec(Dummy);
  if Dummy>Items.Count-1 then
  begin
    Container.DecLock;
    Container.LoadGame(false);
    Redraw;
    exit;
  end;
  repeat
    Items.Delete(Dummy);
  until (Dummy>Items.Count-1) or (Items[Dummy][1]=#255);
  if Items.Count=0 then
  begin
    Container.DecLock;
    Container.LoadGame(false);
    Redraw;
    exit;
  end;
  if (Index=1) and (Items[Dummy][1]=#255) then Items.Delete(Dummy);
  Container.DecLock;
  Container.LoadGame(false);
  Redraw;
end;

procedure TDXPager.DrawHeadRow(Surface: TDirectDrawSurface;const Mem: TDDSurfaceDesc);
var
  Text    : String;
begin
  if AlphaElements then BlendRoundRect(ClientRect,150,fBlendColor,Surface,Mem,11,fCorner,Rect(Left,Top,Right,Top+fHeadRowHeight));
  WhiteStdFont.Draw(Surface,Left+6,Top+2,fHeadData.Titel);
  Text:=ZahlString(fHeadData.Einzel,fHeadData.MehrZahl,MessageCount);
  WhiteStdFont.Draw(Surface,Right-6-WhiteStdFont.TextWidth(Text),Top+2,Text);
  Rectangle(Surface,Mem,Left,Top+fHeadRowHeight-1,Right,Top+fHeadRowHeight,AktColor);
end;

procedure TDXPager.DrawItem(Surface: TDirectDrawSurface;const Mem: TDDSurfaceDesc; Rect: TRect; Index: Integer);
var
  LeftMar : Integer;
  Font    : TDirectFont;
  LineRect: TRect;
begin
  if Items[Index][1]=#255 then
  begin
    if NewMessage(Index-1) then BlendRoundRect(ClientRect,100,bcMaroon,Surface,Mem,11,fCorner,Classes.Rect(Rect.Left,Rect.Top,Rect.Right,Rect.Top+10));
    if NewMessage(Index+1) then BlendRoundRect(ClientRect,100,bcMaroon,Surface,Mem,11,fCorner,Classes.Rect(Rect.Left,Rect.Top+12,Rect.Right,Rect.Bottom));
    LineRect:=Classes.Rect(Rect.Left+1,Rect.Top+10,Rect.Right,Rect.Top+12);
    IntersectRect(LineRect,LineRect,Surface.ClippingRect);
    if IsRectEmpty(LineRect) then exit;
    Rectangle(Surface,Mem,LineRect,fLineColor);
  end
  else
  begin
    if NewMessage(Index) then BlendRoundRect(ClientRect,100,bcMaroon,Surface,Mem,11,fCorner,Rect);
    if IsMessageText(Index) then
    begin
      LeftMar:=Rect.Left+20;
      Font:=YellowStdFont;
    end
    else
    begin
      LeftMar:=Rect.Left+4;
      Font:=WhiteStdFont;
    end;
    Font.Draw(Surface,LeftMar,Rect.Top+2,RemoveLineBreaks(Items[Index]));
  end;
end;

function TDXPager.ExtractMessageText(Index: Integer): String;
var
  Dummy        : Integer;
  AktMessage   : Integer;
  First        : boolean;
begin
  if Items.Count=0 then exit;
  AktMessage:=1;
  Dummy:=0;
  First:=true;
  repeat
    if Items[Dummy][1]=#255 then inc(AktMessage);
    inc(Dummy);
  until (Dummy>Items.Count-1) or (AktMessage=Index);
  if Dummy>Items.Count-1 then exit;
  if Items[Dummy-1][1]<>#255 then dec(Dummy);
  repeat
    result:=result+Items[Dummy];
    if First then result:=result+#10+#10;
    First:=false;
    inc(Dummy);
  until (Dummy>Items.Count-1) or (Items[Dummy][1]=#255);
end;

function TDXPager.GetCountMessage: Integer;
var
  Dummy: Integer;
begin
  if Items.Count=0 then
  begin
    result:=0;
    exit;
  end;
  result:=1;
  for Dummy:=0 to Items.Count-1 do
  begin
    if Items[Dummy][1]=#255 then inc(result);
  end;
end;

function TDXPager.IsMessageText(Index: Integer): boolean;
begin
  result:=true;
  if Index=0 then
    result:=false
  else
    if Items[Index-1][1]=#255 then result:=false;
end;

procedure TDXPager.MouseDown(Button: TMouseButton; X, Y: Integer);
var
  Result: TModalResult;
begin
  inherited;
  if Button=mbRight then
  begin
    if Items.Count=0 then exit;
    if fSave then
    begin
      if Assigned(fShowMessage) then fShowMessage(ExtractMessageText(SelectedMessage),CDeleteMessage,true,result);
      if result=mrYes then DeleteMessage(SelectedMessage);
    end
    else DeleteMessage(SelectedMessage);
  end;
end;

function TDXPager.NewMessage(Index: Integer): boolean;
begin
  result:=false;
  if (Index<0) or (Index>Items.Count-1) then exit;
  result:=(fRound-Integer(Items.Objects[Index])<60) and AlphaElements;
end;

function TDXPager.NextRound(Minuten: Integer): boolean;
var
  Dummy    : Integer;
begin
  Dummy:=0;
  while NewMessage(Dummy) do inc(Dummy);
  inc(fRound,Minuten);
  result:=not NewMessage(Dummy);
end;

function TDXPager.SelectedMessage: Integer;
var
  Dummy: Integer;
begin
  result:=1;
  for Dummy:=0 to Items.Count-1 do
  begin
    if Items[Dummy][1]=#255 then inc(result);
    if Dummy=ItemIndex then exit;
  end;
end;

procedure TDXPager.SetLineColor(const Value: TColor);
begin
  fLineColor := Container.Surface.ColorMatch(Value);
  Redraw;
end;

procedure TDXPager.SetMaxMessage(const Value: Integer);
begin
  fMaxMessage:=max(5,Value);
  CorrectMessages;
end;

end.
