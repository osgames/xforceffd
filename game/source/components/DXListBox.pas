{********************************************************************************
*										*
* Copyright (C) 2004  X-Force Team - jim_raynor@web.de				*
*										*
* This file is part of X-Force: Fight For Destiny (X-Force).			*
*										*
* X-Force is free software; you can redistribute it and/or modify		*
* it under the terms of the GNU General Public License as published by		*
* the Free Software Foundation; either version 2 of the License, or		*
* (at your option) any later version.						*
*										*
* X-Force is distributed in the hope that it will be useful,			*
* but WITHOUT ANY WARRANTY; without even the implied warranty of		*
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the			*
* GNU General Public License for more details.					*
*										*
* You should have received a copy of the GNU General Public License		*
* along with X-Force; if not, write to the Free Software			*
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA	*
*										*
*********************************************************************************
*										*
* Diese Unit stellt eine Listebox zur Verf�gung					*
*										*
********************************************************************************}

{$I ../Settings.inc}

unit DXListBox;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  DXContainer,DXDraws, DXScrollBar, NGTypes, extctrls, Blending, TraceFile,
  math, DXClass, XForce_types, DirectDraw, DirectFont;

type
  TDXListBox = class(TDXComponent)
  private
    fOverItem         : TOverItem;
    fLeaveItem        : TThreadMethod;
    fAlphaValue       : Integer;
    procedure SetBorderColor(const Value: TColor);
    procedure SetScrollFont(const Value: TFont);
    procedure SetColor(Index: Integer; const Value: TColor);
    procedure SetItems(const Value: TStringList);
    procedure SetItemHeight(const Value: Integer);
    procedure SetItemIndex(const Value: Integer);
    procedure SetLeftMargin(const Value: Integer);
    procedure SetTopMargin(const Value: Integer);
    procedure SetFocusColor(const Value: TColor);
    procedure SetHeadRowHeight(const Value: Integer);
    procedure SetCorners(const Value: TRoundCorners);

    function GetItemIndex: Integer;
    function GetItemsInWindow: Integer;
    function GetScrollFont: TFont;
    function GetColor(Index: Integer): TColor;

    { Private-Deklarationen }
  protected
    fColor            : TBlendColor;
    fScrollBar        : TDXScrollBar;
    fOriginalWidth    : Integer;
    fItems            : TStringList;
    fItemHeight       : Integer;
    fItemIndex        : Integer;
    fLeftMargin       : Integer;
    fTopMargin        : Integer;
    fChange           : TNotifyEvent;
    fDrawItem         : TDXOwnerDrawEvent;
    fOwnerDraw        : boolean;
    fFocusColor       : TBlendColor;
    fScrollDown       : boolean;
    fAlways           : boolean;
    fItemChange       : Integer;
    fItemReSize       : boolean;
    fMainHeight       : Integer;
    fHeadRowHeight    : Integer;
    fDrawHeadRow      : TDXDrawHeadRowEvent;
    fBlendColor       : TBlendColor;
    fHeadData         : THeadData;
    fCorners          : TRoundCorners;
    fLastOver         : Integer;
    fSelectRegion     : HRGN;
    fDrawItems        : Boolean;
    fCorner           : TCorners;
    fFullCorner       : TCorners;
    AktColor          : TBlendColor;  // Farbe die aktuell zum Zeichnen genutzt wird
    procedure DrawItem(Surface: TDirectDrawSurface;const Mem: TDDSurfaceDesc; Rect: TRect; Index: Integer);virtual;
    procedure DrawHeadRow(Surface: TDirectDrawSurface;const Mem: TDDSurfaceDesc);virtual;
    procedure ItemsChange(Sender: TObject);
    procedure OnScroll(Sender: TObject);
    procedure ScrollClick(Sender: TObject);
    procedure DoScroll(Direction: TScrollDirection; Pos: TPoint);override;
    procedure SetComponentHint(const Value: String);override;
    procedure CreateMouseRegion;override;

    function ItemAtLine(Y: Integer): Integer;
    function TimeScroll(Sender: TObject;Frames: Integer): boolean;
    function GetHeaderSize: Integer;
    function TopOfItem(Index: Integer): Integer;
    procedure EnsureItemIsVisible(Index: Integer);

    procedure DrawSelectionRect(Canvas: TCanvas;ItemRect: TRect);
    { Protected-Deklarationen }
  public
    constructor Create(Page: TDXPage);override;
    destructor Destroy;override;
    function GetFrameRect: TRect;
    function GetCorners  : TCorners;
    procedure SetScrollBarToTop;
    procedure KeyDown(var Key: Word;State: TShiftState);override;
    procedure MouseDown(Button: TMouseButton;X,Y: Integer);override;
    procedure MouseMove(X,Y: Integer);override;
    procedure MouseUp(Button: TMouseButton;X,Y: Integer);override;
    procedure MouseLeave;override;
    procedure Resize(var NewLeft,NewTop,NewWidth,NewHeight: Integer);override;
    procedure VisibleChange;override;
    procedure ChangeItem(Change: boolean);
    procedure EnableChange;override;
    procedure Redraw;override;
    procedure Draw(Surface: TDirectDrawSurface;var Mem: TDDSurfaceDesc);override;
    procedure ReDrawRect(DrawRect: TRect;Surface: TDirectDrawSurface;var Mem: TDDSurfaceDesc);override;
    procedure DrawSelection(Surface: TDirectDrawSurface;const Mem: TDDSurfaceDesc;Rect: TRect;Color: TBlendColor);virtual;
    procedure ViewAtTop(Index: Integer);
    property ItemsInWindow   : Integer read GetItemsInWindow;
    property ScrollBar       : TDXScrollBar read fScrollBar;
    property BorderColor     : TColor write SetBorderColor;
    property AlphaValue      : Integer read fAlphaValue write fAlphaValue;
    property FocusColor      : TColor write SetFocusColor;
    property ScrollBarFont   : TFont read GetScrollFont write SetScrollFont;
    property FirstColor      : TColor index 0 read GetColor write SetColor;
    property SecondColor     : TColor index 1 write SetColor;
    property Items           : TStringList read fItems write SetItems;
    property ItemHeight      : Integer read fItemHeight write SetItemHeight;
    property ItemIndex       : Integer read GetItemIndex write SetItemIndex;
    property LeftMargin      : Integer read fLeftMargin write SetLeftMargin;
    property TopMargin       : Integer read fTopMargin write SetTopMargin;
    property OnChange        : TNotifyEvent read fChange write fChange;
    property OnDrawItem      : TDXOwnerDrawEvent read fDrawItem write fDrawItem;
    property OwnerDraw       : boolean read fOwnerDraw write fOwnerDraw;
    property OnOverItem      : TOverItem read fOverItem write fOverItem;
    property OnLeaveItem     : TThreadMethod read fLeaveItem write fLeaveItem;
    property AlwaysChange    : boolean read fAlways write fAlways;
    property HeadRowHeight   : Integer read fHeadRowHeight write SetHeadRowHeight;
    property OnDrawHeadRow   : TDXDrawHeadRowEvent read fDrawHeadRow write fDrawHeadRow;
    property HeadBlendColor  : TBlendColor read fBlendColor write fBlendColor;
    property HeadData        : THeadData read fHeadData write fHeadData;
    property RoundCorners    : TRoundCorners read fCorners write SetCorners;
    { Published-Deklarationen }
  end;

implementation

uses KD4Utils;
{ TDXListBox }

procedure TDXListBox.ChangeItem(Change: boolean);
begin
  if Change then
    inc(fItemChange)
  else
  begin
    if fItemChange>0 then
    begin
      dec(fItemChange);
      if fItemChange=0 then
      begin
        ItemIndex:=ItemIndex;
        ItemsChange(Self);
      end;
    end;
  end;
end;

constructor TDXListBox.Create(Page: TDXPage);
begin
  inherited;
  fBlendColor:=bcMaroon;
  fScrollBar:=TDXScrollBar.Create(Page);
  fScrollBar.Kind:=sbVertical;
  fScrollBar.Visible:=false;
  fScrollBar.OnChange:=OnScroll;
  fScrollBar.OnClick:=ScrollClick;
  fScrollBar.RoundCorners:=rcRight;
  fScrollBar.Owner:=Self;
  RoundCorners:=rcall;
  fItems:=TStringList.Create;
  fItems.OnChange:=ItemsChange;
  fItemHeight:=20;
  fScrollBar.Value:=0;
  fLeftMargin:=4;
  fItemChange:=0;
  fTopMargin:=2;
  fAlphaValue:=100;
  TabStop:=true;
end;

procedure TDXListBox.CreateMouseRegion;
var
  FrameRect: TRect;
  RoundRGN : HRGN;
begin
  DeleteObject(fSelectRegion);
  FrameRect:=GetFrameRect;
  RoundRGN:=CreateRoundRectRgn(FrameRect.Left,FrameRect.Top,FrameRect.Right+1,FrameRect.Bottom+1,20,20);
  fMouseRegion:=CreateRectRgn(Left,Top,Right,Bottom);
  CombineRgn(fMouseRegion,fMouseRegion,RoundRGN,RGN_AND);
  DeleteObject(RoundRGN);
  RoundRGN:=CreateRoundRectRgn(FrameRect.Left+1,FrameRect.Top+1,FrameRect.Right,FrameRect.Bottom,20,20);
  fSelectRegion:=CreateRectRgn(Left+1,Top+GetHeaderSize,Right,Bottom-1);
  CombineRgn(fSelectRegion,fSelectRegion,RoundRGN,RGN_AND);
  DeleteObject(ROUNDRGN);
end;

destructor TDXListBox.Destroy;
begin
  fItems.Free;
  DeleteObject(fSelectRegion);
  fScrollBar.Free;
  inherited;
end;

procedure TDXListBox.DoScroll(Direction: TScrollDirection; Pos: TPoint);
begin
  if not fScrollBar.Visible then exit;
  Container.IncLock;
  if Direction=sdUp then ScrollBar.Value:=ScrollBar.Value-(ScrollBar.SmallChange shl 1);
  if Direction=sdDown then ScrollBar.Value:=ScrollBar.Value+(ScrollBar.SmallChange shl 1);
  Container.DecLock;
  Container.DoMouseMessage;
end;

procedure TDXListBox.Draw(Surface: TDirectDrawSurface;var Mem: TDDSurfaceDesc);
begin
  ReDrawRect(ClientRect,Surface,Mem);
end;

procedure TDXListBox.DrawHeadRow(Surface: TDirectDrawSurface;const Mem: TDDSurfaceDesc);
var
  Text     : String;
begin
  if (fHeadRowHeight=0) then exit;
  if AlphaElements then BlendRoundRect(ClientRect,150,fScrollBar.FirstColor,Surface,Mem,11,fCorner,Rect(Left,Top,Right,Top+fHeadRowHeight));
  if Assigned(fDrawHeadRow) then
    fDrawHeadRow(Self,Surface,Rect(Left,Top,Right,Top+fHeadRowHeight))
  else
  begin
    WhiteStdFont.Draw(Surface,Left+6,Top+2,fHeadData.Titel);
    Text:=ZahlString(fHeadData.Einzel,fHeadData.MehrZahl,Items.Count);
    WhiteStdFont.Draw(Surface,Right-6-WhiteStdFont.TextWidth(Text),Top+2,Text);
  end;
  HLine(Surface,Mem,Left,Right-1,Top+fHeadRowHeight-1,AktColor);
end;

procedure TDXListBox.DrawItem(Surface: TDirectDrawSurface;const Mem: TDDSurfaceDesc; Rect: TRect;
  Index: Integer);
var
  Font: TDirectFont;
begin
  if not fOwnerDraw or not Assigned(fDrawItem) then
  begin
    if (Index=fItemIndex) then
    begin
      if AlphaElements then
        BlendRoundRect(ClientRect,100,fScrollBar.FirstColor,Surface,Mem,11,fCorner,Rect)
      else
      begin
        DrawSelectionRect(Surface.Canvas,Rect);
        Surface.Canvas.Release;
      end;
      Font:=WhiteStdFont;
    end
    else
      Font:=YellowStdFont;
    Font.Draw(Surface,Rect.Left+fLeftMargin-1,Rect.Top+fTopMargin,fItems[Index]);
  end
  else
    fDrawItem(Self,Surface,Mem,Rect,Index,(Index=fItemIndex));
  HLine(Surface,Mem,Rect.Left,Rect.Right,Rect.Bottom-1,fColor);
end;

procedure TDXListBox.DrawSelectionRect(Canvas: TCanvas;ItemRect: TRect);
var
  SelRegion: HRGN;
begin
  Canvas.Brush.Color:=clWhite;
  HighLightRect(ItemRect);
  if fScrollBar.Visible then inc(ItemRect.Right);
  SelRegion:=CreateRectRgn(ItemRect.Left,ItemRect.Top,ItemRect.Right,ItemRect.Bottom);
  CombineRgn(SelRegion,SelRegion,fSelectRegion,RGN_AND);
  FrameRgn(Canvas.Handle,SelRegion,Canvas.Brush.Handle,1,1);
  Canvas.Brush.Style:=bsClear;
  DeleteObject(SelRegion);
end;

procedure TDXListBox.DrawSelection(Surface: TDirectDrawSurface;
  const Mem: TDDSurfaceDesc; Rect: TRect; Color: TBlendColor);
begin
  if AlphaElements then
    BlendRoundRect(ClientRect,100,Color,Surface,Mem,11,fCorner,Rect)
  else
  begin
    DrawSelectionRect(Surface.Canvas,Rect);
    Surface.Canvas.Release;
  end;
end;

procedure TDXListBox.EnableChange;
begin
  inherited;
  fScrollBar.Enabled:=Enabled;
end;

procedure TDXListBox.EnsureItemIsVisible(Index: Integer);
var
  TopBorder     : Integer;
  BottomBorder  : Integer;
begin
  TopBorder:=Index*fItemHeight;
  if fScrollBar.Value>TopBorder then
  begin
    fScrollBar.Value:=TopBorder;
    exit;
  end;
  BottomBorder:=(Index+1)*fItemHeight;
  if fScrollBar.Value+fScrollBar.LargeChange<=BottomBorder then
    fScrollBar.Value:=BottomBorder-fScrollBar.LargeChange;
end;

function TDXListBox.GetColor(Index: Integer): TColor;
begin
  result:=clBlack;
  case Index of
    0: result:=fScrollBar.FirstColor;
  end;
end;

function TDXListBox.GetFrameRect: TRect;
begin
  result:=Bounds(Left,Top,fOriginalWidth,Height);
  if fCorners in [rcLeft,rcLeftTop,rcLeftBottom,rcNone] then inc(result.Right,20);
  if fCorners in [rcTop,rcLeftTop,rcRightTop,rcNone] then inc(result.Bottom,20);
  if fCorners in [rcRight,rcRightTop,rcRightBottom,rcNone] then dec(result.Left,20);
  if fCorners in [rcBottom,rcLeftBottom,rcRightBottom,rcNone] then dec(result.Top,20);
end;

function TDXListBox.GetHeaderSize: Integer;
begin
  if fHeadRowHeight>0 then
    result:=fHeadRowheight
  else
    result:=1;
end;

function TDXListBox.GetItemIndex: Integer;
begin
  if Items.Count=0 then result:=-1 else
  result:=fItemIndex;
end;

function TDXListBox.GetItemsInWindow: Integer;
begin
  if fItemHeight=0 then result:=-1 else
  begin
    result:=(fMainHeight div fItemHeight);
  end;
end;

function TDXListBox.GetScrollFont: TFont;
begin
  result:=fScrollBar.Font;
end;

function TDXListBox.ItemAtLine(Y: Integer): Integer;
begin
  result:=-1;
  if (Y<GetHeaderSize) or (Y>Height) then exit;
  Y:=(Y-GetHeaderSize)+fScrollBar.Value;
  result:=Y div fItemHeight;
end;

procedure TDXListBox.ItemsChange(Sender: TObject);
var
  NeedHeight: Integer;
begin
  if fItemChange<>0 then exit;
  Container.Lock;
  NeedHeight:=fItems.Count*fItemHeight;
  if NeedHeight>fMainHeight then
  begin
    fScrollBar.Max:=NeedHeight-fMainHeight;
    if (not fScrollBar.Visible) or (not Visible) then
    begin
      fItemReSize:=true;
      fScrollBar.Visible:=true and Visible;
      fCorner:=fCorner-[cRightBottom,cRightTop];
      fScrollBar.Height:=Height;
      Width:=fOriginalWidth;
      fItemReSize:=false;
    end;
  end
  else
  begin
    fScrollBar.Value:=0;
    fScrollBar.Max:=0;
    if fScrollBar.Visible or (not Visible) then
    begin
      fItemReSize:=true;
      fScrollBar.Visible:=false;
      fCorner:=fFullCorner;
      fScrollBar.Height:=0;
      Width:=fOriginalWidth;
      fItemReSize:=false;
    end;
  end;
  fScrollBar.LargeChange:=Height-fHeadRowHeight;
  ItemIndex:=fItemIndex;
  Container.UnLock;
  if not Visible then exit;
  Redraw;
end;

procedure TDXListBox.KeyDown(var Key: Word; State: TShiftState);
begin
  inherited;
  if (ssAlt in State) or (ssShift in State) then exit;
  case Char(Key) of
    #33 : if ItemIndex>0 then ItemIndex:=fItemIndex-ItemsInWindow;
    #34 : if ItemIndex<fItems.Count-1 then ItemIndex:=fItemIndex+ItemsInWindow;
    #35 : if ItemIndex<>fItems.Count-1 then ItemIndex:=fItems.Count-1;
    #36 : if ItemIndex<>0 then ItemIndex:=0;
    #38 : if ItemIndex>0 then ItemIndex:=fItemIndex-1;
    #40 : if ItemIndex<fItems.Count-1 then ItemIndex:=fItemIndex+1;
  end;
end;

procedure TDXListBox.MouseDown(Button: TMouseButton; X, Y: Integer);
var
  NewIndex: Integer;
begin
  inherited;
  NewIndex:=ItemAtLine(Y);
  if NewIndex>fItems.Count-1 then exit;
  ItemIndex:=NewIndex;
  fLastOver:=-1;
  if Assigned(fLeaveItem) then fLeaveItem;
end;

procedure TDXListBox.MouseLeave;
begin
  inherited;
  if fLastOver<>-1 then
  begin
    if Assigned(fLeaveItem) then fLeaveItem;
    fLastOver:=-1;
  end;
end;

procedure TDXListBox.MouseMove(X, Y: Integer);
var
  NewIndex: Integer;
  DTop    : Integer;
begin
  Container.IncLock;
  if GetKeyState(VK_LBUTTON)<0 then
  begin
    if (Y>fHeadRowHeight) and (Y<Height) then
    begin
      Container.DeleteFrameFunction(TimeScroll,nil);
      NewIndex:=ItemAtLine(Y);
      if NewIndex>fItems.Count-1 then
      begin
        Container.DecLock;
        exit;
      end;
      ItemIndex:=NewIndex;
    end
    else if (Y<fHeadRowHeight) then
    begin
      fScrollDown:=false;
      Container.AddFrameFunction(TimeScroll,nil,75);
    end
    else if (Y>=Height) then
    begin
      fScrollDown:=true;
      Container.AddFrameFunction(TimeScroll,nil,75);
    end;
  end
  else
  begin
    if Y<=fHeadRowHeight then
    begin
      fLastOver:=-1;
      if Assigned(fLeaveItem) then fLeaveItem;
      Container.DecLock;
      exit;
    end;
    NewIndex:=ItemAtLine(Y);
    if NewIndex<>fLastOver then
    begin
      if fLastOver<>-1 then
      begin
        Container.IncLock;
        if Assigned(fLeaveItem) then fLeaveItem;
        Container.DecLock;
      end;
      if NewIndex<=fItems.Count-1 then
      begin
        DTop:=TopOfItem(NewIndex);
        if Assigned(fOverItem) then fOverItem(NewIndex,Rect(Left,DTop,Right,DTop+fItemHeight));
      end;
      fLastOver:=NewIndex;
    end;
  end;
  Container.DecLock;
end;

procedure TDXListBox.MouseUp(Button: TMouseButton; X, Y: Integer);
begin
  inherited;
  Container.DeleteFrameFunction(TimeScroll,nil);
end;

procedure TDXListBox.OnScroll(Sender: TObject);
begin
  Container.RedrawArea(ClientRect,Container.Surface);
end;

procedure TDXListBox.Redraw;
begin
  if (not Visible) then
    exit;
  Container.RedrawArea(Bounds(Left,Top,fOriginalWidth,Height),Container.Surface);
end;

procedure TDXListBox.ReDrawRect(DrawRect: TRect;Surface: TDirectDrawSurface;var Mem: TDDSurfaceDesc);
var
  DTop      : Integer;
  TempRect  : TRect;
  Line      : Integer;
  Index     : Integer;
  Ende      : boolean;
  LastIndex : Integer;
begin
  TempRect:=ClientRect;
  inc(TempRect.Top,GetHeaderSize);
  if AlphaControls then
  begin
    IntersectRect(TempRect,DrawRect,Rect(Left,Top+GetHeaderSize,Right,Bottom));
    BlendRoundRect(ClientRect,fAlphaValue,fScrollBar.FirstColor,Surface,Mem,11,fCorner,TempRect);
  end;
  Line:=GetHeaderSize;
  Ende:=false;
  LastIndex:=-1;
  fDrawItems:=true;
  Surface.ClippingRect:=TempRect;
  while not Ende do
  begin
    if Line=Height then
      Ende:=true;
    Index:=ItemAtLine(Line);
    if LastIndex<>Index then
    begin
      if (Index<fItems.Count) then
      begin
        DTop:=TopOfItem(Index);
        TempRect:=Rect(Left,DTop,Right,DTop+fItemHeight);
        if OverlapRect(DrawRect,TempRect) then
          DrawItem(Surface,Mem,TempRect,Index);
      end
      else
        Ende:=true;
    end;
    inc(Line,fItemHeight);
    if (Line>Height) then
    begin
      Line:=Height;
      LastIndex:=Index;
    end;
  end;
  Surface.ClearClipRect;
  fDrawItems:=false;
  TempRect:=ClientRect;
  inc(TempRect.Top,GetHeaderSize);
  if HasFocus then AktColor:=fFocusColor else AktColor:=fColor;
  if OverlapRect(DrawRect,Rect(Left,Top,Right,Top+fHeadRowHeight)) then DrawHeadRow(Surface,Mem);
  TempRect:=GetFrameRect;
  if fOriginalWidth>Width then
    FramingRect(Surface,Mem,Rect(Left,Top,Right+1,Bottom),fCorner,11,AktColor)
  else
    FramingRect(Surface,Mem,Rect(Left,Top,Right,Bottom),fCorner,11,AktColor);
end;

procedure TDXListBox.Resize(var NewLeft: Integer; var NewTop: Integer;
  var NewWidth: Integer;var NewHeight: Integer);
begin
  if not fItemReSize then
    if fHeadRowHeight>NewHeight then fHeadRowHeight:=NewHeight-fItemHeight;
  fMainHeight:=NewHeight-fHeadRowHeight;
  fOriginalWidth:=NewWidth;
  Container.IncLock;
  if fScrollBar.Visible then
  begin
    fScrollBar.SetRect(NewLeft+NewWidth-17,NewTop,17,NewHeight);
    NewWidth:=fOriginalWidth-17;
  end
  else
  begin
    NewWidth:=fOriginalWidth;
    fScrollBar.SetRect(0,0,0,0);
  end;
  if not fItemReSize then
    ItemsChange(Self);
  Container.DecLock;
end;

procedure TDXListBox.ScrollClick(Sender: TObject);
begin
  if TabStop then Container.FocusControl:=Self;
end;

procedure TDXListBox.SetBorderColor(const Value: TColor);
begin
  fColor := Container.Surface.ColorMatch(Value);
  Redraw;
end;

procedure TDXListBox.SetColor(Index: Integer; const Value: TColor);
begin
  case Index of
    0: fScrollBar.FirstColor:=Value;
    1: fScrollBar.SecondColor:=Value;
  end;
end;

procedure TDXListBox.SetComponentHint(const Value: String);
begin
  inherited;
  fScrollBar.Hint:=Value;
end;

procedure TDXListBox.SetCorners(const Value: TRoundCorners);
begin
  fCorners := Value;
  case Value of
    rcLeft,rcLeftTop,rcLeftBottom,rcNone : fScrollBar.RoundCorners:=rcNone;
    rcAll,rcRight                        : fScrollBar.RoundCorners:=rcRight;
    rcBottom                             : fScrollBar.RoundCorners:=rcRightBottom;
    rcTop                                : fScrollBar.RoundCorners:=rcRightTop;
    rcRightTop,rcRightBottom             : fScrollBar.RoundCorners:=Value;
  end;
  fFullCorner:=[];
  if fCorners in [rcLeft,rcTop,rcLeftTop,rcAll] then include(fFullCorner,cLeftTop);
  if fCorners in [rcRight,rcTop,rcRightTop,rcAll] then include(fFullCorner,cRightTop);
  if fCorners in [rcLeft,rcBottom,rcLeftBottom,rcAll] then include(fFullCorner,cLeftBottom);
  if fCorners in [rcRight,rcBottom,rcRightBottom,rcAll] then include(fFullCorner,cRightBottom);
  fCorner:=fFullCorner;
  RecreateMouseRegion;
  Redraw;
end;

procedure TDXListBox.SetFocusColor(const Value: TColor);
begin
  fFocusColor := Container.Surface.ColorMatch(Value);
  fScrollBar.FocusColor:=Value;
  Redraw;
end;

procedure TDXListBox.SetHeadRowHeight(const Value: Integer);
begin
  fHeadRowHeight := Value;
  if fHeadRowHeight>Height then fHeadRowHeight:=Height-fItemHeight;
  fMainHeight:=Height-fHeadRowHeight;
  RecreateMouseRegion;
end;

procedure TDXListBox.SetItemHeight(const Value: Integer);
begin
  fItemHeight := max(2,Value);
  fScrollBar.SmallChange:=fItemHeight;
  ItemsChange(Self);
  Redraw;
end;

procedure TDXListBox.SetItemIndex(const Value: Integer);
var
  OldIndex  : Integer;
begin
  OldIndex:=fItemIndex;
  fItemIndex:=Value;
  if fItemChange<>0 then exit;
  Container.Lock;
  if fItemIndex>Items.Count-1 then fItemIndex:=Items.Count-1;
  if fItemIndex<0 then fItemIndex:=0;
  if (fItemIndex=OldIndex) and (not fAlways) then
  begin
    Container.UnLock;
    exit;
  end;
  EnsureItemIsVisible(Value);
  Container.LoadGame(false);
  if Assigned(fChange) then fChange(Self);
  Container.DecLock;
  Redraw;
end;

procedure TDXListBox.SetItems(const Value: TStringList);
begin
  fItems.Assign(Value);
  ItemsChange(fItems);
  Redraw;
end;

procedure TDXListBox.SetLeftMargin(const Value: Integer);
begin
  fLeftMargin := Value;
  Redraw;
end;

procedure TDXListBox.SetScrollBarToTop;
begin
  Container.LoadGame(True);
  Container.RemoveComponent(fScrollBar);
  Container.AddComponent(fScrollBar);
  Container.LoadGame(false);
end;

procedure TDXListBox.SetScrollFont(const Value: TFont);
begin
  fScrollBar.Font:=Value;
  Redraw;
end;

procedure TDXListBox.SetTopMargin(const Value: Integer);
begin
  fTopMargin := Value;
  Redraw;
end;

function TDXListBox.TimeScroll(Sender: TObject; Frames: Integer): boolean;
begin
  result:=true;
  Container.IncLock;
  if fScrollDown then
  begin
    if ItemIndex<Items.Count then
    begin
      ItemIndex:=ItemIndex+Frames;
    end
    else
    begin
      ItemIndex:=Items.Count-1;
      Container.DeleteFrameFunction(TimeScroll,nil);
    end;
  end
  else
  begin
    if ItemIndex>0 then
    begin
      ItemIndex:=ItemIndex-Frames;
    end
    else
    begin
      ItemIndex:=0;
      Container.DeleteFrameFunction(TimeScroll,nil);
    end;
  end;
  Container.DecLock;
  Redraw;
end;

function TDXListBox.TopOfItem(Index: Integer): Integer;
begin
  result:=Top+fHeadRowHeight;
  result:=result+(Index*fItemHeight)-fScrollBar.Value;
end;

procedure TDXListBox.ViewAtTop(Index: Integer);
begin
  fScrollBar.Value:=Index*fItemHeight;
end;

procedure TDXListBox.VisibleChange;
begin
  inherited;
  Container.IncLock;
  if Visible=false then
  begin
    fScrollBar.Visible:=false;
  end
  else
  begin
    ItemsChange(Self);
  end;
  Container.DecLock;
end;

function TDXListBox.GetCorners: TCorners;
begin
  result:=fCorner;
end;

end.
