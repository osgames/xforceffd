{********************************************************************************
*										*
* Copyright (C) 2004  X-Force Team - jim_raynor@web.de				*
*										*
* This file is part of X-Force: Fight For Destiny (X-Force).			*
*										*
* X-Force is free software; you can redistribute it and/or modify		*
* it under the terms of the GNU General Public License as published by		*
* the Free Software Foundation; either version 2 of the License, or		*
* (at your option) any later version.						*
*										*
* X-Force is distributed in the hope that it will be useful,			*
* but WITHOUT ANY WARRANTY; without even the implied warranty of		*
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the			*
* GNU General Public License for more details.					*
*										*
* You should have received a copy of the GNU General Public License		*
* along with X-Force; if not, write to the Free Software			*
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA	*
*										*
*********************************************************************************
*										*
* Stellt eine Komponente zur Auswahl der Basisposition zur Verf�gung		*
*										*
********************************************************************************}

{$I ../Settings.inc}

unit DXBasisSelector;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  DXContainer, DXDraws, DXTakticScreen, XForce_types, Blending, math, KD4Utils,
  Defines, StringConst, DXGroupCaption, RaumschiffList, BasisListe, DirectDraw,
  DXBitmapButton, KD4Page, DirectFont, TraceFile, EarthCalcs, D3DRender;

type
  TGetLandName = procedure(X,Y: Integer; var Name: String) of object;
  TValidPos    = procedure(X,Y: Integer; var Valid: boolean) of object;

  TDXBasisSelector = class(TDXComponent)
  private
    fPoint        : TFloatPoint;
    fEarthRect    : TRect;
    fOldLand      : String;
    fOldValid     : TCheckBasePosResult;
    fBasisList    : TBasisListe;
    fUISymbols    : TDirectDrawSurface;
    fBaseType     : TBaseType;
    fNewGame      : Boolean;
    function CheckPos(X,Y: Integer): TCheckBasePosResult;
    procedure CancelButtonClick(Sender: TObject);
    procedure DrawBasisPoint(Surface: TDirectDrawSurface;X,Y: Integer);
    procedure RestoreDirectX;
  protected
  public
    ModalValue    : Integer;
    MouseX        : Integer;
    MouseY        : Integer;
    Caption       : TDXGroupCaption;
    CancelButton  : TDXBitmapButton;
    OKButton      : TDXBitmapButton;
    constructor Create(Page: TDXPage);override;
    procedure Draw(Surface: TDirectDrawSurface;var Mem: TDDSurfaceDesc);override;
    procedure MouseLeave;override;
    procedure MouseUp(Button: TMouseButton;X,Y: Integer);override;
    procedure MouseMove(X,Y: Integer);override;
    procedure RedrawRect(DrawRect: TRect;Surface: TDirectDrawSurface;var Mem: TDDSurfaceDesc);override;
    procedure Resize(var NewLeft,NewTop,NewWidth,NewHeight: Integer);override;
    function SelectPos(var Point: TFloatPoint;NewGame: boolean): boolean;
    property BasisListe     : TBasisListe read fBasisList write fBasisList;

    property BaseType       : TBaseType read fBaseType write fBaseType;
  end;

implementation

uses
  country_api, basis_api;

{ TDXBasisSelector }

procedure TDXBasisSelector.CancelButtonClick(Sender: TObject);
begin
  ModalValue:=2;
end;

function TDXBasisSelector.CheckPos(X, Y: Integer): TCheckBasePosResult;
var
  Dummy: Integer;
begin
  result:=fBasisList.CheckBasePosition(fBaseType,FloatPoint(X,Y),fNewGame);
{  ValidPos(X,Y,result);
  if (not result) or (fBasisList=nil) then exit;
  for Dummy:=0 to 7 do
  begin
    if fBasisList.Basen[Dummy]<>nil then
    begin
      if EarthEntfernung(FloatPoint(X,Y),fBasisList.Basen[Dummy].BasisPos)<2000 then
      begin
        result:=false;
        exit;
      end;
    end;
  end;}
end;

constructor TDXBasisSelector.Create(Page: TDXPage);
begin
  inherited;
  Caption:=TDXGroupCaption.Create(Page);
  { Einstellungen f�r den Abbrechen Button }
  CancelButton:=TDXBitmapButton.Create(Page);
  with CancelButton do
  begin
    Visible:=false;
    Text:=BCancel;
    Height:=28;
    RoundCorners:=rcBottom;
    OnClick:=CancelButtonClick;
    BlendAlpha:=150;
    Enabled:=true;
    FirstColor:=bcDarkNavy;
    SecondColor:=clBlue;
    BlendColor:=bcNavy;
  end;

  with Caption do
  begin
    Visible:=false;
    Font.Size:=14;
    FontColor:=clYellow;
    Font.Name:=coFontName;
    Font.Style:=[fsBold];
    Caption:=CBasisPos;
    RoundCorners:=rcTop;
    BlendColor:=bcDarkNavy;
    BorderColor:=$00380000;
    AlphaValue:=180;
    Alignment:=taCenter;
    DrawBack:=true;
  end;
  Visible:=false;
  TabStop:=false;
  SetRect(0,0,0,0);
  Font.Size:=14;
  Font.Style:=[fsBold];
  MouseX:=-1;
  MouseY:=-1;

  Container.AddRestoreFunction(RestoreDirectX);
  RestoreDirectX;
end;

procedure TDXBasisSelector.Draw(Surface: TDirectDrawSurface;var Mem: TDDSurfaceDesc);
begin
  RedrawRect(ClientRect,Surface,Mem);
end;

procedure TDXBasisSelector.DrawBasisPoint(Surface: TDirectDrawSurface; X,
  Y: Integer);
begin
  Surface.Draw(Left+X-4,Top+Y-4,Rect(0,95,8,103),fUISymbols,true)
end;

procedure TDXBasisSelector.MouseLeave;
begin
  inherited;
  MouseX:=-1;
  MouseY:=-1;
  fOldLand:='';
  fOldValid:=cbprOK;
  Container.RedrawBlackControl(Self,Container.Surface);
end;

procedure TDXBasisSelector.MouseMove(X, Y: Integer);
var
  Val    : TCheckBasePosResult;
  Text   : String;
begin
  inherited;

  if (MouseX=X) and (MouseY=Y) then
    exit;

  MouseX:=X;
  MouseY:=Y;

  fOldValid:=CheckPos(MouseX,MouseY);
  fOldLand:=country_api_LandOverPos(FloatPoint(MouseX,MouseY));

  Container.RedrawBlackControl(Self,Container.Surface);
end;

procedure TDXBasisSelector.MouseUp(Button: TMouseButton; X, Y: Integer);
begin
  inherited;
  if CheckPos(X,Y)<>cbprOK then
    exit;

  ModalValue:=1;
  fPoint:=FloatPoint(X,Y);
end;

procedure TDXBasisSelector.RedrawRect(DrawRect: TRect;
  Surface: TDirectDrawSurface;var Mem: TDDSurfaceDesc);
var
  Dummy      : Integer;
  Text       : String;
begin
  Surface.ClippingRect:=ClientRect;
  if (fBasisList<>nil) and (fBaseType=btAlphatronMine) then
  begin
    Surface.Canvas.Draw(Left,Top,fBasisList.AlphatronMap);
    Surface.Canvas.Release;
  end
  else
    Surface.StretchDraw(ClientRect,gGlobeRenderer.Textur,false);

  Rectangle(Surface,Mem,ClientRect,bcDarkNavy);

  if fBasisList<>nil then
  begin
    for Dummy:=0 to basis_api_GetBasisCount-1 do
      DrawBasisPoint(Surface,round(fBasisList.Basen[Dummy].BasisPos.X),round(fBasisList.Basen[Dummy].BasisPos.Y));
  end;
  if MouseX<>-1 then
  begin
    if fOldLand<>'' then
      WhiteStdFont.Draw(Surface,Left+10,Top+10,fOldLand);

    if fOldValid=cbprOK then
      DrawBasisPoint(Surface,MouseX,MouseY)
    else
      RedStdFont.Draw(Surface,Left+10,Top+30,CheckBaseErrors[fOldValid]);

    if fBaseType=btAlphatronMine then
    begin
      if PtInRect(Rect(0,0,360,180),Point(MouseX,MouseY)) then
      begin
        Text:=Format('Alphatron: %.2n',[fBasisList.AlphatronAtPos(MouseX,MouseY)]);
        WhiteBStdFont.Draw(Surface,Right-10-WhiteBStdFont.TextWidth(Text),Top+10,Text);
      end;
    end;
  end;

  Rectangle(Surface,Mem,ClientRect,bcDarkNavy);
  Surface.ClearClipRect;
end;

procedure TDXBasisSelector.Resize(var NewLeft, NewTop, NewWidth,
  NewHeight: Integer);
begin
  NewWidth:=362;
  NewHeight:=180;
  NewLeft:=(ScreenWidth div 2)-(NewWidth shr 1);
  NewTop:=(ScreenHeight div 2)-(NewHeight shr 1)+27;
  Caption.SetRect((ScreenWidth div 2)-(NewWidth shr 1),(ScreenHeight div 2)-(NewHeight shr 1),NewWidth,27);
  fEarthRect:=Rect(Left,Top+27,Right,Bottom);
  CancelButton.SetRect(NewLeft,NewTop+NewHeight+1,NewWidth,28);
end;

procedure TDXBasisSelector.RestoreDirectX;
begin
  fUISymbols:=Container.ImageList.Items[1].PatternSurfaces[0];
end;

function TDXBasisSelector.SelectPos(var Point: TFloatPoint;
  NewGame: boolean): boolean;
begin
  if Visible or (not Container.CanMessage) then
  begin
    result:=false;
    exit;
  end;
  fOldLand:='';
  fNewGame:=NewGame;

  Container.MessageLock(true);
  Container.Lock;
  Container.RemoveComponent(Self);
  Container.RemoveComponent(Caption);
  Container.RemoveComponent(CancelButton);
  Container.DeactiveAll;
  Caption.Visible:=true;
  Enabled:=true;
  Visible:=true;
  Container.AddComponent(Self);
  Container.AddComponent(Caption);
  if not NewGame then
  begin
    (Parent as TKD4Page).SetButtonFont(CancelButton.Font);
    CancelButton.AccerlateColor:=clYellow;
    CancelButton.Visible:=true;
    Container.AddComponent(CancelButton);
  end;
  Container.ReleaseCapture;
  Container.LoadGame(false);
  Caption.Redraw;
  CancelButton.Redraw;
  Container.DecLock;
  Redraw;
  ModalValue:=0;
  repeat
    Application.HandleMessage;
  until ModalValue <> 0;
  case ModalValue of
    1 :
    begin
      Point:=fPoint;
      result:=true;
    end;
    else
      result:=false;
  end;
  Container.IncLock;
  Container.MessageLock(false);
  Visible:=false;
  Caption.Visible:=false;
  CancelButton.Visible:=false;
  Container.DecLock;
  Container.Lock;
  Container.RemoveComponent(Self);
  Container.RemoveComponent(Caption);
  if not NewGame then
  begin
    Container.RemoveComponent(CancelButton);
  end;
  Container.LoadGame(false);
  Container.ReactiveAll;
  Container.DecLock;
  Container.DoFlip;
end;

end.
