{********************************************************************************
*										*
* Copyright (C) 2004  X-Force Team - jim_raynor@web.de				*
*										*
* This file is part of X-Force: Fight For Destiny (X-Force).			*
*										*
* X-Force is free software; you can redistribute it and/or modify		*
* it under the terms of the GNU General Public License as published by		*
* the Free Software Foundation; either version 2 of the License, or		*
* (at your option) any later version.						*
*										*
* X-Force is distributed in the hope that it will be useful,			*
* but WITHOUT ANY WARRANTY; without even the implied warranty of		*
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the			*
* GNU General Public License for more details.					*
*										*
* You should have received a copy of the GNU General Public License		*
* along with X-Force; if not, write to the Free Software			*
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA	*
*										*
*********************************************************************************
*										*
* Stellt eine Klasse zur Verf�gung um einer ID (Cardinal-Wert) einen Index zu	*
* zuordnen. Wird z.B. in der LagerListe verwendet um schnell auf Ausr�stung 	*
* �ber die eindeutige ID zuzugreifen						*
*										*
********************************************************************************}

{$I ../Settings.inc}

unit IDHashMap;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  math;

type
  TIDHashMap = class;

  THashEntry = record
    HID      : Cardinal;
    HKey     : Integer;
    HEintrag : boolean;
    HDelete  : boolean;
    HHashMap : TIDHashMap;
  end;

  TIDHashMap = class(TObject)
  private
    fEntrys : Array of THashEntry;
    fRand   : Cardinal;
    fSize   : Integer;
    procedure SetSize(const Value: Integer);
    { Private-Deklarationen }
  protected
    { Protected-Deklarationen }
  public
    constructor Create;
    destructor Destroy;override;
    procedure InsertID(ID: Cardinal; Key: Integer);
    function FindKey(ID: Cardinal;var Key: Integer): boolean;
    procedure DeleteKey(ID: Cardinal);
    procedure ClearList;
    property Size: Integer read fSize write SetSize;
    { Public-Deklarationen }
  end;

implementation

{ TIDHashMap }

procedure TIDHashMap.ClearList;
var
  Dummy: Integer;
begin
  for Dummy:=0 to fSize-1 do
  begin
    with fEntrys[Dummy] do
    begin
      HEintrag:=false;
      HDelete:=false;
      if HHashMap<>nil then
      begin
        HHashMap.Free;
        HHashMap:=nil;
      end;
    end;
  end;
end;

constructor TIDHashMap.Create;
begin
  fRand:=round(random*High(Cardinal));
  Size:=59;
end;

procedure TIDHashMap.DeleteKey(ID: Cardinal);
var
  Hash  : Cardinal;
begin
  Hash:=(ID+fRand) mod fSize;
  with fEntrys[Hash] do
  begin
    if HEintrag then
    begin
      if HID=ID then
      begin
        HDelete:=true;
      end
      else
      begin
        if HHashMap<>nil then
          HHashMap.DeleteKey(ID);
      end;
    end;
  end;
end;

destructor TIDHashMap.Destroy;
begin
  ClearList;
  inherited;
end;

function TIDHashMap.FindKey(ID: Cardinal;var Key: Integer): boolean;
var
  Hash  : Cardinal;
begin
  Key:=0;
  result:=true;
  Hash:=(ID+fRand) mod fSize;
  with fEntrys[Hash] do
  begin
    if HEintrag then
    begin
      if HID=ID then
      begin
        if not HDelete then
        begin
          Key:=HKey;
          exit;
        end;
      end
      else
      begin
        if HHashMap<>nil then
        begin
          result:=HHashMap.FindKey(ID,Key);
          exit;
        end;
      end;
    end;
  end;
  result:=false;
end;

procedure TIDHashMap.InsertID(ID: Cardinal; Key: Integer);
var
  Hash  : Cardinal;
begin
  Hash:=(ID+fRand) mod fSize;
  with fEntrys[Hash] do
  begin
    if HEintrag then
    begin
      if not HDelete then
      begin
        if ID=HID then
          raise Exception.Create('ID bereits eingetragen');
        if HHashMap=nil then
        begin
          HHashMap:=TIDHashMap.Create;
          HHashMap.Size:=max(7,fSize div 2);
        end;
        HHashMap.InsertID(ID,Key);
        exit;
      end
      else
        HDelete:=false;
    end;
    HID:=ID;
    HKey:=Key;
    HEintrag:=true;
  end;
end;

procedure TIDHashMap.SetSize(const Value: Integer);
begin
  ClearList;
  fSize := Value;
  SetLength(fEntrys,fSize);
end;

end.
