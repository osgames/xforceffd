{********************************************************************************
*										*
* Copyright (C) 2004  X-Force Team - jim_raynor@web.de				*
*										*
* This file is part of X-Force: Fight For Destiny (X-Force).			*
*										*
* X-Force is free software; you can redistribute it and/or modify		*
* it under the terms of the GNU General Public License as published by		*
* the Free Software Foundation; either version 2 of the License, or		*
* (at your option) any later version.						*
*										*
* X-Force is distributed in the hope that it will be useful,			*
* but WITHOUT ANY WARRANTY; without even the implied warranty of		*
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the			*
* GNU General Public License for more details.					*
*										*
* You should have received a copy of the GNU General Public License		*
* along with X-Force; if not, write to the Free Software			*
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA	*
*										*
*********************************************************************************
*										*
* Verwaltet eine Liste mit AGF-Images (Bilder die von Powerdraw verwendet	*
* werden). Dabei muss nur das Archiv (PAK-Datei) und der Name im Archiv		*
* �ber AddImage festgelegt werden. Bei LoadImages werden automatisch alle	*
* Bilder geladen, bei FreeImage wieder freigegeben. Mit GetAGFImage kann        *
* auf ein bestimmtes Bild zugriffen werden.					*
*										*
* ACHTUNG: Probleme gibt es derzeit wenn zwei Bilder den gleichen Namen, aber   *
*          in unterschiedlichen Archiven ist.					*
*										*
********************************************************************************}

{$I ../Settings.inc}

unit AGFImageList;

interface

uses AGFUnit, ArchivFile, DXContainer, KD4Utils, XForce_types, SysUtils, TraceFile;

type
  TAGFFileInfos = record
                    Name       : String;
                    ArchivFile : String;
                    Image      : TAGFImage;
                  end;

  TAGFImageList = class(TObject)
  private
    fAGFFiles      : Array of TAGFFileInfos;
    fContainer     : TDXContainer;

    function SortFunc(File1,File2: Integer;Typ: TFunctionType): Integer;
  public
    destructor Destroy;override;
    procedure FreeImages;
    procedure LoadImages;

    procedure AddImage(Archiv: String;Name: String);

    function GetAGFImage(Name: String): TAGFImage;

    property Container: TDXContainer read fContainer write fContainer;
  end;

implementation

{ TAGFImageList }

procedure TAGFImageList.AddImage(Archiv, Name: String);
begin
  SetLength(fAGFFiles,length(fAGFFiles)+1);
  fAGFFiles[high(fAGFFiles)].Name:=Name;
  fAGFFiles[high(fAGFFiles)].ArchivFile:=Archiv;
  fAGFFiles[high(fAGFFiles)].Image:=nil;
end;

destructor TAGFImageList.Destroy;
begin
  FreeImages;
  inherited;
end;

procedure TAGFImageList.FreeImages;
var
  Dummy: Integer;
begin
  for Dummy:=0 to high(fAGFFiles) do
  begin
    if fAGFFiles[Dummy].Image<>nil then
      fAGFFiles[Dummy].Image.Free;
    fAGFFiles[Dummy].Image:=nil;
  end;
end;

function TAGFImageList.GetAGFImage(Name: String): TAGFImage;
var
  Dummy: Integer;
begin
  for Dummy:=0 to high(fAGFFiles) do
  begin
    if fAGFFiles[Dummy].Name=Name then
    begin
      result:=fAGFFiles[Dummy].Image;
      exit;
    end;
  end;
  raise Exception.Create('AGF-Image '+Name+' nicht in der Liste');
end;

procedure TAGFImageList.LoadImages;
var
  Archiv: TArchivFile;
  Dummy : Integer;
begin
  QuickSort(0,high(fAGFFiles),SortFunc);
  Assert(fContainer<>nil,'DXContainer muss bei TAGFImageList.LoadImages vergeben sein');
  Archiv:=TArchivFile.Create;

  for Dummy:=0 to high(fAGFFiles) do
  begin
    with fAGFFiles[Dummy] do
    begin
      if Archiv.ArchivFile<>ArchivFile then
        Archiv.OpenArchiv(ArchivFile,true);

      Archiv.OpenRessource(Name);
      if Image<>nil then
        Image.Free;
      Image:=TAGFImage.Create(fContainer,fContainer.PowerDraw.D3DActive);
      Image.LoadFromStream(Archiv.Stream);
      Image.Name:=Name;
    end;
  end;
  Archiv.Free;
end;

function TAGFImageList.SortFunc(File1, File2: Integer;
  Typ: TFunctionType): Integer;
var
  tmp : TAGFFileInfos;
begin
  case Typ of
    ftCompare  : result:=CompareStr(fAGFFiles[File1].ArchivFile,fAGFFiles[File2].ArchivFile);
    ftExchange :
    begin
      tmp:=fAGFFiles[File1];
      fAGFFiles[File1]:=fAGFFiles[File2];
      fAGFFiles[File2]:=tmp;
    end;
  end;
end;

end.
