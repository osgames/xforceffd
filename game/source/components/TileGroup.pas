{********************************************************************************
*										*
* Copyright (C) 2004  X-Force Team - jim_raynor@web.de				*
*										*
* This file is part of X-Force: Fight For Destiny (X-Force).			*
*										*
* X-Force is free software; you can redistribute it and/or modify		*
* it under the terms of the GNU General Public License as published by		*
* the Free Software Foundation; either version 2 of the License, or		*
* (at your option) any later version.						*
*										*
* X-Force is distributed in the hope that it will be useful,			*
* but WITHOUT ANY WARRANTY; without even the implied warranty of		*
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the			*
* GNU General Public License for more details.					*
*										*
* You should have received a copy of the GNU General Public License		*
* along with X-Force; if not, write to the Free Software			*
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA	*
*										*
*********************************************************************************
*										*
* TileGroup verwaltet eine Tileset-Datei. Die Komponente wird allerdings nur    *
* im Karten und Tileset-Editor verwendet. F�r den Bodeneisnatz kann auf die	*
* auf die Zusatzfunktionen dieser Klasse verzichtet werden und die Tileset-	*
* Dateien werden dort direkt eingelesen.					*
*										*
********************************************************************************}

{$I ../Settings.inc}

unit TileGroup;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  DXDraws, XForce_types, DIB, Koding, ExtRecord, Compress, JclFileUtils;

type
  TTileInformationArray = Array of TTileInformation;
  TWallInformationArray = Array of TWallInformation;

  TTileGroup = class(TComponent)
  private
    fImageList     : TDXImageList;
    fFileName      : String;
    fTileName      : NameString;
    fModify        : boolean;
    fNeedName      : TNeedFileName;
    fFileChange    : TNotifyEvent;
    fTiles         : TTileInformationArray;
    fWall          : TWallInformationArray;

    fOnFileReload  : TNotifyEvent;
    fLastDateTime  : TFileTime;

    function GetDXDraw: TCustomDXDraw;
    procedure SetDXDraw(const Value: TCustomDXDraw);
    procedure SetFileName(const Value: String);
    procedure SetTileName(const Value: NameString);
    function GetFloorCount: Integer;
    function GetWallLeftCount: Integer;
    function GetWallRightCount: Integer;
    function GetObjectCount: Integer;
    { Private-Deklarationen }
  protected
    { Protected-Deklarationen }
  public
    constructor Create(AOwner: TComponent);override;
    destructor Destroy;override;
    { Public-Deklarationen }
  published
    procedure New;
    procedure AddFloor(Bitmap:TBitmap);

    procedure ReplaceFloor(Bitmap:TBitmap; Index: Integer);
    procedure ReplaceWallLeft(Bitmap:TBitmap; Index: Integer);
    procedure ReplaceWallRight(Bitmap:TBitmap; Index: Integer);

    procedure AddWall(Bitmap:TBitmap);
    procedure AddObject(Bitmap:TBitmap);
    procedure DrawFloorDirect(x,y: Integer;Floor: Integer; Alpha: Integer = 255);
    procedure DrawWallLeftDirect(x,y: Integer;WallLeft: Integer; Alpha: Integer = 255);
    procedure DrawWallRightDirect(x,y: Integer;Right: Integer; Alpha: Integer = 255);
    procedure DrawObjectDirect(x,y: Integer;Objekt: Integer);
    procedure DrawFloorCanvas(Canvas: TCanvas;x,y: Integer;Floor: Integer);
    procedure DrawWallLeftCanvas(Canvas: TCanvas;x,y: Integer;WallLeft: Integer);
    procedure DrawWallRightCanvas(Canvas: TCanvas;x,y: Integer;Right: Integer);
    procedure DrawObjectCanvas(Canvas: TCanvas;x,y: Integer;Objekt: Integer);
    procedure SaveToFile;
    procedure LoadFromFile(FileName: String);

    function TileInformation(Index: Integer): PTileInformation;
    function WallInformation(Index: Integer): PWallInformation;
    procedure SetTileInformation(Index: Integer; Info: TTileInformation);
    procedure SetWallInformation(Index: Integer; Info: TWallInformation);

    procedure CheckReload;

    property FloorCount      : Integer read GetFloorCount;
    property WallLeftCount   : Integer read GetWallLeftCount;
    property WallRightCount  : Integer read GetWallRightCount;
    property ObjektCount     : Integer read GetObjectCount;
    property DXDraw          : TCustomDXDraw read GetDXDraw write SetDXDraw;
    property FileName        : String read fFileName write SetFileName;
    property TileName        : NameString read fTileName write SetTileName;
    property Modify          : boolean read fModify write fModify;
    property OnNeedFileName  : TNeedFileName read fNeedName write fNeedName;
    property OnFileNameChange: TNotifyEvent read fFileChange write fFileChange;
    property ImageList       : TDXImageList read fImageList;

    property OnFileReload    : TNotifyEvent read fOnFileReload write fOnFileReload;
    { Published-Deklarationen }
  end;

const
  TransparentColor = clBlack;

procedure Register;

procedure ReadTileInformations(Stream: TStream; var TileArray: TTileInformationArray);
procedure ReadWallInformations(Stream: TStream; var WallArray: TWallInformationArray);

implementation

uses Defines, pngimage;

procedure Register;
begin
  RegisterComponents('KD4 Tools', [TTileGroup]);
end;

var
  TileRecord: TExtRecordDefinition;
  WallRecord: TExtRecordDefinition;

procedure ReadTileInformations(Stream: TStream; var TileArray: TTileInformationArray);
var
  Rec    : TExtRecord;
  Dummy  : Integer;
begin
  Rec:=TExtRecord.Create(TileRecord);
  for Dummy:=0 to high(TileArray) do
  begin
    Rec.LoadFromStream(Stream);
    TileArray[Dummy].LeftRotate:=Rec.GetInteger('LeftRotate');
    TileArray[Dummy].HMirror:=Rec.GetInteger('HMirror');
    TileArray[Dummy].VMirror:=Rec.GetInteger('VMirror');
    TileArray[Dummy].RightRotate:=Rec.GetInteger('RightRotate');
    TileArray[Dummy].Begehbar:=Rec.GetBoolean('Begehbar');
  end;
  Rec.Free;
end;

procedure ReadWallInformations(Stream: TStream; var WallArray: TWallInformationArray);
var
  Rec    : TExtRecord;
  Dummy  : Integer;
begin
  Rec:=TExtRecord.Create(WallRecord);
  for Dummy:=0 to high(WallArray) do
  begin
    Rec.LoadFromStream(Stream);
    WallArray[Dummy].DoorType:=TDoorType(Rec.GetInteger('DoorType'));
    WallArray[Dummy].Mirror:=Rec.GetInteger('Mirror');
    WallArray[Dummy].BackSide:=Rec.GetInteger('BackSide');
    WallArray[Dummy].Border:=Rec.GetInteger('Border');
    WallArray[Dummy].DrawWindow:=Rec.GetBoolean('DrawWindow');
    WallArray[Dummy].ShowThrough:=Rec.GetBoolean('ShowThrough');
    WallArray[Dummy].WalkThrough:=Rec.GetBoolean('WalkThrough');
    WallArray[Dummy].WindowColor:=Rec.GetCardinal('WindowColor');
    WallArray[Dummy].Barness:=Rec.GetInteger('Bareness');
    WallArray[Dummy].HitPoints:=Rec.GetInteger('HitPoints');
    WallArray[Dummy].Destroyed:=Rec.GetInteger('Destroyed');
    WallArray[Dummy].AbsWallHeight:=Rec.GetInteger('AbsWallHeight');
    WallArray[Dummy].IsEcke:=Rec.GetBoolean('IsEcke');
//    WallArray[Dummy].Border:=-1;
    if WallArray[Dummy].BackSide=-1 then
      WallArray[Dummy].BackSide:=Dummy;
  end;
  Rec.Free;
end;

{ TTileGroup }

procedure TTileGroup.AddFloor(Bitmap: TBitmap);
begin
  if fImageList.Items[FloorIndex].Picture.Bitmap.Width=1 then
    fImageList.Items[FloorIndex].Picture.Bitmap.Width:=TileWidth
  else
    fImageList.Items[FloorIndex].Picture.Bitmap.Width:=fImageList.Items[FloorIndex].Picture.Bitmap.Width+TileWidth;

  fImageList.Items[FloorIndex].Picture.Bitmap.Canvas.Draw(fImageList.Items[FloorIndex].Picture.Bitmap.Width-TileWidth,0,Bitmap);
  fImageList.Items[FloorIndex].Restore;
  fModify:=true;
  SetLength(fTiles,length(fTiles)+1);
  with fTiles[High(fTiles)] do
  begin
    LeftRotate:=High(fTiles);
    HMirror:=High(fTiles);
    VMirror:=High(fTiles);
    RightRotate:=High(fTiles);
    Begehbar:=true;
  end;
end;

constructor TTileGroup.Create(AOwner: TComponent);
var
  Item: TPictureCollectionItem;
begin
  inherited;
  fImageList:=TDXImageList.Create(Self);
  Item := TPictureCollectionItem.Create(fImageList.Items);
  Item.Transparent:=true;
  Item.PatternWidth:=TileWidth;
  Item.PatternHeight:=TileHeight;
  Item.Picture.Bitmap.Height:=TileHeight;
  Item.SystemMemory:=false;
  Item.TransparentColor:=clFuchsia;
  Item.Restore;

  Item := TPictureCollectionItem.Create(fImageList.Items);
  Item.Transparent:=true;
  Item.PatternWidth:=WallWidth;
  Item.PatternHeight:=WallHeight;
  Item.Picture.Bitmap.Height:=WallHeight;
  Item.SystemMemory:=false;
  Item.TransparentColor:=clFuchsia;
  Item.Restore;

  Item := TPictureCollectionItem.Create(fImageList.Items);
  Item.Transparent:=true;
  Item.PatternWidth:=WallWidth;
  Item.PatternHeight:=WallHeight;
  Item.Picture.Bitmap.Height:=WallHeight;
  Item.SystemMemory:=false;
  Item.TransparentColor:=clFuchsia;
  Item.Restore;

  Item := TPictureCollectionItem.Create(fImageList.Items);
  Item.Transparent:=true;
  Item.PatternWidth:=49;
  Item.PatternHeight:=64;
  Item.Picture.Bitmap.Height:=64;
  Item.TransparentColor:=clFuchsia;
  Item.SystemMemory:=false;
  Item.Restore;

  FileName:='';
  TileName:='';
  fModify:=false;
end;

destructor TTileGroup.Destroy;
begin
  fImageList.Free;
  inherited;
end;

procedure TTileGroup.DrawFloorDirect(x,y: Integer;Floor: Integer;Alpha: Integer);
var
  Rect: TRect;
begin
  if (Floor>=0) and (Floor<GetFloorCount) then
  begin
    Rect:=fImageList.Items[FloorIndex].PatternRects[Floor];

    if Alpha=255 then
      fImageList.DXDraw.Surface.Draw(X,Y,Rect,fImageList.Items[FloorIndex].PatternSurfaces[Floor])
    else
      fImageList.DXDraw.Surface.DrawAlpha(Classes.Rect(X,Y,X+TileWidth,Y+TileHeight),Rect,fImageList.Items[FloorIndex].PatternSurfaces[Floor],true,Alpha);
  end;

end;

function TTileGroup.GetDXDraw: TCustomDXDraw;
begin
  result:=fImageList.DXDraw
end;

function TTileGroup.GetFloorCount: Integer;
begin
  result:=fImageList.Items[FloorIndex].PatternCount;
end;

procedure TTileGroup.LoadFromFile(FileName: String);
var
  f        : TFileStream;
  m        : TMemoryStream;
  Head     : TTileSetHeader;
  Dummy    : Integer;
  Bitmap   : TBitmap;
  HasRight : Boolean;
  IsPNG    : Boolean;
  PNGImage : TPNGObject;
begin
  f:=TFileStream.Create(FileName,fmOpenRead);
  try
    m:=TMemoryStream.Create;
    m.LoadFromStream(f);
    m.Position:=0;
    m.Read(Head,SizeOf(Head));
    IsPNG:=false;
    case Head.Version of
      $2153AE98 :
      begin
        IsPNG:=true;
        HasRight:=true;
      end;
      $3CC4E5B9 : HasRight:=true;
      $4463696C : HasRight:=false;
      else
      begin
        m.Free;
        f.Free;
        raise EInvalidVersion.Create('Ung�ltige Version des TileSets ('+FileName+')');
      end;
    end;

    fTileName:=Head.Name;
    fFileName:=FileName;
    if Assigned(fFileChange) then fFileChange(Self);
    PNGImage:=TPNGObject.Create;
    for Dummy:=0 to 2 do
    begin
      if IsPNG then
      begin
        PNGImage.LoadFromStream(m);
        fImageList.Items[Dummy].Picture.Bitmap.Assign(PNGImage);
      end
      else
        fImageList.Items[Dummy].Picture.Bitmap.LoadFromStream(m);

      fImageList.Items[Dummy].Restore;
    end;
    FreeAndNil(PNGImage);

    if not HasRight then
    begin
      fImageList.Items[RightIndex].Picture.Bitmap.Width:=fImageList.Items[LeftIndex].Picture.Bitmap.Width;
      Bitmap:=TBitmap.Create;
      Bitmap.Width:=WallWidth;
      Bitmap.Height:=WallHeight;
      for Dummy:=0 to WallLeftCount-1 do
      begin
        Bitmap.Canvas.CopyRect(Rect(0,0,WallWidth,WallHeight),fImageList.Items[LeftIndex].Picture.Bitmap.Canvas,Rect((Dummy)*WallWidth,0,((Dummy+1)*WallWidth),WallHeight));
        Bitmap.Canvas.StretchDraw(Rect(WallWidth-1,0,-1,WallHeight),Bitmap);
        fImageList.Items[RightIndex].Picture.Bitmap.Canvas.Draw((Dummy)*WallWidth,0,Bitmap);
      end;
      Bitmap.Free;
    end;

    fImageList.Items[RightIndex].Restore;

    SetLength(fTiles,FloorCount);
    SetLength(fWall,WallLeftCount);
    ReadTileInformations(m,fTiles);
    ReadWallInformations(m,fWall);
    fModify:=false;
    m.Free;
    fLastDateTime:=GetFileLastWrite(FileName);
  finally
    f.Free;
  end;
end;

procedure TTileGroup.New;
var
  Dummy: Integer;
begin
  for DUmmy:=0 to 3 do
  begin
    fImageList.Items[Dummy].Picture.Bitmap.Width:=0;
    fImageList.Items[Dummy].Restore;
  end;
  fFileName:='';
  fTileName:='';
  if Assigned(fFileChange) then fFileChange(Self);
  fModify:=false;
end;

procedure TTileGroup.SaveToFile;
var
  f      : TFileStream;
  m      : TMemoryStream;
  Head   : TTileSetHeader;
  Dummy  : Integer;
  Save   : Boolean;
  Rec    : TExtRecord;
  PNGImage: TPNGObject;

  procedure WallToRecord(Wall: TWallInformation; Rec: TExtRecord);
  begin
    rec.SetInteger('DoorType',Integer(Wall.DoorType));
    rec.SetInteger('Mirror',Wall.Mirror);
    rec.SetInteger('BackSide',Wall.BackSide);
    rec.SetInteger('Border',Wall.Border);
    rec.SetInteger('Destroyed',Wall.Destroyed);
    rec.SetBoolean('DrawWindow',Wall.DrawWindow);
    rec.SetBoolean('ShowThrough',Wall.ShowThrough);
    rec.SetBoolean('WalkThrough',Wall.WalkThrough);
    rec.SetCardinal('WindowColor',Cardinal(Wall.WindowColor));
    rec.SetInteger('Bareness',Wall.Barness);
    rec.SetInteger('AbsWallHeight',Wall.AbsWallheight);
    rec.SetBoolean('IsEcke',Wall.IsEcke);
  end;

begin
  if FileName='' then
  begin
    Save:=true;
    if Assigned(fNeedName) then fNeedName(Self,fFileName,Save) else exit;
    if not Save then exit;
    if Assigned(fFileChange) then fFileChange(Self);
  end;
  m:=TMemoryStream.Create;
  Head.Name:=fTileName;
  Head.Version:=TileVersion;
  m.Write(head,SizeOf(Head));
//  fImageList.Items[RightIndex].Picture.Bitmap.Width:=0;

  PNGImage:=TPNGObject.Create;
  for Dummy:=0 to 2 do
  begin
    if fImageList.Items[Dummy].Picture.Bitmap.Width=0 then
    begin
      fImageList.Items[Dummy].Picture.Bitmap.Width:=1;
    end;
    PNGImage.Assign(fImageList.Items[Dummy].Picture.Bitmap);
//    PNGImage.SaveToFile('C:\Dokumente und Einstellungen\Chrissy\Lokale Einstellungen\Temp\xforce\Objekte\'+IntTostr(Dummy)+'.png');
    PNGImage.SaveToStream(m);
//    fImageList.Items[Dummy].Picture.Bitmap.SaveToStream(m);
  end;
  PNGImage.Free;

  Rec:=TExtRecord.Create(TileRecord);
  for Dummy:=0 to FloorCount-1 do
  begin
    rec.SetInteger('LeftRotate',fTiles[Dummy].LeftRotate);
    rec.SetInteger('HMirror',fTiles[Dummy].HMirror);
    rec.SetInteger('VMirror',fTiles[Dummy].VMirror);
    rec.SetInteger('RightRotate',fTiles[Dummy].RightRotate);
    rec.SetBoolean('Begehbar',fTiles[Dummy].Begehbar);
    Rec.SaveToStream(m);
  end;
  Rec.Free;

  Rec:=TExtRecord.Create(WallRecord);
  for Dummy:=0 to WallLeftCount-1 do
  begin
    WallToRecord(fWall[Dummy],Rec);
    Rec.SaveToStream(m);
  end;

  Rec.Free;

  fModify:=false;
  f:=TFileStream.Create(fFileName,fmCreate);
  m.SaveToStream(f);
//  Encode(m,f);
  m.Free;
  f.Free;
end;

procedure TTileGroup.SetDXDraw(const Value: TCustomDXDraw);
begin
  fImageList.DXDraw:=Value;
end;

procedure TTileGroup.SetFileName(const Value: String);
begin
  fFileName := Value;
  fModify:=true;
end;

procedure TTileGroup.SetTileName(const Value: NameString);
begin
  if fTileName=Value then exit;
  fTileName := Value;
  fModify:=true;
end;

function TTileGroup.GetWallLeftCount: Integer;
begin
  result:=fImageList.Items[LeftIndex].PatternCount;
end;

function TTileGroup.GetWallRightCount: Integer;
begin
  result:=fImageList.Items[RightIndex].PatternCount;
end;

function CutRect(Rect: TRect): TRect ;
begin
  dec(Rect.Right,Wall3DEffekt);
  result:=Rect;
end;

procedure TTileGroup.DrawWallRightDirect(x, y: Integer; Right: Integer; Alpha: Integer);
var
  Rect: TRect;
begin
  if (Right>=0) and (Right<GetWallRightCount) then
  begin
    Rect:=fImageList.Items[RightIndex].PatternRects[Right];

    if Alpha=255 then
      fImageList.DXDraw.Surface.Draw(X,Y,Rect,fImageList.Items[RightIndex].PatternSurfaces[Right])
    else
      fImageList.DXDraw.Surface.DrawAlpha(Classes.Rect(X,Y,X+WallWidth,Y+WallHeight),Rect,fImageList.Items[RightIndex].PatternSurfaces[Right],true,Alpha);
  end;
end;

procedure TTileGroup.AddWall(Bitmap: TBitmap);
begin
  if fImageList.Items[LeftIndex].Picture.Bitmap.Width=1 then
    fImageList.Items[LeftIndex].Picture.Bitmap.Width:=WallWidth
  else
    fImageList.Items[LeftIndex].Picture.Bitmap.Width:=fImageList.Items[LeftIndex].Picture.Bitmap.Width+WallWidth;
  if fImageList.Items[LeftIndex].Picture.Bitmap.Height<>WallHeight then
    fImageList.Items[LeftIndex].Picture.Bitmap.Height:=WallHeight;

  fImageList.Items[LeftIndex].Picture.Bitmap.Canvas.Draw(fImageList.Items[LeftIndex].Picture.Bitmap.Width-WallWidth,0,Bitmap);
  fImageList.Items[LeftIndex].Restore;
  Bitmap.Canvas.StretchDraw(Rect(WallWidth-1,0,-1,WallHeight),Bitmap);

  if fImageList.Items[RightIndex].Picture.Bitmap.Width=1 then
    fImageList.Items[RightIndex].Picture.Bitmap.Width:=WallWidth
  else
    fImageList.Items[RightIndex].Picture.Bitmap.Width:=fImageList.Items[RightIndex].Picture.Bitmap.Width+WallWidth;

  if fImageList.Items[RightIndex].Picture.Bitmap.Height<>WallHeight then
    fImageList.Items[RightIndex].Picture.Bitmap.Height:=WallHeight;
  fImageList.Items[RightIndex].Picture.Bitmap.Canvas.Draw(fImageList.Items[RightIndex].Picture.Bitmap.Width-WallWidth,0,Bitmap);
  fImageList.Items[RightIndex].Restore;

  SetLength(fWall,length(fWall)+1);
  fWall[High(fWall)].DoorType:=dtNoDoor;
  fWall[High(fWall)].Mirror:=high(fWall);
  fWall[High(fWall)].BackSide:=high(fWall);
  fWall[High(fWall)].DrawWindow:=false;
  fWall[High(fWall)].ShowThrough:=false;
  fWall[High(fWall)].WalkThrough:=false;
  fWall[High(fWall)].Barness:=0;
  fWall[High(fWall)].HitPoints:=0;
  fWall[High(fWall)].IsEcke:=false;

  fWall[High(fWall)].Destroyed:=-1;
  fWall[High(fWall)].Border:=-1;

  fModify:=true;
end;

procedure TTileGroup.DrawWallLeftDirect(x, y: Integer; WallLeft: Integer; Alpha: Integer);
var
  Rect: TRect;
begin
  if (WallLeft>=0) and (WallLeft<GetWallLeftCount) then
  begin
    Rect:=fImageList.Items[LeftIndex].PatternRects[WallLeft];

    if Alpha=255 then
      fImageList.DXDraw.Surface.Draw(X,Y,Rect,fImageList.Items[LeftIndex].PatternSurfaces[WallLeft])
    else
      fImageList.DXDraw.Surface.DrawAlpha(Classes.Rect(X,Y,X+WallWidth,Y+WallHeight),Rect,fImageList.Items[LeftIndex].PatternSurfaces[WallLeft],true,Alpha);
  end;
end;

procedure TTileGroup.AddObject(Bitmap: TBitmap);
begin
  fImageList.Items[ObjectIndex].Picture.Bitmap.Width:=fImageList.Items[ObjectIndex].Picture.Bitmap.Width+49;
  fImageList.Items[ObjectIndex].Picture.Bitmap.Canvas.Draw(fImageList.Items[ObjectIndex].Picture.Bitmap.Width-49,0,Bitmap);
  fImageList.Items[ObjectIndex].Restore;
  fModify:=true;
end;

procedure TTileGroup.DrawObjectDirect(x, y: Integer; Objekt: Integer);
begin
  if (Objekt>=0) and (Objekt<GetObjectCount) then
    fImageList.Items[ObjectIndex].Draw(fImageList.DXDraw.Surface,x,y,Objekt);
end;

function TTileGroup.GetObjectCount: Integer;
begin
  result:=fImageList.Items[ObjectIndex].PatternCount;
end;

procedure TTileGroup.DrawFloorCanvas(Canvas: TCanvas; x, y,
  Floor: Integer);
begin
  if (Floor>=0) and (Floor<GetFloorCount) then
  begin
    Canvas.CopyRect(Rect(x,Y,x+TileWidth,Y+TileHeight),fImageList.Items[FloorIndex].Picture.Bitmap.Canvas,Rect(Floor*TileWidth,0,(Floor+1)*TileWidth,TileHeight));
  end;
end;

procedure TTileGroup.DrawObjectCanvas(Canvas: TCanvas; x, y,
  Objekt: Integer);
begin

end;

procedure TTileGroup.DrawWallLeftCanvas(Canvas: TCanvas; x, y,
  WallLeft: Integer);
begin
  if (WallLeft>=0) and (WallLeft<GetWallLeftCount) then
  begin
    Canvas.CopyRect(Rect(x,Y,x+WallWidth,Y+WallHeight),fImageList.Items[LeftIndex].Picture.Bitmap.Canvas,Rect(WallLeft*WallWidth,0,(WallLeft+1)*WallWidth,WallHeight));
  end;
end;

procedure TTileGroup.DrawWallRightCanvas(Canvas: TCanvas; x, y,
  Right: Integer);
begin
  if (Right>=0) and (Right<GetWallRightCount) then
  begin
    Canvas.CopyRect(Rect(x,Y,x+WallWidth,Y+WallHeight),fImageList.Items[RightIndex].Picture.Bitmap.Canvas,Rect(Right*WallWidth,0,(Right+1)*WallWidth,WallHeight));
  end;
end;

function TTileGroup.TileInformation(Index: Integer): PTileInformation;
begin
  Assert((Index>=0) and (Index<FloorCount));
  result:=addr(fTiles[Index]);
end;

function TTileGroup.WallInformation(Index: Integer): PWallInformation;
begin
    Assert((Index>=0) and (Index<WallLeftCount));
  result:=addr(fWall[Index]);
end;

procedure TTileGroup.ReplaceFloor(Bitmap: TBitmap; Index: Integer);
begin
  fImageList.Items[FloorIndex].Picture.Bitmap.Canvas.Draw(Index*TileWidth,0,Bitmap);
end;

procedure TTileGroup.ReplaceWallLeft(Bitmap: TBitmap; Index: Integer);
begin
  fImageList.Items[LeftIndex].Picture.Bitmap.Canvas.Draw(Index*WallWidth,0,Bitmap);
  fImageList.Items[LeftIndex].Restore;
end;

procedure TTileGroup.ReplaceWallRight(Bitmap: TBitmap; Index: Integer);
begin
  fImageList.Items[RightIndex].Picture.Bitmap.Canvas.Draw(Index*WallWidth,0,Bitmap);
  fImageList.Items[RightIndex].Restore;
end;

procedure TTileGroup.SetTileInformation(Index: Integer;
  Info: TTileInformation);
begin
  fTiles[Index]:=Info;
end;

procedure TTileGroup.SetWallInformation(Index: Integer;
  Info: TWallInformation);
begin
  fWall[Index]:=Info;
end;

procedure TTileGroup.CheckReload;
var
  NewFileTime: TFileTime;
begin
  NewFileTime:=GetFileLastWrite(fFileName);
  if CompareFileTime(NewFileTime,fLastDateTime)<>0 then
  begin
    LoadFromFile(fFileName);
    if Assigned(fOnFileReload) then
      fOnFileReload(Self);

    fLastDateTime:=NewFileTime;
  end;
end;

initialization
  TileRecord:=TExtRecordDefinition.Create('MapTiles');
  TileRecord.AddInteger('LeftRotate',0,high(Integer),0);
  TileRecord.AddInteger('HMirror',0,high(Integer),0);
  TileRecord.AddInteger('VMirror',0,high(Integer),0);
  TileRecord.AddInteger('RightRotate',0,high(Integer),0);
  TileRecord.AddBoolean('Begehbar',true);

  WallRecord:=TExtRecordDefinition.Create('MapWalls');
  WallRecord.AddInteger('DoorType',0,Integer(high(TDoorType)),0);
  WallRecord.AddInteger('Mirror',0,high(Integer));
  WallRecord.AddInteger('BackSide',-1,high(Integer),-1);
  WallRecord.AddInteger('Border',-1,high(Integer),-1);
  WallRecord.AddBoolean('DrawWindow',false);
  WallRecord.AddBoolean('ShowThrough',false);
  WallRecord.AddBoolean('WalkThrough',false);
  WallRecord.AddCardinal('WindowColor',0,high(Cardinal));
  WallRecord.AddInteger('Bareness',0,5);
  WallRecord.AddInteger('HitPoints',0,100000,250);
  WallRecord.AddInteger('Destroyed',-1,high(Integer),-1);
  WallRecord.AddInteger('AbsWallHeight',-1,high(Integer),-1);
  WallRecord.AddBoolean('IsEcke',false);

finalization
  WallRecord.Free;
  TileRecord.Free;
end.
