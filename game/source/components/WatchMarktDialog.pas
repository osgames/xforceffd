{********************************************************************************
*										*
* Copyright (C) 2004  X-Force Team - jim_raynor@web.de				*
*										*
* This file is part of X-Force: Fight For Destiny (X-Force).			*
*										*
* X-Force is free software; you can redistribute it and/or modify		*
* it under the terms of the GNU General Public License as published by		*
* the Free Software Foundation; either version 2 of the License, or		*
* (at your option) any later version.						*
*										*
* X-Force is distributed in the hope that it will be useful,			*
* but WITHOUT ANY WARRANTY; without even the implied warranty of		*
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the			*
* GNU General Public License for more details.					*
*										*
* You should have received a copy of the GNU General Public License		*
* along with X-Force; if not, write to the Free Software			*
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA	*
*										*
*********************************************************************************
*										*
* Stellt einen Dialog zur Verf�gung in dem die Einstellungen f�r die Arbeits-   *
* markt�berwachung vorgenommen werden k�nnen. Dieser Dialog wird in der 	*
* Soldatenverwaltung und im Arbeitsmarkt genutzt.
*										*
********************************************************************************}

{$I ../Settings.inc}

unit WatchMarktDialog;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ModalDialog, DXGroupCaption, DXBitmapButton, DXContainer, XForce_types, Blending,
  DXCheckBox, DXSpinEdit, StringConst;

type
  TWatchMarktDialog = class(TModalDialog)
  private
    GroupHeader       : TDXGroupCaption;
    OKButton          : TDXBitmapButton;
    CancelButton      : TDXBitmapButton;
    AktivBox          : TDXCheckBox;
    FaehigCaption     : TDXGroupCaption;
    FaehigSpinBox     : TDXSpinEdit;
    ActionCaption     : TDXGroupCaption;
    MessageBox        : TDXCheckBox;
    HireBox           : TDXCheckBox;
    ExChangeBox       : TDXCheckBox;

    fPersonGruppe     : String;
    fGruppenanhang    : String;
    procedure ButtonClick(Sender: TObject);
    procedure SpinEditKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure AktivChange(Sender: TObject);

    procedure ActionChange(Sender: TObject);
    function GetActive: Boolean;
    procedure SetActive(const Value: Boolean);
    function GetFaehig: Integer;
    procedure SetFaehig(const Value: Integer);
    function GetWatchType: TWatchType;
    procedure SetWatchType(const Value: TWatchType);
    procedure SetPersonGruppe(const Value: String);
    { Private-Deklarationen }
  protected
    { Protected-Deklarationen }
    procedure BeforeShow;override;
  public
    constructor Create(Page: TDXPage);override;
    destructor Destroy;override;

    property PersonenGruppe     : String read fPersonGruppe write SetPersonGruppe;
    property Gruppenanhang      : String read fGruppenanhang write fGruppenanhang;
    property Aktiv              : Boolean read GetActive write SetActive;
    property Faehigkeiten       : Integer read GetFaehig write SetFaehig;
    property UeberwachungsType  : TWatchType read GetWatchType write SetWatchType;        
    { Public-Deklarationen }
  end;

implementation

{ TWatchMarktDialog }

procedure TWatchMarktDialog.AktivChange(Sender: TObject);
begin
  Aktiv:=AktivBox.Checked;
end;

procedure TWatchMarktDialog.BeforeShow;
begin
  GroupHeader.Font.Style:=[fsBold];
  GroupHeader.Font.Size:=Font.Size+4;
  GroupHeader.FontColor:=clYellow;
  GroupHeader.Caption:=ST0309270027;

  AktivBox.Hint:=Format(ST0309270028,[Personengruppe]);
  FaehigCaption.Caption:=Format(ST0309270029,[Personengruppe,GruppenAnhang]);
  ActionCaption.Caption:=ST0309270030;

  MessageBox.Hint:=Format(ST0311200004,[Personengruppe]);

  HireBox.Caption:=Format(ST0309270031,[Personengruppe]);
  HireBox.Hint:=Format(ST0311200003,[Personengruppe]);

  ExChangeBox.Caption:=ST0311200001;
  ExChangeBox.Hint:=Format(ST0311200002,[Personengruppe]);

end;

procedure TWatchMarktDialog.ButtonClick(Sender: TObject);
begin
  ReadyShow;
  if (Sender as TDXBitmapButton).Tag=0 then
    fResult:=mrOK
  else
    fResult:=mrCancel;
end;

constructor TWatchMarktDialog.Create(Page: TDXPage);
begin
  inherited;

  { �berschrift }
  GroupHeader:=TDXGroupCaption.Create(Page);
  with GroupHeader do
  begin
    Visible:=false;
    BorderColor:=$00400000;
    BlendColor:=bcDarkNavy;
    RoundCorners:=rcTop;
    AlphaValue:=200;
    Width:=323;
    Height:=25;
    Alignment:=taCenter;
  end;
  AddComponent(GroupHeader);

  { Aktivieren/deaktivieren Checkbox }
  AktivBox:=TDXCheckbox.Create(Page);
  with AktivBox do
  begin
    Width:=GroupHeader.Width;
    Height:=23;
    Top:=GroupHeader.Bottom+1;
    BorderColor:=$00400000;
    BlendColor:=bcDarkNavy;
    Caption:=ST0309270032;
    RoundCorners:=rcNone;
    LeftMargin:=6;
    OnChange:=AktivChange;
  end;
  AddComponent(AktivBox);

  { F�higkeiten der personen }
  FaehigSpinBox:=TDXSpinEdit.Create(Page);
  with FaehigSpinBox do
  begin
    Width:=100;
    Height:=20;
    Left:=GroupHeader.Width-Width;
    Top:=AktivBox.Bottom+1;
    Min:=0;
    Max:=100;
    FormatString:=FFloat0Percent;
    BlendColor:=bcDarkNavy;
    BorderColor:=$00400000;
    RoundCorners:=rcNone;
    FocusColor:=clNavy;
    TopMargin:=3;
    BlendValue:=175;
    OnKeyPress:=SpinEditKeyDown;
  end;
  AddComponent(FaehigSpinBox);

  { �berschrift }
  FaehigCaption:=TDXGroupCaption.Create(Page);
  with FaehigCaption do
  begin
    Visible:=false;
    BorderColor:=$00400000;
    BlendColor:=bcDarkNavy;
    RoundCorners:=rcNone;
    Width:=GroupHeader.Width-FaehigSpinBox.Width-1;
    Height:=FaehigSpinBox.Height;
    Top:=AktivBox.Bottom+1;
  end;
  AddComponent(FaehigCaption);

  { �berschrift }
  ActionCaption:=TDXGroupCaption.Create(Page);
  with ActionCaption do
  begin
    Visible:=false;
    BorderColor:=$00400000;
    BlendColor:=bcDarkNavy;
    RoundCorners:=rcNone;
    Width:=GroupHeader.Width;
    Height:=FaehigSpinBox.Height;
    Top:=FaehigCaption.Bottom+1;
  end;
  AddComponent(ActionCaption);

  { Aktivieren/deaktivieren Checkbox }
  MessageBox:=TDXCheckbox.Create(Page);
  with MessageBox do
  begin
    Width:=GroupHeader.Width;
    Height:=23;
    Top:=ActionCaption.Bottom+1;
    BorderColor:=$00400000;
    BlendColor:=bcDarkNavy;
    Caption:=ST0309270033;
    RoundCorners:=rcNone;
    LeftMargin:=6;
    OnChange:=ActionChange;
    RadioButton:=true;
  end;
  AddComponent(MessageBox);

  { Aktivieren/deaktivieren Checkbox }
  HireBox:=TDXCheckbox.Create(Page);
  with HireBox do
  begin
    Width:=GroupHeader.Width;
    Height:=23;
    Top:=MessageBox.Bottom+1;
    BorderColor:=$00400000;
    BlendColor:=bcDarkNavy;
    RoundCorners:=rcNone;
    LeftMargin:=6;
    OnChange:=ActionChange;
    RadioButton:=true;
  end;
  AddComponent(HireBox);

  { Aktivieren/deaktivieren Checkbox }
  ExchangeBox:=TDXCheckbox.Create(Page);
  with ExchangeBox do
  begin
    Width:=GroupHeader.Width;
    Height:=23;
    Top:=HireBox.Bottom+1;
    BorderColor:=$00400000;
    BlendColor:=bcDarkNavy;
    RoundCorners:=rcNone;
    LeftMargin:=6;
    OnChange:=ActionChange;
    RadioButton:=true;
  end;
  AddComponent(ExchangeBox);

  { Einstellungen f�r den OK Button }
  OKButton:=TDXBitmapButton.Create(Page);
  with OKButton do
  begin
    Visible:=false;
    OnClick:=ButtonClick;
    Text:=BOK;
    Width:=GroupHeader.Width div 2;
    Top:=ExchangeBox.Bottom+1;
    Font:=Font;
    RoundCorners:=rcLeftBottom;
    Height:=28;
    BlendAlpha:=150;
    BlendColor:=bcDarkNavy;
    FirstColor:=$00400000;
    SecondColor:=clBlue;
    AccerlateColor:=clYellow;
    Tag:=0;
  end;
  AddComponent(OKButton);

  { Einstellungen f�r den Abbrechen Button }
  CancelButton:=TDXBitmapButton.Create(Page);
  with CancelButton do
  begin
    Visible:=false;
    Text:=BCancel;
    Top:=ExchangeBox.Bottom+1;
    Width:=GroupHeader.Width div 2;
    Left:=OKButton.Right+1;
    Font:=Font;
    Height:=28;
    RoundCorners:=rcRightBottom;
    OnClick:=ButtonClick;
    BlendAlpha:=150;
    FirstColor:=$00400000;
    BlendColor:=bcDarkNavy;
    SecondColor:=clBlue;
    AccerlateColor:=clYellow;
    Tag:=1;
  end;
  AddComponent(CancelButton);
end;

function TWatchMarktDialog.GetActive: Boolean;
begin
  result:=AktivBox.Checked;
end;

procedure TWatchMarktDialog.SpinEditKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key=13 then OKButton.Doclick
  else if Key=27 then CancelButton.Doclick;
end;

procedure TWatchMarktDialog.SetActive(const Value: Boolean);
begin
  AktivBox.Container.IncLock;
  AktivBox.Checked:=Value;
  FaehigSpinBox.Enabled:=Value;
  HireBox.Enabled:=Value;
  ExChangeBox.Enabled:=Value;
  MessageBox.Enabled:=Value;
  if Value and (FaehigSpinBox.Value=0) then
  begin
    FaehigSpinBox.Value:=80;
    MessageBox.Checked:=true;
    HireBox.Checked:=false;
  end;
  AktivBox.Container.DecLock;
  AktivBox.Container.DoFlip;
end;

procedure TWatchMarktDialog.ActionChange(Sender: TObject);
begin
  AktivBox.Container.IncLock;
  MessageBox.Checked:=Sender=MessageBox;
  HireBox.Checked:=Sender=HireBox;
  ExchangeBox.Checked:=Sender=ExchangeBox;
  AktivBox.Container.DecLock;
end;

function TWatchMarktDialog.GetFaehig: Integer;
begin
  result:=FaehigSpinBox.Value;
end;

procedure TWatchMarktDialog.SetFaehig(const Value: Integer);
begin
  FaehigSpinBox.Value:=Value;
end;

function TWatchMarktDialog.GetWatchType: TWatchType;
begin
  if MessageBox.Checked then
    result:=wtMessage
  else if HireBox.Checked then
    result:=wtHire
  else
    result:=wtExchange;
end;

procedure TWatchMarktDialog.SetWatchType(const Value: TWatchType);
begin
  MessageBox.Checked:=Value=wtMessage;
  HireBox.Checked:=Value=wtHire;
  ExChangeBox.Checked:=Value=wtExchange;
end;

procedure TWatchMarktDialog.SetPersonGruppe(const Value: String);
begin
  fPersonGruppe := Value;
  if fPersonGruppe=ST0309270025 then
  begin
    DeleteComponent(ExchangeBox);
    OKButton.Top:=HireBox.Bottom+1;
    CancelButton.Top:=HireBox.Bottom+1;
  end
  else
  begin
    AddComponent(ExchangeBox);
    OKButton.Top:=ExchangeBox.Bottom+1;
    CancelButton.Top:=ExchangeBox.Bottom+1;
  end;
end;

destructor TWatchMarktDialog.Destroy;
begin
  AddComponent(ExChangeBox);
  inherited;
end;

end.
