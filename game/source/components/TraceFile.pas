{********************************************************************************
*										*
* Copyright (C) 2004  X-Force Team - jim_raynor@web.de				*
*										*
* This file is part of X-Force: Fight For Destiny (X-Force).			*
*										*
* X-Force is free software; you can redistribute it and/or modify		*
* it under the terms of the GNU General Public License as published by		*
* the Free Software Foundation; either version 2 of the License, or		*
* (at your option) any later version.						*
*										*
* X-Force is distributed in the hope that it will be useful,			*
* but WITHOUT ANY WARRANTY; without even the implied warranty of		*
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the			*
* GNU General Public License for more details.					*
*										*
* You should have received a copy of the GNU General Public License		*
* along with X-Force; if not, write to the Free Software			*
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA	*
*										*
*********************************************************************************
*										*
* TraceFile bietet eine zentrale Schnittstelle, um Debug-Nachrichten auszugeben *
* Dabei besteht die M�glichkeit die Nachrichten in eine Datei (TraceForm=false) *
* oder per Windows-Message an ein Spezielles Programm zu schicken               *
* (TraceForm=true)								*
*										*
* Die Unit legt eine Globale Variable (GlobalFile) an, die Standardm��ig        *
* Ausgaben an das Spezielle Programm weiterleitet.				*
*										*
* Die Komponente wird h�ufig benutzt, da das Debuggen im Vollbild nur schwer    *
* m�glich ist.
*										*
********************************************************************************}

{$I ../Settings.inc}

unit TraceFile;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, 
  TypInfo;

const
  TraceFileName = 'trace.txt';
  Einzug = 40;
type
  TTraceFile = class(TObject)
  private
    fFileStream  : TFileStream;
    DebugHand    : HWND;
    len          : Integer;
    fFileName    : String;
    fNew         : boolean;
    fTrace       : boolean;
    procedure SetTrace(const Value: boolean);
    { Private-Deklarationen }
  protected
    { Protected-Deklarationen }
  public
    constructor Create(FileName: String);
    destructor destroy;override;
    { Schreiben Funktionen }
    procedure Write(Text: String);overload;
    procedure Write(Variable: String;Text     : String);overload;
    procedure Write(Variable: String;Int      : Integer);overload;
    procedure Write(Variable: String;Int      : Int64);overload;
    procedure Write(Variable: String;TypInfo  : PTypeInfo;Value: Integer);overload;
    procedure Write(Variable: String;Hex      : boolean;Int: Integer);overload;
    procedure Write(Variable: String;Rect     : TRect);overload;
    procedure Write(Variable: String;Font     : TFont);overload;
    procedure Write(Variable: String;bool     : boolean);overload;
    procedure Write(Variable: String;Double   : Double);overload;
    procedure Write(Variable: String;Point    : TPoint);overload;
    procedure Write(Strings: TStrings);overload;
//    procedure Write(Variable: String;Point    : TFloatPoint);overload;
    procedure HexDump(Variable: String;Size: Integer;Start: Pointer);
    procedure TraceStack(Version: Integer);
    procedure WriteTime;
    procedure WriteLine;
    procedure StartStackDump;
    procedure EndStackDump;
    procedure Write(Ch: Char;Count: Integer);overload;
    property FileName: String read fFileName write fFileName;
    property New     : boolean read fNew write fNew;
    property TraceForm : boolean read fTrace write SetTrace;
  end;

var
  GlobalFile: TTraceFile = nil;

implementation

{ TTraceFile }

constructor TTraceFile.Create(FileName: String);
begin
  fFileStream:=nil;
  DebugHand:=0;
  {$IFDEF TRACETOFILE}
  fTrace:=true;
  {$ELSE}
  fTrace:=false;
  {$ENDIF}
  fFileName:=FileName;
  fNew:=false;
end;

destructor TTraceFile.destroy;
begin
  if fFileStream<>nil then
    fFileStream.Free;
end;

procedure TTraceFile.Write(Variable: String; Int: Integer);
begin
  len:=length(Variable+': Integer ');
  Write(Variable+': Integer '+StringOfChar(' ',Einzug-len)+'= '+IntToStr(Int));
end;

procedure TTraceFile.Write(Variable, Text: String);
begin
  len:=length(Variable+': String ');
  Write(Variable+': String '+StringOfChar(' ',Einzug-len)+'= '+Text);
end;

procedure TTraceFile.Write(Variable: String; Rect: TRect);
begin
  len:=length(Variable+': Rectangle ');
  Write(Variable+': Rectangle '+StringOfChar(' ',Einzug-len)+'= ('+IntToStr(Rect.Left)+', '+IntToStr(Rect.Top)+', '+IntToStr(Rect.Right)+', '+IntToStr(Rect.Bottom)+')');
end;

procedure TTraceFile.Write(Variable: String; Int: Int64);
begin
  len:=length(Variable+': Int64 ');
  Write(Variable+': Int64 '+StringOfChar(' ',Einzug-len)+'= '+InttoStr(Int));
end;

procedure TTraceFile.Write(Text: String);
var
  ZBText  : String;
  {$IFDEF DEBUGMODE}
  RegAtom : ATOM;
  LineSh  : String[250];
  Line    : String;
  List    : TStringList;
  Dummy   : Integer;
  {$ENDIF}
begin
  if Text='' then exit;
  if (DebugHand=0) and (fFileStream=nil) then
  begin
    try
      if fTrace then
      begin
        DebugHand:=FindWindow(PChar('TApplication'),PChar('DebugTracer'));
        if DebugHand=0 then
          DebugHand:=1;
      end
      else
      begin
        if fNew then
          fFileStream:=TFileStream.Create(FileName,fmCreate or fmShareDenyNone)
        else
          fFileStream:=TFileStream.Create(FileName,fmOpenWrite or fmShareDenyNone);
        fFileStream.Seek(0,soFromEnd);
      end;
    except
      if not fTrace then fFileStream:=TFileStream.Create(FileName,fmCreate or fmShareDenyNone);
    end;
    if fTrace then
      Write(#4);
    WriteLine;
    Write(DateToStr(Now)+' um '+TimeToStr(Now));
    Write(ParamStr(0));
    WriteLine;
  end;
  if not fTrace then
  begin
    ZBText:=Text+#13#10;
    fFileStream.Write(Pointer(ZBText)^,length(ZBText));
  end
  else
  begin
    {$IFDEF DEBUGMODE}
    if DebugHand<>1 then
    begin
      if length(Text)<255 then
      begin
        repeat
          RegAtom:=GlobalAddAtom(PChar(Text));
          Application.ProcessMessages;
        until RegAtom<>0;
        PostMessage(DebugHand,WM_USER+100,RegAtom,0);
      end
      else
      begin
        List:=TStringList.Create;
        List.Text:=Text;
        for Dummy:=0 to List.Count-1 do
        begin
          LineSh:=List[Dummy];
          Line:=LineSh;
          if Line<>EmptyStr then
          begin
            repeat
              RegAtom:=GlobalAddAtom(PChar(String(Line)));
              Application.ProcessMessages;
            until RegAtom<>0;
            PostMessage(DebugHand,WM_USER+100,RegAtom,0);
          end;
        end;
        List.Free;
      end;
    end;
    {$ENDIF}
  end;
end;

procedure TTraceFile.HexDump(Variable: String; Size: Integer;
  Start: Pointer);
const
  CharsPL = 16;
var
  Line   : Integer;
  Hex    : String;
  Ascii  : String;
begin
  WriteLine;
  Write(Format('%s = %d Bytes',[Variable,Size]));
  Hex:='$'+IntToHex(Integer(Start),8)+'  ';
  AScii:='';
  Line:=1;
  while Size>0 do
  begin
    if PByte(Start)^<32 then
      Ascii:=Ascii+'�'
    else
      Ascii:=Ascii+Char(PByte(Start)^);
    Hex:=Hex+IntToHex(PByte(Start)^,2)+' ';
    inc(Integer(Start));
    dec(Size);
    inc(Line);
    if Line>CharsPL then
    begin
      Write(Hex+'|'+Ascii+'|');
      Hex:='$'+IntToHex(Integer(Start),8)+'  ';
      AScii:='';
      Line:=1;
    end;
  end;
  if Line>1 then
  begin
    if Line<=CharsPL then
      Hex:=Hex+StringOfChar(' ',(CharsPL-Line+1)*3);
    Write(Hex+'|'+Ascii+'|');
  end;
  WriteLine;
end;

procedure TTraceFile.Write(Ch: Char; Count: Integer);
begin
  Write(StringOfChar(ch,Count));
end;

procedure TTraceFile.Write(Variable: String; Font: TFont);
begin
  Write(Variable+': TFont');
  Write('    Name    = '+Font.Name);
  Write('    Height  = '+IntToStr(Font.Height));
  Write('    Size    = '+IntToStr(Font.Size));
  Write('    CharSet = '+IntToStr(Font.Charset));
  Write('    Color   = '+ColorToString(Font.Color));
  Write('Ende TFont');
end;

procedure TTraceFile.Write(Variable: String; bool: boolean);
var
  Text: String;
begin
  if bool then Text:='true' else Text:='false';
  len:=length(Variable+': Boolean ');
  Write(Variable+': Boolean '+StringOfChar(' ',Einzug-len)+'= '+Text);
end;

procedure TTraceFile.Write(Variable: String; Hex: boolean; Int: Integer);
var
  Text: String;
begin
  Text:=IntToHex(Int,8);
  len:=length(Variable+': Hex ');
  Write(Variable+': Hex '+StringOfChar(' ',Einzug-len)+'= $'+Text);
end;

procedure TTraceFile.Write(Variable: String; Double: Double);
begin
  len:=length(Variable+': double ');
  Write(Variable+': double '+StringOfChar(' ',Einzug-len)+'= '+FloatToStr(Double));
end;

procedure TTraceFile.Write(Variable: String; Point: TPoint);
begin
  len:=length(Variable+': TPoint ');
  Write(Variable+': TPoint '+StringOfChar(' ',Einzug-len)+'= '+IntTOStr(Point.X)+','+IntToStr(Point.Y));
end;

procedure TTraceFile.Write(Variable: String; TypInfo: PTypeInfo;
  Value: Integer);
begin
  len:=length(Variable+': '+PTypeInfo(TypInfo).Name);
  Write(Variable+': '+PTypeInfo(TypInfo).Name+StringOfChar(' ',Einzug-len)+'= '+GetEnumName(TypInfo,Value));
end;

procedure TTraceFile.WriteLine;
begin
  if fTrace then
    Write(#1)
  else
    Write('-',90);
end;

procedure TTraceFile.WriteTime;
begin
  len:=length('Zeit: TDateTime ');
  Write('Zeit: TDateTime '+StringOfChar(' ',Einzug-len)+'= '+DateTimeToStr(now));
end;

procedure TTraceFile.StartStackDump;
begin
  Write(#2);
end;

procedure TTraceFile.EndStackDump;
begin
  Write(#3);
end;

procedure TTraceFile.TraceStack(Version: Integer);
var
  BasePtr  : Cardinal;
  StackTop : Cardinal;
  LastAdr  : Cardinal;
begin
  asm
    mov BasePtr,ebp
    MOV eax, FS:[4]
    mov stacktop,eax
  end;
  StartStackDump;
  WriteLine;
  if Version<>0 then
    Write('X-Force Version',Version);
  Write(Format('Stackdump, Textstart $%.8x',[Integer(@Textstart)]));
  WriteLine;
  try
    while (BasePtr<stackTop) and (BasePtr>$00400000) do
    begin
      LastAdr:=PDWord(BasePtr+4)^-5;
      if LastAdr>$80000000 then break;
      Write('  $'+IntToHex(LastAdr,8));
      BasePtr:=PDWord(BasePtr)^;
    end;
  except
  end;
  EndStackDump;
  WriteLine;
end;

procedure TTraceFile.SetTrace(const Value: boolean);
begin
  {$IFDEF TRACETOFILE}
  fTrace:=false;
  {$ELSE}
  fTrace := Value;
  {$ENDIF}
end;

procedure TTraceFile.Write(Strings: TStrings);
var
  Dummy: Integer;
begin
  for Dummy:=0 to Strings.Count-1 do
  begin
    Write(Strings[Dummy]);
  end;
end;

initialization

  GlobalFile:=TTraceFile.Create('Trace.txt');
  GlobalFile.TraceForm:=true;

end.

