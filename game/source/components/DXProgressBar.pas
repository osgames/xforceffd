{********************************************************************************
*										*
* Copyright (C) 2004  X-Force Team - jim_raynor@web.de				*
*										*
* This file is part of X-Force: Fight For Destiny (X-Force).			*
*										*
* X-Force is free software; you can redistribute it and/or modify		*
* it under the terms of the GNU General Public License as published by		*
* the Free Software Foundation; either version 2 of the License, or		*
* (at your option) any later version.						*
*										*
* X-Force is distributed in the hope that it will be useful,			*
* but WITHOUT ANY WARRANTY; without even the implied warranty of		*
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the			*
* GNU General Public License for more details.					*
*										*
* You should have received a copy of the GNU General Public License		*
* along with X-Force; if not, write to the Free Software			*
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA	*
*										*
*********************************************************************************
*										*
* Diese Unit stellt eine Fortschrittsanzeige (z.B. Ladebildschirm) zur 		*
* verf�gung.									*
*										*
********************************************************************************}

{$I ../Settings.inc}

unit DXProgressBar;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  DXContainer,DXDraws,Blending, math, DirectDraw, XForce_types, DirectFont;

type
  TDXProgressBar = class(TDXComponent)
  private
    fKind        : TScrollBarKind;
    fMax         : Integer;
    fValue       : double;
    fPercent     : boolean;
    fBColor      : TBlendColor;
    fText        : String;
    fCornerWidth : Integer;
    fFillColor   : TBlendColor;
    procedure SetKind(const Value: TScrollBarKind);
    procedure SetMax(const Value: Integer);
    procedure SetPercent(const Value: boolean);
    procedure SetValue(const Value: Double);
    procedure SetBColor(const Value: TBlendColor);
    procedure SetText(const Value: String);
    procedure SetFillColor(const Value: TBlendColor);
    { Private-Deklarationen }
  protected
    { Protected-Deklarationen }
  public
    constructor Create(Page: TDXPage);override;
    procedure Draw(Surface: TDirectDrawSurface;var Mem: TDDSurfaceDesc);override;
    procedure Resize(var NewLeft,NewTop,NewWidth,NewHeight: Integer);override;
    procedure ReDrawRect(DrawRect: TRect;Surface: TDirectDrawSurface;var Mem: TDDSurfaceDesc);override;
    property Kind         : TScrollBarKind read fKind write SetKind;
    property Max          : Integer read fMax write SetMax;
    property Value        : double read fValue write SetValue;
    property ShowPercent  : boolean read fPercent write SetPercent;
    property BorderColor  : TBlendColor read fBColor write SetBColor;
    property FillColor    : TBlendColor read fFillColor write SetFillColor;
    property Caption      : String read fText write SetText;
  end;

implementation

{ TDXProgressBar }

constructor TDXProgressBar.Create(Page: TDXPage);
begin
  inherited;
  fMax:=100;
  Width:=200;
  Height:=25;
  fKind:=sbHorizontal;
end;

procedure TDXProgressBar.Draw(Surface: TDirectDrawSurface;var Mem: TDDSurfaceDesc);
begin
  RedrawRect(ClientRect,Surface,Mem);
end;

procedure TDXProgressBar.ReDrawRect(DrawRect: TRect;
  Surface: TDirectDrawSurface;var Mem: TDDSurfaceDesc);
var
  p        : single;
  DFont    : TDirectFont;
begin
  if max<>0 then
  begin
    if fKind=sbHorizontal then
    begin
      p:=value/max;
      BlendRoundRect(ClientRect,130,fFillColor,Surface,Mem,fCornerWidth,[cLeftTop,cLeftBottom,cRightTop,cRightBottom],Rect(Left+1,Top+1,Left+round(p*Width)-1,Bottom-1))
    end
    else
    begin
      p:=(max-Value)/max;
      BlendRoundRect(ClientRect,130,fFillColor,Surface,Mem,fCornerWidth,[cLeftTop,cLeftBottom,cRightTop,cRightBottom],Rect(Left+1,round(p*Height)+Top+1,Right-1,Bottom-1))
    end;
  end;
  FramingRect(Surface,Mem,ClientRect,[cLeftTop,cLeftBottom,cRightTop,cRightBottom],fCornerWidth,fBColor);
  if fText='' then exit;
  DFont:=FontEngine.FindDirectFont(Font,clBlack);
  DFont.Draw(Surface,Left+(Width shr 1)-(DFont.TextWidth(fText) shr 1),Top+(Height shr 1)-(DFont.TextHeight(fText) shr 1),fText);
end;

procedure TDXProgressBar.Resize(var NewLeft, NewTop, NewWidth,
  NewHeight: Integer);
begin
  fCornerWidth:=min(11,(NewHeight div 2)-1);
end;

procedure TDXProgressBar.SetBColor(const Value: TBlendColor);
begin
  fBColor := Value;
  Redraw;
end;

procedure TDXProgressBar.SetFillColor(const Value: TBlendColor);
begin
  fFillColor := Value;
  Redraw;
end;

procedure TDXProgressBar.SetKind(const Value: TScrollBarKind);
begin
  fKind := Value;
  Container.RedrawControl(Self,Container.Surface);
end;

procedure TDXProgressBar.SetMax(const Value: Integer);
begin
  fMax := Value;
  Container.RedrawControl(Self,Container.Surface);
end;

procedure TDXProgressBar.SetPercent(const Value: boolean);
begin
  fPercent := Value;
  Container.RedrawControl(Self,Container.Surface);
end;

procedure TDXProgressBar.SetText(const Value: String);
begin
  fText := Value;
  Redraw;
end;

procedure TDXProgressBar.SetValue(const Value: Double);
begin
  fValue := math.min(fmax,math.max(Value,0));
  ReDraw;
end;

end.
