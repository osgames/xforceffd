{********************************************************************************
*										*
* Copyright (C) 2004  X-Force Team - jim_raynor@web.de				*
*										*
* This file is part of X-Force: Fight For Destiny (X-Force).			*
*										*
* X-Force is free software; you can redistribute it and/or modify		*
* it under the terms of the GNU General Public License as published by		*
* the Free Software Foundation; either version 2 of the License, or		*
* (at your option) any later version.						*
*										*
* X-Force is distributed in the hope that it will be useful,			*
* but WITHOUT ANY WARRANTY; without even the implied warranty of		*
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the			*
* GNU General Public License for more details.					*
*										*
* You should have received a copy of the GNU General Public License		*
* along with X-Force; if not, write to the Free Software			*
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA	*
*										*
*********************************************************************************
*										*
* Personlist verwaltet Vor- und Nachnamen sowie Bilder f�r Personen.		*
* �ber die Funktion CreateName wird zuf�llig ein Name erstellt.			*
* 										*
* Zugriff auf Funktionen sollte immer �ber person_api erfolgen.			*
*										*
********************************************************************************}

{$I ../Settings.inc}

unit PersonList;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  XForce_types, DXClass, DXDraws, Koding;


type
  TNameArray = Array of String[40];
  TPersonList = class(TObject)
  private
    fForeNames       : TNameArray;
    fLastNames       : TNameArray;
    fFixedNames      : TStringList;
    fCanUseList      : TStringList;
    fUsedList        : TStringList;
    fImageItem       : TPictureCollectionItem;
    function GetFaceCount: Integer;

    procedure AddForeName(Name: String);
    procedure AddLastName(Name: String);
    procedure NewGameHandler(Sender: TObject);
    procedure CustomSaveGame(Stream: TStream);
    procedure CustomLoadGame(Stream: TStream);
    { Private-Deklarationen }
  protected
    { Protected-Deklarationen }
  public
    constructor Create;
    destructor Destroy;override;
    procedure LoadNames(f: TStream);
    procedure LoadFixedNames(f: TStream);
    function CreateName(FixedName: Boolean = false): String;
    procedure SetFaces(Bitmap: TBitmap);
    procedure DrawFace(Surface: TDirectDrawSurface;x,y: Integer;Index: Integer);
    procedure DrawSmallFace(Surface: TDirectDrawSurface;x,y: Integer;Index: Integer);
    property FaceCount: Integer read GetFaceCount;
  end;

implementation

uses
  game_api, person_api, savegame_api, KD4Utils, Defines;

{ TPersonList }

{ TPersonList }

procedure TPersonList.AddForeName(Name: String);
begin
  SetLength(fForeNames,length(fForeNames)+1);
  fForeNames[high(fForeNames)]:=Trim(Name);
end;

procedure TPersonList.AddLastName(Name: String);
begin
  SetLength(fLastNames,length(fLastNames)+1);
  fLastNames[high(fLastNames)]:=Trim(Name);
end;

constructor TPersonList.Create;
begin
  person_api_init(Self);

  savegame_api_RegisterNewGameHandler(NewGameHandler);
  savegame_api_RegisterCustomSaveHandler(CustomSaveGame,CustomLoadGame,NewGameHandler,$26F71D7C);

  SetLength(fForeNames,0);
  SetLength(fLastNames,0);
  randomize;
  fImageItem := game_api_CreateImageListItem;
  fImageItem.SystemMemory:=true;
  fFixedNames:=TStringList.Create;
  fCanUseList:=TStringList.Create;
  fUsedList:=TStringList.Create;
end;

function TPersonList.CreateName(FixedName: Boolean): String;
var
  Index: Integer;

  function GetRandomName(const List: TNameArray): String;
  begin
    result:=List[random(length(List))];
  end;

begin
  if FixedName and (fCanUseList.Count>0) then
  begin
    Index:=random(fCanUseList.Count);
    result:=fCanUseList[Index];
    fCanUseList.Delete(Index);
    fUsedList.Add(result);
  end
  else
  begin
    if random(50)=10 then
      result:=GetRandomName(fForeNames)+'-'+GetRandomName(fForeNames)+' '+GetRandomName(fLastNames)
    else
      result:=GetRandomName(fForeNames)+' '+GetRandomName(fLastNames);
  end;
end;

procedure TPersonList.CustomLoadGame(Stream: TStream);
var
  Dummy: Integer;
begin
  fUsedList.Text:=ReadString(Stream);

  fCanUseList.Assign(fFixedNames);

  for Dummy:=0 to fUsedList.Count-1 do
  begin
    if fCanUseList.IndexOf(fUsedList[Dummy])<>-1 then
      fCanUseList.Delete(fCanUseList.IndexOf(fUsedList[Dummy]));
  end;
end;

procedure TPersonList.CustomSaveGame(Stream: TStream);
begin
  WriteString(Stream,fUsedList.Text);
end;

destructor TPersonList.Destroy;
begin
  fCanUseList.Free;
  fFixedNames.Free;
  fUsedList.Free;
  fImageItem.Free;
  inherited;
end;

procedure TPersonList.DrawFace(Surface: TDirectDrawSurface; x, y,
  Index: Integer);
begin
//  Surface.BltFast(x,y,fDXImage.Items[0].PatternRects[Index],0,fDXImage.Items[0].PatternSurfaces[Index]);
  fImageItem.Draw(Surface,X,Y,Index);
end;

procedure TPersonList.DrawSmallFace(Surface: TDirectDrawSurface; x, y,
  Index: Integer);
begin
  fImageItem.StretchDraw(Surface,Rect(X,Y,X+PersonSmallImageWidth,Y+PersonSmallImageHeight),Index);
end;

function TPersonList.GetFaceCount: Integer;
begin
  if fImageItem.Initialized then
    result:=fImageItem.PatternCount div 2
  else
    result:=0;
end;

procedure TPersonList.LoadFixedNames(f: TStream);
begin
  fFixedNames.LoadFromStream(f);
end;

procedure TPersonList.LoadNames(f: TStream);
var
  m       : TMemoryStream;
  Dummy   : Integer;
  Header  : TEntryHeader;
  Name    : TName;
  LastName: Boolean;
  Len     : Byte;
  NewName : String[40];
begin
  m:=TMemoryStream.Create;
  Decode(f,m);
  m.Read(Header,SizeOf(TEntryHeader));
  if Header.Version=NameVersion then
  begin
    for Dummy:=0 to Header.Entrys-1 do
    begin
      m.Read(Name,SizeOf(TName));
      AddForeName(Name.Vor);
      AddLastName(Name.Nach);
    end;
  end
  else if Header.Version=NewNameVersion then
  begin
    for Dummy:=0 to Header.Entrys-1 do
    begin
      m.Read(LastName,SizeOf(LastName));
      m.Read(Len,SizeOf(Len));
      SetLength(NewName,Len);
      m.Read(NewName[1],Len);
      if LastName then
        AddLastName(NewName)
      else
        AddForeName(NewName);
    end;
  end
  else
  begin
    m.Free;
    raise EInvalidVersion.Create('Die Datei NAMEN.DAT stammt aus einer ung�ltigen Version');
  end;
  m.Free;
end;

procedure TPersonList.NewGameHandler(Sender: TObject);
begin
  fCanUseList.Assign(fFixedNames);
  fUsedList.Clear;
end;

procedure TPersonList.SetFaces(Bitmap: TBitmap);
begin
  fImageItem.Picture.Bitmap:=Bitmap;
  fImageItem.PatternHeight:=46;
  fImageItem.PatternWidth:=40;
  fImageItem.Transparent:=false;
  fImageItem.Restore;
end;

end.

