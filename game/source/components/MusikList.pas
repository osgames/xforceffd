{********************************************************************************
*										*
* Copyright (C) 2004  X-Force Team - jim_raynor@web.de				*
*										*
* This file is part of X-Force: Fight For Destiny (X-Force).			*
*										*
* X-Force is free software; you can redistribute it and/or modify		*
* it under the terms of the GNU General Public License as published by		*
* the Free Software Foundation; either version 2 of the License, or		*
* (at your option) any later version.						*
*										*
* X-Force is distributed in the hope that it will be useful,			*
* but WITHOUT ANY WARRANTY; without even the implied warranty of		*
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the			*
* GNU General Public License for more details.					*
*										*
* You should have received a copy of the GNU General Public License		*
* along with X-Force; if not, write to the Free Software			*
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA	*
*										*
*********************************************************************************
*										*
* Diese Unit verwaltet die Musikkategorien in X-Force. Dabei besteht die 	*
* Möglichkeit die Informationen zu den Kategorien aus einer Ini-Datei zu laden  *
* oder alle Musikstücke (*.ogg,*.mp3,*.wav) aus einem Verzeichnis zu lesen	*
*										*
********************************************************************************}

{$I ../Settings.inc}

unit MusikList;

interface

uses Classes, SysUtils, TraceFile, Sequenzer;

type
  TMusikCategory = record
    Category     : String;
    List         : TStringList;
  end;

  TMusikList = class(TObject)
  private
    fFiles    : Array of TMusikCategory;
    fAktList  : TStringList;
    fSequenz  : TSequenzer;
    fIniFile  : Boolean;

    procedure AddFile(List: TStringList; FileName: String);
    function GetCategoryList(Category: String): TStringList;
  public
    constructor Create;
    destructor Destroy;override;
    procedure Clear;
    procedure ReadIniFile(IniFile: String; MusikDirectory: String);
    procedure ReadAllFiles(MusikDirectory: String);

    function SetCategory(Category: String): boolean;

    function GetNextFile: String;
    function CategoryFileCount: Integer;
  end;

const
  DefaultCategory = 'all';

implementation
{ TMusikList }

procedure TMusikList.AddFile(List: TStringList; FileName: String);
var
  ext: String;
begin
  ext:=lowercase(ExtractFileExt(FileName));
  if (ext='.ogg') or (ext='.mp3') or (ext='.wav') then
  begin
    List.Add(Filename);
  end;
end;

function TMusikList.CategoryFileCount: Integer;
begin
  if fAktList = nil then
  begin
    result:=0;
    exit;
  end;

  result:=fAktList.Count;
end;

procedure TMusikList.Clear;
var
  Dummy: Integer;
begin
  for Dummy:=0 to high(fFiles) do
    fFiles[Dummy].List.Free;

  SetLength(fFiles,0);
  fAktList:=nil;
end;

constructor TMusikList.Create;
begin
  fSequenz:=TSequenzer.Create;
  fSequenz.SetToleranz(1);
  fIniFile:=false;
end;

destructor TMusikList.Destroy;
begin
  Clear;
  fSequenz.Free;
  inherited;
end;

function TMusikList.GetCategoryList(Category: String): TStringList;
var
  Dummy: Integer;
begin
  Category:=lowercase(Category);
  for Dummy:=0 to high(fFiles) do
  begin
    if fFiles[Dummy].Category=Category then
    begin
      result:=fFiles[Dummy].List;
      exit;
    end;
  end;

  // Kategorie noch nicht vorhanden (neu anlegen)
  SetLength(fFiles,length(fFiles)+1);
  fFiles[high(fFiles)].Category:=Category;
  result:=TStringList.Create;
  fFiles[high(fFiles)].List:=result;
end;

function TMusikList.GetNextFile: String;
var
  Number: Integer;
begin
  if (fAktList=nil) then
  begin
    result:='';
    exit;
  end;
  Number:=fSequenz.GetNumber;
  if (Number>=0) and (Number<fAktList.Count) then
    result:=fAktList[Number]
  else
    result:='';
end;

procedure TMusikList.ReadAllFiles(MusikDirectory: String);
var
  List: TStringList;

  procedure SearchDir(Directory: String);
  var
    Data: TSearchRec;
  begin
    Directory:=IncludeTrailingBackslash(Directory);
    if FindFirst(Directory+'*.*',faAnyFile,Data)=0 then
    begin
      repeat
        if copy(Data.Name,1,1)<>'.' then
        begin
          if (Data.Attr and faDirectory)=faDirectory then
            SearchDir(Directory+Data.Name)
          else
            AddFile(List,Directory+Data.Name);
        end;
      until FindNext(Data)<>0;
      FindClose(Data);
    end;
  end;

begin
  fIniFile:=false;
  Clear;
  List:=GetCategoryList(DefaultCategory);
  SearchDir(MusikDirectory);
//  fAktList:=List;
//  fSequenz.SetHighest(fAktList.Count-1);
end;

procedure TMusikList.ReadIniFile(IniFile, MusikDirectory: String);
var
  IniData   : TStringList;
  Dummy     : Integer;
  List      : TStringList;
  Category  : String;

  procedure SearchFiles(Directory, Files: String);
  var
    Data: TSearchRec;
  begin
    if FindFirst(Directory+Files,faAnyFile,Data)=0 then
    begin
      Files:=IncludeTrailingBackslash(ExtractFilePath(Files));
      if length(Files)=1 then
        Files:=Directory
      else
        Files:=Directory+Files;
      repeat
        if copy(Data.Name,1,1)<>'.' then
        begin
          if (Data.Attr and faDirectory)<>faDirectory then
            AddFile(List,Files+Data.Name);
        end;
      until FindNext(Data)<>0;
      FindClose(Data);
    end;
  end;

begin
  fIniFile:=true;
  Clear;
  IniData:=TStringList.Create;
  IniData.LoadFromFile(IniFile);

  MusikDirectory:=IncludeTrailingBackslash(MusikDirectory);

  // Datei bereinigen (Kommentare und leerzeilen entfernen
  for Dummy:=Inidata.Count-1 downto 0 do
  begin
    if length(Inidata[Dummy])=0 then
    begin
      IniData.Delete(Dummy);
      Continue;
    end;
    if iniData[Dummy][1]='#' then
      IniData.Delete(Dummy); 
  end;

  // Inidatei auswerten
  List:=nil;
  for Dummy:=0 to iniData.Count-1 do
  begin
    if iniData[Dummy][1]='[' then
    begin
      Category:=trim(Copy(iniData[Dummy],2,Pos(']',iniData[Dummy])-2));
      List:=GetCategoryList(Category);
      GlobalFile.Write('Category',Category);
    end
    else if List<>nil then
    begin
      SearchFiles(MusikDirectory,IniData[Dummy]);
    end;
  end;

  IniData.Free;
end;

function TMusikList.SetCategory(Category: String): boolean;
var
  Dummy: Integer;
begin
  if not fIniFile then
  begin
    result:=false;
    if fAktList=fFiles[0].List then
      exit;
    fAktList:=fFiles[0].List;
    fSequenz.SetHighest(fAktList.Count-1);
    result:=true;
    exit;
  end;
  Category:=lowerCase(Category);
  for Dummy:=0 to high(fFiles) do
  begin
    if fFiles[Dummy].Category=Category then
    begin
      fAktList:=fFiles[Dummy].List;
      fSequenz.SetHighest(fAktList.Count-1);
      result:=true;
      exit;
    end;
  end;
  result:=false;
  GlobalFile.Write('Category nicht gefunden',Category);
end;

end.
