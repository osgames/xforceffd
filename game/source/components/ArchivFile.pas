{********************************************************************************
*										*
* Copyright (C) 2004  X-Force Team - jim_raynor@web.de				*
*										*
* This file is part of X-Force: Fight For Destiny (X-Force).			*
*										*
* X-Force is free software; you can redistribute it and/or modify		*
* it under the terms of the GNU General Public License as published by		*
* the Free Software Foundation; either version 2 of the License, or		*
* (at your option) any later version.						*
*										*
* X-Force is distributed in the hope that it will be useful,			*
* but WITHOUT ANY WARRANTY; without even the implied warranty of		*
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the			*
* GNU General Public License for more details.					*
*										*
* You should have received a copy of the GNU General Public License		*
* along with X-Force; if not, write to the Free Software			*
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA	*
*										*
*********************************************************************************
*										*
* Die Klasse verwaltet die Archive (*.pak) in X-Force und erm�glicht so		*
* mehrere Dateien zu einer Zusammenzufassen					*
*										*
********************************************************************************}

{$I ../Settings.inc}

unit ArchivFile;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Consts, Compress;


type
  TNameString  = String[15];
  TArchivEntry = record
    Name     : String;
    OffSet   : Cardinal;
    Size     : Cardinal;
    CRC      : Cardinal;
    Compress : Boolean;
    UCSize   : Cardinal;
  end;

  EInvalidStream = class(Exception);

  TArchivFile = class
  private
    FileStream  : TFileStream;
    fStream     : TMemoryStream;
    datHeader   : String[4];
    fOnChange   : TNotifyEvent;
    fFiles      : Array of TArchivEntry;
    fArchivFile : String;
    fTempFile   : PChar;
    fReadOnly   : boolean;
    function GetFile(Index: Integer): TArchivEntry;
    function GetCount: Integer;
    function GetStream: TMemoryStream;
    function GetIsFileOpen: boolean;
    function GenerateCheckSum(Stream: TStream):Cardinal;
    procedure WriteFileInfo(Stream: TStream);
    { Private-Deklarationen }
  protected
    { Protected-Deklarationen }
  public
    constructor Create;
    destructor destroy;override;
    procedure OpenArchiv(FileName: String;ReadOnly: boolean = false);
    procedure CreateArchiv(FileName: String);
    procedure CloseArchiv;
    procedure OpenRessource(Index: Integer);overload;
    procedure OpenRessource(Name: String);overload;
    procedure DeleteRessource(Index: Integer);overload;
    procedure DeleteRessource(Name: String);overload;
    procedure ReplaceRessource(Stream: TStream;Index: Integer;Compress : Boolean = false);overload;
    procedure ReplaceRessource(Stream: TStream;Name: String;Compress : Boolean = false);overload;
    function ExistRessource(Name: String):boolean;
    procedure CloseRessource;
    procedure AddRessource(Stream: TStream;Name: String;Compress : Boolean = false);overload;
    procedure AddRessource(Liste: TStrings;Compress : Boolean = false);overload;
    procedure LoadFiles;
    procedure RenameRessource(Index: Integer;NewName: String);overload;
    procedure RenameRessource(OldName: String;NewName: String);overload;
    property Stream     : TMemoryStream read GetStream;
    property Count      : Integer read GetCount;
    property OnChange   : TNotifyEvent read fOnChange write fOnChange;
    property IsFileOpen : boolean read GetIsFileOpen;
    property ArchivFile : String read fArchivFile;
    property Files[Index: Integer]: TArchivEntry read GetFile;
  end;

const
  DataBlock           : Integer = $180FCE5E;

procedure WriteString(Stream: TStream; Str: String);
function ReadString(Stream: TStream): String;

implementation

{ TArchivFile }

procedure WriteString(Stream: TStream; Str: String);
var
  Len : Integer;
begin
  Len:=Length(Str);
  Stream.Write(Len,SizeOf(Len));
  Stream.Write(Pointer(Str)^,Len);
end;

function ReadString(Stream: TStream): String;
var
  Len : Integer;
begin
  Stream.Read(Len,SizeOf(Len));
  SetString(result, nil, Len);
  Stream.Read(Pointer(result)^,len);
end;

procedure TArchivFile.AddRessource(Stream: TStream; Name: String;Compress : Boolean);
var
  FilePos      : Cardinal;
  TempStream   : TStream;
  Entry        : TArchivEntry;
  Dummy        : Integer;
  Mem          : TMemoryStream;
begin
  if FileStream=nil then
    raise Exception.Create('Keine Datei ge�ffnet');

  if fReadOnly then
    raise EInvalidOperation.Create('Im Lesemodus kann keine Ressource hinzugef�gt werden.');

  if Stream.Size=0 then exit;
  Stream.Position:=0;
  TempStream:=TFileStream.Create(fTempFile,fmCreate);
  TempStream.Write(datHeader,5);
  for Dummy:=0 to Count-1 do
  begin
    FileStream.Position:=Files[Dummy].OffSet;
    TempStream.CopyFrom(FileStream,Files[Dummy].Size);
  end;
  FilePos:=TempStream.Position;
  Entry.Name:=Name;
  Entry.OffSet:=FilePos;
  Entry.Compress:=false;
  Entry.UCSize:=Stream.Size;
  Mem:=nil;
  if (Stream.Size>102400) or Compress then
  begin
    Entry.Compress:=true;
    Mem:=TMemoryStream.Create;
    Stream.Position:=0;
    LHACompress(Stream,Mem);
    Stream:=Mem;
  end;
  Stream.Position:=0;
  Entry.Size:=Stream.Size;
  Entry.CRC:=GenerateCheckSum(Stream);
  Stream.Position:=0;
  TempStream.CopyFrom(Stream,Stream.Size);

  SetLength(fFiles,Length(fFiles)+1);
  fFiles[high(fFiles)]:=Entry;

  WriteFileInfo(TempStream);
  TempStream.Free;
  FileStream.Free;
  DeleteFile(fArchivFile);
  RenameFile(fTempFile,fArchivFile);
  FileStream:=TFileStream.Create(fArchivFile,fmOpenReadWrite);
  if Mem<>nil then
    Mem.Free;
  if Assigned(fOnChange) then fOnChange(Self);
end;

procedure TArchivFile.CloseArchiv;
begin
  if FileStream<>nil then
    CloseRessource;
  if fArchivFile=EmptyStr then
    exit;
  FileStream.Free;
  FileStream:=nil;
  fArchivFile:=EmptyStr;
  LoadFiles;
  if Assigned(fOnChange) then fOnChange(Self);
end;

procedure TArchivFile.OpenArchiv(FileName: String;ReadOnly: boolean);
var
  Header: String[4];
begin
  CloseArchiv;
  if ReadOnly then
    FileStream:=TFileStream.Create(FileName,fmOpenRead or fmShareDenyNone)
  else
    FileStream:=TFileStream.Create(FileName,fmOpenReadWrite);
  fReadOnly:=ReadOnly;
  SetLength(Header,5);
  FileStream.Read(Header,5);
  if Header<>datHeader then
  begin
    FileStream.Free;
    FileStream:=nil;
    raise Exception.Create('Keine g�ltige Archivdatei');
  end;
  Header:='';
  fArchivFile:=FileName;
  LoadFiles;
  fStream:=nil;
end;

function TArchivFile.GetCount: Integer;
begin
  result:=length(fFiles);
end;

function TArchivFile.GetFile(Index: Integer): TArchivEntry;
begin
  if (Index<0) or (Index>Count-1) then
    raise Exception.CreateFmt('Ung�ltiger Index %d',[Index]);
  result:=fFiles[Index];
end;

function TArchivFile.GetStream: TMemoryStream;
begin
  if fStream=nil then
    raise Exception.Create('Keine Datei ge�ffnet');
  result:=fStream;
end;

procedure TArchivFile.LoadFiles;
var
  InfoPos  : Cardinal;
  Entrys   : Integer;
  Dummy    : Integer;
  Block    : Integer;
  fVersion : Integer;
  OldName  : TNameString;
begin
  SetLength(fFiles,0);
  if FIleStream=nil then
  begin
    if Assigned(fOnChange) then fOnChange(Self);
    exit;
  end;
  FileStream.Position:=FileStream.Size-4;
  FileStream.Read(InfoPos,SizeOf(InfoPos));
  FileStream.Position:=InfoPos;
  FileStream.Read(Block,4);
  case Block of
    $180FCE5E : fVersion:=1;
    $5714CE2D : fVersion:=0;
    else
      raise Exception.Create('Ung�ltige Datei '+fArchivFile);
  end;
  FileStream.Read(Entrys,SizeOf(Entrys));
  SetLength(fFiles,Entrys);
  for Dummy:=0 to Entrys-1 do
  begin
    case fVersion of
      0:
      begin
        FileStream.Read(OldName,SizeOf(TNameString));
        with fFiles[Dummy] do
        begin
          Name:=OldName;
          FileStream.Read(OffSet,Sizeof(Cardinal));
          FileStream.Read(Size,Sizeof(Cardinal));
          FileStream.Read(CRC,Sizeof(Cardinal));
          Compress:=false;
          UCSize:=Size;
        end;
      end;
      1:
      begin
        with fFiles[Dummy] do
        begin
          Name:=ReadString(FileStream);
          FileStream.Read(OffSet,Sizeof(Cardinal));
          FileStream.Read(Size,Sizeof(Cardinal));
          FileStream.Read(CRC,Sizeof(Cardinal));
          FileStream.Read(Compress,Sizeof(Boolean));
          FileStream.Read(UCSize,Sizeof(Cardinal));
        end;
      end;
    end;
  end;
  if Assigned(fOnChange) then fOnChange(Self);
end;

constructor TArchivFile.Create;
begin
  inherited;
  datHeader:='DAT'+#18;
  FileStream:=nil;
  fTempFile:=AllocMem(200);
  GetEnvironmentVariable('TMP',PChar(fTempFile),200);
  fTempFile:=strCat(fTempFile,PChar('\archiv.dat'));
  fStream:=nil;
end;

procedure TArchivFile.CreateArchiv(FileName: String);
var
  Files : Integer;
  Pos   : Cardinal;
  ID    : Cardinal;
begin
  CloseArchiv;
  FileStream:=TFileStream.Create(FileName,fmCreate);
  FileStream.Write(datHeader,5);
  Pos:=FileStream.Position;
  Files:=0;
  ID:=DataBlock;
  FileStream.Write(ID,4);
  FileStream.Write(Files,SizeOf(Files));
  FileStream.Write(Pos,SizeOf(Pos));
  LoadFiles;
  fArchivFile:=FileName;
  fReadOnly:=false;
  if Assigned(fOnChange) then fOnChange(Self);
end;

procedure TArchivFile.CloseRessource;
begin
  if fStream<>nil then
  begin
    fStream.Free;
    fStream:=nil;
  end;
end;

procedure TArchivFile.OpenRessource(Index: Integer);
var
  CRC: Cardinal;
  Temp: TMemoryStream;
begin
  if FileStream=nil then
    raise Exception.Create('Keine Datei ge�ffnet');

  if (Index<0) or (Index>Count-1) then
    raise Exception.CreateFmt('Ung�ltiger Index %d',[Index]);

  CloseRessource;
  fStream:=TMemoryStream.Create;
  FileStream.Position:=Files[Index].OffSet;
  fStream.CopyFrom(FileStream,Files[Index].Size);
  fStream.Position:=0;
  CRC:=GenerateCheckSum(fStream);
  if CRC<>Files[Index].CRC then
  begin
    fStream.Free;
    fStream:=nil;
    raise EInvalidStream.Create('Es liegt ein Dateifehler vor. Die Daten sind fehlerhaft.');
  end;
  if Files[Index].Compress then
  begin
    Temp:=TMemoryStream.Create;
    fStream.Position:=0;
    LHAExpand(fStream,Temp);
    fStream.Free;
    fStream:=Temp;
  end;
  fStream.Position:=0;
end;

function TArchivFile.GetIsFileOpen: boolean;
begin
  result:=FileStream<>nil;
end;

procedure TArchivFile.RenameRessource(Index: Integer; NewName: String);
var
  TempStream   : TFileStream;
  Dummy        : Integer;
begin
  if fReadOnly then
    raise EInvalidOperation.Create('Im Lesemodus kann keine Ressource hinzugef�gt werden.');

  if FileStream=nil then
    raise Exception.Create('Keine Datei ge�ffnet');

  TempStream:=TFileStream.Create(fTempFile,fmCreate);
  TempStream.Write(datHeader,5);
  for Dummy:=0 to Count-1 do
  begin
    FileStream.Position:=Files[Dummy].OffSet;
    TempStream.CopyFrom(FileStream,Files[Dummy].Size);
  end;
  fFiles[Index].Name:=NewName;
  WriteFileInfo(TempStream);
  TempStream.Free;
  FileStream.Free;
  DeleteFile(fArchivFile);
  RenameFile(fTempFile,fArchivFile);
  FileStream:=TFileStream.Create(fArchivFile,fmOpenReadWrite);
end;

procedure TArchivFile.OpenRessource(Name: String);
var
  Dummy: Integer;
begin
  for Dummy:=0 to Count-1 do
  begin
    if fFiles[Dummy].Name=Name then
    begin
      OpenRessource(Dummy);
      exit;
    end;
  end;
   raise EResNotFound.Create('Ressource '''+Name+''' ist nicht im Archiv!');
end;

procedure TArchivFile.DeleteRessource(Index: Integer);
var
  TempStream  : TFileStream;
  Dummy       : Integer;
begin
  if FileStream=nil then
    raise Exception.Create('Keine Datei ge�ffnet');

  if fReadOnly then
    raise EInvalidOperation.Create('Im Lesemodus kann keine Ressource gel�scht werden.');

  TempStream:=TFileStream.Create(fTempFile,fmCreate);
  TempStream.Write(datHeader,5);
  for Dummy:=0 to Count-1 do
  begin
    if Dummy<>Index then
    begin
      FileStream.Position:=Files[Dummy].OffSet;
      fFiles[Dummy].OffSet:=TempStream.Position;
      TempStream.CopyFrom(FileStream,Files[Dummy].Size);
    end;
  end;
  if Count>1 then
  begin
    for Dummy:=Index to Count-2 do
    begin
      fFiles[Dummy]:=fFiles[Dummy+1];
    end;
  end;

  SetLength(fFiles,length(fFiles)-1);
  WriteFileInfo(TempStream);
  TempStream.Free;
  FileStream.Free;
  DeleteFile(fArchivFile);
  RenameFile(fTempFile,fArchivFile);
  FileStream:=TFileStream.Create(fArchivFile,fmOpenReadWrite);
  if Assigned(fOnChange) then fOnChange(Self);
end;

procedure TArchivFile.DeleteRessource(Name: String);
var
  Dummy: Integer;
begin
  if FileStream=nil then
    raise Exception.Create('Keine Datei ge�ffnet');

  if fReadOnly then
    raise EInvalidOperation.Create('Im Lesemodus kann keine Ressource gel�scht werden.');

  for Dummy:=0 to Count-1 do
  begin
    if fFiles[Dummy].Name=Name then
    begin
      DeleteRessource(Dummy);
      exit;
    end;
  end;
end;

procedure TArchivFile.ReplaceRessource(Stream: TStream; Index: Integer;Compress : Boolean);
var
  TempStream : TFileStream;
  Dummy      : Integer;
  Mem        : TMemoryStream;
begin
  if FileStream=nil then
    raise Exception.Create('Keine Datei ge�ffnet');

  if fReadOnly then
    raise EInvalidOperation.Create('Im Lesemodus kann keine Ressource ersetzt werden.');

  if Stream.Size=0 then
  begin
    DeleteRessource(Index);
    LoadFiles;
    exit;
  end;

  TempStream:=TFileStream.Create(fTempFile,fmCreate);
  TempStream.Write(datHeader,5);
  Stream.Position:=0;
  Mem:=nil;
  for Dummy:=0 to Count-1 do
  begin
    FileStream.Position:=fFiles[Dummy].OffSet;
    fFiles[Dummy].OffSet:=TempStream.Position;

    if Dummy=Index then
    begin
      fFiles[Dummy].UCSize:=Stream.Size;
      fFiles[Dummy].Compress:=false;

      if (Stream.Size>102400) or Compress then
      begin
        fFiles[Dummy].Compress:=true;
        if Mem<>nil then
          Mem.Free;
        Mem:=TMemoryStream.Create;
        Stream.Position:=0;
        LHACompress(Stream,Mem);
        Stream:=Mem;
      end;
      Stream.Position:=0;

      fFiles[Dummy].CRC:=GenerateCheckSum(Stream);
      fFiles[Dummy].Size:=Stream.Size;

      Stream.Position:=0;
      TempStream.CopyFrom(Stream,0);
    end
    else
      TempStream.CopyFrom(FileStream,Files[Dummy].Size);
  end;
  if Mem<>nil then
    Mem.Free;

  WriteFileInfo(TempStream);
  TempStream.Free;
  FileStream.Free;

  Assert(CopyFile(PChar(fTempFile),PChar(fArchivFile),false));

  DeleteFile(fTempFile);

  FileStream:=TFileStream.Create(fArchivFile,fmOpenReadWrite);
  if Assigned(fOnChange) then fOnChange(Self);
end;

procedure TArchivFile.ReplaceRessource(Stream: TStream; Name: String;Compress : Boolean);
var
  Dummy: Integer;
begin
  if fReadOnly then
    raise EInvalidOperation.Create('Im Lesemodus kann keine Ressource ersetzt werden.');

  for Dummy:=0 to Count-1 do
  begin
    if fFiles[Dummy].Name=Name then
    begin
      ReplaceRessource(Stream,Dummy,Compress);
      exit;
    end;
  end;
  AddRessource(Stream,Name,Compress);
end;

procedure TArchivFile.AddRessource(Liste: TStrings;Compress : Boolean);
var
  TempStream : TFileStream;
  Mem        : TMemoryStream;
  Dummy      : Integer;
  Stream     : TStream;
begin
  if FileStream=nil then
    raise Exception.Create('Keine Datei ge�ffnet');

  if fReadOnly then
    raise EInvalidOperation.Create('Im Lesemodus kann keine Ressource hinzugef�gt werden.');

  TempStream:=TFileStream.Create(fTempFile,fmCreate);
  TempStream.Write(datHeader,5);
  for Dummy:=0 to Count-1 do
  begin
    FileStream.Position:=Files[Dummy-1].OffSet;
    TempStream.CopyFrom(FileStream,Files[Dummy-1].Size);
  end;
  for Dummy:=0 to Liste.Count-1 do
  begin
    try
      Stream:=TFileStream.Create(Liste[Dummy],fmOpenRead);
      SetLength(fFiles,Count+1);
      with fFiles[high(fFiles)] do
      begin
        Name:=ExtractFileName(Liste[Dummy]);
        OffSet:=TempStream.Position;
        Compress:=false;
        UCSize:=Stream.Size;
      end;
      if Stream.Size>102400 then
      begin
        fFiles[high(fFiles)].Compress:=true;
        Mem:=TMemoryStream.Create;
        Stream.Position:=0;
        LHACompress(Stream,Mem);
        Stream.Free;
        Stream:=Mem;
      end;
      Stream.Position:=0;
      fFiles[high(fFiles)].CRC:=GenerateCheckSum(Stream);
      fFiles[high(fFiles)].Size:=Stream.Size;
      Stream.Position:=0;
      TempStream.CopyFrom(Stream,0);
      Stream.Free;
    except
      raise;
    end;
  end;
  WriteFileInfo(TempStream);
  TempStream.Free;
  FileStream.Free;
  DeleteFile(fArchivFile);
  RenameFile(fTempFile,fArchivFile);
  FileStream:=TFileStream.Create(fArchivFile,fmOpenReadWrite);
  LoadFiles;
end;

function TArchivFile.ExistRessource(Name: String): boolean;
var
  Dummy: Integer;
begin
  for Dummy:=0 to Count-1 do
  begin
    if fFiles[Dummy].Name=Name then
    begin
      result:=true;
      exit;
    end;
  end;
  result:=false;
end;

destructor TArchivFile.destroy;
begin
  FreeMem(fTempFile);
  CloseArchiv;
  inherited;
end;

function TArchivFile.GenerateCheckSum(Stream: TStream): Cardinal;
var
  EOF  : boolean;
  Wert : Cardinal;
begin
  result:=$80808080;
  EOF:=false;
  while not EOF do
  begin
    EOF:=(Stream.Read(Wert,4)<4);
    result:=result xor Wert;
  end;
end;

procedure TArchivFile.RenameRessource(OldName, NewName: String);
var
  Dummy: Integer;
begin
  for Dummy:=0 to Count-1 do
  begin
    if fFiles[Dummy].Name=OldName then RenameRessource(Dummy,NewName);
  end;
end;

procedure TArchivFile.WriteFileInfo(Stream: TStream);
var
  InfoPos : Integer;
  Dummy   : Integer;
  Entrys  : Integer;
begin
  InfoPos:=Stream.Position;
  Dummy:=DataBlock;
  Stream.Write(Dummy,4);
  Entrys:=Count;
  Stream.Write(Entrys,SizeOf(Entrys));
  for Dummy:=0 to Count-1 do
  begin
    with fFiles[Dummy] do
    begin
      WriteString(Stream,Name);
      Stream.Write(OffSet,SizeOf(Cardinal));
      Stream.Write(Size,SizeOf(Cardinal));
      Stream.Write(CRC,SizeOf(Cardinal));
      Stream.Write(Compress,SizeOf(Boolean));
      Stream.Write(UCSize,SizeOf(Cardinal));
    end;
  end;
  Stream.Write(InfoPos,SizeOf(InfoPos));
end;

end.
