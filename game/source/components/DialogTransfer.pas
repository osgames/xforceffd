{********************************************************************************
*										*
* Copyright (C) 2004  X-Force Team - jim_raynor@web.de				*
*										*
* This file is part of X-Force: Fight For Destiny (X-Force).			*
*										*
* X-Force is free software; you can redistribute it and/or modify		*
* it under the terms of the GNU General Public License as published by		*
* the Free Software Foundation; either version 2 of the License, or		*
* (at your option) any later version.						*
*										*
* X-Force is distributed in the hope that it will be useful,			*
* but WITHOUT ANY WARRANTY; without even the implied warranty of		*
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the			*
* GNU General Public License for more details.					*
*										*
* You should have received a copy of the GNU General Public License		*
* along with X-Force; if not, write to the Free Software			*
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA	*
*										*
*********************************************************************************
*										*
* Stellt den Ausrüstungstransfer Dialog zur Verfügung				*
*										*
********************************************************************************}

{$I ../Settings.inc}

unit DialogTransfer;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ModalDialog, DXGroupCaption, DXBitmapButton, DXContainer, XForce_types, Blending,
  DXCheckBox, DXSpinEdit, DXListBox, BasisListe, StringConst;

type
  TDialogTransfer = class(TModalDialog)
  private
    GroupHeader       : TDXGroupCaption;
    ItemLabel         : TDXGroupCaption;
    CountLabel        : TDXGroupCaption;
    CountSpinBox      : TDXSpinEdit;
    BasisLabel        : TDXGroupCaption;
    BasisListBox      : TDXListBox;
    OKButton          : TDXBitmapButton;
    CancelButton      : TDXBitmapButton;
    fItem             : TLagerItem;
    procedure SetItem(const Value: TLagerItem);
    procedure ButtonClick(Sender: TObject);

    procedure ChangeCountSpinBox(Sender: TObject);
    procedure PressKey(Sender: TObject; var Key: Word; Shift: TShiftState);
    function GetBasis: TBasis;
    function GetItemCount: Integer;
  protected
    procedure BeforeShow;override;
  public
    constructor Create(Page: TDXPage);override;

    property Item     : TLagerItem  read fItem write SetItem;
    property Basis    : TBasis read GetBasis;
    property ItemCount: Integer read GetItemCount;
  end;

implementation

uses
  basis_api;

{ TDialogTransfer }

procedure TDialogTransfer.BeforeShow;
begin
  inherited;

  GroupHeader.Font.Style:=[fsBold];
  GroupHeader.Font.Size:=Font.Size+4;
  GroupHeader.FontColor:=clYellow;
  GroupHeader.Caption:=ST0403260003;
end;

procedure TDialogTransfer.ButtonClick(Sender: TObject);
begin
  ReadyShow;
  if (Sender as TDXBitmapButton).Tag=0 then
    fResult:=mrOK
  else
    fResult:=mrCancel;
end;

procedure TDialogTransfer.ChangeCountSpinBox(Sender: TObject);
begin
  OKButton.Enabled:=CountSpinBox.Value>0;
end;

constructor TDialogTransfer.Create(Page: TDXPage);
begin
  inherited;

  { Überschrift }
  GroupHeader:=TDXGroupCaption.Create(Page);
  with GroupHeader do
  begin
    Visible:=false;
    BorderColor:=$00400000;
    BlendColor:=bcDarkNavy;
    RoundCorners:=rcTop;
    AlphaValue:=200;
    Width:=323;
    Height:=25;
    Alignment:=taCenter;
  end;
  AddComponent(GroupHeader);

  { Ausrüstungsname }
  ItemLabel:=TDXGroupCaption.Create(Page);
  with ItemLabel do
  begin
    Visible:=false;
    BorderColor:=$00400000;
    BlendColor:=bcDarkNavy;
    RoundCorners:=rcNone;
    AlphaValue:=200;
    Width:=GroupHeader.Width;
    Height:=20;
    Top:=GroupHeader.Bottom+1;
    Alignment:=taLeftJustify;
    Font.Style:=[fsBold];
  end;
  AddComponent(ItemLabel);

  { Beschrifter Anzahl }
  CountLabel:=TDXGroupCaption.Create(Page);
  with CountLabel do
  begin
    Visible:=false;
    BorderColor:=$00400000;
    BlendColor:=bcDarkNavy;
    RoundCorners:=rcNone;
    AlphaValue:=200;
    Width:=150;
    Height:=20;
    Top:=ItemLabel.Bottom+1;
    Alignment:=taLeftJustify;
    Caption:=LCount;
  end;
  AddComponent(CountLabel);

  { Anzahl der zu transferierenden Ausrüstung }
  CountSpinBox:=TDXSpinEdit.Create(Page);
  with CountSpinBox do
  begin
    Width:=GroupHeader.Width-151;
    Height:=20;
    Left:=CountLabel.Right+1;
    Top:=CountLabel.Top;
    Min:=0;
    Step:=5;
    SmallStepButton:=true;
    SmallStep:=1;

    FormatString:=FFloat;
    BlendColor:=bcDarkNavy;
    BorderColor:=$00400000;
    RoundCorners:=rcNone;
    FocusColor:=clNavy;
    TopMargin:=3;
    BlendValue:=175;
    OnIncrement:=ChangeCountSpinBox;
    OnDecrement:=ChangeCountSpinBox;
    OnKeyPress:=PressKey;
  end;
  AddComponent(CountSpinBox);

  { Beschrifter Basis }
  BasisLabel:=TDXGroupCaption.Create(Page);
  with BasisLabel do
  begin
    Visible:=false;
    BorderColor:=$00400000;
    BlendColor:=bcDarkNavy;
    RoundCorners:=rcNone;
    AlphaValue:=200;
    Width:=GroupHeader.Width;
    Height:=20;
    Top:=CountSpinBox.Bottom+1;
    Alignment:=taLeftJustify;
    Caption:=ST0403260004;
  end;
  AddComponent(BasisLabel);

  { Listbox zum Auswählen }
  BasisListBox:=TDXListBox.Create(Page);
  with BasisListBox do
  begin
    ItemHeight:=17;
    Visible:=false;
    RoundCorners:=rcNone;
    Width:=GroupHeader.Width;
    Top:=BasisLabel.Bottom+1;
//    OnDblClick:=ListDblClick;
    OnKeyPress:=PressKey;
    FirstColor:=bcDarkNavy;
    AlphaValue:=200;
  end;
  AddComponent(BasisListBox);

  { Einstellungen für den OK Button }
  OKButton:=TDXBitmapButton.Create(Page);
  with OKButton do
  begin
    Visible:=false;
    OnClick:=ButtonClick;
    Text:=BOK;
    Width:=GroupHeader.Width div 2;
    Top:=GroupHeader.Bottom+1;
    Font:=Font;
    RoundCorners:=rcLeftBottom;
    Height:=28;
    BlendAlpha:=150;
    BlendColor:=bcDarkNavy;
    FirstColor:=$00400000;
    SecondColor:=clBlue;
    AccerlateColor:=clYellow;
    Tag:=0;
  end;
  AddComponent(OKButton);

  { Einstellungen für den Abbrechen Button }
  CancelButton:=TDXBitmapButton.Create(Page);
  with CancelButton do
  begin
    Visible:=false;
    Text:=BCancel;
    Top:=GroupHeader.Bottom+1;
    Width:=GroupHeader.Width div 2;
    Left:=OKButton.Right+1;
    Font:=Font;
    Height:=28;
    RoundCorners:=rcRightBottom;
    OnClick:=ButtonClick;
    BlendAlpha:=150;
    FirstColor:=$00400000;
    BlendColor:=bcDarkNavy;
    SecondColor:=clBlue;
    AccerlateColor:=clYellow;
    Tag:=1;
  end;
  AddComponent(CancelButton);
end;

function TDialogTransfer.GetBasis: TBasis;
begin
  Assert(BasisListBox.ItemIndex<>-1);
  result:=TBasis(BasisListBox.Items.Objects[BasisListBox.ItemIndex]);
  Assert(result<>nil);
end;

function TDialogTransfer.GetItemCount: Integer;
begin
  Result:=CountSpinBox.Value;
end;

procedure TDialogTransfer.PressKey(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key=13 then
    OKButton.DoClick;
end;

procedure TDialogTransfer.SetItem(const Value: TLagerItem);
var
  Dummy: Integer;
  Basis: TBasis;
begin
  fItem := Value;
  ItemLabel.Caption:=Value.Name;

  BasisListBox.Items.Clear;
  for Dummy:=0 to basis_api_GetBasisCount-1 do
  begin
    Basis:=basis_api_GetBasisFromIndex(Dummy);
    if Basis<>basis_api_GetSelectedBasis then
      BasisListBox.Items.AddObject(Basis.Name,Basis);
  end;
  BasisListBox.Height:=BasisListBox.Items.Count*BasisListBox.ItemHeight;

  OkButton.Top:=BasisListBox.Bottom+1;
  CancelButton.Top:=BasisListBox.Bottom+1;

  CountSpinBox.Max:=Value.Anzahl;
  CountSpinBox.Value:=0;

  OKButton.Enabled:=false;
end;

end.
