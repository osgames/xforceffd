{********************************************************************************
*										*
* Copyright (C) 2004  X-Force Team - jim_raynor@web.de				*
*										*
* This file is part of X-Force: Fight For Destiny (X-Force).			*
*										*
* X-Force is free software; you can redistribute it and/or modify		*
* it under the terms of the GNU General Public License as published by		*
* the Free Software Foundation; either version 2 of the License, or		*
* (at your option) any later version.						*
*										*
* X-Force is distributed in the hope that it will be useful,			*
* but WITHOUT ANY WARRANTY; without even the implied warranty of		*
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the			*
* GNU General Public License for more details.					*
*										*
* You should have received a copy of the GNU General Public License		*
* along with X-Force; if not, write to the Free Software			*
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA	*
*										*
*********************************************************************************
*										*
* Anzeige des Punktestatuses							*
*										*
********************************************************************************}

{$I ../Settings.inc}

unit DXPointStatus;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  DXContainer, DXDraws, XForce_types, StringConst ,OrganisationList, KD4Utils,
  Defines, Blending, DirectDraw, DirectFont;

type
  TDXPointStatus = class(TDXComponent)
  private
    fPoints     : TPunkteStatus;
    fInfoType   : TInfoType;
    fFinanz     : TMonthStatus;
    fOrgan      : TOrganisationList;
    fFormat     : String;
    fKapital    : Integer;
    fMem        : TDDSurfaceDesc;
    procedure SetPoints(const Value: TPunkteStatus);
    procedure SetFinanz(const Value: TMonthStatus);
    procedure SetOrgan(const Value: TOrganisationList);
    { Private-Deklarationen }
  protected
    procedure DrawEntry(var DrawTop: Integer;Text1: String;Zahl: Integer;Surface: TDirectDrawSurface;Bold: Boolean = false);overload;
    procedure DrawEntry(var DrawTop: Integer;Text1: String;Zahl: Integer;Surface: TDirectDrawSurface;Font: TDirectFont);overload;
    procedure DrawFrame(Surface: TDirectDrawSurface; BorderColor: TColor;TopFrame: boolean; var DrawTop: Integer);
    procedure DrawLine(var DrawTop: Integer;Surface: TDirectDrawSurface);
    procedure DrawPoints(Surface: TDirectDrawSurface);
    procedure DrawFinanz(Surface: TDirectDrawSurface);
    procedure DrawOrgan(Surface: TDirectDrawSurface);
    { Protected-Deklarationen }
  public
    { Public-Deklarationen }
    procedure Draw(Surface: TDirectDrawSurface;var Mem: TDDSurfaceDesc);override;

    property Points         : TPunkteStatus read fPoints write SetPoints;
    property Finanz         : TMonthStatus read fFinanz write SetFinanz;
    property Organisation   : TOrganisationList read fOrgan write SetOrgan;
    property Kapital        : Integer read fKapital write fKapital;
  end;

  // Informationen zur Aufbereitung der Punkte
  const
    PointsInfo : Array[0..13] of record
                                   Line : Boolean;
                                   Art  : TPunkteBuch;
                                 end =
                 ((Line: false; Art: pbForsch),
                  (Line: false; Art: pbUpgrade),
                  (Line: false; Art: pbUFOweg),
                  (Line: false; Art: pbUFOAbgeschossen),
                  (Line: false; Art: pbRaumschiffZerstoert),
                  (Line: false; Art: pbInfiltration),
                  (Line: true ; Art: pbForsch),
                  (Line: false; Art: pbEinsatzWin),
                  (Line: false; Art: pbEinsatzLoose),
                  (Line: false; Art: pbSoldatGetoetet),
                  (Line: false; Art: pbAlienGetoetet),
                  (Line: false; Art: pbItemFound),
                  (Line: false; Art: pbItemLost),
                  (Line: false; Art: pbTrefferBilanz));

implementation

{ TDXPointStatus }

procedure TDXPointStatus.DrawFrame(Surface: TDirectDrawSurface;
  BorderColor: TColor; TopFrame: boolean; var DrawTop: Integer);
begin
  if TopFrame then
  begin
    if AlphaElements then
      BlendRoundRect(Rect(Left,Top,Right,Top+20),150,bcMaroon,Surface,fMem,10,[cLeftTop,cRightTop],ClientRect);

    HLine(Surface,fMem,Left,Right-1,Top+20,BorderColor);
  end
  else
  begin
    if AlphaElements then
      BlendRoundRect(Rect(Left,DrawTop,Right,DrawTop+20),150,bcMaroon,Surface,fMem,10,[cLeftBottom,cRightBottom],ClientRect);

    Rectangle(Surface,fMem,Left,DrawTop,Right,DrawTop+1,BorderColor);

    FramingRect(Surface,fMem,Rect(Left,Top,Right,DrawTop+20),[cLeftTop,cRightTop,cLeftBottom,cRightBottom],10,BorderColor);
    inc(DrawTop,2);
  end;
end;

procedure TDXPointStatus.Draw(Surface: TDirectDrawSurface;var Mem: TDDSurfaceDesc);
begin
  fMem:=Mem;
  case fInfoType of
    itPunkte       : DrawPoints(Surface);
    itFinanzen     : DrawFinanz(Surface);
    itOrganisation : DrawOrgan(Surface);
  end;
end;

procedure TDXPointStatus.DrawEntry(var DrawTop: Integer; Text1: String;
  Zahl: Integer; Surface: TDirectDrawSurface; Bold: Boolean);
var
  Text2    : String;
  Font     : TDirectFont;
begin
  if DrawTop>Bottom then exit;
  if Zahl>0 then
  begin
    if Bold then
      Font:=GreenBStdFont
    else
      Font:=GreenStdFont
  end
  else if Zahl<0 then
  begin
    if Bold then
      Font:=MaroonBStdFont
    else
      Font:=MaroonStdFont
  end
  else
  begin
    if Bold then
      Font:=YellowBStdFont
    else
      Font:=YellowStdFont
  end;
  Text2:=Format(fFormat,[Zahl/1]);
  Font.Draw(Surface,Left+4,DrawTop,Text1);
  Font.Draw(Surface,Right-4-Font.TextWidth(Text2),DrawTop,Text2);
  Inc(DrawTop,18);
end;

procedure TDXPointStatus.DrawEntry(var DrawTop: Integer; Text1: String;
  Zahl: Integer; Surface: TDirectDrawSurface; Font: TDirectFont);
var
  Text2    : String;
  OldStyle : TFontStyles;
begin
  if DrawTop>Bottom then exit;
  Text2:=Format(fFormat,[Zahl/1]);
  Font.Draw(Surface,Left+4,DrawTop,Text1);
  Font.Draw(Surface,Right-4-Font.TextWidth(Text2),DrawTop,Text2);
  Inc(DrawTop,18);
end;

procedure TDXPointStatus.DrawFinanz(Surface: TDirectDrawSurface);
var
  DrawTop      : Integer;
  Gesamt       : Integer;

  procedure DrawGroup(Arten: Array of TKapital;Bezeichnung: String);
  var
    Dummy      : Integer;
    Abschnitt  : Integer;
  begin
    Abschnitt:=0;
    for Dummy:=0 to high(Arten) do
    begin
      inc(Abschnitt,fFinanz[Arten[Dummy]]);
    end;
    inc(Gesamt,Abschnitt);
    DrawEntry(DrawTop,Bezeichnung,Abschnitt,Surface);
  end;

begin
  Gesamt:=0;
  DrawFrame(Surface,bcMaroon,true,DrawTop);
  WhiteBStdFont.Draw(Surface,Left+4,Top+4,LShortBilanz);
  DrawTop:=Top+46;
  { Soldaten }
  DrawGroup([kbSVK,kbSK,kbSG,kbSEN],IASoldaten);
  { Wissenschaftler }
  DrawGroup([kbFVK,kbFK,kbFG,kbFEN],LWissenschaf);
  { Techniker }
  DrawGroup([kbHVK,kbHK,kbHG,kbHEN],IATechniker);
  { Ausrüstung }
  DrawGroup([kbLVK,kbLK,kbPG],LItems);
  { Raumschiffe }
  DrawGroup([kbRB,kbRV,kbTrK],LRaumschiffe);
  { Basis }
  DrawGroup([kbBB],LBasis);
  { Ausbildung }
  DrawGroup([kbTK],LTrainLabel);
  { Sonstiges }
  DrawGroup([kbEG,kbFiU,kbSpK,kbSE],LSonstiges);

  { Gesamt }
  DrawLine(DrawTop,Surface);
  DrawEntry(DrawTop,LBilanz,Gesamt,Surface,true);
  DrawFrame(Surface,bcMaroon,false,DrawTop);
  DrawEntry(DrawTop,LNachMonth,fKapital,Surface,WhiteBStdFont);

  { Vormonat }
  DrawTop:=Top+22;
  DrawEntry(DrawTop,LVorMonth,fKapital-Gesamt,Surface,true);
  DrawLine(DrawTop,Surface);
end;

procedure TDXPointStatus.DrawLine(var DrawTop: Integer;
  Surface: TDirectDrawSurface);
begin
  if DrawTop>Bottom then exit;
  HLine(Surface,fMem,Left+1,Right-1,DrawTop,bcMaroon);
  inc(DrawTop,3);
end;

procedure TDXPointStatus.DrawOrgan(Surface: TDirectDrawSurface);
var
  DrawTop : Integer;
  Gesamt  : Integer;
  Gewinn  : Array[TOrganisationStatus] of Integer;
  Dummy   : Integer;
begin
  DrawFrame(Surface,bcMaroon,true,DrawTop);
  WhiteBStdFont.Draw(Surface,Left+4,Top+4,LZFassung);
  DrawTop:=Top+22;
  Gesamt:=0;
  FillChar(Gewinn,SizeOf(Gewinn),#0);
  for Dummy:=0 to fOrgan.Count-1 do
  begin
    inc(Gesamt,fOrgan.Hilfe(Dummy));
    inc(Gewinn[OrganStatus(fOrgan[Dummy].Friendly)],fOrgan.Hilfe(Dummy));
  end;
  DrawEntry(DrawTop,PSAlliance,Gewinn[osAllianz],Surface,LimeStdFont);
  DrawEntry(DrawTop,PSFriendly,Gewinn[osFriendly],Surface,GreenStdFont);
  DrawEntry(DrawTop,PSNeutral,Gewinn[osNeutral],Surface,YellowStdFont);
  DrawEntry(DrawTop,PSUnFriendly,Gewinn[osUnfriendly],Surface,MaroonStdFont);
  DrawEntry(DrawTop,PSEnemy,Gewinn[osEnemy],Surface,RedStdFont);
  DrawFrame(Surface,bcMaroon,false,DrawTop);
  DrawEntry(DrawTop,LCGesamt,Gesamt,Surface,WhiteBStdFont);
end;

procedure TDXPointStatus.DrawPoints(Surface: TDirectDrawSurface);
var
  DrawTop : Integer;
  Gesamt  : Integer;
  Dummy   : Integer;
  Arten   : TPunkteBuch;
begin
  DrawFrame(Surface,bcMaroon,true,DrawTop);
  WhiteBStdFont.Draw(Surface,Left+4,Top+4,LPoints);
  DrawTop:=Top+22;
  for Dummy:=0 to high(PointsInfo) do
  begin
    with PointsInfo[Dummy] do
    begin
      if Line then
        DrawLine(DrawTop,Surface)
      else
      begin
        DrawEntry(DrawTop,PointsInfoText[Art],fPoints[Art],Surface);
      end;
    end;
  end;
  Gesamt:=0;

  for Arten:=low(TPunkteBuch) to high(TPunkteBuch) do
    inc(Gesamt,fPoints[Arten]);

  DrawFrame(Surface,bcMaroon,false,DrawTop);
  DrawEntry(DrawTop,LCGesamt,Gesamt,Surface,WhiteBStdFont);
end;

procedure TDXPointStatus.SetFinanz(const Value: TMonthStatus);
begin
  fFinanz := Value;
  fInfoType:=itFinanzen;
  fFormat:=FCredits;
  Redraw;
end;

procedure TDXPointStatus.SetOrgan(const Value: TOrganisationList);
begin
  fOrgan := Value;
  fInfoType:=itOrganisation;
  fFormat:=FCredits;
  Redraw;
end;

procedure TDXPointStatus.SetPoints(const Value: TPunkteStatus);
begin
  fPoints := Value;
  fInfoType:=itPunkte;
  fFormat:=StringConst.FPoints;
  Redraw;
end;

end.
