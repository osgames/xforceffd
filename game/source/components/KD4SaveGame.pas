{********************************************************************************
*										*
* Copyright (C) 2004  X-Force Team - jim_raynor@web.de				*
*										*
* This file is part of X-Force: Fight For Destiny (X-Force).			*
*										*
* X-Force is free software; you can redistribute it and/or modify		*
* it under the terms of the GNU General Public License as published by		*
* the Free Software Foundation; either version 2 of the License, or		*
* (at your option) any later version.						*
*										*
* X-Force is distributed in the hope that it will be useful,			*
* but WITHOUT ANY WARRANTY; without even the implied warranty of		*
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the			*
* GNU General Public License for more details.					*
*										*
* You should have received a copy of the GNU General Public License		*
* along with X-Force; if not, write t o the Free Software			*
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA	*
*										*
*********************************************************************************
*										*
* Bildet die Oberkomponente f�r den Spielstand in X-Force. In ihr werden die    *
* ganzen Listen (LagerListe, AlienListe, RaumschiffListe ...) gehalten 		*
* Zugriff sollte nur noch �ber die savegame_api erfolgen.			*
*										*
********************************************************************************}

{$I ../Settings.inc}

unit KD4SaveGame;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  XForce_types,SoldatenList,imgList, ForschList, LagerListe, OrganisationList,
  WerkstattList, FileCtrl,PersonList,Koding, BasisListe, ArchivFile, Defines,
  TraceFile,RaumschiffList, UFOList, AlienList, EinsatzListe, MissionList,
  NotifyList, StringConst;

const
  // Event, wenn neues Spiel Erstellt wurde
  EVENT_SAVEGAMENEWGAME      : TEventID   = 0;

  // Event, wenn Spiel geladen oder neues Spiel erstellt wurde
  // bei einem neuen Spiel wird StartGame nach NewGame aufgerufen
  EVENT_SAVEGAMESTARTGAME    : TEventID   = 1;

  // Event, wenn Spiel geladen
  // vor EVENT_SAVEGAMESTARTGAME
  EVENT_SAVEGAMELOADGAME     : TEventID   = 2;

  // Event, wenn eine Nachricht eintrifft. �ber GetLastMessage kann im handler
  // auf die Nachricht zugegriffen werden
  EVENT_SAVEGAMEMESSAGE      : TEventID   = 3;

  // Event, wenn eine Woche vorbei ist
  EVENT_SAVEGAMEWEEKEND      : TEventID   = 4;

  // Event, wenn eine Stunde vorbei ist
  EVENT_SAVEGAMEHOUREND      : TEventID   = 5;

  // Event, wenn ein Spielsatz in LoadGameSet geladen wird
  EVENT_SAVEGAMELOADGAMESET  : TEventID   = 6;

type
  TKD4SaveGame = class(TComponent)
  private
    { Variablen die von der SaveGame Komponente }
    { verwendet werden                          }
    fFileName           : String;                                               // Speichert den Dateinamen des SaveGames, wird bei SaveGame verwendet
    fPlayerName         : MapString;                                            // Speichert den Namen des Spielers
    fKapital            : Int64;                                                // Speichert das verf�gbare Kapital
    fSoldatList         : TSoldatenList;                                        // Die SoldatenListe der Soldaten die dem Spieler geh�ren
    fKaufSoldat         : TSoldatenList;                                        // Die Soldaten die auf der TransferListe stehen
    fLagerList          : TLagerListe;
    fForschList         : TForschList;                                          // Die Forschungsliste zur verwaltung der Forschung
    fOrganList          : TOrganisationList;
    fWerkstattList      : TWerkstattList;
    fPunkte             : Integer;                                              // Punkte des Spielers im Monat
    fDate               : TKD4Date;                                             // Struktur f�r das Datum
    fMonth              : TMonthStatus;                                         // Monatsabrechnungs Record
    fMessages           : TMessages;
    fPersonList         : TPersonList;
    fAlienList          : TAlienList;
    fEinsatzListe       : TEinsatzListe;
    fSellSoldat         : Boolean;
    fDifficult          : TGameDifficult;

    { Variablen zum speichern des Basisraums }
    fTagEnd             : boolean;
    fPunkteStatus       : TPunkteStatus;
    fBasisListe         : TBasisListe;
    fMonthBilanz        : boolean;
    fRaumschiffList     : TRaumschiffList;
    fUFOList            : TUFOList;
    fUFOKampf           : TNotifyEvent;
    fMonthEnd           : boolean;
    fGameSet            : TSetIdentifier;
    fOpenGame           : boolean;
    fMissionList        : TMissionList;
    fSaveDirectory      : String;
    fNotifyList         : TNotifyList;

    // Speichert die Nachricht die zuletzt �ber SendMessage eingetroffen ist
    // per GetLastMessage kann darauf zugegriffen werden
    fLastMessage        : TGameMessage;

    fCustomSaveHandlers : Array of record
                            ID              : Cardinal;
                            LoadHandler     : TCustomSaveGameEvent;
                            SaveHandler     : TCustomSaveGameEvent;
                            NewHandler      : TNotifyEvent;
                            WasLoaded       : Boolean;
                          end;

    // Nur verf�gbar bei NewGame
    fNewGameRecData     : TNewGameRec;

    { private Proceduren und Funktionen zum setzen }
    { und lesen von Eigenschaften                  }
    procedure CreateKaufSoldat;                                                 // Erstellt eine neue Liste von Soldaten auf der Transferliste
    procedure ClearMonthStat;

    procedure CalculateAusgabenEinnahmen;
    procedure SetSaveDirectory(const Value: String);

    procedure SaveCustomData(Stream: TStream);
    procedure LoadCustomData(Stream: TStream);

    procedure SaveExtendedGameInfos(Stream: TStream);
    procedure LoadExtendedGameInfos(Stream: TStream);
    function GetNewGameRecord: TNewGameRec;
  protected

    { Gesch�tzte Funktionen die auf Eregnisse }
    { von anderen Komponenten reagieren       }
    procedure FreeMoney        (Sender: TObject;Need: Integer;                  // Prozedur die aufgerufen wird, falls eine Komponente durch Verkauf Geld vergibt
                                BuchType: TKapital);
    procedure LoadGameSet      (GameSet: String; Language: String);
    procedure MakeFights;
    procedure FindSoldat       (Index: Integer;BasisID: Cardinal; WatchType: TWatchType);

  public

    { Konstruktor und Destruktor }
    constructor Create        (AOwner: TComponent);override;                    // Konstruktor f�r die SaveGame Komponente
    destructor  Destroy                           ;override;                    // Destruktor f�r die SaveGame Komponente

    { �ffentliche Prozeduren }
    function    SellSoldat    (Index: Integer): Boolean;                        // Verkauft einen Soldaten
    function    DateString    (short: Boolean = false): String;          // Eigenschaft um auf den Datumsstring zuzugreifen
    procedure   BuySoldat     (Index: integer; BasisID: Cardinal = 0);          // Kauft einen Soldaten
    procedure   CreateSoldat  (Count: Integer);                                 // Erstellt eine Anzahl von Soldaten
    procedure   NewGame       (Data: TNewGameRec);                              // Erstellt ein neues auf Basis der Data-Struktur
    procedure   LoadGame      (FileName: String;Stream: TStream);               // L�dt ein Spiel aus einer Datei
    procedure   StartGame;                                                      // L�st das Ereigniss OnStartGame aus
    procedure   NextRound(Minuten: Integer);                                    // Berechnet die n�chste Runde
    procedure   SaveGame(SaveGameName: String; AutoSave: Boolean = false);      // Speichert das Spiel direkt im Spielstand des Spielers
    procedure   SaveGameToStream(Stream: TStream);                              // Speichert das Spiel in einen Stream
    procedure   BuchungKapital(Kapital: Integer;Art: TKapital);                 // Dient zur Buchung von Kapital
    procedure   BuchungPunkte(Punkte: Integer;Art: TPunkteBuch);                // Dient zur Buchung von Punkten

    procedure   SendMessage(Sender: TObject;Message: String; MType: TLongMessage; Objekt: TObject = nil);
    function    GetLastMessage: TGameMessage;

    procedure   RegisterCustomSaveHandler(SaveHandler: TCustomSaveGameEvent; LoadHandler: TCustomSaveGameEvent; NewHandler: TNotifyEvent; ID: Cardinal);

    // GIbt einen Text f�r eine Abrechnung der aktuellen Woche aus
    function GetSettlementWeek(FormatStr: String): String;

    property    SoldatenListe : TSoldatenList      read fSoldatList;            // Eigenschaft um auf die SoldatenListe zuzugreifen
    property    SoldatenKauf  : TSoldatenList      read fKaufSoldat;            // Eigenschaft um auf die TransferListe f�r die Soldaten zuzugreifen
    property    ForschListe   : TForschList        read fForschList;            // Eigenschaft um auf die ForschugnsListe zuzugreifen
    property    LagerListe    : TLagerListe        read fLagerList;
    property    Organisations : TOrganisationList  read fOrganList;
    property    WerkStatt     : TWerkStattList     read fWerkStattList;

    property    MonthStatus   : TMonthStatus       read fMonth;
    property    PunkteStatus  : TPunkteStatus      read fPunkteStatus;

    property    PersonList    : TPersonList        read fPersonList;
    property    BasisListe    : TBasisListe        read fBasisListe;
    property    Raumschiffe   : TRaumschiffList    read fRaumschiffList;
    property    UFOListe      : TUFOList           read fUFOList;
    property    AlienList     : TAlienList         read fAlienList;
    property    EinsatzList   : TEinsatzListe      read fEinsatzListe;
    property    MissionList   : TMissionList       read fMissionList;

    property    Gameset       : TSetIdentifier     read fGameSet;

    property    NotifyList    : TNotifyList        read fNotifyList;

    // Bietet zugriff auf die Daten, die Newgame �bergeben wurden. Bei Zugriff
    // nach dem NewGame abgearbeitet ist, wird eine Assertation ausgel�st
    property    NewGameData   : TNewGameRec        read GetNewGameRecord;

  published

    { Eigenschaften auf die man Lese- und Schreibzugriff hat }
    property  PlayerName   : MapString read fPlayerName  write fPlayerName;     // Namen des Spielers
    property  FileName     : String    read fFileName    write fFileName;       // Dateiname des SaveGames

    property  Punkte       : Integer   read fPunkte      write fPunkte;         // Punkte in dieser Woche
    property  LongMessages : TMessages read fMessages    write fMessages;
    property  MonthBilanz  : boolean   read fMonthBilanz write fMonthBilanz;    // Monatsbilanz am Monatsende
    property  SaveDirectory: String    read fSaveDirectory write SetSaveDirectory;

    { Eigenschaften auf die man nur Lesezugriff hat }
    property  Kapital      : Int64     read fKapital;                           // Kapital
    property  Date         : TKD4Date  read fDate;
    property  MonthEnd     : boolean   read fMonthEnd;

    property  OpenGame     : boolean   read fOpenGame;

    property  Difficult    : TGameDifficult read fDifficult;

    { Ereignisse die }
    property  OnUFOKampf       : TNotifyEvent    read  fUFOKampf
                                                 write fUFOKampf;
  end;

procedure Register;

implementation

uses KD4Utils, GameFigureManager, savegame_api, Loader, lager_api, basis_api,
  highscore_api, ExtRecord, gameset_api, soldaten_api, math;

var
  RecordExtendInfos : TExtRecordDefinition;
  RecordPunkteInfos : TExtRecordDefinition;

procedure Register;
begin
  RegisterComponents('KD4 Tools',[TKD4SaveGame]);
  RegisterNonActiveX([TKD4SaveGame],axrIncludeDescendants);
end;

procedure CreateRecordExtendInfos;
begin
  RecordPunkteInfos:=TExtRecordDefinition.Create('PointsStatistic');
  RecordPunkteInfos.AddInteger('PointType',0,high(Integer));
  RecordPunkteInfos.AddInteger('Points',low(Integer),high(Integer));

  RecordExtendInfos:=TExtRecordDefinition.Create('ExtendSaveGameInfos');
  with RecordExtendInfos do
  begin
    AddString('OriginalSaveGameName',50);
    AddString('PlayerName',50);
    AddInteger('Punkte',low(Integer),high(Integer));
    AddInteger('ForschTime',low(Integer),high(Integer));
    AddInteger('ProdTime',low(Integer),high(Integer));
    AddBoolean('MonthBilanz');
    AddString('GameSet.FileName',255);
    AddString('GameSet.Name',255);
    AddString('GameSet.Language',255);
    AddCardinal('GameSet.ID',0,high(Cardinal));

    AddInteger('Date.Year',0,high(Integer));
    AddInteger('Date.Month',0,high(Integer));
    AddInteger('Date.Week',0,high(Integer));
    AddInteger('Date.Day',0,high(Integer));
    AddInteger('Date.Hour',0,high(Integer));
    AddInteger('Date.Minute',0,high(Integer));

    AddRecordList('KapitalStats',RecordPunkteInfos);
    AddRecordList('PointStats',RecordPunkteInfos);

    AddInteger('Difficult',Integer(low(TGameDifficult)),Integer(high(TGameDifficult)),Integer(gdNormal));

    // Kapital ist Int64. Bei ExtRecord gibt es daf�r keine Native Unterst�tzung
    AddBinary('Kapital');

    // fMessages ist ein Set, deren L�nge variabel ist
    AddBinary('MessageOptions');
  end;
end;

{ Proceduren und Funktion zu der Komponente KD4SaveGame }

procedure TKD4SaveGame.BuchungKapital(Kapital: Integer; Art: TKapital);
begin
  // neue Credits berechnen
  fKapital:=fKapital+Kapital;

  // Statistik f�hren
  inc(fMonth[Art],Kapital);
end;

procedure TKD4SaveGame.BuchungPunkte(Punkte: Integer; Art: TPunkteBuch);
begin
  inc(fPunkte,Punkte);
  inc(fPunkteStatus[Art],Punkte);
end;

procedure TKD4SaveGame.BuySoldat(Index: Integer; BasisID: Cardinal);
begin
  { Diese Funktion kauft einen Soldaten. Dabei wird auf das Kapital }
  { und den Freien Wohnraum r�cksicht genommen, falls etwas in      }
  { unzureichender Menge zur verf�gung steht wird eine Exception    }
  { ausgel�st.                                                      }
  if (Index<0) or (Index>fKaufSoldat.Count-1) then exit;

  if Kapital<fKaufSoldat.Wert[Index] then
    raise ENotEnoughMoney.Create(ENoMoney);

  basis_api_NeedWohnRaum(BasisID);

  BuchungKapital(-fKaufSoldat.Wert[Index],kbSK);

  fSoldatList.AddSoldat(fKaufSoldat[Index],BasisID);
  fKaufSoldat.DeleteSoldat(Index);
end;

procedure TKD4SaveGame.ClearMonthStat;
begin
  { Monatsbilanz und Budget der L�nder auf 0 setzen }
  FillChar(fMonth,SizeOf(fMonth),#0);
  FillChar(fPunkteStatus,SizeOf(fPunkteStatus),#0);
  fOrganList.ClearBudget;
  fPunkte:=0;
end;

constructor TKD4SaveGame.Create(AOwner: TComponent);
begin
  { Konstruktor von TKD4SaveGame der die Unterkomponenten }
  { erstellt und Ereignisse der Unterkomponenten setzt    }
  inherited;
  fNotifyList:=TNotifyList.Create;

  fTagEnd:=false;
  fOpenGame:=false;
  fSellSoldat:=false;
  randomize;
//  fUFOList:=TUFOList.Create;

  savegame_api_init(Self);

  // Hier werden die Stammdaten zum Spielsatz gespeichert, deshalb muss diese Funktion als erstes registriert werden
  savegame_api_RegisterCustomSaveHandler(SaveExtendedGameInfos,LoadExtendedGameInfos,nil,$10C2B066);

  // Umgestellte Liste auf savegame_api
  fForschList:=TForschList.Create;
  fWerkstattList:=TWerkstattList.Create;

  fBasisListe:=TBasisListe.Create;
  fLagerList:=TLagerListe.Create;

  fPersonList:=TPersonList.Create;
  fAlienlist:=TAlienList.Create;
  fSoldatList:=TSoldatenList.Create;

  fUFOList:=TUFOList.Create;
  // Einsatzliste muss nach AlienListe erstellt werden, da lade Abh�ngigkeiten
  // bestehen
  fEinsatzListe:=TEinsatzListe.Create;
  fOrganList:=TOrganisationList.Create;
  fRaumschiffList:=TRaumschiffList.Create;
  fKaufSoldat:=TSoldatenList.Create;
  fMissionList:=TMissionList.Create;

  // Ereignisse setzen
  fKaufSoldat.ManagePlayers    :=false;
  fKaufSoldat.OnFindPerson     :=FindSoldat;

  // Dies ist notwendig, da init durch TSoldatList.Create aufgerufen wird.
  // Da die fKaufSoldat sp�ter erstellt wird, w�re diese Soldatenliste
  // die ma�gebende in der API
  soldaten_api_init(fSoldatList);

  SaveDirectory:='save\';
end;

procedure TKD4SaveGame.CreateKaufSoldat;
var
  Dummy: Integer;
begin
  { Aktualisiert die Liste von Soldaten auf dem Arbeitsmarkt }
  { dabei werden zuf�llig neue erstellt oder gel�scht        }
  { Zuerst werden alle Soldaten durchgangen und falls eine   }
  { Zufallszahl bis 100 kleiner als 15 ist wird er gel�scht  }
  Dummy:=0;
  while (Dummy<fKaufSoldat.Count) do
  begin
    if random(100)<=15 then
      fKaufSoldat.DeleteSoldat(Dummy)
    else
      inc(Dummy);
  end;
  { Es werden f�nf durchl�ufe get�tigt, bei jedem Durchlauf }
  { wird eine Zufallszahl erstellt. Wenn diese kleiner als  }
  { 15 ist wird ein neuer Soldat erstellt.Es k�nnen max. 20 }
  { Soldaten auf dem Arbeitsmarkt sein                      }
  for Dummy:=1 to 5 do
  begin
    if not (fKaufSoldat.Count=20) then
    begin
      if Random(100)<=30 then
        fKaufSoldat.CreateSoldaten(1);
    end;
  end;
end;

procedure TKD4SaveGame.CreateSoldat(Count: Integer);
var
  Dummy: Integer;
begin
  { Erstellt Count Soldaten. Die Funktion achtet nicht auf }
  { das Kapital, allerdings wird WohnRaum belegt           }
  if basis_api_FreierWohnRaum<Count then
  begin
    raise ENotEnoughRoom.Create(ENoRoom);
    exit;
  end;
  fSoldatList.CreateSoldaten(Count);
  for Dummy:=1 to Count do
    basis_api_NeedWohnRaum;
end;

destructor TKD4SaveGame.Destroy;
begin
  { Gibt die Unterkomponenten frei }
  fMissionList.Free;

  fAlienList.Free;
  fSoldatList.Free;
  fKaufSoldat.Free;
  fForschList.Free;
  fLagerList.Free;
  fWerkStattList.Free;
  fRaumschiffList.Free;
  fOrganList.Free;
  fPersonList.Free;
  fBasisListe.Free;
  fUFOList.Free;
  fEinsatzListe.Free;
  fNotifyList.Free;
  inherited;
end;

procedure TKD4SaveGame.FreeMoney(Sender: TObject; Need: Integer;BuchType: TKapital);
begin
  { Prozedure die aufgerufen wird falls eine Unterkomponente }
  { z.B. durch Verkaufen bekommt                             }
  BuchungKapital(Need,BuchType);
end;

procedure TKD4SaveGame.LoadGame(FileName: String; Stream: TStream);
var
  SaveGameID : Cardinal;
begin
  { L�dt ein Spielstand aus einer Datei }
  fFileName:=FileName;

  Stream.Position:=0;
  Stream.Read(SaveGameID,SizeOf(SaveGameID));

  if not IsValidSaveGameVersion(SaveGameID) then
  begin
    { Fehler falls das SaveGame ung�ltig ist }
    raise Exception.Create(EInvalidSaveGame);
  end;

  { Ruft die Prozeduren der Unterkomponenten zum Laden }
  { aus dem Stream auf                                 }
  LoadCustomData(Stream);

  fNotifyList.CallEvents(EVENT_SAVEGAMELOADGAME,Self);

  StartGame;
end;

procedure TKD4SaveGame.MakeFights;
begin
  if Assigned(fUFOKampf) then fUFOKampf(Self);
end;

procedure TKD4SaveGame.NewGame(Data: TNewGameRec);
begin
  fNewGameRecData:=Data;

  { Alle Werte l�schen und Startwerte setzen }
  fKapital:=Data.Kapital;
  fFileName:=Data.FileName;
  fPlayerName:=Data.Name;
  fDate:=StartDate;

  fForschList.ProjektTime:=Data.Forsch;
  fWerkstattList.ProjektTime:=Data.Prod;

  fDifficult:=Data.Difficult;

  MonthBilanz:=true;
  fPunkte:=0;
  ClearMonthStat;

  //Alle Nachrichten werden angezeigt
  LongMessages:=AllMessages;

  // Spielsatz laden
  fGameSet:=Data.GameFile;
  fGameSet.Language:=Data.Language;

  LoadGameSet(Data.GameFile.FileName,Data.Language);

  // Hauptbasis erstellen
  fBasisListe.NewGame(Data.Basis);

  fNotifyList.CallEvents(EVENT_SAVEGAMENEWGAME,nil);

  savegame_api_FreeAlphatron(Data.StartAlphatron,fBasisListe.HauptBasis.ID);

  CreateKaufSoldat;
  CreateSoldat(Data.Soldat);

  StartGame;

  // Kennzeichen, dass NewGame fertig ist
  fNewGameRecData.Name:='';
end;

procedure TKD4SaveGame.NextRound(Minuten: Integer);
var
  OldDate,NewDate   : TKD4Date;
begin
  fTagEnd:=true;
  { Berechnet die n�chste Runde }

  { Eintrag in die HighScoreListe Vornehmen }
  OldDate:=fDate;

  {$IFDEF AUTOSAVEONWEEKEND}
  CalculateTime(OldDate,Minuten);
  if OldDate.Week<>fDate.Week then
  begin
    SaveGame(Format('%d. Weekend',[fDate.Week]),true);
  end;
  OldDate:=fDate;
  {$ENDIF}

  { Datum der n�chsten Runde berechnen }
  if CalculateTime(fDate,Minuten) then
  begin
    { Beim Wochenende }
    if fDate.Week<>OldDate.Week then
    begin
      { W�chentlich Kosten berechnen }
      CalculateAusgabenEinnahmen;
      { W�chentlich Unterst�tzung der L�nder }
      fOrganList.NextWeek(fPunkte);

      // Highscore eintragen
      highscore_api_AddEntry(fPlayerName,OldDate.Week,OldDate.Year,Punkte);

      { Monatsbilanz f�r alten Monat aufrufen }
      fMonthEnd:=true;
      NewDate:=fDate;
      fDate:=OldDate;

      // Events ausl�sen f�r WochenEnde
      fNotifyList.CallEvents(EVENT_SAVEGAMEWEEKEND,nil);

      fDate:=NewDate;
      fMonthEnd:=false;

      { Monatsstatus l�schen }
      ClearMonthStat;

    end;
    { Falls ein Tag zu Ende ist Soldaten verbessern }
    fSoldatList.NextDay;
    fForschList.NextDay;
    fWerkstattList.NextDay;
    fBasisListe.NextDay;
    fRaumschiffList.NextDay;
    fUFOList.NextDay;
    fAlienList.NextDay;

    { SoldatenkaufListe erstellen }
    CreateKaufSoldat;
  end;
  { Stunde beendet }
  if OldDate.Hour<>fDate.Hour then
    fNotifyList.CallEvents(EVENT_SAVEGAMEHOUREND,Self);

  fBasisListe.NextRound(Minuten);

  { N�chste Runde f�r Listen aufrufen }
  fRaumschiffList.NachLaden;
  fRaumschiffList.NextRound(Minuten);
  fUFOList.NextRound(Minuten);
  fSoldatList.NextRound(Minuten);

  MakeFights;

  fRaumschiffList.NachLaden;
  fWerkstattList.NextRound(Minuten);
  fForschList.NextRound(Minuten);
  fEinsatzListe.NextRound(Minuten);
  fMissionList.NextRound(Minuten);

  { Angebote erstellen }
  fLagerList.CreateAngebote(Minuten);

  { Datums�nderung ausf�hren }
  fTagEnd:=false;
end;

procedure TKD4SaveGame.SaveGame(SaveGameName: String; AutoSave: Boolean);
var
  Stream  : TMemoryStream;
  Archiv  : TArchivFile;
  Game    : TGameStatus;
begin
  { Speichert den Aktuellen Spielstand }
  Stream:=TMemoryStream.Create;
  if not AutoSave then
    FileName:=SaveGameName;

  SaveGameToStream(Stream);
  Archiv:=TArchivFile.Create;

  try
    if FileExists(fSaveDirectory+Playername+'.sav') then
      Archiv.OpenArchiv(fSaveDirectory+Playername+'.sav')
    else
      Archiv.CreateArchiv(fSaveDirectory+Playername+'.sav');

    Archiv.ReplaceRessource(Stream,SaveGameName,true);

    Game.Name:=SaveGameName;
    Game.Date:=Date;
    Game.Kapital:=Kapital;
    Game.Version:=SaveVersion;

    savegame_api_RegisterSaveGame(Game,Archiv);
    if not AutoSave then
      SaveLastSavedGame(PlayerName,SaveGameName);
  finally
    Stream.Free;
    Archiv.Free;
  end;

end;

function TKD4SaveGame.SellSoldat(Index: Integer): boolean;
var
  RItem  : TEnumRuckSackItemRecord;
  GItem  : TBackPackItem;
  Dummy  : Integer;

  procedure FreeItem(ID: Cardinal);
  begin
    lager_api_PutItem(fSoldatList[Index].BasisID,ID);
  end;

  procedure FreeWaffe(const Waffe: TSoldatWaffe);
  begin
    if Waffe.Gesetzt then
    begin
      if Waffe.MunGesetzt then
        FreeItem(Waffe.MunID);
      FreeItem(Waffe.ID);
    end;
  end;

begin
  result:=false;
  if (Index<0) or (Index>fSoldatList.Count-1) then
    exit;

  if fSoldatList.IsSoldatUnterwegs(Index) then
    exit;

  // Ausr�stung des Soldaten in die entsprechende Basis verfrachten
  with fSoldatList.Managers[Index] do
  begin
    FreeWaffe(LeftHandWaffe);
    FreeWaffe(RightHandWaffe);

    for Dummy:=0 to GuertelSlots-1 do
    begin
      if GetGuertelItem(Dummy,GItem) then
        FreeItem(GItem.ID);
    end;

    if Panzerung.Gesetzt then
      FreeItem(Panzerung.ID);

    if Guertel.Gesetzt then
      FreeItem(Guertel.ID);

    RItem.Index:=0;
    repeat
      EnumRucksackItems(RItem);
      FreeItem(RItem.Item.ID);
    until RItem.Item.ID=0;

  end;

  fSellSoldat:=true;
  fKaufSoldat.AddSoldat(fSoldatList[Index]);
  fSellSoldat:=false;

  fSoldatList.DeleteSoldat(Index);

  basis_api_FreeWohnRaum;

  BuchungKapital(round(fSoldatList.Wert[Index]*0.66),kbSVK);

  result:=true;
end;

procedure TKD4SaveGame.SendMessage(Sender: TObject;Message: String; MType: TLongMessage; Objekt: TObject);
begin
  fLastMessage.Text:=Message;
  fLastMessage.MType:=MType;
  fLastMessage.Objekt:=Objekt;

  fNotifyList.CallEvents(EVENT_SAVEGAMEMESSAGE,nil);
end;

procedure TKD4SaveGame.StartGame;
begin
  fOpenGame:=true;

  fNotifyList.CallEvents(EVENT_SAVEGAMESTARTGAME,nil);
end;

procedure TKD4SaveGame.CalculateAusgabenEinnahmen;
begin
  // Soldatengehalt
  BuchungKapital(-fSoldatList.CalculateWeeklySold,kbSG);

  // Forschergehalt
  BuchungKapital(-fForschList.CalculateWeeklySold,kbFG);

  // Technikergehalt
  BuchungKapital(-fWerkstattList.CalculateWeeklySold,kbHG);

  fBasisListe.NextWeek;
  fRaumschiffList.NextWeek;
end;

procedure TKD4SaveGame.LoadGameSet(GameSet: String; Language: String);
var
  Bitmap: TBitmap;
  Archiv: TArchivFile;
begin
  Archiv:=TArchivFile.Create;
  Archiv.OpenArchiv(GameSet,true);

  savegame_api_SetGameSetLanguage(Language);
  Loader.ReadingLanguage:=Language;

  savegame_api_SetGameSet(Archiv);

  // Symbole laden
  Bitmap:=TBitmap.Create;
  Bitmap.Width:=0;

  if Archiv.ExistRessource(RNWaffenSymbols) then
  begin
    Archiv.OpenRessource(RNWaffenSymbols);
    Bitmap.LoadFromStream(Archiv.Stream);
  end;

  fLagerList.SetLagerBitmap(Bitmap,0,false);
  Bitmap.Free;

  fNotifyList.CallEvents(EVENT_SAVEGAMELOADGAMESET,Self);

  Archiv.Free;
end;

procedure TKD4SaveGame.FindSoldat(Index: Integer;BasisID: Cardinal; WatchType: TWatchType);
var
  DefMessage: String;
begin
  if fSellSoldat then exit;
  if fBasisListe.Count>1 then
    DefMessage:=Format(ST0309260011,[fKaufSoldat[Index].Name,fBasisListe.GetBasisFromID(BasisID).Name])
  else
    DefMessage:=Format(ST0309260012,[fKaufSoldat[Index].Name]);

  if WatchType=wtMessage then
  begin
    SendMessage(Self,DefMessage,lmFindPerson);
  end
  else if WatchType=wtHire then
  begin
    try
      BuySoldat(Index,BasisID);
      SendMessage(Self,DefMessage+#10+ST0309260013,lmFindPerson);
    except
      on E: ENotEnoughMoney do
        SendMessage(Self,DefMessage+#10+ST0309260014,lmReserved);
      on E: ENotEnoughRoom do
        SendMessage(Self,DefMessage+#10+ST0309260015,lmReserved);
    end;
  end;
end;

function TKD4SaveGame.DateString(short: Boolean): String;
var
  WeekDay: Integer;
  SysDate: TDateTime;
  Date   : TTimeStamp;
begin
  { Gibt das Formatierte Datum des Spielstandes aus im Format 'TT.MM.JJ um HH.00' }
  if short then
  begin
    result:=Format(ST0309260016,[fDate.Day,fDate.Month,fDate.Year,fDate.Hour,fDate.Minute]);
  end
  else
  begin
    SysDate:=EncodeDate(fDate.Year,fDate.Month,fDate.Day)+0.5;
    WeekDay:=DayOfWeek(SysDate);
    Date:=DateTimeToTimeStamp(SysDate);
    result:=Format(FLongDate,[ADays[WeekDay],fDate.Day,fDate.Month,fDate.Year,fDate.Hour,fDate.Minute]);
  end;
end;

procedure TKD4SaveGame.SetSaveDirectory(const Value: String);
begin
  fSaveDirectory := IncludeTrailingBackSlash(Value);
end;

procedure TKD4SaveGame.SaveGameToStream(Stream: TStream);
{$IFDEF DEBUGMODE}
var
  Time: Cardinal;
{$ENDIF}
begin
  {$IFDEF DEBUGMODE}
  Time:=GetTickCount;
  GlobalFile.WriteLine;
  GlobalFile.Write('Spielstand wird gespeichert');
    {$IFDEF SHOWMEMORY}
    GlobalFile.Write('Speicherverbrauch',GetProcessMemory);
    {$ENDIF}
  {$ENDIF}
  Stream.Write(SaveVersion,SizeOf(SaveVersion));

  SaveCustomData(Stream);
  {$IFDEF DEBUGMODE}
  GlobalFile.Write('Speichern abgeschlossen (msek)',GetTickCount-Time);
    {$IFDEF SHOWMEMORY}
    GlobalFile.Write('Speicherverbrauch',GetProcessMemory);
    {$ENDIF}
  GlobalFile.WriteLine;
  {$ENDIF}
end;

procedure TKD4SaveGame.RegisterCustomSaveHandler(SaveHandler,
  LoadHandler: TCustomSaveGameEvent; NewHandler: TNotifyEvent; ID: Cardinal);
var
  Dummy: Integer;
  Index: Integer;
begin
  for Dummy:=0 to high(fCustomSaveHandlers) do
    Assert(fCustomSaveHandlers[Dummy].ID<>ID,'Benutzerdaten speicher/lade Funktion mit ID '+IntToHex(ID,8)+' bereits vergeben');

  Assert(Assigned(LoadHandler),'LoadHandler muss angegeben werden');
  Assert(Assigned(SaveHandler),'SaveHandler muss angegeben werden');

  SetLength(fCustomSaveHandlers,length(fCustomSaveHandlers)+1);

  Index:=high(fCustomSaveHandlers);

  fCustomSaveHandlers[Index].ID:=ID;
  fCustomSaveHandlers[Index].LoadHandler:=LoadHandler;
  fCustomSaveHandlers[Index].SaveHandler:=SaveHandler;
  fCustomSaveHandlers[Index].NewHandler:=NewHandler;
  fCustomSaveHandlers[Index].WasLoaded:=false;
end;

procedure TKD4SaveGame.LoadCustomData(Stream: TStream);
var
  Buffer    : Cardinal;
  Count     : Integer;
  Dummy     : Integer;
  Len       : Integer;
  ID        : Cardinal;
  MemStream : TMemoryStream;

  procedure ReadWithCustomHandler(ID: Cardinal; Stream: TStream);
  var
    Dummy: Integer;
  begin
    Stream.Position:=0;
    for Dummy:=0 to high(fCustomSaveHandlers) do
    begin
      if fCustomSaveHandlers[Dummy].ID=ID then
      begin
        fCustomSaveHandlers[Dummy].LoadHandler(Stream);
        // Dadurch wird verhindert, dass NewHandler am Ende aufgerufen wird
        fCustomSaveHandlers[Dummy].WasLoaded:=true;
        exit;
      end;
    end;
  end;

begin
  // Pr�fen, ob Benutzerdaten im neuen Format kommen
  Stream.Read(Buffer,SizeOf(Buffer));

  if Buffer<>CustomDataVersion then
    exit;

  Stream.Read(Count,SizeOf(Count));

  if Count=0 then
    exit;

  // Alle Buckets auf nicht gelesen setzen
  for Dummy:=0 to high(fCustomSaveHandlers) do
  begin
    fCustomSaveHandlers[Dummy].WasLoaded:=false;
  end;

  MemStream:=TMemoryStream.Create;
  for Dummy:=0 to Count-1 do
  begin
    MemStream.Clear;
    Stream.Read(ID,SizeOf(ID));
    Stream.Read(Len,SizeOf(Len));
    MemStream.CopyFrom(Stream,Len);

    ReadWithCustomHandler(ID,MemStream);
  end;
  MemStream.Free;

  // Bei nicht vorhandenen Buckets wird NewHandler aufgerufen
  for Dummy:=0 to high(fCustomSaveHandlers) do
  begin
    if not fCustomSaveHandlers[Dummy].WasLoaded then
    begin
      if Assigned(fCustomSaveHandlers[Dummy].NewHandler) then
        fCustomSaveHandlers[Dummy].NewHandler(Self);
    end;
  end;

end;

procedure TKD4SaveGame.SaveCustomData(Stream: TStream);
var
  Buffer    : Integer;
  Dummy     : Integer;
  MemStream : TMemoryStream;
begin
  // Kennzeichen speichern, dass jetzt die Benutzerdaten kommen
  Stream.Write(CustomDataVersion,SizeOf(CustomDataVersion));

  Buffer:=length(fCustomSaveHandlers);
  Stream.Write(Buffer,sizeof(Buffer));

  if length(fCustomSaveHandlers)=0 then
    exit;

  MemStream:=TMemoryStream.Create;
  for Dummy:=0 to high(fCustomSaveHandlers) do
  begin
    MemStream.Clear;

    fCustomSaveHandlers[Dummy].SaveHandler(MemStream);

    {$IFDEF DEBUGMODE}
    GlobalFile.Write('Speicherverbrauch f�r '+IntToHex(fCustomSaveHandlers[Dummy].ID,8),MemStream.Size);
    {$ENDIF}

    // ID zum Identifizieren abspeichern
    Stream.Write(fCustomSaveHandlers[Dummy].ID,SizeOf(Cardinal));

    // Gr��e des Datenpaketes speichern, um sp�ter �berlesen zu k�nnen, falls
    // Handler entfallen ist
    Buffer:=MemStream.Size;
    Stream.Write(Buffer,sizeOf(Buffer));

    // Daten abspeichern
    MemStream.Position:=0;
    Stream.CopyFrom(MemStream,0);

  end;
  MemStream.Free;
end;

function TKD4SaveGame.GetLastMessage: TGameMessage;
begin
  result:=fLastMessage;
end;

procedure TKD4SaveGame.LoadExtendedGameInfos(Stream: TStream);
var
  Rec        : TExtRecord;
  Test       : TSetIdentifier;
  Dummy      : Integer;
begin
  if RecordExtendInfos=nil then
    CreateRecordExtendInfos;

  Rec:=TExtRecord.Create(RecordExtendInfos);
  Rec.LoadFromStream(Stream);

  FileName:=Rec.GetString('OriginalSaveGameName');

  FillChar(fMessages,sizeof(fMessages),#0);
  CopyMemory(Addr(fMessages),Rec.GetBinary('MessageOptions'),min(Rec.GetBinaryLength('MessageOptions'),SizeOf(fMessages)));

  Include(fMessages,lmReserved);

  // Kapital ist Int64. Bei ExtRecord gibt es daf�r keine Native Unterst�tzung
  CopyMemory(Addr(fKapital),Rec.GetBinary('Kapital'),sizeOf(fKapital));

  PlayerName:=Rec.GetString('PlayerName');

  fPunkte:=Rec.GetInteger('Punkte');
  fForschList.ProjektTime:=Rec.GetInteger('ForschTime');
  fWerkstattList.ProjektTime:=Rec.GetInteger('ProdTime');
  fMonthBilanz:=Rec.GetBoolean('MonthBilanz');

  // Spielsatz ermitteln
  fGameSet.FileName:=Rec.GetString('GameSet.FileName');
  fGameSet.Name:=Rec.GetString('GameSet.Name');
  fGameSet.Language:=Rec.GetString('GameSet.Language');
  fGameSet.ID:=Rec.GetCardinal('GameSet.ID');

  fDifficult:=TGameDifficult(Rec.GetInteger('Difficult'));

  savegame_api_SetGameSetLanguage(fGameSet.Language);

  try
    Test:=gameset_api_GetIdentOfSet(fGameSet.FileName);
    if Test.ID<>fGameSet.ID then
    begin
      raise EInvalidVersion.Create('');
    end;
  except
    on E: EFOpenError do
    begin
      raise Exception.Create(Format(EGameSetNotFound,[fGameSet.Name,fGameSet.FileName]));
    end;
    on E: EInvalidVersion do
    begin
      raise Exception.Create(Format(EWrongGameSet,[fGameSet.Name]));
    end;
    on E: Exception do
    begin
      raise;
    end;
  end;

  LoadGameSet(fGameSet.FileName,fGameSet.Language);

  // Datum laden
  fDate.Year:=Rec.GetInteger('Date.Year');
  fDate.Month:=Rec.GetInteger('Date.Month');
  fDate.Week:=Rec.GetInteger('Date.Week');
  fDate.Day:=Rec.GetInteger('Date.Day');
  fDate.Hour:=Rec.GetInteger('Date.Hour');
  fDate.Minute:=Rec.GetInteger('Date.Minute');

  FillChar(fMonth,sizeof(fMonth),#0);
  with Rec.GetRecordList('KapitalStats') do
  begin
    for Dummy:=0 to Count-1 do
    begin
      with Item[Dummy] do
        fMonth[TKapital(GetInteger('PointType'))]:=GetInteger('Points');
    end;
  end;

  with Rec.GetRecordList('PointStats') do
  begin
    for Dummy:=0 to Count-1 do
    begin
      with Item[Dummy] do
        fPunkteStatus[TPunkteBuch(GetInteger('PointType'))]:=GetInteger('Points');
    end;
  end;

  Rec.Free;
end;

procedure TKD4SaveGame.SaveExtendedGameInfos(Stream: TStream);
var
  Rec     : TExtRecord;
  Dummy   : TPunkteBuch;
  DummyKap: TKapital;
begin
  if RecordExtendInfos=nil then
    CreateRecordExtendInfos;

  Rec:=TExtRecord.Create(RecordExtendInfos);
  Rec.SetString('OriginalSaveGameName',FileName);

  Rec.SetString('PlayerName',PlayerName);

  Rec.SetInteger('Punkte',fPunkte);
  Rec.SetInteger('ForschTime',fForschList.ProjektTime);
  Rec.SetInteger('ProdTime',fWerkstattList.ProjektTime);
  Rec.SetBoolean('MonthBilanz',fMonthBilanz);
  Rec.SetString('GameSet.FileName',fGameSet.FileName);
  Rec.SetString('GameSet.Name',fGameSet.Name);
  Rec.SetString('GameSet.Language',fGameSet.Language);
  Rec.SetCardinal('GameSet.ID',fGameSet.ID);

  Rec.SetInteger('Date.Year',fDate.Year);
  Rec.SetInteger('Date.Month',fDate.Month);
  Rec.SetInteger('Date.Week',fDate.Week);
  Rec.SetInteger('Date.Day',fDate.Day);
  Rec.SetInteger('Date.Hour',fDate.Hour);
  Rec.SetInteger('Date.Minute',fDate.Minute);

  Rec.SetInteger('Difficult',Integer(fDifficult));

  // Kapital ist Int64. Bei ExtRecord gibt es daf�r keine Native Unterst�tzung
  Rec.SetBinary('Kapital',SizeOf(fKapital),Addr(fKapital));

  // fMessages ist ein Set, deren L�nge variabel ist
  Exclude(fMessages,lmReserved);
  Rec.SetBinary('MessageOptions',SizeOf(fMessages),Addr(fMessages));

  with Rec.GetRecordList('KapitalStats') do
  begin
    for DummyKap:=low(TKapital) to high(TKapital) do
    begin
      with Add do
      begin
        SetInteger('PointType',Integer(DummyKap));
        SetInteger('Points',fMonth[DummyKap]);
      end;
    end;
  end;

  with Rec.GetRecordList('PointStats') do
  begin
    for Dummy:=low(TPunkteBuch) to high(TPunkteBuch) do
    begin
      with Add do
      begin
        SetInteger('PointType',Integer(Dummy));
        SetInteger('Points',fPunkteStatus[Dummy]);
      end;
    end;
  end;

  Rec.SaveToStream(Stream);
  Rec.Free;
end;

function TKD4SaveGame.GetNewGameRecord: TNewGameRec;
begin
  Assert(fNewGameRecData.Name<>'');
  result:=fNewGameRecData;
end;

function TKD4SaveGame.GetSettlementWeek(FormatStr: String): String;
begin
  // Abrechnung f�r die Letzte Woche im vergangen Jahr?
  if (fDate.Month=1) and (fDate.Week>50) then
    result:=Format(FormatStr,[fDate.Week,fDate.Year-1])
  else
    result:=Format(FormatStr,[fDate.Week,fDate.Year]);
end;

initialization

finalization

  if RecordExtendInfos<>nil then
  begin
    RecordExtendInfos.Free;
    RecordPunkteInfos.Free;
  end;

end.
