unit DialogTransferAlphatron;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  DXContainer, ModalDialog, DXGroupCaption, Blending, XForce_types, DXLabel,
  DXBitmapButton, DXListBox, DXCheckBox, DXSpinEdit, BasisListe, StringConst;

type
  TDialogTransferAlphatron = class(TModalDialog)
  private
    GroupHeader       : TDXGroupCaption;
    CountLabel        : TDXGroupCaption;
    CountSpinBox      : TDXSpinEdit;
    AutoTransferBox   : TDXCheckBox;
    BasisLabel        : TDXGroupCaption;
    BasisListBox      : TDXListBox;
    OKButton          : TDXBitmapButton;
    CancelButton      : TDXBitmapButton;
    procedure ButtonClick(Sender: TObject);
    procedure ChangeCountSpinBox(Sender: TObject);

    procedure NeedAlphatronText(Value: Integer;var Text: String);
    function GetAlphatronCount: Integer;
    function GetAutoTransfer: Boolean;
    function GetTargetBase: TBasis;
  public
    procedure BeforeShow;override;

    function ShowModal: TModalResult;override;

    constructor Create(Page: TDXPage);override;

    property TargetBase       : TBasis read GetTargetBase;
    property AutoTransfer     : Boolean read GetAutoTransfer;
    property AlphatronCount   : Integer read GetAlphatronCount;
  end;

implementation

uses
  basis_api, menus, math;

{ TDialogTransferAlphatron }

procedure TDialogTransferAlphatron.BeforeShow;
begin
  inherited;

  GroupHeader.Font.Style:=[fsBold];
  GroupHeader.Font.Size:=Font.Size+4;
  GroupHeader.FontColor:=clYellow;
  GroupHeader.Caption:=StripHotkey(ST0412200001);

end;

procedure TDialogTransferAlphatron.ButtonClick(Sender: TObject);
begin
  ReadyShow;
  if (Sender as TDXBitmapButton).Tag=0 then
  begin
    fResult:=mrOK;
  end
  else
    fResult:=mrCancel;
end;

procedure TDialogTransferAlphatron.ChangeCountSpinBox(Sender: TObject);
begin
  if CountSpinBox.Value=0 then
    OKButton.Text:=BOK
  else
    OKButton.Text:=ST0412230001;
end;

constructor TDialogTransferAlphatron.Create(Page: TDXPage);
begin
  inherited;

  { �berschrift }
  GroupHeader:=TDXGroupCaption.Create(Page);
  with GroupHeader do
  begin
    Visible:=false;
    BorderColor:=$00400000;
    BlendColor:=bcDarkNavy;
    RoundCorners:=rcTop;
    AlphaValue:=200;
    Width:=323;
    Height:=25;
    Alignment:=taCenter;
  end;
  AddComponent(GroupHeader);

  { Beschrifter Anzahl }
  CountLabel:=TDXGroupCaption.Create(Page);
  with CountLabel do
  begin
    Visible:=false;
    BorderColor:=$00400000;
    BlendColor:=bcDarkNavy;
    RoundCorners:=rcNone;
    AlphaValue:=200;
    Width:=GroupHeader.Width-151;
    Height:=20;
    Top:=GroupHeader.Bottom+1;
    Alignment:=taLeftJustify;
    Caption:=LCount;
  end;
  AddComponent(CountLabel);

  { Anzahl der zu transferierenden Ausr�stung }
  CountSpinBox:=TDXSpinEdit.Create(Page);
  with CountSpinBox do
  begin
    Width:=150;
    Height:=20;
    Left:=CountLabel.Right+1;
    Top:=CountLabel.Top;
    Min:=0;
    Step:=5;
    SmallStepButton:=true;
    SmallStep:=1;

    FormatString:='';
    BlendColor:=bcDarkNavy;
    BorderColor:=$00400000;
    RoundCorners:=rcNone;
    FocusColor:=clNavy;
    TopMargin:=3;
    BlendValue:=175;
    OnIncrement:=ChangeCountSpinBox;
    OnDecrement:=ChangeCountSpinBox;

    GetValueText:=NeedAlphatronText;
  end;
  AddComponent(CountSpinBox);

  { Aktivieren/deaktivieren Checkbox }
  AutoTransferBox:=TDXCheckbox.Create(Page);
  with AutoTransferBox do
  begin
    Width:=GroupHeader.Width;
    Height:=23;
    Top:=CountSpinBox.Bottom+1;
    BorderColor:=$00400000;
    BlendColor:=bcDarkNavy;
    Caption:=ST0412200007;
    Hint:=ST0412200008;
    RoundCorners:=rcNone;
    LeftMargin:=6;
//    OnChange:=AktivChange;
  end;
  AddComponent(AutoTransferBox);

  { Beschrifter Basis }
  BasisLabel:=TDXGroupCaption.Create(Page);
  with BasisLabel do
  begin
    Visible:=false;
    BorderColor:=$00400000;
    BlendColor:=bcDarkNavy;
    RoundCorners:=rcNone;
    AlphaValue:=200;
    Width:=AutoTransferBox.Width;
    Height:=20;
    Top:=AutoTransferBox.Bottom+1;
    Alignment:=taLeftJustify;
    Caption:=ST0412200003;
  end;
  AddComponent(BasisLabel);

  { Listbox zum Ausw�hlen }
  BasisListBox:=TDXListBox.Create(Page);
  with BasisListBox do
  begin
    ItemHeight:=17;
    Visible:=false;
    RoundCorners:=rcNone;
    Width:=GroupHeader.Width;
    Top:=BasisLabel.Bottom+1;
//    OnDblClick:=ListDblClick;
//    OnKeyPress:=PressKey;
    FirstColor:=bcDarkNavy;
    AlphaValue:=200;
  end;
  AddComponent(BasisListBox);

  { Einstellungen f�r den OK Button }
  OKButton:=TDXBitmapButton.Create(Page);
  with OKButton do
  begin
    Visible:=false;
    OnClick:=ButtonClick;
    Text:=BOK;
    Width:=GroupHeader.Width div 2;
    Top:=GroupHeader.Bottom+1;
    Font:=Font;
    RoundCorners:=rcLeftBottom;
    Height:=28;
    BlendAlpha:=150;
    BlendColor:=bcDarkNavy;
    FirstColor:=$00400000;
    SecondColor:=clBlue;
    AccerlateColor:=clYellow;
    Tag:=0;
  end;
  AddComponent(OKButton);

  { Einstellungen f�r den Abbrechen Button }
  CancelButton:=TDXBitmapButton.Create(Page);
  with CancelButton do
  begin
    Visible:=false;
    Text:=BCancel;
    Top:=GroupHeader.Bottom+1;
    Width:=GroupHeader.Width div 2;
    Left:=OKButton.Right+1;
    Font:=Font;
    Height:=28;
    RoundCorners:=rcRightBottom;
    OnClick:=ButtonClick;
    BlendAlpha:=150;
    FirstColor:=$00400000;
    BlendColor:=bcDarkNavy;
    SecondColor:=clBlue;
    AccerlateColor:=clYellow;
    Tag:=1;
    EscButton:=true;
  end;
  AddComponent(CancelButton);
end;

function TDialogTransferAlphatron.GetAlphatronCount: Integer;
begin
  result:=CountSpinBox.Value;
end;

function TDialogTransferAlphatron.GetAutoTransfer: Boolean;
begin
  result:=AutoTransferBox.Checked;
end;

function TDialogTransferAlphatron.GetTargetBase: TBasis;
begin
  if BasisListBox.ItemIndex<>-1 then
    result:=TBasis(BasisListBox.Items.Objects[BasisListBox.itemIndex])
  else
    result:=nil;
end;

procedure TDialogTransferAlphatron.NeedAlphatronText(Value: Integer;
  var Text: String);
begin
  if Value=0 then
    Text:='kein Transfer'
  else
    Text:=IntToStr(Value);
end;

function TDialogTransferAlphatron.ShowModal: TModalResult;
var
  Dummy: Integer;
  Index: Integer;
begin
  BasisListBox.Items.Clear;
  for Dummy:=0 to basis_api_GetBasisCount-1 do
  begin
    if basis_api_GetBasisFromIndex(Dummy)<>basis_api_GetSelectedBasis then
    begin
      Index:=BasisListBox.Items.AddObject(basis_api_GetBasisFromIndex(Dummy).Name,basis_api_GetBasisFromIndex(Dummy));
      if basis_api_GetBasisFromIndex(Dummy)=basis_api_GetSelectedBasis.AutoTransferBase then
        BasisListBox.ItemIndex:=Index;
    end;
  end;

  BasisListBox.Height:=BasisListBox.Items.Count*BasisListBox.ItemHeight;

  CountSpinBox.max:=basis_api_GetSelectedBasis.Alphatron;
  CountSpinBox.Step:=max(25,trunc((basis_api_GetSelectedBasis.Alphatron div 20)/25)*25);
  CountSpinBox.Value:=0;

  ChangeCountSpinBox(Self);

  OkButton.Top:=BasisListBox.Bottom+1;
  CancelButton.Top:=BasisListBox.Bottom+1;

  AutoTransferBox.Checked:=basis_api_GetSelectedBasis.AutoTransferBase<>nil;

  result:=inherited ShowModal;
end;

end.
