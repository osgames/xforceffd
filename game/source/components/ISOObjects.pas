{********************************************************************************
*										*
* Copyright (C) 2004  X-Force Team - jim_raynor@web.de				*
*										*
* This file is part of X-Force: Fight For Destiny (X-Force).			*
*										*
* X-Force is free software; you can redistribute it and/or modify		*
* it under the terms of the GNU General Public License as published by		*
* the Free Software Foundation; either version 2 of the License, or		*
* (at your option) any later version.						*
*										*
* X-Force is distributed in the hope that it will be useful,			*
* but WITHOUT ANY WARRANTY; without even the implied warranty of		*
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the			*
* GNU General Public License for more details.					*
*										*
* You should have received a copy of the GNU General Public License		*
* along with X-Force; if not, write to the Free Software			*
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA	*
*										*
*********************************************************************************
*										*
* Hier werden verschiedene Objekte im Bodeneinsatz definiert			*
*  										*
*  TDeathObject       	- Leiche von Aliens/Soldaten				*
*  TBeamObject		- Beamanimation						*
*  TGameFigureZiel	- Darstellung des Zieles eines Soldaten			*
*  TISOItem		- Ausr�stung die im Bodeneinsatz fallengelassen/geworfen*
* 			  wurde							*
*  TSensorItem		- Verwaltung eines Sensors der benutzt wurde (Anzeige   *
*                         des Sensors auf dem Boden erfolgt �ber ein verkn�ftes *
*                         TISOItem)						*
*  TMineItem		- Verwaltung einer abgelegten Mine. L�st die Explosion  *
*                         aus, sobald eine Einheit in die N�he kommt (Anzeige   *
*                         des Sensors auf dem Boden erfolgt �ber ein verkn�ftes *
*                         TISOItem)						*
*  TFieldExplosion	- Verwaltet die Explosion auf einem bestimmten Feld	*
*  TDoorObject		- Verwaltet eine T�r, die gerade auf oder zu geht	*
*  TGreanadeTimer	- Stellt einen Timer f�r Granaten zur Verf�gung		*
*			  die nach einer Bestimmten Zeit explodiert		*
*  TWayPointMarker	- Verwaltet die Anzeige eines Wegpunktes		*
*										*
********************************************************************************}

{$I ../Settings.inc}

unit ISOObjects;

interface

uses
  Classes, Windows, DXISOEngine, GameFigure, DXDraws, XForce_types, ISOTools, SysUtils,
  Blending, DirectDraw, ISOMessages, KD4Utils, math, TraceFile,DirectFont,
  NGTypes, AGFUnit, Graphics, Defines, NotifyList, XANBitmap, StringConst;

type
  TVektor = record
    X,Y,Z : Integer;
  end;

  TDeathObject = class(TISOObject)
  private
    fXANBitmap : TXANBitmap;
    fDirection : TViewDirection;
    fOffSet    : TPoint;
    fFrame     : Integer;
    fFiller    : Boolean;

    fFrames    : Integer;
    fImageW    : Integer;
    fImageH    : Integer;
  protected
    function PerformFrame: boolean;override;
    procedure SetFigure(const Figure: TGameFigure);override;

    function GetVisible: Boolean;override;
  public
    procedure Draw(Surface : TDirectDrawSurface;X,Y: Integer);override;

  end;

  // Wird verwendet, um den Infotext zu einer Einheit anzuzeigen
  TFigureObject = class(TISOObject)
  protected
    procedure DestroyFigure(Sender: TObject);override;

    procedure SetInfoTextRect(var Rect: TRect);override;
    procedure SetInfoText;
    procedure SetFigure(const Figure: TGameFigure);override;
  public
    constructor Create(Engine: TDXISOEngine);override;
    function ShowInfoText(Visible: Boolean): Boolean;override;
    procedure Draw(Surface : TDirectDrawSurface;X,Y: Integer);override;
  end;

  TMouseHintInfoObject = class(TISOObject)
  private
    procedure SetShowText(const Value: String);
  protected
    procedure SetInfoTextRect(var Rect: TRect);override;
  public
    constructor Create(Engine: TDXISOEngine);override;
    procedure Draw(Surface : TDirectDrawSurface;X,Y: Integer);override;

    property ShowText: String read fInfoText write SetShowText;
  end;

  {$IFNDEF DISABLEBEAM}
  TBeamObject = class(TISOObject)
  private
    fFrame       : Integer;
    fSource      : TDirectDrawSurface;
    fOnReady     : TNotifyEvent;
    fBeamDown    : boolean;
    procedure SetBeamDown(const Value: boolean);
  protected
    function PerformFrame: boolean;override;
  public
    procedure Draw(Surface : TDirectDrawSurface;X,Y: Integer);override;
    property Source        : TDirectDrawSurface read fSource write fSource;
    property BeamDown      : boolean read fBeamDown write SetBeamDown;
    property OnReady       : TNotifyEvent read fOnReady write fOnReady;
  end;
  {$ENDIF}

  TGameFigureZiel = class(TISOObject)
  private
    fXANBitmap : TXANBitmap;
  protected
    procedure SetFigure(const Figure: TGameFigure);override;
  public
    destructor Destroy;override;
    procedure Draw(Surface : TDirectDrawSurface;X,Y: Integer);override;
  end;

  TISOItem = class(TISOObject)
  private
    fLagerItem           : TLagerItem;
    // OffSet sind immer * 100 (bei zeichnen div 100)
    fOffset              : TVektor;
    fIncOffSet           : TVektor;

    fStatus              : TFigureStatus;
    fLinkedObject        : TISOObject;
    fMode                : (mFall,mThrow,mNone);
    fThrowTarget         : TPoint;
    fThrowWinkel         : double;
    fTime                : double;
    function GetPunkteWert: Integer;
    procedure SetLagerItem(const Value: TLagerItem);
  protected
    function PerformFrame: boolean;override;
    procedure ChangeOffSet;

    procedure SetInfoTextRect(var Rect: TRect);override;
  public
    constructor Create(Engine: TDXISOEngine);override;
    destructor destroy;override;
    procedure Explode;override;
    procedure Draw(Surface : TDirectDrawSurface;X,Y: Integer);override;
    procedure SetLinkedObject(ISOObject: TISOObject);
    procedure ThrowTo(Pos: TPoint; Strength: Integer);
    procedure SetPos(X,Y: Integer);override;
    procedure SetOffSet(Point: TPoint);

    function InRange(Figure: TGameFigure): Boolean;

    property Item          : TLagerItem      read fLagerItem     write SetLagerItem;
    property FriendStatus  : TFigureStatus   read fStatus        write fStatus;
    property PunkteWert    : Integer         read GetPunkteWert;
    property OffSet        : TVektor         read fOffSet        write fOffSet;
  end;

  TSensorItem = class(TISOObject)
  private
    fSensorFigure: TGameFigure;
  protected
  public
    constructor Create(Engine: TDXISOEngine);override;
    destructor destroy;override;

    procedure SetPos(X,Y: Integer);override;

    procedure SetSensorInfo(const Item: TLagerItem; X,Y: Integer);
    procedure Draw(Surface : TDirectDrawSurface;X,Y: Integer);override;
  end;

  TMineItem = class(TISOObject)
  private
    fRange     : Integer;
    fStatus    : TFigureStatus;
    fISOItem   : TISOItem;
  protected
    procedure DoExplosion;
    procedure SetDeleted(const Value: boolean);override;
  public
    constructor Create(Engine: TDXISOEngine);override;
    destructor Destroy;override;
    procedure Explode;override;
    procedure SetMineInfo(const Item: TLagerItem; X,Y: Integer);
    procedure Draw(Surface : TDirectDrawSurface;X,Y: Integer);override;
    procedure SetLinkedItem(ISOItem: TISOItem);

    procedure CheckFigure(Figure: TGameFigure);

    property FigureStatus: TFigureStatus read fStatus write fStatus;
  end;

  TFieldExplosion = class(TISOObject)
  private
    fImage     : TAGFImage;
    fFrame     : Integer;
    fCount     : Integer;
    fStrength  : Integer;
    fPower     : Integer;
    fFrom      : TPoint;
    fOnlyWall  : Boolean;
    fParticel  : Array[0..2] of record
                   BegFrame : Integer;
                   OffX     : Integer;
                   OffY     : Integer;
                 end;
  protected
    procedure DestroyField;
  public
    constructor Create(Engine: TDXISOEngine);override;
    function PerformFrame: boolean;override;
    procedure Draw(Surface : TDirectDrawSurface;X,Y: Integer);override;
    function GetAttackStrength: Integer;override;
    function GetAttackPower   : Integer;override;

    procedure SetImage(Image: TAGFImage);
    procedure SetExplosionParams(Explosion: TExplosionInfo);
  end;

  TDoorObject = class(TISOObject)
  private
    fFrame      : Integer;
    fClose      : Boolean;
    fSurface    : TDirectDrawSurface;
    fTileRect   : TRect;
    fOffSetX    : Integer;
    fOffSetY    : Integer;
    fOpenLeft   : Boolean;
    fWallType   : TWallType;
    fIsNew      : Boolean;
    fOpenCounter: Integer;
  protected
    function PerformFrame: boolean;override;
  public
    constructor Create(Engine: TDXISOEngine);override;

    procedure Draw(Surface : TDirectDrawSurface;X,Y: Integer);override;

    procedure SetDoorInfo(Surface: TDirectDrawSurface; Tile: Integer; WallType: TWallType; OpenLeft: boolean);
    procedure CloseDoor;
    procedure OpenDoor;

    property WallType: TWallType read fWallType;
    property IsNew   : Boolean read fIsNew;
  end;

  TGreanadeTimer = class(TISOObject)
  private
    fRemainTime : Integer;
    Objekt      : TISOItem;
  protected
    function PerformFrame: boolean;override;
  public
    constructor Create(Engine: TDXISOEngine);override;
    procedure Draw(Surface : TDirectDrawSurface;X,Y: Integer);override;

    procedure SetGreanadeInfo(Greanade: TISOItem;Time: Integer);
  end;

  TWayPointMarker = class(TISOObject)
  private
    fDirection   : TViewDirection;
    fColorInd    : Integer;
    procedure SetColorInd(const Value: Integer);
    procedure SetDirection(const Value: TViewDirection);
    procedure Redraw;
  public
    procedure Draw(Surface: TDirectDrawSurface;X,Y: Integer);override;
    destructor Destroy;override;

    property Direction  : TViewDirection read fDirection write SetDirection;
    property ColorIndex : Integer read fColorInd write SetColorInd;
  end;

implementation

uses
  lager_api;

{ TDeathObject }

procedure TDeathObject.Draw(Surface: TDirectDrawSurface; X, Y: Integer);
begin
  fXANBitmap.RenderWithShadow(Surface,X+fOffSet.X,Y+fOffSet.Y,fFrame+(AniFrames*Integer(fDirection)));
end;

function TDeathObject.GetVisible: Boolean;
begin
  result:=inherited GetVisible;
  if result and (fFigure<>nil) and (fFigure.FigureStatus<>fsFriendly) then
  begin
    Result:=ISOMap.TileEntry(X,Y).AlphaV.All>1;
  end;
end;

function TDeathObject.PerformFrame: boolean;
begin
  result:=false;
  if fFrame<fFrames-1 then
  begin
    fFiller:=not fFiller;
    if fFiller then
      exit;

    inc(fFrame);
    Rec.Point:=Position;
    SendVMessage(vmGetTileRect);
    Rec.Rect:=ResizeRect(Rec.Rect,-((fImageH-60) div 2));
    SendVMessage(vmAktuTempSurfaceRect);
    result:=true;
  end;
end;

procedure TDeathObject.SetFigure(const Figure: TGameFigure);
begin
  inherited;

  if fFigure=nil then
    exit;

  fOffSet:=Figure.GetUnitOffset;

  fXANBitmap:=Figure.DeadImageSurface;
  fImageH:=ModelInfos[Figure.Manager.FigureMap].AniHeight;
  fImageW:=ModelInfos[Figure.Manager.FigureMap].AniWidth;

  fFrames:=AniFrames;

  fDirection:=Figure.ViewDirection;
  SetPos(Figure.XPos,Figure.YPos);
end;

{$IFNDEF DISABLEBEAM}

{ TBeamObject }

procedure TBeamObject.Draw(Surface: TDirectDrawSurface; X, Y: Integer);
begin
  if (fFrame<0) or (fFrame>80) then exit;

  DrawBeamEffekt(Surface,X,Y,Rect(fFrame*49,120,(fFrame+1)*49,180),fSource);
  Surface.Draw(X,Y,Rect(fFrame*49,0,(fFrame+1)*49,60),fSource);
  DrawBeamEffekt(Surface,X,Y,Rect(fFrame*49,60,(fFrame+1)*49,120),fSource);
end;

function TBeamObject.PerformFrame: boolean;
var
  EndBeam : boolean;
begin
  if fBeamDown then
  begin
    Inc(fFrame);
    EndBeam:=fFrame>=81;
  end
  else
  begin
    dec(fFrame);
    EndBeam:=fFrame<=0;
  end;
  Rec.Point:=Point(X,Y);
  SendVMessage(vmGetTileRect);
  SendVMessage(vmAktuTempSurfaceRect);
  result:=true;
  if EndBeam then
  begin
    if Assigned(fOnReady) then fOnReady(Self);
    fDeleted:=true;
  end;
end;

procedure TBeamObject.SetBeamDown(const Value: boolean);
begin
  fBeamDown := Value;
  if Value then
    fFrame:=0
  else
    fFrame:=80;
end;

{$ENDIF}

{ TGameFigureZiel }

destructor TGameFigureZiel.Destroy;
begin
  // Bereich neu zeichnen
  Rec.Point:=Point(X,Y);
  SendVMessage(vmGetTileRect);
  SendVMessage(vmAktuTempSurfaceRect);
  inherited;
end;

procedure TGameFigureZiel.Draw(Surface: TDirectDrawSurface; X, Y: Integer);
var
  Font              : TDirectFont;
begin
  fXANBitmap.RenderWithOutShadow(Surface,X,Y,0,true);

  // Zeiteinheiten zeichnen
  if not fFigure.Echtzeit then
  begin
    if fFigure.Way.TimeUnits<=fFigure.Zeiteinheiten then
      Font:=LimeStdFont
    else
      Font:=RedStdFont;
    Font.Draw(Surface,X+18,Y-10,IntToStr(fFigure.Way.TimeUnits));
  end;
end;

procedure TGameFigureZiel.SetFigure(const Figure: TGameFigure);
begin
  inherited;
  if Figure=nil then
    exit;

  fXANBitmap:=Figure.WalkImageSurface;
  SetPos(Figure.Ziel.x,Figure.Ziel.y);
  Rec.Point:=Point(X,Y);
  SendVMessage(vmGetTileRect);
  SendVMessage(vmAktuTempSurfaceRect);
end;

{ TISOItem }

procedure TISOItem.ChangeOffSet;
var
  Pos       : TPoint;
  NewOffSet : TFloatPoint;
  Dummy     : Integer;
begin
  NewOffSet.X:=(fOffSet.X+fIncOffSet.X)/100;
  NewOffSet.Y:=(fOffSet.Y+fIncOffSet.Y)/100;

  Pos:=CheckOffset(NewOffSet,NewOffSet);

  if (Pos.X<>X) or (Pos.Y<>Y) then
  begin
    if CheckWay(Position,Pos) then
    begin
      // Bei einem �bergang mit Mauer muss nochmal genauer gepr�ft werden,
      // (sonst wird bei der Pr�fung => ob durch Mauer durchgeworfen <=
      //  eventuell die Falsche Mauer gepr�ft)
      NewOffSet.X:=fOffSet.X/100;
      NewOffSet.Y:=fOffSet.Y/100;

      for Dummy:=1 to 10 do
      begin
        NewOffSet.X:=NewOffSet.X+(fIncOffSet.X/1000);
        NewOffSet.Y:=NewOffSet.Y+(fIncOffSet.Y/1000);

        Pos:=CheckOffset(NewOffSet,NewOffSet);

        if (Pos.X<>X) or (Pos.Y<>Y) then
        begin
          if CheckWay(Position,Pos) then
          begin
            if CheckWay(NewOffSet,(-fOffSet.Z div 100),Pos,3)>0 then
            begin
              fMode:=mFall;
              exit;
            end;
          end;
        end;
      end;
    end;
    SetPos(Pos);
  end;

  fOffSet.X:=round(NewOffSet.X*100);
  fOffSet.Y:=round(NewOffSet.Y*100);
end;

constructor TISOItem.Create(Engine: TDXISOEngine);
begin
  inherited;
  fIncOffset.Z:=50;
  fOffSet.X:=0;
  fOffSet.Y:=0;
  fOffset.Z:=-3000;

  HitPoints:=10;
  fMode:=mFall;
end;

destructor TISOItem.destroy;
begin
  ShowInfoText(false);
  fDeleted:=true;
  Rec.Point:=Point(X,Y);
  SendVMessage(vmGetTileRect);
  SendVMessage(vmAktuTempSurfaceRect);
  Engine.ItemList.Remove(Self);

  if (fLinkedObject<>nil) and (not fLinkedObject.Deleted) then
    fLinkedObject.Deleted:=true;

  inherited;
end;

procedure TISOItem.Draw(Surface: TDirectDrawSurface; X, Y: Integer);
begin
//  Surface.ClippingRect:=Engine.ClientRect;
  lager_api_DrawISOImage(fLagerItem.ImageIndex,Surface,X-ObjectOffSetX+(fOffSet.X div 100),Y+((fOffSet.Y+fOffSet.Z) div 100)+(ObjectOffSetY));
//  Surface.Pixels[X+(fOffSet.X div 100)-ObjectOffSetX,Y+((fOffSet.Z+fOffSet.Y) div 100)-OffSetYFromOld]:=bcRed;
//  Surface.Pixels[X-ObjectOffSetX+(fOffSet.X div 100),Y+((fOffSet.Y+fOffSet.Z) div 100)+(ObjectOffSetY)]:=bcBlue;
//  Surface.ClearClipRect;
end;

procedure TISOItem.Explode;
begin
  if fDeleted then
    exit;

  inherited;

  if fLinkedObject<>nil then
  begin
    fLinkedObject.Explode;
    fLinkedObject:=nil;
  end;

  case fLagerItem.TypeID of
    ptMine,ptGranate    : Engine.CreateExplosion(Figure,Position,fLagerItem.Reichweite,fLagerItem.Strength,100);
  end;

  if not (fLagerItem.TypeID in [ptMine,ptGranate]) then
  begin
    // Punkte abziehen, wenn es die Eigene Ausr�stung ist
    if FriendStatus=fsFriendly then
      BuchPunkte(bptItemLost,GetPunkteWert);
  end;
end;

function TISOItem.GetPunkteWert: Integer;
begin
  case fLagerItem.KaufPreis of
    0..100      : result:=1;
    101..200    : result:=2;
    201..300    : result:=3;
    301..400    : result:=4;
    401..500    : result:=5;
    501..1000   : result:=8;
    1001..2000  : result:=12;
    2001..3000  : result:=15;
    3001..4000  : result:=20;
    4001..5000  : result:=25;
    else
      result:=30;
  end;
//  GlobalFile.Write(fLagerItem.Name,Format('%s = %d Punkte',[fLagerItem.Kaufpreis,result]));
end;

function TISOItem.InRange(Figure: TGameFigure): Boolean;
var
  WayFree: TWayFreeResult;
begin
  result:=false;
  if (abs(X-Figure.XPos)>1) or (abs(Y-Figure.YPos)>1) then
    exit;
  ISOMap.IsWayFree(Point(Figure.XPos,Figure.YPos),Position,WayFree);
  result:=WayFree.WayBlocked in [wbNone,wbDoor,wbSoldat];
end;

function TISOItem.PerformFrame: boolean;
begin
  result:=false;
  if fMode=mFall then
  begin
    inc(fOffSet.Z,fIncOffSet.Z);
    inc(fIncOffSet.Z,20);
    if fOffSet.Z>BottomZOffset then
    begin
      fOffSet.Z:=BottomZOffset;
      fMode:=mNone;
    end;

    Rec.Point:=Point(X,Y);
    SendVMessage(vmGetTileRect);
    OffSetRect(Rec.Rect,(fOffSet.X div 100)-HalfTileWidth,(fOffSet.Y div 100)-(fOffSet.Z div 100)-HalfTileHeight);
    SendVMessage(vmAktuTempSurfaceRect);

    result:=true;
  end
  else if fMode=mThrow then
  begin
    // Berechnet nur X und Y OffSet
    ChangeOffSet;

    fOffSet.Z:=BottomZOffset-(round(((-Gravitation / 2)*(fTime*fTime)+(50*fTime*sin(fThrowWinkel)))*100))-(UnitShootHeight*100);
    if fOffSet.Z>BottomZOffset then
    begin
      fOffSet.Z:=BottomZOffset;
      fMode:=mNone;
    end;
    fTime:=fTime+0.10;

    Rec.Point:=Point(X,Y);
    SendVMessage(vmGetTileRect);
    OffSetRect(Rec.Rect,(fOffSet.X div 100)-HalfTileWidth,(fOffSet.Y div 100)-(fOffSet.Z div 100)-HalfTileHeight);
    SendVMessage(vmAktuTempSurfaceRect);
    result:=true;
  end;
end;

procedure TISOItem.SetInfoTextRect(var Rect: TRect);
var
  TextWid : Integer;
  TilRect : TRect;
begin
  TextWid:=WhiteStdFont.TextWidth(fLagerItem.Name)+8;
  TilRect:=Engine.GetTileRect(Self,Point(X,Y));
  Rect:=Classes.Rect(TilRect.Left+HalfTileWidth-(TextWid shr 1),TilRect.Bottom-(HalfTileHeight+8),TilRect.Left+(HalfTileWidth)+(TextWid shr 1),TilRect.Bottom-(HalfTileHeight+8-18));
  OffsetRect(Rect,(fOffSet.X div 100)-HalfTileWidth,((fOffSet.Y-HalfTileHeight)+(fOffSet.Z-BottomZOffset)) div 100);
end;

procedure TISOItem.SetLagerItem(const Value: TLagerItem);
begin
  fLagerItem := Value;
  fShowInfoText:=true;
  fInfoText:=fLagerItem.Name;
end;

procedure TISOItem.SetLinkedObject(ISOObject: TISOObject);
begin
  fLinkedObject:=ISOObject;
end;

procedure TISOItem.SetOffSet(Point: TPoint);
begin
  fOffset.X:=(Point.X+HalfTileWidth)*100;
  fOffset.Y:=(Point.Y+HalfTileHeight)*100;
end;

procedure TISOItem.SetPos(X, Y: Integer);
begin
  inherited;
  
  if fLinkedObject<>nil then
    fLinkedObject.SetPos(X,Y);
end;

procedure TISOItem.ThrowTo(Pos: TPoint;Strength: Integer);
var
  Winkel   : Integer;
  fromPos  : TPoint;
  ToPos    : TPoint;
  Width    : double;
  Entf     : Integer;

  // Variablen zur Berechnung des Wurfwinkels
  GravX       : double;
  SqrStrength : double;
  Gravstrength: double;
  BeforeSqrt  : double;
begin
  fThrowTarget:=Pos;
  fMode:=mThrow;

  Rec.Point:=Position;
  SendVMessage(vmGetTileRect);
  FromPos:=Rec.Rect.TopLeft;

  Rec.Point:=Pos;
  SendVMessage(vmGetTileRect);
  ToPos:=Rec.Rect.TopLeft;

  Entf:=CalculateEntfern(FromPos,ToPos);

  { Berechnung des Wurfwinkels }
//  SqrStrength:=Sqr(Strength/);
//    SqrStrength:=Sqr(min(50,Strength)/fLagerItem.Gewicht);
  // Fester Wert wegen Bug 1180
  SqrStrength:=Sqr(150);

  // Zufall einbauen (bisher ohne ber�cksichtigung der Treffsicherheit
  SqrStrength:=SqrStrength*RandG(1,0.2);

  GravX:=Gravitation*Entf;
  Gravstrength:=sqrStrength/gravX;
  
  BeforeSqrt:=sqr(Gravstrength)-((2*SqrStrength*-30)/(gravitation*sqr(Entf)))-1;
  if BeforeSqrt<0 then
    // Maximale Wurfweite �berschritten
    fThrowWinkel:=DegToRad(30)
  else
    fThrowWinkel:=ArcTan(Gravstrength-Sqrt(BeforeSqrt));

  // Zufall einbauen (bisher ohne ber�cksichtigung der Treffsicherheit
  fThrowWinkel:=fThrowWinkel*RandG(1,0.2);


  // OffSet-Verschiebung berechen
  Winkel:=CalculateWinkel(ToPos,FromPos);

//  Width:=((Strength/fLagerItem.Gewicht)*0.1*cos(fThrowWinkel));
  Width:=((125)*0.1*cos(fThrowWinkel));
  fIncOffSet.X:= round(sin(DegToRad(Winkel))*Width*100);
  fIncOffSet.Y:=-round((cos(DegToRad(Winkel))*Width*100));

  fOffSet.Z:=-UnitShootHeight*100;

  fTime:=0;
  MustBehindClip:=true;
end;

{ TSensorItem }

constructor TSensorItem.Create(Engine: TDXISOEngine);
begin
  inherited;
  fSensorFigure:=TGameFigure.Create(Engine.Figures);
  fSensorFigure.ImRaumschiff:=true;
  fSensorFigure.FigureStatus:=fsObject;
  fSensorFigure.Echtzeit:=true;
end;

destructor TSensorItem.destroy;
begin
  fSensorFigure.RemoveFromGameField;
  fSensorFigure.Free;
  SendVMessage(vmAktuTempSurface);
  inherited;
end;

procedure TSensorItem.Draw(Surface: TDirectDrawSurface; X, Y: Integer);
begin
end;

procedure TSensorItem.SetPos(X, Y: Integer);
begin
  inherited;

  if X<>-1 then
    fSensorFigure.SetUnitToPos(Point(X,Y));
end;

procedure TSensorItem.SetSensorInfo(const Item: TLagerItem; X,Y: Integer);
begin
  fSensorFigure.XPos:=X;
  fSensorFigure.YPos:=Y;
  fSensorFigure.SehWeite:=Item.Strength;
  fSensorFigure.ViewDirection:=vdAll;
end;

{ TDoorObject }

procedure TDoorObject.CloseDoor;
begin
  dec(fOpenCounter);
  if fOpenCounter>0 then exit;
  if fFrame=-10 then
  begin
    fIsNew:=false;
    fFrame:=0;
    if fOpenLeft then
    begin
      fTileRect.Left:=fTileRect.Right;
      case fWallType of
        wtTRight,wtBLeft  : dec(fOffSetY,HalfTileHeight);
        wtTLeft,wtBRight  : inc(fOffSetY,HalfTileHeight);
      end;
    end
    else
    begin
      inc(fOffSetX,HalfTileWidth);
      fTileRect.Right:=fTileRect.Left;
      case fWallType of
        wtTRight,wtBLeft  : inc(fOffSetY,HalfTileHeight);
        wtTLeft,wtBRight  : dec(fOffSetY,HalfTileHeight);
      end;
    end;
  end;
  fClose:=true;
end;

constructor TDoorObject.Create(Engine: TDXISOEngine);
begin
  inherited;

  fOpenCounter:=0;
  fIsNew:=true;
  fFrame:=-10;
end;

procedure TDoorObject.Draw(Surface: TDirectDrawSurface; X, Y: Integer);
var
  Alpha : Integer;
begin
  Alpha:=0;
  case fWallType of
    wtTLeft  : Alpha:=ISOMap.TileEntry(Self.X,Self.Y).AlphaV.ATLeft;
    wtTRight : Alpha:=ISOMap.TileEntry(Self.X,Self.Y).AlphaV.ATRight;
    wtBLeft  : Alpha:=ISOMap.TileEntry(Self.X,Self.Y).AlphaV.ABLeft;
    wtBRight : Alpha:=ISOMap.TileEntry(Self.X,Self.Y).AlphaV.ABright;
  end;
  fTileRect.Top:=Alpha*Wallheight;
  fTileRect.Bottom:=fTileRect.Top+WallHeight;

  Surface.Draw(X+fOffSetX,Y+fOffSetY,fTileRect,fSurface);
//  Surface.Pixels[X+fOffSetX,Y+fOffSetY]:=bcRed;
end;

procedure TDoorObject.OpenDoor;
begin
  if fFrame=-10 then
  begin
    fIsNew:=false;
    fFrame:=WallWidth;
  end;
  fClose:=false;
  inc(fOpenCounter);
end;

function TDoorObject.PerformFrame: boolean;
begin
  result:=false;
  if fClose then
  begin
    inc(fFrame);
    if fFrame>=(WallWidth) then
    begin
      fDeleted:=true;
      case fWallType of
        wtTLeft  : ISOMap.TileEntry(X,Y).TLeft:=ISOMap.TileEntry(X,Y).DoorInfo.TLeft;
        wtTRight : ISOMap.TileEntry(X,Y).TRight:=ISOMap.TileEntry(X,Y).DoorInfo.TRight;
        wtBLeft  : ISOMap.TileEntry(X,Y).BLeft:=ISOMap.TileEntry(X,Y).DoorInfo.BLeft;
        wtBRight : ISOMap.TileEntry(X,Y).BRight:=ISOMap.TileEntry(X,Y).DoorInfo.BRight;
      end;
      SendVMessage(vmTerrainChange);
      result:=true;
      exit;
    end;
    if fOpenLeft then
    begin
      dec(fTileRect.Left);
      if (fFrame mod 2)=1 then
      begin
        case fWallType of
          wtTRight,wtBLeft  : inc(fOffSetY);
          wtTLeft,wtBRight  : dec(fOffSetY);
        end;
      end;
    end
    else
    begin
      inc(fTileRect.Right);
      dec(fOffSetX);
      if (fFrame mod 2)=1 then
      begin
        case fWallType of
          wtTRight,wtBLeft  : dec(fOffSetY);
          wtTLeft,wtBRight  : inc(fOffSetY);
        end;
      end;
    end;
  end
  else
  begin
    if fFrame<1 then
      exit;

    dec(fFrame);
    if fOpenLeft then
    begin
      inc(fTileRect.Left);
      if (fFrame mod 2)=1 then
      begin
        case fWallType of
          wtTRight,wtBLeft  : dec(fOffSetY);
          wtTLeft,wtBRight  : inc(fOffSetY);
        end;
      end;
    end
    else
    begin
      dec(fTileRect.Right);
      inc(fOffSetX);
      if (fFrame mod 2)=1 then
      begin
        case fWallType of
          wtTRight,wtBLeft  : inc(fOffSetY);
          wtTLeft,wtBRight  : dec(fOffSetY);
        end;
      end;
    end;
  end;
  Rec.Point:=Point(X,Y);
  SendVMessage(vmGetTileRect);
  inc(Rec.Rect.Bottom);
  SendVMessage(vmAktuTempSurfaceRect);
  result:=true;
end;

procedure TDoorObject.SetDoorInfo(Surface: TDirectDrawSurface;
  Tile: Integer; WallType: TWallType; OpenLeft: Boolean);
begin
  fSurface:=Surface;

  fTileRect:=Rect(Tile*WallWidth,Wallheight*6,(Tile+1)*WallWidth,Wallheight*7);

{  fOffSetX:=-ObjectOffSetX;
  fOffSetY:=(ObjectOffSetY)-(WallHeight-HalfTileHeight);

  case WallType of
    wtTLeft : inc(fOffSetY,Wall3DEffekt+1);
    wtTRight :
    begin
      inc(fOffSetY,Wall3DEffekt+1);
      inc(fOffSetX,HalfTileWidth);
    end;
    wtBLeft :
    begin
      inc(fOffSetY,HalfTileHeight+1);
      BeforeUnit:=true;
    end;
    wtBRight :
    begin
      inc(fOffSetX,HalfTileWidth);
      inc(fOffSetY,HalfTileHeight);
      BeforeUnit:=true;
    end;
  end;}

  fOffSetX:=-ObjectOffSetX;
  fOffSetY:=ObjectOffSetY;

  case WallType of
    wtTLeft :
      dec(fOffSetY,(WallHeight-(Wall3DEffekt div 2)-HalfTileHeight));
    wtTRight :
    begin
      inc(fOffSetX,(HalfTileWidth-Wall3DEffekt));
      dec(fOffSetY,(WallHeight-(Wall3DEffekt div 2)-HalfTileHeight));
    end;
    wtBLeft :
    begin
      dec(fOffSetY,(WallHeight-TileHeight));
      BeforeUnit:=true;
    end;
    wtBRight :
    begin
      inc(fOffSetX,(HalfTileWidth-Wall3DEffekt));
      dec(fOffSetY,(WallHeight-TileHeight));
      BeforeUnit:=true;
    end;
  end;

  fWallType:=WallType;
  fOpenLeft:=OpenLeft;
end;

{ TMineItem }

procedure TMineItem.CheckFigure(Figure: TGameFigure);

  function IsUnitInRange: Boolean;
{  var
    Steps     : Integer;
    XStep     : double;
    YStep     : double;
    X,Y       : double;
    LastPoint : TPoint;}
  begin
    // Entfernung pr�fen
    result:=(abs(Figure.XPos-Position.x)<=(fRange div 2)) and (abs(Figure.YPos-Position.Y)<=(fRange div 2));

    if not result then
      exit;

    result:=not CheckWay(Point(Figure.XPos,Figure.YPos),Position);
{
    Steps:=max(abs(Figure.XPos-Position.x),abs(Figure.YPos-Position.Y));
    if Steps=0 then
      exit;
    XStep:=(Figure.XPos-Position.x) / Steps;
    YStep:=(Figure.YPos-Position.Y) / Steps;
    X:=Position.x;
    Y:=Position.Y;
    LastPoint:=Point(Position.x,Position.Y);
    while Steps>0 do
    begin
      X:=X+XStep;
      Y:=Y+YStep;
      if CheckWay(Point(Round(X),Round(Y)),LastPoint) then
      begin
        result:=false;
        exit;
      end;
      LastPoint:=Point(Round(X),Round(Y));
      dec(Steps);
    end;}
  end;

begin
  if (Figure=nil) or fDeleted then
    exit;

  {$IFNDEF MINEFROMALL}
  if (FigureStatus=Figure.FigureStatus) or (Figure.FigureStatus=fsNeutral) then
    exit;
  {$ENDIF}

  if IsUnitInRange then
    DoExplosion;

end;

constructor TMineItem.Create(Engine: TDXISOEngine);
begin
  inherited;
  HitPoints:=10;
end;

destructor TMineItem.Destroy;
begin
  if (fISOItem<>nil) and (not fISOItem.fDeleted) then
  begin
    fISOItem.Deleted:=true;
    fISOItem:=nil;
  end;
  inherited;
end;

procedure TMineItem.DoExplosion;
begin
  if (not fDeleted) and (fISOItem<>nil) and (not fISOItem.Deleted) then
  begin
    fISOItem.Explode;
    fISOItem:=nil;

    fDeleted:=true;
  end;
end;

procedure TMineItem.Draw(Surface: TDirectDrawSurface; X, Y: Integer);
begin
end;

procedure TMineItem.Explode;
begin
  fISOItem:=nil;
  inherited;
end;

procedure TMineItem.SetDeleted(const Value: boolean);
begin
  inherited;

  if Value then
    fISOItem:=nil;
end;

procedure TMineItem.SetLinkedItem(ISOItem: TISOItem);
begin
  fISOItem:=ISOItem;
end;

procedure TMineItem.SetMineInfo(const Item: TLagerItem; X, Y: Integer);
begin
  fRange:=Item.Reichweite;
  SetPos(X,Y);
  BeforeUnit:=true;
end;

{ TFieldExplosion }

constructor TFieldExplosion.Create(Engine: TDXISOEngine);
begin
  inherited;
  BlockEndGame:=true;
end;

procedure TFieldExplosion.DestroyField;
begin
  ISOMap.DoDamageOnWalls(fFrom,Point(X,Y),fStrength);
end;

procedure TFieldExplosion.Draw(Surface: TDirectDrawSurface; X, Y: Integer);
var
  Dummy: Integer;
begin
  if (fFrame<0) or fOnlyWall then
    exit;
    
//  Engine.Container.PowerDraw.PowerD3D.BeginScene(Surface);
  Engine.Container.PowerDraw.Lock(Surface);
  for Dummy:=0 to high(fParticel) do
  begin
    with fParticel[Dummy] do
      Engine.Container.PowerDraw.RenderAdd(fImage,X+OffX,Y+OffY,BegFrame+fFrame);
  end;
  Engine.Container.PowerDraw.UnLock;
//  Engine.Container.PowerDraw.PowerD3D.EndScene;
end;

function TFieldExplosion.GetAttackPower: Integer;
begin
  result:=fPower;
end;

function TFieldExplosion.GetAttackStrength: Integer;
begin
  result:=fStrength;
end;

function TFieldExplosion.PerformFrame: boolean;
var
  Dummy: Integer;
begin
  result:=false;
  if fDeleted then
    exit;

  result:=false;
  inc(fFrame);
  if fFrame>=fCount then
    fDeleted:=true
  else
  begin
    Rec.Point:=Point(X,Y);
    // Erst, wenn Frame 0 erreicht ist, beginnt die Explosion => heisst erst
    // hier darf der Schaden einsetzen
    if fFrame=0 then
    begin
      if not fOnlyWall then
      begin
        // Ausr�stung Schaden zuf�gen
        Rec.Point:=Point(X,Y);
        SendVMessage(vmGetISOObjects);
        for Dummy:=high(Rec.Objects) downto 0 do
        begin
          TISOObject(Rec.Objects[Dummy]).DoTreffer(GetAttackStrength);
        end;

        // Einheiten Schaden hinzuf�gen
        SendVMessage(vmGetFigure);
        if (Rec.Figure<>nil) and TGameFigure(Rec.Figure).IsOnField then
        begin
          if (TGameFigure(Rec.Figure).XPos=X) and (TGameFigure(Rec.Figure).YPos=Y) then
            TGameFigure(Rec.Figure).DoTreffer(Self);
        end;
      end;

      // Hier wird das Terrain ge�ndert
      DestroyField;
    end;

  end;

  if fFrame>0 then
  begin
    SendVMessage(vmGetTileRect);
    Rec.Rect:=ResizeRect(Rec.Rect,-30);
    SendVMessage(vmAktuTempSurfaceRect);
    result:=true;
  end;
end;

procedure TFieldExplosion.SetImage(Image: TAGFImage);
var
  Dummy: Integer;
begin
  fImage:=Image;
  fFrame:=0;
  fCount:=fImage.Pattern div 2;
  for Dummy:=0 to high(fParticel) do
  begin
    with fParticel[Dummy] do
    begin
      BegFrame:=random(2)*fCount;
      OffX:=random(30)-10;
      OffY:=random(30)-10;
    end;
  end;
end;

procedure TFieldExplosion.SetExplosionParams(Explosion: TExplosionInfo);
begin
  SetPos(Explosion.ToPos);
  fFrame:=round(-Explosion.Delay);
  fPower:=Explosion.Power;

  if fFrame>=0 then
    fFrame:=-1;

  fOnlyWall:=Explosion.OnlyWall;
  
  fStrength:=Explosion.Strength;
  fFrom:=Explosion.FromPos;
end;

{ TGreanadeTimer }

constructor TGreanadeTimer.Create(Engine: TDXISOEngine);
begin
  inherited;
//  Visible:=false;
end;

procedure TGreanadeTimer.Draw(Surface: TDirectDrawSurface; X, Y: Integer);
begin
  // Dummy sections (Objekt wird nicht gezeichnet)
end;

function TGreanadeTimer.PerformFrame: boolean;
begin
  result:=false;

  dec(fRemainTime,25);
  if (fRemainTime<=0) then
  begin
    if (fDeleted) or (Objekt=nil) then
      exit;

    if (not Objekt.Deleted) then
    begin
      Objekt.Explode;
      Objekt:=nil;
    end;
    fDeleted:=true;
  end;
end;

procedure TGreanadeTimer.SetGreanadeInfo(Greanade: TISOItem;
  Time: Integer);
begin
  fRemainTime:=Time;
  Objekt:=Greanade;
end;

{ TWayPointMarker }

destructor TWayPointMarker.Destroy;
begin
  Redraw;
  inherited;
end;

procedure TWayPointMarker.Draw(Surface: TDirectDrawSurface; X, Y: Integer);
var
  Top: Integer;
begin
  Top:=43+(fColorInd*13);
  Surface.Draw(X+18,Y+45,Rect(Integer(fDirection)*13,Top,(Integer(fDirection)+1)*13,Top+12),Engine.Container.ImageList.Items[1].PatternSurfaces[0]);
end;

procedure TWayPointMarker.Redraw;
begin
  Rec.Point:=Position;
  SendVMessage(vmGetTileRect);
  inc(Rec.Rect.Top,36);
  SendVMessage(vmAktuTempSurfaceRect);
end;

procedure TWayPointMarker.SetColorInd(const Value: Integer);
begin
  fColorInd := Value;
  Redraw;
end;

procedure TWayPointMarker.SetDirection(const Value: TViewDirection);
begin
  fDirection := Value;
  Redraw;
end;

{ TFigureObject }

constructor TFigureObject.Create(Engine: TDXISOEngine);
begin
  inherited;
  Visible:=false;
end;

procedure TFigureObject.DestroyFigure(Sender: TObject);
begin
  ShowInfoText(false);
  inherited;
  fDeleted:=true;
end;

procedure TFigureObject.Draw(Surface: TDirectDrawSurface; X, Y: Integer);
begin
end;

procedure TFigureObject.SetFigure(const Figure: TGameFigure);
begin
  inherited;

  fShowInfoText:=Figure<>nil;
  if fShowInfoText then
  begin
    fInfoText:=Figure.Name;
  end;
end;

procedure TFigureObject.SetInfoText;
begin
  if fFigure=nil then
    exit;

  Assert(Engine.Figures.fList.IndexOf(fFigure)<>-1);

  if (not fFigure.EchtZeit) and (fFigure.FigureStatus=fsfriendly) {$IFDEF SHOWENEMYTU} or true{$ENDIF} then
    fInfoText:=fFigure.Name+' '#2'('+IntToStr(fFigure.Zeiteinheiten)+' '+ST0309260010+')'
  else
    fInfoText:=fFigure.Name;

  {$IFDEF SHOWSTATE}
  fInfoText:=fInfoText+#1' - '+fFigure.GetStateText;
  {$ENDIF}
end;

procedure TFigureObject.SetInfoTextRect(var Rect: TRect);
var
  TextWid : Integer;
  TilRect : TRect;
  Off     : TPoint;
begin
  Assert(fFigure<>nil);

  SetInfoText;

  TextWid:=WhiteStdFont.TextWidth(fInfoText)+8;
  TilRect:=Engine.GetTileRect(Self,Point(fFigure.XPos,fFigure.YPos));
  Rect:=Classes.Rect(TilRect.Left+HalfTileWidth-(TextWid shr 1),TilRect.Bottom+1,TilRect.Left+HalfTileWidth+(TextWid shr 1),TilRect.Bottom+19);
  Off:=fFigure.GetUnitOffset;
  OffsetRect(Rect,Off.x,Off.Y);
//  Rect:=TilRect;
end;

function TFigureObject.ShowInfoText(Visible: Boolean): Boolean;
begin
  result:=false;
  if (fFigure=nil) or ((not Visible) and fFigure.Selected and (not fFigure.EchtZeit)) then
    exit;

  if Visible then
  begin
    SetInfoText;
    Rec.Figure:=fFigure;
    SendVMessage(vmCanDrawUnit);
    Visible:=Rec.Result;
  end;

  result:=inherited ShowInfoText(Visible);
end;

{ TMouseHintInfoObject }


{ TMouseHintInfoObject }

constructor TMouseHintInfoObject.Create(Engine: TDXISOEngine);
begin
  inherited;
  fShowInfoText:=true;
end;

procedure TMouseHintInfoObject.Draw(Surface: TDirectDrawSurface; X,
  Y: Integer);
begin
  // do nothing
end;

procedure TMouseHintInfoObject.SetInfoTextRect(var Rect: TRect);
var
  MousePos: TPoint;
begin
  MousePos:=Engine.Container.MousePos;
  inc(MousePos.X,34);

  Rect:=Classes.Rect(MousePos.X,MousePos.Y,MousePos.X+WhiteStdFont.TextWidth(fInfoText)+8,MousePos.Y+19);
end;

procedure TMouseHintInfoObject.SetShowText(const Value: String);
begin
  fInfoText := Value;
  SendVMessage(vmEngineRedraw);
end;

end.
