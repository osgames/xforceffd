{********************************************************************************
*										*
* Copyright (C) 2004  X-Force Team - jim_raynor@web.de				*
*										*
* This file is part of X-Force: Fight For Destiny (X-Force).			*
*										*
* X-Force is free software; you can redistribute it and/or modify		*
* it under the terms of the GNU General Public License as published by		*
* the Free Software Foundation; either version 2 of the License, or		*
* (at your option) any later version.						*
*										*
* X-Force is distributed in the hope that it will be useful,			*
* but WITHOUT ANY WARRANTY; without even the implied warranty of		*
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the			*
* GNU General Public License for more details.					*
*										*
* You should have received a copy of the GNU General Public License		*
* along with X-Force; if not, write to the Free Software			*
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA	*
*										*
*********************************************************************************
*										*
* Diese Klasse verwaltet die abzuspielende Musik-Kategorie im Bodeneinsatz	*
* �ber Unitsseeing und UnitsHiding wird der Klasse bekannt gemacht, sobald	*
* ein Alien gesichtet bzw. verschwunden ist. Dies f�hrt in der Regel zu		*
* �nderung der Musik die abgespielt wird.					*
*										*
********************************************************************************}

{$I ../Settings.inc}

unit Musikverwalter;

interface

uses DXISOEngine, DXContainer;

type
  TMusikVerwaltungEinsatz = class(TObject)
  private
    fEngine       : TDXIsoEngine;
    fContainer    : TDXContainer;

    fNormalTimer  : Integer;
    procedure StartGame(Sender: TObject);
    procedure EndGame(Sender: TObject);
    procedure UnitsSeeing(Sender: TObject);
    procedure UnitsHiding(Sender: TObject);

    function SearchTimer(Sender: TObject; Frames: Integer): boolean;
  public
    constructor Create(Engine: TDXISOEngine);
  end;

implementation

{ TMusikVerwaltungEinsatz }

uses GameFigure;

constructor TMusikVerwaltungEinsatz.Create(Engine: TDXISOEngine);
begin
  fEngine:=Engine;
  fContainer:=Engine.Container;

  fEngine.NotifyList.RegisterEvent(EVENT_ONSTARTGAME,StartGame);
  fEngine.NotifyList.RegisterEvent(EVENT_ONENDGAME,EndGame);

  fEngine.Figures.NotifyList.RegisterEvent(EVENT_SEEUNITS,UnitsSeeing);
  fEngine.Figures.NotifyList.RegisterEvent(EVENT_UNITSHIDE,UnitsHiding);
end;

procedure TMusikVerwaltungEinsatz.EndGame(Sender: TObject);
begin
  fContainer.DeleteFrameFunction(SearchTimer,Self);
end;

function TMusikVerwaltungEinsatz.SearchTimer(Sender: TObject;
  Frames: Integer): boolean;
begin
  dec(fNormalTimer,25*Frames);
  if fNormalTimer<=0 then
  begin
    fContainer.DeleteFrameFunction(SearchTimer,Self);
    fContainer.PlayMusicCategory('Bodeneinsatz - Normal');
  end;
  result:=false;
end;

procedure TMusikVerwaltungEinsatz.StartGame(Sender: TObject);
begin
  fContainer.PlayMusicCategory('Bodeneinsatz - Normal');
end;

procedure TMusikVerwaltungEinsatz.UnitsHiding(Sender: TObject);
begin
  fContainer.PlayMusicCategory('Bodeneinsatz - Aliens versteckt');
  fNormalTimer:=30000;
  fContainer.AddFrameFunction(SearchTimer,Self,25);
end;

procedure TMusikVerwaltungEinsatz.UnitsSeeing(Sender: TObject);
begin
  fContainer.PlayMusicCategory('Bodeneinsatz - Aliens gefunden');
  fContainer.DeleteFrameFunction(SearchTimer,Self);
end;

end.
 
