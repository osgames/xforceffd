{********************************************************************************
*										*
* Copyright (C) 2004  X-Force Team - jim_raynor@web.de				*
*										*
* This file is part of X-Force: Fight For Destiny (X-Force).			*
*										*
* X-Force is free software; you can redistribute it and/or modify		*
* it under the terms of the GNU General Public License as published by		*
* the Free Software Foundation; either version 2 of the License, or		*
* (at your option) any later version.						*
*										*
* X-Force is distributed in the hope that it will be useful,			*
* but WITHOUT ANY WARRANTY; without even the implied warranty of		*
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the			*
* GNU General Public License for more details.					*
*										*
* You should have received a copy of the GNU General Public License		*
* along with X-Force; if not, write to the Free Software			*
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA	*
*										*
*********************************************************************************
*										*
* ExtRecord wird verwendet um einfach Eigenschaften zu einem Objekt zu 		*
* speichern. Vorteil ist, dass ohne Probleme neue Eigenschaften definiert 	*
* werden k�nnen, bzw. veraltete gel�scht ohne das an die Speicherroutinen	*
* hand angelegt werden muss.							*
*										*
* So wird es nun an vielen Stellen genutzt, wo Records gespeichert werden m�ssen*
*										*
********************************************************************************}

{$I ../Settings.inc}

unit ExtRecord;

interface
{
  Konvertierung von Set nach Get
  Regul�rer Ausdruck zum Suchen : ^\( +\)\(.+\)Set\(.+(\)\(.+\),\(.+\));
  Ersetzen                      : \1\5:=\2Get\3\4);
}

uses SysUtils, Classes, Windows,thaXML, IDHashMap, NotifyList;


const
  EVENT_RECORDDEFINITION_DESTROY : TEventID = 0;

type
  TExtRecordValueType = (ervtInteger,
                         ervtCardinal,
                         ervtDouble,
                         ervtString,
                         ervtBoolean,
                         ervtBinary,
                         ervtRecordList);

  TExtRecordList        = class;
  TExtRecordDefinition  = class;
  TExtRecord            = class;

  TRecordArray          = Array of TExtRecord;

  TExtRecordValue = record
    ValueName     : String;
    HashValue     : Cardinal;
    Description   : String;
    ValueType     : TExtRecordValueType;
    defaultstring : String;
    valueString   : String;
    binaryLength  : Integer;
    binaryAddress : Pointer;
    case TExtRecordValueType of
      ervtInteger:(
        minValueInt: Integer;
        maxValueInt: Integer;
        defaultInt : Integer;
        valueInt   : Integer;);
      ervtCardinal:(
        minValueCar: Cardinal;
        maxValueCar: Cardinal;
        defaultCar : Cardinal;
        valueCar   : Cardinal;);
      ervtDouble: (
        minValuedoub: double;
        maxValuedoub: double;
        Kommasdoub  : integer;
        defaultdoub : double;
        valuedoub   : double);
      ervtString: (
        maxLengthstring: Integer;);
      ervtBoolean: (
        defaultbool  : boolean;
        valueBool    : Boolean;);
      ervtRecordList: (
        valueList       : TExtRecordList;
        valueListRec    : TExtRecordDefinition;
        valueListEvent  : TEventHandle);
  end;

  TLoadLink = record
    OldValue  : Cardinal;
    NewValue  : Cardinal;
  end;

  TExtRecordDefinition = class
  private
    fValues        : Array of TExtRecordValue;
    fLoadLinks     : Array of TLoadLink;
    fRecordName    : String;
    fRecordHash    : Cardinal;
    fHashMap       : TIDHashMap;
    fParentDesc    : Boolean;

    fNotifyList    : TNotifyList;

    function GetValueProperty(Index: Integer; Value: String): String;

    function AddField(ValueName: String; Typ: TExtRecordValueType; Description: String): Integer;

    procedure DefinitionFromRecordListDestroy(Sender: TObject);

  public
    constructor Create(Name: String);
    destructor Destroy;override;

    procedure AddInteger(ValueName: String;minValue,maxValue: Integer;default: Integer = 0; description: String = '');
    procedure AddCardinal(ValueName: String;minValue,maxValue: Cardinal;default: Cardinal = 0; description: String = '');
    procedure AddDouble(ValueName: String;minValue,maxValue: double;Kommas: Integer;default: double = 0; description: String = '');
    procedure AddString(ValueName: String;maxLength: Integer;default: String = ''; description: String = '');
    procedure AddBoolean(ValueName: String;default: Boolean = false;description: String = '');
    procedure AddBinary(ValueName: String;description: String = '');
    procedure AddRecordList(ValueName: String;RecordDef: TExtRecordDefinition;description: String = '');

    // Hiermit k�nnen Einzelne Eigenschaften umbenannt werden. Beim Laden wird automatisch auf den neuen Namen verlinkt
    procedure AddLoadLink(OldValueName, NewValueName: String);

    function GetValueIndex(Name: String): Integer;

    function ReadRecordsFromXML(XML: string; ObjectTag: string; MainTag: String): TRecordArray;

    procedure LoadFromStream(Stream: TStream);
    procedure SaveToStream(Stream: TStream);

    property RecordName: String read fRecordName;
    property RecordHash: Cardinal read fRecordHash;

    property ParentDescription: Boolean read fParentDesc write fParentDesc;

    property NotifyList : TNotifyList read fNotifyList write fNotifyList;
  end;

  TExtRecord = class
  private
    fValues        : Array of TExtRecordValue;
    fDiscriptor    : TExtRecordDefinition;
    function GetValue(Index: Integer): TExtRecordValue;
    function GetValueCount: Integer;

  public
    constructor Create(Definition: TExtRecordDefinition);
    destructor Destroy;override;

    procedure Assign(Source: TExtRecord);
    function Compare(Source: TExtRecord): boolean;

    property ValueCount: Integer read GetValueCount;
    property Values[Index: Integer]: TExtRecordValue read GetValue;
    function ValueExists(ValueName: String): boolean;
    function GetValueIndex(Name: String): Integer;

    function GetInteger(ValueName: String): Integer;
    function GetCardinal(ValueName: String): Cardinal;
    function GetString(ValueName: String): string;
    function Getdouble(ValueName: String): double;
    function GetBoolean(ValueName: String): Boolean;

    function GetRecordList(ValueName: String): TExtRecordList;

    function GetBinaryLength(ValueName: String): Integer;
    function GetBinary(ValueName: String): Pointer;

    procedure SetInteger(ValueName: String; Value: Integer);
    procedure SetCardinal(ValueName: String; Value: Cardinal);
    procedure SetString(ValueName: String; Value: String);
    procedure Setdouble(ValueName: String; Value: double);
    procedure SetBoolean(ValueName: String; Value: Boolean);

    procedure SetBinary(ValueName: String; Length: Integer; Buffer: Pointer);

    procedure SetDefaultValues;

    function GetValueProperty(Index: Integer; Value: String): String;

    procedure LoadFromStream(Stream: TStream);
    procedure SaveToStream(Stream: TStream);

    procedure DumpData;

    property RecordDefinition: TExtRecordDefinition read fDiscriptor;
  end;

  TExtRecordList  = class
  private
    fList           : TList;
    fDefinition     : TExtRecordDefinition;
    function GetCount: Integer;
    function GetRecord(Index: Integer): TExtRecord;
  public
    constructor Create(Definition: TExtRecordDefinition);
    destructor Destroy;override;

    procedure Clear;

    function Add: TExtRecord;overload;
    procedure Add(Rec: TExtRecord);overload;
    procedure Delete(Index: Integer);

    procedure SaveToStream(Stream: TStream);
    procedure LoadFromStream(Stream: TStream);

    procedure Assign(List: TExtRecordList);

    property Count                : Integer read GetCount;
    property Item[Index: Integer] : TExtRecord read GetRecord;default;
  end;

  EValueError = class(Exception);

implementation

uses KD4Utils, TraceFile;

{ TExtRecordDefinition }

procedure TExtRecordDefinition.AddBinary(ValueName, description: String);
begin
  AddField(ValueName,ervtBinary,Description);
end;

procedure TExtRecordDefinition.AddBoolean(ValueName: String;
  default: Boolean; description: String);
var
  Index: Integer;
begin
  Index:=AddField(ValueName,ervtBoolean,Description);

  fValues[Index].defaultbool:=default;
end;

procedure TExtRecordDefinition.AddCardinal(ValueName: String; minValue,
  maxValue: Cardinal; default: Cardinal; description: String);
var
  Index: Integer;
begin
  Index:=AddField(ValueName,ervtCardinal,Description);

  fValues[Index].minValueCar:=minValue;
  fValues[Index].maxValueCar:=maxValue;
  fValues[Index].defaultCar:=default;
end;

procedure TExtRecordDefinition.AddDouble(ValueName: String; minValue,
  maxValue: double; Kommas: Integer; default: double; description: String);
var
  Index: Integer;
begin
  Index:=AddField(ValueName,ervtDouble,Description);

  fValues[Index].minValuedoub:=minValue;
  fValues[Index].maxValuedoub:=maxValue;
  fValues[Index].Kommasdoub:=Kommas;
  fValues[Index].defaultdoub:=default;
end;

function TExtRecordDefinition.AddField(ValueName: String;
  Typ: TExtRecordValueType; Description: String): Integer;
begin
  setLength(fValues,length(fValues)+1);
  result:=high(fValues);

  fValues[result].ValueName:=ValueName;
  fValues[result].ValueType:=Typ;
  fValues[result].HashValue:=HashString(ValueName);
  fValues[result].Description:=description;

  fHashMap.InsertID(fValues[result].HashValue,result);
end;

procedure TExtRecordDefinition.AddInteger(ValueName: String; minValue,
  maxValue: Integer; default: Integer; description: String);
var
  Index: Integer;
begin
  Index:=AddField(ValueName,ervtInteger,Description);

  fValues[Index].minValueInt:=minValue;
  fValues[Index].maxValueInt:=maxValue;
  fValues[Index].defaultInt:=default;
end;

procedure TExtRecordDefinition.AddLoadLink(OldValueName,
  NewValueName: String);
begin
  Assert(GetValueIndex(NewValueName)<>-1,'Neue Eigenschaft existiert nicht');
  SetLength(fLoadLinks,length(fLoadLinks)+1);

  fLoadLinks[high(fLoadLinks)].OldValue:=HashString(OldValueName);
  fLoadLinks[high(fLoadLinks)].NewValue:=HashString(NewValueName);
end;

procedure TExtRecordDefinition.AddRecordList(ValueName: String;
  RecordDef: TExtRecordDefinition; description: String);
var
  Index: Integer;
begin
  Assert(RecordDef<>nil);

  Index:=AddField(ValueName,ervtRecordList,Description);

  fValues[Index].valueListRec:=RecordDef;
  fValues[Index].valueListEvent:=RecordDef.NotifyList.RegisterEvent(EVENT_RECORDDEFINITION_DESTROY,DefinitionFromRecordListDestroy);
end;                
                                                                    
procedure TExtRecordDefinition.AddString(ValueName: String;
  maxLength: Integer; default: String; description: String);
var
  Index: Integer;
begin
  Index:=AddField(ValueName,ervtString,Description);

  fValues[Index].maxLengthstring:=maxLength;
  fValues[Index].defaultString:=default;
end;

constructor TExtRecordDefinition.Create(Name: String);
begin
  fRecordName:=Name;
  fRecordHash:=HashString(Name);

  fHashMap:=TIDHashMap.Create;

  fNotifyList:=TNotifyList.Create;

  fParentDesc:=false;
end;

procedure TExtRecordDefinition.DefinitionFromRecordListDestroy(
  Sender: TObject);
var
  Dummy: Integer;
begin
  for Dummy:=0 to high(fValues) do
  begin
    if (fValues[Dummy].ValueType=ervtRecordList) and (fValues[Dummy].valueListRec=Sender) then
      fValues[Dummy].valueListRec:=nil;
  end;
end;

destructor TExtRecordDefinition.Destroy;
var
  Dummy: Integer;
begin
  fNotifyList.CallEvents(EVENT_RECORDDEFINITION_DESTROY,Self);
  FreeAndNil(fNotifyList);

  for Dummy:=0 to high(fValues) do
  begin
    if fValues[Dummy].ValueType=ervtRecordList then
    begin
      if fValues[Dummy].valueListRec<>nil then
      begin
        fValues[Dummy].valueListRec.NotifyList.RemoveEvent(fValues[Dummy].valueListEvent);

        if fValues[Dummy].valueListRec.ParentDescription then
          fValues[Dummy].valueListRec.Free;
      end;
    end;
  end;
  fHashMap.Free;
  inherited;
end;

function TExtRecordDefinition.GetValueIndex(Name: String): Integer;
begin
  if not fHashMap.FindKey(HashString(Name),result) then
    result:=-1;
end;

function TExtRecordDefinition.GetValueProperty(Index: Integer; Value: String): String;
var
  List: TStringList;
begin
  with fValues[Index] do
  begin
    List:=TStringList.Create;
    List.Text:=StringReplace(Description,'|',#13#10,[rfReplaceAll]);
    if (Value='Name') and (List.Values['Name']='') then
      result:=ValueName
    else
      result:=List.Values[Value];
    if (Value='Name') and (List.Values['Einheit']<>'') then
      result:=result+' ('+List.Values['Einheit']+')';
    List.Free;
  end;
end;

procedure TExtRecordDefinition.LoadFromStream(Stream: TStream);
var
  Buffer: Cardinal;
  Dummy : Integer;
begin
  Stream.Read(Buffer,SizeOf(Buffer));
  Assert(Buffer=HashString('Definition'+fRecordName));

  fHashMap.ClearList;

  Stream.Read(Buffer,SizeOf(Buffer));

  SetLength(fValues,Buffer);
  for Dummy:=0 to high(fValues) do
  begin
    fValues[Dummy].ValueName:=ReadString(Stream);
    fValues[Dummy].Description:=ReadString(Stream);
    fValues[Dummy].HashValue:=HashString(fValues[Dummy].ValueName);

    fHashMap.InsertID(fValues[Dummy].HashValue,Dummy);

    Stream.Read(fValues[Dummy].ValueType,sizeOf(fValues[Dummy].ValueType));

    case fValues[Dummy].ValueType of
      ervtInteger:
      begin
        Stream.Read(fValues[Dummy].minValueInt,SizeOf(fValues[Dummy].minValueInt));
        Stream.Read(fValues[Dummy].maxValueInt,SizeOf(fValues[Dummy].maxValueInt));
        Stream.Read(fValues[Dummy].defaultInt,SizeOf(fValues[Dummy].defaultInt));
      end;
      ervtCardinal:
      begin
        Stream.Read(fValues[Dummy].minValueCar,SizeOf(fValues[Dummy].minValueCar));
        Stream.Read(fValues[Dummy].maxValueCar,SizeOf(fValues[Dummy].maxValueCar));
        Stream.Read(fValues[Dummy].defaultCar,SizeOf(fValues[Dummy].defaultCar));
      end;
      ervtDouble:
      begin
        Stream.Read(fValues[Dummy].minValuedoub,SizeOf(fValues[Dummy].minValuedoub));
        Stream.Read(fValues[Dummy].maxValuedoub,SizeOf(fValues[Dummy].maxValuedoub));
        Stream.Read(fValues[Dummy].Kommasdoub,SizeOf(fValues[Dummy].Kommasdoub));
        Stream.Read(fValues[Dummy].defaultdoub,SizeOf(fValues[Dummy].defaultdoub));
      end;
      ervtString:
      begin
        Stream.Read(fValues[Dummy].maxLengthstring,SizeOf(fValues[Dummy].maxLengthstring));
        fValues[Dummy].defaultstring:=ReadString(Stream);
      end;
      ervtBoolean:
      begin
        Stream.Read(fValues[Dummy].defaultBool,SizeOf(fValues[Dummy].defaultBool));
      end;
      ervtRecordList:
      begin
        fValues[Dummy].valueListRec:=TExtRecordDefinition.Create(ReadString(Stream));
        fValues[Dummy].valueListRec.LoadFromStream(Stream);
        fValues[Dummy].valueListRec.ParentDescription:=true;
      end;
    end;
  end;
end;

function TExtRecordDefinition.ReadRecordsFromXML(XML,
  ObjectTag: string; MainTag: String): TRecordArray;
var
  Parser: TXMLParser;
  ARoot : TXMLTag;
  root  : TXMLTag;
  Item  : TXMLTag;
  Dummy : Integer;

  function ReadRecord(Item: TXMLTag): TExtRecord;
  var
    Dummy: Integer;
    Node : TXMLTag;
  begin
    result:=TExtRecord.Create(Self);
    for Dummy:=0 to high(fValues) do
    begin
      Node:=Item.Items.Tags[fValues[Dummy].ValueName];
      Assert(Node<>nil,fValues[Dummy].ValueName+' nicht innerhalb von '+ObjectTag+' gefunden');
      case fValues[Dummy].ValueType of
        ervtInteger : Result.SetInteger(fValues[Dummy].ValueName,StrToInt(Node.Value));
        ervtCardinal : Result.SetCardinal(fValues[Dummy].ValueName,StrToInt(Node.Value));
        ervtDouble : Result.SetDouble(fValues[Dummy].ValueName,StrToFloat(Node.Value));
        ervtString : Result.SetString(fValues[Dummy].ValueName,Node.Value);
        ervtBoolean:
          if UpperCase(node.Value)='TRUE' then
            Result.SetBoolean(fValues[Dummy].ValueName,true)
          else
            Result.SetBoolean(fValues[Dummy].ValueName,false);
        else
          raise Exception.Create('Datentyp von '+fValues[Dummy].ValueName+' wird in XML nicht unterst�tzt');
      end;
    end;
  end;

begin
  SetLength(result,0);
  Parser:=TXMLParser.create(nil);
  Aroot:=TXMLTag.create(nil,'root');
  Parser.ParseXMLText(XML,Aroot);
  root:=ARoot.Items.Tags[MainTag];
  Assert(Root<>nil,MainTag+' nicht im XML-Datenstrom');
  for Dummy:=0 to root.Items.Count-1 do
  begin
    if Root.Items[Dummy] is TXMLTag then
    begin
      Item:=TXMLTag(Root.Items[Dummy]);
      Assert(Item.Caption=ObjectTag,ObjectTag+' im XML-Datenstrom erwartet');

      SetLength(result,length(result)+1);
      result[high(result)]:=ReadRecord(Item);
    end;
  end;
  Aroot.Free;
  Parser.Free;
end;

procedure TExtRecordDefinition.SaveToStream(Stream: TStream);
var
  Buffer: Cardinal;
  Dummy : Integer;
begin
  Buffer:=HashString('Definition'+fRecordName);
  Stream.Write(Buffer,SizeOf(Buffer));

  Buffer:=length(fValues);
  Stream.Write(Buffer,SizeOf(Buffer));

  for Dummy:=0 to high(fValues) do
  begin
    WriteString(Stream,fValues[Dummy].ValueName);
    WriteString(Stream,fValues[Dummy].Description);
    Stream.Write(fValues[Dummy].ValueType,sizeOf(fValues[Dummy].ValueType));

    case fValues[Dummy].ValueType of
      ervtInteger:
      begin
        Stream.Write(fValues[Dummy].minValueInt,SizeOf(fValues[Dummy].minValueInt));
        Stream.Write(fValues[Dummy].maxValueInt,SizeOf(fValues[Dummy].maxValueInt));
        Stream.Write(fValues[Dummy].defaultInt,SizeOf(fValues[Dummy].defaultInt));
      end;
      ervtCardinal:
      begin
        Stream.Write(fValues[Dummy].minValueCar,SizeOf(fValues[Dummy].minValueCar));
        Stream.Write(fValues[Dummy].maxValueCar,SizeOf(fValues[Dummy].maxValueCar));
        Stream.Write(fValues[Dummy].defaultCar,SizeOf(fValues[Dummy].defaultCar));
      end;
      ervtDouble:
      begin
        Stream.Write(fValues[Dummy].minValuedoub,SizeOf(fValues[Dummy].minValuedoub));
        Stream.Write(fValues[Dummy].maxValuedoub,SizeOf(fValues[Dummy].maxValuedoub));
        Stream.Write(fValues[Dummy].Kommasdoub,SizeOf(fValues[Dummy].Kommasdoub));
        Stream.Write(fValues[Dummy].defaultdoub,SizeOf(fValues[Dummy].defaultdoub));
      end;
      ervtString:
      begin
        Stream.Write(fValues[Dummy].maxLengthstring,SizeOf(fValues[Dummy].maxLengthstring));
        WriteString(Stream,fValues[Dummy].defaultstring);
      end;
      ervtBoolean:
      begin
        Stream.Write(fValues[Dummy].defaultBool,SizeOf(fValues[Dummy].defaultBool));
      end;
      ervtRecordList:
      begin
        WriteString(Stream,fValues[Dummy].valueListRec.RecordName);
        fValues[Dummy].valueListRec.SaveToStream(Stream);
      end;
    end;
  end;
end;

{ TExtRecord }

procedure TExtRecord.Assign(Source: TExtRecord);
var
  Dummy: Integer;
begin
  if Source.RecordDefinition<>RecordDefinition then
    raise Exception.Create('Inkompatible Typen');

  for Dummy:=0 to GetValueCount-1 do
  begin
    case fValues[Dummy].ValueType of
      ervtRecordList:
        fValues[Dummy].valueList.Assign(Source.GetValue(Dummy).valueList);
      ervtBinary:
        SetBinary(fValues[Dummy].ValueName,Source.GetValue(Dummy).BinaryLength,Source.GetValue(Dummy).binaryAddress);
      else
        fValues[Dummy]:=Source.GetValue(Dummy)
    end
  end;
end;

function TExtRecord.Compare(Source: TExtRecord): boolean;
var
  Dummy   : Integer;
  DumList : Integer;
begin
  Assert(Source.RecordDefinition=RecordDefinition);
  result:=true;
  for Dummy:=0 to high(fValues) do
  begin
    case fValues[Dummy].ValueType of
      ervtInteger  : result:=fValues[Dummy].valueInt=Source.fValues[Dummy].valueInt;
      ervtCardinal : result:=fValues[Dummy].valueCar=Source.fValues[Dummy].valueCar;
      ervtDouble   : result:=fValues[Dummy].valuedoub=Source.fValues[Dummy].valuedoub;
      ervtString   : result:=fValues[Dummy].valueString=Source.fValues[Dummy].valueString;
      ervtBoolean  : result:=fValues[Dummy].valueBool=Source.fValues[Dummy].valueBool;
      ervtBinary   :
      begin
        if fValues[Dummy].binaryLength<>Source.fValues[Dummy].binaryLength then
          result:=false
        else
          result:=CompareMem(fValues[Dummy].binaryAddress,Source.fValues[Dummy].binaryAddress,Source.fValues[Dummy].binaryLength);
      end;
      ervtRecordList :
      begin
        if fValues[Dummy].valueList.Count<>Source.fValues[Dummy].valueList.Count then
        begin
          result:=false;
        end;

        with fValues[Dummy].valueList do
        begin
          for DumList:=0 to Count-1 do
          begin
            if DumList<Source.fValues[Dummy].valueList.Count then
              result:=GetRecord(DumList).Compare(Source.fValues[Dummy].valueList[DumList])
            else
              result:=false;

            if not result then
              exit;
          end;
        end;
      end;
    end;
    if not result then
      exit;
  end;
end;

constructor TExtRecord.Create(Definition: TExtRecordDefinition);
var
  Dummy: Integer;
begin
  fDiscriptor:=Definition;
  Assert(fDiscriptor<>nil);
  SetLength(fValues,length(Definition.fValues));
  for Dummy:=0 to high(fValues) do
  begin
    fValues[Dummy]:=Definition.fValues[Dummy];
    if fValues[Dummy].ValueType=ervtRecordList then
      fValues[Dummy].valueList:=TExtRecordList.Create(fValues[Dummy].valueListRec);
      
    SetDefaultValues;
  end;
end;

destructor TExtRecord.Destroy;
var
  Dummy: Integer;
begin
  for Dummy:=0 to high(fValues) do
  begin
    case fValues[Dummy].ValueType of
      ervtRecordList: fValues[Dummy].valueList.Free;
      ervtBinary    :
        if fValues[Dummy].binaryLength>0 then
          FreeMemory(fValues[Dummy].binaryAddress);
    end;
  end;
  inherited;
end;

procedure TExtRecord.DumpData;
var
  Dummy: Integer;
  Dummy2: Integer;
begin
  for Dummy:=0 to high(fValues) do
  begin
    case fValues[Dummy].ValueType of
      ervtInteger: GlobalFile.Write(fValues[Dummy].ValueName,fValues[Dummy].ValueInt);
      ervtCardinal: GlobalFile.Write(fValues[Dummy].ValueName,fValues[Dummy].ValueCar);
      ervtDouble: GlobalFile.Write(fValues[Dummy].ValueName,fValues[Dummy].Valuedoub);
      ervtString: GlobalFile.Write(fValues[Dummy].ValueName,fValues[Dummy].ValueString);
      ervtBoolean: GlobalFile.Write(fValues[Dummy].ValueName,fValues[Dummy].ValueBool);
      ervtBinary: GlobalFile.HexDump(fValues[Dummy].ValueName,fValues[Dummy].binaryLength,fValues[Dummy].binaryAddress);
      ervtRecordList:
      begin
        with GetRecordList(fValues[Dummy].ValueName) do
        begin
          for Dummy2:=0 to GetCount-1 do
          begin
            GlobalFile.Write('RecordList '+Self.fValues[Dummy].ValueName+' ['+IntToStr(Dummy2)+']');
            GetRecord(Dummy2).DumpData;
          end;
        end;
      end;
    end;
  end;
end;

function TExtRecord.GetBinary(ValueName: String): Pointer;
var
  Index: Integer;
begin
  Index:=GetValueIndex(ValueName);
  if Index=-1 then
    raise EValueError.Create('Eigenschaft exisitiert nicht: '+ValueName);

  if fValues[Index].ValueType<>ervtBinary then
    raise EValueError.Create('Ung�ltiger Datentyp f�r '+ValueName+' (Binary)');

  result:=fValues[Index].binaryAddress;
end;

function TExtRecord.GetBinaryLength(ValueName: String): Integer;
var
  Index: Integer;
begin
  Index:=GetValueIndex(ValueName);
  if Index=-1 then
    raise EValueError.Create('Eigenschaft exisitiert nicht: '+ValueName);

  if fValues[Index].ValueType<>ervtBinary then
    raise EValueError.Create('Ung�ltiger Datentyp f�r '+ValueName+' (Binary)');

  result:=fValues[Index].binaryLength;
end;

function TExtRecord.GetBoolean(ValueName: String): Boolean;
var
  Index: Integer;
begin
  Index:=GetValueIndex(ValueName);
  if Index=-1 then
    raise EValueError.Create('Eigenschaft exisitiert nicht: '+ValueName);

  if fValues[Index].ValueType<>ervtBoolean then
    raise EValueError.Create('Ung�ltiger Datentyp f�r '+ValueName+' (Boolean)');

  result:=fValues[Index].valueBool;
end;

function TExtRecord.GetCardinal(ValueName: String): Cardinal;
var
  Index: Integer;
begin
  Index:=GetValueIndex(ValueName);
  if Index=-1 then
    raise EValueError.Create('Eigenschaft exisitiert nicht: '+ValueName);

  if fValues[Index].ValueType<>ervtCardinal then
    raise EValueError.Create('Ung�ltiger Datentyp f�r '+ValueName+' (Cardinal)');

  result:=fValues[Index].valueCar;
end;

function TExtRecord.Getdouble(ValueName: String): double;
var
  Index: Integer;
begin
  Index:=GetValueIndex(ValueName);
  if Index=-1 then
    raise EValueError.Create('Eigenschaft exisitiert nicht: '+ValueName);

  if fValues[Index].ValueType<>ervtDouble then
    raise EValueError.Create('Ung�ltiger Datentyp f�r '+ValueName+' (double)');
  
  result:=fValues[Index].valuedoub;
end;

function TExtRecord.GetInteger(ValueName: String): Integer;
var
  Index: Integer;
begin
  Index:=GetValueIndex(ValueName);
  if Index=-1 then
    raise EValueError.Create('Eigenschaft exisitiert nicht: '+ValueName);
    
  if fValues[Index].ValueType<>ervtInteger then
    raise EValueError.Create('Ung�ltiger Datentyp f�r '+ValueName+' (Integer)');
  
  result:=fValues[Index].valueInt;
end;

function TExtRecord.GetRecordList(ValueName: String): TExtRecordList;
var
  Index: Integer;
begin
  Index:=GetValueIndex(ValueName);
  if Index=-1 then
    raise EValueError.Create('Eigenschaft exisitiert nicht: '+ValueName);

  if fValues[Index].ValueType<>ervtRecordList then
    raise EValueError.Create('Ung�ltiger Datentyp f�r '+ValueName+' (RecordList)');

  result:=fValues[Index].valueList;
end;

function TExtRecord.GetString(ValueName: String): string;
var
  Index: Integer;
begin
  Index:=GetValueIndex(ValueName);
  if Index=-1 then
    raise EValueError.Create('Eigenschaft exisitiert nicht: '+ValueName);
  
  if fValues[Index].ValueType<>ervtString then
    raise EValueError.Create('Ung�ltiger Datentyp f�r '+ValueName+' (String)');

  result:=fValues[Index].valuestring;
end;

function TExtRecord.GetValue(Index: Integer): TExtRecordValue;
begin
  if (Index>=0) and (Index<GetValueCount) then
    result:=fValues[Index]
  else
    raise EValueError.Create('Ung�ltiger Index: '+IntTostr(Index));
end;

function TExtRecord.GetValueCount: Integer;
begin
  result:=length(fValues);
end;

function TExtRecord.GetValueIndex(Name: String): Integer;
begin
  Assert(fDiscriptor<>nil);
  result:=fDiscriptor.GetValueIndex(Name);
end;

function TExtRecord.GetValueProperty(Index: Integer;
  Value: String): String;
begin
  result:=fDiscriptor.GetValueProperty(Index,Value);
end;

procedure TExtRecord.LoadFromStream(Stream: TStream);
var
  Buffer: Cardinal;
  Count : Integer;
  Index : Integer;
  Len   : Integer;
  tmp   : TMemoryStream;
  Typ   : TExtRecordValueType;

  function FindValue(hash: Cardinal): Integer;
  var
    Dummy: Integer;
  begin
    result:=-1;
    for Dummy:=0 to high(fValues) do
    begin
      if fValues[Dummy].HashValue=hash then
      begin
        result:=Dummy;
        exit;
      end;
    end;

    // Schauen ob ein LoadLink exisistiert

    for Dummy:=0 to high(fDiscriptor.fLoadLinks) do
    begin
      if fDiscriptor.fLoadLinks[Dummy].OldValue = hash then
      begin
        result:=FindValue(fDiscriptor.fLoadLinks[Dummy].NewValue);
        Assert(result<>-1);
        exit;
      end;
    end;
    // Attribut nicht gefunden, �berlesen
    Stream.Read(Typ,SizeOf(Typ));
    Stream.Read(len,SizeOf(Integer));
    Stream.Seek(len,soFromCurrent);
  end;

begin
  SetDefaultValues;

  Stream.Read(Buffer,SizeOf(Buffer));
  if Buffer<>HashString(fDiscriptor.RecordName) then
    raise Exception.Create('Datei ist besch�digt ('+fDiscriptor.RecordName+')');

  Stream.Read(Count,SizeOf(Count));

  while Count>0 do
  begin
    Stream.Read(Buffer,SizeOf(Buffer));
    Index:=FindValue(Buffer);
                                           
    if Index=-1 then
    begin
      dec(Count);
      continue;
    end;
    Stream.Read(Typ,SizeOf(Typ));
    if Typ<>fValues[Index].ValueType then
    begin
      Stream.Read(len,SizeOf(Integer));
      Stream.Seek(len,soFromCurrent);
    end
    else
    begin
      Stream.Read(len,SizeOf(Integer));
      with fValues[Index] do
      begin
        case ValueType of
          ervtInteger :
          begin
            Stream.Read(ValueInt,SizeOf(ValueInt));
            // Pr�fen
            if GetValueProperty(Index,'NoCheck')<>'true' then
            begin
              if ValueInt>MaxValueInt then
                ValueInt:=MaxValueInt;
              if ValueInt<MinValueInt then
                ValueInt:=MinValueInt;
            end;
          end;
          ervtCardinal :
          begin
            Stream.Read(ValueCar,SizeOf(ValueCar));
            // Pr�fen
            if GetValueProperty(Index,'NoCheck')<>'true' then
            begin
              if ValueCar>MaxValueCar then
                ValueCar:=MaxValueCar;
              if ValueCar<MinValueCar then
                ValueCar:=MinValueCar;
            end;
          end;
          ervtDouble :
          begin
            Stream.Read(Valuedoub,SizeOf(Valuedoub));
            // Pr�fen
            if GetValueProperty(Index,'NoCheck')<>'true' then
            begin
              if Valuedoub>MaxValuedoub then
                Valuedoub:=MaxValuedoub;
              if Valuedoub<MinValuedoub then
                Valuedoub:=MinValuedoub;
            end;
          end;
          ervtString :
            ValueString:=ReadString(Stream);
          ervtBoolean :
            Stream.Read(ValueBool,SizeOf(ValueBool));
          ervtBinary  :
          begin
            if Len>0 then
            begin
              binaryAddress:=GetMemory(Len);
              binaryLength:=Len;
              Stream.Read(binaryAddress^,Len);
            end;
	  end;

          ervtRecordList:
          begin
            tmp:=TMemoryStream.Create;
            tmp.CopyFrom(Stream,Len);
            tmp.Position:=0;
            valueList.LoadFromStream(tmp);
            tmp.Free;
          end;
        end;
      end;
    end;
    dec(Count);
  end;
end;

procedure TExtRecord.SaveToStream(Stream: TStream);
var
  Buffer: Cardinal;
  Count : Integer;
  Dummy : Integer;
  Len   : Integer;
  tmp   : TMemoryStream;

  function MustSave(Index: Integer): Boolean;
  begin
    result:=true;
    with fValues[Dummy] do
    begin
      case ValueType of
        ervtInteger     : result:=valueInt<>defaultInt;
        ervtCardinal    : result:=valueCar<>defaultCar;
        ervtDouble      : result:=valuedoub<>defaultDoub;
        ervtString      : result:=valueString<>defaultstring;
        ervtBoolean     : result:=valueBool<>defaultBool;
        ervtBinary      : result:=binaryLength>0;
        ervtRecordList  : result:=valueList.Count>0;
      end;
    end;
  end;

begin
  Buffer:=HashString(fDiscriptor.RecordName);
  Stream.Write(Buffer,SizeOf(Buffer));

  Count:=0;
  for Dummy:=0 to high(fValues) do
  begin
    if MustSave(Dummy) then
      inc(Count);
  end;

  Stream.Write(Count,SizeOf(Count));

  for Dummy:=0 to high(fValues) do
  begin
    if not MustSave(Dummy) then
      continue;

    Buffer:=fValues[Dummy].HashValue;
    Stream.Write(Buffer,SizeOf(Buffer));
    Stream.Write(fValues[Dummy].ValueType,SizeOf(fValues[Dummy].ValueType));
    case fValues[Dummy].ValueType of
      ervtInteger :
      begin
        Len:=SizeOf(Integer);
        Stream.Write(Len,SizeOf(Len));
        Stream.Write(fValues[Dummy].valueInt,sizeOf(fValues[Dummy].ValueInt));
      end;

      ervtCardinal :
      begin
        Len:=SizeOf(Cardinal);
        Stream.Write(Len,SizeOf(Len));
        Stream.Write(fValues[Dummy].valueCar,sizeOf(fValues[Dummy].ValueCar));
      end;

      ervtDouble :
      begin
        Len:=SizeOf(double);
        Stream.Write(Len,SizeOf(Len));
        Stream.Write(fValues[Dummy].valuedoub,sizeOf(fValues[Dummy].Valuedoub));
      end;

      ervtString :
      begin
        Len:=SizeOf(Integer)+length(fValues[Dummy].valueString);
        Stream.Write(Len,SizeOf(Len));
        WriteString(Stream,fValues[Dummy].valueString);
      end;

      ervtBoolean :
      begin
        Len:=SizeOf(Boolean);
        Stream.Write(Len,SizeOf(Len));
        Stream.Write(fValues[Dummy].valueBool,sizeOf(fValues[Dummy].ValueBool));
      end;

      ervtBinary :
      begin
        Stream.Write(fValues[Dummy].BinaryLength,sizeOf(fValues[Dummy].BinaryLength));
        Stream.Write(fValues[Dummy].BinaryAddress^,fValues[Dummy].BinaryLength);
      end;

      ervtRecordList:
      begin
        tmp:=TMemoryStream.Create;
        fValues[Dummy].valueList.SaveToStream(tmp);
        tmp.Position:=0;
        Len:=tmp.Size;
        Stream.Write(Len,SizeOf(Len));

        Stream.CopyFrom(tmp,0);
        tmp.Free;
      end;
    end;
  end;
end;

procedure TExtRecord.SetBinary(ValueName: String; Length: Integer;
  Buffer: Pointer);
var
  Index: Integer;
begin
  Index:=GetValueIndex(ValueName);
  if Index=-1 then
    raise EValueError.Create('Eigenschaft exisitiert nicht: '+ValueName);

  if fValues[Index].ValueType<>ervtBinary then
    raise EValueError.Create('Ung�ltiger Datentyp f�r '+ValueName+' (Binary)');

  with fValues[Index] do
  begin
    if binaryLength<>0 then
    begin
      FreeMemory(binaryAddress);
    end;
    binaryLength:=Length;
    binaryAddress:=GetMemory(Length);
    CopyMemory(binaryAddress,Buffer,Length);
  end;
end;

procedure TExtRecord.SetBoolean(ValueName: String; Value: Boolean);
var
  Index: Integer;
begin
  Index:=GetValueIndex(ValueName);
  if Index=-1 then
    raise EValueError.Create('Eigenschaft exisitiert nicht: '+ValueName);

  if fValues[Index].ValueType<>ervtBoolean then
    raise EValueError.Create('Ung�ltiger Datentyp f�r '+ValueName+' (Boolean)');

  fValues[Index].valueBool:=Value;
end;

procedure TExtRecord.SetCardinal(ValueName: String; Value: Cardinal);
var
  Index: Integer;
begin
  Index:=GetValueIndex(ValueName);
  if Index=-1 then
    raise EValueError.Create('Eigenschaft exisitiert nicht: '+ValueName);

  if fValues[Index].ValueType<>ervtCardinal then
    raise EValueError.Create('Ung�ltiger Datentyp f�r '+ValueName+' (Cardinal)');

  fValues[Index].valueCar:=Value;
end;

procedure TExtRecord.SetDefaultValues;
var
  Dummy: Integer;
begin
  for DUmmy:=0 to high(fValues) do
  begin
    case fValues[Dummy].ValueType of
      ervtInteger  : fValues[Dummy].valueInt:=fValues[Dummy].defaultInt;
      ervtCardinal : fValues[Dummy].valueCar:=fValues[Dummy].defaultCar;
      ervtDouble   : fValues[Dummy].valuedoub:=fValues[Dummy].defaultdoub;
      ervtString   : fValues[Dummy].valueString:=fValues[Dummy].defaultString;
      ervtBoolean  : fValues[Dummy].valueBool:=fValues[Dummy].defaultbool;
      ervtBinary   :
      begin
        FreeMemory(fValues[Dummy].binaryAddress);
        fValues[Dummy].binaryAddress:=nil;
        fValues[Dummy].binaryLength:=0;
      end;

    end;
  end;
end;

procedure TExtRecord.Setdouble(ValueName: String; Value: double);
var
  Index: Integer;
begin
  Index:=GetValueIndex(ValueName);
  if Index=-1 then
    raise EValueError.Create('Eigenschaft exisitiert nicht: '+ValueName);

  if fValues[Index].ValueType<>ervtDouble then
    raise EValueError.Create('Ung�ltiger Datentyp f�r '+ValueName+' (double)');

  fValues[Index].valuedoub:=Value;
end;

procedure TExtRecord.SetInteger(ValueName: String; Value: Integer);
var
  Index: Integer;
begin
  Index:=GetValueIndex(ValueName);
  if Index=-1 then
    raise EValueError.Create('Eigenschaft exisitiert nicht: '+ValueName);

  if fValues[Index].ValueType<>ervtInteger then
    raise EValueError.Create('Ung�ltiger Datentyp f�r '+ValueName+' (Integer)');

  fValues[Index].valueInt:=Value;
end;

procedure TExtRecord.SetString(ValueName, Value: String);
var
  Index: Integer;
begin
  Index:=GetValueIndex(ValueName);
  if Index=-1 then
    raise EValueError.Create('Eigenschaft exisitiert nicht: '+ValueName);

  if fValues[Index].ValueType<>ervtString then
    raise EValueError.Create('Ung�ltiger Datentyp f�r '+ValueName+' (String)');

  fValues[Index].valuestring:=Value;
end;

function TExtRecord.ValueExists(ValueName: String): boolean;
var
  Dummy: Integer;
begin
  result:=false;
  for Dummy:=0 to high(fValues) do
  begin
    if fValues[Dummy].ValueName=ValueName then
    begin
      result:=true;
      exit;
    end;
  end;
end;

{ TExtRecordList }

function TExtRecordList.Add: TExtRecord;
begin
  result:=TExtRecord.Create(fDefinition);
  fList.Add(Result);
end;

procedure TExtRecordList.Add(Rec: TExtRecord);
begin
  Assert(Rec.RecordDefinition=fDefinition);
  fList.Add(Rec);
end;

procedure TExtRecordList.Assign(List: TExtRecordList);
var
  Dummy: Integer;
begin
  Clear;
  for Dummy:=0 to List.Count-1 do
    Add.Assign(List[Dummy]);
end;

procedure TExtRecordList.Clear;
var
  Dummy: Integer;
begin
  for DUmmy:=0 to fList.Count-1 do
  begin
    TObject(fList[Dummy]).Free;
  end;
  fList.Clear;
end;

constructor TExtRecordList.Create(Definition: TExtRecordDefinition);
begin
  fDefinition:=Definition;
  Assert(fDefinition<>nil);
  fList:=TList.Create;
end;

procedure TExtRecordList.Delete(Index: Integer);
begin
  TObject(fList[Index]).Free;
  fList.Delete(Index);
end;

destructor TExtRecordList.Destroy;
begin
  Clear;
  fList.Free;
  inherited;
end;

function TExtRecordList.GetCount: Integer;
begin
  result:=fList.Count;
end;
                                        
function TExtRecordList.GetRecord(Index: Integer): TExtRecord;
begin
  result:=TExtRecord(fList[Index]);
end;

procedure TExtRecordList.LoadFromStream(Stream: TStream);
var
  Buffer: Integer;
begin
  Clear;
  Stream.Read(Buffer,SizeOf(Integer));
  while (Buffer>0) do
  begin
    Add.LoadFromStream(Stream);
    dec(Buffer);
  end;
end;

procedure TExtRecordList.SaveToStream(Stream: TStream);
var
  Dummy: Integer;
  Buffer: Integer;
begin
  Buffer:=fList.Count;
  Stream.Write(Buffer,SizeOf(Buffer));
  for Dummy:=0 to fList.Count-1 do
    TExtRecord(fList[Dummy]).SaveToStream(Stream);
end;

end.
