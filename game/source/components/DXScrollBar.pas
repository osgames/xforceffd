{********************************************************************************
*										*
* Copyright (C) 2004  X-Force Team - jim_raynor@web.de				*
*										*
* This file is part of X-Force: Fight For Destiny (X-Force).			*
*										*
* X-Force is free software; you can redistribute it and/or modify		*
* it under the terms of the GNU General Public License as published by		*
* the Free Software Foundation; either version 2 of the License, or		*
* (at your option) any later version.						*
*										*
* X-Force is distributed in the hope that it will be useful,			*
* but WITHOUT ANY WARRANTY; without even the implied warranty of		*
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the			*
* GNU General Public License for more details.					*
*										*
* You should have received a copy of the GNU General Public License		*
* along with X-Force; if not, write to the Free Software			*
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA	*
*										*
*********************************************************************************
*										*
* Diese Unit stellt eine Scrollbar zur Verf�gung				*
*										*
********************************************************************************}

{$I ../Settings.inc}

unit DXScrollBar;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  DXDraws, DXContainer, extctrls, NGTypes, Defines, math, BLending, TraceFile,
  XForce_types, DirectDraw;

type
  TDXScrollBar = class(TDXComponent)
  private
    fKind              : TScrollBarKind;
    fMin               : Integer;
    fValue             : Integer;
    fInterval          : Integer;
    fMax               : Integer;
    fOnChange          : TNotifyEvent;
    fLargeChange       : Integer;
    fSmallChange       : Integer;
    fSecondColor       : TColor;
    fFirstColor        : TBlendColor;
    fThumbHeight       : Integer;
    fOverState         : TButtonState;
    fDownState         : TButtonState;
    fListScroll        : Integer;
    fCorners           : TRoundCorners;
    fCorner            : TCorners;
    fOwner             : TDXComponent;
    fFocusColor        : TColor;
    procedure SetKind(const Value: TScrollBarKind);
    procedure SetMax(const Value: Integer);
    procedure SetMin(const Value: Integer);
    procedure SetValue(const Value: Integer);
    procedure SetLargeChange(const Value: Integer);
    procedure SetSmallChange(const Value: Integer);
    procedure SetFirstColor(const Value: TBlendColor);
    procedure SetSecondColor(const Value: TColor);
    procedure SetCorners(const Value: TRoundCorners);
    function GetFrameRect: TRect;
    function GetColor: TColor;
    procedure SetFocusColor(const Value: TColor);
    { Private-Deklarationen }
  protected
    procedure CalcThumbHeight;
    function ScrollTimer(Sender: TObject;Frames: Integer) :boolean;
    procedure DoScroll(Direction: TScrollDirection; Pos: TPoint);override;
    procedure CreateMouseRegion;override;
    { Protected-Deklarationen }
  public
    constructor Create(Page: TDXPage);override;
    procedure Resize(var NewLeft, NewTop, NewWidth, NewHeight: Integer);override;
    procedure Draw(Surface: TDirectDrawSurface;var Mem: TDDSurfaceDesc);override;
    procedure MouseMove(X,Y: Integer);override;
    procedure MouseDown(Button: TMouseButton;X,Y: Integer);override;
    procedure MouseUp(Button: TMouseButton;X,Y: Integer);override;
    procedure MouseLeave;override;
    procedure VisibleChange;override;
    property Kind          : TScrollBarKind read fKind write SetKind default sbVertical;
    property OnChange      : TNotifyEvent read fOnChange write fOnChange;
    property Min           : Integer read fMin write SetMin;
    property Max           : Integer read fMax write SetMax;
    property Value         : Integer read fValue write SetValue;
    property SmallChange   : Integer read fSmallChange write SetSmallChange;
    property LargeChange   : Integer read fLargeChange write SetLargeChange;
    property Interval      : Integer read fInterval write fInterval;
    property FirstColor    : TBlendColor read fFirstColor write SetFirstColor;
    property FocusColor    : TColor write SetFocusColor;
    property SecondColor   : TColor write SetSecondColor;
    property RoundCorners  : TRoundCorners read fCorners write SetCorners;
    property Owner         : TDXComponent read fOwner write fOwner;
    { Published-Deklarationen }
  end;

implementation

{ TDXScrollBar }

procedure TDXScrollBar.CalcThumbHeight;
var
  ran: Integer;
  hei: Integer;
begin
  ran:=fmax-fmin+fLargeChange;
  if fKind=sbVertical then
    hei:=Height-34 else
    hei:=Width-34;
  if fLargeChange=0 then fThumbHeight:=0 else
  begin
    if (ran/fLargeChange)=0 then fThumbHeight:=0 else
    begin
      fThumbHeight:=math.max(round(hei/(ran/fLargeChange)),6);
    end;
  end;
end;

constructor TDXScrollBar.Create(Page: TDXPage);
begin
  inherited;
  fKind:=sbVertical;
  fMin:=0;
  fListScroll:=-1;
  fFirstColor:=bcBlack;
  fLargeChange:=10;
  fSmallChange:=1;
  fOverState:=bsNone;
  fDownState:=bsNone;
  fValue:=0;
  fInterval:=50;
  fMax:=100;
  RoundCorners:=rcAll;
  fFocusColor:=clRed;
  fOwner:=nil;
end;

procedure TDXScrollBar.CreateMouseRegion;
var
  FrameRect: TRect;
  RoundRGN : HRGN;
begin
  FrameRect:=GetFrameRect;
  RoundRGN:=CreateRoundRectRgn(FrameRect.Left,FrameRect.Top,FrameRect.Right+1,FrameRect.Bottom+1,20,20);
  fMouseRegion:=CreateRectRgn(Left,Top,Right,Bottom);
  CombineRgn(fMouseRegion,fMouseRegion,RoundRGN,RGN_AND);
  DeleteObject(RoundRGN);
end;

procedure TDXScrollBar.DoScroll(Direction: TScrollDirection; Pos: TPoint);
begin
  if fKind=sbHorizontal then
  begin
    if Direction=sdLeft then Value:=Value-SmallChange*2;
    if Direction=sdRight then Value:=Value+SmallChange*2;
  end
  else
  begin
    if Direction=sdUp then Value:=Value-SmallChange*2;
    if Direction=sdDown then Value:=Value+SmallChange*2;
  end;
end;

procedure TDXScrollBar.Draw(Surface: TDirectDrawSurface;var Mem: TDDSurfaceDesc);
var
  ThumbX,ThumbY : Integer;
  Alpha         : Integer;
  Color         : TBlendColor;
begin
  Color:=GetColor;
  if fOverState=bsLeft then Alpha:=125 else Alpha:=75;
  BlendRoundRect(ClientRect,Alpha,fFirstColor,Surface,Mem,11,fCorner,Rect(Left,Top,Left+16,Top+16));
  if fKind=sbHorizontal then
  begin
    if fOverState=bsRight then Alpha:=125 else Alpha:=75;
    BlendRoundRect(ClientRect,Alpha,fFirstColor,Surface,Mem,11,fCorner,Rect(Right-15,Top+1,Right-1,Top+17));
    if fMax-fMin=0 then ThumbX:=-20 else
    ThumbX:=Left+16+round(((fValue-fMin)/(fMax-fMin))*(Width-32-fThumbHeight));
    ThumbY:=Top;
    if AlphaElements then
      BlendRectangle(Rect(Thumbx,ThumbY,Thumbx+fThumbHeight,Thumby+16),150,fFirstColor,Surface)
    else
      Surface.FillRect(Rect(ThumbX+1,ThumbY+1,ThumbX+fThumbHeight,ThumbY+16),fFirstColor);
    if ThumbX>Left+16 then
      VLine(Surface,Mem,Top+1,Bottom-1,ThumbX,Color);
    if ThumbX+fThumbHeight<Right-16 then
      VLine(Surface,Mem,Top+1,Bottom-1,ThumbX+fThumbHeight,Color);
    VLine(Surface,Mem,Top+1,Bottom-1,Left+16,Color);
    VLine(Surface,Mem,Top+1,Bottom-1,Right-16,Color);
  end
  else
  begin
    if fOverState=bsRight then Alpha:=125 else Alpha:=75;
    BlendRoundRect(ClientRect,Alpha,fFirstColor,Surface,Mem,11,fCorner,Rect(Left+1,Bottom-15,Left+17,Bottom+1));
    if fMax-fMin=0 then ThumbY:=-20 else
    ThumbY:=Top+16+round(((fValue-fMin)/(fMax-fMin))*(Height-32-fThumbHeight));
    ThumbX:=Left;
    if AlphaElements then
      BlendRectangle(Rect(Thumbx,ThumbY,Thumbx+16,Thumby+fThumbHeight),150,fFirstColor,Surface)
    else
      Surface.FillRect(Rect(ThumbX+1,ThumbY+1,ThumbX+16,ThumbY+fThumbHeight),fFirstColor);
    if ThumbY>Top+16 then
      HLine(Surface,Mem,Left+1,Right-1,ThumbY,Color);
    if ThumbY+fThumbHeight<Bottom-16 then
      HLine(Surface,Mem,Left+1,Right-1,ThumbY+fThumbHeight,Color);
    HLine(Surface,Mem,Left+1,Right-1,Top+16,Color);
    HLine(Surface,Mem,Left+1,Right-1,Bottom-16,Color);
  end;
  FramingRect(Surface,Mem,ClientRect,fCorner,11,Color);
  if fKind=sbHorizontal then
  begin
    if fDownState=bsLeft then
      Container.ImageList.Items[0].Draw(Surface,Left+3,Top+2,2)
    else
      Container.ImageList.Items[0].Draw(Surface,Left+4,Top+2,2);
    if fDownState=bsRight then
      Container.ImageList.Items[0].Draw(Surface,Right-15,Top+2,0)
    else
      Container.ImageList.Items[0].Draw(Surface,Right-16,Top+2,0);
  end
  else
  begin
    if fDownState=bsLeft then
      Container.ImageList.Items[0].Draw(Surface,Left+2,Top+2,1)
    else
      Container.ImageList.Items[0].Draw(Surface,Left+2,Top+3,1);
    if fDownState=bsRight then
      Container.ImageList.Items[0].Draw(Surface,Left+2,Bottom-14,3)
    else
      Container.ImageList.Items[0].Draw(Surface,Left+2,Bottom-15,3);
  end;
end;

function TDXScrollBar.GetColor: TColor;
begin
  if (fOwner<>nil) and (fOwner.HasFocus) then result:=fFocusColor else result:=fSecondColor;
end;

function TDXScrollBar.GetFrameRect: TRect;
begin
  result:=ClientRect;
  if fCorners in [rcLeft,rcLeftTop,rcLeftBottom,rcNone] then inc(result.Right,20);
  if fCorners in [rcTop,rcLeftTop,rcRightTop,rcNone] then inc(result.Bottom,20);
  if fCorners in [rcRight,rcRightTop,rcRightBottom,rcNone] then dec(result.Left,20);
  if fCorners in [rcBottom,rcLeftBottom,rcRightBottom,rcNone] then dec(result.Top,20);
end;


procedure TDXScrollBar.MouseDown(Button: TMouseButton; X, Y: Integer);
var
  yPos: Integer;
  From: Integer;
  Bottom,Top: Integer;
begin
  inherited;
  if Assigned(fOwner) and (fOwner.Tabstop) then Container.FocusControl:=fOwner;
  if not (Button=mbLeft) then exit;
  if fKind=sbHorizontal then
  begin
    From:=Width;
    yPos:=X;
  end
  else
  begin
    yPos:=Y;
    From:=Height;
  end;
  if yPos>From-18 then
  begin
    if not (fDownState=bsRight) then
    begin
      Container.PlaySound(SClick);
      fDownState:=bsRight;
      Value:=Value+fSmallChange;
      Container.AddFrameFunction(ScrollTimer,nil,fInterval);
    end;
    exit;
  end
  else if yPos<17 then
  begin
    if not (fDownState=bsLeft) then
    begin
      Container.PlaySound(SClick);
      fDownState:=bsLeft;
      Value:=Value-fSmallChange;
      Container.AddFrameFunction(ScrollTimer,nil,fInterval);
    end;
    exit;
  end;
  try
    Top:=16+round(((fValue-fMin)/(fMax-fMin))*(From-32-fThumbHeight))
  except
    Top:=-1000;
  end;
  Bottom:=Top+fThumbHeight-1;
  if yPos<Top then
  begin
    Container.PlaySound(SClick);
    Value:=Value-fLargeChange;
    Container.DeleteFrameFunction(ScrollTimer,nil);
  end
  else if yPos>Bottom then
  begin
    Container.PlaySound(SClick);
    Value:=Value+fLargeChange;
    Container.DeleteFrameFunction(ScrollTimer,nil);
  end
  else
  begin
    fListScroll:=YPos-Top;
  end;
end;

procedure TDXScrollBar.MouseLeave;
begin
  inherited;
  fOverState:=bsNone;
  fDownState:=bsNone;
  fListScroll:=-1;
  Container.DeleteFrameFunction(ScrollTimer,nil);
  Redraw;
end;

procedure TDXScrollBar.MouseMove(X, Y: Integer);
var
  RightRect: TRect;
  yPos: Integer;
  From: Integer;
begin
  inherited;
  if fListScroll=-1 then
  begin
    if fKind=sbHorizontal then  RightRect:=Rect(Width-17,0,Width,17)
    else RightRect:=Rect(0,Height-17,17,Height);
    if PtInRect(Rect(0,0,17,17),Point(x,y)) then
    begin
      if not (fOverState=bsLeft) then
      begin
        if (GetKeyState(VK_LBUTTON)<0) then
        begin
          if fDownState=bsLeft then exit;
          Container.PlaySound(SClick);
          fDownState:=bsLeft;
          fOverState:=bsLeft;
          Value:=Value-fSmallChange;
          Container.AddFrameFunction(ScrollTimer,nil,fInterval);
        end
        else
        begin
          if fOverState=bsLeft then exit;
          Container.PlaySound(SOver);
          fOverState:=bsLeft;
          Redraw;
        end;
      end;
    end
    else if PtInRect(RightRect,Point(x,y)) then
    begin
      if not (fOverState=bsRight) then
      begin
        if (GetKeyState(VK_LBUTTON)<0) then
        begin
          if fDownState=bsRight then exit;
          Container.PlaySound(SClick);
          fDownState:=bsRight;
          fOverState:=bsRight;
          Value:=Value+fSmallChange;
          Container.AddFrameFunction(ScrollTimer,nil,Interval);
        end
        else
        begin
          if fOverState=bsRight then exit;
          Container.PlaySound(SOver);
          fOverState:=bsRight;
          Redraw;
        end;
      end;
    end
    else
    begin
      if (fOverState=bsNone) and (fDownState=bsNone) then exit;
      fOverState:=bsNone;
      fDownState:=bsNone;
      Container.DeleteFrameFunction(ScrollTimer,nil);
      Redraw;
    end;
  end
  else
  begin
    if fKind=sbHorizontal then
    begin
      yPos:=x;
      From:=Width;
    end
    else
    begin
      yPos:=y;
      From:=Height;
    end;
    if yPos<=16 then
    begin
      Value:=fMin;
      exit;
    end
    else if yPos>From-16 then
    begin
      Value:=fMax;
      exit;
    end;
    yPos:=yPos-fListScroll;
    try
      Value:=fmin+floor((yPos-16)/((From-31-fThumbHeight)/(fMax-fMin)))+1;
    except
    end;
  end;
end;

procedure TDXScrollBar.MouseUp(Button: TMouseButton; X, Y: Integer);
begin
  inherited;
  if not (Button=mbLeft) then exit;
  fDownState:=bsNone;
  Container.DeleteFrameFunction(ScrollTimer,nil);
  fListScroll:=-1;
  Redraw;
end;

procedure TDXScrollBar.Resize(var NewLeft, NewTop, NewWidth, NewHeight: Integer);
begin
  inherited;
  if fKind=sbHorizontal then NewHeight:=17 else NewWidth:=17;
  CalcThumbHeight;
end;

function TDXScrollBar.ScrollTimer(Sender: TObject;Frames: Integer): boolean;
var
  OldValue: Integer;
begin
  OldValue:=Value;
  result:=false;
  if fDownState=bsLeft then Value:=Value-(fSmallChange*Frames) else Value:=Value+(fSmallChange*Frames);
  if OldValue<>Value then result:=true;
end;

procedure TDXScrollBar.SetCorners(const Value: TRoundCorners);
begin
  fCorners := Value;
  fCorner:=[];
  if fCorners in [rcLeft,rcTop,rcLeftTop,rcAll] then include(fCorner,cLeftTop);
  if fCorners in [rcRight,rcTop,rcRightTop,rcAll] then include(fCorner,cRightTop);
  if fCorners in [rcLeft,rcBottom,rcLeftBottom,rcAll] then include(fCorner,cLeftBottom);
  if fCorners in [rcRight,rcBottom,rcRightBottom,rcAll] then include(fCorner,cRightBottom);
  RecreateMouseRegion;
  Redraw;
end;

procedure TDXScrollBar.SetFirstColor(const Value: TBlendColor);
begin
  fFirstColor := Value;
  Redraw;
end;

procedure TDXScrollBar.SetFocusColor(const Value: TColor);
begin
  fFocusColor := Container.Surface.ColorMatch(Value);
  Redraw;
end;

procedure TDXScrollBar.SetKind(const Value: TScrollBarKind);
begin
  fKind := Value;
  if fKind=sbHorizontal then Height:=18 else Width:=18;
end;

procedure TDXScrollBar.SetLargeChange(const Value: Integer);
begin
  fLargeChange := Value;
  CalcThumbHeight;
end;

procedure TDXScrollBar.SetMax(const Value: Integer);
begin
  fMax := Value;
  if Self.Value>fMax then Self.Value:=fMax;
  if fMin>fMax then fMax:=fMin;
  CalcThumbHeight;
end;

procedure TDXScrollBar.SetMin(const Value: Integer);
begin
  fMin := Value;
  if Self.Value<fMin then Self.Value:=fMin;
  if fMin>fMax then fMin:=fMax;
  CalcThumbHeight;
  Redraw;
end;

procedure TDXScrollBar.SetSecondColor(const Value: TColor);
begin
  fSecondColor := Container.Surface.ColorMatch(Value);
  Redraw;
end;

procedure TDXScrollBar.SetSmallChange(const Value: Integer);
begin
  fSmallChange := Value;
end;

procedure TDXScrollBar.SetValue(const Value: Integer);
var
  OldValue: Integer;
begin
  OldValue:=fValue;
  fValue := Value;
  if fValue<fMin then
  begin
    fValue:=fMin;
  end;
  if fValue>fMax then
  begin
    fValue:=fMax;
  end;
  if fValue=OldValue then exit;
  Container.IncLock;
  if Assigned(fOnChange) then fOnChange(Self);
  Container.DecLock;
  Redraw;
end;

procedure TDXScrollBar.VisibleChange;
begin
  Redraw;
end;

end.
