{********************************************************************************
*										*
* Copyright (C) 2004  X-Force Team - jim_raynor@web.de				*
*										*
* This file is part of X-Force: Fight For Destiny (X-Force).			*
*										*
* X-Force is free software; you can redistribute it and/or modify		*
* it under the terms of the GNU General Public License as published by		*
* the Free Software Foundation; either version 2 of the License, or		*
* (at your option) any later version.						*
*										*
* X-Force is distributed in the hope that it will be useful,			*
* but WITHOUT ANY WARRANTY; without even the implied warranty of		*
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the			*
* GNU General Public License for more details.					*
*										*
* You should have received a copy of the GNU General Public License		*
* along with X-Force; if not, write to the Free Software			*
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA	*
*										*
*********************************************************************************
*										*
* ModalDialog stellt eine Basisklasse zur Verfügung, um Dialogfenster (z.B.	*
* Nachrichtenfenster oder Arbeitsmarktüberwachung) zu erstellen.		*
* Als Beispiele sollte sich WatchMarktDialog.pas angesehen werden		*
*										*
********************************************************************************}

{$I ../Settings.inc}

unit ModalDialog;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  DXContainer, contnrs, math, Defines;

type
  TModalDialog = class(TObject)
  private
    Container    : TDXContainer;
    fList        : TObjectList;
    fLeft        : Integer;
    fTop         : Integer;
    fRect        : TRect;
    fShow        : boolean;
    fFont        : TFont;
    procedure SetFont(const Value: TFont);
    { Private-Deklarationen }
  protected
    fResult      : TModalResult;
    fPage        : TDXPage;
    procedure AddComponent(Component: TDXComponent);
    procedure DeleteComponent(Component: TDXComponent);
    procedure ReadyShow;
    procedure CalculatePositions;
    procedure ResetPositions;
    procedure BeforeShow;virtual;
    { Protected-Deklarationen }
  public
    constructor Create(Page: TDXPage);virtual;
    destructor Destroy;override;
    function ShowModal: TModalResult;virtual;
    property Font: TFont read fFont write SetFont; 
    { Public-Deklarationen }
  end;


implementation

{ TModalDialog }

procedure TModalDialog.AddComponent(Component: TDXComponent);
begin
  if fList.IndexOf(Component)<>-1 then
    exit;

  fList.Add(Component);
  fPage.RemoveComponent(Component);
end;

procedure TModalDialog.BeforeShow;
begin
  fResult:=mrCancel;
end;

procedure TModalDialog.CalculatePositions;
var
  MaxRight: Integer;
  MaxBottom  : Integer;
  Dummy   : Integer;
begin
  MaxRight:=0;
  MaxBottom:=0;
  for Dummy:=0 to fList.Count-1 do
  begin
    MaxRight:=max(MaxRight,TDXComponent(fList[Dummy]).Right);
    MaxBottom:=max(MaxBottom,TDXComponent(fList[Dummy]).Bottom);
  end;
  fLeft:=(ScreenWidth div 2)-MaxRight div 2;
  fTop:=(ScreenHeight div 2)-MaxBottom div 2;
  for Dummy:=0 to fList.Count-1 do
  begin
    TDXComponent(fList[Dummy]).Left:=TDXComponent(fList[Dummy]).Left+fLeft;
    TDXComponent(fList[Dummy]).Top:=TDXComponent(fList[Dummy]).Top+fTop;
  end;
  fRect:=Bounds(fLeft,fTop,MaxRight,MaxBottom);
end;

constructor TModalDialog.Create(Page: TDXPage);
begin
  fList:=TObjectList.Create;
  fList.OwnsObjects:=true;
  fPage:=Page;
  Container:=Page.Container;
  fShow:=false;
  fFont:=TFont.Create;
end;

procedure TModalDialog.DeleteComponent(Component: TDXComponent);
begin
  fList.Extract(Component);
end;

destructor TModalDialog.Destroy;
begin
  fList.Clear;
  fList.Free;
  fFont.Free;
  inherited;
end;

procedure TModalDialog.ReadyShow;
begin
  fShow:=false;
end;

procedure TModalDialog.ResetPositions;
var
  Dummy: Integer;
begin
  for Dummy:=0 to fList.Count-1 do
  begin
    TDXComponent(fList[Dummy]).Left:=TDXComponent(fList[Dummy]).Left-fLeft;
    TDXComponent(fList[Dummy]).Top:=TDXComponent(fList[Dummy]).Top-fTop;
  end;
  fLeft:=0;
  fTop:=0;
end;

procedure TModalDialog.SetFont(const Value: TFont);
begin
  fFont.Assign(Value);
end;

function TModalDialog.ShowModal: TModalResult;
var
  Dummy        : Integer;
  FocusTemp    : TDXComponent;
begin
  if (not Container.CanMessage) then
    exit;
  Container.MessageLock(true);
  Container.IncLock;
  Container.DeactiveAll;
  Container.LoadGame(true);
  CalculatePositions;
  FocusTemp:=Container.FocusControl;
  Container.FocusControl:=nil;
  for Dummy:=0 to fList.Count-1 do
  begin
    Container.RemoveComponent(TDXComponent(fList[Dummy]));
    Container.AddComponent(TDXComponent(fList[Dummy]));
    with TDXComponent(fList[Dummy]) do
    begin
      Visible:=true;
      if TabStop then
        Container.SetFocusControl(TDXComponent(fList[Dummy]));
      Font:=Self.Font;
    end;
  end;
  Container.ReleaseCapture;
  BeforeShow;
  Container.LoadGame(false);
  Container.DecLock;

  Container.UnlockAll;

  Container.RedrawArea(fRect,Container.Surface);
  fShow:=true;;
  repeat
    Application.HandleMessage;
  until not fShow;

  Container.LockAll;

  Container.incLock;
  Container.LoadGame(true);
  for Dummy:=0 to fList.Count-1 do
  begin
    TDXComponent(fList[Dummy]).Visible:=false;
    Container.RemoveComponent(TDXComponent(fList[Dummy]));
  end;
  Container.FocusControl:=FocusTemp;
  Container.ReactiveAll;
  Container.MessageLock(false);
  Container.DecLock;
  Container.LoadGame(false);
  Container.RedrawArea(fRect,Container.Surface);

  ResetPositions;

  result:=fResult;
end;

end.
