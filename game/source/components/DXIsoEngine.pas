{********************************************************************************
*										*
* Copyright (C) 2004  X-Force Team - jim_raynor@web.de				*
*										*
* This file is part of X-Force: Fight For Destiny (X-Force).			*
*										*
* X-Force is free software; you can redistribute it and/or modify		*
* it under the terms of the GNU General Public License as published by		*
* the Free Software Foundation; either version 2 of the License, or		*
* (at your option) any later version.						*
*										*
* X-Force is distributed in the hope that it will be useful,	                *
* but WITHOUT ANY WARRANTY; without even the implied warranty of		*
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the			*
* GNU General Public License for more details.					*
*										*
* You should have received a copy of the GNU General Public License		*
* along with X-Force; if not, write to the Free Software			*
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA	*
*										*
*********************************************************************************
*										*
* Komplette Engine f�r den Bodeneinsatz						*
*										*
********************************************************************************}

{$I ..\settings.inc}

unit DXIsoEngine;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  DXDraws, DXContainer, XForce_types, DXInput, DXClass, math,TraceFile, KD4Utils,
  ArchivFile,Koding, Blending, DirectDraw, ISOTools, GameFigureManager, NGtypes,
  DirectFont, PathFinder, GameFigure, ISOMessages, StringConst, Defines,
  EinsatzKI, typinfo, AGFUnit, AGFImageList, map_utils, NotifyList, TileGroup,
  MapGenerator, IDHashMap, ExtRecord, DXISOTileGroup, XANBitmap;

const
  ModelsOffSet     = 2;
  NeutralRectF     : TRect = (Left:  0; Top:  0; Right: 64; Bottom: 110);
  NeutralRectB     : TRect = (Left: 64; Top:  0; Right:128; Bottom: 110);
  SolRectF         : TRect = (Left:128; Top:  0; Right:192; Bottom: 110);
  SolRectB         : TRect = (Left:192; Top:  0; Right:256; Bottom: 110);
  EnemyRectF       : TRect = (Left:256; Top:  0; Right:320; Bottom: 110);
  EnemyRectB       : TRect = (Left:320; Top:  0; Right:384; Bottom: 110);
  BlankFieldRect   : TRect = (Left:402; Top: 32; Right:466; Bottom:  64);
  GridFieldRect    : TRect = (Left:402; Top: 64; Right:466; Bottom:  96);

  // EVENTS
  EVENT_ONSTARTGAME = 0;
  EVENT_ONENDGAME   = 1;

type
  { Statistik }
  TStatistik        = record
    Anzahl          : Integer;
    Punkte          : Integer;
  end;

  TEinsatzStatistik    = record
    EinsatzErgebnis    : TFightResult;
    Alienabgeschossen  : TStatistik;
    SolVerloren        : TStatistik;
    AusErbeutet        : TStatistik;
    AusVerloren        : TStatistik;
    ohneVerlustBonus   : TStatistik;
    TrefferBilanz      : TStatistik;
    Schuesse           : Integer;
    Treffer            : Integer;
    Contributation     : Integer;
  end;

  { ISO - Engine }

  TISOMap      = class;
  TISOObject   = class;

  TInfoText = record
    ID      : Integer;
    Draw    : Boolean;
    Text    : String;
    Rect    : TRect;
    Objekt  : TISOObject;
  end;

  TMiniMapInfoText = record
    ID      : Integer;
    Draw    : Boolean;
    Text    : String;
    Rect    : TRect;
    Figure  : TGameFigure;
  end;


  TRoundState = record
    Round     : Integer;
    ZugSeite  : TZugSeite;
  end;

  TCheckTile        = function(X,Y: Integer): boolean of object;
  TChangeFigure     = procedure(Figure: TGameFigure) of object;
  TRedrawInterface  = procedure(UIObject: TUIInterface) of object;

  PTileState = ^TTileState;
  TTileState = record
    Figure       : TGameFigure;
    ObjectCount  : Integer;
    Objects      : Array of TISOObject;
    WayPoint     : boolean;
    ToOffSet     : TPoint;
    FromOffSet   : TPoint;
  end;

  TUnitDrawingRect = record
    Figure  : TGameFigure;
    Rect    : TRect;
    beginX  : Integer;
    beginY  : Integer;
  end;

  TUnitDrawRectArray = Array of TUnitDrawingRect;

  TModelSurface = (msNormal, msDead);

  {/$DEFINE SHOWMOUSEPOS}
  TDXIsoEngine = class(TDXComponent)
  private
    fTemp            : TDirectDrawSurface;
    fISOMap          : TISOMap;
    fPlayGame        : boolean;
    fInput           : TDXInput;
    fXSTempo         : Integer;
    fYSTempo         : Integer;
    fSList           : TList;
    fXANList         : TList;
    fObjectList      : TList;
    fItemList        : TList;
    fMovingList      : TList;
    fFigures         : TGameFigureList;
    fGroups          : Array[0..9] of TFigureGroup;
    fMousePos        : TPoint;
    fNeedRedraw      : boolean;
    fEchtZeit        : boolean;
    fTerrainChanged  : Boolean;
    fOverFigure      : TGameFigure;
    fPause           : boolean;
    fInfoTexts       : Array of TInfoText;
    fShowAll         : Boolean;
    fRedrawUI        : TRedrawInterface;
    fOnTileClick     : TTileEvent;
    fStatistik       : TEinsatzStatistik;

    fMouseHint       : TISOObject;

    fShowMessage     : String;
    fShowTime        : Integer;
    fShowWidth       : Integer;

    fFormationList   : TList;
    fRedrawRect      : TRect;
    fAktuSurface     : boolean;
    fCompleteRedraw  : boolean;

    fEnemyKI         : TGroupKI;
    fOwnKI           : TGroupKI;
    fSperre          : Integer;
    fCameraUnit      : TGameFigure;
    fOnOptions       : TNotifyEvent;
    fRoundState      : TRoundState;

    // Speichert welche Seite zuletzt in der MovingList stand
    fLastUnit        : TGameFigure;

    fAGFFiles        : TAGFImageList;

    // damit k�nnen Frames bei der Berechnung �bersprungen werden
    // 1 = jeder Frame, 2 = jeden zweiten, 3 = jeden dritten ...
    fNotifyList      : TNotifyList;

    // Speichert die Einheit, auf der zuletzt die Kameraverfolgung stand
    fLastCapturedFig : TGameFigure;

    { Private-Deklarationen }
    procedure CreateLightMap(Surface: TDirectDrawSurface);

    procedure ReadFormations;
    function GetFormationCount: Integer;
    function GetFormation(Index: Integer): TGroupFormation;
    function GetGroup(Index: Integer): TFigureGroup;
    procedure SetPause(const Value: boolean);

    function FindDoorObject(const Point: TPoint; Tile: Integer; WallType: TWallType; OpenLeft: Boolean): TISOObject;

    function GenerateMouseHint: Boolean;

    procedure CheckPositionAssert(Point: TPoint);overload;
    procedure CheckPositionAssert(X,Y: Integer);overload;
    function CheckPosition(Point: TPoint): Boolean;overload;
    function CheckPosition(X,Y: Integer): Boolean;overload;
  protected
    procedure Start;

    procedure LoadImages;
    procedure FreeImages;
    procedure RestoreSurfaces;

    procedure DoGroupMove;
    { Protected-Deklarationen }
  public
    constructor Create(Page: TDXPage);override;
    destructor destroy;override;
    procedure Clear;
    procedure Draw(Surface: TDirectDrawSurface;var Mem: TDDSurfaceDesc);override;
    procedure LoadMap(Map: String);
    procedure ReDrawRect(DrawRect: TRect;Surface: TDirectDrawSurface;var Mem: TDDSurfaceDesc);override;
    procedure Redraw;override;
    procedure Resize(var NewLeft,NewTop,NewWidth,NewHeight: Integer);override;

    procedure MouseDown(Button: TMouseButton;X,Y: Integer);override;
    procedure MouseUp(Button: TMouseButton;X,Y: Integer);override;
    procedure MouseLeave;override;

    procedure KeyDown(var Key: Word;State: TShiftState);override;

    procedure AddFigure(Manager: TGameFigureManager);

    procedure AktuTempSurface(Rect: TRect);overload;
    procedure AktuTempSurface;overload;

    function GetModelAnimation(Model: String): TXANBitmap;
    function GetAGFImage(Name: String): TAGFImage;
    function GetSurface(Name: String): TDirectDrawSurface;

    procedure Startvorbereitungen;
    procedure Init(Echtzeit : Boolean);
    procedure PlayGame;

    procedure ScrollBy(X,Y: Integer);
    procedure ChangeFigure;
    procedure NextRound(Side: TZugSeite);
    procedure EndGame;

    procedure TerrainChange;

    procedure ShowTextMessage(Text: String);
    procedure BerechnePunkte(Typ: TBuchungPunkteType; Punkte : Integer = 0);
    {$IFNDEF DISABLEBEAM}
    function CanBeamTo(Point: TPoint): boolean;
    {$ENDIF}
    function SetCameraTo(Figure: TGameFigure): TGameFigure;
    function CenterOfSelected: TPoint;

    function BreakRound(WantUnit, BreakingUnit: TGameFigure): Boolean;

    { Anlegen/entfernen von ISOObject (vm-Messages) }
    procedure CreateDeathObject(DeathFigure: TGameFigure);
    {$IFNDEF DISABLEBEAM}
    procedure CreateBeamObject(BeamFigure: TGameFigure; Down: boolean);
    {$ENDIF}
    procedure CreateZielObject(ZielFigure: TGameFigure);
    procedure DeleteZielObject(Figure: TGameFigure);
    function  CreateItemObject(PItem: PLagerItem; Figure: TGameFigure; const Point, OffSet: TPoint): TISOObject;
    procedure CreateFieldExplosion(Explosion: TExplosionInfo);
    function  CreateFigureObject(Figure: TGameFigure): TISOObject;
    function  DoShoot(GameFigure: TGameFigure;Ziel: TPoint; const Infos: TWaffenParamInfos): TISOObject;

    procedure CreateWayPoint(Point: TPoint; Direct: TViewDirection;Color: Integer; Show: Boolean);

    procedure CreateExplosion(Figure: TGameFigure; const Pos: TPoint; Range: Integer; Strength: Integer; Power: Integer);

    procedure OpenDoor(const Point: TPoint; WallType: TWallType);
    procedure CloseDoor(const Point: TPoint; WallType: TWallType);

    procedure UseObject(Figure: TGameFigure; Slot: TSoldatConfigSlot; SlotIndex: Integer;ThrowPos: TPoint; Throw: Boolean = false);

    procedure GetObjects(var Rec: TMessageRecord);

    function CanFriendsMove: Boolean;
    function CanUnitMove(Figure: TGameFigure): Boolean;

    function IsBreakingRound: Boolean;
    function GetObjectList: TList;

    { Infotext Funktionen }
    function AddInfoText(IText: String; IRect: TRect; IObject: TISOObject = nil): Integer;
    procedure DeleteInfoText(ID: Integer);

    function PerformFrame(Sender: TObject;Frames: Integer): boolean;

    function GetTileRect(Sender: TObject;Point: TPoint): TRect;

    procedure SoldatConfigComun(Auftrag: Integer;var Param: Pointer; Point: TPoint;Manager: TObject = nil);

    procedure ZaehleTreffer;
    procedure SetSelected(Figure: TGameFigure);
    function GetSurfaceList : TList;
    function GetTempSurface : TDirectDrawSurface;
    property ISOMap         : TISOMap read fISOMap;
    property Echtzeit       : boolean read fEchtZeit;
    property Pause          : boolean read fPause write SetPause;
    property UserSperre     : Integer read fSperre write fSperre;
    property Figures        : TGameFigureList read fFigures;
    property ItemList       : TList read fItemList;

    property FormationCount : Integer read GetFormationCount;
    property Statistik      : TEinsatzStatistik read fStatistik;
    property Formation[Index: Integer]: TGroupFormation read GetFormation;
    property Groups[Index: Integer] : TFigureGroup read GetGroup;

    property OnNeedRedrawUI   : TRedrawInterface read fRedrawUI write fRedrawUI;
    property OnTileClick      : TTileEvent read fOnTileClick write fOnTileClick;
    property OnShowOptions    : TNotifyEvent read fOnOptions write fOnOptions;

    property NotifyList       : TNotifyList read fNotifyList;
  end;

  TISOMap = class(TObject)
  private
    fTiles       : TTileArray;
    fAlphaMap    : Array of Array of Integer;
    fWidth       : Integer;
    fHeight      : Integer;
    fMapName     : String;
    fXOff        : Integer;
    fYOff        : Integer;
    fMouseMap    : TBitmap;
    fVWidth      : Integer;
    fVHeight     : Integer;

    fTempSur     : TDirectDrawSurface;
    fTempTile    : TDirectDrawSurface;
    fTempMem     : TDDSurfaceDesc;
    fClipMask    : TDirectDrawSurface;

    fDRect       : TRect;
    fDDraw       : TDirectDraw;
    fISOEngine   : TDXIsoEngine;
    fPathFinder  : TPathFinder;

    fMapGenerator: TMapGenerator;

    fTileGroup   : TDXISOTileGroup;

    fShowGrid    : Boolean;
    fWallsTrans  : Boolean;

    fUnitRects   : TUnitDrawRectArray;

    procedure SetMouseMap(const Value: TBitmap);
    procedure SetXOff(const Value: Integer);
    procedure SetyOff(const Value: Integer);

    procedure RecalcAlpha(X,Y: Integer);
    procedure DiscoverField(X,Y: Integer);
    procedure AktuWallAlpha(X, Y: Integer);

    procedure RenderTile(X,Y: Integer; Surface: TDirectDrawSurface;var Mem: TDDSurfaceDesc;XPos,YPos: Integer);
    procedure RenderFloor(X,Y: Integer; Surface: TDirectDrawSurface;var Mem: TDDSurfaceDesc;XPos,YPos: Integer);

    procedure RenderLightNormalEffect(X,Y: Integer;Surface: TDirectDrawSurface;var Mem: TDDSurfaceDesc;XPos,YPos: Integer);
    procedure RenderLightHighEffect(X,Y: Integer;Surface: TDirectDrawSurface;var Mem: TDDSurfaceDesc;XPos,YPos: Integer);

    procedure SetViewAreaSize(Width: Integer; Height: Integer);
    procedure Clear;

    procedure RestoreSurfaces;
    procedure SetShowGrid(const Value: Boolean);

    procedure SetDimension(Width,Height: Integer);
  public
    Figures     : Array of Array of TTileState;

    constructor Create(Container: TDXContainer);
    destructor destroy;override;

    procedure DrawRect(Surface: TDirectDrawSurface;Rect: TRect);

    procedure CreateMap(Enemys: Integer);
    procedure LoadFromFile(FileName: String);

    procedure CenterTo(Pos: TPoint);overload;
    procedure CenterTo(Figure: TGameFigure);overload;

    function TileEntry(X,Y: Integer): PMapTileEntry;
    function TileState(X,Y: Integer): PTileState;

    procedure SetTile(X,Y: Integer; const Tile: TMapTileEntry);

    procedure GetDoorInfo(const Entry: TMapTileEntry; WallType: TWallType;var Tile: Integer; var OpenLeft: Boolean);
    function TileHasWall(X,Y: Integer;Wall: TWallType): boolean;
    procedure IsWayFree(FromP: TPoint; ToP: TPoint; var Result: TWayFreeResult);

    function CheckWalls(X,Z: Integer; WallType: TWallType;Tile: TPoint; ObjektSize: Integer): Integer;

    procedure DoDamageOnWalls(FromPos, ToPos: TPoint; Damage: Integer);

    function CanDrawFigure(Figure: TGameFigure): boolean;

    function TileAtPos(x, y: Integer; Units: Boolean = true): TPoint;

    function GetTileSurface(Index: Integer): TDirectDrawSurface;

    procedure SetDrawingRect(Figure: TGameFigure; Rect: TRect);

    { Einheiten }
    function SetFigureToPos(Point: TPoint; Figure: TGameFigure): Boolean;
    function GetFigureAtPos(Point: TPoint): TGameFigure;

    { Sichtlinien }
    procedure IncSeeing(Figure: TGameFigure);
    procedure DecSeeing(Figure: TGameFigure);

    { Minmaler/Maximaler Offset}
    function GetMaxYOffSet: Integer;
    function GetMaxXOffSet: Integer;
    function GetMinXOffSet: Integer;

    property MapName        : String read fMapName;
    property MouseMap       : TBitmap read fMouseMap write SetMouseMap;
    property xOff           : Integer read fxOff write SetXOff;
    property yOff           : Integer read fyOff write SetyOff;
    property Width          : Integer read fWidth;
    property Height         : Integer read fHeight;
    property IsoEngine      : TDXIsoEngine read fISOEngine write fISOEngine;
    property TileGroup      : TDXISOTileGroup read fTileGroup;
    property ShowGrid       : Boolean read fShowGrid write SetShowGrid;
  end;

  TISOObject = class(TObject)
  private
    fISOEngine    : TDXISOEngine;
    fMap          : TISOMap;
    fX            : Integer;
    fY            : Integer;
    fStart        : TPoint;
    fVisible      : boolean;
    fBefore       : Boolean;
    fHitPoints    : Integer;
    fClipping     : boolean;

    // Interne Daten f�r Infotext
    fIsShowInfo   : boolean;
    fInfoTextID   : Integer;
    fBlockEndGame : Boolean;

    fFigureDestroy: TEventHandle;

    function GetBlockEndGame: Boolean;
    function GetPosition: TPoint;
  protected
    fFreed        : boolean;
    fFirst        : boolean;
    fDeleted      : boolean;
    fFigure       : TGameFigure;

    // Einstellung f�r Infotext (k�nnen von den Objekten ge�ndert werden)
    fShowInfoText : Boolean;  // Infotext anzeigen
    fInfoText     : String;   // Text der als Infotext angezeigt wird

    function PerformFrame: boolean;virtual;

    function CheckWay(FromPoint: TPoint;ToPoint: TPoint): Boolean;overload;
    function CheckWay(OffSet: TFloatPoint;ZAxis: Integer;Pos: TPoint; ObjektSize: Integer = 1;Damage: Integer = 0): Integer;overload;

    function CheckOffset(OffSet: TFloatPoint; out NewOffSet: TFloatPoint): TPoint;

    procedure SetInfoTextRect(var Rect: TRect);virtual;

    procedure SetFigure(const Value: TGameFigure);virtual;
    procedure SetDeleted(const Value: boolean);virtual;

    procedure DestroyFigure(Sender: TObject);virtual;

    function GetVisible: boolean;virtual;
  public
    constructor Create(Engine: TDXISOEngine);virtual;
    destructor destroy;override;
    procedure Draw(Surface : TDirectDrawSurface;X,Y: Integer);virtual;

    procedure DoTreffer(Strength: Integer);
    procedure Explode;virtual;

    function GetAttackStrength: Integer;virtual;
    function GetAttackPower   : Integer;virtual;
    function GetSchadensTyp   : TSchadensTyp;virtual;

    // Gibt die Position des Objektes innerhalb des Tiles zur�ck
    procedure GetPositionVektor(out X,Y,Z: Integer);virtual;

    procedure SetPos(X,Y: Integer);overload;virtual;
    procedure SetPos(Point: TPoint);overload;virtual;

    function  ShowInfoText(Visible: Boolean): Boolean;virtual;
    procedure ScreenOffSet(X,Y: Integer);virtual;

    function GetStartPoint: TPoint;

    property X              : Integer read fX write fX;
    property Y              : Integer read fY write fY;
    property Position       : TPoint read GetPosition;

    property Engine         : TDXISOEngine read fISOEngine write fISOEngine;
    property ISOMap         : TISOMap read fMap write fMap;
    property Visible        : boolean read GetVisible write fVisible;
    property BeforeUnit     : Boolean read fBefore write fBefore;
    property Deleted        : boolean read fDeleted write SetDeleted;
    property HitPoints      : Integer read fHitPoints write fHitPoints;
    property MustBehindClip : boolean read fClipping write fClipping;

    property Figure         : TGameFigure read fFigure write SetFigure;

    // Ist diese Eigenschaft wahr, stoppt der Bodeneinsatz erst, wenn dieses
    // Object gel�scht wurde
    property BlockEndGame   : Boolean read GetBlockEndGame write fBlockEndGame;
  end;

  const
    GreanadeTime = 3000;

  var
    {$IFNDEF DISABLEBEAM}
    AutoPlace       : boolean = true;
    {$ELSE}
    AutoPlace       : boolean = false;
    {$ENDIF}
    ScrollTempo     : double  = 1.0;
    ImageNames      : Array of String;
    XANImages       : Array of record
      Name          : String;
      OffSet        : TPoint;
    end;

implementation

uses
  ISOObjects, ISOWaffenObjects, game_api, ui_utils, array_utils, MusikVerwalter,
  EinsatzIntro;

var
  InfoTextID        : Integer = 0;
  SinTab            : Array[0..359] of double;
  CosTab            : Array[0..359] of double;
  Archiv            : TArchivFile = nil;
  InPerform         : Integer = 0;
  gMusikVerwaltung  : TMusikVerwaltungEinsatz;


procedure AddLoadingImages(ImageName: String);
begin
  SetLength(ImageNames,length(ImageNames)+1);
  ImageNames[high(ImageNames)]:=ImageName;
end;

procedure AddLoadingXAN(XANName: String; OffSet: TPoint);
begin
  SetLength(XANImages,length(XANImages)+1);
  XANImages[high(XANImages)].Name:=XANName;
  XANImages[high(XANImages)].OffSet:=OffSet;
end;

procedure OpenModels;
begin
  if Archiv=nil then
    Archiv:=TArchivFile.Create;
  Archiv.OpenArchiv('data\models.pak',true);
end;

procedure CloseModels;
begin
  if Archiv=nil then
    exit;
  Archiv.Free;
  Archiv:=nil;
end;

{ TDXIsoEngine }

procedure TDXIsoEngine.AddFigure(Manager: TGameFigureManager);
var
  Figure   : TGameFigure;
begin
  {$IFDEF NOENEMYS}if Manager.FigureStatus=fsEnemy then exit;{$ENDIF}
  if fISOMap.Width=0 then
    exit;

  Figure:=TGameFigure.Create(fFigures);
  Figure.Echtzeit:=Self.fEchtzeit;
  Figure.Manager:=Manager;

  if Figure.FigureStatus=fsEnemy then
    fEnemyKI.AddUnit(Figure)
  else if Figure.FigureStatus=fsFriendly then
    fOwnKI.AddUnit(Figure);

  Figure.ViewDirection:=TViewDirection(random(8));
  Figure.NextRound;

  Manager.ZaehleEinsatz;
  Manager.BeginnEinsatz;
end;

procedure TDXIsoEngine.AktuTempSurface(Rect: TRect);
begin
  if fCompleteRedraw then exit;
  IntersectRect(Rect,Rect,ClientRect);
  if IsRectEmpty(Rect) then exit;
  if EqualRect(ClientRect,Rect) then
  begin
    fAktuSurface:=true;
    fRedrawRect:=ClientRect;
    fCompleteRedraw:=true;
    exit;
  end;
  if (Rect.Right-Rect.Left=Width) or
     (Rect.Bottom-Rect.Top=Height) then
  begin
    fISOMap.DrawRect(fTemp,Rect);
    fNeedRedraw:=true;
    exit;
  end;
  fAktuSurface:=true;
  UnionRect(fRedrawRect,fRedrawRect,Rect);
end;

function TDXIsoEngine.AddInfoText(IText: String; IRect: TRect; IObject: TISOObject): Integer;
var
  Dummy    : Integer;
  Index    : Integer;
  Okay     : boolean;
  Last     : Integer;
  Height   : Integer;
begin
  Index:=-1;
  for Dummy:=0 to High(fInfoTexts) do
  begin
    with fInfoTexts[Dummy] do
    begin
      if not Draw then
      begin
        Index:=Dummy;
        break;
      end;
    end;
  end;
  if Index=-1 then
  begin
    Index:=length(fInfoTexts);
    SetLength(fInfoTexts,Index+5);
  end;

  // Pr�fen, ob das Rechteck �ber einem anderem Steht
  if IObject=nil then
  begin
    Okay:=false;
    Last:=High(fInfoTexts);
    Dummy:=0;
    while not Okay do
    begin
      with fInfoTexts[Dummy] do
      begin
        if Draw and (Objekt=nil) then
        begin
          if OverlapRect(Rect,IRect) then
          begin
            Height:=IRect.Bottom-IRect.Top;

            IRect.Top:=Rect.Bottom+1;
            IRect.Bottom:=IRect.Top+Height;
            Dummy:=0;
          end;
        end;
      end;
      if Dummy=Last then Okay:=true;
      inc(Dummy);
    end;
  end;

  Assert((Index>=0) and (Index<length(fInfoTexts)));

  with fInfoTexts[Index] do
  begin
    ID:=InfoTextID;
    Draw:=true;
    Text:=IText;
    Rect:=IRect;
    Objekt:=IObject;
  end;
  result:=InfoTextID;
  inc(InfoTextID);
end;

procedure TDXIsoEngine.AktuTempSurface;
begin
  fRedrawRect:=ClientRect;
  fCompleteRedraw:=true;
  fAktuSurface:=true;
  fNeedRedraw:=true;
end;

procedure TDXIsoEngine.ChangeFigure;
begin
  if Assigned(fRedrawUI) then fRedrawUI(uiSoldatList);
end;

procedure TDXIsoEngine.Clear;
var
  Dummy: Integer;
begin
  while fObjectList.Count>0 do
  begin
    TISOObject(fObjectList[0]).Deleted:=true;
    TISOObject(fObjectList[0]).Free;
  end;

  while fFigures.Count>0 do
    TGameFigure(fFigures[0]).Free;

  for Dummy:=0 to 9 do
    fGroups[Dummy].Clear;

  fObjectList.Clear;
  for Dummy:=0 to high(fInfoTexts) do
    fInfoTexts[Dummy].Draw:=false;

  fFigures.Clear;
  
  fLastCapturedFig:=nil;
  fLastUnit:=nil;
end;

constructor TDXIsoEngine.Create(Page: TDXPage);
var
  Temp      : TDirectDrawSurface;
  TempXAN   : TXANBitmap;
  Dummy     : Integer;
begin
  inherited;

  fEnemyKI:=TGroupKI.Create;
  fEnemyKI.ReserveTUWhenNoSeeing:=20;

  fOwnKI:=TGroupKI.Create;
  // Alle Einheiten werden nah beieinander gesetzt
  fOwnKI.UnitSettingMode:=usmCloseQuartersGroups;

  fItemList:=TList.Create;
  fSperre:=0;
  fOnTileClick:=nil;
  TabStop:=true;
  SetLength(fInfoTexts,2);
  fTemp:=TDirectDrawSurface.Create(Page.Container.DDraw);
  fPause:=false;

  fISOMap:=TISOMap.Create(Self.Container);
  fISOMap.SetDimension(20,20);
  fISOMap.IsoEngine:=Self;

  fXSTempo:=0;
  fYSTempo:=0;
  fSList:=TList.Create;
  fXANList:=TList.Create;

  fFigures:=TGameFigureList.Create;
  fFigures.OnNeedTileRect:=GetTileRect;
  fFigures.SurfaceList:=fSList;
  fFigures.PathFinder:=ISOMap.fPathFinder;

  AddLoadingImages('TileSelection');
  {$IFNDEF DISABLEBEAM}
  AddLoadingImages('FigureBeam');
  {$ENDIF}

  // Surfaces f�r die Einheiten laden
  for Dummy:=0 to high(ModelInfos) do
  begin
    AddLoadingXAN(ModelInfos[Dummy].imageset,Point(ModelInfos[Dummy].AniOffSetX,ModelInfos[Dummy].AniOffSetY));
    AddLoadingXAN(ModelInfos[Dummy].imageset+'TOD',Point(ModelInfos[Dummy].AniOffSetX,ModelInfos[Dummy].AniOffSetY));
  end;

  // da es in einem anderem Package ist, wird es zum schluss geladen
  AddLoadingImages('xforce.pak:Symbole');

  // Surfaces anlegen
  for Dummy:=0 to high(ImageNames) do
  begin
    Temp:=TDirectDrawSurface.Create(Page.Container.DDraw);
    Temp.RessourceName:=ImageNames[Dummy];
    fSList.Add(Temp);
  end;

  // XAN-Bitmaps anlegen
  for Dummy:=0 to high(XANImages) do
  begin
    TempXAN:=TXANBitmap.Create(Container);
    TempXAN.SetAdditionalOffSet(XANImages[Dummy].OffSet);
    fXANList.Add(TempXAN);
  end;

  Container.AddRestoreFunction(RestoreSurfaces);
  fObjectList:=TList.Create;

  for Dummy:=0 to 9 do
  begin
    fGroups[Dummy]:=TFigureGroup.Create(fFigures);
  end;

  { Formationen erstellen }
  fFormationList:=TList.Create;

  fMovingList:=TList.Create;

  fEnemyKI.UnitList:=fMovingList;
  fEnemyKI.SetFormationList(fFormationList);
  fOwnKI.SetFormationList(fFormationList);

  // Formationen Laden
  ReadFormations;

  fAGFFiles:=TAGFImageList.Create;
  fAGFFiles.Container:=Container;

  fAGFFiles.AddImage('data\models.pak','MineExplosion');
  fAGFFiles.AddImage(fDataFile,'RaketenSchweif');

  fNotifyList:=TNotifyList.Create;

  gMusikVerwaltung:=TMusikVerwaltungEinsatz.Create(Self);

  fInput:=game_api_GetDirectInput;
end;

procedure TDXIsoEngine.DeleteInfoText(ID: Integer);
var
  Dummy: Integer;
begin
  for Dummy:=0 to High(fInfoTexts) do
  begin
    if fInfoTexts[Dummy].ID=ID then
    begin
      fInfoTexts[Dummy].Draw:=false;
      exit;
    end;
  end;
end;

destructor TDXIsoEngine.destroy;
var
  Dummy: Integer;
begin
  Container.DeleteRestoreFunction(Restore);
  Clear;
  for Dummy:=0 to 9 do
  begin
    fGroups[Dummy].Free;
  end;

  for Dummy:=fFormationList.Count-1 downto 0 do
    TObject(fFormationList[Dummy]).Free;

  for Dummy:=0 to fXANList.Count-1 do
    TObject(fXANList[Dummy]).Free;

  fXANList.Free;
  fMovingList.Free;
  fFormationList.Free;
  fISOMap.Free;
  fItemList.Free;
  fSList.Free;
  fFigures.Free;
  fEnemyKI.Free;
  fOwnKI.Free;
  fNotifyList.Free;
  gMusikVerwaltung.Free;
  fObjectList.Free;
  fAGFFiles.Free;
  inherited;
end;

procedure TDXIsoEngine.Draw(Surface: TDirectDrawSurface;var Mem: TDDSurfaceDesc);
begin
  RedrawRect(ClientRect,Surface,Mem);
end;

function TDXIsoEngine.GetTileRect(Sender: TObject;Point: TPoint): TRect;
begin
  result.Left:=(((Width div 2)-((HalfTileWidth*(-Point.X))+(Point.Y*HalfTileWidth)))+fISOMap.fXOff);
  result.Right:=Result.Left+TileWidth;
  result.Bottom:=(((Point.X+Point.Y)*HalfTileHeight)+fISOMap.fYOff+HalfTileWidth)+1;
  result.Top:=result.Bottom-(WallHeight+HalfTileHeight);
end;

procedure TDXIsoEngine.Init(Echtzeit : Boolean);
var
  Dummy   : Integer;
begin
  // Siehe PlayGame
  fPlayGame:=true;

  Clear;
  fEchtZeit:=EchtZeit;
  fISOMap.Clear;
  fOverFigure:=nil;
  fShowAll:=false;
  fCameraUnit:=nil;
  // Nach dem ersten Aufruf von NextRound in Start
  // wird die Sperre auf 0 gesetzt
  fSperre:=1;
  fPause:=false;
  for Dummy:=0 to high(fInfoTexts) do
  begin
    fInfoTexts[Dummy].Draw:=false;
    fInfoTexts[Dummy].Objekt:=nil;
  end;

  // Einstellungen f�r den Rundenbasierten Modus
  fRoundState.Round:=0;
  fRoundState.ZugSeite:=zsFriend;

  LoadImages;

  // KI initsialisieren
  fEnemyKI.Clear(fEchtZeit);

  fOwnKI.Clear(fEchtZeit);

  fMovingList.Clear;
end;

procedure TDXIsoEngine.LoadImages;
var
  Dummy   : Integer;
  Ress    : String;
  Arch    : String;
begin
  OpenModels;
  for Dummy:=0 to fSList.Count-1 do
  begin
    Ress:=TDirectDrawSurface(fSList[Dummy]).RessourceName;
    if Pos(':',Ress)<>0 then
    begin
      Arch:='data\'+copy(Ress,1,Pos(':',Ress)-1);
      if Arch<>Archiv.ArchivFile then
      begin
        Archiv.OpenArchiv(Arch,true);
      end;
      Ress:=copy(Ress,Pos(':',Ress)+1,100);
    end;
    Archiv.OpenRessource(Ress);
    TDirectDrawSurface(fSList[Dummy]).SystemMemory:=true;
    TDirectDrawSurface(fSList[Dummy]).LoadFromStream(Archiv.Stream);

    IncEinsatzLoadProgress;
  end;
  TDirectDrawSurface(fSList[0]).TransparentColor:=Container.Surface.ColorMatch(clFuchsia);

  OpenModels;
  for Dummy:=0 to fXANList.Count-1 do
  begin
    Archiv.OpenRessource(XANImages[Dummy].Name);
    TXANBitmap(fXANList[Dummy]).LoadFromStream(Archiv.Stream);
  end;

  fAGFFiles.LoadImages;
end;

procedure TDXIsoEngine.LoadMap(Map: String);
begin
  fISOMap.LoadFromFile(Map);
  AktuTempSurface;
end;

procedure TDXIsoEngine.MouseUp(Button: TMouseButton; X, Y: Integer);
var
  Pos    : TPoint;
  Dummy  : Integer;
  Figure : TGameFigure;
  AddWay : boolean;
  NoWait : Boolean;
begin
  if fSperre>0 then exit;

  if not CanFriendsMove then
    exit;

  Pos:=fISOMap.TileAtPos(X,Y);

  if Assigned(fOnTileClick) and PtInRect(Rect(0,0,fISOMap.Width,fISOMap.Height),Pos) then
  begin
    // Eine Spezielle Behandlungsprozedur ist gesetzt
    fOnTileClick(Self,Pos.X,Pos.Y,Button);
    exit;
  end;

  case Button of
    mbLeft :
    begin
      if not PtInRect(Rect(0,0,fISOMap.Width,fISOMap.Height),Pos) then
        exit;

      if GetKeyState(VK_SHIFT)<0 then
      begin
        // Freies Feuern
        for Dummy:=0 to fFigures.Count-1 do
        begin
          if TGameFigure(fFigures[Dummy]).Selected then
          begin
            TGameFigure(fFigures[Dummy]).ShootToPos(Pos,scsLinkeHand);
            TGameFigure(fFigures[Dummy]).ShootToPos(Pos,scsRechteHand);
          end;
        end;
        exit;
      end;
      SendVMessage(vmGetSelectedFormation);
      fFigures.PathFinder.Formation:=TGroupFormation(Rec.resultObj);
      Container.IncLock;

      CheckPositionAssert(Pos);

      if fISOMap.Figures[Pos.X,Pos.Y].Figure<>nil then
      begin
        if fISOMap.Figures[Pos.X,Pos.Y].Figure.FigureStatus=fsEnemy then
        begin
          // Feind angreifen
          Figure:=fISOMap.Figures[Pos.X,Pos.Y].Figure;
          for Dummy:=0 to fFigures.Count-1 do
          begin
            if TGameFigure(fFigures[Dummy]).Selected then
            begin
              TGameFigure(fFigures[Dummy]).ShootToUnit(Figure,Container.MousePos);
            end;
          end;
        end
        else
        begin
          // Auswahl der Soldaten
          if GetKeyState(VK_SHIFT)<0 then
            fISOMap.Figures[Pos.X,Pos.Y].Figure.Selected:=not fISOMap.Figures[Pos.X,Pos.Y].Figure.Selected
          else
            fISOMap.Figures[Pos.X,Pos.Y].Figure.Select;
        end;
      end
      else
      begin
        { Einheiten Ziele setzen }
        Figures.PathFinder.SetZielPos(CenterOfSelected,Pos);
        AddWay:=GetKeyState(VK_CONTROL)<0;
        NoWait:=GetKeyState(VK_MENU)<0;
        for Dummy:=0 to fFigures.Count-1 do
        begin
          if TGameFigure(fFigures[Dummy]).Selected then
          begin
            TGameFigure(fFigures[Dummy]).MoveTo(Figures.PathFinder.GetNextZiel,NoWait,AddWay);
            AktuTempSurface;
          end;
        end;
      end;
      Redraw;
      Container.DecLock;
    end;
    mbRight :
    begin
      Container.IncLock;
      for Dummy:=0 to fFigures.Count-1 do
      begin
        if TGameFigure(fFigures[Dummy]).Selected then
        begin
          if not TGameFigure(fFigures[Dummy]).StopMove then
            TGameFigure(fFigures[Dummy]).ViewTo(Pos);
        end;
      end;
      Redraw;
      Container.DecLock;
    end;
  end;
end;

procedure TDXIsoEngine.NextRound(Side: TZugSeite);

  procedure AddUnitsToMovingList(Status: TFigureStatus);
  var
    Dummy: Integer;
  begin
    if fMovingList.Count=0 then
    begin
      for Dummy:=0 to fFigures.Count-1 do
      begin
        if (fFigures[Dummy].FigureStatus=Status) and (fFigures[Dummy].IsOnField) then
        begin
          fMovingList.Add(fFigures[Dummy]);
          fFigures[Dummy].NextRound;
        end;
      end;
    end;
  end;

  procedure DeleteUnitsFromTop(Status: TFigureStatus);
  begin
    while (fMovingList.Count>0) and (TGameFigure(fMovingList[0]).FigureStatus=Status) do
      fMovingList.Delete(0);
  end;

begin
  Assert(not Echtzeit);

  inc(fSperre);

  // Warten bis alle Aktionen abgeschlossen sind
  fFigures.WaitForRoundEnd;

  DeleteUnitsFromTop(ZugSeiteToFriendStatus[Side]);

  dec(fSperre);

  if fMovingList.Count=0 then
  begin
    case fRoundState.ZugSeite of
      zsFriend   :
      begin
        if fRoundState.Round = 0 then
          fRoundState.Round := 1
        else
        begin
          fRoundState.ZugSeite:=zsEnemy;
          fEnemyKI.BeginNextRound;
        end;
      end;
      zsNeutral  : fRoundState.ZugSeite:=zsEnemy;
      zsEnemy    :
      begin
        fRoundState.ZugSeite:=zsFriend;
        inc(fRoundState.Round);
      end;
    end;
  end;

  if fRoundState.ZugSeite=zsEnemy then
  begin
    // Liste mit Einheiten f�llen, die Aktionen durchf�hren d�rfen
    AddUnitsToMovingList(fsEnemy);
  end
  else if fRoundState.ZugSeite=zsFriend then
  begin
    AddUnitsToMovingList(fsFriendly);
  end;

  Container.Inclock;
  ShowTextMessage(Format(ST0309220003,[SideNames[fRoundState.ZugSeite],fRoundState.Round]));

  SendVMessage(vmRedrawSolList);
  Redraw;

  Container.DecLock;
  Container.DoFlip;
end;

function TDXIsoEngine.PerformFrame(Sender: TObject;
  Frames: Integer): boolean;
// werden wie statische Variablen behandelt
const
  AktFrame: Integer = 0;
var
  Pos        : TPoint;
  NeedRedraw : boolean;
  OldPos     : TPoint;
  Dummy      : Integer;
  OldFigure  : TGameFigure;
  FrameDummy : Integer;
  TempFrame  : Integer;
begin
  Assert(fSperre>=0,'Sperre aktiv');
  result:=false;
  if not Visible then
    exit;

  // Verhindert geschachtelte Aufrufe von PerformFrame
  if InPerform<>0 then
    exit;

  inc(InPerform);

  Container.Lock;
  fInput.Update;

  NeedRedraw:=false;

  if (isButton31 in fInput.States) and (Assigned(fOnOptions)) and (not (fSperre>0)) and (fRoundState.ZugSeite=zsFriend) then
  begin
    fInput.States:=fInput.States-[isButton31];
    Container.MessageLock(false);
    Container.UnlockAll;
    Container.DeleteFrameFunction(PerformFrame,Self);
    Rec.Bool:=false;
    SendVMessage(vmChangeHotkeys);
    fOnOptions(Self);
    Rec.Bool:=true;
    SendVMessage(vmChangeHotkeys);
    Container.LockAll;
    Container.MessageLock(true);
    Container.ReactiveFrameFunctions;
    Container.AddFrameFunction(PerformFrame,Self,25);
  end;

  if fShowTime>0 then
  begin
    dec(fShowTime,25*Frames);
    if fShowTime<=0 then
    begin
      fShowMessage:=EmptyStr;
      NeedRedraw:=true;
    end;
  end;

  if not Pause then
  begin
    for Dummy:=0 to fFigures.Count-1 do
    begin
      if TGameFigure(fFigures[Dummy]).MoveUnit(Frames) then
      begin
        NeedRedraw:=true;
        if fFigures[Dummy]=fCameraUnit then
          ISOMap.CenterTo(fCameraUnit);
      end;
    end;

    { Objekte verarbeiten }
    for Dummy:=0 to fObjectList.Count-1 do
    begin
      for FrameDummy:=1 to Frames do
      begin
        if (not TISOObject(fObjectList[Dummy]).Deleted) and TISOObject(fObjectList[Dummy]).PerformFrame then
          NeedRedraw:=true;
      end;
    end;
  end;

  { Objekte freigeben }
  Container.Lock;
  for Dummy:=fObjectList.Count-1 downto 0 do
  begin
    if TISOObject(fObjectList[Dummy]).Deleted then
      TISOObject(fObjectList[Dummy]).Free;
  end;
  Container.Unlock;

  {$IFDEF SHOWMOUSEPOS}
  NeedRedraw:=true;
  {$ENDIF}

  // Scroll des Einsatzes
  if (fCameraUnit=nil) or (not fCameraUnit.IsVisible) or (not fCameraUnit.IsMoving) then
  begin
    if (Container.MousePos.x<1) or (isButton24 in fInput.States) then
      fXSTempo:=max(-48,fXSTempo-4)
    else if (Container.MousePos.x>ScreenWidth-2)  or (isButton25 in fInput.States) then
      fXSTempo:=min(48,fXSTempo+4)
    else
    begin
      if fXSTempo>0 then fXSTempo:=max(0,fXSTempo-4)
      else if fXSTempo<0 then fXSTempo:=min(0,fXSTempo+4);
    end;
    if (Container.MousePos.Y<1) or (isButton26 in fInput.States) then
      fYSTempo:=max(-24,fYSTempo-2)
    else if (Container.MousePos.Y>ScreenHeight-2) or (isButton27 in fInput.States) then
      fYSTempo:=min(24,fYSTempo+2)
    else
    begin
      if fYSTempo>0 then fYSTempo:=max(0,fYSTempo-2)
      else if fYSTempo<0 then fYSTempo:=min(0,fYSTempo+2);
    end;
    if (fXSTempo<>0) or (fYSTempo<>0) then
    begin
      ScrollBy(round(fXSTempo*ScrollTempo),round(fYSTempo*ScrollTempo));
      NeedRedraw:=true;
    end;
  end;

  Pos:=ISOMap.TileAtPos(Container.MousePos.X,Container.MousePos.Y);
  if PtInRect(ClientRect,Point(Container.MousePos.X,Container.MousePos.Y)) and (Pos.X<>fMousePos.X) or (Pos.Y<>fMousePos.Y) then
  begin
    OldPos:=fMousePos;
    fMousePos:=Pos;
    AktuTempSurface(GetTileRect(Self,OldPos));
    AktuTempSurface(GetTileRect(Self,fMousePos));
    NeedRedraw:=true;
  end;

  // Anzeige der Objektnamen
  OldFigure:=fOverFigure;
  if CheckPosition(fMousePos) then
    fOverFigure:=fISOMap.Figures[fMousePos.X,fMousePos.Y].Figure
  else
    fOverFigure:=nil;

  if isButton30 in fInput.States then // Alle Objektnamen anzeigen
  begin
    for Dummy:=0 to fObjectList.Count-1 do
    begin
      if (fObjectList[Dummy]<>nil) then
      begin
        if TISOObject(fObjectList[Dummy]).ShowInfoText(true) then
          NeedRedraw:=true;
      end;
    end;
    fShowAll:=true;
  end
  else if fShowAll then
  begin
    for Dummy:=0 to fObjectList.Count-1 do
    begin
      if (fObjectList[Dummy]<>nil) then
      begin
        if TISOObject(fObjectList[Dummy]).ShowInfoText(false) then
          NeedRedraw:=true;
      end;
    end;
    fShowAll:=false;
  end
  else
  begin
    if fOverFigure<>OldFigure then
    begin
      if OldFigure<>nil then
        OldFigure.ShowInfoText(false);
      if (fOverFigure<>nil) and fISOMap.CanDrawFigure(fOverFigure) then
        fOverFigure.ShowInfoText(true);
    end;
  end;

  if GenerateMouseHint then
    NeedRedraw:=true;

  // W�nde transparent anzeigen
  if (isButton23 in fInput.States)<>ISOMap.fWallsTrans then
  begin
    ISOMap.fWallsTrans:=(isButton23 in fInput.States);
    AktuTempSurface;
    NeedRedraw:=true;
  end;
  if not (fSperre>0) then
  begin
    if (isButton28 in fInput.States) then
    begin
      SendVMessage(vmShowNextUnit);
      fInput.States:=fInput.States-[isButton28];
      NeedRedraw:=true;
    end
    else if (isButton29 in fInput.States) then
    begin
      SendVMessage(vmShowPrevUnit);
      fInput.States:=fInput.States-[isButton29];
      NeedRedraw:=true;
    end;
  end;

  Container.UnLock;
  SendVMessage(vmCheckRedrawOfSolList);
  NeedRedraw:=NeedRedraw or fNeedRedraw;
  fNeedRedraw:=false;
  Container.IncLock;
  if NeedRedraw then
  begin
//    Container.RedrawBlackControl(Self,Container.Surface);
    Redraw;
  end;
  dec(InPerform);

  // Pr�fen, ob sich am Terrain etwas ge�ndert hat
  TerrainChange;

  Rec.NotifyUIType:=nuitCheckMerkeRedraw;
  SendVMessage(vmNotifyUI);
  NeedRedraw:=NeedRedraw or Rec.result;
  result:=NeedRedraw;
  Container.DecLock;

end;

procedure TDXIsoEngine.PlayGame;

  function AllObjectsDelete: Boolean;
  var
    Dummy: Integer;
  begin
    result:=true;
    for Dummy:=0 to fObjectList.Count-1 do
    begin
      if TISOObject(fObjectList[Dummy]).BlockEndGame then
      begin
        result:=false;
        exit;
      end;
    end;
  end;

begin
  FillChar(fStatistik,sizeOf(fStatistik),#0);
  fStatistik.EinsatzErgebnis:=frCancel;
  SetFocus;
  Start;
  fPause:=false;
  fNeedRedraw:=false;

  Container.AddFrameFunction(PerformFrame,Self,25);

  // Spiel spielen
  while fPlayGame do
  begin
    Application.HandleMessage;
    DoGroupMove;
  end;

  // fPlayGame muss nochmal auf true gesetzt werden, damit auch Zeichenoperationen
  // durchgef�hrt werden
  fPlayGame:=true;

  // Auf dem Abschluss warten
  // alle Objekte die noch Schaden k�nnen (Merkmal BlockEndGame = true )
  // m�ssen gel�scht
  while not AllObjectsDelete do
    Application.HandleMessage;

  fPlayGame:=false;
  // Ob der Einsatz ohne Verluste erfolgreich war
  if fStatistik.SolVerloren.Anzahl=0 then
  begin
    fStatistik.ohneVerlustBonus.Anzahl:=fStatistik.AlienAbgeschossen.Anzahl;
    fStatistik.ohneVerlustBonus.Punkte:=fStatistik.AlienAbgeschossen.Anzahl*15;
  end;

  Container.DeleteFrameFunction(PerformFrame,Self);
  fAktuSurface:=false;

  if fStatistik.Schuesse>0 then
  begin
    fStatistik.TrefferBilanz.Anzahl:=(fStatistik.Treffer*100) div fStatistik.Schuesse;
    fStatistik.TrefferBilanz.Punkte:=(fStatistik.TrefferBilanz.Anzahl*50) div 100;
  end;

  // Nach dem Bodeneinsatz wieder aufr�umen um Speicher zu sparen
  FreeImages;
end;

procedure TDXIsoEngine.ReDrawRect(DrawRect: TRect;
  Surface: TDirectDrawSurface;var Mem: TDDSurfaceDesc);
var
  TempRect: TRect;
  {$IFDEF SHOWUNITRECTS}
  Dummy   : Integer;
  {$ENDIF}
  {$IFDEF SHOWMOUSEPOS}
  Poi    : TPoint;
  {$ENDIF}

  procedure AktuInfoTextsRects(Index: Integer);
  var
    NewRect: TRect;
    Height : Integer;
    Dummy  : Integer;
  begin
    if (Index<Length(fInfoTexts)) and (Index>=0) then
    begin
      if Assigned(fInfoTexts[Index].Objekt) then
      begin
        fInfoTexts[Index].Objekt.SetInfoTextRect(NewRect);
        fInfoTexts[Index].Text:=fInfoTexts[Index].Objekt.fInfoText;
      end;

      if Index>0 then
      begin
        // Pr�fen, ob das Rechteck �ber einem anderem Steht
        Dummy:=0;
        while Dummy<Index do
        begin
          with fInfoTexts[Dummy] do
          begin
            if Draw then
            begin
              if OverlapRect(Rect,NewRect) then
              begin
                Height:=NewRect.Bottom-NewRect.Top;

                NewRect.Top:=Rect.Bottom+1;
                NewRect.Bottom:=NewRect.Top+Height;
                Dummy:=0;
              end;
            end;
          end;
          inc(Dummy);
        end;
      end;
    end;

    fInfoTexts[Index].Rect:=NewRect;
//    InfoText.Text:=InfoText.Figure.Name;
  end;

  procedure DrawInfoTexts;
  var
    Dummy   : Integer;
    WFigur  : boolean;
  begin
    { Infotexte zeichnen }
    for WFigur:=false to true do
    begin
      for Dummy:=0 to High(fInfoTexts) do
      begin
        Assert(Dummy<length(fInfoTexts));
        with fInfoTexts[Dummy] do
        begin
          if Draw and ((Objekt<>nil)=WFigur) then
          begin
            AktuInfoTextsRects(Dummy);

            BlendRectangle(CorrectBottomOfRect(Rect),150,bcDisabled,Surface,Mem);

            Rectangle(Surface,Mem,Rect,bcDisabled);
            FontEngine.DynamicText(Surface,Rect.Left+2,Rect.Top+2,Text,[WhiteStdFont,YellowStdFont]);
          end;
        end;
      end;
    end;
  end;

  procedure DrawText(CenterAt: TPoint; Text: String; Progress: Integer = 0; Max: Integer = 0);
  var
    tmp     : Integer;
  begin
    tmp:=WhiteBStdFont.TextWidth(Text);
    TempRect:=Rect(CenterAt.X-(tmp shr 1)-5,CenterAt.Y,CenterAt.X+(tmp shr 1)+5,CenterAt.Y+18);

    if Max = 0 then
      BlendRectangle(CorrectBottomOfRect(TempRect),200,bcDisabled,Surface,Mem)
    else
    begin
      BlendPercentBar(CorrectBottomOfRect(TempRect),200,bcMaroon,bcDisabled,Progress/Max,Surface,Mem)
    end;

    Rectangle(Surface,Mem,ResizeRect(TempRect,-1),bcStdGray);
    WhiteBStdFont.Draw(Surface,Left+(Width shr 1)-(tmp shr 1),CenterAt.Y+3,Text);
  end;

begin

  if fAktuSurface then
  begin
    fISOMap.DrawRect(fTemp,fRedrawRect);
    fAktuSurface:=false;
    fCompleteRedraw:=false;
    FillChar(fRedrawRect,SizeOf(TRect),#0);
  end;

  TempRect:=DrawRect;
  OffsetRect(TempRect,-Left,-Top);
  Surface.Draw(DrawRect.Left,DrawRect.Top,TempRect,fTemp,false);

  Surface.ClippingRect:=CorrectBottomOfRect(ClientRect);

  Assert(Surface.Lock(Mem));
  Surface.UnLock;

  DrawInfoTexts;

  if fShowMessage<>EmptyStr then
  begin
    BlendRectangle(Rect(Left+6,Top+6,Left+9+fShowWidth,Top+22),175,bcDisabled,Surface,Mem);
    RedStdFont.Draw(Surface,Left+7,Top+7,fShowMessage);
    Rectangle(Surface,Mem,Left+5,Top+5,Left+10+fShowWidth,Top+23,bcDisabled);
  end;

  Surface.ClearClipRect;

  if fPause then
    DrawText(Point(Left+(Width shr 1),Top+7),ST0309220004);

  if not CanFriendsMove then
    DrawText(Point(Left+(Width shr 1),Top+27),Format(ST0309220005,[fRoundState.Round]),fFigures.Enemys-fMovingList.Count,fFigures.Enemys);

  if IsBreakingRound then
    DrawText(Point(Left+(Width shr 1),Top+47),Format('Unterbrechung der %s-Runde',[SideNames[fRoundState.ZugSeite]]));

  {$IFDEF SHOWUNITRECTS}
  for Dummy:=0 to high(ISOMap.fUnitRects) do
    Rectangle(Surface,Mem,ISOMap.fUnitRects[Dummy].Rect,bcMaroon);
  {$ENDIF}
end;

procedure TDXIsoEngine.Resize(var NewLeft, NewTop, NewWidth,
  NewHeight: Integer);
begin
  fTemp.SystemMemory:=true;
  fTemp.SetSize(NewWidth,NewHeight);
  fTemp.Restore;
  fISOMap.SetViewAreaSize(NewWidth,NewHeight);
end;

procedure TDXIsoEngine.RestoreSurfaces;
begin
  LoadImages;
  fISOMap.RestoreSurfaces;
end;

procedure TDXIsoEngine.ScrollBy(X, Y: Integer);
var
  OldX   : Integer;
  OldY   : Integer;
  Dummy  : Integer;

  procedure ScrollSurface;
  var
    ScrollRect: TRect;
    XRedraw   : TRect;
    YRedraw   : TRect;
  begin
    ScrollRect:=Rect(0,0,Width,Height);
    XRedraw:=Rect(0,0,0,Height);
    YRedraw:=Rect(0,0,Width,0);
    if X>0 then
    begin
      XRedraw.Left:=Width-X;
      XRedraw.Right:=Width;
    end
    else if X<0 then
    begin
      XRedraw.Left:=0;
      XRedraw.Right:=-X;
    end;
    if Y>0 then
    begin
      YRedraw.Top:=Height-Y;
      YRedraw.Bottom:=Height;
    end
    else if Y<0 then
    begin
      YRedraw.Top:=0;
      YRedraw.Bottom:=-Y;
    end;
    ScrollDC(fTemp.Canvas.Handle,-X,-Y,ScrollRect,ScrollRect,0,nil);
    fTemp.Canvas.Release;
    AktuTempSurface(YRedraw);
    AktuTempSurface(XRedraw);
  end;

begin
  OldX:=ISOMap.XOff;
  OldY:=ISOMap.YOff;
  ISOMap.XOff:=OldX-X;
  ISOMap.YOff:=OldY-Y;
  X:=OldX-ISOMap.XOff;
  Y:=OldY-ISOMap.YOff;
  for Dummy:=0 to High(fInfoTexts) do
  begin
    if fInfoTexts[Dummy].Draw then
      OffsetRect(fInfoTexts[Dummy].Rect,-X,-Y);
  end;
  if not fCompleteRedraw then
  begin
    OffsetRect(fRedrawRect,-X,-Y);
    IntersectRect(fRedrawRect,fRedrawRect,ClientRect);
  end;
  if not Visible then exit;
  if fCompleteRedraw then exit;
  ScrollSurface;
end;

procedure TDXIsoEngine.Startvorbereitungen;
const
  StartDiscoverSize = 30;
var
  X,Y   : Integer;
  Dummy : Integer;
begin
  {$IFDEF FULLMAP}
    for X:=0 to fISOMap.fWidth-1 do
    begin
      for Y:=0 to fISOMap.fHeight-1 do
      begin
        fISOMap.fTiles[X,Y].AlphaV.All:=8;
        fISOMap.fTiles[X,Y].AlphaV.Objekt:=8;
        fISOMap.fTiles[X,Y].AlphaV.Discover:=true;
        fISOMap.RecalcAlpha(X,Y);
        fISOMap.DiscoverField(X,Y);
      end;
    end;
  {$ELSE}
    for X:=0 to fISOMap.fWidth-1 do
    begin
      for Y:=0 to fISOMap.fHeight-1 do
      begin
        fISOMap.fTiles[X,Y].AlphaV.All:=0;
        fISOMap.fTiles[X,Y].AlphaV.Objekt:=0;
        fISOMap.fTiles[X,Y].AlphaV.Discover:=false;
        fISOMap.fTiles[X,Y].BlockInfo.Discover:=false;
      end;
    end;

    {$IFNDEF DISABLEBEAM}
      for X:=0 to StartDiscoverSize-1 do
      begin
        for Y:=0 to StartDiscoverSize-1-X do
        begin
          ISOMap.TileEntry(X,Y).AlphaV.Discover:=true;
          ISOMap.DiscoverField(X,Y);
          ISOMap.RecalcAlpha(X,Y);
        end;
      end;
    {$ENDIF}

    // Durch das SetGameFigureToField beim Setzen der Soldaten an die Einsatzpositionen
    // wird das IncSeeing aufgerufen. Allerdings wird erst hier, die Sichtbarkeitsinformationen
    // initsialisiert. Deshalb muss vor Decseeing aufgerufen werden, damit IncSeeing �berhaupt
    // zieht.
    for Dummy:=0 to fFigures.Count-1 do
      fISOMap.DecSeeing(fFigures[Dummy]);

    for Dummy:=0 to fFigures.Count-1 do
      fISOMap.IncSeeing(fFigures[Dummy]);

  {$ENDIF}

  for Dummy:=0 to fFigures.Count-1 do
    fFigures[Dummy].AktuSeeingList;

  fFigures.CountEnemys;

  ui_utils_ChangeKeyAssign(fInput,isButton23,KeyConfiguration.BodenKeys.WallsTrans);
  ui_utils_ChangeKeyAssign(fInput,isButton24,KeyConfiguration.BodenKeys.ScrollLeft);
  ui_utils_ChangeKeyAssign(fInput,isButton25,KeyConfiguration.BodenKeys.ScrollRight);
  ui_utils_ChangeKeyAssign(fInput,isButton26,KeyConfiguration.BodenKeys.ScrollUp);
  ui_utils_ChangeKeyAssign(fInput,isButton27,KeyConfiguration.BodenKeys.ScrollDown);
  ui_utils_ChangeKeyAssign(fInput,isButton30,KeyConfiguration.BodenKeys.ObjektNamen);
  ui_utils_ChangeKeyAssign(fInput,isButton28,KeyConfiguration.BodenKeys.NextSoldat);
  ui_utils_ChangeKeyAssign(fInput,isButton29,KeyConfiguration.BodenKeys.PrevSoldat);

  {$IFDEF DISABLEBEAM}
  fISOMap.CenterTo(fFigures[0]);
  {$ENDIF}

  AktuTempSurface;
end;

procedure TDXIsoEngine.Start;
begin
  fMouseHint:=TMouseHintInfoObject.Create(Self);

  GLobalFile.WriteLine;
  GLobalFile.Write('BEGINN BODENEINSATZ');
  GlobalFile.Write('Echtzeit',fEchtzeit);
  GLobalFile.WriteLine;

  fGroups[9].AddAll;

  fNotifyList.CallEvents(EVENT_ONSTARTGAME,Self);

  SetSelected(fFigures[0]);

  NextRound(zsEnemy);
end;

function TDXIsoEngine.GetSurfaceList: TList;
begin
  result:=fSList;
end;

function TDXIsoEngine.GetTempSurface: TDirectDrawSurface;
begin
  result:=fTemp;
end;

procedure TDXIsoEngine.CreateLightMap(Surface: TDirectDrawSurface);
var
  Mem          : TDDSurfaceDesc;
  X,Y          : Integer;
  Pointer      : Cardinal;
begin
  Surface.Lock(Mem);
  if Mode32Bit then
  begin
    for Y:=0 to 56 do
    begin
      Pointer:=Integer(Mem.lpSurface)+(Y*Mem.lPitch)+(357 shl 2);
      for X:=0 to 48 do
      begin
             if PDWord(Pointer)^=bcYellow then PInteger(Pointer)^:=1
        else if PDWord(Pointer)^=bcBlue then PInteger(Pointer)^:=2
        else if PDWord(Pointer)^=bcRed then PInteger(Pointer)^:=3
        else if PDWord(Pointer)^=bcAqua then PInteger(Pointer)^:=4
        else if PDWord(Pointer)^=bcPurple then PInteger(Pointer)^:=5
        else if PDWord(Pointer)^=bcLime then PInteger(Pointer)^:=6
        else if PDWord(Pointer)^=bcSilver then PInteger(Pointer)^:=7
        else if PDWord(Pointer)^=bcNavy then PInteger(Pointer)^:=8
        else if PDWord(Pointer)^=bcBlack then PInteger(Pointer)^:=9
        else if PDWord(Pointer)^=bcMaroon then PInteger(Pointer)^:=10
        else if PDWord(Pointer)^=bcStdGray then PInteger(Pointer)^:=11
        else if PDWord(Pointer)^=bcOlive then PInteger(Pointer)^:=12
        else if PDWord(Pointer)^=bcWhite then PInteger(Pointer)^:=13
        else if PDWord(Pointer)^=bcFuchsia then PInteger(Pointer)^:=14
        else if PDWord(Pointer)^=bcGreen then PInteger(Pointer)^:=15
        else if PDWord(Pointer)^=bcTeal then PInteger(Pointer)^:=16
        else PDWord(Pointer)^:=0;
        inc(Pointer,4);
      end;
    end;
  end
  else
  begin
    for Y:=0 to 56 do
    begin
      Pointer:=Integer(Mem.lpSurface)+(Y*Mem.lPitch)+(357 shl 1);
      for X:=0 to 48 do
      begin
             if PWord(Pointer)^=bcYellow then PWord(Pointer)^:=1
        else if PWord(Pointer)^=bcBlue then PWord(Pointer)^:=2
        else if PWord(Pointer)^=bcRed then PWord(Pointer)^:=3
        else if PWord(Pointer)^=bcAqua then PWord(Pointer)^:=4
        else if PWord(Pointer)^=bcPurple then PWord(Pointer)^:=5
        else if PWord(Pointer)^=bcLime then PWord(Pointer)^:=6
        else if PWord(Pointer)^=bcSilver then PWord(Pointer)^:=7
        else if PWord(Pointer)^=bcNavy then PWord(Pointer)^:=8
        else if PWord(Pointer)^=bcBlack then PWord(Pointer)^:=9
        else if PWord(Pointer)^=bcMaroon then PWord(Pointer)^:=10
        else if PWord(Pointer)^=bcStdGray then PWord(Pointer)^:=11
        else if PWord(Pointer)^=bcOlive then PWord(Pointer)^:=12
        else if PWord(Pointer)^=bcWhite then PWord(Pointer)^:=13
        else if PWord(Pointer)^=bcFuchsia then PWord(Pointer)^:=14
        else if PWord(Pointer)^=bcGreen then PWord(Pointer)^:=15
        else if PWord(Pointer)^=bcTeal then PWord(Pointer)^:=16
        else PWord(Pointer)^:=0;
        inc(Pointer,2);
      end;
    end;
  end;
  Surface.Unlock;
end;

procedure TDXIsoEngine.Redraw;
var
  Mem : TDDSurfaceDesc;
begin
//  if Visible then
//    Container.RedrawBlackControl(Self,Container.Surface,false);
  inherited;
end;

procedure TDXIsoEngine.SetSelected(Figure: TGameFigure);
var
  Dummy: Integer;
begin
  if (Figure.FigureStatus<>fsFriendly) or (not Figure.IsOnField) then
    exit;

  if not CanUnitMove(Figure) then
    exit;

  Container.Lock;
  for Dummy:=0 to fFigures.Count-1 do
  begin
    TGameFigure(fFigures[Dummy]).ChangeSelected(Figure=fFigures[Dummy]);
  end;
  Container.IncLock;
  AktuTempSurface;
//  SendVMessage(vmShowNextUnit);
  Container.Unlock;
  Rec.NotifyUIType:=nuitSelectionChange;
  Rec.Figure:=Figure;
  SendVMessage(vmNotifyUI);
  SendVMessage(vmRedrawSolList);
  Container.DecLock;
  Redraw;
end;

procedure TDXIsoEngine.EndGame;
begin
  fPlayGame:=false;
  fNotifyList.CallEvents(EVENT_ONENDGAME,Self);
end;

procedure TDXIsoEngine.CreateDeathObject(DeathFigure: TGameFigure);
begin
  with TDeathObject.Create(Self) do
    Figure:=DeathFigure;
end;

function TDXIsoEngine.DoShoot(GameFigure: TGameFigure;Ziel: TPoint; const Infos: TWaffenParamInfos): TISOObject;
var
  Obj   : TWaffenObject;
begin
  {$IFNDEF NOSHOOT}
  case Infos.Art of
    wtRaketen     : Obj := TRaketenWaffe.Create(Self);
    wtShortRange  : Obj := TShortRangeWaffe.Create(Self);
    else
      Obj:=TLaserWaffe.Create(Self);
  end;

  with Obj do
  begin
    SetParams(Point(GameFigure.XPos,GameFigure.YPos),Ziel,Infos,GameFigure.FigureStatus,GameFigure);

    if GameFigure.FigureStatus=fsFriendly then
      inc(fStatistik.Schuesse);

    if (ISOMap.CanDrawFigure(GameFigure)) and (Infos.Art<>wtShortRange) then
    begin
      Container.PlaySound(Infos.ShootSound);
    end;

  end;

  if Infos.Art=wtRaketen then
  begin
    Rec.ID:=Infos.MunID;
    SendVMessage(vmGetLagerItem);

    Assert(Rec.Item<>nil,'Ung�ltige Munition bei TDXIsoEngine.DoShoot');
    Obj.SetExplosionSize(Rec.Item.Explosion);
  end;
  result:=Obj;
  {$ENDIF}
end;

procedure TDXIsoEngine.KeyDown(var Key: Word; State: TShiftState);
var
  Group         : Integer;
  GroupSelected : boolean;
  Dummy         : Integer;
begin
  inherited;
  if fSperre>0 then exit;

  if (Char(Key)>='0') and (Char(Key)<='9') then
  begin
    Group:=Key-48;

    if fEchtzeit then
    begin
      if ssCtrl in State then
        fGroups[Group].CreateGroup
      else
      begin
        GroupSelected:=true;
        for Dummy:=0 to fFigures.Count-1 do
        begin
          if fFigures[Dummy].Selected<>fGroups[Group].UnitInGroup(fFigures[Dummy]) then
          begin
            GroupSelected:=false;
            break;
          end;
        end;
        if not GroupSelected then
          fGroups[Group].SelectGroup
        else
          fISOMap.CenterTo(CenterOfSelected);
      end;
      Key:=0;
      exit;
    end
    else
    begin
      if Group=0 then
        Group:=10;
      dec(Group);
      for Dummy:=0 to fFigures.Count-1 do
      begin
        if fFigures[Dummy].FigureStatus=fsFriendly then
        begin
          if Group=0 then
          begin
            if fFigures[Dummy].IsOnField then
              fFigures[Dummy].Select;
            exit;
	  end;
          dec(Group);
	end;
      end;
    end;
  end;
end;

{$IFNDEF DISABLEBEAM}
procedure TDXIsoEngine.CreateBeamObject(BeamFigure: TGameFigure; Down: boolean);
begin
  with TBeamObject.Create(Self) do
  begin
    Source:=fSList[1];
    SetPos(BeamFigure.XPos,BeamFigure.YPos);
    Figure:=BeamFigure;
    if Down then
      OnReady:=BeamFigure.BeamDownReady
    else
      OnReady:=BeamFigure.BeamUpReady;
    BeamDown:=Down;
  end;
end;
{$ENDIF}

function TDXIsoEngine.GetFormationCount: Integer;
begin
  result:=fFormationList.Count;
end;

function TDXIsoEngine.GetFormation(Index: Integer): TGroupFormation;
begin
  result:=TGroupFormation(fFormationList[Index]);
end;

procedure TDXIsoEngine.CreateZielObject(ZielFigure: TGameFigure);
var
  Dummy: Integer;
begin
  if ZielFigure=nil then
    exit;
    
  if not ZielFigure.Selected then
    exit;
    
  for Dummy:=0 to fObjectList.Count-1 do
  begin
    if TObject(fObjectList[Dummy]) is TGameFigureZiel then
    begin
      if TGameFigureZiel(fObjectList[Dummy]).Figure=ZielFigure then
      begin
        // Neu zuweisen, damit das neue Ziel �bernommen wird
        TGameFigureZiel(fObjectList[Dummy]).Figure:=ZielFigure;
        TGameFigureZiel(fObjectList[Dummy]).fDeleted:=false;
        exit;
      end;
    end;
  end;

  with TGameFigureZiel.Create(Self) do
    Figure:=ZielFigure;
end;

procedure TDXIsoEngine.DeleteZielObject(Figure: TGameFigure);
var
  Dummy: Integer;
begin
  for Dummy:=0 to fObjectList.Count-1 do
  begin
    if TObject(fObjectList[Dummy]) is TGameFigureZiel then
    begin
      if TGameFigureZiel(fObjectList[Dummy]).Figure=Figure then
      begin
        TISOObject(fObjectList[Dummy]).fDeleted:=true;
        exit;
      end;
    end;
  end;
end;

procedure TDXIsoEngine.ReadFormations;
var
  Archiv    : TArchivFile;
  Stream    : TMemoryStream;
  Buffer    : Cardinal;
  Count     : Integer;
  Formation : TGroupFormation;
begin
  Archiv:=TArchivFile.Create;
  Archiv.OpenArchiv(FGameDataFile,true);
  Archiv.OpenRessource('FormationData');
  Stream:=TMemoryStream.Create;
  Decode(Archiv.Stream,Stream);
  Archiv.Free;
  Stream.Read(Buffer,SizeOf(Buffer));
  if Buffer<>FormationVersion then
    raise Exception.Create('Ung�ltige Formationsdaten');
  // Anzahl der Formationen
  Stream.Read(Count,SizeOf(Count));
  while Count>0 do
  begin
    Formation:=TGroupFormation.Create;
    fFormationList.Add(Formation);
    Formation.LoadFromStream(Stream);
    dec(Count);
  end;
  Stream.Free;
end;

function TDXIsoEngine.CreateItemObject(PItem: PLagerItem; Figure: TGameFigure; const Point, OffSet: TPoint): TISOObject;
var
  ISOItem : TISOItem;
begin
  Assert(Figure<>nil);
  ISOItem:=TISOItem.Create(Self);
  with ISOItem do
  begin
    SetPos(Point);
    Item:=PItem^;
  end;
  ISOItem.FriendStatus:=Figure.FigureStatus;
  ISOItem.Figure:=Figure;
  ISOItem.SetOffSet(OffSet);
  result:=ISOItem;
  fItemList.Add(ISOItem);
end;

procedure TDXIsoEngine.SoldatConfigComun(Auftrag: Integer; var Param: Pointer; Point: TPoint;Manager: TObject);
var
  Dummy   : Integer;
  Figure  : TGameFigure;
begin
  case Auftrag of
    1: Param:=fItemList;
    2:
    begin
      if TISOItem(Param).FriendStatus<>fsFriendly then
        BerechnePunkte(bptItemFound,TISOItem(Param).PunkteWert);
      TISOItem(Param).Free;
      fItemList.Remove(Param);
    end;
    3:
    begin
      // Soldatconfig kennt nur den Manager -> GameFigure muss ermittelt werden
      Figure:=nil;
      for Dummy:=0 to fFigures.Count-1 do
      begin
        if fFigures[Dummy].Manager=Manager then
        begin
          Figure:=fFigures[Dummy];
          break;
        end;
      end;
      CreateItemObject(Param,TGameFigure(Figure),Point,TGameFigure(Figure).GetUnitOffset);
      Param:=fItemList[fItemList.Count-1];
    end;
    4:
    begin
      CheckPositionAssert(Point);
      if ISOMap.fTiles[Point.X,Point.Y].BlockInfo.BlockType[Integer(Param)].Block=wbNone then
        Param:=Pointer(true)
      else
        Param:=Pointer(false);
    end;
  end;
end;

procedure TDXIsoEngine.ShowTextMessage(Text: String);
begin
  fShowMessage:=Text;
  fShowTime:=500+(length(Text)*50);
  fShowWidth:=RedStdFont.TextWidth(Text);
  Redraw;
end;

{$IFNDEF DISABLEBEAM}
function TDXIsoEngine.CanBeamTo(Point: TPoint): boolean;
var
  Dummy: Integer;
  TileEntry : PMapTileEntry;
  TileState : PTileState;
begin
  result:=false;
  TileEntry:=ISOMap.TileEntry(Point.X,Point.Y);
  // Feld erforscht
  if not TileEntry.AlphaV.Discover then
  begin
    Rec.Text:=ST0309220006;
    SendVMessage(vmTextMessage);
    exit;
  end;

  if not TileEntry.Begehbar then
  begin
    Rec.Text:=ST0409260001;
    SendVMessage(vmTextMessage);
    exit;
  end;
  TileState:=ISOMap.TileState(Point.X,Point.Y);

  // Feld bereits belegt
  if TileState.Figure<>nil then
  begin
    Rec.Text:=ST0309220007;
    SendVMessage(vmTextMessage);
    exit;
  end;

  // Pr�fen, ob gerade eine Einheit an die Position gebeamt wird
  with TileState^ do
  begin
    for Dummy:=0 to ObjectCount-1 do
    begin
      if Objects[Dummy] is TBeamObject then
      begin
        Rec.Text:=ST0309220008;
        SendVMessage(vmTextMessage);
        exit;
      end;
    end;
  end;
  result:=true;
end;
{$ENDIF}

procedure TDXIsoEngine.BerechnePunkte(Typ: TBuchungPunkteType; Punkte: Integer);
begin
  case Typ of
    bptAlienTot  :
    begin
      inc(fStatistik.AlienAbgeschossen.Anzahl);
      inc(fStatistik.AlienAbgeschossen.Punkte,20);
    end;
    bptSoldatTot :
    begin
      inc(fStatistik.SolVerloren.Anzahl);
      inc(fStatistik.SolVerloren.Punkte,-30);
      fStatistik.ohneVerlustBonus.Anzahl:=0;
      fStatistik.ohneVerlustBonus.Punkte:=0;
    end;
    bptGewonnen  : fStatistik.EinsatzErgebnis:=frWin;
    bptVerloren  : fStatistik.EinsatzErgebnis:=frLoose;
    bptItemFound :
    begin
      inc(fStatistik.AusErbeutet.Anzahl);
      inc(fStatistik.AusErbeutet.Punkte,Punkte);
    end;
    bptItemLost  :
    begin
      inc(fStatistik.AusVerloren.Anzahl);
      inc(fStatistik.AusVerloren.Punkte,-Punkte);
    end;
  end;
end;

procedure TDXIsoEngine.MouseDown(Button: TMouseButton; X, Y: Integer);
{$IFDEF DEADONRIGHTCLICK}
var
  Tile: TPoint;
{$ENDIF}
begin
  if fSperre>0 then
  begin
    if (Button=mbRight) then
    begin
      SendVMessage(vmGetPause);
      if rec.Result then
      begin
        rec.Bool:=true;
        SendVMessage(vmGoOnAfterMessage);
      end;
    end;
    exit;
  end;

  if not CanFriendsMove then
    exit;

  {$IFDEF DEADONRIGHTCLICK}
  if Button=mbRight then
  begin
    Tile:=fISOMap.TileAtPos(X,Y);
    if CheckPosition(Tile) then
    begin
      if fISOMap.Figures[Tile.X,Tile.Y].Figure<>nil then
        fISOMap.Figures[Tile.X,Tile.Y].Figure.KillUnit;
    end;
  end;
  {$ENDIF}
end;

procedure TDXIsoEngine.ZaehleTreffer;
begin
  inc(fStatistik.Treffer);
end;

function TDXIsoEngine.GetGroup(Index: Integer): TFigureGroup;
begin
  result:=fGroups[Index];
end;

procedure TDXIsoEngine.SetPause(const Value: boolean);
begin
  if fPause=Value then exit;
  fPause := Value;
  Rec.NotifyUIType:=nuitPauseChange;
  Rec.Bool:=Value;
  SendVMessage(vmNotifyUI);
end;

function TDXIsoEngine.SetCameraTo(Figure: TGameFigure): TGameFigure;
begin
  if (Figure<>nil) and (Figure.IsVisible) then
    ISOMap.CenterTo(Point(Figure.XPos,Figure.YPos));
  result:=fCameraUnit;
  fCameraUnit:=Figure;
end;

procedure TDXIsoEngine.UseObject(Figure: TGameFigure; Slot: TSoldatConfigSlot; SlotIndex: Integer; ThrowPos: TPoint; Throw: Boolean = false);
var
  Item      : TLagerItem;
  Obj       : TISOItem;
  Sensor    : TSensorItem;
  Mine      : TMineItem;
  Greanade  : TGreanadeTimer;
  Error     : TDragActionError;
begin
  if not Figure.Manager.GetItemInSlot(Slot,SlotIndex,Item) then
    exit;

  if Throw then
  begin
    // Einheit muss in die Richtung schauen, in der es das Object wirft
    if not Figure.ViewTo(ThrowPos) then
      exit;

    // Ausr�stung "verbrauchen" und neue "nachladen"
    Error:=Figure.Manager.UseObject(Slot,SlotIndex);
    if Error<>daeNone then
    begin
      Rec.Text:=Figure.Name+': '+DropErrors[Error];
      SendVMessage(vmTextMessage);
      exit;
    end;

    // Buttons im Interface aktualisieren
    Rec.NotifyUIType:=nuitSelectionChange;
    SendVMessage(vmNotifyUI);

    // Objekt werfen (Granate wird aktiviert)
    Obj:=TISOItem(CreateItemObject(Addr(Item),Figure,Point(Figure.XPos,Figure.YPos),Figure.GetUnitOffset));

    case Item.TypeID of
      ptGranate:
      begin
      // Sonderfall Granate die beim Wurf aktiviert wird
        Greanade:=TGreanadeTimer.Create(Self);
        Greanade.SetGreanadeInfo(Obj,GreanadeTime);
        Obj.SetLinkedObject(Greanade);

        // Wartet der Soldat auf die Best�tigung eines Laufzieles (fState = usWaitForOK)
        // dann wird angenommen, dass nach dem Benutzen einer Granate der Soldat zum Ziel
        // laufen soll
        Figure.GiveOK;
      end;
      ptSensor :
      begin
        Sensor:=TSensorItem.Create(Self);
        Sensor.SetSensorInfo(Item,Figure.XPos,Figure.YPos);
        Obj.SetLinkedObject(Sensor);
      end;
    end;
    Obj.ThrowTo(ThrowPos,Figure.Manager.Strength);

    Figure.BerechneExp(EPThrow,etWerfen);
  end
  else
  begin
    // Sensor oder Mine benutzen
    if not (Item.TypeID in [ptSensor,ptMine]) then
      exit;

    // Ausr�stung "verbrauchen" und neue "nachladen"
    Error:=Figure.Manager.UseObject(Slot,SlotIndex);
    if Error<>daeNone then
    begin
      Rec.Text:=Figure.Name+': '+DropErrors[Error];
      SendVMessage(vmTextMessage);
      exit;
    end;

    // Buttons im Interface aktualisieren
    Rec.NotifyUIType:=nuitSelectionChange;
    SendVMessage(vmNotifyUI);

    Obj:=TISOItem(CreateItemObject(Addr(Item),Figure,Point(Figure.XPos,Figure.YPos),Figure.GetUnitOffset));

    case Item.TypeID of
      ptSensor :
      begin
        Sensor:=TSensorItem.Create(Self);
        Sensor.SetSensorInfo(Item,Figure.XPos,Figure.YPos);
        Obj.SetLinkedObject(Sensor);
      end;
      ptMine :
      begin
        Mine:=TMineItem.Create(Self);
        Mine.SetMineInfo(Item,Figure.XPos,Figure.YPos);
        Mine.FigureStatus:=Figure.FigureStatus;

        Obj.SetLinkedObject(Mine);
        Mine.SetLinkedItem(Obj);
      end;
    end;
  end;
end;

function TDXIsoEngine.CenterOfSelected: TPoint;
var
  Dummy: Integer;
  Count: Integer;
  X,Y  : Integer;
begin
  X:=0;
  Y:=0;
  Count:=0;
  for Dummy:=0 to fFigures.Count-1 do
  begin
    if fFigures[Dummy].Selected then
    begin
      inc(X,fFigures[Dummy].XPos);
      inc(Y,fFigures[Dummy].YPos);
      inc(Count);
    end;
  end;
  if Count=0 then
    result:=Point(-1,-1)
  else
    result:=Point(X div Count,Y div Count);
end;

procedure TDXIsoEngine.CloseDoor(const Point: TPoint; WallType: TWallType);
var
  Door     : TDoorObject;
  OpenLeft : Boolean;
  Tile     : Integer;
  Entry    : PMapTileEntry;
begin
  Entry:=ISOMap.TileEntry(Point.X,Point.Y);
  ISOMap.GetDoorInfo(Entry^,WallType,Tile,OpenLeft);
  Door:=TDoorObject(FindDoorObject(Point,Tile,WallType,OpenLeft));
  Door.CloseDoor;
end;

procedure TDXIsoEngine.OpenDoor(const Point: TPoint; WallType: TWallType);
var
  Door     : TDoorObject;
  OpenLeft : Boolean;
  Tile     : Integer;
  Entry    : PMapTileEntry;
begin
  Entry:=ISOMap.TileEntry(Point.X,Point.Y);
  ISOMap.GetDoorInfo(Entry^,WallType,Tile,OpenLeft);
  Door:=TDoorObject(FindDoorObject(Point,Tile,WallType,OpenLeft));

  if Door.IsNew then
  begin
    case WallType of
      wtTLeft  : Entry.TLeft:=-1;
      wtTRight : Entry.TRight:=-1;
      wtBLeft  : Entry.BLeft:=-1;
      wtBRight : Entry.BRight:=-1;
    end;
    SendVMessage(vmTerrainChange);
  end;
  Door.OpenDoor;
end;

function TDXIsoEngine.FindDoorObject(const Point: TPoint; Tile: Integer;
  WallType: TWallType; OpenLeft: Boolean): TISOObject;
var
  Dummy: Integer;
  Door : TDoorObject;
  Sur  : TDirectDrawSurface;
begin
  for Dummy:=0 to fObjectList.Count-1 do
  begin
    if (TObject(fObjectList[Dummy]) is TDoorObject) then
    begin
      Door:=TDoorObject(fObjectList[Dummy]);
      if (Door.X=Point.X) and (Door.Y=Point.Y) and (Door.WallType=WallType) then
      begin
        result:=Door;
        exit;
      end;
    end;
  end;

  // T�r existiert an dieser stelle noch nicht => Neue erstellen
  result:=TDoorObject.Create(Self);
  result.SetPos(Point);
  case WallType of
    wtTRight,wtBLeft : Sur:=TDirectDrawSurface(ISOMap.GetTileSurface(RightIndex));
    wtTLeft,wtBRight : Sur:=TDirectDrawSurface(ISOMap.GetTileSurface(LeftIndex));
    else
      Sur:=nil;
  end;
  TDoorObject(result).SetDoorInfo(Sur,Tile,WallType,OpenLeft);
end;

procedure TDXIsoEngine.CreateFieldExplosion(Explosion: TExplosionInfo);
begin
  CheckPositionAssert(Explosion.ToPos);
  with TFieldExplosion.Create(Self) do
  begin
    SetImage(fAGFFiles.GetAGFImage('MineExplosion'));
    SetExplosionParams(Explosion);
    Figure:=TGameFigure(Explosion.Figure);
  end;
end;

procedure TDXIsoEngine.GetObjects(var Rec: TMessageRecord);
begin
  CheckPositionAssert(Rec.Point);
  with fISOMap.Figures[Rec.Point.X,Rec.Point.Y] do
  begin
    SetLength(Rec.Objects,ObjectCount);

    // Steht erst nach dem Setlength, um eventuell vorherige Eintr�ge in der Liste
    // zu l�schen
    if ObjectCount=0 then
      exit;
    CopyMemory(Addr(Rec.Objects[0]),Addr(Objects[0]),ObjectCount*4);
  end;
end;

procedure TDXIsoEngine.CreateExplosion(Figure: TGameFigure; const Pos: TPoint; Range,
  Strength: Integer;Power: Integer);
var
  XDum,YDum : Integer;
  Entf      : double;
  HasExpl   : Array of Array of Boolean;

  procedure Explosion(X,Y: Integer;const Last: TPoint; OnlyWall: Boolean);
  begin
    if HasExpl[X-Pos.X+Range,Y-Pos.Y+Range]=true then
      exit;

    Entf:=Sqrt(Sqr(abs(Pos.x-X))+Sqr(abs(Pos.Y-Y)));

    Rec.Explosion.Power:=round(max(Range-max(0,(Entf-(Range div 4))),Range div 4)/Range*Power);
    Rec.Explosion.Delay:=Entf*2;
    Rec.Explosion.FromPos:=Last;
    Rec.Explosion.ToPos:=Point(X,Y);
    Rec.Explosion.Figure:=Figure;
    Rec.Explosion.OnlyWall:=OnlyWall;

    SendVMessage(vmCreateFieldExplosion);

    HasExpl[X-Pos.X+Range,Y-Pos.Y+Range]:=true;
  end;

  procedure CreateExplosions(FromX,FromY,ToX,ToY: Integer);
  var
    RX,RY     : Integer;
    LastPoint : TPoint;
    X,Y       : double;
    Steps     : Integer;
    XStep     : double;
    YStep     : double;
    WayFree   : TWayFreeResult;
    Breaking  : Boolean;
  begin
    Steps:=max(abs(ToX-FromX),abs(ToY-FromY));
    if Steps=0 then
      exit;
    XStep:=(ToX-FromX) / Steps;
    YStep:=(ToY-FromY) / Steps;
    X:=FromX;
    Y:=FromY;
    LastPoint:=Point(FromX,FromY);
    while Steps>0 do
    begin
      X:=X+XStep;
      Y:=Y+YStep;
      RX:=Round(X);
      RY:=Round(Y);

      if (RX<0) or (RX>=ISOMap.Width) or (RY<0) or (RY>=ISOMap.Height) then
        exit;

      // Pr�fen, ob der Weg frei ist
      ISOMap.IsWayFree(Point(RX,RY),LastPoint,WayFree);

      Breaking:=WayFree.WayBlocked in [wbDoor,wbObject,wbTransparentWall,wbWall,wbOutOfBounds];

      // wenn Breaking true ist wird nur noch Schaden an der Mauer erzeugt und
      // es wird keine Animation angezeigt
      Explosion(RX,RY,LastPoint, Breaking);

      if Breaking then
        exit;

      LastPoint:=Point(RX,RY);
      dec(Steps);
    end;
  end;

begin
  Rec.Explosion.Strength:=Strength;
  SetLength(HasExpl,(Range*2)+1,(Range*2)+1);

  // Explosion auf dem Explosionsherd erzeugen
  Explosion(Pos.X,Pos.Y,Pos,false);

  for YDum:=-Range to Range do
  begin
    for XDum:=-Range to Range do
    begin
      if not HasExpl[XDum+Range,YDum+Range] then
      begin
        Entf:=Sqrt(Sqr(XDum)+Sqr(YDum));
        if (Entf<Range) then
        begin
          with Pos do
            CreateExplosions(X,Y,XDum+X,YDum+Y);
        end;
      end;
    end;
  end;
end;

function TDXIsoEngine.CanFriendsMove: Boolean;
begin
  result:=fEchtZeit or (fRoundState.ZugSeite=zsFriend) or ((fMovingList.Count>0) and (TGameFigure(fMovingList[0]).FigureStatus=fsFriendly));
end;

function TDXIsoEngine.GetObjectList: TList;
begin
  result:=fObjectList;
end;

procedure TDXIsoEngine.CreateWayPoint(Point: TPoint;Direct: TViewDirection;Color: Integer; Show: Boolean);
var
  Dummy: Integer;
begin
  if not CheckPosition(Point) then
    exit;

  if Show then // Waypoint anlegen
  begin
    if Echtzeit then
      Color:=3;
    // Pr�fen, ob es vielleicht auf dem Feld bereits ein Wegpunkt gibt
    with ISOMap.TileState(Point.X,Point.Y)^ do
    begin
      for Dummy:=0 to ObjectCount-1 do
      begin
        if TObject(Objects[Dummy]) is TWayPointMarker then
        begin
          // Es existiert bereits ein Wegpunkt
          with TWayPointMarker(Objects[Dummy]) do
          begin
            Direction:=Direct;
            ColorIndex:=Color;
            Deleted:=false;
            Visible:=true;
          end;
          exit;
        end;
      end;
    end;

    with TWayPointMarker.Create(Self) do
    begin
      SetPos(Point);
      Direction:=Direct;
      ColorIndex:=Color;
    end;
  end
  else
  begin
    with ISOMap.TileState(Point.X,Point.Y)^ do
    begin
      for Dummy:=0 to ObjectCount-1 do
      begin
        if TObject(Objects[Dummy]) is TWayPointMarker then
        begin
          // Es existiert bereits ein Wegpunkt
          Objects[Dummy].Deleted:=true;
          Objects[Dummy].Visible:=false;
          exit;
        end;
      end;
    end;
  end;


end;

function TDXIsoEngine.CreateFigureObject(Figure: TGameFigure): TISOObject;
begin
  result:=TFigureObject.Create(Self);
  TFigureObject(result).Figure:=Figure;
end;

procedure TDXIsoEngine.FreeImages;
var
  Dummy: Integer;
begin
  for Dummy:=0 to fSList.Count-1 do
    TDirectDrawSurface(fSList[Dummy]).SetSize(1,1);

  fAGFFiles.FreeImages;
end;

function TDXIsoEngine.GetModelAnimation(Model: String): TXANBitmap;
var
  Name  : String;
  Dummy : Integer;
begin
  Name:=Model;

  result:=nil;
  for Dummy:=0 to high(XANImages) do
  begin
    if XANImages[Dummy].Name=Name then
    begin
      result:=TXANBitmap(fXANList[Dummy]);
      break;
    end;
  end;
end;

function TDXIsoEngine.GetAGFImage(Name: String): TAGFImage;
begin
  result:=fAGFFiles.GetAGFImage(Name);
end;

function TDXIsoEngine.GetSurface(Name: String): TDirectDrawSurface;
var
  Dummy: Integer;
begin
  Name:=UpperCase(Name);
  for Dummy:=0 to high(ImageNames) do
  begin
    if Name=UpperCase(Copy(ImageNames[Dummy],Pos(':',ImageNames[Dummy])+1,200)) then
    begin
      result:=TDirectDrawSurface(fSList[Dummy]);
      exit;
    end;
  end;
  raise Exception.Create('Surface '+Name+' nicht gefunden');
end;

function TDXIsoEngine.BreakRound(WantUnit, BreakingUnit: TGameFigure): Boolean;
var
  Chance: Integer;
begin
  result:=false;

  if IsBreakingRound and (WantUnit.FigureStatus<>TGameFigure(fMovingList[0]).FigureStatus) then
    exit;

  if BreakingUnit.LastVisible then
    exit;

  if WantUnit.Manager=nil then
    exit;

  if WantUnit.FigureStatus=fsNeutral then
    exit;

  if CanUnitMove(WantUnit) then
    exit;
{  if fRoundState.ZugSeite=zsFriend then
  begin
    if WantUnit.FigureStatus=fsFriendly then
      exit;
  end
  else if fRoundState.ZugSeite=zsEnemy then
  begin
    if WantUnit.FigureStatus=fsEnemy then
      exit;
  end;}

  Chance:=WantUnit.Manager.Reaktion;

  if BreakingUnit.CanSeeUnit(WantUnit) then
  begin
    Chance:=Chance-BreakingUnit.Manager.Reaktion;
    if Chance<=0 then
      exit;
  end;

  Chance:=round(Chance*(WantUnit.Zeiteinheiten/WantUnit.Manager.UnitZeitEinheiten));

  if not RandomChance(Chance) then
    exit;

  result:=true;

  // Runde wird durch die Einheit unterbrochen
  fMovingList.Insert(0,WantUnit);

  // Einheit ausw�hlen
  WantUnit.Select;
end;

procedure TDXIsoEngine.CheckPositionAssert(Point: TPoint);
begin
  Assert(CheckPosition(Point));
end;

procedure TDXIsoEngine.CheckPositionAssert(X, Y: Integer);
begin
  Assert(CheckPosition(X,Y));
end;

function TDXIsoEngine.IsBreakingRound: Boolean;
begin
  result:=(fMovingList.Count>0) and (TGameFigure(fMovingList[0]).FigureStatus<>ZugSeiteToFriendStatus[fRoundState.ZugSeite]);
end;

procedure TDXIsoEngine.TerrainChange;
begin
  if InPerform>0 then
  begin
    fTerrainChanged:=true;
    exit;
  end;

  if not fTerrainChanged then
    exit;

  Figures.AktuSeeing;
  fTerrainChanged:=false;
end;

function TDXIsoEngine.CheckPosition(Point: TPoint): Boolean;
begin
  result:=(Point.X>=0) and (Point.X<fISOMap.Width);
  result:=result and (Point.Y>=0) and (Point.Y<fISOMap.Height);
end;

function TDXIsoEngine.CheckPosition(X, Y: Integer): Boolean;
begin
  result:=(X>=0) and (X<fISOMap.Width);
  result:=result and (Y>=0) and (Y<fISOMap.Height);
end;

procedure TDXIsoEngine.DoGroupMove;

  procedure SetLastUnit;
  begin
    if fLastUnit<>TGameFigure(fMovingList[0]) then
    begin
      TGameFigure(fMovingList[0]).ResetNeedTUState;
      fLastUnit:=TGameFigure(fMovingList[0]);

      SendVMessage(vmEngineRedraw);
    end;
  end;

begin
  if fPause then
    exit;
    
  if fMovingList.Count=0 then
  begin
    NextRound(fRoundState.ZugSeite);
    exit;
  end;

  if (fLastUnit=nil) or (fLastUnit.FigureStatus<>TGameFigure(fMovingList[0]).FigureStatus) then
  begin
    // Aktionen ausf�hren, wenn sich die Seite ge�ndert hat, die aktuell am Zug ist
    if TGameFigure(fMovingList[0]).FigureStatus=fsEnemy then
    begin
      inc(fSperre);
      // Sichern der Figur die zur Zeit von der Kamera verfolgt wird
      if not fEchtzeit then
      begin
        rec.Figure:=nil;
        SendVMessage(vmKameraVerfolgung);
        // Nachricht liefert die Einheit davor zur�ck
        fLastCapturedFig:=TGameFigure(rec.Figure);
      end;

      fEnemyKI.ZugSideChange;
    end
    else
    begin
      dec(fSperre);
      rec.Figure:=fLastCapturedFig;
      SendVMessage(vmKameraVerfolgung);
    end;

    rec.NotifyUIType:=nuitNextRound;
    SendVMessage(vmNotifyUI);

    SetLastUnit;
  end
  else
  begin
    SetLastUnit;
    
    // Den Zug des Gegners ausf�hren
    if TGameFigure(fMovingList[0]).FigureStatus=fsEnemy then
      fEnemyKI.DoUnitRound;
  end;

//  SendVMessage(vmEngineRedraw);

end;

function TDXIsoEngine.CanUnitMove(Figure: TGameFigure): Boolean;
var
  Dummy: Integer;
begin
  result:=false;
  for Dummy:=0 to fMovingList.Count-1 do
  begin
    if TGameFigure(fMovingList[Dummy]).FigureStatus<>Figure.FigureStatus then
      exit;

    if fMovingList[Dummy]=Figure then
    begin
      result:=true;
      exit;
    end;
  end;
end;

function TDXIsoEngine.GenerateMouseHint: Boolean;
var
  ShootModus: Boolean;
  ZielPos   : TPoint;
  FromPos   : TPoint;
  WayFree   : TWayFreeResult;
begin
  result:=false;
  if Assigned(fOnTileClick) then
    exit;

  if not PointInRect(Container.MousePos,ClientRect) then
  begin
    Container.SetCursor(CZeiger);
    TMouseHintInfoObject(fMouseHint).ShowInfoText(false);
    exit;
  end;

  if fFigures.Selected=nil then
    ShootModus:=false
  else
  begin
    rec.Figure:=fOverFigure;
    SendVMessage(vmCanDrawUnit);
    ShootModus:=((rec.result) and (fOverFigure.FigureStatus=fsEnemy)) or (GetKeyState(VK_SHIFT)<0);
    FromPos:=Point(fFigures.Selected.XPos,fFigures.Selected.YPos);
  end;

  if ShootModus then
  begin
    if fOverFigure<>nil then
      ZielPos:=Point(fOverFigure.XPos,fOverFigure.YPos)
    else
      ZielPos:=fISOMap.TileAtPos(Container.MousePos.X,Container.MousePos.Y,false);

    if not CheckPosition(ZielPos) then
    begin
      Container.SetCursor(CZeiger);
      TMouseHintInfoObject(fMouseHint).ShowInfoText(false);
      exit;
    end;
    fISOMap.IsWayFree(FromPos,ZielPos,WayFree);
    if WayFree.WayBlocked in [wbNone,wbTransparentWall,wbInvalidField,wbObject,wbSoldat,wbFigure] then
    begin
      Container.SetCursor(CAuswahl);

      TMouseHintInfoObject(fMouseHint).ShowText:=fFigures.Selected.GetTargetInfoText(ZielPos,Container.MousePos.X,Container.MousePos.Y);

      TMouseHintInfoObject(fMouseHint).ShowInfoText(true);
      result:=true;
    end
    else
    begin
      Container.SetCursor(CZeiger);
      TMouseHintInfoObject(fMouseHint).ShowText:=CR0505290001;
      TMouseHintInfoObject(fMouseHint).ShowInfoText(true);
      result:=true;
    end;
  end
  else
  begin
    Container.SetCursor(CZeiger);
    TMouseHintInfoObject(fMouseHint).ShowInfoText(false);
  end;
end;

procedure TDXIsoEngine.MouseLeave;
begin
  inherited;

  if not Assigned(fOnTileClick) then
    Container.SetCursor(CZeiger);
end;

{ TISOMap }

function TISOMap.CanDrawFigure(Figure: TGameFigure): boolean;
begin
  if Figure=nil then
    result:=false
  else if not Figure.IsOnField then
    result:=false
  else if Figure.FigureStatus=fsFriendly then
    result:=true
  else
    result:=fTiles[Figure.XPos,Figure.YPos].AlphaV.All>1;
end;

procedure TISOMap.CenterTo(Pos: TPoint);
var
  TileRect : TRect;
  TilePos  : TPoint;
begin
  if (Pos.X=-1) or (Pos.Y=-1) then
    exit;
    
  TileRect:=fISOEngine.GetTileRect(Self,Pos);
  TilePos.X:=TileRect.Left+((TileRect.Right-TileRect.Left) shr 1);
  TilePos.Y:=TileRect.Top+((TileRect.Bottom-TileRect.Top) shr 1);
  fISOEngine.ScrollBy(TilePos.X-(fISOEngine.Width shr 1),TilePos.Y-(fISOEngine.Height shr 1));
end;

procedure TISOMap.Clear;
begin
  SetLength(Figures,0,0);
  SetLength(fTiles,0,0);
  SetLength(fUnitRects,0);

  fWidth:=0;
  fHeight:=0;

  fTileGroup.Clear;
end;

constructor TISOMap.Create(Container: TDXContainer);
begin
  fDDraw:=Container.DDraw;
  fMouseMap:=TBitmap.Create;
  fPathFinder:=TPathFinder.Create;
  fPathFinder.IsWayFree:=IsWayFree;

  fMapGenerator:=TMapGenerator.Create;
  fMapGenerator.SetTileFunc:=SetTile;
  fMapGenerator.OnSetMapSize:=SetDimension;

  fTileGroup:=TDXISOTileGroup.Create(Container);
end;

procedure TISOMap.CreateMap(Enemys: Integer);
var
  X,Y   : Integer;
  Size  : TSize;
begin
  fMapGenerator.Generate(Enemys) ;

  Size:=TSize(Point(fMapGenerator.MapWidth*15,fMapGenerator.MapHeight*15));

  IsoEngine.fEnemyKI.MapSize:=Size;
  IsoEngine.fOwnKI.MapSize:=Size;

  // Sicherstellen, dass die Aliens nicht in der N�he der Aliens starten
  IsoEngine.fEnemyKI.SetForbiddenZone(IsoEngine.fOwnKI.StartingPoint,20);

  for X:=0 to fWidth-1 do
  begin
    for Y:=0 to fHeight-1 do
    begin
      with fTiles[X,Y] do
      begin
        FillChar(AlphaV,SizeOf(AlphaV),#0);
        FillChar(BlockInfo,SizeOf(BlockInfo),#0);
        RecalcAlpha(X,Y);
        if HotSpot=1 then
          IsoEngine.fOwnKI.AddSpawnPoint(Point(X,Y))
        else if HotSpot=2 then
          IsoEngine.fEnemyKI.AddSpawnPoint(Point(X,Y))
      end;
    end;
  end;
end;

procedure TISOMap.DecSeeing(Figure: TGameFigure);
var
  X,Y   : Integer;
  YP,XP : Integer;
  XA,YA : Integer;
  XB,YB : Integer;
  Ptr   : PSeeTile;
begin
//  if Figure.FigureStatus<>fsEnemy then exit;
  XP:=Figure.IncreasePos.X-Figure.SehWeite;
  for X:=0 to (Figure.SehWeite*2) do
  begin
    if (XP>=0) and (XP<fWidth) then
    begin
      YP:=Figure.IncreasePos.Y-Figure.SehWeite;
      Ptr:=Addr(Figure.SeeingTiles[X,0]);
      for Y:=0 to (Figure.SehWeite*2) do
      begin
        if (YP>=0) and (YP<fHeight) then
        begin
          if Ptr.HasIncrease then
          begin
            Ptr.HasIncrease:=false;
            {$IFNDEF FULLMAP}
            if Figure.FigureStatus in [fsFriendly,fsObject] then
            begin
              fTiles[XP,YP].AlphaV.All:=max(0,fTiles[XP,YP].AlphaV.All-Ptr.SeeingRange);
              RecalcAlpha(XP,YP);
              if LightDetail=ldDoppelt then
              begin
                fAlphaMap[(XP*2),(YP*2)]:=max(0,fAlphaMap[(XP*2),(YP*2)]-Figure.AlphaMap[(X*2),(Y*2)]);
                fAlphaMap[(XP*2),(YP*2)+1]:=max(0,fAlphaMap[(XP*2),(YP*2)+1]-Figure.AlphaMap[(X*2),(Y*2)+1]);
                fAlphaMap[(XP*2)+1,(YP*2)]:=max(0,fAlphaMap[(XP*2)+1,(YP*2)]-Figure.AlphaMap[(X*2)+1,(Y*2)]);
                fAlphaMap[(XP*2)+1,(YP*2)+1]:=max(0,fAlphaMap[(XP*2)+1,(YP*2)+1]-Figure.AlphaMap[(X*2)+1,(Y*2)+1]);
              end
              else if LightDetail=ldVierfach then
              begin
                XB:=X*4;
                for XA:=(XP*4) to (XP*4)+3 do
                begin
                  YB:=Y*4;
                  for YA:=(YP*4) to (YP*4)+3 do
                  begin
                    fAlphaMap[XA,YA]:=max(0,fAlphaMap[XA,YA]-Figure.AlphaMap[XB,YB]);
                    inc(YB);
                  end;
                  inc(XB);
                end;
              end;
            end;
            {$ENDIF}
          end;
        end;
        inc(Ptr);
        inc(YP);
      end;
    end;
    inc(XP);
  end;
end;

destructor TISOMap.destroy;
begin
  fMapGenerator.Free;
  fMouseMap.Free;
  fPathFinder.Free;
  fTileGroup.Free;
  CloseModels;
  inherited;
end;

procedure TISOMap.DrawRect(Surface: TDirectDrawSurface; Rect: TRect);
var
  X,Y       : integer;
  XPos,YPos : integer;
  Mem       : TDDSurfaceDesc;
begin
  if not fISOEngine.fPlayGame then
    exit;

  if IsRectEmpty(Rect) then
    exit;

  fDRect:=Rect;

  fTempSur.FillRect(Rect,0);
  fTempSur.Lock(Mem);
  fTempSur.Unlock;

  fTempTile.Lock(fTempMem);
  fTempTile.Unlock;

  Surface.ClippingRect:=IsoEngine.ClientRect;

  YPos:=0;
  Y:=YOff;
  while (YPos<fHeight) and (y<=Rect.Bottom+WallHeight) do
  begin
    x:=((Surface.Width div 2)-(YPos*HalfTileWidth))+XOff;
    XPos:=0;
    while (XPos<fWidth) and (x-DrawingRangeX<Rect.Right) do
    begin
      if (x>Rect.Left-DrawingRangeX) and (y>Rect.Top-WallHeight) and
         (x-DrawingRangeX<Rect.Right) and (y-WallHeight<Rect.Bottom) then
      begin
      RenderFloor(XPos,YPos,fTempSur,Mem,X,Y);
      end;
      inc(x,HalfTileWidth);
      inc(y,HalfTileHeight);
      inc(XPos);
    end;
    inc(ypos);
    y:=(YPOS*HalfTileHeight)+YOff;
  end;
  YPos:=0;
  Y:=YOff;

  while (YPos<fHeight) and (y<=Rect.Bottom+WallHeight) do
  begin
    x:=((Surface.Width div 2)-(YPos*HalfTileWidth))+XOff;
    XPos:=0;
    while (XPos<fWidth) and (x-DrawingRangeX<Rect.Right) do
    begin
      if (x>Rect.Left-DrawingRangeX) and (y>Rect.Top-WallHeight) and
         (x-DrawingRangeX<Rect.Right) and (y-WallHeight<Rect.Bottom) then
      begin
        RenderTile(XPos,YPos,fTempSur,Mem,X,Y);
      end;
      inc(x,HalfTileWidth);
      inc(y,HalfTileHeight);
      inc(XPos);
    end;
    inc(ypos);
    y:=(YPOS*HalfTileHeight)+YOff;
  end;

  Surface.Draw(Rect.Left,Rect.Top,Rect,fTempSur,false);

  Surface.ClearClipRect;
end;

function TISOMap.GetMaxXOffSet: Integer;
var
  xTemp: Integer;
begin
  xTemp:=fWidth;
  result:=(xTemp*HalfTileWidth)-round(fVWidth*1.5)+TileWidth;
end;

function TISOMap.GetMaxYOffSet: Integer;
var
  yTemp: Integer;
begin
  yTemp:=ceil((fWidth+fHeight)/2);
  result:=(yTemp*HalfTileWidth)-fVHeight+TileHeight;
end;

function TISOMap.GetMinXOffSet: Integer;
begin
  result:=-(fHeight*HalfTileWidth)+(fVWidth div 4)-TileWidth;
end;

procedure TISOMap.IncSeeing(Figure: TGameFigure);
var
  X,Y           : Integer;
  LastPoint     : TPoint;
  XA,YA         : Integer;
  SehWeite      : Integer;
  Tile          : PSeeTile;

  function SetField(RX,RY: Integer;LastPoint: TPoint): boolean;
  var
    PX,PY     : Integer;
    SeeTile   : PSeeTile;
    Blocked   : TWayFreeResult;
    XA,YA     : Integer;
    XB,YB     : Integer;
  begin
    // Beginn Sehbereich festlegen
    result:=false;
    IsWayFree(LastPoint,Point(RX,RY),Blocked);
    if Blocked.WayBlocked=wbOutOfBounds then
      exit;
    if {(Figure.FigureStatus<>fsObject) and} (Blocked.WayBlocked in [wbWall,wbDoor]) then
      exit;
    PX:=RX-Figure.XPos+Sehweite;
    PY:=RY-Figure.YPos+Sehweite;

    SeeTile:=Addr(Figure.SeeingTiles[PX,PY]);
    if SeeTile.SeeingRange=0 then
      exit;
    if (not SeeTile.HasIncrease) then
    begin
      IsoEngine.CheckPositionAssert(RX,RY);
      with fTiles[RX,RY] do
      begin
        SeeTile.HasIncrease:=true;
        {$IFNDEF FULLMAP}
        if Figure.FigureStatus in [fsFriendly,fsObject] then
        begin
          inc(AlphaV.All,SeeTile.SeeingRange);
          AlphaV.Discover:=true;
          if LightDetail=ldDoppelt then
          begin
            inc(fAlphaMap[RX*2,RY*2],Figure.AlphaMap[PX*2,PY*2]);
            inc(fAlphaMap[(RX*2),(RY*2)+1],Figure.AlphaMap[(PX*2),(PY*2)+1]);
            inc(fAlphaMap[(RX*2)+1,(RY*2)],Figure.AlphaMap[(PX*2)+1,(PY*2)]);
            inc(fAlphaMap[(RX*2)+1,(RY*2)+1],Figure.AlphaMap[(PX*2)+1,(PY*2)+1]);
          end
          else if LightDetail=ldVierfach then
          begin
            XB:=PX*4;
            for XA:=(RX*4) to (RX*4)+3 do
            begin
              YB:=PY*4;
              for YA:=(RY*4) to (RY*4)+3 do
              begin
                inc(fAlphaMap[XA,YA],Figure.AlphaMap[XB,YB]);
                inc(YB);
              end;
              inc(XB);
            end;
          end;
          DiscoverField(RX,RY);
          RecalcAlpha(RX,RY);
        end;
        {$ENDIF}
      end;
    end;
    if {(Figure.FigureStatus<>fsObject) and} (Blocked.WayBlocked=wbObject) then exit;
    result:=true;
    // Ende Sehbereich festlegen
  end;

  procedure IncLine(FromX,FromY,ToX,ToY: Integer);
  var
    RX,RY     : Integer;
    LastPoint : TPoint;
    X,Y       : double;
    Steps     : Integer;
    XStep     : double;
    YStep     : double;
  begin
    Steps:=max(abs(ToX-FromX),abs(ToY-FromY));
    if Steps=0 then
      exit;
    XStep:=(ToX-FromX) / Steps;
    YStep:=(ToY-FromY) / Steps;
    X:=FromX;
    Y:=FromY;
    LastPoint:=Point(FromX,FromY);
    while Steps>0 do
    begin
      X:=X+XStep;
      Y:=Y+YStep;
      RX:=Round(X);
      RY:=Round(Y);
      if (RX<0) or (RX>=fWidth) or (RY<0) or (RY>=fHeight) then exit;
      if not SetField(RX,RY,LastPoint) then
        exit;
      LastPoint:=Point(RX,RY);
      dec(Steps);
    end;
  end;

begin
  if (not Figure.IsOnField) and (Figure.FigureStatus<>fsObject) then exit;
  X:=Figure.XPos;
  Y:=Figure.YPos;

  fISOEngine.CheckPositionAssert(X,Y);

  Figure.IncreasePos:=Point(X,Y);
  LastPoint:=Point(x,y);
  SehWeite:=Figure.SehWeite;
  if (x>=0) and (y>=0) and (x<fWidth) and (y<fHeight) then
    SetField(X,Y,Point(X,Y));
  for XA:=X-Sehweite to X+Sehweite do
  begin
    Assert((XA-X+SehWeite>=low(Figure.SeeingTiles)) and (XA-X+SehWeite<=high(Figure.SeeingTiles)),'Ung�ltiger Index SeeingTiles');
    Tile:=Addr(Figure.SeeingTiles[XA-X+SehWeite,0]);
    for YA:=Y-Sehweite to Y+Sehweite do
    begin
      if ((XA<>X) or (YA<>Y)) and (not Tile.HasIncrease) then
        IncLine(X,Y,XA,YA);
      inc(Tile);
    end;
  end;
  Figure.AktuSeeingList;
end;

procedure TISOMap.IsWayFree(FromP, ToP: TPoint; var result: TWayFreeResult);
var
  RX,RY     : Integer;
  LastPoint : TPoint;
  X,Y       : double;
  Steps     : Integer;
  XStep     : double;
  YStep     : double;
  WayFree   : TWayFreeResult;
  Index     : Integer;
  Figure    : TGameFigure;
  Dummy     : Integer;
  BType     : PBlockType;
begin
  if (abs(FromP.X-ToP.X)<=1) and (Abs(FromP.Y-ToP.Y)<=1) then
  begin
    fISOEngine.CheckPositionAssert(FromP);
    DiscoverField(FromP.X,FromP.Y);
    Index:=((ToP.Y-FromP.Y+1)*3)+(ToP.X-FromP.X+1);
    if Index=4 then
    begin
      result.WayBlocked:=wbNone;
      exit;
    end;
    if Index>=5 then
      dec(Index);

    Assert(Index<=7);

    result.WayBlocked:=fTiles[FromP.X,FromP.Y].BlockInfo.BlockType[Index].Block;

    // Pr�fen, ob die T�ren ge�ffnet sind
    if (result.WayBlocked=wbDoor) then
    begin
      BType:=Addr(fTiles[FromP.X,FromP.Y].BlockInfo.BlockType[Index]);
      result.WayBlocked:=wbNone;
      for Dummy:=0 to high(BType.Doors) do
      begin
        if (BType.Doors[Dummy]^)<>-1 then
        begin
          result.WayBlocked:=wbDoor;
          break;
        end;
      end;
    end;

    if (result.WayBlocked=wbNone) then
    begin
      Figure:=GetFigureAtPos(ToP);
      if Figure<>nil then
      begin
        result.WayBlocked:=wbSoldat;
        result.BlockObj:=Figure;
      end;
    end;
  end
  else
  begin
    result.WayBlocked:=wbNone;
    result.BlockObj:=nil;
    
    Steps:=max(abs(ToP.X-FromP.X),abs(ToP.Y-FromP.Y));
    if Steps=0 then
      exit;
    XStep:=(ToP.X-FromP.X) / Steps;
    YStep:=(ToP.Y-FromP.Y) / Steps;
    X:=FromP.X;
    Y:=FromP.Y;
    LastPoint:=Point(FromP.X,FromP.Y);
    while Steps>0 do
    begin
      X:=X+XStep;
      Y:=Y+YStep;
      RX:=Round(X);
      RY:=Round(Y);
      if (RX<0) or (RX>=Width) or (RY<0) or (RY>=Height) then
        exit;

      IsWayFree(Point(RX,RY),LastPoint,WayFree);

      if WayFree.WayBlocked in [wbDoor,wbTransparentWall,wbInvalidField,wbObject,wbWall,wbOutOfBounds] then
      begin
        if result.WayBlocked<WayFree.WayBlocked then
          result:=WayFree;
      end;

      LastPoint:=Point(RX,RY);
      dec(Steps);
    end;
  end;
end;

procedure TISOMap.LoadFromFile(FileName: String);
var
  f       : TFileStream;
  ID      : Cardinal;
begin
  f:=TFileStream.Create(FileName,fmOpenRead);
  f.Read(ID,SizeOf(ID));
  if not ValidMapVersion(ID) then
  begin
    f.Free;
    raise Exception.Create('Ung�ltige Karte');
  end;

  fTileGroup.ReadUsedTiles(f);

  IncEinsatzLoadProgress;

  f.Position:=0;
  fMapGenerator.LoadMapData(f);

  f.Free;
end;

procedure TISOMap.AktuWallAlpha(X,Y: Integer);
begin
  fISOEngine.CheckPositionAssert(X,Y);

  with fTiles[X,Y] do
  begin
    // Berechnung Alpha Oben Rechts
    if DoorInfo.TRight<>-1 then
    begin
      AlphaV.ATRight:=-1;
      if PTopRight<>nil then
      begin
        if PTopRight.AlphaV.Discover or AlphaV.Discover then
          AlphaV.ATRight:=max(0,min(7,max(PTopRight.AlphaV.All,AlphaV.All)-1));
      end
      else if AlphaV.Discover then
        AlphaV.ATRight:=max(0,min(7,AlphaV.All-1));
    end;

    // Berechnung Alpha Oben Links
    if DoorInfo.TLeft<>-1 then
    begin
      AlphaV.ATLeft:=-1;
      if PTopLeft<>nil then
      begin
        if PTopLeft.AlphaV.Discover or AlphaV.Discover then
          AlphaV.ATLeft:=max(0,min(7,max(PTopLeft.AlphaV.All,AlphaV.All)-1));
      end
      else if AlphaV.Discover then
        AlphaV.ATLeft:=max(0,min(7,AlphaV.All-1));
    end;

    // Berechnung Alpha Unten Links
    if DoorInfo.BLeft<>-1 then
    begin
      AlphaV.ABLeft:=-1;
      if PBottomLeft<>nil then
      begin
        if PBottomLeft.AlphaV.Discover or AlphaV.Discover then
          AlphaV.ABLeft:=max(0,min(7,max(PBottomLeft.AlphaV.All,AlphaV.All)-1));
      end
      else if AlphaV.Discover then
        AlphaV.ABLeft:=max(0,min(7,AlphaV.All-1));
    end;

    // Berechnung Alpha Unten Rechts
    if DoorInfo.BRight<>-1 then
    begin
      AlphaV.ABRight:=-1;
      if PBottomRight<>nil then
      begin
        if PBottomRight.AlphaV.Discover or AlphaV.Discover then
          AlphaV.ABright:=max(0,min(7,max(PBottomRight.AlphaV.All,AlphaV.All)-1));
      end
      else if AlphaV.Discover then
        AlphaV.ABright:=max(0,min(7,AlphaV.All-1));
    end;
  end;
end;

procedure TISOMap.RecalcAlpha(X, Y: Integer);
begin
  fISOEngine.CheckPositionAssert(X,Y);

  with fTiles[X,Y].AlphaV do
  begin
    if Discover then
      NAll:=max(0,min(7,All-1))
    else
      NAll:=-1;
    Lightning:=(LightDetail<>ldEinfach) and (NAll<>7) and (NAll<>-1);
    if Lightning then NAll:=7;

    AktuWallAlpha(X,Y);
    if Y>0 then
      AktuWallAlpha(X,Y-1);
    if X>0 then
      AktuWallAlpha(X-1,Y);
    if Y<fHeight-1 then
      AktuWallAlpha(X,Y+1);
    if X<fWidth-1 then
      AktuWallAlpha(X+1,Y);
  end;
end;

procedure TISOMap.RenderFloor(X, Y: Integer; Surface: TDirectDrawSurface;
  var Mem: TDDSurfaceDesc; XPos, YPos: Integer);
var
  Alpha     : Integer;
  {$IFDEF SHOWCOORDINATION}
  Font      : TFont;
  {$ENDIF}
begin
//  if not OverlapRect(Rect(Xpos-1,Ypos-1,Xpos+49,YPos+29),fDRect) then exit;
  fISOEngine.CheckPositionAssert(X,Y);

  with fTiles[X,Y] do
  begin
    { Zeichnen des Bodens }
    if AlphaV.Discover then
    begin
      Alpha:=AlphaV.NAll;
      fTileGroup.DrawFloor(Surface,XPos,YPos,Floor,Alpha);
//      Surface.Draw(xPos,YPos,Rect(Floor*TileWidth,Alpha*TileHeight,(Floor+1)*TileWidth,(Alpha+1)*TileHeight),TDirectDrawSurface(fTSet[FloorIndex]));
      if AlphaV.Lightning then
      begin
        if LightDetail=ldDoppelt then
          RenderLightNormalEffect(X,Y,Surface,Mem,XPos,YPos)
        else
          RenderLightHighEffect(X,Y,Surface,Mem,XPos,YPos)
      end;
    end
    else
    begin
      Surface.Draw(xPos,YPos,BlankFieldRect,TDirectDrawSurface(ISOEngine.fSList[0]));
    end;
    if ShowGrid then
      Surface.Draw(xPos,yPos,GridFieldRect,TDirectDrawSurface(ISOEngine.fSList[0]));

    {$IFDEF SHOWCOORDINATION}
    Font:=TFont.Create;
    Font.Assign(WhiteStdFont.Font);
    Font.Size:=6;
    Font.Name:='SmallFonts';
    FontEngine.TextOut(Font,Surface,XPos+10,YPos+5,Format('%d,%d',[X,Y]),clBlack);
    Font.Free;
    {$ENDIF}
  end;
end;

procedure TISOMap.RenderLightHighEffect(X, Y: Integer;
  Surface: TDirectDrawSurface; var Mem: TDDSurfaceDesc; XPos,
  YPos: Integer);
var
  XD,YD                : Integer;
  MemSource            : TDDSurfaceDesc;
  PoiSource            : Cardinal;
  PoiDest              : Cardinal;
  RMask,BMask,GMask    : Integer;
  Alpha                : Integer;
  RValue,BValue,GValue : Integer;
  LightArea            : Integer;
begin
  TDirectDrawSurface(fISOEngine.fSList[0]).Lock(MemSource);
  RMask:=Mem.ddpfPixelFormat.dwRBitMask;
  BMask:=Mem.ddpfPixelFormat.dwBBitMask;
  GMask:=Mem.ddpfPixelFormat.dwGBitMask;
  if Mode32Bit then
  begin
    for YD:=0 to 28 do
    begin
      if (YPos>=Surface.ClippingRect.Top) and (YPos<Surface.ClippingRect.Bottom) then
      begin
        PoiSource:=Integer(MemSource.lpSurface)+((YD)*MemSource.lPitch)+(357 shl 2);
        PoiDest:=Integer(Mem.lpSurface)+(YPos*Mem.lPitch)+(XPos shl 2);
        for XD:=0 to 48 do
        begin
          if ((XPos+XD)>=Surface.ClippingRect.Left) and ((XPos+XD)<Surface.ClippingRect.Right) then
          begin
            if PInteger(PoiSource)^>0 then
            begin
              LightArea:=PInteger(PoiSource)^;
              Alpha:=max(32,min(255,fAlphaMap[(X*4)+((LightArea-1) div 4),(Y*4)+(LightArea-1) mod 4]));
              if Alpha<>255 then
              begin
                RValue:=(RMask and (((PInteger(PoiDest)^ and RMask)*Alpha) shr 8));
                GValue:=(GMask and (((PInteger(PoiDest)^ and GMask)*Alpha) shr 8));
                BValue:=(BMask and (((PInteger(PoiDest)^ and BMask)*Alpha) shr 8));
                PDWord(PoiDest)^:=(RValue or GValue or BValue);
              end;
            end;
          end;
          inc(PoiSource,4);
          inc(PoiDest,4);
        end;
      end;
      inc(YPos);
    end;
  end
  else
  begin
    for YD:=0 to 28 do
    begin
      if (YPos>=Surface.ClippingRect.Top) and (YPos<Surface.ClippingRect.Bottom) then
      begin
        PoiSource:=Integer(MemSource.lpSurface)+((YD)*MemSource.lPitch)+(357 shl 1);
        PoiDest:=Integer(Mem.lpSurface)+(YPos*Mem.lPitch)+(XPos shl 1);
        for XD:=0 to 48 do
        begin
          if ((XPos+XD)>=Surface.ClippingRect.Left) and ((XPos+XD)<Surface.ClippingRect.Right) then
          begin
            if PWord(PoiSource)^>0 then
            begin
              LightArea:=PWord(PoiSource)^;
              Alpha:=max(32,min(255,fAlphaMap[(X*4)+((LightArea-1) div 4),(Y*4)+(LightArea-1) mod 4]));
              if Alpha<>255 then
              begin
                RValue:=(RMask and (((PSmallInt(PoiDest)^ and RMask)*Alpha) shr 8));
                GValue:=(GMask and (((PSmallInt(PoiDest)^ and GMask)*Alpha) shr 8));
                BValue:=(BMask and (((PSmallInt(PoiDest)^ and BMask)*Alpha) shr 8));
                PWord(PoiDest)^:=(RValue or GValue or BValue);
              end;
            end;
          end;
          inc(PoiSource,2);
          inc(PoiDest,2);
        end;
      end;
      inc(YPos);
    end;
  end;
  TDirectDrawSurface(fISOEngine.fSList[0]).UnLock;
end;

procedure TISOMap.RenderLightNormalEffect(X, Y: Integer;Surface: TDirectDrawSurface;
  var Mem: TDDSurfaceDesc; XPos, YPos: Integer);
var
  XD,YD                : Integer;
  MemSource            : TDDSurfaceDesc;
  PoiSource            : Cardinal;
  PoiDest              : Cardinal;
  RMask,BMask,GMask    : Integer;
  Alpha                : Integer;
  RValue,BValue,GValue : Integer;
  LightArea            : Integer;
begin
  TDirectDrawSurface(fISOEngine.fSList[0]).Lock(MemSource);
  RMask:=Mem.ddpfPixelFormat.dwRBitMask;
  BMask:=Mem.ddpfPixelFormat.dwBBitMask;
  GMask:=Mem.ddpfPixelFormat.dwGBitMask;
  if Mode32Bit then
  begin
    for YD:=0 to 28 do
    begin
      if (YPos>=Surface.ClippingRect.Top) and (YPos<Surface.ClippingRect.Bottom) then
      begin
        PoiSource:=Integer(MemSource.lpSurface)+((YD+28)*MemSource.lPitch)+(357 shl 2);
        PoiDest:=Integer(Mem.lpSurface)+(YPos*Mem.lPitch)+(XPos shl 2);
        for XD:=0 to 48 do
        begin
          if ((XPos+XD)>=Surface.ClippingRect.Left) and ((XPos+XD)<Surface.ClippingRect.Right) then
          begin
            if PWord(PoiSource)^>0 then
            begin
              LightArea:=PInteger(PoiSource)^;
              Alpha:=max(32,min(255,fAlphaMap[(X*2)+((LightArea+1) div 2)-1,(Y*2)+(LightArea-1) mod 2]));
              if Alpha<>255 then
              begin
                RValue:=(RMask and (((PInteger(PoiDest)^ and RMask)*Alpha) shr 8));
                GValue:=(GMask and (((PInteger(PoiDest)^ and GMask)*Alpha) shr 8));
                BValue:=(BMask and (((PInteger(PoiDest)^ and BMask)*Alpha) shr 8));
                PInteger(PoiDest)^:=(RValue or GValue or BValue);
              end;
            end;
          end;
          inc(PoiSource,4);
          inc(PoiDest,4);
        end;
      end;
      inc(YPos);
    end;
  end
  else
  begin
    for YD:=0 to 28 do
    begin
      if (YPos>=Surface.ClippingRect.Top) and (YPos<Surface.ClippingRect.Bottom) then
      begin
        PoiSource:=Integer(MemSource.lpSurface)+((YD+28)*MemSource.lPitch)+(357 shl 1);
        PoiDest:=Integer(Mem.lpSurface)+(YPos*Mem.lPitch)+(XPos shl 1);
        for XD:=0 to 48 do
        begin
          if ((XPos+XD)>=Surface.ClippingRect.Left) and ((XPos+XD)<Surface.ClippingRect.Right) then
          begin
            if PWord(PoiSource)^>0 then
            begin
              LightArea:=PWord(PoiSource)^;
              Alpha:=max(32,min(255,fAlphaMap[(X*2)+((LightArea+1) div 2)-1,(Y*2)+(LightArea-1) mod 2]));
              if Alpha<>255 then
              begin
                RValue:=(RMask and (((PWord(PoiDest)^ and RMask)*Alpha) shr 8));
                GValue:=(GMask and (((PWord(PoiDest)^ and GMask)*Alpha) shr 8));
                BValue:=(BMask and (((PWord(PoiDest)^ and BMask)*Alpha) shr 8));
                PWord(PoiDest)^:=(RValue or GValue or BValue);
              end;
            end;
          end;
          inc(PoiSource,2);
          inc(PoiDest,2);
        end;
      end;
      inc(YPos);
    end;
  end;
  TDirectDrawSurface(fISOEngine.fSList[0]).UnLock;
end;

procedure TISOMap.RenderTile(X, Y: Integer; Surface: TDirectDrawSurface;var Mem: TDDSurfaceDesc;
  XPos, YPos: Integer);
var
  Dummy   : Integer;
  Figur   : TGameFigure;
  ObjPtr  : ^TISOObject;
  MustClip: Boolean;
  SavePos : TPoint;
  SaveSur : TDirectDrawSurface;

  function CheckFigure(X,Y: Integer): boolean;
  begin
    if (X<0) or (X>=fWidth) or (Y<0) or (Y>=fHeight) or (Figures[X,Y].Figure=nil) then
      result:=false
    else
      result:=CanDrawFigure(Figures[X,Y].Figure);
  end;

  procedure DrawMauer(Wall: Integer; AlphaV: Integer; Door: Integer; xpos,ypos,WallIndex: Integer; Transparent: Boolean;OtherSide: Integer = -1);
  begin
    if Wall<>-1 then
    begin
      if AlphaV>-1 then
        fTileGroup.DrawMauer(Surface,XPos,YPos,Wall,AlphaV,OtherSide<>-1,Transparent or fWallsTrans,WallIndex);

      if fTileGroup.GetWallInfos(Wall).Border<>-1 then
        DrawMauer(fTileGroup.GetWallInfos(Wall).Border,AlphaV,-1,xPos,YPos,WallIndex,Transparent,OtherSide);
    end;
  end;

begin
//  if not OverlapRect(Rect(Xpos-1,Ypos-43,Xpos+50,YPos+29),fDRect) then exit;
  {$IFDEF NOCLIPPING}
  MustClip:=false;
  {$ELSE}
  SaveSur:=Surface;
  MustClip:=(Figures[X,Y].ObjectCount>0) or (Figures[X,Y].Figure<>nil);
  if MustClip then
  begin
    Surface:=fTempTile;
    Surface.Fill(bcFuchsia);
    Surface.Draw(0,0,Bounds(XPos-ClippingX,YPos-ClippingY,ClippingWidth,ClippingHeight),SaveSur,false);
    SavePos:=Point(XPos,YPos);
    XPos:=ClippingX;
    YPos:=ClippingY;
  end;
  {$ENDIF}

  Figur:=Figures[X,Y].Figure;
  if (Figur<>nil) and (not CanDrawFigure(Figur)) then Figur:=nil;
  if (X=fISOEngine.fMousePos.X) and (Y=fISOEngine.fMousePos.Y) then
  begin
    if Figur=nil then
      Surface.Draw(XPos,YPos-ZOffSet,NeutralRectF,TDirectDrawSurface(fISOEngine.fSList[0]))
    else
    begin
      if Figur.FigureStatus=fsEnemy then
        Surface.Draw(XPos,YPos-ZOffSet,EnemyRectF,TDirectDrawSurface(fISOEngine.fSList[0]))
      else
        Surface.Draw(XPos,YPos-ZOffSet,SolRectF,TDirectDrawSurface(fISOEngine.fSList[0]));
    end;
  end;

  with fTiles[X,Y] do
  begin
    { Zeichnen der oberen rechten Mauer}

    DrawMauer(TRight,AlphaV.ATRight,DoorInfo.TRight,xPos+(HalfTileWidth-Wall3DEffekt),yPos-(WallHeight-(Wall3DEffekt div 2)-HalfTileHeight),RightIndex,AlphaV.UTTRight>0);

    { Zeichnen der oberen linken Mauer}
    DrawMauer(TLeft,AlphaV.ATLeft,DoorInfo.TLeft,xPos,yPos-(WallHeight-(Wall3DEffekt div 2)-HalfTileHeight),LeftIndex,AlphaV.UTTLeft>0,TRight);

    { Objekt zeichnen }
//    Surface.Draw(xPos,yPos-42,Rect(Obj*49,Alpha*64,(Obj+1)*49,(Alpha+1)*64),TDirectDrawSurface(fTSet[ObjectIndex]));

//    IsoEngine.Container.PowerDraw.PowerD3D.BeginScene(Surface);
    inc(XPos,ObjectOffSetX);
    dec(YPos,ObjectOffSetY);
    with Figures[X,Y] do
    begin
      ObjPtr:=Addr(Objects[0]);
      for Dummy:=0 to ObjectCount-1 do
      begin
        if (ObjPtr^.Visible) and not (ObjPtr^.BeforeUnit) then
          ObjPtr^.Draw(Surface,XPos,YPos);
        inc(ObjPtr);
      end;
    end;

    if (Figur<>nil) then
    begin
      if CanDrawFigure(Figur) then
        {$IFDEF NOCLIPPING}
        Figur.Draw(Surface,Mem,XPos,Ypos,X,Y)
        {$ELSE}
        Figur.Draw(Surface,fTempMem,XPos,Ypos,X,Y)
        {$ENDIF}
    end;

    with Figures[X,Y] do
    begin
      ObjPtr:=Addr(Objects[0]);
      for Dummy:=0 to ObjectCount-1 do
      begin
        if (ObjPtr^.Visible) and (ObjPtr^.BeforeUnit) then
          ObjPtr^.Draw(Surface,XPos,YPos);
        {$IFNDEF NOCLIPPING}
        if ObjPtr^.MustBehindClip then
          ObjPtr^.Draw(SaveSur,SavePos.x+ObjectOffSetX,SavePos.Y-ObjectOffSetY);
        {$ENDIF}
        inc(ObjPtr);
      end;
    end;
    dec(XPos,ObjectOffSetX);
    inc(YPos,ObjectOffSetY);
//    IsoEngine.Container.PowerDraw.Unlock;

    { Zeichnen der unteren rechten Mauer}
    DrawMauer(BRight,AlphaV.ABright,DoorInfo.BRight,xPos+(HalfTileWidth-Wall3DEffekt),yPos-(WallHeight-TileHeight),LeftIndex,AlphaV.UTBRight>0);

    { Zeichnen der unteren linken Mauer}
    DrawMauer(BLeft,AlphaV.ABLeft,DoorInfo.BLeft,xPos,yPos-(WallHeight-TileHeight),RightIndex,AlphaV.UTBLeft>0,BRight);
  end;

  if (X=fISOEngine.fMousePos.X) and (Y=fISOEngine.fMousePos.Y) then
  begin
    if Figur=nil then
      Surface.Draw(XPos,YPos-ZOffSet,NeutralRectB,TDirectDrawSurface(fISOEngine.fSList[0]))
    else            
    begin
      if Figur.FigureStatus=fsEnemy then
        Surface.Draw(XPos,YPos-ZOffSet,EnemyRectB,TDirectDrawSurface(fISOEngine.fSList[0]))
      else
        Surface.Draw(XPos,YPos-ZOffSet,SolRectB,TDirectDrawSurface(fISOEngine.fSList[0]));
    end;
  end;

  if MustClip then
  begin
    Surface.TransparentColor:=bcFuchsia;
    // Clipping
    // Unten Links clippen
    if TileHasWall(X,Y,wtBLeft) then
      Surface.Draw(0,ClippingY,Rect(0,0,TileWidth,TileWidth),fClipMask,true);

    // Unten Rechts clippen
    if TileHasWall(X,Y,wtBRight) then
      Surface.Draw(TileWidth,ClippingY,Rect(TileWidth,0,ClippingWidth,TileWidth),fClipMask,true);

    if (TileHasWall(X,Y,wtBLeft)) {or (TileHasWall(X,Y,wtTLeft))} then
      Surface.FillRect(Rect(0,0,(TileWidth div 2),ClippingHeight),bcFuchsia);

    if (TileHasWall(X,Y,wtBRight)) {or (TileHasWall(X,Y,wtTRight))} then
      Surface.FillRect(Rect(ClippingWidth-HalfTileWidth,0,ClippingWidth,ClippingHeight),bcFuchsia);

    SaveSur.Draw(SavePos.X-ClippingX,SavePos.Y-ClippingY,Surface,true);
  end;
end;

procedure TISOMap.SetDimension(Width, Height: Integer);
var
  X,Y : Integer;
begin
  fWidth:=Width;
  fHeight:=Height;
  SetLength(fTiles,Width,Height);
  SetLength(Figures,Width,Height);
  for X:=0 to Width-1 do
  begin
    for Y:=0 to Height-1 do
    begin
      with fTiles[X,Y] do
      begin
        BLeft:=-1;
        Bright:=-1;
        TLeft:=-1;
        TRight:=-1;
      end;
      with Figures[X,Y] do
      begin
        Figure:=nil;
        WayPoint:=false;
        ObjectCount:=0;
        SetLength(Objects,0);
      end;
    end;
  end;
  fPathFinder.SetTileArray(fTiles,fWidth,fHeight);

  if LightDetail=ldDoppelt then
    SetLength(fAlphaMap,Width*2,Height*2)
  else if LightDetail=ldVierfach then
    SetLength(fAlphaMap,Width*4,Height*4)
  else
    SetLength(fAlphaMap,0,0);

  for X:=0 to length(fAlphaMap)-1 do
    for Y:=0 to length(fAlphaMap[X])-1 do
      fAlphaMap[X,Y]:=0;
end;

procedure TISOMap.SetMouseMap(const Value: TBitmap);
begin
  fMouseMap.Assign(Value);
end;

procedure TISOMap.SetTile(X, Y: Integer; const Tile: TMapTileEntry);
var
  PTile : ^TMapTileEntry;
  X2,Y2 : Integer;

  procedure CheckWall(Wall: Integer; var WayBlocked: TWayBlocked);
  begin
    if (Wall<>-1) then
    begin
      if (fTileGroup.GetWallInfos(Wall).DoorType<>dtNoDoor) then
        WayBlocked:=wbDoor
      else if (fTileGroup.GetWallInfos(Wall).WalkThrough) or (fTileGroup.GetWallInfos(Wall).IsEcke) then
        WayBlocked:=wbNone
      else if (fTileGroup.GetWallInfos(Wall).ShowThrough) then
        WayBlocked:=wbTransparentWall
      else
        WayBlocked:=wbWall;
    end
    else
      WayBlocked:=wbNone;
  end;

  function GetHitPoints(Wall: Integer): Integer;
  begin
    if Wall<>-1 then
      result:=TileGroup.GetWallInfos(Wall).HitPoints
    else
      result:=-1;
  end;

begin
  if not IsoEngine.CheckPosition(X,Y) then
    exit;

  PTile:=Addr(fTiles[x,y]);
  if PTile.Init then
  begin
    // Vergleichen, ob sich was ge�ndert hat
    if (PTile.Floor=Tile.Floor) and
       (PTile.BLeft=Tile.BLeft) and
       (PTile.Bright=Tile.Bright) and
       (PTile.TLeft=Tile.TLeft) and
       (PTile.TRight=Tile.TRight) and
       (PTile.HTBLeft=Tile.HTBLeft) and
       (PTile.HTBright=Tile.HTBright) and
       (PTile.HTTLeft=Tile.HTTLeft) and
       (PTile.HTTRight=Tile.HTTRight) and
       (PTile.Obj=Tile.Obj) then

       exit;
  end;

  PTile^:=Tile;

  PTile.Begehbar:=fTileGroup.GetTileInfos(PTile.Floor).Begehbar;

  // Verkn�pfung initsialisieren
  if not Tile.Init then
  begin
    PTile.HTTLeft:=GetHitPoints(PTile.TLeft);
    PTile.HTTRight:=GetHitPoints(PTile.TRight);
    PTile.HTBLeft:=GetHitPoints(PTile.BLeft);
    PTile.HTBRight:=GetHitPoints(PTile.Bright);

    PTile.PTopLeft:=nil;
    PTile.PTopRight:=nil;
    PTile.PBottomLeft:=nil;
    PTile.PBottomRight:=nil;
    PTile.PTop:=nil;
    PTile.PRight:=nil;
    PTile.PBottom:=nil;
    PTile.PLeft:=nil;

    if (X>0) then
    begin
      // Verkn�pfung herstellen
      PTile.PTopLeft:=Addr(fTiles[X-1,Y]);

      if PTile.PTopLeft.BRight<>-1 then
        PTile.TLeft:=-1;

      if (Y>0) then
        PTile.PTop:=Addr(fTiles[X-1,Y-1]);

      if (Y<fHeight-1) then
        PTile.PLeft:=Addr(fTiles[X-1,Y+1]);
    end;

    if (Y>0) then
    begin
      // Verkn�pfung herstellen
      PTile.PTopRight:=Addr(fTiles[X,Y-1]);

      if PTile.PTopRight.BLeft<>-1 then
        PTile.TRight:=-1;

      if (X<fWidth-1) then
        PTile.PRight:=Addr(fTiles[X+1,Y-1]);
    end;

    if (y<fHeight-1) then
    begin
      // Verkn�pfung herstellen
      PTile.PBottomLeft:=Addr(fTiles[X,Y+1]);

      if PTile.PBottomLeft.TRight<>-1 then
        PTile.BLeft:=-1;

      if (X<fWidth-1) then
        PTile.PBottom:=Addr(fTiles[X+1,Y+1]);
    end;

    if (x<fWidth-1) then
    begin
      // Verkn�pfung herstellen
      PTile.PBottomRight:=Addr(fTiles[X+1,Y]);

      if PTile.PBottomRight.TLeft<>-1 then
        PTile.BRight:=-1;
    end;
  end;

  CheckWall(PTile.TLeft,PTile.DoorInfo.TLeftW);
  PTile.DoorInfo.TLeft:=PTile.TLeft;

  CheckWall(PTile.TRight,PTile.DoorInfo.TRightW);
  PTile.DoorInfo.TRight:=PTile.TRight;

  CheckWall(PTile.BLeft,PTile.DoorInfo.BLeftW);
  PTile.DoorInfo.BLeft:=PTile.BLeft;

  CheckWall(PTile.BRight,PTile.DoorInfo.BRightW);
  PTile.DoorInfo.BRight:=PTile.BRight;
  // Hat dieses Tile T�ren
  with PTile.DoorInfo do
    HasDoors:=(TLeft<>-1) or (TRight<>-1) or (BLeft<>-1) or (BRight<>-1);

  if PTile.Init then
  begin
    // Informationen aktualisieren
    SendVMessage(vmTerrainChange);
    for X2:=max(0,X-1) to min(X+1,fWidth-1) do
    begin
      for Y2:=max(0,Y-1) to min(Y+1,fHeight-1) do
      begin
        if fTiles[X2,Y2].BlockInfo.Discover then
        begin
          fTiles[X2,Y2].BlockInfo.Discover:=false;
          DiscoverField(X2,Y2);
        end;
      end;
    end;
  end
  else
    PTile.Init:=true;
end;

procedure TISOMap.SetViewAreaSize(Width, Height: Integer);
begin
  fVWidth:=Width;
  fVHeight:=Height;

  if fTempSur<>nil then
    fTempSur.Free;

  fTempSur:=TDirectDrawSurface.Create(fDDraw);
  fTempSur.SystemMemory:=true;
  fTempSur.SetSize(Width+5,Height+5);
  Assert(fTempSur.Restore);

  if fTempTile<>nil then
    fTempTile.Free;

  fTempTile:=TDirectDrawSurface.Create(fDDraw);
  fTempTile.SystemMemory:=true;
  fTempTile.SetSize(ClippingWidth,ClippingHeight);
  fTempTile.ClippingRect:=fTempTile.ClientRect;
  Assert(fTempTile.Restore);


  if fClipMask<>nil then
    fClipMask.Free;
    
  fClipMask:=TDirectDrawSurface.Create(fDDraw);
  fClipMask.SystemMemory:=true;
  OpenModels;
  Archiv.OpenRessource('ClipMask');
  fClipMask.LoadFromStream(Archiv.Stream);
  fClipMask.TransparentColor:=bcWhite;
  Archiv.CloseRessource;
end;

procedure TISOMap.SetXOff(const Value: Integer);
var
  Dummy: Integer;
  OldX : Integer;
begin
  OldX:=fxOff;
  fxOff := min(max(Value,-GetMaxXOffSet-fVWidth),-GetMinXOffSet-200);
  OldX:=fXOff-OldX;
  for Dummy:=fISOEngine.fObjectList.Count-1 downto 0 do
    TISOObject(fISOEngine.fObjectList[Dummy]).ScreenOffSet(OldX,0);

  for Dummy:=0 to high(fUnitRects) do
    OffSetRect(fUnitRects[Dummy].Rect,OldX,0);
end;

procedure TISOMap.SetyOff(const Value: Integer);
var
  Dummy: Integer;
  OldY : Integer;
begin
  OldY:=fYOff;
  fyOff := min(max(Value,-GetMaxYOffSet),Wallheight);
  OldY:=fYOff-OldY;
  for Dummy:=fISOEngine.fObjectList.Count-1 downto 0 do
    TISOObject(fISOEngine.fObjectList[Dummy]).ScreenOffSet(0,OldY);

  for Dummy:=0 to high(fUnitRects) do
    OffSetRect(fUnitRects[Dummy].Rect,0,OldY);
end;

function TISOMap.TileAtPos(x, y: Integer; Units: Boolean): TPoint;
var
  xMap,yMap   : Integer;
  xTemp,yTemp : Integer;
  xPos,yPos   : Integer;
  Dummy       : Integer;
begin
  // Erstmal pr�fen, ob eine Einheit �ber der Mauskoordinate ist
  if Units then
  begin
    for Dummy:=0 to high(fUnitRects) do
    begin
      if PtInRect(fUnitRects[Dummy].Rect,Point(X,Y)) then
      begin
        if fUnitRects[Dummy].Figure.CheckHover(X,Y) then
        begin
          result:=Point(fUnitRects[Dummy].Figure.XPos,fUnitRects[Dummy].Figure.YPos);
          exit;
        end;
      end;
    end;
  end;

  xMap:=(x-(fVWidth div 2)-xOff) mod NormalTileWidth;
  yMap:=(y-yoff) mod NormalTileHeight;
  xTemp:=(x-(fVWidth div 2)-xOff) div NormalTileWidth;
  ytemp:=(y-yoff) div NormalTileHeight;
  if xMap<0 then dec(xTemp);
  if yMap<0 then dec(yTemp);
  yPos:=yTemp-xTemp;
  xPos:=yTemp+xTemp;
  if xMap<0 then inc(xMap,NormalTileWidth);
  if yMap<0 then inc(yMap,NormalTileHeight);
       if fMouseMap.Canvas.Pixels[xMap,yMap]=clGreen then inc(YPos)
  else if fMouseMap.Canvas.Pixels[xMap,yMap]=clTeal  then inc(XPos)
  else if fMouseMap.Canvas.Pixels[xMap,yMap]=clOlive then dec(YPos)
  else if fMouseMap.Canvas.Pixels[xMap,yMap]=clMaroon then dec(XPos);
  result:=Point(xPos,yPos);
end;

function TISOMap.TileEntry(X, Y: Integer): PMapTileEntry;
begin
  fISOEngine.CheckPositionAssert(X,Y);

  result:=Addr(fTiles[X,Y]);
end;

function TISOMap.TileState(X, Y: Integer): PTileState;
begin
  fISOEngine.CheckPositionAssert(X,Y);

  result:=Addr(Figures[X,Y]);
end;

procedure TISOMap.DiscoverField(X, Y: Integer);
var
  PMTFrom : ^TMapTileEntry;
  PMTTo   : ^TMapTileEntry;
  BType   : TWayBlocked;

  procedure CheckType(var Old: TWayBlocked; Check1, Check2: TWayBlocked);
  begin
    Old:=TWayBlocked(Max(Max(Integer(Old),Integer(Check1)),Integer(Check2)));
  end;

  procedure AddDoor(var BlockType: TBlockType; PtrDoor: PSmallInt);
  begin
    SetLength(BlockType.Doors,length(BlockType.Doors)+1);
    BlockType.Doors[high(BlockType.Doors)]:=PtrDoor;
  end;

begin
  fISOEngine.CheckPositionAssert(X,Y);

  with fTiles[X,Y] do
  begin
    if BlockInfo.Discover then exit;
    BlockInfo.Discover:=true;

    PMTFrom:=Addr(fTiles[X,Y]);

    // (0) Oben Links (X=-1, Y=-1)
    if (X>0) and (Y>0) then
    begin
      BType:=wbNone;
      PMTTo:=Addr(fTiles[X-1,Y-1]);

      if not PMTTo.Begehbar then
        BType:=wbInvalidField;

      if PMTTo.Obj<>-1 then
        BType:=wbObject;

      if BType<>wbWall then CheckType(BType,PMTFrom.DoorInfo.TLeftW,PMTFrom.DoorInfo.TRightW);
      if BType<>wbWall then CheckType(BType,PMTTo.DoorInfo.BLeftW,PMTTo.DoorInfo.BRightW);
      if BType<>wbWall then CheckType(BType,PMTFrom.PTopLeft.DoorInfo.TRightW,PMTFrom.PTopLeft.DoorInfo.BRightW);
      if BType<>wbWall then CheckType(BType,PMTFrom.PTopRight.DoorInfo.BLeftW,PMTFrom.PTopRight.DoorInfo.TLeftW);
      if BType<>wbWall then
      begin
        if (PMTFrom.PTopLeft.Obj<>-1) then CheckType(BType,wbWall,wbNone);
        if (PMTFrom.PTopRight.Obj<>-1) then CheckType(BType,wbWall,wbNone);
      end;

      BlockInfo.BlockType[0].Block:=BType;

      if BType=wbDoor then
      begin
        if PMTFrom.DoorInfo.TLeftW=wbDoor        then AddDoor(BlockInfo.BlockType[0],Addr(PMTFrom.TLeft));
        if PMTFrom.DoorInfo.TRightW=wbDoor       then AddDoor(BlockInfo.BlockType[0],Addr(PMTFrom.TRight));
        if PMTTo.DoorInfo.BLeftW=wbDoor          then AddDoor(BlockInfo.BlockType[0],Addr(PMTTo.BLeft));
        if PMTTo.DoorInfo.BRightW=wbDoor         then AddDoor(BlockInfo.BlockType[0],Addr(PMTTo.BRight));
        if PMTFrom.PTopLeft.DoorInfo.TRightW=wbDoor then AddDoor(BlockInfo.BlockType[0],Addr(PMTFrom.PTopLeft.TRight));
        if PMTFrom.PTopLeft.DoorInfo.BRightW=wbDoor then AddDoor(BlockInfo.BlockType[0],Addr(PMTFrom.PTopLeft.BRight));
        if PMTFrom.PTopRight.DoorInfo.BLeftW=wbDoor  then AddDoor(BlockInfo.BlockType[0],Addr(PMTFrom.PTopRight.BLeft));
        if PMTFrom.PTopRight.DoorInfo.TLeftW=wbDoor  then AddDoor(BlockInfo.BlockType[0],Addr(PMTFrom.PTopRight.TLeft));
      end;
    end
    else
      BlockInfo.BlockType[0].Block:=wbOutOfBounds;

    // (1) Oben (X=0, Y=-1)
    if (Y>0) then
    begin
      BType:=wbNone;
      PMTTo:=Addr(fTiles[X,Y-1]);

      if not PMTTo.Begehbar then
        BType:=wbInvalidField;

      if PMTTo.Obj<>-1 then
        BType:=wbObject;

      CheckType(BType,PMTTo.DoorInfo.BLeftW,PMTFrom.DoorInfo.TRightW);

      BlockInfo.BlockType[1].Block:=BType;
      if BType=wbDoor then
      begin
        if PMTFrom.DoorInfo.TRightW=wbDoor then AddDoor(BlockInfo.BlockType[1],Addr(PMTFrom.TRight));
        if PMTTo.DoorInfo.BLeftW=wbDoor    then AddDoor(BlockInfo.BlockType[1],Addr(PMTTo.BLeft));
      end;
    end
    else
      BlockInfo.BlockType[1].Block:=wbOutOfBounds;

    // (2) Oben Rechts (X=+1, Y=-1)
    if (X<fWidth-1) and (Y>0) then
    begin
      BType:=wbNone;
      PMTTo:=Addr(fTiles[X+1,Y-1]);

      if not PMTTo.Begehbar then
        BType:=wbInvalidField;

      if PMTTo.Obj<>-1 then
        BType:=wbObject;

      if BType<>wbWall then CheckType(BType,PMTFrom.DoorInfo.BRightW,PMTFrom.DoorInfo.TRightW);
      if BType<>wbWall then CheckType(BType,PMTTo.DoorInfo.TLeftW,PMTTo.DoorInfo.BLeftW);
      if BType<>wbWall then CheckType(BType,PMTFrom.PBottomRight.DoorInfo.TRightW,PMTFrom.PBottomRight.DoorInfo.TLeftW);
      if BType<>wbWall then CheckType(BType,PMTFrom.PTopRight.DoorInfo.BLeftW,PMTFrom.PTopRight.DoorInfo.BRightW);
      if BType<>wbWall then
      begin
        if (PMTFrom.PBottomRight.Obj<>-1) then CheckType(BType,wbWall,wbNone);
        if (PMTFrom.PTopRight.Obj<>-1) then CheckType(BType,wbWall,wbNone);
      end;

      BlockInfo.BlockType[2].Block:=BType;

      if BType=wbDoor then
      begin
        if PMTFrom.DoorInfo.BRightW=wbDoor              then AddDoor(BlockInfo.BlockType[2],Addr(PMTFrom.BRight));
        if PMTFrom.DoorInfo.TRightW=wbDoor              then AddDoor(BlockInfo.BlockType[2],Addr(PMTFrom.TRight));
        if PMTTo.DoorInfo.TLeftW=wbDoor                 then AddDoor(BlockInfo.BlockType[2],Addr(PMTTo.TLeft));
        if PMTTo.DoorInfo.BLeftW=wbDoor                 then AddDoor(BlockInfo.BlockType[2],Addr(PMTTo.BLeft));
        if PMTFrom.PBottomRight.DoorInfo.TRightW=wbDoor then AddDoor(BlockInfo.BlockType[2],Addr(PMTFrom.PBottomRight.TRight));
        if PMTFrom.PBottomRight.DoorInfo.TLeftW=wbDoor  then AddDoor(BlockInfo.BlockType[2],Addr(PMTFrom.PBottomRight.TLeft));
        if PMTFrom.PTopRight.DoorInfo.BLeftW=wbDoor     then AddDoor(BlockInfo.BlockType[2],Addr(PMTFrom.PTopRight.BLeft));
        if PMTFrom.PTopRight.DoorInfo.BRightW=wbDoor    then AddDoor(BlockInfo.BlockType[2],Addr(PMTFrom.PTopRight.BRight));
      end;
    end
    else
      BlockInfo.BlockType[2].Block:=wbOutOfBounds;

    // (3) Links (X=-1, Y=0)
    if (X>0) then
    begin
      BType:=wbNone;
      PMTTo:=Addr(fTiles[X-1,Y]);

      if not PMTTo.Begehbar then
        BType:=wbInvalidField;

      if PMTTo.Obj<>-1 then
        BType:=wbObject;

      CheckType(BType,PMTTo.DoorInfo.BRightW,PMTFrom.DoorInfo.TLeftW);
      BlockInfo.BlockType[3].Block:=BType;
      if BType=wbDoor then
      begin
        if PMTFrom.DoorInfo.TLeftW=wbDoor then AddDoor(BlockInfo.BlockType[3],Addr(PMTFrom.TLeft));
        if PMTTo.DoorInfo.BRightW=wbDoor  then AddDoor(BlockInfo.BlockType[3],Addr(PMTTo.BRight));
      end;
    end
    else
      BlockInfo.BlockType[3].Block:=wbOutOfBounds;

    // (4) Rechts (X=+1, Y=0)
    if (X<fWidth-1) then
    begin
      BType:=wbNone;
      PMTTo:=Addr(fTiles[X+1,Y]);

      if not PMTTo.Begehbar then
        BType:=wbInvalidField;

      if PMTTo.Obj<>-1 then
        BType:=wbObject;

      CheckType(BType,PMTTo.DoorInfo.TLeftW,PMTFrom.DoorInfo.BRightW);
      BlockInfo.BlockType[4].Block:=BType;
      if BType=wbDoor then
      begin
        if PMTFrom.DoorInfo.BRightW=wbDoor then AddDoor(BlockInfo.BlockType[4],Addr(PMTFrom.BRight));
        if PMTTo.DoorInfo.TLeftW=wbDoor    then AddDoor(BlockInfo.BlockType[4],Addr(PMTTo.TLeft));
      end;
    end
    else
      BlockInfo.BlockType[4].Block:=wbOutOfBounds;

    // (5) Unten Links (X=-1, Y=+1)
    if (X>0) and (Y<fHeight-1) then
    begin
      BType:=wbNone;
      PMTTo:=Addr(fTiles[X-1,Y+1]);

      if not PMTTo.Begehbar then
        BType:=wbInvalidField;

      if PMTTo.Obj<>-1 then
        BType:=wbObject;

      if BType<>wbWall then CheckType(BType,PMTFrom.DoorInfo.TLeftW,PMTFrom.DoorInfo.BLeftW);
      if BType<>wbWall then CheckType(BType,PMTTo.DoorInfo.TRightW,PMTTo.DoorInfo.BRightW);
      if BType<>wbWall then CheckType(BType,PMTFrom.PTopLeft.DoorInfo.BRightW,PMTFrom.PTopLeft.DoorInfo.BLeftW);
      if BType<>wbWall then CheckType(BType,PMTFrom.PBottomLeft.DoorInfo.TLeftW,PMTFrom.PBottomLeft.DoorInfo.TRightW);
      if BType<>wbWall then
      begin
        if (PMTFrom.PTopLeft.Obj<>-1) then CheckType(BType,wbWall,wbNone);
        if (PMTFrom.PBottomLeft.Obj<>-1) then CheckType(BType,wbWall,wbNone);
      end;

      BlockInfo.BlockType[5].Block:=BType;
      if BType=wbDoor then
      begin
        if PMTFrom.DoorInfo.TLeftW=wbDoor        then AddDoor(BlockInfo.BlockType[5],Addr(PMTFrom.TLeft));
        if PMTFrom.DoorInfo.BLeftW=wbDoor        then AddDoor(BlockInfo.BlockType[5],Addr(PMTFrom.BLeft));
        if PMTTo.DoorInfo.TRightW=wbDoor         then AddDoor(BlockInfo.BlockType[5],Addr(PMTTo.TRight));
        if PMTTo.DoorInfo.BRightW=wbDoor         then AddDoor(BlockInfo.BlockType[5],Addr(PMTTo.BRight));
        if PMTFrom.PTopLeft.DoorInfo.BRightW=wbDoor then AddDoor(BlockInfo.BlockType[5],Addr(PMTFrom.PTopLeft.BRight));
        if PMTFrom.PTopLeft.DoorInfo.BLeftW=wbDoor  then AddDoor(BlockInfo.BlockType[5],Addr(PMTFrom.PTopLeft.BLeft));
        if PMTFrom.PBottomLeft.DoorInfo.TLeftW=wbDoor  then AddDoor(BlockInfo.BlockType[5],Addr(PMTFrom.PBottomLeft.TLeft));
        if PMTFrom.PBottomLeft.DoorInfo.TRightW=wbDoor then AddDoor(BlockInfo.BlockType[5],Addr(PMTFrom.PBottomLeft.TRight));
      end;
    end
    else
      BlockInfo.BlockType[5].Block:=wbOutOfBounds;

    // (6) Unten (X=0, Y=+1)
    if (Y<fHeight-1) then
    begin
      BType:=wbNone;
      PMTTo:=Addr(fTiles[X,Y+1]);

      if not PMTTo.Begehbar then
        BType:=wbInvalidField;

      if PMTTo.Obj<>-1 then
        BType:=wbObject;

      CheckType(BType,PMTTo.DoorInfo.TRightW,PMTFrom.DoorInfo.BLeftW);
      BlockInfo.BlockType[6].Block:=BType;
      if BType=wbDoor then
      begin
        if PMTFrom.DoorInfo.BLeftW=wbDoor then AddDoor(BlockInfo.BlockType[6],Addr(PMTFrom.BLeft));
        if PMTTo.DoorInfo.TRightW=wbDoor  then AddDoor(BlockInfo.BlockType[6],Addr(PMTTo.TRight));
      end;
    end
    else
      BlockInfo.BlockType[6].Block:=wbOutOfBounds;

    // (7) Unten Rechts (X=+1, Y=+1)
    if (X<fWidth-1) and (Y<fHeight-1) then
    begin
      BType:=wbNone;
      PMTTo:=Addr(fTiles[X+1,Y+1]);

      if not PMTTo.Begehbar then
        BType:=wbInvalidField;

      if PMTTo.Obj<>-1 then
        BType:=wbObject;

      if BType<>wbWall then CheckType(BType,PMTFrom.DoorInfo.BLeftW,PMTFrom.DoorInfo.BRightW);
      if BType<>wbWall then CheckType(BType,PMTTo.DoorInfo.TLeftW,PMTTo.DoorInfo.TRightW);
      if BType<>wbWall then CheckType(BType,PMTFrom.PBottomRight.DoorInfo.BLeftW,PMTFrom.PBottomRight.DoorInfo.TLeftW);
      if BType<>wbWall then CheckType(BType,PMTFrom.PBottomLeft.DoorInfo.BRightW,PMTFrom.PBottomLeft.DoorInfo.TRightW);
      if BType<>wbWall then
      begin
        if (PMTFrom.PBottomRight.Obj<>-1) then CheckType(BType,wbWall,wbNone);
        if (fTiles[X,Y+1].Obj<>-1) then CheckType(BType,wbWall,wbNone);
      end;

      BlockInfo.BlockType[7].Block:=BType;
      if BType=wbDoor then
      begin
        if PMTFrom.DoorInfo.BLeftW=wbDoor        then AddDoor(BlockInfo.BlockType[7],Addr(PMTFrom.BLeft));
        if PMTFrom.DoorInfo.BRightW=wbDoor       then AddDoor(BlockInfo.BlockType[7],Addr(PMTFrom.BRight));
        if PMTTo.DoorInfo.TLeftW=wbDoor          then AddDoor(BlockInfo.BlockType[7],Addr(PMTTo.TLeft));
        if PMTTo.DoorInfo.TRightW=wbDoor         then AddDoor(BlockInfo.BlockType[7],Addr(PMTTo.TRight));
        if PMTFrom.PBottomRight.DoorInfo.BLeftW=wbDoor  then AddDoor(BlockInfo.BlockType[7],Addr(PMTFrom.PBottomRight.BLeft));
        if PMTFrom.PBottomRight.DoorInfo.TLeftW=wbDoor  then AddDoor(BlockInfo.BlockType[7],Addr(PMTFrom.PBottomRight.TLeft));
        if PMTFrom.PBottomLeft.DoorInfo.BRightW=wbDoor then AddDoor(BlockInfo.BlockType[7],Addr(PMTFrom.PBottomLeft.BRight));
        if PMTFrom.PBottomLeft.DoorInfo.TRightW=wbDoor then AddDoor(BlockInfo.BlockType[7],Addr(PMTFrom.PBottomLeft.TRight));
      end;
    end
    else
      BlockInfo.BlockType[7].Block:=wbOutOfBounds;
  end;
end;

procedure TISOMap.CenterTo(Figure: TGameFigure);
begin
  CenterTo(Point(Figure.XPos,Figure.YPos));
end;

function TISOMap.GetFigureAtPos(Point: TPoint): TGameFigure;
begin
  fISOEngine.CheckPositionAssert(Point);

  Result:=Figures[Point.X,Point.Y].Figure;
  {$IFNDEF DISABLEBEAM}
  if result=nil then
  begin
    with Figures[Point.X,Point.Y] do
    begin
      for Dummy:=0 to ObjectCount-1 do
      begin
        if Objects[Dummy] is TBeamObject then
        begin
          result:=TBeamObject(Objects[Dummy]).Figure;
          exit;
        end;
      end;
    end;
  end;
  {$ENDIF}
end;


function TISOMap.SetFigureToPos(Point: TPoint; Figure: TGameFigure): Boolean;
var
  Tile     : PMapTileEntry;
  TmpPoint : TPoint;
  Func     : procedure(const Point: TPoint;WallType: TWalltype) of object;
  Dummy    : Integer;
begin
  fISOEngine.CheckPositionAssert(Point);

  if Figures[Point.X,Point.Y].Figure=Figure then
  begin
    result:=true;
    exit;
  end;

  Tile:=Addr(fTiles[Point.X,Point.Y]);

  if not Tile.Begehbar then
  begin
    result:=false;
    exit;
  end;

  result:=true;
  Figures[Point.X,Point.Y].Figure:=Figure;

  if Figure<>nil then
    Func:=ISOEngine.OpenDoor
  else
    Func:=ISOEngine.CloseDoor;

  if Tile.DoorInfo.HasDoors then
  begin
    if Tile.DoorInfo.TLeftW=wbDoor  then Func(Point,wtTLeft);
    if Tile.DoorInfo.TRightW=wbDoor then Func(Point,wtTRight);
    if Tile.DoorInfo.BLeftW=wbDoor  then Func(Point,wtBLeft);
    if Tile.DoorInfo.BRightW=wbDoor then Func(Point,wtBRight);
  end;

  if Point.X>0 then
  begin
    if fTiles[Point.X-1,Point.Y].DoorInfo.BRightW=wbDoor then
    begin
      tmpPoint:=Point;
      dec(tmpPoint.X);
      Func(tmpPoint,wtBRight);
    end;
  end;
  if Point.Y>0 then
  begin
    if fTiles[Point.X,Point.Y-1].DoorInfo.BLeftW=wbDoor then
    begin
      tmpPoint:=Point;
      dec(tmpPoint.Y);
      Func(tmpPoint,wtBLeft);
    end;
  end;
  if Point.X<fWidth-1 then
  begin
    if fTiles[Point.X+1,Point.Y].DoorInfo.TLeftW=wbDoor then
    begin
      tmpPoint:=Point;
      inc(tmpPoint.X);
      Func(tmpPoint,wtTLeft);
    end;
  end;
  if Point.Y<fHeight-1 then
  begin
    if fTiles[Point.X,Point.Y+1].DoorInfo.TRightW=wbDoor then
    begin
      tmpPoint:=Point;
      inc(tmpPoint.Y);
      Func(tmpPoint,wtTRight);
    end;
  end;

  // Pr�fen, ob Einheit in der N�he einer gegnerischen Mine
  if Figure<>nil then
  begin
    for Dummy:=fISOEngine.fObjectList.Count-1 downto 0 do
    begin
      if TObject(fISOEngine.fObjectList[Dummy]) is TMineItem then
      begin
        TMineItem(fISOEngine.fObjectList[Dummy]).CheckFigure(Figure);
      end;
    end;
  end;
end;

function TISOMap.TileHasWall(X, Y: Integer; Wall: TWallType): boolean;
begin
  result:=false;
  fISOEngine.CheckPositionAssert(X,Y);

  case Wall of
    wtTLeft     : result:=(fTiles[X,Y].TLeft<>-1) or ((X>0) and (fTiles[X-1,Y].BRight<>-1));
    wtTRight    : result:=(fTiles[X,Y].TRight<>-1) or ((Y>0) and (fTiles[X,Y-1].BLeft<>-1));
    wtBLeft     : result:=(fTiles[X,Y].BLeft<>-1) or ((Y<fHeight-1) and (fTiles[X,Y+1].TRight<>-1));
    wtBRight    : result:=(fTiles[X,Y].Bright<>-1) or ((X<fWidth-1) and (fTiles[X+1,Y].TLeft<>-1));
  end;
end;

procedure TISOMap.RestoreSurfaces;
begin
  fTileGroup.LoadTiles;
end;

function TISOMap.GetTileSurface(Index: Integer): TDirectDrawSurface;
begin
  Assert((Index>=0) and (Index<fTileGroup.TileSurfaces.Count));
  result:=TDirectDrawSurface(fTileGroup.TileSurfaces[Index]);
end;

procedure TISOMap.GetDoorInfo(const Entry: TMapTileEntry;
  WallType: TWallType; var Tile: Integer; var OpenLeft: Boolean);
begin
  case WallType of
    wtTLeft  :
    begin
      Tile:=Entry.DoorInfo.TLeft;
      OpenLeft:=fTileGroup.GetWallInfos(Tile).DoorType=dtOpenLeft;
    end;
    wtTRight :
    begin
      Tile:=Entry.DoorInfo.TRight;
      OpenLeft:=fTileGroup.GetWallInfos(Tile).DoorType=dtOpenRight;
    end;                    
    wtBLeft  :
    begin
      Tile:=Entry.DoorInfo.BLeft;
      OpenLeft:=fTileGroup.GetWallInfos(Tile).DoorType=dtOpenRight;
    end;
    wtBRight :
    begin
      Tile:=Entry.DoorInfo.BRight;
      OpenLeft:=fTileGroup.GetWallInfos(Tile).DoorType=dtOpenLeft;
    end;
  end;
end;

function TISOMap.CheckWalls(X, Z: Integer; WallType: TWallType;
  Tile: TPoint; ObjektSize: Integer): Integer;

  procedure CheckWall(Index: Integer;Left: Boolean);
  var
    Value: Integer;
    XOrg,ZOrg: Integer;
  begin
    if Index<>-1 then
    begin
      dec(ObjektSize);
      for XOrg:=X-ObjektSize to X+ObjektSize do
      begin
        for ZOrg:=Z-ObjektSize to Z+ObjektSize do
        begin
          Value:=fTileGroup.CheckWall(Index,X,Z,Left);
          if Value>0 then
          begin
            result:=result+Value;
            break;
          end;
        end;
      end;
    end;
  end;

begin
  result:=0;
  if not IsoEngine.CheckPosition(Tile) then
  begin
    result:=200;
    exit;
  end;

  with fTiles[Tile.X,Tile.Y] do
  begin
    case WallType of
      wtTLeft:
      begin
        CheckWall(TLeft,true);
        if PTopLeft<>nil then
          CheckWall(PTopLeft.Bright,true);
      end;
      wtTRight:
      begin
        CheckWall(TRight,false);
        if PTopRight<>nil then
          CheckWall(PTopRight.BLeft,false);
      end;
      wtBLeft:
      begin
        CheckWall(BLeft,false);
        if PBottomLeft<>nil then
          CheckWall(PBottomLeft.TRight,false);
      end;
      wtBRight:
      begin
        CheckWall(Bright,true);
        if PBottomRight<>nil then
          CheckWall(PBottomRight.TLeft,true);
      end;
    end;
  end;
end;

procedure TISOMap.SetShowGrid(const Value: Boolean);
begin
  if fShowGrid=Value then
    exit;

  fShowGrid := Value;
  SendVMessage(vmAktuTempSurface);
end;

procedure TISOMap.SetDrawingRect(Figure: TGameFigure; Rect: TRect);
var
  Dummy: Integer;
  Index: Integer;

  procedure ChangeWallTransparency(Increment: Boolean; const beginPos: TPoint; const RelRect: TRect);
  var
    X    : Integer;
    Y    : Integer;
    YEnd : Integer;
    XEnd : Integer;
    Pos  : TPoint;

    procedure CheckWall(var UnitCounter: Integer; WallRect: TRect);
    begin

      if (not IsRectEmpty(WallRect)) and OverlapRect(WallRect,RelRect) then
      begin
        if Increment then
        begin
          inc(UnitCounter);
          if UnitCounter=1 then
          begin
            Rec.Rect:=WallRect;
            SendVMessage(vmAktuTempSurfaceRect);
          end;
        end
        else
        begin
          dec(UnitCounter);
          if UnitCounter=0 then
          begin
            Rec.Rect:=WallRect;
            SendVMessage(vmAktuTempSurfaceRect);
          end;
          if UnitCounter<0 then
            UnitCounter:=0;
        end;
      end;
    end;

    function GetWallRect(Wall: Integer;BottomLeft: TPoint): TRect;
    var
      Height: Integer;
    begin
      if Wall<>-1 then
      begin
        Height:=fTileGroup.GetWallInfos(Wall).AbsWallHeight+(WallWidth div 2);
        result:=Classes.Rect(BottomLeft.X,BottomLeft.Y-Height,BottomLeft.X+WallWidth-Wall3DEffekt,BottomLeft.Y);
      end
      else
        result:=Classes.Rect(0,0,0,0);
    end;

    procedure CheckTile(X,Y: Integer);
    var
      TileRect: TRect;
    begin
      if IsoEngine.CheckPosition(X,Y) then
      begin
        TileRect:=fISOEngine.GetTileRect(nil,Point(X,Y));
        TileRect.Top:=TileRect.Bottom-TileHeight;
        with TileEntry(X,Y)^ do
        begin
          // Mauer Oben Links pr�fen
          if X>=beginPos.X then
            CheckWall(AlphaV.UTTLeft,GetWallRect(TLeft,Point(TileRect.Left,TileRect.Top+HalfTileHeight)));

          // Mauer Oben Rechts pr�fen
          if Y>=beginPos.Y then
            CheckWall(AlphaV.UTTRight,GetWallRect(TRight,Point(TileRect.Left+HalfTileWidth,TileRect.Top+HalfTileHeight)));

          // Mauer Unten Links
          CheckWall(AlphaV.UTBLeft,GetWallRect(BLeft,Point(TileRect.Left,TileRect.Top+TileHeight)));

          // Mauer Unten Rechts
          CheckWall(AlphaV.UTBRight,GetWallRect(BRight,Point(TileRect.Left+HalfTileWidth,TileRect.Top+TileHeight)));
        end;
      end;
    end;

  begin
    Y:=RelRect.Bottom;
    YEnd:=RelRect.Bottom+2*TileHeight;
    XEnd:=RelRect.Right+HalfTileWidth;
    while Y<YEnd do
    begin
      X:=RelRect.Left;
      while X<XEnd do
      begin
        Pos:=TileAtPos(X,Y,false);

        CheckTile(Pos.X,Pos.Y);

        inc(X,HalfTileWidth);
      end;
      inc(Y,HalfTileHeight);
    end;
  end;

begin
  Index:=-1;
  for Dummy:=0 to high(fUnitRects) do
  begin
    if fUnitRects[Dummy].Figure=Figure then
    begin
      Index:=Dummy;
      break;
    end;
  end;

  if Index<>-1 then
  begin
    with fUnitRects[Index] do
      ChangeWallTransparency(false,Point(beginX,beginY),Rect);
  end;

  if (not CanDrawFigure(Figure)) then
  begin
    if Index<>-1 then
      DeleteArray(Addr(fUnitRects),TypeInfo(TUnitDrawRectArray),Index);
    exit;
  end;

  if Index=-1 then
  begin
    SetLength(fUnitRects,length(fUnitRects)+1);
    Index:=high(fUnitRects);
    fUnitRects[Index].Figure:=Figure;
  end;

  fUnitRects[Index].Rect:=Rect;
  fUnitRects[Index].beginX:=Figure.XPos;
  fUnitRects[Index].beginY:=Figure.YPos;

  ChangeWallTransparency(true,Point(Figure.XPos,Figure.YPos),Rect);
end;

procedure TISOMap.DoDamageOnWalls(FromPos, ToPos: TPoint;
  Damage: Integer);
var
  PMTTo   : TMapTileEntry;

  Temp    : TMapTileEntry;
  Index   : Integer;

  procedure DoDamageOnWall(var WallIndex: Smallint; var HitPoints: Integer);
  var
    Info: TWallInformation;
  begin
    if WallIndex<>-1 then
    begin
      Info:=TileGroup.GetWallInfos(WallIndex);
      if Info.Destroyed<>-1 then
      begin
        HitPoints:=HitPoints-Damage;
        if HitPoints<0 then
        begin
          WallIndex:=Info.Destroyed;
          HitPoints:=TileGroup.GetWallInfos(WallIndex).HitPoints;
        end;
      end;
    end;
  end;

begin
  PMTTo:=TileEntry(FromPos.X,FromPos.Y)^;

  // Sicherstellen, dass es auf ein Nachbarfeld geht
  Assert((Abs(FromPos.Y-ToPos.Y)<=1) and (Abs(FromPos.X-ToPos.X)<=1));
  // Floortile anpassen
//  PMTTo.Floor:=1;

  { ** Mauern zerst�ren ** }

  // Index berechnen (0 = oben Links, 1 =oben, 2 =oben rechts ... 8 = unten rechts)
  Index:=((FromPos.Y-ToPos.Y+1)*3)+(FromPos.X-ToPos.X+1);

  case Index of
    // (0) Oben Links (X=-1, Y=-1)
    0 :
    begin
      DoDamageOnWall(PMTTo.BLeft,PMTTo.HTBLeft);
      DoDamageOnWall(PMTTo.BRight,PMTTo.HTBRight);

      if PMTTo.PBottom<>nil then
      begin
        Temp:=PMTTo.PBottomLeft^;
        DoDamageOnWall(Temp.BRight,Temp.HTBRight);
        DoDamageOnWall(Temp.TRight,Temp.HTTRight);
        SetTile(FromPos.X,FromPos.Y+1,Temp);
      end;
    end;
    // (1) Oben (X=0, Y=-1)
    1 :
    begin
      DoDamageOnWall(PMTTo.BLeft,PMTTo.HTBLeft);

      if PMTTo.PBottomLeft<>nil then
      begin
        Temp:=PMTTo.PBottomLeft^;
        DoDamageOnWall(Temp.TRight,Temp.HTTRight);
        SetTile(FromPos.X,FromPos.Y+1,Temp);
      end;
    end;
    // (2) Oben Rechts (X=+1, Y=-1)
    2 :
    begin
      DoDamageOnWall(PMTTo.TLeft,PMTTo.HTTLeft);
      DoDamageOnWall(PMTTo.BLeft,PMTTo.HTBLeft);

      if PMTTo.PLeft<>nil then
      begin
        Temp:=PMTTo.PLeft^;
        DoDamageOnWall(Temp.TRight,Temp.HTTRight);
        DoDamageOnWall(Temp.BRight,Temp.HTBRight);
        SetTile(FromPos.X-1,FromPos.Y+1,Temp);
      end;

      if PMTTo.PTopLeft<>nil then
      begin
        Temp:=PMTTo.PTopLeft^;
        DoDamageOnWall(Temp.BRight,Temp.HTBRight);
        DoDamageOnWall(Temp.BLeft,Temp.HTBLeft);
        SetTile(FromPos.X-1,FromPos.Y,Temp);
      end;

      if PMTTo.PBottomLeft<>nil then
      begin
        Temp:=PMTTo.PBottomLeft^;
        DoDamageOnWall(Temp.TLeft,Temp.HTTLeft);
        DoDamageOnWall(Temp.TRight,Temp.HTTRight);
        SetTile(FromPos.X,FromPos.Y+1,Temp);
      end;
    end;
    // (3) Links (X=-1, Y=0)
    3 :
    begin
      DoDamageOnWall(PMTTo.BRight,PMTTo.HTBRight);

      if PMTTo.PBottomRight<>nil then
      begin
        Temp:=PMTTo.PBottomRight^;
        DoDamageOnWall(Temp.TLeft,Temp.HTTLeft);
        SetTile(FromPos.X+1,FromPos.Y,Temp);
      end;
    end;
    // (4) Mitte (X=0, Y=0);
    4:
    begin
      DoDamageOnWall(PMTTo.TLeft,PMTTo.HTTLeft);
      DoDamageOnWall(PMTTo.TRight,PMTTo.HTTRight);
      DoDamageOnWall(PMTTo.BLeft,PMTTo.HTBLeft);
      DoDamageOnWall(PMTTo.BRight,PMTTo.HTBRight);

      if PMTTo.PTopLeft<>nil then
      begin
        Temp:=PMTTo.PTopLeft^;
        DoDamageOnWall(Temp.BRight,Temp.HTBRight);
        SetTile(FromPos.X-1,FromPos.Y,Temp);
      end;

      if PMTTo.PTopRight<>nil then
      begin
        Temp:=PMTTo.PTopRight^;
        DoDamageOnWall(Temp.BLeft,Temp.HTBLeft);
        SetTile(FromPos.X,FromPos.Y-1,Temp);
      end;

      if PMTTo.PBottomLeft<>nil then
      begin
        Temp:=PMTTo.PBottomLeft^;
        DoDamageOnWall(Temp.TRight,Temp.HTTRight);
        SetTile(FromPos.X,FromPos.Y+1,Temp);
      end;

      if PMTTo.PBottomRight<>nil then
      begin
        Temp:=PMTTo.PBottomRight^;
        DoDamageOnWall(Temp.TLeft,Temp.HTTLeft);
        SetTile(FromPos.X+1,FromPos.Y,Temp);
      end;
    end;
    // (5) Rechts (X=+1, Y=0)
    5 :
    begin
      DoDamageOnWall(PMTTo.TLeft,PMTTo.HTTLeft);

      if PMTTo.PTopLeft<>nil then
      begin
        Temp:=PMTTo.PTopLeft^;
        DoDamageOnWall(Temp.BRight,Temp.HTBRight);
        SetTile(FromPos.X-1,FromPos.Y,Temp);
      end;
    end;
    // (6) Unten Links (X=-1, Y=+1)
    6 :
    begin
      DoDamageOnWall(PMTTo.BRight,PMTTo.HTBRight);
      DoDamageOnWall(PMTTo.TRight,PMTTo.HTTRight);

      if PMTTo.PRight<>nil then
      begin
        Temp:=PMTTo.PRight^;
        DoDamageOnWall(Temp.TLeft,Temp.HTTLeft);
        DoDamageOnWall(Temp.BLeft,Temp.HTBLeft);
        SetTile(FromPos.X+1,FromPos.Y-1,Temp);
      end;

      if PMTTo.PBottomRight<>nil then
      begin
        Temp:=PMTTo.PBottomRight^;
        DoDamageOnWall(Temp.TRight,Temp.HTTRight);
        DoDamageOnWall(Temp.TLeft,Temp.HTTLeft);
        SetTile(FromPos.X+1,FromPos.Y,Temp);
      end;

      if PMTTo.PTopRight<>nil then
      begin
        Temp:=PMTTo.PTopRight^;
        DoDamageOnWall(Temp.BLeft,Temp.HTBLeft);
        DoDamageOnWall(Temp.BRight,Temp.HTBRight);
        SetTile(FromPos.X,FromPos.Y-1,Temp);
      end;
    end;
    // (7) Unten (X=0, Y=+1)
    7 :
    begin
      DoDamageOnWall(PMTTo.TRight,PMTTo.HTTRight);
      if PMTTo.PTopRight<>nil then
      begin
        Temp:=PMTTo.PTopRight^;
        DoDamageOnWall(Temp.BLeft,Temp.HTBLeft);
        SetTile(FromPos.X,FromPos.Y-1,Temp);
      end;
    end;
    // (8) Unten Rechts (X=+1, Y=+1)
    8:
    begin
      DoDamageOnWall(PMTTo.TLeft,PMTTo.HTTLeft);
      DoDamageOnWall(PMTTo.TRight,PMTTo.HTTRight);

      if PMTTo.PTop<>nil then
      begin
        Temp:=PMTTo.PTop^;
        DoDamageOnWall(Temp.BLeft,Temp.HTBLeft);
        DoDamageOnWall(Temp.BRight,Temp.HTBRight);
        SetTile(FromPos.X-1,FromPos.Y-1,Temp);
      end;

      if PMTTo.PTopLeft<>nil then
      begin
        Temp:=PMTTo.PTopLeft^;
        DoDamageOnWall(Temp.TRight,Temp.HTTRight);
        DoDamageOnWall(Temp.BRight,Temp.HTBRight);
        SetTile(FromPos.X-1,FromPos.Y,Temp);
      end;

      if PMTTo.PTopRight<>nil then
      begin
        Temp:=PMTTo.PTopRight^;
        DoDamageOnWall(Temp.BLeft,Temp.HTBLeft);
        DoDamageOnWall(Temp.TLeft,Temp.HTTLeft);
        SetTile(FromPos.X,FromPos.Y-1,Temp);
      end;
    end;
  end;

  SetTile(FromPos.X,FromPos.Y,PMTTo);

end;

{ TISOObject }

constructor TISOObject.Create(Engine: TDXISOEngine);
begin
  fHitPoints:=-1;
  fFreed:=false;
  fBefore:=false;
  fDeleted:=false;
  fFirst:=true;
  fISOEngine:=Engine;
  fMap:=Engine.ISOMap;
  fISOEngine.fObjectList.Add(Self);
  fX:=-1;
  fY:=-1;
  fVisible:=true;
  fClipping:=false;
  fShowInfoText:=false;
  fIsShowInfo:=false;
  fBlockEndGame:=false;
end;

procedure TISOObject.SetPos(X, Y: Integer);
var
  Dummy: Integer;
begin
  if (fX=X) and (fY=Y) then exit;
  if (fX>=0) and (fY>=0) and (fX<ISOMap.Width) and (fY<ISOMap.Height) then
  begin
    // Alten Eintrag l�schen
    with fMap.Figures[fX,fY] do
    begin
      for Dummy:=0 to ObjectCount-1 do
      begin
        if Objects[Dummy]=Self then
        begin
          if ObjectCount>1 then
            Objects[Dummy]:=Objects[ObjectCount-1];
          dec(ObjectCount);
          break;
        end;
      end;
    end;
  end;
  if fFreed then
    exit;
  if (X<0) or (Y<0) or (X>=ISOMap.Width) or (Y>=ISOMap.Height) then
  begin
    fDeleted:=true;
    exit;
  end;
  fx:=X;
  fy:=Y;
  if fFirst then
  begin
    fStart:=Point(fX,fY);
    fFirst:=false;
  end;
  if fX<>-1 then
  begin
    // Neuen Eintrag setzen
    with fMap.Figures[fX,fY] do
    begin
      if ObjectCount=Length(Objects) then
        SetLength(Objects,ObjectCount+2);
      Objects[ObjectCount]:=Self;
      inc(ObjectCount);
    end;
  end;
end;

destructor TISOObject.destroy;
begin
  Assert(fDeleted,'ISOObject darf nicht direkt freigegeben werden. Einfach Deleted auf true setzen.');
  ShowInfoText(false);
  SetFigure(nil);

  fFreed:=true;
  SetPos(-1,-1);
  fISOEngine.fObjectList.Remove(Self);
  inherited;
end;

procedure TISOObject.SetPos(Point: TPoint);
begin
  SetPos(Point.X,Point.Y);
end;

function TISOObject.ShowInfoText(Visible: Boolean): Boolean;
begin
  result:=false;
  if not fShowInfoText then
    exit;

  if fInfoText='' then
    Visible:=false;

  if Visible=fIsShowInfo then
    exit;

  if (Visible) and fDeleted then
    exit;

  if Visible then
  begin
    Rec.Text:=fInfoText;
    Rec.Figure:=nil;
    Rec.Objekt:=Self;
    SendVMessage(vmAddInfoText);
    fInfoTextID:=Rec.InfoTextID;
  end
  else
  begin
    Rec.InfoTextID:=fInfoTextID;
    SendVMessage(vmDeleteInfoText);
  end;
  fIsShowInfo:=Visible;
  result:=true;
end;

function TISOObject.PerformFrame: boolean;
begin
  result:=false;
end;

procedure TISOObject.ScreenOffSet(X, Y: Integer);
begin
end;

function TISOObject.GetAttackStrength: Integer;
begin
  result:=0;
end;

function TISOObject.GetStartPoint: TPoint;
begin
  result:=fStart;
end;

function TISOObject.GetAttackPower: Integer;
begin
  result:=0;
end;

procedure TISOObject.Explode;
begin
  fDeleted:=true;
end;

procedure TISOObject.DoTreffer(Strength: Integer);
begin
  if fDeleted or (fHitPoints=-1) then
    exit;

  dec(fHitPoints,Strength);

  if fHitPoints<0 then
    Explode;
end;

function TISOObject.GetVisible: boolean;
begin
  Result := fVisible and (not fDeleted);
end;

function TISOObject.CheckWay(FromPoint, ToPoint: TPoint): Boolean;
var
  WayFree: TWayFreeResult;
begin
  Engine.ISOMap.IsWayFree(FromPoint,ToPoint,WayFree);
  result:=WayFree.WayBlocked in [wbDoor,wbObject,wbWall,wbTransparentWall,wbOutOfBounds];
end;

// Variablen f�r Initialization
var
  Dummy: Integer;

procedure TISOObject.SetInfoTextRect(var Rect: TRect);
begin
end;

function TISOObject.CheckOffset(OffSet: TFloatPoint;
  out NewOffSet: TFloatPoint): TPoint;
var
  Pos: TPoint;
  Col: TColor;
begin
  Pos:=Position;
  NewOffSet:=OffSet;

  while (NewOffSet.X)<0 do
  begin
    dec(Pos.X);
    inc(Pos.Y);
    NewOffSet.X:=NewOffSet.X+TileWidth;
  end;

  while (NewOffSet.X>TileWidth) do
  begin
    inc(Pos.X);
    dec(Pos.Y);
    NewOffSet.X:=NewOffSet.X-TileWidth;
  end;

  while (NewOffSet.Y<0) do
  begin
    dec(Pos.X);
    dec(Pos.Y);
    NewOffSet.Y:=NewOffSet.Y+TileHeight;
  end;

  while (NewOffSet.Y>TileHeight) do
  begin
    inc(Pos.X);
    inc(Pos.Y);
    NewOffSet.Y:=NewOffSet.Y-TileHeight;
  end;

  Col:=ISOMap.MouseMap.Canvas.Pixels[trunc(NewOffSet.X),trunc(NewOffSet.Y)];
  if Col=clGreen then
  begin
    inc(Pos.Y);
    NewOffSet.X:=NewOffSet.X+HalfTileWidth;
    NewOffSet.Y:=NewOffSet.Y-HalfTileHeight;
  end
  else if Col=clTeal then
  begin
    inc(Pos.X);
    NewOffSet.X:=NewOffSet.X-HalfTileWidth;
    NewOffSet.Y:=NewOffSet.Y-HalfTileHeight;
  end
  else if Col=clOlive then
  begin
    dec(Pos.Y);
    NewOffSet.X:=NewOffSet.X-HalfTileWidth;
    NewOffSet.Y:=NewOffSet.Y+HalfTileHeight;
  end
  else if Col=clMaroon then
  begin
    dec(Pos.X);
    NewOffSet.X:=NewOffSet.X+HalfTileWidth;
    NewOffSet.Y:=NewOffSet.Y+HalfTileHeight;
  end;

  result:=Pos;
end;

procedure TISOObject.SetFigure(const Value: TGameFigure);
begin
  if fFigure<>nil then
    fFigure.NotifyList.RemoveEvent(fFigureDestroy);

  fFigure := Value;
  if Value<>nil then
    fFigureDestroy:=Value.NotifyList.RegisterEvent(EVENT_FIGURE_ONDESTROY,DestroyFigure);
end;

procedure TISOObject.Draw(Surface: TDirectDrawSurface; X, Y: Integer);
begin
end;

function TISOObject.GetSchadensTyp: TSchadensTyp;
begin
  result:=stGlobal;
end;

procedure TISOObject.GetPositionVektor(out X, Y, Z: Integer);
begin
  X:=HalfTileWidth;
  Y:=HalfTileHeight;
  Z:=0;
end;

function TISOObject.GetBlockEndGame: Boolean;
begin
  Result := fBlockEndGame and (not fDeleted);
end;

procedure TISOObject.DestroyFigure(Sender: TObject);
begin
  fFigure:=nil;
end;

function TISOObject.GetPosition: TPoint;
begin
  result:=Point(X,Y);
end;

// Hier gibt es einen Integer zur�ck, der angibt, wieviel Durschlagskraft ben�tigt wird,
// um die W�nde zu durchbrechen
// Damage ist der Schaden, der an der Mauer verursacht wird
function TISOObject.CheckWay(OffSet: TFloatPoint; ZAxis: Integer; Pos: TPoint; ObjektSize: Integer;Damage: Integer): Integer;
var
  Wall    : TWallType;
  X       : Integer;
  FromPos : TPoint;
begin
  Assert(ObjektSize>=1);
  if trunc(OffSet.X)<HalfTileWidth then
  begin
    X:=trunc(OffSet.X);

    if trunc(OffSet.Y)<HalfTileHeight then
    begin
      Wall:=wtTLeft;
      FromPos:=Point(Pos.X-1,Pos.y);
    end
    else
    begin
      Wall:=wtBLeft;
      FromPos:=Point(Pos.X,Pos.y+1);
    end;
  end
  else
  begin
    X:=trunc(OffSet.X)-HalfTileWidth;

    if trunc(OffSet.Y)<HalfTileHeight then
    begin
      Wall:=wtTRight;
      FromPos:=Point(Pos.X,Pos.y-1);
    end
    else
    begin
      Wall:=wtBRight;
      FromPos:=Point(Pos.X+1,Pos.y);
    end;
  end;

  result:=ISOMap.CheckWalls(X,ZAxis,Wall,Pos,ObjektSize);

  if (result>0) and (Damage>0) then
    ISOMap.DoDamageOnWalls(FromPos,Pos,Damage);
end;

procedure TISOObject.SetDeleted(const Value: boolean);
begin
  fDeleted := Value;
end;

initialization

  for Dummy:=0 to 359 do
  begin
    SinTab[Dummy]:=Sin(DegToRad(Dummy));
    CosTab[Dummy]:=Cos(DegToRad(Dummy));
  end;

end.


