{********************************************************************************
*										*
* Copyright (C) 2004  X-Force Team - jim_raynor@web.de				*
*										*
* This file is part of X-Force: Fight For Destiny (X-Force).			*
*										*
* X-Force is free software; you can redistribute it and/or modify		*
* it under the terms of the GNU General Public License as published by		*
* the Free Software Foundation; either version 2 of the License, or		*
* (at your option) any later version.						*
*										*
* X-Force is distributed in the hope that it will be useful,			*
* but WITHOUT ANY WARRANTY; without even the implied warranty of		*
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the			*
* GNU General Public License for more details.					*
*										*
* You should have received a copy of the GNU General Public License		*
* along with X-Force; if not, write to the Free Software			*
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA	*
*										*
*********************************************************************************
*										*
* Dialog mit einer Listbox. Wird z.B. bei der Auswahl der Basis benutzt		*
*										*
********************************************************************************}

{$I ../Settings.inc}

unit DXListBoxWindow;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  DXContainer, DXDraws, DXBitmapButton, math, Blending, TraceFile, XForce_types,
  DXListBox, NGTypes, DirectDraw, DirectFont, Defines, StringConst;

type
  TDXListBoxWindow = class(TDXComponent)
  private
    fCaption      : String;
    fCaptionColor : TColor;
    fBlendColor   : TBlendColor;
    fMaxRows      : Integer;
    fCaptionFont  : TDirectFont;
    procedure SetCaption(const Value: String);
    procedure SetFirstColor(const Value: TColor);
    procedure SetSecondColor(const Value: TColor);
    procedure SetCaptionColor(const Value: TColor);
    procedure SetAccColor(const Value: TColor);
    procedure SetBlendColor(const Value: TBlendColor);
    procedure PressKey(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure ListDblClick(Sender: TObject; Button: TMouseButton;Shift: TShiftState; X, Y: Integer);
    procedure DrawItem(Sender: TDXComponent;Surface: TDirectDrawSurface;const Mem: TDDSurfaceDesc;Rect: TRect; Index: Integer;Selected: boolean);
    function GetFirstColor: TColor;
    function GetAccColor: TColor;
    function GetItems: TStringList;
    { Private-Deklarationen }
  protected
    { Protected-Deklarationen }
    ModalValue: Integer;
    procedure DrawText(Surface: TDirectDrawSurface);
    procedure ButtonClick(Sender: TObject);
  public
    CancelButton  : TDXBitmapButton;
    OKButton      : TDXBitmapButton;
    ListBox       : TDXListBox;
    constructor Create(Page: TDXPage);override;
    procedure RedrawRect(DrawRect: TRect;Surface: TDirectDrawSurface;var Mem: TDDSurfaceDesc);override;
    procedure Draw(Surface: TDirectDrawSurface;var Mem: TDDSurfaceDesc);override;
    procedure KeyPress(var Key: Word;State: TShiftState);override;
    procedure Resize(var NewLeft,NewTop,NewWidth,NewHeight: Integer);override;
    procedure SetOwnerDraw(ItemHeight: Integer;MaxRow: Integer;OnDrawItem: TDXOwnerDrawEvent);
    function Show: Integer;
    property Caption         : String read fCaption write SetCaption;
    property FirstColor      : TColor read GetFirstColor write SetFirstColor;
    property SecondColor     : TColor write SetSecondColor;
    property CaptionColor    : TColor read fCaptionColor write SetCaptionColor;
    property AccerlateColor  : TColor read GetAccColor write SetAccColor;
    property BlendColor      : TBlendColor read fBlendColor write SetBlendColor;
    property Items           : TStringList read GetItems;
    property MaxRows         : Integer read fMaxRows write fMaxRows;
  end;


implementation

uses KD4Utils;

{ TDXListBoxWindow }

constructor TDXListBoxWindow.Create(Page: TDXPage);
var
  Font: TFont;
begin
  inherited;
  Visible:=false;
  fMaxRows:=8;
  Font:=TFont.Create;
  Font.Name:=coFontName;
  Font.Size:=coFontSize+4;
  Font.Color:=clYellow;
  Font.Style:=[fsBold];
  fCaptionFont:=FontEngine.FindDirectFont(Font,clBlack);
  Font.Free;

  { Einstellungen f�r den Abbrechen Button }
  CancelButton:=TDXBitmapButton.Create(Page);
  with CancelButton do
  begin
    Visible:=false;
    Text:=BCancel;
    Height:=28;
    RoundCorners:=rcRightBottom;
    OnClick:=ButtonClick;
    BlendAlpha:=150;
  end;

  { Einstellungen f�r den OK Button }
  OKButton:=TDXBitmapButton.Create(Page);
  with OKButton do
  begin
    Visible:=false;
    OnClick:=ButtonClick;
    RoundCorners:=rcLeftBottom;
    Text:=BOK;
    Height:=28;
    BlendAlpha:=150;
  end;

  { Listbox zum Ausw�hlen }
  ListBox:=TDXListBox.Create(Page);
  with ListBox do
  begin
    ItemHeight:=17;
    Visible:=false;
    RoundCorners:=rcNone;
    OnDblClick:=ListDblClick;
    OnKeyPress:=PressKey;
    OwnerDraw:=true;
    OnDrawItem:=DrawItem;
    FirstColor:=bcDarkNavy;
    AlphaValue:=200;
  end;
end;

procedure TDXListBoxWindow.Draw(Surface: TDirectDrawSurface;var Mem: TDDSurfaceDesc);
begin
  RedrawRect(ClientRect,Surface,Mem);
end;

procedure TDXListBoxWindow.ButtonClick(Sender: TObject);
begin
  if Sender=OKButton then
    ModalValue:=1
  else
    ModalValue:=2;
end;

procedure TDXListBoxWindow.SetCaption(const Value: String);
begin
  fCaption := Value;
end;

function TDXListBoxWindow.Show: Integer;
var
  FocusTemp : TDXComponent;
  Rows      : Integer;
begin
  if Visible or (not Container.CanMessage) then
  begin
    result:=mrCancel;
    exit;
  end;
  Container.UnLockAll;

  Container.MessageLock(true);
  Container.IncLock;
  Container.DeactiveAll;
  Container.DecLock;
  Container.Lock;
  Rows:=min(maxRows,Items.Count);
  height:=(Rows*ListBox.ItemHeight)+57;
  Container.RemoveComponent(OKButton);
  Container.RemoveComponent(CancelButton);
  Container.RemoveComponent(ListBox);
  Container.RemoveComponent(Self);
  Container.RemoveComponent(ListBox.ScrollBar);
  FocusTemp:=Container.FocusControl;
  Visible:=true;
  Container.AddComponent(Self);
  Container.AddComponent(OKButton);
  Container.AddComponent(CancelButton);
  Container.AddComponent(ListBox);
  Container.AddComponent(ListBox.ScrollBar);
  OKButton.Font:=Font;
  CancelButton.Font:=Font;
  ListBox.Font:=Font;
  ListBox.ItemIndex:=0;
  Enabled:=false;
  OKButton.Visible:=true;
  CancelButton.Visible:=true;
  ListBox.Visible:=true;
  OKButton.Enabled:=true;
  CancelButton.Enabled:=true;
  ListBox.Enabled:=true;
  Container.SetFocusControl(ListBox);
  Container.ReleaseCapture;
  Container.UnLock;
  Redraw;
  ModalValue:=-2;
  repeat
    Application.HandleMessage;
  until ModalValue<>-2;
  case ModalValue of
    1: result:=ListBox.ItemIndex;
    else
      result:=-1;
  end;
  Container.incLock;
  Container.LoadGame(true);
  Container.MessageLock(false);
  OKButton.Visible:=false;
  CancelButton.Visible:=false;
  ListBox.Visible:=false;
  Container.ReactiveAll;
  Container.FocusControl:=FocusTemp;
  Visible:=false;
  Container.RemoveComponent(OKButton);
  Container.RemoveComponent(ListBox);
  Container.RemoveComponent(CancelButton);
  Container.RemoveComponent(Self);
  Container.RemoveComponent(ListBox.ScrollBar);
  Container.MessageLock(false);
  Container.LoadGame(false);
  Container.DecLock;
  Container.RedrawArea(ClientRect,Container.Surface);

  Container.LockAll;
end;

function TDXListBoxWindow.GetFirstColor: TColor;
begin
  result:=OKButton.FirstColor;
end;

procedure TDXListBoxWindow.SetFirstColor(const Value: TColor);
begin
  OKButton.FirstColor:=Value;
  CancelButton.FirstColor:=Value;
  ListBox.BorderColor:=Value;
  ListBox.FocusColor:=Value;
end;

procedure TDXListBoxWindow.SetSecondColor(const Value: TColor);
begin
  OKButton.SecondColor:=Value;
  CancelButton.SecondColor:=Value;
end;

procedure TDXListBoxWindow.SetCaptionColor(const Value: TColor);
begin
  fCaptionColor := Value;
  if Visible then Parent.Redraw(Container.Surface);
end;

function TDXListBoxWindow.GetAccColor: TColor;
begin
  Result:=OKButton.AccerlateColor;
end;

procedure TDXListBoxWindow.SetAccColor(const Value: TColor);
begin
  OKButton.AccerlateColor:=Value;
  CancelButton.AccerlateColor:=Value;
end;

procedure TDXListBoxWindow.KeyPress(var Key: Word; State: TShiftState);
begin
  inherited;
  if (LowerCase(Char(Key))='o') or (Char(Key)=#13)  then
  begin
    OKButton.DoClick;
    if ModalValue<>-2 then Key:=0;
  end;
  if (LowerCase(Char(Key))='a') or (Char(Key)=#27) then
  begin
    CancelButton.DoClick;
    if ModalValue<>-2 then Key:=0;
  end;
end;

procedure TDXListBoxWindow.RedrawRect(DrawRect: TRect; Surface: TDirectDrawSurface;
    var Mem: TDDSurfaceDesc);
var
  TempRect      : TRect;
  CaptionRect   : TRect;
begin
  IntersectRect(DrawRect,DrawRect,ClientRect);
  TempRect:=DrawRect;
  TempRect.Top:=max(DrawRect.Top,Top);
  TempRect.Bottom:=min(DrawRect.Bottom,Top+27);
  CaptionRect:=Rect(Left,Top,Right,Top+27);
  BlendRoundRect(CaptionRect,225,bcDarkNavy,Surface,Mem,11,[cLeftTop,cRightTop],TempRect);
  if AlphaElements then
    FramingRect(Surface,Mem,CaptionRect,[cLeftTop,cRightTop],11,bcDarkNavy)
  else
    FramingRect(Surface,Mem,CaptionRect,[cLeftTop,cRightTop],11,bcNavy);
  if (not AlphaControls) and AlphaElements then
    BlendRectangle(CorrectBottomOfRect(ListBox.ClientRect),200,bcDarkNavy,Surface,Mem)
  else if not AlphaElements then
  begin
    BlendRoundRect(OKButton.ClientRect,255,bcNavy,Surface,Mem,11,[cLeftBottom],OKButton.ClientRect);
    BlendRoundRect(CancelButton.ClientRect,255,bcNavy,Surface,Mem,11,[cRightBottom],CancelButton.ClientRect);
    Surface.FillRect(ListBox.ClientRect,bcDarkNavy);
  end;
  DrawText(Surface);
end;

procedure TDXListBoxWindow.DrawText(Surface: TDirectDrawSurface);
begin
  fCaptionFont.Draw(Surface,Left+(Width shr 1)-(fCaptionFont.TextWidth(fCaption) shr 1),Top+4,fCaption );
end;

procedure TDXListBoxWindow.Resize(var NewLeft, NewTop, NewWidth,
  NewHeight: Integer);
begin
  NewLeft:=(Container.Width shr 1)-(Width shr 1);
  NewTop:=(Container.Height shr 1)-(Height shr 1);
  OKButton.SetRect(Left,Bottom-28,(Width shr 1)-1,OKButton.Height);
  CancelButton.SetRect(Right-(Width shr 1),Bottom-28,(Width shr 1),CancelButton.Height);
  ListBox.SetRect(Left,Top+28,Width,Height-57);
end;

procedure TDXListBoxWindow.SetBlendColor(const Value: TBlendColor);
begin
  fBlendColor := Value;
  OKButton.BlendColor:=fBlendColor;
  CancelButton.BlendColor:=fBlendColor;
end;

function TDXListBoxWindow.GetItems: TStringList;
begin
  Result:=ListBox.Items;
end;

procedure TDXListBoxWindow.PressKey(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  KeyPress(Key,Shift);
end;

procedure TDXListBoxWindow.ListDblClick(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  if Button=mbLeft then OKButton.DoClick;
end;

procedure TDXListBoxWindow.DrawItem(Sender: TDXComponent;Surface: TDirectDrawSurface;const Mem: TDDSurfaceDesc;
  Rect: TRect; Index: Integer;Selected: boolean);
var
  Font  : TDirectFont;
begin
  if Selected then
  begin
    ListBox.DrawSelection(Surface,Mem,Rect,bcMaroon);
    Font:=WhiteStdFont;
  end
  else
    Font:=YellowStdFont;
  Font.Draw(Surface,Rect.Left+3,Rect.Top+1,ListBox.Items[Index]);
end;

procedure TDXListBoxWindow.SetOwnerDraw(ItemHeight: Integer;MaxRow: Integer;
  OnDrawItem: TDXOwnerDrawEvent);
begin
  MaxRows:=MaxRow;
  ListBox.ItemHeight:=ItemHeight;
  if @OnDrawItem=nil then
    ListBox.OnDrawItem:=DrawItem
  else
    ListBox.OnDrawItem:=OnDrawItem;
end;

end.
