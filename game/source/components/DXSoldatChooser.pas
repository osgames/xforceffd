{********************************************************************************
*										*
* Copyright (C) 2004  X-Force Team - jim_raynor@web.de				*
*										*
* This file is part of X-Force: Fight For Destiny (X-Force).			*
*										*
* X-Force is free software; you can redistribute it and/or modify		*
* it under the terms of the GNU General Public License as published by		*
* the Free Software Foundation; either version 2 of the License, or		*
* (at your option) any later version.						*
*										*
* X-Force is distributed in the hope that it will be useful,			*
* but WITHOUT ANY WARRANTY; without even the implied warranty of		*
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the			*
* GNU General Public License for more details.					*
*										*
* You should have received a copy of the GNU General Public License		*
* along with X-Force; if not, write to the Free Software			*
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA	*
*										*
*********************************************************************************
*										*
* Stellt die Soldatenliste beim Soldaten->Ausr�sten zur Verf�gung		*
*										*
********************************************************************************}

{$I ../Settings.inc}

unit DXSoldatChooser;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  DXContainer, Blending, DXDraws, DirectDraw, GameFigureManager, XForce_types,
  KD4Utils, DirectFont, TraceFile, RaumschiffList, NotifyList,
  StringConst;

type
  TDXSoldatChooser = class(TDXComponent)
  private
    fManagerList     : TList;
    fActive          : TGameFigureManager;
    fOnChange        : TNotifyEvent;
    fCorners         : TRoundCorners;
    fCorner          : TCorners;
    fNeedRedraw      : boolean;
    fHighLight       : Integer;
    fCanKeyChange    : boolean;
    fNeedScrolls     : boolean;
    fFirstUnitIndex  : Integer;
    fVisibleUnits    : Integer;
    fLeftScrollOver  : boolean;
    fRightScrollOver : boolean;
    fEventChangeKapa : TEventHandle;
    fEventDestroy    : TEventHandle;
    procedure SetActive(const Value: TGameFigureManager);
    procedure SetCorners(const Value: TRoundCorners);
    procedure OnChangeKapazitaet(Sender: TObject);
    procedure SetHighLight(Value: Integer);
    procedure SetManagerList(const Value: TList);
    function  GetLeftStartPos: Integer;
    function  GetFrameRect: TRect;
    procedure DestroyManager(Sender: TObject);
  protected
    function  GetManagerAtPoint(X,Y: Integer): TGameFigureManager;
    procedure CreateMouseRegion;override;

    function  Scroll(Sender: TObject;Frames: Integer): boolean;
  public
    constructor Create(Page: TDXPage);override;
    procedure Draw(Surface: TDirectDrawSurface;var Mem: TDDSurfaceDesc);override;
    procedure MouseMove(X,Y: Integer);override;
    procedure MouseDown(Button: TMouseButton;X,Y: Integer);override;
    procedure MouseUp(Button: TMouseButton;X,Y: Integer);override;
    procedure MouseLeave;override;
    procedure KeyDown(var Key: Word;State: TShiftState);override;
    procedure Resize(var NewLeft,NewTop,NewWidth,NewHeight: Integer);override;
    procedure CheckRedraw;

    function CheckDragNDrop(X,Y: Integer): TGameFigureManager;

    property ManagerList     : TList read fManagerList write SetManagerList;
    property ActiveManager   : TGameFigureManager read fActive write SetActive;
    property OnChange        : TNotifyEvent read fOnChange write fOnChange;
    property RoundCorners    : TRoundCorners read fCorners write SetCorners;
    property CanKeyChange    : boolean read fCanKeyChange write fCanKeyChange;
  end;

implementation

uses
  Defines;

{ TDXSoldatChooser }

function TDXSoldatChooser.CheckDragNDrop(X, Y: Integer): TGameFigureManager;
begin
  dec(X,Left);
  dec(Y,Top);
  result:=GetManagerAtPoint(X,Y);
  if result<>nil then
  begin
    SetHighlight(fManagerList.IndexOf(result));
    Parent.SetHint(Format(ST0309220026,[result.Name]));
  end
  else
  begin
    SetHighLight(-1);
    Parent.SetHint('');
  end;
  if PtInRect(Rect(3,5,13,PersonImageHeight),Point(X,Y)) then
  begin
    if not fLeftScrollOver then
    begin
      fLeftScrollOver:=true;
      Container.AddFrameFunction(Scroll,Self,100);
    end;
  end
  else
  begin
    if fLeftScrollOver then
    begin
      fLeftScrollOver:=false;
      Container.DeleteFrameFunction(Scroll,Self);
    end;
  end;
  if PtInRect(Rect(Width-13,5,Width-3,PersonImageHeight),Point(X,Y)) then
  begin
    if not fRightScrollOver then
    begin
      fRightScrollOver:=true;
      Container.AddFrameFunction(Scroll,Self,100);
    end;
  end
  else
  begin
    if fRightScrollOver then
    begin
      fRightScrollOver:=false;
      if not fLeftScrollOver then
        Container.DeleteFrameFunction(Scroll,Self);
    end;
  end;
end;

procedure TDXSoldatChooser.CheckRedraw;
begin
  if fNeedRedraw then
  begin
    Redraw;
    fNeedRedraw:=false;
  end;
end;

constructor TDXSoldatChooser.Create(Page: TDXPage);
begin
  inherited;
  fHighLight:=-1;
  TabStop:=true;
  fCanKeyChange:=true;
  fFirstUnitIndex:=0;
end;

procedure TDXSoldatChooser.CreateMouseRegion;
var
  FrameRect: TRect;
  RoundRGN : HRGN;
begin
  FrameRect:=GetFrameRect;
  RoundRGN:=CreateRoundRectRgn(FrameRect.Left,FrameRect.Top,FrameRect.Right+1,FrameRect.Bottom+1,20,20);
  fMouseRegion:=CreateRectRgn(Left,Top,Right,Bottom);
  CombineRgn(fMouseRegion,fMouseRegion,RoundRGN,RGN_AND);
  DeleteObject(RoundRGN);
end;

procedure TDXSoldatChooser.DestroyManager(Sender: TObject);
begin
  fActive:=nil;
end;

procedure TDXSoldatChooser.Draw(Surface: TDirectDrawSurface;
  var Mem: TDDSurfaceDesc);
var
  Dummy   : Integer;
  LPos    : Integer;
  LTop    : Integer;
  Figure  : TGameFigureManager;
  Text    : String;
  Alpha   : Integer;
  Color   : TBlendColor;
begin
  BlendRoundRect(ClientRect,150,bcMaroon,Surface,Mem,11,fCorner,ClientRect);
  FramingRect(Surface,Mem,ClientRect,fCorner,11,bcMaroon);
//BlendRectangle(CorrectBottomOfRect(ClientRect),150,bcMaroon,Surface,Mem);
//  Rectangle(Surface,Mem,ClientRect,bcMaroon);
  LPos:=Left+GetLeftStartPos;
  LTop:=Top+5;
  for Dummy:=fFirstUnitIndex to fManagerList.Count-1 do
  begin
    if LPos>Right-(PersonImageWidth+3) then
      break;

    Figure:=TGameFigureManager(fManagerList[Dummy]);
    Figure.DrawGesicht(Surface,Mem,LPos,LTop,false);

    if ((Figure=fActive) and (fHighlight=-1)) then
      Rectangle(Surface,Mem,LPos,LTop,LPos+PersonImageWidth,LTop+PersonImageHeight,bcWhite)
    else if Dummy=fHighlight then
    begin
      BlendRectangle(Rect(LPos,LTop,LPos+PersonImageWidth,LTop+PersonImageHeight-1),50,bcBlue,Surface,Mem);
      Rectangle(Surface,Mem,LPos,LTop,LPos+PersonImageWidth,LTop+PersonImageHeight,bcWhite)
    end
    else
      Rectangle(Surface,Mem,LPos,LTop,LPos+PersonImageWidth,LTop+PersonImageHeight,bcBlack);

    inc(LPos,PersonImageWidth+1);
  end;
  HLine(Surface,Mem,Left+1,Right-1,Bottom-20,bcMaroon);
  
  if fActive<>nil then
  begin
    WhiteBStdFont.Draw(Surface,Left+5,Bottom-17,fActive.Name);
    Text:=Format(LTragLast,[fActive.Kapazitat,fActive.MaxKapazitat]);
    WhiteBStdFont.Draw(Surface,Right-5-WhiteBStdFont.TextWidth(Text),Bottom-17,Text);

    Text:='';

    if fActive.OnBattleField then
      Text:=ST0309220027
    else if (fActive.Raumschiff<>nil) then
    begin
      if ((sabEquipt in fActive.Raumschiff.Abilities)) then
        Text:=ST0309220029;

      Text:=Text+Format(' ('+CR0504280003+')',[fActive.Raumschiff.Name]);
    end
    else
      Text:=ST0309220029;

    WhiteStdFont.Draw(Surface,Left+(Width div 2)-(WhiteBStdFont.TextWidth(Text) div 2),Bottom-17,Text);
  end;

  if fNeedScrolls then
  begin
    if fFirstUnitIndex=0 then
    begin
      Color:=bcDisabled;
      Alpha:=50;
    end
    else
    begin
      if fLeftScrollOver then
        Alpha:=150
      else
        Alpha:=50;

      Color:=bcMaroon;
    end;

    BlendRectangle(Rect(Left+6,LTop+1,Left+22,LTop+PersonImageHeight-1),Alpha,Color,Surface,Mem);
    Rectangle(Surface,Mem,Left+5,LTop,Left+23,LTop+PersonImageHeight,Color);
    Container.ImageList.Items[0].Draw(Surface,Left+9,LTop+(PersonImageHeight div 2)-6,2);

    if fFirstUnitIndex=fManagerList.Count-fVisibleUnits then
    begin
      Color:=bcDisabled;
      Alpha:=50;
    end
    else
    begin
      if fRightScrollOver then
        Alpha:=100
      else
        Alpha:=50;

      Color:=bcMaroon;

    end;

    BlendRectangle(Rect(Right-22,LTop+1,Right-6,LTop+PersonImageHeight-1),Alpha,Color,Surface,Mem);
    Rectangle(Surface,Mem,Right-23,LTop,Right-5,LTop+PersonImageHeight,Color);
    Container.ImageList.Items[0].Draw(Surface,Right-20,LTop+(PersonImageHeight div 2)-6,0);
  end;
end;

function TDXSoldatChooser.GetFrameRect: TRect;
begin
  result:=ClientRect;
  if fCorners in [rcLeft,rcLeftTop,rcLeftBottom,rcNone] then inc(result.Right,20);
  if fCorners in [rcTop,rcLeftTop,rcRightTop,rcNone] then inc(result.Bottom,20);
  if fCorners in [rcRight,rcRightTop,rcRightBottom,rcNone] then dec(result.Left,20);
  if fCorners in [rcBottom,rcLeftBottom,rcRightBottom,rcNone] then dec(result.Top,20);
end;

function TDXSoldatChooser.GetLeftStartPos: Integer;
begin
  if fNeedScrolls then
    result:=24
  else
    result:=5;
end;

function TDXSoldatChooser.GetManagerAtPoint(X,
  Y: Integer): TGameFigureManager;
var
  LPos   : Integer;
  Dummy  : Integer;
begin
  result:=nil;
  if (Y<5) or (Y>=PersonImageHeight) then exit;
  Lpos:=GetLeftStartPos;
  for Dummy:=fFirstUnitIndex to fManagerList.Count-1 do
  begin
    if LPos>Right-(PersonImageWidth+3) then break;
    if (X>=LPos) and (X<LPos+PersonImageWidth) and (not TGameFigureManager(fManagerList[Dummy]).IsDead) then
    begin
      result:=TGameFigureManager(fManagerList[Dummy]);
      exit;
    end;
    inc(LPos,PersonImageWidth+1);
  end;
end;

procedure TDXSoldatChooser.KeyDown(var Key: Word; State: TShiftState);
var
  Index   : Integer;
  First   : TGameFigureManager;
  Figure  : TGameFigureManager;
begin
  if fCanKeyChange and (Char(Key) in [#37,#39]) then
  begin
    Index:=fManagerList.IndexOf(fActive);
    First:=fActive;
    if Char(Key)=#39 then
    begin
      while true do
      begin
        Index:=(Index+1) mod fManagerList.Count;

        Figure:=TGameFigureManager(fManagerList[Index]);
        if (Figure=First) then break;
        if (not Figure.IsDead) then break;
      end;
    end
    else
    begin
      while true do
      begin
        dec(Index);
        if Index<0 then Index:=fManagerList.Count-1;

        Figure:=TGameFigureManager(fManagerList[Index]);
        if (Figure=First) then break;
        if (not Figure.IsDead) then break;
      end;
    end;
    ActiveManager:=TGameFigureManager(fManagerList[Index]);
  end;
  inherited;
end;

procedure TDXSoldatChooser.MouseDown(Button: TMouseButton; X, Y: Integer);
var
  Manager  : TGameFigureManager;
begin
  Manager:=GetManagerAtPoint(X,Y);
  if (Manager<>nil) then
    ActiveManager:=Manager;
  if fLeftScrollOver or fRightScrollOver then
  begin
    Container.AddFrameFunction(Scroll,Self,100);
  end;
end;

procedure TDXSoldatChooser.MouseLeave;
begin
  inherited;
  if fLeftScrollOver or fRightScrollOver then
  begin
    fLeftScrollOver:=false;
    fRightScrollOver:=false;
    Redraw;
  end;
end;

procedure TDXSoldatChooser.MouseMove(X, Y: Integer);
var
  Manager    : TGameFigureManager;
  NeedRedraw : boolean;
begin
  Manager:=GetManagerAtPoint(X,Y);
  if Manager=nil then
    Hint:=''
  else
    Hint:=Manager.Name;
  NeedRedraw:=false;
  if PtInRect(Rect(5,5,23,PersonImageHeight+5),Point(X,Y)) then
  begin
    if not fLeftScrollOver then
    begin
      fLeftScrollOver:=true;
      NeedRedraw:=true;
    end;
  end
  else
  begin
    if fLeftScrollOver then
    begin
      fLeftScrollOver:=false;
      NeedRedraw:=true;
    end;
  end;
  if PtInRect(Rect(Width-23,5,Width-5,PersonImageHeight+5),Point(X,Y)) then
  begin
    if not fRightScrollOver then
    begin
      fRightScrollOver:=true;
      NeedRedraw:=true;
    end;
  end
  else
  begin
    if fRightScrollOver then
    begin
      fRightScrollOver:=false;
      NeedRedraw:=true;
    end;
  end;
  if NeedRedraw then Redraw;
end;

procedure TDXSoldatChooser.MouseUp(Button: TMouseButton; X, Y: Integer);
begin
  inherited;
  Container.DeleteFrameFunction(Scroll,Self);
end;

procedure TDXSoldatChooser.OnChangeKapazitaet(Sender: TObject);
begin
  if Sender=fActive then
  begin
    if Container.IsLoadGame then
      fNeedRedraw:=true
    else
      Redraw;
  end;
end;

procedure TDXSoldatChooser.Resize(var NewLeft, NewTop, NewWidth,
  NewHeight: Integer);
begin
  inherited;
  fVisibleUnits:=(NewWidth-20) div (PersonImageWidth+1);
end;

function TDXSoldatChooser.Scroll(Sender: TObject;
  Frames: Integer): boolean;
begin
  result:=false;
  if fLeftScrollOver then
  begin
    dec(fFirstUnitIndex);
    if fFirstUnitIndex<0 then
      fFirstUnitIndex:=0
    else
    begin
      Redraw;
      result:=true;
    end;
  end
  else if fRightScrollOver then
  begin
    inc(fFirstUnitIndex);
    if fFirstUnitIndex>fManagerList.Count-fVisibleUnits then
      fFirstUnitIndex:=fManagerList.Count-fVisibleUnits
    else
    begin
      result:=true;
      Redraw;
    end;
  end;
end;

procedure TDXSoldatChooser.SetActive(const Value: TGameFigureManager);
var
  Index: Integer;
begin
  if Value.IsDead then
    exit;
    
  if fActive<>nil then
  begin
    fActive.NotifyList.RemoveEvent(fEventChangeKapa);
    fActive.NotifyList.RemoveEvent(fEventDestroy);
  end;
  fActive := Value;

  if fNeedScrolls then
  begin
    Index:=fManagerList.IndexOf(Value);
    if (Index<fFirstUnitIndex) then
      fFirstUnitIndex:=Index
    else if (Index>=fFirstUnitIndex+fVisibleUnits) then
    begin
      fFirstUnitIndex:=fManagerList.IndexOf(Value)+1-fVisibleUnits;
      if fFirstUnitIndex<0 then
        fFirstUnitIndex:=0;
    end;
  end
  else
    fFirstUnitIndex:=0;

  fEventChangeKapa:=fActive.NotifyList.RegisterEvent(EVENT_MANAGER_ONCHANGE,OnChangeKapazitaet);
  fEventDestroy:=fActive.NotifyList.RegisterEvent(EVENT_MANAGER_ONDESTROY,DestroyManager);
  if Assigned(fOnChange) then fOnChange(Self);
  Redraw;
end;

procedure TDXSoldatChooser.SetCorners(const Value: TRoundCorners);
begin
  fCorners := Value;
  if fCorners in [rcLeft,rcTop,rcLeftTop,rcAll] then include(fCorner,cLeftTop);
  if fCorners in [rcRight,rcTop,rcRightTop,rcAll] then include(fCorner,cRightTop);
  if fCorners in [rcLeft,rcBottom,rcLeftBottom,rcAll] then include(fCorner,cLeftBottom);
  if fCorners in [rcRight,rcBottom,rcRightBottom,rcAll] then include(fCorner,cRightBottom);
  RecreateMouseRegion;
  Redraw;
end;

procedure TDXSoldatChooser.SetHighLight(Value: Integer);
begin
  if Value=fHighlight then exit;
  fHighLight:=Value;
  Redraw;
end;

procedure TDXSoldatChooser.SetManagerList(const Value: TList);
var
  Dummy: Integer;
begin
  fManagerList := Value;
  for Dummy:=fManagerList.Count-1 downto 0 do
  begin
    if (fManagerList[Dummy]=nil) {$IFNDEF ALLMANAGERS}or (TGameFigureManager(fManagerList[Dummy]).FigureStatus<>fsFriendly){$ENDIF} then
      fManagerList.Delete(Dummy);
  end;
  fNeedScrolls:=(fManagerList.Count*(PersonImageWidth+1))>Width-38;
  fLeftScrollOver:=false;
  fRightScrollOver:=false;
end;

end.
