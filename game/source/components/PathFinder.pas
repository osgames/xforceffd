{********************************************************************************
*										*
* Copyright (C) 2004  X-Force Team - jim_raynor@web.de				*
*										*
* This file is part of X-Force: Fight For Destiny (X-Force).			*
*										*
* X-Force is free software; you can redistribute it and/or modify		*
* it under the terms of the GNU General Public License as published by		*
* the Free Software Foundation; either version 2 of the License, or		*
* (at your option) any later version.						*
*										*
* X-Force is distributed in the hope that it will be useful,			*
* but WITHOUT ANY WARRANTY; without even the implied warranty of		*
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the			*
* GNU General Public License for more details.					*
*										*
* You should have received a copy of the GNU General Public License		*
* along with X-Force; if not, write to the Free Software			*
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA	*
*										*
*********************************************************************************
*										*
* Realsiert das Pathfinding f�r den Bodeneinsatz mit Hilfe des A*-Algorhytmus	*
* (TPathfinder und TWay) und stellt eine Klasse zur Verwaltung von Formationen  *
* (TGroupFormation) zur Verf�gung.						*
*										*
********************************************************************************}

{$I ../Settings.inc}

unit PathFinder;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  XForce_types, DXDraws, DirectDraw, Blending, TraceFile, math, KD4Utils,
  StringConst, ISOMessages, ISOTools, Defines;

type
  { Pathfinding }
  PPathNode = ^TPathNode;
  TPathNode = record
    f,h        : Double;
    g,tmpg     : Integer;
    x,y        : Integer;
    NodeNum    : Integer;
    Parent     : PPathNode;
    Child      : Array[0..7] of PPathNode;
    Direction  : TViewDirection;
    NextNode   : PPathNode;
  end;

  PPathStack = ^TPathStack;
  TPathStack = record
    NodePtr      : PPathNode;
    NextStackPtr : PPathStack;
  end;

  TWayFreeFunc = procedure(FromP: TPoint; ToP: TPoint;var result: TWayFreeResult) of object;

  TGroupFormation = class;

  TPathFinder = class(TObject)
  private
    fTiles          : TTileArray;
    Open            : PPathNode;
    Closed          : PPathNode;
    Path            : PPathNode;
    Stack           : PPathStack;
    isPath          : boolean;
    fWay            : Array of TPoint;
    fWayCost        : Integer;
    fIsWayFree      : TWayFreeFunc;
    fWidth          : Integer;
    fHeight         : Integer;
    fUmgeheObject   : TObject;
    fMaxSchritts    : Integer;

    { Verwaltung Formationen }
    fFormation      : TGroupFormation;
    fIgnoreObject   : TObject;

    { Path Finding }
    procedure FreeNodes;
    function FindPath(sx,sy,dx,dy: Integer;StartDirection: TViewDirection): boolean;
    procedure GenerateSuccessors(BestNode: PPathNode;dx,dy: Integer);
    procedure GenerateSucc(BestNode: PPathNode;x,y,dx,dy: Integer;TU: Integer; Direction: TViewDirection);
    procedure Insert(Successor: PPathNode);
    procedure PropagateDown(Old: PPathNode);
    procedure Push(Node: PPathNode);

    function CalcHeuristic(Source: TPoint; Dest: TPoint): double;

    procedure SetUmgeheObject(const Value: TObject);

    function ReturnBestNode: PPathNode;
    function TileNum(x,y: Integer): Integer;
    function CheckOpen(TileNum: Integer): PPathNode;
    function CheckClosed(TileNum: Integer): PPathNode;
    function Pop: PPathNode;
    function GetWayPoints: Integer;
    function GetWayPoint(Index: Integer): TPoint;
  protected
    { Protected-Deklarationen }
  public
    constructor Create;
    destructor destroy;override;
    function GetMapSize: TPoint;
    procedure SetTileArray(Tiles: TTileArray; Width, Height: Integer);

    { Altes Interface }
    function CreatePath(sx,sy,dx,dy: Integer; StartDirection: TViewDirection): boolean;

    property IsWayFree : TWayFreeFunc read fIsWayFree write fIsWayFree;

    property WayPoints : Integer read GetWayPoints;
    property WayCost   : Integer read fWayCost;
    property Way[Index: Integer] : TPoint read GetWayPoint;

    { Neues Interface }
    procedure SetZielPos(FromPos,ToPos: TPoint);

    function GetNextZiel: TPoint;

    property Formation           : TGroupFormation read fFormation write fFormation;

    property UmgeheObject        : TObject         read fUmgeheObject write SetUmgeheObject;
    property IgnoreUnit          : TObject         read fIgnoreObject write fIgnoreObject;

    { Public-Deklarationen }
  end;

  PWayPoint       = ^TWayPoint;
  TWayPoint       = record
    X             : Integer;
    Y             : Integer;
    Reached       : boolean;
    TimeUnits     : Integer;
    Next          : PWayPoint;
  end;

  TWay = class(TObject)
  private
    fNextPoint : PWayPoint;
    fZiel      : PWayPoint;
    fList      : TList;
    fTimeUnits : Integer;
    fFigure    : TObject;
    function CreateWayPointNode: PWayPoint;
    procedure FreeWayPointNode(Node: PWayPoint);
    function GetHasPath: boolean;
    function GetNextPoint: PWayPoint;
    function GetZiel: PWayPoint;
    function GetReached: boolean;
    procedure SetReached(const Value: boolean);
    function GetZielErreicht: boolean;
    function GetTimeUnits: Integer;
  public
    constructor Create;
    destructor Destroy;override;
    procedure Clear;
    procedure TraceWay;
    procedure AddPointAtEnd(Point: TPoint);
    procedure AddPointBefore(Point: TPoint);
    procedure GoToNextPoint;
    procedure SetNextPointToZiel;
    procedure SetFigurePtr(Figure: TObject);

    procedure ShowWayPoints(Show: Boolean);

    function PointInSteps(Steps: Integer): PWayPoint;
    function ConvertToPoint(WayPoint: PWayPoint): TPoint;
    property TimeUnits      : Integer read GetTimeUnits;
    property NextPoint      : PWayPoint read GetNextPoint;
    property Ziel           : PWayPoint read GetZiel;
    property HasPath        : boolean read GetHasPath;
    property ReachedPoint   : boolean read GetReached write SetReached;
    property ZielErreicht   : boolean read GetZielErreicht;
  end;

  TGroupFormation = class(TObject)
  private
    fPositions     : Array[0..7] of Array of TPoint;
    fDrawXOffSet   : Integer;
    fDrawYOffSet   : Integer;
    fName          : String;
    fWinkelInd     : Integer;
    fPositionCount : Integer;

    { Positions berechnung }
    fFirstPos    : TPoint;
    fFromPos     : TPoint;
    fPosition    : Integer;
    fMapSize     : TSize;

  public
    constructor Create;
    procedure DrawSymbol(Surface: TDirectDrawSurface;X,Y: Integer);
    procedure SetPositions(Positions: Array of TPoint);
    procedure SetzePosition(FromPos: TPoint; ToPos: TPoint; MapSize: TSize);overload;
    procedure SetzePosition(Position: TPoint; MapSize: TSize);overload;
    function GetNextZiel: TPoint;

    procedure LoadFromStream(Stream: TStream);

    property FormationName : String read fName write fName;
  end;

implementation

uses GameFigure;
{ TPathFinder }

function TPathFinder.CheckClosed(TileNum: Integer): PPathNode;
var
  tmp: PPathNode;
begin
  tmp:=Closed.NextNode;
  while (tmp<>nil) do
  begin
    if tmp.NodeNum=TileNum then
    begin
      result:=tmp;
      exit;
    end
    else tmp:=tmp.NextNode;
  end;
  result:=nil;
end;

function TPathFinder.CheckOpen(TileNum: Integer): PPathNode;
var
  tmp: PPathNode;
begin
  tmp:=Open.NextNode;
  while (tmp<>nil) do
  begin
    if tmp.NodeNum=TileNum then
    begin
      result:=tmp;
      exit;
    end
    else tmp:=tmp.NextNode;
  end;
  result:=nil;
end;

constructor TPathFinder.Create;
begin
  fMaxSchritts:=-1;
  Stack:=AllocMem(SizeOf(TPathStack));
  Open:=nil;
  Closed:=nil;
  Path:=nil;
end;

function TPathFinder.CreatePath(sx, sy, dx, dy: Integer; StartDirection: TViewDirection): boolean;
var
  LastIndex: Integer;
begin
  SetLength(fWay,0);
  assert((sx>=0) and (sx<fWidth),Format('Source X nicht g�ltig : %d',[sx]));
  assert((sy>=0) and (sy<fHeight),Format('Source Y nicht g�ltig : %d',[sy]));
  assert((dx>=0) and (dx<fWidth),Format('Destination X nicht g�ltig : %d',[dx]));
  assert((dy>=0) and (dy<fHeight),Format('Destination Y nicht g�ltig : %d',[dy]));

  if (fTiles[sx,sy].Obj=-1) and (fTiles[dx,dy].Obj=-1) and (TileNum(sx,sy)<>TileNum(dx,dy)) and (fTiles[dx,dy].Begehbar) then
  begin
    FreeNodes;
    if not FindPath(dx,dy,sx,sy,StartDirection) then
    begin
      IsPath:=false;
      result:=false;
      exit;
    end;
    IsPath:=true;
    result:=true;
    fWayCost:=Path.g;
    LastIndex:=0;
    repeat
      if LastIndex>high(fWay) then
        SetLength(fWay,length(fWay)+50);

      MoveMemory(Addr(fWay[1]),Addr(fWay[0]),sizeOf(fWay[0])*LastIndex);
      fWay[0]:=Point(Path.x,Path.y);
      Path:=Path.Parent;
      inc(LastIndex);
    until Path.Parent=nil;
    SetLength(fWay,LastIndex+1);
    MoveMemory(Addr(fWay[1]),Addr(fWay[0]),sizeOf(fWay[0])*LastIndex);
    fWay[0]:=Point(Path.x,Path.y);
  end
  else
  begin
    IsPath:=false;
    result:=false;
  end;
end;

destructor TPathFinder.destroy;
begin
  FreeNodes;
  FreeMem(Stack);
  inherited;
end;

function TPathFinder.FindPath(sx, sy, dx, dy: Integer; StartDirection: TViewDirection): boolean;
var
  Node,BestNode : PPathNode;
  TileNumDest   : Integer;
begin
  BestNode:=nil;
  TileNumDest:=TileNum(sx,sy);
  Open:=AllocMem(SizeOf(TPathNode));
  Closed:=AllocMem(SizeOf(TPathNode));
  Node:=AllocMem(SizeOf(TPathNode));
  Node.g:=0;
  Node.tmpg:=0;
  Node.h:=CalcHeuristic(Point(dx,dy),Point(sx,sy));
  Node.f:=(Node.g)+Node.h;
  Node.NodeNum:=TileNum(dx,dy);
  Node.x:=dx;
  Node.y:=dy;
  Node.Direction:=StartDirection;
  Open.NextNode:=Node;
  result:=false;
  while true do
  begin
    BestNode:=ReturnBestNode;
    if BestNode=nil then
      exit;
    if (BestNode.NodeNum=TileNumDest) then break;
    GenerateSuccessors(BestNode,sx,sy);
  end;
  result:=true;
  Path:=BestNode;
end;

procedure TPathFinder.FreeNodes;
var
  Node,OldNode: PPathNode;
begin
  if Open<>nil then
  begin
    Node:=Open;
    while (Node<>nil) do
    begin
      OldNode:=Node;
      Node:=Node.NextNode;
      FreeMem(OldNode);
    end;
  end;
  if Closed<>nil then
  begin
    Node:=Closed;
    while (Node<>nil) do
    begin
      OldNode:=Node;
      Node:=Node.NextNode;
      FreeMem(OldNode);
    end;
  end;
end;

procedure TPathFinder.GenerateSucc(BestNode: PPathNode; x, y, dx, dy,
  TU: Integer; Direction: TViewDirection);
var
  g,TileNumS,c: Integer;
  Old,Successor: PPathNode;
begin
  g:=BestNode.g+TU;
  if fMaxSchritts<>-1 then
    if g>fMaxSchritts then
      exit;
  TileNumS:=TileNum(x,y);
  Old:=CheckOpen(TileNumS);
  if Old<>nil then
  begin
    for c:=0 to 7 do
      if BestNode.Child[c]=nil then break;
    BestNode.Child[c]:=Old;
    if (g<Old.g) or ((g=Old.g) and (TU<Old.tmpg)) then
    begin
      Old.Parent:=BestNode;
      Old.g:=g;
      Old.Direction:=Direction;
      Old.tmpg:=TU;
      Old.f:=(g)+Old.h;
    end;
  end
  else
  begin
    Old:=CheckClosed(TileNumS);
    if Old<>nil then
    begin
      for c:=0 to 7 do
        if BestNode.Child[c]=nil then break;
      BestNode.Child[c]:=Old;
      if (g<Old.g) or ((g=Old.g) and (TU<Old.tmpg)) then
      begin
        Old.Parent:=BestNode;
        Old.tmpg:=TU;
        Old.g:=g;
        Old.Direction:=Direction;
        Old.f:=(g)+Old.h;
        PropagateDown(Old);
      end;
    end
    else
    begin
      Successor:=AllocMem(SizeOf(TPathNode));
      Successor.Parent:=BestNode;
      Successor.g:=g;
      Successor.Direction:=Direction;
      Successor.h:=CalcHeuristic(Point(x,y),point(dx,dy));
      Successor.f:=(g)+Successor.h;
      Successor.x:=x;
      Successor.y:=y;
      Successor.tmpg:=TU;
      Successor.NodeNum:=TileNumS;
      Insert(Successor);
      for c:=0 to 7 do
        if BestNode.Child[c]=nil then break;
      BestNode.Child[c]:=Successor;
    end;
  end;
end;

procedure TPathFinder.GenerateSuccessors(BestNode: PPathNode; dx,
  dy: Integer);
var
  x,y      : Integer;
  Res      : TWayFreeResult;

  procedure GeneratePos(Point: TPoint; Direction: TViewDirection);
  var
    Kost : Integer;
  begin
//    Direction:=GetDirection(Point,Classes.Point(X,Y));
    if Direction<>BestNode.Direction then
      Kost:=4
    else
      Kost:=3;

    IsWayFree(Classes.Point(x,y),Point,Res);
    if Res.WayBlocked in [wbNone,wbDoor] then
      GenerateSucc(BestNode,Point.X,Point.Y,dx,dy,Kost,Direction)
    else if Res.WayBlocked=wbSoldat then
    begin
      if (TGameFigure(Res.BlockObj).FigureStatus=fsEnemy) then
        GenerateSucc(BestNode,Point.X,Point.Y,dx,dy,Kost+6,Direction)
      else if (fIgnoreObject=Res.BlockObj) or (fUmgeheObject=nil) or TGameFigure(Res.BlockObj).IsMoving then
        GenerateSucc(BestNode,Point.X,Point.Y,dx,dy,Kost+100,Direction);
    end;
  end;

begin
  x:=BestNode.X;
  y:=BestNode.Y;
  {     x-1 x x+1 }
  { y-1  +  +  +  }
  { y    +  -  +  }
  { y+1  +  +  +  }
(*                      {vdLeft}       ((X:  0; Y:  1),
                      {vdLeftTop}     (X: -1; Y:  1),
                      {vdTop}         (X: -1; Y:  0),
                      {vdRightTop}    (X: -1; Y: -1),
                      {vdRight}       (X:  0; Y: -1),
                      {vdRightBottom} (X:  1; Y: -1),
                      {vdBottom}      (X:  1; Y:  0),
                      {vdLeftBottom}  (X:  1; Y:  1),*)
  GeneratePos(Point(x,y+1),vdLeft);
  GeneratePos(Point(x-1,y+1),vdLeftTop);
  GeneratePos(Point(x-1,y),vdTop);
  GeneratePos(Point(x-1,y-1),vdRightTop);
  GeneratePos(Point(x,y-1),vdRight);
  GeneratePos(Point(x+1,y-1),vdRightBottom);
  GeneratePos(Point(x+1,y),vdBottom);
  GeneratePos(Point(x+1,y+1),vdLeftBottom);

end;

function TPathFinder.GetMapSize: TPoint;
begin
  result:=Point(fWidth,fHeight);
end;

function TPathFinder.GetWayPoint(Index: Integer): TPoint;
begin
  result:=fWay[Index];
end;

function TPathFinder.GetWayPoints: Integer;
begin
  result:=Length(fWay);
end;

procedure TPathFinder.Insert(Successor: PPathNode);
var
  tmp1,tmp2 : PPathNode;
  f         : Double;
begin
  if (Open.NextNode=nil) then
  begin
    Open.NextNode:=Successor;
    exit;
  end;
  f:=Successor.f;
  tmp1:=Open;
  tmp2:=Open.NextNode;
  while (tmp2<>nil) and (tmp2.f<f) do
  begin
    tmp1:=tmp2;
    tmp2:=tmp2.NextNode;
  end;
  Successor.NextNode:=tmp2;
  tmp1.NextNode:=Successor;
end;

function TPathFinder.Pop: PPathNode;
var
  tmp: PPathNode;
  tmpStk: PPathStack;
begin
  tmpStk:=Stack.NextStackPtr;
  tmp:=tmpStk.NodePtr;
  Stack.NextStackPtr:=tmpStk.NextStackPtr;
  freeMem(tmpSTK);
  result:=tmp;
end;

procedure TPathFinder.PropagateDown(Old: PPathNode);
var
  c,g         : Integer;
  Child,Father: PPathNode;
begin
  g:=Old.g;
  for c:=0 to 7 do
  begin
    Child:=Old.Child[c];
    if Child=nil then break;
    if (g+Child.tmpg<Child.g) then
    begin
      Child.g:=g+Child.tmpg;
      Child.f:=(Child.g)+Child.h;
      Child.Parent:=Old;
      Push(Child);
    end;
  end;
  while (Stack.NextStackPtr<>nil) do
  begin
    Father:=Pop;
    for c:=0 to 7 do
    begin
      Child:=Father.Child[c];
      if Child=nil then break;
      if Father.g+Child.tmpg < Child.g then
      begin
        Child.g:=Father.g+Child.tmpg;
        Child.f:=(Child.g)+Child.h;
        Child.Parent:=Father;
        Push(Child);
      end;
    end;
  end;
end;

procedure TPathFinder.Push(Node: PPathNode);
var
  tmp: PPathStack;
begin
  tmp:=AllocMem(SizeOf(TPathStack));
  tmp.NodePtr:=Node;
  tmp.NextStackPtr:=Stack.NextStackPtr;
  Stack.NextStackPtr:=tmp;
end;

function TPathFinder.ReturnBestNode: PPathNode;
var
  tmp: PPathNode;
begin
  if Open.NextNode=nil then
  begin
    result:=nil;
    exit;
  end;
  tmp:=Open.NextNode;
  Open.NextNode:=tmp.NextNode;
  tmp.NextNode:=Closed.NextNode;
  Closed.NextNode:=tmp;
  result:=tmp;
end;

procedure TPathFinder.SetZielPos(FromPos,ToPos: TPoint);
begin
  Formation.SetzePosition(FromPos,ToPos,TSize(Point(fWidth,fHeight)));
end;

procedure TPathFinder.SetTileArray(Tiles: TTileArray; Width, Height: Integer);
begin
  fTiles:=Tiles;
  fWidth:=Width;
  fHeight:=Height;
end;

function TPathFinder.TileNum(x, y: Integer): Integer;
begin
  result:=(y*fWidth)+x;
end;

function TPathFinder.GetNextZiel: TPoint;
begin
  result:=Formation.GetNextZiel;
end;

procedure TPathFinder.SetUmgeheObject(const Value: TObject);
begin
  fUmgeheObject := Value;
  if Value=nil then
    fMaxSchritts:=-1
  else
    fMaxSchritts:=10;
end;

function TPathFinder.CalcHeuristic(Source, Dest: TPoint): double;
begin
  if abs(Source.X-Dest.X)>abs(Source.Y-Dest.Y) then
    result:=abs(Source.X-Dest.X)*3
  else
    result:=abs(Source.Y-Dest.Y)*3;
    
  if (Source.X<>Dest.X) and (Source.Y<>Dest.Y) then
    result:=result+1;

  result:=(result*10)+(Power(Dest.X-Source.X,2)*Power(Dest.Y-Source.Y,2))
end;

{ TGroupFormation }

constructor TGroupFormation.Create;
begin
  fDrawXOffSet:=14;
  fDrawYOffSet:=14;
end;

procedure TGroupFormation.DrawSymbol(Surface: TDirectDrawSurface; X,
  Y: Integer);
var
  Dummy  : Integer;
  Mem    : TDDSurfaceDesc;
  CenterX: Integer;
  CenterY: Integer;
begin
  Surface.Lock(Mem);
  Surface.Unlock;
  Surface.FillRect(Rect(X+2,Y+2,X+29,Y+29),(bcMaroon shr 1) and Surface.SurfaceDesc.ddpfPixelFormat.dwRBitMask);
  Rectangle(Surface,Mem,X+1,Y+1,X+30,Y+30,bcMaroon);
  for Dummy:=0 to high(fPositions[0]) do
  begin
    if (abs(fPositions[0,Dummy].X)<4) and (abs(fPositions[0,Dummy].Y)<4) then
    begin
      CenterX:=(fDrawXOffSet+(fPositions[0,Dummy].X*4));
      CenterY:=(fDrawYOffSet+(fPositions[0,Dummy].Y*4));
      Surface.Pixels[x+1+CenterX,Y+1+CenterY]:=bcRed;
      Surface.Pixels[x+1+CenterX,Y+2+CenterY]:=bcMaroon;
      Surface.Pixels[x+2+CenterX,Y+1+CenterY]:=bcMaroon;
      Surface.Pixels[x+  CenterX,Y+1+CenterY]:=bcMaroon;
      Surface.Pixels[x+1+CenterX,Y+  CenterY]:=bcMaroon;
    end;
  end;
end;

function TGroupFormation.GetNextZiel: TPoint;
var
  PointOK: boolean;
begin
  PointOK:=false;
  result.X:=-1;
  repeat
    if fPosition>=fPositionCount then
    begin
      result.X:=-1;
      exit;
    end;

    with fPositions[fWinkelInd,fPosition] do
    begin
      result.X:=fFirstPos.X+X;
      result.Y:=fFirstPos.Y+Y;
    end;
    inc(fPosition);
    PointOK:=PtInRect(Rect(0,0,fMapSize.cx,fMapSize.cy),result);
  until PointOK;
end;

procedure TGroupFormation.LoadFromStream(Stream: TStream);
var
  Buffer    : Cardinal;
  Count     : Integer;
  Dummy     : Integer;
  Field     : Array of TPoint;
begin
  FormationName:=ReadString(Stream);
  // Anzahl der Position in der Formation
  Stream.Read(Buffer,SizeOf(Buffer));
  SetLength(Field,Buffer);
  for Dummy:=0 to Buffer-1 do
  begin
    with Field[Dummy] do
    begin
      Stream.Read(X,SizeOf(X));
      Stream.Read(Y,SizeOf(X));
    end;
  end;
  SetPositions(Field);
end;

procedure TGroupFormation.SetPositions(Positions: array of TPoint);
var
  Dummy: Integer;
begin
  fPositionCount:=length(Positions);
  for Dummy:=0 to 7 do
    SetLength(fPositions[Dummy],fPositionCount);
  for Dummy:=0 to fPositionCount-1 do
  begin
    with Positions[Dummy] do
    begin
      fPositions[0,Dummy]:=Point(X,Y);
      fPositions[1,Dummy]:=Point((X-Y),(Y+X));
      fPositions[2,Dummy]:=Point(-Y,X);
      fPositions[3,Dummy]:=Point(-(X+Y),-(Y-X));
      fPositions[4,Dummy]:=Point(X,Y);
      fPositions[5,Dummy]:=Point((X-Y),(Y+X));
      fPositions[6,Dummy]:=Point(-Y,X);
      fPositions[7,Dummy]:=Point(-(X+Y),-(Y-X));
    end;
  end;
end;

procedure TGroupFormation.SetzePosition(FromPos, ToPos: TPoint; MapSize: TSize);
begin
  fFirstPos:=ToPos;
  fFromPos:=FromPos;
  fPosition:=0;
  fMapSize:=MapSize;

  fWinkelInd:=CalculateWinkel(FromPos,ToPos) div 45;
end;

procedure TGroupFormation.SetzePosition(Position: TPoint; MapSize: TSize);
begin
  fFirstPos:=Position;
  fFromPos:=Position;
  fPosition:=0;
  fMapSize:=MapSize;

  fWinkelInd:=random(8);
end;

{ TWay }

procedure TWay.AddPointAtEnd(Point: TPoint);
var
  NewPoint: PWayPoint;
begin
  NewPoint:=CreateWayPointNode;
  NewPoint.X:=Point.X;
  NewPoint.Y:=Point.Y;
  if fZiel=nil then
  begin
    fZiel:=NewPoint;
    fNextPoint:=NewPoint;
    NewPoint.Reached:=true;
  end
  else
  begin
    fZiel.Next:=NewPoint;
    fZiel:=NewPoint;
  end;
end;

procedure TWay.Clear;
var
  Dummy: Integer;
begin
  fZiel:=nil;
  fNextPoint:=nil;
  for Dummy:=fList.Count-1 downto 0 do
  begin
    FreeWayPointNode(fList[Dummy]);
  end;
end;

function TWay.ConvertToPoint(WayPoint: PWayPoint): TPoint;
begin
  Assert(WayPoint<>nil,'Kein Wegpunkt angegeben');
  result.X:=WayPoint.X;
  result.Y:=WayPoint.Y;
end;

function TWay.CreateWayPointNode: PWayPoint;
begin
  GetMem(Result,SizeOf(TWayPoint));
  fList.Add(Result);
  result.Next:=nil;
  result.TimeUnits:=0;
  result.Reached:=false;
  fTimeUnits:=-1;
end;

destructor TWay.Destroy;
begin
  Clear;
  fList.Free;
  inherited;
end;

function TWay.GetHasPath: boolean;
begin
  result:=(fZiel<>nil) and (fNextPoint<>nil);
end;

function TWay.GetNextPoint: PWayPoint;
begin
  Assert(HasPath,'Kein Path gespeichert');
  Assert(fNextPoint<>nil,'NextPoint nicht angegeben');
  result:=fNextPoint;
end;

function TWay.GetReached: boolean;
begin
  if fNextPoint<>nil then
    result:=fNextPoint.Reached
  else
    result:=true;
end;

function TWay.GetZiel: PWayPoint;
begin
  Assert(HasPath,'Kein Path gespeichert');
  result:=fZiel;
end;

function TWay.GetZielErreicht: boolean;
begin
  result:=fZiel=fNextPoint;
end;

procedure TWay.GoToNextPoint;
var
  Old   : PWayPoint;
begin
  if fNextPoint=nil then exit;
  Old:=fNextPoint;
  fNextPoint:=fNextPoint.Next;
  // Wegpunkt ausblenden
  FreeWayPointNode(Old);

  if fNextPoint=nil then
    fZiel:=nil;
end;

procedure TWay.AddPointBefore(Point: TPoint);
var
  NewPoint: PWayPoint;
begin
  NewPoint:=CreateWayPointNode;
  NewPoint.X:=Point.X;
  NewPoint.Y:=Point.Y;
  NewPoint.Next:=fNextPoint;
  fNextPoint:=NewPoint;
  if fZiel=nil then
    fZiel:=fNextPoint;
end;

function TWay.PointInSteps(Steps: Integer): PWayPoint;
var
  tmp: PWayPoint;
begin
  tmp:=fNextPoint;
  while tmp<>nil do
  begin
    tmp:=tmp.Next;
    dec(Steps);
    if (Steps=0) and (tmp<>nil) then
    begin
      result:=tmp;
      exit;
    end;
  end;
  Assert(false);
end;

procedure TWay.SetNextPointToZiel;
var
  tmp: PWayPoint;
  Old: PWayPoint;
begin
  if fZiel=fNextPoint then exit;
  fZiel:=fNextPoint;
  tmp:=fNextPoint.Next;
  while tmp<>nil do
  begin
    Old:=tmp;
    tmp:=tmp.Next;
    FreeWayPointNode(Old);
  end;
  fNextPoint.Next:=nil;
end;

procedure TWay.SetReached(const Value: boolean);
begin
  if fNextPoint<>nil then
    fNextPoint.Reached:=Value;
end;

procedure TWay.TraceWay;
var
  tmp: PWayPoint;
  Dummy: Integer;
begin
  GlobalFIle.WriteLine;
  tmp:=fNextPoint;
  Dummy:=0;
  while tmp<>nil do
  begin
    GlobalFile.Write(IntToStr(Dummy),Point(tmp.X,tmp.Y));
    inc(Dummy);
    tmp:=tmp.Next;
  end;
  GlobalFile.WriteLine;
end;

procedure TWay.FreeWayPointNode(Node: PWayPoint);
begin
  Rec.Bool:=false;
  Rec.Point.X:=Node.X;
  Rec.Point.Y:=Node.Y;
  SendVMessage(vmShowWayPoint);

  FreeMem(Node);
  fList.Remove(Node);
  fTimeUnits:=-1;
end;

constructor TWay.Create;
begin
  fList:=TList.Create;
  fTimeUnits:=-1;
end;

procedure TWay.ShowWayPoints(Show: Boolean);
var
  Point     : PWayPoint;
  TimeUnits : Integer;
begin
  SendVMessage(vmContainerLock);
  Point:=fNextPoint;
  if Show then
  begin
    // Berechnen der Zeiteinheiten pro Feld
    TimeUnits:=GetTimeUnits;

    TimeUnits:=TGameFigure(fFigure).ZeitEinheiten;
  end;
  while Point<>nil do
  begin
    if Show then
    begin
      dec(TimeUnits,Point.TimeUnits);
      if TimeUnits>=(TGameFigure(fFigure).MaxZeiteinheiten*0.666) then
        Rec.ColorInd:=3
      else if TimeUnits>=(TGameFigure(fFigure).MaxZeiteinheiten*0.4) then
        Rec.ColorInd:=2
      else if TimeUnits>=0 then
        Rec.ColorInd:=1
      else
        Rec.ColorInd:=0;
      if Point.Next<>nil then
        Rec.Direction:=GetDirection(ConvertToPoint(Point),ConvertToPoint(Point.Next));
    end;
    rec.Bool:=Show;
    Rec.Point.X:=Point.X;
    Rec.Point.Y:=Point.Y;
    SendVMessage(vmShowWayPoint);
    Point:=Point.Next;
  end;
  SendVMessage(vmContainerUnLock);
end;

function TWay.GetTimeUnits: Integer;
var
  LastDirect : TViewDirection;
  NewDirect  : TViewDirection;
  LastPoint  : TPoint;
  tmp        : PWayPoint;
begin
  if fTimeUnits=-1 then
  begin
    fTimeUnits:=0;
    LastDirect:=TGameFigure(fFigure).ViewDirection;
    tmp:=fNextPoint;
    while tmp<>nil do
    begin
      LastPoint:=ConvertToPoint(tmp);
      tmp:=tmp.Next;
      if tmp<>nil then
      begin
        NewDirect:=GetDirection(LastPoint,ConvertToPoint(tmp));
        if NewDirect<>LastDirect then
        begin
          LastDirect:=NewDirect;
          tmp.TimeUnits:=TUMoveField+TUChangeView;
        end
        else
          tmp.TimeUnits:=TUMoveField;
        Inc(fTimeUnits,tmp.TimeUnits);
      end;
    end;
  end;
  result:=fTimeUnits;
end;

procedure TWay.SetFigurePtr(Figure: TObject);
begin
  Assert(Figure is TGameFigure);
  fFigure:=Figure;
end;

end.
