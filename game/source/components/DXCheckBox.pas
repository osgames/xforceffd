{********************************************************************************
*										*
* Copyright (C) 2004  X-Force Team - jim_raynor@web.de				*
*										*
* This file is part of X-Force: Fight For Destiny (X-Force).			*
*										*
* X-Force is free software; you can redistribute it and/or modify		*
* it under the terms of the GNU General Public License as published by		*
* the Free Software Foundation; either version 2 of the License, or		*
* (at your option) any later version.						*
*										*
* X-Force is distributed in the hope that it will be useful,			*
* but WITHOUT ANY WARRANTY; without even the implied warranty of		*
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the			*
* GNU General Public License for more details.					*
*										*
* You should have received a copy of the GNU General Public License		*
* along with X-Force; if not, write to the Free Software			*
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA	*
*										*
*********************************************************************************
*										*
* Diese Unit stellt eine Checkbox zur Verf�gung					*
*										*
********************************************************************************}

{$I ../Settings.inc}

unit DXCheckBox;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  DXContainer,DXDraws,KD4Utils, Defines, Blending, XForce_types, DirectDraw, DirectFont;

type
  TDXCheckBox = class(TDXComponent)
  private
    fChecked          : boolean;
    fMargin           : Integer;
    fOldHotKey        : Char;
    fCaption          : TCaption;
    fBColor           : TColor;
    fOnChange         : TNotifyEvent;
    fCorners          : TRoundCorners;
    fCorner           : TCorners;
    fBlendColor       : TBlendColor;
    fMouseOver        : boolean;
    fRadio            : Boolean;
    procedure SetBorderColor(const Value: TColor);
    procedure SetCaption(const Value: TCaption);
    procedure SetChecked(const Value: boolean);
    procedure SetMargin(const Value: Integer);
    procedure SetCorners(const Value: TRoundCorners);
    function GetFrameRect: TRect;
    { Private-Deklarationen }
  protected
    procedure CreateMouseRegion;override;
    { Protected-Deklarationen }
  public
    constructor Create(Page: TDXPage);override;
    procedure Draw(Surface: TDirectDrawSurface;var Mem: TDDSurfaceDesc);override;
    procedure DoClick;Override;
    procedure MouseOver;override;
    procedure MouseLeave;override;
    property Caption        : TCaption read fCaption write SetCaption;
    property LeftMargin     : Integer read fMargin write SetMargin;
    property Checked        : boolean read fChecked write SetChecked;
    property BorderColor    : TColor read fBColor write SetBorderColor;
    property BlendColor     : TBlendColor read fBlendColor write fBlendColor;
    property OnChange       : TNotifyEvent read fOnChange write fOnChange;
    property RoundCorners   : TRoundCorners read fCorners write SetCorners;
    property RadioButton    : Boolean read fRadio write fRadio;
  end;

implementation

uses
  draw_utils;


{ TDXCheckBox }

constructor TDXCheckBox.Create(Page: TDXPage);
begin
  inherited;
  fCorners:=rcAll;
  fMouseOver:=false;
  fRadio:=false;
end;

procedure TDXCheckBox.CreateMouseRegion;
var
  FrameRect: TRect;
  RoundRGN : HRGN;
begin
  FrameRect:=GetFrameRect;
  RoundRGN:=CreateRoundRectRgn(FrameRect.Left,FrameRect.Top,FrameRect.Right+1,FrameRect.Bottom+1,20,20);
  fMouseRegion:=CreateRectRgn(Left,Top,Right,Bottom);
  CombineRgn(fMouseRegion,fMouseRegion,RoundRGN,RGN_AND);
  DeleteObject(RoundRGN);
end;

procedure TDXCheckBox.DoClick;
begin
  inherited;
  if fRadio then
    fChecked:=true
  else
    fChecked:=not fChecked;
  Container.PlaySound(SClick);
  if Assigned(fOnChange) then fOnChange(Self);
  Redraw;
end;

procedure TDXCheckBox.Draw(Surface: TDirectDrawSurface;var Mem: TDDSurfaceDesc);
begin
  if DrawEnabled then
  begin
    if AlphaElements then
    begin
      BlendRoundRect(ClientRect,175,fBlendColor,Surface,Mem,11,fCorner,Rect(Left+1,Top+1,Left+Height+1,Bottom-1));
      BlendRoundRect(ClientRect,125,fBlendColor,Surface,Mem,11,fCorner,Rect(Left+Height+1,Top+1,Right-1,Bottom-1));
    end;
    FramingRect(Surface,Mem,ClientRect,fCorner,11,fBColor);
    VLine(Surface,Mem,Top+1,Bottom-1,Left+Height+1,fBColor);
    if fMouseOver then
      Rectangle(Surface,Mem,Left+5,Top+4,Left+Height-3,Bottom-4,fBColor);
    if Checked then
    begin
      if fRadio then
        Container.ImageList.Items[0].Draw(Surface,Left+(Height shr 1)-5,Top+(Height shr 1)-5,10)
      else
        Container.ImageList.Items[0].Draw(Surface,Left+(Height shr 1)-5,Top+(Height shr 1)-5,4);
    end;
    draw_utils_AccerlateTextOut(Surface,Font,Height+fMargin+Left,(Height shr 1)-(WhiteStdFont.TextHeight(fCaption) shr 1)+Top,fCaption,clYellow);
  end
  else
  begin
    if AlphaElements then
    begin
      BlendRoundRect(ClientRect,175,bcGray,Surface,Mem,11,fCorner,Rect(Left+1,Top+1,Left+Height+1,Bottom-1));
      BlendRoundRect(ClientRect,125,bcGray,Surface,Mem,11,fCorner,Rect(Left+Height+1,Top+1,Right-1,Bottom-1));
    end;
    FramingRect(Surface,Mem,ClientRect,fCorner,11,bcDisabled);
    VLine(Surface,Mem,Top+1,Bottom-1,Left+Height+1,bcDisabled);
    if Checked then
    begin
      if fRadio then
        Container.ImageList.Items[0].Draw(Surface,Left+(Height shr 1)-5,Top+(Height shr 1)-5,10)
      else
        Container.ImageList.Items[0].Draw(Surface,Left+(Height shr 1)-5,Top+(Height shr 1)-5,4);
    end;
    draw_utils_AccerlateTextOut(Surface,Font,Height+fMargin+Left,(Height shr 1)-(WhiteStdFont.TextHeight(fCaption) shr 1)+Top,StripHotKey(fCaption),$00606060);
  end;
end;

function TDXCheckBox.GetFrameRect: TRect;
begin
  result:=ClientRect;
  if fCorners in [rcLeft,rcLeftTop,rcLeftBottom,rcNone] then inc(result.Right,20);
  if fCorners in [rcTop,rcLeftTop,rcRightTop,rcNone] then inc(result.Bottom,20);
  if fCorners in [rcRight,rcRightTop,rcRightBottom,rcNone] then dec(result.Left,20);
  if fCorners in [rcBottom,rcLeftBottom,rcRightBottom,rcNone] then dec(result.Top,20);
end;

procedure TDXCheckBox.MouseLeave;
begin
  inherited;
  fMouseOver:=false;
  Redraw;
end;

procedure TDXCheckBox.MouseOver;
begin
  inherited;
  Container.PlaySound(SOver);
  fMouseOver:=true;
  Redraw;
end;

procedure TDXCheckBox.SetBorderColor(const Value: TColor);
begin
  fBColor := Container.Surface.ColorMatch(Value);
  Redraw;
end;

procedure TDXCheckBox.SetCaption(const Value: TCaption);
var
  NewHotKey: Char;
begin
  fCaption := Value;
  NewHotKey:=GetShortCut(fCaption);
  if NewHotKey<>fOldHotKey then
  begin
    if fOldHotKey<>#0 then Parent.DeleteHotKey(fOldHotKey,Self);
    if NewHotKey<>#0 then Parent.RegisterHotKey(NewHotKey,Self);
  end;
  Redraw;
end;

procedure TDXCheckBox.SetChecked(const Value: boolean);
begin
  fChecked := Value;
  Redraw;
end;

procedure TDXCheckBox.SetCorners(const Value: TRoundCorners);
begin
  fCorners := Value;
  fCorner:=[];
  if fCorners in [rcLeft,rcTop,rcLeftTop,rcAll] then include(fCorner,cLeftTop);
  if fCorners in [rcRight,rcTop,rcRightTop,rcAll] then include(fCorner,cRightTop);
  if fCorners in [rcLeft,rcBottom,rcLeftBottom,rcAll] then include(fCorner,cLeftBottom);
  if fCorners in [rcRight,rcBottom,rcRightBottom,rcAll] then include(fCorner,cRightBottom);
  RecreateMouseRegion;
  Redraw;
end;

procedure TDXCheckBox.SetMargin(const Value: Integer);
begin
  fMargin := Value;
  Redraw;
end;

end.
