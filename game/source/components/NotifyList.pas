{********************************************************************************
*										*
* Copyright (C) 2004  X-Force Team - jim_raynor@web.de				*
*										*
* This file is part of X-Force: Fight For Destiny (X-Force).			*
*										*
* X-Force is free software; you can redistribute it and/or modify		*
* it under the terms of the GNU General Public License as published by		*
* the Free Software Foundation; either version 2 of the License, or		*
* (at your option) any later version.						*
*										*
* X-Force is distributed in the hope that it will be useful,			*
* but WITHOUT ANY WARRANTY; without even the implied warranty of		*
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the			*
* GNU General Public License for more details.					*
*										*
* You should have received a copy of the GNU General Public License		*
* along with X-Force; if not, write to the Free Software			*
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA	*
*										*
*********************************************************************************
*										*
* Stellt ein Klasse zur Verf�gung um Events zu sammeln				*
* Beispiel: Das Raumschiff stellt eine Notifylist zur Verf�gung			*
*         Nun kann jeder �ber RegisterEvent sich bei einem Ereignis anmelden    *
*         (Die IDs zum Event legt das Raumschiff selber fest)			*
*         �ber CallEvents werden dann alle Registrierten Ereignisse aufgerufen  *
*         und als Sender das �bergebene Objekt �bergeben			*
*       									*
* Diese Klasse sollte immer dann benutzt werden, wenn mehrere Komponenten	*
* auf das Ereignis eines Objektes reagieren k�nnen (z.B. Raumschiff wird 	*
* zerst�rt.									*
*										*
********************************************************************************}

{$I ../Settings.inc}

unit NotifyList;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  TraceFile;

type
  TEventHandle        = type Integer;
  TEventID            = type Integer;

  TNotifyEventRec = record
    Handle        : TEventHandle;
    Enabled       : Boolean;
    EventID       : TEventID;
    Event         : TNotifyEvent;
  end;

  TNotifyList = class(TObject)
  private
    fEvents     : Array of TNotifyEventRec;
    fLastHandle : TEventHandle;
    fCallEvents : Integer;
    fPack       : Boolean;
    { Private-Deklarationen }
  protected
    procedure Pack;
    { Protected-Deklarationen }
  public
    { Public-Deklarationen }

    procedure Clear;
    
    function RegisterEvent(ID: TEventID; CallBack: TNotifyEvent): TEventHandle;

    procedure RemoveEvent(EventHandle: TEventHandle);

    procedure CallEvents(EventID: TEventID; Objekt: TObject);
  end;

implementation

{ TNotifyList }

procedure TNotifyList.CallEvents(EventID: TEventID; Objekt: TObject);
var
  Dummy: Integer;
begin
  inc(fCallEvents);
  for Dummy:=0 to high(fEvents) do
  begin
    if fEvents[Dummy].Enabled and (fEvents[Dummy].EventID=EventID) then
      fEvents[Dummy].Event(Objekt);
  end;
  dec(fCallEvents);

  if fPack then
    Pack;
end;

procedure TNotifyList.RemoveEvent(EventHandle: TEventHandle);
var
  Dummy: Integer;
begin
  for Dummy:=0 to high(fEvents) do
  begin
    if fEvents[Dummy].Handle=EventHandle then
    begin
      fEvents[Dummy].Enabled:=false;
      fEvents[Dummy].EventID:=-1;
      break;
    end;
  end;
  Pack;
end;

function TNotifyList.RegisterEvent(ID: TEventID; CallBack: TNotifyEvent): TEventHandle;
begin
  SetLength(fEvents,Length(fEvents)+1);

  with fEvents[High(fEvents)] do
  begin
    Handle:=fLastHandle;
    Enabled:=true;
    EventID:=ID;
    Event:=CallBack;
  end;
  result:=fLastHandle;
  inc(fLastHandle);
end;

procedure TNotifyList.Pack;
var
  Dummy      : Integer;
  Deleted    : Integer;
  Count      : Integer;
begin
  if fCallEvents>0 then
  begin
    fPack:=true;
    exit;
  end;

  Deleted:=0;
  Dummy:=0;
  Count:=length(fEvents);
  while (Dummy<Count) do
  begin
    if Deleted<>0 then
      fEvents[Dummy]:=fEvents[Dummy+Deleted];

    if not fEvents[Dummy].Enabled then
    begin
      inc(deleted);
      dec(Count);
    end
    else
      inc(Dummy);
  end;
  SetLength(fEvents,length(fEvents)-Deleted);
  fPack:=false;
end;

procedure TNotifyList.Clear;
begin
  SetLength(fEvents,0);
end;

end.
