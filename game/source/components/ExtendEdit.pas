{********************************************************************************
*										*
* Copyright (C) 2004  X-Force Team - jim_raynor@web.de				*
*										*
* This file is part of X-Force: Fight For Destiny (X-Force).			*
*										*
* X-Force is free software; you can redistribute it and/or modify		*
* it under the terms of the GNU General Public License as published by		*
* the Free Software Foundation; either version 2 of the License, or		*
* (at your option) any later version.						*
*										*
* X-Force is distributed in the hope that it will be useful,			*
* but WITHOUT ANY WARRANTY; without even the implied warranty of		*
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the			*
* GNU General Public License for more details.					*
*										*
* You should have received a copy of the GNU General Public License		*
* along with X-Force; if not, write to the Free Software			*
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA	*
*										*
*********************************************************************************
*										*
* Nur f�r den Editor: Eingabekomponente 					*
*										*
********************************************************************************}

{$I ../Settings.inc}

unit ExtendEdit;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  extctrls,stdctrls, menus,Clipbrd;

type
  TExtendBasis = class(TPanel)
  protected
    fError      : boolean;
  public
    constructor Create(AOwner: TComponent);override;
    procedure ShowError(Error: boolean);virtual;
    procedure Resize;override;
  published
    property TabStop;
    property TabOrder;
    property Visible;
  end;

type
  TExtendEdit = class(TExtendBasis)
  private
    LabelRahmen : TPanel;                    
    EditFeld    : TEdit;
    fColor      : TColor;
    fEinheit    : TCaption;
    Popup       : TPopupMenu;
    Item        : TMenuItem;
    fLabelWidth : Integer;
    fDefault    : String;
    fChange     : boolean;
    fReset      : boolean;
    function GetCaption: TCaption;
    procedure SetCaption(const Value: TCaption);
    function GetText: TCaption;
    procedure SetText(const Value: TCaption);
    function GetEditEnabled: boolean;
    procedure SetEditEnabled(const Value: boolean);
    procedure SetEinheit(const Value: TCaption);
    procedure CheckDefault(Sender: TObject);
    procedure DoPopupCommand(Sender: TObject);
    procedure SetLabelWidth(const Value: Integer);
    function GetPasswordChar: Char;
    procedure SetPasswordChar(const Value: Char);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure OnPopup(Sender: TObject);
    function GetOnChange: TNotifyEvent;
    procedure SetOnChange(const Value: TNotifyEvent);
    { Private-Deklarationen }
  protected
    procedure WMSetFocus(var Message: TWMSetFocus); message WM_SETFOCUS;
    procedure LabelClick(Sender: TObject);
    procedure CreatePopup;
    { Protected-Deklarationen }
  public
    constructor Create(AOwner: TComponent);override;
    destructor destroy;override;
    procedure ShowError(Error: boolean);override;
    procedure Resize;override;
    procedure DeactiveReset;
    { Public-Deklarationen }
  published
    property Caption      : TCaption read GetCaption write SetCaption;
    property Text         : TCaption read GetText write SetText;
    property EditEnabled  : boolean read GetEditEnabled write SetEditEnabled;
    property Einheit      : TCaption read fEinheit write SetEinheit;
    property LabelWidth   : Integer read fLabelWidth write SetLabelWidth;
    property PasswordChar : Char read GetPasswordChar write SetPasswordChar;
    property OnChange     : TNotifyEvent read GetOnChange write SetOnChange;
    { Published-Deklarationen }
  end;

  TExtendCheckBox = class(TExtendBasis)
  private
    CheckBox: TCheckBox;
    function GetChecked: boolean;
    procedure SetChecked(const Value: boolean);
    function GetText: TCaption;
    procedure SetText(const Value: TCaption);
    function GetCheckEnabled: boolean;
    procedure SetCheckEnabled(const Value: boolean);
    function GetOnChange: TNotifyEvent;
    procedure SetOnChange(const Value: TNotifyEvent);
  protected
    procedure WMSetFocus(var Message: TWMSetFocus); message WM_SETFOCUS;
  public
    constructor Create(AOwner: TComponent);override;
    procedure Resize;override;
    destructor Destroy; override;
  published
    property Checked : boolean read GetChecked write SetChecked;
    property OnChange       : TNotifyEvent read GetOnChange write SetOnChange;
    property Caption : TCaption read GetText write SetText;
    property Enabled : boolean read GetCheckEnabled write SetCheckEnabled;
  end;

  TExtendComboBox = class(TExtendBasis)
  private
    LabelRahmen : TPanel;
    ComboBox    : TComboBox;
    fLabelWidth: Integer;
    function GetText: TCaption;
    procedure SetText(const Value: TCaption);
    function GetItems: TStrings;
    procedure SetItems(const Value: TStrings);
    function GetItemIndex: Integer;
    procedure SetItemIndex(const Value: Integer);
    function GetOnChange: TNotifyEvent;
    procedure SetOnChange(const Value: TNotifyEvent);
    function GetOnDrawItem: TDrawItemEvent;
    procedure SetOnDrawItem(const Value: TDrawItemEvent);
    function GetBoxEnabled: boolean;
    procedure SetBoxEnabled(const Value: boolean);
    procedure SetLabelWidth(const Value: Integer);
  protected
    procedure WMSetFocus(var Message: TWMSetFocus); message WM_SETFOCUS;
    procedure LabelClick(Sender: TObject);
  public
    constructor Create(AOwner: TComponent);override;
    procedure Resize;override;
    destructor Destroy; override;
    procedure ShowError(Error: boolean);override;
  published
    property Caption        : TCaption read GetText write SetText;
    property Items          : TStrings read GetItems write SetItems;
    property ItemIndex      : Integer read GetItemIndex write SetItemIndex;
    property OnChange       : TNotifyEvent read GetOnChange write SetOnChange;
    property OnDrawItem     : TDrawItemEvent read GetOnDrawItem write SetOnDrawItem stored true;
    property Enabled        : boolean read GetBoxEnabled write SetBoxEnabled;
    property LabelWidth     : Integer read fLabelWidth write SetLabelWidth;
  end;

  TExtendPanel = class(TExtendBasis)
  public
    procedure ShowError(Error: boolean);override;
  end;


procedure Register;

implementation

procedure Register;
begin
  RegisterComponents('KD4 Tools', [TExtendEdit,TExtendCheckBox,TExtendComboBox,TExtendPanel]);
end;

{ TExtendEdit }

procedure TExtendEdit.CheckDefault(Sender: TObject);
begin
  if not fReset then exit;
  if EditFeld.Text=fDefault then
    EditFeld.Font.Style:=[fsBold]
  else
  begin
    EditFeld.Font.Style:=[];
    EditFeld.ParentFont:=true;
  end
end;

constructor TExtendEdit.Create(AOwner: TComponent);
begin
  inherited;
  BevelOuter:=bvNone;

  LabelRahmen:=TPanel.Create(Self);
  fReset:=true;
  fChange:=false;
  fLabelWidth:=88;
  BorderWidth:=2;
  EditFeld:=TEdit.Create(Self);
  with LabelRahmen do
  begin
    Parent:=Self;
    Left:=1;
    Top:=1;
    OnClick:=LabelClick;
    Height:=20;
    Width:=88;
    FullRepaint:=true;
    ParentFont:=false;
    BevelOuter:=bvNone;
    Alignment:=taLeftJustify;
    Caption:='';
  end;
  with EditFeld do
  begin
    AutoSelect:=false;
    Parent:=Self;
    Left:=91;
    OnKeyPress:=FormKeyPress;
    Color:=clWindow;
    OnChange:=CheckDefault;
    Top:=0;
    ParentFont:=true;
    ParentColor:=false;
//    BorderStyle:=bsNone;
    Width:=Width-(fLabelWidth+6);
  end;
  CreatePopup;
end;

procedure TExtendEdit.CreatePopup;

  procedure AddItem(Text,Hint: String;Tag: Integer);
  begin
    Item:=TMenuItem.Create(Self);
    Item.Caption:=Text;
    Item.Hint:=Hint;
    Item.Tag:=Tag;
    Item.OnClick:=DoPopupCommand;
    Popup.Items.Add(Item);
    Popup.OnPopup:=OnPopup;
  end;

begin
  Popup:=TPopupMenu.Create(Self);
  AddItem('Zur�cksetzen','Stellt den gespeicherten Wert wieder her',0);
  AddItem('-','',-1);
  AddItem('R�ckg�ngig','Macht die letzt Eingabe r�ckg�ngig',1);
  AddItem('-','',-1);
  AddItem('Ausschneiden','Schneidet den gew�hlten Text aus und kopiert ihn in die Zwischenablage',2);
  AddItem('Kopieren','Kopiert den gew�hlten Text in die Zwischenablage',3);
  AddItem('Einf�gen','F�gt den Text aus Zwischenablage ein',4);
  AddItem('L�schen','L�scht den gew�hlten Text',5);
  AddItem('-','',-1);
  AddItem('Alles markieren','W�hlt den gesamten Feldinhalt aus',6);
  EditFeld.PopupMenu:=Popup;
end;

procedure TExtendEdit.DeactiveReset;
begin
  Popup.Items.Delete(0);
  Popup.Items.Delete(0);
  fReset:=false;
end;

destructor TExtendEdit.destroy;
begin
  EditFeld.Free;
  LabelRahmen.Free;
  Popup.Free;
  inherited;
end;

procedure TExtendEdit.DoPopupCommand(Sender: TObject);
begin
  case TComponent(Sender).Tag of
    0:
    begin
      fChange:=true;
      Text:=fDefault;
      fChange:=false;
    end;
    1: EditFeld.Undo;
    2: EditFeld.CutToClipboard;
    3: EditFeld.CopyToClipboard;
    4: EditFeld.PasteFromClipboard;
    5: EditFeld.ClearSelection;
    6: EditFeld.SelectAll;
  end;
end;

procedure TExtendEdit.FormKeyPress(Sender: TObject; var Key: Char);
begin
  if Key=#13 then Key:=#0;
end;

function TExtendEdit.GetCaption: TCaption;
begin
  result:=LabelRahmen.Caption;
end;

function TExtendEdit.GetEditEnabled: boolean;
begin
  Result:=EditFeld.Enabled;
end;

function TExtendEdit.GetOnChange: TNotifyEvent;
begin
  result:=EditFeld.OnChange;
end;

function TExtendEdit.GetPasswordChar: Char;
begin
  result:=EditFeld.PasswordChar;
end;

function TExtendEdit.GetText: TCaption;
begin
  result:=EditFeld.Text;
end;

procedure TExtendEdit.LabelClick(Sender: TObject);
begin
  if EditEnabled then EditFeld.SetFocus;
end;

procedure TExtendEdit.OnPopup(Sender: TObject);
var
  Dummy: Integer;
begin
  SetFocus;
  for Dummy:=0 to Popup.Items.Count-1 do
  begin
    with Popup.Items[Dummy] do
    begin
      case Tag of
        0: Enabled:=fDefault<>EditFeld.Text;
        1: Enabled:=EditFeld.CanUndo;
        2: Enabled:=EditFeld.SelLength>0;
        3: Enabled:=EditFeld.SelLength>0;
        4: Enabled:=Clipboard.HasFormat(CF_TEXT);
        5: Enabled:=EditFeld.SelLength>0;
        6: Enabled:=EditFeld.SelLength<>Length(EditFeld.Text);
      end;
    end;
  end;
end;

procedure TExtendEdit.Resize;
begin
  inherited;
  LabelRahmen.Width:=fLabelWidth;
  EditFeld.Width:=Width-(fLabelWidth+6)-Canvas.TextWidth(fEinheit);
  EditFeld.Left:=(fLabelWidth+3);
end;

procedure TExtendEdit.SetCaption(const Value: TCaption);
begin
  LabelRahmen.Caption:=Value;
end;

procedure TExtendEdit.SetEditEnabled(const Value: boolean);
begin
  EditFeld.Enabled:=Value;
end;

procedure TExtendEdit.SetEinheit(const Value: TCaption);
begin
  fEinheit := Value;
  inherited Caption:=Value;
  EditFeld.Width:=Width-(fLabelWidth+6)-Canvas.TextWidth(Value);
end;

procedure TExtendEdit.SetLabelWidth(const Value: Integer);
begin
  fLabelWidth := Value;
  Resize;
end;

procedure TExtendEdit.SetOnChange(const Value: TNotifyEvent);
begin
  EditFeld.OnChange:=Value;
end;

procedure TExtendEdit.SetPasswordChar(const Value: Char);
begin
  EditFeld.PasswordChar:=Value;
end;

procedure TExtendEdit.SetText(const Value: TCaption);
begin
  EditFeld.Text:=Value;
  if not fChange then
  begin
    fDefault:=Value;
    CheckDefault(Self);
  end;
end;


procedure TExtendEdit.ShowError(Error: boolean);
begin
  inherited;
  if Error then
    EditFeld.Font.Color:=clWhite
  else
    EditFeld.Font.Color:=clWindowText;
end;

procedure TExtendEdit.WMSetFocus(var Message: TWMSetFocus);
begin
  EditFeld.SetFocus;
end;

{ TExtendCheckBox }

constructor TExtendCheckBox.Create(AOwner: TComponent);
begin
  inherited;
  CheckBox:=TCheckBox.Create(Self);
  with CheckBox do
  begin
    ParentColor:=true;
    Parent:=Self;
    Left:=2;
    Top:=3;
    Width:=Width-5;
  end;
end;

destructor TExtendCheckBox.Destroy;
begin
  CheckBox.Free;
  inherited;
end;

function TExtendCheckBox.GetChecked: boolean;
begin
  result:=CheckBox.Checked;
end;

function TExtendCheckBox.GetCheckEnabled: boolean;
begin
  result:=CheckBox.Enabled;
end;

function TExtendCheckBox.GetOnChange: TNotifyEvent;
begin
  result:=CheckBox.OnClick;
end;

function TExtendCheckBox.GetText: TCaption;
begin
  result:=CheckBox.Caption;
end;

procedure TExtendCheckBox.Resize;
begin
  inherited;
  CheckBox.Width:=Width-5;
end;

procedure TExtendCheckBox.SetChecked(const Value: boolean);
begin
  if Value=CheckBox.Checked then exit;
  CheckBox.Checked:=Value;
end;

procedure TExtendCheckBox.SetCheckEnabled(const Value: boolean);
begin
  CheckBox.Enabled:=Value;
end;

procedure TExtendCheckBox.SetOnChange(const Value: TNotifyEvent);
begin
  CheckBox.OnClick:=Value;
end;

procedure TExtendCheckBox.SetText(const Value: TCaption);
begin
  CheckBox.Caption:=Value;
end;

procedure TExtendCheckBox.WMSetFocus(var Message: TWMSetFocus);
begin
  CheckBox.SetFocus;
end;

{ TExtendBasis }

constructor TExtendBasis.Create(AOwner: TComponent);
begin
  inherited;
  Height:=25;
  Alignment:=taRightJustify;
  Caption:='';
  Width:=210;
end;

procedure TExtendBasis.Resize;
begin
  inherited;
end;

procedure TExtendBasis.ShowError(Error: boolean);
begin
  fError:=Error;
  if Error then
  begin
    Color:=clMaroon;
    Font.Color:=clWhite;
  end
  else
  begin
    Color:=clBtnFace;
    Font.Color:=clWindowText;
  end;
end;

{ TExtendComboBox }

constructor TExtendComboBox.Create(AOwner: TComponent);
begin
  inherited;
  LabelRahmen:=TPanel.Create(Self);
  fLabelWidth:=88;
  with LabelRahmen do
  begin
    Parent:=Self;
    Left:=1;
    Top:=1;
    OnClick:=LabelClick;
    Height:=20;
    ParentFont:=false;
    FullRepaint:=true;
    ParentColor:=false;
    Width:=88;
    BevelOuter:=bvNone;
    Alignment:=taLeftJustify;
    Caption:='';
  end;
  ComboBox:=TComboBox.Create(Self);
  with ComboBox do
  begin
    Parent:=Self;
    Left:=88;
    Color:=clWindow;
    Style:=csDropDownList;
    Top :=0;
    ParentFont:=true;
    Width:=Self.Width-88;
  end;
end;

destructor TExtendComboBox.Destroy;
begin
  LabelRahmen.Free;
  ComboBox.Free;
  inherited;
end;

function TExtendComboBox.GetItemIndex: Integer;
begin
  result:=ComboBox.ItemIndex;
end;

function TExtendComboBox.GetItems: TStrings;
begin
  result:=ComboBox.Items;
end;

function TExtendComboBox.GetText: TCaption;
begin
  result:=LabelRahmen.Caption;
end;

procedure TExtendComboBox.SetItemIndex(const Value: Integer);
begin
  ComboBox.ItemIndex:=Value;
end;

procedure TExtendComboBox.Resize;
begin
  inherited;
  ComboBox.Left:=fLabelWidth;
  ComboBox.Width:=Width-fLabelWidth;
  LabelRahmen.Width:=fLabelWidth;
end;

procedure TExtendComboBox.SetItems(const Value: TStrings);
begin
  ComboBox.Items.Assign(Value);
end;

procedure TExtendComboBox.SetText(const Value: TCaption);
begin
  LabelRahmen.Caption:=Value;
end;

function TExtendComboBox.GetOnChange: TNotifyEvent;
begin
  result:=ComboBox.OnChange;
end;

procedure TExtendComboBox.SetOnChange(const Value: TNotifyEvent);
begin
  ComboBox.OnChange:=Value;
end;

function TExtendComboBox.GetOnDrawItem: TDrawItemEvent;
begin
  result:=ComboBox.OnDrawItem;
end;

procedure TExtendComboBox.SetOnDrawItem(const Value: TDrawItemEvent);
begin
  if Assigned(Value) then
  begin
    ComboBox.Style:=csOwnerDrawFixed;
    ComboBox.OnDrawItem:=Value;
    ComboBox.Height:=21;
  end
  else
  begin
    ComboBox.Style:=csDropDownList;
    ComboBox.OnDrawItem:=nil;
  end;
end;

function TExtendComboBox.GetBoxEnabled: boolean;
begin
  result:=ComboBox.Enabled;
end;

procedure TExtendComboBox.SetBoxEnabled(const Value: boolean);
begin
  ComboBox.Enabled:=Value;
end;

procedure TExtendComboBox.ShowError(Error: boolean);
begin
  inherited;
  if Error then
  begin
    ComboBox.Color:=clMaroon;
    ComboBox.Font.Color:=clWhite;
  end
  else
  begin
    ComboBox.Color:=clBtnFace;
    ComboBox.Font.Color:=clWindowText;
  end;
end;

procedure TExtendComboBox.WMSetFocus(var Message: TWMSetFocus);
begin
  ComboBox.SetFocus;
end;

procedure TExtendComboBox.LabelClick(Sender: TObject);
begin
  if Enabled then
  ComboBox.SetFocus;
end;

procedure TExtendComboBox.SetLabelWidth(const Value: Integer);
begin
  fLabelWidth := Value;
  Resize;
end;

{ TExtendPanel }

procedure TExtendPanel.ShowError(Error: boolean);
var
  Dummy: Integer;
begin
  for Dummy:=0 to ControlCount-1 do
  begin
    if Controls[Dummy] is TExtendBasis then TExtendBasis(Controls[Dummy]).ShowError(Error);
  end;
end;

end.
