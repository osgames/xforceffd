{********************************************************************************
*										*
* Copyright (C) 2004  X-Force Team - jim_raynor@web.de				*
*										*
* This file is part of X-Force: Fight For Destiny (X-Force).			*
*										*
* X-Force is free software; you can redistribute it and/or modify		*
* it under the terms of the GNU General Public License as published by		*
* the Free Software Foundation; either version 2 of the License, or		*
* (at your option) any later version.						*
*										*
* X-Force is distributed in the hope that it will be useful,			*
* but WITHOUT ANY WARRANTY; without even the implied warranty of		*
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the			*
* GNU General Public License for more details.					*
*										*
* You should have received a copy of the GNU General Public License		*
* along with X-Force; if not, write to the Free Software			*
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA	*
*										*
*********************************************************************************
*										*
* TUFOList verwaltet die UFOs, die in einem Spiel erscheinen. TUFO stellt ein   *
* UFO dar.									*
*										*
********************************************************************************}

{$I ../Settings.inc}

unit UFOList;

interface

uses
  ObjektRecords, Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms,
  Dialogs, XForce_types, Koding, math, KD4Utils, TraceFile, DXDraws, Loader,
  Defines, EarthCalcs, Sequenzer, OrganisationList, EinsatzListe, BasisListe,
  IDHashMap, NotifyList, ExtRecord, StringConst;

{.$DEFINE TRACEUFOSTAT}                                                      
const
  EVENT_ONUFODESTROY    : TEventID = 0;
  EVENT_ONUFOSHOOTDOWN  : TEventID = 1;
  EVENT_ONUFOESCAPE     : TEventID = 2;
  EVENT_ONUFODISCOVERED : TEventID = 3;

type
  {$M+}
  TUFO = class;
  {$M-}

  TUFOList = class(TObject)
  private
    fSchiffe          : TList;
    fUFOModels        : Array of TUFOModel;
    fOriginalModels   : Array of TUFOModel;

    // Hier werden die Models abgelegt (als Index), die im moment angreifen k�nnen
    fAvailModels      : Array of record
                          ModelIndex  : Integer;
                          lastValue   : Integer;
                        end;

    fDay              : Integer;
    fHashMap          : TIDHashMap;
    
    fUFOIcons         : TPictureCollectionItem;

    function GetModelCount: Integer;
    function GetUFO(Index: Integer): TUFO;
    function GetCount: Integer;
    function GetUFOModel(Index: Integer): TUFOModel;
    procedure DeleteUFO(UFO: TUFO);
    procedure ModelAbgleich;
    procedure Clear;
    function GetGesichtet: Integer;

    // Hiermit wird das Array fAvailModels mit den UFOs gef�llt, die zum jetzigen
    // Zeitpunkt angriefen k�nnen
    procedure BuildAvailModels;

    { Private-Deklarationen }
  protected
    { Protected-Deklarationen }
    procedure AddUFO(UFO: TUFO);

    procedure LoadGameSetHandler(Sender: TObject);
    procedure NewGameHandler(Sender: TObject);
  public
    constructor Create;
    destructor destroy;override;
    procedure SetBitmap(Bitmap: TBitmap);
    procedure DrawIcon(Surface: TDirectDrawSurface;x,y: Integer;Symbol: Integer);
    procedure NextDay;
    procedure NextHour(Sender: TObject);
    procedure NextRound(Minuten: Integer);
    procedure LoadFromStream(Stream: TStream);
    procedure SaveToStream(Stream: TStream);

    function CreateUFO: TUFO;
    function CreateUFOFromModel(Model: TUFOModel): TUFO;
    
    function PosOverLand(X,Y: Integer): boolean;
    function UFOInList(UFO: TUFO): boolean;
    function IDofUFO(ID: Cardinal): TUFO;
    function GetIndexOfModel(ID: Cardinal): Integer;
    function ShowFirstMessage(UFO: TUFO): boolean;

    // Setzt bei allen St�dten in aktuellen Spiel das Merkmal Land, so dass keine
    // Stadt �ber Wasser ist (Ereigniss wird in ufo_api_init registriert)
    procedure StartGameHandlerEarthMap(Sender: TObject);

    { Eigenschaften }
    property UFOs[Index:Integer]    : TUFO read GetUFO;default;
    property Models[Index: Integer] : TUFOModel read GetUFOModel;
    property ModelCount             : Integer read GetModelCount;
    property Count                  : Integer read GetCount;
    property Tag                    : Integer read fDay;
    property Gesichtet              : Integer read GetGesichtet;
  end;

  TUFO = class(TObject)
  private
    fList            : TUFOList;
    fName            : String;
    fModel           : TUFOModel;
    fDate            : TKD4Date;
    fID              : Cardinal;
    fRounds          : Integer;
    fHitPoints       : double;
    fShieldPoints    : Integer;
    fShieldRecharge  : Integer;
    fSize            : Integer;
    fPoint           : TFloatPoint;
    fLoadStat        : Integer;
    fMinutes         : Integer;
    fLandMinuten     : Integer;
    fAliens          : Array of TAlien;
    fStop            : boolean;
    fVisible         : boolean;
    fLastPosition    : TFloatPoint;
    fNotifyList      : TNotifyList;
    fDestinationInfo : record
                         Destination    : TDestination;
                         Objekt         : TObject;
                         Point          : TFloatPoint;
                         AttackMessage  : Boolean;
                       end;
    fOnDestroyEvent  : TEventHandle;

    function GetShieldType: TUFOShield;
    function GetOverLand: boolean;
    function GetHitPoints: Integer;
    function GeneriereName: String;
    function GetBesatzung: Integer;
    function GetSchaden: Integer;
    procedure SetVisible(const Value: boolean);

    // Setzen der Ziel
    procedure ClearDestination;
    procedure FlyToPoint(Point: TFloatPoint);
    procedure GenerateRandomDestination;

    procedure DestinationDestroyed(Sender: TObject);
    function GetEntfernung: Integer;
  protected
    procedure InitAliens;
    procedure CheckPosition;

    function GetUFODestination: TFloatPoint;
  public
    procedure Abschuss;
    procedure Escape;

    constructor Create(Liste: TUFOList);
    destructor Destroy; override;
    procedure LoadFromStream(Stream: TStream);
    procedure SaveToStream(Stream: TStream);
    procedure SetModel(Model: TUFOModel);
    procedure RechargeShield(Time: Integer; Full: boolean=false);
    procedure Init;
    procedure MoveUFO(Minuten: Integer);
    procedure Draw(Surface: TDirectDrawSurface;X,Y: Integer);
    procedure Stop;
    procedure SetOverLand(Land: Integer);

    procedure HuntObject(Objekt: TObject);

    function Attack(Raumschiff: TObject; Minuten: Integer): boolean;
    function DoTreffer(Strength: Integer; WaffType: TWaffenType;var Absorb: Integer): boolean;
    function DirektUeberBasis(Basis: TBasis): boolean;
    function NextRound: boolean;
    function GetLandName: String;

    function Model: TUFOModel;

    function GetAlien(Index: Integer): TAlien;
    
    procedure SetPosition(X,Y: double);

    { Eigenschaften }
    property Date                   : TKD4Date read fDate write fDate;
    property Position               : TFloatPoint read fPoint;
    property ID                     : Cardinal read fID;
    property Size                   : Integer read fSize;
    property ShieldPoints           : Integer read fShieldPoints;
    property ShieldTyp              : TUFOShield read GetShieldType;
    property HitPoints              : Integer read GetHitPoints;
    property Entfernung             : Integer read GetEntfernung;
    property OverLand               : boolean read GetOverLand;
    property Minuten                : Integer read fMinutes;
    property LandMinuten            : Integer read fLandMinuten;
    property Schaden                : Integer read GetSchaden;

    property PositionBeforeMove     : TFloatPoint read fLastPosition;

    property NotifyList             : TNotifyList read fNotifyList;

  published
    function WriteToScriptString: String;
    class function ReadFromScriptString(ObjStr: String): TObject;

    property Name                   : String read fName write fName;
    property Visible                : boolean read fVisible write SetVisible;
    property Besatzung              : Integer read GetBesatzung;
  end;

var
  ImageProducer  : TSequenzer;
  gEarthMask     : TBitmap;


function PointOverLand(Point: TFloatPoint): boolean;overload;
function PointOverLand(X,Y: Integer): boolean;overload;

implementation

uses
  RaumschiffList, alien_api, savegame_api, basis_api, raumschiff_api,
  country_api, town_api, einsatz_api, game_api, ufo_api, AlienList, earth_api;

var
  g_savedEarthMask : TBitmap;
  TUFOListRecord   : TExtRecordDefinition;
  TUFORecord       : TExtRecordDefinition;

function PointOverLand(X,Y: Integer): boolean;overload;
begin
  result:=gEarthMask.Canvas.Pixels[X,Y]=clWhite;
end;

function PointOverLand(Point: TFloatPoint): boolean;
begin
  result:=PointOverLand(round(Point.X),round(Point.Y));
end;

procedure CreateTUFORecord;
var
  Aliens: TExtRecordDefinition;
begin
  if TUFORecord=nil then
  begin
    TUFORecord:=TExtRecordDefinition.Create('TUFORecord');
    TUFORecord.AddString('Name',30);
    TUFORecord.AddCardinal('ID',0,high(Cardinal));
    TUFORecord.AddInteger('Rounds',0,high(Integer));
    TUFORecord.AddInteger('Size',0,high(Integer));
    TUFORecord.AddInteger('HitPoints',0,high(Integer));
    TUFORecord.AddInteger('ShieldPoints',0,high(Integer));
    TUFORecord.AddInteger('Entfernung',0,high(Integer));

    TUFORecord.AddRecordList('Model',RecordUFOModel);

    TUFORecord.Adddouble('Position.X',-360,360,10);
    TUFORecord.Adddouble('Position.Y',-360,360,10);

    TUFORecord.AddInteger('Date.Year',0,high(Integer));
    TUFORecord.AddInteger('Date.Month',0,high(Integer));
    TUFORecord.AddInteger('Date.Week',0,high(Integer));
    TUFORecord.AddInteger('Date.Day',0,high(Integer));
    TUFORecord.AddInteger('Date.Hour',0,high(Integer));
    TUFORecord.AddInteger('Date.Minute',0,high(Integer));

    TUFORecord.AddInteger('Minuten',0,high(Integer));
    TUFORecord.AddInteger('LandMinuten',0,high(Integer));
    TUFORecord.AddBoolean('Visible');
    // Zieleinstellungen speichern
    TUFORecord.AddInteger('DestinationInfo',0,Integer(high(TDestination)));

    TUFORecord.Adddouble('Destination.X',-360,360,10);
    TUFORecord.Adddouble('Destination.Y',-360,360,10);
    TUFORecord.AddCardinal('DestinationID',0,high(Cardinal));

    TUFORecord.AddBoolean('AttackMessage');

    Aliens:=TExtRecordDefinition.Create('UFOAliens');
    Aliens.ParentDescription:=true;
    Aliens.AddCardinal('ID',0,high(Cardinal));
    Aliens.AddInteger('Ges',0,high(Integer));

    TUFORecord.AddRecordList('Aliens',Aliens);
  end;

  if TUFOListRecord=nil then
  begin
    TUFOListRecord:=TExtRecordDefinition.Create('TUFOListRecord');
    TUFOListRecord.AddInteger('Day',low(Integer),high(Integer));
    TUFOListRecord.AddRecordList('Models',RecordUFOModel);
    TUFOListRecord.AddInteger('UFOCount',0,high(Integer));
  end;

end;

{ TUFOList }

procedure TUFOList.AddUFO(UFO: TUFO);
begin
  fSchiffe.Add(UFO);
  fHashMap.InsertID(UFO.ID,Integer(UFO));
end;

procedure TUFOList.BuildAvailModels;
var
  Dummy: Integer;
  Index: Integer;
  Value: Integer;
begin
  SetLength(fAvailModels,length(fUFOModels));

  Index:=0;
  Value:=0;
  for Dummy:=0 to high(fUFOModels) do
  begin
    if ((fUFOModels[Dummy].Ver<=fDay) or (fUFOModels[Dummy].Ver=0)) and (fUFOModels[Dummy].Probability>0) then
    begin
      inc(Value,fUFOModels[Dummy].Probability);
      fAvailModels[Index].ModelIndex:=Dummy;
      fAvailModels[Index].lastValue:=Value;
      inc(Index);
    end;
  end;

  SetLength(fAvailModels,Index);
end;

procedure TUFOList.Clear;
begin
  // Try..except entfernt (Performance)
  while (fSchiffe.Count>0) do
  begin
    TUFO(fSchiffe[0]).Free;
  end;
  SetLength(fUFOModels,0);
  fHashMap.ClearList;
end;

constructor TUFOList.Create;
begin
  ufo_api_init(Self);

  fSchiffe:=TList.Create;

  CreateTUFORecord;

  SetLength(fUFOModels,0);
  fUFOIcons := game_api_CreateImageListItem;
  fUFOIcons.Transparent:=true;
  fUFOIcons.TransparentColor:=clGreen;
  fUFOIcons.SystemMemory:=true;

  fHashMap:=TIDHashMap.Create;

  savegame_api_RegisterCustomSaveHandler(SaveToStream,LoadFromStream,nil,$669A7507);
  savegame_api_RegisterLoadGameSetHandler(LoadGameSetHandler);
  savegame_api_RegisterNewGameHandler(NewGameHandler);

  savegame_api_RegisterHourEndHandler(NextHour);
end;

function TUFOList.CreateUFO: TUFO;
var
  Value : Integer;
  UFO   : Integer;
  Model : Integer;
begin
  if length(fAvailModels)=0 then
  begin
    result:=nil;
    exit;
  end;
  {$IFDEF MAXONEUFO}
  if Count>0 then
  begin
    result:=TUFO(fSchiffe[0]);
    exit;
  end;
  {$ENDIF}

  {$IFNDEF COMPLETERESEARCH}
  Value:=random(fAvailModels[high(fAvailModels)].lastValue);
  UFO:=0;
  while Value>fAvailModels[UFO].lastValue do
    inc(UFO);

  Model:=fAvailModels[UFO].ModelIndex;
  {$ELSE}
  Model:=random(length(fUFOModels));
  {$ENDIF}

  result:=CreateUFOFromModel(fUFOModels[Model]);
end;

function TUFOList.CreateUFOFromModel(Model: TUFOModel): TUFO;
begin
  result:=TUFO.Create(Self);
  result.Init;
  result.Date:=savegame_api_GetDate;
  result.SetModel(Model);
end;

procedure TUFOList.DeleteUFO(UFO: TUFO);
var
  Ind: Integer;
begin
  fHashMap.DeleteKey(UFO.ID);
  Ind:=fSchiffe.IndexOf(UFO);
  if Ind<>-1 then
  begin
    fSchiffe.Delete(Ind);
  end;
end;

destructor TUFOList.destroy;
begin
  Clear;
  fSchiffe.Free;
  fHashMap.Free;
  inherited;
end;

procedure TUFOList.DrawIcon(Surface: TDirectDrawSurface; x, y,
  Symbol: Integer);
begin
  if Symbol<=fUFOIcons.PatternCount then
  begin
    Surface.Canvas.Release;
    Surface.Draw(x,y,fUFOIcons.PatternRects[Symbol],fUFOIcons.PatternSurfaces[Symbol],true);
  end;
end;

function TUFOList.GetCount: Integer;
begin
  result:=fSchiffe.Count;
end;

function TUFOList.GetGesichtet: Integer;
var
  Dummy: Integer;
begin
  result:=0;
  for Dummy:=0 to Count-1 do
  begin
    if Ufos[Dummy].Visible then
      inc(result);
  end;
end;

function TUFOList.GetIndexOfModel(ID: Cardinal): Integer;
var
  Dummy: Integer;
begin
  for Dummy:=0 to high(fUFOModels) do
  begin
    if fUFOModels[Dummy].ID=ID then
    begin
      result:=Dummy;
      exit;
    end;
  end;
  result:=-1;
end;

function TUFOList.GetModelCount: Integer;
begin
  result:=length(fUFOModels);
end;

function TUFOList.GetUFO(Index: Integer): TUFO;
begin
  result:=nil;
  if (Index<0) or (Index>=Count) then
    exit;
    
  result:=TUFO(fSchiffe[Index]);
end;

function TUFOList.GetUFOModel(Index: Integer): TUFOModel;
begin
  result:=fUFOModels[Index];
end;

function TUFOList.IDofUFO(ID: Cardinal): TUFO;
begin
  if not fHashMap.FindKey(ID,Integer(result)) then
    result:=nil;
end;

procedure TUFOList.LoadFromStream(Stream: TStream);
var
  UFOCount : Integer;
  Dummy    : Integer;
  Rec      : TExtRecord;
begin
  Clear;

  Rec:=TExtRecord.Create(TUFOListRecord);
  Rec.LoadFromStream(Stream);

  fDay:=Rec.GetInteger('Day');

  with Rec.GetRecordList('Models') do
  begin
    SetLength(fUFOModels,Count);
    for Dummy:=0 to high(fUFOModels) do
      fUFOModels[Dummy]:=ExtRecordToUFO(Item[Dummy]);
  end;

  {$IFNDEF MAXONEUFO}
  UFOCount:=Rec.GetInteger('UFOCount');
  while UFOCount>0 do
  begin
    with TUFO.Create(Self) do
      LoadFromStream(Stream);

    dec(UFOCount);
  end;
  {$ENDIF}

  Rec.Free;

  {$IFDEF TRACEUFOSTAT}
  for Dummy:=0 to fSchiffe.Count-1 do
  begin
    GlobalFile.Write('Name',UFOs[Dummy].Name);
    GlobalFile.Write('Position.X',UFOs[Dummy].Position.X);
    GlobalFile.Write('Position.Y',UFOs[Dummy].Position.Y);
    GlobalFile.Write('Ziel',TypeInfo(TDestination),Ord(UFOs[Dummy].fDestinationInfo.Destination));
    GlobalFile.WriteLine;
  end;
  {$ENDIF}

  ModelAbgleich;

  BuildAvailModels;
end;

procedure TUFOList.LoadGameSetHandler(Sender: TObject);
var
  Dummy  : Integer;
  UFOs   : TRecordArray;
begin
  Clear;
  UFOs:=savegame_api_GameSetGetUFOs;
  SetLength(fOriginalModels,length(UFOs));

  for Dummy:=0 to high(UFOs) do
  begin
    fOriginalModels[Dummy]:=ExtRecordToUFO(UFOs[Dummy]);
    fOriginalModels[Dummy].Image:=ImageProducer.GetNumber;
  end;
end;

procedure TUFOList.ModelAbgleich;
var
  Dummy1,Dummy2 : Integer;
  ID            : Cardinal;
  Found         : boolean;
begin
  for Dummy1:=0 to length(fOriginalModels)-1 do
  begin
    ID:=fOriginalModels[Dummy1].ID;
    Dummy2:=0;
    Found:=false;
    while (Dummy2<ModelCount) and (not Found) do
    begin
      if fUFOModels[Dummy2].ID=ID then
      begin
        fUFOModels[Dummy2].Info:=fOriginalModels[Dummy1].Info;
        Found:=true;
      end;
      inc(Dummy2);
    end;
    if not Found then
    begin
      SetLength(fUFOModels,ModelCount+1);
      fUFOModels[ModelCount-1]:=fOriginalModels[Dummy1];
      fUFOModels[ModelCount-1].FirstMes:=false;
    end;
  end;
end;

procedure TUFOList.NewGameHandler(Sender: TObject);
var
  Dummy : Integer;
begin
  fDay:=-savegame_api_GetNewGameData.UFO;

  Clear;
  SetLength(fUFOModels,0);
  ModelAbgleich;
  for Dummy:=0 to high(fUFOModels) do
    {$IFNDEF COMPLETERESEARCH}
    fUFOModels[Dummy].FirstMes:=false;
    {$ELSE}
    fUFOModels[Dummy].FirstMes:=true;
    {$ENDIF}

  BuildAvailModels;
end;

procedure TUFOList.NextDay;
begin
  inc(fDay);

  // Liste der angreifbaren UFOs erneuern
  BuildAvailModels;
end;

procedure TUFOList.NextHour(Sender: TObject);
var
  Dummy     : Integer;
  Cou       : Integer;
  Name      : String;
begin
  Dummy:=0;
  Cou:=Count;
  while (Dummy<Cou) do
  begin
    Name:=UFOS[Dummy].Name;
    if not UFOs[Dummy].NextRound then
    begin
      country_api_UFOLebt(UFOs[Dummy]);
      // Punktabz�ge, wenn das UFO zu lange auf der Erde ist
      if (UFOs[Dummy].Minuten>6*60) then
        savegame_api_BuchPunkte(-2,pbInfiltration);

      inc(Dummy);
    end
    else
    begin
      dec(Cou);
    end;
  end;
end;

procedure TUFOList.NextRound(Minuten:Integer);
var
  Dummy     : Integer;
begin
  // Neue UFOs erstellen
  for Dummy:=1 to Minuten do
  begin
    if random(600)=random(600) then
    begin
      CreateUFO;
    end;
  end;

  basis_api_KAMPFInitShootTable(Minuten);

  // UFOs berechnen
  for Dummy:=Count-1 downto 0 do
    UFOS[Dummy].MoveUFO(Minuten);
end;

function TUFOList.PosOverLand(X, Y: Integer): boolean;
begin
  result:=PointOverLand(X,Y);
end;

procedure TUFOList.SaveToStream(Stream: TStream);
var
  Dummy  : Integer;
  Rec    : TExtRecord;
begin
  Rec:=TExtRecord.Create(TUFOListRecord);

  Rec.SetInteger('Day',fDay);

  with Rec.GetRecordList('Models') do
  begin
    for Dummy:=0 to ModelCount-1 do
      Add(UFOToExtRecord(fUFOModels[Dummy]));
  end;
  Rec.SetInteger('UFOCount',fSchiffe.Count);
  Rec.SaveToStream(Stream);
  Rec.Free;

  for Dummy:=0 to Count-1 do
    UFOs[Dummy].SaveToStream(Stream);
end;

procedure TUFOList.SetBitmap(Bitmap: TBitmap);
begin
  fUFOIcons.Picture.Bitmap:=Bitmap;
  fUFOIcons.TransparentColor:=Bitmap.Canvas.Pixels[0,0];
  fUFOIcons.PatternHeight:=32;
  fUFOIcons.PatternWidth:=32;
  fUFOIcons.Restore;
end;

function TUFOList.ShowFirstMessage(UFO: TUFO): boolean;
var
  Dummy: Integer;
  ModelID: Cardinal;
begin
  result:=false;
  ModelID:=UFO.fModel.ID;
  for Dummy:=0 to ModelCount-1 do
  begin
    if Models[Dummy].ID=ModelID then
    begin
      if not Models[Dummy].FirstMes then
      begin
        fUFOModels[Dummy].FirstMes:=true;
        savegame_api_Message(Format(MFirstSichtung,[Models[Dummy].Name]),lmUFOs,UFO);
        result:=true;
      end;
      exit;
    end;
  end;
end;

procedure TUFOList.StartGameHandlerEarthMap(Sender: TObject);
var
  Dummy: Integer;
  Pos  : TFloatPoint;
begin
  if g_savedEarthMask=nil then
  begin
    // Karte f�r neue Spiele speichern
    g_savedEarthMask:=TBitmap.Create;
    g_savedEarthMask.Width:=gEarthMask.Width;
    g_savedEarthMask.Height:=gEarthMask.Height;
    g_savedEarthMask.Canvas.Draw(0,0,gEarthMask);
  end
  else
  begin
    // Karte wiederherstellen
    gEarthMask.Canvas.Draw(0,0,g_savedEarthMask);
  end;

  for Dummy:=0 to town_api_GetTownCount-1 do
  begin
    Pos:=town_api_GetTown(Dummy).Position;

    gEarthMask.Canvas.Pixels[round(Pos.X),round(Pos.Y)]:=clWhite;
  end;
end;

function TUFOList.UFOInList(UFO: TUFO): boolean;
begin
  result:=fHashMap.FindKey(UFO.ID,Integer(UFO));
end;

{ TUFO }

procedure TUFO.Abschuss;
var
  Dummy   : Integer;
  Einsatz : TEinsatz;
  Percent : Integer;
  Punkte  : Integer;
  {$IFDEF MASSIVALIENS}
  Dummy2  : Integer;
  {$ENDIF}
begin
  { Aufrufen der Funktion zum Berechnen der Unterst�zung und zum Addieren der Punkte }
  Assert(fList<>nil,'keine UFO-Liste angegeben (Abschuss - 1)');
  country_api_UFOAbschuss(Self);

  if OverLand then
  begin
    Percent:=round((-fHitPoints)/fModel.HitPoints*100);
    fHitPoints:=0;

    // Neuen Einsatz erstellen
    Einsatz:=einsatz_api_GenerateEinsatz(fPoint);
    Einsatz.RemainTime:=2880+random(7200);
    for Dummy:=0 to high(fAliens) do
    begin
      {$IFDEF MASSIVALIENS}
      for Dummy2:=0 to 5 do
      {$ELSE}
      if not RandomChance(percent) then
      {$ENDIF}
        Einsatz.AddAlien(Addr(fAliens[Dummy]));
    end;
    Einsatz.CheckEinsatz;

    Punkte:=fModel.Points;
  end
  else
  begin
    Punkte:=round(fModel.Points*1.5);
  end;
  Assert(fList<>nil,'keine UFO-Liste angegeben (Abschuss - 2)');

  savegame_api_BuchPunkte(Punkte,pbUFOAbgeschossen);

  fNotifyList.CallEvents(EVENT_ONUFOSHOOTDOWN,Self);
  Free;
end;

function TUFO.Attack(Raumschiff: TObject; Minuten: Integer): boolean;
var
  Abs    : Integer;
  Raum   : TRaumschiff;
  Schuss : Integer;
begin
  result:=false;
  if Raumschiff=nil then
    exit;

  Schuss:=Minuten+Minuten;
  Raum:=TRaumschiff(Raumschiff);
{ Wenn das Raumschiff �ber Waffen verf�gt wird hier abgebrochen, da }
{ K�mpfe sp�ter berechnet werden                                    }
  if sabHuntUFO in Raum.Abilities then
    exit;
    
  while Schuss>0 do
  begin
    if RandomChance(7) then
    begin
      if Raum.DoTreffer(fModel.Angriff,Abs,Self) then
      begin
        result:=true;
        exit;
      end;
    end;
    dec(Schuss);
  end;
end;

procedure TUFO.CheckPosition;
begin
  if fPoint.x<0 then fPoint.x:=fPoint.x+360;
  if fPoint.y<0 then fPoint.y:=fPoint.y+180;
  if fPoint.x>360 then fPoint.x:=fPoint.x-360;
  if fPoint.y>180 then fPoint.y:=fPoint.y-180;
end;

procedure TUFO.ClearDestination;
begin
  if (fDestinationInfo.Destination=dSchiff) and (fDestinationInfo.Objekt<>nil) then
    TRaumschiff(fDestinationInfo.Objekt).NotifyList.RemoveEvent(fOnDestroyEvent);

  if (fDestinationInfo.Destination=dBase) and (fDestinationInfo.Objekt<>nil) then
    TBasis(fDestinationInfo.Objekt).NotifyList.RemoveEvent(fOnDestroyEvent);

  fDestinationInfo.Destination:=dNone;
  fDestinationInfo.Objekt:=nil;
  fDestinationInfo.AttackMessage:=false;
end;

constructor TUFO.Create(Liste: TUFOList);
begin
  fList:=Liste;
  FillChar(fName,SizeOf(fName),#0);
  FillChar(fDate,SizeOf(fDate),#0);
  SetLength(fAliens,0);
  fStop:=false;
  fVisible:=false;

  GenerateRandomDestination;
  
  fNotifyList:=TNotifyList.Create;
end;

destructor TUFO.Destroy;
begin
  ClearDestination;
  fNotifyList.CallEvents(EVENT_ONUFODESTROY,Self);
  fNotifyList.Free;
  SetLength(fAliens,0);
  fList.DeleteUFO(Self);
  inherited;
end;

function TUFO.DirektUeberBasis(Basis: TBasis): boolean;
var
  Pt    : TFloatPoint;
begin
  result:=false;

  if Basis=nil then
    exit;

  Pt:=Basis.BasisPos;
  if (Pt.X=fPoint.x) and (Pt.Y=fPoint.Y) then
    result:=true;
end;

function TUFO.DoTreffer(Strength: Integer; WaffType: TWaffenType;var Absorb: Integer): boolean;
var
  ShieldSchutz: Integer;
begin
  result:=false;
  ShieldSchutz:=0;
  case WaffType of
    wtProjektil : ShieldSchutz:=round(Strength/100*Model.ShieldType.Projektil);
    wtRaketen   : ShieldSchutz:=round(Strength/100*Model.ShieldType.Rakete);
    wtLaser     : ShieldSchutz:=round(Strength/100*Model.ShieldType.Laser);
  end;
  ShieldSchutz:=min(ShieldSchutz,fShieldPoints);
  fShieldPoints:=fShieldPoints-ShieldSchutz;
  fHitPoints:=fHitPoints-(Strength-ShieldSchutz);
  Absorb:=round(ShieldSchutz/Strength*100);
  if fHitPoints<=0 then
  begin
    Abschuss;
    result:=true;
  end;
end;

procedure TUFO.Draw(Surface: TDirectDrawSurface; X, Y: Integer);
begin
  fList.DrawIcon(Surface,X,Y,Size);
end;

procedure TUFO.FlyToPoint(Point: TFloatPoint);
begin
  ClearDestination;
  fDestinationInfo.Destination:=dPoint;
  fDestinationInfo.Point:=Point;
end;

procedure TUFO.GenerateRandomDestination;
var
  Base     : Integer;
  Land     : Integer;
  NewLand  : Integer;
  Vert     : Integer;
  Seq      : TSequenzer;
  Anzahl   : Integer;
  Town     : TTown;
  NewTown  : TTown;
begin
  ClearDestination;

  // Ermitteln eines Zieles des UFOs
  if (basis_api_GetBasisCount>0) and RandomChance(2) then
  begin
    Base:=random(basis_api_GetBasisCount);
    HuntObject(basis_api_GetBasisFromIndex(Base));
  end
  else if RandomChance(5) and (town_api_GetTownCount>0) then
  begin
    // Zuf�lliges Land ermitteln
    Seq:=TSequenzer.Create;
    Seq.SetHighest(country_api_GetCountryCount-1);
    Vert:=101;
    Anzahl:=20;
    Land:=-1;
    while Anzahl>0 do
    begin
      NewLand:=Seq.GetNumber;
      if country_api_GetCountry(NewLand).Towns>0 then
      begin
        if country_api_GetCountry(NewLand).Friendly<Vert then
        begin
          Vert:=country_api_GetCountry(NewLand).Friendly;
          Land:=NewLand;
        end;
      end;
      dec(Anzahl);
    end;
    if Land>0 then
    begin
      // Stadt ermitteln
      Seq.SetHighest(town_api_GetTownCount-1);
      Town:=nil;
      Anzahl:=8;
      while Anzahl>0 do
      begin
        NewTown:=town_api_GetTown(Seq.GetNumber,true);
        if NewTown.Country=Land then
        begin
          if (Town=nil) or (Town.Infiltration<NewTown.Infiltration) then
            Town:=NewTown;
          dec(Anzahl);
        end;
      end;
      HuntObject(Town);
    end;
    Seq.Free;
  end
  else
    FlyToPoint(FloatPoint(random*360,random*180));

end;

function TUFO.GeneriereName: String;
var
  Vorschlag: String;
  Dummy    : Integer;
  VorOK    : boolean;
begin
  VorOK:=false;
  while not VorOK do
  begin
    Vorschlag:=fModel.Name+' '+Copy(IntToHex(fID,4),1,4);
    VorOK:=true;
    for Dummy:=0 to fList.Count-1 do
    begin
      if (fList[Dummy]<>Self) and (fList[Dummy].Name=Vorschlag) then
      begin
        VorOK:=false;
        break;
      end;
    end;
  end;
  result:=Vorschlag;
end;

function TUFO.GetBesatzung: Integer;
begin
  result:=Length(fAliens);
end;

function TUFO.GetHitPoints: Integer;
begin
  result:=round(fHitPoints);
end;

function TUFO.GetLandName: String;
begin
  Assert(fList<>nil,'UFOList nicht gesetzt');
  result:=country_api_LandOverPos(Position);
end;

function TUFO.GetOverLand: boolean;
begin
  result:=fList.PosOverLand(round(fPoint.X),round(fPoint.Y));
end;

function TUFO.GetSchaden: Integer;
begin
  result:=100-round(fHitPoints/fModel.HitPoints*100);
end;

function TUFO.GetShieldType: TUFOShield;
begin
  result:=fModel.ShieldType;
end;

procedure TUFO.Init;
var
  Dummy     : Integer;
  ID        : Cardinal;
  IDOK      : boolean;
begin
  IDOK:=false;
  ID:=0;
  while not IDOK do
  begin
    ID:=random(High(Cardinal));
    if ID<>0 then
    begin
      IDOK:=true;
      for Dummy:=0 to fList.Count-1 do if fList[Dummy].fID=ID then IDOK:=false;
    end;
  end;
  fID:=ID;
  fRounds:=random(200)+50;

  GenerateRandomDestination;
  // UFO zuf�llig auf der Erde positionieren
  fPoint:=earth_api_RandomEarthPoint;

  fList.AddUFO(Self);
end;

procedure TUFO.InitAliens;
var
  Aliens: Integer;
  Dummy : Integer;
  Value : Integer;
  Dum2  : Integer;
  max   : Integer;
begin
  SetLength(fAliens,0);
  Aliens:=fModel.MinBesatz+random(fModel.ZusBesatz+1);

  if Aliens=0 then
    exit;

  GlobalFile.WriteLine;
  SetLength(fAliens,Aliens);

  // Maximal Wert ermitteln
  max:=0;
  for Dummy:=0 to high(fModel.Aliens) do
  begin
    if alien_api_CanAttack(fModel.Aliens[Dummy].AlienID) then
      inc(max,fModel.Aliens[Dummy].Chance);
  end;

  // Wenn max 0 ist, dann kann keins der Aliens, dass dem UFO zugeordnet ist,
  // angreifen. In diesem Fall wird das UFO mit allen verf�gbaren Aliens best�ckt
  if max=0 then
  begin
    for Dummy:=0 to Aliens-1 do
      fAliens[Dummy]:=alien_api_GetRandomAlien^;
    exit;
  end;

  // Zuf�llig Aliens ermitteln aus dem Pott der Aliens, die diesem UFO zugeordnet sind
  for Dummy:=0 to Aliens-1 do
  begin
    Value:=random(max);
    Dum2:=0;
    while (Dum2<length(fModel.Aliens)) do
    begin
      if ((Value<=fModel.Aliens[Dum2].Chance) and (alien_api_CanAttack(fModel.Aliens[Dum2].AlienID))) then
        break;

      if alien_api_CanAttack(fModel.Aliens[Dum2].AlienID) then
        dec(Value,fModel.Aliens[Dum2].Chance);

      inc(Dum2);
    end;

{    while (Dum2<length(fModel.Aliens)) and (Value>fModel.Aliens[Dum2].Chance) or (not alien_api_CanAttack(fModel.Aliens[Dum2].AlienID)) do
    begin
      if alien_api_CanAttack(fModel.Aliens[Dum2].AlienID) then
        dec(Value,fModel.Aliens[Dum2].Chance);
      inc(Dum2);
    end;}

    if (Dum2<length(fModel.Aliens)) then
    begin
      fAliens[Dummy]:=alien_api_GetAlienInfo(fModel.Aliens[Dum2].AlienID)^;
      GlobalFile.Write(fAliens[Dummy].Name);
    end;
  end;
  GlobalFile.WriteLine;
end;

procedure TUFO.LoadFromStream(Stream: TStream);
var
  LoadRec: TExtRecord;
  Dummy  : Integer;
begin
  LoadRec:=TExtRecord.Create(TUFORecord);
  LoadRec.LoadFromStream(Stream);

  fName:=LoadRec.GetString('Name');
  fID:=LoadRec.GetCardinal('ID');
  fRounds:=LoadRec.GetInteger('Rounds');
  fSize:=LoadRec.GetInteger('Size');

  fHitPoints:=LoadRec.GetInteger('HitPoints');
  fShieldPoints:=LoadRec.GetInteger('ShieldPoints');

  fModel:=ExtRecordToUFO(LoadRec.GetRecordList('Model').Item[0]);

  fPoint.x:=LoadRec.Getdouble('Position.X');
  fPoint.y:=LoadRec.Getdouble('Position.Y');

  fDate.Year:=LoadRec.GetInteger('Date.Year');
  fDate.Month:=LoadRec.GetInteger('Date.Month');
  fDate.Week:=LoadRec.GetInteger('Date.Week');
  fDate.Day:=LoadRec.GetInteger('Date.Day');
  fDate.Hour:=LoadRec.GetInteger('Date.Hour');
  fDate.Minute:=LoadRec.GetInteger('Date.Minute');

  fMinutes:=LoadRec.GetInteger('Minuten');
  fLandMinuten:=LoadRec.GetInteger('LandMinuten');
  fVisible:=LoadRec.GetBoolean('Visible');

  // Zieleinstellungen speichern
  fDestinationInfo.Destination:=TDestination(LoadRec.GetInteger('DestinationInfo'));

  case fDestinationInfo.Destination of
    dPoint :
    begin
      fDestinationInfo.Point.x:=LoadRec.Getdouble('Destination.X');
      fDestinationInfo.Point.y:=LoadRec.Getdouble('Destination.Y');
    end;
    dBase  : fDestinationInfo.Objekt:=basis_api_GetBasisFromID(LoadRec.GetCardinal('DestinationID'));
    dTown  : fDestinationInfo.Objekt:=town_api_GetTown(LoadRec.GetCardinal('DestinationID'));
    dSchiff: fDestinationInfo.Objekt:=raumschiff_api_GetRaumschiff(LoadRec.GetCardinal('DestinationID'));
  end;

  // Korrektur, wenn beim speichern/laden etwas nicht geklappt hat
  if (fDestinationInfo.Objekt=nil) and (fDestinationInfo.Destination<>dPoint) then
    fDestinationInfo.Destination:=dNone
  else
    HuntObject(fDestinationInfo.Objekt);

  fDestinationInfo.AttackMessage:=LoadRec.GetBoolean('AttackMessage');

  // Aliens speichern
  with LoadRec.GetRecordList('Aliens') do
  begin
    SetLength(fAliens,Count);
    for Dummy:=0 to High(fAliens) do
    begin
      fAliens[Dummy].ID:=Item[Dummy].GetCardinal('ID');
      if fAliens[Dummy].ID<>0 then
      begin
        fAliens[Dummy]:=alien_api_GetAlienInfo(fAliens[Dummy].ID)^;
        fAliens[Dummy].Ges:=Item[Dummy].GetInteger('Ges');
      end
      else
        fAliens[Dummy]:=alien_api_GetRandomAlien^;
    end;
  end;

  LoadRec.Free;

  fList.AddUFO(Self);
  fShieldRecharge:=ShieldTyp.Laden;
end;

procedure TUFO.MoveUFO(Minuten: Integer);
var
  Pt      : TFloatPoint;
  Land    : Integer;
  OldLand : Integer;
  Kilom   : Integer;
  LEntf   : Integer;
  Dummy   : Integer;
  Raum    : TRaumschiff;
begin
  fLastPosition:=fPoint;
  if not fVisible then
  begin
    if raumschiff_api_InScanArea(Position) then
      Visible:=true;
  end;
  if fStop then
  begin
    fStop:=false;
    exit;
  end;
  OldLand:=country_api_LandNrOverPos(fPoint);
  Kilom:=fModel.Pps*15;

  // UFO hat kein Ziel? Neues Ziel zuweisen
  if (fDestinationInfo.Destination=dNone) or ((GetUFODestination.X=0) and (GetUFODestination.Y=0)) then
    GenerateRandomDestination;

  // UFO soll ein Raumschiff verfolgen
  if fDestinationInfo.Destination=dSchiff then
    Raum:=TRaumschiff(fDestinationInfo.Objekt)
  else if fDestinationInfo.Destination<>dBase then
  begin
    // Hier wird das n�chste Raumschiff zum UFO ermittelt, das maximal 5000 km entfernt ist
    Raum:=raumschiff_api_getNearestSchiff(fPoint);
    if (Raum<>nil) and (EarthEntfernung(Raum.Position,fPoint)<=5000) then
      HuntObject(Raum)
    else
      Raum:=nil;
  end
  else
    Raum:=nil;

  // Nachricht "UFO verfolgt Raumschiff" pr�fen/erstellen
  if Raum<>nil then
  begin
    if not Raum.IsInHomeBase then
    begin
      Pt:=Raum.Position;
      // �berpr�fen, ob das Raumschiff das UFO jagt, wenn ja, verhindern das eine
      // Nachricht angezeigt wird
      if Raum.HuntedUFO=Self then
        fDestinationInfo.AttackMessage:=true;

      // Wenn es sichtbar ist, und die Angriffsnachricht noch nicht gesendet wurde
      // Dann Nachricht schicken und Variablen aktualisieren
      if (not fDestinationInfo.AttackMessage) and fVisible then
      begin
        savegame_api_Message(Format(LVerfolgungskurs,[Name,Raum.Name]),lmUFOs,Self);
        fDestinationInfo.AttackMessage:=true;
      end;
    end
    else
      GenerateRandomDestination;
  end;

  // UFO bewegen und zum Ziel fliegen lassen
  Pt:=GetUFODestination;
  for Dummy:=0 to Minuten-1 do
  begin
    if basis_api_KAMPFShootToUFO(Self,Dummy) then
      exit;

    if EarthCalcFlight(fPoint,Pt,Kilom,LEntf) then
    begin
      // UFO hat das Ziel erreicht, jetzt werden die Aktionen ausgef�hrt
      if (Raum<>nil) and Attack(Raum,1) then
        Raum:=nil
      else if fDestinationInfo.Destination=dPoint then
      begin
        // Neues Ziel suchen
        GenerateRandomDestination;
        Pt:=GetUFODestination;
      end
      else if fDestinationInfo.Destination=dTown then
        // Stadt infiltrieren
        TTown(fDestinationInfo.Objekt).Infiltrate(Besatzung,Self);
    end;

    // UFO greift die Basis an
    if fDestinationInfo.Destination=dBase then
    begin
      LEntf:=EarthEntfernung(TBasis(fDestinationInfo.Objekt).BasisPos,fPoint);
      if LEntf<=fModel.BaseAttackRange then
        TBasis(fDestinationInfo.Objekt).DoTreffer(fModel.BaseAttackStrength);
    end;
  end;
  
{ ******************************** Abschlussarbeit **************************** }
{ Zum Beispiel �berpr�fung der Position, Berechnung zu einer Basis, �ber        }
{ Land das UFO ist, oder die Repeartur                                          }
{ ***************************************************************************** }
  CheckPosition;

  Land:=country_api_LandNrOverPos(fPoint);
  if OldLand<>Land then
  begin
    fLandMinuten:=0;
  end;
  if Land<>-1 then
    inc(fLandMinuten,Minuten);

  inc(fMinutes,Minuten);

  // Reparatur
  if fHitPoints<Model.HitPoints then
    fHitPoints:=min(Model.HitPoints,fHitPoints+((Model.HitPoints*0.001)*Minuten));

  if (not fVisible) and raumschiff_api_InScanArea(Position) then
    Visible:=true;
end;

function TUFO.NextRound: boolean;
begin
  dec(fRounds);
  result:=fRounds=0;
  if fRounds=0 then
  begin
    Escape;
    exit;
  end;
end;

procedure TUFO.DestinationDestroyed(Sender: TObject);
begin
  // Das UFO, das das Raumschiff jagt wurde zerst�rt. Somit kann ein neues Ziel
  // gew�hlt werden.
  if (fDestinationInfo.Destination in [dSchiff,dBase]) and (Sender=fDestinationInfo.Objekt) then
    ClearDestination;
end;

procedure TUFO.RechargeShield(Time: Integer; Full: boolean);
var
  Add: Integer;
begin
  if Full then
  begin
    fShieldPoints:=fModel.Shield;
    fShieldRecharge:=ShieldTyp.Laden;
  end
  else
  begin
    if (ShieldPoints<Model.Shield) then
    begin
      dec(fShieldRecharge,Time);
      while (fShieldRecharge<0) do
      begin
        inc(fLoadStat);
        if fLoadStat>(Model.Shield div 20) then
        begin
          Add:=min(fLoadStat,Model.Shield-fShieldPoints);
          inc(fShieldPoints,Add);
          dec(fLoadStat,Add);
        end;
        inc(fShieldRecharge,ShieldTyp.Laden);
      end;
    end;
  end;
end;

procedure TUFO.SaveToStream(Stream: TStream);
var
  Dummy  : Integer;
  Rec    : TExtRecord;
  ModelR : TExtRecord;
begin
  Rec:=TExtRecord.Create(TUFORecord);

  Rec.SetString('Name',fName);
  Rec.SetCardinal('ID',fID);
  Rec.SetInteger('Rounds',fRounds);
  Rec.SetInteger('Size',fSize);
  Rec.SetInteger('HitPoints',HitPoints);
  Rec.SetInteger('ShieldPoints',fShieldPoints);

  ModelR:=UFOToExtRecord(fModel);
  Rec.GetRecordList('Model').Add(ModelR);

  Rec.Setdouble('Position.X',fPoint.x);
  Rec.Setdouble('Position.Y',fPoint.y);

  Rec.SetInteger('Date.Year',fDate.Year);
  Rec.SetInteger('Date.Month',fDate.Month);
  Rec.SetInteger('Date.Week',fDate.Week);
  Rec.SetInteger('Date.Day',fDate.Day);
  Rec.SetInteger('Date.Hour',fDate.Hour);
  Rec.SetInteger('Date.Minute',fDate.Minute);

  Rec.SetInteger('Minuten',fMinutes);
  Rec.SetInteger('LandMinuten',fLandMinuten);
  Rec.SetBoolean('Visible',fVisible);

  // Zieleinstellungen speichern
  if (fDestinationInfo.Destination in [dBase,dTown,dSchiff]) and (fDestinationInfo.Objekt=nil) then
    fDestinationInfo.Destination:=dNone;
  Rec.SetInteger('DestinationInfo',Integer(fDestinationInfo.Destination));

  case fDestinationInfo.Destination of
    dPoint :
    begin
      Rec.Setdouble('Destination.X',fDestinationInfo.Point.x);
      Rec.Setdouble('Destination.Y',fDestinationInfo.Point.y);
    end;
    dBase  : Rec.SetCardinal('DestinationID',TBasis(fDestinationInfo.Objekt).ID);
    dTown  : Rec.SetCardinal('DestinationID',TTown(fDestinationInfo.Objekt).ID);
    dSchiff: Rec.SetCardinal('DestinationID',TRaumschiff(fDestinationInfo.Objekt).ID);
  end;

  Rec.SetBoolean('AttackMessage',fDestinationInfo.AttackMessage);

  // Aliens speichern
  for Dummy:=0 to High(fAliens) do
  begin
    with Rec.GetRecordList('Aliens').Add do
    begin
      SetCardinal('ID',fAliens[Dummy].ID);
      SetInteger('Ges',fAliens[Dummy].Ges);
    end;
  end;

  Rec.SaveToStream(Stream);
  Rec.Free;
end;

procedure TUFO.SetModel(Model: TUFOModel);
begin
  fModel:=Model;
  fName:=GeneriereName;
  fHitPoints:=Model.HitPoints;
  fShieldPoints:=Model.Shield;
  fSize:=Model.Image;
  fShieldRecharge:=ShieldTyp.Laden;
  fModel.Treffsicherheit:=MinMax(randomPercent(fModel.Treffsicherheit,15),0,100);
  fModel.ShieldType.Projektil:=MinMax(randomPercent(fModel.ShieldType.Projektil,15),0,100);
  fModel.ShieldType.Rakete:=MinMax(randomPercent(fModel.ShieldType.Rakete,15),0,100);
  fModel.ShieldType.Laser:=MinMax(randomPercent(fModel.ShieldType.Laser,15),0,100);
  InitAliens;
end;

procedure TUFO.SetOverLand(Land: Integer);
var
  X,Y     : Integer;
  Found   : boolean;
begin
  repeat
    X:=random(64);
    Y:=random(40);
    Found:=country_api_IsLandAtPos(FloatPoint(X,Y),Land);
  until Found;
  fPoint:=FloatPoint(X*SquareXSize+random*SquareYSize,Y*SquareYSize+random*SquareYSize);
  ClearDestination;
  CheckPosition;
end;

procedure TUFO.SetVisible(const Value: boolean);
var
  LandText : String;
  Text     : String;
begin
  if fVisible=Value then
    exit;

  fVisible := Value;

  if Visible then
  begin
    if fList.ShowFirstMessage(Self) then
      exit;

    LandText:='';

    LandText:=country_api_LandOverPos(Position);
    if LandText<>EmptyStr then
      LandText:=MOver+LandText;

    Text:=Format(MUFOInSicht,[Model.Name,LandText]);
    savegame_api_Message(Text+'.',lmUFOs,Self);

    fNotifyList.CallEvents(EVENT_ONUFODISCOVERED,Self);
  end;
end;

procedure TUFO.Stop;
begin
  fStop:=true;
end;

procedure TUFO.HuntObject(Objekt: TObject);
begin
  ClearDestination;
  if Objekt is TBasis then
  begin
    fDestinationInfo.Destination:=dBase;
    fDestinationInfo.Objekt:=Objekt;
    fOnDestroyEvent:=TBasis(Objekt).NotifyList.RegisterEvent(EVENT_BASISFREE,DestinationDestroyed);
  end
  else if Objekt is TRaumschiff then
  begin
    fDestinationInfo.Destination:=dSchiff;
    fDestinationInfo.Objekt:=Objekt;
    fOnDestroyEvent:=TRaumschiff(Objekt).NotifyList.RegisterEvent(EVENT_SCHIFFONDESTROY,DestinationDestroyed);
  end
  else if Objekt is TTown then
  begin
    fDestinationInfo.Destination:=dTown;
    fDestinationInfo.Objekt:=Objekt;
  end
  else if Objekt=nil then
    exit
  else
    Assert(false,'Invalid Object');
end;

function TUFO.GetUFODestination: TFloatPoint;
begin
  if fDestinationInfo.Destination=dPoint then
    result:=fDestinationInfo.Point
  else if (fDestinationInfo.Destination=dTown) and (fDestinationInfo.Objekt<>nil) then
    result:=TTown(fDestinationInfo.Objekt).Position
  else if (fDestinationInfo.Destination=dBase) and (fDestinationInfo.Objekt<>nil) then
    result:=TBasis(fDestinationInfo.Objekt).BasisPos
  else if (fDestinationInfo.Destination=dSchiff) and (fDestinationInfo.Objekt<>nil) then
    result:=TRaumschiff(fDestinationInfo.Objekt).Position
  else
    result:=FloatPoint(0,0);
end;

function TUFO.Model: TUFOModel;
begin
  result:=fModel;
end;

procedure TUFO.SetPosition(X,Y: double);
begin
  fPoint.X:=X;
  fPoint.Y:=Y;
end;

procedure TUFO.Escape;
begin
  savegame_api_Message(Format(MUFOWeg,[Name]),lmUFOs);
  savegame_api_BuchPunkte(-50,pbUFOWeg);
  
  fNotifyList.CallEvents(EVENT_ONUFOESCAPE,Self);
  Free;
end;

class function TUFO.ReadFromScriptString(ObjStr: String): TObject;
begin
  result:=ufo_api_GetUFO(StrToInt(ObjStr));
end;

function TUFO.WriteToScriptString: String;
begin
  result:=intToStr(ID);
end;

function TUFO.GetEntfernung: Integer;
begin
  result:=round(EarthEntfernung(basis_api_GetSelectedBasis.BasisPos,fPoint));
end;

function TUFO.GetAlien(Index: Integer): TAlien;
begin
  Assert((Index>=0) and (Index<=high(fAliens)),'Ung�ltiger Index bei GetAlien');
  result:=fAliens[Index];
end;

initialization

  ImageProducer:=TSequenzer.Create;
  ImageProducer.SetHighest(UFOImages-1);

  if gEarthMask=nil then
    gEarthMask:=TBitmap.Create;

finalization
  ImageProducer.Free;

  FreeAndNil(TUFORecord);
  FreeAndNil(TUFOListRecord);
  if gEarthMask<>nil then
    gEarthMask.Free;

  if g_savedEarthMask<>nil then
    g_savedEarthMask.Free;
end.
