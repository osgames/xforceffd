{********************************************************************************
*										*
* Copyright (C) 2004  X-Force Team - jim_raynor@web.de				*
*										*
* This file is part of X-Force: Fight For Destiny (X-Force).			*
*										*
* X-Force is free software; you can redistribute it and/or modify		*
* it under the terms of the GNU General Public License as published by		*
* the Free Software Foundation; either version 2 of the License, or		*
* (at your option) any later version.						*
*										*
* X-Force is distributed in the hope that it will be useful,			*
* but WITHOUT ANY WARRANTY; without even the implied warranty of		*
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the			*
* GNU General Public License for more details.					*
*										*
* You should have received a copy of the GNU General Public License		*
* along with X-Force; if not, write to the Free Software			*
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA	*
*										*
*********************************************************************************
*										*
* Verwaltet die Aliens. Ein Zugriff auf Daten von Aliens sollte nur �ber die	*
* alien_api erfolgen.								*
*										*
********************************************************************************}

{$I ../Settings.inc}

unit AlienList;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  XForce_types, DXDraws, Sequenzer, Defines, IDHashMap, TraceFile,
  KD4Utils, StringConst;

type
  TAlienList = class(TObject)
  private
    fSetAliens : Array of TAlien;
    fAliens    : Array of TAlien;
    fDay       : Integer;
    fHashMap   : TIDHashMap;

    fAlienIcons: TPictureCollectionItem;

    procedure Abgleich;
    function GetCount: Integer;
    function GetAlien(Index: Integer): TAlien;
    procedure AddAlienToHashList;
    { Private-Deklarationen }
  protected
    procedure LoadGameSetHandler(Sender: TObject);
    procedure NewGameHandler(Sender: TObject);

    { Protected-Deklarationen }
  public
    constructor Create;
    destructor destroy;override;
    procedure SaveToStream(Stream: TStream);
    procedure LoadFromStream(Stream: TStream);
    procedure NextDay;
    procedure SetBitmap(Bitmap: TBitmap);
    procedure DrawImage(Index: Integer;Surface: TDirectDrawSurface;x,y: Integer);

    function CanAttack(Index: Integer): boolean;
    function GenerateAlien: PAlien;
    function IndexOfID(ID: Cardinal): Integer;
    function AddrOfID(ID: Cardinal): PAlien;

    procedure AutopsieEnd(ID: Cardinal);

    property Count: Integer read GetCount;
    property Aliens[Index: Integer]: TAlien read GetAlien;default;
    { Public-Deklarationen }
  end;

var
  AlienImageProducer: TSequenzer;

implementation

uses
  Loader, savegame_api, game_api, alien_api, ExtRecord, ObjektRecords,
  ProjektRecords, ConvertRecords;

var
  TAlienListRecord : TExtRecordDefinition;
{ TAlienList }

procedure TAlienList.Abgleich;
var
  Dummy1,Dummy2 : Integer;
  ID            : Cardinal;
  Found         : boolean;
  Count         : Integer;
begin
  for Dummy1:=0 to high(fSetAliens) do
  begin
    ID:=fSetAliens[Dummy1].ID;
    Dummy2:=0;
    Found:=false;
    Count:=length(fAliens);
    while (Dummy2<Count) and (not Found) do
    begin
      if fAliens[Dummy2].ID=ID then
      begin
        // Hier neue Eigenschaften von fSetAliens nach fAliens �bernehmen
        
        Found:=true;
      end;
      inc(Dummy2);
    end;

    // Alien hinzuf�gen, wenn dieser noch nicht im Spielstand gespeichert ist
    if not Found then
    begin
      SetLength(fAliens,length(fAliens)+1);
      fAliens[high(fAliens)]:=fSetAliens[Dummy1];
    end;
  end;
  AddAlienToHashList;
end;

function TAlienList.CanAttack(Index: Integer): boolean;
begin
  if Index=-1 then
  begin
    result:=false;
    exit;
  end;
  {$IFDEF COMPLETERESEARCH}
  result:=true;
  {$ELSE}
  result:=(fAliens[Index].Ver<=fDay) or (fAliens[Index].Ver=0);
  {$ENDIF}
end;

constructor TAlienList.Create;
begin
  alien_api_init(Self);

  if TAlienListRecord=nil then
  begin
    TAlienListRecord:= TExtRecordDefinition.Create('TAlienListRecord');
    TAlienListRecord.AddInteger('Day',low(Integer),high(Integer));
    TAlienListRecord.AddRecordList('Aliens',RecordAlien);
  end;


  fAlienIcons := game_api_CreateImageListItem;
  fAlienIcons.Transparent:=true;
  fAlienIcons.TransparentColor:=clBlack;
  fAlienIcons.SystemMemory:=false;

  fHashMap:=TIDHashMap.Create;

  savegame_api_RegisterCustomSaveHandler(SaveToStream,LoadFromStream,nil,$6BF4F767);
  savegame_api_RegisterLoadGameSetHandler(LoadGameSetHandler);
  savegame_api_RegisterNewGameHandler(NewGameHandler);
end;

function TAlienList.GenerateAlien: PAlien;
var
  Index   : Integer;
  IndexOK : boolean;
  Anzahl  : Integer;
begin
  IndexOK:=false;
  Index:=0;
  Anzahl:=Count;
  while not IndexOK do
  begin
    Index:=random(Anzahl);
    if (Index>=0) and (Index<Anzahl) and (CanAttack(Index)) then
      IndexOK:=true;
  end;
  result:=Addr(fAliens[Index]);
end;

function TAlienList.GetAlien(Index: Integer): TAlien;
begin
  result:=fAliens[Index];
end;

function TAlienList.GetCount: Integer;
begin
  result:=Length(fAliens);
end;

procedure TAlienList.LoadFromStream(Stream: TStream);
var
  Dummy : Integer;
  Rec   : TExtRecord;
begin
  Rec:=TExtRecord.Create(TAlienListRecord);
  Rec.LoadFromStream(Stream);

  fDay:=Rec.GetInteger('Day');

  with Rec.GetRecordList('Aliens') do
  begin
    SetLength(fAliens,Count);
    for Dummy:=0 to Count-1 do
      fAliens[Dummy]:=ExtRecordToAlien(Item[Dummy]);
  end;

  Rec.Free;

  Abgleich;

  if length(fAliens)=0 then
    raise Exception.Create(ENoAliensInSave);
end;

procedure TAlienList.NewGameHandler(Sender: TObject);
var
  Dummy : Integer;
  Valid : boolean;
begin
  fDay:=-savegame_api_GetNewGameData.Alien;
  SetLength(fAliens,length(fSetAliens));
  for Dummy:=0 to high(fAliens) do
  begin
    fAliens[Dummy]:=fSetAliens[Dummy];
    {$IFDEF COMPLETERESEARCH}
    fAliens[Dummy].Autopsie:=true;
    {$ENDIF}
  end;

  Valid:=false;
  for Dummy:=0 to high(fAliens) do
  begin
    if CanAttack(Dummy) then
    begin
      Valid:=true;
      break;
    end;
  end;

  if not Valid then
    raise Exception.Create(ENoAliensInSet);

  AddAlienToHashList;
end;

procedure TAlienList.NextDay;
begin
  inc(fDay);
end;

procedure TAlienList.SaveToStream(Stream: TStream);
var
  Dummy : Integer;
  Rec   : TExtRecord;
begin
  Rec:=TExtRecord.Create(TAlienListRecord);

  Rec.SetInteger('Day',fDay);
  with Rec.GetRecordList('Aliens') do
  begin
    for Dummy:=0 to Self.Count-1 do
      Add(AlienToExtRecord(fAliens[Dummy]));
  end;

  Rec.SaveToStream(Stream);
  Rec.Free;
end;

procedure TAlienList.SetBitmap(Bitmap: TBitmap);
begin
  fAlienIcons.Picture.Bitmap:=Bitmap;
  fAlienIcons.TransparentColor:=Bitmap.Canvas.Pixels[0,0];
  fAlienIcons.PatternHeight:=32;
  fAlienIcons.PatternWidth:=32;
  fAlienIcons.Restore;
end;

procedure TAlienList.DrawImage(Index: Integer; Surface: TDirectDrawSurface;
  x, y: Integer);
begin
  Surface.Canvas.Release;
  Surface.Draw(X,Y,fAlienIcons.PatternRects[Index],fAlienIcons.PatternSurfaces[Index]);
end;

function TAlienList.IndexOfID(ID: Cardinal): Integer;
var
  Key: Integer;
begin
  if fHashMap.FindKey(ID,Key) then
    result:=Key
  else
    result:=-1;
end;

destructor TAlienList.destroy;
begin
  fHashMap.Free;
  inherited;
end;

function TAlienList.AddrOfID(ID: Cardinal): PAlien;
var
  Key   : Integer;
begin
  if fHashMap.FindKey(ID,Key) then
    result:=Addr(fAliens[Key])
  else
    result:=nil;
end;

procedure TAlienList.AddAlienToHashList;
var
  Dummy: Integer;
begin
  fHashMap.ClearList;

  // Aliens in die Hashliste eintragen
  for Dummy:=0 to High(fAliens) do
  begin
    fHashMap.InsertID(fAliens[Dummy].ID,Dummy);
  end;
end;

procedure TAlienList.AutopsieEnd(ID: Cardinal);
var
  Index: Integer;
begin
  Index:=IndexOfID(ID);
  if Index=-1 then
    exit;

  savegame_api_Message(Format(ST0312110001,[fAliens[Index].Name]),lmProjektEnd);

  fAliens[Index].Autopsie:=true;
end;

procedure TAlienList.LoadGameSetHandler(Sender: TObject);
var
  Aliens : TRecordArray;
  Dummy  : Integer;
begin
  Aliens:=savegame_api_GameSetGetAliens;
  SetLength(fSetAliens,length(Aliens));
  for Dummy:=0 to high(Aliens) do
  begin
    fSetAliens[Dummy]:=ExtRecordToAlien(Aliens[Dummy]);
  end;                                                  
end;

initialization

  AlienImageProducer:=TSequenzer.Create;
  AlienImageProducer.SetHighest(AlienImages-1);

finalization

  FreeAndNil(TAlienListRecord);
  AlienImageProducer.Free;

end.
