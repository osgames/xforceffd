{********************************************************************************
*										*
* Copyright (C) 2004  X-Force Team - jim_raynor@web.de				*
*										*
* This file is part of X-Force: Fight For Destiny (X-Force).			*
*										*
* X-Force is free software; you can redistribute it and/or modify		*
* it under the terms of the GNU General Public License as published by		*
* the Free Software Foundation; either version 2 of the License, or		*
* (at your option) any later version.						*
*										*
* X-Force is distributed in the hope that it will be useful,			*
* but WITHOUT ANY WARRANTY; without even the implied warranty of		*
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the			*
* GNU General Public License for more details.					*
*					    					*
* You should have received a copy of the GNU General Public License		*
* along with X-Force; if not, write to the Free Software			*
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA	*
*										*
*********************************************************************************
*										*
* Verwaltet bis zu acht Basen mit den Einrichtung. Zugriff sollte nur �ber	*
* die basis_api erfolgen.							*
*										*
********************************************************************************}

{$I ../Settings.inc}

unit BasisListe;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  XForce_types, DXDraws, DXClass, Koding, Loader, KD4Utils, TraceFile, EarthCalcs,
  NotifyList, ExtRecord, ProjektRecords, StringConst;

const
  // Ereignisse BasisListe
    // Neue Basis erbaut
  EVENT_BASISLISTBUILDNEW    : TEventID = 0;

    // Basis vernichtet
  EVENT_BASISLISTDESTROY     : TEventID = 1;

    // Ausgew�hlte Basis ge�ndert (�ber SetSelected)
  EVENT_BASISLISTCHANGE      : TEventID = 2;

  // Ereignisse Basis
  EVENT_BASISFREE            : TEventID = 3;
  EVENT_BASISVERNICHTET      : TEventID = 4;
    // Einrichtung wird zerst�rt
  EVENT_ROOMDESTROY          : TEventID = 5;

type
  TKillProcedure   = function(BaseID: Cardinal;out Name: String): Boolean;

  TCanSellRoomResult = (csrCanSell,csrInUse,csrNeedForCennection);

  {$M+}
  TBasis = class;
  {$M-}

  TBasisEvent      = procedure(ID: Cardinal;EventType: TBasisEventType) of object;

  TEinrichtungArray = Array of TEinrichtung;

  TBasisArray       = Array of TBasis;

  TIntegerArray     = Array of Integer;

  TAlphatronMap     = Array[0..359] of Array[0..179] of double;

  TBaseField = record
    Objekt   : PEinrichtung;
    Index    : Integer;
    TopLeft  : Boolean;
  end;

  TRepairRoom = record
    RoomID    : Cardinal;
    Techniker : Cardinal;
  end;

  TArrayRepairRoom = Array of TRepairRoom;

  TBasisListe = class(TObject)
  private
    fEinrichtungen   : TEinrichtungArray;
    fBasen           : TBasisArray;
    fSelectedBasis   : TBasis;
    fBasisIcons      : TPictureCollectionItem;
    fNotifyList      : TNotifyList;

    fAlphatronMap    : TAlphatronMap;
    fAlphatronBitmap : TBitmap;
    function AddBasis(Basis: TBasis): Integer;
    procedure DeleteBasis(Basis: TBasis);

    function GetEinrichtungen: Integer;
    function GetEinrichtung(Index: Integer): TEinrichtung;

    function GetBasis(Index: Integer): TBasis;
    function GetHauptBasis: TBasis;
    procedure SetSelectedBasis(const Value: TBasis);
    function GetCount: Integer;

    procedure AddForschItem(Projekt: TObject);
    procedure AddLiftRoom;
    function GetSelectedBasis: TBasis;
    { Private-Deklarationen }
  protected
    procedure CreateAlphatronMap;
    procedure BuildAlphatronImage;

    procedure Clear;

    procedure NextHour(Sender: TObject);
    procedure AfterLoadHandler(Sender: TObject);
    procedure ChangeAlphatronMining(Sender: TObject);
    { Protected-Deklarationen }
  public
    constructor Create;
    destructor destroy;override;

    procedure SetBitmap(Bitmap: TBitmap);
    procedure DrawIcon(Surface: TDirectDrawSurface;x,y: Integer);
    procedure NewGame(pos: TFloatPoint);
    procedure Abgleich;

    procedure NextRound(Minuten: Integer);
    procedure NextDay;
    procedure NextWeek;

    procedure SaveToStream(Stream: TStream);
    procedure LoadFromStream(Stream: TStream);

    // Einrichtungen
    function IndexOfID(ID: Cardinal): Integer;

    function GetBasisIndex(Basis: TBasis): Integer;overload;
    function GetBasisIndex(BasisID: Cardinal): Integer;overload;

    function GetBasisFromID(ID: Cardinal): TBasis;
    function GesamteEinrichtungen: Integer;
    function GetWeeklyKost: Integer;

    function GetAddrOfEinrichtung(Index: Integer): PEinrichtung;

    function CanRoomRepair(Room: PEinrichtung): Boolean;
    function RoomInRepair(Room: PEinrichtung): Boolean;

    function BuildNewBasis(Point: TFloatPoint; BaseType: TBaseType): TBasis;
    function CheckBasePosition(Typ: TBaseType; Point: TFloatPoint; NewGame: Boolean = false): TCheckBasePosResult;
    procedure VernichteBasis(Basis: TBasis);

    // Verwaltung Alphatron
    function AlphatronPerHour: double;
    function AlphatronAtPos(Point: TFloatPoint): double;overload;
    function AlphatronAtPos(X,Y: Integer): double;overload;

    { Funktionen zur Basisverteidigung }
    procedure KAMPFInitShootTable(Minuten: Integer);
    function KAMPFShootToUFO(UFORef: TObject; Minute: Integer): boolean;

    property Einrichtung[Index: Integer]: TEinrichtung read GetEinrichtung;default;
    property Basen[Index: Integer]: TBasis read GetBasis;

    property Count         : Integer read GetCount;
    property Einrichtungen : Integer read GetEinrichtungen;

    property Selected      : TBasis read GetSelectedBasis write SetSelectedBasis;
    property HauptBasis    : TBasis read GetHauptBasis;

    property NotifyList    : TNotifyList read fNotifyList;

    property AlphatronMap  : TBitmap read fAlphatronBitmap;
  end;

  TBasis = class(TObject)
  private
    fList              : TBasisListe;
    fName              : String;
    fBasis             : TEinrichtungArray;
    fBasisPos          : TFloatPoint;
    fAbwehrRange       : Integer;
    fShootTable        : Array of Array of Boolean;
    fID                : Cardinal;
    fNotifyList        : TNotifyList;

    // Alphatron
    fAlphatronPerHour  : double;
    fQuarryAlphatron   : double;
    fAlphatronStorage  : Integer;
    fAlphatron         : Integer;
    fMiningStrength    : Integer;

    fBaseSize          : TSize;
    fBaseMap           : Array of Array of Array of TBaseField;
    fBuildingFloors    : Integer;

    // Speicherung von Konstanten die in RecalcStaticAttributes ermittelt werden
    fSensorWidth       : Integer;
    fShieldPoints      : Integer;

    // Speichern, wann
    fBaseAttackMessage : Integer;

    fRunningCosts      : double;
    fBaseType          : TBaseType;

    // Basis zur der Automatisch am Tagesende das vorhandene Alphatron transportiert wird
    fAlphatronTransferBase : Cardinal;
    fEBaseDestroy          : TEventHandle;

    fRepairList        : TArrayRepairRoom;

    // Feste Attribute (Sensorreichweite/Abwehrreichweiten) neu berechnen
    procedure RecalcStaticAttributes;

    function GetCount: Integer;
    function GetEinrichtung(Index: Integer): TEinrichtung;
    function GetHangarBelegt: Integer;
    function GetHangarRaum: Integer;
    function GetLaborBelegt: Integer;
    function GetLaborRaum: Integer;
    function GetLagerBelegt: double;
    function GetLagerRaum: double;
    function GetWerkBelegt: Integer;
    function GetWerkRaum: Integer;
    function GetWohnBelegt: Integer;
    function GetWohnRaum: Integer;
    function GetWeeklyKost: Integer;

    procedure RebuildBaseMap;

    function GenerateRoomID: Cardinal;

    procedure RecalcAlphatronMiningStrength;
    procedure NextMinute(Minuten: Integer);
    procedure QuarryAlphatron(Minuten: Integer);

    function CanRoomUse(const Room: TEinrichtung): Boolean;
    function GetAutoTransferBase: TBasis;
    procedure SetAutoTransferBase(const Value: TBasis);
    function GetAlphatronMiningEffectiveness: double;

    procedure AddHeadGear;

    function DestroyRoom(RoomIndex: Integer): Boolean;

    procedure TransferBaseDestroyed(Sender: TObject);
  protected
    procedure Abgleich(Projekt: TForschProject);

    procedure CancelBuild(Index: Integer);
    function UseShields(Strength: Integer): Integer;

    procedure OptimizeFloors;

    procedure CreateStartBase;

    procedure DeleteFloors(BuildFloor, X, Y, Width, Height: Integer);

    procedure NextDay;
    procedure NextHour;

    // an allen Einrichtungen gleichm��igen Schaden verursachen
    // Damage im Bereich 0..1 (%-Wert)
    procedure NotEnoughUpKeepExpense(Damage: double);

    procedure SetBaseSize(Width, Height, BuildFloors: Integer);
  public
    constructor Create(List: TBasisListe; Typ: TBaseType;Position: TFloatPoint);overload;
    constructor Create(List: TBasisListe);overload;
    destructor Destroy; override;
    procedure SaveToStream(Stream: TStream);
    procedure LoadFromStream(Stream: TStream);
    procedure FreeHangar;
    procedure FreeWohnRoom;
    procedure FreeLaborRoom;
    procedure FreeWerkRoom;
    procedure FreeLagerRoom(Room:double);
    function NeedWohnRoom: boolean;
    function NeedLaborRoom: boolean;
    function NeedWerkRoom: boolean;
    function NeedLagerRoom(Room: double): boolean;
    function NeedHangar: boolean;

    // Erstellt eine Einrichtung an einer zuf�lligen Position in der Basis
    procedure AddEinrichtung(Einrichtung: TEinrichtung);
    procedure DeleteEinrichtung(RoomID: Cardinal);

    // Basisbau
    function GetRoomIndex(ID: Cardinal): Integer;

    function CanPlaceRoom(BuildFloor,X,Y: Integer; Room: PEinrichtung;CheckConnection: Boolean = true): Boolean;
    function CanPlaceRoomInFloor(RoomTyp: TRoomTyp; BuildFloor: Integer): boolean;

    procedure Build(BuildFloor,X,Y: Integer;const Einrichtung: TEinrichtung);
    procedure SellRoom(Index: Integer);
    function RoomInUse(Index: Integer): Boolean;overload;
    function RoomInUse(const Room: TEinrichtung): Boolean;overload;
    function CanSellRoom(Index: Integer): TCanSellRoomResult;

    procedure BuildNewFloor;
    function NeedBuildLift: Boolean;

    procedure RepairRoom(RoomID: Cardinal; TechnikerID: Cardinal =0);
    procedure RepairAllRooms;
    function RoomInRepair(RoomID: Cardinal): Boolean;

    function GetRoomImage(const Room: TEinrichtung;X,Y,BuildFloor: Integer): String;

    // Alphatron verwaltung
    function GetAlphatronPerHour: double;
    function NeedAlphatron(NeedAlphatron: Integer): Integer;
    procedure FreeAlphatron(FreeAlphatron: Integer);

    procedure TransferAlphatron(Alphatron: Integer; Basis: TBasis);

    // Basis-Angriff
    procedure DoTreffer(Schaden: Integer);
    procedure Vernichten;

    { Funktionen zur Basisverteidigung }
    procedure KAMPFInitShootTable(Minuten: Integer);
    function KAMPFShootToUFO(UFORef: TObject; Minute: Integer): boolean;

    function GetBaseFieldInfo(BuildFloor,X,Y: Integer): TBaseField;

    property Count                         : Integer read GetCount;
    property Einrichtungen[Index: Integer] : TEinrichtung read GetEinrichtung;
    property WohnRaum                      : Integer read GetWohnRaum;
    property WohnBelegt                    : Integer read GetWohnBelegt;
    property LaborRaum                     : Integer read GetLaborRaum;
    property LaborBelegt                   : Integer read GetLaborBelegt;
    property WerkRaum                      : Integer read GetWerkRaum;
    property WerkBelegt                    : Integer read GetWerkBelegt;
    property LagerRaum                     : double read GetLagerRaum;
    property LagerBelegt                   : double read GetLagerBelegt;
    property HangerRaum                    : Integer read GetHangarRaum;
    property HangerBelegt                  : Integer read GetHangarBelegt;
    property NotifyList                    : TNotifyList read fNotifyList;

    property BasisPos                      : TFloatPoint read fBasisPos;
    property BaseSize                      : TSize read fBaseSize;
    property BuildingFloors                : Integer read fBuildingFloors;

    property Alphatron                     : Integer read fAlphatron;
    property AutoTransferBase              : TBasis read GetAutoTransferBase write SetAutoTransferBase;
    property AlphatronPerHour              : double read fAlphatronPerHour;
    property AlphatronMiningEffectiveness  : double read GetAlphatronMiningEffectiveness;

    property SensorWidth                   : Integer read fSensorWidth;
    property AbwehrRange                   : Integer read fAbwehrRange;
    property ShieldStrength                : Integer read fShieldPoints;
    property AlphatronStorage              : Integer read fAlphatronStorage;

    property RunningCostWeek               : double read fRunningCosts;

  published
    function WriteToScriptString: String;
    class function ReadFromScriptString(ObjStr: String): TObject;

    property ID                            : Cardinal read fID;
    property Name                          : String read fName write fName;

    property BaseType                      : TBaseType read fBaseType;
  end;


implementation

uses UFOList, string_utils, savegame_api, game_api, basis_api, array_utils,
  forsch_api, ConvertRecords, country_api, Defines, math, Blending,
  raumschiff_api, werkstatt_api, soldaten_api, lager_api, earth_api,
  formular_utils;
{ TBasisListe }

var
  TBasisListRecord  : TExtRecordDefinition;
  TBasisRecord      : TExtRecordDefinition;
  TRepairListRecord : TExtRecordDefinition;

procedure TBasisListe.Abgleich;
var
  Projects : TRecordArray;
  Entry    : TForschProject;
  Dummy    : Integer;
  Dummy2   : Integer;
  Index    : Integer;
begin
  Projects:=savegame_api_GameSetGetProjects;

  for Dummy:=0 to high(Projects) do
  begin
    Entry:=RecordToProject(Projects[Dummy]);

    if Entry.TypeID<>ptEinrichtung then
      continue;

    for Index:=0 to high(fEinrichtungen) do
    begin
      if fEinrichtungen[Index].ID=Entry.ID then
      begin
        // Hier m�ssen in Zukunft neue Eigenschaften aus Entry in fEinrichtungen �bernommen werden
        break;
      end;
    end;

    for Dummy2:=0 to high(fBasen) do
      fBasen[Dummy2].Abgleich(Entry);
  end;
end;

function TBasisListe.AddBasis(Basis: TBasis): Integer;
begin
  SetLength(fBasen,Length(fBasen)+1);

  fBasen[high(fBasen)]:=Basis;

  result:=high(fBasen);
//  raise Exception.Create(ETooManyBases);
end;

procedure TBasisListe.AddForschItem(Projekt: TObject);
var
  Forschung: PForschProject;
begin
  Assert(Projekt<>nil);
  Forschung:=PForschProject(Projekt);

  if not (Forschung.TypeID=ptEinrichtung) then
    exit;

  SetLength(fEinrichtungen,Einrichtungen+1);

  with fEinrichtungen[high(fEinrichtungen)] do
  begin
    Name:=Forschung.Name;
    ID:=Forschung.ID;
    KaufPreis:=Forschung.KaufPreis;
    SmallSchiff:=Forschung.SmallSchiff;
    LargeSchiff:=Forschung.LargeSchiff;
    LagerRaum:=Forschung.LagerRaum;
    Quartiere:=Forschung.Quartiere;
    WerkRaum:=Forschung.WerkRaum;
    Abwehr:=Forschung.Abwehr;
    LaborRaum:=Forschung.LaborRaum;

    days:=0;
    BauZeit:=Forschung.BauZeit;
    Info:=Forschung.Info;
    Reichweite:=Forschung.Reichweite;
    Frequenz:=max(1,Forschung.PixPerSec);
    ImageBase:=Forschung.BuildImageBase;
    ImageUnder:=Forschung.BuildImageUnder;

    RoomTyp:=Forschung.RoomTyp;

    SensorWidth:=Forschung.SensorWidth;
    ShieldStrength:=Forschung.ShieldStrength;
    Treffsicherheit:=Forschung.Treffsicherheit;
    maxReichweite:=Forschung.maxReichweite;
    AlphatronStorage:=Forschung.AlphatronStorage;
    WaffenArt:=Forschung.WaffType;

    HitPoints:=Forschung.RoomHitPoints;
    AktHitpoints:=Forschung.RoomHitPoints;

    Width:=Forschung.UnitWidth;
    Height:=Forschung.UnitHeight;
  end;
end;

function TBasisListe.AlphatronAtPos(Point: TFloatPoint): double;
begin
  result:=AlphatronAtPos(round(Point.X),round(Point.Y));
end;

procedure TBasisListe.AfterLoadHandler(Sender: TObject);
var
  Dummy: Integer;
begin
  SetSelectedBasis(fBasen[0]);

  Abgleich;

  for Dummy:=0 to high(fBasen) do
    fBasen[Dummy].RecalcStaticAttributes;
end;

function TBasisListe.AlphatronAtPos(X, Y: Integer): double;
begin
  Assert((X>=0) and (X<=359));
  Assert((Y>=0) and (Y<=179));

  result:=fAlphatronMap[X,Y];
end;

function TBasisListe.AlphatronPerHour: double;
var
  Dummy: Integer;
begin
  result:=0;
  for Dummy:=0 to high(fBasen) do
    result:=result+fBasen[Dummy].GetAlphatronPerHour;
end;

procedure TBasisListe.BuildAlphatronImage;
var
  X,Y: Integer;
begin
  for X:=0 to 359 do
  begin
    for Y:=0 to 179 do
    begin
      if PointOverLand(X,Y) then
        fAlphatronBitmap.Canvas.Pixels[X,Y]:=LineWinColorTable[round(fAlphatronMap[X,Y]/AlphatronMax*100)]
      else
        fAlphatronBitmap.Canvas.Pixels[x,y]:=clBlack;
    end;
  end;
end;

function TBasisListe.BuildNewBasis(Point: TFloatPoint; BaseType: TBaseType): TBasis;
begin
  result:=nil;

  if length(fBasen)>0 then
  begin
    // Nur Pr�fen und Geld abziehen, wenn es nicht die Hauptbasis ist
    if not (CheckBasePosition(BaseType,Point)=cbprOK) then
      exit;

    savegame_api_NeedMoney(BaseCosts[BaseType],kbBB);
  end;

  result:=TBasis.Create(Self,BaseType,Point);

  fNotifyList.CallEvents(EVENT_BASISLISTBUILDNEW,result);
end;

function TBasisListe.CheckBasePosition(Typ: TBaseType;
  Point: TFloatPoint; NewGame: Boolean = false): TCheckBasePosResult;
const
  NeedFriendly : Array[TBaseType] of Integer = ( {btBase}          80,
                                                 {btAlphatronMine} 60,
                                                 {btOutpost}       45);
  BaseCount    : Array[TBaseType] of Integer = ( {btBase}          8,
                                                 {btAlphatronMine} 15,
                                                 {btOutpost}       10);
var
  LandIndex: Integer;
  Land     : PLand;
  Dummy    : Integer;
  Count    : Integer;
begin
  result:=cbprOK;

  if not PointOverLand(Point) then
  begin
    result:=cbprNotOnLand;
    exit;
  end;

  // Bei beginn eines neuen Spiels muss nur gepr�ft werden, ob die Basis �ber Land
  // gebaut wird
  if NewGame then
    exit;

  LandIndex:=country_api_LandNrOverPos(Point);
  if LandIndex<>-1 then
  begin
    Land:=country_api_GetCountry(LandIndex);
    // Vertrauen pr�fen
    if Land.Friendly<NeedFriendly[Typ] then
    begin
      result:=cbprFriendlytooSmall;
      exit;
    end;
  end;

  // Pr�fen, ob bereits ein St�tzpunkt vom gleichem Typ in diesem Land gebaut wurde
  // Z�hlen der St�tzpunkte vom gleichen Typ
  Count:=0;
  for Dummy:=0 to high(fBasen) do
  begin
    if fBasen[Dummy].BaseType=Typ then
    begin
      inc(Count);

      if LandIndex=country_api_LandNrOverPos(fBasen[Dummy].BasisPos) then
      begin
        case Typ of
          btBase          : result:=cbprBaseInThisCountry;
          btAlphatronMine : result:=cbprAlphatronMineInThisCountry;
          btOutPost       : result:=cbprOutPostInThisCountry;
        end;
        exit;
      end;
    end;
  end;

  // Anzahl der St�tzpunkte pr�fen
  if Count>=BaseCount[Typ] then
  begin
    case Typ of
      btBase          : result:=cbprTooManyBases;
      btAlphatronMine : result:=cbprTooManyAlphatronMines;
      btOutPost       : result:=cbprTooManyOutposts;
    end;
    exit;
  end;
end;

procedure TBasisListe.Clear;
var
  Dummy: Integer;
begin
  for Dummy:=high(fBasen) downto 0 do
  begin
    if Selected<>fBasen[Dummy] then
      fBasen[Dummy].Free;
  end;

  if Selected<>nil then
  begin
    Selected.Free;
    Selected:=nil;
  end;

  SetLength(fBasen,0);
  SetLength(fEinrichtungen,0);
end;

constructor TBasisListe.Create;
begin
  basis_api_init(Self);

  fBasisIcons := game_api_CreateImageListItem;
  fBasisIcons.Transparent:=true;
  fBasisIcons.TransparentColor:=clWhite;
  fBasisIcons.SystemMemory:=true;

  fNotifyList:=TNotifyList.Create;

  fAlphatronBitmap:=TBitmap.Create;
  fAlphatronBitmap.Width:=360;
  fAlphatronBitmap.Height:=180;

  forsch_api_RegisterProjektEndHandler(AddForschItem);

  savegame_api_RegisterCustomSaveHandler(SaveToStream,LoadFromStream,nil,$4A57112C);
  savegame_api_RegisterAfterLoadHandler(AfterLoadHandler);

  savegame_api_RegisterHourEndHandler(NextHour);

  werkstatt_api_RegisterAlphatronMiningChangeHandler(ChangeAlphatronMining);
end;

procedure TBasisListe.CreateAlphatronMap;
var
  Dummy    : Integer;
  X        : Integer;
  Y        : Integer;
  X2,Y2    : Integer;
  Entfern  : double;
  Value    : double;
  EntVal   : double;
begin
  // Alphatron-Karte zur�cksetzen
  FillChar(fAlphatronMap,SizeOf(fALphatronMap),#0);

  for Dummy:=1 to AlphatronHotSpotCount do
  begin
    X:=random(360);
    Y:=random(180);

    Value:=AlphatronMin+(random*AlphatronAdd);
    for X2:=X-AlphatronRange to X+AlphatronRange do
    begin
      for Y2:=Y-AlphatronRange to Y+AlphatronRange do
      begin
        if PtInRect(Rect(0,0,360,180),Point(X2,Y2)) then
        begin
          Entfern:=CalculateEntfern(Point(X2,Y2),Point(X,Y));
          EntVal:=(1-(Entfern/AlphatronRange))*Value;
          fAlphatronMap[X2,Y2]:=min(AlphatronMax,fAlphatronMap[X2,Y2]+max(0,EntVal));
        end;
      end;
    end;
  end;

  BuildAlphatronImage;
end;

procedure TBasisListe.DeleteBasis(Basis: TBasis);
begin
  if fSelectedBasis=Basis then
    fSelectedBasis:=nil;

  DeleteArray(Addr(fBasen),TypeInfo(TBasisArray),GetBasisIndex(Basis));

  if length(fBasen)>0 then
    SetSelectedBasis(fBasen[0]);
end;

destructor TBasisListe.destroy;
begin
  fNotifyList.Clear;
  Clear;
  fNotifyList.Free;
  fAlphatronBitmap.Free;
  inherited;
end;

procedure TBasisListe.DrawIcon(Surface: TDirectDrawSurface; x, y: Integer);
begin
  Surface.Draw(x,y,fBasisIcons.PatternRects[0],fBasisIcons.PatternSurfaces[0],true);
end;

function TBasisListe.GesamteEinrichtungen: Integer;
var
  Dummy: Integer;
begin
  result:=0;
  for Dummy:=0 to high(fBasen) do
  begin
    inc(result,fBasen[Dummy].Count);
  end;
end;

function TBasisListe.GetAddrOfEinrichtung(Index: Integer): PEinrichtung;
begin
  Assert((Index>=0) and (Index<length(fEinrichtungen)));

  result:=Addr(fEinrichtungen[Index]);
end;

function TBasisListe.GetBasis(Index: Integer): TBasis;
begin
  result:=fBasen[Index];
end;

function TBasisListe.GetBasisFromID(ID: Cardinal): TBasis;
var
  Dummy: Integer;
begin
  for Dummy:=0 to high(fBasen) do
  begin
    if fBasen[Dummy].ID=ID then
    begin
      result:=fBasen[Dummy];
      exit;
    end;
  end;
  result:=nil;
end;

function TBasisListe.GetBasisIndex(Basis: TBasis): Integer;
var
  Dummy: Integer;
begin
  result:=-1;
  for Dummy:=0 to high(fBasen) do
  begin
    if fBasen[Dummy]=Basis then
    begin
      result:=Dummy;
      exit;
    end;
  end;
end;

function TBasisListe.GetBasisIndex(BasisID: Cardinal): Integer;
var
  Dummy: Integer;
begin
  result:=-1;
  for Dummy:=0 to high(fBasen) do
  begin
    if fBasen[Dummy].ID=BasisID then
    begin
      result:=Dummy;
      exit;
    end;
  end;
end;

function TBasisListe.GetCount: Integer;
begin
  result:=length(fBasen);
end;

function TBasisListe.GetEinrichtung(Index: Integer): TEinrichtung;
begin
  Assert((Index>=0) and (Index<length(fEinrichtungen)),'Ung�ltiger Index bei GetEinrichtung');
  result:=fEinrichtungen[Index];
end;

function TBasisListe.GetEinrichtungen: Integer;
begin
  result:=length(fEinrichtungen);
end;

function TBasisListe.GetHauptBasis: TBasis;
var
  Dummy: Integer;
begin
  result:=nil;
  for Dummy:=0 to high(fBasen) do
  begin
    if fBasen[Dummy].BaseType=btBase  then
    begin
      result:=fBasen[Dummy];
      exit;
    end;
  end;
end;

function TBasisListe.GetWeeklyKost: Integer;
var
  Dummy: Integer;
begin
  result:=0;
  for Dummy:=0 to high(fBasen) do
    inc(result,fBasen[Dummy].GetWeeklyKost);
end;

function TBasisListe.IndexOfID(ID: Cardinal): Integer;
var
  Dummy: Integer;
begin
  result:=-1;
  for Dummy:=0 to Einrichtungen-1 do
  begin
    if fEinrichtungen[Dummy].ID=ID then
    begin
      result:=Dummy;
      exit;
    end;
  end;
end;

procedure TBasisListe.KAMPFInitShootTable(Minuten: Integer);
var
  Dummy : Integer;
begin
  for Dummy:=0 to high(fBasen) do
    fBasen[Dummy].KAMPFInitShootTable(Minuten);
end;

function TBasisListe.KAMPFShootToUFO(UFORef: TObject; Minute: Integer): boolean;
var
  Dummy : Integer;
begin
  result:=false;
  for Dummy:=0 to high(fBasen) do
  begin
    result:=fBasen[Dummy].KAMPFShootToUFO(UFORef,Minute);
    if result then
      exit;
  end;
end;

procedure TBasisListe.LoadFromStream(Stream: TStream);
var
  Dummy  : Integer;
  Rec    : TExtRecord;
  Basen  : Integer;
begin
  Clear;

  Rec:=TExtRecord.Create(TBasisListRecord);
  Rec.LoadFromStream(Stream);

  with Rec.GetRecordList('Einrichtungen') do
  begin
    SetLength(fEinrichtungen,Count);
    for Dummy:=0 to high(fEinrichtungen) do
      fEinrichtungen[Dummy]:=RecordToEinrichtung(Item[Dummy]);
  end;

  if Rec.GetBinaryLength('AlphatronMap')>0 then
  begin
    CopyMemory(Addr(fAlphatronMap),Rec.GetBinary('AlphatronMap'),sizeof(fAlphatronMap));
    BuildAlphatronImage;
  end
  else
    CreateAlphatronMap;

  Basen:=Rec.GetInteger('BasisCount');

  Rec.Free;

  for Dummy:=1 to Basen do
  begin
    with TBasis.Create(Self) do
      LoadFromStream(Stream);
  end;

  for Dummy:=0 to Basen-1 do
    fBasen[Dummy].SetAutoTransferBase(fBasen[Dummy].AutoTransferBase);
end;

procedure TBasisListe.NewGame(Pos: TFloatPoint);
var
  Entry          : TForschProject;
  Projects       : TRecordArray;
  Dummy          : Integer;
  Basis          : TBasis;
begin
  Clear;

  // Hauptbasis erstellen
  Basis:=BuildNewBasis(Pos,btBase);
  Assert(Basis<>nil);
  Basis.Name:=LBasisName;

  Basis.FreeAlphatron(savegame_api_GetNewGameData.StartAlphatron);

  Projects:=savegame_api_GameSetGetProjects;

  // Der Aufzug wird immer als erstes der Einrichtungsliste hinzugef�gt
  // (Aufz�ge k�nnen nicht im Editor bearbeitet werden
  AddLiftRoom;

  for Dummy:=0 to high(Projects) do
  begin
    Entry:=RecordToProject(Projects[Dummy]);
    if forsch_api_isStartProject(Entry) and (not Entry.AlienItem) and (Entry.TypeID=ptEinrichtung) then
      AddForschItem(TObject(Addr(Entry)));
  end;

  Basis.CreateStartBase;

  CreateAlphatronMap;
  
  fSelectedBasis:=Basis;
end;

procedure TBasisListe.NextDay;
var
  Dummy: Integer;
begin
  for Dummy:=0 to high(fBasen) do
    fBasen[Dummy].NextDay;
end;

procedure TBasisListe.NextHour(Sender: TObject);
var
  Dummy : Integer;
begin
  for Dummy:=0 to high(fBasen) do
    fBasen[Dummy].NextHour;
end;

procedure TBasisListe.NextRound(Minuten: Integer);
var
  Dummy: Integer;
begin
  for Dummy:=0 to high(fBasen) do
    fBasen[Dummy].NextMinute(Minuten);
end;

procedure TBasisListe.NextWeek;
var
  Gesamt  : Integer;
  Schaden : double;

  Dummy   : Integer;
begin
  Gesamt:=GetWeeklyKost;
  if not savegame_api_NeedMoney(Gesamt,kbBB) then
  begin
    // Schaden an allen St�tzbunkten berechnen, wenn nicht gen�gend Geld f�r die
    // Wartungsarbeiten zur Verf�gung steht
    // Es werden bis zu 5% schaden verursacht
    Schaden:=(1-savegame_api_GetCredits/Gesamt)*0.05;
    savegame_api_NeedMoney(savegame_api_GetCredits,kbBB,true);

    if Schaden=0 then
      exit;
    for Dummy:=0 to Count-1 do
      Basen[Dummy].NotEnoughUpKeepExpense(Schaden);
  end;
end;

procedure TBasisListe.SaveToStream(Stream: TStream);
var
  Dummy  : Integer;
  Rec    : TExtRecord;
begin
  Rec:=TExtRecord.Create(TBasisListRecord);

  with Rec.GetRecordList('Einrichtungen') do
  begin
    for Dummy:=0 to Einrichtungen-1 do
      Add(EinrichtungToRecord(fEinrichtungen[Dummy]));
  end;

  Rec.SetInteger('BasisCount',Count);
  Rec.SetBinary('AlphatronMap',sizeOf(fAlphatronMap),Addr(fAlphatronMap));
  Rec.SaveToStream(Stream);

  for Dummy:=0 to high(fBasen) do
    fBasen[Dummy].SaveToStream(Stream);

  Rec.Free;
end;

procedure TBasisListe.SetBitmap(Bitmap: TBitmap);
begin
  fBasisIcons.Picture.Bitmap:=Bitmap;
  fBasisIcons.PatternHeight:=32;
  fBasisIcons.PatternWidth:=32;
  fBasisIcons.Restore;
end;

procedure TBasisListe.SetSelectedBasis(const Value: TBasis);
begin
  if fSelectedBasis<>Value then
  begin
    fSelectedBasis := Value;
    fNotifyList.CallEvents(EVENT_BASISLISTCHANGE,Value);
  end;
end;

procedure TBasisListe.VernichteBasis(Basis: TBasis);
begin
  savegame_api_Message(Format(ST0309210010,[Basis.Name]),lmReserved);

  Basis.NotifyList.CallEvents(EVENT_BASISVERNICHTET,Basis);

  fNotifyList.CallEvents(EVENT_BASISLISTDESTROY,Basis);

  Basis.Free;

  if HauptBasis=nil then
    raise ELooseGame.Create(ST0309210011);
end;

procedure TBasisListe.ChangeAlphatronMining(Sender: TObject);
var
  Dummy: Integer;
begin
  for Dummy:=0 to high(fBasen) do
  begin
    fBasen[Dummy].RecalcAlphatronMiningStrength;
  end;
end;

procedure TBasisListe.AddLiftRoom;
begin
  SetLength(fEinrichtungen,Einrichtungen+2);

  with fEinrichtungen[high(fEinrichtungen)-1] do
  begin
    Name:=RoomTypNames[rtLift];
    RoomTyp:=rtLift;
    ImageBase:='default\BasisBau:LiftBase';
    ImageUnder:='default\BasisBau:LiftUnder';
    Width:=1;
    Height:=1;
    HitPoints:=500000;
  end;

  with fEinrichtungen[high(fEinrichtungen)] do
  begin
    Name:=RoomTypNames[rtFloor];
    RoomTyp:=rtFloor;
    Width:=1;
    Height:=1;
    HitPoints:=500000;
  end;

end;

function TBasisListe.CanRoomRepair(Room: PEinrichtung): Boolean;
begin
  result:=(Room.AktHitpoints<Room.Hitpoints) and (not RoomInRepair(Room));
end;

function TBasisListe.RoomInRepair(Room: PEinrichtung): Boolean;
var
  Base: TBasis;
begin
  Base:=basis_api_GetBasisFromID(Room.BaseID);
  result:=Base.RoomInRepair(Room.RoomID);
end;

function TBasisListe.GetSelectedBasis: TBasis;
begin
  if (fSelectedBasis=nil) and (length(fBasen)>0) then
    result:=Basen[0]
  else
    result:=fSelectedBasis;
end;

{ TBasis }

procedure TBasis.Abgleich(Projekt: TForschProject);
var
  Dummy: Integer;
begin
  for Dummy:=0 to Length(fBasis)-1 do
  begin
    if fBasis[Dummy].ID=Projekt.ID then
    begin
      // Hier m�ssen in Zukunft neue Eigenschaften aus Entry in fEinrichtungen �bernommen werden
    end;
  end;
end;

procedure TBasis.AddEinrichtung(Einrichtung: TEinrichtung);

  procedure GeneratePosition;
  var
    X,Y       : Integer;
    CountDown : Integer;
  begin
    case Einrichtung.RoomTyp of
      rtBaseRooms     : Einrichtung.Position.BuildFloor:=random(fBuildingFloors-1)+1;
      rtDefense,
      rtSensor,
      rtDefenseShield,
      rtLivingRoom,
      rtLift          : Einrichtung.Position.BuildFloor:=0;
      rtHangar        : Einrichtung.Position.BuildFloor:=1;
    end;

    // Zuf�llige Position ermitteln
    X:=-1;
    CountDown:=5000;
    while not CanPlaceRoom(Einrichtung.Position.BuildFloor,X,Y,Addr(Einrichtung),false) do
    begin
      X:=random(max(0,fBaseSize.cx-(Einrichtung.Width-1)));
      Y:=random(max(0,fBaseSize.cy-(Einrichtung.Height-1)));
      dec(CountDown);
      Assert(CountDown>0,'Kein Platz mehr in der Basis');
    end;

    Einrichtung.Position.X:=X;
    Einrichtung.Position.Y:=Y;
  end;

  procedure CreateConnectionToLift;
  var
    XDest,YDest: Integer;
    X1,Y1      : Integer;
  begin
    // Vertikalen Floor bauen
    if (Einrichtung.Position.Y-fBasis[0].Position.Y)>((Einrichtung.Position.Y+Einrichtung.Height-1)-fBasis[0].Position.Y) then
      YDest:=Einrichtung.Position.Y+Einrichtung.Height-1
    else
      YDest:=Einrichtung.Position.Y;

    XDest:=fBasis[0].Position.X;

    if fBasis[0].Position.Y<YDest then
    begin
      for Y1:=fBasis[0].Position.Y+1 to YDest do
      begin
        Build(Einrichtung.Position.BuildFloor,XDest,Y1,fList.Einrichtung[1]);
      end;
    end
    else if fBasis[0].Position.Y>YDest then
    begin
      for Y1:=fBasis[0].Position.Y-1 downto YDest do
      begin
        Build(Einrichtung.Position.BuildFloor,XDest,Y1,fList.Einrichtung[1]);
      end;
    end;

    // Horizontalen Floor bauen
    if (Einrichtung.Position.Y-fBasis[0].Position.Y)>((Einrichtung.Position.Y+Einrichtung.Width-1)-fBasis[0].Position.Y) then
      XDest:=Einrichtung.Position.X+Einrichtung.Width-1
    else
      XDest:=Einrichtung.Position.X;

    if fBasis[0].Position.X<XDest then
    begin
      for X1:=fBasis[0].Position.X+1 to XDest do
      begin
        Build(Einrichtung.Position.BuildFloor,X1,YDest,fList.Einrichtung[1]);
      end;
    end
    else if fBasis[0].Position.X>XDest then
    begin
      for X1:=fBasis[0].Position.X-1 downto XDest do
      begin
        Build(Einrichtung.Position.BuildFloor,X1,YDest,fList.Einrichtung[1]);
      end;
    end;

  end;

begin
  if Einrichtung.ID=0 then
    Einrichtung.ID:=GenerateRoomID;

  if Einrichtung.BauZeit=0 then
    Einrichtung.Bauzeit:=10;

  Einrichtung.RoomID:=GenerateRoomID;
  Einrichtung.BaseID:=fID;

  Einrichtung.ActualValue:=round(Einrichtung.KaufPreis*InitValuePercent/100);
  Einrichtung.DaysToDecValue:=DaysToDecValue;

  Einrichtung.AktHitpoints:=Einrichtung.Hitpoints;

  // Position berechnen
  GeneratePosition;

  DeleteFloors(Einrichtung.Position.BuildFloor,Einrichtung.Position.X,Einrichtung.Position.Y,Einrichtung.Width,Einrichtung.Height);

  SetLength(fBasis,length(fBasis)+1);

  fBasis[high(fBasis)]:=Einrichtung;

  RecalcStaticAttributes;

  RebuildBaseMap;

  // Verbindung zum Aufzug herstellen
  if (Einrichtung.RoomTyp<>rtLift) and (Einrichtung.Position.BuildFloor>0) then
  begin
    Assert(fBasis[0].RoomTyp=rtLift);
    Assert(fList.fEinrichtungen[1].RoomTyp=rtFloor);
    CreateConnectionToLift;

    RebuildBaseMap;
  end;
end;

procedure TBasis.Build(BuildFloor,X,Y: Integer;const Einrichtung: TEinrichtung);
begin
  if not CanPlaceRoom(BuildFloor,X,Y,Addr(Einrichtung)) then
    exit;

  savegame_api_NeedMoney(Einrichtung.KaufPreis,kbBB,true);

  // Alle Verbindungstunnel, die auf der Baufl�che gebaut wurden, m�ssen
  // gel�scht werden
  DeleteFloors(BuildFloor,X,Y,Einrichtung.Width,Einrichtung.Height);

  SetLength(fBasis,Count+1);
  fBasis[high(fBasis)]:=Einrichtung;
  {$IFDEF FASTBUILD}
  fBasis[high(fBasis)].days:=0;
  {$ELSE}
  fBasis[high(fBasis)].days:=Einrichtung.BauZeit;
  {$ENDIF}
  fBasis[high(fBasis)].Position.X:=X;
  fBasis[high(fBasis)].Position.Y:=Y;
  fBasis[high(fBasis)].Position.BuildFloor:=BuildFloor;

  fBasis[high(fBasis)].AktHitpoints:=Einrichtung.Hitpoints;
  fBasis[high(fBasis)].AktShieldPoints:=Einrichtung.ShieldStrength;

  fBasis[high(fBasis)].RoomID:=GenerateRoomID;
  fBasis[high(fBasis)].BaseID:=fID;

  fBasis[high(fBasis)].ActualValue:=Einrichtung.KaufPreis;
  fBasis[high(fBasis)].DaysToDecValue:=DaysToDecValue;

  RebuildBaseMap;
  RecalcStaticAttributes;
end;

procedure TBasis.CancelBuild(Index: Integer);
begin
  if (Index<0) and (Index>Count-1) then
    exit;

  // Einrichtung noch im Bau?
  if fBasis[Index].days=0 then
    exit;

  savegame_api_FreeMoney(fBasis[Index].ActualValue,kbBB);

  DeleteEinrichtung(fBasis[Index].RoomID);

  RebuildBaseMap;
end;

function TBasis.CanSellRoom(Index: Integer): TCanSellRoomResult ;
var
  Objekts: TIntegerArray;
  Floor  : Integer;
  Dummy  : Integer;
  Room   : TEinrichtung;

  function IndexOfRoom(Room: Integer): Integer;
  var
    Dummy: Integer;
  begin
    result:=-1;
    for Dummy:=0 to high(Objekts) do
    begin
      if Objekts[Dummy]=Room then
      begin
        result:=Dummy;
        exit;
      end;
    end;
  end;

  procedure Check(Room: Integer);
  var
    X1,X2,Y1,Y2: Integer;
    Einr       : PEinrichtung;
    DumX,Dumy  : Integer;
  begin
    if (Room=Index) or (IndexOfRoom(Room)=-1) then
      // Raum wurde schon gepr�ft, bzw ist der, der gel�scht wird
      exit;

    DeleteArray(Addr(Objekts),TypeInfo(TIntegerArray),IndexOfRoom(Room));
    
    // Pr�fen, ob Einrichtung an einer anderen gebaut wird
    Einr:=Addr(fBasis[Room]);
    Y1:=Einr.Position.Y-1;
    Y2:=Einr.Position.Y+Einr.Height;
    for DumX:=Einr.Position.X to Einr.Position.X+Einr.Width-1 do
    begin
      if Y1>=0 then
      begin
        if fBaseMap[Floor,DumX,Y1].Objekt<>nil then
          Check(fBaseMap[Floor,DumX,Y1].Index);
      end;
      if Y2<fBaseSize.cy then
      begin
        if fBaseMap[Floor,DumX,Y2].Objekt<>nil then
          Check(fBaseMap[Floor,DumX,Y2].Index);
      end;
    end;

    X1:=Einr.Position.X-1;
    X2:=Einr.Position.X+Einr.Width;
    for DumY:=Einr.Position.Y to Einr.Position.Y+Einr.Height-1 do
    begin
      if X1>=0 then
      begin
        if fBaseMap[Floor,X1,DumY].Objekt<>nil then
          Check(fBaseMap[Floor,X1,DumY].Index);
      end;
      if X2<fBaseSize.cy then
      begin
        if fBaseMap[Floor,X2,DumY].Objekt<>nil then
          Check(fBaseMap[Floor,X2,DumY].Index);
      end;
    end;
  end;

begin
  result:=csrInUse;
  if (Index<0) and (Index>Count-1) then
    exit;

  Room:=fBasis[Index];

  // Der Aufzug kann nicht abgerissen werden
  if (Room.RoomTyp=rtLift) then
  begin
    result:=csrNeedForCennection;
    exit;
  end;

  if (Room.RoomTyp=rtGearHead) then
  begin
    result:=csrInUse;
    exit;
  end;

  if (Room.LagerBelegt>0) or (Room.WohnBelegt>0)
    or (Room.LaborBelegt>0) or (Room.WerkBelegt>0)
    or (Room.SmallBelegt>0) then
  begin
    result:=csrInUse;
    exit;
  end;

  Floor:=Room.Position.BuildFloor;

  // Im Obergeschoss brauchen keine Verbindungen eingehalten werden
  if Floor=0 then
  begin
    result:=csrCanSell;
    exit;
  end;

  // Erstmal alle Objekte auf der Ebene zusammenfassen
  for Dummy:=0 to high(fBasis) do
  begin
    if (Dummy<>Index) and ((fBasis[Dummy].RoomTyp=rtLift) or (fBasis[Dummy].Position.BuildFloor=Floor)) then
    begin
      SetLength(Objekts,length(Objekts)+1);
      Objekts[high(Objekts)]:=Dummy;
    end;
  end;

  if length(Objekts)>0 then
    Check(Objekts[0]);

  if length(Objekts)>0 then
    result:=csrNeedForCennection
  else
    result:=csrCanSell;
end;

function TBasis.CanPlaceRoom(BuildFloor,X, Y: Integer; Room: PEinrichtung;CheckConnection: Boolean): Boolean;
var
  DumX,DumY : Integer;
  Y1,Y2     : Integer;
  X1,X2     : Integer;
begin
  result:=false;
  if Room=nil then
    exit;

  if ((X+Room.Width)>fBaseSize.cx) or (X<0) then
    exit;

  if ((Y+Room.Height)>fBaseSize.cy) or (Y<0) then
    exit;

  // Pr�fen, ob Einrichtung auf einer anderen gebaut wird
  for DumX:=X to X+Room.Width-1 do
  begin
    for DumY:=Y to Y+Room.Height-1 do
    begin
      if (fBaseMap[BuildFloor,DumX,DumY].Objekt<>nil) and ((Room.RoomTyp=rtFloor) or (fBaseMap[BuildFloor,DumX,DumY].Objekt.RoomTyp<>rtFloor)) then
        exit;
    end;
  end;

  result:=CanPlaceRoomInFloor(Room.RoomTyp,BuildFloor);

  // Beim Aufzug m�ssen alle Etagen frei sein
  if (result) and (Room.RoomTyp=rtLift) then
  begin
    for DumY:=0 to fBuildingFloors-1 do
    begin
      if fBasemap[DumY,X,Y].Objekt<>nil then
        result:=false;
    end;
    exit;
  end;

  // Beim Hangar muss die Luftschleuse frei bleiben
  if (result) and (Room.RoomTyp=rtHangar) then
  begin
    if fBasemap[0,X,Y].Objekt<>nil then
      result:=false;
  end;

  if (result) and (BuildFloor>0) and CheckConnection then
  begin
    // Pr�fen, ob Einrichtung an einer anderen gebaut wird (in unterirdischen Ebenen)
    Y1:=Y-1;
    Y2:=Y+Room.Height;
    for DumX:=X to X+Room.Width-1 do
    begin
      if Y1>=0 then
      begin
        if fBaseMap[BuildFloor,DumX,Y1].Objekt<>nil then
        begin
          result:=true;
          exit;
        end;
      end;
      if Y2<fBaseSize.cy then
      begin
        if fBaseMap[BuildFloor,DumX,Y2].Objekt<>nil then
        begin
          result:=true;
          exit;
        end;
      end;
    end;

    X1:=X-1;
    X2:=X+Room.Width;
    for DumY:=Y to Y+Room.Height-1 do
    begin
      if X1>=0 then
      begin
        if fBaseMap[BuildFloor,X1,DumY].Objekt<>nil then
        begin
          result:=true;
          exit;
        end;
      end;
      if X2<fBaseSize.cx then
      begin
        if fBaseMap[BuildFloor,X2,DumY].Objekt<>nil then
        begin
          result:=true;
          exit;
        end;
      end;
    end;

    result:=false;
  end;
end;

function TBasis.CanPlaceRoomInFloor(RoomTyp: TRoomTyp;
  BuildFloor: Integer): boolean;
begin
  case RoomTyp of
    rtDefense,
    rtSensor,
    rtDefenseShield   : result:=BuildFloor=0;
    rtHangar          : result:=BuildFloor=1;
    rtBaseRooms       : result:=BuildFloor>=1;
    rtFloor           : result:=BuildFloor>=1;
    else
      result:=true;
  end;
end;

constructor TBasis.Create(List: TBasisListe; Typ: TBaseType; Position: TFloatPoint);
var
  Index: Integer;
  Land : String;
begin
  Assert(List<>nil);

  fEBaseDestroy:=-1;

  fNotifyList:=TNotifyList.Create;

  fList:=List;

  fID:=random(high(Cardinal)-1)+1;

  Index:=List.AddBasis(Self);

  if Index=0 then
    Name:=LBasisName
  else
  begin
    Land:=country_api_LandOverPos(Position);
    Name:=Format('%s %s',[BaseNames[Typ],Land]);
  end;

  if Typ=btAlphatronMine then
  begin
    fBaseSize.cx:=6;
    fBaseSize.cy:=6;

    fBuildingFloors:=1;

    fAlphatronPerHour:=fList.AlphatronAtPos(Position);

    SetAutoTransferBase(basis_api_GetMainBasis);
  end
  else
  begin
    fBaseSize.cx:=8;
    fBaseSize.cy:=8;
    if Typ=btOutPost then
      fBuildingFloors:=1
    else
      fBuildingFloors:=2;
  end;

  fBaseType:=Typ;
  fBasisPos:=Position;

  // F�rderturm bei Alphatronbergwerken hinzuf�gen
  if Typ=btAlphatronMine then
    AddHeadGear;

  SetBaseSize(fBaseSize.cx,fBaseSize.cy,fBuildingFloors);

  RecalcStaticAttributes;
end;

function TBasis.CanRoomUse(const Room: TEinrichtung): Boolean;
begin
  result:=false;

  if (Room.days>0) then
    exit;

  result:=true;
end;

constructor TBasis.Create(List: TBasisListe);
begin
  Assert(List<>nil);

  fNotifyList:=TNotifyList.Create;

  fEBaseDestroy:=-1;

  fList:=List;
  List.AddBasis(Self);
end;

destructor TBasis.Destroy;
begin
  SetAutoTransferBase(nil);
  fList.DeleteBasis(Self);
  fNotifyList.CallEvents(EVENT_BASISFREE,Self);
  fNotifyList.Free;
  inherited;
end;

procedure TBasis.SellRoom(Index: Integer);
begin
  if (Index<0) and (Index>Count-1) then
    exit;

  if fBasis[Index].days>0 then
  begin
    CancelBuild(Index);
    exit;
  end;

  if CanSellRoom(Index)<>csrCanSell then
    raise Exception.Create(MNoDeaktivate);

  savegame_api_FreeMoney(fBasis[Index].ActualValue,kbBB);

  DeleteEinrichtung(fBasis[Index].RoomID);

  RebuildBaseMap;

  RecalcStaticAttributes;
end;

procedure TBasis.FreeHangar;
var
  Dummy: Integer;
begin
  for Dummy:=Count-1 downto 0 do
  begin
    if CanRoomUse(fBasis[Dummy]) then
    begin
      if (fBasis[Dummy].SmallBelegt>0) then
      begin
        dec(fBasis[Dummy].SmallBelegt);
        Exit;
      end;
    end;
  end;
end;

procedure TBasis.FreeLaborRoom;
var
  Dummy: Integer;
begin
  for Dummy:=Count-1 downto 0 do
  begin
    if CanRoomUse(fBasis[Dummy]) then
    begin
      if (fBasis[Dummy].LaborBelegt>0) then
      begin
        dec(fBasis[Dummy].LaborBelegt);
        Exit;
      end;
    end;
  end;
end;

procedure TBasis.FreeLagerRoom(Room: double);
var
  Noch  : double;
  Dummy : Integer;
begin
  Assert(Room>=0);
  Noch:=Room;
  for Dummy:=Count-1 downto 0 do
  begin
    if CanRoomUse(fBasis[Dummy]) then
    begin
      if (fBasis[Dummy].LagerBelegt>Noch) then
      begin
        fBasis[Dummy].LagerBelegt:=fBasis[Dummy].LagerBelegt-Noch;
        Exit;
      end
      else if (fBasis[Dummy].LagerBelegt>0) then
      begin
        Noch:=Noch-fBasis[Dummy].LagerBelegt;
        fBasis[Dummy].LagerBelegt:=0;
      end;
    end;
  end;
end;

procedure TBasis.FreeWerkRoom;
var
  Dummy: Integer;
begin
  for Dummy:=Count-1 downto 0 do
  begin
    if CanRoomUse(fBasis[Dummy]) then
    begin
      if (fBasis[Dummy].WerkBelegt>0) then
      begin
        dec(fBasis[Dummy].WerkBelegt);
        Exit;
      end;
    end;
  end;
end;

procedure TBasis.FreeWohnRoom;
var
  Dummy: Integer;
begin
  for Dummy:=Count-1 downto 0 do
  begin
    if CanRoomUse(fBasis[Dummy]) then
    begin
      if (fBasis[Dummy].WohnBelegt>0) then
      begin
        dec(fBasis[Dummy].WohnBelegt);
        Exit;
      end;
    end;
  end;
end;

function TBasis.GetBaseFieldInfo(BuildFloor, X, Y: Integer): TBaseField;
begin
  Assert((BuildFloor>=0) and (BuildFloor<fBuildingFloors));
  Assert((X>=0) and (X<length(fBaseMap[BuildFloor])));
  Assert((Y>=0) and (Y<length(fBaseMap[BuildFloor,X])));

  result:=fBaseMap[BuildFloor,X,Y];
end;

function TBasis.GetCount: Integer;
begin
  result:=length(fBasis);
end;

function TBasis.GetEinrichtung(Index: Integer): TEinrichtung;
begin
  result:=fBasis[Index];
end;

function TBasis.GetHangarBelegt: Integer;
var
  Dummy: Integer;
begin
  result:=0;
  for Dummy:=0 to Count-1 do
  begin
    if CanRoomUse(fBasis[Dummy]) then
      inc(Result,fBasis[dummy].SmallBelegt);
  end;
end;

function TBasis.GetHangarRaum: Integer;
var
  Dummy: Integer;
begin
  result:=0;
  for Dummy:=0 to Count-1 do
  begin
    if CanRoomUse(fBasis[Dummy]) then
      inc(Result,fBasis[dummy].SmallSchiff);
  end;
end;

function TBasis.GetLaborBelegt: Integer;
var
  Dummy: Integer;
begin
  result:=0;
  for Dummy:=0 to Count-1 do
  begin
    if CanRoomUse(fBasis[Dummy]) then
      inc(Result,fBasis[dummy].LaborBelegt);
  end;
end;

function TBasis.GetLaborRaum: Integer;
var
  Dummy: Integer;
begin
  result:=0;
  for Dummy:=0 to Count-1 do
  begin
    if CanRoomUse(fBasis[Dummy]) then
      inc(Result,fBasis[dummy].LaborRaum);
  end;
end;

function TBasis.GetLagerBelegt: double;
var
  Dummy: Integer;
begin
  result:=0;
  for Dummy:=0 to Count-1 do
  begin
    if CanRoomUse(fBasis[Dummy]) then
      Result:=Result+fBasis[dummy].LagerBelegt;
  end;
end;

function TBasis.GetLagerRaum: double;
var
  Dummy: Integer;
begin
  result:=0;
  for Dummy:=0 to Count-1 do
  begin
    if CanRoomUse(fBasis[Dummy]) then
      Result:=Result+fBasis[dummy].LagerRaum;
  end;
end;

function TBasis.GetWeeklyKost: Integer;
begin
  result:=round(fRunningCosts);
  fRunningCosts:=0;
end;

function TBasis.GetWerkBelegt: Integer;
var
  Dummy: Integer;
begin
  result:=0;
  for Dummy:=0 to Count-1 do
  begin
    if CanRoomUse(fBasis[Dummy]) then
      inc(Result,fBasis[dummy].WerkBelegt);
  end;
end;

function TBasis.GetWerkRaum: Integer;
var
  Dummy: Integer;
begin
  result:=0;
  for Dummy:=0 to Count-1 do
  begin
    if CanRoomUse(fBasis[Dummy]) then
      inc(Result,fBasis[dummy].WerkRaum);
  end;
end;

function TBasis.GetWohnBelegt: Integer;
var
  Dummy: Integer;
begin
  result:=0;
  for Dummy:=0 to Count-1 do
  begin
    if CanRoomUse(fBasis[Dummy]) then
      inc(Result,fBasis[dummy].WohnBelegt);
  end;
end;

function TBasis.GetWohnRaum: Integer;
var
  Dummy: Integer;
begin
  result:=0;
  for Dummy:=0 to Count-1 do
  begin
    if CanRoomUse(fBasis[Dummy]) then
      inc(Result,fBasis[dummy].Quartiere);
  end;
end;

procedure TBasis.KAMPFInitShootTable(Minuten: Integer);
var
  Dummy : Integer;
begin
  SetLength(fShootTable,Count,Minuten);
  for Dummy:=0 to Count-1 do
    FillChar(Pointer(fShootTable[Dummy])^,SizeOf(Boolean)*Minuten,#0);
end;

function TBasis.KAMPFShootToUFO(UFORef: TObject; Minute: Integer): boolean;
var
  UFO     : TUFO;
  Dummy   : Integer;
  Absorb  : Integer;
  Dummy2  : Integer;
  Entf    : Integer;
  Name    : String;
  Treff   : double;
begin
  result:=false;
  UFO:=UFORef as TUFO;
  Entf:=EarthEntfernung(fBasisPos,UFO.Position);
  // Ausserhalb der Reichweite
  if Entf>fAbwehrRange then
    exit;

  for Dummy:=0 to Count-1 do
  begin
    // Abwehrturm der nicht im Bau ist?
    if (not CanRoomUse(fBasis[Dummy])) or (fBasis[Dummy].RoomTyp<>rtDefense) then
      continue;

    // Hat diese Einrichtung in dieser Minute bereits geschossen?
    if fShootTable[Dummy,Minute] then
      continue;

    // UFO befindet sich innerhalb der Reichweite?
    if (Entf<fBasis[Dummy].Reichweite) or (Entf>fBasis[Dummy].maxReichweite) then
      continue;

    fShootTable[Dummy,Minute]:=true;
    Name:=UFO.Name;

    // Berechnung der Treffsicherheit (siehe Basisbaukonzept)
    Treff:=((fBasis[Dummy].maxReichweite-(Entf - fBasis[Dummy].Reichweite)) / (fBasis[Dummy].maxReichweite-fBasis[Dummy].Reichweite)) * fBasis[Dummy].Treffsicherheit;

    // UFO wird beschossen
    for Dummy2:=1 to fBasis[Dummy].Frequenz do
    begin
      if RandomChance(Treff) then
      begin
        result:=UFO.DoTreffer(round(fBasis[Dummy].Abwehr),fBasis[Dummy].WaffenArt,Absorb);
        if result then
        begin
          // UFO wurde abgeschossen
          savegame_api_message(Format(MDestruction,[Name,fName]),lmUFOs);
          exit;
        end;
      end;
    end;
  end;
end;

procedure TBasis.LoadFromStream(Stream: TStream);
var
  Dummy  : Integer;
  Rec    : TExtRecord;
begin
  Rec:=TExtRecord.Create(TBasisRecord);
  Rec.LoadFromStream(Stream);

  fName:=Rec.GetString('Name');
  fID:=Rec.GetCardinal('ID');

  fBasisPos.X:=Rec.GetDouble('Position.X');
  fBasisPos.Y:=Rec.GetDouble('Position.Y');

  fBuildingFloors:=Rec.GetInteger('BuildingFloors');
  fBaseSize.cx:=Rec.GetInteger('BaseSize.X');
  fBaseSize.cy:=Rec.GetInteger('BaseSize.Y');

  fBaseType:=TBaseType(Rec.GetInteger('BaseType'));

  fAlphatron:=Rec.GetInteger('Alphatron');
  fQuarryAlphatron:=Rec.GetDouble('QuarryAlphatron');

  if fBaseType=btAlphatronMine then
    fAlphatronPerHour:=fList.AlphatronAtPos(fBasisPos)
  else
    fAlphatronPerHour:=0;

  fAlphatronTransferBase:=Rec.GetCardinal('TransferBase');

  with Rec.GetRecordList('Einrichtungen') do
  begin
    SetLength(fBasis,Count);
    for Dummy:=0 to high(fBasis) do
    begin
      fBasis[Dummy]:=RecordToEinrichtung(Item[Dummy]);
    end;
  end;

  with Rec.GetRecordList('RepairList') do
  begin
    SetLength(fRepairList,Count);
    for Dummy:=0 to high(fRepairList) do
    begin
      fRepairList[Dummy].RoomID:=Item[Dummy].GetCardinal('RoomID');
      fRepairList[Dummy].Techniker:=Item[Dummy].GetCardinal('TechnikerID');
    end;
  end;
  SetBaseSize(fBaseSize.cx,fBaseSize.cy,fBuildingFloors);

  Rec.Free;
end;

function TBasis.NeedHangar: boolean;
var
  Dummy: Integer;
begin
  result:=false;
  for Dummy:=0 to Count-1 do
  begin
    if CanRoomUse(fBasis[Dummy]) then
    begin
      if (fBasis[Dummy].SmallSchiff>0) and (fBasis[Dummy].SmallSchiff<>fBasis[Dummy].SmallBelegt) then
      begin
        inc(fBasis[Dummy].SmallBelegt);
        result:=true;
        exit;
      end;
    end;
  end;
end;

function TBasis.NeedLaborRoom: boolean;
var
  Dummy: Integer;
begin
  result:=false;
  for Dummy:=0 to Count-1 do
  begin
    if CanRoomUse(fBasis[Dummy]) then
    begin
      if (fBasis[Dummy].LaborRaum>0) and (fBasis[Dummy].LaborRaum<>fBasis[Dummy].LaborBelegt) then
      begin
        inc(fBasis[Dummy].LaborBelegt);
        result:=true;
        exit;
      end;
    end;
  end;
end;

function TBasis.NeedLagerRoom(Room: double): boolean;
var
  Dummy: Integer;
  Noch: Int64;
  IRoom: Int64;
  IFrei: Int64;
  IBele: Int64;
  IRaum: Int64;
begin
  IFrei:=round(LagerRaum-LagerBelegt)*10;
  IRoom:=round(Room)*10;
  if IFrei<IRoom then
  begin
    result:=false;
    exit;
  end;
  Result:=true;
  Noch:=round(Room)*10;
  for Dummy:=0 to high(fBasis) do
  begin
    if CanRoomUse(fBasis[Dummy]) then
    begin
      IRaum:=round(fBasis[Dummy].LagerRaum)*10;
      IFrei:=(round(fBasis[Dummy].LagerBelegt)*10)+Noch;
      IBele:=round(fBasis[Dummy].LagerBelegt)*10;
      if (IRaum>0) and (IRaum>=IFrei) then
      begin
        fBasis[Dummy].LagerBelegt:=fBasis[Dummy].LagerBelegt+(Noch/10);
        exit;
      end
      else if (IRaum>0) and (IBele<IRaum) then
      begin
        Noch:=round((Noch/10-(fBasis[Dummy].LagerRaum-fBasis[Dummy].LagerBelegt)))*10;
        fBasis[Dummy].LagerBelegt:=fBasis[Dummy].LagerRaum;
      end;
    end;
  end;
end;

function TBasis.NeedWerkRoom: boolean;
var
  Dummy: Integer;
begin
  result:=false;
  for Dummy:=0 to Count-1 do
  begin
    if CanRoomUse(fBasis[Dummy]) then
    begin
      if (fBasis[Dummy].WerkRaum>0) and (fBasis[Dummy].WerkRaum<>fBasis[Dummy].WerkBelegt) then
      begin
        inc(fBasis[Dummy].WerkBelegt);
        result:=true;
        exit;
      end;
    end;
  end;
end;

function TBasis.NeedWohnRoom: boolean;
var
  Dummy: Integer;
begin
  result:=false;
  for Dummy:=0 to Count-1 do
  begin
    if CanRoomUse(fBasis[Dummy]) then
    begin
      if (fBasis[Dummy].Quartiere>0) and (fBasis[Dummy].Quartiere<>fBasis[Dummy].WohnBelegt) then
      begin
        inc(fBasis[Dummy].WohnBelegt);
        result:=true;
        exit;
      end;
    end;
  end;
end;

procedure TBasis.NextDay;
var
  Dummy: Integer;
begin
  // Bau von Einrichtungen voranschreiten
  for Dummy:=0 to Count-1 do
  begin
    if fBasis[Dummy].days=0 then
    begin
      dec(fBasis[Dummy].DaysToDecValue);
      // Einrichtung verliert an Wert
      if fBasis[Dummy].DaysToDecValue=0 then
      begin
        {$IFDEF EXTERNALFORMULARS}
        fBasis[Dummy].ActualValue:=round(formular_utils_SolveFormular('CalcActualValue',['ActualValue'],[fBasis[Dummy].ActualValue]));
        {$ELSE}
        dec(fBasis[Dummy].ActualValue,round(fBasis[Dummy].ActualValue*DecValuePercent/100));
        {$ENDIF}
        fBasis[Dummy].DaysToDecValue:=DaysToDecValue;
      end;
    end;
    
    if fBasis[Dummy].days>0 then
    begin
      dec(fBasis[Dummy].days);
      if fBasis[Dummy].days=0 then
      begin
        savegame_api_message(Format(MBauEnd,[fBasis[Dummy].Name,fName]),lmBauEnd);
        fBasis[Dummy].ActualValue:=round(fBasis[Dummy].KaufPreis*InitValuePercent/100);
        RecalcStaticAttributes;
      end
      else
      begin
        // Wert f�r die verbleibende Bauzeit
        fBasis[Dummy].ActualValue:=round(fBasis[Dummy].KaufPreis*fBasis[Dummy].days/fBasis[Dummy].BauZeit);
        // Wert f�r die vergangene Zeit
        inc(fBasis[Dummy].ActualValue,round((fBasis[Dummy].KaufPreis*(fBasis[Dummy].BauZeit-fBasis[Dummy].days)/fBasis[Dummy].BauZeit)*InitValuePercent/100));
      end;
    end;
  end;

  if (fBaseType=btAlphatronMine) and (fAlphatronTransferBase<>0) then
  begin
    TransferAlphatron(fAlphatron,basis_api_GetBasisFromID(fAlphatronTransferBase));
  end;
end;

procedure TBasis.NextHour;
var
  Dummy  : Integer;
  Faktor : double;
  Ptr    : PEinrichtung;

  Ind    : Integer;
  RoomInd: Integer;
  Faehig : double;
  Delete : Boolean;
begin
  Ptr:=Addr(fBasis[0]);
  for Dummy:=0 to high(fBasis) do
  begin
    // Laufende Kosten ermitteln
    if not CanRoomUse(fBasis[Dummy]) then
      Faktor:=0
    else if RoomInUse(Dummy) then
      Faktor := 0.0045/100
    else
      Faktor := 0.00045/100;

    fRunningCosts:=fRunningCosts+(Ptr.KaufPreis*Faktor);

    // Schutzschilde aufladen
    if (Ptr.RoomTyp=rtDefenseShield) and (CanRoomUse(fBasis[Dummy])) then
    begin
      if Ptr.AktShieldPoints<>Ptr.ShieldStrength then
        Ptr.AktShieldPoints:=min(Ptr.ShieldStrength,Ptr.AktShieldPoints+round(Ptr.ShieldStrength*0.05));
    end;
    inc(Ptr);
  end;

  // Einrichtungen reparieren
  for Dummy:=high(fRepairList) downto 0 do
  begin
    Ind:=werkstatt_api_GetTechnikerIndex(fRepairList[Dummy].Techniker);
    Delete:=false;
    
    // Pr�fen, ob Techniker noch f�r die Reparatur eingeteilt ist
    if Ind<>-1 then
    begin
      if not werkstatt_api_GetRepairStatus(Ind) then
        Ind:=-1;
    end;

    if Ind=-1 then
    begin
      fRepairList[Dummy].Techniker:=werkstatt_api_FindTechnikerForRepair(fID);

      if fRepairList[Dummy].Techniker<>0 then
      begin
        Ind:=werkstatt_api_GetTechnikerIndex(fRepairList[Dummy].Techniker);
        werkstatt_api_SetRepairStatus(Ind);
      end;
    end;

    if Ind<>-1 then
    begin
      RoomInd:=GetRoomIndex(fRepairList[Dummy].RoomID);

      if RoomInd<>-1 then
      begin
        Faehig:=werkstatt_api_GetTechniker(Ind).Strength;
        // Pro 4 Faehigkeitenpunkt wird ein Prozent der Hitpoints am Tag wieder hergestellt
        Faehig:=(Faehig/400)/24;

        fBasis[RoomInd].AktHitPoints:=min(fBasis[RoomInd].HitPoints,fBasis[RoomInd].AktHitPoints+round(fBasis[RoomInd].Hitpoints*Faehig));
        if fBasis[RoomInd].AktHitPoints=fBasis[RoomInd].HitPoints then
          Delete:=true;
      end
      else
        Delete:=true;
    end;

    if Delete then
    begin
      // Auftrag l�schen
      if fRepairList[Dummy].Techniker<>0 then
        werkstatt_api_FreeTechniker(werkstatt_api_GetTechnikerIndex(fRepairList[Dummy].Techniker,true));
        
      DeleteArray(Addr(fRepairList),TypeInfo(TArrayRepairRoom),Dummy);
    end;
  end;
end;

class function TBasis.ReadFromScriptString(ObjStr: String): TObject;
begin
  result:=basis_api_GetBasisFromID(Cardinal(StrToInt64(ObjStr)));
end;

procedure TBasis.RebuildBaseMap;
var
  Dummy: Integer;

  procedure SetRoom(Room: PEinrichtung; IndexInArray: Integer);
  var
    X,Y  : Integer;
    Dummy: Integer;
  begin
    CheckBorders(Room.Position.BuildFloor,0,high(fBaseMap));
    for X:=Room.Position.X to Room.Position.X+Room.Width-1 do
    begin
      for Y:=Room.Position.Y to Room.Position.Y+Room.Height-1 do
      begin
        CheckBorders(X,0,high(fBaseMap[Room.Position.BuildFloor]));
        CheckBorders(Y,0,high(fBaseMap[Room.Position.BuildFloor,X]));
        with fBaseMap[Room.Position.BuildFloor,X,Y] do
        begin
          Assert(Objekt=nil);
          Objekt:=Room;
          Index:=IndexInArray;
          TopLeft:=false;
        end;
      end;
    end;
    fBaseMap[Room.Position.BuildFloor,Room.Position.X,Room.Position.Y].TopLeft:=true;

    if Room.RoomTyp=rtLift then
    begin
      for Dummy:=0 to fBuildingFloors-1 do
      begin
        with fBaseMap[Dummy,Room.Position.X,Room.Position.Y] do
        begin
          Objekt:=Room;
          Index:=IndexInArray;
          TopLeft:=true;
        end;
      end;
    end;
    // Luftschleuse belegen
    if Room.RoomTyp=rtHangar then
    begin
      with fBaseMap[0,Room.Position.X,Room.Position.Y] do
      begin
        Objekt:=Room;
        Index:=IndexInArray;
        TopLeft:=true;
      end;
    end;
  end;

begin
  // Neu initsialisiern
  fBaseMap:=nil;
  SetLength(fBaseMap,fBuildingFloors,fBaseSize.cx,fBaseSize.cy);

  for Dummy:=0 to high(fBasis) do
    SetRoom(Addr(fBasis[Dummy]),Dummy);
end;

procedure TBasis.RecalcStaticAttributes;
var
  Dummy           : Integer;
  OldSensorWidth  : Integer;
  OldAbwehrRange  : Integer;
begin
  OldSensorWidth := fSensorWidth;
  OldAbwehrRange := fAbwehrRange;

  fAbwehrRange:=0;
  fSensorWidth:=0;
  fAlphatronStorage:=0;

  if BaseType=btAlphatronMine then
    fAlphatronStorage:=1000;

  fShieldPoints:=0;

  for Dummy:=0 to high(fBasis) do
  begin
    if CanRoomUse(fBasis[Dummy]) then
    begin
      if fBasis[Dummy].RoomTyp=rtDefense then
        fAbwehrRange:=max(fAbwehrRange,fBasis[Dummy].maxReichweite);

      if fBasis[Dummy].RoomTyp=rtDefenseShield then
        inc(fShieldPoints,fBasis[Dummy].ShieldStrength);

      if fBasis[Dummy].RoomTyp=rtSensor then
        fSensorWidth:=max(fSensorWidth,fBasis[Dummy].SensorWidth);

      inc(fAlphatronStorage,fBasis[Dummy].AlphatronStorage);
    end;
  end;

  RecalcAlphatronMiningStrength;

  fAlphatron:=min(fAlphatronStorage,fAlphatron);

  if (OldSensorWidth<>fSensorWidth) or (OldAbwehrRange<>fAbwehrRange) then
    earth_api_ChangeScanArea;
end;

function TBasis.RoomInUse(Index: Integer): Boolean;
begin
  if (Index<0) or (Index>=high(fBasis)) then
  begin
    result:=false;
    exit;
  end;

  result:=RoomInUse(fBasis[Index]);
end;

procedure TBasis.SaveToStream(Stream: TStream);
var
  Dummy  : Integer;
  Rec    : TExtRecord;
begin
  Rec:=TExtRecord.Create(TBasisRecord);
  Rec.SetString('Name',fName);
  Rec.SetCardinal('ID',fID);

  Rec.SetDouble('Position.X',fBasisPos.X);
  Rec.SetDouble('Position.Y',fBasisPos.Y);

  Rec.SetInteger('BuildingFloors',fBuildingFloors);
  Rec.SetInteger('BaseSize.X',fBaseSize.cX);
  Rec.SetInteger('BaseSize.Y',fBaseSize.cY);

  Rec.SetInteger('BaseType',Integer(fBaseType));

  Rec.SetInteger('Alphatron',fAlphatron);
  Rec.SetDouble('QuarryAlphatron',fQuarryAlphatron);

  Rec.SetCardinal('TransferBase',fAlphatronTransferBase);

  with Rec.GetRecordList('Einrichtungen') do
  begin
    for Dummy:=0 to high(fBasis) do
      Add(EinrichtungToRecord(fBasis[Dummy]));
  end;

  with Rec.GetRecordList('RepairList') do
  begin
    for Dummy:=0 to high(fRepairList) do
    begin
      with Add do
      begin
        SetCardinal('RoomID',fRepairList[Dummy].RoomID);
        SetCardinal('TechnikerID',fRepairList[Dummy].Techniker);
      end;
    end;
  end;

  Rec.SaveToStream(Stream);
  Rec.Free;
end;

procedure TBasis.SetBaseSize(Width, Height, BuildFloors: Integer);
begin
  fBuildingFloors:=BuildFloors;
  RebuildBaseMap;
end;

procedure TBasis.Vernichten;
begin
  fList.VernichteBasis(Self);
end;

function TBasis.WriteToScriptString: String;
begin
  result:=IntToStr(ID);
end;

procedure TBasis.QuarryAlphatron(Minuten: Integer);
var
  Quarried       : double;
  MiningPercent  : double;
  PerHour        : double;
begin
  if fBaseType<>btAlphatronMine then
    exit;

  Assert(Minuten>0);
  
  MiningPercent:=GetAlphatronMiningEffectiveness;
  PerHour:=fAlphatronPerHour*MiningPercent;
  Quarried:=(PerHour - (random*PerHour/2))/60*Minuten;
  fQuarryAlphatron:=fQuarryAlphatron+Quarried;

  if fQuarryAlphatron>1 then
  begin
    FreeAlphatron(trunc(fQuarryAlphatron));
    fQuarryAlphatron:=fQuarryAlphatron-trunc(fQuarryAlphatron);
  end;
end;

function TBasis.GetAlphatronPerHour: double;
begin
  result:=fAlphatronPerHour;
end;

function TBasis.NeedAlphatron(NeedAlphatron: Integer): Integer;
begin
  if NeedAlphatron < fAlphatron then
    result:=NeedAlphatron
  else
    result:=fAlphatron;

  dec(fAlphatron,result);
end;

procedure TBasis.FreeAlphatron(FreeAlphatron: Integer);
begin
  fAlphatron:=min(fAlphatronStorage,fAlphatron+FreeAlphatron);
end;

function TBasis.GetAutoTransferBase: TBasis;
begin
  if fAlphatronTransferBase<>0 then
    result:=basis_api_GetBasisFromID(fAlphatronTransferBase)
  else
    result:=nil;
end;

procedure TBasis.SetAutoTransferBase(const Value: TBasis);
var
  Old: TBasis;
begin
  if fAlphatronTransferBase<>0 then
  begin
    Old:=basis_api_GetBasisFromID(fAlphatronTransferBase);
    Old.NotifyList.RemoveEvent(fEBaseDestroy);
  end;

  if Value=nil then
  begin
    fAlphatronTransferBase:=0
  end
  else
  begin
    fAlphatronTransferBase:=Value.ID;

    fEBaseDestroy:=Value.NotifyList.RegisterEvent(EVENT_BASISFREE,TransferBaseDestroyed);
  end;
end;

procedure TBasis.TransferAlphatron(Alphatron: Integer; Basis: TBasis);
begin
  if Alphatron=0 then
    exit;
    
  Alphatron:=min(fAlphatron,Alphatron);
  
  raumschiff_api_DoAlphatronTransport(fBasisPos,Basis.BasisPos,Alphatron);
  dec(fAlphatron,Alphatron);
end;

procedure TBasis.RecalcAlphatronMiningStrength;
var
  Dummy: Integer;
begin
  fMiningStrength:=0;
  if BaseType<>btAlphatronMine then
    exit;

  for Dummy:=0 to werkstatt_api_GetTechnikerCount-1 do
  begin
    with werkstatt_api_GetTechniker(Dummy) do
    begin
      if (Self.fID=BasisID) and (AlphatronMining) then
        inc(fMiningStrength, trunc(Strength));
    end;
  end;
end;

function TBasis.GetAlphatronMiningEffectiveness: double;
begin
  result:=min(1,max(AlphatronMinMiningEffectiveness/100,fMiningStrength/AlphatronMaxMiningEffectiveness))
end;

procedure TBasis.BuildNewFloor;
begin
  if fBaseType<>btBase then
    exit;

  if fBuildingFloors=MaxFloors then
    exit;

  savegame_api_NeedMoney(FloorCosts[fBuildingFloors],kbBB,true);

  SetBaseSize(fBaseSize.cx,fBaseSize.cy,fBuildingFloors+1);
end;

procedure TBasis.DoTreffer(Schaden: Integer);
var
  Dummy      : Integer;
  Abs        : Integer;
  Destroyed  : Boolean;
begin
  // Angriffsnachricht anzeigen
  if fBaseAttackMessage<=0 then
    savegame_api_Message(Format(ST0503220003,[Name]),lmBaseAttacked);

  // N�chste Nachricht erst anzeigen, wenn 120 Minuten ruhe war
  fBaseAttackMessage:=120;

  // Schutzschilde verbrauchen
  Schaden:=UseShields(Schaden);

  // Der Schaden wurde komplett von den Schutzschilden absorbiert
  if Schaden=0 then
    exit;

  Destroyed:=false;
  for Dummy:=high(fBasis) downto 0 do
  begin
    Abs:=round(Schaden*random*(1/(Power(2,fBasis[Dummy].Position.BuildFloor))));
    dec(fBasis[Dummy].AktHitpoints,Abs);
    if fBasis[Dummy].AktHitpoints<0 then
    begin
      Destroyed:=true;

      if DestroyRoom(Dummy) then
        exit;
    end;
  end;

  if Destroyed then
    RebuildBaseMap;
end;

function TBasis.UseShields(Strength: Integer): Integer;
var
  Dummy  : Integer;
  Points : Integer;
begin
  result:=Strength;

  // Basis hat keine Schutzschilde
  if fShieldPoints=0 then
    exit;

  for Dummy:=0 to high(fBasis) do
  begin
    if fBasis[Dummy].RoomTyp=rtDefenseShield then
    begin
      Points:=min(fBasis[Dummy].AktShieldPoints,result);
      dec(result,Points);
      dec(fBasis[Dummy].AktShieldPoints,Points);
      if result=0 then
        exit;
    end;
  end;
end;

function TBasis.NeedBuildLift: Boolean;
var
  Dummy: Integer;
begin
  result:=false;
  if BaseType<>btBase then
    exit;

  for Dummy:=0 to high(fBasis) do
  begin
    if fbasis[Dummy].RoomTyp=rtLift then
      exit;
  end;
  result:=true;
end;

function TBasis.GenerateRoomID: Cardinal;
var
  ID     : Cardinal;
  IDOk   : Boolean;
  Dummy  : Integer;
begin
  repeat
    IDOk:=true;
    ID:=random(high(Cardinal));

    for Dummy:=0 to high(fBasis) do
    begin
      if ID=fBasis[Dummy].RoomID then
      begin
        IDOk:=false;
        continue;
      end;
    end;
  until IDOk;
  result:=ID;
end;

procedure TBasis.RepairRoom(RoomID, TechnikerID: Cardinal);
var
  Dummy: Integer;
begin
  if RoomInRepair(RoomID) then
    exit;
    
  for Dummy:=0 to high(fRepairList) do
  begin
    if fRepairList[Dummy].RoomID=RoomID then
    begin
      // Alten Techniker freistellen
      if fRepairList[Dummy].Techniker<>0 then
        werkstatt_api_FreeTechniker(werkstatt_api_GetTechnikerIndex(fRepairList[Dummy].Techniker));

      fRepairList[Dummy].Techniker:=TechnikerID;

      // Neuen Techniker f�r die Reparatur einteilen
      if fRepairList[Dummy].Techniker<>0 then
        werkstatt_api_SetRepairStatus(werkstatt_api_GetTechnikerIndex(TechnikerID));

      exit;
    end;
    if (fRepairList[Dummy].Techniker=TechnikerID) and (TechnikerID<>0) then
      exit;

  end;
  SetLength(fRepairList,length(fRepairList)+1);

  fRepairList[high(fRepairList)].RoomID:=RoomID;

  if TechnikerID=0 then
    TechnikerID:=werkstatt_api_FindTechnikerForRepair(fID);

  if TechnikerID<>0 then
    werkstatt_api_SetRepairStatus(werkstatt_api_GetTechnikerIndex(TechnikerID));

  fRepairList[high(fRepairList)].Techniker:=TechnikerID;

end;

function TBasis.GetRoomIndex(ID: Cardinal): Integer;
var
  Dummy: Integer;
begin
  result:=-1;
  for Dummy:=0 to high(fBasis) do
  begin
    if fBasis[Dummy].RoomID=ID then
    begin
      result:=Dummy;
      exit;
    end;
  end;
end;

procedure TBasis.RepairAllRooms;
var
  Dummy: Integer;
begin
  for Dummy:=0 to high(fBasis) do
  begin
    if fBasis[Dummy].AktHitPoints<fBasis[Dummy].Hitpoints then
      RepairRoom(fBasis[Dummy].RoomID);
  end;
end;

function TBasis.RoomInRepair(RoomID: Cardinal): Boolean;
var
  Dummy: Integer;
  Index: Integer;
begin
  result:=false;
  for Dummy:=0 to high(fRepairList) do
  begin
    if fRepairList[Dummy].RoomID=RoomID then
    begin
      if fRepairList[Dummy].Techniker<>0 then
      begin
        Index:=werkstatt_api_GetTechnikerIndex(fRepairList[Dummy].Techniker);
        if Index=-1 then
          exit;

        result:=werkstatt_api_GetRepairStatus(Index);
      end
      else
        result:=true;
      exit;
    end;
  end;
  result:=false;
end;

function TBasis.GetRoomImage(const Room: TEinrichtung; X, Y,
  BuildFloor: Integer): String;
var
  Connection: String;
begin
  if Room.RoomTyp = rtFloor then
  begin
    Connection:='----';
    if (X>=0) and (Y>=0) and (X<fBaseSize.cx) and (Y<fBaseSize.cy) then
    begin
      if (X>0) and (fBaseMap[BuildFloor,X-1,Y].Objekt<>nil) then
        Connection[1]:='l';

      if (Y>0) and (fBaseMap[BuildFloor,X,Y-1].Objekt<>nil) then
        Connection[3]:='u';

      if (X<fBaseSize.cx-1) and (fBaseMap[BuildFloor,X+1,Y].Objekt<>nil) then
        Connection[2]:='r';

      if (Y<fBaseSize.cy-1) and (fBaseMap[BuildFloor,X,Y+1].Objekt<>nil) then
        Connection[4]:='d';
    end;

    result:='default\BasisBau:Floor'+Connection;
  end
  else
  begin
    if BuildFloor = 0 then
      result:=room.ImageBase
    else
      result:=room.ImageUnder;
  end;
end;

function TBasis.DestroyRoom(RoomIndex: Integer): Boolean;
var
  Text: String;
  Room: TEinrichtung;

  procedure RelocateRooms;
  var
    NewLagerRoom: double;
  begin
    // Lagerraum neu verteilen
    if Room.LagerBelegt>0 then
    begin
      NewLagerRoom:=max(0,min(LagerRaum-LagerBelegt,Room.LagerBelegt));
      Assert(NeedLagerRoom(NewLagerRoom));
      Room.LagerBelegt:=Room.LagerBelegt-NewLagerRoom;
    end;

    // Quartiere neu verteilen
    while (Room.WohnBelegt>0) and (NeedWohnRoom) do
      dec(Room.WohnBelegt);

    // Labore neu verteilen
    while (Room.LaborBelegt>0) and (NeedLaborRoom) do
      dec(Room.LaborBelegt);

    // Werken neu verteilen
    while (Room.WerkBelegt>0) and (NeedWerkRoom) do
      dec(Room.WerkBelegt);

    // Hangar neu verteilen
    while (Room.SmallBelegt>0) and (NeedHangar) do
      dec(Room.SmallBelegt);
  end;

  // T�tet Soldaten, die nicht mehr in der Basis unterkommen, weil zu wenig Platz ist
  procedure FreeQuartiere;
  const                                                 
    KillProcedures : Array[0..2] of TKillProcedure = (forsch_api_KillRandomForschInBase,
                                                      soldaten_api_KillRandomSoldatInBase,
                                                      werkstatt_api_KillRandomTechnikerInBase);
  var
    Name: String;
  begin
    while Room.WohnBelegt>0 do
    begin
      if KillProcedures[random(3)](fID,Name) then
      begin
        Text:=Text+#13#10+Format(ST0503060002,[Name]);
        dec(Room.WohnBelegt);
      end;
    end;
  end;

  procedure FreeLagerRoom;
  begin
    Text:=Text+#13#10+lager_api_DestroyItems(fID,Room.LagerBelegt);
  end;

  procedure FreeLaborRoom;
  begin
    Text:=Text+#13#10+forsch_api_CheckLaborRoom(fID);
  end;

  procedure FreeWerkRoom;
  begin
    Text:=Text+#13#10+werkstatt_api_CheckWerkRoom(fID);
  end;

  procedure FreeHangarRoom;
  var
    Text2: String;
  begin
    while Room.SmallBelegt>0 do
    begin
      Assert(raumschiff_api_DestroyRandomShip(fID,Text2));
      Text:=Text+#13#10+Text2;
      dec(Room.SmallBelegt);
    end;
  end;

begin
  result:=false;

  Room:=fBasis[RoomIndex];
  DeleteArray(Addr(fBasis),TypeInfo(TEinrichtungArray),RoomIndex);

  if RoomInUse(Room) then
  begin
    RelocateRooms;

    Text:=Format(ST0503060001,[Room.Name,fName])+#13#10#13#10;

    // Wenn noch Quartiere �brig sind, m�ssen Soldaten/Techniker/Wissenschaftler dran glauben
    if Room.WohnBelegt>0 then
      FreeQuartiere;

    if Room.LagerBelegt>0 then
      FreeLagerRoom;

    if Room.LaborBelegt>0 then
      FreeLaborRoom;

    if Room.WerkBelegt>0 then
      FreeWerkRoom;

    if Room.SmallBelegt>0 then
      FreeHangarRoom;

    savegame_api_Message(NormalizeText(Text),lmBaseAttacked,Self);

  end;

  // Wenn der F�rderturm zerst�rt wird, gilt das gesamte Alphatronbergwerk als zerst�rt
  if Room.RoomTyp=rtGearHead then
  begin
    Vernichten;
    result:=true;
  end
  else
  begin
    RebuildBaseMap;
    RecalcStaticAttributes;
  end;
end;

function TBasis.RoomInUse(const Room: TEinrichtung): Boolean;
begin
  result:=false;
  if not CanRoomUse(Room) then
    exit;

  if (Room.RoomTyp in [rtDefense,rtDefenseShield,rtSensor]) then
  begin
    result:=true;
    exit;
  end;

  result:=(Room.LagerBelegt>0) or (Room.WohnBelegt>0)
    or (Room.LaborBelegt>0) or (Room.WerkBelegt>0)
    or (Room.SmallBelegt>0);
end;

procedure TBasis.OptimizeFloors;
var
  Floor,X,Y: Integer;
begin
  for Floor:=1 to fBuildingFloors-1 do
  begin
    for Y:=0 to fBaseSize.cy-1 do
    begin
      for X:=0 to fBaseSize.cx-1 do
      begin
        if (fBaseMap[Floor,X,Y].Objekt<>nil) and (fBaseMap[Floor,X,Y].Objekt.RoomTyp=rtFloor) then
        begin
          if CanSellRoom(fBaseMap[Floor,X,Y].Index)=csrCanSell then
            SellRoom(fBaseMap[Floor,X,Y].Index);
        end;
      end;
    end;
  end;
end;

procedure TBasis.NextMinute(Minuten: Integer);
begin
  QuarryAlphatron(Minuten);

  if fBaseAttackMessage>0 then
    dec(fBaseAttackMessage,Minuten);
end;

procedure TBasis.DeleteEinrichtung(RoomID: Cardinal);
var
  Dummy: Integer;
begin
  for Dummy:=0 to high(fBasis) do
  begin
    if RoomID=fBasis[Dummy].RoomID then
    begin
      DeleteArray(Addr(fBasis),TypeInfo(TEinrichtungArray),Dummy);
      exit;
    end;
  end;
  Assert(false);
end;

procedure TBasis.DeleteFloors(BuildFloor,X, Y, Width, Height: Integer);
var
  XDum,YDum: Integer;
begin
  for XDum:=X to X+Width-1 do
  begin
    for YDum:=Y to Y+Height-1 do
    begin
      if fBaseMap[BuildFloor,XDum,YDum].Objekt<>nil then
      begin
        Assert(fBaseMap[BuildFloor,XDum,YDum].Objekt.RoomTyp=rtFloor);
        DeleteEinrichtung(fBaseMap[BuildFloor,XDum,YDum].Objekt.RoomID);

        RebuildBaseMap;
      end;
    end;
  end;
end;

procedure TBasis.AddHeadGear;
var
  Room: TEinrichtung;
begin
  FillChar(Room,sizeOf(Room),#0);
  Room.Name:=RoomTypNames[rtGearHead];
  Room.ImageBase:=BackGroundAlphatron;
  Room.Hitpoints:=20000;
  Room.AktHitpoints:=20000;
  Room.Position.BuildFloor:=0;
  Room.Position.X:=1;
  Room.Position.Y:=1;
  Room.Width:=4;
  Room.Height:=4;
  Room.RoomTyp:=rtGearHead;
  SetLength(fBasis,1);
  fBasis[0]:=Room;
end;

procedure TBasis.NotEnoughUpKeepExpense(Damage: double);
var
  Dummy: Integer;
begin
  for Dummy:=0 to high(fBasis) do
  begin
    if fBasis[Dummy].Hitpoints>0 then
    begin
      dec(fBasis[Dummy].AktHitPoints,round(fBasis[Dummy].AktHitPoints*Damage));

      // Einrichtung nicht durch Wartungsmangel zerst�ren
      fBasis[Dummy].AktHitPoints:=max(1,fBasis[Dummy].AktHitPoints);
    end;
  end;
end;

procedure TBasis.TransferBaseDestroyed(Sender: TObject);
begin
  Assert(Sender is TBasis);
  if fAlphatronTransferBase=TBasis(Sender).ID then
    fAlphatronTransferBase:=0;
end;

procedure TBasis.CreateStartBase;
var
  BuildOK: Boolean;
  Trys   : Integer;

  procedure AddStartRooms;
  var
    Dummy   : Integer;
    Anzahl  : Integer;
    Project : TForschProject;
  begin
    // Aufzug zur Basis hinzuf�gen
    AddEinrichtung(fList.Einrichtung[0]);

    for Dummy:=0 to fList.Einrichtungen-1 do
    begin
      if fList.Einrichtung[Dummy].ID=0 then
        continue;

      Project:=forsch_api_GetProject(fList.Einrichtung[Dummy].ID);
      if Project.Anzahl>0 then
      begin
        Anzahl:=Project.Anzahl;

        while Anzahl>0 do
        begin
          AddEinrichtung(fList.Einrichtung[Dummy]);
          dec(Anzahl);
        end;
      end;
    end;
  end;

begin
  BuildOK:=false;

  Trys:=20;
  repeat
    try
      SetLength(fBasis,0);
      AddStartRooms;
      BuildOK:=true;
    except
      dec(Trys);
      if Trys<0 then
        raise Exception.Create('Nicht genug Platz in der Basis');
    end;
  until BuildOK;

  OptimizeFloors;
end;

initialization;

  TBasisListRecord:=TExtRecordDefinition.Create('TBasisListRecord');
  TBasisListRecord.AddRecordList('Einrichtungen',RecordEinrichtung);
  TBasisListRecord.AddInteger('BasisCount',0,high(Integer));
  TBasisListRecord.AddBinary('AlphatronMap');

  TBasisRecord:=TExtRecordDefinition.Create('TBasisRecord');
  TBasisRecord.AddRecordList('Einrichtungen',RecordEinrichtung);
  TBasisRecord.AddString('Name',50);
  TBasisRecord.AddCardinal('ID',0,high(Cardinal));
  TBasisRecord.AddDouble('Position.X',1,359,10);
  TBasisRecord.AddDouble('Position.Y',1,179,10);
  TBasisRecord.AddInteger('BaseSize.X',0,50,10);
  TBasisRecord.AddInteger('BaseSize.Y',0,50,10);
  TBasisRecord.AddInteger('BuildingFloors',0,MaxFloors,1);
  TBasisRecord.AddInteger('BaseType',0,Integer(high(TBaseType)));

  TBasisRecord.AddInteger('Alphatron',0,high(Integer),0);
  TBasisRecord.AddDouble('QuarryAlphatron',0,10000,5);
  TBasisRecord.AddCardinal('TransferBase',0,high(Cardinal),0);

  TRepairListRecord:=TExtRecordDefinition.Create('TRepairListRecord');
  TRepairListRecord.AddCardinal('RoomID',0,high(Cardinal));
  TRepairListRecord.AddCardinal('TechnikerID',0,high(Cardinal));

  TBasisRecord.AddRecordList('RepairList',TRepairListRecord);
end.


