{********************************************************************************
*										*
* Copyright (C) 2004  X-Force Team - jim_raynor@web.de				*
*										*
* This file is part of X-Force: Fight For Destiny (X-Force).			*
*										*
* X-Force is free software; you can redistribute it and/or modify		*
* it under the terms of the GNU General Public License as published by		*
* the Free Software Foundation; either version 2 of the License, or		*
* (at your option) any later version.						*
*										*
* X-Force is distributed in the hope that it will be useful,			*
* but WITHOUT ANY WARRANTY; without even the implied warranty of		*
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the			*
* GNU General Public License for more details.					*
*										*
* You should have received a copy of the GNU General Public License		*
* along with X-Force; if not, write to the Free Software			*
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA	*
*										*
*********************************************************************************
*										*
* Diese Unit stellt einen Button zur Verf�gung					*
*										*
********************************************************************************}

{$I ../Settings.inc}

unit DXBitmapButton;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  DXContainer, KD4Utils, Defines, DXDraws, TraceFile, Blending, XForce_types, NGTypes,
  DirectDraw,DirectFont;

type
  TButtonState=(bsNormal,bsOver,bsDown);

  TDXBitmapButton = class(TDXComponent)
  private
    fFirst       : TColor;
    fState       : TButtonState;
    fCaption     : TCaption;
    fOldHotKey   : Char;
    fSecond      : TBlendColor;
    fAccColor    : TColor;
    fSound       : boolean;
    fEscape      : boolean;
    fPageIndex   : Integer;
    fBlendColor  : TBlendColor;
    fDisabledCol : TBlendColor;
    fTextWidth   : Integer;
    fTextHeight  : Integer;
    fHighLight   : boolean;
    fCorners     : TRoundCorners;
    fCorner      : TCorners;
    fAlpha       : Integer;
    AValue       : Integer;
    fOwnerDraw   : TDXDrawEvent;
    fCornerWidth : Integer;
    fInDoClick   : Boolean;
    fNormalFont  : TDirectFont;
    fAccelFont   : TDirectFont;
    procedure SetFirstColor(const Value: TColor);
    procedure SetCaption(const Value: TCaption);
    procedure SetSecondColor(const Value: TColor);
    procedure SetAccColor(const Value: TColor);
    procedure SetEscape(const Value: boolean);
    procedure SetBlendColor(const Value: TBlendColor);
    procedure CalcWidth(Sender: TObject);
    procedure SetHighLight(const Value: boolean);
    function GetFrameRect: TRect;
    function AddValue(Sender: TObject;Frames: Integer): boolean;
    function DeleteValue(Sender: TObject;Frames: Integer): boolean;
    procedure SetCorners(const Value: TRoundCorners);
    procedure SetCornerWidth(const Value: Integer);
    { Private-Deklarationen }
  protected
    procedure NeedRedraw(Sender: TObject);
    procedure CreateMouseRegion;override;
    { Protected-Deklarationen }
  public
    constructor Create(Page: TDXPage);override;
    destructor destroy;override;
    procedure VisibleChange;override;
    procedure Draw(Surface: TDirectDrawSurface;var Mem: TDDSurfaceDesc);override;
    procedure ReDrawRect(DrawRect: TRect;Surface: TDirectDrawSurface;var Mem: TDDSurfaceDesc);override;
    procedure DoClick;override;
    procedure MouseOver;override;
    procedure MouseLeave;override;
    procedure MouseDown(Button: TMouseButton;X,Y: Integer);override;
    procedure MouseUp(Button: TMouseButton;X,Y: Integer);override;
    procedure MouseMove(X,Y: Integer);override;
    property FirstColor     : TColor read fFirst write SetFirstColor;
    property SecondColor    : TColor write SetSecondColor;
    property Text           : TCaption read fCaption write SetCaption;
    property HotKey         : Char read fOldHotKey;
    property AccerlateColor : TColor read fAccColor write SetAccColor;
    property Sound          : boolean read fSound write fSound;
    property EscButton      : boolean read fEscape write SetEscape;
    property PageIndex      : Integer read fPageIndex write fPageIndex;
    property BlendColor     : TBlendColor read fBlendColor write SetBlendColor;
    property HighLight      : boolean read fHighLight write SetHighLight;
    property RoundCorners   : TRoundCorners read fCorners write SetCorners;
    property CornerWidth    : Integer read fCornerWidth write SetCornerWidth;
    property BlendAlpha     : Integer read fAlpha write fAlpha;
    property OnOwnerDraw    : TDXDrawEvent read fOwnerDraw write fOwnerDraw;
  end;

implementation

{ TDXBitmapButton }

constructor TDXBitmapButton.Create(Page: TDXPage);
begin
  inherited;
  CornerWidth:=11;
  fState:=bsNormal;
  fOwnerDraw:=nil;
  AValue:=0;
  fOldHotKey:=#0;
  fAlpha:=75;
  fSound:=true;
  fFirst:=bcMaroon;
  fBlendColor:=bcMaroon;
  Font.OnChange:=CalcWidth;
  fPageIndex:=-1;
  RoundCorners:=rcAll;
  fDisabledCol:=Container.Surface.ColorMatch($00303030);
  fInDoClick:=false;
end;

destructor TDXBitmapButton.destroy;
begin
  Container.DeleteFrameFunction(AddValue,Self);
  Container.DeleteFrameFunction(DeleteValue,Self);
  Font.OnChange:=nil;
  inherited;
end;

procedure TDXBitmapButton.Draw(Surface: TDirectDrawSurface;var Mem: TDDSurfaceDesc);
begin
  ReDrawRect(ClientRect,Surface,Mem);
end;

procedure TDXBitmapButton.DoClick;
begin
  // Dieses Workaround verhinder, dass �ber einen Hotkey der Button mehrmals 'gedr�ckt' wird
  if fInDoClick then
    exit;
  if (not Visible) or (not Enabled) then exit;
  if fSound then Container.PlaySound(SClick);
  fInDoClick:=true;
  try
    if not Assigned(OnClick) and (fPageIndex<>-1) then Parent.ChangePage(PageIndex);
    inherited;
  finally
    fInDoClick:=false;
  end;
end;

procedure TDXBitmapButton.MouseDown(Button: TMouseButton; X, Y: Integer);
begin
  inherited;
  if Button<>mbLeft then exit;
  fState:=bsDown;
  if Visible then Redraw;
end;

procedure TDXBitmapButton.MouseLeave;
var
  OState: TButtonState;
begin
  inherited;
  OState:=fState;
  fState:=bsNormal;
  if AlphaElements then
  begin
    Container.DeleteFrameFunction(AddValue,Self);
    Container.AddFrameFunction(DeleteValue,Self,25);
    if OState=bsDown then Redraw;
  end
  else Redraw;
end;

procedure TDXBitmapButton.MouseOver;
begin
  inherited;
  if fSound then Container.PlaySound(SOver);
  if (GetKeyState(VK_LBUTTON)<0) then fState:=bsDown else fState:=bsOver;
  if AlphaElements then
  begin
    Container.DeleteFrameFunction(DeleteValue,Self);
    Container.AddFrameFunction(AddValue,Self,25);
  end
  else
  Redraw;
end;

procedure TDXBitmapButton.MouseUp(Button: TMouseButton; X, Y: Integer);
begin
  if Button<>mbLeft then exit;
  if (X<0) or (Y<0) or (X>Width) or (Y>Height) then fState:=bsNormal else fState:=bsOver;
  inherited;
  Redraw;
end;

procedure TDXBitmapButton.NeedRedraw(Sender: TObject);
begin
  if Visible then Redraw;
end;

procedure TDXBitmapButton.SetAccColor(const Value: TColor);
var
  FontColor: TColor;
begin
  fAccColor := Value;
  Font.OnChange:=nil;
  FontColor:=Font.Color;
  Font.Color:=Value;
  fAccelFont:=FontEngine.FindDirectFont(Font,clBlack);
  Font.Color:=FontColor;
  Font.OnChange:=CalcWidth;
  if Visible then Redraw;
end;

procedure TDXBitmapButton.SetCaption(const Value: TCaption);
var
  NewHotKey: Char;
begin
  if fCaption=Value then exit;
  NewHotKey:=GetShortCut(Value);
  if NewHotKey<>fOldHotKey then
  begin
    if fOldHotKey<>#0 then Parent.DeleteHotKey(fOldHotKey,Self);
    if NewHotKey<>#0 then Parent.RegisterHotKey(NewHotKey,Self);
    fOldHotKey:=NewHotKey;
  end;
  // Ersetzen des & und einschlie�en in #2 und #1 f�r Dynamic Font
  if NewHotKey<>#0 then
    fCaption:=copy(Value,1,Pos('&',Value)-1)+#2+copy(Value,Pos('&',Value)+1,1)+#1+copy(Value,Pos('&',Value)+2,200)
  else
    fCaption:=Value;
  CalcWidth(Self);
  if Visible then Redraw;
end;

procedure TDXBitmapButton.SetFirstColor(const Value: TColor);
begin
  fFirst := Container.Surface.ColorMatch(Value);
  if Visible then Redraw;
end;

procedure TDXBitmapButton.SetSecondColor(const Value: TColor);
begin
  fSecond := Container.Surface.ColorMatch(Value);
  if Visible then Redraw;
end;

procedure TDXBitmapButton.SetEscape(const Value: boolean);
begin
  if fEscape<>Value then
  begin
    fEscape := Value;
    if Value then Parent.RegisterHotKey(#27,Self)
    else Parent.DeleteHotKey(#27,Self);
  end;
end;

procedure TDXBitmapButton.VisibleChange;
begin
  inherited;
  fState:=bsNormal;
end;

procedure TDXBitmapButton.SetBlendColor(const Value: TBlendColor);
begin
  fBlendColor := Value;
  Redraw;
end;

procedure TDXBitmapButton.CalcWidth(Sender: TObject);
begin
  fNormalFont:=FontEngine.FindDirectFont(Self.Font,clBlack);
  fTextWidth:=fNormalFont.TextWidth(fCaption) shr 1;
  fTextHeight:=fNormalFont.TextHeight(fCaption) shr 1;
end;

procedure TDXBitmapButton.SetHighLight(const Value: boolean);
begin
  if fHighLight=Value then exit;
  fHighLight := Value;
  Redraw;
end;

procedure TDXBitmapButton.ReDrawRect(DrawRect: TRect; Surface: TDirectDrawSurface;var Mem: TDDSurfaceDesc);
var
  TempRect : TRect;
begin
  IntersectRect(DrawRect,DrawRect,Rect(Left,Top,Right,Bottom));
  if DrawEnabled then
  begin
    if AlphaElements then
      BlendRoundRect(ClientRect,fAlpha+AValue,fBlendColor,Surface,Mem,fCornerWidth,fCorner,DrawRect);
    if fHighLight then
    begin
      if AlphaElements then
      begin
        IntersectRect(TempRect,ResizeRect(ClientRect,7),DrawRect);
        BlendRectangle(TempRect,160,fBlendColor,Surface,Mem);
        Blending.Rectangle(Surface,Mem,ResizeRect(ClientRect,6),fSecond);
      end
      else
      begin
        TempRect:=ResizeRect(ClientRect,7);
        Blending.Rectangle(Surface,Mem,ResizeRect(ClientRect,6),fSecond);
      end;
    end;
    if fState=bsOver then
    begin
      if not AlphaElements then
        Blending.Rectangle(Surface,Mem,ResizeRect(ClientRect,5),fFirst);
    end
    else if fState=bsDown then
    begin
      Blending.Rectangle(Surface,Mem,ResizeRect(ClientRect,4),fSecond);
      Blending.Rectangle(Surface,Mem,ResizeRect(ClientRect,6),fSecond);
    end;
    FramingRect(Surface,Mem,ClientRect,fCorner,fCornerWidth,fFirst);
    if fHighLight and (not AlphaElements) then
      Surface.FillRect(TempRect,fBlendColor);
    if Assigned(fOwnerDraw) then
      fOwnerDraw(Self,Surface,Mem)
    else
    begin
      FontEngine.DynamicText(Surface,Left+((Width shr 1)-fTextWidth),Top+((Height shr 1)-fTextHeight)-1,fCaption,[fNormalFont,fAccelFont]);
    end;
//      AccerlateTextOut(Surface,Font,Left+((Width shr 1)-fTextWidth),Top+((Height shr 1)-fTextHeight)-1,fCaption,fAccColor);
  end
  else
  begin
    if AlphaElements then
      BlendRoundRect(ClientRect,125,bcGray,Surface,Mem,fCornerWidth,fCorner,DrawRect);
    FramingRect(Surface,Mem,ClientRect,fCorner,fCornerWidth,fDisabledCol);
    DisabledStdFont.Draw(Surface,Left+((Width shr 1)-fTextWidth),Top+((Height shr 1)-fTextHeight)-1,StripHotKey(fCaption));
  end;
end;

procedure TDXBitmapButton.MouseMove(X, Y: Integer);
begin
  inherited;
  if GetKeyState(VK_LBUTTON)<0 then
  begin
    if not PtInRect(Rect(0,0,Width,Height),Point(X,Y)) then
    begin
      if fState=bsNormal then exit;
      fState:=bsNormal;
      Redraw;
    end
    else
    begin
      if fState=bsDown then exit;
      fState:=bsDown;
      Redraw;
    end;
  end;
end;

function TDXBitmapButton.GetFrameRect: TRect;
begin
  result:=ClientRect;
  if fCorners in [rcLeft,rcLeftTop,rcLeftBottom,rcNone] then inc(result.Right,20);
  if fCorners in [rcTop,rcLeftTop,rcRightTop,rcNone] then inc(result.Bottom,20);
  if fCorners in [rcRight,rcRightTop,rcRightBottom,rcNone] then dec(result.Left,20);
  if fCorners in [rcBottom,rcLeftBottom,rcRightBottom,rcNone] then dec(result.Top,20);
end;

procedure TDXBitmapButton.SetCorners(const Value: TRoundCorners);
begin
  fCorners := Value;
  fCorner:=[];
  if fCorners in [rcLeft,rcTop,rcLeftTop,rcAll] then include(fCorner,cLeftTop);
  if fCorners in [rcRight,rcTop,rcRightTop,rcAll] then include(fCorner,cRightTop);
  if fCorners in [rcLeft,rcBottom,rcLeftBottom,rcAll] then include(fCorner,cLeftBottom);
  if fCorners in [rcRight,rcBottom,rcRightBottom,rcAll] then include(fCorner,cRightBottom);
  RecreateMouseRegion;
  Redraw;
end;

function TDXBitmapButton.AddValue(Sender: TObject;Frames: Integer): boolean;
var
  Button: TDXBitmapButton;
begin
  Result:=true;
  Button:=Self;//Sender as TDXBitmapButton;
  inc(Button.AValue,10*Frames);
  if Button.AValue>=100 then
  begin
    Button.AValue:=100;
    Container.DeleteFrameFunction(AddValue,Button);
    Button.Redraw;
  end
  else
  begin
    Button.Redraw;
    result:=true;
  end;
end;

function TDXBitmapButton.DeleteValue(Sender: TObject;Frames: Integer): boolean;
var
  Button: TDXBitmapButton;
begin
  Result:=true;
  Button:=Self;//Sender as TDXBitmapButton;
  dec(Button.AValue,10*Frames);
  if Button.AValue<=0 then
  begin
    Button.AValue:=0;
    Container.DeleteFrameFunction(DeleteValue,Button);
    Button.Redraw;
  end
  else
  begin
    Button.Redraw;
    result:=true;
  end;
end;

procedure TDXBitmapButton.CreateMouseRegion;
var
  FrameRect: TRect;
  RoundRGN : HRGN;
begin
  FrameRect:=GetFrameRect;
  RoundRGN:=CreateRoundRectRgn(FrameRect.Left,FrameRect.Top,FrameRect.Right+1,FrameRect.Bottom+1,(fCornerWidth-1) shl 1,(fCornerWidth-1) shl 1);
  fMouseRegion:=CreateRectRgn(Left,Top,Right,Bottom);
  CombineRgn(fMouseRegion,fMouseRegion,RoundRGN,RGN_AND);
  DeleteObject(RoundRGN);
end;

procedure TDXBitmapButton.SetCornerWidth(const Value: Integer);
begin
  if CornerWidth=Value then exit;
  fCornerWidth := Value;
  RecreateMouseRegion;
end;

end.
