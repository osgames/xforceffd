{********************************************************************************
*										*
* Copyright (C) 2004  X-Force Team - jim_raynor@web.de				*
*										*
* This file is part of X-Force: Fight For Destiny (X-Force).			*
*										*
* X-Force is free software; you can redistribute it and/or modify		*
* it under the terms of the GNU General Public License as published by		*
* the Free Software Foundation; either version 2 of the License, or		*
* (at your option) any later version.						*
*										*
* X-Force is distributed in the hope that it will be useful,			*
* but WITHOUT ANY WARRANTY; without even the implied warranty of		*
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the			*
* GNU General Public License for more details.					*
*										*
* You should have received a copy of the GNU General Public License		*
* along with X-Force; if not, write to the Free Software			*
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA	*
*										*
*********************************************************************************
*										*
* Stellt die Komponente zum Ausr�sten der Soldaten zur Verf�gung		*
*										*
********************************************************************************}

{$I ../Settings.inc}

unit DXSoldatConfig;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  DXContainer, DXDraws, DXClass, Blending, DirectDraw, ArchivFile, Defines,
  XForce_types, GameFigureManager, NGTypes, RaumschiffList, KD4Utils, DXScrollBar,
  LagerListe, DirectFont, math, TraceFile, DXItemInfo, DXCanvas, typinfo,
  DXSoldatChooser, ISOObjects, StringConst;

const
  SlotPadding    = 4;
  ItemHeight     = 50;
  SlotSize       = 36;
  XSlotOffSet    = 16;
  ImageMargin    = 1;
  LeftSplitter   = 225;
  FigureWidth    = 282;
  RightSplitter  = LeftSplitter+FigureWidth;
  FigureCenter   = LeftSplitter+(FigureWidth div 2);

type
  TItemObjectArray = Array of TISOItem;

  TRaumschiffObjectInfo = record
    Raumschiff  : TRaumschiff;
    ID          : Cardinal;
    Count       : Integer;
  end;

  TBasisObjectInfo = record
    Basis       : Cardinal;
    ID          : Cardinal;
    Add         : Boolean;
    Schuesse    : Integer;
  end;

  TDXSoldatConfig = class(TDXComponent)
  private
    fSoldatSurface  : TDirectDrawSurface;
    fManager        : TGameFigureManager;
    fRaumschiff     : TRaumschiff;
    fItemInfo       : TDXPopupItemInfo;
    fDownIndex      : Integer;         // Dieser Slot wurde angeklickt
    fOverIndex      : Integer;
    fItemSlots      : Array of TItemSlot;
    fFromSlots      : Array of TItemSlot;
    fScrollBar      : TDXScrollBar;
    fDragnDropCan   : TDXCanvas;
    fDragnDropMode  : boolean;
    fDragnDropSlot  : TItemSlot;
    fDragonSlot     : Integer;
    fErrorText      : String;
    fShowTime       : Integer;
    fDragEnd        : TNotifyEvent;
    fChooser        : TDXSoldatChooser;
    fComun          : TConfigCommunication;
    fCaptions       : Array of record
                        Text  : String;
                        Draw  : Boolean;
                        Top   : Integer;
                        Obj   : TObject;
                        Level : Integer;
                      end;
    fUseTimeUnits   : Boolean;

    // Die Arrays werden ben�tigt, um den Status zur�ckzudrehen, falls
    // nicht etwas schief geht

    // Items, die auf den Boden abgelegt wurden
    fNewBodenObjects     : TItemObjectArray;
    // Items, die vom Boden aufgehoben wurden
    fDeletedBodenObjects : TItemObjectArray;
    // Ausr�stung die in eine Basis kommt / aus einer Basis entfernt wird
    fChangedBasisObjects : Array of TBasisObjectInfo;
    // Ausr�stung die aus einem Raumschiff entfernt wird / bzw. entfernt wird
    fChangedSchiffObjects: Array of TRaumschiffObjectInfo;

    procedure SetManager(const Value: TGameFigureManager);

    procedure EnumItems;

    procedure OnScrollItems(Sender: TObject);
    procedure DrawDragnDropCanvas(Sender: TDXComponent;Surface: TDirectDrawSurface;var Mem: TDDSurfaceDesc);

    procedure DeleteFromSlot(const FromSlot: TItemSlot);
    procedure AddFromSlot(const FromSlot, ToSlot: TItemSlot);

    procedure CorrectFromSlots;
    function SortFrom(Index1,Index2: Integer;Typ: TFunctionType): Integer;

    function CountDownErrorText(Sender: TObject;Frames: Integer): boolean;

    function CheckPlacable(FromSlot, ToSlot: TItemSlot): Boolean;
    function SetSlot(FromSlot, ToSlot: TItemSlot): boolean;
    procedure DoAction(FromSlot, ToSlot: TItemSlot);

    procedure RestoreExternalAction;
    procedure DoExternalAction;

    function MustUseTimeUnits: Boolean;

    function GetTransferUnit(X,Y: Integer): TGameFigureManager;
    procedure TransferSlot(const Slot: TItemSlot; ToManager: TGameFigureManager);

    procedure BeginDragnDrop;
    procedure CancelDragnDrop;

    procedure ChangeSchiffObject(Schiff: TRaumschiff; ItemID: Cardinal; ItemCount: Integer);
    procedure ChangeBasisObject(BasisID: Cardinal; ItemID: Cardinal; AddToBasis: Boolean; MShoots: Integer);
    { Private-Deklarationen }
  protected
    procedure RestoreSurface;
    procedure LoadImages;
    procedure DoScroll(Direction: TScrollDirection;Pos: TPoint);override;
    function SlotAtPos(Point: TPoint): Integer;
    function RectOfSlot(Slot: Integer): TRect;

    function ZeigeMunitionsFeldLinks: boolean;
    function ZeigeMunitionsFeldRechts: boolean;

    function GetSlotIndex(Slot: TSoldatConfigSlot): Integer;

    function SlotColor(Slot: TSoldatConfigSlot; LeftHand: boolean = false): TBlendColor;

    procedure AktuSlotArray;
    procedure AktuIDSofFromItems;

    procedure ShowErrorText(Text: String);

    function ShowPopups(Sender: TObject;Frames: Integer): Boolean;
  public
    constructor Create(Page: TDXPage);override;
    destructor Destroy;override;
    procedure MouseMove(X,Y: Integer);override;
    procedure MouseLeave;override;
    procedure MouseUp(Button: TMouseButton;X,Y: Integer);override;
    procedure MouseDown(Button: TMouseButton;X,Y: Integer);override;
    procedure DblClick(Button: TMouseButton;x,y: Integer);override;
    procedure Draw(Surface: TDirectDrawSurface;var Mem: TDDSurfaceDesc);override;
    procedure ReDrawRect(DrawRect: TRect;Surface: TDirectDrawSurface;var Mem: TDDSurfaceDesc);override;
    procedure Resize(var NewLeft,NewTop,NewWidth,NewHeight: Integer);override;
    procedure SetPopupInfo(ItemInfo: TDXPopupItemInfo);
    procedure CreatePopupInfo;
    procedure ScrollFromItems(Up: boolean);

    procedure AddItems(SID: Cardinal; SSlot: TSoldatConfigSlot;Schuss: Integer = -1; SAnzahl: Integer = 1; SZusData: Pointer = Nil);
    procedure ClearItems;

    property UseTimeUnits   : Boolean                read fUseTimeUnits write fUseTimeUnits;
    property Manager        : TGameFigureManager     read fManager write SetManager;
    property Raumschiff     : TRaumschiff            read fRaumschiff write fRaumschiff;

    // Kommunikationsprozedur mit SoldatenListe/Bodeneinsatz
    property Comun          : TConfigCommunication   read fComun write fComun;

    { F�r DragnDrop Soldaten�bergreifend }
    property SoldatChooser  : TDXSoldatChooser       read fChooser write fChooser;
    property OnDragnDropEnd : TNotifyEvent           read fDragEnd write fDragEnd;
    { Public-Deklarationen }
  end;

implementation

uses
  lager_api, BasisListe, basis_api, raumschiff_api, ISOMessages, game_utils;

var
  SmallMunitionFont : TDirectFont;


{ TDXSoldatConfig }

constructor TDXSoldatConfig.Create(Page: TDXPage);
var
  Font: TFont;
begin
  inherited;
  Container.AddRestoreFunction(RestoreSurface);
  fOverIndex:=-1;
  fSoldatSurface:=TDirectDrawSurface.Create(Container.DDraw);
  fSoldatSurface.SystemMemory:=true;
  fItemInfo:=nil;

  fScrollBar:=TDXScrollBar.Create(Page);
  fScrollBar.RoundCorners:=rcNone;
  fScrollBar.FirstColor:=bcMaroon;
  fScrollBar.SecondColor:=clMaroon;
  fScrollBar.OnChange:=OnScrollItems;

  fDragnDropCan:=TDXCanvas.Create(Page);
  fDragnDropCan.Width:=32;
  fDragnDropCan.Height:=32;
  fDragnDropCan.OnPaint:=DrawDragnDropCanvas;
  fDragnDropCan.Visible:=false;

  if SmallMunitionFont=nil then
  begin
    Font:=TFont.Create;
    Font.Name:='Arial';
    Font.Color:=clYellow;
    Font.Size:=7;

    SmallMunitionFont:=FontEngine.FindDirectFont(Font,clBlack);

    Font.Free;
  end;

  fUseTimeUnits:=true;

  LoadImages;
end;

destructor TDXSoldatConfig.Destroy;
begin
  Container.DeleteRestoreFunction(RestoreSurface);
  fScrollBar.Free;
  fDragnDropCan.Free;
  inherited;
end;

procedure TDXSoldatConfig.Draw(Surface: TDirectDrawSurface;
  var Mem: TDDSurfaceDesc);
begin
  RedrawRect(ClientRect,Surface,Mem);
end;

procedure TDXSoldatConfig.LoadImages;
var
  Archiv: TArchivFile;
begin
  Archiv:=TArchivFile.Create;
  Archiv.OpenArchiv(FDataFile,true);
  Archiv.OpenRessource('SoldatAusr�sten');
  fSoldatSurface.Restore;
  fSoldatSurface.LoadFromStream(Archiv.Stream);
  fSoldatSurface.TransparentColor:=fSoldatSurface.Pixels[0,0];
  Archiv.Free;
end;

procedure TDXSoldatConfig.ReDrawRect(DrawRect: TRect;
  Surface: TDirectDrawSurface; var Mem: TDDSurfaceDesc);
const
  TimeUnitsRect  : TRect = (Left:  43;Top: 20;Right:  58; Bottom: 35);

var
  CenterX   : Integer;
  CenterY   : Integer;
  BarWidth  : Integer;
  TempRect  : TRect;
  Dummy     : Integer;
  tmpText   : String;
  YOffSet   : Integer;

  procedure DrawSlot(const Slot: TItemSlot);
  var
    BlendRect: TRect;
  begin
    with Slot do
    begin
      BlendRect:=RectOfSlot(Dummy);

      // Erster Slot in einer Reihe
      if Left=Self.Left+XSlotOffSet then
      begin
        TempRect:=Rect(Self.Left+2,Top+YOffSet-1,Self.Left+XSlotOffSet-2,Top+SlotSize+YOffSet);
        BlendRectangle(TempRect,120,bcMaroon,Surface,Mem);
      end;

      if not OverlapRect(BlendRect,DrawRect) then
        exit;

      if SlotID=fOverIndex then
        BlendRectangle(CorrectBottomOfRect(BlendRect),200,Color,Surface,Mem)
      else
        BlendRectangle(CorrectBottomOfRect(BlendRect),150,Color,Surface,Mem);

      Rectangle(Surface,Mem,BlendRect,Color);

      if ID<>0 then
        lager_api_DrawItem(ID,Surface,BlendRect.Left+ImageMargin,BlendRect.Top+ImageMargin)
      else
        exit;

      if (Anzahl>=1) and (not (Slot in EinheitSlot)) then
      begin
        if SlotID=fOverIndex then
          YellowStdFont.Draw(Surface,BlendRect.Left+2,BlendRect.Top+SlotSize-16,Format(FIntegerx,[Anzahl]))
        else
          YellowStdFont.DrawAlpha(Surface,BlendRect.Left+2,BlendRect.Top+SlotSize-16,Format(FIntegerx,[Anzahl]),48);
      end;
      if (Schuesse<>-1) and (Slot in SupportMunSlots) then
      begin
        if (SlotID=fOverIndex) then
          SmallMunitionFont.Draw(Surface,BlendRect.Left+2,BlendRect.Top+2,Format('[%d]',[Schuesse]))
        else
          SmallMunitionFont.DrawAlpha(Surface,BlendRect.Left+2,BlendRect.Top+2,Format('[%d]',[Schuesse]),64)
      end;

    end;
  end;

begin
  if fManager=nil then exit;

  IntersectRect(TempRect,DrawRect,Rect(Left+LeftSplitter,Top,Left+RightSplitter,Bottom));

  Surface.ClippingRect:=ClientRect;

  if AlphaElements then
    BlendRectangle(CorrectBottomOfRect(TempRect),125,bcMaroon,Surface,Mem);

  Rectangle(Surface,Mem,ClientRect,bcMaroon);

  CenterX:=Left+FigureCenter-3;
  CenterY:=Top+(Height shr 1)-15;

  Surface.Draw(CenterX-(fSoldatSurface.Width shr 1),CenterY-(fSoldatSurface.Height shr 1),fSoldatSurface);

  VLine(Surface,Mem,Top+1,Bottom-1,Left+LeftSplitter,bcMaroon);
  VLine(Surface,Mem,Top+1,Bottom-1,Left+RightSplitter-1,bcMaroon);

  if fScrollBar.Visible then
  begin
    BarWidth:=LeftSplitter-17;
    YOffSet:=-fScrollBar.Value;
  end
  else
  begin
    BarWidth:=LeftSplitter-1;
    YOffSet:=0;
  end;

  { G�rtel }
  TempRect:=Rect(Left+LeftSplitter+2,CenterY,Right-2,CenterY+18);
  BlendRectangle(TempRect,100,bcOlive,Surface,Mem);
  YellowStdFont.Draw(Surface,TempRect.Left+5,TempRect.Top+2,ST0309260009);

  // Kapazit�t des G�rtels
  tmpText:=Format(ST0501230005,[fManager.GuertelKapazitat,fManager.MaxGuertelKapazitat]);
  YellowStdFont.Draw(Surface,TempRect.Right-5-YellowStdFont.TextWidth(tmpText),TempRect.Top+2,tmpText);

  { Rucksack }
  TempRect:=Rect(Left+LeftSplitter+2,CenterY+77,Right-2,CenterY+95);
  BlendRectangle(TempRect,100,bcMaroon,Surface,Mem);
  YellowStdFont.Draw(Surface,TempRect.Left+5,TempRect.Top+2,ST0309220048);

  // Kapazit�t des Rucksack
  tmpText:=Format(ST0502050001,[fManager.RucksackSlotsUsed,RucksackSlots]);
  YellowStdFont.Draw(Surface,TempRect.Right-5-YellowStdFont.TextWidth(tmpText),TempRect.Top+2,tmpText);

  { Itemsslots zeichnen }
  for Dummy:=0 to high(fItemSlots) do
  begin
    DrawSlot(fItemSlots[Dummy]);
  end;

  // �berschriften zeichnen
  for Dummy:=0 to high(fCaptions) do
  begin
    if fCaptions[Dummy].Draw then
    begin
      with fCaptions[Dummy] do
      begin
        if Level=0 then
        begin
          TempRect:=Rect(Left+2,Top+YOffSet,Left+BarWidth,Top+18+YOffSet);
          BlendRectangle(TempRect,120,bcMaroon,Surface,Mem);
          YellowStdFont.Draw(Surface,TempRect.Left+3,TempRect.Top+3,Text);
        end
        else if Level=1 then
        begin
          TempRect:=Rect(Left+XSlotOffSet,Top+YOffSet,Left+BarWidth,Top+18+YOffSet);
          BlendRectangle(TempRect,75,bcMaroon,Surface,Mem);
          YellowStdFont.Draw(Surface,TempRect.Left+3,TempRect.Top+3,Text);

          TempRect:=Rect(Left+2,Top+YOffSet-1,Left+XSlotOffSet-2,Top+19+YOffSet);
          BlendRectangle(TempRect,120,bcMaroon,Surface,Mem);
        end;


        tmpText:='';
        if Obj is TRaumschiff then
          tmpText:=Format(FLagerRoom,[TRaumschiff(Obj).LagerRoomBelegt/10,TRaumschiff(Obj).Model.LagerPlatz/1])
        else if Obj is TBasis then
          tmpText:=Format(FLagerRoom,[TBasis(Obj).LagerBelegt,TBasis(Obj).LagerRaum/1]);

        if tmpText<>'' then
        begin
          tmpText:='('+tmpText+')';
          WhiteStdFont.Draw(Surface,TempRect.Right-4-WhiteStdFont.TextWidth(tmpText),TempRect.Top+3,tmpText);
        end;
      end;
    end;
  end;

  if fErrorText<>EmptyStr then
  begin
    BlendRectangle(Rect(Left+2,Bottom-41,Left+10+RedStdFont.TextWidth(fErrorText),Bottom-26),175,bcDisabled,Surface,Mem);
    Rectangle(Surface,Mem,Left+2,Bottom-41,Left+10+RedStdFont.TextWidth(fErrorText),Bottom-25,bcDisabled);
    RedStdFont.Draw(Surface,Left+5,Bottom-40,fErrorText);
  end;

  // Verbleibende Zeiteinheiten zeichnen
  if MustUseTimeUnits then
  begin
    BlendRectangle(Rect(Left+RightSplitter-43,Top+1,Left+RightSplitter-1,Top+20),150,bcMaroon,Surface,Mem);
    Surface.Draw(Left+RightSplitter-40,Top+4,TimeUnitsRect,Container.ImageList.Items[1].PatternSurfaces[0],true);

    rec.Objekt:=fManager;
    SendVMessage(vmGetUnitTimeUnits);
    WhiteStdFont.Draw(Surface,Left+RightSplitter-20,Top+5,IntToStr(rec.TimeUnits));
  end;

  Surface.ClearClipRect;
end;

procedure TDXSoldatConfig.RestoreSurface;
begin
  LoadImages;
end;

procedure TDXSoldatConfig.SetManager(const Value: TGameFigureManager);
var
  tmp : Integer;
begin
  tmp:=fScrollBar.Value;
  Container.Lock;
  fManager := Value;
  fErrorText:=EmptyStr;
  Container.DeleteFrameFunction(CountDownErrorText,Self);
  Container.DeleteFrameFunction(ShowPopups,Self);
  if fManager=nil then
    exit;
    
  EnumItems;
  QuickSort(0,High(fFromSlots),SortFrom);
  AktuSlotArray;
  fScrollBar.Value:=tmp;
  fOverIndex:=0;
  Container.Unlock;
  Container.Inclock;
  Container.DoMouseMessage;
  Redraw;
  Container.Declock;
end;

function TDXSoldatConfig.SlotAtPos(Point: TPoint): Integer;
var
  Dummy: Integer;
begin
  result:=-1;
  for Dummy:=0 to High(fItemSLots) do
  begin
    if PtInRect(RectOfSlot(Dummy),Point) then
      result:=Dummy;
  end;
end;

procedure TDXSoldatConfig.MouseMove(X, Y: Integer);
var
  Slot     : Integer;
  Item     : TLagerItem;
  Rect     : TRect;
  OldIndex : Integer;
begin
  Slot:=SlotAtPos(Point(Left+X,Top+Y));

  if (fOverIndex<>-1) and (GetKeyState(VK_LBUTTON)<0) then
    BeginDragnDrop;

  { Im DragNDropModus }
  if fDragnDropMode then
  begin
    if (Slot<>-1) and CheckPlacable(fDragnDropSlot,fItemSlots[Slot]) then
    begin
      fDragonSlot:=Slot;
      Rect:=RectOfSlot(Slot);
      fDragnDropCan.SetRect(Rect.Left,Rect.Top,Rect.Right-Rect.Left,Rect.Bottom-Rect.Top);
    end
    else
    begin
      fDragonSlot:=-1;
      fDragnDropCan.SetRect(Left+X-16,Top+Y-16,32,32);
    end;
    if fChooser<>nil then
      fChooser.CheckDragNDrop(Left+X,Top+Y);
    exit;
  end;

  { Normaler Modus }
  if Slot<>-1 then
  begin
    if fOverIndex<>fItemSlots[Slot].SlotID then
    begin
      OldIndex:=fOverIndex;

      fOverIndex:=fItemSlots[Slot].SlotID;

      Container.DeleteFrameFunction(ShowPopups,Self);
      fItemInfo.Visible:=false;

      if fItemSlots[Slot].ID<>0 then
      begin
        if (fItemSlots[Slot].Slot in EinheitSlot) then
        begin
          fItemInfo.Munition:=fItemSlots[Slot].Schuesse;
          if fItemInfo.Munition<>-1 then
          begin
            Item:=lager_api_GetItem(fItemSlots[Slot].ID)^;
            fItemInfo.MaxMunition:=Item.Munition;
          end;
        end
        else
          fItemInfo.Munition:=-1;

        // Nur den Objektnamen anzeigen
        fItemInfo.ShowNameOnly:=true;
        fItemInfo.Show(csRed,RectOfSlot(fOverIndex),lager_api_GetItem(fItemSlots[fOverIndex].ID)^);

        // Anzeige nach einer Sekunde aktivieren
        Container.AddFrameFunction(ShowPopups,Self,1000);
      end;

      if OldIndex<>-1 then
        Container.RedrawArea(RectOfSlot(OldIndex),Container.Surface,false);

      Container.RedrawArea(RectOfSlot(Slot),Container.Surface,true);
    end;
  end
  else if fOverIndex<>-1 then
  begin
    Container.DeleteFrameFunction(ShowPopups,Self);
    fOverIndex:=-1;
    fItemInfo.Visible:=false;
    Redraw;
  end;
end;

procedure TDXSoldatConfig.MouseUp(Button: TMouseButton;X, Y: Integer);
var
  OverMan   : TGameFigureManager;
  Item      : TLagerItem;
  GItem     : TBackPackItem;
  FromSlot  : TItemSlot;
  ToSlot    : TItemSlot;
  Data      : Integer;
  Dummy     : Integer;
  Index     : Integer;


  function HasFromSlot(Slot: TSoldatConfigSlot;out Data: Integer): Boolean;
  var
    Dummy: Integer;
  begin
    result:=false;
    for Dummy:=0 to high(fFromSlots) do
    begin
      if fFromSlots[Dummy].Slot=Slot then
      begin
        result:=true;
        Data:=fFromSlots[Dummy].Data;
        exit;
      end;
    end;
  end;

begin
  FromSlot.Slot:=scsNone;
  ToSlot.Slot:=scsNone;

  if Button=mbLeft then
  begin
    if fDragnDropMode then
    begin
      // Pr�fen, ob Ausr�stung zu einer anderen Einheit �bergeben werden soll
      OverMan:=GetTransferUnit(X+Left,Y+Top);

      if OverMan=nil then
      begin
        if (fDragonSlot<>-1) then
        begin
          FromSlot:=fDragnDropSlot;
          ToSlot:=fItemSlots[fDragOnSlot];
        end;
      end
      else
        // Ausr�stung transferieren
        TransferSlot(fDragnDropSlot,OverMan);
    end;
  end
  else if Button=mbRight then
  begin
    if (fDragOnSlot<>-1) or (fOverIndex=-1) then
    begin
      CancelDragnDrop;
      exit;
    end;

    // Ermitteln des Zieles f�r den Transfer
    ToSlot.Slot:=scsNone;
    FromSlot:=fItemSlots[fOverIndex];

    // Pr�fen, ob Ausr�stung im Slot ist
    Index:=lager_api_GetItemIndex(FromSlot.ID,false);
    if Index=-1 then
    begin
      CancelDragnDrop;
      exit;
    end;

    if FromSlot.Slot in EinheitSlot then
    begin
      if HasFromSlot(scsBodenItem,Data) then
        ToSlot.Slot:=scsBodenItem
      else if HasFromSlot(scsBasisItem,Data) then
      begin
        ToSlot.Slot:=scsBasisItem;
        ToSlot.ZusData:=fManager.HomeBase;
      end
      else if HasFromSlot(scsSchiffItem,Data) then
      begin
        ToSlot.Slot:=scsSchiffItem;
        ToSlot.ZusData:=fManager.Raumschiff;
      end;

      ToSlot.Data:=Data;
    end
    else if (FromSlot.Slot<>scsMunition) then
    begin
      // Munition und Granaten k�nnen in den Munitionsg�rtel gehen
      Item:=lager_api_GetItem(Index)^;
      if Item.TypeID in [ptMunition,ptGranate] then
      begin
        for Dummy:=0 to fManager.GuertelSlots-1 do
        begin
          if not fManager.GetGuertelItem(Dummy,GItem) then
          begin
            ToSlot.Slot:=scsMunition;
            ToSlot.Data:=Dummy;
            break;
	  end;
	end;
      end;
    end;
    if ToSlot.Slot=scsNone then
      ToSlot.Slot:=scsRucksack;
  end;
  // Aktion ausf�hren
  DoAction(FromSlot,ToSlot);

  // DragnDrop abbrechen
  CancelDragnDrop;
end;

function TDXSoldatConfig.RectOfSlot(Slot: Integer): TRect;
begin
  with fItemSlots[Slot] do
  begin
    result:=Bounds(Left,Top,SlotSize,SlotSize);

    if Slot in [scsBasisItem,scsSchiffItem,scsBodenItem] then
      result:=OffSetRectangle(result,0,-fScrollBar.Value);
  end;
end;

function TDXSoldatConfig.ZeigeMunitionsFeldLinks: boolean;
begin
  result:=fManager.GetSlotStatus(scsLinkeHandMunition)=scssVisible;
end;

function TDXSoldatConfig.ZeigeMunitionsFeldRechts: boolean;
begin
  result:=fManager.GetSlotStatus(scsRechteHandMunition)=scssVisible;
end;

procedure TDXSoldatConfig.SetPopupInfo(ItemInfo: TDXPopupItemInfo);
begin
  if ItemInfo=nil then fItemInfo.Free;
  fItemInfo:=ItemInfo;
end;

procedure TDXSoldatConfig.CreatePopupInfo;
begin
  if fItemInfo<>nil then
    exit;
  fItemInfo:=TDXPopupItemInfo.Create(Parent);
  Parent.SetFont(fItemInfo.Font);
end;

procedure TDXSoldatConfig.AktuSlotArray;
var
  CenterX    : Integer;
  CenterY    : Integer;
  Dummy      : Integer;
  EnumRec    : TEnumRuckSackItemRecord;
  Item       : PLagerItem;
  LastSlot   : TSoldatConfigSlot;
  LastTyp    : TProjektType;
  LastData   : Pointer;
  Index      : Integer;
  XLeft      : Integer;
  YTop       : Integer;
  Spl        : Integer;  {SlotPerLine}
  Count      : Integer;
  Guertel    : TBackPackItem;

  WasHeader  : Boolean;

  procedure AddSlot(SLeft,STop: Integer;SID: Cardinal;SColor: TBlendColor;SSlot: TSoldatConfigSlot;SAnzahl: Integer = 1;SData: Integer = 0);overload;
  var
    Index: Integer;
  begin
    Index:=length(fItemSlots);
    SetLength(fItemSlots,Index+1);
    with fItemSlots[Index] do
    begin
      SlotID    :=Index;
      Left      :=SLeft;
      Top       :=STop;
      Color     :=SColor;
      ID        :=SID;
      Slot      :=SSlot;
      Schuesse  :=-1;
      Data      :=SData;
      Deleted   :=false;
      Anzahl    :=SAnzahl;
    end;
  end;

  procedure AddSlot(SLeft,STop: Integer;SID: Cardinal;SColor: TBlendColor;SSlot: TSoldatConfigSlot;SSchuss: Integer;SAnzahl: Integer;SData: Integer;SZusData: Pointer);overload;
  var
    Index: Integer;
    Item : PLagerItem;
  begin
    Index:=length(fItemSlots);
    SetLength(fItemSlots,Index+1);
    if (SSchuss=-1) and (SID<>0) and (SSlot<>scsBasisItem) then
    begin
      Item:=lager_api_GetItem(SID);
      if Item.TypeID=ptMunition then
        SSchuss:=Item.Munition
    end;
    if (SSchuss<>-1) and (SID<>0) then
    begin
      Item:=lager_api_GetItem(SID);
      // Sch�sse sind nur bei Munition erlaubt
      if Item.TypeID<>ptMunition then
        SSchuss:=-1;
    end;
    with fItemSlots[Index] do
    begin
      SlotID    :=Index;
      Left      :=SLeft;
      Top       :=STop;
      Color     :=SColor;
      ID        :=SID;
      Schuesse  :=SSchuss;
      Slot      :=SSlot;
      Data      :=SData;
      ZusData   :=SZusData;
      Deleted   :=false;
      Anzahl    :=SAnzahl;
    end;
  end;

  procedure AddHeader(Text: String; ZusData: TObject; Level: Integer = 0);
  begin
    if Index>high(fCaptions) then
      SetLength(fCaptions,length(fCaptions)+3);

    if XLeft<>Self.Left+XSlotOffSet then
      inc(YTop,SlotSize+2);

    if WasHeader then
      dec(YTop);

    WasHeader:=true;

    fCaptions[Index].Top:=YTop;

    inc(YTop,21);

    fCaptions[Index].Draw:=true;

    fCaptions[Index].Text:=Text;
    fCaptions[Index].Obj:=ZusData;
    fCaptions[Index].Level:=Level;

    XLeft:=Self.Left+XSlotOffSet;

    inc(Index);
  end;
begin
  Container.DeleteFrameFunction(ShowPopups,Self);

  SetLength(fItemSlots,0);

  CenterX:=Left+FigureCenter;
  CenterY:=Top+(Height shr 1)-85;

  if fManager=nil then
    exit;

  { Linke Hand }
  if fManager.LeftHandWaffe.Gesetzt then
  begin
    AddSlot(CenterX+63,CenterY-42,fManager.LeftHandWaffe.ID,SlotColor(scsLinkeHand,true),scsLinkeHand);
    if ZeigeMunitionsFeldLinks then
    begin
      if fManager.LeftHandWaffe.MunGesetzt then
        AddSlot(CenterX+63,CenterY,fManager.LeftHandWaffe.MunID,SlotColor(scsLinkeHandMunition,true),scsLinkeHandMunition,fManager.LeftHandWaffe.Schuesse,1,0,nil)
      else
        AddSlot(CenterX+63,CenterY,0,SlotColor(scsLinkeHandMunition,true),scsLinkeHandMunition)
    end;
  end
  else
  begin
    AddSlot(CenterX+63,CenterY-42,0,SlotColor(scsLinkeHand,true),scsLinkeHand);
  end;

  { Rechte Hand}
  if fManager.RightHandWaffe.Gesetzt then
  begin
    AddSlot(CenterX-(63+SlotSize),CenterY-42,fManager.RightHandWaffe.ID,SlotColor(scsRechteHand,false),scsRechteHand);
    if ZeigeMunitionsFeldRechts then
    begin
      if fManager.RightHandWaffe.MunGesetzt then
        AddSlot(CenterX-(63+SlotSize),CenterY,fManager.RightHandWaffe.MunID,SlotColor(scsRechteHandMunition,false),scsRechteHandMunition,fManager.RightHandWaffe.Schuesse,1,0,nil)
      else
        AddSlot(CenterX-(63+SlotSize),CenterY,0,SlotColor(scsRechteHandMunition,false),scsRechteHandMunition)
    end;
  end
  else
  begin
    AddSlot(CenterX-(63+SlotSize),CenterY-42,0,SlotColor(scsRechteHand,false),scsRechteHand);
  end;

  { R�stung }
  if fManager.Panzerung.Gesetzt then
    AddSlot(CenterX-(SlotSize div 2),CenterY-42,fManager.Panzerung.ID,bcMaroon,scsPanzerung)
  else
    AddSlot(CenterX-(SlotSize div 2),CenterY-42,0,SlotColor(scsPanzerung),scsPanzerung);

  { G�rtel }
  if fManager.Guertel.Gesetzt then
    AddSlot(CenterX-(SlotSize div 2),CenterY,fManager.Guertel.ID,bcMaroon,scsGuertel)
  else
    AddSlot(CenterX-(SlotSize div 2),CenterY,0,bcMaroon,scsGuertel);

  { Munitionen im G�rtel }
  Count:=fManager.GuertelSlots;

  XLeft:=CenterX-4-((min(6,Count)*SlotSize+2) div 2);
  YTop:=CenterY+90;

  Spl:=6;

  for Dummy:=0 to fManager.GuertelSlots-1 do
  begin
    if not fManager.GetGuertelItem(Dummy,Guertel) then
    begin
      Guertel.ID:=0;
      Guertel.Schuesse:=-1;
    end;
    AddSlot(XLeft,YTop,Guertel.ID,bcOlive,scsMunition,Guertel.Schuesse,1,Dummy,nil);
    inc(XLeft,SlotSize+2);
    dec(Spl);
    dec(Count);
    if Spl=0 then
    begin
      inc(YTop,SlotSize+2);
      XLeft:=CenterX-4-((min(6,Count)*SlotSize+2) div 2);
      if Count<6 then
        inc(XLeft,2);
      Spl:=6;
    end;
  end;

  { Slots f�r den Rucksack }
  EnumRec.Index:=0;
  YTop:=CenterY+167;
  XLeft:=Left+LeftSplitter+2;
  repeat
    fManager.EnumRucksackItems(EnumRec);
    if (EnumRec.Item.ID<>0) or (fManager.RucksackSlotsUsed<RucksackSlots) then
      AddSlot(XLeft,YTop,EnumRec.Item.ID,SlotColor(scsRucksack),scsRucksack,EnumRec.Item.Schuesse,1,EnumRec.Index-1,nil);
    inc(XLeft,SlotSize+2);
    if (XLeft+SlotSize)>Right then
    begin
      inc(YTop,SlotSize+2);
      XLeft:=Left+LeftSplitter+2;
    end;
  until (EnumRec.Item.ID=0);

  { Schiff/Basisausr�stung/Bodenausr�stung }
  YTop:=Top+3;
  XLeft:=Left+XSlotOffSet;

  LastSlot:=scsNone;
  LastData:=nil;

  for Dummy:=0 to high(fCaptions) do
    fCaptions[Dummy].Draw:=false;

  // SlotsPerLine

  WasHeader:=true;

  Index:=0;
  LastTyp:=ptNone;
  for Dummy:=0 to High(fFromSlots) do
  begin
    with fFromSlots[Dummy] do
    begin
      // �berschrift hinzuf�gen
      if (LastSlot<>Slot) or ((Slot<>scsBodenItem) and (LastData<>ZusData)) then
      begin
        case Slot of
          scsSchiffItem: AddHeader(TRaumschiff(ZusData).Name,ZusData);
          scsBasisItem: AddHeader(TBasis(ZusData).Name,ZusData);
          scsBodenItem: AddHeader(ST0309220047,nil);
        end;

        LastSlot:=Slot;
        LastData:=ZusData;
        LastTyp:=ptNone;
      end;

      Item:=lager_api_GetItem(ID,false);
      if (Item<>nil) and ((LastTyp=ptNone) or ((LastTyp<>Item.TypeID) and (Item.TypeID<>ptMunition))) then
      begin
        if Item.TypeID=ptMunition then
          LastTyp:=ptWaffe
        else
          LastTyp:=Item.TypeID;

        AddHeader(game_utils_TypeToStr(LastTyp),nil,1);
      end;

      AddSlot(XLeft,YTop,ID,Color,Slot,Schuesse,Anzahl,Dummy,ZusData);
      WasHeader:=false;
    end;

    inc(XLeft,SlotSize+2);
    if (XLeft+SlotSize)>(Left+LeftSplitter) then
    begin
      inc(YTop,SlotSize+2);

      XLeft:=Left+XSlotOffSet;
    end;
  end;

  // Ermittlung, ob Scrollbar angezeigt werden muss oder nicht

  if XLeft<>Self.Left+3 then
    inc(YTop,SlotSize+2);

  dec(YTop,Top);
  // LinesPerScreen

  Container.Lock;
  if YTop>Height then
  begin
    fScrollBar.Max:=YTop-Height;
    fScrollBar.Visible:=true;
  end
  else
  begin
    fScrollBar.Max:=0;
    fScrollBar.Value:=0;
    fScrollBar.Visible:=false;
  end;
  Container.Unlock;

//  AddSlot(XLeft+10,YTop+10,0,bcRed,scsTrash);
//  else
//    fScrollBar.Value:=0;
end;

procedure TDXSoldatConfig.MouseLeave;
begin
  if fDragnDropMode then
    CancelDragnDrop;

  if fOverIndex<>-1 then
  begin
    Container.DeleteFrameFunction(ShowPopups,Self);
    fOverIndex:=-1;
    fItemInfo.Visible:=false;
    Redraw;
  end;
end;

procedure TDXSoldatConfig.AddItems(SID: Cardinal;SSlot: TSoldatConfigSlot;Schuss: Integer; SAnzahl: Integer;SZusData: Pointer);
var
  Index: Integer;
  {$IFDEF NOSTACKING}
  Dummy: Integer;
  {$ENDIF}
begin
  {$IFDEF NOSTACKING}
  for Dummy:=1 to SAnzahl do
  begin
  {$ENDIF}
  Index:=length(fFromSlots);
  SetLength(fFromSlots,Index+1);
  with fFromSlots[Index] do
  begin
    SlotID    :=Index;
    Color     :=SlotColor(SSlot);
    ID        :=SID;
    Slot      :=SSlot;
    Schuesse  :=Schuss;
    Deleted   :=false;
    Anzahl    :=SAnzahl;
    ZusData   :=SZusData;
  end;
  {$IFDEF NOSTACKING}
  end;
  {$ENDIF}
end;

procedure TDXSoldatConfig.ClearItems;
begin
  SetLength(fFromSlots,0);
end;


procedure TDXSoldatConfig.EnumItems;
var
  fItemList  : TList;
  Pos        : TPoint;
  HasOne     : Boolean;
  Dummy      : Integer;
  Par        : Integer;

  const Umgebung   : Array[0..7] of record
          XOffSet  : Integer;
          YOffSet  : Integer;
        end = ((XOffSet: -1;YOffSet: -1),
               (XOffSet:  0;YOffSet: -1),
               (XOffSet: +1;YOffSet: -1),
               (XOffSet: -1;YOffSet:  0),
               (XOffSet: +1;YOffSet:  0),
               (XOffSet: -1;YOffSet: +1),
               (XOffSet:  0;YOffSet: +1),
               (XOffSet: +1;YOffSet: +1));

  procedure AddItemsAtPos(X,Y: Integer);
  var
    Dummy      : Integer;
  begin
    for Dummy:=0 to fItemList.Count-1 do
    begin
      if (X=TISOItem(fItemList[Dummy]).Position.X) and (Y=TISOItem(fItemList[Dummy]).Position.Y) then
      begin
        // Sicherstellen, dass die Ausr�stung der Lagerliste bekannt ist
        lager_api_RegisterAlienItem(TISOItem(fItemList[Dummy]).Item);

        AddItems(TISOItem(fItemList[Dummy]).Item.ID,scsBodenItem,TISOItem(fItemList[Dummy]).Item.Munition);
        fFromSlots[high(fFromSlots)].ZusData:=fItemList[Dummy];
        HasOne:=true;
      end;
    end;
  end;

  procedure EnumBasisItems;
  var
    Dummy    : Integer;
    BasisID  : Cardinal;
    Item     : TLagerItem;
    HasSlot  : boolean;
    Schuss   : Integer;
    Base     : TBasis;
  begin
    if not (fManager is TSoldatManager) then
      exit;

    BasisID:=TSoldatManager(fManager).SoldatInfo.BasisID;
    Base:=basis_api_GetBasisFromID(BasisID);
    HasSlot:=false;

    for Dummy:=0 to lager_api_count-1 do
    begin
      Item:=lager_api_GetItem(Dummy)^;
      if not lager_api_ItemVisible(Item) then
        continue;

      Item.Anzahl:=lager_api_GetItemCountInBase(BasisID,Dummy);
      if Item.TypeID in SoldatenTypes then
      begin
        if (Item.TypeID=ptMunition) then
        begin
          if Item.ShootSurPlus=0 then
            Schuss:=-1
          else
            Schuss:=Item.ShootSurPlus;
        end
        else
          Schuss:=-1;

        if (Item.Anzahl>0) or (Schuss<>-1) then
        begin
          AddItems(Item.ID,scsBasisItem,Schuss,Item.Anzahl,Base);
          HasSlot:=true;
        end;
      end;
    end;
    if not HasSlot then
      AddItems(0,scsBasisItem,-1,1,Base);
  end;

  procedure AddItemsFromSchiff(RSchiff: TRaumschiff);
  var
    Dummy   : Integer;
    Item    : TItemsInSchiff;
    LItem   : TLagerItem;
    HasSlot : boolean;
    Schuss  : Integer;
  begin
    HasSlot:=false;
    for Dummy:=0 to RSchiff.ItemCount-1 do
    begin
      Item:=RSchiff.Items[Dummy];
      LItem:=lager_api_GetItem(Item.ID)^;
      if LItem.TypeID=ptMunition then
        Schuss:=LItem.Munition
      else
        Schuss:=-1;
      if Item.Anzahl>0 then
      begin
        AddItems(Item.ID,scsSchiffItem,Schuss,Item.Anzahl,RSchiff);
        HasSlot:=true;
      end;
    end;
    if not HasSlot then
      AddItems(0,scsSchiffItem,-1,0,RSchiff);
  end;

  procedure EnumSchiffItems;
  var
    RSchiff: TRaumschiff;
    Dummy  : Integer;
  begin
    if fManager.InBasis then
    begin
      // Ausr�stung f�r alle Raumschiffe anzeigen
      for Dummy:=0 to raumschiff_api_GetRaumschiffCount-1 do
      begin
        RSchiff:=raumschiff_api_GetIndexedRaumschiff(Dummy);

        if (RSchiff.XCOMSchiff) and (RSchiff.Model.LagerPlatz>0) and (RSchiff.GetHomeBasis.ID = TSoldatManager(fManager).SoldatInfo.BasisID) and (sabEquipt in RSchiff.Abilities) then
        begin
          AddItemsFromSchiff(RSchiff);
        end;
      end;
    end
    else
    begin
      // Ausr�stung nur f�r das Raumschiff anzeigen, in dem der Soldat unterwegs ist
      RSchiff:=TRaumschiff(TSoldatManager(fManager).Raumschiff);

      if (RSchiff=nil) or (fManager.OnBattleField) then
        exit;

      AddItemsFromSchiff(RSchiff);
    end;
  end;

begin
  ClearItems;

  if (fManager is TSoldatManager) then
  begin
    if TSoldatManager(fManager).InBasis and (not TSoldatManager(fManager).OnBattleField) then
      EnumBasisItems;

    EnumSchiffItems;
  end;

  // Bodenausr�stung wird hier verwaltet
  if fManager.OnBattleField then
  begin
    HasOne:=false;
    if Assigned(fComun) then
    begin
      Assert(fManager.GetEinsatzPosition(Pos));
      Comun(1,Pointer(fItemList),Point(0,0));
      AddItemsAtPos(Pos.X,Pos.Y);
      for Dummy:=0 to 7 do
      begin
        Par:=Dummy;
        Comun(4,Pointer(Par),Pos);
        if Boolean(Par) then
          AddItemsAtPos(Pos.X+Umgebung[Dummy].XOffSet,Pos.Y+Umgebung[Dummy].YOffSet);
      end;
    end;
    if not HasOne then
      AddItems(0,scsBodenItem);
  end;
end;

procedure TDXSoldatConfig.Resize(var NewLeft, NewTop, NewWidth,
  NewHeight: Integer);
begin
  inherited;
  fScrollBar.SetRect(Left+LeftSplitter-16,Top,17,Height);
  fScrollBar.LargeChange:=Height-5;
  fScrollBar.SmallChange:=SlotSize+2;
  NewWidth:=RightSplitter;
end;

procedure TDXSoldatConfig.OnScrollItems(Sender: TObject);
begin
//  AktuSlotArray;
  Redraw;

  Container.DoMouseMessage;
end;

procedure TDXSoldatConfig.DrawDragnDropCanvas(Sender: TDXComponent;
  Surface: TDirectDrawSurface; var Mem: TDDSurfaceDesc);
begin
  if fDragonSlot<>-1 then
  begin
    BlendRectangle(CorrectBottomOfRect(fDragnDropCan.ClientRect),150,bcBlue,Surface,Mem);
    lager_api_DrawItem(fDragnDropSlot.ID,Surface,fDragnDropCan.Left+ImageMargin,fDragnDropCan.Top+ImageMargin);
  end
  else
    lager_api_DrawItem(fDragnDropSlot.ID,Surface,fDragnDropCan.Left,fDragnDropCan.Top,160);
//    fManager.LagerListe.DrawImage(fManager.LagerListe[fManager.LagerListe.IndexOfID(fDragnDropSlot.ID)].ImageIndex,Surface,fDragnDropCan.Left,fDragnDropCan.Top);
end;

procedure TDXSoldatConfig.DoScroll(Direction: TScrollDirection;
  Pos: TPoint);
begin
  if (not fScrollBar.Visible) or fDragnDropMode then exit;
  if PtInRect(Rect(Left,Top,LeftSplitter,Bottom),Pos) then
  begin
    if Direction=sdUp then
      fScrollBar.Value:=fScrollBar.Value-fScrollBar.SmallChange*2
    else if Direction=sdDown then
      fScrollBar.Value:=fScrollBar.Value+fScrollBar.SmallChange*2;
      
    exit;
  end;
end;

function TDXSoldatConfig.CheckPlacable(FromSlot, ToSlot: TItemSlot): Boolean;
const
  TypeAllowed: Array[TSoldatConfigSlot] of TProjektTypes =(
  {scsNone}                 [low(TProjektType)..high(TProjektType)],
  {scsRucksack}             [ptWaffe..ptGuertel],
  {scsBasisItem}            [ptWaffe..ptGuertel],
  {scsSchiffItem}           [ptWaffe..ptGuertel],
  {scsBodenItem}            [ptWaffe..ptGuertel],
  {scsLinkeHand}            [ptWaffe,ptGranate,ptMine,ptSensor],
  {scsLinkeHandMunition}    [ptMunition],
  {scsRechteHand}           [ptWaffe,ptGranate,ptMine,ptSensor],
  {scsRechteHandMunition}   [ptMunition],
  {scsPanzerung}            [ptPanzerung],
  {scsGuertel}              [ptGuertel],
  {scsMunition}             [low(TProjektType)..high(TProjektType)],
  {scsTrash}                [low(TProjektType)..high(TProjektType)]);

var
  Item  : PLagerItem;
begin
  Item:=lager_api_GetItem(FromSlot.ID);
  result:=false;
  if (Item.TypeID in TypeAllowed[ToSlot.Slot]) then
  begin
    if ToSlot.Slot=scsMunition then
    begin
      if (ToSlot.ID<>0) and (ToSlot.ID<>FromSlot.ID) then
        exit;
      result:=(not (Item.TypeID in [ptPanzerung,ptGuertel])) and ((Item.TypeID<>ptWaffe) or (Item.Einhand));
    end
    else
      result:=true;
  end;
end;

function TDXSoldatConfig.SetSlot(FromSlot, ToSlot: TItemSlot): boolean;
const
  MunitionSlot = [scsRucksack,scsMunition,scsLinkeHandMunition,scsRechteHandMunition];
var
  VirFromSlot : TItemSlot;
  MunSlot     : TItemSlot;
  HasMun      : boolean;
  Waffe       : PSoldatWaffe;

  FromItem    : TLagerItem;
  ToItem      : TLagerItem;

  function FindMunForIDinSlot(ID: Cardinal;
    SlotType: TSoldatConfigSlot; var Slot: TItemSlot): boolean;
  var
    Dummy     : Integer;
    EnumRec   : TEnumRuckSackItemRecord;

    function CheckMunition(TestID: Cardinal): boolean;
    var
      Item    : TLagerItem;
    begin
      result:=false;
      if TestID=0 then
        exit;
      Item:=lager_api_GetItem(TestID)^;
      if Item.TypeID=ptMunition then
        result:=Item.Munfor=ID;
    end;

  begin
    result:=false;
    if SlotType in [scsSchiffItem,scsBasisItem,scsBodenItem] then
    begin
      for Dummy:=0 to high(fFromSlots) do
      begin
        if (fFromSlots[Dummy].Slot=SlotType) and CheckMunition(fFromSlots[Dummy].ID) then
        begin
          result:=true;
          Slot:=fFromSlots[Dummy];
          Slot.Schuesse:=fFromSlots[Dummy].Schuesse;
          Slot.Data:=Dummy;
          exit;
        end;
      end;
    end
    else if SlotType=scsRucksack then
    begin
      { Slots f�r den Rucksack }
      EnumRec.Index:=0;
      repeat
        fManager.EnumRucksackItems(EnumRec);
        if CheckMunition(EnumRec.Item.ID) then
        begin
          result:=true;
          with Slot do
          begin
            ID:=EnumRec.Item.ID;
            Data:=EnumRec.Index-1;
            Schuesse:=EnumRec.Item.Schuesse;
            Slot:=scsRucksack;
            exit;
          end;
        end;
      until (EnumRec.Item.ID=0);
    end
    else if SlotType=scsLinkeHand then
    begin
      if fManager.LeftHandWaffe.MunGesetzt then
      begin
        result:=true;
        with Slot do
        begin
          ID:=fManager.LeftHandWaffe.MunID;
          Schuesse:=fManager.LeftHandWaffe.Schuesse;
          Slot:=scsLinkeHandMunition;
        end;
        exit;
      end;
    end
    else if SlotType=scsRechteHand then
    begin
      if fManager.RightHandWaffe.MunGesetzt then
      begin
        result:=true;
        with Slot do
        begin
          ID:=fManager.RightHandWaffe.MunID;
          Schuesse:=fManager.RightHandWaffe.Schuesse;
          Slot:=scsRechteHandMunition;
        end;
        exit;
      end;
    end;
  end;

begin
  Waffe:=nil;

  result:=false;
  if lager_api_GetItemIndex(FromSlot.ID,false)<>-1 then
    FromItem:=lager_api_GetItem(FromSlot.ID)^;

  if lager_api_GetItemIndex(ToSlot.ID,false)<>-1 then
    ToItem:=lager_api_GetItem(ToSlot.ID)^;

  // Bei Basisitems werden prim�r volle Munitionspacks genommen
  if (FromSlot.Slot=scsBasisItem) and (FromItem.TypeID=ptMunition) and (FromSlot.Anzahl>=1) then
    FromSlot.Schuesse:=-1;

  // Pr�fen, ob Munition zusammengepackt wird
  if (FromSlot.ID=ToSlot.ID) and (FromItem.TypeID=ptMunition) and (ToItem.TypeID=ptMunition) then
  begin
    if (FromSlot.Slot in (MunitionSlot+[scsBasisItem])) and (ToSlot.Slot in MunitionSlot) then
    begin
      if (FromSlot.Schuesse<>FromItem.Munition) or (ToSlot.Schuesse<>ToItem.Munition) then
      begin
        if FromSlot.Schuesse=-1 then
          FromSlot.Schuesse:=FromItem.Munition;

        if ToSlot.Schuesse=-1 then
          ToSlot.Schuesse:=ToItem.Munition;
          
        VirFromSlot.Slot:=scsNone;
        VirFromSlot.ID:=FromSlot.ID;
        // wenn beide Ausr�stungen aus dem Rucksack kommen, muss der Index von zweiten
        // um eins verringert werden, da ja dort Ausr�stung aus dem Rucksack gel�scht wurde
        // und nachfolgende Ausr�stung vorgeschoben wird
        if (FromSlot.Slot=scsRucksack) and (ToSlot.Slot=scsRucksack) and (FromSlot.Data<ToSlot.Data) then
          dec(ToSlot.Data);

        // Alte Slots rausschmeissen
        SetSlot(FromSlot,VirFromSlot);
        SetSlot(ToSlot,VirFromSlot);

        ToSlot.Schuesse:=FromSlot.Schuesse+ToSlot.Schuesse;

        if ToSlot.Schuesse>ToItem.Munition then
        begin
          FromSlot.Schuesse:=ToSlot.Schuesse-FromItem.Munition;
          ToSlot.Schuesse:=ToItem.Munition;
          VirFromSlot.Schuesse:=FromSlot.Schuesse;
          SetSlot(VirFromSlot,FromSlot);
        end;
        VirFromSlot.Schuesse:=ToSlot.Schuesse;
        SetSlot(VirFromSlot,ToSlot);
      end;
      exit;
    end;
    // Ausr�stung aus Munislot in G�rtel ist nicht erlaubt, wird aber durch CheckDrag durchgelassen
    // Wenn es identische Munition ist, um auch Munition aus dem MuniSlot mit dem aus dem G�rtel
    // Zusammenzuf�hren
    if (FromSlot.Slot in [scsLinkeHandMunition,scsRechteHandMunition]) and (ToSlot.Slot=scsMunition) then
      exit;
  end;

  if (FromSlot.Slot=ToSlot.Slot) and (FromSlot.Slot<>scsMunition) and (FromSlot.ZusData=ToSlot.ZusData) then
    exit;

  if ToSlot.Slot in [scsLinkeHand,scsRechteHand,scsPanzerung,scsGuertel] then
  begin
    // Gleiche Ausr�stung muss nicht neu gesetzt werden
    if FromSlot.ID=ToSlot.ID then
      exit;
  end;
  result:=true;

  // Tausch der Waffen geht �ber den Manager
  if ((FromSlot.Slot=scsRechteHand) and (ToSlot.Slot=scsLinkeHand)) or
     ((FromSlot.Slot=scsLinkeHand) and (ToSlot.Slot=scsRechteHand)) then
  begin
    fManager.ChangeWeapons;
    exit;
  end;

  if (FromSlot.Slot in [scsRucksack,scsSchiffItem,scsBodenItem,scsBasisItem,scsMunition]) then
  begin
    case ToSlot.Slot of
      scsLinkeHand,scsRechteHand :
      begin
        Waffe:=fManager.GetAddrOfSlot(ToSlot.Slot);
        if Waffe.Gesetzt then
          SetSlot(ToSlot,FromSlot);
      end;
      scsPanzerung:
      begin
        if fManager.Panzerung.Gesetzt then
          SetSlot(ToSlot,FromSlot);
      end;
      scsGuertel:
      begin
        if fManager.Guertel.Gesetzt then
          SetSlot(ToSlot,FromSlot);
      end;
      scsLinkeHandMunition:
      begin
        if fManager.LeftHandWaffe.MunGesetzt then
          SetSlot(ToSlot,FromSlot);
      end;
      scsRechteHandMunition:
      begin
        if fManager.RightHandWaffe.MunGesetzt then
          SetSlot(ToSlot,FromSlot);
      end;
    end;
  end;

  // Wenn das Ziel eine Hand, muss die Munition vorher ermittelt und gespeichert
  HasMun:=false;
  if ToSlot.Slot in [scsLinkeHand,scsRechteHand] then
  begin
    if (FromItem.TypeID=ptWaffe) and (FromItem.WaffType<>wtLaser) then
    begin
      // Munition suchen
      if FindMunForIDinSlot(FromSlot.ID,FromSlot.Slot,VirFromSlot) then
      begin
        with MunSlot do
        begin
          Waffe:=fManager.GetAddrOfSlot(ToSlot.Slot);
          if Waffe.MunGesetzt then
            ID:=Waffe.MunID
          else
            ID:=0;
          if ToSlot.Slot=scsLinkeHand then
            Slot:=scsLinkeHandMunition
          else
            Slot:=scsRechteHandMunition;
        end;
        HasMun:=true;
      end;
    end;
  end
  else if FromSlot.Slot in [scsLinkeHand,scsRechteHand] then
  begin
    Waffe:=fManager.GetAddrOfSlot(FromSlot.Slot);
    if Waffe.MunGesetzt then
    begin
      HasMun:=true;
      with VirFromSlot do
      begin
        ID:=Waffe.MunID;
        if FromSlot.Slot=scsLinkeHand then
          Slot:=scsLinkeHandMunition
        else
          Slot:=scsRechteHandMunition;
        Schuesse:=Waffe.Schuesse;
      end;
      MunSlot:=ToSlot;
    end;
  end;

  // Tausch der Munitionen
  if (FromSlot.Slot=scsRechteHandMunition) and (ToSlot.Slot=scsLinkeHandMunition) then
  begin
    if fManager.LeftHandWaffe.MunGesetzt then
    begin
      HasMun:=true;
      VirFromSlot.Slot:=scsNone;
      VirFromSlot.ID:=fManager.LeftHandWaffe.MunID;
      VirFromSlot.Schuesse:=fManager.LeftHandWaffe.Schuesse;
      MunSlot.Slot:=scsRechteHandMunition;
    end;
  end
  else if (FromSlot.Slot=scsLinkeHandMunition) and (ToSlot.Slot=scsRechteHandMunition) then
  begin
    if fManager.RightHandWaffe.MunGesetzt then
    begin
      HasMun:=true;
      VirFromSlot.Slot:=scsNone;
      VirFromSlot.ID:=fManager.RightHandWaffe.MunID;
      VirFromSlot.Schuesse:=fManager.RightHandWaffe.Schuesse;
      MunSlot.Slot:=scsLinkeHandMunition;
    end;
  end;

  // Waffe nachladen
  if (FromSlot.Slot=scsMunition) and (ToSlot.Slot in [scsRechteHandMunition,scsLinkeHandMunition]) then
  begin
    case ToSlot.Slot of
      scsLinkeHandMunition : Waffe:=fManager.GetAddrOfSlot(scsLinkeHand);
      scsRechteHandMunition: Waffe:=fManager.GetAddrOfSlot(scsRechteHand);
      else
        Assert(false);
    end;
    if Waffe.MunGesetzt then
    begin
      HasMun:=true;
      VirFromSlot.Slot:=scsNone;
      VirFromSlot.ID:=Waffe.MunID;
      VirFromSlot.Schuesse:=Waffe.Schuesse;
      MunSlot.Slot:=scsMunition;
      MunSlot.Data:=FromSlot.Data;
    end;
  end;

  // Ausr�stung altem Ort wegnehmen
  case FromSlot.Slot of
    scsBasisItem..scsBodenItem    : DeleteFromSlot(FromSlot);
    scsLinkeHand..scsPanzerung,
    scsGuertel                    : fManager.SetzeAusruestung(FromSlot.Slot,0);
    scsRucksack                   :
    begin
      fManager.DeleteFromRucksack(FromSlot.Data);
      if HasMun then
      begin
        FindMunForIDinSlot(FromSlot.ID,FromSlot.Slot,VirFromSlot);
      end;
    end;
    scsMunition                   : fManager.DeleteFromGuertel(FromSlot.Data);
  end;

  // Ausr�stung neuem Ort hinzuf�gen
  case ToSlot.Slot of
    scsBasisItem..scsBodenItem    : AddFromSlot(FromSlot,ToSlot);
    scsLinkeHand,scsRechteHand,
    scsGuertel                    :
    begin
      fManager.SetzeAusruestung(ToSlot.Slot,FromSlot.ID);
    end;
    scsLinkeHandMunition,
    scsRechteHandMunition,
    scsPanzerung                  : fManager.SetzeAusruestung(ToSlot.Slot,FromSlot.ID,FromSlot.Schuesse);
    scsRucksack                   : fManager.AddToRucksack(lager_api_GetItem(FromSlot.ID)^,FromSlot.Schuesse);
    scsMunition                   : fManager.AddToGuertel(lager_api_GetItem(FromSlot.ID)^,FromSlot.Schuesse,ToSlot.Data);
  end;

  if HasMun then
    SetSlot(VirFromSlot,MunSlot);

  fOverIndex:=-1;
end;

procedure TDXSoldatConfig.DeleteFromSlot(const FromSlot: TItemSlot);
var
  RSchiff : TRaumschiff;
begin
  // Ausr�stung in der Basis freigeben
  case FromSlot.Slot of
    scsBasisItem   : ChangeBasisObject(TSoldatManager(fManager).SoldatInfo.BasisID,FromSlot.ID,false,FromSlot.Schuesse);
    scsSchiffItem  :
    begin
      RSchiff:=TRaumschiff(FromSlot.ZusData);
      // Pr�fen auf g�ltiges Raumschiff
      Assert(raumschiff_api_GetRaumschiff(RSchiff.ID,false)=RSchiff);

      ChangeSchiffObject(RSchiff,FromSlot.ID,-1);
    end;
    scsBodenItem   :
    begin
      if not fManager.NeedTU(TUTakeBoden) then
        exit;

      // Ausr�stung merken, um alles zur�ckdrehen, falls nicht genug ZE
      // vorhanden waren
      SetLength(fDeletedBodenObjects,length(fDeletedBodenObjects)+1);
      fDeletedBodenObjects[high(fDeletedBodenObjects)]:=FromSlot.ZusData;
//      Comun(2,Item,Point(0,0));
    end;
  end;
  with fFromSlots[FromSlot.Data] do
  begin
    if Anzahl>1 then
      dec(Anzahl)
    else
      Deleted:=true;
  end;
end;

procedure TDXSoldatConfig.AddFromSlot(const FromSlot, ToSlot: TItemSlot);
var
  RSchiff : TRaumschiff;
  Item    : TLagerItem;
  PItem   : Pointer;
  Pos     : TPoint;
  ZData   : Pointer;
  Index   : Integer;
  Dummy   : Integer;
begin
  ZData:=nil;
  case ToSlot.Slot of
    scsBasisItem  :
      ChangeBasisObject(TSoldatManager(fManager).SoldatInfo.BasisID,FromSlot.ID,true,FromSlot.Schuesse);
    scsSchiffItem :
    begin
      RSchiff:=TRaumschiff(ToSlot.ZusData);
      // Pr�fen auf g�ltiges Raumschiff
      Assert(raumschiff_api_GetRaumschiff(RSchiff.ID,false)=RSchiff);

      ChangeSchiffObject(RSchiff,FromSlot.ID,1);
      ZData:=RSchiff;
    end;
    scsBodenItem  :
    begin
      if not fManager.NeedTU(TUPutBoden) then
        exit;

      Item:=lager_api_GetItem(FromSlot.ID)^;
      Item.Munition:=FromSlot.Schuesse;
      Assert(fManager.GetEinsatzPosition(Pos));
      PItem:=Pointer(@Item);
      Comun(3,PItem,Pos,fManager);

      SetLength(fNewBodenObjects,length(fNewBodenObjects)+1);
      fNewBodenObjects[high(fNewBodenObjects)]:=PItem;

      ZData:=PItem;
    end;
  end;
  // Pr�fen, ob die ID bereits vergeben ist
  if ToSlot.Slot<>scsBodenItem then
  begin
    for Dummy:=0 to high(fFromSlots) do
    begin
      if (ToSlot.Slot=fFromSlots[Dummy].Slot) and (FromSlot.ID=fFromSlots[Dummy].ID) then
      begin
        inc(fFromSlots[Dummy].Anzahl);
        exit;
      end;
    end;
  end;
  // Slot hinzuf�gen
  if fFromSlots[ToSlot.Data].ID=0 then
    Index:=ToSlot.Data
  else
  begin
    SetLength(fFromSlots,Length(fFromSlots)+1);
    Index:=High(fFromSlots);
  end;
  with fFromSlots[Index] do
  begin
    ID          :=FromSlot.ID;
    Color       :=SlotColor(ToSlot.Slot);
    Slot        :=ToSlot.Slot;
    Data        :=SlotID;
    Schuesse    :=FromSlot.Schuesse;
    ZusData     :=ZData;
    Anzahl      :=1;
  end;
end;

function TDXSoldatConfig.SlotColor(Slot: TSoldatConfigSlot;
  LeftHand: boolean): TBlendColor;
begin
  result:=bcBlack;
  case Slot of
    scsRucksack,scsBasisItem            : result:=bcMaroon;
    scsSchiffItem                       : result:=bcDarkNavy;
    scsBodenItem                        : result:=bcGreen;
    scsLinkeHand..scsRechteHandMunition :
    begin
      if fManager.GetSlotStatus(Slot)=scssVisible then
      begin
        if (LeftHand=fManager.LeftHand) then
          result:=bcGreen
        else
          result:=bcMaroon;
      end
      else
      begin
        result:=bcDisabled;
      end;
    end;
    scsPanzerung                        : result:=bcMaroon;
  end;
end;

procedure TDXSoldatConfig.AktuIDSofFromItems;
var
  Dummy: Integer;
begin
  for Dummy:=0 to High(fFromSlots) do
  begin
    fFromSlots[Dummy].Data:=Dummy;
  end;
end;

function TDXSoldatConfig.CountDownErrorText(Sender: TObject;
  Frames: Integer): boolean;
begin
  inc(fShowTime,25);
  result:=false;
  if fShowTime>3000 then
  begin
    fErrorText:=EmptyStr;
    Redraw;
    Container.DeleteFrameFunction(CountDownErrorText,Self);
    result:=true;
    exit;
  end;
end;

function TDXSoldatConfig.SortFrom(Index1, Index2: Integer;
  Typ: TFunctionType): Integer;
var
  Temp  : TItemSlot;
  Item1 : PLagerItem;
  Item2 : PLagerItem;

  function GetRelevantIndex(Item: PLagerItem): Integer;
  var
    ID  : Cardinal;
  begin
    Assert(Item<>nil);

    if Item.TypeID=ptMunition then
      ID:=Item.Munfor
    else
      ID:=Item.ID;

    result:=lager_api_GetItemIndex(ID,false);
  end;

begin
  result:=0;
  case Typ of
    ftCompare :
    begin
      result:=ord(fFromSlots[Index1].Slot)-ord(fFromSlots[Index2].Slot);
      if result=0 then
        // Gleiche Raumschiffe sollen weiterhin beieinander bleiben
        result:=Integer(fFromSlots[Index1].ZusData)-Integer(fFromSlots[Index2].ZusData);

      if result=0 then
      begin
        Item1:=lager_api_GetItem(fFromSlots[Index1].ID,false);
        Item2:=lager_api_GetItem(fFromSlots[Index2].ID,false);

        if Item1=nil then
          result:=-1
        else if Item2=nil then
          result:=1;

        if result=0 then
        begin
          if (Item1.TypeID in [ptWaffe,ptMunition]) and
             (Item2.TypeID in [ptWaffe,ptMunition]) then
          begin
            result:=GetRelevantIndex(Item1)-GetRelevantIndex(Item2);
          end;
        end;

        if result=0 then
          result:=Integer(Item1.TypeID)-Integer(Item2.TypeID);
      end;
    end;
    ftExchange:
    begin
      Temp:=fFromSlots[Index1];
      fFromSlots[Index1]:=fFromSlots[Index2];
      fFromSlots[Index2]:=Temp;
    end;
  end;
end;

procedure TDXSoldatConfig.CorrectFromSlots;
var
  Dummy      : Integer;
  Deleted    : Integer;
  Count      : Integer;
  Counts     : Array[TSoldatConfigSlot] of Integer;
begin
  FillChar(Counts,Sizeof(Counts),#0);
  for Dummy:=0 to high(fFromSlots) do
  begin
    inc(Counts[fFromSlots[Dummy].Slot]);
  end;
  Deleted:=0;
  Dummy:=0;
  Count:=length(fFromSlots);
  while (Dummy<Count) do
  begin
    if (deleted<>0) and not (fFromSlots[Dummy].Deleted) then
      fFromSlots[Dummy-deleted]:=fFromSlots[Dummy];
    if (fFromSlots[Dummy].Deleted) then
    begin
      if Counts[fFromSlots[Dummy].Slot]>1 then
      begin
        inc(Deleted);
        dec(Counts[fFromSlots[Dummy].Slot]);
      end
      else
      begin
        fFromSlots[Dummy].ID:=0;
        fFromSlots[Dummy].Deleted:=false;
      end;
    end;
    inc(Dummy);
  end;
  SetLength(fFromSlots,length(fFromSlots)-Deleted);
end;

procedure TDXSoldatConfig.ScrollFromItems(Up: boolean);
begin
  if Up then
    DoScroll(sdUp,Point(Left+2,Top+25))
  else
    DoScroll(sdDown,Point(Left+2,Top+25));
end;

function TDXSoldatConfig.MustUseTimeUnits: Boolean;
begin
  result:=fUseTimeUnits and (fManager.OnBattleField);
end;

procedure TDXSoldatConfig.ShowErrorText(Text: String);
begin
  fErrorText:=Text;
  fShowTime:=0;
  Container.DeleteFrameFunction(CountDownErrorText,Self);
  Container.AddFrameFunction(CountDownErrorText,Self,25);
end;

function TDXSoldatConfig.GetTransferUnit(X, Y: Integer): TGameFigureManager;
begin
  result:=nil;

 if fChooser<>nil then
   result:=fChooser.CheckDragNDrop(X,Y);
end;

procedure TDXSoldatConfig.CancelDragnDrop;
begin
  // Auswahl in der Soldatenliste entfernen
  Container.Inclock;
  fDragnDropMode:=false;
  
  fOverIndex:=-1;
  fDragOnSlot:=-1;

  fChooser.CheckDragNDrop(-1,-1);
  fChooser.CanKeyChange:=true;

  fDragnDropCan.Visible:=false;

  CorrectFromSlots;
  QuickSort(0,high(fFromSlots),SortFrom);
  AktuIDSofFromItems;
  AktuSlotArray;

  Container.DoMouseMessage;

  if Assigned(fDragEnd) then
    fDragEnd(Self);

  Container.Declock;
  Redraw;
end;

procedure TDXSoldatConfig.TransferSlot(const Slot: TItemSlot;
  ToManager: TGameFigureManager);
var
  IDs     : TBackPackArray;
  Waffe   : PSoldatWaffe;
  Error   : TDragActionError;
  ToNone  : TItemSlot;
begin
  { Transfer zum Manager }
  ToNone.Slot:=scsNone;

  SetLength(IDs,1);
  IDs[0].ID:=fDragnDropSlot.ID;
  IDs[0].Schuesse:=fDragnDropSlot.Schuesse;

  if fDragnDropSlot.Slot in [scsLinkeHand,scsRechteHand] then
  begin
    Waffe:=fManager.GetAddrOfSlot(fDragnDropSlot.Slot);
    if Waffe.MunGesetzt then
    begin
      SetLength(IDs,2);
      IDs[1].ID:=Waffe.MunID;
      IDs[1].Schuesse:=Waffe.Schuesse;
    end;
  end;
{  else if fDragnDropSlot.Slot=scsGuertel then
  begin
    for Dummy:=0 to fManager.GuertelSlots-1 do
    begin
      if fManager.GetGuertelItem(Dummy,Item) then
      begin
        SetLength(IDs,length(IDs)+1);
        IDs[high(IDs)]:=Item;
      end;
    end;
  end; }

  Error:=ToManager.Transfer(IDs,fManager);
  if Error<>daeNone then
  begin
    ShowErrorText(DropErrors[Error]);
    fItemSlots[fDragnDropSlot.SlotID]:=fDragnDropSlot;
    exit;
  end;

  // Ausr�stung von diesem Soldaten l�schen
  SetSlot(fDragnDropSlot,ToNone);

  DoExternalAction;
end;

procedure TDXSoldatConfig.DoExternalAction;
var
  Dummy: Integer;
begin
  // Aufgehobene Ausr�stung vom Bodeneinsatz endg�ltig freigeben
  for Dummy:=0 to high(fDeletedBodenObjects) do
    Comun(2,Pointer(fDeletedBodenObjects[Dummy]),Point(0,0));

  // Ausr�stung in den Raumschiffen �ndern
  for Dummy:=0 to high(fChangedSchiffObjects) do
  begin
    with fChangedSchiffObjects[Dummy] do
      Raumschiff.ChangeItem(ID,Count,false);
  end;

  // Ausr�stung in der Basis �ndern
  for Dummy:=0 to high(fChangedBasisObjects) do
  begin
    with fChangedBasisObjects[Dummy] do
    begin
      if Add then
      begin
        // Dieser Put ist unsicher, da eine Exception nicht abgefangen wird
        // Es sollte aber hier nie eine auftreten, da der Lagerraum in der Basis
        // an anderer Stelle gepr�ft wird
        if Schuesse = -1 then
          lager_api_PutItem(Basis,ID)
        else
          lager_api_PutMunition(Basis,ID,Schuesse);
      end
      else
      begin
        if Schuesse = -1 then
          lager_api_DeleteItem(Basis,ID)
        else
          lager_api_PutMunition(Basis,ID,-Schuesse);
      end;
    end;
  end;

  SetLength(fDeletedBodenObjects,0);
  SetLength(fChangedBasisObjects,0);
  SetLength(fChangedSchiffObjects,0);

  EnumItems;
end;

procedure TDXSoldatConfig.RestoreExternalAction;
var
  Dummy: Integer;
begin
  // Fallengelasse Ausr�stung wieder l�schen
  for Dummy:=0 to high(fNewBodenObjects) do
    fNewBodenObjects[Dummy].Free;
  SetLength(fNewBodenObjects,0);

  SetLength(fDeletedBodenObjects,0);
  SetLength(fChangedBasisObjects,0);
  SetLength(fChangedSchiffObjects,0);

  EnumItems;
end;

procedure TDXSoldatConfig.ChangeSchiffObject(Schiff: TRaumschiff;
  ItemID: Cardinal; ItemCount: Integer);
begin
  SetLength(fChangedSchiffObjects,length(fChangedSchiffObjects)+1);
  with fChangedSchiffObjects[high(fChangedSchiffObjects)] do
  begin
    Raumschiff:=Schiff;
    ID:=ItemID;
    Count:=ItemCount;
  end;
end;

procedure TDXSoldatConfig.ChangeBasisObject(BasisID, ItemID: Cardinal;
  AddToBasis: Boolean; MShoots: Integer);
begin
  SetLength(fChangedBasisObjects,length(fChangedBasisObjects)+1);
  with fChangedBasisObjects[high(fChangedBasisObjects)] do
  begin
    Basis:=BasisID;
    ID:=ItemID;
    Add:=AddToBasis;
    Schuesse:=MShoots;
  end;
end;

procedure TDXSoldatConfig.DblClick(Button: TMouseButton; x, y: Integer);
var
  FromSlot,ToSlot: TItemSlot;
  Slot           : Integer;
  Item           : TLagerItem;
begin
  Slot:=SlotAtPos(Point(Left+X,Top+Y));
  if Slot=-1 then
    exit;

  FromSlot:=fItemSlots[Slot];

  if FromSlot.ID=0 then
    exit;

  ToSlot.Slot:=scsNone;

  Item:=lager_api_GetItem(FromSlot.ID)^;

  Slot:=-1;
  case Item.TypeID of
    ptPanzerung : Slot:=GetSlotIndex(scsPanzerung);
    ptGuertel   : Slot:=GetSlotIndex(scsGuertel);
    ptWaffe,ptGranate,ptSensor,ptMine:
    begin
      if fManager.LeftHand then
      begin
        if fManager.LeftHandWaffe.Gesetzt then
          Slot:=GetSlotIndex(scsRechteHand)
        else
          Slot:=GetSlotIndex(scsLinkeHand);
      end
      else
      begin
        if fManager.RightHandWaffe.Gesetzt then
          Slot:=GetSlotIndex(scsLinkeHand)
        else
          Slot:=GetSlotIndex(scsRechteHand);
      end;
    end;
  end;

  if Slot=-1 then
    exit;

  ToSlot:=fItemSlots[Slot];

  DoAction(FromSlot,ToSlot);
  // DragnDrop abbrechen
  CancelDragnDrop;

  Redraw;
end;

procedure TDXSoldatConfig.DoAction(FromSlot, ToSlot: TItemSlot);
var
  Error     : TDragActionError;
begin
  if (FromSlot.Slot<>scsNone) and (ToSlot.Slot<>scsNone) then
  begin
    // Pr�fen, ob die Aktion ausgef�hrt werden darf
    Error:=fManager.CheckAction(fManager,FromSlot,ToSlot);
    if Error<>daeNone then
      ShowErrorText(DropErrors[Error])
    else
    begin
      // Aktuellen Status speichern, um bei einem Fehler, alles zur�ckzudrehen
      fManager.BeginnEquip;
      SetLength(fNewBodenObjects,0);
      SetLength(fDeletedBodenObjects,0);
      Container.Lock;
      SetSlot(FromSlot,ToSlot);
      Container.Unlock;

      // Pr�fen, ob genug ZEs vorhanden waren und wenn nicht Ausr�stung der Einheit
      // zur�cksetzen
      Error:=fManager.EndEquip;
      if Error<>daeNone then
      begin
        RestoreExternalAction;
        ShowErrorText(DropErrors[Error]);
      end
      else
        DoExternalAction;
    end;
  end;
end;

procedure TDXSoldatConfig.BeginDragnDrop;
begin
  if fDownIndex<>-1 then
  begin
    if fItemSlots[fDownIndex].ID<>0 then
    begin
      fDragnDropSlot:=fItemSlots[fDownIndex];
      fItemSlots[fDownIndex].ID:=0;
      fDragOnSlot:=fDownIndex;
      fDragnDropCan.SetRect(fItemSlots[fDownIndex].Left,fItemSlots[fDownIndex].Top,SlotSize,SlotSize);
      fDragnDropCan.Visible:=true;
      fDragnDropMode:=true;
      fItemInfo.Visible:=false;
      fChooser.CanKeyChange:=false;
      Container.SetCapture(Self);

      Container.DeleteFrameFunction(ShowPopups,Self);
    end;
  end;
end;

function TDXSoldatConfig.GetSlotIndex(Slot: TSoldatConfigSlot): Integer;
var
  Dummy: Integer;
begin
  result:=-1;
  for Dummy:=0 to high(fItemSlots) do
  begin
    if fItemSlots[Dummy].Slot=Slot then
    begin
      result:=Dummy;
      exit;
    end;
  end;
end;

procedure TDXSoldatConfig.MouseDown(Button: TMouseButton; X, Y: Integer);
begin
  if Button=mbLeft then
    fDownIndex:=SlotAtPos(Point(Left+X,Top+Y));
end;

function TDXSoldatConfig.ShowPopups(Sender: TObject;
  Frames: Integer): Boolean;
begin
  result:=false;
  Container.DeleteFrameFunction(ShowPopups,Self);
  if Container.ActivePage<>Parent then
    exit;

  result:=true;
  if (fOverIndex<>-1) and (fItemSlots[fOverIndex].ID<>0) then
  begin
    fItemInfo.ShowNameOnly:=false;
    fItemInfo.Show(csRed,RectOfSlot(fOverIndex),lager_api_GetItem(fItemSlots[fOverIndex].ID)^);
  end;
end;

end.
