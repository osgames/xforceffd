{********************************************************************************
*										*
* Copyright (C) 2004  X-Force Team - jim_raynor@web.de				*
*										*
* This file is part of X-Force: Fight For Destiny (X-Force).			*
*										*
* X-Force is free software; you can redistribute it and/or modify		*
* it under the terms of the GNU General Public License as published by		*
* the Free Software Foundation; either version 2 of the License, or		*
* (at your option) any later version.						*
*										*
* X-Force is distributed in the hope that it will be useful,			*
* but WITHOUT ANY WARRANTY; without even the implied warranty of		*
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the			*
* GNU General Public License for more details.					*
*										*
* You should have received a copy of the GNU General Public License		*
* along with X-Force; if not, write to the Free Software			*
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA	*
*										*
*********************************************************************************
*										*
* Anzeige der Nachrichten im Geoscape mit der Checkbox (Nachricht erneut	*
* anzeigen)									*
*										*
********************************************************************************}

{$I ../Settings.inc}

unit DXGameMessage;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  DXContainer, ModalDialog, DXGroupCaption, Blending, XForce_types, DXBitmapButton,
  DXCheckBox, DXTextViewer, DirectFont, math, DirectDraw, DXDraws, KD4Utils,
  StringConst;

type
  TMessageButton = (mbStopp,mbWeiter,mbAktion);
  TDXGameMessage = class(TModalDialog)
  private
    GroupHeader       : TDXGroupCaption;
    GoonButton        : TDXBitmapButton;
    StopButton        : TDXBitmapButton;
    ActionButton      : TDXBitmapButton;
    TextViewer        : TDXTextViewer;
    ShowAgain         : TDXCheckBox;

    fText             : String;
    fLines            : TStringList;
    fType             : TLongMessage;
    fButton           : TMessageButton;
    fObject           : TObject;
    procedure SetDimension;
    procedure SetText(const Value: String);
    procedure SetType(const Value: TLongMessage);
    function GetChecked: Boolean;
    { Private-Deklarationen }
  protected
    procedure BeforeShow;override;
    procedure ButtonClick(Sender: TObject);
    { Protected-Deklarationen }
  public
    constructor Create(Page: TDXPage);override;
    destructor destroy;override;

    property Text    : String read fText write SetText;
    property MType   : TLongMessage read fType write SetType;
    property Objekt  : TObject read fObject write fObject;

    property Button  : TMessageButton read fButton;

    property Checked : Boolean read GetChecked;
    { Public-Deklarationen }
  end;

implementation

uses
  ForschList, GameFigureManager;

const
  CharsInLongText  : Integer = 500;
  MaxWidthLong     : Integer = 500;
  MaxWidthShort    : Integer = 320;
  MaxHeight        : Integer = 300;

{ TDXGameMessage }

procedure TDXGameMessage.BeforeShow;
begin
  inherited;

  GroupHeader.Font.Style:=[fsBold];
  GroupHeader.Font.Size:=Font.Size+4;
  GroupHeader.FontColor:=clYellow;
  GroupHeader.Caption:=ST0309210023;

  if Objekt<>nil then
  begin
    ActionButton.Visible:=true;
    ActionButton.Width := GroupHeader.Width div 3;
    GoonButton.Width := GroupHeader.Width div 3;   
    StopButton.Width := GroupHeader.Width div 3;
    ActionButton.Left := GoonButton.Right+1;
    StopButton.Left := GroupHeader.Right-StopButton.Width;
    StopButton.RoundCorners:=rcRightBottom;
    if Objekt is TForschList then
      ActionButton.Text:=BUfoPadie
    else if Objekt is TGameFigureManager then
      ActionButton.Text:=ST0410220002
    else
      ActionButton.Text:=ST0311060001;
  end
  else if MType=lmReserved then
  begin
    ActionButton.Visible:=false;
    GoonButton.Visible:=false;
    StopButton.Width:=GroupHeader.Width;
    StopButton.Left:=GroupHeader.Left;
    StopButton.RoundCorners:=rcBottom;
  end
  else
  begin
    ActionButton.Visible:=false;
    GoonButton.Width := GroupHeader.Width div 2;
    StopButton.Width:=GroupHeader.Width div 2;
    StopButton.Left:=GoonButton.Right+1;
    StopButton.RoundCorners:=rcRightBottom;
  end;

  TextViewer.ScrollBar.Enabled:=true;
end;

constructor TDXGameMessage.Create(Page: TDXPage);
begin
  inherited;

  { �berschrift }
  GroupHeader:=TDXGroupCaption.Create(Page);
  with GroupHeader do
  begin
    Visible:=false;
    BorderColor:=$00400000;
    BlendColor:=bcDarkNavy;
    RoundCorners:=rcTop;
    AlphaValue:=200;
    Width:=323;
    Height:=25;
    Alignment:=taCenter;
  end;
  AddComponent(GroupHeader);

  { Einstellungen f�r den TextViewer }
  TextViewer:=TDXTextViewer.Create(Page);
  with TextViewer do
  begin
    Visible:=false;
    BlendColor:=bcMaroon;
    RoundCorners:=rcNone;
    LeftMargin:=15;
    Topmargin:=8;
    Top:=GroupHeader.Bottom+1;
    Blending:=true;
    BlendValue:=200;
    BorderColor:=$00400000;
    BlendColor:=bcDarkNavy;

    // Schriften Definieren
    RegisterFont('white',WhiteStdFont);
    RegisterFont('yellow',YellowStdFont);
    RegisterFont('green',GreenStdFont);
    RegisterFont('red',RedStdFont);
    RegisterFont('lime',LimeStdFont);
    RegisterFont('blue',BlueStdFont);
    RegisterFont('maroon',MaroonStdFont);
    RegisterFont('yellowbold',YellowBStdFont);
    RegisterFont('whitebold',WhiteBStdFont);
    RegisterFont('greenbold',GreenBStdFont);
    RegisterFont('redbold',RedBStdFont);
    RegisterFont('limebold',LimeBStdFont);
    RegisterFont('bluebold',BlueBStdFont);
    RegisterFont('maroonbold',MaroonBStdFont);

  end;
  AddComponent(TextViewer);

  { Aktivieren/deaktivieren Checkbox }
  ShowAgain:=TDXCheckbox.Create(Page);
  with ShowAgain do
  begin
    Width:=GroupHeader.Width;
    Height:=23;
    BorderColor:=$00400000;
    BlendColor:=bcDarkNavy;
    RoundCorners:=rcNone;
    LeftMargin:=6;
//    OnChange:=AktivChange;
  end;
  AddComponent(ShowAgain);

  { Einstellungen f�r den OK Button }
  GoonButton:=TDXBitmapButton.Create(Page);
  with GoonButton do
  begin
    Visible:=false;
    OnClick:=ButtonClick;
    Text:=ST0311020001;
    Width:=GroupHeader.Width div 2;
    Top:=GroupHeader.Bottom+1;
    Font:=Font;
    RoundCorners:=rcLeftBottom;
    Height:=28;
    BlendAlpha:=150;
    BlendColor:=bcDarkNavy;
    FirstColor:=$00400000;
    SecondColor:=clBlue;
    AccerlateColor:=clYellow;
    Tag:=Integer(mbWeiter);
  end;
  AddComponent(GoonButton);

  { Einstellungen f�r den Aktions Button }
  ActionButton:=TDXBitmapButton.Create(Page);
  with ActionButton do
  begin
    Visible:=false;
    OnClick:=ButtonClick;
    Text:=ST0311060001;
    Width:=GroupHeader.Width div 2;
    Top:=GroupHeader.Bottom+1;
    Font:=Font;
    RoundCorners:=rcNone;
    Height:=28;
    BlendAlpha:=150;
    BlendColor:=bcDarkNavy;
    FirstColor:=$00400000;
    SecondColor:=clBlue;
    AccerlateColor:=clYellow;
    Tag:=Integer(mbAktion);
  end;
  AddComponent(ActionButton);

  { Einstellungen f�r den OK Button }
  StopButton:=TDXBitmapButton.Create(Page);
  with StopButton do
  begin
    Visible:=false;
    OnClick:=ButtonClick;
    Text:=ST0311020002;
    Width:=GroupHeader.Width div 2;
    Top:=GroupHeader.Bottom+1;
    Font:=Font;
    RoundCorners:=rcRightBottom;
    Height:=28;
    BlendAlpha:=150;
    BlendColor:=bcDarkNavy;
    FirstColor:=$00400000;
    SecondColor:=clBlue;
    AccerlateColor:=clYellow;
    Tag:=Integer(mbStopp);
  end;
  AddComponent(StopButton);

  fLines:=TStringList.Create;
end;

destructor TDXGameMessage.destroy;
begin
  fLines.Free;
  inherited;
end;

function TDXGameMessage.GetChecked: Boolean;
begin
  result:=ShowAgain.Checked;
end;

procedure TDXGameMessage.ButtonClick(Sender: TObject);
begin
  fButton:=TMessageButton((Sender as TDXComponent).Tag);
  ReadyShow;
end;

procedure TDXGameMessage.SetDimension;
var
  maxW   : Integer;
  Dummy  : Integer;
begin
  TextViewer.Height:=min(MaxHeight,fLines.Count*(-(Font.Height-4))+20);
  maxW:=150;
  for Dummy:=0 to fLines.Count-1 do
  begin
    maxW:=max(WhiteStdFont.TextWidth(fLines[dummy]),maxW);
  end;

  if Objekt<>nil then
    maxW:=((maxW div 3)*3)+1
  else if not Odd(maxW) then
    inc(maxW);

  TextViewer.Width:=maxW+40;

  StopButton.Width:=0;
  ActionButton.Width:=0;
  GoonButton.Width:=0;
  StopButton.Left:=0;
  ActionButton.Left:=0;
  GoonButton.Left:=0;
end;

procedure TDXGameMessage.SetText(const Value: String);
var
  Dummy     : Integer;
  Line      : String;
  Word      : String;
  Ch        : Char;
  LineWidth : Integer;
  Added     : boolean;
  Bis       : Integer;
  Break     : boolean;
  MaxWidth  : Integer;
begin
  fLines.Clear;
  Added:=false;
  LineWidth:=0;
  Bis:=length(Value);
  break:=false;

  if Bis>CharsInLongText then
    MaxWidth:=MaxWidthLong
  else
    MaxWidth:=MaxWidthShort;

  for Dummy:=1 to Bis do
  begin
    Ch:=Value[Dummy];
    if ch=#10 then
    begin
      break:=true;
      Line:=Line+Word;
      Word:='';
    end
    else Word:=Word+Ch;
    Added:=false;
    if (Ch=' ') or break then
    begin
      LineWidth:=LineWidth+WhiteStdFont.TextWidth(Word);
      if (LineWidth>MaxWidth-40) or break then
      begin
        fLines.Add(Line);
        LineWidth:=WhiteStdFont.TextWidth(Word);
        Line:='';
        Added:=true;
        break:=false;
      end;
      Line:=Line+Word;
      Word:='';
    end;
  end;
  if not Added then
  begin
    LineWidth:=LineWidth+WhiteStdFont.TextWidth(Word);
    if LineWidth>MaxWidth-40 then
    begin
      fLines.Add(Line);
      Line:='';
    end;
    Line:=Line+Word;
    fLines.Add(Line);
  end;
  TextViewer.Text:=Value;
end;

procedure TDXGameMessage.SetType(const Value: TLongMessage);
begin
  fType:=Value;
  StopButton.Container.Lock;

  ShowAgain.Checked:=true;
  ShowAgain.Caption:=ST0309210024;
  ShowAgain.Hint:=Format(ST0309210025,[MessageNames[Value]]);
  ShowAgain.Enabled:=Value<>lmReserved;

  SetDimension;

  GroupHeader.Width:=TextViewer.Width;
  ShowAgain.Top:=TextViewer.Bottom+1;
  ShowAgain.Width:=TextViewer.Width;

  GoonButton.Top:=ShowAgain.Bottom+1;
  StopButton.Top:=ShowAgain.Bottom+1;
  ActionButton.Top:=ShowAgain.Bottom+1;

  StopButton.Container.Unlock;
end;

end.
