{********************************************************************************
*										*
* Copyright (C) 2004  X-Force Team - jim_raynor@web.de				*
*										*
* This file is part of X-Force: Fight For Destiny (X-Force).			*
*										*
* X-Force is free software; you can redistribute it and/or modify		*
* it under the terms of the GNU General Public License as published by		*
* the Free Software Foundation; either version 2 of the License, or		*
* (at your option) any later version.						*
*										*
* X-Force is distributed in the hope that it will be useful,			*
* but WITHOUT ANY WARRANTY; without even the implied warranty of		*
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the			*
* GNU General Public License for more details.					*
*										*
* You should have received a copy of the GNU General Public License		*
* along with X-Force; if not, write to the Free Software			*
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA	*
*										*
*********************************************************************************
*										*
* Erweitertes Label mit Hintergrundfarbe und abgerundeten Ecken.		*
*										*
********************************************************************************}

{$I ../Settings.inc}

unit DXGroupCaption;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  DXDraws, DXContainer, Blending, XForce_types, KD4Utils, NGTypes, DirectDraw,
  DirectFont, TraceFile;

type
  TDXGroupCaption = class(TDXComponent)
  private
    fCaption      : String;
    fCorners      : TRoundCorners;
    fCorner       : TCorners;
    fAlpha        : Integer;
    fDrawCaption  : TDXDrawHeadRowEvent;
    fTopMargin    : Integer;
    fBlendColor   : TBlendColor;
    fBorderColor  : TBlendColor;
    fFontColor    : TColor;
    fAlignment    : TAlignment;
    fFont         : TDirectFont;
    fDrawBack     : boolean;
    procedure SetCaption(const Value: String);
    procedure SetCorners(const Value: TRoundCorners);
    procedure SetBlendColor(const Value: TBlendColor);
    procedure SetBorderColor(const Value: TColor);
    procedure SetFontCColor(const Value: TColor);
    { Private-Deklarationen }
  protected
    procedure FontChange(Sender: TObject);override;
    { Protected-Deklarationen }
  public
    constructor Create(Page: TDXPage);override;
    procedure ReDrawRect(DrawRect: TRect;Surface: TDirectDrawSurface;var Mem: TDDSurfaceDesc);override;
    procedure Draw(Surface: TDirectDrawSurface;var Mem: TDDSurfaceDesc);override;
    property Caption      : String read fCaption write SetCaption;
    property BlendColor   : TBlendColor read fBlendColor write SetBlendColor;
    property BorderColor  : TColor write SetBorderColor;
    property RoundCorners : TRoundCorners read fCorners write SetCorners;
    property AlphaValue   : Integer read fAlpha write fAlpha;
    property DrawCaption  : TDXDrawHeadRowEvent read fDrawCaption write fDrawCaption;
    property TopMargin    : Integer read fTopMargin write fTopMargin;
    property FontColor    : TColor read fFontColor write SetFontCColor;
    property Alignment    : TAlignment read fAlignment write fAlignment;
    property DrawBack     : boolean read fDrawBack write fDrawBack;
  end;

implementation

{ TDXGroupCaption }

constructor TDXGroupCaption.Create(Page: TDXPage);
begin
  inherited;
  fCorners:=rcNone;
  fFontColor:=clWhite;
  fBorderColor:=bcMaroon;
  fBlendColor:=bcMaroon;
  fAlignment:=taLeftJustify;
  fAlpha:=150;
  fTopMargin:=3;
  fFont:=FontEngine.FindDirectFont(Font,clBlack);
end;

procedure TDXGroupCaption.Draw(Surface: TDirectDrawSurface;var Mem: TDDSurfaceDesc);
begin
  RedrawRect(ClientRect,Surface,Mem);
end;

procedure TDXGroupCaption.FontChange(Sender: TObject);
begin
  if Font.Color<>clWhite then
  begin
    Font.Color:=clWhite;
    // Durch das Setzen der Farbe wird erneut OnChange aufgerufen
    // Da dann die Farbe wei� ist wird die Schrift erstellt
    exit;
  end;
  fFont:=FontEngine.FindDirectFont(Font,clBlack);
end;

procedure TDXGroupCaption.ReDrawRect(DrawRect: TRect;
  Surface: TDirectDrawSurface;var Mem: TDDSurfaceDesc);
var
  TextLeft  : Integer;
begin
  IntersectRect(DrawRect,DrawRect,ClientRect);
  if AlphaElements or DrawBack then
    BlendRoundRect(ClientRect,fAlpha,fBlendColor,Surface,Mem,11,fCorner,DrawRect);
  FramingRect(Surface,Mem,ClientRect,fCorner,11,fBorderColor);
  if Assigned(fDrawCaption) then
    fDrawCaption(Self,Surface,ClientRect)
  else
  begin
    TextLeft:=0;
    case fAlignment of
      taLeftJustify  : TextLeft:=Left+6;
      taRightJustify : TextLeft:=Right-fFont.TextWidth(fCaption)-6;
      taCenter       : TextLeft:=Left+(Width shr 1)-(fFont.TextWidth(fCaption) shr 1);
    end;
    fFont.Draw(Surface,TextLeft,Top+fTopMargin,fCaption);
  end;
end;

procedure TDXGroupCaption.SetBlendColor(const Value: TBlendColor);
begin
  fBlendColor := Value;
  Redraw;
end;

procedure TDXGroupCaption.SetBorderColor(const Value: TColor);
begin
  fBorderColor := Container.Surface.ColorMatch(Value);
  Redraw;
end;

procedure TDXGroupCaption.SetCaption(const Value: String);
begin
  fCaption := Value;
  Redraw;
end;

procedure TDXGroupCaption.SetCorners(const Value: TRoundCorners);
begin
  fCorners := Value;
  fCorner:=[];
  if fCorners in [rcLeft,rcTop,rcLeftTop,rcAll] then include(fCorner,cLeftTop);
  if fCorners in [rcRight,rcTop,rcRightTop,rcAll] then include(fCorner,cRightTop);
  if fCorners in [rcLeft,rcBottom,rcLeftBottom,rcAll] then include(fCorner,cLeftBottom);
  if fCorners in [rcRight,rcBottom,rcRightBottom,rcAll] then include(fCorner,cRightBottom);
  RecreateMouseRegion;
  Redraw;
end;

procedure TDXGroupCaption.SetFontCColor(const Value: TColor);
begin
  fFontColor := Value;
  Font.OnChange:=nil;
  Font.Color:=Value;
  fFont:=FontEngine.FindDirectFont(Font,clBlack);
  Font.OnChange:=FontChange;
end;

end.
