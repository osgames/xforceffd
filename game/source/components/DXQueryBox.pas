{********************************************************************************
*										*
* Copyright (C) 2004  X-Force Team - jim_raynor@web.de				*
*										*
* This file is part of X-Force: Fight For Destiny (X-Force).			*
*										*
* X-Force is free software; you can redistribute it and/or modify		*
* it under the terms of the GNU General Public License as published by		*
* the Free Software Foundation; either version 2 of the License, or		*
* (at your option) any later version.						*
*										*
* X-Force is distributed in the hope that it will be useful,			*
* but WITHOUT ANY WARRANTY; without even the implied warranty of		*
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the			*
* GNU General Public License for more details.					*
*										*
* You should have received a copy of the GNU General Public License		*
* along with X-Force; if not, write to the Free Software			*
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA	*
*										*
*********************************************************************************
*										*
* Stellt einen einfachen Ja/Nein Dialog zur Verf�gung				*
*										*
********************************************************************************}

{$I ../Settings.inc}

unit DXQueryBox;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  DXContainer, DXDraws, DXBitmapButton, math, DXEdit, Defines, DirectDraw,
  KD4Page, TraceFile, Blending, XForce_types, DXClass, ModalDialog, DXGroupCaption,
  DirectFont, StringConst;

type
  TDXQueryBox = class(TModalDialog)
  private
    GroupHeader   : TDXGroupCaption;
    CancelButton  : TDXBitmapButton;
    OKButton      : TDXBitmapButton;
    Editor        : TDXEdit;
    fText         : String;
    fMaxLength    : Integer;
    function GetCaption: String;
    procedure SetCaption(const Value: String);

    procedure ButtonClick(Sender: TObject);
    procedure SetText(const Value: String);
    procedure ChangeText(Sender: TObject);
    procedure EditKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure SetColors;
    { Private-Deklarationen }
  protected
    procedure BeforeShow;override;
    { Protected-Deklarationen }
  public
    constructor Create(Page: TDXPage);override;
    property Caption        : String read GetCaption write SetCaption;
    property Text           : String read fText write SetText;
    property MaxLength      : Integer read fMaxLength write fMaxLength;
  end;


implementation


{ TDXQueryBox }

procedure TDXQueryBox.BeforeShow;
begin
  inherited;
  GroupHeader.Font.Style:=[fsBold];
  GroupHeader.Font.Size:=Font.Size+4;
  GroupHeader.FontColor:=clYellow;
  Editor.MaxLength:=fMaxLength;
  Editor.SelectAll;
end;

procedure TDXQueryBox.ButtonClick(Sender: TObject);
begin
  ReadyShow;
  if (Sender as TDXBitmapButton).Tag=0 then
    fText:=Editor.Text
  else
    fText:='';
end;

procedure TDXQueryBox.ChangeText(Sender: TObject);
begin
  OKButton.Enabled:=not (Editor.Text=EmptyStr);
end;

constructor TDXQueryBox.Create(Page: TDXPage);
begin
  inherited;

  fMaxLength:=15;
  
  { Einstellungen f�r den OK Button }
  OKButton:=TDXBitmapButton.Create(Page);
  with OKButton do
  begin
    Visible:=false;
    OnClick:=ButtonClick;
    Text:=BOKWA;
    Width:=116;
    Top:=50;
    Font:=Font;
    RoundCorners:=rcLeftBottom;
    Height:=28;
    BlendAlpha:=150;
    Tag:=0;
  end;
  AddComponent(OKButton);

  { Einstellungen f�r den Abbrechen Button }
  CancelButton:=TDXBitmapButton.Create(Page);
  with CancelButton do
  begin
    Visible:=false;
    Text:=BCancelWA;
    Width:=116;
    Left:=117;
    Top:=50;
    Font:=Font;
    Height:=28;
    RoundCorners:=rcRightBottom;
    OnClick:=ButtonClick;
    BlendAlpha:=150;
    Tag:=1;
  end;
  AddComponent(CancelButton);

  { Einstellungen f�r Eingabefeld }
  Editor:=TDXEdit.Create(Page);
  with Editor do
  begin
    Visible:=false;
    Width:=233;
    Height:=23;
    Top:=26;
    BorderColor:=clAqua;
    FocusColor:=$00600000;
    OnKeyUp:=EditKeyDown;
    RoundCorners:=rcNone;
    BlendAlpha:=150;
    Font.Name:=coFontName;
    OnChange:=ChangeText;
  end;
  AddComponent(Editor);

  { Einstellungen f�r Eingabefeld }
  GroupHeader:=TDXGroupCaption.Create(Page);
  with GroupHeader do
  begin
    Visible:=false;
    BorderColor:=$00400000;
    BlendColor:=bcDarkNavy;
    RoundCorners:=rcTop;
    Width:=233;
    Height:=25;
    Alignment:=taCenter;
  end;
  AddComponent(GroupHeader);

  SetColors;
end;

procedure TDXQueryBox.EditKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key=VK_RETURN then
    OKButton.DoClick
  else if Key=VK_ESCAPE then
    CancelButton.DoClick;
end;

function TDXQueryBox.GetCaption: String;
begin
  result:=GroupHeader.Caption;
end;

procedure TDXQueryBox.SetCaption(const Value: String);
begin
  GroupHeader.Caption:=Value;
end;

procedure TDXQueryBox.SetColors;
begin
  OKButton.BlendColor:=bcDarkNavy;
  CancelButton.BlendColor:=bcDarkNavy;
  Editor.BlendColor:=bcDarkNavy;
  if AlphaElements then
  begin
    OKButton.FirstColor:=$00400000;
    CancelButton.FirstColor:=$00400000;
  end
  else
  begin
    OKButton.FirstColor:=$00800000;
    CancelButton.FirstColor:=$00800000;
  end;
  OKButton.SecondColor:=clBlue;
  CancelButton.SecondColor:=clBlue;
end;

procedure TDXQueryBox.SetText(const Value: String);
begin
  fText := Value;
  Editor.Text:=fText;
end;

end.
