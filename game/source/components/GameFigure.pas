{********************************************************************************
*										*
* Copyright (C) 2004  X-Force Team - jim_raynor@web.de				*
*										*
* This file is part of X-Force: Fight For Destiny (X-Force).			*
*										*
* X-Force is free software; you can redistribute it and/or modify		*
* it under the terms of the GNU General Public License as published by		*
* the Free Software Foundation; either version 2 of the License, or		*
* (at your option) any later version.						*
*										*
* X-Force is distributed in the hope that it will be useful,			*
* but WITHOUT ANY WARRANTY; without even the implied warranty of		*
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the			*
* GNU General Public License for more details.					*
*										*
* You should have received a copy of the GNU General Public License		*
* along with X-Force; if not, write to the Free Software			*
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA	*
*										*
*********************************************************************************
*										*
* Stellt Funktionen zur Verwaltung einer Einheit im Bodeneinsatz zur Verf�gung	*
*										*
********************************************************************************}

{$I ../Settings.inc}

unit GameFigure;

interface

uses
  Windows, math, Classes, XForce_types, DXDraws, KD4Utils, GameFigureManager,
  DirectFont, ISOTools, ISOMessages, PathFinder, TraceFile, StringConst,
  Blending, DirectDraw, SysUtils, Forms, Defines, NotifyList,typinfo, XANBitmap;

const
  // Event GameFigure
  EVENT_FIGURE_SEEUNIT                = 0; // Einheit hat Sender gesichtet
  EVENT_FIGURE_TREFFER                = 1; // Einheit wurde von Sender getroffen
  EVENT_FIGURE_ONDESTROY              = 2; // TGameFigure-Objekt zerst�rt
  EVENT_FIGURE_WAYTODESTBLOCKED       = 3; // Weg zum Ziel wird blockiert
  EVENT_FIGURE_UNDERFIRE              = 4; // Einheit befindet sich unter Feuer (sieht Projektil)

  // Events GameFigureList
  EVENT_SEEUNITS   = 0;
  EVENT_UNITSHIDE  = 1;

type
  PSeeTile = ^TSeeTile;
  TSeeTile = record
    SeeingRange : Integer;
    HasIncrease : boolean;  // Hier wird eingetragen, wenn das Feld �ber IncSeeing �bertragen wurde
  end;

  TNeedTileRect = function(Sender: TObject;Point: TPoint): TRect of object;

  TGameFigureList = class;

  TGameFigure = class(TObject)
  private
    fViewDirection  : TViewDirection;

    fFramesPerDirection : Integer;

    fYPos           : Integer;
    fXPos           : Integer;
    fFigureStatus   : TFigureStatus;
    fName           : String;
    fList           : TGameFigureList;
    fSeeFigures     : Array of TGameFigure;      // Alle gesichteten Aliens speichern
    fAllSeeFigures  : Array of TGameFigure;      // Alle jemals gesichteten Aliens (f�r EXP Berechnung)
    fWay            : TWay;
    fLastShowIndex  : Integer;
    fIncreasePos    : TPoint;
    fSehWeite       : Integer;

    fFrame          : Integer;
    fMoving         : boolean;
    fHasMoveTU      : boolean;

    fEchtzeit       : boolean;
    fFiller         : boolean;
    fManager        : TGameFigureManager;
    fSeeCount       : Integer;
    fDead           : boolean;
    fZielUnit       : TGameFigure;
    fImRaumschiff   : boolean;

    {$IFNDEF DISABLEBEAM}
    fCanBeam        : boolean;
    fFirstBeam      : boolean;
    {$ENDIF}

    fViewTo         : TPoint;
    fLastPoint      : TPoint;
    fSavedUnit      : TGameFigure;
    fLastVisible    : boolean;
    fInjuredStatus  : Integer;
    fState          : set of TUnitState;

    fManagerDestroy : TEventHandle;

    SENachladError  : boolean;
    fHintObject     : TObject;

    fSelected       : boolean;
    fRemoveSelection: Boolean;

    fLaufEXP        : Integer;  // Erfahrungspunkte f�rs Laufen sammeln
    fSchussEXP      : Integer;  // Erfahrungspunkte f�rs Schiessen sammeln
    fNotifyList     : TNotifyList;
    fEinsatzKI      : TObject;

    fModelInfo      : TModelInformation;

    // Zeiger auf die Einheitenanimationen
    fDeadImage      : TXANBitmap;
    fImages         : TXANBitmap;

    fDrawingRect    : TRect;

    // Zeiteinheiten
    fTimeUnits      : Integer;
    fReservedTU     : Integer;

    procedure SetXPos(const Value: Integer);
    procedure SetYPos(const Value: Integer);
    procedure SetSehWeite(const Value: Integer);
    procedure RecalcSeeingRange;
    procedure SetViewDirection(const Value: TViewDirection);
    procedure FindNextAlien;
    procedure SetManager(const Value: TGameFigureManager);
    function GetMaxTimeUnits: Integer;
    function GetZiel: TPoint;
    procedure IncSeeing;
    procedure DecSeeing;

    procedure SetSelected(const Value: boolean);

    procedure Dead;
    procedure ClearSeeingList;
    procedure AddFigureToSeeList(Figure: TGameFigure);
    procedure RemoveFromSeeingList(Figure: TGameFigure);
    procedure CheckSeeing(Figure: TGameFigure);

    procedure AktuFigureRect;
    function GetIsOnField: boolean;
    procedure DoMove;
    function GetMoving: boolean;
    function CheckOffset(XOff, YOff: Integer;const WantTo: TPOint): boolean;
    function GetSeeUnit(Index: Integer): TGameFigure;

    procedure CountSeeUnits;
    procedure SetFigureStatus(const Value: TFigureStatus);
    procedure CalcEXPSeeing(Figure: TGameFigure);

    procedure EventManagerDestroy(Sender: TObject);
    function GetName: String;

    function GetTrefferZone(X,Y: Integer;Region: TBodyRegion): TTrefferZone;overload;
    procedure SetTimeUnits(const Value: Integer);
    function GetPosition: TPoint;
  public
    SeeingTiles     : Array of Array of TSeeTile;
    AlphaMap        : Array of Array of Word;    // Alphamap abh�ngig von Details beim Schattenverlauf
    constructor Create(List: TGameFigureList);virtual;
    destructor destroy;override;
    procedure Draw(Surface : TDirectDrawSurface;Mem: TDDSurfaceDesc;X,Y: Integer;Xpos,YPos: Integer);
    function GetUnitOffset: TPoint;

    function MoveTo(Point: TPoint; NoWait: Boolean = false; AddWay: Boolean = false): boolean;
    // Im Rundenmodus muss erst mit MoveTo das Ziel festgelegt werden und entweder
    // durch einen zweiten Aufruf von MoveTo mit den gleichen Parametern best�tigt
    // werden oder durch den Aufruf von GiveOK
    procedure GiveOK;

    procedure Select;
    procedure ChangeSelected(Selection: Boolean);

    procedure NextRound;
    procedure ResetNeedTUState;

    procedure EndRound;
    procedure CenterToNextAlien;
    procedure ShowInfoText(Visible: Boolean);
    procedure AktuSeeingList;
    function CanSeeUnit(Figure: TGameFigure): boolean;
    function CanSeePoint(X,Y: Integer): boolean;

    procedure RecalcDrawingRect;

    // Einheiten schiessen
    function  ShootToUnit(Figure: TGameFigure; MousePos: TPoint): Boolean;overload;
    function  ShootToUnit(Figure: TGameFigure): Boolean;overload;
    function  ShootToUnit(Figure: TGameFigure; Slot: TSoldatConfigSlot; ShootInfos: PShootInfos = nil): Boolean;overload;

    function  ShootToPos(Pos: TPoint; Slot: TSoldatConfigSlot; ShootInfos: PShootInfos = nil): Boolean;

    // Bewegung
    function ViewTo(Point: TPoint;RecalcSight: boolean = true): boolean;
    function StopMove: boolean;
    function MoveUnit(Frames: Integer): boolean;
    function IsVisible: boolean;

    function GetBodyRegion(ZAxis: Integer): TBodyRegion;

    function GetTargetInfoText(ZielPos: TPoint; X,Y: Integer): String;

    procedure SetUnitToPos(Pos: TPoint);

    function NeedTU(TU: Integer): TNeedTimeUnitResult;
    procedure NoTUforAction;

    procedure ShowFriendMessage(Mess: String);

    procedure BerechneExp(Points: Integer; Typ: TExpType);

    procedure TakeItem(Item: TObject);

    // Pr�ft, ob der angegebene Offset eines Tiles innerhalb des Zielradius
    // der Einheit ist
    function GetTrefferZone(ISOObject: TObject): TTrefferZone;overload;

    // ermittelt die Position in der Grafik der Einheit unter ber�cksichtigung
    // der aktuellen Position auf der Sichtbaren Karte. �bergeben wird die
    // Absolute Cursor Position
    function GetImagePosition(AbsX,AbsY: Integer): TPoint;

    // ermittelt die ZAxis unter ber�cksichtigung der aktuellen Position auf
    // der Sichtbaren Karte. �bergeben wird die
    // Absolute Cursor Position
    function GetTargetHeight(AbsX,AbsY: Integer): Integer;

    function CheckHover(X,Y: Integer): Boolean;

    function DoTreffer(ISOObject: TObject): boolean;
    procedure KillUnit;
    procedure ZaehleAbschuss;

    {$IFDEF DEBUGMODE}
    function GetStateText: String;
    {$ENDIF}

    function SetToGameField: Boolean;
    procedure RemoveFromGameField;

    {$IFNDEF DISABLEBEAM}
    function BeamTo(X,Y: Integer): Boolean;
    procedure BeamUpReady(Sender: TObject);
    procedure BeamDownReady(Sender: TObject);
    function BeamUp: Boolean;
    {$ENDIF}

    function MakeWayFree(WantTo: TPoint): boolean;
    procedure GoBack;

    function RoundFinished: Boolean;

    property ViewDirection       : TViewDirection read fViewDirection write SetViewDirection;

    property Zeiteinheiten       : Integer read fTimeUnits write SetTimeUnits;
    property ReservedTimeUnits   : Integer read fReservedTU write fReservedTU;

    property MaxZeiteinheiten    : Integer read GetMaxTimeUnits;
    property SehWeite            : Integer read fSehWeite write SetSehWeite;
    property XPos                : Integer read fXPos write SetXPos;
    property YPos                : Integer read fYPos write SetYPos;
    property Position            : TPoint read GetPosition;
    property FigureStatus        : TFigureStatus read fFigureStatus write SetFigureStatus;
    property Name                : String read GetName write fName;
    property EchtZeit            : boolean read fEchtzeit write fEchtzeit;
    property Manager             : TGameFigureManager read fManager write SetManager;
    property Ziel                : TPoint read GetZiel;
    property IncreasePos         : TPoint read fIncreasePos write fIncreasePos;

    property IsDead              : boolean read fDead;
    property IsOnField           : boolean read GetIsOnField;
    property ImRaumschiff        : boolean read fImRaumschiff write fImRaumschiff;

    {$IFNDEF DISABLEBEAM}
    property IsFirstBeam         : boolean read fFirstBeam;
    property CanBeam             : boolean read fCanBeam write fCanBeam;
    {$ENDIF}

    property IsMoving            : boolean read GetMoving;

    // Einheit war am Beginn der Runde sichtbar
    property LastVisible         : boolean read fLastVisible;

    property Way                 : TWay read fWay;

    // Images holen
    property WalkImageSurface    : TXANBitmap read fImages;
    property DeadImageSurface    : TXANBitmap read fDeadImage;

    property Selected            : boolean read fSelected write SetSelected;

    // Sichtbereich
    property SeeCount            : Integer read fSeeCount;
    property SeeUnit[Index: Integer]: TGameFigure read GetSeeUnit;

    // Einstellungen f�r Einheit
    property EinsatzKI           : TObject read fEinsatzKI write fEinsatzKI;

    property NotifyList          : TNotifyList read fNotifyList;

    property DrawingRect         : TRect read fDrawingRect;

  end;

  TGameFigureList = class
  private
    fNeedTileRect   : TNeedTileRect;
    fSurfaceList    : TList;
    fPathFinder     : TPathFinder;
    fNotifyList     : TNotifyList;
    fEnemySeeing    : Integer;
    fEnemys         : Integer;
    procedure AddFigure(Figure: TGameFigure);
    procedure DeleteFigure(Figure: TGameFigure);
    function GetCount: Integer;
    function GetFigure(Index: Integer): TGameFigure;

    procedure EinheitGestorben(Figure: TGameFigure);
    procedure EinheitAufFeldPlaziert(Figure: TGameFigure);
    function GetMapXSize: Integer;
    function GetMapYSize: Integer;

    procedure IncEnemysSeeing;
    procedure DecEnemysSeeing;
    function GetSelected: TGameFigure;
    procedure SetSelected(const Value: TGameFigure);
  public
    fList           : TList;
    constructor Create;
    destructor destroy;override;
    procedure AktuSeeingLists;
    procedure WaffenPosChanged(Obj: TObject);

    procedure AktuSeeing;

    procedure Clear;
    procedure NextRound;
    procedure WaitForRoundEnd;

    procedure CountEnemys;

    function GetFigureOfManager(Manager: TGameFigureManager): TGameFigure;

    function SelectedCount: Integer;

    property Count: Integer read GetCount;
    property Figure[Index: Integer] : TGameFigure read GetFigure;default;
    property OnNeedTileRect      : TNeedTileRect read fNeedTileRect write fNeedTileRect;
    property SurfaceList         : TList read fSurfaceList write fSurfaceList;
    property PathFinder          : TPathFinder read fPathFinder write fPathFinder;
    property MapXSize            : Integer read GetMapXSize;
    property MapYSize            : Integer read GetMapYSize;

    property Enemys              : Integer read fEnemys;

    property NotifyList          : TNotifyList read fNotifyList;

    property Selected            : TGameFigure read GetSelected write SetSelected;
  end;

  TFigureGroup = class
  private
    fList      : TGameFigureList;
    fFigures   : Array of TGameFigure;
    function GetUnitCount: Integer;
  public
    constructor Create(List: TGameFigureList);
    procedure AddUnit(Figure: TGameFigure);
    procedure AddAll;
    procedure Clear;
    procedure SelectGroup;
    procedure CreateGroup;

    function UnitInGroup(Figure: TGameFigure): boolean;

    property  UnitCount: Integer read GetUnitCount;
  end;

  const
    SelectedRect : TRect = (Left: 402; Top: 0; Right: 466; Bottom: 32);

    NormalOffset = 32/8;
    SmallOffset  = NormalOffSet/2;
    LargeOffset  = NormalOffSet*2;

implementation

uses
  DXISOEngine, ISOObjects, ISOWaffenObjects, EinsatzKI, lager_api;

var
  SeeingData: Array[1..100] of Pointer;
  NeededMem : Integer;

{ Verwaltung der Seefelder }

function GetSeeingData(SehWeite: Integer): Pointer;
var
  Direct             : TViewDirection;
  Ptr                : PSeeTile;
  StartW             : Integer;
  EndW               : Integer;
  OStartW,OEndW      : Integer;
  ConvNull           : Boolean;
  fTemp              : Array of Array of Integer;
  Umfeld             : Integer;
  UmfeldCount        : Integer;
  X,Y                : Integer;
  XSquare            : Integer;
  Winkel             : Integer;
  MaxValue           : Integer;
  Temp               : PInteger;

  function InSehWinkel(StartW,EndW: Integer): boolean;
  begin
    result:=false;
    if ConvNull and (Winkel=0) then Winkel:=360;
    if (StartW<EndW) then
    begin
      if (Winkel>=StartW) and (Winkel<=EndW) then
        result:=true;
    end
    else
    begin
      if (Winkel>=StartW) or (Winkel<=EndW) then
        result:=true;
    end;
  end;

  function WinkelRandBereich: boolean;
  begin
    if Direct=vdAll then
      result:=false
    else
      result:=not InSehWinkel(OStartW,OEndW);
  end;

begin
  // Pr�fen, ob zul�ssige Sehweite
  if (SehWeite<low(SeeingData)) or (Sehweite>high(SeeingData)) then
    raise Exception.CreateFmt('Sehweite liegt nicht innerhalb der erlaubten Gr��e (%d)',[Sehweite]);
  // Speicher anfordern
  if SeeingData[Sehweite]<>nil then
  begin
    result:=SeeingData[Sehweite];
    exit;
  end;
  GetMem(Ptr,((Sehweite*2)+1)*((Sehweite*2)+1)*9*Sizeof(TSeeTile));
  inc(NeededMem,((Sehweite*2)+1)*((Sehweite*2)+1)*9*Sizeof(TSeeTile));
  SetLength(fTemp,(Sehweite*2)+1,(Sehweite*2)+1);
  result:=Ptr;
  SeeingData[SehWeite]:=result;
  for Direct:=vdLeft to vdAll do
  begin
    StartW:=-1;
    EndW:=-1;
    ConvNull:=false;
    case Direct of
      vdTop          : begin;StartW:=225;EndW:=315;end;
      vdRightTop     : begin;StartW:=270;EndW:=360;ConvNull:=true;end;
      vdRight        : begin;StartW:=315;EndW:= 45;end;
      vdRightBottom  : begin;StartW:=  0;EndW:= 90;end;
      vdBottom       : begin;StartW:= 45;EndW:=135;end;
      vdLeftBottom   : begin;StartW:= 90;EndW:=180;end;
      vdLeft         : begin;StartW:=135;EndW:=225;end;
      vdLeftTop      : begin;StartW:=180;EndW:=270;end;
      vdAll          : begin;StartW:=  0;EndW:=360;end;
    end;
    OStartW:=StartW;
    OEndW:=EndW;
    if Direct<>vdAll then
    begin
      StartW:=StartW-20;
      EndW:=EndW+20;
      if StartW<0 then inc(StartW,360);
      if EndW>360 then dec(EndW,360);
    end;
    for X:=0 to (Sehweite shl 1) do
    begin
      Temp:=Addr(fTemp[X,0]);
      XSquare:=Sqr(X-Sehweite);
      for Y:=0 to (Sehweite shl 1) do
      begin
        Winkel:=CalculateWinkel(Point(X,Y),Point(SehWeite,SehWeite));
        if InSehWinkel(StartW,EndW) then
        begin
          if WinkelRandBereich then
            MaxValue:=4
          else
            MaxValue:=8;
          Temp^:=max(0,min(MaxValue,SehWeite-round(Sqrt(XSquare+Sqr(Y-SehWeite)))))
        end
        else
          Temp^:=0;
        inc(Temp);
      end;
    end;
    fTemp[SehWeite,SehWeite]:=8;
    for X:=0 to (SehWeite shl 1) do
    begin
      for Y:=0 to (SehWeite shl 1) do
      begin
        Umfeld:=fTemp[X,Y];
        UmfeldCount:=1;
        if (X>0) then
        begin
          inc(Umfeld,fTemp[X-1,Y]);
          inc(UmfeldCount);
        end;
        if (Y>0) then
        begin
          inc(Umfeld,fTemp[X,Y-1]);
          inc(UmfeldCount);
        end;
        if (X<(SehWeite shl 1)) then
        begin
          inc(Umfeld,fTemp[X+1,Y]);
          inc(UmfeldCount);
        end;
        if (Y<(SehWeite shl 1)) then
        begin
          inc(Umfeld,fTemp[X,Y+1]);
          inc(UmfeldCount);
        end;
        PSeeTile(Ptr).HasIncrease:=false;
        PSeeTile(Ptr).SeeingRange:=Umfeld div UmfeldCount;
        inc(Ptr);
      end;
    end;
  end;
end;

function WayPointToPoint(Way: TWayPoint): TPoint;
begin
  result.X:=Way.X;
  result.Y:=Way.Y;
end;

function PointToWayPoint(Point: TPoint): TWayPoint;
begin
  result.x:=Point.X;
  result.y:=Point.Y;
  result.Reached:=false;
end;

{ TGameFigure }

// Einheit in die gesichtete Liste aufnehmen und im Echtzeitmodus
// als Ziel einstellen
procedure TGameFigure.AddFigureToSeeList(Figure: TGameFigure);
var
  Dummy  : Integer;
  Index  : Integer;
begin

// Pr�fen, ob Einheit bereits gesichtet wurde
  Index:=-1;
  for Dummy:=0 to high(fSeeFigures) do
  begin
    // gesichtete Einheit ist bereits in der Liste
    if fSeeFigures[Dummy]=Figure then exit;

    // Freies Feld im Array gefunden, Schleife l�uft weiter, da eventuell
    // EInheit noch im Array drin ist
    if (Index=-1) and (fSeeFigures[Dummy]=nil) then
      Index:=Dummy;
  end;

  // Array ist voll -> Erweitern
  if Index=-1 then
  begin
    Index:=length(fSeeFigures);
    SetLength(fSeeFigures,Index+5);
  end;

// Einheit wurde gesichtet
  fSeeFigures[Index]:=Figure;

  // Als Feind z�hlen
  if Figure.FigureStatus=fsEnemy then
  begin
    fList.IncEnemysSeeing;
    CalcEXPSeeing(Figure);
  end;

{  // Ziel setzen
  if EchtZeit and (GetTarget=nil) then
    SetTarget(Figure);

  // Einheit stoppen, wenn KI so eingestellt ist
  if FigureAI in [faiNormal,faiCarefully] then
    StopMove;}

  fNotifyList.CallEvents(EVENT_FIGURE_SEEUNIT,Figure);

  if (fManager<>nil) then
  begin
    Rec.WantBreakUnit:=Self;
    Rec.BreakingUnit:=Figure;
    SendVMessage(vmBreakRound);
    if Rec.Result then
      StopMove;
  end;

  Figure.RecalcDrawingRect;
end;

// Einheit neu zeichnen
procedure TGameFigure.AktuFigureRect;
begin
  Assert(fList<>nil);
  Rec.Rect:=fList.OnNeedTileRect(Self,Point(XPos,YPos));
  with Rec.Rect do
  begin
    inc(Bottom,30);
    dec(Top,30);
    inc(Right,60);
    dec(Left,60);
  end;
  SendVMessage(vmAktuTempSurfaceRect);

  RecalcDrawingRect;
end;

// Sehliste aktualisieren
procedure TGameFigure.AktuSeeingList;
var
  Dummy  : Integer;
  Fig    : TGameFigure;
begin
//  GlobalFile.Write(Name);
  if fDead then
  begin
    ClearSeeingList;
    exit;
  end;

  for Dummy:=0 to fList.Count-1 do
    CheckSeeing(fList[Dummy]);

  for Dummy:=0 to High(fSeeFigures) do
  begin
    Fig:=fSeeFigures[Dummy];
    if Fig<>nil then
    begin
      if (not Fig.IsVisible) or (not Fig.IsOnField) or (Fig.IsDead) then
      begin
        RemoveFromSeeingList(Fig);
        continue;
      end;
      if not CanSeePoint(Fig.XPos,Fig.YPos) then
        RemoveFromSeeingList(Fig);

    end;
  end;
  CountSeeUnits;
end;

{$IFNDEF DISABLEBEAM}
// Rauf-Teleportieren ist abgeschlossen
procedure TGameFigure.BeamUpReady(Sender: TObject);
begin
  CanBeam:=true;
  Rec.NotifyUIType:=nuitBeamReady;
  SendVMessage(vmNotifyUI);
end;

// Runter-Teleportieren ist abgeschlossen
procedure TGameFigure.BeamDownReady(Sender: TObject);
begin
  SetToGameField;
  if fEchtzeit then
    Selected:=true
  else
    Select;
  SendVMessage(vmAktuTempSurface);
  CanBeam:=true;
  Rec.NotifyUIType:=nuitBeamReady;
  SendVMessage(vmNotifyUI);
end;

// Einheit ins Kampfgeschehen beamen und erforderliche
// Einstellungen t�tigen
function TGameFigure.BeamTo(X, Y: Integer): Boolean;
begin
  result:=false;
  if not CanBeam then
    exit;

  Rec.Point:=Point(X,Y);
  SendVMessage(vmCanBeamTo);

  if not rec.Result then
    exit;

  if (not IsFirstBeam) and (not NeedTU(TUBeam)) then
    exit;

  XPos:=X;
  YPos:=Y;
  fFirstBeam:=false;
  Rec.Bool:=true;
  Rec.Figure:=Self;
  SendVMessage(vmCreateBeamObject);
  ImRaumschiff:=false;
  CanBeam:=false;
  result:=true;
end;

// Einheit hochbeamen
function TGameFigure.BeamUp: boolean;
begin
  result:=false;
  if not CanBeam then
    exit;

  SendVMessage(vmContainerLock);
  if not NeedTU(TUBeam) then
  begin
    SendVMessage(vmContainerUnLock);
    exit;
  end;

  StopMove;

  ImRaumschiff:=true;

  // Beamanimation erstellen
  Rec.Figure:=Self;
  Rec.Bool:=false;
  SendVMessage(vmCreateBeamObject);

  ShowInfoText(false);
  CanBeam:=false;

  SendVMessage(vmContainerUnLock);
  RemoveFromGameField;
  result:=true;
end;
{$ENDIF}

// Sucht in der Sehliste nach Figure
function TGameFigure.CanSeeUnit(Figure: TGameFigure): boolean;
var
  Dummy: Integer;
begin
  result:=false;
  if Figure=nil then
    exit;

  // Erstmal schauen, ob die Einheit in der Liste der gesichteten Einheiten steht
  for Dummy:=0 to High(fSeeFigures) do
  begin
    if fSeeFigures[Dummy]=Figure then
    begin
      result:=true;
      exit;
    end;
  end;
end;

// Auf n�chsten Feind zentrieren
procedure TGameFigure.CenterToNextAlien;
begin
  if fSeeCount=0 then exit;
  FindNextAlien;
  Rec.Point:=Point(fSeeFigures[fLastShowIndex].XPos,fSeeFigures[fLastShowIndex].YPos);
  SendVMessage(vmCenterPoint);
end;

// Sehliste l�schen
procedure TGameFigure.ClearSeeingList;
var
  Dummy: Integer;
begin
  for Dummy:=0 to high(fSeeFigures) do
  begin
    if (fSeeFigures[Dummy]<>nil) and (fSeeFigures[Dummy].FigureStatus=fsEnemy) then
      fList.DecEnemysSeeing;
  end;
  SetLength(fSeeFigures,0);
  fSeeCount:=0;
end;

constructor TGameFigure.Create(List: TGameFigureList);
begin
  fNotifyList:=TNotifyList.Create;

  fInjuredStatus:=0;

  {$IFNDEF DISABLEBEAM}
  fFirstBeam:=true;
  fCanBeam:=true;
  {$ENDIF}

  fSeeCount:=0;
  fMoving:=false;
  fFrame:=0;
  fFiller:=true;
  fList:=List;
  fDead:=false;
  fImRaumschiff:=true;
  fSavedUnit:=nil;
  fLastVisible:=false;
  fState:=[usReady];
  fRemoveSelection:=false;

  fLaufExp:=0;
  fSchussEXP:=0;

  fWay:=TWay.Create;
  fWay.SetFigurePtr(Self);

  List.AddFigure(Self);

  fReservedTU:=0;

  Rec.Figure:=Self;
  SendVMessage(vmCreateHintForFigure);
  fHintObject:=(Rec.resultObj as TFigureObject);
  Assert(fHintObject<>nil);
end;

// Einheit stirbt
procedure TGameFigure.Dead;
begin
  if fDead then
    exit;

  GlobalFile.Write('Einheit get�tet',Name);

  // Todesnachricht abschicken
  Rec.Figure:=Self;
  SendVMessage(vmCanDrawUnit);

  // Nur wenn die Einheit sichtbar ist Nachricht verschicken
  if Rec.Result then
  begin
    Rec.Text:=Format(ST0309260001,[Name]);
    Rec.Point:=Point(XPos,YPos);

    if FigureStatus=fsFriendly then
      Rec.MessageType:=emFriendDie
    else if FigureStatus=fsEnemy then
      Rec.MessageType:=emEnemyDie;

    SendVMessage(vmMessage);
  end;

  // Einstellungen �bernehmen
  fDead:=true;

  Selected:=false;

  if fManager<>nil then
    fManager.Dead;

  // Punkte berechnen
  if FigureStatus=fsFriendly then
  begin
    SendVMessage(vmRedrawSolList);
    BuchPunkte(bptSoldatTot);
  end
  else if FigureStatus=fsEnemy then
    BuchPunkte(bptAlienTot);

  // Liste �ber Tot benachrichtigen
  fList.EinheitGestorben(Self);

  // Sterbeanimation erstellen und Einheit vom Spielfeld entfernen
  Rec.Figure:=Self;
  SendVMessage(vmCreateDeathObject);

  RemoveFromGameField;

  ShowInfoText(false);

  // DXUIBodeneinsatz aktualisieren
  Rec.Figure:=Self;
  Rec.NotifyUIType:=nuitUnitDeath;
  SendVMessage(vmNotifyUI);

  SendVMessage(vmContainerLock);

  AktuFigureRect;
  SendVMessage(vmContainerUnLock);
  fState:=[usDead];
end;

procedure TGameFigure.DecSeeing;
begin
  Rec.Figure:=Self;
  SendVMessage(vmDecSeeing);
  if FigureStatus in [fsFriendly,fsObject] then
    SendVMessage(vmAktuTempSurface);
end;

destructor TGameFigure.destroy;
begin
  fNotifyList.CallEvents(EVENT_FIGURE_ONDESTROY,Self);
  if not fDead then
  begin
    BerechneExp(fLaufEXP,etLaufen);
    BerechneExp(fSchussEXP,etSchiessen);
  end;

  if fManager<>nil then
    fManager.NotifyList.RemoveEvent(fManagerDestroy);

  fList.DeleteFigure(Self);

  fNotifyList.Free;
  fWay.Free;
  inherited;
end;

// Einheit wurde getroffen, bei eigener Einheit wird eine Meldung
// ausgegeben, wenn die Einheit schwer verletzt ist
function TGameFigure.DoTreffer(ISOObject: TObject): boolean;
var
  Dummy    : Integer;
  Fig      : TGameFigure;
  Treffer  : TTrefferParameter;
  IStatus  : Integer;
  OffSets  : TPoint;
begin
  Assert(ISOObject is TISOObject,'TGameFigure.DoTreffer: Object ist kein g�ltiges ISOObject');

  result:=false;

  if ( fManager=nil) or fDead then
    exit;

  Treffer.Strength:=random(10)+(TISOObject(ISOObject).GetAttackStrength-10);
  Treffer.Power:=TISOObject(ISOObject).GetAttackPower;
  Treffer.Zone:=GetTrefferZone(ISOObject);

  result:=fManager.DoTreffer(Treffer);

  if (fEinsatzKI<>nil) then
    TUnitKI(fEinsatzKI).DoTreffer(TISOObject(ISOObject).Figure);

  if Treffer.SchadenAbs>0 then
  begin
    // Neu zeichnen
    Rec.Point:=Point(XPos,YPos);
    SendVMessage(vmGetTileRect);

    OffSets:=GetUnitOffset;
    OffSetRectangle(Rec.Rect,OffSets.x,OffSets.Y);
    SendVMessage(vmAktuTempSurfaceRect);

    Rec.Figure:=Self;
    Rec.NotifyUIType:=nuitHitPointsChange;
    SendVMessage(vmNotifyUI);
  end;

  if (not result) and (FigureStatus=fsFriendly) then
  begin
    if fManager.Gesundheit<=round(fManager.MaxGesundheit*0.30) then
    begin
      if fManager.Gesundheit<=round(fManager.MaxGesundheit*0.15) then
      begin
        rec.Text:=Format(ST0309260002,[Name]);
        IStatus:=2;
      end
      else
      begin
        rec.Text:=Format(ST0309260003,[Name]);
        IStatus:=1;
      end;
      if IStatus>fInjuredStatus then
      begin
        rec.Figure:=Self;
        rec.Point:=Point(XPos,YPos);
        rec.MessageType:=emUnitHeavyInjured;
        SendVMessage(vmMessage);
        fInjuredStatus:=IStatus;
      end;
    end;
  end;

  if result then
  begin
    // Einheit wurde get�tet, wird somit den Erzeuger des Objektes (in der
    // Regel der Sch�tze der Waffe) zugerechnet
    if TISOObject(ISOObject).Figure<>nil then
      TISOObject(ISOObject).Figure.ZaehleAbschuss;

    Dead;
  end;

  // Alle Einheiten die in der N�he sind sollen in die Richtung schauen, aus der der Schuss kam
  for Dummy:=0 to fList.Count-1 do
  begin
    Fig:=fList[Dummy];
    if Fig.IsDead then
      continue;

    if (Fig.FigureStatus=FigureStatus) and (abs(Fig.XPos-XPos)<=5) and (abs(Fig.YPos-YPos)<=5) then
    begin
      if Fig.CanSeeUnit(TISOObject(ISOObject).Figure) then
        fNotifyList.CallEvents(EVENT_FIGURE_TREFFER,TISOObject(ISOObject).Figure);
    end;
  end;

  // Berechnung der Erfahrungspunkte f�r einen Treffer
  if TISOObject(ISOObject).Figure<>nil then
  begin
    if TISOObject(ISOObject).Figure.FigureStatus<>FigureStatus then
      TISOObject(ISOObject).Figure.BerechneExp(round(fManager.ExpierencePoints*(Treffer.SchadenAbs/fManager.MaxGesundheit)),etTreffer);
  end;
end;

procedure TGameFigure.Draw(Surface: TDirectDrawSurface; Mem: TDDSurfaceDesc; X, Y: Integer;XPos,YPos: Integer);
const
  BarBeginnX  =  10;
  BarWidth    = 28;
var
  AniFrame    : Integer;
  FieldWidth  : Integer;
  DrawShadow  : Boolean;
  p           : Integer;
  BarRect     : TRect;
begin
  Assert(fManager<>nil);

  Assert(fViewDirection<=high(TViewDirection));

  if usDead in fState then
    exit; // Wird gesetzt wenn die Sterbeanimation beendet ist

  DrawShadow:=true;
  if fMoving then
  begin
    AniFrame := fFrame+(Integer(fViewDirection)*fFramesPerDirection);

    if fFrame>=8 then
      FieldWidth:=fFrame-8
    else
      FieldWidth:=fFrame;
    if ((fViewDirection=vdRightTop) and ((XPos<>fXPos) or (YPos<>fYPos))) then exit;
    if ((fViewDirection=vdLeftBottom) and (XPos=fXPos) and (YPos=fYPos)) then exit;

    // Berechnung des Offsets zum Zeichnen des Soldaten
    case ViewDirection of
      vdRightTop     : dec(Y,round(FieldWidth*NormalOffSet));
      vdLeftBottom   : dec(Y,round((8-FieldWidth)*NormalOffSet));
      vdBottom       :
      begin
        if (XPos=fXPos) and (YPos=fYPos) then
        begin
          inc(Y,round(FieldWidth*SmallOffSet));
          inc(X,round(FieldWidth*NormalOffSet));
        end
        else
        begin
          dec(Y,round((8-FieldWidth)*SmallOffSet));
          dec(X,round((8-FieldWidth)*NormalOffSet));
          DrawShadow:=false;
        end;
      end;
      vdTop          :
      begin
//        Surface.ClippingRect:=fList.fNeedTileRect(Self,Point(XPos,YPos));
        if (XPos=fXPos) and (YPos=fYPos) then
        begin
          dec(Y,round(FieldWidth*SmallOffSet));
          dec(X,round(FieldWidth*NormalOffSet));
        end
        else
        begin
          inc(Y,round((8-FieldWidth)*SmallOffSet));
          inc(X,round((8-FieldWidth)*NormalOffSet));
          DrawShadow:=false;
        end;
      end;
      vdLeft         :
      begin
        if (XPos=fXPos) and (YPos=fYPos) then
        begin          inc(Y,round(FieldWidth*SmallOffSet));
          dec(X,round(FieldWidth*NormalOffSet));
        end
        else
        begin
          dec(Y,round((8-FieldWidth)*SmallOffSet));
          inc(X,round((8-FieldWidth)*NormalOffSet));
          DrawShadow:=false;
        end;
      end;
      vdRight        :
      begin
        if (XPos=fXPos) and (YPos=fYPos) then
        begin
          dec(Y,round(FieldWidth*SmallOffSet));
          inc(X,round(FieldWidth*NormalOffSet));
        end
        else
        begin
          inc(Y,round((8-FieldWidth)*SmallOffSet));
          dec(X,round((8-FieldWidth)*NormalOffSet));
          DrawShadow:=false;
        end;
      end;
      vdLeftTop      :
      begin
        if (XPos=fXPos) and (YPos=fYPos) then
        begin
          dec(X,round(FieldWidth*LargeOffSet));
        end
        else
        begin
          inc(X,round((8-FieldWidth)*LargeOffSet));
          DrawShadow:=false;
        end;
      end;
      vdRightBottom :
      begin
        if (XPos=fXPos) and (YPos=fYPos) then
        begin
          inc(X,round(FieldWidth*LargeOffSet));
        end
        else
        begin
          dec(X,round((8-FieldWidth)*LargeOffSet));
          DrawShadow:=false;
        end;
      end;
    end;
  end
  else
  begin
    AniFrame := Integer(fViewDirection)*fFramesPerDirection;

    if (XPos<>fXPos) or (YPos<>fYPos) then
      exit;
  end;

  if Selected then
    Surface.Draw(X-ObjectOffSetX,Y+ObjectOffSetY,SelectedRect,TDirectDrawSurface(fList.SurfaceList[0]));

//  dec(Y,(HalfTileWidth-6)-ObjectOffSetY);
  if DrawShadow then
  //  Surface.Draw(X-AniOffSetX,Y-AniOffSetY,TempRect,fImages,false)
    fImages.RenderWithShadow(Surface,X,Y,AniFrame)
  else
    fImages.RenderWithOutShadow(Surface,X,Y,AniFrame);

  // Gesundheitsbalken zeichnen
  BarRect:=Bounds(X+BarBeginnX,Y+ObjectOffSetY+HalfTileHeight,BarWidth+2,4);

  Rectangle(Surface,Mem,BarRect,bcBlack);
  Assert(Manager.MaxGesundheit<>0);
  p:=max(0,round((Manager.Gesundheit/Manager.MaxGesundheit)*BarWidth));
  Surface.FillRect(Rect(BarRect.Left+1,BarRect.Top+1,BarRect.Left+p+1,BarRect.Top+3),bcGreen);
  Surface.FillRect(Rect(BarRect.Left+1+p,BarRect.Top+1,BarRect.Left+BarWidth+1,BarRect.Top+3),bcRed);
  Surface.ClearClipRect;
end;

procedure TGameFigure.FindNextAlien;
begin
  if fSeeCount=0 then exit;
  repeat
    fLastShowIndex:=(fLastShowIndex+1) mod length(fSeeFigures);
  until fSeeFigures[fLastShowIndex]<>nil;
end;

function TGameFigure.GetIsOnField: boolean;
begin
  Assert(Self<>nil,'GetIsOnField gescheitert. Keine Einheit');
  result:=(not fImRaumschiff);
end;

function TGameFigure.GetMaxTimeUnits: Integer;
begin
  Assert(fManager<>nil);
  result:=fManager.MaxZeiteinheiten;
end;

function TGameFigure.GetZiel: TPoint;
begin
  if fWay.HasPath then
    result:=fWay.ConvertToPoint(fWay.Ziel)
  else
    result:=Point(XPos,YPos);
end;

procedure TGameFigure.IncSeeing;
begin
  Rec.Figure:=Self;
  SendVMessage(vmIncSeeing);
  if FigureStatus in [fsFriendly,fsObject] then
    SendVMessage(vmAktuTempSurface);
end;

function TGameFigure.IsVisible: boolean;
begin
  Rec.Figure:=Self;
  SendVMessage(vmCanDrawUnit);
  result:=Rec.Result;
end;

function TGameFigure.MoveTo(Point: TPoint; NoWait, AddWay: Boolean): boolean;
var
  Dummy       : Integer;
  Last        : TPoint;
  SaveReached : Boolean;
begin
  result:=false;
  if (Point.X<0) or (Point.Y<0) or (Point.X>=fList.MapXSize) or (Point.Y>=fList.MapYSize) then
    exit;

  if fDead then
    exit;

  Rec.Point:=Point;
  SendVMessage(vmGetFigure);
  if Rec.Figure<>nil then
    exit;

  if (FigureStatus=fsFriendly) and (not fEchtzeit) and (not NoWait) then
  begin
    if fWay.HasPath and (fWay.Ziel.X=Point.X) and (fWay.Ziel.Y=Point.Y) then
    begin
      // Die Statis m�ssen gesetzt werden, da diese beim Unterbrechen einer Runde gel�scht werden (#960)
      Include(fState,usMoveUnit);
      Exclude(fState,usNeedTu);
      Exclude(fState,usWaitForOK);
      exit;
    end
    else
      Include(fState,usWaitForOK);
  end
  else
    Exclude(fState,usWaitForOK);

  if AddWay and (fWay.HasPath) then
  begin
    if fList.PathFinder.CreatePath(Ziel.x,Ziel.Y,Point.X,Point.Y,fViewDirection) then
    begin
      // Wir beginnen bei 1, um nicht den letzten Ziel Punkt doppelt in der Liste
      // zu haben
      for Dummy:=1 to fList.PathFinder.WayPoints-1 do
        fWay.AddPointAtEnd(fList.PathFinder.Way[Dummy]);

      result:=true;
    end;
  end
  else if not fMoving then
  begin
    fList.PathFinder.IgnoreUnit:=Self;

    if fList.PathFinder.CreatePath(XPos,YPos,Point.x,Point.y,fViewDirection) then
    begin
      fWay.Clear;
      for Dummy:=0 to fList.PathFinder.WayPoints-1 do
        fWay.AddPointAtEnd(fList.PathFinder.Way[Dummy]);

      result:=true;
    end;
    fList.PathFinder.IgnoreUnit:=nil;
  end
  else
  begin
    StopMove;

    if fWay.HasPath then
      Last:=fWay.ConvertToPoint(fWay.NextPoint)
    else
      Last:=Classes.Point(XPos,YPos);

    fList.PathFinder.IgnoreUnit:=Self;

    if fList.PathFinder.CreatePath(Last.X,Last.Y,Point.x,Point.y,fViewDirection) then
    begin
      SaveReached:=fWay.ReachedPoint;
      fWay.Clear;
      for Dummy:=0 to fList.PathFinder.WayPoints-1 do
        fWay.AddPointAtEnd(fList.PathFinder.Way[Dummy]);
      result:=true;
      fWay.ReachedPoint:=SaveReached;
    end;

    fList.PathFinder.IgnoreUnit:=nil;
  end;

  if Selected then
    fWay.ShowWayPoints(true);

  if result then
  begin
    Rec.Figure:=Self;
    SendVMessage(vmCreateZielObject);
    if not (usMoveUnit in fState) then
    begin
      Include(fState,usMoveUnit);
      Exclude(fState,usReady);
      Exclude(fState,usNeedTU);
    end;
  end
  else
    StopMove;
end;

function TGameFigure.MoveUnit(Frames: Integer): boolean;
var
  ChangePos : boolean;
  BlockObj  : TGameFigure;
  Dummy     : Integer;
begin
  // Ohne angegebenen Manager kann keine Bewegung erfolgen
  result:=false;
  if fManager=nil then
    exit;

  if usDead in fState then
    exit;

  // Haupts�chlich Laserwaffen aufladen
  Manager.Timer(Frames*25);

  ChangePos:=false;

  // Pr�fen, ob gegnerische Einheit gesichtet wurde
  if (FigureStatus=fsEnemy) and (fLastVisible<>IsVisible) then
  begin
    fLastVisible:=IsVisible;
    if IsVisible then
    begin
      Rec.Text:=Format(ST0309260004,[Name]);
      Rec.Point:=Point(XPos,YPos);
      Rec.MessageType:=emUnitViewed;
      SendVMessage(vmMessage);
    end
    else
      AktuFigureRect;
  end;

//  if (usNeedTU in fState) then
//    exit;

  if usWantViewTo in fState then
  begin
    ViewTo(fViewTo);
    Exclude(fState,usWantViewTo);
  end;

  SendVMessage(vmContainerLock);

  fHasMoveTU:=false;

  while Frames>0 do
  begin
    dec(fManager.GetAddrOfSlot(scsLinkeHand).TimeLeft,25);
    dec(fManager.GetAddrOfSlot(scsRechteHand).TimeLeft,25);

    fFiller:=not fFiller;

    if (usMoveUnit in fState) and fFiller and not (usWaitForOK in fState) then
    begin
      if fMoving then
      begin
        if (not IsVisible) and (not fEchtzeit) then
        begin
          if fFrame<8 then
            fFrame:=8
          else
            fFrame:=0;
        end
        else
          fFrame:=(fFrame+1) mod 16;
      end;

      fMoving:=true;

      {$IFNDEF FASTMOVE}
      if (fFrame=0) or (fFrame=8) then
      {$ENDIF}
      begin

        // Auf das n�chste Feld positionieren
        if not fWay.ReachedPoint then
        begin
          DecSeeing;

          Rec.Point:=Point(fXPos,fYPos);
          Rec.Figure:=nil;
          SendVMessage(vmSetFigure);
          Assert(Rec.Result);

          fXPos:=fWay.NextPoint.X;
          fYPos:=fWay.NextPoint.Y;

          // Auskommentiert => �berfl�ssig da die Figure bei beginn der Bewegung
          // auf die neue Position gesetzt wird
{          Rec.Point:=Point(fXPos,fYPos);
          Rec.Figure:=Self;
          SendVMessage(vmSetFigure);
          Assert(Rec.Result);}

          IncSeeing;
          DoMove;
          fWay.ReachedPoint:=true;

          ChangePos:=true;
        end;

        if (not fWay.ZielErreicht) then
        begin
          // Stopp da n�chstes Feld belegt
          Rec.Point:=fWay.ConvertToPoint(fWay.PointInSteps(1));
          SendVMessage(vmGetFigure);
          if (Rec.Figure<>nil) then
          begin
            BlockObj:=TGameFigure(Rec.Figure);
            if not BlockObj.fMoving then
            begin
              // Das Zielfeld wird blockiert, dann Bewegung abbrechen
              if fWay.PointInSteps(1)=fWay.Ziel then
              begin
                StopMove;
                break;
              end;

              // Alternativ Route pr�fen
              fList.PathFinder.UmgeheObject:=BlockObj;
              if not fList.PathFinder.CreatePath(XPos,YPos,fWay.PointInSteps(2).X,fWay.PointInSteps(2).Y,fViewDirection) then
              begin
//                fMoving:=false;
//                if not BlockObj.MakeWayFree(fWay.ConvertToPoint(fWay.PointInSteps(2))) then
//                begin
                  fNotifyList.CallEvents(EVENT_FIGURE_WAYTODESTBLOCKED,Self);
                  StopMove
//                end
//              else
//                  fSavedUnit:=BlockObj;
              end
              else
              begin
                fMoving:=false;
                fWay.GoToNextPoint;  // Aktuelle Position
                fWay.GoToNextPoint;  // Position die Blockiert wird
                fWay.GoToNextPoint;  // Position danach
                for Dummy:=fList.PathFinder.WayPoints-1 downto 0 do
                begin
                  fWay.AddPointBefore(fList.PathFinder.Way[Dummy]);
                end;
                fWay.ReachedPoint:=true;
              end;
              fList.PathFinder.UmgeheObject:=nil;
              Exclude(fState,usMoveUnit);
              break;
            end
            else
            begin
              // Bewegung vorl�ufig stoppen, Soldat der das Feld blockiert, bewegt sich weg
              fMoving:=false;
              Exclude(fState,usMoveUnit);
              break;
            end;
          end;

          // Stop aufgrund von mangelnden Zeiteinheiten
          if (NeedTU(TUMoveField)<>nurOkay) then
          begin
            fMoving:=false;
            Exclude(fState,usMoveUnit);
            break;
          end
          else
            fHasMoveTU:=true;

        end;

        if (fWay.ZielErreicht) then
        begin
          fWay.Clear;
          fMoving:=false;
          fState:=[usReady];

          Rec.Figure:=Self;
          SendVMessage(vmDeleteZielObject);

          ChangePos:=true;
        end
        else
        begin
          if not ViewTo(fWay.ConvertToPoint(fWay.PointInSteps(1))) then
          begin
            fMoving:=false;
            break;
          end;
          if not fWay.HasPath then
          begin
            fMoving:=false;
            break;
          end;

          if (fManager<>nil) then
          begin
            rec.Figure:=Self;
            SendVMessage(vmCanDrawUnit);
            if rec.Result then
              fManager.PlayWalkSound('');
          end;

          fWay.GoToNextPoint;

          Rec.Point:=fWay.ConvertToPoint(fWay.NextPoint);
          Rec.Figure:=Self;
          SendVMessage(vmSetFigure);
          Assert(Rec.Result);

          ChangePos:=true;

          if fSavedUnit<>nil then
          begin
            fSavedUnit.GoBack;
            fSavedUnit:=nil;
          end;
        end;
      end
      {$IFNDEF FASTMOVE}
      else
        ChangePos:=true;
      {$ENDIF}
    end;
    dec(Frames);
  end;
  SendVMessage(vmContainerUnlock);
  if ChangePos and IsVisible then
  begin
    AktuFigureRect;
    result:=true;
  end;
end;

function TGameFigure.NeedTU(TU: Integer): TNeedTimeUnitResult;
begin
  Assert(fManager<>nil);

  result:=nurOkay;
  if fEchtzeit then
    exit;

  // Pr�fen, ob die Einheit am Zug ist
  // Zeiteinheiten abziehen
  rec.Figure:=Self;
  SendVMessage(vmCanUnitDoAction);

  if not rec.result then
  begin
    ShowFriendMessage(CR0504140001);
    Include(fState,usNeedTU);
    result:=nurNotOnMove;
    exit;
  end;

  if (fTimeUnits-fReservedTU)<TU then
  begin
    result:=nurNoTU;
    NoTUforAction;
  end
  else
  begin
    result:=nurOkay;
    dec(fTimeUnits,TU);
  end;

  SendVMessage(vmRedrawSolList);
  Rec.Figure:=Self;
  Rec.NotifyUIType:=nuitTimeUnitChange;
  SendVMessage(vmNotifyUI);
end;

procedure TGameFigure.NextRound;
begin
  if fDead or (fManager=nil) then
    exit;

  ResetNeedTUState;

  Assert(fManager<>nil);
  fTimeUnits:=fManager.MaxZeiteinheiten;

  // Wegpunkte neu berechnen
  if Selected and not Echtzeit then
    fWay.ShowWayPoints(true);
end;

procedure TGameFigure.RecalcSeeingRange;
var
  X,Y           : Integer;
  Winkel        : Integer;
  StartW,EndW   : Integer;
  ConvNull      : boolean;
  OStartW,OEndW : Integer;
  Entfern       : single;
  Ptr           : PSeeTile;

  function InSehWinkel(StartW,EndW: Integer): boolean;
  begin
    result:=false;
    if ConvNull and (Winkel=0) then Winkel:=360;
    if (StartW<EndW) then
    begin
      if (Winkel>=StartW) and (Winkel<=EndW) then
        result:=true;
    end
    else
    begin
      if (Winkel>=StartW) or (Winkel<=EndW) then
        result:=true;
    end;
  end;

  function WinkelRandBereich: boolean;
  begin
    result:=not InSehWinkel(OStartW,OEndW);
  end;

  function GetAbstandRand: Integer;
  begin
    if OStartW<OEndW then
    begin
      if (OEndW=360) then
      begin
        if Winkel<21 then
          result:=20-Winkel
        else
          result:=20-(OStartW-Winkel);
      end
      else if (OStartW=0) then
      begin
        if Winkel>339 then
          result:=Winkel-340
        else
          result:=110-Winkel;
      end
      else
        result:=min(20-(Winkel-OEndW),20-(OStartW-Winkel))
    end
    else
      result:=20-min((OStartW-Winkel),(Winkel-OEndW));
    result:=min(20,result);
  end;

begin
  if fDead then exit;
  Ptr:=GetSeeingData(Sehweite);
  inc(Ptr,((Sehweite*2)+1)*((Sehweite*2)+1)*Ord(fViewDirection));
  for X:=0 to (Sehweite shl 1) do
  begin
    MoveMemory(Addr(SeeingTiles[X,0]),Ptr,((Sehweite*2)+1)*sizeOf(TSeeTile));
    inc(Ptr,(Sehweite*2)+1);
  end;
  if LightDetail<>ldEinfach then
  begin
    StartW:=-1;
    EndW:=-1;
    ConvNull:=false;
    case fViewDirection of
      vdTop          : begin;StartW:=225;EndW:=315;end;
      vdRightTop     : begin;StartW:=270;EndW:=360;ConvNull:=true;end;
      vdRight        : begin;StartW:=315;EndW:= 45;end;
      vdRightBottom  : begin;StartW:=  0;EndW:= 90;end;
      vdBottom       : begin;StartW:= 45;EndW:=135;end;
      vdLeftBottom   : begin;StartW:= 90;EndW:=180;end;
      vdLeft         : begin;StartW:=135;EndW:=225;end;
      vdLeftTop      : begin;StartW:=180;EndW:=270;end;
      vdAll          : begin;StartW:=  0;EndW:=360;end;
    end;
    OStartW:=StartW;
    OEndW:=EndW;
    StartW:=StartW-20;
    EndW:=EndW+20;
    if StartW<0 then inc(StartW,360);
    if EndW>360 then dec(EndW,360);
    if LightDetail=ldDoppelt then
    begin
      for X:=0 to (SehWeite*4)+1 do
      begin
        for Y:=0 to (SehWeite*4)+1 do
        begin
          Winkel:=CalculateWinkel(FloatPoint(X,Y),FloatPoint((SehWeite*2)+0.5,(SehWeite*2)+0.5));
          if InSehWinkel(StartW,EndW) then
          begin
            Entfern:=Sqrt(Sqr(x-((SehWeite*2)+0.5))+Sqr(Y-((SehWeite*2)+0.5)));
            if Entfern>(Sehweite*2)-15 then
            begin
              Entfern:=(Sehweite*2)-Entfern;
              AlphaMap[X,Y]:=max(32,min(255,round(Entfern*17)));
            end
            else
              AlphaMap[X,Y]:=255;
            if WinkelRandBereich then
            begin
              AlphaMap[X,Y]:=round(AlphaMap[X,Y]*(GetAbstandRand/20));
            end;
          end
          else if (X div 2=SehWeite) and (Y div 2=SehWeite) then
            AlphaMap[X,Y]:=255
          else
            AlphaMap[X,Y]:=32;
        end;
      end;
    end
    else if LightDetail=ldVierfach then
    begin
      for X:=0 to (SehWeite*8)+3 do
      begin
        for Y:=0 to (SehWeite*8)+3 do
        begin
          Winkel:=CalculateWinkel(FloatPoint(X,Y),FloatPoint((SehWeite*4)+0.5,(SehWeite*4)+0.5));
          if InSehWinkel(StartW,EndW) then
          begin
            Entfern:=Sqrt(Sqr(x-((SehWeite*4)+0.5))+Sqr(Y-((SehWeite*4)+0.5)));
            if Entfern>(Sehweite*4)-30 then
            begin
              Entfern:=(Sehweite*4)-Entfern;
              AlphaMap[X,Y]:=max(32,min(255,round(Entfern*8.5)));
            end
            else
              AlphaMap[X,Y]:=255;
            if WinkelRandBereich then
            begin
              AlphaMap[X,Y]:=round(AlphaMap[X,Y]*(GetAbstandRand/20));
            end;
          end
          else
            AlphaMap[X,Y]:=32;
        end;
      end;
    end;
  end;
  SeeingTiles[SehWeite,SehWeite].SeeingRange:=8;
end;

procedure TGameFigure.RemoveFromGameField;
begin
  if FigureStatus<>fsObject then
  begin
    Rec.Figure:=nil;
    Rec.Point:=Point(XPos,YPos);
    SendVMessage(vmSetFigure);
    Assert(Rec.Result);

    if fWay.HasPath then
    begin
      Rec.Point:=fWay.ConvertToPoint(fWay.NextPoint);
      Rec.Figure:=nil;
      SendVMessage(vmSetFigure);
      Assert(Rec.Result);
    end;

    fImRaumschiff:=true;
    ClearSeeingList;
  end;

  SendVMessage(vmContainerLock);
  DecSeeing;

  Selected:=false;

  if fManager<>nil then
    fManager.OnBattleField:=false;

  if FigureStatus=fsFriendly then
    SendVMessage(vmRedrawSolList);

  RecalcDrawingRect;
  SendVMessage(vmContainerUnLock);
  SendVMessage(vmEngineRedraw);
end;

// Einheit aus der Sehliste entfernen
procedure TGameFigure.RemoveFromSeeingList(Figure: TGameFigure);
var
  Dummy: Integer;
  Ok   : Boolean;
begin
  Ok:=false;
  for Dummy:=0 to High(fSeeFigures) do
  begin
    if fSeeFigures[Dummy]=Figure then
    begin
      fSeeFigures[Dummy]:=nil;
      ok:=true;
    end;
  end;
  if fZielUnit=Figure then
    fZielUnit:=nil;

  if ok and (Figure.FigureStatus=fsEnemy) then
    fList.DecEnemysSeeing;

  CountSeeUnits;

  Figure.RecalcDrawingRect;
end;

procedure TGameFigure.Select;
const {static}
  DoSelect : Boolean = false;
begin
  if DoSelect then
    exit;

  if fDead then
    exit;

  DoSelect:=true;

  Rec.Figure:=Self;
  SendVMessage(vmSelection);

  DoSelect:=false;
end;

procedure TGameFigure.SetManager(const Value: TGameFigureManager);
begin
  if fManager<>nil then
    fManager.NotifyList.RemoveEvent(fManagerDestroy);

  fManager := Value;
  if fManager<>nil then
  begin
    fManagerDestroy:=fManager.NotifyList.RegisterEvent(EVENT_MANAGER_ONDESTROY,EventManagerDestroy);
    FigureStatus:=fManager.FigureStatus;
    fName:=fManager.Name;
    SehWeite:=fManager.Sichtweite;
    fManager.Echtzeit:=fEchtzeit;
    fModelInfo:=ModelInfos[Manager.FigureMap];

    Rec.Text:=fModelInfo.imageset;
    SendVMessage(vmGetXANBitmap);
    fImages:=Rec.XANBitmap;

    Rec.Text:=fModelInfo.imageset+'TOD';
    SendVMessage(vmGetXANBitmap);
    fDeadImage:=Rec.XANBitmap;

    fFramesPerDirection:=fImages.Frames div (Integer(high(TViewDirection)));
  end;
end;

procedure TGameFigure.SetSehWeite(const Value: Integer);
begin
  Assert(Value>0,'Ung�ltige Sichtweite Name: '+Name);

  fSehWeite := Value;
  SetLength(SeeingTiles,(Value*2)+1,(Value*2)+1);
  if LightDetail=ldDoppelt then
    SetLength(AlphaMap,(Value*4)+2,(Value*4)+2)
  else if LightDetail=ldVierfach then
  begin
    SetLength(AlphaMap,(Value*8)+4,(Value*8)+4);
  end;
  RecalcSeeingRange;
end;

procedure TGameFigure.SetSelected(const Value: boolean);
begin
  if FigureStatus<>fsFriendly then exit;
  if (fSelected=Value) then exit;
  if (IsDead or fImRaumschiff) and Value then exit;

  if (not fEchtzeit) and (not fRemoveSelection) then
  begin
    if (not Value and fSelected) then
    begin
      fSelected:=Value;
      SendVMessage(vmShowNextUnit);
    end
    else if (Value and not fSelected) then
    begin
      fSelected := Value;
      Select;
    end;
  end;

  fSelected := Value;
  Rec.NotifyUIType:=nuitSelectionChange;
  Rec.Figure:=Self;
  SendVMessage(vmNotifyUI);
  Rec.Figure:=Self;
  SendVMessage(vmMiniMapSelectionChange);
  if Value then
  begin
    if fWay.HasPath then
    begin
      Rec.Figure:=Self;
      SendVMessage(vmCreateZielObject);
      {$IFDEF TRACEWAY}
      GlobalFIle.Write(Name);
      fWay.TraceWay;
      {$ENDIF}
    end
  end
  else
    SendVMessage(vmDeleteZielObject);

  if not fEchtzeit then
  begin
    if not fSelected then
      ShowInfoText(false)
    else
      ShowInfoText(true);
  end;
  fWay.ShowWayPoints(fSelected);
  SendVMessage(vmAktuTempSurface);
  SendVMessage(vmRedrawSolList);
//  SendVMessage(vmEngineRedraw);
end;

// Einheit auf dem Kampfplatzieren, so dass sie hinterher benutzt werden kann
function TGameFigure.SetToGameField: Boolean;
begin
  Rec.Figure:=Self;
  Rec.Point:=Point(XPos,YPos);
  SendVMessage(vmSetFigure);
  result:=rec.result;

  if not result then
    exit;
    
  fImRaumschiff:=false;
  IncSeeing;
  if fManager<>nil then
    fManager.OnBattleField:=true;
  fList.EinheitAufFeldPlaziert(Self);

  RecalcDrawingRect;
end;

procedure TGameFigure.SetViewDirection(const Value: TViewDirection);
begin
  if fDead then
    exit;

  SendVMessage(vmGetPause);
  // Wenn der Soldat gerade l�uft, dann kann er nicht in eine andere Richtung schauen
  if (FigureStatus<>fsObject) and (Rec.Result or ((not (usWaitForOK in fState)) and (fWay.HasPath and (not fWay.ReachedPoint)))) then
    exit;

  fViewDirection := Value;
  DecSeeing;
  RecalcSeeingRange;
  IncSeeing;
  AktuSeeingList;

  if FigureStatus<>fsFriendly then
    AktuFigureRect
  else
    SendVMessage(vmAktuTempSurface);
end;

procedure TGameFigure.SetXPos(const Value: Integer);
begin
  Assert((Value>=0) and (Value<=fList.MapXSize),'ung�ltige X ('+IntToStr(Value)+') Position');
  fXPos := Value;
  if fManager<>nil then
    fManager.SetEinsatzPosition(Point(fXPos,fYPos));
end;

procedure TGameFigure.SetYPos(const Value: Integer);
begin
  Assert((Value>=0) and (Value<=fList.MapYSize),'ung�ltige Y ('+IntToStr(Value)+') Position');
  fYPos := Value;
  if fManager<>nil then
    fManager.SetEinsatzPosition(Point(fXPos,fYPos));
end;

function TGameFigure.ShootToUnit(Figure: TGameFigure; Slot: TSoldatConfigSlot; ShootInfos: PShootInfos = nil): Boolean;

  function CanShootTo: Boolean;
  begin
    result:=CanSeeUnit(Figure);

    if (not result) and (FigureStatus=fsFriendly) then
    begin
      // Pr�fen, ob die Einheit durch einen anderen gesehen wird (eventuell auch Sensor)
      rec.Figure:=Figure;
      SendVMessage(vmCanDrawUnit);
      if rec.result then
      begin
        rec.FromP:=Point(XPos,YPOs);
        rec.ToP:=Point(Figure.XPos,Figure.YPos);
        SendVMessage(vmIsWayFree);

        result:=rec.Result;
      end;
    end;
  end;

begin
  result:=false;

  if Figure=nil then
    exit;

  if FigureStatus=fsObject then
    exit;

  if not CanShootTo then
  begin
    ViewTo(Point(Figure.XPos,Figure.YPos));
    if not CanShootTo then
    begin
      fZielUnit:=nil;
      exit;
    end;
  end;

  result:=ShootToPos(Point(Figure.XPos,Figure.YPos),Slot,ShootInfos);
end;

function TGameFigure.ShootToPos(Pos: TPoint; Slot: TSoldatConfigSlot; ShootInfos: PShootInfos): Boolean;
var
  Waffe   : PSoldatWaffe;
  Infos   : TWaffenParamInfos;
  Shoot   : TShootInfos;
  Item    : PLagerItem;
begin
  result:=false;

  if FigureStatus=fsObject then
    exit;

  if (Pos.X=XPos) and (Pos.Y=YPos) then
    exit;
    
  Assert(fManager<>nil,Name+', '+GetEnumName(TypeInfo(TFigureStatus),Ord(FigureStatus)));
  Waffe:=fManager.GetAddrOfSlot(Slot);

  // Waffe bereit zum schiessen ?
  if (not Waffe.Gesetzt) or (Waffe.TypeID<>ptWaffe) then
    exit;

  // Einheit in die richtung des Zieles blicken lassen
  if not fMoving then
    ViewTo(Pos);

  // Entfernung bei Nahkampfwaffe pr�fen
  if Waffe.WaffenArt=wtShortRange then
  begin
    if not PointsSideBySide(Pos,Point(XPos,YPos)) then
    begin
      Rec.Text:=CR0506270001;
      SendVMessage(vmTextMessage);
      exit;
    end;
  end;

  // Munition verbrauchen
  Shoot.Range:=CalculateEntfern(Pos,Point(XPos,YPos));
  if ShootInfos=nil then
  begin
    Shoot.TargetUnit:=nil;
    Shoot.TargetHeight:=-1;
  end
  else
  begin
    Shoot.TargetHeight:=ShootInfos.TargetHeight;
    Shoot.TargetUnit:=ShootInfos.TargetUnit;
  end;

  case fManager.DoShoot(Slot,Shoot,Infos) of
    dsrReload:
    begin
      if FigureStatus=fsFriendly then
      begin
        // Nachladesound abspielen
        Rec.Text:='ReloadWeapon';
        SendVMessage(vmPlaySound);

        Rec.MessageType:=emReloadWeapon;
        Rec.Text:=Format(ST0309260007,[Name,Waffe.Name]);
        Rec.Figure:=Self;
        Rec.Point:=Point(XPos,YPos);
        SendVMessage(vmMessage);
      end;
      SENachladError:=false;
    end;
    dsrNotEnoughTUs:
    begin
      NoTUforAction;
      exit;
    end;
    dsrNotReloaded:
    begin
      result:=true;
      exit;
    end;
    dsrNotFire:
    begin
      result:=false;
      exit;
    end;
    dsrNoMunition:
    begin
      // Keine Munition zu nachladen
      if not SENachladError then
      begin
        if FigureStatus=fsFriendly then
        begin
          Rec.Text:=Format(ST0309260006,[Name]);
          Rec.Point:=Point(XPos,YPos);
          Rec.Figure:=Self;
          Rec.MessageType:=emNoMunition;
          SendVMessage(vmMessage);
        end;

        SENachladError:=true;
      end;
      exit;
    end;
  end;

  // Schuss war erfolgreich, Meldung "Keine Munition zum Nachladen" zur�cksetzen
  SENachladError:=false;

  // Schuss abgegeben
  Rec.Figure:=Self;
  Rec.Ziel:=Pos;

  // Ist der Soldat in Bewegung, 40% abziehen
  if fMoving then
    Infos.Treff:=max(0,Infos.Treff-40);

  // Sounds ermitteln
  Item:=lager_api_GetItem(Waffe.ID);
  Assert(Item<>nil);
  Infos.ShootSound:=Item.SoundShoot;

  Rec.Figure:=Self;
  Rec.Infos:=Infos;
  SendVMessage(vmShoot);

  // Erfahrungspunkte berechnen
  inc(fSchussEXP,max(1,Waffe.TimeUnits div 2));

  // GUI aktualisieren
  Rec.Figure:=Self;
  SendVMessage(vmRedrawSolList);

  Rec.NotifyUIType:=nuitMunitionChange;
  Rec.Figure:=Self;
  SendVMessage(vmNotifyUI);

  result:=true;
end;

function TGameFigure.StopMove: boolean;
begin
  result:=false;
  if fDead then
    exit;
  fZielUnit:=nil;

  if not fWay.HasPath then
    exit;

  result:=true;

  if fWay.ReachedPoint then
  begin
    fWay.Clear;
    if fHasMoveTU then
    begin
      Inc(fTimeUnits,3);
      fHasMoveTU:=false;
    end;
    fMoving:=false;
    fFrame:=0;
    Exclude(fState,usMoveUnit);
    result:=false;
  end
  else
  begin
    fWay.SetNextPointToZiel;
  end;
  
  Rec.Figure:=Self;
  SendVMessage(vmDeleteZielObject);
  Exclude(fState,usWaitForOK);
end;

function TGameFigure.ViewTo(Point: TPoint; RecalcSight: boolean): boolean;
var
  Winkel : double;
  OldDir : TViewDirection;
  NewDir : TViewDirection;
begin
  result:=false;
  if fDead then
    exit;

  if (Point.X=XPos) and (Point.Y=YPos) then
    exit;

  SendVMessage(vmGetPause);
  // Wenn der Soldat gerade l�uft, dann kann er nicht in eine andere Richtung schauen
  if Rec.Result or ((not (usWaitForOK in fState)) and (fWay.HasPath and (not fWay.ReachedPoint))) then
  begin
    fViewTo:=Point;
    Include(fState,usWantViewTo);
    exit;
  end;

  // Einheit ist nicht am Zug
  Rec.Figure:=Self;
  SendVMessage(vmCanUnitDoAction);
  if not rec.Result then
  begin
    fViewTo:=Point;
    Include(fState,usWantViewTo);
    exit;
  end;

  fViewTo.X:=-1;

  OldDir:=fViewDirection;
  NewDir:=OldDir;
  Winkel:=CalculateWinkel(PointToFloat(Point),FloatPoint(fXPos,fYPos));
  if (Winkel>337.5) or (Winkel<22.5) then NewDir:=vdRight
  else if (Winkel>22.5) and (Winkel<67.5) then NewDir:=vdRightBottom
  else if (Winkel>67.5) and (Winkel<112.5) then NewDir:=vdBottom
  else if (Winkel>112.5) and (Winkel<157.5) then NewDir:=vdLeftBottom
  else if (Winkel>157.5) and (Winkel<202.5) then NewDir:=vdLeft
  else if (Winkel>202.5) and (Winkel<247.5) then NewDir:=vdLeftTop
  else if (Winkel>247.5) and (Winkel<292.5) then NewDir:=vdTop
  else if (Winkel>292.5) and (Winkel<337.5) then NewDir:=vdRightTop;

  result:=true;
  if NewDir=OldDir then
    exit;

  if RecalcSight then
  begin
    if NeedTU(TUChangeView)<>nurOkay then
    begin
      fViewTo:=Point;
      Include(fState,usWantViewTo);
      result:=false;
      exit;
    end;
    fViewDirection:=NewDir;
    result:=true;
    DecSeeing;
    RecalcSeeingRange;
    IncSeeing;
    if fFigureStatus in [fsEnemy,fsNeutral] then
      AktuFigureRect;
  end
  else
  begin
    fViewDirection:=NewDir;
    RecalcSeeingRange;
  end;
end;


procedure TGameFigure.ZaehleAbschuss;
begin
  if fManager=nil then
    exit;
  fManager.ZaehleAbschuss;
end;

// Position wurde ge�ndert
procedure TGameFigure.DoMove;
begin
  if fManager<>nil then
    fManager.SetEinsatzPosition(Point(fXPos,fYPos));
  fList.AktuSeeingLists;

  inc(fLaufEXP);
end;

// Property IsMoving
function TGameFigure.GetMoving: boolean;
begin
  result:=fWay.hasPath and not (usWaitForOK in fState);
end;

// Ausweichen, wenn die Figure den Weg blockiert
function TGameFigure.CheckOffset(XOff,YOff: Integer;const WantTo: TPOint): boolean;
var
  ToX    : Integer;
  ToY    : Integer;
begin
  result:=false;
  if (usNeedTU in fState) then
    exit;
    
  if (XOff=0) and (YOff=0) then
    exit;
  ToX:=XPos+XOff;
  ToY:=YPos+YOff;
  if not PtInRect(Rect(0,0,fList.MapXSize-1,fList.MapYSize),Point(ToX,ToY)) then
    exit;
  if (ToX<>WantTo.X) or (ToY<>WantTo.Y) then
  begin
    Rec.Point:=Point(ToX,ToY);
    SendVMessage(vmGetFigure);
    if (Rec.Figure=nil) then
    begin
      fList.PathFinder.CreatePath(XPos,YPos,ToX,ToY,fViewDirection);
      if fList.PathFinder.WayCost<=10 then
      begin
        fLastPoint:=Point(XPos,YPos);
        MoveTo(Point(ToX,ToY));
        result:=true;
      end;
    end;
  end;
end;

function TGameFigure.MakeWayFree(WantTo: TPoint): boolean;
const
  MaxEntfern = 3;

var
  Dummy     : Integer;
  XOffSet   : Integer;
  YOffSet   : Integer;
  Save      : TObject;
  hasCheck  : Array[-MaxEntfern..MaxEntfern,-MaxEntfern..MaxEntfern] of boolean;
begin
  result:=false;
  FillChar(HasCheck,SizeOf(HasCheck),#0);
  for Dummy:=1 to MaxEntfern do
  begin
    for XOffSet:=-Dummy to Dummy do
    begin
      for YOffSet:=-Dummy to Dummy do
      begin
        if not hasCheck[XOffSet,YOffSet] then
        begin
          result:=CheckOffset(XOffSet,YOffSet,WantTo);
          hasCheck[XOffSet,YOffSet]:=true;
          if result then break;
        end;
      end;
      if result then break;
    end;
    if result then break;
  end;
end;

// Pr�ft, ob die Einheit gesehen wird und f�gt sie der Sehliste hinzu
procedure TGameFigure.CheckSeeing(Figure: TGameFigure);
begin
  Assert(Figure<>nil);
  
  // Damit Sensoren nicht auf die Gegner schiessen
  if (Figure=Self) or (not Figure.IsVisible) or (not Figure.IsOnField) or (Figure.IsDead) then exit;
  if (FigureStatus=Figure.FigureStatus) or (FigureStatus=fsNeutral) or (Figure.FigureStatus=fsNeutral) then exit;
  // Pr�fung auf die Sehweite
  
  if CanSeePoint(Figure.XPos,Figure.YPos) then
  begin
    AddFigureToSeeList(Figure);
  end;
end;

function TGameFigure.GetSeeUnit(Index: Integer): TGameFigure;
var
  Dummy: Integer;
begin
  result:=nil;
  for Dummy:=0 to High(fSeeFigures) do
  begin
    if fSeeFigures[Dummy]<>nil then
    begin
      dec(Index);
      if Index=-1 then
      begin
        result:=fSeeFigures[Dummy];
        exit;
      end;
    end;
  end;
end;

procedure TGameFigure.CountSeeUnits;
var
  Dummy: Integer;
begin
  fSeeCount:=0;
  for Dummy:=0 to High(fSeeFigures) do
  begin
    if fSeeFigures[Dummy]<>nil then
      inc(fSeeCount);
  end;
  fLastShowIndex:=-1;
  if FigureStatus=fsFriendly then
  begin
    FindNextAlien;
    SendVMessage(vmRedrawSolList)
  end;
end;

// Pr�ft, ob ein Punkt von der Einheit gesichtet wird
function TGameFigure.CanSeePoint(X,Y: Integer): boolean;
begin
  result:=(abs(X-XPos)<Sehweite) and (abs(Y-YPos)<Sehweite) and SeeingTiles[X-XPos+SehWeite,Y-YPos+SehWeite].HasIncrease;
end;

function TGameFigure.GetUnitOffset: TPoint;
var
  FieldWidth: Integer;
begin
  result.X:=0;
  result.Y:=0;
  if not fMoving then
    FieldWidth:=0
  else
    FieldWidth:=fFrame mod 8;
  if FieldWidth<>0 then
  begin
    case ViewDirection of
      vdRightTop     : result.Y:=round(-(FieldWidth*NormalOffSet));
      vdLeftBottom   : result.Y:=round((FieldWidth*NormalOffSet));
      vdBottom       :
      begin
        result.Y:=round(FieldWidth*SmallOffSet);
        result.X:=round(FieldWidth*NormalOffSet);
      end;
      vdTop          :
      begin
        result.Y:=-round(FieldWidth*SmallOffSet);
        result.X:=round(-(FieldWidth*NormalOffSet));
      end;
      vdLeft         :
      begin
        result.Y:=round(FieldWidth*SmallOffSet);
        result.X:=round(-FieldWidth*NormalOffSet);
      end;
      vdRight        :
      begin
        result.Y:=-round(FieldWidth*SmallOffSet);
        result.X:=round(FieldWidth*NormalOffSet);
      end;
      vdLeftTop      : result.X:=round(-FieldWidth*LargeOffSet);
      vdRightBottom  : result.X:=round(FieldWidth*LargeOffSet);
    end;
  end;
end;

procedure TGameFigure.GoBack;
begin
  MoveTo(fLastPoint);
end;


function TGameFigure.RoundFinished: Boolean;
var
  Dummy: Integer;
  List : TList;
begin
  if (usDead in fState) then
  begin
    result:=true;
    exit;
  end;

  if fState=[] then
    fState:=[usReady];

  if (fState=[usMoveUnit]) and (not fWay.HasPath) then
    fState:=[usReady];

  result:=(usNeedTU in fState) or (usReady in fState) or (usWaitForOK in fState);

  // Pr�fen, ob noch Sch�sse von der Einheit unterwegs sind
  if result then
  begin
    SendVMessage(vmGetObjectList);
    List:=TList(Rec.resultObj);
    for Dummy:=0 to List.Count-1 do
    begin
      if TObject(List[Dummy]) is TWaffenObject then
      begin
        if TWaffenObject(List[Dummy]).Figure=Self then
        begin
          result:=false;
          exit;
        end;
      end;
    end;
  end;
end;

procedure TGameFigure.EndRound;
begin
  GiveOK;
  if fState=[] then
    fState:=[usReady];

  while (usMoveUnit in fState) and (not (usNeedTU in fState)) and (not (usDead in fState)) do
    Application.HandleMessage;

  fFrame:=0;
  fMoving:=false;
      
  Include(fState,usNeedTU);

  if fState=[usNeedTU] then
    fState:=[usReady];
end;

procedure TGameFigure.TakeItem(Item: TObject);
begin
  Assert(Manager<>nil);
  Manager.TakeItem(Item);
end;

procedure TGameFigure.ShowInfoText(Visible: Boolean);
begin
  Assert(fHintObject<>nil,Name);
  TFigureObject(fHintObject).ShowInfoText(Visible);
end;

{$IFDEF DEBUGMODE}
function TGameFigure.GetStateText: String;
var
  State: TUnitState;
begin
  result:='';
  for State:=low(TUnitState) to high(TUnitState) do
    if State in fState then
      result:=GetEnumName(TypeInfo(TUnitState),Ord(State))+', ';
end;
{$ENDIF}

procedure TGameFigure.KillUnit;
begin
  Dead;
end;

procedure TGameFigure.SetFigureStatus(const Value: TFigureStatus);
begin
  fFigureStatus := Value;
end;

function TGameFigure.GetTrefferZone(ISOObject: TObject): TTrefferZone;
var
  ShootX,ShootY,ShootZ : Integer;
  BodyRegion           : TBodyRegion;
begin
  Assert(ISOObject is TISOObject,'TGameFigure.GetTrefferZone: Object ist kein g�ltiges ISOObject');
  with TISOObject(ISOObject) do
  begin
    if GetSchadensTyp=stGlobal then
      // Globaler Schaden, die Position des Objektes spielt keine Rolle. Der Schaden wird gleichm��ig auf das Feld verteilt
      result:=tzGlobal
    else
    begin
      // Lokaler Schaden, nun muss nur noch die Trefferzone ermittelt werden
      GetPositionVektor(ShootX,ShootY,ShootZ);
      // Pr�fen in welcher K�rperregion (Z-Position) der Schuss liegt
      BodyRegion:=GetBodyRegion(ShootZ);
      if BodyRegion=brNone then
      begin
        // Schuss ging vermutlich �ber seinen Kopf hinaus
        result:=tzNone;
        exit;
      end;

      // Trefferzone ermitteln
      result:=GetTrefferZone(ShootX,ShootY,BodyRegion);
    end;
  end;
  // Neues Tileformat beachten
end;

procedure TGameFigure.BerechneExp(Points: Integer; Typ: TExpType);
var
  Dummy : Integer;
  Ent   : Integer;
begin
  if Points=0 then
    exit;
    
  if Typ=etTreffer then
  begin
//    exit;
    for Dummy:=0 to fList.Count-1 do
    begin
      if fList[Dummy]<>Self then
      begin
        with fList[Dummy] do
        begin
          if (Manager<>nil) and (FigureStatus=Self.FigureStatus) and (not ImRaumschiff) and (not IsDead) then
          begin
            Ent:=CalculateEntfern(Point(XPos,YPos),Point(Self.XPos,Self.YPos));
            Manager.berechneExp(round(Points/((Ent div 4)+2)),Typ)
          end;
        end;
      end;
    end;
  end;
  if Manager<>nil then
    Manager.BerechneExp(Points,Typ);
end;

procedure TGameFigure.CalcEXPSeeing(Figure: TGameFigure);
var
  Dummy: Integer;
begin
  Assert(Figure<>nil);

  for Dummy:=0 to high(fAllSeeFigures) do
  begin
    // Punkte gibt es nur bei der ersten Entdeckung
    if fAllSeeFigures[Dummy]=Figure then
      exit;
  end;

  // Einheit eintragen, damit die Punkte nicht noch mal verteilt werden
  SetLength(fAllSeeFigures,length(fAllSeeFigures)+1);
  fAllSeeFigures[High(fAllSeeFigures)]:=Figure;
  Assert(Figure.Manager<>nil);

  BerechneExp(Figure.Manager.ExpierencePoints div 4,etEntdeckt);
end;

procedure TGameFigure.EventManagerDestroy(Sender: TObject);
begin
  if fManager=Sender then
    fManager:=nil;
end;

function TGameFigure.GetName: String;
begin
  if (Manager<>nil) and (Manager is TSoldatManager) then
    Result := Format('[%.2s] %s',[TSoldatManager(Manager).SoldatInfo.ClassName,fName])
  else
    result:=fName;
end;

procedure TGameFigure.ChangeSelected(Selection: Boolean);
begin
  // Erzwingt das die Auswahl der Einheit aufgehoben wird, ohne eine neue Einheit
  // im Rundenmodus zu w�hlen
  fRemoveSelection:=true;
  Selected:=Selection;
  fRemoveSelection:=false;
end;

procedure TGameFigure.NoTUforAction;
begin
  ShowFriendMessage(Format(ST0309260005,[Name]));

  Include(fState,usNeedTU);
end;

function TGameFigure.GetBodyRegion(ZAxis: Integer): TBodyRegion;
begin
  result:=brNone;
  if ZAxis<=fModelInfo.FootMaxZ then
    result:=brFoot
  else if ZAxis<=fModelInfo.TorsoMaxZ then
    result:=brTorso
  else if ZAxis<=fModelInfo.HeadMaxZ then
    result:=brHead;
end;

function TGameFigure.GetTrefferZone(X, Y: Integer; Region: TBodyRegion): TTrefferZone;
type
  TTrefferKreis = (ttLeft,ttRight,ttFront,ttBack);
const
  Zones: Array[TViewDirection] of Array[TTrefferKreis] of record
          minW: Integer;
          maxW: Integer;
         end = (          {ttLeft}                {ttRight}               {ttFront}               {ttBack}
         {vdLeft}         ((minW:  105;maxW:  165),(minW:  -75;maxW:   30),(minW:  165;maxW:  -75),(minW:  -15;maxW:  105)),
         {vdLeftTop}      ((minW:  150;maxW: -150),(minW:  -30;maxW:   75),(minW: -150;maxW:  -30),(minW:   30;maxW:  150)),
         {vdTop}          ((minW: -165;maxW: -105),(minW:   15;maxW:   75),(minW: -105;maxW:   15),(minW:   75;maxW: -165)),
         {vdRightTop}     ((minW: -120;maxW:  -60),(minW:   60;maxW:  120),(minW:  -60;maxW:   60),(minW:  120;maxW: -120)),
         {vdRight}        ((minW:  -75;maxW:  -15),(minW:  105;maxW:  165),(minW:  -15;maxW:  105),(minW:  165;maxW:  -75)),
         {vdRightBottom}  ((minW:  -30;maxW:   30),(minW:  150;maxW: -150),(minW:   30;maxW:  150),(minW: -150;maxW:  -30)),
         {vdBottom}       ((minW:   15;maxW:   75),(minW: -165;maxW:  -60),(minW:   75;maxW: -165),(minW: -105;maxW:   15)),
         {vdLeftBottom}   ((minW:   60;maxW:  120),(minW: -120;maxW:  -15),(minW:  120;maxW: -120),(minW:  -60;maxW:   60)),


         {vdAll}         ((minW:    0;maxW:    0),(minW:    0;maxW:    0),(minW:    0;maxW:    0),(minW:    0;maxW:    0)));

  TrefferZone: Array[TBodyRegion] of Array[TTrefferKreis] of TTrefferZone =(
                    {ttLeft}     {ttRight}     {ttFront}     {ttBack}
        {brNone}   (tzNone,      tzNone,       tzNone,       tzNone),
        {brHead}   (tzHeadLeft,  tzHeadRight,  tzHeadFront,  tzHeadBack),
        {brTorso}  (tzTorsoLeft, tzTorsoRight, tzTorsoFront, tzTorsoBack),
        {brFoot}   (tzFootLeft,  tzFootRight,  tzFootFront,  tzFootBack));

var
  UnitOffSet: TPoint;
  Middle    : TPoint;
  Size      : Integer;
  Zone      : TTrefferKreis;
  Winkel    : Integer;
  Dummy     : TTrefferKreis;
begin
  UnitOffSet:=GetUnitOffset;
  Middle:=Point(HalfTileWidth+UnitOffSet.X,HalfTileHeight+UnitOffSet.Y);
  Size:=0;

  case Region of
    brHead  : Size:=fModelInfo.HeadSize div 2;
    brTorso : Size:=fModelInfo.torsoSize div 2;
    brFoot  : Size:=fModelInfo.FootSize div 2;
  end;

  Zone:=ttFront;

  if CalculateEntfern(Point(X,Y), Middle)<Size then
  begin
    if (Middle.X=x) and (Middle.Y=Y)then
      Zone:=ttFront
    else
    begin
      // Winkel Berechnen
      Winkel:=CalculateWinkel(Point(X,Y),Middle);
      if Winkel>180 then
        Winkel:=Winkel-360;
      for Dummy:=ttLeft to ttBack do
      begin
        with Zones[fViewDirection,Dummy] do
        begin
          if minW<maxW then
          begin
            if (Winkel>=minW) and (Winkel<=MaxW) then
            begin
              Zone:=Dummy;
              break;
	    end;
	  end
          else
          begin
            if (Winkel>=minW) or (Winkel<=MaxW) then
            begin
              Zone:=Dummy;
              break;
	    end;
	  end;
	end;
      end;
    end;
    result:=TrefferZone[Region,Zone];
  end
  else
    result:=tzNone;

  {$IFDEF DEBUGMODE}
  Rec.Text:=GetEnumName(TypeInfo(TTrefferZone),Ord(result));
  SendVMessage(vmTextMessage);
  {$ENDIF}
end;

procedure TGameFigure.GiveOK;
begin
  Exclude(fState,usWaitForOK);

  if fWay.HasPath and (not (usMoveUnit in fState)) then
  begin
    Exclude(fState,usReady);
    Include(fState,usMoveUnit);
  end;
end;

procedure TGameFigure.RecalcDrawingRect;
var
  OffSet: TPoint;
begin
  Rec.Figure:=Self;
  SendVMessage(vmCanDrawUnit);
  if (not Rec.Result) or (not IsOnField) then
  begin
    fDrawingRect:=Rect(-1,-1,-1,-1);
  end
  else
  begin
    Rec.Point:=Point(XPos,YPos);
    SendVMessage(vmGetTileRect);

    OffSet:=GetUnitOffset;

    fDrawingRect:=Bounds(Rec.Rect.Left+OffSet.X+ObjectOffSetX,Rec.Rect.Bottom-HalfTileHeight-fModelinfo.SelHeight+OffSet.Y,fModelInfo.SelWidth,fModelinfo.SelHeight);
  end;

  // Engine �ber Einheitenposition informieren
  Rec.Rect:=fDrawingRect;
  Rec.Figure:=Self;
  SendVMessage(vmSetDrawingRect);
end;

function TGameFigure.CheckHover(X, Y: Integer): Boolean;
var
  OffSet: TPoint;
begin
  Offset:=GetImagePosition(X,Y);

  result:=fImages.GetPixel(offSet.X,OffSet.Y,fFrame+(Integer(fViewDirection)*fFramesPerDirection))<>0;
end;

procedure TGameFigure.SetUnitToPos(Pos: TPoint);
begin
  Assert((Pos.X>=0) and (Pos.X<=fList.MapXSize),'ung�ltige X ('+IntToStr(Pos.X)+') Position');
  Assert((Pos.Y>=0) and (Pos.Y<=fList.MapYSize),'ung�ltige Y ('+IntToStr(Pos.Y)+') Position');

  DecSeeing;

  fXPos := Pos.X;
  fYPos := Pos.y;
  if fManager<>nil then
    fManager.SetEinsatzPosition(Pos);

  IncSeeing;
end;


procedure TGameFigure.ShowFriendMessage(Mess: String);
begin
  if (FigureStatus=fsFriendly) then
  begin
    Rec.Text:=Mess;
    SendVMessage(vmTextMessage);
  end;
end;

procedure TGameFigure.ResetNeedTUState;
begin
  if usNeedTU in fState then
  begin
    Include(fState,usWaitForOK);

    Exclude(fState,usNeedTU);
  end;
end;

procedure TGameFigure.SetTimeUnits(const Value: Integer);
begin
  fTimeUnits := Value;

  Rec.NotifyUIType:=nuitTimeUnitChange;
  SendVMessage(vmNotifyUI);
end;

function TGameFigure.GetTargetInfoText(ZielPos: TPoint;X,Y: Integer): String;
var
  Target : TGameFigure;
  Gezielt: Boolean;

  function GetTreffChance(Slot: TSoldatConfigSlot): String;
  var
    Infos: TShootInfos;
    Waffe: PSoldatWaffe;
  begin
    Waffe:=Manager.GetAddrOfSlot(Slot);

    if Waffe.TypeID=ptWaffe then
    begin
      if Waffe.WaffenArt=wtShortRange then
      begin
        if PointsSideBySide(Point(XPos,YPos),ZielPos) then
          result:='In Reichweite'
        else
          result:=CR0506270001;
      end
      else
      begin
        Infos.Range:=CalculateEntfern(Point(XPos,YPos),ZielPos);
        Infos.TargetUnit:=Target;
        Infos.TargetHeight:=-1;
        if (Target<>nil) and (Waffe.SchussArt=stGezielt) then
        begin
          Gezielt:=true;
          Infos.TargetHeight:=Target.GetTargetHeight(X,Y);
        end;

        result:=Format(FPercent,[Manager.CalcTreffChance(Slot,Infos)]);
      end;
    end;
  end;

begin
  Assert(Manager<>nil);
  Rec.Point:=ZielPos;
  SendVMessage(vmGetFigure);

  Target:=TGameFigure(Rec.Figure);

  result:='';

  Gezielt:=false;

  if Manager.LeftHandWaffe.Gesetzt then
    result:=GetTreffChance(scsLinkeHand);

  if Manager.RightHandWaffe.Gesetzt then
  begin
    if result='' then
      result:=GetTreffChance(scsRechteHand)
    else
      result:=Format('%s / %s',[result,GetTreffChance(scsRechteHand)]);
  end;

  if (Target<>nil) and Gezielt then
  begin
    Assert(Target.Manager<>nil);
    result:=Format('%s (%s)',[BodyRegionNames[Target.GetBodyRegion(Target.GetTargetHeight(X,Y))],result]);
  end;

end;

function TGameFigure.GetImagePosition(AbsX, AbsY: Integer): TPoint;
var
  OffSet: TPoint;
begin
  Offset:=GetUnitOffset;

  Rec.Point:=Point(XPos,YPos);
  SendVMessage(vmGetTileRect);

  dec(AbsX,Rec.Rect.Left);
  dec(AbsY,(Rec.Rect.Bottom-TileHeight));

  dec(AbsX,OffSet.X);
  dec(AbsX,ObjectOffSetX);

  dec(AbsY,OffSet.Y);
  inc(AbsY,ObjectOffSetY);

  result.Y:=AbsY;
  result.X:=AbsX;
end;

{ TFigureGroup }

procedure TFigureGroup.AddAll;
var
  Dummy: Integer;
begin
  Clear;
  for Dummy:=0 to fList.Count-1 do
  begin
    if fList[Dummy].FigureStatus=fsFriendly then
      AddUnit(fList[Dummy]);
  end;
end;

procedure TFigureGroup.AddUnit(Figure: TGameFigure);
begin
  SetLength(fFigures,length(fFigures)+1);
  fFigures[length(fFigures)-1]:=Figure;
end;

procedure TFigureGroup.Clear;
begin
  SetLength(fFigures,0);
end;

constructor TFigureGroup.Create(List: TGameFigureList);
begin
  fList:=List;
end;

procedure TFigureGroup.CreateGroup;
var
  Dummy: Integer;
begin
  Clear;
  for Dummy:=0 to fList.Count-1 do
    if fList[Dummy].Selected then AddUnit(fList[Dummy]);
end;

function TFigureGroup.GetUnitCount: Integer;
begin
  result:=length(fFigures);
end;

procedure TFigureGroup.SelectGroup;
var
  Dummy: Integer;
begin
  SendVMessage(vmContainerLock);
  for Dummy:=0 to fList.Count-1 do
    fList[Dummy].Selected:=false;
  for Dummy:=0 to High(fFigures) do
    fFigures[Dummy].Selected:=true;
  SendVMessage(vmContainerUnLock);
  SendVMessage(vmContainerIncLock);
  SendVMessage(vmAktuTempSurface);
  SendVMessage(vmRedrawSolList);
  SendVMessage(vmContainerDecLock);
//  SendVMessage(vmEngineRedraw);
end;

function TFigureGroup.UnitInGroup(Figure: TGameFigure): boolean;
var
  Dummy: Integer;
begin
  result:=false;
  for Dummy:=0 to high(fFigures) do
  begin
    if fFigures[Dummy]=Figure then
    begin
      result:=true;
      exit;
    end;
  end;
end;

{ TGameFigureList }

procedure TGameFigureList.AddFigure(Figure: TGameFigure);
begin
  fList.Add(Figure);
end;

procedure TGameFigureList.AktuSeeing;
var
  Dummy   : Integer;
  Figure  : ^Pointer;
begin
  SendVMessage(vmContainerLock);
  Figure:=Addr(fList.List^[0]);
  for Dummy:=0 to Count-1 do
  begin
    TGameFigure(Figure^).DecSeeing;
    TGameFigure(Figure^).IncSeeing;
    inc(Figure);
  end;
  SendVMessage(vmContainerUnLock);
  SendVMessage(vmAktuTempSurface);
end;

procedure TGameFigureList.AktuSeeingLists;
var
  Dummy: Integer;
begin
  for Dummy:=0 to Count-1 do
    Figure[Dummy].AktuSeeingList;
end;

procedure TGameFigureList.Clear;
begin
  fEnemySeeing:=0;
end;

procedure TGameFigureList.CountEnemys;
var
  Dummy: Integer;
begin
  fEnemys:=0;
  for Dummy:=0 to fList.Count-1 do
  begin
    if TGameFigure(fList[Dummy]).FigureStatus=fsEnemy then
      inc(fEnemys);
  end;
end;

constructor TGameFigureList.Create;
begin
  fList:=TList.Create;
  fNotifyList:=TNotifyList.Create;
end;

procedure TGameFigureList.DecEnemysSeeing;
begin
  dec(fEnemySeeing);
  if fEnemySeeing=0 then
    fNotifyList.CallEvents(EVENT_UNITSHIDE,Self);

  assert(fEnemySeeing>=0,Format('Enemysseeing ist im Minus (%d)',[fEnemySeeing]));
end;

procedure TGameFigureList.DeleteFigure(Figure: TGameFigure);
var
  Index: Integer;
begin
  Index:=fLIst.IndexOf(Figure);
  if Index<>-1 then
    fList.Delete(Index);
end;

destructor TGameFigureList.destroy;
begin
  fList.Free;
  fNotifyList.Free;
  inherited;
end;

procedure TGameFigureList.EinheitAufFeldPlaziert(Figure: TGameFigure);
var
  Dummy   : Integer;
begin
  for Dummy:=0 to fList.Count-1 do
  begin
    TGameFigure(fList[Dummy]).CheckSeeing(Figure);
    TGameFigure(fList[Dummy]).CountSeeUnits;
  end;
end;

procedure TGameFigureList.EinheitGestorben(Figure: TGameFigure);
var
  Dummy   : Integer;
  Friends : Integer;
begin
  Figure.ClearSeeingList;
  Friends:=0;
  fEnemys:=0;
  for Dummy:=0 to fList.Count-1 do
  begin
    TGameFigure(fList[Dummy]).RemoveFromSeeingList(Figure);
    if not TGameFigure(fList[Dummy]).IsDead then
    begin
      case TGameFigure(fList[Dummy]).FigureStatus of
        fsFriendly : inc(Friends);
        fsEnemy    : inc(fEnemys);
      end;
    end;
  end;
  if (Enemys=0) and (Friends>0) then
  begin
    SendVMessage(vmMissionWin);
  end;
  if (Friends=0) and (Enemys>0) then
  begin
    SendVMessage(vmMissionLose);
  end;
end;

function TGameFigureList.GetCount: Integer;
begin
  result:=fList.Count;
end;

function TGameFigureList.GetFigure(Index: Integer): TGameFigure;
begin
  result:=fList[Index];
end;

function TGameFigureList.GetFigureOfManager(Manager: TGameFigureManager): TGameFigure;
var
  Dummy: Integer;
begin
  for Dummy:=0 to fList.Count-1 do
  begin
    if TGameFigure(fList[Dummy]).Manager=Manager then
    begin
      result:=fList[Dummy];
      exit;
    end;
  end;
  result:=nil;
  Assert(false,'Keine passende TGameFigure gefunden');
end;

function TGameFigureList.GetMapXSize: Integer;
begin
  result:=PathFinder.GetMapSize.x;
end;

function TGameFigureList.GetMapYSize: Integer;
begin
  result:=PathFinder.GetMapSize.Y;
end;

function TGameFigureList.GetSelected: TGameFigure;
var
  Dummy: Integer;
begin
  result:=nil;
  for Dummy:=0 to Count-1 do
  begin
    if TGameFigure(fList[Dummy]).Selected then
    begin
      result:=fList[Dummy];
      exit;
    end;
  end;
end;

procedure TGameFigureList.IncEnemysSeeing;
begin
  inc(fEnemySeeing);
  if fEnemySeeing=1 then
    fNotifyList.CallEvents(EVENT_SEEUNITS,Self);
end;

procedure TGameFigureList.NextRound;
var
  Dummy: Integer;
begin
  // Zeiteinheiten f�r Eigene Einheiten zur�cksetzen
  // Das zur�cksetzen, f�r die feindlichen/neutralen Einheiten erfolgt in den KIs
  for Dummy:=0 to Count-1 do
    TGameFigure(fList[Dummy]).NextRound;
end;

function TGameFigureList.SelectedCount: Integer;
var
  Dummy  : Integer;
  Figure : ^Pointer;
begin
  result:=0;
  Figure:=Addr(fList.List^[0]);
  for Dummy:=0 to fList.Count-1 do
  begin
    if TGameFigure(Figure^).Selected and (TGameFigure(Figure^).FigureStatus=fsFriendly) then
      inc(result);
    inc(Figure);
  end;
end;

procedure TGameFigureList.SetSelected(const Value: TGameFigure);
begin
  Assert(Value<>nil);
  Value.Select;
end;

procedure TGameFigureList.WaffenPosChanged(Obj: TObject);
var
  Dummy   : Integer;
  Point   : TPoint;
  Figure  : ^Pointer;
begin
  Assert(Obj is TWaffenObject);
  Point.X:=TWaffenObject(Obj).X;
  Point.Y:=TWaffenObject(Obj).Y;
  Figure:=Addr(fList.List^[0]);
  for Dummy:=0 to Count-1 do
  begin
    if (TGameFigure(Figure^).FigureStatus<>TWaffenObject(Obj).Figure.FigureStatus) and (TGameFigure(Figure^).CanSeePoint(Point.X,Point.Y)) then
    begin
      TWaffenObject(Obj).WasSeen:=true;
      TGameFigure(Figure^).NotifyList.CallEvents(EVENT_FIGURE_UNDERFIRE,Obj);
      if (TGameFigure(Figure^).FigureStatus=fsFriendly) and (TGameFigure(Figure^).SeeCount=0) then
      begin
        Rec.Point:=Point;
        Rec.Text:=Format(ST0309260008,[TGameFigure(fList[Dummy]).Name]);
        Rec.MessageType:=emUnitUnderFire;
        SendVMessage(vmMessage);
      end;
      exit;
    end;
    inc(Figure);
  end;
end;

var
  Dummy: Integer;

procedure TGameFigureList.WaitForRoundEnd;
var
  Dummy: Integer;
begin
  for Dummy:=0 to Count-1 do
  begin
    TGameFigure(fList[Dummy]).EndRound;
    while not TGameFigure(fList[Dummy]).RoundFinished do
      Application.HandleMessage;
  end;
end;

function TGameFigure.ShootToUnit(Figure: TGameFigure): Boolean;
begin
  result:=ShootToUnit(Figure,scsLinkeHand);
  result:=ShootToUnit(Figure,scsRechteHand) or result;
end;

function TGameFigure.ShootToUnit(Figure: TGameFigure; MousePos: TPoint): Boolean;
var
  Infos : TShootInfos;

  procedure Shoot(Slot: TSoldatConfigSlot);
  var
    Waffe: PSoldatWaffe;
  begin
    Waffe:=fManager.GetAddrOfSlot(Slot);
    if Waffe.Gesetzt then
    begin
      if Waffe.SchussArt=stGezielt then
      begin
        Infos.TargetHeight:=Figure.GetTargetHeight(MousePos.X,MousePos.Y);
        Infos.TargetUnit:=Figure;
      end
      else
      begin
        Infos.TargetHeight:=-1;
        Infos.TargetUnit:=nil;
      end;

      result:=ShootToUnit(Figure,Slot,Addr(Infos)) or result;
    end;
  end;
begin
  result:=false;
  Assert(fManager<>nil);
  Assert(Figure<>nil);

  Infos.TargetUnit:=Figure;

  Shoot(scsLinkeHand);
  Shoot(scsRechteHand);
end;

function TGameFigure.GetTargetHeight(AbsX, AbsY: Integer): Integer;
var
  OffSet: TPoint;
begin
  Offset:=GetUnitOffset;

  Rec.Point:=Point(XPos,YPos);
  SendVMessage(vmGetTileRect);

  result:=-(AbsY-(Rec.Rect.Bottom-HalfTileHeight));
  if result<0 then
    result:=0;
end;

function TGameFigure.GetPosition: TPoint;
begin
  result:=Point(XPos,YPos);
end;

initialization

  { Seefelder initsialisieren }
  NeededMem:=0;

finalization

  for Dummy:=low(SeeingData) to high(SeeingData) do
  begin
    if SeeingData[Dummy]<>nil then
      FreeMem(SeeingData[Dummy]);
  end;
  GlobalFile.Write('NeededMem',NeededMem);
end.


