{********************************************************************************
*										*
* Copyright (C) 2004  X-Force Team - jim_raynor@web.de				*
*										*
* This file is part of X-Force: Fight For Destiny (X-Force).			*
*										*
* X-Force is free software; you can redistribute it and/or modify		*
* it under the terms of the GNU General Public License as published by		*
* the Free Software Foundation; either version 2 of the License, or		*
* (at your option) any later version.						*
*										*
* X-Force is distributed in the hope that it will be useful,			*
* but WITHOUT ANY WARRANTY; without even the implied warranty of		*
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the			*
* GNU General Public License for more details.					*
*										*
* You should have received a copy of the GNU General Public License		*
* along with X-Force; if not, write to the Free Software			*
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA	*
*										*
*********************************************************************************
*										*
*										*
********************************************************************************}

{$I ../Settings.inc}

unit BitmapMask;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs;

type
  TBitmapMask = class(TObject)
  private
    fMask   : Array of Array of Byte;
    fWidth  : Integer;
    fHeight : Integer;
    function GetPixel(X, Y: Integer): Byte;
    { Private-Deklarationen }
  public
    procedure GenerateMask(Bitmap: TBitmap);
    property Pixel[X,Y: Integer]: Byte read GetPixel;default;
    property Width  : Integer read fWidth;
    property Height : Integer read fHeight;
    { Public-Deklarationen }
  end;

implementation

{ TBitmapMask }

procedure TBitmapMask.GenerateMask(Bitmap: TBitmap);
var
  X: Integer;
  Y: Integer;
begin
  fWidth:=Bitmap.Width;
  fHeight:=Bitmap.Height;
  SetLength(fMask,fWidth,fHeight);
  for X:=0 to Bitmap.Width-1 do
    for Y:=0 to Bitmap.Height-1 do
      fMask[X,Y]:=GetRValue(Bitmap.Canvas.Pixels[X,Y])
end;

function TBitmapMask.GetPixel(X, Y: Integer): Byte;
begin
  result:=fMask[X,Y];
end;

end.
