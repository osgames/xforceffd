{********************************************************************************
*										*
* Copyright (C) 2004  X-Force Team - jim_raynor@web.de				*
*										*
* This file is part of X-Force: Fight For Destiny (X-Force).			*
*										*
* X-Force is free software; you can redistribute it and/or modify		*
* it under the terms of the GNU General Public License as published by		*
* the Free Software Foundation; either version 2 of the License, or		*
* (at your option) any later version.						*
*										*
* X-Force is distributed in the hope that it will be useful,			*
* but WITHOUT ANY WARRANTY; without even the implied warranty of		*
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the			*
* GNU General Public License for more details.					*
*										*
* You should have received a copy of the GNU General Public License		*
* along with X-Force; if not, write to the Free Software			*
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA	*
*										*
*********************************************************************************
*										*
* Stellt den Geoscape mit allen Verwaltungsoptionen zur Verf�gung		*
*										*
********************************************************************************}

{$I ../Settings.inc}

unit DXTakticScreen;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  DXContainer, DXDraws, UFOList,Defines, KD4Utils, Blending,DirectDraw, math,
  TraceFile, XForce_types, NGTypes, RaumschiffList, OrganisationList, EinsatzListe,
  BasisListe, DXBitmapButton, DirectFont, Direct3d, D3DUtils, EarthCalcs,
  GlobeRenderer, KD4SaveGame, D3DRender, StringConst, NotifyList
  {$IFDEF DEBUGMODE},TypInfo{$ENDIF};

type
  TBasisSelect        = procedure(Basis: TBasis) of object;
  TUFOSelect          = procedure(UFO: TUFO) of object;
  TEinsatzSelect      = procedure(Einsatz: TEinsatz) of object;
  TRaumschiffSelect   = procedure(Raumschiff: TRaumschiff) of object;
  TTownSelect         = procedure(Town: TTown) of object;
  TObjectSelect       = procedure(Objekt    : TObject) of object;
  TPositionSelect     = procedure(Position: TFloatPoint) of object;
  TMultipleObjekts    = function(Objekts: TStringList): Integer of object;

  TButtonClick        = procedure(Data: Integer) of object;

  TSelectRecord       = record
    Result      : Boolean;
    UFO         : TUFO;
    Raumschiff  : TRaumschiff;
    Einsatz     : TEinsatz;
    Basis       : TBasis;
    Town        : TTown;
    Position    : TFloatPoint;
  end;

  PDrawingObject = ^TDrawingObject;
  TDrawingObject  = record
    Objekt    : TObject;
    Position  : TPoint;
    FieldSize : Integer;
    Pattern   : Integer;
    Alpha     : Integer;
  end;

  PDiffusevertex = ^TDiffuseVertex;
  TDiffuseVertex = record
    sx,sy,sz : Single;
    rhw      : single;
    Color    : TD3DColor;
  end;

  TDXTakticInfo       = class;
  TDXTakticPlugIn     = class;
  TDXFilterButtons    = class;

  TDXTakticScreen = class(TDXComponent)
  private
    // Objekte auf der Weltkugel
    fObjekts         : Array of TDrawingObject;
    fObjektCount     : Integer;

    fOverObjekt      : Integer;
    fSelectedObjekt  : TSelectRecord;
    fZoom            : double;
    fDrawText        : boolean;
    fSelectModus     : boolean;
    fMultiple        : TMultipleObjekts;
    fText            : TStringList;
    fTextPoint       : TPoint;
    fOnHunting       : TNotifyEvent;

    // Events im SelectModus (Rechtsklick)
    fUFOSelect       : TUFOSelect;
    fBasisSelect     : TBasisSelect;
    fEinsatzSelect   : TEinsatzSelect;
    fPosSelect       : TPositionSelect;
    fSelectSchiff    : TRaumschiffSelect;
    fSelectTown      : TTownSelect;

    // Objekt wird ausgew�hlt (Linksklick)
    fSelectObject    : TObjectSelect;

    fScrollPos       : TPoint;

    fMoveRedraw      : boolean;

    // Rechte Maustaste gedr�ckt? wird zur�ckgesetzt wenn Maus bewegt wird
    fRightDown       : boolean;

    fUISymbols       : TDirectDrawSurface;
    fUITexture       : TDirect3DTexture2;

    // Rotationsmatrix
    fMatrix          : TD3DMatrix;

    fPlugIns         : TList;
    fShortCuts       : Array of record
                         Key      : Char;
                         Enabled  : Boolean;
                         OnClick  : TButtonClick;
                         Data     : Integer;
                       end;

    // Fluglinie von Objekten als LineStrip
    fLineVertex      : Array of TDiffuseVertex;
    fVertexCount     : Integer;

    // Plugins die direkt von Takticscreen verwaltet werden
    fButtons         : TDXFilterButtons;
    fInfo            : TDXTakticInfo;

    // Anzeige eines Hints
    fHintInfo        : record
                         Show       : boolean;
                         Text       : String;
                         Rect       : TRect;
                       end;

    // Neue Funktionen zur Berechnung der Erde
    procedure RenderEarth;

    function GradToScreen(Grad: TFloatPoint; out Screen: TPoint; var Alpha: Integer; ToComponent: Boolean = true): Boolean;
    function MouseToGrad(Mouse: TPoint;out Grad: TFloatPoint): Boolean;

    function GetGlobusRadius: double;

    procedure ShortCutObjekts(Data: Integer);

    procedure SetZoom(const Value: double);
    function SelectObjekts(Point: TPoint;out ShouldCenter: Boolean): TSelectRecord;
    procedure SelectObjekt(Point: TPoint);
    function PointOverObject(Pt1: TPoint;Objekt: PDrawingObject): boolean;
//    procedure SetObject(const Value: TFloatPoint);
    function AktuText(MousePos: TPoint): boolean;
    procedure ClearSelection;

    procedure RefreshObjectList;
    procedure AddObject(Pt: TFloatPoint;Pattern: Integer; Objekt: TObject; FieldSize: Integer = 4);
    function IsWarningIconVisible: Boolean;

    procedure AktuInfoTextPos;
    { Private-Deklarationen }
  protected
    function PerformFrame(Sender: TObject; Frames: Integer): Boolean;
    procedure DrawObjects(Surface: TDirectDrawSurface);
    procedure DrawObject(Surface: TDirectDrawSurface; ObjektPtr: PDrawingObject);
    procedure DrawSelection(Surface: TDirectDrawSurface);
    procedure DrawMenu(Surface: TDirectDrawSurface);
    procedure RestoreDirectX;
    procedure DoScroll(Direction: TScrollDirection; Pos: TPoint);override;
    { Protected-Deklarationen }
  public
    constructor Create(Page: TDXPage);override;
    destructor Destroy;override;
    procedure DeactiveControl;override;
    procedure Draw(Surface: TDirectDrawSurface;var Mem: TDDSurfaceDesc);override;
    procedure ReDrawRect(DrawRect: TRect;Surface: TDirectDrawSurface;var Mem: TDDSurfaceDesc);override;
    procedure DrawSymbol(Surface: TDirectDrawSurface; X,Y: Integer; Pattern: Integer; Alpha: Integer = 255);
    procedure MouseDown(Button: TMouseButton;X,Y: Integer);override;
    procedure MouseUp(Button: TMouseButton;X,Y: Integer);override;
    procedure MouseMove(X,Y: Integer);override;

    procedure KeyPress(var Key: Word;State: TShiftState);override;
    procedure SetHotKey(Key: Char; Enabled: Boolean; Onclick: TButtonClick;Data: Integer);

    procedure RegisterPlugIn(PlugIn: TDXTakticPlugIn);
    
    procedure AktuMousePosition;

    procedure CenterView(Pt: TFloatPoint);
    procedure CenterSelected;

    procedure MouseLeave;override;
    procedure MouseOver;override;
    procedure Reset;
    procedure Redraw;override;
    procedure Resize(var NewLeft,NewTop,NewWidth,NewHeight: Integer);override;
    procedure SetSelectPosModus(Modus: boolean);

    procedure ShowHint(Text: String; Pos: TPoint);

    function NoObjektSelect: boolean;
    function GetSelectedPoint(out Point: TFloatPoint): boolean;

    procedure DrawListItem(Sender: TDXComponent;Surface: TDirectDrawSurface;const Mem: TDDSurfaceDesc;Rect: TRect; Index: Integer;Selected: boolean);

    property Zoom               : double read fZoom write SetZoom;

    // Events im SelectModus (Rechtsklick)
    property OnSelectUFO        : TUFOSelect read fUfoSelect write fUFOSelect;
    property OnSelectBasis      : TBasisSelect read fBasisSelect write fBasisSelect;
    property OnSelectEinsatz    : TEinsatzSelect read fEinsatzSelect write fEinsatzSelect;
    property OnSelectPosition   : TPositionSelect read fPosSelect write fPosSelect;
    property OnSelectSchiff     : TRaumschiffSelect read fSelectSchiff write fSelectSchiff;
    property OnSelectTown       : TTownSelect read fSelectTown write fSelectTown;
    property OnHunting          : TNotifyEvent read fOnHunting write fOnHunting;
    // Objekt wird ausgew�hlt (Linksklick)
    property OnSelectObject     : TObjectSelect read fSelectObject write fSelectObject;

    property OnMultipleObjekts  : TMultipleObjekts read fMultiple write fMultiple;
    property MoveRedraw         : boolean read fMoveRedraw write fMoveRedraw;
    { Public-Deklarationen }
  end;

  PDrawDotScreen      = ^TDrawDotScreen;
  TDrawDotScreen      = record
    Screen            : TDXTakticScreen;
    Surface           : TDirectDrawSurface;
    Mem               : TDDSurfaceDesc;
    LastPoint         : TPoint;
    FirstPoint        : Boolean;
    Alpha             : Integer;
    Color             : TBlendColor;
  end;

  TDXTakticPlugIn = class(TObject)
  protected
    FLeft      : Integer;
    FTop       : Integer;
    fScreen    : TDXTakticScreen;
    fUISymbols : TDirectDrawSurface;
    procedure MouseUp(Button: TMouseButton;X,Y: Integer);virtual;
    procedure MouseMove(X,Y: Integer; var NRedraw: Boolean);virtual;
    procedure MouseLeave;virtual;
    procedure Restore;virtual;
  public
    constructor Create(Screen: TDXTakticScreen);virtual;
    destructor Destroy;override;
    procedure Draw(Surface: TDirectDrawSurface);virtual;abstract;
    property Left      : Integer read FLeft write FLeft;
    property Top       : Integer read FTop write FTop;
    property UISymbols : TDirectDrawSurface read fUISymbols write fUISymbols;
  end;

  TDXFilterButtons = class(TDXTakticPlugIn)
  private
    fButtons    : Array of record
                    ID        : Integer;
                    Visible   : boolean;
                    Pos       : TPoint;
                    Image     : TRect;
                    Hint      : String;
                    Flashing  : Boolean;
                    Incr      : Boolean;
                    Alpha     : Integer;
                  end;
    fOverID     : Integer;
    function FindButton(ID: Integer): Integer;
  protected
    procedure MouseMove(X,Y: Integer; var NRedraw: Boolean);override;
    procedure MouseLeave;override;
  public
    procedure Draw(Surface: TDirectDrawSurface);override;
    procedure AddButton(ID: Integer; Pos: TPoint; Image: TRect; Hint: String; Flashing: Boolean = false);
    procedure SetVisible(ID: Integer;Visible: Boolean);
    function PerformFrame: Boolean;

    function GetButtonAtPos(MousePos: TPoint): Integer;

    property OverButton: Integer read fOverID;
  end;

  TPutLineEvent = procedure(Text: String;var Top: Integer;Font: TDirectFont = nil;Margin: Integer = 0) of object;

  TDXTakticInfo = class(TDXTakticPlugin)
  private
    fUFO             : TUFO;
    fRaumschiff      : TRaumschiff;
    fEinsatz         : TEinsatz;
    fBasis           : TBasis;
    fTown            : TTown;
    fVisible         : Boolean;
    PutLine          : TPutLineEvent;

    fShowPadie       : TShowUFOPadie;
    fSurface         : TDirectDrawSurface;

    fRectWidth       : Integer;
    fRectHeight      : Integer;

    fOnDestroyEvent  : TEventHandle;
  protected
    procedure DefPutLine(Text: String;var Top: Integer;Font: TDirectFont = nil;Margin: Integer = 0);
    procedure EnumPutLine(Text: String;var Top: Integer;Font: TDirectFont = nil;Margin: Integer = 0);
    procedure DestroySelection(Sender: TObject);
    procedure ClearSelection;
    procedure CalcSize;
  public
    constructor Create(Screen: TDXTakticScreen);override;
    destructor destroy;override;
    procedure Draw(Surface: TDirectDrawSurface);override;
    procedure SelectUFO(UFO: TUFO);
    procedure SelectRaumschiff(Raumschiff: TRaumschiff);
    procedure SelectEinsatz(Einsatz: TEinsatz);
    procedure SelectTown(Town: TTown);
    procedure SelectBasis(Basis: TBasis);

    property OnUFOPadie         : TShowUFOPadie read fShowPadie write fShowPadie;
    property Visible            : Boolean read fVisible write fVisible;
  end;

  const
    WarningIcon    : TRect = (Left: 13; Top: 20; Right: 28; Bottom: 35);
    WarningIconL   : TRect = (Left:107; Top: 20; Right:138; Bottom: 52);
    WarningRect    : TRect = (Left:200; Top: 30; Right:233; Bottom: 49);
    WarningRectL   : TRect = (Left:  7; Top: 57; Right: 43; Bottom: 93);
    SmallButtonOver: TRect = (Left:190; Top: 20; Right:223; Bottom: 39);
    SmallButton    : TRect = (Left:190; Top: 39; Right:223; Bottom: 58);

    ImageHeight    = 180;
    ImageWidth     = 360;
    EarthSize      = 120;
    ButtonWidth    = 33;
    ButtonHeight   = 19;

    MinZoom        = 1;
    MaxZoom        = 3;

var
  EinsatzFont     : TDirectFont;
  RaumschiffFont  : TDirectFont;
  UFOFont         : TDirectFont;
  BasisFont       : TDirectFont;
  SmallWhiteFont  : TDirectFont;
  SmallYellowFont : TDirectFont;
  SmallRedFont    : TDirectFont;
  SmallLimeFont   : TDirectFont;

// Globale Instanz des Erden Renderers
  gGlobeRenderer: TGlobeRenderer;

  procedure SetCompatibleMode(Compatible: Boolean);
  function GetCompatibleMode: Boolean;

implementation

uses
  DXListBox, basis_api, ufo_api, einsatz_api, raumschiff_api, town_api,
  game_api, game_utils, earth_api;

var
  CompatibleMode  : Boolean = false;

const
  WarningButton    = $5F09552D;
  SchiffButton     = $488EA3E8;
  TownButton       = $5E95578C;
  BasisButton      = $027BABC3;
  CenterButton     = $3BEE10F2;
  BackBasisButton  = $4563E92E;
  HuntUFOButton    = $20A7C9EB;

procedure SetCompatibleMode(Compatible: Boolean);
begin
  if CompatibleMode = Compatible then
    exit;

  CompatibleMode := Compatible;

  if gGlobeRenderer<>nil then
  begin
    gGlobeRenderer.SetCompatibleMode(Compatible);
  end;
end;

function GetCompatibleMode: Boolean;
begin
  result:=CompatibleMode;
end;

procedure DrawDots(Point: TFloatPoint; Data: Pointer; var Stop :boolean);
var
  ScreenPos  : TPoint;
  Alpha      : Integer;
  Screen     : PDrawDotScreen;
  Dummy      : Integer;
begin
  Screen:=Data;
  if not Screen.Screen.GradToScreen(Point,ScreenPos,Alpha,false) then
  begin
    if not Screen.FirstPoint then
      Stop:=true;
    exit;
  end;
  if Screen.FirstPoint then
  begin
    Screen.FirstPoint:=false;
    Screen.LastPoint:=ScreenPos;
    exit;
  end;

  with Screen.Screen do
  begin
    inc(fVertexCount);
    if fVertexCount>high(fLineVertex) then
    begin
      SetLength(fLineVertex,length(fLineVertex)+10);
      for Dummy:=fVertexCount to high(fLineVertex) do
        fLineVertex[Dummy].rhw:=1.0;
    end;

    with fLineVertex[fVertexCount-1] do
    begin
      sx:=ScreenPos.X;
      sy:=ScreenPos.Y;
      Color:=(Screen.Color and $00FFFFFF) or ((Byte(Screen.Alpha) shl 24) and $FF000000);
    end;
  end;
//  Screen.LastPoint:=ScreenPos;
end;

{ TDXTakticScreen }

procedure TDXTakticScreen.CenterView(Pt: TFloatPoint);
var
  XMatrix,YMatrix: TD3DMatrix;
begin
  D3DUtil_SetRotateYMatrix(YMatrix,(DegToRad(((360-Pt.X)-90))));
  D3DUtil_SetRotateXMatrix(XMatrix,(DegToRad((90-Pt.Y))));

  D3DMath_MatrixMultiply(fMatrix,YMatrix,XMatrix);

  RenderEarth;
  RefreshObjectList;
  Redraw;
end;

constructor TDXTakticScreen.Create(Page: TDXPage);
var
  Font : TFont;
begin
  inherited;
  GlobalFile.Write('TDXTakticScreen.Create');
  fPlugIns:=TList.Create;
  TabStop:=true;

  fMoveRedraw:=true;
  fScrollPos.X:=-1;
  fDrawText:=true;
  fText:=TStringList.Create;
  fSelectModus:=false;
  fZoom:=MinZoom;

  if EinsatzFont=nil then
  begin
    Font:=TFont.Create;
    Font.Size:=coFontSize;
    Font.Name:=coFontName;
    Font.Color:=clBlue;
    EinsatzFont:=FontEngine.FindDirectFont(Font,clBlack);
    Font.Color:=$004A4AFF;
    UFOFont:=FontEngine.FindDirectFont(Font,clBlack);
    Font.Color:=clSilver;
    RaumschiffFont:=FontEngine.FindDirectFont(Font,clBlack);
    Font.Color:=clGreen;
    BasisFont:=FontEngine.FindDirectFont(Font,clBlack);

    Font.Color:=clWhite;
    Font.Name:='Arial';
    Font.Size:=7;
    SmallWhiteFont:=FontEngine.FindDirectFont(Font,clBlack,true);
    Font.Color:=clYellow;
    SmallYellowFont:=FontEngine.FindDirectFont(Font,clBlack,true);
    Font.Color:=clLime;
    SmallLimeFont:=FontEngine.FindDirectFont(Font,clBlack,true);
    Font.Color:=clRed;
    SmallRedFont:=FontEngine.FindDirectFont(Font,clBlack,true);
    Font.Free;
  end;

  Container.AddFrameFunction(PerformFrame,Self,25);

  Container.AddRestoreFunction(RestoreDirectX);

  if gGlobeRenderer=nil then
  begin
    gGlobeRenderer:=TGlobeRenderer.Create(Page.Container);
  end;

  // Auswahlbuttons
  fButtons:=TDXFilterButtons.Create(Self);
  fButtons.AddButton(WarningButton,Point(WarningRect.Left,WarningRect.Top),WarningIcon,ST0311070001+' ('#2'6'#1')',true);
  fButtons.AddButton(SchiffButton,Point(WarningRect.Left+40,WarningRect.Top),Rect(73,20,89,36),ST0311070002+' ('#2'7'#1')');
  fButtons.AddButton(TownButton,Point(WarningRect.Left+80,WarningRect.Top),Rect(89,20,105,36),ST0311070003+' ('#2'8'#1')');
  fButtons.AddButton(BasisButton,Point(WarningRect.Left+120,WarningRect.Top),Rect(105,20,121,36),ST0311070004+' ('#2'9'#1')');

  fButtons.AddButton(CenterButton,Point(10,65),Rect(174,19,190,35),ST0311070005+' ('#2'Z'#1')');

  fButtons.AddButton(BackBasisButton,Point(10,95),Rect(122,43,138,58),ST0311070006);
  fButtons.AddButton(HuntUFOButton,Point(10,95),Rect(193,64,209,80),ST0311070007+' ('#2'B'#1')');

  // Infobildschirm
  fInfo:=TDXTakticInfo.Create(Self);
  fInfo.Left:=55;
  fInfo.Top:=65;

  ClearSelection;

  RestoreDirectX;

  SetHotKey('6',true,ShortCutObjekts,0);
  SetHotKey('7',true,ShortCutObjekts,1);
  SetHotKey('8',true,ShortCutObjekts,2);
  SetHotKey('9',true,ShortCutObjekts,3);

  SetHotKey('Z',true,ShortCutObjekts,4);

  SetHotKey('S',true,ShortCutObjekts,5);

  SetHotKey('B',true,ShortCutObjekts,6);
end;

destructor TDXTakticScreen.Destroy;
var
  Dummy: Integer;
begin
  for Dummy:=fPlugIns.Count-1 downto 0 do
    TObject(fPlugIns[Dummy]).Free;

  Container.DeleteRestoreFunction(RestoreDirectX);
  fText.Free;
  fPlugIns.Free;
  if fUITexture<>nil then
    fUITexture.Free;
  inherited;
end;

procedure TDXTakticScreen.Draw(Surface: TDirectDrawSurface;var Mem: TDDSurfaceDesc);
begin
  ReDrawRect(ClientRect,Surface,Mem);
end;

procedure TDXTakticScreen.DrawObject(Surface: TDirectDrawSurface; ObjektPtr: PDrawingObject);
var
  TempX       : Integer;
  TempY       : Integer;
begin
  TempX:=round(ObjektPtr.Position.X)+Left;
  TempY:=round(ObjektPtr.Position.Y)+Top;

  DrawSymbol(Surface,TempX-4,TempY-4,ObjektPtr.Pattern,ObjektPtr.Alpha);
end;

procedure TDXTakticScreen.MouseUp(Button: TMouseButton; X, Y: Integer);
var
  Mouse    : TPoint;
  Select   : TSelectRecord;
  Dummy    : Integer;
  Center   : Boolean;
begin
  inherited;
  Container.ReleaseCapture;
  if Button=mbRight then
  begin
    fScrollPos.X:=-1;
    if fRightDown then
    begin
      // Status zur Auswahl eines Befehles
      fSelectModus:=true;
    end
    else
      exit;
  end;
  for Dummy:=0 to fPlugIns.Count-1 do
    TDXTakticPlugIn(fPlugIns[Dummy]).MouseUp(Button,X,Y);
  Mouse:=Point(X,Y);
  if not PtInRect(Rect(0,0,Width,Height),Mouse) then
    exit;

  if not fSelectModus then
  begin
    SelectObjekt(Mouse);
  end
  else
  begin
    // Wird nicht mehr ben�tigt und kann zur�ckgesetzt werden
    if fRightDown then
    begin
      SetSelectPosModus(false);
      fRightDown:=false;
    end;
    if (fSelectedObjekt.Raumschiff=nil) or (not fSelectedObjekt.Raumschiff.XCOMSchiff) or (fSelectedObjekt.Raumschiff.Status=ssNoMotor) then
    begin
      exit;
    end;
    Select:=SelectObjekts(Mouse,Center);
    if not Select.Result then
      exit;
    if Select.Basis<>nil then
    begin
      if Assigned(fBasisSelect) then
        fBasisSelect(Select.Basis);
    end
    else if Select.UFO<>nil then
    begin
      if Assigned(fUFOSelect) then
        fUFOSelect(Select.UFO);
    end
    else if Select.Raumschiff<>nil then
    begin
      if Assigned(fSelectSchiff) then
        fSelectSchiff(Select.Raumschiff);
    end
    else if Select.Einsatz<>nil then
    begin
      if Assigned(fEinsatzSelect) then
        fEinsatzSelect(Select.Einsatz);
    end
    else if Select.Town<>nil then
    begin
      if Assigned(fSelectTown) then
        fSelectTown(Select.Town);
    end
    else
    begin
      if Assigned(fPosSelect) then
        fPosSelect(Select.Position);
    end;

    fButtons.SetVisible(BackBasisButton,(fSelectedObjekt.Raumschiff<>nil) and (Select.Basis<>fSelectedObjekt.Raumschiff.GetHomeBasis));
    
    Container.SetCursor(CZeiger);
    fSelectModus:=false;
    Redraw;
  end;
  fRightDown:=false;
  fScrollPos.X:=-1;
end;

procedure TDXTakticScreen.MouseLeave;
var
  Dummy: Integer;
begin
  fScrollPos.X:=-1;

  for Dummy:=0 to fPlugIns.Count-1 do
    TDXTakticPlugIn(fPlugIns[Dummy]).MouseLeave;

  Container.SetCursor(CZeiger);
  if (fText.Count>0) then
  begin
    fText.Clear;
    Redraw;
  end;
end;

procedure TDXTakticScreen.MouseMove(X, Y: Integer);
var
  Dummy     : Integer;
  Objekt    : Integer;
  ObjektPtr : PDrawingObject;
  Mouse     : TPoint;
  XOff      : Integer;
  YOff      : Integer;
  NRedraw   : boolean;
  Temp      : TD3DMatrix;
  Temp2     : TD3DMatrix;
begin
  inherited;

  NRedraw:=false;

  for Dummy:=0 to fPlugIns.Count-1 do
  begin
    TDXTakticPlugIn(fPlugIns[Dummy]).MouseMove(X,Y,NRedraw);
  end;

  if fRightDown then
  begin
    if abs(fScrollPos.x-X)+abs(fScrollPos.y-Y)>=3 then
    begin
      fScrollPos.x:=X;
      fScrollPos.Y:=Y;
      fRightDown:=false;
    end;
  end;
  if (fScrollPos.X<>-1) and (not fRightDown) then
  begin
    Container.LoadGame(true);
    XOff:=round((X-fScrollPos.X)/fZoom);
    YOff:=round((Y-fScrollPos.Y)/fZoom);
    if (XOff<>0) then
    begin
      // Matrix f�r Rotation berechnen
      D3DUtil_SetRotateYMatrix(Temp2,DegToRad(Xoff));
      D3DMath_MatrixMultiply(Temp,fMatrix,Temp2);
      fMatrix:=Temp;

      fScrollPos.X:=X;
      NRedraw:=true;
    end;
    if (YOff<>0) then
    begin
      // Matrix f�r Rotation berechnen
      D3DUtil_SetRotateXMatrix(Temp2,DegToRad(Yoff));
      D3DMath_MatrixMultiply(Temp,fMatrix,Temp2);
      fMatrix:=Temp;

      fScrollPos.Y:=Y;
      NRedraw:=true;
    end;

    // Bis hier her kann NRedraw nur gesetzt sein, wenn sich an der Rotations-
    // matrix etwas ge�ndert hat. Davon wird ausgegangen, da nur dann die Erde
    // neu berechnet werden muss
    if NRedraw then
      RenderEarth;
    RefreshObjectList;
    Container.LoadGame(false);
    exit;
  end;

  Mouse:=Point(X,Y);
  // Pr�fen, ob die Maus �ber dem Warnsymbol ist

  if fButtons.OverButton<>-1 then
    Mouse:=Point(-10,-10);

  // Pr�fen, ob die Maus �ber einem Objekt ist
  Objekt:=-1;
  if PtInRect(Rect(0,0,Width,Height),Mouse) then
  begin
    ObjektPtr:=Addr(fObjekts[0]);
    for Dummy:=0 to fObjektCount-1 do
    begin
      if PointOverObject(Mouse,ObjektPtr) then
        Objekt:=Dummy;
      inc(ObjektPtr);
    end;
  end;

  if Objekt<>fOverObjekt then
  begin
    if (Objekt=-1) and not fSelectModus then
    begin
      Container.SetCursor(CZeiger)
    end
    else
    begin
      if fDrawText then
      begin
        Container.SetCursor(CAuswahl);
      end;
    end;
  end;

  fOverObjekt:=Objekt;
  Container.LoadGame(true);
  NRedraw:=(AktuText(Mouse) or NRedraw) and fMoveRedraw;
  Container.LoadGame(false);

  if NRedraw then
  begin
    Container.IncLock;
    Redraw;
    Container.DecLock;
    Container.DoFlip;
  end;
end;

procedure TDXTakticScreen.MouseOver;
begin
  fScrollPos.X:=-1;
  if fSelectModus then
    Container.SetCursor(CAuswahl);
end;

procedure TDXTakticScreen.Reset;
var
  Pt   : TFloatPoint;
begin
//  RestoreDirectX;

  fSelectModus:=false;
  SetFocus;

  // Entweder auf die Auswahl oder auf die Hauptbasis zentrieren
  if GetSelectedPoint(Pt) then
    CenterView(Pt)
  else
  begin
    CenterView(basis_api_GetMainBasis.BasisPos);
  end;

  RenderEarth;

  RefreshObjectList;
end;

procedure TDXTakticScreen.SetSelectPosModus(Modus: boolean);
begin
  fSelectModus:=Modus;
  if PtInRect(ClientRect,Container.MousePos) then
  begin
    if Modus then
      Container.SetCursor(CAuswahl)
    else
      Container.SetCursor(CZeiger);
  end;
end;

procedure TDXTakticScreen.SetZoom(const Value: double);
begin
  fZoom := min(max(Value,MinZoom),MaxZoom);

  RenderEarth;
  AktuMousePosition;
//  fXOffSet:=30;
  Redraw;
end;

// globale Liste
var
  Liste      : TStringList = nil;

function TDXTakticScreen.SelectObjekts(Point: TPoint;out ShouldCenter: Boolean): TSelectRecord;
var
  Wahl       : Array of TSelectRecord;
  Dummy      : Integer;
  ObjektPtr  : PDrawingObject;
  Select     : Integer;
  SelectPos  : Boolean;
  Pt         : TFloatPoint;
  ID         : Integer;
begin
  Liste.Clear;
  SetLength(Wahl,0);

  ID:=fButtons.GetButtonAtPos(Point);

  if IsWarningIconVisible and (ID=WarningButton) then
  begin
    for Dummy:=0 to ufo_api_GetUFOCount-1 do
    begin
      if ufo_api_GetUFO(Dummy).Visible then
      begin
        SetLength(Wahl,Length(Wahl)+1);
        Wahl[high(Wahl)].result:=true;
        Wahl[high(Wahl)].UFO:=ufo_api_GetUFO(Dummy);
        Liste.AddObject(ufo_api_GetUFO(Dummy).Name,ufo_api_GetUFO(Dummy));
      end;
    end;

    for Dummy:=0 to einsatz_api_GetEinsatzCount-1 do
    begin
      SetLength(Wahl,Length(Wahl)+1);
      Wahl[high(Wahl)].result:=true;
      Wahl[high(Wahl)].Einsatz:=einsatz_api_GetEinsatz(Dummy);
      Liste.AddObject(einsatz_api_GetEinsatz(Dummy).Name,einsatz_api_GetEinsatz(Dummy));
    end;
    SelectPos:=false;
    ShouldCenter:=true;
  end
  else if ID=SchiffButton then
  begin
    for Dummy:=0 to raumschiff_api_GetRaumschiffCount-1 do
    begin
      if (raumschiff_api_GetIndexedRaumschiff(Dummy).XCOMSchiff) and (raumschiff_api_GetIndexedRaumschiff(Dummy).Status<>ssBuild) then
      begin
        SetLength(Wahl,Length(Wahl)+1);
        Wahl[high(Wahl)].result:=true;
        Wahl[high(Wahl)].Raumschiff:=raumschiff_api_GetIndexedRaumschiff(Dummy);
        Liste.AddObject(raumschiff_api_GetIndexedRaumschiff(Dummy).Name,raumschiff_api_GetIndexedRaumschiff(Dummy));
      end;
    end;
    SelectPos:=false;
    ShouldCenter:=true;
  end
  else if ID=TownButton then
  begin
    SetLength(Wahl,town_api_GetTownCount);
    for Dummy:=0 to town_api_GetTownCount-1 do
    begin
      Wahl[Dummy].result:=true;
      Wahl[Dummy].Town:=town_api_GetTown(Dummy);
      Liste.AddObject(town_api_GetTown(Dummy).Name,town_api_GetTown(Dummy));
    end;
    SelectPos:=false;
    ShouldCenter:=true;
  end
  else if ID=BasisButton then
  begin
    SetLength(Wahl,basis_api_GetBasisCount);
    for Dummy:=0 to basis_api_GetBasisCount-1 do
    begin
      Wahl[Dummy].result:=true;
      Wahl[Dummy].Basis:=basis_api_GetBasisFromIndex(Dummy);
      Liste.AddObject(basis_api_GetBasisFromIndex(Dummy).Name,basis_api_GetBasisFromIndex(Dummy));
    end;
    SelectPos:=false;
    ShouldCenter:=true;
  end
  else if fButtons.OverButton=CenterButton then
  begin
    CenterSelected;
    Result.result:=false;
    exit;
  end
  else if fButtons.OverButton=BackBasisButton then
  begin
    fButtons.SetVisible(BackBasisButton,false);
    if fSelectedObjekt.Raumschiff<>nil then
      fSelectedObjekt.Raumschiff.HuntingUFO(0);

    Result.Result:=false;
    exit;
  end
  else if fButtons.OverButton=HuntUFOButton then
  begin
    if Assigned(fOnHunting) then
      fOnHunting(Self);

    Result.Result:=false;
    exit;
  end
  else
  begin
    ObjektPtr:=Addr(fObjekts[0]);
    for Dummy:=0 to fObjektCount-1 do
    begin
      if PointOverObject(Point,ObjektPtr) then
      begin
        SetLength(Wahl,Length(Wahl)+1);
        Wahl[high(Wahl)].result:=true;
        with ObjektPtr^ do
        begin
          if Objekt is TBasis then
          begin
            Liste.AddObject(TBasis(Objekt).Name,Objekt);
            Wahl[high(Wahl)].Basis:=TBasis(Objekt);
          end
          else if Objekt is TRaumschiff then
          begin
            Liste.AddObject(TRaumschiff(Objekt).Name,Objekt);
            Wahl[high(Wahl)].Raumschiff:=TRaumschiff(Objekt);
          end
          else if Objekt is TUFO then
          begin
            Liste.AddObject(TUFO(Objekt).Name,Objekt);
            Wahl[high(Wahl)].UFO:=TUFO(Objekt);
          end
          else if Objekt is TEinsatz then
          begin
            Liste.AddObject(TEinsatz(Objekt).Name,Objekt);
            Wahl[high(Wahl)].Einsatz:=TEinsatz(Objekt);
          end
          else if Objekt is TTown then
          begin
            Liste.AddObject(TTown(Objekt).Name,Objekt);
            Wahl[high(Wahl)].Town:=TTown(Objekt);
          end;
        end;
      end;
      inc(ObjektPtr);
    end;
    SelectPos:=true;
    ShouldCenter:=false;
  end;
  Select:=-1;
  if Liste.Count>1 then
  begin
    fDrawText:=false;
    fOverObjekt:=-1;
    if Assigned(fMultiple) then
      Select:=fMultiple(Liste);
    fDrawText:=true;
    Container.DoMouseMessage;
    if Select=-1 then
      Result.Result:=false
    else
      result:=Wahl[Select];
    AktuText(Classes.Point(-1,-1));
  end
  else if Liste.Count=1 then
    result:=Wahl[0]
  else
  begin
    if SelectPos then
    begin
      FillChar(result,SizeOf(Result),#0);
      result.Result:=MouseToGrad(Point,result.Position);
    end
    else
      result.result:=false;
  end;
end;

function TDXTakticScreen.PointOverObject(Pt1: TPoint;
  Objekt: PDrawingObject): boolean;
begin
  result:=(Abs(Objekt.Position.X-Pt1.X)<=Objekt.FieldSize) and (abs(Objekt.Position.Y-Pt1.Y)<=Objekt.FieldSize);
end;

function TDXTakticScreen.AktuText(MousePos: TPoint): boolean;
var
  Dummy    : Integer;
  ObjektPtr: PDrawingObject;
  maxW     : Integer;
  Old      : Integer;
  OldPoint : TPoint;
begin
  result:=false;
  if CompatibleMode then
    exit;
  Old:=fText.Count;
  OldPoint:=fTextPoint;
  fText.Clear;
  if fOverObjekt<>-1 then
  begin
    fText.Clear;
    ObjektPtr:=Addr(fObjekts[0]);
    for Dummy:=0 to fObjektCount-1 do
    begin
      if PointOverObject(MousePos,ObjektPtr) then
      begin
        with ObjektPtr^ do
        begin
          if Objekt is TBasis then
            fText.AddObject(TBasis(Objekt).Name,BasisFont)
          else if Objekt is TRaumschiff then
            fText.AddObject(TRaumschiff(Objekt).Name,RaumschiffFont)
          else if Objekt is TUFO then
            fText.AddObject(TUFO(Objekt).Name,UFOFont)
          else if Objekt is TEinsatz then
            fText.AddObject(TEinsatz(Objekt).Name,EinsatzFont)
          else if Objekt is TTown then
            fText.AddObject(TTown(Objekt).Name,YellowStdFont)
        end;
      end;
      inc(ObjektPtr);
    end;
    if fText.Count>0 then
    begin
      fTextPoint:=MousePos;
      inc(fTextPoint.Y,10);
      MaxW:=WhiteStdFont.TextWidth(fText[0]);
      for Dummy:=1 to fText.Count-1 do
      begin
        maxW:=max(MaxW,WhiteStdFont.TextWidth(fText[Dummy]));
      end;
      if fTextPoint.X<3 then fTextPoint.X:=3;
      if fTextPoint.Y<3 then fTextPoint.Y:=3;
      if (fTextPoint.X+MaxW)>(Width-3) then fTextPoint.X:=(Width-maxW-3);
      if (fTextPoint.Y+(fText.Count*17))>Height then fTextPoint.Y:=(Height-(fText.Count*17));
    end;
  end;
  if (Old<>fText.Count) or (fTextPoint.X<>OldPoint.X) or (fTextPoint.Y<>OldPoint.Y) then
  begin
    result:=true;
  end;
end;

procedure TDXTakticScreen.DeactiveControl;
begin
  inherited;
  Container.SetCursor(CZeiger);
end;

function TDXTakticScreen.NoObjektSelect: boolean;
begin
  result:=(fSelectedObjekt.UFO=nil) and
          (fSelectedObjekt.Raumschiff=nil) and
          (fSelectedObjekt.Basis=nil) and
          (fSelectedObjekt.Einsatz=nil) and
          (fSelectedObjekt.Town=nil);
end;

function TDXTakticScreen.GetSelectedPoint(out Point: TFloatPoint): boolean;
begin
  result:=true;
  if fSelectedObjekt.UFO<>nil then
    Point:=fSelectedObjekt.UFO.Position
  else if fSelectedObjekt.Raumschiff<>nil then
    Point:=fSelectedObjekt.Raumschiff.Position
  else if fSelectedObjekt.Basis<>nil then
    Point:=fSelectedObjekt.Basis.BasisPos
  else if fSelectedObjekt.Einsatz<>nil then
    Point:=fSelectedObjekt.Einsatz.Position
  else if fSelectedObjekt.Town<>nil then
    Point:=fSelectedObjekt.Town.Position
  else
    result:=false;
end;

procedure TDXTakticScreen.DrawSelection(Surface: TDirectDrawSurface);
var
  ScreenPoint : TPoint;
  TempX       : Integer;
  InPoint     : TFloatPoint;
begin
  if not GetSelectedPoint(InPoint) then
    exit;
  if not GradToScreen(InPoint,ScreenPoint,TempX,false) then
    exit;
  DrawSymbol(Surface,ScreenPoint.X-4,ScreenPoint.Y-10,5);
  DrawSymbol(Surface,ScreenPoint.X-10,ScreenPoint.Y-4,6);
  DrawSymbol(Surface,ScreenPoint.X-4,ScreenPoint.Y+6,7);
  DrawSymbol(Surface,ScreenPoint.X+6,ScreenPoint.Y-4,8);
end;

procedure TDXTakticScreen.ClearSelection;
begin
  FillChar(fSelectedObjekt,sizeOf(fSelectedObjekt),0);
  if Assigned(fSelectObject) then
    fSelectObject(nil);

  fButtons.SetVisible(CenterButton,false);
  fButtons.SetVisible(BackBasisButton,false);
  fButtons.SetVisible(HuntUFOButton,false);
  
  fInfo.Visible:=false;
end;

procedure TDXTakticScreen.RestoreDirectX;
var
  Dummy: Integer;
begin
  GlobalFile.Write('TDXTakticScreen.RestoreDirectX');

  if fUITexture<>nil then
    fUITexture.Free;

  fUITexture:=TDirect3DTexture2.Create(Container,Container.ImageList.Items[1].Picture.Bitmap,false);
  fUITexture.Transparent:=true;
  fUITexture.TransparentColor:=fUITexture.Surface.Pixels[0,0];

  fUISymbols:=Container.ImageList.Items[1].PatternSurfaces[0];

  for Dummy:=0 to fPlugIns.Count-1 do
    TDXTakticPlugIn(fPlugIns[Dummy]).Restore;

  RenderEarth;
  GlobalFile.WriteLine;
end;

procedure TDXTakticScreen.MouseDown(Button: TMouseButton; X, Y: Integer);
begin
  inherited;
  if Button=mbRight then
  begin
    Container.SetCapture(Self);
    fRightDown:=true;
    fScrollPos.X:=X;
    fScrollPos.Y:=Y;
  end;
end;

procedure TDXTakticScreen.DoScroll(Direction: TScrollDirection;
  Pos: TPoint);
begin
  if Direction=sdUp then //Zoom out
    Zoom:=Zoom+0.15
  else if Direction=sdDown then
    Zoom:=Zoom-0.15;
end;

procedure TDXTakticScreen.AktuMousePosition;
var
  Temp: Boolean;
begin
//  Redraw;
  RefreshObjectList;
  if PtInRect(ClientRect,Container.MousePos) then
  begin
    Temp:=fRightDown;
    fRightDown:=false;
    Container.Lock;
    MouseMove(Container.MousePos.X-Left,Container.MousePos.Y-Top);
    Container.Unlock;
    fRightDown:=Temp;
  end;
  AktuInfoTextPos;
//  Redraw;
end;
procedure TDXTakticScreen.RenderEarth;
begin
  if Container.ActivePage<>Parent then
    exit;

  gGlobeRenderer.SetMatrix(fMatrix,fZoom);
  AktuInfoTextPos;
  gGlobeRenderer.RenderEarth;
end;


function TDXTakticScreen.GradToScreen(Grad: TFloatPoint;
  out Screen: TPoint; var Alpha: Integer; ToComponent: Boolean): Boolean;
var
  Vektor : TD3DVector;
  temp   : TD3DVector;
  matrix : TD3DMatrix;
  vp     : TD3DViewport7;
  RadX   : Double;
  RadY   : Double;
begin
  result:=false;

  if Container.D3DDevice7 = nil then
    exit;
    
  RadX:=DegToRad(360-Grad.X);
  RadY:=DegToRad(90-(Grad.Y));

  temp.x:=System.cos(RadX)*System.cos(RadY);
  temp.y:=System.sin(RadY);
  temp.z:=System.sin(RadX)*System.cos(RadY);

  Container.D3DDevice7.GetTransform(D3DTRANSFORMSTATE_WORLD, matrix);
  D3DMath_VectorMatrixMultiply(Vektor,temp,matrix);

  if Vektor.z<0 then
    exit;

  Container.D3DDevice7.GetTransform(D3DTRANSFORMSTATE_VIEW, matrix);
  D3DMath_VectorMatrixMultiply(temp,Vektor,matrix);

  Container.D3DDevice7.GetTransform(D3DTRANSFORMSTATE_PROJECTION, matrix);
  D3DMath_VectorMatrixMultiply(Vektor,temp,matrix);

//  Vektor:=temp;
  Container.D3DDevice7.GetViewport(vp);

  result:=true;
  Screen.X:=round(Vektor.X/2*vp.dwWidth)+(Width div 2);
  Screen.Y:=round(-Vektor.Y/2*vp.dwHeight)+(Height div 2);
{  if not GradToImage(Grad,Image,Alpha) then
    exit;
  if not ImageToScreen(Image,Screen) then
    exit;
  result:=true;}
  if not ToComponent then
  begin
    inc(Screen.X,Left);
    inc(Screen.Y,Top);
  end;
end;

function TDXTakticScreen.IsWarningIconVisible: Boolean;
begin
  result:=(ufo_api_GetVisibleUFOCount>0) or (einsatz_api_GetEinsatzCount>0);
end;

procedure TDXTakticScreen.RefreshObjectList;
var
  Dummy: Integer;
begin
  fObjektCount:=0;
  for Dummy:=0 to town_api_GetTownCount-1 do
  begin
    AddObject(town_api_GetTown(Dummy).Position,4,town_api_GetTown(Dummy));
  end;

  // Basen
  for Dummy:=0 to basis_api_GetBasisCount-1 do
  begin
    case basis_api_GetBasisFromIndex(Dummy).BaseType of
      btBase            : AddObject(basis_api_GetBasisFromIndex(Dummy).BasisPos,0,basis_api_GetBasisFromIndex(Dummy));
      btAlphatronMine   : AddObject(basis_api_GetBasisFromIndex(Dummy).BasisPos,9,basis_api_GetBasisFromIndex(Dummy));
      btOutPost         : AddObject(basis_api_GetBasisFromIndex(Dummy).BasisPos,10,basis_api_GetBasisFromIndex(Dummy));
    end;
  end;

  // Eins�tze
  for Dummy:=0 to einsatz_api_GetEinsatzCount-1 do
    AddObject(einsatz_api_GetEinsatz(Dummy).Position,3,einsatz_api_GetEinsatz(Dummy));

  // Ufos
  for Dummy:=0 to ufo_api_GetUFOCount-1 do
  begin
    if ufo_api_GetUFO(Dummy).Visible then
      AddObject(ufo_api_GetUFO(Dummy).Position,1,ufo_api_GetUFO(Dummy));
  end;

  // Raumschiffe
  for Dummy:=0 to raumschiff_api_GetRaumschiffCount-1 do
  begin
    if raumschiff_api_GetIndexedRaumschiff(Dummy).Status<>ssBuild then
      AddObject(raumschiff_api_GetIndexedRaumschiff(Dummy).Position,2,raumschiff_api_GetIndexedRaumschiff(Dummy));
  end;

  fButtons.SetVisible(WarningButton,IsWarningIconVisible);
end;

procedure TDXTakticScreen.AddObject(Pt: TFloatPoint; Pattern: Integer;
  Objekt: TObject; FieldSize: Integer);
var
  Point       : TPoint;
  Alpha       : Integer;
  ObjektPtr   : PDrawingObject;
begin
  if not GradToScreen(Pt,Point,Alpha,true) then
    exit;

  // Wenn kein Platz im Array ist, dann um 10 Eintr�ge erweitern
  if fObjektCount>High(fObjekts) then
    SetLength(fObjekts,length(fObjekts)+10);

  ObjektPtr:=Addr(fObjekts[fObjektCount]);
  ObjektPtr.Objekt:=Objekt;
  ObjektPtr.Position:=Point;
  ObjektPtr.Alpha:=Alpha;
  ObjektPtr.Pattern:=Pattern;
  ObjektPtr.FieldSize:=FieldSize;
                                    
  inc(fObjektCount);
end;

procedure TDXTakticScreen.DrawObjects(Surface: TDirectDrawSurface);
var
  Dummy     : Integer;
  ObjektPtr : PDrawingObject;
begin
  ObjektPtr:=Addr(fObjekts[0]);
  for Dummy:=0 to fObjektCount-1 do
  begin
    DrawObject(Surface,ObjektPtr);
    inc(ObjektPtr);
  end;
end;

procedure TDXTakticScreen.DrawListItem(Sender: TDXComponent;
  Surface: TDirectDrawSurface; const Mem: TDDSurfaceDesc; Rect: TRect;
  Index: Integer; Selected: boolean);
var
  Font    : TDirectFont;
  ListBox : TDXListBox;
  Obj     : TObject;
begin
  ListBox:=(Sender as TDXListBox);
  if Selected then
  begin
    ListBox.DrawSelection(Surface,Mem,Rect,bcMaroon);
  end;
  Obj:=ListBox.Items.Objects[Index];
  if Obj<>nil then
  begin
    if Obj is TBasis then
      Font:=BasisFont
    else if Obj is TRaumschiff then
    begin
      TRaumschiff(Obj).Liste.DrawIcon(Surface,Rect.Left+3,Rect.Top+2,TRaumschiff(Obj).WaffenZellen);
      WhiteStdFont.Draw(Surface,Rect.Left+39,Rect.Top+20,TRaumschiff(Obj).StatusText);
      Font:=RaumschiffFont;
    end
    else if Obj is TUFO then
    begin
      TUFO(Obj).Draw(Surface,Rect.Left+3,Rect.Top+2);
      Font:=UFOFont;
    end
    else if Obj is TEinsatz then
    begin
      WhiteStdFont.Draw(Surface,Rect.Left+39,Rect.Top+20,Format(LSeeingAliens,[TEinsatz(Obj).AlienCount]));
      Font:=EinsatzFont;
    end
    else if Obj is TTown then
    begin
      Font:=YellowStdFont;
    end;
    Font.Draw(Surface,Rect.Left+39,Rect.Top+4,ListBox.Items[Index]);
  end;
end;

procedure TDXTakticScreen.DrawSymbol(Surface: TDirectDrawSurface; X, Y, Pattern: Integer; Alpha: Integer);
begin
//  if Alpha=255 then
    Surface.Draw(X,Y,Rect(Pattern*8,95,(Pattern+1)*8,103),fUISymbols,true)
//  else
//    DrawTransAlpha(Surface,X,Y,Rect(Pattern*8,95,(Pattern+1)*8,103),fUISymbols,Alpha,fUISymbols.TransparentColor);
end;

procedure TDXTakticScreen.Resize(var NewLeft, NewTop, NewWidth,
  NewHeight: Integer);
begin
  inherited;
  gGlobeRenderer.SetSize(NewWidth,NewHeight);
  gGlobeRenderer.SetCompatibleMode(CompatibleMode);
end;

procedure TDXTakticScreen.Redraw;
begin
  if Visible and (Container.ActivePage=Parent) and (Container.CanMessage) then
    Container.RedrawBlackControl(Self,Container.Surface,false);
end;

procedure TDXTakticScreen.ReDrawRect(DrawRect: TRect;
  Surface: TDirectDrawSurface; var Mem: TDDSurfaceDesc);
var
  Dummy   : Integer;
  XStart  : Integer;
  YStart  : Integer;
  DrawDot : TDrawDotScreen;
  TempRe  : TRect;
  Ptr     : PDiffusevertex;
  TmpSur  : TDirectDrawSurface;
  Schiff  : TRaumschiff;
  Basis   : TBasis;
  tmp     : Boolean;
begin
  Surface.ClippingRect:=ClientRect;
//  Surface.FillRect(ClientRect,0);
  IntersectRect(DrawRect,DrawRect,ClientRect);
  TempRe:=DrawRect;
  OffSetRect(TempRe,-Left,-Top);

  TmpSur:=nil;

  // Wenn sich der Scannerbereich ge�ndert hat, dann Erde neu Rendern
  if earth_api_AktuScanArea then
    RenderEarth;

  if not Surface.SystemMemory then
    Surface.Draw(DrawRect.Left,DrawRect.Top,TempRe,gGlobeRenderer.EarthSurface,false)
  else
    Surface.Draw(DrawRect.Left,DrawRect.Top,TempRe,gGlobeRenderer.SystemSurface,false);

//  Surface.Draw(DrawRect.Left,DrawRect.Top,TempRe,gGlobeRenderer.Textur,false);

  // Einstellung f�r 3D Aktionen setzen
  if (not Surface.SystemMemory) then
  begin
    if (Container.D3DDevice7.SetRenderTarget(Surface.IDDSurface7,0)<>DD_OK) then
    begin
      TmpSur:=Surface;
      Surface:=Container.Surface;
      Surface.Draw(0,0,TmpSur,false);
    end;

    D3DEnabledAlpha(Container);

    Container.D3DDevice7.SetTexture(0,nil);

    // Pfade der Raumschiff zeichnen
    DrawDot.Screen:=Self;
    DrawDot.Surface:=Surface;
    DrawDot.Mem:=Mem;

    for Dummy:=0 to raumschiff_api_GetRaumschiffCount-1 do
    begin
      Schiff:=raumschiff_api_GetIndexedRaumschiff(Dummy);
      if Schiff.ZielPosition.X<>-1 then
      begin
        DrawDot.FirstPoint:=true;
        if Schiff.HuntedUFO<>nil then
          DrawDot.Color:=D3DRGB(1,0,0)
        else
          DrawDot.Color:=D3DRGB(0,0,1);

        if Schiff=fSelectedObjekt.Raumschiff then
          DrawDot.Alpha:=255
        else
          DrawDot.Alpha:=64;
        fVertexCount:=0;
        EarthCalcRoute(Schiff.Position,Schiff.ZielPosition,2,@DrawDot,DrawDots);
        DrawDots(Schiff.ZielPosition,@DrawDot,tmp);

        Ptr:=Addr(fLineVertex[0]);

        Container.D3DDevice7.DrawPrimitive(D3DPT_LINESTRIP,D3DFVF_XYZRHW or D3DFVF_DIFFUSE,Ptr^,fVertexCount,0);
      end;
    end;

    // Alphatron Autotransferrouten Zeichnen
    for Dummy:=0 to basis_api_GetBasisCount-1 do
    begin
      Basis:=basis_api_GetBasisFromIndex(Dummy);
      if Basis.AutoTransferBase<>nil then
      begin
        DrawDot.FirstPoint:=true;
        DrawDot.Color:=D3DRGB(0,1,0);
        DrawDot.Alpha:=64;

        fVertexCount:=0;
        EarthCalcRoute(Basis.BasisPos,Basis.AutoTransferBase.BasisPos,2,@DrawDot,DrawDots);

        Ptr:=Addr(fLineVertex[0]);

        Container.D3DDevice7.DrawPrimitive(D3DPT_LINESTRIP,D3DFVF_XYZRHW or D3DFVF_DIFFUSE,Ptr^,fVertexCount,0);
      end;
    end;

    D3DDisabledAlpha(Container);
  end;

  if (Container.D3DDevice7.SetTexture(0,fUITexture.Surface.IDDSurface7)<>DD_OK) then
    RaiseLastWin32Error;

  DrawObjects(Surface);
  DrawSelection(Surface);

//  Rectangle(Surface,Mem,ClientRect,bcMaroon);
  if fDrawText then
  begin
    if fText.Count>0 then
    begin
      XStart:=fTextPoint.X+Left;
      YStart:=fTextPoint.Y+Top;
      for Dummy:=0 to fText.Count-1 do
      begin
        TDirectFont(fText.Objects[Dummy]).Draw(Surface,XStart,YStart,fText[Dummy]);
        inc(YStart,17);
      end;
    end;
  end;

  if not Surface.SystemMemory then
  begin
    Container.D3DDevice7.SetRenderState(D3DRENDERSTATE_COLORKEYENABLE, Ord(TRUE));

    Container.D3DDevice7.BeginScene;

    DrawMenu(Surface);

    // Hint zeichenen
    if fHintInfo.Show then
    begin
      D3DBlendRectangle(Container,fHintInfo.Rect,RGB_MAKE($40,$40,$40),175);
      FontEngine.DynamicText(Surface,fHintInfo.Rect.Left+2,fHintInfo.Rect.Top+2,fHintInfo.Text,[WhiteStdFont,YellowStdFont]);
    end;

    // Einstellungen zur�cksetzen
    Container.D3DDevice7.EndScene;

    Container.D3DDevice7.SetRenderState(D3DRENDERSTATE_COLORKEYENABLE, Ord(FALSE));
  end;

  if TmpSur<>nil then
  begin
    Surface.Draw(0,0,TmpSur,false);
  end;
  Surface.ClearClipRect;
end;

procedure TDXTakticScreen.DrawMenu(Surface: TDirectDrawSurface);
var
  Dummy  : Integer;
begin
  SetLastError(0);

  for Dummy:=0 to fPlugIns.Count-1 do
    TDXTakticPlugIn(fPlugIns[Dummy]).Draw(Surface);
end;

procedure TDXTakticScreen.KeyPress(var Key: Word; State: TShiftState);
var
  Ch     : Char;
  Dummy  : Integer;
begin
  inherited;
  // Shortcuts durchgehen
  Ch:=LowerChar(Char(Key));
  for Dummy:=0 to high(fShortCuts) do
  begin
    if fShortCuts[Dummy].Key=Ch then
    begin
      if fShortCuts[Dummy].Enabled and Assigned(fShortCuts[Dummy].Onclick) then
      begin
        fShortCuts[Dummy].Onclick(fShortCuts[Dummy].Data);
      end;
      Key:=0;
      exit;
    end;
  end;
end;

procedure TDXTakticScreen.SetHotKey(Key: Char; Enabled: Boolean;
  Onclick: TButtonClick; Data: Integer);
var
  Dummy : Integer;
  Index : Integer;
begin
  Key:=LowerChar(Key);

  // Suchen, ob zu der Taste ein Eintrag existiert
  Index:=-1;
  for Dummy:=0 to high(fShortCuts) do
  begin
    if fShortCuts[Dummy].Key=Key then
    begin
      Index:=Dummy;
      break;
    end;
  end;

  // Index = -1 wenn noch kein Eintrag existiert -> neu hinzuf�gen
  if Index=-1 then
  begin
    SetLength(fShortCuts,length(fShortCuts)+1);
    Index:=High(fShortCuts);
  end;

  // Eigenschaften festlegen
  fShortCuts[Index].Key:=Key;
  fShortCuts[Index].Enabled:=Enabled;
  fShortCuts[Index].OnClick:=Onclick;
  fShortCuts[Index].Data:=Data;
end;

function TDXTakticScreen.PerformFrame(Sender: TObject;
  Frames: Integer): Boolean;
begin
  result:=false;

  if CompatibleMode then
    exit;
    
  if Container.ActivePage<>Parent then
    exit;

  // Buttons blinken
  if fButtons.PerformFrame then
  begin
    Redraw;
    result:=true;
  end;
end;

procedure TDXTakticScreen.AktuInfoTextPos;
var
  Pt      : TFloatPoint;
  Screen  : TPoint;
  Alpha   : Integer;
begin
  if not GetSelectedPoint(Pt) then
    fInfo.Visible:=false
  else
  begin
    fInfo.Visible:=GradToScreen(Pt,Screen,Alpha);
    fInfo.Left:=Screen.X-4;
    fInfo.Top:=Screen.Y+6;
  end;
end;

procedure TDXTakticScreen.ShowHint(Text: String; Pos: TPoint);
begin
  fHintInfo.Show:=Text<>'';
  fHintInfo.Text:=Text;
  fHintInfo.Rect:=Rect(Pos.X,Pos.Y,Pos.X+WhiteStdFont.TextWidth(Text)+6,Pos.Y+18);
end;

procedure TDXTakticScreen.RegisterPlugIn(PlugIn: TDXTakticPlugIn);
begin
  fPlugIns.Add(PlugIn);

  PlugIn.Restore;
end;

function TDXTakticScreen.MouseToGrad(Mouse: TPoint;out Grad: TFloatPoint): Boolean;
var
  Radius        : Double;
  Vektor        : TD3DVector;
  Matrix        : TD3DMatrix;
  ViewMatrix    : TD3DMatrix;
  InverseRotate : TD3DMatrix;
  InverseView   : TD3DMatrix;
  vp            : TD3DViewport7;
  RadX,RadY     : double;
  SideFormat    : double;
begin

  Radius:=GetGlobusRadius;

  Container.D3DDevice7.GetTransform(D3DTRANSFORMSTATE_WORLD, Matrix);
  D3DMath_MatrixInvert(InverseRotate, Matrix);

  Container.D3DDevice7.GetTransform(D3DTRANSFORMSTATE_VIEW, ViewMatrix);

  Container.D3DDevice7.GetViewport(vp);

  SideFormat:=(vp.dwHeight/vp.dwWidth);

  // ViewMatrix._41 gibt die Verschiebung der Erde im Bildschirm an, diese muss hier
  // rausgerechnet werden, damit bei der R�ckrechnung keine Probleme entstehen
  Vektor.x := ((Mouse.X - (Width div 2))*2)/(vp.dwWidth*SideFormat)-(ViewMatrix._41/SideFormat);
  Vektor.y := ((Height div 2) - Mouse.Y)*2/vp.dwHeight;

  // Markiert Punkt �berhaupt �ber der Erde?
  if Sqrt(Sqr(Vektor.x) + Sqr(Vektor.y))>Radius then
  begin
    result:=false;
    exit;
  end;

// nachfolgendes ist einfach die Umstellung nach Vektor.Z der Formel zur Berechnung des Radius einer Kugel
  Vektor.z := Sqrt(Abs(Sqr(Vektor.x) + Sqr(Vektor.y) - sqr(Radius)));

  // _11 muss auf Zoom gesetzt werden, da sonst das Seitenverh�ltnis des Bildschirms mit in die Berechnung
  // einfliessen w�rde
  // _41 muss auf 0 gesetzt werden, da sonst die Verschiebung der Erde mit in die Berechnung
  // einfliessen w�rde
  ViewMatrix._11:=fZoom;
  ViewMatrix._41:=0;
  D3DMath_MatrixInvert(InverseView, ViewMatrix);

  D3DMath_VectorMatrixMultiply(Vektor,Vektor,InverseView);
  D3DMath_VectorMatrixMultiply(Vektor,Vektor,InverseRotate);

  // Umrechnung des 3D-Vektors in Rad-Angaben
  if vektor.X < 0 then
    RadX := arctan(Vektor.Z/Vektor.x) + PI
  else if vektor.X > 0 then
    RadX := arctan(vektor.z/Vektor.X)
  else
    RadX := 0;

  RadY := arcsin(Vektor.y);

  // Umrechnung der Rad Angaben in tats�chliche Grad-Angaben
  Grad.X := 360 - (180/PI * RadX);
  Grad.Y := 90 - (180/PI * RadY);

  if Grad.X > 360 then
    Grad.X := Grad.X - 360;

  result:=true;
end;

function TDXTakticScreen.GetGlobusRadius: double;
begin
  result:=fZoom;
end;

procedure TDXTakticScreen.ShortCutObjekts(Data: Integer);
begin
  case Data of
    0: SelectObjekt(Point(WarningRect.Left,WarningRect.Top));
    1: SelectObjekt(Point(WarningRect.Left+40,WarningRect.Top));
    2: SelectObjekt(Point(WarningRect.Left+80,WarningRect.Top));
    3: SelectObjekt(Point(WarningRect.Left+120,WarningRect.Top));
    4: CenterSelected;
    5:
    begin
      ShowSensorMap:=not ShowSensorMap;
      RenderEarth;
    end;
    6:
    begin
      if Assigned(fOnHunting) then
        fOnHunting(Self);
    end;

  end;
end;

procedure TDXTakticScreen.SelectObjekt(Point: TPoint);
var
  Select : TSelectRecord;
  Center : Boolean;
  Pt     : TFloatPoint;
begin
  // Einfache Objektauswahl
  Select:=SelectObjekts(Point,Center);

  // Wurde ein Objekt ausgew�hlt (MultiSelectDialog OK)
  if not Select.Result then
    exit;

  if Select.Basis<>nil then
  begin
    ClearSelection;
    fInfo.SelectBasis(Select.Basis);
    if Assigned(fSelectObject) then
      fSelectObject(Select.Basis);
  end
  else if Select.UFO<>nil then
  begin
    ClearSelection;
    fInfo.SelectUFO(Select.UFO);
    if Assigned(fSelectObject) then
      fSelectObject(Select.UFO);

    // UFO ausgew�hlt
    fButtons.SetVisible(HuntUFOButton,true);
  end
  else if Select.Raumschiff<>nil then
  begin
    ClearSelection;
    fInfo.SelectRaumschiff(Select.Raumschiff);
    if Assigned(fSelectObject) then
      fSelectObject(Select.Raumschiff);

    // Raumschiff ausgew�hlt
    fButtons.SetVisible(BackBasisButton,(sabClearCommand in Select.Raumschiff.Abilities));
  end
  else if Select.Einsatz<>nil then
  begin
    ClearSelection;
    fInfo.SelectEinsatz(Select.Einsatz);
    if Assigned(fSelectObject) then
      fSelectObject(Select.Einsatz);

    // Einsatz ausgew�hlt
    fButtons.SetVisible(HuntUFOButton,true);
  end
  else if Select.Town<>nil then
  begin
    ClearSelection;
    fInfo.SelectTown(Select.Town);
    if Assigned(fSelectObject) then
      fSelectObject(Select.Town);
  end
  else
    exit;

  fButtons.SetVisible(CenterButton,true);
  fSelectedObjekt:=Select;

  if Center then
  begin
    GetSelectedPoint(Pt);
    CenterView(Pt);
  end;

  AktuInfoTextPos;
  Redraw;
end;

procedure TDXTakticScreen.CenterSelected;
var
  Pt: TFloatPoint;
begin
  if GetSelectedPoint(Pt) then
  begin
    CenterView(Pt);
    RenderEarth;
  end;
end;

{ TDXTakticInfo }

procedure TDXTakticInfo.CalcSize;
begin
  PutLine:=EnumPutLine;
  fRectWidth:=0;
  fRectHeight:=0;
  Draw(nil);

  PutLine:=DefPutLine;
end;

procedure TDXTakticInfo.ClearSelection;
begin
  if fUFO<>nil then
    fUFO.NotifyList.RemoveEvent(fOnDestroyEvent);

  if fRaumschiff<>nil then
    fRaumschiff.NotifyList.RemoveEvent(fOnDestroyEvent);

  if fEinsatz<>nil then
    fEinsatz.NotifyList.RemoveEvent(fOnDestroyEvent);

  if fTown<>nil then
    fTown.OnDestroy:=nil;

  if fBasis<>nil then
    fBasis.NotifyList.RemoveEvent(fOnDestroyEvent);

  fUFO:=nil;
  fRaumschiff:=nil;
  fEinsatz:=nil;
  fTown:=nil;
  fBasis:=nil;

end;

constructor TDXTakticInfo.Create(Screen: TDXTakticScreen);
begin
  inherited;
  ClearSelection;
  PutLine:=DefPutLine;
end;

procedure TDXTakticInfo.DefPutLine(Text: String; var Top: Integer;
  Font: TDirectFont; Margin: Integer);
begin
  if Font=nil then
    Font:=SmallYellowFont;
  Font.Draw(fSurface,Left+Margin+2,Top,Text);
  
  inc(Top,15);
end;

destructor TDXTakticInfo.destroy;
begin
  ClearSelection;
  inherited;
end;

procedure TDXTakticInfo.DestroySelection(Sender: TObject);
begin
  ClearSelection;
  fScreen.ClearSelection;
  fScreen.Redraw;
end;

procedure TDXTakticInfo.Draw(Surface: TDirectDrawSurface);
var
  Land   : String;
  TTop   : Integer;
  Item   : TLagerItem;
  Entf   : Integer;
  Font   : TDirectFont;

  procedure DrawCaption(Caption: String);
  begin
    if fSurface<>nil then
    begin
      D3DBlendRectangle(fScreen.Container,Bounds(Left,Top,fRectWidth,16),RGB_MAKE($80,0,0),75);
      SmallWhiteFont.Draw(Surface,Left+1,Top+3,Caption);
      D3DBlendRectangle(fScreen.Container,Bounds(Left,Top+16,fRectWidth,fRectHeight),RGB_MAKE($80,0,0),50);
    end;
  end;

begin
  if not Visible then
    exit;
  if Surface<>nil then
    CalcSize;
  fSurface:=Surface;  // Wird von der Funktion PutLine zum Ausgeben von Texten genutzt
  TTop:=Top+19;
  if fUFO<>nil then
  begin
    DrawCaption(fUFO.Name);
    PutLine(Format(LEntdecktam,[game_utils_KD4DateToStr(fUFO.Date)]),TTop);
    PutLine(Format(LBesatzung,[fUFO.Besatzung]),TTop);
    Font:=SmallLimeFont;
    case fUFO.Schaden of
      66..100 : Font:=SmallRedFont;
      33..65  : Font:=SmallYellowFont;
    end;
    PutLine(Format(LSchaden,[fUFO.Schaden]),TTop,Font);
    PutLine(Format(LEntfernung,[fUFO.Entfernung/1]),TTop);
    PutLine(Format(LZeitEntdeck,[IntToTime(fUFO.Minuten)]),TTop);
    Land:=fUFO.GetLandName;
    if Land<>'' then
      PutLine(Format(ILandTime,[Land,IntToTime(fUFO.LandMinuten)]),TTop);
    if fUFO.OverLand then
      PutLine(LLand,TTop)
    else
      PutLine(LWasser,TTop);
    {$IFDEF DEBUGMODE}
    PutLine('Visible: '+GetEnumName(TypeInfO(Boolean),Ord(fUFO.Visible)),TTop,SmallWhiteFont);
    {$ENDIF}
  end
  else if fRaumschiff<>nil then
  begin
    DrawCaption(fRaumschiff.Name);
    if not fRaumschiff.XCOMSchiff then
    begin
      PutLine(Format(LEntfernung,[fRaumschiff.EntfernungToZiel/1]),TTop);
      Font:=SmallLimeFont;
      case fRaumschiff.GetSchaden of
        66..100 : Font:=SmallRedFont;
        33..65  : Font:=SmallYellowFont;
      end;
      PutLine(Format(LSchaden,[fRaumschiff.GetSchaden]),TTop,Font);
      PutLine(fRaumschiff.StatusText,TTop);
      PutLine(MTransportZiel+fRaumschiff.GetZielName,TTop,SmallWhiteFont,14);
      PutLine(LLadung,TTop);
      if fRaumschiff.MissionType=rmTransferAlphatron then
      begin
        PutLine(Format(FAlphatron,[fRaumschiff.Alphatron]),TTop,SmallWhiteFont,14);
      end
      else
      begin
        Item:=fRaumschiff.Item;
        PutLine(Item.Name,TTop,SmallWhiteFont,14);
        PutLine(Format(FCount,[Item.Anzahl]),TTop,SmallWhiteFont,14);
      end;
    end
    else
    begin
      if fRaumschiff.Status=ssBuild then
      begin
        PutLine(ZahlString(STBauDay,STBauDays,fRaumschiff.BuildDays),TTop);
      end
      else
      begin
        if fRaumschiff.GetHomeBasis<>nil then
        begin
          Entf:=round(EarthEntfernung(fRaumschiff.Position,fRaumschiff.GetHomeBasis.BasisPos));
          if Entf=0 then
            PutLine(LInBasis,TTop)
          else
            PutLine(Format(LEntfernung,[Entf/1]),TTop);
        end;
        Font:=SmallLimeFont;
        case fRaumschiff.GetSchaden of
          66..100 : Font:=SmallRedFont;
          33..65  : Font:=SmallYellowFont;
        end;
        PutLine(Format(LSchaden,[fRaumschiff.GetSchaden]),TTop,Font);
        if sabRepair in fRaumschiff.Abilities then
          PutLine(ST0502220001+' '+IntToTime(fRaumschiff.GetRepairTime),TTop,SmallWhiteFont);

        // Treibstoff und Reichweite
        if fRaumschiff.Status=ssNoMotor then
          PutLine(STNoMotor,TTop,SmallRedFont)
        else
        begin
          PutLine(LTreibKost+' '+Format('%.1n',[fRaumschiff.Treibstoff])+' %',TTop);
          PutLine(Format(CR0504100001,[fRaumschiff.CruisingRange/1]),TTop);
        end;

        PutLine(LEinsatzorder,TTop);
        if fRaumschiff.HuntedUFO<>nil then
        begin
          PutLine(CMDUFOdestroy,TTop,SmallWhiteFont,14);
          PutLine(fRaumschiff.HuntedUFO.Name,TTop,SmallWhiteFont,14);
          PutLine(Format(LEntfernung,[EarthEntfernung(fRaumschiff.HuntedUFO.Position,fRaumschiff.Position)/1]),TTop,SmallWhiteFont,14)
        end
        else if fRaumschiff.Einsatz<>nil then
        begin
          PutLine(CMDFlyEinsatz,TTop,SmallWhiteFont,14);
          PutLine(fRaumschiff.Einsatz.Name,TTop,SmallWhiteFont,14);
          PutLine(Format(LEntfernung,[EarthEntfernung(fRaumschiff.Einsatz.Position,fRaumschiff.Position)/1]),TTop,SmallWhiteFont,14)
        end
        else if (fRaumschiff.Town<>nil) then
        begin
          if fRaumschiff.IsOverTown(fRaumschiff.Town) then
            PutLine(Format(ST0309220049,[fRaumschiff.Town.Name]),TTop,SmallWhiteFont,14)
          else
            PutLine(Format(ST0309220050,[fRaumschiff.Town.Name]),TTop,SmallWhiteFont,14);
        end
        else if (fRaumschiff.Ziel.X<>-1) then
        begin
          PutLine(CMDFlyToPos,TTop,SmallWhiteFont,14);
        end
        else if (fRaumschiff.EskortedSchiff<>nil) then
        begin
          PutLine(CMDEskorte,TTop,SmallWhiteFont,14);
          PutLine(Format(CMDEskortiert,[fRaumschiff.EskortedSchiff.Name]),TTop,SmallWhiteFont,14);
        end
        else if not (fRaumschiff.Status in [ssAir,ssBackToBasis]) then
        begin
          PutLine(CMDNoCommand,TTop,SmallWhiteFont,14);
        end
        else
        begin
          PutLine(CMDBacktoBasis,TTop,SmallWhiteFont,14);
        end;
      end;
    end;
  end
  else if fEinsatz<>nil then
  begin
    DrawCaption(fEinsatz.Name);
    PutLine(Format(LSeeingAliens,[fEinsatz.AlienCount]),TTop);
  end
  else if fTown<>nil then
  begin
    DrawCaption(fTown.Name);
    PutLine(fTown.CountryName,TTop);
    PutLine(Format(ST0309220051,[fTown.Einwohner/1]),TTop);
    PutLine(Format(ST0309220052,[fTown.Infiltration]),TTop);
    {$IFDEF DEBUGMODE}
    PutLine(Format('Position: %.2n:%.2n',[fTown.Position.X,fTown.Position.Y]),TTop);
    {$ENDIF}
  end
  else if fBasis<>nil then
  begin
    DrawCaption(fBasis.Name);
    PutLine(Format(LBLagerRaum+' '+FLagerRoom,[fBasis.LagerBelegt,fBasis.LagerRaum]),TTop);
    PutLine(Format(LBWohnRaum+' '+FRoom,[fBasis.WohnBelegt/1,fBasis.WohnRaum/1]),TTop);
    PutLine(Format(LBLaborRaum+' '+FRoom,[fBasis.LaborBelegt/1,fBasis.LaborRaum/1]),TTop);
    PutLine(Format(LBWerkRaum+' '+FRoom,[fBasis.WerkBelegt/1,fBasis.WerkRaum/1]),TTop);
    PutLine(Format(IASmallSchiff+' '+FRoom,[fBasis.HangerBelegt/1,fBasis.HangerRaum/1]),TTop);
    PutLine(Format(LSichtungen,[ufo_api_GetVisibleUFOCount]),TTop);
    PutLine(Format(LVerSchiffe,[fBasis.HangerBelegt]),TTop);
  end;
//  Rectangle(Surface,fMem,ClientRect,bcMaroon);
end;

procedure TDXTakticInfo.EnumPutLine(Text: String; var Top: Integer;
  Font: TDirectFont; Margin: Integer);
begin
  if Font=nil then
    Font:=SmallYellowFont;

  fRectWidth:=max(Font.TextWidth(Text)+Margin+4,fRectWidth);

  inc(Top,15);
  inc(fRectHeight,15);
end;

procedure TDXTakticInfo.SelectBasis(Basis: TBasis);
begin
  ClearSelection;
  fBasis:=Basis;
  fOnDestroyEvent:=fBasis.NotifyList.RegisterEvent(EVENT_BASISFREE,DestroySelection);
end;

procedure TDXTakticInfo.SelectEinsatz(Einsatz: TEinsatz);
begin
  ClearSelection;
  fEinsatz:=Einsatz;
  fOnDestroyEvent:=fEinsatz.NotifyList.RegisterEvent(EVENT_EINSATZONDESTROY,DestroySelection);
end;

procedure TDXTakticInfo.SelectRaumschiff(Raumschiff: TRaumschiff);
begin
  ClearSelection;
  fRaumschiff:=Raumschiff;
  fOnDestroyEvent:=Raumschiff.NotifyList.RegisterEvent(EVENT_SCHIFFONDESTROY,DestroySelection);
end;

procedure TDXTakticInfo.SelectTown(Town: TTown);
begin
  ClearSelection;
  fTown:=Town;
  fTown.OnDestroy:=DestroySelection;
end;

procedure TDXTakticInfo.SelectUFO(UFO: TUFO);
begin
  ClearSelection;
  fUFO:=UFO;
  fOnDestroyEvent:=UFO.NotifyList.RegisterEvent(EVENT_ONUFODESTROY,DestroySelection);
end;

{ TDXTakticPlugIn }

constructor TDXTakticPlugIn.Create(Screen: TDXTakticScreen);
begin
  fScreen:=Screen;
  Screen.RegisterPlugIn(Self);
end;

destructor TDXTakticPlugIn.Destroy;
begin
  fScreen.fPlugIns.Remove(Self);
  inherited;
end;

procedure TDXTakticPlugIn.MouseLeave;
begin
end;

procedure TDXTakticPlugIn.MouseMove(X, Y: Integer; var NRedraw: Boolean);
begin
end;

procedure TDXTakticPlugIn.MouseUp(Button: TMouseButton; X, Y: Integer);
begin
end;

procedure TDXTakticPlugIn.Restore;
begin
  fUISymbols:=fScreen.Container.ImageList.Items[1].PatternSurfaces[0];
end;

{ TDXFilterButtons }

procedure TDXFilterButtons.AddButton(ID: Integer; Pos: TPoint;
  Image: TRect; Hint: String;Flashing: Boolean);
begin
  SetLength(fButtons,length(fButtons)+1);
  fButtons[high(fButtons)].ID:=ID;
  fButtons[high(fButtons)].Pos:=Pos;
  fButtons[high(fButtons)].Visible:=true;
  fButtons[high(fButtons)].Flashing:=Flashing;
  fButtons[high(fButtons)].Alpha:=255;
  fButtons[high(fButtons)].Hint:=Hint;
  fButtons[high(fButtons)].Image:=Image;
end;

procedure TDXFilterButtons.Draw(Surface: TDirectDrawSurface);
var
  Dummy: Integer;
begin
  for Dummy:=0 to high(fButtons) do
  begin
    with fButtons[Dummy] do
    begin
      if not Visible then
        continue;
      // Warnsymbol
      if Flashing and (not CompatibleMode) then
      begin
        if fOverID=ID then
        begin
          Surface.Draw(Pos.X,Pos.Y,SmallButtonOver,fUISymbols);
          Surface.Draw(Pos.X+9,Pos.Y+2,WarningIcon,fUISymbols);
        end
        else
        begin
          D3DRenderAlpha(fScreen.Container,Pos.X,Pos.Y,SmallButton,75);
          D3DRenderAlpha(fScreen.Container,Pos.X+9,Pos.Y+2,Image,Alpha);
        end;
      end
      else
      begin
        // Raumschiffe
        if fOverID=ID then
          Surface.Draw(Pos.X,Pos.Y,SmallButtonOver,fUISymbols)
        else
          D3DRenderAlpha(fScreen.Container,Pos.X,Pos.Y,SmallButton,75);
        Surface.Draw(Pos.X+9,Pos.Y+2,Image,fUISymbols);
      end;
    end;
  end;
end;

function TDXFilterButtons.FindButton(ID: Integer): Integer;
var
  Dummy: Integer;
begin
  for Dummy:=0 to high(fButtons) do
  begin
    if fButtons[Dummy].ID=ID then
    begin
      result:=Dummy;
      exit;
    end;
  end;
  result:=-1;
end;

function TDXFilterButtons.GetButtonAtPos(MousePos: TPoint): Integer;
var
  Dummy: Integer;
begin
  result:=-1;
  for Dummy:=0 to high(fButtons) do
  begin
    with fButtons[Dummy] do
    begin
      if PtInRect(Rect(Pos.x,Pos.Y,Pos.X+ButtonWidth,Pos.Y+ButtonHeight),MousePos) and Visible then
      begin
        result:=ID;
        exit;
      end;
    end;
  end;
end;

procedure TDXFilterButtons.MouseLeave;
begin
  fOverID:=-1;
  fScreen.ShowHint('',Point(10,10));
end;

procedure TDXFilterButtons.MouseMove(X, Y: Integer; var NRedraw: Boolean);
var
  New  : Integer;
begin
  New:=GetButtonAtPos(Point(X,Y));

  if New<>fOverID then
  begin
    fOverID:=New;
    NRedraw:=true;
    if New=-1 then
      fScreen.ShowHint('',Point(10,10))
    else
    begin
      with fButtons[FindButton(New)] do
      begin
        fScreen.ShowHint(Hint,Point(Pos.X,Pos.Y+20));
      end;
    end;
  end;
end;

function TDXFilterButtons.PerformFrame: Boolean;
var
  Dummy: Integer;
begin
  result:=false;
  for Dummy:=0 to high(fButtons) do
  begin
    with fButtons[Dummy] do
    begin
      if Flashing then
      begin
        if Incr then
        begin
          inc(Alpha,10);
          if Alpha>=255 then
          begin
            Alpha:=255;
            Incr:=false;
          end;
        end
        else
        begin
          dec(Alpha,10);
          if Alpha<=30 then
          begin
            Alpha:=30;
            Incr:=true;
          end;
        end;
        result:=true;
      end;
    end;
  end;
end;

procedure TDXFilterButtons.SetVisible(ID: Integer; Visible: Boolean);
var
  Button: Integer;
begin
  Button:=FindButton(ID);
  if Button<>-1 then
    fButtons[Button].Visible:=Visible;
end;

initialization

  Liste:=TStringList.Create;

finalization
  if gGlobeRenderer<>nil then
    gGlobeRenderer.Free;
    
  Liste.Free;
end.
