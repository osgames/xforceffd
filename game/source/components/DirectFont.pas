{********************************************************************************
*										*
* Copyright (C) 2004  X-Force Team - jim_raynor@web.de				*
*										*
* This file is part of X-Force: Fight For Destiny (X-Force).			*
*										*
* X-Force is free software; you can redistribute it and/or modify		*
* it under the terms of the GNU General Public License as published by		*
* the Free Software Foundation; either version 2 of the License, or		*
* (at your option) any later version.						*
*										*
* X-Force is distributed in the hope that it will be useful,			*
* but WITHOUT ANY WARRANTY; without even the implied warranty of		*
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the			*
* GNU General Public License for more details.					*
*										*
* You should have received a copy of the GNU General Public License		*
* along with X-Force; if not, write to the Free Software			*
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA	*
*										*
*********************************************************************************
*										*
* Erm�glicht die Ausgabe von Texten direkt �ber DirectDraw ohne den Umweg �ber	*
* die GDI.									*
*										*
********************************************************************************}

{$I ../Settings.inc}

unit DirectFont;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  DXDraws, math, DirectDraw, Defines, TraceFile, Blending, DXContainer;

type
  TCharRecord = record
    Rect  : TRect;
    Width : Integer;
  end;

  TDirectFont = class(TObject)
  private
    fShadow      : boolean;
    fShadowColor : TColor;
    fFont        : TFont;
    fInit        : boolean;
    fArray       : Array[32..255] of TCharRecord;
    fSurface     : TDirectDrawSurface;
    fVideoMem    : boolean;
    fDXDraw      : TDXContainer;
    procedure SetFont(const Value: TFont);
    { Private-Deklarationen }
  protected
    function RetryInit: boolean;
    procedure Restore;
    { Protected-Deklarationen }
  public
    constructor Create;
    destructor Destroy;override;
    procedure Init(Draw: TDXContainer);
    procedure Draw(Surface: TDirectDrawSurface;X,Y: Integer;Text: String);
    procedure DrawAlpha(Surface: TDirectDrawSurface;X,Y: Integer;Text: String;Alpha: Integer);
    procedure DrawInRect(Surface: TDirectDrawSurface;Rect: TRect;X,Y: Integer;Text: String);
    procedure TextRect(Surface: TDirectDrawSurface;Rect: TRect;X,Y: Integer;Text: String);
    procedure DrawChar(Surface: TDirectDrawSurface;X,Y: Integer;Ch: Char);
    function TextWidth(Text: String): Integer;
    function TextHeight(Text: String): Integer;
    function CharWidth(Ch: Char): Integer;
    property Font        : TFont read fFont write SetFont;
    property Shadow      : boolean read fShadow write fShadow;
    property ShadowColor : TColor read fShadowColor write fShadowColor;
    property VideoMem    : boolean read fVideoMem;
    { Public-Deklarationen }
  end;

  TDirectFontEngine = class(TObject)
  private
    fFontList : TList;
    fDXDraw   : TDXContainer;
    procedure SetDXDraw(const Value: TDXContainer);
    procedure Restore;
  public
    constructor Create;
    destructor destroy;override;
    procedure TextOut(Font: TFont;Surface: TDirectDrawSurface;X,Y: Integer; Text: String;ShadowColor: TColor = clNone);overload;
    procedure DynamicText(Surface: TDirectDrawSurface;X,Y: Integer; Text: String; Fonts: Array of TDirectFont);
    function FindDirectFont(Font: TFont;ShadowColor: TColor = clNone;VideoMem: boolean = false): TDirectFont;
    property DXDraw : TDXContainer read fDXDraw write SetDXDraw;
  end;

var
  FontEngine        : TDirectFontEngine;
  WhiteStdFont      : TDirectFont;
  WhiteBStdFont     : TDirectFont;
  YellowStdFont     : TDirectFont;
  YellowBStdFont    : TDirectFont;
  GreenStdFont      : TDirectFont;
  GreenBStdFont     : TDirectFont;
  RedStdFont        : TDirectFont;
  RedBStdFont       : TDirectFont;
  LimeStdFont       : TDirectFont;
  LimeBStdFont      : TDirectFont;
  BlueStdFont       : TDirectFont;
  BlueBStdFont      : TDirectFont;
  MaroonStdFont     : TDirectFont;
  MaroonBStdFont    : TDirectFont;
  DisabledStdFont   : TDirectFont;

implementation

{ TDirectFont }

function TDirectFont.CharWidth(Ch: Char): Integer;
begin
  if (Ch>=#32) then
    result:=fArray[Ord(Ch)].Width-2
  else
    result:=0;
end;

constructor TDirectFont.Create;
var
  lf: TLogFont;
begin
  fFont:=TFont.Create;

  // Hack, damit ClearType f�r alle Schriften deaktiviert wird
  GetObject(fFont.Handle, sizeof(lf), @lf);
  lf.lfQuality := NONANTIALIASED_QUALITY;
  fFont.Handle := CreateFontIndirect(lf);

  fShadowColor:=clBlack;
  fInit:=false;
  fVideoMem:=false;
  fSurface:=nil;
end;

destructor TDirectFont.Destroy;
begin
  fFont.Free;
  inherited;
end;

procedure TDirectFont.Draw(Surface: TDirectDrawSurface; X, Y: Integer; Text: String);
var
  Dummy: Integer;
  Pos  : Integer;
  ch   : Char;
  df   : TDDBltFx;
begin
  if not fInit then
  begin
    if not RetryInit then exit;
  end;
  Pos:=X;
  df.dwSize:=SizeOf(Df);
  df.dwDDFX:=0;
  for Dummy:=1 to length(Text) do
  begin
    ch:=Text[Dummy];
    if Ch>=#32 then
    begin
      Surface.Draw(Pos,Y,fArray[Integer(Ch)].Rect,fSurface);
      inc(Pos,CharWidth(Ch));
    end;
  end;
end;

procedure TDirectFont.DrawAlpha(Surface: TDirectDrawSurface; X, Y: Integer;
  Text: String; Alpha: Integer);
var
  Dummy        : Integer;
  Pos          : Integer;
  ch           : Char;
  df           : TDDBltFx;
  fTempSurface : TDirectDrawSurface;
begin
  if not fInit then
  begin
    if not RetryInit then exit;
  end;
  if Alpha<=0 then exit;
  fTempSurface:=TDirectDrawSurface.Create(Surface.DDraw);
  fTempSurface.SystemMemory:=false;
  fTempSurface.SetSize(TextWidth(Text)+1,TextHeight(Text));
  fTempSurface.Fill($1234);
  Alpha:=min(255,Alpha);
  Pos:=0;
  df.dwSize:=SizeOf(Df);
  df.dwDDFX:=0;
  for Dummy:=1 to length(Text) do
  begin
    ch:=Text[Dummy];
    if Ch>=#32 then
    begin
      fTempSurface.Draw(Pos,0,fArray[Integer(Ch)].Rect,fSurface);
      inc(Pos,CharWidth(Ch));
    end;
  end;
  DrawTransAlpha(Surface,X,Y,fTempSurface.ClientRect,fTempSurface,Alpha,$1234);
  fTempSurface.Free;
end;

procedure TDirectFont.DrawChar(Surface: TDirectDrawSurface; X, Y: Integer;
  Ch: Char);
begin
  if not fInit then
  begin
    if not RetryInit then exit;
  end;
  Surface.Draw(X,Y,fArray[Integer(Ch)].Rect,fSurface);
end;

procedure TDirectFont.DrawInRect(Surface: TDirectDrawSurface; Rect: TRect;
  X, Y: Integer; Text: String);
var
  Dummy : Integer;
  Pos   : Integer;
  ch    : Char;
  DRect : TRect;
  YBot  : Integer;
  YTop  : Integer;
begin
  if not fInit then
  begin
    if not RetryInit then exit;
  end;
  if Y>Rect.Bottom then exit;
  if Y<Rect.Top-fArray[32].Rect.Bottom then exit;
  if Y>Rect.Bottom-fArray[32].Rect.Bottom then
    YBot:=Rect.Bottom-Y
  else
    YBot:=fArray[32].Rect.Bottom;
  if Y<Rect.Top then
  begin
    YTop:=Rect.Top-Y;
    Y:=Rect.Top;
  end
  else
    YTop:=0;
  Pos:=X;
  for Dummy:=1 to length(Text) do
  begin
    ch:=Text[Dummy];
    DRect:=fArray[Integer(Ch)].Rect;
    DRect.Top:=YTop;
    DRect.Bottom:=YBot;
    if Ch>=#32 then
    begin
      Surface.ISurface7.BltFast(Pos,Y,fSurface.IDDSurface7,@DRect,DDBLTFAST_SRCCOLORKEY);
      inc(Pos,CharWidth(Ch));
    end;
  end;
end;

procedure TDirectFont.Init(Draw: TDXContainer);
var
  Canvas : TDirectDrawSurfaceCanvas;
  Dummy  : Integer;
  X      : Integer;
  Height : Integer;
  Size   : TSize;
  Space  : Integer;
begin
  if Draw=nil then
  begin
    fInit:=false;
    if fSurface<>nil then fSurface.Free;
    fSurface:=nil;
    exit;
  end;

  fDXDraw:=Draw;
  Canvas:=Draw.Surface.Canvas;
  Canvas.Font:=fFont;
  X:=0;
  Height:=0;
  Space:=0;
  if (Win32Platform=VER_PLATFORM_WIN32_WINDOWS) and (fsBold in fFont.Style) then
    Space:=1;
  for Dummy:=32 to 255 do
  begin
    Size:=Canvas.TextExtent(Char(Dummy));
    dec(size.cx,Space);
    if Shadow then
    begin
      inc(Size.cy,2);
      inc(Size.cx,2);
    end;
    Height:=max(Height,Size.cy);
    fArray[Dummy].Rect:=Rect(X,0,X+Size.cx,Size.cy);
    fArray[Dummy].Width:=Size.cx;
    inc(X,Size.cx+1);
  end;
  Canvas.Release;

  if fSurface<>nil then
    fSurface.Free;
//  if fVideoMem then
//    Draw.RegisterNotifyEvent(DXDrawNotify);

  fSurface:=TDirectDrawSurface.Create(Draw.DDraw);
  fSurface.SystemMemory:=not fVideoMem;
  fSurface.Do3DStuff:=fVideoMem;
  fSurface.SetSize(X,Height);
  fSurface.Fill($1234);
  fSurface.TransparentColor:=$1234;
  Canvas:=fSurface.Canvas;
  Canvas.Font:=fFont;
  Canvas.Brush.Style:=bsClear;
  X:=0;
  for Dummy:=32 to 255 do
  begin
    if Shadow then
    begin
      Canvas.Font.Color:=fShadowColor;
      Canvas.TextOut(X,0,Char(Dummy));
      Canvas.TextOut(X+1,0,Char(Dummy));
      Canvas.TextOut(X+2,0,Char(Dummy));

      Canvas.TextOut(X,1,Char(Dummy));
      Canvas.TextOut(X+2,1,Char(Dummy));

      Canvas.TextOut(X,2,Char(Dummy));
      Canvas.TextOut(X+1,2,Char(Dummy));
      Canvas.TextOut(X+2,2,Char(Dummy));
    end;
    Canvas.Font.Color:=fFont.Color;
    Canvas.TextOut(X+1,1,Char(Dummy));
    inc(X,fArray[Dummy].Width+1);
  end;
  fInit:=true;
  fSurface.Canvas.Release;
  Canvas.Release;
end;

procedure TDirectFont.Restore;
begin
  fInit:=false;
  if fSurface<>nil then
  begin
    fSurface.Free;
    fSurface:=nil;
  end;
end;

function TDirectFont.RetryInit: boolean;
begin
  result:=false;
  if FontEngine.DXDraw<>nil then
  begin
    Init(FontEngine.DXDraw);
    result:=true;
  end;
end;

procedure TDirectFont.SetFont(const Value: TFont);
var
  lf: TLogFont;
begin
  fFont.Assign(Value);

  // Hack, damit ClearType f�r alle Schriften deaktiviert wird
  GetObject(fFont.Handle, sizeof(lf), @lf);
  lf.lfQuality := NONANTIALIASED_QUALITY;
  fFont.Handle := CreateFontIndirect(lf);
end;

function TDirectFont.TextHeight(Text: String): Integer;
begin
  if not fInit then
  begin
    result:=1;
    if not RetryInit then exit;
  end;
  result:=fArray[32].Rect.Bottom-1;
end;

procedure TDirectFont.TextRect(Surface: TDirectDrawSurface; Rect: TRect; X,
  Y: Integer; Text: String);
var
  TempText   : String;
  Width      : Integer;
  PointWidth : Integer;
  TextWid    : Integer;
  Ch         : Char;
begin
  if not fInit then exit;
  TempText:=Text;
  PointWidth:=TextWidth(' ...');
  TextWid:=TextWidth(TempText);
  Width:=Rect.Right-Rect.Left;
  if (TextWid>Width) then
  begin
    while (TextWid>Width-PointWidth) do
    begin
      ch:=TempText[length(TempText)];
      Delete(TempText,length(TempText),1);
      TextWid:=TextWid-fArray[Integer(ch)].Width;
    end;
    TempText:=TempText+' ...';
    X:=Rect.Left+3
  end;
  Draw(Surface,X,Y,TempText);
end;

function TDirectFont.TextWidth(Text: String): Integer;
var
  Dummy: Integer;
begin
  result:=0;
  if not fInit then
    if not RetryInit then exit;
  for Dummy:=length(Text) downto 1 do
  begin
    inc(result,CharWidth(Text[Dummy]));
  end;
end;

{ TDirectFontEngine }

constructor TDirectFontEngine.Create;
begin
  if FontEngine<>nil then raise Exception.Create('Nur eine Instanz der Font-Engine erlaubt');
  fFontList:=TList.Create;
end;

destructor TDirectFontEngine.destroy;
begin
  while fFontList.Count>0 do
  begin
    TDirectFont(fFontList[0]).Free;
    fFontList.Delete(0);
  end;
  fFontList.Free;
  FontEngine:=nil;
  inherited;
end;

procedure TDirectFontEngine.DynamicText(Surface: TDirectDrawSurface; X,
  Y: Integer; Text: String; Fonts: array of TDirectFont);
var
  Dummy        : Integer;
  Pos          : Integer;
  ch           : Char;
  Font         : TDirectFont;
  maxFontIndex : Integer;
begin
  Pos:=X;
  maxFontIndex:=high(Fonts);
  if maxFontIndex=-1 then
    exit;
  Font:=Fonts[0];
  for Dummy:=1 to length(Text) do
  begin
    ch:=Text[Dummy];
    if ch<#32 then
    begin
      dec(ch);
      if (Ord(ch)<=maxFontIndex) and (Fonts[Ord(ch)]<>nil) then
        Font:=Fonts[Ord(ch)];
    end
    else
    begin
      Font.DrawChar(Surface,Pos,Y,ch);
      inc(Pos,Font.CharWidth(ch));
    end;
  end;
end;

function TDirectFontEngine.FindDirectFont(Font: TFont;ShadowColor: TColor;VideoMem: boolean): TDirectFont;
var
  Dummy : Integer;
  DFont : TDirectFont;
begin
  for Dummy:=0 to fFontList.Count-1 do
  begin
    DFont:=TDirectFont(fFontList[Dummy]);
    if (Font.Color=DFont.Font.Color) and (Font.Name=DFont.Font.Name) and (Font.Size=DFont.Font.Size) and (Font.Style=DFont.Font.Style) then
    begin
      if ((DFont.Shadow) and (DFont.ShadowColor=ShadowColor)) or ((not DFont.Shadow) and (ShadowColor=clNone)) then
      begin
        result:=DFont;
        exit;
      end;
    end;
  end;
  DFont:=TDirectFont.Create;
  DFont.Font:=Font;
  DFont.Shadow:=ShadowColor<>clNone;
  DFont.ShadowColor:=ShadowColor;
  DFont.fVideoMem:=VideoMem;
  DFont.Init(fDXDraw);
  fFontList.Add(DFont);
  result:=DFont;
end;

procedure TDirectFontEngine.Restore;
var
  Dummy: Integer;
begin
  for Dummy:=0 to fFontList.Count-1 do
  begin
    if TDirectFont(fFontList[Dummy]).VideoMem then
      TDirectFont(fFontList[Dummy]).Restore;
  end;
end;

procedure TDirectFontEngine.SetDXDraw(const Value: TDXContainer);
var
  Font: TFont;
begin
  fDXDraw := Value;
  Font:=TFont.Create;
  Font.Size:=coFontSize;
  Font.Name:=coFontName;
  Font.Color:=clYellow;
  YellowStdFont:=FontEngine.FindDirectFont(Font,clBlack);
  Font.Color:=clRed;
  RedStdFont:=FontEngine.FindDirectFont(Font,clBlack);
  Font.Color:=clGreen;
  GreenStdFont:=FontEngine.FindDirectFont(Font,clBlack);
  Font.Color:=clLime;
  LimeStdFont:=FontEngine.FindDirectFont(Font,clBlack);
  Font.Color:=clMaroon;
  MaroonStdFont:=FontEngine.FindDirectFont(Font,clBlack);
  Font.Color:=clBlue;
  BlueStdFont:=FontEngine.FindDirectFont(Font,clBlack);
  Font.Color:=$00606060;
  DisabledStdFont:=FontEngine.FindDirectFont(Font,clBlack);
  Font.Color:=clWhite;
  WhiteStdFont:=FontEngine.FindDirectFont(Font,clBlack);
  Font.Style:=[fsBold];
  WhiteBStdFont:=FontEngine.FindDirectFont(Font,clBlack);
  Font.Color:=clYellow;
  YellowBStdFont:=FontEngine.FindDirectFont(Font,clBlack);
  Font.Color:=clRed;
  RedBStdFont:=FontEngine.FindDirectFont(Font,clBlack);
  Font.Color:=clMaroon;
  MaroonBStdFont:=FontEngine.FindDirectFont(Font,clBlack);
  Font.Color:=clGreen;
  GreenBStdFont:=FontEngine.FindDirectFont(Font,clBlack);
  Font.Color:=clLime;
  LimeBStdFont:=FontEngine.FindDirectFont(Font,clBlack);
  Font.Color:=clBlue;
  BlueBStdFont:=FontEngine.FindDirectFont(Font,clBlack);
  Font.Free;

  DXDraw.AddRestoreFunction(Restore);
end;

procedure TDirectFontEngine.TextOut(Font: TFont;Surface: TDirectDrawSurface; X, Y: Integer; Text: String;ShadowColor: TColor);
var
  DFont: TDirectFont;
begin
  DFont:=FindDirectFont(Font,ShadowColor);
  if DFont<>nil then
  begin
    DFont.Draw(Surface,X,Y,Text);
//    Surface.Draw(X,Y,DFont.fSurface);
  end
  else
    raise Exception.Create('Fehler beim Erstellen der Schrift');
end;

initialization
  FontEngine:=TDirectFontEngine.Create;

finalization
  FontEngine.Free;

end.
