{********************************************************************************
*										*
* Copyright (C) 2004  X-Force Team - jim_raynor@web.de				*
*										*
* This file is part of X-Force: Fight For Destiny (X-Force).			*
*										*
* X-Force is free software; you can redistribute it and/or modify		*
* it under the terms of the GNU General Public License as published by		*
* the Free Software Foundation; either version 2 of the License, or		*
* (at your option) any later version.						*
*										*
* X-Force is distributed in the hope that it will be useful,			*
* but WITHOUT ANY WARRANTY; without even the implied warranty of		*
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the			*
* GNU General Public License for more details.					*
*										*
* You should have received a copy of the GNU General Public License		*
* along with X-Force; if not, write to the Free Software			*
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA	*
*										*
*********************************************************************************
*										*
* Stellt eine Listbox zur Verf�gung, bei der jeder Eintrag per Checkbox		*
* aktiviert/deaktiviert werden kann.						*
*										*
********************************************************************************}

{$I ../Settings.inc}

unit DXCheckList;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  DXListBox, TraceFile,DXContainer, DXDraws, Blending, KD4Utils, math,
  Defines, DirectDraw, DirectFont;

type
  TCheckListEntry = record
    Hint    : String;
    Checked : boolean;
    Enabled : boolean;
  end;

type
  TDXCheckList = class(TDXListBox)
  private
    fEntrys      : Array of TCheckListEntry;
    fItemIndex   : Integer;
    fButtonSize  : Integer;
    GrayFont     : TDirectFont;
    procedure SetItemIndex(const Value: Integer);
    procedure SetButtonSize(const Value: Integer);
    function GetSelected(Index: Integer): boolean;
    procedure SetSelected(Index: Integer; const Value: boolean);
    function GetEnabled(Index: Integer): boolean;
    procedure SetEnabled(Index: Integer; const Value: boolean);
    { Private-Deklarationen }
  protected
    procedure DrawItem(Surface: TDirectDrawSurface;const Mem: TDDSurfaceDesc; Rect: TRect; Index: Integer);override;
    procedure DrawBox(Surface: TDirectDrawSurface;const Mem: TDDSurfaceDesc; Rect: TRect; Index: Integer; Enabled: boolean);
    { Protected-Deklarationen }
  public
    constructor Create(Page: TDXPage);override;
    destructor destroy;override;
    procedure MouseDown(Button: TMouseButton;X,Y: Integer);override;
    procedure MouseLeave;override;
    procedure MouseMove(X,Y: Integer);override;
    procedure AddItem(Text: String; Hint: String);
    procedure MouseUp(Button: TMouseButton;X,Y: Integer);override;

    procedure DrawSelection(Surface: TDirectDrawSurface;const Mem: TDDSurfaceDesc;Rect: TRect;Color: TBlendColor);override;

    property Checked[Index: Integer]     : boolean     read GetSelected write SetSelected;
    property ItemEnabled[Index: Integer] : boolean     read GetEnabled  write SetEnabled;
    property ItemIndex                   : Integer     read fItemIndex  write SetItemIndex;
    property ButtonSize                  : Integer     read fButtonSize write SetButtonSize;
  end;

implementation

{ TDXCheckList }

procedure TDXCheckList.AddItem(Text, Hint: String);
begin
  Items.Add(Text);
  SetLength(fEntrys,length(fEntrys)+1);
  fEntrys[high(fEntrys)].Hint:=Hint;
  fEntrys[high(fEntrys)].Checked:=true;
  fEntrys[high(fEntrys)].Enabled:=true;
end;

constructor TDXCheckList.Create(Page: TDXPage);
var
  Font: TFont;
begin
  inherited;
  ItemHeight:=22;
  fButtonSize:=22;
  fItemIndex:=-1;
  fLeftMargin:=6;
  fTopMargin:=4;
  Font:=TFont.Create;
  FOnt.Name:=coFontName;
  Font.Size:=coFontSize;
  Font.Color:=clGray;
  GrayFont:=FontEngine.FindDirectFont(Font,clBlack);
  Font.Free;
end;

destructor TDXCheckList.destroy;
begin
  SetLength(fEntrys,0);
  inherited;
end;

procedure TDXCheckList.DrawBox(Surface: TDirectDrawSurface;const Mem: TDDSurfaceDesc;
  Rect: TRect; Index: Integer;Enabled: boolean);
begin
  if AlphaElements then
  begin
    if Enabled then
      BlendRoundRect(ClientRect,100,fScrollBar.FirstColor,Surface,Mem,11,fCorner,Bounds(Rect.Left+1,Rect.Top+1,fButtonSize,fButtonSize-1))
    else
      BlendRoundRect(ClientRect,200,bcGray,Surface,Mem,11,fCorner,Bounds(Rect.Left+1,Rect.Top+1,fButtonSize,fButtonSize-1));
  end;
  if (Index=ItemIndex) and (Enabled) then Rectangle(Surface,Mem,Rect.Left+5,Rect.Top+4,Rect.Left+fButtonSize-2,Rect.Top+fButtonSize-4,fColor);
  if fEntrys[Index].Checked then
    Container.ImageList.Items[0].Draw(Surface,Rect.Left+6,Rect.Top+5,4);
end;

procedure TDXCheckList.DrawItem(Surface: TDirectDrawSurface;const Mem: TDDSurfaceDesc; Rect: TRect;
  Index: Integer);
var
  Font    : TDirectFont;
  Enabled : boolean;
begin
  Enabled:=fEntrys[Index].Enabled;

  DrawBox(Surface,Mem,Rect,Index,Enabled);

  inc(Rect.Left,fButtonSize);

  if (not fOwnerDraw) or (not Assigned(fDrawItem)) then
  begin
    if (Index=fItemIndex) and Enabled then
    begin
      Font:=WhiteStdFont;
      DrawSelection(Surface,Mem,Rect,fScrollBar.FirstColor);
    end
    else if not Enabled then
      Font:=GrayFont
    else
      Font:=YellowStdFont;

    Font.Draw(Surface,Rect.Left+fLeftMargin,Rect.Top+fTopMargin,fItems[Index]);
  end
  else
    fDrawItem(Self,Surface,Mem,Rect,Index,Index=fItemIndex);
end;

procedure TDXCheckList.DrawSelection(Surface: TDirectDrawSurface;
  const Mem: TDDSurfaceDesc; Rect: TRect; Color: TBlendColor);
begin
  inc(Rect.Left,2);
  inc(Rect.Top);
  if AlphaElements then
    BlendRoundRect(ClientRect,100,Color,Surface,Mem,11,fCorner,Rect)
  else
  begin
    DrawSelectionRect(Surface.Canvas,Rect);
    Surface.Canvas.Release;
  end;
end;

function TDXCheckList.GetEnabled(Index: Integer): boolean;
begin
  result:=fEntrys[Index].Enabled;
end;

function TDXCheckList.GetSelected(Index: Integer): boolean;
begin
  result:=fEntrys[Index].Checked;
end;

procedure TDXCheckList.MouseDown(Button: TMouseButton; X, Y: Integer);
begin
  if Assigned(OnMouseDown) then OnMouseDown(Self,Button,[],x,y);
  if TabStop then Container.FocusControl:=Self;
end;

procedure TDXCheckList.MouseLeave;
begin
  ItemIndex:=-1;
  Redraw;
  inherited;
end;

procedure TDXCheckList.MouseMove(X, Y: Integer);
var
  NewIndex: Integer;
begin
  Y:=min(max(0,Y),Height-1);
  NewIndex:=ItemAtLine(Y);
  if NewIndex=ItemIndex then exit;
  if NewIndex>fItems.Count-1 then exit;
  ItemIndex:=NewIndex;
end;

procedure TDXCheckList.MouseUp(Button: TMouseButton; X, Y: Integer);
var
  DTop: Integer;
begin
  inherited;
  if fItemIndex=-1 then
    exit;
    
  DTop:=TopOfItem(ItemIndex)-Top;
  if PtInRect(Rect(0,DTop,Width,DTop+fItemHeight),Point(X,Y)) then
  begin
    if fEntrys[fItemIndex].Enabled then
    begin
      fEntrys[fItemIndex].Checked:=not fEntrys[fItemIndex].Checked;
      Container.PlaySound(SClick);
      Container.IncLock;
      if Assigned(OnChange) then OnChange(Self);
      Container.DecLock;
      Redraw;
    end;
  end;
end;

procedure TDXCheckList.SetButtonSize(const Value: Integer);
begin
  fButtonSize := Value;
end;

procedure TDXCheckList.SetEnabled(Index: Integer; const Value: boolean);
begin
  fEntrys[Index].Enabled:=Value;
  Redraw;
end;

procedure TDXCheckList.SetItemIndex(const Value: Integer);
begin
  if fItemChange<>0 then exit;
  Container.Lock;
  fItemIndex:=Value;
  if fItemIndex>Items.Count-1 then fItemIndex:=Items.Count-1;
{  if fItemIndex<>-1 then
  begin
    if fItemIndex<fListTop then fListTop:=fItemIndex;
    if fItemIndex>ItemsInWindow+fListTop-1 then fListTop:=fItemIndex-ItemsInWindow+1;
    if fListTop<0 then fListTop:=0;
  end;
  fScrollBar.Value:=fListTop;}
  Container.LoadGame(false);
  if (fItemIndex<>-1) and fEntrys[fItemIndex].Enabled then
    Hint:=fEntrys[fItemIndex].Hint
  else
    Hint:='';
  if Assigned(fChange) then fChange(Self);
  Container.DecLock;
  Container.RedrawArea(ClientRect,Container.Surface);
end;

procedure TDXCheckList.SetSelected(Index: Integer; const Value: boolean);
begin
  fEntrys[Index].Checked:=Value;
  Redraw;
end;

end.

