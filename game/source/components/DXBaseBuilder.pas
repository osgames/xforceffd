unit DXBaseBuilder;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  DXContainer, KD4Utils, Defines, DXDraws, Blending, XForce_types, NGTypes,
  BasisListe, DirectDraw, IDHashMap, DXScrollBar, DirectFont, DXBitmapButton,
  StringConst;

type
  TBuildRoomEvent       = procedure(BuildFloor,X,Y: Integer; Room: PEinrichtung) of object;
  TRoomEvent            = procedure(BuildFloor,X,Y: Integer) of object;

  PBaseBuilderImageInfo = ^TBaseBuilderImageInfo;
  TBaseBuilderImageInfo = record
    ImageName           : String;
    Hash                : Cardinal;
    ImagePosX           : Integer;
    ImagePosY           : Integer;
    Width               : Integer;
    Height              : Integer;
  end;

  TBaseBuildButtonOption = (bbboInvisible,bbboActive,bbboImageOnly);

  TButtonAvaibleProc    = function(Room: PEinrichtung): TBaseBuildButtonOption of object;

  TRoomButton           =  record
    AvaibleProc         : TButtonAvaibleProc;
    ClickProc           : TNotifyEvent;
    Position            : TPoint;
    ImgOffSet           : TPoint;
    Image               : TRect;
    Hint                : String;
  end;

  TDXBaseBuilder = class(TDXComponent)
  private
    fBase       : TBasis;
    fImages     : TDirectDrawSurface;
    fImageInfos : Array of TBaseBuilderImageInfo;
    fHashMap    : TIDHashMap;

    fHScroll    : TDXScrollBar;
    fVScroll    : TDXScrollBar;
    fBuildFloor : Integer;

    fBaseWidth  : Integer;
    fBaseHeight : Integer;

    fRoomRows   : Integer;
    fRoomCols   : Integer;

    fBeginX     : Integer;
    fBeginY     : Integer;

    fOverPoint    : TPoint;
    fBuildingRoom : PEinrichtung;
    fOnBuildRoom  : TBuildRoomEvent;
    fOnClickRoom  : TRoomEvent;
    fOnOverRoom   : TRoomEvent;
    fFloorButtons : Array of TDXBitmapButton;
    fRepairButton : TDXBitmapButton;
    fBackGround   : String;

    fRoomButtons  : Array of TRoomButton;
    fOnChangeFloor: TNotifyEvent;

    procedure SetBase(const Value: TBasis);

    procedure LoadImages(Sender: TObject);
    procedure ForschEnd(Sender: TObject);

    function GetImageInfo(Image: String): PBaseBuilderImageInfo;

    procedure OnScroll(Sender: TObject);
    procedure FloorButtonClick(Sender: TObject);

    procedure SetBaseSize(Width, Height: Integer);
    procedure SetBuildingRoom(const Value: PEinrichtung);
    procedure SetBuildingFloor(const Value: Integer);

    function GetRoomRect: TRect;

    procedure DrawRepairButton(Sender: TDXComponent;Surface: TDirectDrawSurface;var Mem: TDDSurfaceDesc);

    function GetButtonRect(const Button: TRoomButton; RoomRect: TRect): TRect;

    procedure RepairClick(Sender: TObject);
  public
    constructor Create(Page: TDXPage);override;
    destructor Destroy;override;

    procedure Resize(var NewLeft, NewTop, NewWidth, NewHeight: Integer);override;
    procedure Draw(Surface: TDirectDrawSurface;var Mem: TDDSurfaceDesc);override;
    procedure ReDrawRect(DrawRect: TRect;Surface: TDirectDrawSurface;var Mem: TDDSurfaceDesc);override;
    procedure MouseMove(X,Y: Integer);override;

    procedure MouseUp(Button: TMouseButton;X,Y: Integer);override;
    procedure MouseLeave;override;

    procedure AddRoomButton(CheckProc: TButtonAvaibleProc; ClickEvent: TNotifyEvent; Pos, ImgOff: TPoint; ImageRect: TRect; HintStr: String);

    property Base         : TBasis read fBase write SetBase;
    property BuildingRoom : PEinrichtung read fBuildingRoom write SetBuildingRoom;

    property BuildingFloor: Integer read fBuildFloor write SetBuildingFloor;


    property OnBuildRoom  : TBuildRoomEvent read fOnBuildRoom write fOnBuildRoom;
    property OnClickRoom  : TRoomEvent read fOnClickRoom write fOnClickRoom;
    property OnOverRoom   : TRoomEvent read fOnOverRoom write fOnOverRoom;

    property OnChangeFloor: TNotifyEvent read fOnChangeFloor write fOnChangeFloor;
  end;

const
  FloorImages : Array[0..15] of String = ('Floor--ud','Floor--u-','Floor-rud','Floor-ru-','Floor-r-d',
                                          'Floor-r--','Floorl-ud','Floorl-u-','Floorlrud','Floorlru-',
                                          'Floorlr-d','Floorlr--','Floorl--d','Floorl---','Floor---d',
                                          'Floor----');

  BackGroundWidth    = 4;
  BackGroundHeight   = 4;

  ObjektSize        = 64;

var
  BuildDayFont      : TDirectFont = nil;

implementation

uses
  savegame_api, basis_api, forsch_api, math, archiv_utils, ArchivFile, game_api;

{ TDXBaseBuilder }

procedure TDXBaseBuilder.AddRoomButton(CheckProc: TButtonAvaibleProc; ClickEvent: TNotifyEvent; Pos, ImgOff: TPoint; ImageRect: TRect; HintStr: String);
begin
  SetLength(fRoomButtons,length(fRoomButtons)+1);

  with fRoomButtons[high(fRoomButtons)] do
  begin
    AvaibleProc:=CheckProc;
    ClickProc:=ClickEvent;
    Position:=Pos;
    ImgOffSet:=ImgOff;
    Image:=ImageRect;
    Hint:=HintStr;
  end;
end;

constructor TDXBaseBuilder.Create(Page: TDXPage);
var
  Dummy: Integer;
begin
  inherited;
  // Beim Starten eines Spiels m�ssen die Grafiken aktualisiert werden
  savegame_api_RegisterStartGameHandler(LoadImages);

  // Wenn eine neue Einrichtung erforscht wurde, m�ssen die Bilder aktualisiert werden
  forsch_api_RegisterProjektEndHandler(ForschEnd);

  // Horizontale Scrollbar
  fHScroll:=TDXScrollBar.Create(Page);
//  fHScroll.Visible:=false;
  fHScroll.Kind:=sbHorizontal;
  fHScroll.RoundCorners:=rcNone;

  fHScroll.FirstColor:=bcMaroon;
  fHScroll.SecondColor:=clMaroon;
  fHScroll.OnChange:=OnScroll;

  // Vertikale Scrollbar
  fVScroll:=TDXScrollBar.Create(Page);
//  fVScroll.Visible:=false;
  fVScroll.Kind:=sbVertical;
  fVScroll.RoundCorners:=rcNone;

  fVScroll.FirstColor:=bcMaroon;
  fVScroll.SecondColor:=clMaroon;
  fVScroll.OnChange:=OnScroll;

  fBuildFloor:=0;

  fHashMap:=TIDHashMap.Create;
  fImages:=TDirectDrawSurface.Create(Page.Container.DDraw);
  fImages.SystemMemory:=true;
  fImages.TransparentColor:=bcFuchsia;

  SetBaseSize(20,20);

  if BuildDayFont=nil then
  begin
    Font.Size:=14;
    Font.Color:=clNavy;
    Font.Style:=[fsBold];
    BuildDayFont:=FontEngine.FindDirectFont(Font,clBlack);
  end;

  SetLength(fFloorButtons,MaxFloors+1);
  for Dummy:=0 to MaxFloors do
  begin
    fFloorButtons[Dummy]:=TDXBitmapButton.Create(Page);
    with fFloorButtons[Dummy] do
    begin
      Visible:=false;
      FirstColor:=coFirstColor;
      SecondColor:=coSecondColor;
      AccerlateColor:=coAccerlateColor;
      RoundCorners:=rcNone;
      Tag:=Dummy;
      OnClick:=FloorButtonClick;
      Page.SetFont(Font);
      if Dummy=0 then
        Text:='&O'
      else
        Text:='&'+IntToStr(Dummy);
    end;
  end;

  fRepairButton:=TDXBitmapButton.Create(Page);
  with fRepairButton do
  begin
    FirstColor:=coFirstColor;
    SecondColor:=coSecondColor;
    AccerlateColor:=coAccerlateColor;
    RoundCorners:=rcLeft;
    OnClick:=RepairClick;
    Page.SetFont(Font);
    OnOwnerDraw:=DrawRepairButton;
    Hint:=ST0501270001;
  end;
end;

destructor TDXBaseBuilder.Destroy;
begin
  fImages.Free;
  fHashMap.Free;
  inherited;
end;

procedure TDXBaseBuilder.Draw(Surface: TDirectDrawSurface;
  var Mem: TDDSurfaceDesc);
begin
  ReDrawRect(ClientRect,Surface,Mem);
end;

procedure TDXBaseBuilder.DrawRepairButton(Sender: TDXComponent;
  Surface: TDirectDrawSurface; var Mem: TDDSurfaceDesc);
begin
  Surface.Draw(Sender.Left+8,Sender.Top+8,Rect(223,20,239,36),Container.ImageList.Items[1].PatternSurfaces[0]);
end;

procedure TDXBaseBuilder.FloorButtonClick(Sender: TObject);
begin
  BuildingFloor:=(Sender as TDXBitmapButton).Tag;

  if assigned(fOnChangeFloor) then
    fOnChangeFloor(Self);
end;

procedure TDXBaseBuilder.ForschEnd(Sender: TObject);
begin
  if forsch_api_GetLastForschprojekt.TypeId=ptEinrichtung then
    LoadImages(Self);
end;

function TDXBaseBuilder.GetButtonRect(const Button: TRoomButton; RoomRect: TRect): TRect;
begin
  if Button.Position.X>0 then
    result.Left:=RoomRect.Left+Button.Position.X
  else
    result.Left:=RoomRect.Right-22+Button.Position.X;

  result.Right:=result.Left+22;

  if Button.Position.Y>0 then
    result.Top:=RoomRect.Top+Button.Position.Y
  else
    result.Top:=RoomRect.Bottom-22+Button.Position.Y;

  result.Bottom:=result.Top+22;
end;

function TDXBaseBuilder.GetImageInfo(Image: String): PBaseBuilderImageInfo;
var
  Index: Integer;
begin
  if fHashMap.FindKey(HashString(Image),Index) then
    result:=Addr(fImageInfos[Index])
  else
    result:=nil;
end;

function TDXBaseBuilder.GetRoomRect: TRect;
var
  Info        : TBaseField;
  Pt          : TPoint;
begin
  Info:=fBase.GetBaseFieldInfo(fBuildFloor,fOverPoint.X,fOverPoint.Y);

  if (fBuildingRoom<>nil) or (Info.Objekt=nil) then
    Pt:=fOverPoint
  else
    Pt:=Point(Info.Objekt.Position.X,Info.Objekt.Position.Y);

  inc(Pt.X,fBeginX);
  inc(Pt.Y,fBeginY);

  result.Left:=Pt.X*ObjektSize+1;
  result.Top:=Pt.Y*ObjektSize+1;

  if fBuildingRoom<>nil then
  begin
    result.Right:=result.Left+ObjektSize*fBuildingRoom.Width;
    result.Bottom:=result.Top+ObjektSize*fBuildingRoom.Height;
  end
  else if Info.Objekt=nil then
  begin
    result.Right:=result.Left+ObjektSize;
    result.Bottom:=result.Top+ObjektSize;
  end
  else
  begin
    if (Info.Objekt.RoomTyp=rtHangar) and (fBuildFloor=0) then
    begin
      result.Right:=result.Left+ObjektSize;
      result.Bottom:=result.Top+ObjektSize;
    end
    else
    begin
      result.Right:=result.Left+ObjektSize*Info.Objekt.Width;
      result.Bottom:=result.Top+ObjektSize*Info.Objekt.Height;
    end;
  end;
end;

procedure TDXBaseBuilder.LoadImages(Sender: TObject);
var
  Dummy      : Integer;
  UsedFields : Array of Array of Boolean;
  Index      : Integer;
  Bitmap     : TBitmap;

  function FindFreePosition(Width,Height: Integer; var Point: TPoint): Boolean;
  var
    X,Y: Integer;

    function IsRectFree(X,Y: Integer): Boolean;
    var
      DummyX,DummyY: Integer;
    begin
      if (X+Width>length(UsedFields)) or (Y+Height>length(UsedFields[X])) then
      begin
        result:=false;
        exit;
      end;
      result:=true;
      for DummyX:=X to X+Width-1 do
      begin
        for DummyY:=Y to Y+Height-1 do
        begin
          if UsedFields[DummyX,DummyY] then
          begin
            result:=false;
            exit;
          end;
        end;
      end;
    end;

  begin
    result:=false;
    for X:=0 to high(UsedFields) do
    begin
      for Y:=0 to high(UsedFields[X]) do
      begin
        if not UsedFields[X,Y] then
        begin
          if IsRectFree(X,Y) then
          begin
            result:=true;
            Point.X:=X;
            Point.Y:=Y;
            exit;
          end;
        end;
      end;
    end;
  end;

  procedure SetImageToPoint(Image: String; Point: TPoint; Width, Height: Integer);
  var
    X,Y: Integer;
    Archiv : TArchivFile;
  begin
    if Image='' then
      exit;
      
    for X:=Point.X to Point.X+Width-1 do
      for Y:=Point.Y to Point.Y+Height-1 do
        UsedFields[X,Y]:=true;

    SetLength(fImageInfos,Length(fImageInfos)+1);

    Index:=high(fImageInfos);
    fImageInfos[Index].ImageName:=Image;
    fImageInfos[Index].Hash:=HashString(Image);
    fImageInfos[Index].ImagePosX:=Point.X*ObjektSize;
    fImageInfos[Index].ImagePosY:=Point.Y*ObjektSize;
    fImageInfos[Index].Width:=Width;
    fImageInfos[Index].Height:=Height;

    fHashMap.InsertID(fImageInfos[Index].Hash,Index);

    Archiv:=archiv_utils_OpenSpecialRessource(Image,savegame_api_GetGameSet.FileName);

    Assert(Archiv<>nil);

    Bitmap.LoadFromStream(Archiv.Stream);

    fImages.Canvas.Draw(fImageInfos[Index].ImagePosX,fImageInfos[Index].ImagePosY,Bitmap);

    Archiv.Free;
  end;

  procedure AddImage(Image: String; Width, Height: Integer);
  var
    Point: TPoint;
  begin
    // Bild f�r Einrichtung bereits eingetragen?
    if GetImageInfo(Image)=nil then
    begin
      if not FindFreePosition(Width,Height,Point) then
      begin
        // Image vergr��ern
        Bitmap.Width:=fImages.Width;
        Bitmap.Height:=fImages.Height;
        Bitmap.PixelFormat:=pf32bit;
        Bitmap.Canvas.CopyRect(fImages.ClientRect,fImages.Canvas,fImages.ClientRect);
        fImages.Canvas.Release;
        fImages.SetSize(fImages.Width+256,fImages.Height);
        fImages.FillRect(fImages.ClientRect,0);
        fImages.Canvas.Draw(0,0,Bitmap);

        SetLength(UsedFields,Length(UsedFields)+4,length(UsedFields[0]));

        Assert(FindFreePosition(Width,Height,Point));
      end;

      SetImageToPoint(Image,Point,Width,Height);
    end;
  end;

begin
  SetLength(fImageInfos,0);
  fHashMap.ClearList;

  Bitmap:=TBitmap.Create;

  try
    fImages.Canvas.Release;
    fImages.SetSize(512,512);
    fImages.FillRect(fImages.ClientRect,bcBlack);
    SetLength(UsedFields,8,8);

    // Hintergrundgrafik laden
    AddImage(BackGroundTextureO,4,4);
    AddImage(BackGroundTextureU,4,4);
    AddImage(BackGroundAlphatron,4,4);

    for Dummy:=0 to high(FloorImages) do
    begin
      AddImage('default\BasisBau:'+FloorImages[Dummy],1,1);
    end;

    for Dummy:=0 to basis_api_GetEinrichtungCount-1 do
    begin
      with basis_api_GetEinrichtung(Dummy) do
      begin
        if RoomTyp=rtHangar then
          AddImage(ImageBase,1,1)
        else
          AddImage(ImageBase,Width,Height);

        AddImage(ImageUnder,Width,Height);
      end;
    end;
    fImages.Canvas.Release;
    fImages.Restore;
  finally
    Bitmap.Free;
  end;
end;

procedure TDXBaseBuilder.MouseLeave;
begin
  if (fOverPoint.X=-1) and (fOverPoint.Y=-1) then
    exit;

  fOverPoint:=Point(-1,-1);

  if (fBuildingRoom=nil) and Assigned(fOnOverRoom) then
    fOnOverRoom(fBuildFloor,fOverPoint.X,fOverPoint.Y);

  Redraw;
  inherited;
end;

procedure TDXBaseBuilder.MouseMove(X, Y: Integer);
var
  NewPoint : TPoint;
  Room     : TRect;
  BtnRect  : TRect;
  Dummy    : Integer;
  Info     : TBaseField;
begin
  NewPoint:=Point(((X-1) div ObjektSize),((Y-1) div ObjektSize));

  dec(NewPoint.X,fBeginX);
  dec(NewPoint.Y,fBeginY);

  NewPoint.X:=max(0,min(NewPoint.X,fBaseWidth-1));
  NewPoint.Y:=max(0,min(NewPoint.Y,fBaseHeight-1));
  if (NewPoint.X<>fOverPoint.X) or (NewPoint.Y<>fOverPoint.Y) then
  begin
    fOverPoint:=NewPoint;

    if (fBuildingRoom=nil) and Assigned(fOnOverRoom) then
      fOnOverRoom(fBuildFloor,fOverPoint.X,fOverPoint.Y);

    Redraw;
  end;

  if (fBuildingRoom=nil) and (fOverPoint.X<>-1) then
  begin
    Info:=fBase.GetBaseFieldInfo(fBuildFloor,fOverPoint.X,fOverPoint.Y);
    if Info.Objekt<>nil then
    begin
      Room:=GetRoomRect;
      // Pr�fen, ob einer der Buttons gedr�ckt wurde
      for Dummy:=0 to high(fRoomButtons) do
      begin
        if (not (Assigned(fRoomButtons[Dummy].AvaibleProc)) or (fRoomButtons[Dummy].AvaibleProc(Info.Objekt)=bbboActive)) then
        begin
          BtnRect:=GetButtonRect(fRoomButtons[Dummy],Room);
          if PtInRect(BtnRect,Point(X,Y)) then
          begin
            Hint:=fRoomButtons[Dummy].Hint;
            exit;
          end;
        end;
      end;
    end;
  end;

  Hint:='';
end;

procedure TDXBaseBuilder.MouseUp(Button: TMouseButton; X, Y: Integer);
var
  Dummy   : Integer;
  Info    : TBaseField;
  Room    : TRect;
  BtnRect : TRect;
begin
  inherited;

  if fOverPoint.X=-1 then
    exit;
    
  if (Button=mbLeft) then
  begin
    if (fBuildingRoom<>nil) then
    begin
      // Ein neuer Raum soll gebaut werden
      if Assigned(fOnBuildRoom) then
        fOnBuildRoom(fBuildFloor,fOverPoint.X,fOverPoint.Y,fBuildingRoom);
    end
    else
    begin
      // Es wurde einfach so geklickt

      Info:=fBase.GetBaseFieldInfo(fBuildFloor,fOverPoint.X,fOverPoint.Y);
      if Info.Objekt<>nil then
      begin
        Room:=GetRoomRect;
        // Pr�fen, ob einer der Buttons gedr�ckt wurde
        for Dummy:=0 to high(fRoomButtons) do
        begin
          if (not (Assigned(fRoomButtons[Dummy].AvaibleProc))) or (fRoomButtons[Dummy].AvaibleProc(Info.Objekt)=bbboActive) then
          begin
            BtnRect:=GetButtonRect(fRoomButtons[Dummy],Room);
            if PtInRect(BtnRect,Point(X,Y)) then
            begin
              fRoomButtons[Dummy].ClickProc(TObject(Info.Objekt));
              exit;
            end;
          end;
        end;
      end;

      if Assigned(fOnClickRoom) then
        fOnClickRoom(fBuildFloor,fOverPoint.X,fOverPoint.Y);
    end;
  end;
end;

procedure TDXBaseBuilder.OnScroll(Sender: TObject);
begin
  fBeginX:=-fHScroll.Value;
  fBeginY:=-fVScroll.Value;
  
  Container.RedrawArea(Rect(Left+1,Top+1,Right-17,Bottom-17),Container.Surface);
end;

procedure TDXBaseBuilder.ReDrawRect(DrawRect: TRect;
  Surface: TDirectDrawSurface; var Mem: TDDSurfaceDesc);
var
  X,Y         : Integer;
  DrawX,DrawY : Integer;
  Info        : TBaseField;
  XMod,YMod   : Integer;
  HighRect    : TRect;
  Index       : Integer;
  Find        : boolean;
  tmpRect     : TRect;

  procedure DrawImage(Image: String; X,Y: Integer);
  var
    Index: Integer;
  begin
    if fHashMap.FindKey(HashString(Image),Index) then
    begin
      with fImageInfos[Index] do
      begin
        Surface.Draw(Left+X,Top+Y,Bounds(ImagePosX,ImagePosY,Width*ObjektSize,Height*ObjektSize),fImages,true);
      end;
    end;
  end;

  procedure DrawPercentBar(const RoomRect: TRect; Top: Integer; Percent: double; FirstColor,Second: TBlendColor);
  var
    Wid      : Integer;
  begin
    Rectangle(Surface,Mem,RoomRect.Left+2,Top,RoomRect.Right-2,Top+4,bcBlack);

    Wid:=round(((RoomRect.Right-RoomRect.Left)-6)*Percent);

    Rectangle(Surface,Mem,RoomRect.Left+3,Top+1,RoomRect.Left+3+Wid,Top+3,FirstColor);
    Rectangle(Surface,Mem,RoomRect.Left+3+Wid,Top+1,RoomRect.Right-3,Top+3,Second);
  end;

  procedure DrawRoom(Room: PEinrichtung;X,Y: Integer);
  var
    Text     : String;
    DayRect  : TRect;
    RoomRect : TRect;
    BtnRect  : TRect;
    Dummy    : Integer;
    ButtonO  : TBaseBuildButtonOption;
  begin
    Assert(Room<>nil);
    DrawImage(fBase.GetRoomImage(Room^,Room.Position.X,Room.Position.Y,fBuildFloor),X,Y);

    inc(X,Left);
    inc(Y,Top);

    if (Room.RoomTyp=rtHangar) and (fBuildFloor=0) then
      RoomRect:=Bounds(X,Y,ObjektSize,ObjektSize)
    else
      RoomRect:=Bounds(X,Y,(ObjektSize*Room.Width),(ObjektSize*Room.Height));

    if Room.days>0 then
    begin
      BlendRectangle(RoomRect,100,bcRed,Surface,Mem);

      Text:=IntToStr(Room.Days);

      DayRect.Left:=X+((RoomRect.Right-RoomRect.Left) div 2)-(BuildDayFont.TextWidth(Text) div 2)-2;
      DayRect.Right:=X+((RoomRect.Right-RoomRect.Left) div 2)+(BuildDayFont.TextWidth(Text) div 2)+2;

      DayRect.Top:=Y+((RoomRect.Bottom-RoomRect.Top) div 2)-(BuildDayFont.TextHeight(Text) div 2)-2;
      DayRect.Bottom:=Y+((RoomRect.Bottom-RoomRect.Top) div 2)+(BuildDayFont.TextHeight(Text) div 2)+2;

      Surface.FillRect(DayRect,bcWhite);
      Rectangle(Surface,Mem,DayRect,bcRed);

      BuildDayFont.Draw(Surface,DayRect.Left,DayRect.Top+2,Text);
    end;

    if (Room.AktHitPoints<Room.HitPoints) then
      DrawPercentBar(RoomRect,RoomRect.Bottom-6,Room.AktHitpoints/Room.HitPoints,bcGreen,bcRed);

    if (Room.RoomTyp=rtDefenseShield) then
      DrawPercentBar(RoomRect,RoomRect.Bottom-11,Room.AktShieldPoints/Room.ShieldStrength,bcBlue,bcNavy);

    // Buttons zeichnen
    for Dummy:=0 to high(fRoomButtons) do
    begin
      if not assigned(fRoomButtons[Dummy].AvaibleProc) then
        ButtonO:=bbboActive
      else
        ButtonO:=fRoomButtons[Dummy].AvaibleProc(Room);

      if ButtonO=bbboInvisible then
        continue;

      with fRoomButtons[Dummy] do
      begin
        BtnRect:=GetButtonRect(fRoomButtons[Dummy],RoomRect);
        
        if ButtonO=bbboActive then
        begin
          Rectangle(Surface,Mem,BtnRect,bcMaroon);
          BlendRectangle(CorrectBottomOfRect(BtnRect),100,bcMaroon,Surface,Mem);
        end;

        if ButtonO in [bbboActive,bbboImageOnly] then
          Surface.Draw(BtnRect.Left+ImgOffSet.X,BtnRect.Top+ImgOffSet.Y,Image,Container.ImageList.Items[1].PatternSurfaces[0]);
      end;
    end;
  end;

begin
  fImages.TransparentColor:=bcFuchsia;
  tmpRect:=Rect(Left+1,Top+1,Right-1,Bottom-1);
  if fHScroll.Visible then
    dec(tmpRect.Right,16);

  if fVScroll.Visible then
    dec(tmpRect.Bottom,16);

  IntersectRect(DrawRect,DrawRect,tmpRect);

  Rectangle(Surface,Mem,ClientRect,bcMaroon);

  Surface.ClippingRect:=DrawRect;

  XMod:=fHScroll.Value mod 2;
  YMod:=fVScroll.Value mod 2;

  DrawX:=1;
  for X:=0 to fRoomCols-1 do
  begin
    DrawY:=1;
    for Y:=0 to fRoomRows-1 do
    begin
      if ((X mod 4)=XMod) and ((Y mod 4)=YMod) then
        DrawImage(fBackGround,DrawX,DrawY);

      inc(DrawY,ObjektSize);
    end;
    inc(DrawX,ObjektSize);
  end;

  if fBase=nil then
    exit;
    
  DrawX:=(fBeginX*ObjektSize)+1;
  for X:=0 to fBaseWidth-1 do
  begin
    DrawY:=(fBeginY*ObjektSize)+1;
    for Y:=0 to fBaseHeight-1 do
    begin
      Info:=fBase.GetBaseFieldInfo(fBuildFloor,X,Y);

      if (Info.Objekt<>nil) and (Info.TopLeft) then
        DrawRoom(Info.Objekt,DrawX,DrawY);
      inc(DrawY,ObjektSize);
    end;
    inc(DrawX,ObjektSize);
  end;

  // Auswahlrechteck zeichnen
  if (fOverPoint.x<>-1) and (fOverPoint.y<>-1) then
  begin
    HighRect:=OffSetRectangle(GetRoomRect,Left,Top);
    if fBuildingRoom<>nil then
    begin
      Find:=fHashMap.FindKey(HashString(fBase.GetRoomImage(fBuildingRoom^,fOverPoint.X,fOverPoint.Y,fBuildFloor)),Index);

      if Find then
      begin
        with fImageInfos[Index] do
        begin
          DrawTransAlpha(Surface,HighRect.Left,HighRect.Top,Bounds(ImagePosX,ImagePosY,Width*ObjektSize,Height*ObjektSize),fImages,150,bcFuchsia);
          if not fBase.CanPlaceRoom(fBuildFloor,fOverPoint.X,fOverPoint.Y,fBuildingRoom) then
            BlendRectangle(HighRect,100,bcRed,Surface,Mem);
        end;
      end;
    end;
    Rectangle(Surface,Mem,HighRect,bcWhite);
  end;

  Surface.ClearClipRect;
end;

procedure TDXBaseBuilder.RepairClick(Sender: TObject);
begin
  fBase.RepairAllRooms;

  ReDraw;
end;

procedure TDXBaseBuilder.Resize(var NewLeft, NewTop, NewWidth,
  NewHeight: Integer);
var
  Dummy: Integer;
begin
  fHScroll.SetRect(NewLeft,NewTop+NewHeight-17,NewWidth-16,17);
  fVScroll.SetRect(NewLeft+NewWidth-17,NewTop,17,NewHeight-16);

  SetBaseSize(fBaseWidth,fBaseHeight);

  for Dummy:=0 to high(fFloorButtons) do
    fFloorButtons[Dummy].SetRect(NewLeft-31,Top+Dummy*31,30,30);

  fRepairButton.SetRect(NewLeft-31,NewTop+NewHeight-30,30,30);
end;

procedure TDXBaseBuilder.SetBase(const Value: TBasis);
var
  Dummy: Integer;
begin
  Assert(Value<>nil);

  fBase := Value;

  Container.IncLock;
  SetBaseSize(fBase.BaseSize.cx,fBase.BaseSize.cy);

  for Dummy:=0 to high(fFloorButtons) do
  begin
    fFloorButtons[Dummy].Visible:=Dummy<fBase.BuildingFloors;
    fFloorButtons[Dummy].RoundCorners:=rcNone;
  end;

  if fBase.BuildingFloors=1 then
    fFloorButtons[0].RoundCorners:=rcLeft
  else
  begin
    fFloorButtons[0].RoundCorners:=rcLeftTop;
    fFloorButtons[fBase.BuildingFloors-1].RoundCorners:=rcLeftBottom;
  end;

  fRepairButton.Visible:=true;
  Container.Declock;
  Redraw;
end;

procedure TDXBaseBuilder.SetBaseSize(Width, Height: Integer);
begin
  fBaseWidth:=Width;
  fBaseHeight:=Height;

  Container.Lock;

  fRoomRows:=(Self.Height div ObjektSize);
  fRoomCols:=(Self.Width div ObjektSize);

  if fBaseWidth>fRoomCols then
  begin
    fHScroll.Max:=fBaseWidth-fRoomCols;
    fHScroll.LargeChange:=fRoomCols;
    fHScroll.Visible:=true;

    fBeginX:=-fHScroll.Value;
  end
  else
  begin
    fHScroll.Value:=0;
    fHScroll.Visible:=false;

    fBeginX:=((fRoomCols-fBaseWidth) div 2);
  end;

  if fBaseHeight>fRoomRows then
  begin
    fVScroll.Max:=fBaseHeight-fRoomRows;
    fVScroll.LargeChange:=fRoomRows;
    fVScroll.Visible:=true;

    fBeginY:=-fVScroll.Value;
  end
  else
  begin
    fVScroll.Value:=0;
    fVScroll.Visible:=false;

    fBeginY:=((fRoomRows-fBaseHeight) div 2);
  end;


  BuildingFloor:=0;

  fOverPoint:=Point(-1,-1);
  Container.Unlock;

  Redraw;
end;

procedure TDXBaseBuilder.SetBuildingFloor(const Value: Integer);
var
  Dummy: Integer;
begin
  fBuildFloor := Value;

  Container.IncLock;

  for Dummy:=0 to high(fFloorButtons) do
    fFloorButtons[Dummy].HighLight:=Dummy=Value;

  Container.Declock;

  if fBuildFloor=0 then
    fBackGround:=BackGroundTextureO
  else
    fBackGround:=BackGroundTextureU;

  Redraw;
end;

procedure TDXBaseBuilder.SetBuildingRoom(const Value: PEinrichtung);
begin
  fBuildingRoom := Value;

  if fOverPoint.X<>-1 then
    Redraw;            
end;

end.
