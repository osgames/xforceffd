{********************************************************************************
*										*
* Copyright (C) 2004  X-Force Team - jim_raynor@web.de				*
*										*
* This file is part of X-Force: Fight For Destiny (X-Force).			*
*										*
* X-Force is free software; you can redistribute it and/or modify		*
* it under the terms of the GNU General Public License as published by		*
* the Free Software Foundation; either version 2 of the License, or		*
* (at your option) any later version.						*
*										*
* X-Force is distributed in the hope that it will be useful,			*
* but WITHOUT ANY WARRANTY; without even the implied warranty of		*
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the			*
* GNU General Public License for more details.					*
*										*
* You should have received a copy of the GNU General Public License		*
* along with X-Force; if not, write to the Free Software			*
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA	*
*										*
*********************************************************************************
*										*
* Ermöglicht das freie Zeichnen in einer Ansicht				*
*										*
********************************************************************************}

{$I ../Settings.inc}

unit DXCanvas;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  DXContainer, NGTypes, DXDraws, DirectDraw;

type
  TDXCanvas = class(TDXComponent)
  private
    fOnPaint     : TDXDrawEvent;
    fOnRectPaint : TDXDrawRectEvent;
    { Private-Deklarationen }
  protected
    { Protected-Deklarationen }
  public
    procedure Draw(Surface: TDirectDrawSurface;var Mem: TDDSurfaceDesc);override;
    procedure RedrawRect(DrawRect: TRect;Surface: TDirectDrawSurface;var Mem: TDDSurfaceDesc);override;
    property OnPaint      : TDXDrawEvent read fOnPaint write fOnPaint;
    property OnPaintRect  : TDXDrawRectEvent read fOnRectPaint write fOnRectPaint;
  end;


implementation

{ TDXCanvas }

procedure TDXCanvas.Draw(Surface: TDirectDrawSurface;var Mem: TDDSurfaceDesc);
begin
  inherited;
  if Assigned(fOnPaint) then fOnPaint(Self,Surface,Mem);
end;

procedure TDXCanvas.RedrawRect(DrawRect: TRect; Surface: TDirectDrawSurface;var Mem: TDDSurfaceDesc);
begin
  IntersectRect(DrawRect,DrawRect,ClientRect);
  if Assigned(fOnRectPaint) then fOnRectPaint(Self,Surface,DrawRect,Mem) else Draw(Surface,Mem);
end;

end.

