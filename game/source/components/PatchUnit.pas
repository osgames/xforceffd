unit PatchUnit;

interface

uses Classes, SysUtils, Windows;

type
  TPatchHeader = record
    Signatur    : Cardinal;
    InfoText    : String[50];
  end;

  TActionType = (atNone,atReplace,atFile,atDeleteRessource,atDeleteFile,atMoveRessource,
                 atRenameRessource,atCreateDir,atStartProgramm,atApplyGameSetLanguage);

  TPatchAction = class
  private
    fAction  : TActionType;
    fParam1  : String;
    fParam2  : String;
    fParam3  : String;
    fReplaceDirs: Boolean;
    fInstallDir: String;
    function GetNeededRessource: String;
    function GetNeedRessource: Boolean;
    procedure SetInstallDir(const Value: String);
  public
    constructor Create(ReplaceDirs: Boolean = true);
    procedure LoadFromStream(Stream: TStream);
    procedure SaveToStream(Stream: TStream);
    function GetText: String;
    property Action: TActionType read fAction write fAction;
    property Param1: String read fParam1 write fParam1;
    property Param2: String read fParam2 write fParam2;
    property Param3: String read fParam3 write fParam3;
    property NeedRessource : Boolean read GetNeedRessource;
    property Ressource     : String read GetNeededRessource;
    property InstallDir    : String read fInstallDir write SetInstallDir;
  end;

implementation

{ TPatchAction }

procedure WriteString(Stream: TStream; Str: String);
var
  Len : Integer;
begin
  Len:=Length(Str);
  Stream.Write(Len,SizeOf(Len));
  Stream.Write(Pointer(Str)^,Len);
end;

function ReadString(Stream: TStream): String;
var
  Len : Integer;
begin
  Stream.Read(Len,SizeOf(Len));
  SetString(result, nil, Len);
  Stream.Read(Pointer(result)^,len);
end;

constructor TPatchAction.Create(ReplaceDirs: Boolean = true);
begin
  fAction:=atNone;
  fReplaceDirs:=ReplaceDirs;
end;

function TPatchAction.GetNeededRessource: String;
begin
  if fAction in [atReplace,atFile,atApplyGameSetLanguage] then
    result:=fParam1
  else
    result:='';
end;

function TPatchAction.GetNeedRessource: Boolean;
begin
  result:=fAction in [atReplace,atFile,atApplyGameSetLanguage];
end;

function TPatchAction.GetText: String;
begin
  case fAction of
    atNone                  : result:='keine Aktion';
    atReplace               : result:=Format('Ressource %s ersetzt %s in %s',[Param1,Param3,Param2]);
    atFile                  : result:=Format('Ressource %s ersetzt die Datei %s.',[Param1,Param2]);
    atDeleteRessource       : result:=Format('L�sche Ressource %s in %s.',[Param1,Param2]);
    atDeleteFile            : Result:=Format('L�sche Datei %s.',[Param1]);
    atMoveRessource         : Result:=Format('Verschiebe Ressource %s von %s nach %s.',[Param1,Param2,Param3]);
    atRenameRessource       : Result:=Format('Ressource %s in %s wird umbenannt in %s.',[Param1,Param2,Param3]);
    atCreateDir             : Result:=Format('Verzeichnis %s erstellen.',[Param1]);
    atStartProgramm         : result:=Format('Programm %s mit Parameter %s ausf�hren',[Param1,Param2]);
    atApplyGameSetLanguage  : result:=Format('Ressource %s als Sprache %s in Spielsatz %s einf�gen',[Param1,Param2,Param3]);
  end;
end;

procedure TPatchAction.LoadFromStream(Stream: TStream);
var
  Buffer: Array[0..239] of Char;
  tmpPath: String;

  procedure ReadParams(Par1,Par2,Par3: boolean);
  begin
    if Par1 then Param1:=ReadString(Stream);
    if Par2 then Param2:=ReadString(Stream);
    if Par3 then Param3:=ReadString(Stream);
  end;

  function ReplaceConst(Param: String): String;
  begin
    result:=StringReplace(Param,'{tmp}',tmpPath,[rfReplaceAll,rfIgnoreCase]);
    result:=StringReplace(result,'{install}',fInstalldir,[rfReplaceAll,rfIgnoreCase]);
  end;

begin
  Stream.Read(fAction,SizeOf(fAction));
  case fAction of
    atReplace                : ReadParams(true,true,true);
    atFile                   : ReadParams(true,true,false);
    atDeleteRessource        : ReadParams(true,true,false);
    atDeleteFile             : ReadParams(true,false,false);
    atMoveRessource          : ReadParams(true,true,true);
    atRenameRessource        : ReadParams(true,true,true);
    atCreateDir              : ReadParams(true,false,false);
    atStartProgramm          : ReadParams(true,true,true);
    atApplyGameSetLanguage   : ReadParams(true,true,true);
  end;

  if fReplaceDirs then
  begin
    GetTempPath(240,Buffer);
    tmpPath:=IncludeTrailingBackslash(Buffer);

    Param1:=ReplaceConst(Param1);
    Param2:=ReplaceConst(Param2);
    Param3:=ReplaceConst(Param3);
  end;

end;

procedure TPatchAction.SaveToStream(Stream: TStream);

  procedure SaveParams(Par1,Par2,Par3: boolean);
  begin
    if Par1 then WriteString(Stream,Param1);
    if Par2 then WriteString(Stream,Param2);
    if Par3 then WriteString(Stream,Param3);
  end;

begin
  Stream.Write(fAction,SizeOf(fAction));
  case fAction of
    atReplace                 : SaveParams(true,true,true);
    atFile                    : SaveParams(true,true,false);
    atDeleteRessource         : SaveParams(true,true,false);
    atDeleteFile              : SaveParams(true,false,false);
    atMoveRessource           : SaveParams(true,true,true);
    atRenameRessource         : SaveParams(true,true,true);
    atCreateDir               : SaveParams(true,false,false);
    atStartProgramm           : SaveParams(true,true,true);
    atApplyGameSetLanguage    : SaveParams(true,true,true);
  end;
end;

procedure TPatchAction.SetInstallDir(const Value: String);
begin
  fInstallDir := IncludeTrailingBackSlash(Value);
end;

end.
