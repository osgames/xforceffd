{********************************************************************************
*										*
* Copyright (C) 2004  X-Force Team - jim_raynor@web.de				*
*										*
* This file is part of X-Force: Fight For Destiny (X-Force).			*
*										*
* X-Force is free software; you can redistribute it and/or modify		*
* it under the terms of the GNU General Public License as published by		*
* the Free Software Foundation; either version 2 of the License, or		*
* (at your option) any later version.						*
*										*
* X-Force is distributed in the hope that it will be useful,			*
* but WITHOUT ANY WARRANTY; without even the implied warranty of		*
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the			*
* GNU General Public License for more details.					*
*										*
* You should have received a copy of the GNU General Public License		*
* along with X-Force; if not, write to the Free Software			*
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA	*
*										*
*********************************************************************************
*										*
* Eingeben eines Hotkeys (Tastatureinstellungen)				*
*										*
********************************************************************************}

{$I ../Settings.inc}

unit DXHotKey;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  DXContainer, DXDraws, XForce_types, Blending, KD4Utils, NGTypes, DirectDraw,
  DirectFont;

type
  TDXHotKey = class(TDXComponent)
  private
    fCorners      : TRoundCorners;
    fCorner       : TCorners;
    fBlendColor   : TBlendColor;
    fFocusColor   : TColor;
    fBorder       : TColor;
    fHotKey       : Char;
    fCanChange    : TCanChangeHotKey;
    fOnChange     : TNotifyEvent;
    function GetFrameRect: TRect;
    procedure SetCorners(const Value: TRoundCorners);
    procedure SetBlendColor(const Value: TBlendColor);
    procedure SetBorder(const Value: TColor);
    procedure SetFocusColor(const Value: TColor);
    procedure SetHotKey(const Value: Char);
  protected
    procedure CreateMouseRegion;override;
  public
    constructor Create(Page: TDXPage);override;
    procedure Draw(Surface: TDirectDrawSurface;var Mem: TDDSurfaceDesc);override;
    procedure KeyDown(var Key: Word;State: TShiftState);override;
    property RoundCorners   : TRoundCorners read fCorners write SetCorners;
    property BlendColor     : TBlendColor read fBlendColor write SetBlendColor;
    property BorderColor    : TColor read fBorder write SetBorder;
    property FocusColor     : TColor read fFocusColor write SetFocusColor;
    property HotKey         : Char read fHotKey write SetHotKey;
    property CanChange      : TCanChangeHotKey read fCanChange write fCanChange;
    property OnChange       : TNotifyEvent read fOnChange write fOnChange;
  end;

implementation

uses
  ui_utils;

{ TDXHotKey }

constructor TDXHotKey.Create(Page: TDXPage);
begin
  inherited;
  TabStop:=true;
end;

procedure TDXHotKey.CreateMouseRegion;
var
  FrameRect: TRect;
  RoundRGN : HRGN;
begin
  FrameRect:=GetFrameRect;
  RoundRGN:=CreateRoundRectRgn(FrameRect.Left,FrameRect.Top,FrameRect.Right+1,FrameRect.Bottom+1,20,20);
  fMouseRegion:=CreateRectRgn(Left,Top,Right,Bottom);
  CombineRgn(fMouseRegion,fMouseRegion,RoundRGN,RGN_AND);
  DeleteObject(RoundRGN);
end;

procedure TDXHotKey.Draw(Surface: TDirectDrawSurface;var Mem: TDDSurfaceDesc);
var
  Text     : String;
  fColor   : TBlendColor;
  Font     : TDirectFont;
  Alpha    : Integer;
begin
  if HasFocus then
  begin
    Font:=WhiteStdFont;
    fColor:=fFocusColor;
    Alpha:=150;
  end
  else
  begin
    fColor:=fBorder;
    Font:=YellowStdFont;
    Alpha:=75;
  end;
  if AlphaElements then
  begin
    BlendRoundRect(ClientRect,Alpha,fBlendColor,Surface,Mem,11,fCorner,ClientRect);
  end;
  FramingRect(Surface,Mem,ClientRect,fCorner,11,fColor);
  Text:=ui_utils_GetText(Char(fHotKey));
  Font.Draw(Surface,Left+(Width shr 1)-(Font.TextWidth(Text) shr 1),Top+3,Text);
end;

function TDXHotKey.GetFrameRect: TRect;
begin
  result:=ClientRect;
  if fCorners in [rcLeft,rcLeftTop,rcLeftBottom,rcNone] then inc(result.Right,20);
  if fCorners in [rcTop,rcLeftTop,rcRightTop,rcNone] then inc(result.Bottom,20);
  if fCorners in [rcRight,rcRightTop,rcRightBottom,rcNone] then dec(result.Left,20);
  if fCorners in [rcBottom,rcLeftBottom,rcRightBottom,rcNone] then dec(result.Top,20);
end;

procedure TDXHotKey.KeyDown(var Key: Word; State: TShiftState);
var
  OldHot: Char;
begin
  OldHot:=fHotKey;
  if Key=$59 then Key:=$5A
  else if Key=$5A then Key:=$59;
  HotKey:=Char(Key);
  if OldHot<>fHotKey then Key:=0;
end;

procedure TDXHotKey.SetBlendColor(const Value: TBlendColor);
begin
  fBlendColor := Value;
  Redraw;
end;

procedure TDXHotKey.SetBorder(const Value: TColor);
begin
  fBorder := Container.Surface.ColorMatch(Value);
  Redraw;
end;

procedure TDXHotKey.SetCorners(const Value: TRoundCorners);
begin
  fCorners := Value;
  fCorner:=[];
  if fCorners in [rcLeft,rcTop,rcLeftTop,rcAll] then include(fCorner,cLeftTop);
  if fCorners in [rcRight,rcTop,rcRightTop,rcAll] then include(fCorner,cRightTop);
  if fCorners in [rcLeft,rcBottom,rcLeftBottom,rcAll] then include(fCorner,cLeftBottom);
  if fCorners in [rcRight,rcBottom,rcRightBottom,rcAll] then include(fCorner,cRightBottom);
  RecreateMouseRegion;
  Redraw;
end;

procedure TDXHotKey.SetFocusColor(const Value: TColor);
begin
  fFocusColor := Container.Surface.ColorMatch(Value);;
  Redraw;
end;

procedure TDXHotKey.SetHotKey(const Value: Char);
var
  Valid : boolean;
begin
  if Value=fHotKey then exit;
  if (ui_utils_GetText(Value)<>'') then
  begin
    Valid:=true;
    if Assigned(fCanChange) then fCanChange(Self,Value,Valid);
    if Valid then
    begin
      fHotKey := Value;
      if Assigned(fOnChange) then fOnChange(Self);
      Redraw;
    end;
  end;
end;

end.
