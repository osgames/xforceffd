unit DialogBaseType;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  DXContainer, ModalDialog, DXGroupCaption, Blending, XForce_types, DXLabel,
  DXBitmapButton, StringConst;

type
  TBaseTypRec     = record
    Button        : TDXBitmapButton;
    CostLabel     : TDXLabel;
    Cost          : Integer;
  end;

  TDialogBaseType = class(TModalDialog)
  private
    fResult           : Integer;
    fBaseTypes        : Array of TBaseTypRec;

    GroupHeader       : TDXGroupCaption;
    TypesBox          : TDXGroupCaption;
    CancelButton      : TDXBitmapButton;

    procedure ButtonClick(Sender: TObject);

    procedure AddBaseTyp(Name: String; Value: Integer; Cost: Integer);

  public
    procedure BeforeShow;override;

    constructor Create(Page: TDXPage);override;

    property Result: Integer read fResult;
  end;

implementation

uses
  savegame_api, Defines;

{ TDialogBaseType }

procedure TDialogBaseType.AddBaseTyp(Name: String; Value, Cost: Integer);
var
  Button: TDXBitmapButton;
  Lab   : TDXLabel;
begin
  { Einstellungen f�r den OK Button }
  Button:=TDXBitmapButton.Create(fPage);
  with Button do
  begin
    Visible:=false;
    OnClick:=ButtonClick;
    Text:=Name;
    Left:=TypesBox.Left+10;
    Width:=175;
    Top:=TypesBox.Top+10+(length(fBaseTypes)*29);
    Font:=Font;
    RoundCorners:=rcNone;
    Height:=28;
    BlendAlpha:=150;
    BlendColor:=bcDarkNavy;
    FirstColor:=$00400000;
    SecondColor:=clBlue;
    AccerlateColor:=clYellow;
    Tag:=Value;
  end;
  AddComponent(Button);

  Lab:=TDXLabel.Create(fPage);
  with Lab do
  begin
    Visible:=false;
    Text:=Format(FBauZeit,[Cost/1]);
    Top:=Button.Top+5;
    Left:=Button.Right+10;
    Width:=100;
    Height:=18;

    Alignment:=taRightJustify;
  end;
  AddComponent(Lab);

  SetLength(fBaseTypes,length(fBaseTypes)+1);
  fBaseTypes[high(fBaseTypes)].Button:=Button;
  fBaseTypes[high(fBaseTypes)].CostLabel:=Lab;
  fBaseTypes[high(fBaseTypes)].Cost:=Cost;
end;

procedure TDialogBaseType.BeforeShow;
var
  Dummy: Integer;
begin
  inherited;

  GroupHeader.Font.Style:=[fsBold];
  GroupHeader.Font.Size:=Font.Size+4;
  GroupHeader.FontColor:=clYellow;
  GroupHeader.Caption:=ST0412070001;

  for Dummy:=0 to high(fBaseTypes) do
  begin
    if fBaseTypes[Dummy].Cost<=savegame_api_GetCredits then
    begin
      fBaseTypes[Dummy].Button.Enabled:=true;
      fBaseTypes[Dummy].CostLabel.Font.Color:=clWhite;
    end
    else
    begin
      fBaseTypes[Dummy].Button.Enabled:=false;
      fBaseTypes[Dummy].CostLabel.Font.Color:=clRed;
    end;
  end;
end;

procedure TDialogBaseType.ButtonClick(Sender: TObject);
begin
  ReadyShow;
  fResult:=(Sender as TDXBitmapButton).Tag;
end;

constructor TDialogBaseType.Create(Page: TDXPage);
var
  Dummy : TBaseType;
begin
  inherited;

  { �berschrift }
  GroupHeader:=TDXGroupCaption.Create(Page);
  with GroupHeader do
  begin
    Visible:=false;
    BorderColor:=$00400000;
    BlendColor:=bcDarkNavy;
    RoundCorners:=rcTop;
    AlphaValue:=200;
    Width:=323;
    Height:=25;
    Alignment:=taCenter;
  end;
  AddComponent(GroupHeader);

  { Hintergrund }
  TypesBox:=TDXGroupCaption.Create(Page);
  with TypesBox do
  begin
    Visible:=false;
    BorderColor:=$00400000;
    BlendColor:=bcDarkNavy;
    RoundCorners:=rcNone;
    AlphaValue:=200;
    Width:=323;
    Height:=106;
    Alignment:=taCenter;
    Top:=GroupHeader.Bottom+1;
  end;
  AddComponent(TypesBox);

  { Einstellungen f�r den OK Button }
  CancelButton:=TDXBitmapButton.Create(Page);
  with CancelButton do
  begin
    Visible:=false;
    OnClick:=ButtonClick;
    Text:=BCancel;
    Width:=TypesBox.Width;
    Top:=TypesBox.Bottom+1;
    Font:=Font;
    RoundCorners:=rcBottom;
    Height:=28;
    BlendAlpha:=150;
    BlendColor:=bcDarkNavy;
    FirstColor:=$00400000;
    SecondColor:=clBlue;
    AccerlateColor:=clYellow;
    Tag:=-1;
    EscButton:=true;
  end;
  AddComponent(CancelButton);

  // Buttons f�r Basistypen erstellen
  for Dummy:=low(TBaseType) to high(TBaseType) do
    AddBaseTyp(BaseNames[Dummy],Integer(Dummy),BaseCosts[Dummy]);
end;

end.
