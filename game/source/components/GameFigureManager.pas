{********************************************************************************
*										*
* Copyright (C) 2004  X-Force Team - jim_raynor@web.de				*
*										*
* This file is part of X-Force: Fight For Destiny (X-Force).			*
*										*
* X-Force is free software; you can redistribute it and/or modify		*
* it under the terms of the GNU General Public License as published by		*
* the Free Software Foundation; either version 2 of the License, or		*
* (at your option) any later version.						*
*										*
* X-Force is distributed in the hope that it will be useful,			*
* but WITHOUT ANY WARRANTY; without even the implied warranty of		*
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the			*
* GNU General Public License for more details.					*
*										*
* You should have received a copy of the GNU General Public License		*
* along with X-Force; if not, write to the Free Software			*
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA	*
*										*
*********************************************************************************
*										*
* Stellt die Verbindung zwischen einer Einheit im Bodeneinsatz und den Daten	*
* TSoldatInfo bzw. TAlien her.							*
*										*
********************************************************************************}

{$I ../Settings.inc}

unit GameFigureManager;

interface


uses
  Classes, SysUtils, XForce_types, DXDraws, DirectDraw, NGTypes, Windows, TraceFile,
  IDHashMap, AlienList, math, KD4Utils, Defines, TypInfo, NotifyList, ConvertRecords,
  ExtRecord, BasisListe, RaumschiffList, StringConst
  {$IFDEF EXTERNALFORMULARS},formular_utils{$ENDIF};

const
  // Events f�r GameFigureManager
  EVENT_MANAGER_ONDESTROY        = 0;
  EVENT_MANAGER_ONCHANGE         = 1;

type
  TBackPackArray = Array of TBackPackItem;

  TEnumRuckSackItemRecord = record
    Index    : Integer;
    Item     : TBackPackItem;
  end;

  TTrefferParameter       = record
    Strength   : Integer;
    Power      : Integer;
    Zone       : TTrefferZone;

    SchadenAbs : Integer;   // Tats�chlich verursachter Schaden
  end;

  TChangedItems           = record
    ID         : Cardinal;
    Used       : Boolean;
  end;

  TChangedItemsArray = Array of TChangedItems;

  TSoldatConfigSlotStatus = (scssVisible,
                             scssDisabled,
                             scssUnvisible);

  TSetzeAusruestungValue  = (savOK,
                             savNoItem,
                             savNotValidItem,
                             savTwoHandWeaponSet,
                             savNoKapazitat,
                             savNotUseable,
                             savNoTU);

  TDoShootResult          = (dsrShootOK,       // Schuss erfolgt
                             dsrReload,        // Schuss erfolgt, Waffe wurde nachgeladen
                             dsrNotEnoughTUs,  // Nicht gen�gend ZEs
                             dsrNoMunition,    // Keine Munition mehr
                             dsrNotReloaded,   // Die Nachladezeit ist noch nicht abgelaufen
                             dsrNotFire);      // Die Waffe ist auf nicht Schiessen eingestellt

const
  ShootOKResults  = [dsrShootOK,dsrReload];

type
  TGameFigureManager = class(TObject)
  private
    fFStatus             : TFigureStatus;
    fKapazitat           : double;
    fIMap                : Integer;
    fIsDead              : boolean;
    fOnBattleField       : boolean;
    fBattleFieldPos      : TPoint;
    fNotifyList          : TNotifyList;
    fEchtzeit            : Boolean;

    // BeginEquip, EndEquip
    fItemsAtBeginn       : TMemoryStream;
    fEquipError          : TDragActionError;
    fSavedTimeUnits      : Integer;

    fWalkLeft            : Boolean;

    fItemsUsedList       : TChangedItemsArray;
    fTemporaryItems      : TChangedItemsArray;

    function GetGuertelSlots: Integer;virtual;
    procedure SetOnBattleField(const Value: boolean);
    function GetGuertelKapazitat: double;
  protected
    fGesundheit   : Integer;
    fLeftHand     : TSoldatWaffe;
    fRightHand    : TSoldatWaffe;
    fPanzerung    : TSoldatPanzerung;
    fGuertel      : TSoldatGuertel;
    fRucksack     : TBackPackArray;
    fRechargeTime : record
                      LeftHand  : Integer;
                      RightHand : Integer;
                    end;
    fMunGuertel   : TBackPackArray;
    fIsEquip      : Integer;
    fRucksackUsed : Boolean;
    function GetSoldatName           : String;virtual;abstract;
    function GetLeftHand             : boolean;virtual;
    function GetMaxKapazitat         : double;virtual;
    function GetMaxGuertelKapazitat  : double;virtual;
    function GetMaxGesundheit        : Integer;virtual;abstract;
    function GetPanzer               : Integer;virtual;abstract;
    function GetTreffSicherheit      : Integer;virtual;abstract;
    function GetEP                   : Integer;virtual;abstract;
    function GetSichtWeite           : Integer;virtual;abstract;
    function GetStrength             : Integer;virtual;abstract;
    function GetUnitZeiteinheiten    : Integer;virtual;abstract;
    function GetReaktion             : Integer;virtual;abstract;

    function GetHomeBasis: TBasis;virtual;
    function GetRaumschiff: TRaumschiff;virtual;

    function ChangeKapazitat(Kapazitat: double): boolean;
    function ItemInBackPack(ID: Cardinal;const List: TBackPackArray): Integer;
    procedure ChangeGesundheit(Gesundheit: Integer);virtual;
    function  UseItem(Index: Integer; Slot: TSoldatConfigSlot = scsRucksack): boolean;
    function  Nachladen(Slot: TSoldatConfigSlot): boolean;

    // Aufnahme einer Beliebigen Ausr�stung
    function CanUseItem(const Item: TLagerItem): TDragActionError;virtual;
    procedure FindItem(const Item: TLagerItem);virtual;

    procedure LaserWaffenaufladen(Time: Integer = 0);virtual;
    procedure ErmittleMaxMunition(var Waffe: TSoldatWaffe);

    procedure ChangeGuertelSize(NewSize: Integer);

    // �bernimmt die Eigenschaften der Ausr�stung in den entsprechenden Slot
    procedure SetItemToSlot(Slot: TSoldatConfigSlot; const Item: TLagerItem; Schuesse: Integer = -1);

    function CalculateGewicht(ID: Cardinal; Schuesse: Integer): double;overload;
    function CalculateGewicht(const Item: TLagerItem; Schuesse: Integer): double;overload;

    procedure EquipError(Error: TDragActionError);

    procedure AddToUsedList(ID: Cardinal; Used: Boolean = true);
    procedure DeleteFromUsed(ID: Cardinal);
  public
    constructor Create(FigureRef: Pointer);virtual;
    destructor Destroy;override;

    { Zeichnen }
    procedure DrawGesicht(Surface: TDirectDrawSurface;const Mem: TDDSurfaceDesc;Left,Top: Integer;Ges: boolean = true);virtual;abstract;
    procedure DrawSmallGesicht(Surface: TDirectDrawSurface;const Mem: TDDSurfaceDesc;Left,Top: Integer;Zeit: Integer;Ges: boolean = true);virtual;abstract;

    { Bodeneinsatz }
    function  NeedTU(TU: Integer): boolean;virtual;
    procedure Timer(Time: Integer);

    function  DoShoot(Slot: TSoldatConfigSlot; ShootInfos: TShootInfos; var Infos: TWaffenParamInfos): TDoShootResult;virtual;
    function CalcTreffChance(Slot: TSoldatConfigSlot; ShootInfos: TShootInfos): Integer;

    procedure BeginnEinsatz;virtual;
    function  DoTreffer(var Treffer: TTrefferParameter): boolean;

    procedure SetEinsatzPosition(const Pos: TPoint);
    function  GetEinsatzPosition(var Pos: TPoint): boolean;

    function  UseObject(Slot: TSoldatConfigSlot;SlotIndex: Integer): TDragActionError;

    procedure PlayWalkSound(Ground: String);

    { Allgemein }
    procedure ZaehleEinsatz;virtual;
    procedure ZaehleAbschuss;virtual;
    procedure ChangeWeapons;
    procedure UpgradeID(ID: Cardinal);
    function  SetzeAusruestung(Slot: TSoldatConfigSlot;ID: Cardinal;Schuesse: Integer = -1): TSetzeAusruestungValue;virtual;
    function  GetSlotStatus(Slot: TSoldatConfigSlot): TSoldatConfigSlotStatus;
    function  GetItemInSlot(Slot: TSoldatConfigSlot; SlotIndex: Integer;out Item: TLagerItem): Boolean;

    procedure BerechneExp(Points: Integer; Typ: TExpType);virtual;

    // Berechnet die ZE unter Ber�cksichtigung der Beladung des Soldaten
    function ZEKapazitat: Integer;

    procedure Dead;virtual;

    { Ausr�stung }
    procedure BeginnEquip;
    function  EndEquip: TDragActionError;

    function CheckAction(Manager: TGameFigureManager;const FromSlot,ToSlot: TItemSlot): TDragActionError;

    // Stellt die Ausr�stung wieder her, die w�hrend eines Einsatzes benutzt wurde
    procedure Reequip;

    { Verwaltung des Rucksacks }
    function AddToRucksack(const Item: TLagerItem;Schuss: Integer = -1): Boolean;overload;
    function AddToRucksack(ID: Cardinal; Schuss: Integer = -1): Boolean;overload;
    function DeleteFromRucksack(Index: Integer): Boolean;
    procedure EnumRucksackItems(var EnumData: TEnumRuckSackItemRecord);
    procedure AktuMunition;
    function Transfer(IDs: TBackPackArray; From: TGameFigureManager): TDragActionError;
    function RucksackSlotsUsed: Integer;
    procedure CompactRucksack;

    { Verwaltung des G�rtel }
    function AddToGuertel(const Item: TLagerItem;Schuss: Integer = -1; Index: Integer = -1): Boolean;
    function DeleteFromGuertel(Index: Integer): Boolean;
    function GetGuertelItem(Index: Integer; out Item: TBackPackItem): Boolean;

    function TakeItem(Item: TObject; Temporary: Boolean = false): Boolean;

    { Speichern/Laden }
    procedure SaveToStream(Stream: TStream);
    procedure LoadFromStream(Stream: TStream);

    { Pointerfunktionen }
    procedure SetRefPointer(FigureRef: Pointer);virtual;abstract;
    function GetAddrOfSlot(Slot: TSoldatConfigSlot): PSoldatWaffe;

    function GetBackPack(Slot: TSoldatConfigSlot): TBackPackArray;

    { Eigenschaften }
    property Name               : String read GetSoldatName;
    property IsDead             : boolean read fIsDead;

    property MaxZeiteinheiten   : Integer read ZEKapazitat;

    property MaxKapazitat       : double read GetMaxKapazitat;
    property Kapazitat          : double read fKapazitat;

    property MaxGuertelKapazitat: double read GetMaxGuertelKapazitat;
    property GuertelKapazitat   : double read GetGuertelKapazitat;

    // Einheiteneigenschaften
    property TreffSicherheit    : Integer read GetTreffSicherheit;
    property Panzer             : Integer read GetPanzer;
    property Gesundheit         : Integer read fGesundheit;
    property MaxGesundheit      : Integer read GetMaxGesundheit;
    property GuertelSlots       : Integer read GetGuertelSlots;
    property ExpierencePoints   : Integer read GetEP;
    property Sichtweite         : Integer read GetSichtWeite;
    property Strength           : Integer read GetStrength;
    property UnitZeitEinheiten  : Integer read GetUnitZeiteinheiten;
    property Reaktion           : Integer read GetReaktion;

    property FigureMap          : Integer read fIMap write fIMap;
    property FigureStatus       : TFigureStatus read fFStatus write fFStatus;
    property LeftHand           : boolean read GetLeftHand;
    property LeftHandWaffe      : TSoldatWaffe read fLeftHand;
    property RightHandWaffe     : TSoldatWaffe read fRightHand;
    property Panzerung          : TSoldatPanzerung read fPanzerung;
    property Guertel            : TSoldatGuertel read fGuertel;

    // Befindet sich die Einheit momentan auf dem Spielfeld ?
    property OnBattleField      : boolean read fOnBattleField write SetOnBattleField;
    function InBasis: boolean;

    // Der aktuelle Einsatz wird in Echtzeit gespielt
    property Echtzeit           : Boolean read fEchtzeit write fEchtzeit;

    //
    property HomeBase           : TBasis read GetHomeBasis;
    property Raumschiff         : TRaumschiff read GetRaumschiff;

    property NotifyList         : TNotifyList read fNotifyList;
  end;

  { Ein Soldat f�r den Bodeneinsatz }
  TSoldatManager = class(TGameFigureManager)
  private
    fSoldatInfo       : PSoldatInfo;
    fDrawSmallGesicht : TDrawSmallSolEvent;
    fDrawGesicht      : TDrawSoldatEvent;
    function GetSoldatInfo: PSoldatInfo;
  protected
    function GetSoldatName           : String;override;
    function GetLeftHand             : boolean;override;
    function GetMaxKapazitat         : double;override;
    function GetMaxGesundheit        : Integer;override;
    function GetPanzer               : Integer;override;
    function GetTreffSicherheit      : Integer;override;
    function GetEP                   : Integer;override;
    function GetSichtWeite           : Integer;override;
    function GetStrength             : Integer;override;
    function GetUnitZeiteinheiten    : Integer;override;
    function GetReaktion             : Integer;override;

    function GetHomeBasis: TBasis;override;
    function GetRaumschiff: TRaumschiff;override;

    function CanUseItem(const Item: TLagerItem): TDragActionError;override;

    procedure FindItem(const Item: TLagerItem);override;

  public
    constructor Create(FigureRef: Pointer);override;

    procedure DrawGesicht(Surface: TDirectDrawSurface;const Mem: TDDSurfaceDesc;Left,Top: Integer;Ges: boolean = true);override;
    procedure DrawSmallGesicht(Surface: TDirectDrawSurface;const Mem: TDDSurfaceDesc;Left,Top: Integer;Zeit: Integer;Ges: boolean = true);override;
    procedure ChangeGesundheit(Gesundheit: Integer);override;
    procedure BeginnEinsatz;override;

    procedure ZaehleEinsatz;override;
    procedure ZaehleAbschuss;override;
    procedure BerechneExp(Points: Integer; Typ: TExpType);override;

    procedure SetRefPointer(FigureRef: Pointer);override;

    property OnDrawGesicht      : TDrawSoldatEvent read fDrawGesicht write fDrawGesicht;
    property OnDrawSmallGeischt : TDrawSmallSolEvent read fDrawSmallGesicht write fDrawSmallGesicht;
    property SoldatInfo         : PSoldatInfo read GetSoldatInfo;
  end;

  TAlienManager = class(TGameFigureManager)
  private
    fAlien        : PAlien;
  protected
    function GetSoldatName           : String;override;
    function GetMaxKapazitat         : double;override;
    function GetMaxGesundheit        : Integer;override;
    function GetPanzer               : Integer;override;
    function GetTreffSicherheit      : Integer;override;
    function GetLeftHand             : boolean;override;
    function GetEP                   : Integer;override;
    function GetSichtWeite           : Integer;override;
    function GetStrength             : Integer;override;
    function GetUnitZeiteinheiten    : Integer;override;
    function GetReaktion             : Integer;override;

    procedure ChangeGesundheit(Gesundheit: Integer);override;

    function CanUseItem(const Item: TLagerItem): TDragActionError;override;
  public
    constructor Create(FigureRef: Pointer);override;

    procedure DrawGesicht(Surface: TDirectDrawSurface;const Mem: TDDSurfaceDesc;Left,Top: Integer;Ges: boolean = true);override;
    procedure DrawSmallGesicht(Surface: TDirectDrawSurface;const Mem: TDDSurfaceDesc;Left,Top,Zeit: Integer;Ges: boolean = true);override;

    procedure SetRefPointer(FigureRef: Pointer);override;

    property Alien        : PAlien read fAlien;
  end;

implementation

uses
  ISOObjects, array_utils, lager_api, alien_api, basis_api, raumschiff_api,
  savegame_api, GameFigure, soldaten_api, ISOMessages;

var
  TGameFigureManagerRecord : TExtRecordDefinition;

{ TGameFigureManager }

function TGameFigureManager.AddToRucksack(const Item: TLagerItem;Schuss: Integer): boolean;
var
  Gewicht  : double;
  RItem    : TBackPackItem;
begin
  Assert(Item.ID<>0,'Ung�ltige ID zum hinzuf�gen beim Rucksack');
  result:=false;

  FindItem(Item);

  if length(fRucksack)>=RucksackSlots then
  begin
    EquipError(daeRucksackKeinPlatz);
    exit;
  end;

  if (not (fIsEquip=0)) or (not fRucksackUsed) then
  begin
    NeedTU(TUUseRucksack);
    fRucksackUsed:=true;
  end;

  if not NeedTU(TUPutRucksack) then
    exit;

  // Gewicht berechnen
  Gewicht:=CalculateGewicht(Item.ID,Schuss);
  if (not ChangeKapazitat(Gewicht)) then
  begin
    result:=false;
    exit;
  end;

  result:=true;

  RItem.ID:=Item.ID;
  RItem.Schuesse:=Schuss;
  RItem.Gewicht:=Gewicht;
  RItem.IsSet:=true;
  SetLength(fRucksack,length(fRucksack)+1);

  fRucksack[high(fRucksack)]:=RItem;

  DeleteFromUsed(Item.ID);

  CompactRucksack;
end;

procedure TGameFigureManager.AktuMunition;
var
  Dummy  : Integer;
  Item   : TLagerItem;
begin
  for Dummy:=0 to high(fRucksack) do
  begin
    Item:=lager_api_GetItem(fRucksack[Dummy].ID)^;
    if (Item.TypeID=ptMunition) and (Item.WaffType<>wtLaser) then
      fRucksack[Dummy].Schuesse:=Item.Munition;
  end;
end;

procedure TGameFigureManager.ChangeGesundheit(Gesundheit: Integer);
begin
  inc(fGesundheit,Gesundheit);
end;

function TGameFigureManager.ChangeKapazitat(Kapazitat: double): boolean;
var
  DezKap : Integer;
  DezCha : Integer;
  MaxKap : Integer;
begin
  DezCha:=trunc(Kapazitat*10);
  DezKap:=trunc(fKapazitat*10);
  MaxKap:=trunc(MaxKapazitat*10);
  if Kapazitat>0 then
  begin
    if DezKap+DezCha>MaxKap then
    begin
      EquipError(daeZuschwer);
      result:=false;
      exit;
    end;
  end;
  inc(DezKap,DezCha);
  fKapazitat:=DezKap/10;
  if fKapazitat<0 then
    fKapazitat:=0;

  NotifyList.CallEvents(EVENT_MANAGER_ONCHANGE,Self);

  result:=true;
end;

procedure TGameFigureManager.ChangeWeapons;
var
  Temp: TSoldatWaffe;
begin
  Temp:=fLeftHand;
  fLeftHand:=fRightHand;
  fRightHand:=Temp;
end;

constructor TGameFigureManager.Create(FigureRef: Pointer);
begin
  fLeftHand.Gesetzt:=false;
  fRightHand.Gesetzt:=false;
  fKapazitat:=0;
  fIsDead:=false;
  ChangeGuertelSize(0);
  fNotifyList:=TNotifyList.Create;

  fIsEquip:=0;
  fOnBattleField:=false;
end;

function TGameFigureManager.DeleteFromRucksack(Index: Integer): Boolean;
begin
  result:=false;
  Assert((Index>=0) and (Index<length(fRucksack)));

  if not NeedTU(TUTakeRucksack) then
    exit;

  ChangeKapazitat(-fRucksack[Index].Gewicht);

  AddToUsedList(fRucksack[Index].ID,false);

  DeleteArray(Addr(fRucksack),TypeInfo(TBackPackArray),Index);

  result:=true;
end;

function TGameFigureManager.DoShoot(Slot: TSoldatConfigSlot; ShootInfos: TShootInfos; var Infos: TWaffenParamInfos): TDoShootResult;
var
  SlotPtr    : PSoldatWaffe;
  TUs        : Integer;
  MunGewicht : double;
begin
  SlotPtr:=GetAddrOfSlot(Slot);
  Assert(SlotPtr<>nil);

  // Keine Waffe gesetzt
  if not SlotPtr.Gesetzt then
  begin
    result:=dsrNoMunition;
    exit;
  end;

  // Waffe steht auf einem Schusstyp, der hier nicht abgehandelt wird
  if (TimeUnitsMultiplikator[SlotPtr.SchussArt]=0) then
  begin
    result:=dsrNotFire;
    exit;
  end;

  // Waffe noch nicht wieder bereit
  if (SlotPtr.TimeLeft>0) then
  begin
    result:=dsrNotReloaded;
    exit;
  end;

  result:=dsrShootOK;

  {$IFNDEF ENDLESSMUNITION}

  BeginnEquip;

  if SlotPtr.WaffenArt<>wtShortRange then
  begin

    // Munition nachladen
    if not (SlotPtr.WaffenArt in NoMunition) and ((SlotPtr.Schuesse=0) or (not SlotPtr.MunGesetzt)) then
    begin
      if not Nachladen(Slot) then
      begin
        EndEquip;
        result:=dsrNoMunition;
        exit;
      end;
      result:=dsrReload;  // Waffe wurde nachladen
    end;

    // Keine Aufladungen in der Laserwaffe
    if (SlotPtr.Schuesse=0) and (SlotPtr.WaffenArt=wtLaser) then
    begin
      EndEquip;
      result:=dsrNoMunition;
      exit;
    end;

    // Schuss abfeuern
    dec(SlotPtr.Schuesse);

    if not (SlotPtr.WaffenArt in NoMunition) then
    begin
      MunGewicht:=CalculateGewicht(SlotPtr.MunID,SlotPtr.Schuesse);
      ChangeKapazitat(MunGewicht-SlotPtr.MunGewicht);

      SlotPtr.MunGewicht:=MunGewicht;

      // Als benutzt markieren, wenn Munition aufgebraucht ist
      if SlotPtr.Schuesse=0 then
      begin
        AddToUsedList(SlotPtr.MunID);
        SlotPtr.MunGesetzt:=false;
      end;
    end;

  end;
  {$ENDIF}

  // Zeiteinheiten verbrauchen
  TUs:=round(SlotPtr.TimeUnits*TimeUnitsMultiplikator[SlotPtr.SchussArt]);

  NeedTU(TUs);

  {$IFNDEF ENDLESSMUNITION}
  case EndEquip of
    daeNone                    : ; // Nichts machen, alles okay
    daeFromKeineZeiteinheiten  :
    begin
      result:=dsrNotEnoughTUs;
      exit;
    end;
    else
    begin
      result:=dsrNotFire;
      exit;
    end;
  end;
  {$ENDIF}

  // Treffsicherheit berechnen
  Infos.Treff:=CalcTreffChance(Slot,ShootInfos);

  // Nachladezeit berechnen
  SlotPtr.TimeLeft:=SlotPtr.TimeUnits*ReloadTimeMultiplikator[SlotPtr.SchussArt];

  Infos.Power:=SlotPtr.Power+PowerModifikator[SlotPtr.SchussArt];
  Infos.Strength:=SlotPtr.Strength+StrengthModifikator[SlotPtr.SchussArt];
  Infos.Art:=SlotPtr.WaffenArt;
  Infos.Schusstyp:=SlotPtr.SchussArt;
  Infos.MunID:=SlotPtr.MunID;

  Infos.ZAxisStart:=UnitShootHeight;
  if ShootInfos.TargetHeight<0 then
    Infos.TargetHeight:=UnitShootHeight
  else
    Infos.TargetHeight:=ShootInfos.TargetHeight;
    
  result:=dsrShootOK;
end;

function TGameFigureManager.DoTreffer(var Treffer: TTrefferParameter): boolean;
const
  SchadensWerte: Array[TTrefferZone] of Double = (
                 {tzNone}               0.0,
                 {tzGlobal}             1.0,
                 {tzHeadLeft}           1.3,
                 {tzHeadRight}          1.3,
                 {tzHeadFront}          1.6,
                 {tzHeadBack}           1.2,
                 {tzTorsoLeft}          1.0,
                 {tzTorsoRight}         1.0,
                 {tzTorsoFront}         0.9,
                 {tzTorsoBack}          1.1,
                 {tzFootLeft}           1.1,
                 {tzFootRight}          1.1,
                 {tzFootFront}          1.1,
                 {tzFootBack}           1.1);






var
  Schaden : Integer;
  Abwehr  : double;
begin
  {$IFDEF NOTREFFER}
  result:=false;
  exit;
  {$ENDIF}
  if Treffer.Power>0 then
    {$IFDEF EXTERNALFORMULARS}
    Abwehr:=formular_utils_SolveFormular('CalcAbwehr',['Angriff','Panzer','Power'],[Treffer.Strength,Panzer,Treffer.Power])
    {$ELSE}
    Abwehr:=Treffer.Strength*(Panzer/Treffer.Power)
    {$ENDIF}
  else
    Abwehr:=Treffer.Strength;

  Schaden:=max(0,round((Treffer.Strength-Abwehr)*SchadensWerte[Treffer.Zone]));

  // Schaden in der Trefferzone berechnen

  Treffer.SchadenAbs:=min(Gesundheit,Schaden);

  ChangeGesundheit(-Schaden);
  result:=fGesundheit<=0;
  {$IFDEF TRACETREFFER}
  GlobalFile.WriteLine;
  GlobalFile.Write('Einheit getroffen',Name);
  GlobalFile.Write('Angriffsst�rke',Treffer.Strength);
  if Panzer>0 then
    GlobalFile.Write('Panzerung',Format('100 / (%d / %d) = %.1n %%',[Treffer.Power,Panzer,(100/(Treffer.Power/Panzer))]))
  else
    GlobalFile.Write('Panzerung','0 %');
  GlobalFile.Write('Abwehr',Format('%.0n',[Abwehr]));
  GlobalFile.Write('Schaden',Format('%d - %.0n = %d',[Treffer.Strength,Abwehr,Schaden]));
  GlobalFile.Write('Gesundheit',fGesundheit);
  GlobalFile.WriteLine;
  {$ENDIF}
end;


procedure TGameFigureManager.EnumRucksackItems(var EnumData: TEnumRuckSackItemRecord);
begin
  if EnumData.Index>High(fRucksack) then
    EnumData.Item.ID:=0
  else
  begin
    EnumData.Item:=fRucksack[EnumData.Index];
    inc(EnumData.Index);
  end;
end;

function TGameFigureManager.GetAddrOfSlot(Slot: TSoldatConfigSlot): PSoldatWaffe;
begin
  case Slot of
    scsLinkeHand  : result:=Addr(fLeftHand);
    scsRechteHand : result:=Addr(fRightHand);
    else result:=nil;
  end;
end;

function TGameFigureManager.GetEinsatzPosition(var Pos: TPoint): boolean;
begin
  if OnBattleField then
  begin
    Pos:=fBattleFieldPos;
    result:=true;
  end
  else
    result:=false;
end;

procedure TGameFigureManager.SetEinsatzPosition(const Pos: TPoint);
begin
  fBattleFieldPos:=Pos;
end;

function TGameFigureManager.GetLeftHand: boolean;
begin
  result:=true;
end;

function TGameFigureManager.GetMaxKapazitat: double;
begin
  result:=0;
end;

function TGameFigureManager.GetSlotStatus(
  Slot: TSoldatConfigSlot): TSoldatConfigSlotStatus;
begin
  result:=scssVisible;
  case Slot of
    scsLinkeHand          :
      if fRightHand.ZweiHand then
        result:=scssDisabled;
    scsRechteHand         :
      if fLeftHand.ZweiHand then
        result:=scssDisabled;
    scsLinkeHandMunition  :
      if fLeftHand.Gesetzt and (fLeftHand.TypeID=ptWaffe) and (not (fLeftHand.WaffenArt in NoMunition)) then
        result:=scssVisible
      else
        result:=scssUnvisible;
    scsRechteHandMunition :
      if fRightHand.Gesetzt and (fRightHand.TypeID=ptWaffe) and (not (fRightHand.WaffenArt in NoMunition)) then
        result:=scssVisible
      else
        result:=scssUnvisible;
  end;
end;

function TGameFigureManager.InBasis: boolean;
begin
  if OnBattleField then
  begin
    result:=false;
    exit;
  end;
  result:=true;
  if Raumschiff=nil then exit;
  if not (sabEquipt in Raumschiff.Abilities) then result:=false;
end;

function TGameFigureManager.ItemInBackPack(ID: Cardinal; const List: TBackPackArray): Integer;
var
  Dummy: Integer;
begin
  result:=-1;
  for Dummy:=0 to high(List) do
  begin
    if (List[Dummy].IsSet) and (List[Dummy].ID=ID) then
    begin
      result:=Dummy;
      exit;
    end;
  end;
end;

procedure TGameFigureManager.ErmittleMaxMunition(var Waffe: TSoldatWaffe);
var
  ID : Cardinal;
begin
  if not Waffe.Gesetzt then exit;
  if Waffe.WaffenArt=wtLaser then
    ID:=Waffe.ID
  else if Waffe.MunGesetzt then
    ID:=Waffe.MunID
  else
    ID:=0;
  if ID<>0 then
  begin
    Waffe.SchussMax:=lager_api_GetItem(ID).Munition;
  end
  else
    Waffe.SchussMax:=0;
end;

procedure TGameFigureManager.LoadFromStream(Stream: TStream);
var
  Rec     : TExtRecord;

  procedure ReadBackPack(var BackPack: TBackPackArray; List: TExtRecordList);
  var
    Dummy: Integer;
  begin
    SetLength(BackPack,List.Count);
    for Dummy:=0 to high(BackPack) do
      BackPack[Dummy]:=RecordToBackPackItem(List.Item[Dummy]);
  end;

  procedure ReadChangeItems(RecList: TExtRecordList; var Arr: TChangedItemsArray);
  var
    Dummy: Integer;
  begin
    SetLength(Arr,RecList.Count);
    for Dummy:=0 to high(Arr) do
    begin
      with RecList.Item[Dummy] do
      begin
        Arr[Dummy].ID:=GetCardinal('ID');
        Arr[Dummy].Used:=GetBoolean('Used');
      end;
    end;
  end;

begin
  Rec:=TExtRecord.Create(TGameFigureManagerRecord);
  Rec.LoadFromStream(Stream);

  fLeftHand:=RecordToSoldatWaffe(Rec.GetRecordList('Waffen').Item[0]);
  fRightHand:=RecordToSoldatWaffe(Rec.GetRecordList('Waffen').Item[1]);

  fPanzerung:=RecordToSoldatPanzerung(Rec.GetRecordList('Panzerung').Item[0]);
  fGuertel:=RecordToSoldatGuertel(Rec.GetRecordList('Guertel').Item[0]);

  fKapazitat:=Rec.GetDouble('Kapazitaet');

  ReadBackPack(fMunGuertel,Rec.GetRecordList('GuertelSlots'));
  ReadBackPack(fRucksack,Rec.GetRecordList('Rucksack'));

  // UsedItems laden
  ReadChangeItems(Rec.GetRecordList('ItemsUsedList'),fItemsUsedList);

  ReadChangeItems(Rec.GetRecordList('TemporaryItems'),fTemporaryItems);

  Rec.Free;
end;

function TGameFigureManager.Nachladen(Slot: TSoldatConfigSlot): boolean;
var
  Waffe   : PSoldatWaffe;
  Dummy   : Integer;
  Item    : TLagerItem;
  GItem   : TBackPackItem;
  Index   : Integer;
begin
  // Automatisches Nachladen geschieht aus dem G�rtel heraus
  result:=true;
  Waffe:=GetAddrOfSlot(Slot);
  if not (Waffe.WaffenArt in NoMunition) then
  begin
    result:=false;
    // Nach passender Munition im G�rtel suchen
    Index:=-1;
    for Dummy:=0 to GuertelSlots-1 do
    begin
      if fMunGuertel[Dummy].Isset then
      begin
        Item:=lager_api_GetItem(fMunGuertel[Dummy].ID)^;
        if (Item.TypeID=ptMunition) and (Item.MunFor=Waffe.ID) then
        begin
          Index:=Dummy;
          break;
        end;
      end;
    end;

    // Keine Munition gefunden?
    if Index=-1 then
      exit;

    GItem:=fMunGuertel[Index];

    DeleteFromGuertel(Index);

    case Slot of
      scsLinkeHand  : result:=SetzeAusruestung(scsLinkeHandMunition,GItem.ID,GItem.Schuesse)=savOK;
      scsRechteHand : result:=SetzeAusruestung(scsRechteHandMunition,GItem.ID,GItem.Schuesse)=savOK;
    end;
  end;
end;

function TGameFigureManager.NeedTU(TU: Integer): boolean;
begin
  result:=true;
  {$IFDEF NOTUCOST}
  exit;
  {$ENDIF}
  if (not fOnBattleField) or fEchtzeit then
    exit;

  rec.Objekt:=Self;
  rec.TimeUnits:=TU;
  SendVMessage(vmNeedTimeUnits);

  case rec.TUresult of
    nurOkay: result:=true;
    nurNoTU:
      begin
        EquipError(daeFromKeineZeiteinheiten);
        result:=false;
      end;
    nurNotOnMove:
      begin
        EquipError(daeUnitNotOnMove);
        result:=false;
      end;
  end;
end;

function TGameFigureManager.RucksackSlotsUsed: Integer;
begin
  result:=length(fRucksack);
end;

procedure TGameFigureManager.SaveToStream(Stream: TStream);
var
  Rec     : TExtRecord;

  procedure SaveBackPack(const BackPack: TBackPackArray; List: TExtRecordList);
  var
    Dummy: Integer;
  begin
    for Dummy:=0 to high(BackPack) do
      List.Add(BackPackItemToRecord(BackPack[Dummy]));
  end;

  procedure SaveChangedItems(RecList: TExtRecordList; const Arr: TChangedItemsArray);
  var
    Dummy: Integer;
  begin
    for Dummy:=0 to high(Arr) do
    begin
      with RecList.Add do
      begin
        SetCardinal('ID',Arr[Dummy].ID);
        SetBoolean('Used',Arr[Dummy].Used);
      end;
    end;
  end;

begin

  // Achtung hier muss aufgepasst werden. Es werden die Einstellungen beim Ausr�stung
  // �ber SaveToStream gespeichert und bei Fehlern wieder durch LoadFromStream geladen
  // Es w�re also riskant hier Eigenschaften zu speichern, die sich w�rend dem Ausr�sten
  // �ndern k�nnten
  Rec:=TExtRecord.Create(TGameFigureManagerRecord);

  Rec.GetRecordList('Waffen').Add(SoldatWaffeToRecord(fLeftHand));
  Rec.GetRecordList('Waffen').Add(SoldatWaffeToRecord(fRightHand));

  Rec.GetRecordList('Panzerung').Add(SoldatPanzerungToRecord(fPanzerung));
  Rec.GetRecordList('Guertel').Add(SoldatGuertelToRecord(fGuertel));

  Rec.SetDouble('Kapazitaet',fKapazitat);

  SaveBackPack(fMunGuertel,Rec.GetRecordList('GuertelSlots'));
  SaveBackPack(fRucksack,Rec.GetRecordList('Rucksack'));

  // UsedItems speichern
  SaveChangedItems(Rec.GetRecordList('ItemsUsedList'),fItemsUsedList);

  // Tempor�re Items speichern
  SaveChangedItems(Rec.GetRecordList('TemporaryItems'),fTemporaryItems);

  Rec.SaveToStream(Stream);

  Rec.Free;
end;

function TGameFigureManager.SetzeAusruestung(Slot: TSoldatConfigSlot; ID: Cardinal;Schuesse: Integer): TSetzeAusruestungValue;
var
  SetWaffe   : PSoldatWaffe;
  OtherWaffe : PSoldatWaffe;
  TU         : Integer;
  ItemPtr    : PLagerItem;
  Item       : TLagerItem;
  MunGewicht : double;
  Error      : TDragActionError;
begin
  result:=savOK;
  ItemPtr:=lager_api_GetItem(ID,false);
  if ItemPtr<>nil then
  begin
    Item:=ItemPtr^;
    FindItem(Item);
  end;

  BeginnEquip;

  try
    if (ItemPtr<>nil) then
    begin
      Error:=CanUseItem(Item);
      if Error<>daeNone then
      begin
        EquipError(Error);
        result:=savNotUseable;
        exit;
      end;
    end;

    case Slot of
      scsLinkeHand,scsRechteHand:
      begin
        SetWaffe:=nil;
        OtherWaffe:=nil;
        case Slot of
          scsLinkeHand:
          begin
            SetWaffe:=@fLeftHand;
            OtherWaffe:=@fRightHand;
          end;
          scsRechteHand:
          begin
            SetWaffe:=@fRightHand;
            OtherWaffe:=@fLeftHand;
          end;
        end;
        if (ItemPtr<>nil) and (not (Item.TypeID in [ptWaffe,ptMine,ptGranate,ptSensor])) then
        begin
          EquipError(daeInvalidItemInSlot);
          result:=savNotValidItem;
          exit;
        end;
        if (ItemPtr<>nil) and OtherWaffe.Gesetzt and OtherWaffe.ZweiHand then
        begin
          case Slot of
            scsLinkeHand  : EquipError(daeLinksZweiHandwaffeGesetzt);
            scsRechteHand : EquipError(daeRechteZweiHandwaffeGesetzt);
          end;
          result:=savTwoHandWeaponSet;
          exit;
        end;
        if SetWaffe.Gesetzt and (SetWaffe.ID=ID) then
          exit;
        if (ItemPtr<>nil) and (Item.TypeID=ptWaffe) and (not Item.Einhand) and (OtherWaffe.Gesetzt) then
        begin
          case Slot of
            scsLinkeHand  : EquipError(daeRechteHandGesetzt);
            scsRechteHand : EquipError(daeLinkeHandGesetzt);
          end;
          result:=savTwoHandWeaponSet;
          exit;
        end;
        if SetWaffe.Gesetzt then
        begin
          TU:=TUTakeHand;
          if SetWaffe.MunGesetzt then
            inc(TU,TUTakeMunition);

          if not NeedTU(TU) then
            exit;

          ChangeKapazitat(-SetWaffe.Gewicht);
          AddToUsedList(SetWaffe.ID,false);
          if SetWaffe.MunGesetzt then
          begin
            ChangeKapazitat(-SetWaffe.MunGewicht);
            AddToUsedList(SetWaffe.MunID,false);
          end;

          FillChar(SetWaffe^,SizeOf(SetWaffe^),#0);

        end;
        if ItemPtr=nil then
          exit;

        with Item do
        begin
          if not ChangeKapazitat(Gewicht) then
          begin
            result:=savNoKapazitat;
            exit;
          end;
          if not NeedTU(TUPutHand) then
          begin
            result:=savNoTU;
            exit;
          end;
          SetItemToSlot(Slot,Item,Schuesse);
          SetWaffe.Gesetzt:=true;
        end;
      end;
      scsLinkeHandMunition,scsRechteHandMunition:
      begin
        SetWaffe:=nil;
        case Slot of
          scsLinkeHandMunition:   SetWaffe:=@fLeftHand;
          scsRechteHandMunition:  SetWaffe:=@fRightHand;
        end;
        Assert(SetWaffe<>nil);
        if (ItemPtr<>nil) and (not ((Item.TypeID=ptMunition) and (Item.Munfor=SetWaffe.ID))) then
        begin
          EquipError(daeFalscheMunition);
          exit;
        end;
        if SetWaffe.MunGesetzt and (SetWaffe.MunID=ID) and (SetWaffe.Schuesse=Schuesse) then exit;
        if SetWaffe.MunGesetzt then
        begin
          if not NeedTU(TUTakeMunition) then
            exit;

          ChangeKapazitat(-SetWaffe.MunGewicht);
          AddToUsedList(SetWaffe.MunID,false);
          SetWaffe.MunGesetzt:=false;
        end;

        // Neue Ausr�stung
        if ItemPtr=nil then
          exit;

        with Item do
        begin
          MunGewicht:=CalculateGewicht(Item,Schuesse);
          if not ChangeKapazitat(MunGewicht) then
          begin
            result:=savNoKapazitat;
            exit;
          end;

          if not NeedTU(TUPutMunition) then
          begin
            result:=savNoTU;
            exit;
          end;

          SetItemToSlot(Slot,Item,Schuesse);
          SetWaffe.MunGesetzt:=true;
        end;
      end;
      scsPanzerung:
      begin
        if (ItemPtr<>nil) and (Item.TypeID<>ptPanzerung) then
        begin
          EquipError(daeInvalidItemInSlot);
          exit;
        end;
        if fPanzerung.Gesetzt and (fPanzerung.ID=ID) then exit;

        if fPanzerung.Gesetzt then
        begin
          ChangeKapazitat(-fPanzerung.Gewicht);
          AddToUsedList(fPanzerung.ID,false);
          FillChar(fPanzerung,SizeOf(fPanzerung),#0);
        end;

        if ItemPtr=nil then
          exit;

        with Item do
        begin
          if not ChangeKapazitat(Gewicht) then
          begin
            result:=savNoKapazitat;
            exit;
          end;
          if not NeedTU(TUPutPanzerung) then
          begin
            result:=savNoTU;
            exit;
          end;
          SetItemToSlot(Slot,Item);
          fPanzerung.Gesetzt:=true;
        end;
      end;
      scsGuertel:
      begin
        if (ItemPtr<>nil) and (Item.TypeID<>ptGuertel) then
        begin
          EquipError(daeInvalidItemInSlot);
          exit;
        end;
        if fGuertel.Gesetzt and (fGuertel.ID=ID) then
          exit;
        if fGuertel.Gesetzt then
        begin
          if not NeedTU(TUTakeGuertel) then
            exit;

          ChangeKapazitat(-fGuertel.Gewicht);
          AddToUsedList(fGuertel.ID,false);
          FillChar(fGuertel,SizeOf(fGuertel),#0);
        end;
        if ItemPtr=nil then
        begin
          ChangeGuertelSize(0);
          exit;
        end;

        with Item do
        begin
          if not ChangeKapazitat(Gewicht) then
          begin
            result:=savNoKapazitat;
            exit;
          end;
          if not NeedTU(TUPutGuertel) then
          begin
            result:=savNoTU;
            exit;
          end;
          SetItemToSlot(Slot,Item);
          fGuertel.Gesetzt:=true;
        end;
      end;
    end;
  finally
    if EndEquip<>daeNone then
      result:=savNotUseable;
  end;
end;

function TGameFigureManager.Transfer(IDs: TBackPackArray;From: TGameFigureManager): TDragActionError;
var
  Item      : TLagerItem;
  Dummy     : Integer;
  AbstandOK : boolean;
  Pos1,Pos2 : TPoint;
begin
  { Pr�fen, ob sich die Einheiten in der N�he befinden }
  AbstandOK:=true;

  // Beide in der Basis
  if InBasis<>From.InBasis then
    AbstandOK:=false;

  if not InBasis then
  begin
    // Beide auf dem Kampffeld
    if AbstandOK and OnBattleField<>From.OnBattleField then
      AbstandOK:=false;

    // Beide im Gleichem Raumschiff
    if AbstandOK then
      AbstandOK:=(Raumschiff=From.Raumschiff);

    // Beide auf dem Kampffeld (Entfernung pr�fen)
    if AbstandOK and OnBattleField then
    begin
      Assert(GetEinsatzPosition(Pos1));
      Assert(From.GetEinsatzPosition(Pos2));
      AbstandOK:=(abs(Pos1.X-Pos2.X)<3) and (abs(Pos1.Y-Pos2.Y)<3);
      if AbstandOK then
      begin
        Rec.FromP:=Pos1;
        Rec.ToP:=Pos2;
        SendVMessage(vmIsWayFree);
        if Rec.WayFreeRes.WayBlocked in [wbDoor,wbObject,wbWall,wbOutOfBounds] then
          AbstandOK:=false;
      end;
    end;
  end
  else
  begin
    // Pr�fen, ob beide Einheiten in der gleichen Basis sind
    if HomeBase<>From.HomeBase then
      AbstandOK:=false;
  end;

  if not AbstandOK then
  begin
    result:=daeAbstandNichtOK;
    exit;
  end;

  BeginnEquip;
  { �bertragen }
  for Dummy:=0 to high(IDs) do
  begin
    Item:=lager_api_GetItem(IDs[Dummy].ID)^;
    AddToRucksack(Item,IDs[Dummy].Schuesse);
  end;
  result:=EndEquip;
end;

function TGameFigureManager.UseItem(Index: Integer; Slot: TSoldatConfigSlot): boolean;
begin
  result:=false;
  case Slot of
    scsRucksack:
      result:=DeleteFromRucksack(Index);
    scsMunition:
      result:=DeleteFromGuertel(Index);
    scsLinkeHand,scsRechteHand,
    scsLinkeHandMunition,scsRechteHandMunition:
      result:=SetzeAusruestung(Slot,0)=savOK;
  end;

  // Wenn alles Fehlerfrei funktioniert hat
  if result then
  begin
    // jetzt ein kleiner Hack. DeleteFromGuertel legt einen Eintrag in die benutzen
    // Ausr�stungen ab. Allerdings mit Used=false. Der Eintrag sollte im letzten Element
    // liegen
    Assert(length(fItemsUsedList)>0);
    fItemsUsedList[high(fItemsUsedList)].Used:=true;
  end;
end;

procedure TGameFigureManager.ZaehleAbschuss;
begin

end;

procedure TGameFigureManager.ZaehleEinsatz;
begin

end;

procedure TGameFigureManager.UpgradeID(ID: Cardinal);
var
  MunOld  : Integer;
  Item    : TLagerItem;

  procedure UpgradeSlot(Slot: TSoldatConfigSlot);
  var
    Item: TLagerItem;
  begin
    if GetItemInSlot(Slot,0,Item) then
    begin
      if Item.ID=ID then
        SetItemToSlot(Slot,Item,Item.Munition);
    end;
  end;

  procedure UpgradeBackPack(var BackPack: TBackPackArray);
  var
    Dummy      : Integer;
    tmpGewicht : double;
  begin
    for Dummy:=0 to High(BackPack) do
    begin
      if (BackPack[Dummy].IsSet) and (ID=BackPack[Dummy].ID) then
      begin
        if (BackPack[Dummy].Schuesse=MunOld) or (BackPack[Dummy].Schuesse=-1) then
          BackPack[Dummy].Schuesse:=Item.Munition;

        tmpGewicht:=CalculateGewicht(Item,BackPack[Dummy].Schuesse);

        if BackPack[Dummy].Gewicht<>tmpGewicht then
        begin
          ChangeKapazitat(-BackPack[Dummy].Gewicht);
          ChangeKapazitat(tmpGewicht);
          BackPack[Dummy].Gewicht:=tmpGewicht;
        end;
      end;
    end;
  end;

begin
  UpgradeSlot(scsLinkeHand);
  UpgradeSlot(scsRechteHand);
  UpgradeSlot(scsLinkeHandMunition);
  UpgradeSlot(scsRechteHandMunition);
  UpgradeSlot(scsGuertel);
  UpgradeSlot(scsPanzerung);

  Item:=lager_api_GetItem(ID)^;

  // Munition vor dem Upgrade berechnen
  MunOld:=min(Item.Munition-1,round(Item.Munition/1.05));

  UpgradeBackPack(fRucksack);
  UpgradeBackPack(fMunGuertel);
end;

procedure TGameFigureManager.BeginnEinsatz;
begin
end;

procedure TGameFigureManager.Timer(Time: Integer);
begin
  // LaserWaffen nachladen
  // Im Rundenbasierten Modus werden Laserwaffen nach R�ckkehr ins Raumschiff nachgeladen
  if Echtzeit then
    LaserWaffenaufladen(Time);
end;

procedure TGameFigureManager.LaserWaffenaufladen(Time: Integer);

  function RechargeLaser(var Waffe: TSoldatWaffe; RemainTime: Integer): Integer;
  begin
    if Waffe.Gesetzt and (Waffe.WaffenArt=wtLaser) then
    begin
      if Waffe.SchussMax>Waffe.Schuesse then
      begin
        dec(RemainTime,Time);
        if RemainTime<0 then
        begin
          // Maximum an Ladung ermitteln
          inc(Waffe.Schuesse);
          inc(RemainTime,(Waffe.Strength div 20)*250);
          if FigureStatus=fsFriendly then
            SendVMessage(vmRedrawSolList);
        end;
      end;
    end;
    result:=RemainTime;
  end;

  function LaserWaffeAufladen(var Waffe: TSoldatWaffe): Integer;
  begin
    ErmittleMaxMunition(Waffe);
    if Waffe.Gesetzt and (Waffe.WaffenArt=wtLaser) then
    begin
      //Waffe.MunGesetzt:=true;
      Waffe.Schuesse:=Waffe.SchussMax;
      result:=(Waffe.Strength div 20)*250;
    end
    else
      result:=-1;
  end;

begin
  if Time>0 then
  begin
    fRechargeTime.LeftHand:=RechargeLaser(fLeftHand,fRechargeTime.LeftHand);
    fRechargeTime.RightHand:=RechargeLaser(fRightHand,fRechargeTime.RightHand);
  end
  else
  begin
    fRechargeTime.LeftHand:=LaserWaffeAufladen(fLeftHand);
    fRechargeTime.RightHand:=LaserWaffeAufladen(fRightHand);
  end;
end;

function TGameFigureManager.UseObject(Slot: TSoldatConfigSlot;SlotIndex: Integer): TDragActionError;
var
  Index: Integer;
  Item : TLagerItem;
begin
  {$IFDEF ENDLESSUSE}
  result:=daeNone;
  exit;
  {$ENDIF}

  // Pr�fen, ob Ausr�stung im angegeben Slot �bergeben wird
  if not GetItemInSlot(Slot,SlotIndex,Item) then
  begin
    result:=daeInvalidItemInSlot;
    exit;
  end;

  // Pr�fen, ob Ausr�stung Sensor, Mine oder Granate ist
  if not (Item.TypeID in [ptSensor,ptMine,ptGranate]) then
  begin
    result:=daeInvalidItemInSlot;
    exit;
  end;

  BeginnEquip;
  // Pr�fen, ob die Ausr�stung �berhaupt benutzt werden kann
  EquipError(CanUseItem(Item));

  case Slot of
    scsLinkeHand,scsRechteHand:
    begin

      Index:=ItemInBackPack(Item.ID,fRucksack);
      if Index<>-1 then
        UseItem(Index,scsRucksack)
      else
      begin
        Index:=ItemInBackPack(Item.ID,fMunGuertel);
        if Index<>-1 then
        begin
          UseItem(Index,scsMunition)
        end
        else
          UseItem(0,Slot);
      end;
    end;
    scsRucksack:   UseItem(SlotIndex,scsRucksack);
    scsMunition:   UseItem(SlotIndex,scsMunition);
  end;

  // F�r Granaten TUThrowItem berechnen
  // F�r Sensoren, Minen TUUseItem berechnen
  case Item.TypeID of
    ptGranate        : NeedTU(TUThrowItem);
    ptSensor,ptMine  : NeedTU(TUUseItem);
  end;

  // Zur�cksetzen und Nachricht senden, wenn nicht genug ZEs zur Verf�gung standen
  result:=EndEquip;
end;

function TGameFigureManager.TakeItem(Item: TObject; Temporary: Boolean): Boolean;
var
  LItem    : TLagerItem;
  WasTaken : boolean;
  Dummy    : Integer;
begin
  Assert(Item is TISOItem,'Item muss TISOItem sein');

  WasTaken:=false;
  BeginnEquip;
  if NeedTU(TUTakeBoden) then
  begin
    LItem:=TISOItem(Item).Item;

    FindItem(LItem);

    // Noch nicht erforschte Ausr�stung darf nur in den Rucksack
    if LItem.Useable then
    begin
      if LItem.TypeID in [ptMine,ptGranate,ptSensor,ptWaffe] then
      begin
        if not LeftHandWaffe.Gesetzt then
        begin
          if SetzeAusruestung(scsLinkeHand,LItem.ID,LItem.Munition)=savOK then
            WasTaken:=true;
        end
        else if not RightHandWaffe.Gesetzt then
        begin
          if SetzeAusruestung(scsRechteHand,LItem.ID,LItem.Munition)=savOK then
            WasTaken:=true;
        end;
      end
      else if LItem.TypeID=ptMunition then
      begin
        if LeftHandWaffe.Gesetzt and (LItem.MunFor=LeftHandWaffe.ID) and (not (LeftHandWaffe.MunGesetzt)) then
        begin
          if SetzeAusruestung(scsLinkeHandMunition,LItem.ID,LItem.Munition)=savOK then
            WasTaken:=true;
        end else if RightHandWaffe.Gesetzt and (LItem.MunFor=RightHandWaffe.ID) and (not (RightHandWaffe.MunGesetzt)) then
        begin
          if SetzeAusruestung(scsRechteHandMunition,LItem.ID,LItem.Munition)=savOK then
            WasTaken:=true;
        end;
      end
      else if LItem.TypeID=ptPanzerung then
      begin
        if not Panzerung.Gesetzt then
        begin
          if SetzeAusruestung(scsPanzerung,LItem.ID)=savOK then
            WasTaken:=true;
        end;
      end;
    end;

    if not WasTaken then
    begin
      fEquipError:=daeNone;

      for Dummy:=0 to high(fItemsUsedList) do
      begin
        if (fItemsUsedList[Dummy].ID=LItem.ID) and fItemsUsedList[Dummy].Used then
        begin
          WasTaken:=AddToGuertel(LItem,LItem.Munition);
          break;
        end;
      end;

      if not WasTaken then
        WasTaken:=AddToRucksack(LItem,LItem.Munition);
    end;
  end;
  if EndEquip<>daeNone then
  begin
    result:=false;
    exit;
  end;

  result:=WasTaken;

  if WasTaken then
  begin
    if FigureStatus=fsFriendly then
    begin
      if TISOItem(Item).FriendStatus<>fsFriendly then
        BuchPunkte(bptItemFound,TISOItem(Item).PunkteWert);
    end
    else if FigureStatus=fsEnemy then
    begin
      if TISOItem(Item).FriendStatus<>fsEnemy then
        BuchPunkte(bptItemLost,TISOItem(Item).PunkteWert);
    end;

    // Zum L�schen markieren
    TISOItem(Item).Deleted:=true;

    // Auf die Liste der Tempor�ren Items legen
    if Temporary then
    begin
      SetLength(fTemporaryItems,length(fTemporaryItems)+1);

      fTemporaryItems[high(fTemporaryItems)].ID:=TISOItem(Item).Item.ID;
    end;
  end;

end;

function TGameFigureManager.AddToGuertel(const Item: TLagerItem; Schuss: Integer; Index: Integer): Boolean;
var
  Dummy    : Integer;
  Gewicht  : double;
begin
  result:=false;
  // Ersten leeren Slot im G�rtel finden
  if Index=-1 then
  begin
    for Dummy:=0 to GuertelSlots-1 do
    begin
      if not fMunGuertel[Dummy].IsSet then
      begin
        Index:=Dummy;
        break;
      end;
    end;
    if Index=-1 then
    begin
      EquipError(daeGuertelNichtFrei);
      exit;
    end;
  end;

  if (Item.TypeID in [ptPanzerung,ptGuertel]) and ((Item.TypeID<>ptWaffe) or (Item.Einhand)) then
  begin
    EquipError(daeInvalidItemInSlot);
    exit;
  end;

  // Maximalgewicht im G�rtel pr�fen
  Gewicht:=GetGuertelKapazitat;

  // Gewicht des neuen Items hinzurechnen
  Gewicht:=Gewicht+CalculateGewicht(Item,Schuss);

  if (MaxGuertelKapazitat)<Gewicht then
  begin
    EquipError(daeGuertelUeberladen);
    exit;
  end;

  if not fMunGuertel[Index].IsSet then
  begin
    if not NeedTU(TUPutFromGuertel) then
      exit;
    if not ChangeKapazitat(CalculateGewicht(Item,Schuss)) then
      exit;

    fMunGuertel[Index].IsSet:=true;
    fMunGuertel[Index].ID:=Item.ID;
    fMunGuertel[Index].Schuesse:=Schuss;
    fMunGuertel[Index].Gewicht:=CalculateGewicht(Item,Schuss);

    DeleteFromUsed(Item.ID);
    result:=true;
  end
  else
    EquipError(daeGuertelNichtFrei);
end;

function TGameFigureManager.DeleteFromGuertel(Index: Integer): Boolean;
begin
  result:=false;

  if not NeedTU(TUTakeFromGuertel) then
    exit;

  with fMunGuertel[Index] do
  begin
    if IsSet then
    begin
      ChangeKapazitat(-Gewicht);
    end;
    AddToUsedList(ID,false);
    IsSet:=false;
  end;

  result:=true;
end;

function TGameFigureManager.GetGuertelItem(Index: Integer;
  out Item: TBackPackItem): Boolean;
begin
  result:=false;
  if (Index<0) or (Index>=GuertelSlots) then
    exit;
  if not fMunGuertel[Index].IsSet then
    exit;
  Item:=fMunGuertel[Index];
  result:=true;
end;

function TGameFigureManager.GetGuertelSlots: Integer;
begin
  result:=length(fMunGuertel);
end;

procedure TGameFigureManager.ChangeGuertelSize(NewSize: Integer);
var
  Dummy  : Integer;
  OldSize: Integer;
begin
  if NewSize>length(fMunGuertel) then
  begin
    OldSize:=length(fMunGuertel);
    SetLength(fMunGuertel,NewSize);
    for Dummy:=OldSize to NewSize-1 do
      fMunGuertel[Dummy].IsSet:=false;
  end
  else
  begin
    for Dummy:=NewSize to high(fMunGuertel) do
    begin
      if fMunGuertel[Dummy].IsSet then
      begin
        AddToRucksack(fMunGuertel[Dummy].ID,fMunGuertel[Dummy].Schuesse);
      end;
    end;
    SetLength(fMunGuertel,NewSize);
  end;
end;

function TGameFigureManager.AddToRucksack(ID: Cardinal;
  Schuss: Integer): Boolean;
begin
  Assert(ID<>0,'Ung�ltige ID zum hinzuf�gen beim Rucksack');
  result:=false;

  if length(fRucksack)>=RucksackSlots then
  begin
    EquipError(daeRucksackKeinPlatz);
    exit;
  end;

  if (not (fIsEquip=0)) or (not fRucksackUsed) then
  begin
    NeedTU(TUUseRucksack);
    fRucksackUsed:=true;
  end;

  if not NeedTU(TUPutRucksack) then
    exit;

  result:=true;
  SetLength(fRucksack,length(fRuckSack)+1);
  fRucksack[high(fRucksack)].IsSet:=true;
  fRucksack[high(fRucksack)].ID:=ID;
  fRucksack[high(fRucksack)].Schuesse:=Schuss;
  fRucksack[high(fRucksack)].Gewicht:=CalculateGewicht(lager_api_getItem(ID)^,Schuss);

  DeleteFromUsed(ID);

  CompactRucksack;
end;

procedure TGameFigureManager.BerechneExp(Points: Integer; Typ: TExpType);
begin
end;

destructor TGameFigureManager.Destroy;
begin
  fNotifyList.CallEvents(EVENT_MANAGER_ONDESTROY,Self);
  fNotifyList.Free;

  if fItemsAtBeginn<>nil then
    fItemsAtBeginn.Free;
  inherited;
end;

function TGameFigureManager.GetItemInSlot(Slot: TSoldatConfigSlot;
  SlotIndex: Integer; out Item: TLagerItem): Boolean;
var
  ID     : Cardinal;
  Waffe  : PSoldatWaffe;
  RItem  : TEnumRuckSackItemRecord;
  GItem  : TBackPackItem;
begin
  ID:=0;
  case Slot of
    scsLinkeHand,scsRechteHand:
    begin
      Waffe:=GetAddrOfSlot(Slot);
      ID:=Waffe.ID;
    end;
    scsLinkeHandMunition:
    begin
      if fLeftHand.Gesetzt and fLeftHand.MunGesetzt then
        ID:=fLeftHand.MunID;
    end;
    scsRechteHandMunition:
    begin
      if fRightHand.Gesetzt and fRightHand.MunGesetzt then
        ID:=fRightHand.MunID;
    end;
    scsPanzerung:
    begin
      if fPanzerung.Gesetzt then
        ID:=fPanzerung.ID;
    end;
    scsGuertel:
    begin
      if fGuertel.Gesetzt then
        ID:=fGuertel.ID;
    end;
    scsRucksack:
    begin
      RItem.Index:=SlotIndex;
      EnumRucksackItems(RItem);
      ID:=RItem.Item.ID;
    end;
    scsMunition:
    begin
      if GetGuertelItem(SlotIndex,GItem) then
        ID:=GItem.ID;
    end;
  end;

  if ID=0 then
    result:=false
  else
  begin
    result:=true;
    Item:=lager_api_GetItem(ID)^;
    if Slot=scsLinkeHandMunition then
      Item.Munition:=fLeftHand.Schuesse
    else if Slot=scsRechteHandMunition then
      Item.Munition:=fRightHand.Schuesse
    else if Slot=scsMunition then
      Item.Munition:=GItem.Schuesse
    else if Slot=scsRucksack then
      Item.Munition:=RItem.Item.Schuesse;
  end;
end;

{ Diese Funktion speichert den aktuellen Status der Einheit.
  Muss immer dann benutzt werden, wenn mehrere Aktionen hintereinander
  ausgef�hrt werden, die Zeiteinheiten kosten. Nach Abschluss aller
  Aktionen wird der Status der Einheit zur�ckgesetzt, falls mind. einaml
  nicht gen�gend ZEs zur Verf�gung standen }
procedure TGameFigureManager.BeginnEquip;
begin
  inc(fIsEquip);
  if fIsEquip>1 then
    exit;

  if fItemsAtBeginn<>nil then
    fItemsAtBeginn.Free;

  fItemsAtBeginn:=TMemoryStream.Create;
  SaveToStream(fItemsAtBeginn);
  fEquipError:=daeNone;

  // Zeiteinheiten f�rs zur�cksetzen speichern
  if (fOnBattleField) and (not fEchtzeit) then
  begin
    rec.Objekt:=Self;
    SendVMessage(vmGetUnitTimeUnits);
    fSavedTimeUnits:=rec.TimeUnits;
  end;

  fRucksackUsed:=false;
end;

function TGameFigureManager.EndEquip: TDragActionError;
begin
  result:=fEquipError;
  dec(fIsEquip);
  if fIsEquip>0 then
    exit;

  fEquipError:=daeNone;

  fIsEquip:=0;

  // Ausr�stung zur�cksetzen wenn nicht gen�gend ZEs bei einer Aktion
  // seit BeginnEquip zur Verf�gung stand
  if result<>daeNone then
  begin
    Assert(fItemsAtBeginn<>nil);
    fItemsAtBeginn.Position:=0;

    LoadFromStream(fItemsAtBeginn);

    fItemsAtBeginn.Free;
    fItemsAtBeginn:=nil;

    // Zeiteinheiten zur�cksetzen
    if (fOnBattleField) and (not fEchtzeit) then
    begin
      rec.TimeUnits:=fSavedTimeUnits;
      rec.Objekt:=Self;
      SendVMessage(vmSetUnitTimeUnits);
    end;

    NotifyList.CallEvents(EVENT_MANAGER_ONCHANGE,Self);
  end;

  fRucksackUsed:=false;
end;

procedure TGameFigureManager.SetItemToSlot(Slot: TSoldatConfigSlot;
  const Item: TLagerItem;Schuesse: Integer);
var
  SetWaffe   : PSoldatWaffe;
  Art        : TSchussType;
  tmpGewicht : double;
  OrgItem    : PLagerItem;
begin
  SetWaffe:=nil;

  DeleteFromUsed(Item.ID);

  OrgItem:=lager_api_GetItem(Item.ID);

  case Slot of
    scsLinkeHand,scsRechteHand:
    begin
      SetWaffe:=nil;
      case Slot of
        scsLinkeHand:  SetWaffe:=@fLeftHand;
        scsRechteHand: SetWaffe:=@fRightHand;
      end;

      Assert(SetWaffe<>nil);

      if SetWaffe.Gesetzt and (SetWaffe.Gewicht<>Item.Gewicht) then
      begin
        ChangeKapazitat(-SetWaffe.Gewicht);
        ChangeKapazitat(OrgItem.Gewicht);
      end;

      with OrgItem^ do
      begin
        SetWaffe.ID:=ID;
        SetWaffe.Name:=Name;
        SetWaffe.Image:=ImageIndex;
        SetWaffe.TypeID:=TypeID;
        SetWaffe.WaffenArt:=WaffType;
        SetWaffe.Gewicht:=Gewicht;
        SetWaffe.TimeUnits:=TimeUnits;
        SetWaffe.Genauigkeit:=Genauigkeit;
        SetWaffe.Ladezeit:=Laden;
        if SetWaffe.TypeID=ptWaffe then
          SetWaffe.ZweiHand:=not Einhand
        else
          SetWaffe.ZweiHand:=false;

        if Strength<>0 then
        begin
          SetWaffe.Power:=Power;
          SetWaffe.Strength:=Strength;
        end;

        if (SetWaffe.TypeID=ptWaffe) and (SetWaffe.WaffenArt=wtLaser) then
        begin
          SetWaffe.SchussMax:=Munition;
          SetWaffe.Schuesse:=Item.Munition;
        end;

        // Schussart setzen, wenn die aktuell ausgew�hlte Schussart bei der
        // Waffe nicht existiert
        SetWaffe.SchussArt:=stSpontan;
        if SetWaffe.TypeID=ptWaffe then
        begin
          if not (SetWaffe.SchussArt in Schuss) then
          begin
            for Art:=low(TSchussType) to high(TSchussType) do
            begin
              if (Art in Schuss) and (Art<>stNichtSchiessen) then
              begin
                SetWaffe.SchussArt:=Art;
                break;
              end;
            end;
          end;
        end;
      end;
    end;
    scsLinkeHandMunition,scsRechteHandMunition:
    begin
      case Slot of
        scsLinkeHandMunition:   SetWaffe:=@fLeftHand;
        scsRechteHandMunition:  SetWaffe:=@fRightHand;
      end;

      tmpGewicht:=CalculateGewicht(OrgItem^,Schuesse);

      if SetWaffe.MunGesetzt and (SetWaffe.MunGewicht<>tmpGewicht) then
      begin
        ChangeKapazitat(-SetWaffe.MunGewicht);
        ChangeKapazitat(tmpGewicht);
      end;

      with OrgItem^ do
      begin
        SetWaffe.MunImage:=ImageIndex;
        SetWaffe.MunID:=ID;
        SetWaffe.MunGewicht:=tmpGewicht;
        if Schuesse=-1 then
          SetWaffe.Schuesse:=OrgItem.Munition
        else
          SetWaffe.Schuesse:=Schuesse;

        SetWaffe.SchussMax:=Munition;

        if Strength<>0 then
        begin
          SetWaffe.Power:=Power;
          SetWaffe.Strength:=Strength;
        end;
      end;
    end;
    scsGuertel:
    begin
      if fGuertel.Gesetzt and (fGuertel.Gewicht<>Item.Gewicht) then
      begin
        ChangeKapazitat(-fGuertel.Gewicht);
        ChangeKapazitat(OrgItem.Gewicht);
      end;

      with OrgItem^ do
      begin
        fGuertel.ID:=ID;
        fGuertel.Name:=Name;
        fGuertel.Gewicht:=Gewicht;
        fGuertel.Slots:=Strength;
      end;
      ChangeGuertelSize(fGuertel.Slots);
    end;
    scsPanzerung:
    begin
      if fPanzerung.Gesetzt and (fPanzerung.Gewicht<>Item.Gewicht) then
      begin
        ChangeKapazitat(-fPanzerung.Gewicht);
        ChangeKapazitat(OrgItem.Gewicht);
      end;

      with OrgItem^ do
      begin
        fPanzerung.ID:=ID;
        fPanzerung.Name:=Name;
        fPanzerung.Image:=ImageIndex;
        fPanzerung.Gewicht:=Gewicht;
      end;
    end;
  end;
end;

procedure TGameFigureManager.Dead;
var
  Slot  : TSoldatConfigSlot;
  Item  : TLagerItem;
  Dummy : Integer;
begin
  fIsDead:=true;

  if not OnBattleField then
    exit;

  Rec.Text:='HumanDie';
  SendVMessage(vmPlaySound);
  // Ausr�stung fallen lassen
  Rec.Point:=Point(fBattleFieldPos.X,fBattleFieldPos.Y);


  // TGameFigure des Manages in Rec.Figure ablegen
  Rec.Manager:=Self;
  SendVMessage(vmGetFigureOfManager);

  for Slot:=scsLinkeHand to scsGuertel do
  begin
    if GetItemInSlot(Slot,0,Item) then
    begin
      Rec.Item:=@Item;
      SendVMessage(vmCreateItemObject);
    end;
  end;

  // Ausr�stung im Rucksack fallen lassen
  for Dummy:=0 to RucksackSlotsUsed-1 do
  begin
    if GetItemInSlot(scsRucksack,Dummy,Item) then
    begin
      Rec.Item:=@Item;
      SendVMessage(vmCreateItemObject);
    end;
  end;

  // Ausr�stung im G�rtel fallen lassen
  for Dummy:=0 to GuertelSlots-1 do
  begin
    if GetItemInSlot(scsMunition,Dummy,Item) then
    begin
      Rec.Item:=@Item;
      SendVMessage(vmCreateItemObject);
    end;
  end;
end;

procedure TGameFigureManager.PlayWalkSound(Ground: String);
begin
  if fWalkLeft then
    Rec.Text:='WalkLeft'
  else
    Rec.Text:='WalkRight';

  SendVMessage(vmPlaySound);
  
  fWalkLeft:=not fWalkLeft;
end;

function TGameFigureManager.ZEKapazitat: Integer;
begin
  result:=GetUnitZeiteinheiten;
  if MaxKapazitat>0 then
    result:=round(result*(1-Kapazitat/(MaxKapazitat*1.5)));
end;

function TGameFigureManager.CalculateGewicht(ID: Cardinal; Schuesse: Integer): double;
var
  Item : TLagerItem;
begin
  Item:=lager_api_GetItem(ID)^;
  result:=CalculateGewicht(Item,Schuesse);
end;

function TGameFigureManager.CalculateGewicht(const Item: TLagerItem; Schuesse: Integer): double;
begin
  // Gewicht der Munition berechnen
  if Item.TypeID=ptMunition then
    result:=GetMunitionGewicht(Item.Gewicht,Item.Munition,Schuesse)
  else
    result:=Item.Gewicht;
end;

procedure TGameFigureManager.EquipError(Error: TDragActionError);
begin
  if fEquipError=daeNone then
    fEquipError:=Error;
end;

procedure TGameFigureManager.SetOnBattleField(const Value: boolean);
begin
  fOnBattleField := Value;
  // Einheit ist ins Raumschiff zur�ckgekehrt -> Laserwaffen aufladen
  if (not fOnBattleField) then
    LaserWaffenaufladen;
end;

function TGameFigureManager.CanUseItem(const Item: TLagerItem): TDragActionError;
begin
  result:=daeNone;
end;

procedure TGameFigureManager.FindItem(const Item: TLagerItem);
begin
end;

function TGameFigureManager.CheckAction(Manager: TGameFigureManager; const FromSlot,
  ToSlot: TItemSlot): TDragActionError;
var
  Item2      : TLagerItem;
  Item       : TLagerItem;
  Basis      : TBasis;
  RSchiff    : TRaumschiff;
  SetWaffe   : PSoldatWaffe;
  LagerV     : Integer;

  function ErmittleLagerV(const Slot: TItemSlot): Integer;
  var
    Item     : TLagerItem;
    Dummy    : Integer;
  begin
    if Slot.ID<>0 then
    begin
      Item:=lager_api_GetItem(Slot.ID)^;
      result:=round(Item.LagerV*10);
    end
    else
      result:=0;

    // Pr�fen, ob �berhaupt Waffe mit Munition �bertragen wird
    if (not (FromSlot.Slot in [scsLinkeHand,scsRechteHand])) and (not (ToSlot.Slot in [scsLinkeHand,scsRechteHand])) then
      exit;

    if (Item.TypeID=ptWaffe) and (Slot.Slot in [scsLinkeHand,scsRechteHand]) then
    begin
      SetWaffe:=Manager.GetAddrOfSlot(Slot.Slot);
      if SetWaffe.MunGesetzt then
      begin
        Item:=lager_api_GetItem(SetWaffe.MunID)^;
        inc(result,round(Item.LagerV*10));
      end;
    end
    else if (Item.TypeID=ptWaffe) and (Slot.Slot=scsSchiffItem) then
    begin
      for Dummy:=0 to RSchiff.ItemCount-1 do
      begin
        if RSchiff.Items[Dummy].Anzahl>0 then
        begin
          Item:=lager_api_GetItem(RSchiff.Items[Dummy].ID)^;
          if (Item.TypeID=ptMunition) and (Item.MunFor=Slot.ID) then
          begin
            inc(result,round(Item.LagerV*10));
            exit;
          end;
        end;
      end;
    end
    else if (Item.TypeID=ptWaffe) and (Slot.Slot=scsBasisItem) then
    begin
      Assert(Basis<>nil);
      for Dummy:=0 to lager_api_Count-1 do
      begin
        if lager_api_GetItemCountInBase(Basis.ID,Dummy)>0 then
        begin
          Item:=lager_api_GetItem(Dummy)^;
          if (Item.TypeID=ptMunition) and (Item.MunFor=Slot.ID) then
          begin
            inc(result,round(Item.LagerV*10));
            exit;
          end;
        end;
      end;
    end;
  end;

begin
  result:=daeNone;
  Item:=lager_api_GetItem(FromSlot.ID)^;

  { Lagerverbrauch in der Basis pr�fen }
  if (ToSlot.Slot=scsBasisItem) xor (FromSlot.Slot=scsBasisItem) then
  begin
    Basis:=GetHomeBasis;

    if Basis<>nil then
    begin
      LagerV:=ErmittleLagerV(FromSlot);
      if ToSlot.Slot in [scsLinkeHand,scsRechteHand] then
        LagerV:=ErmittleLagerV(ToSlot) - LagerV;

      if round((Basis.LagerRaum-Basis.LagerBelegt)*10) < LagerV then
      begin
        result:=daeBasisZuwenigLagerRaum;
        exit;
      end;
    end;
  end;

  { Lagerverbrauch im Raumschiff pr�fen }
  if (ToSlot.Slot=scsSchiffItem) xor (FromSlot.Slot=scsSchiffItem) then
  begin
    if ToSlot.Slot=scsSchiffItem then
      RSchiff:=TRaumschiff(ToSlot.ZusData)
    else
      RSchiff:=TRaumschiff(FromSlot.ZusData);

    if RSchiff<>nil then
    begin
      LagerV:=ErmittleLagerV(FromSlot);
      if ToSlot.Slot in [scsLinkeHand,scsRechteHand] then
        LagerV:=ErmittleLagerV(ToSlot) - LagerV;

      // LagerRoomBelegt gibt den Lagerraum schon mit 10 multipliziert zur�ck
      if round((RSchiff.Model.LagerPlatz*10)-RSchiff.LagerRoomBelegt) < LagerV then
      begin
        result:=daeSchiffZuwenigLagerRaum;
        exit;
      end;
    end;
  end;

  { Pr�fen, ob benutzte Munition zur Basis oder ins Raumschiff gehen soll }
  if (Item.TypeID=ptMunition) and (Item.WaffType<>wtLaser) and ((FromSlot.Schuesse<>-1) and (FromSlot.Schuesse<Item.Munition)) then
  begin
    // F�llt weg, da Munition durch ShootSurPlus auch anteilig in die Basis gepackt werden kann 
{    if ToSlot.Slot=scsBasisItem then
    begin
      result:=daeBenMunNichtInBasis;
      exit;
    end}
    if ToSlot.Slot=scsSchiffItem then
    begin
      result:=daeBenMunNichtInSchiff;
      exit;
    end;
  end;

  // Pr�fen, ob Munition der Waffe bereits angebrochen ist
  if (FromSlot.Slot in [scsLinkeHand,scsRechteHand]) and (ToSlot.Slot in [scsSchiffItem,scsBasisItem]) then
  begin
    SetWaffe:=Manager.GetAddrOfSlot(FromSlot.Slot);
    if SetWaffe.MunGesetzt then
    begin
      if Item.WaffType<>wtLaser then
      begin
        Item2:=lager_api_GetItem(SetWaffe.MunID)^;
        if (SetWaffe.Schuesse<Item2.Munition) then
        begin
          if ToSlot.Slot=scsSchiffItem then
          begin
            result:=daeBenMunNichtInSchiff;
            exit;
          end;
        end;
      end;
    end;
  end;

  // Waffe austauschen aus Raumschiff/Basis -> Pr�fen der Munition
  if (ToSlot.Slot in [scsLinkeHand,scsRechteHand]) and (FromSlot.Slot in [scsSchiffItem,scsBasisItem]) then
  begin
    SetWaffe:=Manager.GetAddrOfSlot(ToSlot.Slot);
    if SetWaffe.MunGesetzt then
    begin
      if SetWaffe.WaffenArt<>wtLaser then
      begin
        Item2:=lager_api_GetItem(SetWaffe.MunID)^;
        if (SetWaffe.Schuesse<Item2.Munition) then
        begin
          if ToSlot.Slot=scsBasisItem then
            result:=daeBenMunNichtInBasis
          else
            result:=daeBenMunNichtInSchiff;
          exit;
        end;
      end;
    end;
  end;

  // Munition austauschen aus Raumschiff/Basis -> Pr�fen der Munition
  if (ToSlot.Slot in [scsLinkeHandMunition,scsRechteHandMunition]) and (FromSlot.Slot in [scsSchiffItem]) then
  begin
    if ToSlot.Slot=scsLinkeHandMunition then
      SetWaffe:=Manager.GetAddrOfSlot(scsLinkeHand)
    else
      SetWaffe:=Manager.GetAddrOfSlot(scsRechteHand);
    if SetWaffe.MunGesetzt then
    begin
      if SetWaffe.WaffenArt<>wtLaser then
      begin
        Item2:=lager_api_GetItem(SetWaffe.MunID)^;
        if (SetWaffe.Schuesse<Item2.Munition) then
        begin
          result:=daeBenMunNichtInSchiff;
          exit;
        end;
      end;
    end;
  end;
end;

function TGameFigureManager.GetHomeBasis: TBasis;
begin
  result:=nil;
end;

function TGameFigureManager.GetRaumschiff: TRaumschiff;
begin
  result:=nil;
end;

procedure TGameFigureManager.CompactRucksack;
var
  Dummy  : Integer;
  Item   : PLagerItem;

  procedure Compact(Index: Integer; Item: PLagerItem);
  var
    Dummy     : Integer;
    MuniCount : Integer;
    Count     : Integer;
  begin
    // Anzahl der Sch�sse die Verteilt werden m�ssen
    MuniCount:=fRucksack[Index].Schuesse;
    // Suche nach einer zweiten unvollst�ndigen Munition vom gleichem Typ
    for Dummy:=0 to high(fRucksack) do
    begin
      if (Dummy<>Index) then
      begin
        if (fRucksack[Index].ID=fRucksack[Dummy].ID) and (Item.Munition<>fRucksack[Dummy].Schuesse) and (fRucksack[Dummy].Schuesse<>-1) then
        begin
          // Munition zusammenfassen
          Count:=Item.Munition-fRucksack[Dummy].Schuesse;
          Count:=min(MuniCount,Count);

          // Gewicht anpassen. Auf grund von Rundungen ist es nicht empfehlenswert
          // einfach darauf zu vertrauen, dass Gewicht beim alten Slot abzuziehen
          // und beim neuen hinzuzuaddieren
          ChangeKapazitat(-fRucksack[Dummy].Gewicht);
          ChangeKapazitat(-fRucksack[Index].Gewicht);

          fRucksack[Dummy].Schuesse:=fRucksack[Dummy].Schuesse+Count;
          fRucksack[Dummy].Gewicht:=CalculateGewicht(Item^,fRucksack[Dummy].Schuesse);
          ChangeKapazitat(fRucksack[Dummy].Gewicht);

          MuniCount:=MuniCount-Count;
          if MuniCount=0 then
            break;

          fRucksack[Index].Schuesse:=MuniCount;
          fRucksack[Index].Gewicht:=CalculateGewicht(Item^,fRucksack[Index].Schuesse);
          ChangeKapazitat(fRucksack[Index].Gewicht);

        end;
      end;
    end;

    // Munition wurde komplett verbraucht
    if MuniCount=0 then
      DeleteArray(Addr(fRucksack),TypeInfo(TBackPackArray),Index);
  end;

begin
  for Dummy:=high(fRucksack) downto 0 do
  begin
    if fRucksack[Dummy].IsSet then
    begin
      Item:=lager_api_GetItem(fRucksack[Dummy].ID);

      // Unvollst�ndige Munition
      if (Item.TypeID=ptMunition) and (Item.Munition<>fRucksack[Dummy].Schuesse) and (fRucksack[Dummy].Schuesse<>-1) then
      begin
        Compact(Dummy, Item);
      end;
    end;
  end;
end;

procedure TGameFigureManager.AddToUsedList(ID: Cardinal; Used: Boolean);
begin
  SetLength(fItemsUsedList,length(fItemsUsedList)+1);

  fItemsUsedList[high(fItemsUsedList)].ID:=ID;
  fItemsUsedList[high(fItemsUsedList)].Used:=Used;
end;

procedure TGameFigureManager.DeleteFromUsed(ID: Cardinal);
var
  Used : Boolean;
  Dummy: Integer;
begin
  // Erstmal wird versucht eine nicht benutzte Ausr�stung zu entfernen
  for Used:=false to true do
  begin
    for Dummy:=high(fItemsUsedList) downto 0 do
    begin
      if (fItemsUsedList[Dummy].ID=ID) and (fItemsUsedList[Dummy].Used=Used) then
      begin
        DeleteArray(Addr(fItemsUsedList),TypeInfo(TChangedItemsArray),Dummy);
        exit;
      end;
    end;
  end;
end;

procedure TGameFigureManager.Reequip;
var
  Dummy   : Integer;
  Dummy2  : Integer;
  Mess    : String;
  Needed  : Array of record
              ID          : Cardinal;
              NeededCount : Integer;
            end;

  procedure AddToNeeded(ID: Cardinal);
  var
    Dummy: Integer;
  begin
    for Dummy:=0 to high(Needed) do
    begin
      if Needed[Dummy].ID=ID then
      begin
        inc(Needed[Dummy].NeededCount);
        exit;
      end;
    end;

    // Nicht gefunden => neu eintragen
    SetLength(Needed,length(Needed)+1);
    Needed[high(Needed)].ID:=ID;
    Needed[high(Needed)].NeededCount:=1;
  end;

  procedure FillMunitionSlot(Slot: TSoldatConfigSlot);
  var
    Waffe: PSoldatWaffe;
    Item : PLagerItem;
  begin
    Waffe:=GetAddrOfSlot(Slot);
    if Waffe.Gesetzt and Waffe.MunGesetzt then
    begin
      Item:=lager_api_GetItem(Waffe.MunID);
      if (Waffe.Schuesse<Item.Munition) and (Waffe.Schuesse<>-1) then
      begin
        if lager_api_PutMunition(HomeBase.ID,Waffe.MunID,Waffe.Schuesse-Item.Munition) then
        begin
          Waffe.Schuesse:=Item.Munition;
          Waffe.MunGewicht:=Item.Gewicht;
        end
        else
          AddToNeeded(Waffe.MunID);
      end;
    end;
  end;

begin
  if not InBasis then
    exit;

  SetLength(Needed,0);

  Assert(HomeBase<>nil);

  // Tempor�r abgelegte Ausr�stung in die Basis packen
  for Dummy:=high(fTemporaryItems) downto 0 do
  begin
    for Dummy2:=high(fRucksack) downto 0 do
    begin
      if fRucksack[Dummy2].ID=fTemporaryItems[Dummy].ID then
      begin
        if lager_api_PutMunition(HomeBase.ID,fRucksack[Dummy2].ID,fRucksack[Dummy2].Schuesse) then
        begin
          DeleteFromRucksack(Dummy2);
          DeleteArray(Addr(fTemporaryItems),TypeInfo(TChangedItemsArray),Dummy);
          break;
        end;
      end;
    end;
  end;

  // Benutzte Ausr�stung nachladen
  for Dummy:=high(fItemsUsedList) downto 0 do
  begin
    if fItemsUsedList[Dummy].Used then
    begin
      // Nur wenn es benutzt wurde nachf�llen
      if lager_api_DeleteItem(HomeBase.ID,fItemsUsedList[Dummy].ID) then
      begin
        // Ausr�stung zum Nachf�llen befindet sich in der Basis
        if not AddToGuertel(lager_api_GetItem(fItemsUsedList[Dummy].ID)^) then
        begin
          if not AddToRucksack(lager_api_GetItem(fItemsUsedList[Dummy].ID)^) then
          begin
            AddToNeeded(fItemsUsedList[Dummy].ID);
            // Wurde schon aus der Basis gel�scht => deshalb wieder auff�llen
            lager_api_PutItem(HomeBase.ID,fItemsUsedList[Dummy].ID);
          end;
        end;
        // Zur�cksetzen, da Fehler schon abgefangen sind
        fEquipError:=daeNone;
      end
      else
        AddToNeeded(fItemsUsedList[Dummy].ID);
    end;
  end;

  // Nachr�stliste l�schen
  SetLength(fItemsUsedList,0);
  SetLength(fTemporaryItems,0);

  FillMunitionSlot(scsLinkeHand);
  FillMunitionSlot(scsRechteHand);

  if length(Needed)>0 then
  begin
    Mess:=Format(ST0410220001+#13#10,[Name]);

    for Dummy:=0 to high(Needed) do
    begin
      with Needed[Dummy]do
      begin
        Mess:=Mess+#13#10;
        if NeededCount>1 then
          Mess:=Mess+Format('(%dx) ',[NeededCount]);

        Mess:=Mess+lager_api_GetItem(ID).Name;
      end;
    end;
    savegame_api_Message(Mess,lmReserved,Self);
  end;
end;

function TGameFigureManager.GetBackPack(Slot: TSoldatConfigSlot): TBackPackArray;
begin
  case Slot of
    scsMunition: Result:=fMunGuertel;
    scsRucksack: Result:=fRucksack;
    else
      Assert(false);
  end;
end;

function TGameFigureManager.GetMaxGuertelKapazitat: double;
begin
  result:=Strength/10;
end;

function TGameFigureManager.GetGuertelKapazitat: double;
var
  Dummy: Integer;
begin
  result:=0;
  for Dummy:=0 to GuertelSlots-1 do
  begin
    if fMunGuertel[Dummy].IsSet then
    begin
      result:=result+CalculateGewicht(fMunGuertel[Dummy].ID,fMunGuertel[Dummy].Schuesse);
    end;
  end;
end;

function TGameFigureManager.CalcTreffChance(Slot: TSoldatConfigSlot; ShootInfos: TShootInfos): Integer;
var
  SlotPtr: PSoldatWaffe;
  Faktor : double;
  Target : TGameFigure;
  Region : TBodyRegion;
  Modell : TModelInformation;
begin
  if ShootInfos.Range=0 then
  begin
    result:=100;
    exit;
  end;

  SlotPtr:=GetAddrOfSlot(Slot);

  // Infos f�r den Schuss berechnen
  // Treffsicherheit ermitteln (-15% wenn Waffe nicht in der Waffenhand liegt
  if (LeftHand and (Slot=scsRechteHand)) or ((not LeftHand) and (Slot=scsLinkeHand)) then
    result:=TreffSicherheit-15
  else
    result:=TreffSicherheit;

  // Schussart bei Treffsicherheit ber�cksichtigen
  result:=round(result*(TreffModifikator[SlotPtr.SchussArt]/100));

  if (ShootInfos.TargetUnit<>nil) and (ShootInfos.TargetHeight<>-1) then
  begin
    Target:=TGameFigure(ShootInfos.TargetUnit);
    Assert((Target is TGameFigure) and (Target.Manager<>nil));
    Region:=Target.GetBodyRegion(ShootInfos.TargetHeight);
    Modell:=ModelInfos[Target.Manager.FigureMap];
    case Region of
      brHead   : Faktor:=(Modell.HeadSize*Modell.HeadHeight)/(Modell.torsoSize*Modell.torsoHeight);
      brTorso  : Faktor:=(Modell.torsoSize*Modell.TorsoHeight)/(Modell.torsoSize*Modell.TorsoHeight);
      brFoot   : Faktor:=(Modell.FootSize*Modell.FootHeight)/(Modell.torsoSize*Modell.TorsoHeight);
      else
        Faktor:=1;
    end;
  end
  else
    Faktor:=1;

  // Treffsicherheit mit der Genauigkeit der Waffe verrechnen
  {$IFDEF EXTERNALFORMULARS}
  result:=round(formular_utils_SolveFormular('CalcTreff',['UnitTreff','Range','BaseRange','Faktor'],[result,ShootInfos.Range,SlotPtr.Genauigkeit,Faktor]));
  {$ELSE}
  result:=round(result*(1/(ShootInfos.Range*ShootInfos.Range))*(SlotPtr.Genauigkeit*SlotPtr.Genauigkeit)*Faktor);
  {$ENDIF}
  result:=MinMax(result,0,100);
end;

{ TSoldatManager }

constructor TSoldatManager.Create(FigureRef: Pointer);
{$IFDEF AUTOEQUIPSOLDIERS}
var
  Item   : PLagerItem;
  Dummy  : Integer;
  WaffID : Cardinal;
{$ENDIF}
begin
  inherited;
  fSoldatInfo:=PSoldatInfo(FigureRef);
  fGesundheit:=trunc(fSoldatInfo.Ges);

  {$IFDEF AUTOEQUIPSOLDIERS}
  // Waffe f�r den Soldaten
  Item:=lager_api_GetRandomItem([ptPanzerung],trunc(fSoldatInfo.IQ));
  if Item<>nil then
  begin
    SetzeAusruestung(scsPanzerung,Item.ID);
  end;

  Item:=lager_api_GetRandomItem([ptWaffe],trunc(fSoldatInfo.IQ));
  if Item<>nil then
  begin
    SetzeAusruestung(scsLinkeHand,Item.ID);

    WaffID:=Item.ID;
    Item:=lager_api_GetRandomMunition(WaffID,trunc(fSoldatInfo.IQ));
    if Item<>nil then
    begin
      SetzeAusruestung(scsLinkeHandMunition,Item.ID);
      fLeftHand.SchussArt:=stGezielt;
    end;

    Item:=lager_api_GetRandomItem([ptGuertel],trunc(fSoldatInfo.IQ));
    if Item<>nil then
    begin
      SetzeAusruestung(scsGuertel,Item.ID);
      // Zus�tzliche Munition im Rucksack
      for Dummy:=1 to min(GuertelSlots,random(4)) do
      begin
        Item:=lager_api_GetRandomMunition(WaffID,trunc(fSoldatInfo.IQ));
        if Item<>nil then
          AddToGuertel(Item^);
      end;
    end;

  end;

  for Dummy:=0 to random(6) do
  begin
    Item:=lager_api_GetRandomItem([ptGranate,ptMine,ptSensor],trunc(fSoldatInfo.IQ));
    if Item<>nil then
      AddToRucksack(Item^);
  end;
  {$ENDIF}
end;

procedure TSoldatManager.DrawGesicht(Surface: TDirectDrawSurface;
  const Mem: TDDSurfaceDesc; Left, Top: Integer; Ges: boolean);
begin
  if Assigned(fDrawGesicht) then
    fDrawGesicht(fSoldatInfo^,Surface,Mem,Left,Top,Ges);
end;

procedure TSoldatManager.DrawSmallGesicht(Surface: TDirectDrawSurface;
  const Mem: TDDSurfaceDesc; Left, Top: Integer;Zeit: Integer; Ges: boolean);
begin
  if Assigned(fDrawSmallGesicht) then
    fDrawSmallGesicht(fSoldatInfo^,Surface,Mem,Left,Top,Zeit,Ges,MaxZeiteinheiten);
end;

function TSoldatManager.GetMaxKapazitat: double;
begin
  result:=trunc(fSoldatInfo.Kraft)*1.5;
end;

function TSoldatManager.GetLeftHand: boolean;
begin
  result:=fSoldatInfo.LeftHand;
end;

function TSoldatManager.GetPanzer: Integer;
begin
  if fPanzerung.Gesetzt then
    result:=lager_api_GetItem(fPanzerung.ID).Panzerung
  else
    result:=0;
end;

function TSoldatManager.GetSoldatName: String;
begin
  result:=fSoldatInfo.Name;
end;

procedure TSoldatManager.ZaehleEinsatz;
begin
  inc(fSoldatInfo.Einsatz);
end;

function TSoldatManager.GetMaxGesundheit: Integer;
begin
  result:=trunc(fSoldatInfo.MaxGes);
end;

procedure TSoldatManager.ChangeGesundheit(Gesundheit: Integer);
begin
  inherited;
  if fGesundheit=MaxGesundheit then
    fSoldatInfo.Ges:=fSoldatInfo.MaxGes
  else
    fSoldatInfo.Ges:=fGesundheit;
end;

function TSoldatManager.GetTreffSicherheit: Integer;
begin
  result:=trunc(fSoldatInfo.Treff);
end;

procedure TSoldatManager.ZaehleAbschuss;
begin
  inc(fSoldatInfo.Treffer);
end;

function TSoldatManager.GetSoldatInfo: PSoldatInfo;
begin
  result:=fSoldatInfo;
end;

procedure TSoldatManager.SetRefPointer(FigureRef: Pointer);
begin
  fSoldatInfo:=PSoldatInfo(FigureRef);
end;

procedure TSoldatManager.BeginnEinsatz;
begin
  inherited;
  fGesundheit:=trunc(fSoldatInfo.Ges);
end;

function TSoldatManager.GetEP: Integer;
begin
  result:=0;
end;

procedure TSoldatManager.BerechneExp(Points: Integer; Typ: TExpType);
const
  Werte: Array[TExpType,0..8] of double =  {Treff, Kraft, Ges  , Zeit , Sicht, PSIAn, PSIAb, IQ   , React}
                           {etTreffer}    ((  0.7,   0.4,   0.4,   0.1,   0.1,   0.1,   0.1,   0.2,   0.2),
                           {etEntdeckt}    ( 0.05,  0.05,   0.2,   0.2,   0.9,  0.05,  0.05,   0.2,   0.5),
                           {etLaufen}      (  0.1,   0.8,   0.4,   0.8,   0.3,   0.1,   0.1,   0.2,   0.2),
                           {etSchiessen}   (  0.8,   0.4,   0.1,   0.4,   0.4,   0.1,   0.1,   0.2,   0.2),
                           {etWerfen}      (  0.3,   0.7,   0.3,   0.7,   0.4,   0.1,   0.1,   0.2,   0.2));

var
  FullGes : Boolean;

  procedure CalculateWert(var Wert: single; Talent: Integer; Index: Integer);
  begin
    Wert:=min(100,Wert+(Talent*3)*(Points*Werte[Typ,Index]/10)/(Wert*100));
  end;

begin
  {$IFDEF DEBUGMODE}
  GlobalFile.Write('EP - '+Name+'('+GetEnumName(TypeInfo(TExpType),Ord(Typ))+')',Points);
  {$ENDIF}

  FullGes:=trunc(fSoldatInfo.MaxGes)=fGesundheit;

  with fSoldatInfo^ do
  begin
    CalculateWert(Treff,FaehigTreff,0);
    CalculateWert(Kraft,FaehigKraft,1);
    CalculateWert(MaxGes,FaehigGes,2);
    CalculateWert(Zeit,FaehigZeit,3);
    CalculateWert(Sicht,FaehigSicht,4);
    CalculateWert(PSIAn,FaehigPSIAn,5);
    CalculateWert(PSIAb,FaehigPSIAb,6);
    CalculateWert(IQ,FaehigIQ,7);
    CalculateWert(React,FaehigReact,8);

    if FullGes then
      Ges:=MaxGes;

    fGesundheit:=trunc(Ges);
    ChangeGesundheit(0);
  end;

  inc(fSoldatInfo.EP,Points);
end;

function TSoldatManager.GetSichtWeite: Integer;
begin
//  result:=round((trunc(fSoldatInfo.Sicht)/100)*30);
  result:=15+(trunc(fSoldatInfo.Sicht) div 10);
end;

function TSoldatManager.GetStrength: Integer;
begin
  result:=trunc(fSoldatInfo.Kraft);
end;

function TSoldatManager.GetUnitZeiteinheiten: Integer;
begin
  result:=trunc(fSoldatInfo.Zeit);
end;

function TSoldatManager.CanUseItem(const Item: TLagerItem): TDragActionError;
begin
  result:=daeNone;
  if not Item.Useable then
  begin
    result:=daeNichtBenutztbar;
    exit;
  end;
  if Item.NeedIQ>fSoldatInfo.IQ then
  begin
    result:=daeNichtgenugIntelligenz;
    exit;
  end;
end;

procedure TSoldatManager.FindItem(const Item: TLagerItem);
begin
  inherited;
  lager_api_RegisterAlienItem(Item);
end;

function TSoldatManager.GetReaktion: Integer;
begin
  result:=trunc(fSoldatInfo.React);
end;

function TSoldatManager.GetHomeBasis: TBasis;
begin
  result:=basis_api_GetBasisFromID(SoldatInfo.BasisID);
end;

function TSoldatManager.GetRaumschiff: TRaumschiff;
begin
  result:=soldaten_api_GetRaumschiff(fSoldatInfo^);
end;

{ TAlienManager }

function TAlienManager.CanUseItem(const Item: TLagerItem): TDragActionError;
begin
  result:=daeNone;
  
  if (Item.NeedIQ>fAlien.IQ) then
  begin
    result:=daeNichtgenugIntelligenz;
    exit;
  end;
end;

procedure TAlienManager.ChangeGesundheit(Gesundheit: Integer);
begin
  inherited;
  fAlien.Ges:=fGesundheit;
end;

constructor TAlienManager.Create(FigureRef: Pointer);
var
  Item   : PLagerItem;
  Dummy  : Integer;
  WaffID : Cardinal;
begin
  inherited;
  fAlien:=FigureRef;
  fGesundheit:=fAlien.Ges;

  // Waffe des Aliens
  Item:=lager_api_GetRandomItem([ptWaffe],fAlien.IQ,true);
  if Item<>nil then
  begin
    SetzeAusruestung(scsLinkeHand,Item.ID);

    WaffID:=Item.ID;
    Item:=lager_api_GetRandomMunition(WaffID,fAlien.IQ,true);
    if Item<>nil then
    begin
      SetzeAusruestung(scsLinkeHandMunition,Item.ID);
      fLeftHand.SchussArt:=stGezielt;
    end;

    Item:=lager_api_GetRandomItem([ptGuertel],fAlien.IQ,true);
    if Item<>nil then
    begin
      SetzeAusruestung(scsGuertel,Item.ID);
      // Zus�tzliche Munition im Rucksack
      for Dummy:=1 to min(GuertelSlots,random(4)) do
      begin
        Item:=lager_api_GetRandomMunition(WaffID,fAlien.IQ,true);
        if Item<>nil then
          AddToGuertel(Item^);
      end;
    end;

    for Dummy:=0 to random(6) do
    begin
      Item:=lager_api_GetRandomItem([ptGranate,ptMine,ptSensor],fAlien.IQ,true);
      if Item<>nil then
        AddToRucksack(Item^);
    end;
  end;
end;

procedure TAlienManager.DrawGesicht(Surface: TDirectDrawSurface;
  const Mem: TDDSurfaceDesc; Left, Top: Integer; Ges: boolean);
begin

end;

procedure TAlienManager.DrawSmallGesicht(Surface: TDirectDrawSurface;
  const Mem: TDDSurfaceDesc; Left, Top, Zeit: Integer; Ges: boolean);
begin

end;

function TAlienManager.GetEP: Integer;
begin
  result:=fAlien.EP;
end;

function TAlienManager.GetLeftHand: boolean;
begin
  result:=true;
end;

function TAlienManager.GetMaxGesundheit: Integer;
begin
  result:=alien_api_GetAlienInfo(fAlien.ID).Ges;
end;

function TAlienManager.GetMaxKapazitat: double;
begin
  result:=Strength*1.5;
end;

function TAlienManager.GetPanzer: Integer;
begin
  result:=fAlien.Pan;
end;

function TAlienManager.GetReaktion: Integer;
begin
  result:=fAlien.Reaktion;
end;

function TAlienManager.GetSichtWeite: Integer;
begin
  result:=fAlien.Sicht;
end;

function TAlienManager.GetSoldatName: String;
begin
  result:=fAlien.Name;
end;

function TAlienManager.GetStrength: Integer;
begin
  result:=50;
end;

function TAlienManager.GetTreffSicherheit: Integer;
begin
  result:=fAlien.Treff;
end;

function TAlienManager.GetUnitZeiteinheiten: Integer;
begin
  result:=fAlien.Zei;
end;

procedure TAlienManager.SetRefPointer(FigureRef: Pointer);
begin
  fAlien:=FigureRef;
end;

var
  Temp: TExtRecordDefinition;

initialization

  TGameFigureManagerRecord:=TExtRecordDefinition.Create('TGameFigureManagerRecord');

  TGameFigureManagerRecord.AddDouble('Kapazitaet',0,10000,10);
  TGameFigureManagerRecord.AddRecordList('Waffen',RecordSoldatWaffe);
  TGameFigureManagerRecord.AddRecordList('Panzerung',RecordSoldatPanzerung);
  TGameFigureManagerRecord.AddRecordList('Rucksack',RecordBackPackItem);
  TGameFigureManagerRecord.AddRecordList('Guertel',RecordSoldatGuertel);
  TGameFigureManagerRecord.AddRecordList('GuertelSlots',RecordBackPackItem);

  // Record f�r TGameFigureManager.fItemsUsedList
  Temp:=TExtRecordDefinition.Create('UsedItem');
  Temp.AddCardinal('ID',0,high(Cardinal));
  Temp.AddBoolean('Used',false);

  TGameFigureManagerRecord.AddRecordList('ItemsUsedList',Temp);
  TGameFigureManagerRecord.AddRecordList('TemporaryItems',Temp);
finalization
  TGameFigureManagerRecord.Free;
end.
