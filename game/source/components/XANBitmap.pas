unit XANBitmap;

interface

uses
  Windows, Classes, Graphics, SysUtils, DXDraws, XForce_Types;

const
  XANHeader = (ord('X'))+(ord('A') shl 8)+(ord('N') shl 16)+(00 shl 24);

type
  TXANImageInfo = record
    OffSet : TPoint;
    Rect   : TRect;
  end;

  EInvalidXANImage = class(Exception);

  TXANBitmap = class(TObject)
  private
    fDXDraw     : TDXDraw;
    fSurface    : TDirectDrawSurface;
    fImageInfos : Array of TXANImageInfo;
    fOffSet     : TPoint;
    fFrames     : Integer;
  public
    constructor Create(DXDraw: TDXDraw);
    procedure LoadFromFile(FileName: String);
    procedure LoadFromStream(Stream: TStream);

    procedure SetAdditionalOffSet(OffSet: TPoint);

    function GetPixel(X,Y: Integer; Frame: Integer): Cardinal;

    procedure DrawImage(Surface: TDirectDrawSurface; X,Y: Integer; Frame: Integer);
    procedure RenderWithShadow(Surface: TDirectDrawSurface; X,Y: Integer; Frame: Integer);
    procedure RenderWithOutShadow(Surface: TDirectDrawSurface; X,Y: Integer; Frame: Integer; Half: Boolean = false);

    property Frames: Integer read fFrames;
  end;

implementation

uses
  ISOTools;

{ TXANBitmap }

constructor TXANBitmap.Create(DXDraw: TDXDraw);
begin
  fDXDraw:=DXDraw;
end;

procedure TXANBitmap.DrawImage(Surface: TDirectDrawSurface; X, Y,
  Frame: Integer);
begin
  if (Frame<0) or (Frame>=fFrames) then
    exit;
  Surface.Draw(X-fImageInfos[Frame].OffSet.X-fOffSet.X,Y-fImageInfos[Frame].OffSet.Y-fOffSet.Y,fImageInfos[Frame].Rect,fSurface,true);
end;

function TXANBitmap.GetPixel(X, Y, Frame: Integer): Cardinal;
begin
  result:=0;

  if (Frame<0) or (Frame>=fFrames) then
    exit;

  inc(X,fOffSet.X);
  inc(X,fImageInfos[Frame].OffSet.X);

  inc(Y,fOffSet.Y);
  inc(Y,fImageInfos[Frame].OffSet.Y);

  if (X>=0) and (X<fImageInfos[Frame].Rect.Right-fImageInfos[Frame].Rect.Left) and
     (Y>=0) and (X<fImageInfos[Frame].Rect.Bottom-fImageInfos[Frame].Rect.Top) then
    result:=fSurface.Pixels[X+fImageInfos[Frame].Rect.Left,Y+fImageInfos[Frame].Rect.Top];
end;

procedure TXANBitmap.LoadFromFile(FileName: String);
var
  fs: TFileStream;
begin
  fs:=TFileStream.Create(FileName,fmOpenRead);
  LoadFromStream(fs);
  fs.Free;
end;

procedure TXANBitmap.LoadFromStream(Stream: TStream);
var
  Buffer: Cardinal;
  Bitmap: TBitmap;
begin
  if fSurface=nil then
  begin
    fSurface:=TDirectDrawSurface.Create(fDXDraw.DDraw);
    fSurface.SystemMemory:=true;
  end;

  Stream.Read(Buffer,SizeOf(Buffer));

  if Buffer<>XANHeader then
    raise EInvalidXANImage.Create('Ungültiges XAN-Bitmap');

  Stream.Read(fFrames,SizeOf(fFrames));

  SetLength(fImageInfos,fFrames);

  Stream.Read(fImageInfos[0],sizeOf(fImageInfos[0])*fFrames);

  Bitmap:=TBitmap.Create;
  Bitmap.IgnorePalette:=false;
  Bitmap.LoadFromStream(Stream);

  fSurface.SetSize(Bitmap.Width,Bitmap.Height);
  fSurface.LoadFromGraphic(Bitmap);

  Bitmap.Free;
end;

procedure TXANBitmap.RenderWithOutShadow(Surface: TDirectDrawSurface; X, Y,
  Frame: Integer; Half: Boolean);
begin
  if (Frame<0) or (Frame>=fFrames) then
    exit;

  if Half then
    DrawWithOutShadowTrans(Surface,X-fImageInfos[Frame].OffSet.X-fOffSet.X,Y-fImageInfos[Frame].OffSet.Y-fOffSet.Y,fImageInfos[Frame].Rect,fSurface)
  else
    DrawWithOutShadow(Surface,X-fImageInfos[Frame].OffSet.X-fOffSet.X,Y-fImageInfos[Frame].OffSet.Y-fOffSet.Y,fImageInfos[Frame].Rect,fSurface);
end;

procedure TXANBitmap.RenderWithShadow(Surface: TDirectDrawSurface; X, Y,
  Frame: Integer);
begin
  if (Frame<0) or (Frame>=fFrames) then
    exit;

  DrawWithShadow(Surface,X-fImageInfos[Frame].OffSet.X-fOffSet.X,Y-fImageInfos[Frame].OffSet.Y-fOffSet.Y,fImageInfos[Frame].Rect,fSurface);
end;

procedure TXANBitmap.SetAdditionalOffSet(OffSet: TPoint);
begin
  fOffSet:=OffSet;
end;

end.
