{********************************************************************************
*										*
* Copyright (C) 2004  X-Force Team - jim_raynor@web.de				*
*										*
* This file is part of X-Force: Fight For Destiny (X-Force).			*
*										*
* X-Force is free software; you can redistribute it and/or modify		*
* it under the terms of the GNU General Public License as published by		*
* the Free Software Foundation; either version 2 of the License, or		*
* (at your option) any later version.						*
*										*
* X-Force is distributed in the hope that it will be useful,			*
* but WITHOUT ANY WARRANTY; without even the implied warranty of		*
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the			*
* GNU General Public License for more details.					*
*										*
* You should have received a copy of the GNU General Public License		*
* along with X-Force; if not, write to the Free Software			*
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA	*
*										*
*********************************************************************************
*										*
* Verwaltung der L�nder und St�dte						*
*										*
********************************************************************************}

{$I ../Settings.inc}

unit OrganisationList;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  XForce_types, math, DXDraws, Koding, TraceFile, ArchivFile, Compress, Sequenzer,
  IDHashMap, NotifyList, StringConst;

const
  SchSatz = 308;
  DangerWert = 20;           // L�nder die weniger Vertrauen haben, z�hlen als
                             // kein Vertrauen mehr
  DangerCountries = 10;      // L�nder bis Dangermode ausgerufen wird

  // EVENTS
  EVENT_ONDANGERMODE = 0;    // Mehr als 10 L�nder haben kein Vertrauen mehr
  EVENT_ONDANGERMODEOFF = 1; // Es sind wieder weniger als 10 L�nder
type
  TTown = class;

  TOrganisationList = class(TObject)
  private
    fOnChange      : TNotifyEvent;
    fOrganisations : Array[0..24] of TLand;
    fLaender       : Array[0..24] of TLandInfo;
    fWeltKarte     : Array[0..63,0..39] of Shortint;
    fTownHashMap   : TIDHashMap;
    fAllTowns      : TList;
    fTowns         : TList;
    fOnLoose       : TNotifyEvent;

    fDangerCountry : Integer;
    fNotifyList    : TNotifyList;

    fCountryFlags  : TPictureCollectionItem;
    function GetCount: Integer;
    function GetLaender(Index: Integer): TLand;
    procedure ChangeStat(Old,New: TOrganisationStatus;Name: string);
    function GetTownCount: Integer;
    function GetTown(Index: Integer): TTown;
    function GetDangerMode: Boolean;

    { Private-Deklarationen }
  protected
    procedure Clear;
    procedure GenerateTowns;
    function  SortTowns(Index1,Index2: Integer;Typ: TFunctionType): Integer;

    procedure NewGameHandler(Sender: TObject);
    { Protected-Deklarationen }
  public
    constructor Create;
    destructor destroy;override;
    procedure SetBitmaps(Bitmap: TBitmap);

    procedure DrawOrgan(Surface: TDirectDrawSurface;x,y: Integer;Index: Integer);
    procedure DrawSmallOrgan(Surface: TDirectDrawSurface;x,y: Integer;Index: Integer);

    procedure LoadLandInfo(LandDat: TStream);
    procedure LoadTownInfo(TownDat: TStream);
    procedure NextWeek(Punkte: Integer);
    procedure ClearBudget;
    procedure LoadFromStream(Stream: TStream);
    procedure SaveToStream(Stream: TStream);
    procedure PayForschMoney(const Project: TForschProject);
    procedure Abschuss(Pos: TFloatPoint;Minuten: Integer);
    function EinsatzGewonnen(Einsatz: TObject;Punkte: Integer): Integer;
    procedure EinsatzVerloren(Einsatz: TObject;Punkte: Integer);
    procedure UFOLebt(UFO: TObject);
    procedure AendereKapital(Land: Integer; Kapital: Integer);
    procedure ChangeZufriedenheit(Index: Integer;Wert: Integer);
    function CreateAngebot(const Auftrag : TAuftrag;var Angebot: TAngebot;Organ: Integer; Minuten: Integer):boolean;
    function GetNearestPoint(Index: Integer): TFloatPoint;
    function Hilfe(Index: Integer): Integer;
    function LandOverPos(Pos: TFloatPoint): String;
    function LandNrOverPos(Pos: TFloatPoint): Integer;
    function IsLandOverPos(Index: Integer;X,Y: Integer): boolean;
    function TownOfID(ID: Cardinal): TTown;

    function AddrOfLand(Index: Integer): PLand;

    property Laender[Index: Integer]: TLand read GetLaender;default;
    property Count       : Integer read GetCount;
    property OnChange    : TNotifyEvent read fOnChange write fOnChange;

    property OnLooseGame : TNotifyEvent read fOnLoose write fOnLoose;

    property TownCount   : Integer read GetTownCount;
    property Towns[Index: Integer]: TTown read GetTown;

    property IsDangerMode : Boolean read GetDangerMode;

    property NotifyList   : TNotifyList read fNotifyList;
  end;

  TTown = class
  private
    fPosition     : TFloatPoint;
    fName         : String;
    fList         : TOrganisationList;

    fLandIndex    : Integer;
    fEinwohner    : Integer;
    fInfiltration : Integer;
    fInfMessage   : Boolean;  // Wurde eine Nachricht abgeschickt, das die Stadt infiltriert wird
    fID           : Cardinal;
    fUFOAttack    : Boolean;
    fUFOMinuten   : Integer;
    fSchiffMinuten: Integer;
    fOnDestroy    : TNotifyEvent;
    function GetCountryName: String;
    function GetInfiltration: double;
  public
    constructor Create(List: TOrganisationList);overload;
    constructor Create(Town: TTown);overload;
    destructor Destroy;override;
    procedure LoadTownInfo(Stream: TStream);
    procedure LoadFromStream(Stream: TStream);
    procedure SaveToStream(Stream: TStream);
    procedure Infiltrate(Besatzung: Integer;UFO: Pointer);
    procedure Protection(Schiff: Pointer; Minuten: Integer);
    procedure RoundEnd(Minuten: Integer);

    property Position     : TFloatPoint read fPosition;
    property Name         : String read fName;
    property CountryName  : String read GetCountryName;
    property Country      : Integer read fLandIndex;
    property Einwohner    : Integer read fEinwohner;
    property Infiltration : double read GetInfiltration;
    property ID           : Cardinal read fID write fID;
    property OnDestroy    : TNotifyEvent read fOnDestroy write fOnDestroy;
  end;

const
  SquareXSize = 5.625;
  SquareYSize = 4.5;

implementation

uses KD4Utils, UFOList, Loader, EinsatzListe, RaumschiffList, savegame_api,
  game_api, game_utils, country_api, town_api, basis_api, ExtRecord,
  ConvertRecords;

var
  TOrganisationListRecord : TExtRecordDefinition;
  TTownRecord             : TExtRecordDefinition;

{ TOrganisationList }

procedure TOrganisationList.ChangeStat(Old, New: TOrganisationStatus;
  Name: string);
var
  Von   : String;
  Zu    : String;
  Stat  : String;
begin
  if Old=New then exit;
  Zu :=game_utils_OrganStatusToStr(New);
  Von:=game_utils_OrganStatusToStr(Old);
  if Ord(Old)<Ord(New) then
  begin
    Stat:=OASenkung;
    if New=osEnemy then
    begin
      inc(fDangerCountry);
      if fDangerCountry=DangerCountries then
        fNotifyList.CallEvents(EVENT_ONDANGERMODE,Self);

      // Spiel verloren?
      if fDangerCountry=15 then
        raise ELooseGame.Create(ST0309270009);
    end;
  end
  else
  begin
    Stat:=OASteigung;
    if Old=osEnemy then
    begin
      dec(fDangerCountry);
      if fDangerCountry=DangerCountries-1 then
        fNotifyList.CallEvents(EVENT_ONDANGERMODEOFF,Self);
    end;
  end;
  savegame_api_Message(Format(OAChangeStat,[Name,Stat,Von,Zu]),lmOrganStat);
end;

constructor TOrganisationList.Create;
begin
  country_api_init(Self);
  town_api_init(Self);

  randomize;
  fCountryFlags := game_api_CreateImageListItem;
  fCountryFlags.Transparent:=true;
  fCountryFlags.TransparentColor:=clFuchsia;
  fCountryFlags.SystemMemory:=true;

  fAllTowns:=TList.Create;
  fTowns:=TList.Create;
  fTownHashMap:=TIDHashMap.Create;

  fNotifyList:=TNotifyList.Create;

  savegame_api_RegisterCustomSaveHandler(SaveToStream,LoadFromStream,nil,$228AB5B9);
  savegame_api_RegisterNewGameHandler(NewGameHandler);
end;

function TOrganisationList.CreateAngebot(const Auftrag: TAuftrag;
  var Angebot: TAngebot;Organ: Integer; Minuten: Integer): boolean;
var
  Percent : real;
  Preis   : Integer;
  Anzahl  : Integer;
begin
  result:=false;
  while Minuten>0 do
  begin
    if random(60+round(100.2*(100-fOrganisations[Organ].Friendly)))=3 then
      result:=true;
    dec(Minuten);
  end;

  if result then
  begin
    Percent:=((-round(randG(fOrganisations[Organ].Friendly,10)))+50)/500;
    Preis:=round(Auftrag.Preis*(1+Percent));
    if Auftrag.Kauf then
      Anzahl:=Auftrag.Anzahl
    else
      Anzahl:=Random(Auftrag.Anzahl)+1;
    Angebot.ID:=Auftrag.ID;
    Angebot.Preis:=Preis;
    Angebot.Anzahl:=Anzahl;
    Angebot.Organisation:=Organ;
    Angebot.Rounds:=random(20)+30;
  end;
end;

destructor TOrganisationList.destroy;
var
  Dummy: Integer;
begin
  Clear;
  for Dummy:=0 to fAllTowns.Count-1 do
    TObject(fAllTowns[Dummy]).Free;

  fTowns.Free;
  fAllTowns.Free;
  fTownHashMap.Free;
  fNotifyList.Free;
  inherited;
end;

procedure TOrganisationList.DrawOrgan(Surface: TDirectDrawSurface; x, y,
  Index: Integer);
begin
  Surface.Canvas.Release;
  fCountryFlags.Draw(Surface,X,Y,Index);
end;

function TOrganisationList.GetCount: Integer;
begin
  result:=length(fOrganisations);
end;

function TOrganisationList.Hilfe(Index: Integer): Integer;
begin
  result:=0;
  if (Index<0) or (Index>Count-1) then
    exit;
  result:=fOrganisations[Index].Budget;
end;

function TOrganisationList.GetLaender(Index: Integer): TLand;
begin
  if (Index<0) or (Index>Count-1) then
    exit;
  result:=fOrganisations[Index];
end;

procedure TOrganisationList.LoadFromStream(Stream: TStream);
var
  Dummy          : Integer;
  Count          : Integer;
  Rec            : TExtRecord;
  Town           : TTown;
begin
  Clear;

  fDangerCountry:=0;

  Rec:=TExtRecord.Create(TOrganisationListRecord);
  Rec.LoadFromStream(Stream);

  with Rec.GetRecordList('Laender') do
  begin
    for Dummy:=0 to 24 do
    begin
      fOrganisations[Dummy]:=RecordToLand(Item[Dummy]);
      fOrganisations[Dummy].Towns:=0;
      fOrganisations[Dummy].Name:=fLaender[fOrganisations[Dummy].InfoIndex].Name;

      if fOrganisations[Dummy].Friendly<DangerWert then
        inc(fDangerCountry);
    end;
  end;

  Count:=Rec.GetInteger('TownCount');
  Rec.Free;

  // Stadtinformationen laden
  fTownHashMap.ClearList;

  while Count>0 do
  begin
    Town:=TTown.Create(Self);
    Town.LoadFromStream(Stream);
    fTownHashMap.InsertID(Town.ID,Integer(Town));
    fTowns.Add(Town);
    inc(fOrganisations[Town.Country].Towns);
    dec(Count);
  end;
  
  QuickSort(0,fTowns.Count-1,SortTowns);
end;

procedure TOrganisationList.NewGameHandler(Sender: TObject);
var
  Dummy      : Integer;
  Pos        : TFloatPoint;
begin
  Clear;
  for Dummy:=0 to 24 do
  begin
    with fOrganisations[Dummy] do
    begin
      Name:=fLaender[Dummy].Name;
      ID:=fLaender[Dummy].ID;
      Image:=Dummy;
      InfoIndex:=Dummy;
      {$IFDEF MAXCONFIDENT}
      Friendly:=100;
      {$ELSE}
      Friendly:=max(0,min(100,round(RandG(fLaender[Dummy].StartFriendly,5))));
      {$ENDIF}
      UFOHours:=0;
      Einwohner:=fLaender[Dummy].Felder*round(RandG(fLaender[Dummy].Dichte,fLaender[Dummy].Dichte/10));
      Zufriedenheit:=0;
      Budget:=0;
    end
  end;
  // Das Land, in dem die Hauptbasis liegt erh�lt 100% Vertrauen
  Pos:=basis_api_GetMainBasis.BasisPos;
  if LandNrOverPos(Pos)<>-1 then
    fOrganisations[LandNrOverPos(Pos)].Friendly:=100;

  // St�dte ermitteln
  GenerateTowns;

  if Assigned(fOnChange) then fOnChange(Self);
end;

procedure TOrganisationList.PayForschMoney(const Project: TForschProject);
var
  Dummy: Integer;
  Zahlen: Integer;
begin
  for Dummy:=0 to Count-1 do
  begin
    Zahlen:=round(Project.Stunden*(0.2-((fOrganisations[Dummy].Friendly-50)*0.002)));
    inc(fOrganisations[Dummy].Budget,Zahlen);
    ChangeZufriedenheit(Dummy,max(1,round(fOrganisations[Dummy].Friendly*0.05)));
  end;
end;

procedure TOrganisationList.SaveToStream(Stream: TStream);
var
  Dummy   : Integer;
  Rec     : TExtRecord;
begin
  Rec:=TExtRecord.Create(TOrganisationListRecord);

  with Rec.GetRecordList('Laender') do
  begin
    for Dummy:=0 to 24 do
      Add(LandToRecord(fOrganisations[Dummy]));
  end;

  Rec.SetInteger('TownCount',fTowns.Count);
  Rec.SaveToStream(Stream);

  Rec.Free;

  for Dummy:=0 to fTowns.Count-1 do
    TTown(fTowns[Dummy]).SaveToStream(Stream);
end;

procedure TOrganisationList.SetBitmaps(Bitmap: TBitmap);
begin
  fCountryFlags.Picture.Bitmap:=Bitmap;
  fCountryFlags.PatternHeight:=21;
  fCountryFlags.PatternWidth:=42;
  fCountryFlags.Restore;
end;

procedure TOrganisationList.NextWeek(Punkte: Integer);
var
  Dummy    : Integer;
  Hilfe    : Integer;
begin
  Hilfe:=0;
  for Dummy:=0 to 24 do
  begin
    with fOrganisations[Dummy] do
    begin
      inc(Budget,round(Punkte*(Friendly/150)));
      if Budget<0 then
        Budget:=0;
      inc(Hilfe,Budget);
    end;
  end;
  savegame_api_FreeMoney(Hilfe,kbFiU);
end;

procedure TOrganisationList.LoadLandInfo(LandDat: TStream);
var
  Version : Integer;
  Dummy   : Integer;
  Stream  : TMemoryStream;
begin
  Stream:=TMemoryStream.Create;
  Decode(LandDat,Stream);
  Stream.Read(Version,SizeOf(Version));
  if Version<>LandVersion then
    raise EInvalidVersion.Create('Ung�ltige L�nderinformationen');
  for Dummy:=0 to 24 do
  begin
    with fLaender[Dummy] do
    begin
      Stream.Read(ID,SizeOf(ID));
      Name:=ReadString(Stream);
      Oberhaupt:=ReadString(Stream);
      Stream.Read(StartFriendly,SizeOf(StartFriendly));
      Stream.Read(NearestSquare,SizeOf(NearestSquare));
      Stream.Read(Dichte,SizeOf(Dichte));
      Stream.Read(Felder,SizeOf(Felder));
      Name:=StringConst.Country[Dummy+1];
    end;
  end;
  Stream.Read(Version,SizeOf(Version));
  if Version=$29CF1C9F then
  begin
    Stream.Read(fWeltKarte,SizeOf(fWeltKarte));
  end;
  Stream.Free;
end;

function TOrganisationList.LandOverPos(Pos: TFloatPoint): String;
var
  XPos,YPos: Integer;
  Land     : Integer;
begin
  XPos:=min(63,Trunc(Pos.X/SquareXSize));
  YPos:=min(39,Trunc(Pos.Y/SquareYSize));
  if PtInRect(Rect(0,0,length(fWeltKarte),length(fWeltKarte[0])),Point(XPos,YPos)) then
    Land:=fWeltKarte[Xpos,YPos]
  else
    Land:=-1;

  if Land<>-1 then
    result:=fLaender[Land].Name
  else
    result:='';
end;

function TOrganisationList.IsLandOverPos(Index, X, Y: Integer): boolean;
begin
  result:=(fWeltKarte[X,Y]=fOrganisations[Index].InfoIndex);
end;

procedure TOrganisationList.Abschuss(Pos: TFloatPoint; Minuten: Integer);
var
  XPos,YPos: Integer;
  Land     : Integer;
  Dummy    : Integer;
  Prozent  : Integer;
begin
  XPos:=round(Pos.X/SquareXSize);
  YPos:=round(Pos.Y/SquareYSize);
  Land:=fWeltKarte[Xpos,YPos];
  Prozent:=round(max(115-((Minuten div 60)*5),0)*min(1.1,max(randG(1,0.1),0.9)));
  if Land<>-1 then
  begin
    for Dummy:=0 to 24 do
    begin
      with fOrganisations[Dummy] do
      begin
        if Dummy=Land then
        begin
          inc(Budget,round((Friendly*4)*Prozent/100));
          case OrganStatus(Friendly) of
            osAllianz    : if Minuten<=180 then ChangeZufriedenheit(Dummy,8);
            osFriendly   : if Minuten<=300 then ChangeZufriedenheit(Dummy,8);
            osNeutral    : if Minuten<=420 then ChangeZufriedenheit(Dummy,8);
            osUnFriendly : if Minuten<=540 then ChangeZufriedenheit(Dummy,8);
            osEnemy      : if Minuten<=660 then ChangeZufriedenheit(Dummy,8);
          end;
        end
        else
        begin
          inc(Budget,round((Friendly*0.7)*Prozent/100));
          case OrganStatus(Friendly) of
            osAllianz    : if Minuten<=180 then ChangeZufriedenheit(Dummy,4);
            osFriendly   : if Minuten<=300 then ChangeZufriedenheit(Dummy,4);
            osNeutral    : if Minuten<=420 then ChangeZufriedenheit(Dummy,4);
            osUnFriendly : if Minuten<=540 then ChangeZufriedenheit(Dummy,4);
            osEnemy      : if Minuten<=660 then ChangeZufriedenheit(Dummy,4);
          end;
        end;
      end;
    end;
  end
  else
  begin
    for Dummy:=0 to 24 do
    begin
      with fOrganisations[Dummy] do
      begin
        inc(Budget,round((Friendly*0.9)*Prozent/100));
        case OrganStatus(Friendly) of
          osAllianz    : if Minuten<=180 then ChangeZufriedenheit(Dummy,4);
          osFriendly   : if Minuten<=300 then ChangeZufriedenheit(Dummy,4);
          osNeutral    : if Minuten<=420 then ChangeZufriedenheit(Dummy,4);
          osUnFriendly : if Minuten<=540 then ChangeZufriedenheit(Dummy,4);
          osEnemy      : if Minuten<=660 then ChangeZufriedenheit(Dummy,4);
        end;
      end;
    end;
  end;
end;

procedure TOrganisationList.ClearBudget;
var
  Dummy: Integer;
begin
  for Dummy:=0 to 24 do
  begin
    fOrganisations[Dummy].Budget:=0;
  end;
end;

procedure TOrganisationList.ChangeZufriedenheit(Index: Integer;Wert: Integer);
var
  OldState: TOrganisationStatus;
  NewState: TOrganisationStatus;
begin
  {$IFDEF MAXCONFIDENT}
  exit;
  {$ENDIF}
  if (Index<0) or (Index>Count-1) then exit;
  with fOrganisations[Index] do
  begin
    OldState:=OrganStatus(Friendly);
    inc(Zufriedenheit,Wert);
    while abs(Zufriedenheit)>Friendly do
    begin
      if Zufriedenheit>Friendly then
      begin
        dec(Zufriedenheit,Friendly);
        if Friendly>0 then
          Friendly:=min(100,Friendly+min(5,max(1,round(100/Friendly))))
        else
          Friendly:=1;
      end
      else if Zufriedenheit<-Friendly then
      begin
        inc(Zufriedenheit,Friendly);
        Assert(101-Friendly<>0,'Friendly hat einen Wert von 101');
        Friendly:=max(0,Friendly-min(5,max(1,round(100/(101-Friendly)))));
      end;
      if Friendly=0 then
        Zufriedenheit:=0;
    end;
    NewState:=OrganStatus(Friendly);
    ChangeStat(OldState,NewState,Name);
  end;
end;

procedure TOrganisationList.UFOLebt(UFO: TObject);
var
  XPos,YPos: Integer;
  Land     : Integer;
  Dummy    : Integer;
  UFOO     : TUFO;
begin
  UFOO:=TUFO(UFO);
  XPos:=round(UFOO.Position.X/SquareXSize);
  YPos:=round(UFOO.Position.Y/SquareYSize);
  Land:=fWeltKarte[Xpos,YPos];
  if Land=-1 then exit;
  for Dummy:=0 to 24 do
  begin
    with fOrganisations[Dummy] do
    begin
      case OrganStatus(Friendly) of
        osAllianz    : if UFOO.Minuten>= 900 then ChangeZufriedenheit(Dummy,-3);
        osFriendly   : if UFOO.Minuten>=1020 then ChangeZufriedenheit(Dummy,-3);
        osNeutral    : if UFOO.Minuten>=1140 then ChangeZufriedenheit(Dummy,-3);
        osUnFriendly : if UFOO.Minuten>=1260 then ChangeZufriedenheit(Dummy,-3);
        osEnemy      : if UFOO.Minuten>=1380 then ChangeZufriedenheit(Dummy,-3);
      end;
    end;
  end;
  if (UFOO.LandMinuten>=240) then
  begin
    ChangeZufriedenheit(Land,-6)
  end;
end;

function TOrganisationList.LandNrOverPos(Pos: TFloatPoint): Integer;
var
  XPos,YPos: Integer;
begin
  XPos:=min(63,Trunc(Pos.X/SquareXSize));
  YPos:=min(39,Trunc(Pos.Y/SquareYSize));
  result:=fWeltKarte[Xpos,YPos];
end;

procedure TOrganisationList.DrawSmallOrgan(Surface: TDirectDrawSurface; x,
  y, Index: Integer);
begin
  Surface.Canvas.Release;
  Surface.StretchDraw(Rect(x,y,x+21,y+11),fCountryFlags.PatternRects[Index],fCountryFlags.PatternSurfaces[Index],false);
end;

function TOrganisationList.GetNearestPoint(Index: Integer): TFloatPoint;
begin
  result:=FloatPoint(fLaender[Index].NearestSquare.x*SquareXSize,fLaender[Index].NearestSquare.Y*SquareYSize);
end;

procedure TOrganisationList.AendereKapital(Land, Kapital: Integer);
begin
  if (Land<0) or (Land>=Count) then
    exit;
  inc(fOrganisations[Land].Budget,Kapital);
end;

function TOrganisationList.EinsatzGewonnen(Einsatz: TObject; Punkte: Integer): Integer;
var
  Land  : Integer;
begin
  with TEinsatz(Einsatz).Position do
    Land:=fWeltKarte[round(X/SquareXSize),round(Y/SquareYSize)];

  if Land<>-1 then
  begin
    inc(fOrganisations[Land].Budget,round(Punkte*randG(10,1)));
    result:=round(Punkte*randG(30,1));
    savegame_api_FreeMoney(result,kbEG);
  end
  else
  begin
    result:=round(Punkte*randG(40,1));
    savegame_api_FreeMoney(result,kbEG);
  end;
end;

procedure TOrganisationList.EinsatzVerloren(Einsatz: TObject; Punkte: Integer);
begin
end;

function TOrganisationList.GetTownCount: Integer;
begin
  result:=fTowns.Count;
end;

function TOrganisationList.GetTown(Index: Integer): TTown;
begin
  result:=nil;
  if (Index<0) or (Index>=TownCount) then
    exit;
  result:=TTown(fTowns[Index]);
end;

procedure TOrganisationList.LoadTownInfo(TownDat: TStream);
var
  Count   : Integer;
  Stream  : TMemoryStream;
  Town    : TTown;
begin
  Stream:=TMemoryStream.Create;
  LHAExpand(TownDat,Stream);
  Stream.Position:=0;
  Stream.Read(Count,SizeOf(Count));
//  Count:=100;
  while Count>0 do
  begin
    Town:=TTown.Create(Self);
    Town.LoadTownInfo(Stream);
    fAllTowns.Add(Town);
    dec(Count);
  end;
  Stream.Free;
end;

procedure TOrganisationList.Clear;
var
  Dummy: Integer;
begin
  for Dummy:=fTowns.Count-1 downto 0 do
    TTown(fTowns[Dummy]).Free;
  fTowns.Clear;
end;

procedure TOrganisationList.GenerateTowns;
var
  Sequenz    : TSequenzer;
  Dummy      : Integer;
  Number     : Integer;
  Town       : TTown;
  ID         : Cardinal;
  Temp       : Integer;
begin
  // Anzahl der St�dte in einem Land zur�cksetzen
  for Dummy:=0 to high(fOrganisations) do
    fOrganisations[Dummy].Towns:=0;

  fTownHashMap.ClearList;
  Sequenz:=TSequenzer.Create;
  Sequenz.SetHighest(fAllTowns.Count-1);
  for Dummy:=1 to 100 do
  begin
    Number:=Sequenz.GetNumber;
    Town:=TTown.Create(TTown(fAllTowns[Number]));
    repeat
      ID:=random(high(Cardinal));
    until not fTownHashMap.FindKey(ID,Temp);
    fTownHashMap.InsertID(ID,Integer(Town));
    Town.ID:=ID;
    fTowns.Add(Town);
    inc(fOrganisations[Town.Country].Towns);
  end;

  QuickSort(0,fTowns.Count-1,SortTowns);
  Sequenz.Free;
end;

function TOrganisationList.TownOfID(ID: Cardinal): TTown;
begin
  if not fTownHashMap.FindKey(ID,Integer(result)) then
    result:=nil;
end;

function TOrganisationList.SortTowns(Index1, Index2: Integer;
  Typ: TFunctionType): Integer;
begin
  case Typ of
    ftCompare :
      result:=CompareStr(TTown(fTowns[Index1]).Name,TTown(fTowns[Index2]).Name);
    ftExchange :
      fTowns.Exchange(Index1,Index2);
  end;
end;

function TOrganisationList.GetDangerMode: Boolean;
begin
  result:=fDangerCountry>=DangerCountries;
end;

function TOrganisationList.AddrOfLand(Index: Integer): PLand;
begin
  result:=Addr(fOrganisations[Index]);
end;

{ TTown }

constructor TTown.Create(List: TOrganisationList);
begin
  fList:=List;
  fSchiffMinuten:=0;
  fUFOMinuten:=0;
  fUFOAttack:=false;
  fEinwohner:=200000+random(10000000);
end;

constructor TTown.Create(Town: TTown);
begin
  fList:=Town.fList;
  fName:=Town.Name;
  fPosition:=Town.Position;
  fEinwohner:=Town.Einwohner;
  fInfiltration:=0;
  fLandIndex:=Town.fLandIndex;
  fSchiffMinuten:=0;
  fUFOAttack:=false;
end;

destructor TTown.Destroy;
begin
  if Assigned(fOnDestroy) then
    fOnDestroy(Self);
  inherited;
end;

function TTown.GetCountryName: String;
begin
  if fLandIndex<>-1 then
    result:=fList.Laender[fLandIndex].Name
  else
    result:='';
end;

function TTown.GetInfiltration: double;
begin
  if fEinwohner>0 then
    result:=fInfiltration/fEinwohner
  else
    result:=0;
end;

procedure TTown.Infiltrate(Besatzung: Integer;UFO: Pointer);
var
  Besatzungswert : double;
  FriendlyFaktor : double;
begin
  Assert(TObject(UFO) is TUFO);
  Besatzungswert:=Besatzung*(1+(GetInfiltration/100));
  if fLandIndex<>-1 then
    FriendlyFaktor:=2+(((100-fList.Laender[fLandIndex].Friendly)/100)*28)
  else
    FriendlyFaktor:=1;

  fInfiltration:=fInfiltration+round(Besatzungswert*FriendlyFaktor);
  if not fInfMessage then
  begin
    savegame_api_Message(Format(ST0309270010,[Name,CountryName]),lmUFOs,TObject(UFO));
    fInfMessage:=true;
  end;
  TUFO(UFO).Visible:=true;
  fUFOAttack:=true;

  // Punktabz�ge f�r Infiltration
  inc(fUFOMinuten);
  if fUFOMinuten>=5 then
  begin
    dec(fUFOMinuten,5);
    savegame_api_BuchPunkte(-1,pbInfiltration);
  end;
end;

procedure TTown.LoadFromStream(Stream: TStream);
var
  Rec: TExtRecord;
begin
  Rec:=TExtRecord.Create(TTownRecord);
  Rec.LoadFromStream(Stream);

  fID:=Rec.GetCardinal('ID');
  fName:=Rec.GetString('Name');
  fInfMessage:=Rec.GetBoolean('InfiltrationMessage');
  fPosition.X:=Rec.GetDouble('Position.X');
  fPosition.Y:=Rec.GetDouble('Position.Y');
  fEinwohner:=Rec.GetInteger('Einwohner');
  fInfiltration:=Rec.GetInteger('Infiltration');

  Rec.Free;

  fLandIndex:=fList.LandNrOverPos(fPosition);
end;

procedure TTown.LoadTownInfo(Stream: TStream);
begin
  fName:=ReadString(Stream);
  Stream.Read(fPosition.X,SizeOf(fPosition.X));
  Stream.Read(fPosition.Y,SizeOf(fPosition.Y));

  fLandIndex:=fList.LandNrOverPos(fPosition);
end;

procedure TTown.Protection(Schiff: Pointer; Minuten: Integer);
var
  RSchiff: TRaumschiff;
begin
  Assert(TObject(Schiff) is TRaumschiff);
  Inc(fSchiffMinuten,Minuten);
  RSchiff:=TRaumschiff(Schiff);

  // Aktionen f�r das Raumschiff durchf�hren, wenn die Stadt verb�ndet ist
  if OrganStatus(fList.Laender[fLandIndex].Friendly)=osAllianz then
  begin
    RSchiff.Tanken(Minuten);
    RSchiff.Repair(Minuten);
  end;
end;

procedure TTown.RoundEnd(Minuten: Integer);
begin
  // Wurde in der Letzten Runde kein Angriff durchgef�hrt
  if not fUFOAttack then
  begin
    dec(fInfiltration);
    fInfMessage:=false;
  end;

  fSchiffMinuten:=0;
  fUFOAttack:=false;
end;

procedure TTown.SaveToStream(Stream: TStream);
var
  Rec  : TExtRecord;
begin
  Rec:=TExtRecord.Create(TTownRecord);
  Rec.SetCardinal('ID',fID);
  Rec.SetString('Name',fName);
  Rec.SetBoolean('InfiltrationMessage',fInfMessage);
  Rec.SetDouble('Position.X',fPosition.X);
  Rec.SetDouble('Position.Y',fPosition.Y);
  Rec.SetInteger('Einwohner',fEinwohner);
  Rec.SetInteger('Infiltration',fInfiltration);

  Rec.SaveToStream(Stream);
  Rec.Free;
end;

initialization

  TOrganisationListRecord:=TExtRecordDefinition.Create('TOrganisationListRecord');
  TOrganisationListRecord.AddRecordList('Laender',RecordLand);
  TOrganisationListRecord.AddInteger('TownCount',0,high(Integer));

  TTownRecord:=TExtRecordDefinition.Create('TTownRecord');
  TTownRecord.AddCardinal('ID',0,high(Cardinal));
  TTownRecord.AddString('Name',50);
  TTownRecord.AddBoolean('InfiltrationMessage');
  TTownRecord.AddDouble('Position.X',-360,360,10);
  TTownRecord.AddDouble('Position.Y',-360,360,10);
  TTownRecord.AddInteger('Einwohner',0,high(Integer));
  TTownRecord.AddInteger('Infiltration',0,high(Integer));

finalization
  TTownRecord.Free;
  TOrganisationListRecord.Free;
end.
