{********************************************************************************
*										*
* Copyright (C) 2004  X-Force Team - jim_raynor@web.de				*
*										*
* This file is part of X-Force: Fight For Destiny (X-Force).			*
*										*
* X-Force is free software; you can redistribute it and/or modify		*
* it under the terms of the GNU General Public License as published by		*
* the Free Software Foundation; either version 2 of the License, or		*
* (at your option) any later version.						*
*										*
* X-Force is distributed in the hope that it will be useful,			*
* but WITHOUT ANY WARRANTY; without even the implied warranty of		*
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the			*
* GNU General Public License for more details.					*
*										*
* You should have received a copy of the GNU General Public License		*
* along with X-Force; if not, write to the Free Software			*
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA	*
*										*
*********************************************************************************
*										*
* Verwaltet alle Forschungsprojekte und �bernimmt die Steuerung, ab wann	*
* Ausr�stung zur Verf�gung steht. Auf die Forschliste sollte nur �ber die	*
* forsch_api zugegriffen werden.						*
*										*
********************************************************************************}

{$I ../Settings.inc}

unit ForschList;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  XForce_types, math, DXClass, DXDraws, Defines,Koding, NGTypes, TraceFile,
  Loader, Blending, DirectDraw, ExtRecord, ProjektRecords, ConvertRecords,
  LagerListe, BasisListe, NotifyList, StringConst;

const
  EVENT_FORSCHLISTPROJEKTEND   : TEventID    = 0;
  EVENT_FORSCHLISTUPGRADEEND   : TEventID    = 1;

type
  TForscherArray    = Array of TForscher;
  TForschungenArray = Array of TForschungen;
  TPatentArray      = Array of TPatent;

  TForschList = class(TObject)
  private
    fTime            : Integer;
    AlleProjekte     : Array of TForschProject;
    Projekte         : TForschungenArray;
    fForscher        : TForscherArray;
    fKaufList        : TForscherArray;
    fCompleteID      : Array of TEndedForschProjekt;
    fPatents         : TPatentArray;
    fDay             : Integer;
    fWatchs          : TWatchMarktArray;
    fSortKauf        : Boolean;
    fNotSort         : Boolean;
    fNotifyList      : TNotifyList;

    // Hier wird das letzte abgeschlossene Projekt gespeichert. Dadurch kann
    // sp�ter zum Beispiel direkt in die UFOP�die gewechselt werden
    fLastProjekt     : TForschungen;
    procedure SetForscherProjekt(Index: Integer; const Value: Integer);

    procedure DeleteProjekt(Index: Integer);
    function AddProjekt(Index: Integer): Boolean;

    procedure CorrectForscher;
    procedure Abgleich;

    // Pr�ft, ob Projekte das Verf�gbar nach ... Tagen erf�llt haben
    procedure CheckStartVer;

    procedure CheckProjekts;

    //GeniusErlaubt bestimmt, ob der Forscher eine gute Chance hat, die St�rke �ber 75 zu haben
    function RandomForsch(GeniusErlaubt: boolean): TForscher;
    function GetKaufForscher(Index: integer): TForscher;
    function GetKaufCount: Integer;
    function GetProjektText(Index: integer): String;
    function GetIndexOfRunningProjectId(ID: Cardinal): Integer;
    function GetProjektCount: Integer;
    function GetProjektName(Index: integer): String;
    function GetPercentDone(Index: integer): Real;
    function GetForschCount: Integer;
    function GetForscher(Index: integer): TForscher;
    function GetForscherProjekt(Index: Integer): Integer;
    function IDisProjekt(ID: Cardinal): boolean;
    function GetProjektBasis(Index: Integer): TBasis;
    function InCompleteID(ID: Cardinal): boolean;
    function InProjekte(ID: Cardinal): boolean;
    function CanForschProjekt(Index: Integer): boolean;

    procedure Sort;
    function SortFunc(KForscher1,KForscher2: Integer;Typ: TFunctionType): Integer;

    procedure ProjektEnd(Index: Integer);
    function GetCompletedProjects: Integer;

    procedure CheckWatchForsch(Index: Integer);

    { Private-Deklarationen }
  protected
    procedure Clear;
    procedure NewGameHandler(Sender: TObject);
    procedure LoadGameSetHandler(Sender: TObject);
    procedure AfterLoadHandler(Sender: TObject);

    { Protected-Deklarationen }
  public
    constructor Create;
    destructor Destroy;override;

    procedure DrawDXForscher(Index: integer; Surface: TDirectDrawSurface;const Mem: TDDSurfaceDesc; x,y: Integer);
    procedure DrawDXKaufForscher(Index: integer; Surface: TDirectDrawSurface;const Mem: TDDSurfaceDesc; x,y: Integer);

    procedure SaveToStream(Stream: TStream);
    procedure LoadFromStream(Stream: TStream);
    procedure DeleteForscher(Index: Integer;FreeWohnRaum: Boolean = true);
    procedure BuyForscher(Index: Integer; BasisID: Cardinal = 0);

    procedure CreateKaufList;
    procedure CreateForscher(Anzahl: Integer);
    procedure ChangeTraining(Index: Integer);
    procedure SetTraining(Index: Integer);
    procedure FreeForscher(Index: Integer);
    procedure SellForscher(Index: Integer);
    procedure RenameForscher(Index: Integer;Name: String);

    procedure NextDay;
    procedure NextRound(Minuten: Integer);

    function ForscherInSelectedBasis(Index: Integer): boolean;
    function ProjektInSelectedBasis(Index: Integer): boolean;
    function ProjektStrength(Index: Integer): Integer;
    function IndexOfUpgrade(ID: Cardinal): Integer;
    function GetIndexOfProjectId(ID: Cardinal): Integer;
    
    procedure StartUpgrade(const Item: TLagerItem);
    procedure MoveProject(Index: Integer; Down: Boolean);

    // Forschungsprojekt als erforschbar definieren
    procedure AddAlienItemResearch(ItemID: Cardinal);

    // Alienautopsie erforschen
    procedure AddAlienAutopsie(const Alien: TAlien);

    // Forschungsprojekt endg�ltig starten
    procedure StartResearch(Index: Integer);

    procedure CancelResearch(Index: Integer);

    procedure CheckNewItems;

    function CompletedAsProject(Index: Integer; out Projekt: TForschProject): Boolean;
    function IndexOfCompletedProject(ID: Cardinal): Integer;

    function GetForschProject(Index: Integer): TForschProject;

    function CheckLaborRoom(BaseID: Cardinal): String;

    function GetLastProject: TForschungen;

    // Berechnet das W�chentliche Gehalt f�r alle Wissenschaftler
    function CalculateWeeklySold: Integer; overload;
    // Berechnet das W�chentliche Gehalt f�r alle Wissenschaftler der Basis
    function CalculateWeeklySold(BasisID: Cardinal): Integer; overload;

    { Steuerung zur �berwachung der Liste }
    procedure CancelWatch(BasisID: Cardinal);
    procedure SetWatch(Basis: Cardinal;minStrength: Integer;WType: TWatchType);
    function GetWatch(BasisID: Cardinal;var Watch: TWatchMarktSettings): Boolean;

    function GetProjektInfo(Index: Integer): TForschungen;

    function CountInBase(BasisID: Cardinal): Integer;

    property ProjektCount    : Integer read GetProjektCount;
    property ForscherCount   : Integer read GetForschCount;
    property KaufCount       : Integer read GetKaufCount;
    property ProjektTime     : Integer read fTime write fTime;

    property ProjektName    [Index: integer]: String read GetProjektName;
    property ProjektText    [Index: integer]: String read GetProjektText;
    property ProjektBasis   [Index: Integer]: TBasis read GetProjektBasis;
    property PercentDone    [Index: integer]: Real read GetPercentDone;
    property Forscher       [Index: integer]: TForscher read GetForscher;default;
    property ForscherProjekt[Index: Integer]: Integer read GetForscherProjekt write SetForscherProjekt;
    property KaufForscher   [Index: integer]: TForscher read GetKaufForscher;

    property CompletedProjects   : Integer read GetCompletedProjects;

    property NotifyList      : TNotifyList read fNotifyList;
  end;

implementation

uses
  KD4Utils, savegame_api, basis_api, forsch_api, country_api, lager_api,
  array_utils, alien_api, person_api, string_utils, game_utils;

{ TForschList }

var
  TForschListRecord: TExtRecordDefinition;

function TForschList.AddProjekt(Index: Integer): boolean;
var
  PIndex : Integer;
  Info   : String;
begin
  result:=false;
  if InProjekte(AlleProjekte[Index].ID) or InCompleteID(AlleProjekte[Index].ID) then
    exit;

  if AlleProjekte[Index].Ver>fDay then
    exit;

  SetLength(Projekte,Length(Projekte)+1);
  PIndex:=high(Projekte);
  Projekte[PIndex].ParentID:=0;

  with Projekte[PIndex] do
  begin
    ID:=AlleProjekte[Index].ID;
    Hour:=forsch_api_calculateForschTime(AlleProjekte[Index].Stunden);
    Gesamt:=Projekte[PIndex].Hour;

    TypeId:=AlleProjekte[Index].TypeID;
    Update:=false;
    ResearchItem:=AlleProjekte[Index].AlienItem;

    if AlleProjekte[Index].ResearchInfo='' then
      Info:=AlleProjekte[Index].Info
    else
      Info:=AlleProjekte[Index].ResearchInfo;

    if ResearchItem then
    begin
      ResearchCount:=AlleProjekte[Index].ResearchCount;
      Info:=Format(ST0310260001,[ResearchCount,Info]);
      ResearchType:=ftItemResearch;
    end
    else
    begin
      Info:=Info;
      ResearchCount:=0;
      ResearchType:=ftNormal;
    end;
  end;

  result:=true;
  // Ausr�stung ist sofort verf�gbar sobald alle Vorraussetzungen erf�llt sind und
  // keine Forschungszeit angegeben wurde
  if AlleProjekte[Index].Stunden=0 then
  begin
    ProjektEnd(PIndex);
    result:=false;
  end;
end;

procedure TForschList.BuyForscher(Index: Integer; BasisID: Cardinal);
begin
  Assert((Index>=0) and (Index<=High(fKaufList)));

  basis_api_NeedWohnRaum(BasisID);

  if not savegame_api_NeedMoney(round(fKaufList[Index].Strength*ForscherBuy),kbFK) then
  begin
    basis_api_FreeWohnRaum(BasisID);
    raise ENotEnoughMoney.Create(ENoMoney);
  end;

  SetLength(fForscher,ForscherCount+1);

  fForscher[ForscherCount-1]:=fKaufList[Index];
  fForscher[ForscherCount-1].BasisID:=basis_api_GetbasisFromID(BasisID).ID;

  DeleteArray(Addr(fKaufList),TypeInfo(TForscherArray),Index);

  Sort;
end;

procedure TForschList.CorrectForscher;
var
  Dummy: Integer;
begin
  for Dummy:=0 to ForscherCount-1 do
    if not IDisProjekt(fForscher[Dummy].ID) then FreeForscher(Dummy);
end;

constructor TForschList.Create;
begin
  inherited;
  randomize;
  fNotSort:=false;

  fNotifyList:=TNotifyList.Create;

  forsch_api_init(Self);

  savegame_api_RegisterCustomSaveHandler(SaveToStream,LoadFromStream,nil,$7CE1009C);
  savegame_api_RegisterNewGameHandler(NewGameHandler);
  savegame_api_RegisterLoadGameSetHandler(LoadGameSetHandler);
  savegame_api_RegisterAfterLoadHandler(AfterLoadHandler);
end;

procedure TForschList.CreateForscher(Anzahl: Integer);
var
  Dummy: Integer;
  Index: Integer;
begin
  Index:=ForscherCount;
  for Dummy:=1 to Anzahl do
  begin
    basis_api_NeedWohnRaum;

    SetLength(fForscher,ForscherCount+1);
    fForscher[Index]:=RandomForsch(true);
    fForscher[Index].BasisID:=basis_api_GetMainBasis.ID;
    inc(Index);
  end;
end;

procedure TForschList.CreateKaufList;
var
  Dummy         : Integer;
begin
  { Aktualisiert die Liste von Forschern auf dem Arbeitsmarkt }
  { dabei werden zuf�llig neue erstellt oder gel�scht         }
  { Zuerst werden alle Forscher durchgangen und falls eine    }
  { Zufallszahl bis 100 kleiner als 15 ist wird er gel�scht   }
  Dummy:=0;
  while (Dummy<KaufCount) do
  begin
    if random(100)<=15 then
    begin
      DeleteArray(Addr(fKaufList),TypeInfo(TForscherArray),Dummy);
    end
    else
      inc(Dummy);
  end;
  { Es werden f�nf Durchl�ufe get�tigt, bei jedem Durchlauf   }
  { wird eine Zufallszahl erstellt. Wenn diese kleiner als    }
  { 15 ist wird ein neuer Forscher erstellt.Es k�nnen max. 20 }
  { Forscher auf dem Arbeitsmarkt sein                        }
  for Dummy:=1 to 5 do
  begin
    if not (KaufCount=20) then
    begin
      if Random(100)<=30 then
      begin
        SetLength(fKaufList,KaufCount+1);
        fKaufList[KaufCount-1]:=RandomForsch(false);
        CheckWatchForsch(high(fKaufList));
      end;
    end;
  end;

  Sort;
end;

procedure TForschList.DeleteForscher(Index: Integer;FreeWOhnRaum: Boolean);
begin
  if (Index>=0) and (Index<ForscherCount) then
  begin
    FreeForscher(Index);

    if FreeWohnRaum then
      basis_api_FreeWohnRaum(fForscher[Index].BasisID);

    SetLength(fKaufList,Length(fKaufList)+1);
    fKaufList[length(fKaufList)-1]:=fForscher[Index];

    DeleteArray(Addr(fForscher),TypeInfo(TForscherArray),Index);

    Sort;
  end;
end;

procedure TForschList.DeleteProjekt(Index: Integer);
var
  Dummy      : Integer;
  Zaehler    : Integer;
  ID         : Cardinal;
  NextIndex  : Integer;
  OK         : Boolean;

  procedure GotoNextProject;
  begin
    // N�chstes freies Projekt ermitteln
    NextIndex:=-1;
    repeat
      Zaehler:=(Zaehler+1) mod ProjektCount;
      if Projekte[Zaehler].Started and (ProjektStrength(Zaehler)=0) then
        NextIndex:=Zaehler;
    until (NextIndex<>-1) or (Zaehler=Index);
  end;

begin
  if (Index>=0) and (Index<ProjektCount) then
  begin
    Zaehler:=Index;
    ID:=Projekte[Index].ID;

    GotoNextProject;

    // Forscher dem n�chsten Projekt zuweisen
    if NextIndex<>-1 then
    begin
      for Dummy:=0 to ForscherCount-1 do
      begin
        if fForscher[Dummy].ID=ID then
        begin
          OK:=false;
          while not OK do
          begin
            try
              SetForscherProjekt(Dummy,NextIndex);
              OK:=true;
            except
              GotoNextProject;
              if NextIndex=-1 then
                OK:=true;
            end;
          end;
        end;
        if NextIndex=-1 then
          break;
      end;
    end;

    // Projekt l�schen
    DeleteArray(Addr(Projekte),TypeInfo(TForschungenArray),Index);
  end;
end;

destructor TForschList.Destroy;
begin
  SetLength(AlleProjekte,0);
  fNotifyList.Free;
  inherited;
end;

procedure TForschList.DrawDXForscher(Index: integer; Surface: TDirectDrawSurface;
  const Mem: TDDSurfaceDesc; x, y: Integer);
begin
  if (Index<0) or (Index>ForscherCount-1) then exit;
  person_api_DrawFace(Surface,X,Y,fForscher[Index].BitNr);
end;

procedure TForschList.DrawDXKaufForscher(Index: integer; Surface: TDirectDrawSurface;
  const Mem: TDDSurfaceDesc; x, y: Integer);
begin
  if (Index<0) or (Index>KaufCount-1) then exit;
  person_api_DrawFace(Surface,X,Y,fKaufList[Index].BitNr);
end;

procedure TForschList.Clear;
begin
  SetLength(fForscher,0);
  SetLength(Projekte,0);
  SetLength(fKaufList,0);
  SetLength(fCompleteID,0);
  SetLength(fPatents,0);
  SetLength(fWatchs,0);
end;

procedure TForschList.FreeForscher(Index: Integer);
begin
  if fForscher[Index].ID<>0 then
  begin
    basis_api_FreeLaborRaum(fForscher[Index].BasisID);

    fForscher[Index].ID:=0;
  end;
end;

function TForschList.GetForschCount: Integer;
begin
  result:=length(fForscher);
end;

function TForschList.GetForscher(Index: integer): TForscher;
begin
  if (Index<0) or (Index>ForscherCount-1) then
    exit;
  result:=fForscher[Index];
end;

function TForschList.GetForscherProjekt(Index: Integer): Integer;
var
  Dummy: integer;
begin
  result:=-1;
  if (Index<0) or (Index>ForscherCount-1) then
    exit;

  for Dummy:=0 to High(Projekte) do
  begin
    if (Projekte[Dummy].ID=fForscher[Index].ID) and (Projekte[Dummy].Started) then
    begin
      result:=dummy;
      exit;
    end;
  end;
end;

function TForschList.GetIndexOfProjectId(ID: Cardinal): Integer;
var
  Dummy: Integer;
begin
  result:=-1;
  if ID=0 then
    exit;

  for Dummy:=0 to High(AlleProjekte) do
  begin
    if AlleProjekte[Dummy].ID=ID then
    begin
      result:=dummy;
      exit;
    end;
  end;
end;

function TForschList.GetIndexOfRunningProjectId(ID: Cardinal): Integer;
var
  Dummy: Integer;
begin
  result:=-1;
  if ID=0 then
    exit;

  for Dummy:=0 to High(Projekte) do
  begin
    if Projekte[Dummy].ID=ID then
    begin
      result:=dummy;
      exit;
    end;
  end;
end;

function TForschList.GetKaufCount: Integer;
begin
  result:=length(fKaufList);
end;

function TForschList.GetKaufForscher(Index: integer): TForscher;
begin
  FillChar(result,SizeOf(TForscher),#0);
  if (Index<0) or (Index>Length(fKaufList)-1) then
    exit;
  result:=fKaufList[Index];
end;

function TForschList.GetPercentDone(Index: integer): Real;
begin
  result:=0;

  if (Index<0) or (Index>ProjektCount-1) then
    exit;

  if Projekte[Index].Gesamt=0 then
    exit;

  result:=100-((Projekte[Index].Hour/Projekte[Index].Gesamt)*100);
end;

function TForschList.GetProjektCount: Integer;
begin
  result:=length(Projekte);
end;

function TForschList.GetProjektName(Index: integer): String;
var
  Item: PLagerItem;
begin
  if (Index<0) or (Index>ProjektCount-1) then
    exit;

  if (Projekte[Index].ResearchType in [ftNormal,ftItemResearch]) then
    result:=AlleProjekte[GetIndexOfProjectId(Projekte[Index].ID)].Name
  else if (Projekte[Index].ResearchType=ftUpgrade) then
  begin
    Item:=lager_api_GetItem(Projekte[Index].ID);

    Assert(Item<>nil);

    result:=Format(LUpgradeName,[Item.Name,Item.Level+1]);
  end
  else if (Projekte[Index].ResearchType=ftAlienAutopsie) then
  begin
    result:=alien_api_GetAlienInfo(Projekte[Index].ID).Name;
  end;
end;

function TForschList.IDisProjekt(ID: Cardinal): boolean;
var
  Dummy: Integer;
begin
  result:=false;
  if ID=0 then
    exit;

  for Dummy:=0 to ProjektCount-1 do
  begin
    if Projekte[Dummy].ID=ID then
    begin
      result:=true;
      exit;
    end;
  end;
end;

procedure TForschList.LoadFromStream(Stream: TStream);
var
  Dummy  : Integer;
  Rec    : TExtRecord;
begin
  Clear;
  
  Rec:=TExtRecord.Create(TForschListRecord);
  Rec.LoadFromStream(Stream);
  fDay:=Rec.GetInteger('Day');

  with Rec.GetRecordList('Projekte') do
  begin
    SetLength(Projekte,Count);
    for Dummy:=0 to high(Projekte) do
      Projekte[Dummy]:=RecordToForschungen(Item[Dummy]);
  end;

  // Arbeitsmarkteinstellungen speichern
  with Rec.GetRecordList('WatchMarkt') do
  begin
    SetLength(fWatchs,Count);
    for Dummy:=0 to high(fWatchs) do
      fWatchs[Dummy]:=RecordToWatchMarktSettings(Item[Dummy]);
  end;

  // Eigene Forscher speichern
  with Rec.GetRecordList('Forscher') do
  begin
    SetLength(fForscher,Count);
    for Dummy:=0 to high(fForscher) do
      fForscher[Dummy]:=RecordToForscher(Item[Dummy]);
  end;

  // Forscher auf den Arbeitsmarkt speichern
  with Rec.GetRecordList('KaufForscher') do
  begin
    SetLength(fKaufList,Count);
    for Dummy:=0 to high(fKaufList) do
      fKaufList[Dummy]:=RecordToForscher(Item[Dummy]);
  end;

  // beendete Projekte speichern
  with Rec.GetRecordList('EndedProjects') do
  begin
    SetLength(fCompleteID,Count);
    for Dummy:=0 to high(fCompleteID) do
      fCompleteID[Dummy]:=RecordToEndedForschProject(Item[Dummy]);
  end;

  // Laufende Patente abspeichern
  with Rec.GetRecordList('Patente') do
  begin
    SetLength(fPatents,Count);
    for Dummy:=0 to high(fPatents) do
      fPatents[Dummy]:=RecordToPatent(Item[Dummy]);
  end;

  Rec.Free;
  
  Abgleich;
  Sort;
end;

procedure TForschList.NewGameHandler(Sender: TObject);
var
  Dummy: Integer;
begin
  Clear;

  fDay:=0;
  // Startprojekte festlegen
  for Dummy:=0 to High(AlleProjekte) do
  begin
    if not ((AlleProjekte[Dummy].Start) or (AlleProjekte[Dummy].Ver<>0)) then
    begin
      if length(AlleProjekte[Dummy].Parents)=0 then
        AddProjekt(Dummy);
    end;
    {$IFDEF COMPLETERESEARCH}
    if AlleProjekte[Dummy].AlienItem then
    begin
      fNotifyList.CallEvents(EVENT_FORSCHLISTPROJEKTEND,TObject(Addr(AlleProjekte[Dummy])));
    end;
    {$ENDIF}
  end;

  CreateForscher(savegame_api_GetNewGameData.Forscher);
  CreateKaufList;
end;

procedure TForschList.NextDay;
var
  Dummy      : integer;
  Pension    : boolean;
  TrainTime  : Double;
  ForschTime : Double;

  procedure Train(TrainRatio: Double);
  begin
    fForscher[Dummy].Strength := MinReal(fForscher[Dummy].Strength + (TrainRatio/MaxReal(1,fForscher[Dummy].Strength)),100);
  end;

begin
  inc(fDay);
  CheckStartVer;
  CreateKaufList;

  Dummy:=0;
  while (Dummy<ForscherCount) do
  begin
    inc(fForscher[Dummy].Days);
    Pension:=false;
    if fForscher[Dummy].Days>1500 then
    begin
      if fForscher[Dummy].Days>2500 then
        Pension:=(random(10)=5)
      else
        Pension:=(random(30)=random(30));
    end;
    if Pension then
    begin
      savegame_api_FreeMoney(round(fForscher[Dummy].Strength*ForscherBuy),kbFEn);
      savegame_api_message(Format(HPension,[SForscher,fForscher[Dummy].Name,fForscher[Dummy].Days,round(fForscher[Dummy].Strength*ForscherBuy)]),lmPension);

      FreeForscher(Dummy);

      basis_api_FreeWohnRaum(fForscher[Dummy].BasisID);

      DeleteArray(Addr(fForscher),TypeInfo(TForscherArray),Dummy);
      dec(Dummy);
    end
    else
    begin
      if fForscher[Dummy].TrainTime + fForscher[Dummy].ForschTime>0 then
      begin
        if fForscher[Dummy].Strength<100 then
        begin
          try
            TrainTime := fForscher[Dummy].TrainTime/1440;
            ForschTime := fForscher[Dummy].ForschTime/1440;

            if fForscher[Dummy].ForschTime>0 then
              Train(3*ForschTime);

            if fForscher[Dummy].TrainTime>0 then
            begin
              savegame_api_NeedMoney(Round(TrainKost*TrainTime), kbTK);
              Train(20*TrainTime);
            end;

            if fForscher[Dummy].Train and (fForscher[Dummy].Strength=100) then
            begin
              fForscher[Dummy].Train:=false;
              savegame_api_message(Format(MTrainEnd,[SForscher,fForscher[Dummy].Name]),lmTrainEnd);
            end;
          except
            on ENotEnoughMoney do
            begin
              savegame_api_message(Format(ETrainCancel,[fForscher[Dummy].Name]),lmTrainCancel);
              fForscher[Dummy].Train:=false;
            end;
          end;
        end
        else fForscher[Dummy].Train := false;
        
        fForscher[Dummy].TrainTime := 0;
        fForscher[Dummy].ForschTime := 0;
      end;
    end;
    inc(Dummy);
  end;
end;

procedure TForschList.NextRound(Minuten: Integer);
var
  Dummy                   : Integer;
  DummyFortschrittStatus1 : Single;
  DummyFortschrittStatus2 : Single;
  DummyFortschrittIndex1  : Integer;
  DummyFortschrittIndex2  : Integer;
  DummyProjektIndex       : Integer;

  function ForschrittIndex(Rest: Single): Integer;
  begin
    if Trunc(Rest)>20 then //Wenn noch nicht �ber 80%, dann 0% Chance
      Result := -1
    else if Rest<=0 then //Wenn �ber 100% -> Maximale Chance
      Result := 10
    else
    begin
      Result := ((20-Trunc(Rest)) div 2);
    end;
  end;
  
begin
  // Forschungsprojekte
  for Dummy:=0 to ForscherCount-1 do
  begin
    DummyProjektIndex := GetIndexOfRunningProjectId(fForscher[Dummy].ID);
    // Forschung
    if DummyProjektIndex >= 0 then
    begin
      //Dies stimmt nicht ganz, da die Runde l�nger dauern kann, als Projektrestzeit
      Inc(fForscher[Dummy].ForschTime, Minuten);

      //Wie viel Prozent blieb �brig? 
      DummyFortschrittStatus1 := Projekte[DummyProjektIndex].Hour/Projekte[DummyProjektIndex].Gesamt*100;
      
      // Ein Forscher schafft mit Faehigkeit 80 eine Forscher Stunde
      Projekte[DummyProjektIndex].Hour:=Projekte[DummyProjektIndex].Hour-((fForscher[Dummy].Strength/80)*(Minuten/60));

      //Wie viel Prozent bleiben jetzt �brig? 
      DummyFortschrittStatus2 := Projekte[DummyProjektIndex].Hour/Projekte[DummyProjektIndex].Gesamt*100;

      //80 Prozent schon erreicht?
      if DummyFortschrittStatus2<=20 then
      begin
        DummyFortschrittIndex1 := ForschrittIndex(DummyFortschrittStatus1);
        DummyFortschrittIndex2 := ForschrittIndex(DummyFortschrittStatus2);
        //Bei �berschritten von 80%, bei allen 2% mal w�rfeln und gucken, ob Durchbruch kommt
        while ((DummyFortschrittIndex1<10) and (DummyFortschrittIndex1<>DummyFortschrittIndex2)) or (DummyFortschrittIndex1=10) do
        begin
          Inc(DummyFortschrittIndex1);
          if (Random(100)+1)<=5+(DummyFortschrittIndex1*8.5) then //1..100 < als 5 + (8,5 * Index) (also 5..90)
          begin
            ProjektEnd(DummyProjektIndex);
//            if ProjektCount=0 then exit; //Jim: Ist dies noch richtig? z.B. wegen Patente da unten
            if ProjektCount=0 then
              break; // break ist besser
            DummyFortschrittIndex1 := DummyFortschrittIndex2;
          end;   
        end;
      end;
    end
    else if fForscher[Dummy].Train then
    begin
      Inc(fForscher[Dummy].TrainTime, Minuten); 
    end;
  end;

  // Patente berechnen
  for Dummy:=high(fPatents) downto 0 do
  begin
    with fPatents[Dummy] do
    begin
      dec(Zeit,Minuten);
      if Zeit<0 then
      begin
        inc(Woche);
        inc(Zeit,{$IFDEF SHORTPATENT}10{$ELSE}1440*7{$ENDIF});

        savegame_api_FreeMoney(Gebuehr,kbPG);

        if (Woche>=Satt) then
          Anstieg:=1+(Anstieg-1)*(Laufzeit-(Woche-1))/(Laufzeit-(Satt-2));

        Gebuehr:=round(Gebuehr*Anstieg);

        if Woche=Laufzeit then
          DeleteArray(Addr(fPatents),TypeInfo(TPatentArray),Dummy);
      end;
    end;
  end;

end;

function TForschList.ProjektStrength(Index: Integer): Integer;
var
  ID: Cardinal;
  Dummy: Integer;
begin
  result:=0;
  if (Index<0) or (Index>ProjektCount-1) then
    exit;

  ID:=Projekte[Index].ID;
  for Dummy:=0 to ForscherCount-1 do
  begin
    if ID=Forscher[Dummy].ID then
    begin
       inc(result,round(Forscher[Dummy].Strength));
    end;
  end;
end;

function TForschList.RandomForsch(GeniusErlaubt: boolean): TForscher;
var
  Dummy: Integer;
begin
  if GeniusErlaubt then
    result.Strength:=random(60)+40
  else
  begin
    Result.Strength := random(50)+ 25; //25..74
    Dummy := Random(25);  //bis zu 24 mal Gl�ck probieren
    while Dummy>0 do
    begin
      if Random(100)>95 then //wenn Gl�ck gehabt, ein Punkt dazu
        Result.Strength := Result.Strength + 1;
      dec(Dummy);
    end;
  end;

  result.Name:=person_api_GenerateName;
  result.BitNr:=person_api_GenerateFace;

  result.ID:=0;
  result.days:=0;
  result.Train:=false;
  Result.TrainTime := 0;
  Result.ForschTime := 0;
end;

procedure TForschList.SaveToStream(Stream: TStream);
var
  Dummy  : Integer;
  Rec    : TExtRecord;
begin
  Rec:=TExtRecord.Create(TForschListRecord);
  Rec.SetInteger('Day',fDay);

  with Rec.GetRecordList('Projekte') do
  begin
    for Dummy:=0 to high(Projekte) do
      Add(ForschungenToRecord(Projekte[Dummy]));
  end;

  // Arbeitsmarkteinstellungen speichern
  with Rec.GetRecordList('WatchMarkt') do
  begin
    for Dummy:=0 to high(fWatchs) do
      Add(WatchMarktSettingsToRecord(fWatchs[Dummy]));
  end;

  // Eigene Forscher speichern
  with Rec.GetRecordList('Forscher') do
  begin
    for Dummy:=0 to high(fForscher) do
      Add(ForscherToRecord(fForscher[Dummy]));
  end;

  // Forscher auf den Arbeitsmarkt speichern
  with Rec.GetRecordList('KaufForscher') do
  begin
    for Dummy:=0 to high(fKaufList) do
      Add(ForscherToRecord(fKaufList[Dummy]));
  end;

  // beendete Projekte speichern
  with Rec.GetRecordList('EndedProjects') do
  begin
    for Dummy:=0 to high(fCompleteID) do
      Add(EndedForschProjectToRecord(fCompleteID[Dummy]));
  end;

  // Laufende Patente abspeichern
  with Rec.GetRecordList('Patente') do
  begin
    for Dummy:=0 to high(fPatents) do
      Add(PatentToRecord(fPatents[Dummy]));
  end;

  Rec.SaveToStream(Stream);
  Rec.Free;
end;

procedure TForschList.SellForscher(Index: Integer);
begin
  if (Index<0) or (Index>ForscherCount-1) then exit;

  savegame_api_FreeMoney(round(fForscher[Index].Strength*ForscherSell),kbFVK);

  DeleteForscher(Index);
end;

procedure TForschList.SetForscherProjekt(Index: Integer;
  const Value: Integer);
var
  Anzahl: Integer;
begin
  if (Index<0) or (Index>ForscherCount-1) then exit;
  if (Value<0) or (Value>ProjektCount-1) then exit;
  if Projekte[Value].BasisID<>0 then
  begin
    if (fForscher[Index].BasisID<>Projekte[Value].BasisID) and (ProjektStrength(Value)>0) then
      raise Exception.Create(Format(EInvalidBasis,[ProjektName[Value]]));
  end;

  // Pr�fen, ob f�r Alienausr�stung genug im Lager ist
  if (Projekte[Value].ResearchItem) and (Projekte[Value].ResearchCount>0) then
  begin
    if not lager_api_DeleteItem(fForscher[Index].BasisID,Projekte[Value].ID,Projekte[Value].ResearchCount) then
      raise Exception.Create(Format(ST0311150001,[ProjektName[Value],Projekte[Value].ResearchCount]));

    Projekte[Value].ResearchCount:=0;
  end;

  // Raum f�r die Forschung belegen
  if fForscher[Index].ID=0 then
    basis_api_NeedLaborRaum(fForscher[Index].BasisID);

  fForscher[Index].ID:=Projekte[Value].ID;
  Projekte[Value].BasisID:=fForscher[Index].BasisID;
  fForscher[Index].Train:=false;
end;

procedure TForschList.ChangeTraining(Index: Integer);
begin
  if (Index<0) or (Index>ForscherCount-1) then
    exit;

  if fForscher[Index].Strength=100 then
    raise Exception.Create(Format(ENoTrainNeed,[SForscher,fForscher[Index].Name]));

  fForscher[Index].Train:=not fForscher[Index].Train;
  FreeForscher(Index);
end;

procedure TForschList.SetTraining(Index: Integer);
begin
  if (Index<0) or (Index>ForscherCount-1) then exit;
  if fForscher[Index].Strength=100 then
    raise Exception.Create(Format(ENoTrainNeed,[SForscher,fForscher[Index].Name]));
  fForscher[Index].Train:=true;
  FreeForscher(Index);
end;

procedure TForschList.RenameForscher(Index: Integer; Name: String);
begin
  fForscher[Index].Name:=Name;
end;

procedure TForschList.Abgleich;
var
  Dummy    : Integer;
  Dummy1   : Integer;
  Found    : boolean;
  ID       : Cardinal;
  Count    : Integer;
begin
  Dummy:=0;
  Count:=length(Projekte);
  while (Dummy<Count) do
  begin
    // Alle Projekte die jetzt erforscht werden bzw. Upgrades, m�ssen sichtbar
    // gemacht werden (ABW�RTSKOMPATIBILIT�T)
    if (ProjektStrength(Dummy)>0) or (Projekte[Dummy].ResearchType=ftUpgrade) then
      Projekte[Dummy].Started:=true;

    if Projekte[Dummy].ResearchType=ftAlienAutopsie then
    begin
      inc(Dummy);
      continue;
    end;

    ID:=Projekte[Dummy].ID;
    Found:=false;
    for Dummy1:=0 to High(AlleProjekte) do
    begin
      if AlleProjekte[Dummy1].ID=ID then
      begin
        Found:=true;
        if not AlleProjekte[Dummy1].AlienItem then
        begin
          if AlleProjekte[Dummy1].ResearchInfo='' then
            Projekte[Dummy].Info:=AlleProjekte[Dummy1].Info
          else
            Projekte[Dummy].Info:=AlleProjekte[Dummy1].ResearchInfo;
        end;
      end;
    end;
    if not Found then
    begin
      DeleteProjekt(Dummy);
      dec(Count);
    end
    else
      inc(Dummy);
  end;

  CheckProjekts;
end;

procedure TForschList.StartUpgrade(const Item: TLagerItem);
var
  PIndex   : Integer;
  ItemUpg  : TItemUpgrade;
begin
  // Ausr�stung erforscht?
  if not Item.Useable then
    raise Exception.Create(ST0310250003);

  // Konstruktionspl�ne verf�gbar
  if not Item.HerstellBar then
    raise Exception.Create(Format(ENoUpgrade,[Item.Name]));

  // Alienausr�stung
  if Item.AlienItem then
    raise Exception.Create(Format(ST0409110001,[Item.Name]));

  // Upgrade l�uft bereits
  if IndexOfUpgrade(Item.ID)<>-1 then
    exit;

  SetLength(Projekte,Length(Projekte)+1);
  PIndex:=high(Projekte);

  ItemUpg.Old:=Item;
  lager_api_UpgradeItem(ItemUpg);

  Projekte[PIndex].ID:=Item.ID;
  Projekte[PIndex].Started:=true;
  // Zeit wird bereits durch UpgradeItem korrekt berechnet
  Projekte[PIndex].Hour:=ItemUpg.Time;

  Projekte[PIndex].Gesamt:=Projekte[PIndex].Hour;

  Projekte[PIndex].TypeId:=ptNone;
  Projekte[PIndex].ParentID:=0;
  Projekte[PIndex].Update:=true;
  Projekte[PIndex].Info:=Format(ST0309230031,[Item.Name]);

  Projekte[PIndex].ResearchType:=ftUpgrade;
end;

function TForschList.IndexOfUpgrade(ID: Cardinal): Integer;
var
  Dummy: Integer;
begin
  result:=-1;
  for Dummy:=0 to ProjektCount-1 do
  begin
    if (Projekte[Dummy].ResearchType=ftUpgrade) and (Projekte[Dummy].ID=ID) then
    begin
      result:=Dummy;
      exit;
    end;
  end;
end;

function TForschList.GetProjektText(Index: integer): String;
begin
  result:=Projekte[Index].Info;
end;

procedure TForschList.CheckStartVer;
var
  Dummy    : Integer;
  Text     : String;
  Org      : String;
begin
  for Dummy:=0 to High(AlleProjekte) do
  begin
    if (AlleProjekte[Dummy].Ver=fDay) and (AlleProjekte[Dummy].Ver<>0) then
    begin
      if (AlleProjekte[Dummy].Start) and (not AlleProjekte[Dummy].AlienItem) then
      begin
        // Pr�fen ob neue Ausr�stung verf�gbar ist
        Text:=LNewItems;

        fNotifyList.CallEvents(EVENT_FORSCHLISTPROJEKTEND,TObject(Addr(AlleProjekte[Dummy])));

        // Nachricht erstellen: Neue Ausr�stung verf�gbar
        Text:=Text+'    '+AlleProjekte[Dummy].Name+' ('+game_utils_TypeToStr(AlleProjekte[Dummy].TypeID)+')';
        if AlleProjekte[Dummy].Land<>-1 then
        begin
          Org:=country_api_GetCountryName(AlleProjekte[Dummy].Land);

          if Org<>'' then
            Text:=Text+#10'    '+IAMadeIn+' '+Org;
        end;
        if (AlleProjekte[Dummy].TypeID in LagerItemType) and (AlleProjekte[Dummy].Herstellbar) then
          Text:=Text+LConstruction;
        savegame_api_message(Text,lmNewItems);
      end;
    end;
  end;

  CheckProjekts;
end;

function TForschList.ForscherInSelectedBasis(Index: Integer): boolean;
begin
  result:=fForscher[Index].BasisID=basis_api_GetSelectedBasis.ID;
end;

function TForschList.ProjektInSelectedBasis(Index: Integer): boolean;
begin
  result:=(ProjektStrength(Index)=0) or (Projekte[Index].BasisID=basis_api_GetSelectedBasis.ID);
end;

function TForschList.GetProjektBasis(Index: Integer): TBasis;
begin
  result:=nil;
  if (ProjektStrength(Index)=0) then
    exit;
  result:=basis_api_GetBasisFromID(Projekte[Index].BasisID);
end;

procedure TForschList.CheckNewItems;
var
  Dummy: Integer;
begin
  for Dummy:=High(AlleProjekte) downto 0 do
  begin
    if AlleProjekte[Dummy].Start then
    begin
      // Startausr�stung erg�nzen
      // �berpr�fen, ob es eventuell schon eingetragen wurde
      if (lager_api_GetItemIndex(AlleProjekte[Dummy].ID,false)=-1) and ((AlleProjekte[Dummy].Ver=0) or (AlleProjekte[Dummy].Ver<=fDay)) then
        lager_api_AddForschItem(AlleProjekte[Dummy]);
    end;
  end;
end;

procedure TForschList.CancelWatch(BasisID: Cardinal);
var
  Dummy: Integer;
begin
  for Dummy:=0 to high(fWatchs) do
  begin
    if fWatchs[Dummy].Active and (fWatchs[Dummy].BasisID=BasisID) then
    begin
      DeleteArray(Addr(fWatchs),TypeInfo(TWatchMarktArray),Dummy);
      exit;
    end;
  end;
end;

function TForschList.GetWatch(BasisID: Cardinal;
  var Watch: TWatchMarktSettings): Boolean;
var
  Dummy: Integer;
begin
  result:=false;
  for Dummy:=0 to high(fWatchs) do
  begin
    if fWatchs[Dummy].Active and (fWatchs[Dummy].BasisID=BasisID) then
    begin
      Watch:=fWatchs[Dummy];
      result:=true;
      exit;
    end;
  end;
end;

procedure TForschList.SetWatch(Basis: Cardinal; minStrength: Integer;
  WType: TWatchType);
var
  Dummy: Integer;
begin
  CancelWatch(Basis);

  SetLength(fWatchs,length(fWatchs)+1);

  with fWatchs[high(fWatchs)] do
  begin
    BasisID:=Basis;
    Active:=true;
    minFaehigkeiten:=minStrength;
    WatchType:=WType;
  end;
end;

procedure TForschList.CheckProjekts;
var
  Dummy   : Integer;
  Mess    : String;
begin
  Mess:='';

  for Dummy:=0 to high(AlleProjekte) do
  begin
    if InProjekte(AlleProjekte[Dummy].ID) or InCompleteID(AlleProjekte[Dummy].ID) then
      continue;

    if CanForschProjekt(Dummy) then
    begin
      if AddProjekt(Dummy) then
        Mess:=Mess+#10'    '+AlleProjekte[Dummy].Name+' ('+game_utils_TypeToStr(AlleProjekte[Dummy].TypeID)+')';
    end;
  end;

  if Mess<>'' then
    savegame_api_Message(LNewProjekts+Mess,lmNewItems);
end;

function TForschList.InCompleteID(ID: Cardinal): boolean;
var
  Dummy: Integer;
begin
  result:=false;
  for Dummy:=0 to high(fCompleteID) do
  begin
    if fCompleteID[Dummy].ID=ID then
    begin
      Result:=true;
      exit;
    end;
  end;
end;

function TForschList.InProjekte(ID: Cardinal): boolean;
var
  Dummy: Integer;
begin
  result:=false;
  for Dummy:=0 to high(Projekte) do
  begin
    if Projekte[Dummy].ID=ID then
    begin
      result:=true;
      exit;
    end;
  end;
end;

function TForschList.CanForschProjekt(Index: Integer): boolean;
var
  Dummy  : Integer;
begin
  if ((AlleProjekte[Index].Ver>0) and (AlleProjekte[Index].Ver>fDay)) or (AlleProjekte[Index].Start) then
  begin
    result:=false;
    exit;
  end;
  result:=true;
  for Dummy:=0 to high(AlleProjekte[Index].Parents) do
  begin
    if not InCompleteID(AlleProjekte[Index].Parents[Dummy]) then
    begin
      result:=false;
      exit;
    end;
  end;
end;

function TForschList.SortFunc(KForscher1, KForscher2: Integer;
  Typ: TFunctionType): Integer;
var
  Forscher : TForscher;
  List     : TForscherArray;
begin
  if fSortKauf then
    List:=fKaufList
  else
    List:=fForscher;
  case Typ of
    ftCompare  : result:=round((List[KForscher2].Strength-List[KForscher1].Strength)*100);
    ftExchange :
    begin
      Forscher:=List[KForscher2];
      List[KForscher2]:=List[KForscher1];
      List[KForscher1]:=Forscher;
    end;
  end;
end;

procedure TForschList.Sort;
begin
  if fNotSort then
    exit;

  fSortKauf:=true;
  QuickSort(0,High(fKaufList),SortFunc);

  fSortKauf:=false;
  QuickSort(0,High(fForscher),SortFunc);
end;

procedure TForschList.AddAlienItemResearch(ItemID: Cardinal);
var
  Index: Integer;
  Text : String;
begin
  // Ausr�stung wird bereits erforscht bzw. wurde bereits erforscht
  if InProjekte(ItemID) or InCompleteID(ItemID) then
    exit;

  Index:=GetIndexOfProjectId(ItemID);

  Assert(Index<>-1);

  AddProjekt(Index);

  // Nachricht 'Ausr�stung x kann erforscht werden' absenden
  Text:=Format(ST0311170001,[AlleProjekte[Index].Name]);

  if AlleProjekte[Index].ResearchCount=0 then
    Text:=Text+ST0401210002
  else
    Text:=Text+Format(ST0401210001,[AlleProjekte[Index].ResearchCount]);

  savegame_api_message(Text,lmNewItems);
end;

procedure TForschList.ProjektEnd(Index: Integer);
var
  Projekt: TForschProject;

  procedure AddToCompleteID;
  begin
    SetLength(fCompleteID,length(fCompleteID)+1);
    with fCompleteID[high(fCompleteID)] do
    begin
      ID:=Projekte[Index].ID;
      ParentID:=Projekte[Index].ParentID;
      Name:=GetProjektName(Index);
      TypeID:=Projekte[Index].TypeId;
      {$IFDEF WRITEENDED}
        GlobalFile.Write('Name',Name);
        GlobalFile.Write('ID',ID);
        GlobalFile.Write('ParentID',ParentID);
        GlobalFile.Write('*',60);
      {$ENDIF}
    end;
  end;

begin
  fLastProjekt:=Projekte[Index];

  AddToCompleteID;

  if not (Projekte[Index].ResearchType=ftUpgrade) then
  begin

    if Projekte[Index].ResearchType in [ftItemResearch,ftNormal] then
    begin
      Projekt:=AlleProjekte[GetIndexOfProjectId(Projekte[Index].ID)];
      Projekt.Land:=-1;
      Projekt.Herstellbar:=true;

      fNotifyList.CallEvents(EVENT_FORSCHLISTPROJEKTEND,TObject(Addr(Projekt)));

      //
      if not Projekt.AlienItem then
      begin
        // normales Forschungsprojekt
        savegame_api_BuchPunkte(15,pbForsch);
        savegame_api_Message(Format(MForschEnd,[Projekt.Name]),lmProjektEnd,Self);
        country_api_PayForschMoney(Projekt);
      end
      else
      begin
        // Erforschung einer Ausr�stung (in der Regel Alienausr�stung)
        savegame_api_BuchPunkte(30,pbForsch);
        lager_api_ResearchComplete(Projekt.ID);
        savegame_api_Message(Format(ST0310260002,[Projekt.Name]),lmProjektEnd,Self);
      end;

      // Patentgewinn eintragen
      if Projekt.PatentGebuehr>0 then
      begin
        SetLength(fPatents,length(fPatents)+1);
        with fPatents[high(fPatents)] do
        begin
          Gebuehr:=Projekt.Patentgebuehr;
          Laufzeit:=Projekt.Patentzeit;
          Woche:=0;
          // Eine Woche
          Zeit:={$IFDEF SHORTPATENT}10{$ELSE}1440*7{$ENDIF};
          Anstieg:=1+(Projekt.Patentanstieg/100);
          Satt:=Projekt.PatentSatt;
        end;
      end;
    end
    else if Projekte[Index].ResearchType=ftAlienAutopsie then
      alien_api_AutopsieEnd(Projekte[Index].ID);

    CheckProjekts;
  end
  else
  begin
    savegame_api_BuchPunkte(20,pbUpgrade);
    fNotifyList.CallEvents(EVENT_FORSCHLISTUPGRADEEND,TObject(Projekte[Index].ID));
  end;
  DeleteProjekt(Index);
  CorrectForscher;
end;

procedure TForschList.AddAlienAutopsie(const Alien: TAlien);
var
  PIndex: Integer;
begin
  if InProjekte(Alien.ID) or InCompleteID(Alien.ID) then
    exit;

  SetLength(Projekte,Length(Projekte)+1);
  PIndex:=high(Projekte);

  Projekte[PIndex].ID:=Alien.ID;
  Projekte[PIndex].Hour:=forsch_api_calculateForschTime(Alien.AutopTime);
  Projekte[PIndex].Gesamt:=Projekte[PIndex].Hour;

  Projekte[PIndex].Started:=false;
  Projekte[PIndex].TypeId:=ptNone;
  Projekte[PIndex].ParentID:=0;

  if Alien.ResearchInfo<>'' then
    Projekte[PIndex].Info:=Alien.ResearchInfo
  else
    Projekte[PIndex].Info:=Format(ST0311300002,[Alien.Name]);

  Projekte[PIndex].ResearchType:=ftAlienAutopsie;

  savegame_api_Message(Format(ST0405250001,[Alien.Name]),lmNewItems);
end;

function TForschList.GetCompletedProjects: Integer;
begin
  result:=length(fCompleteID);
end;

function TForschList.CompletedAsProject(Index: Integer;
  out Projekt: TForschProject): Boolean;
var
  ProjIndex: Integer;
begin
  ProjIndex:=GetIndexOfProjectId(fCompleteID[Index].ID);
  result:=false;

  // Projekt ist vermutlich eine Alienautopsie
  if ProjIndex=-1 then
    exit;

  Projekt:=AlleProjekte[ProjIndex];
  result:=true;
end;

function TForschList.GetForschProject(Index: Integer): TForschProject;
var
  ProIndex: Integer;
begin
  Assert((Index>=0) and (Index<=high(AlleProjekte)));

  result:=AlleProjekte[Index];
end;

function TForschList.GetProjektInfo(Index: Integer): TForschungen;
begin
  if (Index<0) or (Index>ProjektCount-1) then
    exit;

  result:=Projekte[Index];
end;

procedure TForschList.StartResearch(Index: Integer);
var
  Dummy: Integer;
  Temp : TForschungen;
begin
  if (Index<0) or (Index>ProjektCount-1) then
    exit;

  Projekte[Index].Started:=true;

  // Projekt nach Hinten schieben
  Temp:=Projekte[Index];
  for Dummy:=Index to high(Projekte)-1 do
    Projekte[Dummy]:=Projekte[Dummy+1];

  Projekte[high(Projekte)]:=Temp;

end;

procedure TForschList.CheckWatchForsch(Index: Integer);
var
  Dummy       : Integer;
  Defmessage  : String;
  Forscher    : TForscher;

  function SellWeakForscher(BasisID: Cardinal): TForscher;
  var
    Dummy: Integer;
    Index: Integer;
  begin
    result.Name:='';
    if length(fForscher)=0 then
      exit;

    Index:=-1;
    for Dummy:=0 to high(fForscher) do
    begin
      if (Index=-1) and (fForscher[Dummy].BasisID=BasisID) then
      begin
        result:=fForscher[Dummy];
        Index:=Dummy;
      end
      else if (result.Strength>fForscher[Dummy].Strength) and (fForscher[Dummy].BasisID=BasisID) then
      begin
        result:=fForscher[Dummy];
        Index:=Dummy;
      end;
    end;
    if Index<>-1 then
      SellForscher(Index);
  end;

  function AllForscherBetter(BasisID: Cardinal; Faehigkeit: single): boolean;
  var
    Dummy: Integer;
  begin
    result:=true;
    for Dummy:=0 to high(fForscher) do
    begin
      if (fForscher[Dummy].BasisID=BasisID) and (fForscher[Dummy].Strength<Faehigkeit) then
      begin
        result:=false;
        exit;
      end;
    end;
  end;

begin
  fNotSort:=true;
  for Dummy:=0 to high(fWatchs) do
  begin
    // �berwachung f�r Basis aktiv und Forscher Besser als Erwartung
    if (not fWatchs[Dummy].Active) or (fKaufList[Index].Strength<fWatchs[Dummy].minFaehigkeiten) then
      continue;

    // Nachricht: xxx entspricht ihren Erwartungen
    if basis_api_GetBasisCount>1 then
      DefMessage:=Format(ST0309230026,[fKaufList[Index].Name,basis_api_GetBasisFromID(fWatchs[Dummy].BasisID).Name])
    else
      DefMessage:=Format(ST0309230027,[fKaufList[Index].Name]);

    if fWatchs[Dummy].WatchType=wtMessage then
    begin
      // Nachricht absenden
      savegame_api_message(DefMessage,lmFindPerson);
      break;
    end;

    try
      Forscher.Name:='';
      if fWatchs[Dummy].WatchType=wtExchange then
      begin
        if AllForscherBetter(fWatchs[Dummy].BasisID,fKaufList[KaufCount-1].Strength) then
        begin
          // Nicht angeheuert, da alle Forscher Besser sind
          savegame_api_message(DefMessage+#10+Format(ST0311210002,[SForscher]),lmFindPerson);
          break;
        end;

        // Schw�chsten Forscher entlassen
        Forscher:=SellWeakForscher(fWatchs[Dummy].BasisID);
      end;
      BuyForscher(Index,fWatchs[Dummy].BasisID);
      DefMessage:=DefMessage+#10+ST0309230028;

      if Forscher.Name<>'' then
      begin
        // Ein anderer Forscher wurde entlassen (wtExchange)
        DefMessage:=DefMessage+#10+Format(ST0311210001,[Forscher.Name]);
        // Neuen Forscher das Projekt weiter erforschen lassen
        ForscherProjekt[high(fForscher)]:=GetIndexOfProjectId(Forscher.ID);
      end;

      savegame_api_message(DefMessage,lmFindPerson);
    except
      on E: ENotEnoughMoney do
        savegame_api_message(DefMessage+#10+ST0309230029,lmReserved);
      on E: ENotEnoughRoom do
        savegame_api_message(DefMessage+#10+ST0309230030,lmReserved);
    end;
  end;
  fNotSort:=false;
end;

procedure TForschList.MoveProject(Index: Integer; Down: Boolean);
var
  Temp: TForschungen;
  MoveTo: Integer;
begin
  Temp:=Projekte[Index];
  MoveTo:=Index;
  if Down then
  begin
    repeat
      MoveTo:=MoveTo+1;
      if MoveTo>high(Projekte) then
        exit;
    until Projekte[MoveTo].Started;
  end
  else
  begin
    repeat
      MoveTo:=MoveTo-1;
      if MoveTo<0 then
        exit;
    until Projekte[MoveTo].Started;
  end;
  Projekte[Index]:=Projekte[MoveTo];
  Projekte[MoveTo]:=Temp;
end;

procedure TForschList.CancelResearch(Index: Integer);
begin
  if PercentDone[Index]>0 then
    raise Exception.Create(ST0402160002);

  if Projekte[Index].ResearchType=ftUpgrade then
    DeleteProjekt(Index)
  else
    Projekte[Index].Started:=false;
end;

function TForschList.CalculateWeeklySold: Integer;
var
  Dummy: Integer;
begin
  result:=0;
  for Dummy:=0 to ForscherCount-1 do
    inc(result,round(fForscher[Dummy].Strength*ForscherBuy/100));
end;

function TForschList.CalculateWeeklySold(BasisID: Cardinal): Integer;
var
  Dummy: Integer;
begin
  result:=0;
  for Dummy:=0 to ForscherCount-1 do
    if fForscher[Dummy].BasisID=BasisID then
      inc(result,round(fForscher[Dummy].Strength*ForscherBuy/100));
end;

procedure TForschList.LoadGameSetHandler(Sender: TObject);
var
  Dummy    : Integer;
  Projekts : TRecordArray;
begin
  Projekts:=savegame_api_GameSetGetProjects;
  SetLength(AlleProjekte,length(Projekts));

  for Dummy:=0 to high(Projekts) do
  begin
    AlleProjekte[Dummy]:=RecordToProject(Projekts[Dummy]);
  end;
end;

procedure TForschList.AfterLoadHandler(Sender: TObject);
begin
  CheckNewItems;
end;

function TForschList.GetLastProject: TForschungen;
begin
  result:=fLastProjekt;
end;

function TForschList.IndexOfCompletedProject(ID: Cardinal): Integer;
var
  Dummy: Integer;
begin
  result:=-1;
  for Dummy:=0 to high(fCompleteID) do
  begin
    if fCompleteID[Dummy].ID=ID then
    begin
      result:=Dummy;
      exit;
    end;
  end;
end;

function TForschList.CheckLaborRoom(BaseID: Cardinal): String;
var
  Dummy: Integer;
  Count: Integer;
  Res  : Integer;
begin
  Count:=0;
  for Dummy:=0 to high(fForscher) do
  begin
    if (fForscher[Dummy].BasisID=BaseID) and (GetForscherProjekt(Dummy)<>-1) then
      inc(Count);
  end;

  Assert(Count>=basis_api_FreierLaborRaum(BaseID));
  Res:=0;
  Dummy:=high(fForscher);
  while (Dummy>=0) and (Count>basis_api_FreierLaborRaum(BaseID)) do
  begin
    if (fForscher[Dummy].BasisID=BaseID) and (GetForscherProjekt(Dummy)<>-1) then
    begin
      FreeForscher(Dummy);
      dec(Count);
      inc(Res);
    end;
    dec(Dummy);
  end;
  if Res>0 then
    result:=Format(ST0503100001,[Res,SForscher])
  else
    result:='';
end;

function TForschList.CountInBase(BasisID: Cardinal): Integer;
var
  Dummy: Integer;
begin
  Result := 0;
  for Dummy:=0 to High(fForscher) do
    if fForscher[Dummy].BasisID=BasisID then
      inc(Result);
end;

initialization

  TForschListRecord:=TExtRecordDefinition.Create('TForschListRecord');
  TForschListRecord.AddInteger('Day',low(Integer),high(Integer));

  TForschListRecord.AddRecordList('Projekte',RecordForschung);
  TForschListRecord.AddRecordList('WatchMarkt',RecordWatchMarktSettings);
  TForschListRecord.AddRecordList('Forscher',RecordForscher);
  TForschListRecord.AddRecordList('KaufForscher',RecordForscher);
  TForschListRecord.AddRecordList('EndedProjects',RecordEndedForschProject);
  TForschListRecord.AddRecordList('Patente',RecordPatent);

end.

