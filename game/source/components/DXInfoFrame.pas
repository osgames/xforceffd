{********************************************************************************
*										*
* Copyright (C) 2004  X-Force Team - jim_raynor@web.de				*
*										*
* This file is part of X-Force: Fight For Destiny (X-Force).			*
*										*
* X-Force is free software; you can redistribute it and/or modify		*
* it under the terms of the GNU General Public License as published by		*
* the Free Software Foundation; either version 2 of the License, or		*
* (at your option) any later version.						*
*										*
* X-Force is distributed in the hope that it will be useful,			*
* but WITHOUT ANY WARRANTY; without even the implied warranty of		*
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the			*
* GNU General Public License for more details.					*
*										*
* You should have received a copy of the GNU General Public License		*
* along with X-Force; if not, write to the Free Software			*
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA	*
*										*
*********************************************************************************
*										*
* InfoFrame der auf den meisten Verwaltungsseiten am oberen Bildschirmrand	*
* zu finden ist									*
*										*
********************************************************************************}

{$I ../Settings.inc}

unit DXInfoFrame;

interface


uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  DXContainer, KD4SaveGame, DXDraws, Blending, XForce_types, DXBitmapButton,
  KD4Page, DXListBoxWindow, Defines, BasisListe, TraceFile, DirectDraw,
  DirectFont, math, StringConst;

type

  TInfoColorSheme = (icsRed,icsBlue,icsGreen);

  TColorRecord = record
    BackColor   : TBlendColor;
    PenColor    : Array[boolean] of TBlendColor;
    FirstColor  : Array[boolean] of TColor;
    SecondColor : TColor;
  end;

type
  TDXPageNavigator = class;

  TDXInfoFrame = class(TDXComponent)
  private
    fFrameInfos     : TFrameInfos;
    fGameInfo       : boolean;
    fSelectBase     : TDXBitmapButton;
    fKeyChange      : boolean;
    fColorSheme     : TInfoColorSheme;
    fBlend          : TBlendColor;
    fBorder         : TBlendColor;
    fAccel          : Integer;
    fClosePage      : TDXBitmapButton;
    fReturnPage     : Integer;
    procedure SetFrameInfo(const Value: TFrameInfos);
    procedure SetGameInfo(const Value: boolean);
    procedure SelectBaseClick(Sender: TObject);
    procedure AktuBorderColor;
    procedure SetKeyChange(const Value: boolean);
    procedure KeyChangeBasis(BasisIndex: Integer);
    procedure InitColorTable;
    function ExpandSize(Sender: TObject;Frames: Integer): boolean;
    function CollapsSize(Sender: TObject;Frames: Integer): boolean;
    procedure Collaps;

    procedure SetSelectedBasis(Basis: TBasis);
    procedure ClosePageClick(Sender: TObject);

    procedure DrawCloseButton(Sender: TDXComponent;Surface: TDirectDrawSurface;var Mem: TDDSurfaceDesc);
  protected
    { Protected-Deklarationen }
  public
    constructor Create(Page: TDXPage);override;
    procedure Resize(var NewLeft,NewTop,NewWidth,NewHeight: Integer);override;
    procedure Draw(Surface: TDirectDrawSurface;var Mem: TDDSurfaceDesc);override;
    procedure MouseMove(X,Y: Integer);override;
    procedure MouseLeave;override;
    procedure Reset;

    property Infos           : TFrameInfos read fFrameInfos write SetFrameInfo;
    property ColorSheme      : TInfoColorSheme read fColorSheme write fColorSheme;
    property GameInfo        : boolean read fGameInfo write SetGameInfo;
    property SystemKeyChange : boolean read fKeyChange write SetKeyChange;

    property ReturnPage      : Integer read fReturnPage write fReturnPage;
  end;

  TDXPageNavigator = class(TDXComponent)
  private
    fAccel          : Integer;
    function ExpandSize(Sender: TObject;Frames: Integer): boolean;
    function CollapsSize(Sender: TObject;Frames: Integer): boolean;
    function CheckCollapsing(Sender: TObject;Frames: Integer): boolean;
    procedure CorrectButtonPositions;

    procedure ButtonClick(Sender: TObject);
  public
    constructor Create(Page: TDXPage);override;
    destructor Destroy;override;
    procedure AktuFKeyInfo;
    procedure MouseMove(X,Y: Integer);override;
    procedure Draw(Surface: TDirectDrawSurface;var Mem: TDDSurfaceDesc);override;
    procedure Resize(var NewLeft,NewTop,NewWidth,NewHeight: Integer);override;
    procedure RegisterPage(Caption: String; Index: Integer);
  end;

type
  TFKeyArray = Array[1..12] of Shortint;

var
  FKeys               : TFKeyArray;

const
  FDefaultKeys        : TFKeyArray = (3,7,9,11,16,17,18,20,22,-1,-1,12);

implementation

uses
  basis_api, savegame_api;

const
  NormalSize   = 44;
  PopupSize    = 44;
  ButtonMargin = 8;
  TextMargin   = 30;
  ButtonHeight = 25;

var
  GlobalBasisSelector : TDXListBoxWindow = nil;
  PageList            : TStringList = nil;
  MaxNavigatorSize    : Integer = 50;
  ColorTableInit      : Boolean = false;
  ColorTable          : Array[TInfoColorSheme] of TColorRecord;

{ TDXInfoFrame }

procedure TDXInfoFrame.AktuBorderColor;
begin
  with ColorTable[fColorSheme] do
  begin
    fBlend:=BackColor;
    fBorder:=PenColor[AlphaElements];
    if fSelectBase<>nil then
    begin
      fSelectBase.BlendColor:=fBlend;
      fSelectBase.FirstColor:=FirstColor[AlphaElements];
      fSelectBase.SecondColor:=SecondColor;
    end;
    if fClosePage<>nil then
    begin
      fClosePage.BlendColor:=fBlend;
      fClosePage.FirstColor:=FirstColor[AlphaElements];
      fClosePage.SecondColor:=SecondColor;
    end;
  end;
end;

procedure TDXInfoFrame.Collaps;
begin
  if Height>NormalSize then
  begin
    Container.AddFrameFunction(CollapsSize,Self,25);
    Container.DeleteFrameFunction(ExpandSize,Self);
  end;
end;

function TDXInfoFrame.CollapsSize(Sender: TObject;
  Frames: Integer): boolean;
begin
  dec(fAccel);
  Height:=Height+fAccel;
  if Height<=NormalSize then
  begin
    fAccel:=0;
    Container.DeleteFrameFunction(CollapsSize,Self);
  end;
  result:=true;
end;

constructor TDXInfoFrame.Create(Page: TDXPage);
begin
  inherited;

  InitColorTable;
  fColorSheme:=icsRed;
  fKeyChange:=false;
  GameInfo:=false;
  Height:=21;

  fReturnPage:=-1;

  fSelectBase:=TDXBitmapButton.Create(Page);
  with fSelectBase do
  begin
    Text:='...';
    RoundCorners:=rcNone;
    TKD4Page(Page).SetButtonFont(Font);
    BlendColor:=bcMaroon;
    FirstColor:=clMaroon;
    SecondColor:=clRed;
    OnClick:=SelectBaseClick;
    CornerWidth:=8;
  end;

  fClosePage:=TDXBitmapButton.Create(Page);
  with fClosePage do
  begin
    Hint:=HCloseButton;
    RoundCorners:=rcAll;
    TKD4Page(Page).SetButtonFont(Font);
    BlendColor:=bcBlack;
    FirstColor:=clBlack;
    SecondColor:=clDkGray;
    AccerlateColor:=coAccerlateColor;
    OnClick:=ClosePageClick;
    RoundCorners:=rcNone;
    CornerWidth:=8;
    EscButton:=true;
    OnOwnerDraw:=DrawCloseButton;
  end;

  { die erste instanzierung erzeugt eine ListBox zum ausw�hlen der Basis }
  if GlobalBasisSelector=nil then
  begin
    GlobalBasisSelector:=TDXListBoxWindow.Create(Page);
    with GlobalBasisSelector do
    begin
      SetRect(0,0,300,200);
      Caption:=CChooseBase;
      TKD4Page(Page).SetButtonFont(Font);
      CaptionColor:=coFontColor;
      AccerlateColor:=coAccerlateColor;
      Font.Size:=coFontSize;
      FirstColor:=$00400000;
      SecondColor:=clBlue;
      BlendColor:=bcDarkNavy;
    end;
  end;
end;

procedure TDXInfoFrame.Draw(Surface: TDirectDrawSurface;var Mem: TDDSurfaceDesc);
var
  FrameCount : Integer;
  FrameDraw  : Integer;
  Dummy      : TFrameInfo;
  Corner     : TCorners;
  FrameWidth : double;
  Text       : string;

//  procedure DrawText(Rect: TRect; Text: String;Align: TAlignment;Corners: TCorners);
  procedure DrawText(LeftPos, BoxWidth: Integer; NormalFrame: Boolean; Text: String; Align: TAlignment;Corners: TCorners);
  var
    X        : Integer;
    Top      : Integer;
  begin
    if NormalFrame then
    begin
      Exclude(Corners,cRightTop);
      Exclude(Corners,cLeftTop);
      Top:=4+(Height-25);
    end
    else
    begin
      Exclude(Corners,cRightBottom);
      Exclude(Corners,cLeftBottom);
      Top:=Height-40;
    end;
    FramingRect(Surface,Mem,Bounds(LeftPos,Top,BoxWidth,18),Corners,8,fBorder);
    case Align of
      taLeftJustify   : X:=LeftPos+4;
      taRightJustify  : X:=(LeftPos+BoxWidth)-4-WhiteBStdFont.TextWidth(Text);
      taCenter        : X:=LeftPos+(BoxWidth shr 1)-(WhiteBStdFont.TextWidth(Text) shr 1);
    end;
    WhiteBStdFont.TextRect(Surface,Bounds(LeftPos,Top,BoxWidth,18),X,Top+2,Text);
  end;

  function GetFrameRect: TRect;
  begin
    Result.Top:=Bottom-21;
    Result.Bottom:=Bottom-3;
    Result.Left:=182+round(FrameWidth*FrameDraw);
    inc(FrameDraw);
    Result.Right:=182+round(FrameWidth*FrameDraw)-1;
  end;

  procedure DrawInfo(Info: TFrameInfo);
  var
    FrameRect  : TRect;
    Caption    : String;
    Belegt     : double;
    Gesamt     : double;
    Prozent    : double;
    BlendRect  : TRect;
  begin
    case Info of
      ifLager : begin Belegt:=basis_api_BelegterLagerRaum; Gesamt:=basis_api_LagerRaum; end;
      ifWohn  : begin Belegt:=basis_api_BelegterWohnRaum; Gesamt:=basis_api_WohnRaum; end;
      ifLabor : begin Belegt:=basis_api_BelegterLaborRaum; Gesamt:=basis_api_LaborRaum; end;
      ifWerk  : begin Belegt:=basis_api_BelegterWerkRaum; Gesamt:=basis_api_WerkRaum; end;
      ifHangar: begin Belegt:=basis_api_BelegterHangarRaum; Gesamt:=basis_api_HangarRaum; end;
    end;
    FrameRect:=GetFrameRect;
    if Gesamt=0 then
      Prozent:=0
    else
      Prozent:=Belegt/Gesamt;
    BlendRect:=FrameRect;
    inc(BlendRect.Top);
    dec(BlendRect.Bottom,2);
    BlendRect.Right:=BlendRect.Left+round((FrameRect.Right-FrameRect.Left)*Prozent);
    BlendRectangle(BlendRect,128,bcBlack,Surface,Mem);
    Rectangle(Surface,Mem,FrameRect,fBorder);
    if FrameCount<3 then
    begin
      Caption:=AInfoFrame[Info,Gesamt=0];
      if Gesamt>0 then
      begin
        Caption:=Caption+Format(FPercent,[round(Prozent*100)]);
        if FrameCount=1 then
          Caption:=Caption+Format(AInfoFormat[Info],[Belegt,Gesamt]);
      end;
    end
    else
      Caption:=AInfoShortFrame[Info];
    WhiteStdFont.TextRect(Surface,FrameRect,FrameRect.Left+((FrameRect.Right-FrameRect.Left) shr 1)-(WhiteStdFont.TextWidth(Caption) shr 1),FrameRect.Top+2,Caption);
  end;

begin
  if AlphaElements then
    BlendRectangle(ClientRect,175,fBlend,Surface,Mem)
  else
  begin
    Surface.FillRect(ClientRect,fBlend);
  end;
  HLine(Surface,Mem,Left,Right,Bottom,fBorder);
  if GameInfo then
  begin
    DrawText(4,156,true,savegame_api_GetPlayerName,taLeftJustify,[cLeftTop,cLeftBottom]);
    Text:=savegame_api_GetDateString;
    DrawText(161,209,true,Text,taCenter,[]);

    //Text:=Format(FIAlphatron,[savegame_api_GetAlphatron/1]);
    //DrawText(371,132,true,Text,taCenter,[]);

    Text:=Format(FICredits,[savegame_api_GetCredits/1]);
    DrawText(504,132,true,Text,taRightJustify,[cRightTop,cRightBottom]);
  end
  else
  begin
    FrameCount:=0;
    // Anzahl der Angezeigten Informationen ermitteln
    for Dummy:=ifLager to ifHangar do
      if (Dummy in Infos) then inc(FrameCount);

    if fSelectBase.Visible then
      DrawText(4,156,true,basis_api_GetSelectedBasis.Name,taLeftJustify,[cLeftTop,cLeftBottom])
    else
    begin
      if FrameCount=0 then
        Corner:=[cLeftTop,cLeftBottom,cRightTop,cRightBottom]
      else
        Corner:=[cLeftTop,cLeftBottom];

      DrawText(4,177,true,basis_api_GetSelectedBasis.Name,taLeftJustify,Corner);
    end;

    if FrameCount=0 then
      Corner:=[cLeftTop,cLeftBottom,cRightTop,cRightBottom]
    else
      Corner:=[cRightTop,cRightBottom];

    if (ifAlphatron in Infos) then
      Text:=Format(FIAlphatron,[basis_api_GetSelectedBasis.Alphatron/1])
    else if (ifCredits in Infos) then
      Text:=Format(FICredits,[savegame_api_GetCredits/1]);

    DrawText(504,132,true,Text,taRightJustify,Corner);

    // In der zweite Zeile wird das andere angezeigt
    if (ifAlphatron in Infos) then
      Text:=Format(FICredits,[savegame_api_GetCredits/1])
    else if (ifCredits in Infos) then
      Text:=Format(FIAlphatron,[basis_api_GetSelectedBasis.Alphatron/1]);
      
    DrawText(504,132,false,Text,taRightJustify,[cRightTop,cRightBottom]);

    // Ausgabe des Seitennames
    DrawText(182,321,false,Parent.Caption,taCenter,[]);

    // Ausgabe der Datum/Uhrzeit
    DrawText(4,177,false,savegame_api_GetDateString(true),taLeftJustify,[cLeftTop]);

    if FrameCount=0 then
      exit;

    FrameWidth:=(322)/FrameCount;
    FrameDraw:=0;
    for Dummy:=ifLager to ifHangar do
      if (Dummy in Infos) then DrawInfo(Dummy);
  end;
end;

function TDXInfoFrame.ExpandSize(Sender: TObject;
  Frames: Integer): boolean;
begin
  inc(fAccel);
  Height:=Height+fAccel;
  if Height>=PopupSize then
  begin
    Container.DeleteFrameFunction(ExpandSize,Self);
    fAccel:=0;
  end;
  result:=true;
end;

procedure TDXInfoFrame.InitColorTable;
begin
  if not ColorTableInit then
  begin
    { ******************************** Rotes Farbschema **************************** }
    with ColorTable[icsRed] do
    begin
      BackColor             :=bcMaroon;     // Hintergrundfarbe
      PenColor[false]       :=bcRed;        // Linien ohne Alpha-Blending
      PenColor[true]        :=bcMaroon;     // Linien mit Alpha-Blending
      FirstColor[false]     :=clRed;        // Firstcolor des BitmapButtons ohne Alpha-Blending
      FirstColor[true]      :=$000000A0;    // Firstcolor des BitmapButtons mit Alpha-Blending
      SecondColor           :=clRed;        // SecondColor des BitmapButtons
    end;
    { ******************************** Gr�nes Farbschema **************************** }
    with ColorTable[icsGreen] do
    begin
      BackColor             :=bcGreen;
      PenColor[false]       :=bcLime;
      PenColor[true]        :=bcGreen;
      FirstColor[false]     :=clLime;
      FirstColor[true]      :=$0000A000;
      SecondColor           :=clLime;
    end;
    { ******************************** Blaues Farbscheme **************************** }
    with ColorTable[icsBlue] do
    begin
      BackColor             :=bcNavy;
      PenColor[false]       :=bcBlue;
      PenColor[true]        :=bcNavy;
      FirstColor[false]     :=clBlue;
      FirstColor[true]      :=$00A00000;
      SecondColor           :=clBlue;
    end;
  end;
end;

procedure TDXInfoFrame.KeyChangeBasis(BasisIndex: Integer);
begin
  if (not Container.CanMessage) then exit;

  if basis_api_GetBasisFromIndex(BasisIndex)<>nil then
  begin
    SetSelectedBasis(basis_api_GetBasisFromIndex(BasisIndex));
  end;
end;

procedure TDXInfoFrame.MouseLeave;
begin
  Collaps;
end;

procedure TDXInfoFrame.MouseMove(X, Y: Integer);
begin
  if GameInfo then
    exit;

  if Y<Height-(NormalSize-3) then
  begin
    if Height<PopupSize  then
    begin
      Container.AddFrameFunction(ExpandSize,Self,25);
      Container.DeleteFrameFunction(CollapsSize,Self);
    end;
  end;
end;

procedure TDXInfoFrame.Reset;
begin
  Container.Lock;
  Container.BringToTop(Self);
  if fSelectBase<>nil then
    Container.BringToTop(fSelectBase);
  if fClosePage<>nil then
    Container.BringToTop(fClosePage);
  Height:=1;
  AktuBorderColor;

  // Button zur Basis-Auswahl anzeigen, wenn mehr als eine Basis existiert
  if not GameInfo then
    fSelectBase.Visible:=basis_api_GetBasisCount>1;

  Container.UnLock;
  Redraw;
end;

procedure TDXInfoFrame.Resize(var NewLeft, NewTop, NewWidth,
  NewHeight: Integer);
begin
  NewTop:=0;
  NewLeft:=0;
  NewWidth:=ScreenWidth;
  NewHeight:=max(NormalSize,min(PopupSize,NewHeight));
  if fSelectBase<>nil then
  begin
    fSelectBase.SetRect(161,Bottom-21,20,18);
  end;
  if fClosePage<>nil then
  begin
    fClosePage.SetRect(780,4,16,15);
  end;
end;

procedure TDXInfoFrame.SelectBaseClick(Sender: TObject);
var
  Dummy  : Integer;
  Result : Integer;
begin
  Container.LoadGame(true);
  GlobalBasisSelector.Items.Clear;

  // Namen aller Basen �bernehmen
  for Dummy:=0 to basis_api_GetBasisCount-1 do
  begin
    GlobalBasisSelector.Items.AddObject(basis_api_GetBasisFromIndex(Dummy).Name,basis_api_GetBasisFromIndex(Dummy))
  end;

  Container.LoadGame(false);
  Result:=GlobalBasisSelector.Show;
  
  // Neue Basis setzen
  if Result<>-1 then
    SetSelectedBasis(TBasis(GlobalBasisSelector.Items.Objects[Result]));
end;

procedure TDXInfoFrame.SetFrameInfo(const Value: TFrameInfos);
var
  FrameCount  : Integer;
  Dummy       : TFrameInfo;
begin
  fFrameInfos := Value;
  FrameCount:=0;
  for Dummy:=ifLager to ifHangar do
  begin
    if (Dummy in Infos) then inc(FrameCount);
  end;
  if (FrameCount=0) and (fSelectBase<>nil) then fSelectBase.RoundCorners:=rcRight;
  Redraw;
end;

procedure TDXInfoFrame.SetGameInfo(const Value: boolean);
begin
  if Value then
  begin
    if fSelectBase<>nil then
    begin
      Container.LoadGame(true);
      fSelectBase.Free;
      fSelectBase:=nil;
      Container.LoadGame(false);
    end;
  end;
  fGameInfo := Value;
  Redraw;
end;

procedure TDXInfoFrame.SetKeyChange(const Value: boolean);
begin
  if fKeyChange=Value then exit;
  fKeyChange := Value;
  if fKeyChange then
  begin
    Parent.RegisterSystemKey('1',KeyChangeBasis,0);
    Parent.RegisterSystemKey('2',KeyChangeBasis,1);
    Parent.RegisterSystemKey('3',KeyChangeBasis,2);
    Parent.RegisterSystemKey('4',KeyChangeBasis,3);
    Parent.RegisterSystemKey('5',KeyChangeBasis,4);
    Parent.RegisterSystemKey('6',KeyChangeBasis,5);
    Parent.RegisterSystemKey('7',KeyChangeBasis,6);
    Parent.RegisterSystemKey('8',KeyChangeBasis,7);
  end
  else
  begin
    Parent.DeleteSystemKey('1',KeyChangeBasis,0);
    Parent.DeleteSystemKey('2',KeyChangeBasis,1);
    Parent.DeleteSystemKey('3',KeyChangeBasis,2);
    Parent.DeleteSystemKey('4',KeyChangeBasis,3);
    Parent.DeleteSystemKey('5',KeyChangeBasis,4);
    Parent.DeleteSystemKey('6',KeyChangeBasis,5);
    Parent.DeleteSystemKey('7',KeyChangeBasis,6);
    Parent.DeleteSystemKey('8',KeyChangeBasis,7);
  end;
end;

{procedure TDXInfoFrame.SetSaveGame(const Value: TKD4SaveGame);
begin
  // SetToTop
  Parent.RemoveComponent(Self);
  Height:=21;
  Parent.AddComponent(Self);

  if fSelectBase<>nil then
  begin
    Parent.RemoveComponent(fSelectBase);
    Parent.AddComponent(fSelectBase);
  end;

  fSaveGame := Value;
  Redraw;
end;}

procedure TDXInfoFrame.SetSelectedBasis(Basis: TBasis);
begin
  if basis_api_GetSelectedBasis<>Basis then
  begin
    Container.IncLock;
    basis_api_SetSelectedBasis(Basis);
    Container.DecLock;
    Redraw;
  end;
end;

procedure TDXInfoFrame.ClosePageClick(Sender: TObject);
begin
  if fReturnPage=-1 then
    Container.CloseModal
  else
    Parent.ChangePage(fReturnPage);
end;

procedure TDXInfoFrame.DrawCloseButton(Sender: TDXComponent;
  Surface: TDirectDrawSurface; var Mem: TDDSurfaceDesc);
begin
  Container.ImageList.Items[0].Draw(Surface,fClosePage.Left+2,fClosePage.Top+2,4);
end;

{ TDXPageNavigator }

procedure TDXPageNavigator.ButtonClick(Sender: TObject);
var
  NewPage: TDXPage;
begin
  NewPage:=Container.GetPageOfIndex(TDXBitmapButton(Sender).Tag);
  if NewPage<>Container.ActivePage then
  begin
    Container.ActivePage:=NewPage;
  end;
end;

function TDXPageNavigator.CheckCollapsing(Sender: TObject;
  Frames: Integer): boolean;
begin
  if TDXPageNavigator(Sender).Parent<>Container.ActivePage then
  begin
    Width:=1;
    Container.DeleteFrameFunction(CheckCollapsing,Self);
    exit;
  end;

  if Container.MousePos.X>=Right then
  begin
    if fAccel>=0 then
    begin
      fAccel:=0;
      Container.AddFrameFunction(CollapsSize,Self,25);
      Container.DeleteFrameFunction(ExpandSize,Self);
    end;
  end;
end;

function TDXPageNavigator.CollapsSize(Sender: TObject;
  Frames: Integer): boolean;
var
  Dummy   : Integer;
  OldRect : TRect;
begin
  Container.Lock;
  OldRect:=ClientRect;
  dec(fAccel,2);
  Width:=Width+fAccel;
  if Width=1 then
  begin
    Container.DeleteFrameFunction(CollapsSize,Self);
    Container.DeleteFrameFunction(CheckCollapsing,Self);
    fAccel:=0;

    // Buttons wieder entfernen
    if PageList<>nil then
    begin
      for Dummy:=0 to PageList.Count-1 do
        Container.RemoveComponent(TDXBitmapButton(PageList.Objects[Dummy]));
    end;
  end;
  CorrectButtonPositions;
  Container.Unlock;
  Container.RedrawArea(OldRect,Container.Surface,false);
  result:=true;
end;

procedure TDXPageNavigator.CorrectButtonPositions;
var
  Dummy: Integer;
begin
  if PageList<>nil then
  begin
    for Dummy:=0 to PageList.Count-1 do
    begin
      TDXBitmapButton(PageList.Objects[Dummy]).Left:=Right-MaxNavigatorSize+ButtonMargin;
    end;
  end;
end;

constructor TDXPageNavigator.Create(Page: TDXPage);
begin
  inherited;
  SetRect(0,0,1,Screen.Height);
end;

procedure TDXPageNavigator.Draw(Surface: TDirectDrawSurface;
  var Mem: TDDSurfaceDesc);
begin
  if Width=1 then
    exit;

  BlendRectangle(ClientRect,175,bcNavy,Surface,Mem);
  VLine(Surface,Mem,Top,Bottom,Right-1,bcDarkNavy);
  HLine(Surface,Mem,Left,Right-1,20,bcDarkNavy);
  WhiteBStdFont.Draw(Surface,Right-(MaxNavigatorSize div 2)-(WhiteBStdFont.TextWidth(ST0309210026) div 2),4,ST0309210026);
  YellowBStdFont.Draw(Surface,Right-YellowBStdFont.TextWidth(ST0309210027)-25,Bottom-45,ST0309210027);
  YellowBStdFont.Draw(Surface,Right-YellowBStdFont.TextWidth(ST0309210028)-25,Bottom-25,ST0309210028);
end;

function TDXPageNavigator.ExpandSize(Sender: TObject;
  Frames: Integer): boolean;
var
  Dummy  : Integer;
  YTop   : Integer;
  Corner : TRoundCorners;
  Button : TDXBitmapButton;
begin
  Container.Lock;
  // �bernahme / Anzeige der Buttons
  if (PageList<>nil) and (Width=1) then
  begin
    // Farben setzen
    Container.BringToTop(Self);
    YTop:=50;
    Corner:=rcTop;
    Button:=nil;
    for Dummy:=0 to PageList.Count-1 do
    begin
      Button:=TDXBitmapButton(PageList.Objects[Dummy]);
      Container.AddComponent(Button);
      Button.Top:=YTop;
      Button.RoundCorners:=Corner;
      Button.BlendColor:=bcDarkNavy;
      Button.Text:=Copy(Button.Text,1,Pos(#1,Button.Text)-1)+#1;

      Button.HighLight:=Button.Parent=Container.ActivePage;

      if AlphaElements then
        Button.FirstColor:=$00400000
      else
        Button.FirstColor:=$00800000;
      Button.SecondColor:=clBlue;
      inc(YTop,Button.Height+1);

      Corner:=rcNone;
    end;
    AktuFKeyInfo;
    if Button<>nil then
      Button.RoundCorners:=rcBottom;

    // Registrierung der Funktion, um das zusammenklappen zu pr�fen
    Container.AddFrameFunction(CheckCollapsing,Self,250);
  end;
  while Frames>0 do
  begin
    inc(fAccel,2);
    Width:=Width+fAccel;
    if Width=MaxNavigatorSize then
    begin
      Container.DeleteFrameFunction(ExpandSize,Self);
      fAccel:=0;
      Frames:=0;
    end;
    dec(Frames);
  end;

  // Position anpassen
  CorrectButtonPositions;
  Container.Unlock;
  Redraw;
  result:=true;
end;

procedure TDXPageNavigator.AktuFKeyInfo;
var
  Dummy  : Integer;
  Key    : Integer;
  Button : TDXBitmapButton;
  Name   : String;

  function FindKeyOfPage(PageIndex: Integer): Integer;
  var
    Dummy: Integer;
  begin
    result:=0;
    for Dummy:=1 to 12 do
    begin
      if FKeys[Dummy]=PageIndex then
      begin
        result:=Dummy;
        exit;
      end;
    end;
  end;

begin
  for Dummy:=0 to PageList.Count-1 do
  begin
    Button:=TDXBitmapButton(PageList.Objects[Dummy]);
    Key:=FindKeyOfPage(Button.Tag);

    // Ermitteln des Seitennamens
    Name:=Container.GetPageOfIndex(Button.Tag).Caption;
    if Key<>0 then
      Button.Text:=Format('%s ('#2'F%d'#1')',[Name,Key])
    else
      Button.Text:=Name;
  end;
end;

procedure TDXPageNavigator.MouseMove(X, Y: Integer);
begin
  inherited;
  if fAccel<=0 then
  begin
    fAccel:=0;
    Container.AddFrameFunction(ExpandSize,Self,25);
    Container.DeleteFrameFunction(CollapsSize,Self);
  end;
end;

procedure TDXPageNavigator.RegisterPage(Caption: String; Index: Integer);
var
  Button  : TDXBitmapButton;
  Dummy   : Integer;
begin
//  fCaption:=Caption;

  // Registrierung der Seite f�r den Navigator
  if PageList=nil then
  begin
    PageList:=TStringList.Create;
//    PageList.Sorted:=true;
  end;

  // Erzeugen des Buttons
  Button:=TDXBitmapButton.Create(Parent);
  Parent.RemoveComponent(Button);
  with Button do
  begin
    Height:=ButtonHeight;
    Width:=MaxNavigatorSize-(ButtonMargin*2);
    Tag:=Index;
    Text:=Caption+#1;
    BlendAlpha:=150;
    Parent.SetButtonFont(Font);
    AccerlateColor:=coAccerlateColor;
    OnClick:=ButtonClick;
  end;

  PageList.AddObject(Caption,Button);

  // Berechnung der Maximalen Navigatorbreite
  if MaxNavigatorSize<WhiteStdFont.TextWidth(Caption)+((ButtonMargin*2)+(TextMargin*2)) then
  begin
    MaxNavigatorSize:=WhiteStdFont.TextWidth(Caption)+((ButtonMargin*2)+(TextMargin*2));
    // Breite der vorhandenen Buttons anpassen
    for Dummy:=0 to PageList.Count-1 do
      TDXBitmapButton(PageList.Objects[Dummy]).Width:=MaxNavigatorSize-(ButtonMargin*2);
  end;
end;

procedure TDXPageNavigator.Resize(var NewLeft, NewTop, NewWidth,
  NewHeight: Integer);
begin
  NewWidth:=max(1,min(MaxNavigatorSize,NewWidth));
end;

destructor TDXPageNavigator.Destroy;
var
  Dummy: Integer;
begin
  if PageList<>nil then
  begin
    for Dummy:=0 to PageList.Count-1 do
    begin
      if TDXComponent(PageList.Objects[Dummy]).Parent=Parent then
      begin
        TDXComponent(PageList.Objects[Dummy]).Free;
        PageList.Delete(Dummy);
        break;
      end;
    end;
  end;
  inherited;
end;

initialization

finalization

  if PageList<>nil then
    PageList.Free;
end.
