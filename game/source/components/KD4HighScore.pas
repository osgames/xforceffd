{********************************************************************************
*										*
* Copyright (C) 2004  X-Force Team - jim_raynor@web.de				*
*										*
* This file is part of X-Force: Fight For Destiny (X-Force).			*
*										*
* X-Force is free software; you can redistribute it and/or modify		*
* it under the terms of the GNU General Public License as published by		*
* the Free Software Foundation; either version 2 of the License, or		*
* (at your option) any later version.						*
*										*
* X-Force is distributed in the hope that it will be useful,			*
* but WITHOUT ANY WARRANTY; without even the implied warranty of		*
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the			*
* GNU General Public License for more details.					*
*										*
* You should have received a copy of the GNU General Public License		*
* along with X-Force; if not, write to the Free Software			*
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA	*
*										*
*********************************************************************************
*										*
* Verwaltet die Highscore in X-Force						*
*										*
********************************************************************************}

{$I ../Settings.inc}

unit KD4HighScore;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  XForce_types,Koding, NotifyList;

const
  EVENT_HIGHSCORECHANGE : TEventID  = 0;

type
  TKD4HighScore = class(TComponent)
  private
    fList         : Array of THighScoreEntry;
    fNotifyList   : TNotifyList;
    function GetCount: Integer;
    function GetEntry(Index: integer): THighScoreEntry;
    { Private-Deklarationen }
  protected
    procedure Sort;
    { Protected-Deklarationen }
  public
    property Entry[Index: integer]: THighScoreEntry read GetEntry;

    property NotifyList: TNotifyList read fNotifyList write fNotifyList;
    { Public-Deklarationen }
  published
    constructor Create(AOwner: TComponent);override;
    destructor Destroy;override;
    procedure SaveToStream(Stream: TStream);
    procedure LoadFromStream(Stream: TStream);
    procedure AddHigh(HighScore: THighScoreEntry);
    property Count: Integer read GetCount;
    { Published-Deklarationen }
  end;

procedure Register;

implementation

procedure Register;
begin
  RegisterComponents('KD4 Tools',[TKD4HighScore]);
  RegisterNonActiveX([TKD4HighScore],axrIncludeDescendants);
end;

{ TKD4HighScore }

procedure TKD4HighScore.AddHigh(HighScore: THighScoreEntry);
begin
  SetLength(fList,Count+1);
  fList[length(fList)-1]:=HighScore;
  Sort;
  fNotifyList.CallEvents(EVENT_HIGHSCORECHANGE,nil);
end;

constructor TKD4HighScore.Create(AOwner: TComponent);
begin
  inherited;
  fNotifyList:=TNotifyList.Create;
end;

destructor TKD4HighScore.Destroy;
begin
  fNotifyList.Free;
  inherited;
end;

function TKD4HighScore.GetCount: Integer;
begin
  result:=length(fList);
end;

function TKD4HighScore.GetEntry(Index: integer): THighScoreEntry;
begin
  if (Index<0) or (Index>Count-1) then exit;
  result:=fList[Index];
end;

procedure TKD4HighScore.LoadFromStream(Stream: TStream);
var
  m: TMemoryStream;
  Header: TEntryHeader;
  Dummy: integer;
begin
  SetLength(fList,0);
  m:=TMemoryStream.Create;
  Decode(Stream,m);
  m.read(Header,SizeOf(TEntryHeader));
  if Header.Version<>HighVersion then
  begin
    raise EInvalidVersion.Create('Ung�ltige Version der High-Score Datei');
  end;
  SetLength(fList,Header.Entrys);
  for Dummy:=0 to Header.Entrys-1 do
    m.Read(fList[Dummy],SizeOf(THighScoreEntry));

  m.Free;

  fNotifyList.CallEvents(EVENT_HIGHSCORECHANGE,nil);
end;

procedure TKD4HighScore.SaveToStream(Stream: TStream);
var
  m       : TMemoryStream;
  Header  : TEntryHeader;
  Dummy   : Integer;
  Entry   : THighScoreEntry;
begin
  Sort;
  m:=TMemoryStream.Create;
  Header.Version:=HighVersion;
  Header.Entrys:=Count;
  m.Write(Header,SizeOf(TEntryHeader));
  for Dummy:=0 to Count-1 do
  begin
    FillChar(Entry.Spieler,SizeOf(Entry.Spieler),#0);
    with fList[Dummy] do
    begin
      Entry.Spieler:=Spieler;
      Entry.Week:=Week;
      Entry.Jahr:=Jahr;
      Entry.Punkte:=Punkte;
    end;
    m.Write(Entry,SizeOf(Entry));
  end;
  Encode(m,Stream);
  m.Free;
end;

procedure TKD4HighScore.Sort;
var
  Dummy: Integer;
  Temp: THighScoreEntry;
  First: Integer;
  Second: integer;
begin
  for Dummy:=0 to Count-2 do
  begin
    First:=Dummy;
    for Second:=First+1 to Count-1 do
    begin
      if fList[First].Punkte<fList[Second].Punkte then
      begin
        Temp:=fList[First];
        fList[First]:=fList[Second];
        fList[Second]:=Temp;
      end;
    end;
  end;
  if Count>100 then
    SetLength(fList,100);
end;

end.
