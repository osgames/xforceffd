{********************************************************************************
*										*
* Copyright (C) 2004  X-Force Team - jim_raynor@web.de				*
*										*
* This file is part of X-Force: Fight For Destiny (X-Force).			*
*										*
* X-Force is free software; you can redistribute it and/or modify		*
* it under the terms of the GNU General Public License as published by		*
* the Free Software Foundation; either version 2 of the License, or		*
* (at your option) any later version.						*
*										*
* X-Force is distributed in the hope that it will be useful,			*
* but WITHOUT ANY WARRANTY; without even the implied warranty of		*
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the			*
* GNU General Public License for more details.					*
*										*
* You should have received a copy of the GNU General Public License		*
* along with X-Force; if not, write to the Free Software			*
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA	*
*										*
*********************************************************************************
*										*
* Diese Unit stellt ein Eingabefeld zur Verf�gung				*
*										*
********************************************************************************}

{$I ../Settings.inc}

unit DXEdit;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  DXContainer, DXDraws, math, KD4Utils, Blending, TraceFile, XForce_types, Defines,
  DirectDraw, DirectFont;

type
  TDXEdit = class(TDXComponent)
  private
    fBorder            : TColor;
    fLeftMargin        : Integer;
    fTopMargin         : Integer;
    fText              : String;
    fCursorPos         : Integer;
    fMaxLength         : Integer;
    fReadOnly          : boolean;
    fChange            : TNotifyEvent;
    fFocusColor        : TColor;
    fFileName          : boolean;
    fBlendColor        : TBlendColor;
    fAlignment         : TAlignment;
    fSelStart          : Integer;
    fDFont             : TDirectFont;
    fSelLength         : Integer;
    fCorners           : TRoundCorners;
    fCorner            : TCorners;
    fAlpha             : Integer;
    fDrawCursor        : boolean;
    procedure SetBorder(const Value: TColor);
    procedure SetLeftMargin(const Value: Integer);
    procedure SetTopMargin(const Value: Integer);
    procedure SetText(const Value: String);
    procedure SetMaxLength(const Value: Integer);
    procedure SetFocusColor(const Value: TColor);
    procedure SetBlendColor(const Value: TBlendColor);
    procedure SetAlignment(const Value: TAlignment);
    procedure SetSelLength(const Value: Integer);
    procedure SetSelStart(const Value: Integer);
    function GetSelLength: Integer;
    function GetSelStart: Integer;
    procedure SetCorners(const Value: TRoundCorners);
    function GetFrameRect: TRect;
    function CanInsert(Key: Char): boolean;
    { Private-Deklarationen }
  protected
    function PerformFrame(Sender: TObject;Frames: Integer): boolean;
    procedure Change;
    procedure DrawSel(Surface: TDirectDrawSurface;const Mem: TDDSurfaceDesc);
    procedure CreateMouseRegion;override;
    procedure KillFocus;override;
    procedure GetFocus;override;
    procedure OnFontChange(Sender: TObject);
    { Protected-Deklarationen }
  public
    constructor Create(Page: TDXPage);override;
    procedure Draw(Surface: TDirectDrawSurface;var Mem: TDDSurfaceDesc);override;
    procedure KeyPress(var Key: Word;State: TShiftState);override;
    procedure KeyDown(var Key: Word;State: TShiftState);override;
    procedure MouseMove(X,Y: Integer);Override;
    procedure MouseDown(Button: TMouseButton;X,Y: Integer);Override;
    procedure SelectAll;
    property BorderColor   : TColor read fBorder write SetBorder;
    property FocusColor    : TColor read fFocusColor write SetFocusColor;
    property LeftMargin    : Integer read fLeftMargin write SetLeftMargin;
    property TopMargin     : Integer read fTopMargin write SetTopMargin;
    property Text          : String read fText write SetText;
    property MaxLength     : Integer read fMaxLength write SetMaxLength;
    property ReadOnly      : boolean read fReadOnly write fReadOnly;
    property OnChange      : TNotifyEvent read fChange write fChange;
    property FileName      : boolean read fFileName write fFileName;
    property BlendColor    : TBlendColor read fBlendColor write SetBlendColor;
    property Alignment     : TAlignment read fAlignment write SetAlignment;
    property SelStart      : Integer read GetSelStart write SetSelStart;
    property SelLength     : Integer read GetSelLength write SetSelLength;
    property RoundCorners  : TRoundCorners read fCorners write SetCorners;
    property BlendAlpha    : Integer read fAlpha write fAlpha;
  end;

implementation

{ TDXEdit }

function TDXEdit.CanInsert(Key: Char): boolean;
var
  MaxWidth  : Integer;
  TextWidth : Integer;
  KeyWidth  : Integer;
  SelWidth  : Integer;
begin
  MaxWidth:=Width-(fLeftMargin*2);
  TextWidth:=fDFont.TextWidth(fText); // Weite des Bisherigen Textes
  KeyWidth:=fDFont.TextWidth(Key);    // Weite des neuen Zeichen
  SelWidth:=fDFont.TextWidth(Copy(fText,SelStart,SelLength)); // Weite der Auswahl
  result:=(MaxWidth>(TextWidth-SelWidth)+KeyWidth);
  if result and (fMaxLength>0) then
  begin
    result:=result and (not (fMaxLength=Length(fText)-SelLength));
  end;
end;

procedure TDXEdit.Change;
begin
  Container.LoadGame(false);
  if Assigned(fChange) then fChange(Self);
  Container.LoadGame(true);
end;

constructor TDXEdit.Create(Page: TDXPage);
begin
  inherited;
  TabStop:=true;
  fDrawCursor:=true;
  fBlendColor:=bcMaroon;
  fAlpha:=75;
  fLeftMargin:=5;
  fTopMargin:=5;
  fCursorPos:=0;
  fSelStart:=0;
  fSelLength:=0;
  fReadOnly:=false;
  RoundCorners:=rcAll;
  Font.OnChange:=OnFontChange;
end;

procedure TDXEdit.CreateMouseRegion;
var
  FrameRect: TRect;
  RoundRGN : HRGN;
begin
  FrameRect:=GetFrameRect;
  RoundRGN:=CreateRoundRectRgn(FrameRect.Left,FrameRect.Top,FrameRect.Right+1,FrameRect.Bottom+1,20,20);
  fMouseRegion:=CreateRectRgn(Left,Top,Right,Bottom);
  CombineRgn(fMouseRegion,fMouseRegion,RoundRGN,RGN_AND);
  DeleteObject(RoundRGN);
end;

procedure TDXEdit.Draw(Surface: TDirectDrawSurface;var Mem: TDDSurfaceDesc);
var
  CaretPos    : Integer;
  Dummy       : Integer;
  TempText    : String;
  TextWidth   : Integer;
  PenColor    : TBlendColor;
begin
  if HasFocus then PenColor:=fFocusColor else PenColor:=fBorder;
  if AlphaElements then
    BlendRoundRect(ClientRect,fAlpha,fBlendColor,Surface,Mem,11,fCorner,ClientRect);
  FramingRect(Surface,Mem,ClientRect,fCorner,11,PenColor);
  TextWidth:=fDFont.TextWidth(fText);
  if HasFocus then DrawSel(Surface,Mem);
  if Alignment=taCenter then
    fDFont.TextRect(Surface,Rect(Left+fTopMargin,Top,Right-fTopMargin,Bottom),Left+(Width shr 1)-(TextWidth shr 1),Top+fTopMargin,fText)
  else
    fDFont.TextRect(Surface,ClientRect,Left+fLeftMargin,Top+fTopMargin,fText);
  TempText:='';
  if fDrawCursor and HasFocus and Enabled and not fReadOnly then
  begin
    if fCursorPos=length(fText) then
    begin
      CaretPos:=fDfont.TextWidth(fText);
    end
    else
    begin
      for Dummy:=1 to min(fCursorPos,length(ftext)) do TempText:=TempText+fText[Dummy];
      CaretPos:=fDfont.TextWidth(TempText);
    end;
    inc(CaretPos,fLeftMargin);
    if CaretPos+Left+1>Right then exit;
    VLine(Surface,Mem,Top+fTopMargin+1,Top+fTopMargin+fDFont.TextHeight('W'),CaretPos+Left,bcWhite);
  end;
end;

procedure TDXEdit.DrawSel(Surface: TDirectDrawSurface;const Mem: TDDSurfaceDesc);
var
  BegX   : Integer;
  EndX   : Integer;
begin
  if SelLength=0 then exit;
  if SelStart>Length(fText) then exit;
  BegX:=Left+fLeftMargin+fDFont.TextWidth(Copy(fText,0,SelStart));
  EndX:=BegX+fDFont.TextWidth(Copy(fText,SelStart+1,SelLength))+1;
  if AlphaElements then
    BlendRectangle(Rect(BegX,Top+fTopMargin+1,EndX,Top+fTopMargin+(fDFont.TextHeight('W'))+1),100,bcBlue,Surface,Mem)
  else
  begin
    Surface.FillRect(Rect(BegX,Top+fTopMargin+1,EndX,Top+fTopMargin+(fDFont.TextHeight('W')+1)),bcBlue);
  end;
end;

procedure TDXEdit.GetFocus;
begin
  Container.AddFrameFunction(PerformFrame,Self,500);
  fDrawCursor:=true;
end;

function TDXEdit.GetFrameRect: TRect;
begin
  result:=ClientRect;
  if fCorners in [rcLeft,rcLeftTop,rcLeftBottom,rcNone] then inc(result.Right,20);
  if fCorners in [rcTop,rcLeftTop,rcRightTop,rcNone] then inc(result.Bottom,20);
  if fCorners in [rcRight,rcRightTop,rcRightBottom,rcNone] then dec(result.Left,20);
  if fCorners in [rcBottom,rcLeftBottom,rcRightBottom,rcNone] then dec(result.Top,20);
end;

function TDXEdit.GetSelLength: Integer;
begin
  if fSelLength>=0 then result:=fSelLength else result:=abs(fSelLength);
end;

function TDXEdit.GetSelStart: Integer;
begin
  if fSelLength>=0 then result:=fSelStart else result:=max(fSelStart+fSelLength,0);
end;

procedure TDXEdit.KeyDown(var Key: Word; State: TShiftState);
var
  Mark       : boolean;
  NeedRedraw : boolean;
begin
  inherited;
  if fReadOnly then exit;
  if ssAlt in State then exit;
  NeedRedraw:=false;
  Container.Lock;
  Mark:=ssShift in State;
  if (Char(Key)=#37) or (Char(Key)=#38)then
  begin
    dec(fCursorPos);
    fCursorPos:=max(fCursorPos,0);
    if not Mark then
    begin
      SelStart:=fCursorPos;
    end
    else SelLength:=fCursorPos-fSelStart;
    NeedRedraw:=true;
  end;
  if (Char(Key)=#39) or (Char(Key)=#40) then
  begin
    inc(fCursorPos);
    fCursorPos:=min(fCursorPos,Length(fText));
    if not Mark then
    begin
      SelStart:=fCursorPos;
    end
    else SelLength:=fCursorPos-fSelStart;
    NeedRedraw:=true;
  end;
  if Char(Key)=#35 then
  begin
    fCursorPos:=length(fText);
    if not Mark then
    begin
      SelStart:=fCursorPos;
    end
    else SelLength:=fCursorPos-fSelStart;
    NeedRedraw:=true;
  end;
  if Char(Key)=#36 then
  begin
    fCursorPos:=0;
    if not Mark then
    begin
      SelStart:=fCursorPos;
    end
    else SelLength:=fCursorPos-fSelStart;
    NeedRedraw:=true;
  end;
  if Char(Key)=#46 then
  begin
    if not ((fCursorPos=length(fText)) and (SelLength=0)) then
    begin
      fCursorPos:=SelStart;
      if SelLength=0 then Delete(fText,min(length(fText),fCursorPos+1),1)
      else Delete(fText,SelStart+1,SelLength);
      fCursorPos:=max(min(fSelStart,fCursorPos),0);
      SelStart:=fCursorPos;
      Change;
      NeedRedraw:=true;
    end;
  end;
  Container.UnLock;
  if NeedRedraw then
  begin
    fDrawCursor:=true;
    Container.RedrawArea(ClientRect,Container.Surface);
    Key:=0;
  end;
end;

procedure TDXEdit.KeyPress(var Key: Word; State: TShiftState);
var
  Add: boolean;
  Press: Char;
begin
  if fReadOnly then exit;
  if ssAlt in State then exit;
  Add:=false;
  Press:=Char(Key);
  Container.Lock;
  if ssShift in State then UpperCase(Press);
  if (Press>=#32)  and (Press<=#126) then Add:=true;
  if (Press>=#161) and (Press<=#255) then Add:=true;
  if fFileName then
  begin
    if (Press='/') or (Press='\') or (Press=':') or (Press='*')
      or (Press='?') or (Press='"') or (Press='<') or (Press='>') or (Press='|') then
    Add:=false;
  end;
  if Add then
  begin
    if CanInsert(Press) then
    begin
      if SelLength>0 then
      begin
        fText:=Stuff(fText,SelStart,SelLength,Press);
        fCursorPos:=max(1,min(SelStart+1,length(fText)));
      end
      else
      begin
        fText:=Stuff(fText,fCursorPos,0,Press);
        fCursorPos:=max(1,min(fCursorPos+1,length(fText)));
      end;
      SelStart:=fCursorPos;
      Container.PlaySound(SPressKey);
      Change;
    end;
    Key:=0;
  end;
  if (Press=#8) then
  begin
    fCursorPos:=SelStart;
    if fSelLength=0 then
    begin
      Delete(fText,fCursorPos,1);
      dec(fCursorPos);
    end
    else Delete(fText,SelStart+1,SelLength);
    fCursorPos:=max(min(fSelStart,fCursorPos),0);
    SelStart:=fCursorPos;
    Container.PlaySound(SPressKey);
    Change;
    Key:=0;
  end;
  fDrawCursor:=true;
  Container.UnLock;
  Container.RedrawArea(ClientRect,Container.Surface);
end;

procedure TDXEdit.KillFocus;
begin
  Container.DeleteFrameFunction(PerformFrame,Self);
end;

procedure TDXEdit.MouseDown(Button: TMouseButton; X, Y: Integer);
var
  Text   : String;
  Pos    : Integer;
  Mark   : boolean;
begin
  inherited;
  Mark:=(GetKeyState(VK_SHIFT)<0);
  if Button<>mbLeft then exit;
  if fReadOnly then exit;
  Container.Lock;
  X:=X-fLeftMargin;
  Text:='';
  Pos:=0;
  while (X>fDFont.TextWidth(Text)) and (length(Text)<length(fText)) do
  begin
    Text:=Text+fText[Pos+1];
    inc(Pos);
  end;
  if X>fDFont.TextWidth(Text) then
    fCursorPos:=Pos
  else
    fCursorPos:=Pos-1;
  fCursorPos:=max(fCursorPos,0);
  if Mark then SelLength:=fCursorPos-fSelStart else SelStart:=fCursorPos;
  Container.UnLock;
  Redraw;
end;

procedure TDXEdit.MouseMove(X, Y: Integer);
var
  Text   : String;
  Pos    :Integer;
  Temp   : Integer;
begin
  inherited;
  if fReadOnly then exit;
  Temp:=SelLength;
  if not (GetKeyState(VK_LBUTTON)<0) then exit;
  Container.Lock;
  Text:='';
  Pos:=0;
  while (X>fDFont.TextWidth(Text)) and (length(Text)<length(fText)) do
  begin
    Text:=Text+fText[Pos+1];
    inc(Pos);
  end;
  if X>fDFont.TextWidth(Text) then fCursorPos:=Pos else fCursorPos:=Pos-1;
  fCursorPos:=max(fCursorPos,0);
  SelLength:=fCursorPos-fSelStart;
  Container.UnLock;
  if Temp<>SelLength then Redraw;
end;

procedure TDXEdit.OnFontChange(Sender: TObject);
begin
  fDFont:=FontEngine.FindDirectFont(Font,clBlack);
end;

function TDXEdit.PerformFrame(Sender: TObject; Frames: Integer): boolean;
begin
  fDrawCursor:=not fDrawCursor;
  Redraw;
  result:=true;
end;

procedure TDXEdit.SelectAll;
begin
  fCursorPos:=length(fText);
  SelStart:=0;
  SelLength:=length(fText);
end;

procedure TDXEdit.SetAlignment(const Value: TAlignment);
begin
  fAlignment := Value;
  Redraw;
end;

procedure TDXEdit.SetBlendColor(const Value: TBlendColor);
begin
  fBlendColor := Value;
  Redraw;
end;

procedure TDXEdit.SetBorder(const Value: TColor);
begin
  fBorder := Container.Surface.ColorMatch(Value);
  Container.RedrawControl(Self,Container.Surface);
end;

procedure TDXEdit.SetCorners(const Value: TRoundCorners);
begin
  fCorners := Value;
  fCorner:=[];
  if fCorners in [rcLeft,rcTop,rcLeftTop,rcAll] then include(fCorner,cLeftTop);
  if fCorners in [rcRight,rcTop,rcRightTop,rcAll] then include(fCorner,cRightTop);
  if fCorners in [rcLeft,rcBottom,rcLeftBottom,rcAll] then include(fCorner,cLeftBottom);
  if fCorners in [rcRight,rcBottom,rcRightBottom,rcAll] then include(fCorner,cRightBottom);
  RecreateMouseRegion;
  Redraw;
end;

procedure TDXEdit.SetFocusColor(const Value: TColor);
begin
  fFocusColor := Container.Surface.ColorMatch(Value);
  Redraw;
end;

procedure TDXEdit.SetLeftMargin(const Value: Integer);
begin
  fLeftMargin := Value;
  Container.RedrawControl(Self,Container.Surface);
end;

procedure TDXEdit.SetMaxLength(const Value: Integer);
begin
  fMaxLength := Value;
  if fMaxLength=0 then exit;
  if length(fText)>fMaxLength then Text:=Cut(fText,1,fMaxLength);
end;

procedure TDXEdit.SetSelLength(const Value: Integer);
begin
//  if Value+fSelStart<0 then exit;
  fSelLength := Value;
  Redraw;
end;

procedure TDXEdit.SetSelStart(const Value: Integer);
begin
  fSelStart := Value;
  fSelLength:=0;
  Redraw;
end;

procedure TDXEdit.SetText(const Value: String);
begin
  Container.IncLock;
  Container.LoadGame(true);
  fText := Value;
  if fMaxLength<>0 then if length(fText)>fMaxLength then Text:=Cut(fText,1,fMaxLength);
  fCursorPos:=length(fText);
  SelStart:=length(fText);
  Change;
  Container.DecLock;
  Container.LoadGame(false);
  Redraw;
  Self.Redraw;
end;

procedure TDXEdit.SetTopMargin(const Value: Integer);
begin
  fTopMargin := Value;
  Redraw;
end;

end.
