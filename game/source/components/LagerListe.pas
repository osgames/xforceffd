{********************************************************************************
*										*
* Copyright (C) 2004  X-Force Team - jim_raynor@web.de				*
*										*
* This file is part of X-Force: Fight For Destiny (X-Force).			*
*										*
* X-Force is free software; you can redistribute it and/or modify		*
* it under the terms of the GNU General Public License as published by		*
* the Free Software Foundation; either version 2 of the License, or		*
* (at your option) any later version.						*
*										*
* X-Force is distributed in the hope that it will be useful,			*
* but WITHOUT ANY WARRANTY; without even the implied warranty of		*
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the			*
* GNU General Public License for more details.					*
*										*
* You should have received a copy of the GNU General Public License		*
* along with X-Force; if not, write to the Free Software			*
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA	*
*										*
*********************************************************************************
*										*
* LagerListe stellt alles zur Verwaltung von Ausr�stung (Kaufen, Verkaufen) zur *
* Verf�gung.									*
*										*
********************************************************************************}

{$I ../Settings.inc}

unit LagerListe;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Loader, XForce_types,OrganisationList, DXClass, DXDraws, Koding, Blending, IDHashMap,
  TraceFile, ExtRecord, ProjektRecords, ConvertRecords, Defines,
  StringConst;

type
  TAngebotArray   = Array of TAngebot;
  TAuftragArray   = Array of TAuftrag;

  TLagerListe = class(TObject)
  private
    fListe        : Array of TLagerItem;
    fLagerCount   : Array of TBasisLager;
    fAuftrag      : TAuftragArray;
    fAngebot      : TAngebotArray;

    fItemIcons    : TPictureCollectionItem;
    fEquipIcons   : TPictureCollectionItem;

    fGlobalImages : Integer;
    fHashMap      : TIDHashMap;
    fWatchSettings: TWatchAuftragSettings;
    fLoadedBasis  : TObject;
    function GetCount: Integer;
    function GetLagerItem(Index: Integer): TLagerItem;
    function GetAuftrag(Index: Integer): TAuftrag;
    function GetAuftragCount: Integer;
    function GetAngebotCount: Integer;
    function GetAngebot(Index: Integer): TAngebot;
    procedure CheckAngebote;

    function ItemKaufbar(const LagerItem: TLagerItem; out Tage: Integer): boolean;

    function CompareAngebote(Index1,Index2: Integer;Typ: TFunctionType): Integer;

    function GetImageOfIndex(Index: Integer): TPictureCollectionItem;

    function GetBaseIndexForItemCount(Basis: TObject): Integer;overload;
    function GetBaseIndexForItemCount(BasisID: Cardinal): Integer;overload;

    procedure SaveCountToItemCount;
    procedure LoadCountFromItemCount(Basis: TObject);
    { Private-Deklarationen }
  protected
    procedure AddLagerItem(const Item: TLagerItem);

    procedure NewGameHandler(Sender: TObject);
    procedure AfterLoadHandler(Sender: TObject);
    { Protected-Deklarationen }
  public
    constructor Create;
    destructor destroy;override;

    procedure LoadFromStream(Stream: TStream);
    procedure SaveToStream(Stream: TStream);
    procedure Abgleich;

    procedure Clear;

    procedure AddItem(ID: Cardinal;BasisID: Cardinal;BelegRoom: boolean=true);overload;
    function  AddItem(ID: Cardinal;BasisID: Cardinal; Count: Integer): Integer;overload;
    function  AddMunition(ID: Cardinal;BasisID: Cardinal; Schuss: Integer): Boolean;

    function RecycleItem(Index: Integer; BasisID: Cardinal; Count: Integer = 1): Boolean;
    function DeleteItem(Index: Integer; BasisID: Cardinal;Count: Integer = 1; FreeRoom: Boolean = true): Boolean;

    procedure UpgradeID(ID: TObject);
    procedure SetLagerBitmap(Bitmap: TBitmap;Index: Integer; Global: boolean);

    procedure NewAuftrag(Item: Integer;Kaufen: boolean;Anzahl: Integer);
    procedure DeleteAuftrag(Index: Integer);
    procedure DeleteAngebot(Index: Integer);
    function AcceptAngebot(Index: Integer): Integer;
    procedure CorrectAngebot;
    function GetAngebotCountForAuftrag(ID: Cardinal): Integer;

    function FindAuftrag(Item: Integer; BasisID: Cardinal = 0): Integer;

    function CreateAngebote(Minuten: Integer): Integer;
    function AuftragOfID(ID: Cardinal):Integer;

    procedure NextHour(Sender: TObject);
    procedure DrawImage(Image: Integer;Surface: TDirectDrawSurface;x,y: Integer;Index: Integer=0;Shadow: boolean=false);
    procedure DrawImageISO(Image: Integer;Surface: TDirectDrawSurface;x,y: Integer);
    procedure DrawImageAlpha(Image: Integer;Surface: TDirectDrawSurface;x,y: Integer;Alpha: Integer);
    procedure BuyPlan(Index: Integer);

    procedure AddBasis(Basis: TObject);
    procedure DeleteBasis(Basis: TObject);
    procedure ChangeBasis(Basis: TObject);

    procedure DeleteFromBasis(ID: Cardinal;BasisID: Cardinal);

    function IndexOfID(ID: Cardinal): Integer;
    procedure AddForschItem(Projekt: TObject);

    function OrganHasAngebot(ID: Cardinal;Organ: Integer): boolean;
    function GetMaxMunition(Index: Integer): Integer;
    function PreisOfConstruction(Index: Integer): Integer;
    function AuftragInSelectedBasis(Dummy: Integer): boolean;
    function AddrLagerItem(Index: Integer): PLagerItem;

    procedure RegisterAlienItem(Item: TLagerItem);
    procedure ResearchComplete(ID: Cardinal);

    function GetRandomItem(ItemType: TProjektTypes; IQ: Integer; Weapon: Cardinal = 0;Alien: Boolean = false): PLagerItem;

    // Achtung. Ist nur noch zur Abw�rtskompatibilit�t
    procedure ItemFromBasis(Index: Integer; BasisID: Cardinal;out Item: TLagerItem);

    // Neuere Variante von ItemFromBasis
    function GetItemCountInBase(Index: Integer; BasisID: Cardinal): Integer;

    property Items[Index: Integer]     : TLagerItem read GetLagerItem;default;
    property Auftrag[Index: Integer]   : TAuftrag read GetAuftrag;
    property Angebote[Index: Integer]  : TAngebot read GetAngebot;
    property Count                     : Integer read GetCount;
    property AuftragCount              : Integer read GetAuftragCount;
    property AngebotCount              : Integer read GetAngebotCount;

    property WatchSettings             : TWatchAuftragSettings read fWatchSettings write fWatchSettings;
  end;


implementation

uses
  KD4Utils, RaumschiffList, BasisListe, basis_api, lager_api, country_api,
  savegame_api, array_utils, string_utils, raumschiff_api, game_api, forsch_api,
  game_utils;

var
  TLagerListRecord : TExtRecordDefinition;
{ TLagerListe }

procedure TLagerListe.Abgleich;
var
  Entry     : TForschProject;
  Projects  : TRecordArray;
  Dummy     : Integer;
  Index     : Integer;

  Basis   : Integer;
  Lager   : Integer;
  LItem   : PLagerItem;
  Count   : Integer;
begin
  Projects:=savegame_api_GameSetGetProjects;

  for Dummy:=0 to high(Projects) do
  begin
    Entry:=RecordToProject(Projects[Dummy]);

    Index:=IndexOfID(Entry.ID);
    if Index<>-1 then
    begin
      // Hier m�ssen in Zukunft neue Eigenschaften aus Entry in fListe �bernommen werden

      // Sicherstellen das spezielle Schusstypen in den Items abgelegt sind
      fListe[Index].Schuss:=CheckSchussType(fListe[Index].TypeID,fListe[Index].WaffType,fListe[Index].Schuss);
    end;
  end;

  // Lagerraum neuberechnen (Dadurch werden ungenauigkeiten ausgeglichen
  for Basis:=0 to high(fLagerCount) do
  begin
    Lager:=0;
    for Dummy:=0 to Self.Count-1 do
    begin
      LItem:=lager_api_GetItem(Dummy);
      Count:=fLagerCount[Basis].ItemCount[Dummy].Count;

      inc(Lager,round(LItem.LagerV)*10*Count);
    end;

    // Sicherung, damit es nicht zur Exception wegen unzureichendem Platz kommt (BUG 1105)
    if (Lager/10)<basis_api_LagerRaum(fLagerCount[Basis].BasisID) then
    begin
      basis_api_FreeLagerRaum(basis_api_BelegterLagerRaum(fLagerCount[Basis].BasisID),fLagerCount[Basis].BasisID);
      basis_api_NeedLagerRaum(Lager/10,fLagerCount[Basis].BasisID,true);
    end;
  end;
end;

function TLagerListe.AcceptAngebot(Index: Integer): Integer;
var
  Auftrag  : Integer;
  LItem    : TLagerItem;
  Money    : Integer;
  Item     : Integer;
begin
  result:=0;
  if (Index<0) and (Index>high(fAngebot)) then
    exit;

  Auftrag:=AuftragOfID(fAngebot[Index].ID);

  Item:=fAuftrag[Auftrag].Item;
  LItem:=fListe[Item];

  if fAuftrag[Auftrag].Kauf then
  begin
    result:=fAngebot[Index].Anzahl;
    Money:=result*fAngebot[Index].Preis;

    // Ausr�stung kaufen
    savegame_api_NeedMoney(Money,kbLK,true);

    // Anzahl der zu kaufenden Ausr�stung wird im LagerItem angegeben, dass DoTransport �bergeben wird
    LItem.Anzahl:=Angebote[Index].Anzahl;
    raumschiff_api_DoTransport(country_api_GetRandomLandPosition(Angebote[Index].Organisation,true),basis_api_GetBasisFromID(fAuftrag[Auftrag].BasisID).BasisPos,LItem,Angebote[Index].Preis,true);
    DeleteAuftrag(Auftrag);
  end
  else
  begin
    // Anzahl der Ausr�stung in der Basis ermitteln
    LItem.Anzahl:=lager_api_GetItemCountInBase(fAuftrag[Auftrag].BasisID,Item);

    // Verkaufbare Ausr�stung ermitteln
    result:=min(fAngebot[Index].Anzahl,LItem.Anzahl);

    if (result=0) then
      exit;

    DeleteItem(Item,fAuftrag[Auftrag].BasisID,result);

    dec(fAuftrag[Auftrag].Anzahl,result);

    LItem.Anzahl:=result;
    raumschiff_api_DoTransport(basis_api_GetBasisFromID(fAuftrag[Auftrag].BasisID).BasisPos,country_api_GetRandomLandPosition(Angebote[Index].Organisation,true),LItem,Angebote[Index].Preis,false);

    DeleteAngebot(Index);
    CorrectAngebot;

    if fAuftrag[Auftrag].Anzahl=0 then
      DeleteAuftrag(Auftrag);
  end;
end;

// F�gt in fLagercount eine neue Basis hinzu
procedure TLagerListe.AddBasis(Basis: TObject);
var
  Index: Integer;
begin
  Index:=length(fLagerCount);
  SetLength(fLagerCount,Index+1);
  fLagerCount[Index].BasisID:=TBasis(Basis).ID;
  SetLength(fLagerCount[Index].ItemCount,Count);
end;

procedure TLagerListe.AddForschItem(Projekt: TObject);
var
  Item      : TLagerItem;
  Forschung : PForschProject;
begin
  Assert(Projekt<>nil);
  Forschung:=PForschProject(Projekt);

  if (Forschung.TypeID in [ptEinrichtung,ptRaumSchiff,ptOrganisation,ptNone]) then
    exit;

  Item:=ForschProjektToLagerItem(Forschung^);
  Item.PublicPlan:=true;
  {$IFDEF COMPLETERESEARCH}
  Item.Useable:=true;
  Item.Visible:=true;
  Item.HerstellBar:=true;
  {$ENDIF}
  AddLagerItem(Item);

end;

procedure TLagerListe.AddItem(ID: Cardinal;BasisID: Cardinal; BelegRoom: boolean);
var
  Index : Integer;
  Dummy : Integer;
  Basis : TBasis;
begin
  Index:=IndexOfID(ID);

  if Index=-1 then
    exit;

  if BelegRoom then
  begin
    if not basis_api_NeedLagerRaum(fListe[Index].LagerV,BasisID,false) then
    begin
      Basis:=basis_api_GetBasisFromID(BasisID);
      raise ENotEnoughRoom.CreateFmt(ENoLagerRoom,[Basis.Name]);
    end;
  end;

  // Wenn es zu der ausgew�hlten Basis hinzugef�gt werden soll, kann die Anzahl
  // direkt in fListe erh�ht werden
  // Wenn es zu einen anderen Basis hinzugef�gt werden soll, so muss der entsprechende
  // Eintrag in fLagerCount gemacht werden
  if BasisID=basis_api_GetSelectedBasis.ID then
    inc(fListe[Index].Anzahl)
  else
  begin
    Dummy:=GetBaseIndexForItemCount(BasisID);

    Assert(Dummy<>-1);

    inc(fLagerCount[Dummy].ItemCount[Index].Count);
  end;
end;

function TLagerListe.AddItem(ID, BasisID: Cardinal; Count: Integer): Integer;
var
  Index : Integer;
  Dummy : Integer;
  Basis : TBasis;
  Room  : Integer;
begin
  Index:=IndexOfID(ID);

  result:=0;

  if Index=-1 then
    exit;

  Basis:=basis_api_GetBasisFromID(BasisID);
  // freien Lagerraum ermitteln (Berechnung des Lagerraumes immer mit Integers
  // und dem Faktor 10
  Room:=round((Basis.LagerRaum-Basis.LagerBelegt)*10);
  if round((fListe[Index].LagerV)*10)>0 then
    Count:=min(Count,Room div round((fListe[Index].LagerV)*10));

  if Count=0 then
    exit;

  basis_api_NeedLagerRaum((round(fListe[Index].LagerV*10)/10)*Count,BasisID);

  // Wenn es zu der ausgew�hlten Basis hinzugef�gt werden soll, kann die Anzahl
  // direkt in fListe erh�ht werden
  // Wenn es zu einen anderen Basis hinzugef�gt werden soll, so muss der entsprechende
  // Eintrag in fLagerCount gemacht werden
  if BasisID=basis_api_GetSelectedBasis.ID then
    inc(fListe[Index].Anzahl,Count)
  else
  begin
    Dummy:=GetBaseIndexForItemCount(BasisID);

    Assert(Dummy<>-1);

    inc(fLagerCount[Dummy].ItemCount[Index].Count,Count);
  end;

  result:=Count;
end;

procedure TLagerListe.AddLagerItem(const Item: TLagerItem);
var
  Index: Integer;
  Dummy: Integer;
begin
  Index:=Count;
  if fHashMap.FindKey(Item.ID,Dummy) then
    exit;

  fHashMap.InsertID(Item.ID,Index);

  // Ausr�stungsliste vergr��ern
  SetLength(fListe,Count+1);

  // Vergr��ern der Arrays zum Speichern der Anzahl in den anderen Basen
  for dummy:=0 to High(fLagerCount) do
    SetLength(fLagerCount[Dummy].ItemCount,Count);

  for Dummy:=0 to High(fLagerCount) do
  begin
    if Dummy=0 then
      fLagerCount[Dummy].ItemCount[Index].Count:=Item.Anzahl
    else
      fLagerCount[Dummy].ItemCount[Index].Count:=0;
  end;

  fListe[Index]:=Item;
end;

function TLagerListe.AddrLagerItem(Index: Integer): PLagerItem;
begin
  result:=nil;
  if (Index<0) or (Index>=Count) then
    exit;
  result:=Addr(fListe[Index]);
end;

function TLagerListe.AuftragOfID(ID: Cardinal): Integer;
var
  Dummy: Integer;
begin
  result:=-1;
  for Dummy:=0 to AuftragCount-1 do
  begin
    if fAuftrag[Dummy].ID=ID then
    begin
      result:=dummy;
    end;
  end;
end;

procedure TLagerListe.BuyPlan(Index: Integer);
var
  Preis  : Integer;
begin
  if fListe[Index].HerstellBar then
    exit;

  Preis:=PreisOfConstruction(Index);

  savegame_api_NeedMoney(Preis,kbLK,true);

  fListe[Index].HerstellBar:=true;
end;

// Wird aufgerufen, wenn die ausgew�hlte Basis ge�ndert wird
// Speichert die Anzahl der Ausr�stungen in fLagerCount
// Setzt in fListe die Anzahl aller Ausr�stungen auf die Werte
// die in fLagerCount angegeben wurden
procedure TLagerListe.ChangeBasis(Basis: TObject);
begin
  // Beim Laden wird die Basis gewechselt, bevor die lagerdaten geladen werden
  if length(fLagerCount)=0 then
    exit;

  SaveCountToItemCount;

  if Basis=nil then
    exit;

  LoadCountFromItemCount(Basis);
end;

procedure TLagerListe.CheckAngebote;
var
  Dummy1     : Integer;
  Dummy2     : Integer;
  Auftrag    : TAuftrag;
  Angebot    : TAngebot;
  Item       : PLagerItem;
  Preis      : Integer;
  Count      : Integer;
begin
  if (fWatchSettings.KaufType<>wtAccept) and (fWatchSettings.VerkaufType<>wtAccept) then exit;

  if (length(fAuftrag)=0) or (length(fAngebot)=0) then
    exit;

  for Dummy1:=high(fAuftrag) downto 0 do
  begin
    Auftrag:=fAuftrag[Dummy1];
    Item:=Addr(fListe[Auftrag.Item]);
    if (Auftrag.Kauf) and (fWatchSettings.KaufType=wtAccept) then
    begin
      Preis:=round(Item.KaufPreis*(fWatchSettings.KaufPreis/1000));
      for Dummy2:=high(fAngebot) downto 0 do
      begin
        Angebot:=fAngebot[Dummy2];
        if Angebot.ID=Auftrag.ID then
        begin
          if Angebot.Preis<=Preis then
          begin
            Count:=AcceptAngebot(Dummy2);
            savegame_api_Message(Format(ST0309260030,[Count,Item.Name,country_api_GetCountryName(Angebot.Organisation),Count*Angebot.Preis/1]),lmAngebote);

            break;
          end;
        end;
      end;
    end
    else if (not Auftrag.Kauf) and (fWatchSettings.VerkaufType=wtAccept) then
    begin
      Preis:=round(Item.VerKaufPreis*(fWatchSettings.VerkaufPreis/1000));
      Dummy2:=high(fAngebot);
      while (Dummy2>0) do
      begin
        Angebot:=fAngebot[Dummy2];
        if Angebot.ID=Auftrag.ID then
        begin
          if Angebot.Preis>=Preis then
          begin
            Count:=AcceptAngebot(Dummy2);
            if Count>0 then
            begin
              savegame_api_Message(Format(ST0309260031,[Count,Item.Name,Count*Angebot.Preis/1,country_api_GetCountryName(Angebot.Organisation)]),lmAngebote);
              // Von hinten neu anfangen, da durch Acceptangebot viele Angebot gel�scht
              // worden sein k�nnen
              Dummy2:=length(fAngebot);
            end;
          end;
        end;
        dec(Dummy2);
      end;
    end;
  end;
end;

procedure TLagerListe.Clear;
begin
  SetLength(fListe,0);
  SetLength(fAuftrag,0);
  SetLength(fAngebot,0);
  SetLength(fLagerCount,0);

  fLoadedBasis:=nil;

  fHashMap.ClearList;
end;

function TLagerListe.CompareAngebote(Index1, Index2: Integer;
  Typ: TFunctionType): Integer;
var
  Temp     : TAngebot;
  Ang1,Ang2: TAngebot;
begin
  result:=-1;
  case Typ of
    ftCompare  :
    begin
      Ang1:=fAngebot[Index1];
      Ang2:=fAngebot[Index2];
      if Ang1.ID=Ang2.ID then
      begin
        if fAuftrag[AuftragOfID(Ang1.ID)].Kauf then
          result:=Ang1.Preis-Ang2.Preis
        else
          result:=Ang2.Preis-Ang1.Preis;
      end;
    end;
    ftExchange :
    begin
      Temp:=fAngebot[Index1];
      fAngebot[Index1]:=fAngebot[Index2];
      fAngebot[Index2]:=Temp;
    end;
  end;
end;

procedure TLagerListe.CorrectAngebot;
var
  Dummy: Integer;
  delete: boolean;
begin
  Dummy:=0;
  while (Dummy<AngebotCount) do
  begin
    delete:=false;
    if AuftragOfID(fAngebot[Dummy].ID)=-1 then delete:=true;
    if not delete then
    begin
      if fAngebot[Dummy].Rounds=0 then
        delete:=true;
      if fAngebot[Dummy].Anzahl>fAuftrag[AuftragOfID(fAngebot[Dummy].ID)].Anzahl then
        delete:=true;
    end;
    if delete then
      DeleteAngebot(Dummy)
    else
      inc(Dummy);
  end;
end;

constructor TLagerListe.Create;
begin
  inherited;

  lager_api_init(Self);

  fItemIcons := game_api_CreateImageListItem;
  fItemIcons.Transparent:=true;
  fItemIcons.TransparentColor:=clFuchsia;
  fItemIcons.SystemMemory:=true;

  fEquipIcons := game_api_CreateImageListItem;
  fEquipIcons.Transparent:=true;
  fEquipIcons.TransparentColor:=clFuchsia;
  fEquipIcons.SystemMemory:=true;

  fHashMap:=TIDHashMap.Create;

  basis_api_RegisterNewBaseHandler(AddBasis);
  basis_api_RegisterDestroyBaseHandler(DeleteBasis);
  basis_api_RegisterChangeBaseHandler(ChangeBasis);

  forsch_api_RegisterProjektEndHandler(AddForschItem);
  forsch_api_RegisterUpgradeEndHandler(UpgradeID);

  savegame_api_RegisterCustomSaveHandler(SaveToStream,LoadFromStream,nil,$2006D957);
  savegame_api_RegisterAfterLoadHandler(AfterLoadHandler);

  savegame_api_RegisterNewGameHandler(NewGameHandler);
  savegame_api_RegisterHourEndHandler(NextHour);
end;

function TLagerListe.CreateAngebote(Minuten: Integer): Integer;
var
  Dummy      : Integer;
  Organ      : Integer;
  Angebot    : TAngebot;
  Item       : TLagerItem;
  Auftrag    : PAuftrag;
  Preis      : Integer;
  DummyTage  : Integer;

  procedure SendAngebotsMessage(const Auftrag: TAuftrag;const Angebot: TAngebot);
  var
    Text: String;
  begin
    if Auftrag.Kauf then
      Text:=ST0309260033
    else
      Text:=ST0309260034;
    savegame_api_Message(Format(ST0309260032,[country_api_GetCountryName(Angebot.Organisation),Text,Auftrag.Anzahl,Item.Name]),lmAngebote);
  end;

begin
  CorrectAngebot;
  result:=0;
  Auftrag:=Addr(fAuftrag[0]);
  for Dummy:=0 to AuftragCount-1 do
  begin
    Item:=fListe[Auftrag.Item];
    //es wird verkauft oder die Ausr�stung ist schon auf dem Markt vorhanden
    if (not Auftrag.Kauf) or (ItemKaufbar(Item, DummyTage)) then
    begin
      if Auftrag.Kauf and (Item.Land>-1) then
      begin
        if not OrganHasAngebot(Auftrag.ID,fListe[Auftrag.Item].Land) then
        begin
          if country_api_CreateAngebot(Auftrag^,Angebot,fListe[Auftrag.Item].Land,Minuten) then
          begin
            SetLength(fAngebot,AngebotCount+1);
            inc(result);
            fAngebot[AngebotCount-1]:=Angebot;
            if fWatchSettings.KaufType=wtMessage then
            begin
              Preis:=round(Item.KaufPreis*(fWatchSettings.KaufPreis/1000));
              if (Angebot.Preis<=Preis) then
                SendAngebotsMessage(Auftrag^,Angebot);
            end;
          end;
        end;
      end
      else
      begin
        if Auftrag.Kauf then
          Preis:=round(Item.KaufPreis*(fWatchSettings.KaufPreis/1000))
        else
          Preis:=round(Item.VerkaufPreis*(fWatchSettings.VerkaufPreis/1000));
        for Organ:=0 to country_api_GetCountryCount-1 do
        begin
          if not OrganHasAngebot(Auftrag.ID,Organ) then
          begin
            if country_api_CreateAngebot(Auftrag^,Angebot,Organ,Minuten) then
            begin
              SetLength(fAngebot,AngebotCount+1);
              inc(result);
              fAngebot[AngebotCount-1]:=Angebot;
              if (Auftrag.Kauf and (Angebot.Preis<=Preis) and (fWatchSettings.KaufType=wtMessage)) or
                 ((not Auftrag.Kauf) and (Angebot.Preis>=Preis) and (fWatchSettings.VerkaufType=wtMessage)) then
              begin
                SendAngebotsMessage(Auftrag^,Angebot);
              end;
            end;
          end;
        end;
      end;
    end;
    inc(Auftrag);
  end;

  // Sortieren der Angebote
  if result>0 then
  begin
    QuickSort(0,High(fAngebot),CompareAngebote);
    CheckAngebote;
  end;
end;

procedure TLagerListe.DeleteAngebot(Index: Integer);
begin
  if (Index<0) and (Index>AngebotCount-1) then exit;
    DeleteArray(Addr(fAngebot),TypeInfo(TAngebotArray),Index);
end;

procedure TLagerListe.DeleteAuftrag(Index: Integer);
begin
  if (Index<0) and (Index>AuftragCount-1) then exit;
    DeleteArray(Addr(fAuftrag),TypeInfo(TAuftragArray),Index);
end;

// L�scht aus fLagercount eine zerst�rte Basis
procedure TLagerListe.DeleteBasis(Basis: TObject);
var
  Dummy: Integer;
begin
  if TBasis(Basis).ID=TBasis(fLoadedBasis).ID then
    fLoadedBasis:=nil;

  Dummy:=GetBaseIndexForItemCount(Basis);

  Assert(Dummy<>-1);

  fLagerCount[Dummy]:=fLagerCount[High(fLagerCount)];
  SetLength(fLagerCount,length(fLagerCount)-1);
end;

procedure TLagerListe.DeleteFromBasis(ID, BasisID: Cardinal);
begin
  DeleteItem(IndexOfID(ID),BasisID);
end;

function TLagerListe.DeleteItem(Index: Integer;BasisID: Cardinal;Count: Integer; FreeRoom: Boolean): Boolean;
var
  Dummy: Integer;
  Item : TLagerItem;
begin
  result:=false;
  ItemFromBasis(Index,BasisID,Item);
  if Item.Anzahl<Count then
    exit;
//    raise ENotEnoughItems.Create(Format(ENotItems,[fListe[Index].Name]));

  // Raum freigeben
  if FreeRoom then
    basis_api_FreeLagerRaum(fListe[Index].LagerV*Count,BasisID);

  if BasisID=basis_api_GetSelectedBasis.ID then
    dec(fListe[Index].Anzahl,Count)
  else
  begin
    Dummy:=GetBaseIndexForItemCount(BasisID);

    Assert(Dummy<>-1);

    dec(fLagerCount[Dummy].ItemCount[Index].Count,Count);
  end;

  result:=true;

end;

destructor TLagerListe.destroy;
begin
  fHashMap.Free;
  SetLength(fListe,0);
  inherited;
end;

procedure TLagerListe.DrawImage(Image: Integer;Surface: TDirectDrawSurface;x,y: Integer;
  Index: Integer;Shadow: boolean);
var
  PatRect: TRect;
  Source: TDirectDrawSurface;
  Collection : TPictureCollectionItem;
begin
  if Image<0 then
    exit;

  Collection:=GetImageOfIndex(Index);

  if Image>=Collection.PatternCount then
    exit;

  PatRect:=Collection.PatternRects[Image];
  Source:=Collection.PatternSurfaces[Image];
  Assert(Source<>nil);
  if Shadow then
  begin
    DrawShadow(Surface,x,Y,PatRect,Source,Source.Pixels[PatRect.Left,PatRect.Top]);
  end
  else
  begin
    Source.TransparentColor:=Source.Pixels[PatRect.Left,PatRect.Top];
    Surface.Draw(x,y,PatRect,Source,true);
  end;
end;

procedure TLagerListe.DrawImageAlpha(Image: Integer;
  Surface: TDirectDrawSurface; x, y, Alpha: Integer);
var
  PatRect: TRect;
  Source: TDirectDrawSurface;
begin
  if (Image<0) or (Image>=fItemIcons.PatternCount) then
    exit;

  PatRect:=fItemIcons.PatternRects[Image];
  Source:=fItemIcons.PatternSurfaces[Image];
  Source.TransparentColor:=Source.Pixels[PatRect.Left,PatRect.Top];
  DrawTransAlpha(Surface,X,Y,PatRect,Source,Alpha,Source.Pixels[PatRect.Left,PatRect.Top]);
end;

procedure TLagerListe.DrawImageISO(Image: Integer;
  Surface: TDirectDrawSurface; x, y: Integer);
var
  PatRect: TRect;
  Source: TDirectDrawSurface;
begin
  if (Image<0) or (Image>=fItemIcons.PatternCount) then
    exit;

  PatRect:=fItemIcons.PatternRects[Image];
  Source:=fItemIcons.PatternSurfaces[Image];
  Source.TransparentColor:=Source.Pixels[PatRect.Left,PatRect.Top];
//  Surface.Draw(X,Y,PatRect,Source,true);
  Surface.StretchDraw(Rect(x,y,x+16,y+16),PatRect,Source,true);
end;

function TLagerListe.GetAngebot(Index: Integer): TAngebot;
begin
  if (Index<0) and (Index>AngebotCount-1) then
    exit;
  result:=fAngebot[Index];
end;

function TLagerListe.GetAngebotCount: Integer;
begin
  result:=length(fAngebot);
end;

function TLagerListe.GetAuftrag(Index: Integer): TAuftrag;
begin
  if (Index<0) or (Index>AuftragCount-1) then
    exit;
  result:=fAuftrag[Index];
end;

function TLagerListe.GetAuftragCount: Integer;
begin
  result:=length(fAuftrag);
end;

function TLagerListe.GetCount: Integer;
begin
  result:=length(fListe);
end;

function TLagerListe.GetImageOfIndex(
  Index: Integer): TPictureCollectionItem;
begin
  result:=nil;
  if Index=0 then
    result:=fItemIcons
  else if Index=1 then
    result:=fEquipIcons
  else
    Assert(false);
end;

function TLagerListe.GetItemCountInBase(Index: Integer; BasisID: Cardinal): Integer;
var
  Dummy: Integer;
begin
  // Ist die Basis ausgew�hlt kann die Anzahl aus der Liste �bernommen werden,
  // ansonsten Anzahl aus fLagerCount holen
  if basis_api_GetSelectedBasis.ID=BasisID then
  begin
    result:=fListe[Index].Anzahl;
    exit;
  end;

  Dummy:=GetBaseIndexForItemCount(BasisID);

  Assert(Dummy<>-1);

  result:=fLagerCount[Dummy].ItemCount[Index].Count;
end;

function TLagerListe.GetLagerItem(Index: Integer): TLagerItem;
begin
  if (Index<0) or (Index>Count-1) then exit;
  result:=fListe[Index];
end;

function TLagerListe.GetMaxMunition(Index: Integer): Integer;
var
  Dummy: Integer;
  ID   : Cardinal;
begin
  if fListe[Index].WaffType=wtLaser then result:=fListe[Index].Munition else
  begin
    ID:=fListe[Index].ID;
    result:=-1;
    for Dummy:=0 to Count-1 do
    begin
      if ID=fListe[Dummy].Munfor then
      begin
        result:=fListe[Dummy].Munition;
        exit;
      end;
    end;
  end;
end;

function TLagerListe.GetRandomItem(ItemType: TProjektTypes;
  IQ: Integer; Weapon: Cardinal;Alien: Boolean): PLagerItem;
var
  Dummy   : Integer;
  Items   : Array of record
    Index : Integer;
    Chance: Integer;
  end;
  Index   : Integer;
  Total   : Integer;

  procedure AddItem(Item: Integer);
  begin
    if Index>=length(Items) then
      SetLength(Items,length(Items)+10);

    inc(Total,fListe[Item].AlienChance);

    Items[Index].Index:=Item;
    Items[Index].Chance:=Total;

    inc(Index);
  end;

begin
  result:=nil;
  Index:=0;
  Total:=0;

  if length(fListe)=0 then
    exit;

  for Dummy:=0 to high(fListe) do
  begin
    with fListe[Dummy] do
    begin
      // Pr�fen, ob Ausr�stung Alienausr�stung ist
      if Alien<>AlienItem then
        continue;

      // Pr�fung Ausr�stungs-Typ und IQ
      if (not (TypeID in ItemType)) or (NeedIQ>IQ) then
        continue;

      // Pr�fen, ob Munition zur angegeben Waffe geh�rt
      if (Weapon<>0) and (Munfor<>Weapon) then
        continue;
    end;

    // Ausr�stung erf�llt die vorraussetzungen
    AddItem(Dummy);
  end;

  if length(Items)>0 then
  begin
    Dummy:=random(Total);
    Index:=0;
    while Dummy>Items[Index].Chance do
      inc(Index);

    Assert(Index<length(Items));
    result:=Addr(fListe[Items[Index].Index]);
  end;

end;

function TLagerListe.IndexOfID(ID: Cardinal): Integer;
begin
  if not fHashMap.FindKey(ID,result) then
    result:=-1;
end;

function TLagerListe.AuftragInSelectedBasis(Dummy: Integer): boolean;
begin
  result:=fAuftrag[Dummy].BasisID=basis_api_GetSelectedBasis.ID;
end;

procedure TLagerListe.ItemFromBasis(Index: Integer; BasisID: Cardinal;
  out Item: TLagerItem);
var
  BasisIndex: Integer;
begin
  Item:=Items[Index];
  if basis_api_GetSelectedBasis.ID=BasisID then
    exit;

  // Als erstes wird der Index der Basis gesucht, die ausgew�hlt wurde
  BasisIndex:=GetBaseIndexForItemCount(BasisID);
  Item.Anzahl:=fLagerCount[BasisIndex].ItemCount[Index].Count;
end;

procedure TLagerListe.LoadFromStream(Stream: TStream);
var
  Rec     : TExtRecord;
  Dummy   : Integer;
  Dummy2  : Integer;
begin
  // Speichern der aktuell ausgew�hlten Basis in fLagerCount
  Clear;

  Rec:=TExtRecord.Create(TLagerListRecord);
  Rec.LoadFromStream(Stream);

  with Rec.GetRecordList('Items') do
  begin
    SetLength(fListe,Count);
    for Dummy:=0 to high(fListe) do
    begin
      fListe[Dummy]:=RecordToLagerItem(Item[Dummy]);
      fHashMap.InsertID(fListe[Dummy].ID,Dummy);
    end;
  end;

  with Rec.GetRecordList('Auftraege') do
  begin
    SetLength(fAuftrag,Count);
    for Dummy:=0 to high(fAuftrag) do
      fAuftrag[Dummy]:=RecordToLagerAuftrag(Item[Dummy]);
  end;

  with Rec.GetRecordList('Angebote') do
  begin
    SetLength(fAngebot,Count);
    for Dummy:=0 to high(fAngebot) do
      fAngebot[Dummy]:=RecordToLagerAngebot(Item[Dummy]);
  end;

  // Arbeitsmarkteinstellungen speichern
  fWatchSettings.KaufPreis:=Rec.GetInteger('KaufPreis');
  fWatchSettings.KaufType:=TWatchType(Rec.GetInteger('KaufType'));
  fWatchSettings.VerkaufPreis:=Rec.GetInteger('VerkaufPreis');
  fWatchSettings.VerkaufType:=TWatchType(Rec.GetInteger('VerkaufType'));

  with Rec.GetRecordList('BasisItems') do
  begin
    SetLength(fLagerCount,Count);
    for Dummy:=0 to Count-1 do
    begin
      fLagerCount[Dummy].BasisID:=Item[Dummy].GetCardinal('BasisID');
      SetLength(fLagerCount[Dummy].ItemCount,length(fListe));
      with Item[Dummy].GetRecordList('BasisItemCount') do
      begin
        for Dummy2:=0 to high(fLagerCount[Dummy].ItemCount) do
        begin
          fLagerCount[Dummy].ItemCount[Dummy2].Count:=Item[Dummy2].GetInteger('ItemCount');
          fLagerCount[Dummy].ItemCount[Dummy2].ShootSurPlus:=Item[Dummy2].GetInteger('ShootSurPlus');
        end;
      end;
    end;
  end;

  Rec.Free;
end;

procedure TLagerListe.NewAuftrag(Item: Integer; Kaufen: boolean;
  Anzahl: Integer);
var
  Index: Integer;
  DummyTage: Integer;
begin
  //Pr�fen, ob es sich um Alienausr�stung handelt
  if Items[Item].AlienItem then
  begin
    if Kaufen then
      raise EErrorGUIException.Create(MK0505050002)
    else
      raise EErrorGUIException.Create(MK0505050001);
  end;

  // Pr�fen, ob die Ausr�stung gekauft werden kann
  if not fListe[Item].PublicPlan then
    raise EErrorGUIException.Create(ST0310250001);

  // Pr�fen, ob die Ausr�stung verkauft werden kann
  if (not Kaufen) and ((Items[Item].VerKaufPreis=0) or (Items[Item].AlienItem)) then
    raise EErrorGUIException.Create(Format(ST0401120005,[fListe[Item].Name]));

  // Pro Ausr�stung darf es nur einen Auftrag geben. Was passiert, wenn ein zweiter
  // Auftrag gestartet wird, muss noch gekl�rt werden. Bisher wird der Alte Auftrag
  // einfach gel�scht und die Angebote verworfen

  Index:=FindAuftrag(Item);
  if Index=-1 then
  begin
    SetLength(fAuftrag,AuftragCount+1);
    Index:=AuftragCount-1;

    fAuftrag[Index].ID:=random(High(Cardinal));
    fAuftrag[Index].Kauf:=Kaufen;
    fAuftrag[Index].Item:=Item;
    fAUftrag[Index].BasisID:=basis_api_GetSelectedBasis.ID;
  end
  else
  begin
    // Neue ID vergeben, wenn Kaufen/Verkaufen ge�ndert wird
    if fAuftrag[Index].Kauf<>Kaufen then
      fAuftrag[Index].ID:=random(High(Cardinal));
  end;

  fAuftrag[Index].Anzahl:=Anzahl;

  if Kaufen then
    fAuftrag[Index].Preis:=Items[Item].KaufPreis
  else
    fAuftrag[Index].Preis:=Items[Item].VerKaufPreis;

  CorrectAngebot;

  // Pr�fen, ob die Ausr�stung gleich gekauft werden kann -> nur Hinweis

  if Kaufen and (not ItemKaufbar(Items[Item], DummyTage)) and (DummyTage>1) then
      raise EHintGUIException.Create(Format(MK0504160001,[DummyTage]));
end;

procedure TLagerListe.NextHour(Sender: TObject);
var
  Dummy: Integer;
begin
  Dummy:=0;
  while (Dummy<AngebotCount) do
  begin
    dec(fAngebot[Dummy].Rounds,1);
    inc(Dummy);
  end;
  CorrectAngebot;
end;

function TLagerListe.OrganHasAngebot(ID: Cardinal;Organ: Integer): boolean;
var
  Dummy: Integer;
begin
  result:=false;
  for Dummy:=0 to AngebotCount-1 do
  begin
    if (fAngebot[Dummy].ID=ID) and (fAngebot[Dummy].Organisation=Organ) then
    begin
      result:=true;
    end;
  end;
end;

function TLagerListe.PreisOfConstruction(Index: Integer): Integer;
var
  Land  : Integer;
begin
  result:=0;
  if fListe[Index].HerstellBar then exit;
  Land:=fListe[Index].Land;
  if Land=-1 then exit;
  result:=round((((100-country_api_GetCountry(Land).Friendly)*0.2)+2)*fListe[Index].KaufPreis);
end;

procedure TLagerListe.RegisterAlienItem(Item: TLagerItem);
var
  Index : Integer;
begin
  Index:=IndexOfID(Item.ID);
  if Index=-1 then
  begin
    Item.Useable:=false;
    Item.PublicPlan:=false;
    Item.Visible:=true;
    Item.Anzahl:=0;

    AddLagerItem(Item);
  end
  else
  begin
    with fListe[Index] do
    begin
      // Ausr�stung kann bereits erforscht, bzw. ist schon erforscht
      if Visible then
        exit;

      Useable:=false;
      PublicPlan:=false;
      Visible:=true;
    end;
  end;

  forsch_api_AddAlienItemResearch(Item.ID);
end;

procedure TLagerListe.ResearchComplete(ID: Cardinal);
var
  Index: Integer;
begin
  Index:=IndexOfID(ID);
  if Index<>-1 then
  begin
    fListe[Index].Useable:=true;
    fListe[Index].PublicPlan:=true;
  end;
end;

procedure TLagerListe.SaveToStream(Stream: TStream);
var
  Dummy  : Integer;
  Dummy2 : Integer;
  Rec    : TExtRecord;
begin
  // Speichern der aktuell ausgew�hlten Basis in fLagerCount
  SaveCountToItemCount;

  Rec:=TExtRecord.Create(TLagerListRecord);

  with Rec.GetRecordList('Items') do
  begin
    for Dummy:=0 to high(fListe) do
      Add(LagerItemToRecord(fListe[Dummy]));
  end;

  with Rec.GetRecordList('Auftraege') do
  begin
    for Dummy:=0 to high(fAuftrag) do
      Add(LagerAuftragToRecord(fAuftrag[Dummy]));
  end;

  with Rec.GetRecordList('Angebote') do
  begin
    for Dummy:=0 to high(fAngebot) do
      Add(LagerAngebotToRecord(fAngebot[Dummy]));
  end;

  // Arbeitsmarkteinstellungen speichern
  Rec.SetInteger('KaufPreis',fWatchSettings.KaufPreis);
  Rec.SetInteger('KaufType',Integer(fWatchSettings.KaufType));
  Rec.SetInteger('VerkaufPreis',fWatchSettings.VerkaufPreis);
  Rec.SetInteger('VerkaufType',Integer(fWatchSettings.VerkaufType));

  with Rec.GetRecordList('BasisItems') do
  begin
    for Dummy:=0 to high(fLagerCount) do
    begin
      with Add do
      begin
        SetCardinal('BasisID',fLagerCount[Dummy].BasisID);

        for Dummy2:=0 to high(fLagerCount[Dummy].ItemCount) do
        begin
          with GetRecordList('BasisItemCount').Add do
          begin
            SetInteger('ItemCount',fLagerCount[Dummy].ItemCount[Dummy2].Count);
            SetInteger('ShootSurPlus',fLagerCount[Dummy].ItemCount[Dummy2].ShootSurPlus);
          end;
        end;
      end;
    end;
  end;

  Rec.SaveToStream(Stream);
  Rec.Free;
end;

procedure TLagerListe.SetLagerBitmap(Bitmap: TBitmap; Index: Integer; Global: boolean);
var
  Bit: TBitmap;
begin
  if Index=0 then
  begin
    if Global then
    begin
      fItemIcons.Picture.Bitmap:=Bitmap;
      fItemIcons.PatternHeight:=32;
      fItemIcons.PatternWidth:=32;
      fItemIcons.Restore;
      fGlobalImages:=Bitmap.Width div 32;
    end
    else
    begin
      Bit:=fItemIcons.Picture.Bitmap;
      Bit.Width:=fGlobalImages*32;
      Bit.Width:=(fGlobalImages*32)+(Bitmap.Width);
      Bit.Canvas.Draw(fGlobalImages*32,0,Bitmap);
      fItemIcons.Restore;
    end;
  end
  else
  begin
    fEquipIcons.Picture.Bitmap:=Bitmap;
    fEquipIcons.PatternHeight:=62;
    fEquipIcons.PatternWidth:=39;
    fEquipIcons.Restore;
  end;
end;

procedure TLagerListe.UpgradeID(ID: TObject);
var
  Dummy : Integer;
  Item  : TItemUpgrade;
begin
  for Dummy:=0 to Count-1 do
  begin
    if fListe[Dummy].ID=Cardinal(ID) then
    begin
      Item.Old:=fListe[Dummy];
      lager_api_UpgradeItem(Item);
      fListe[Dummy]:=Item.New;

      savegame_api_Message(Format(MUpgradeEnd,[fListe[Dummy].Name,fListe[Dummy].Level]),lmProjektEnd);
      break;
    end;
  end;
end;

procedure TLagerListe.SaveCountToItemCount;
var
  Dummy     : Integer;
  BaseIndex : Integer;
begin
  if fLoadedBasis=nil then
    exit;

  BaseIndex:=GetBaseIndexForItemCount(fLoadedBasis);

  if BaseIndex=-1 then
    exit;

  with fLagerCount[BaseIndex] do
  begin
    for Dummy:=0 to Count-1 do
    begin
      ItemCount[Dummy].Count:=fListe[Dummy].Anzahl;
      ItemCount[Dummy].ShootSurPlus:=fListe[Dummy].ShootSurPlus;
    end;
  end;
end;

procedure TLagerListe.LoadCountFromItemCount(Basis: TObject);
var
  Dummy     : Integer;
  BaseIndex : Integer;
begin
  BaseIndex:=GetBaseIndexForItemCount(Basis);

  Assert(BaseIndex<>-1);

  with fLagerCount[BaseIndex] do
  begin
    for Dummy:=0 to Count-1 do
    begin
      fListe[Dummy].Anzahl:=ItemCount[Dummy].Count;
      fListe[Dummy].ShootSurPlus:=ItemCount[Dummy].ShootSurPlus;
    end;
  end;

  fLoadedBasis:=Basis;
end;

procedure TLagerListe.NewGameHandler(Sender: TObject);
var
  Entry      : TForschProject;
  Projects   : TRecordArray;
  Dummy      : Integer;
  CanAdd     : boolean;
begin
  Clear;

  Projects:=savegame_api_GameSetGetProjects;
  for Dummy:=0 to high(Projects) do
  begin
    Entry:=RecordToProject(Projects[Dummy]);

    {$IFDEF COMPLETERESEARCH}
    CanAdd:=true;
    {$ELSE}
    CanAdd:=forsch_api_isStartProject(Entry) and (Entry.TypeID in LagerItemType);
    {$ENDIF}
    if CanAdd then
    begin
      basis_api_NeedLagerRaum(Entry.Anzahl*Entry.LagerV);
      AddForschItem(TObject(Addr(Entry)));
    end;
  end;

  fWatchSettings.KaufPreis:=100;
  fWatchSettings.KaufType:=wtNone;
  fWatchSettings.VerkaufPreis:=100;
  fWatchSettings.VerkaufType:=wtNone;

  fLoadedBasis:=basis_api_GetSelectedBasis;

  SetLength(fLagerCount,basis_api_GetBasisCount);

  for Dummy:=0 to high(fLagerCount) do
  begin
    fLagerCount[Dummy].BasisID:=basis_api_GetBasisFromIndex(Dummy).ID;
    SetLength(fLagerCount[Dummy].ItemCount,length(fListe));
  end;

  SaveCountToItemCount;
end;

function TLagerListe.GetBaseIndexForItemCount(Basis: TObject): Integer;
begin
  for result:=0 to high(fLagerCount) do
  begin
    if fLagerCount[result].BasisID=TBasis(Basis).ID then
      exit;
  end;
  result:=-1;
end;

function TLagerListe.GetBaseIndexForItemCount(BasisID: Cardinal): Integer;
begin
  for result:=0 to high(fLagerCount) do
  begin
    if fLagerCount[result].BasisID=BasisID then
      exit;
  end;
  result:=-1;
end;

var
  Temp1: TExtRecordDefinition;
  Temp2: TExtRecordDefinition;

procedure TLagerListe.AfterLoadHandler(Sender: TObject);
begin
  LoadCountFromItemCount(basis_api_GetSelectedBasis);
  Abgleich;
end;

function TLagerListe.AddMunition(ID, BasisID: Cardinal;
  Schuss: Integer): Boolean;
var
  Item      : PLagerItem;
  BaseIndex : Integer;
  ItemIndex : Integer;
begin
  Item:=lager_api_GetItem(ID);

  result:=false;
  if (Item.TypeID<>ptMunition) then
  begin
    if Schuss>0 then
      result:=AddItem(ID,BasisID,1)=1
    else
      result:=DeleteItem(IndexOfID(ID),BasisID);
    exit;
  end;

  if BasisID=basis_api_GetSelectedBasis.ID then
  begin
    inc(Item.ShootSurPlus,Schuss);

    if Item.ShootSurPlus >= Item.Munition then
    begin
      if AddItem(ID,BasisID,1)=1 then
      begin
        dec(Item.ShootSurPlus,Item.Munition);
        result:=true;
      end
      else
        dec(Item.ShootSurPlus,Schuss);
    end
    else if Item.ShootSurPlus < 0 then
    begin
      if DeleteItem(IndexOfID(ID),BasisID) then
      begin
        inc(Item.ShootSurPlus,Item.Munition);
        result:=true;
      end
      else
        dec(Item.ShootSurPlus,Schuss);
    end
    else
      result:=true;
  end
  else
  begin
    BaseIndex:=GetBaseIndexForItemCount(BasisID);

    Assert(BaseIndex<>-1);

    ItemIndex:=IndexOfID(ID);

    with fLagerCount[BaseIndex].ItemCount[ItemIndex] do
    begin
      inc(ShootSurPlus,Schuss);

      if ShootSurPlus >= Item.Munition then
      begin
        if AddItem(ID,BasisID,1)=1 then
        begin
          dec(ShootSurPlus,Item.Munition);
          result:=true;
        end
        else
          dec(ShootSurPlus,Schuss);
      end
      else if Item.ShootSurPlus < 0 then
      begin
        if DeleteItem(IndexOfID(ID),BasisID) then
        begin
          inc(ShootSurPlus,Item.Munition);
          result:=true;
        end
        else
          dec(ShootSurPlus,Schuss);
      end
      else
        result:=true;
    end;
  end;
end;

function TLagerListe.FindAuftrag(Item: Integer; BasisID: Cardinal): Integer;
var
  Dummy: Integer;
begin
  BasisID:=basis_api_GetBasisFromID(BasisID).ID;
  for Dummy:=0 to high(fAuftrag) do
  begin
    if (fAuftrag[Dummy].Item=Item) and (fAuftrag[Dummy].BasisID=BasisID) then
    begin
      result:=Dummy;
      Exit ;
    end;
  end;
  
  result:=-1;
end;

function TLagerListe.GetAngebotCountForAuftrag(ID: Cardinal): Integer;
var
  Dummy: Integer;
begin
  result:=0;
  for Dummy:=0 to high(fAngebot) do
  begin
    if fAngebot[Dummy].ID=ID then
      inc(result);
  end;
end;

function TLagerListe.ItemKaufbar(const LagerItem: TLagerItem; out Tage: Integer): boolean;
var
  CurrentDate: TKD4Date;
  DateStart  : TDateTime;
  DateCurrent: TDateTime;
begin
  CurrentDate := savegame_api_GetDate;

  //KD4Date ins TDate umwandeln
  try
    DateCurrent := EncodeDate(CurrentDate.Year, CurrentDate.Month, CurrentDate.Day);
    result:=true;
  except
    result:=false;
    Tage:=60; //2 Monate
  end;

  if Result then
  begin
    DateStart := LagerItem.ResearchedDate;
    DateStart := IncMonth(DateStart, 2); //Zwei Monate sp�ter
    Result := DateCurrent>=DateStart;
    if not Result then
    begin
      Tage := round(DateStart-DateCurrent);
    end;
  end;
end;

function TLagerListe.RecycleItem(Index: Integer; BasisID: Cardinal;
  Count: Integer): Boolean;
var
  Item: TLagerItem;
  Gewinn: Integer;
  Auftrag: Integer;
begin
  ItemFromBasis(Index,BasisID,Item);
  Gewinn := lager_api_GetRecyclingAlphatron(Item)*Count;
  Result := game_api_Question(Format(MK0505030002, [Gewinn]), MK0505030001) and DeleteItem(Index, BasisID, Count);

  if Result then
  begin
    dec(Item.Anzahl,Count);
    savegame_api_FreeAlphatron(Gewinn, BasisID);

    Auftrag:=FindAuftrag(Index);
    if Auftrag<>-1 then
    begin
      if Item.Anzahl<fAuftrag[Auftrag].Anzahl then
      begin
        fAuftrag[Auftrag].Anzahl:=Item.Anzahl;
        if Item.Anzahl=0 then
          DeleteAuftrag(Auftrag)
        else
          CorrectAngebot;
      end;
    end;
  end;
end;

initialization

  TLagerListRecord:=TExtRecordDefinition.Create('TLagerListRecord');

  TLagerListRecord.AddRecordList('Items',RecordLagerListe);
  TLagerListRecord.AddRecordList('Auftraege',RecordLagerAuftrag);
  TLagerListRecord.AddRecordList('Angebote',RecordLagerAngebot);

  TLagerListRecord.AddInteger('KaufPreis',0,high(Integer));
  TLagerListRecord.AddInteger('KaufType',0,high(Integer));
  TLagerListRecord.AddInteger('VerkaufPreis',0,high(Integer));
  TLagerListRecord.AddInteger('VerkaufType',0,high(Integer));

  Temp2:=TExtRecordDefinition.Create('ItemCount');
  Temp2.AddInteger('ItemCount',0,high(Integer));
  Temp2.AddInteger('ShootSurPlus',0,high(Integer));
  Temp2.ParentDescription:=true;

  Temp1:=TExtRecordDefinition.Create('BasisItemCount');
  Temp1.AddCardinal('BasisID',0,high(Cardinal));
  Temp1.AddRecordList('BasisItemCount',Temp2);
  Temp1.ParentDescription:=true;

  TLagerListRecord.AddRecordList('BasisItems',Temp1);

finalization
  TLagerListRecord.Free;
end.


