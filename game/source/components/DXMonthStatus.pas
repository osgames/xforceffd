{********************************************************************************
*										*
* Copyright (C) 2004  X-Force Team - jim_raynor@web.de				*
*										*
* This file is part of X-Force: Fight For Destiny (X-Force).			*
*										*
* X-Force is free software; you can redistribute it and/or modify		*
* it under the terms of the GNU General Public License as published by		*
* the Free Software Foundation; either version 2 of the License, or		*
* (at your option) any later version.						*
*										*
* X-Force is distributed in the hope that it will be useful,			*
* but WITHOUT ANY WARRANTY; without even the implied warranty of		*
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the			*
* GNU General Public License for more details.					*
*										*
* You should have received a copy of the GNU General Public License		*
* along with X-Force; if not, write to the Free Software			*
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA	*
*										*
*********************************************************************************
*										*
* Anzeige der Finanzübersicht (Einnahmen/Ausgaben)				*
*										*
********************************************************************************}

{$I ../Settings.inc}

unit DXMonthStatus;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  DXContainer,DXDraws, XForce_types, StringConst, Blending, DXClass, TraceFile,
  DirectDraw, DirectFont;

type
  TDXMonthStatus = class(TDXComponent)
  private
    fFinanz  : TMonthStatus;
    Surface  : TDirectDrawSurface;
    Mem      : TDDSurfaceDesc;
    fRedraw  : TRect;
    procedure SetFinanz(const Value: TMonthStatus);
    procedure Absatz(var DrawTop1,DrawTop2: Integer);
    { Private-Deklarationen }
  protected
    procedure CalculateGesamt(var PBil,NBil,Bil: Integer);
    procedure DrawEntry(var DrawTop: Integer;Text1: String;Zahl: Integer;Spalte: Integer=1;Style: TFontStyles=[];Color: TColor=clNone);
    { Protected-Deklarationen }
  public
    procedure Draw(Surface: TDirectDrawSurface;var Mem: TDDSurfaceDesc);override;
    procedure ReDrawRect(DrawRect: TRect;Surface: TDirectDrawSurface;var Mem: TDDSurfaceDesc);override;
    property Finanz   : TMonthStatus read fFinanz write SetFinanz;
  end;

implementation

uses
  savegame_api;

{ TDXMonthStatus }

procedure TDXMonthStatus.Absatz(var DrawTop1, DrawTop2: Integer);
begin
  if DrawTop1>DrawTop2 then
  begin
    inc(DrawTop1,5);
    DrawTop2:=DrawTop1;
  end
  else
  begin
    inc(DrawTop2,5);
    DrawTop1:=DrawTop2;
  end;
end;

procedure TDXMonthStatus.CalculateGesamt(var PBil, NBil, Bil: Integer);
begin
  PBil:=fFinanz[kbSVK];
  inc(PBil,fFinanz[kbFVK]);
  inc(PBil,fFinanz[kbHVK]);
  inc(PBil,fFinanz[kbLVK]);
  inc(PBil,fFinanz[kbEG]);
  inc(PBil,fFinanz[kbFiU]);
  inc(PBil,fFinanz[kbSpK]);
  inc(PBil,fFinanz[kbSEn]);
  inc(PBil,fFinanz[kbFEn]);
  inc(PBil,fFinanz[kbHEn]);
  inc(PBil,fFinanz[kbRV]);
  inc(PBil,fFinanz[kbPG]);

  { Negativ Bilanz }
  NBil:=fFinanz[kbSK];
  inc(NBil,fFinanz[kbSG]);
  inc(NBil,fFinanz[kbFK]);
  inc(NBil,fFinanz[kbFG]);
  inc(NBil,fFinanz[kbHK]);
  inc(NBil,fFinanz[kbHG]);
  inc(NBil,fFinanz[kbLK]);
  inc(NBil,fFinanz[kbTrK]);
  inc(NBil,fFinanz[kbTK]);
  inc(NBil,fFinanz[kbSE]);
  inc(NBil,fFinanz[kbBB]);
  inc(NBil,fFinanz[kbRB]);
  Bil:=PBil+NBil;
end;

procedure TDXMonthStatus.Draw(Surface: TDirectDrawSurface;var Mem: TDDSurfaceDesc);
begin
  RedrawRect(ClientRect,Surface,Mem);
end;

procedure TDXMonthStatus.DrawEntry(var DrawTop: Integer; Text1: String;
  Zahl: Integer; Spalte: Integer; Style: TFontStyles;Color: TColor);
var
  Text2    : String;
  OldStyle : TFontStyles;
  Rechts   : Integer;
  Links    : Integer;
  Font     : TDirectFont;
begin
  if (DrawTop>fRedraw.Bottom+18) or ((DrawTop)<fRedraw.Top-18) then
  begin
    inc(DrawTop,18);
    exit;
  end;
  Font:=WhiteStdFont;
  if Color=clNone then
  begin
    if Zahl>0 then
      Font:=GreenStdFont
    else if Zahl<0 then
      Font:=MaroonStdFont
    else
      Font:=YellowStdFont;
  end;
  Text2:=Format(FFloat,[Zahl/1]);
  Rechts:=0;
  Links:=0;
  case Spalte of
    1:
    begin
      Links:=Left+8;
      Rechts:=Left+(Width shr 1)-8-Font.TextWidth(Text2);
    end;
    2:
    begin
      Links:=Left+(Width shr 1)+8;
      Rechts:=Right-8-Font.TextWidth(Text2);
    end;
  end;
  Font.Draw(Surface,Links,DrawTop,Text1);
  Font.Draw(Surface,Rechts,DrawTop,Text2);
  Inc(DrawTop,18);
end;

procedure TDXMonthStatus.ReDrawRect(DrawRect: TRect;Surface: TDirectDrawSurface;
    var Mem: TDDSurfaceDesc);
var
  PBil,NBil : Integer;
  Bil       : Integer;
  DrawTop1  : Integer;
  DrawTop2  : Integer;
  BilanzTop : Integer;
  Region    : HRGN;
  TempRect  : TRect;
  Color     : TColor;
  Blend     : TBlendColor;
begin
  Self.Surface:=Surface;
  Self.Mem:=Mem;
  IntersectRect(DrawRect,DrawRect,ClientRect);
  fRedraw:=DrawRect;
  CalculateGesamt(PBil,NBil,Bil);
  DrawTop1:=Top;
  BilanzTop:=DrawTop1;
  if AlphaElements then
  begin
    IntersectRect(TempRect,DrawRect,Rect(Left,DrawTop1,Left+(Width shr 1)-1,DrawTop1+20));
    BlendRoundRect(Rect(Left,DrawTop1,Right+1,Bottom),150,bcGreen,Surface,20,TempRect);
    IntersectRect(TempRect,DrawRect,Rect(Left+(Width shr 1)+1,DrawTop1,Right+1,DrawTop1+20));
    BlendRoundRect(Rect(Left,DrawTop1,Right+1,Bottom),150,bcMaroon,Surface,20,Rect(Left+(Width shr 1)+1,DrawTop1,Right+1,DrawTop1+20));
  end;
  WhiteBStdFont.Draw(Surface,Left+8,DrawTop1+3,LGewinne);
  WhiteBStdFont.Draw(Surface,Left+(Width shr 1)+8,DrawTop1+3,LVerluste);
  inc(DrawTop1,25);
  DrawTop2:=DrawTop1;

  { Soldaten }
  DrawEntry(DrawTop1,LSolVer,fFinanz[kbSVK]);
  DrawEntry(DrawTop1,LSolEnt,fFinanz[kbSEn]);
  DrawEntry(DrawTop2,LSolKauf,fFinanz[kbSK],2);
  DrawEntry(DrawTop2,LSolGehalt,fFinanz[kbSG],2);
  Absatz(DrawTop1,DrawTop2);

  { Wissenschaftler }
  DrawEntry(DrawTop1,LForVer,fFinanz[kbFVK]);
  DrawEntry(DrawTop1,LForEnt,fFinanz[kbFEn]);
  DrawEntry(DrawTop2,LForKauf,fFinanz[kbFK],2);
  DrawEntry(DrawTop2,LForGehalt,fFinanz[kbFG],2);
  Absatz(DrawTop1,DrawTop2);

  { Techniker }
  DrawEntry(DrawTop1,LHerVer,fFinanz[kbHVK]);
  DrawEntry(DrawTop1,LHerEnt,fFinanz[kbHEn]);
  DrawEntry(DrawTop2,LHerKauf,fFinanz[kbHK],2);
  DrawEntry(DrawTop2,LHerGehalt,fFinanz[kbHG],2);
  Absatz(DrawTop1,DrawTop2);

  { Ausrüstung }
  DrawEntry(DrawTop1,LAusVer,fFinanz[kbLVK]);
  DrawEntry(DrawTop1,ST0311280001,fFinanz[kbPG]);
  DrawEntry(DrawTop2,LAusKauf,fFinanz[kbLK],2);
  Absatz(DrawTop1,DrawTop2);

  { Raumschiff }
  DrawEntry(DrawTop1,LRauVerkauf,fFinanz[kbRV]);
  DrawEntry(DrawTop2,LRauBau,fFinanz[kbRB],2);
  DrawEntry(DrawTop2,LTreibKost,fFinanz[kbTrK],2);
  Absatz(DrawTop1,DrawTop2);

  { Sonstiges }
  DrawEntry(DrawTop1,LSpeGewin,fFinanz[kbSpK]);
  DrawEntry(DrawTop1,LForGewin,fFinanz[kbFiU]);
  DrawEntry(DrawTop1,LEinGewin,fFinanz[kbEG]);
  DrawEntry(DrawTop2,LBasBau,fFinanz[kbBB],2);
  DrawEntry(DrawTop2,LSchadKost,fFinanz[kbSE],2);
  DrawEntry(DrawTop2,LTrainKost,fFinanz[kbTK],2);
  inc(DrawTop1,5);
  { Zusammenfassung }
  if AlphaElements then
  begin
    IntersectRect(TempRect,DrawRect,Rect(Left,DrawTop1,Left+(Width shr 1)-1,DrawTop1+20));
    BlendRoundRect(Rect(Left,Top,Right+1,DrawTop1+20),150,bcGreen,Surface,20,TempRect);
    IntersectRect(TempRect,DrawRect,Rect(Left+(Width shr 1)+1,DrawTop1,Right+1,DrawTop1+20));
    BlendRoundRect(Rect(Left,Top,Right+1,DrawTop1+20),150,bcMaroon,Surface,20,TempRect);
  end;
  Region:=CreateRoundRectRgn(Left,BilanzTop,Right+1,DrawTop1+21,20,20);
  FrameRegion(Region,Rect(Left,BilanzTop,Left+(Width shr 1)-1,DrawTop1+21),clGreen,Surface);
  FrameRegion(Region,Rect(Left+(Width shr 1)+1,BilanzTop,Right+1,DrawTop1+21),clMaroon,Surface);
  DeleteObject(Region);

  HLine(Surface,Mem,Left,Left+(Width shr 1)-2,BilanzTop+20,bcGreen);
  HLine(Surface,Mem,Left,Left+(Width shr 1)-2,DrawTop1,bcGreen);

  HLine(Surface,Mem,Left+(Width shr 1)+1,Right-1,BilanzTop+20,bcMaroon);
  HLine(Surface,Mem,Left+(Width shr 1)+1,Right-1,DrawTop1,bcMaroon);

  dec(DrawTop1,2);
  Absatz(DrawTop1,DrawTop2);
  DrawEntry(DrawTop1,LGewinn,PBil,1,[fsBold],clWhite);
  DrawEntry(DrawTop2,LVerlust,NBil,2,[fsBold],clWhite);
  { Auswertung }
  inc(DrawTop1,10);
  Absatz(DrawTop1,DrawTop2);

  if savegame_api_GetCredits>0 then
  begin
    Blend:=bcGreen;
    Color:=clGreen;
  end
  else
  begin
    Blend:=bcMaroon;
    Color:=clMaroon;
  end;
  
  if AlphaElements then
  begin
    IntersectRect(TempRect,DrawRect,Rect(Left,DrawTop1,Left+(Width shr 1)-1,DrawTop1+20));
    BlendRoundRect(Rect(Left,DrawTop1,Left+(Width shr 1)-1,DrawTop1+40),150,Blend,Surface,20,TempRect);
  end;
  BilanzTop:=DrawTop1;
  inc(DrawTop1,4);
  DrawEntry(DrawTop1,LVorMonth,savegame_api_GetCredits-Bil,1,[fsBold],clWhite);
  inc(DrawTop1,4);
  DrawEntry(DrawTop1,LBilanz,Bil,1,[fsBold]);
  if AlphaElements then
  begin
    IntersectRect(TempRect,DrawRect,Rect(Left,DrawTop1,Left+(Width shr 1)-1,DrawTop1+20));
    BlendRoundRect(Rect(Left,DrawTop1-20,Left+(Width shr 1)-1,DrawTop1+20),150,Blend,Surface,20,TempRect);
  end;
  Region:=CreateRoundRectRgn(Left,BilanzTop,Left+(Width shr 1)-1,DrawTop1+20,20,20);
  FrameRegion(Region,Rect(Left,BilanzTop,Left+(Width shr 1)-1,DrawTop1+20),Color,Surface);
  DeleteObject(Region);
  inc(DrawTop1,4);
  DrawEntry(DrawTop1,LNachMonth,savegame_api_GetCredits,1,[fsBold],clWhite);
end;

procedure TDXMonthStatus.SetFinanz(const Value: TMonthStatus);
begin
  fFinanz := Value;
  Redraw;
end;


end.
