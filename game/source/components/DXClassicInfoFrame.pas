{********************************************************************************
*										*
* Copyright (C) 2004  X-Force Team - jim_raynor@web.de				*
*										*
* This file is part of X-Force: Fight For Destiny (X-Force).			*
*										*
* X-Force is free software; you can redistribute it and/or modify		*
* it under the terms of the GNU General Public License as published by		*
* the Free Software Foundation; either version 2 of the License, or		*
* (at your option) any later version.						*
*										*
* X-Force is distributed in the hope that it will be useful,			*
* but WITHOUT ANY WARRANTY; without even the implied warranty of		*
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the			*
* GNU General Public License for more details.					*
*										*
* You should have received a copy of the GNU General Public License		*
* along with X-Force; if not, write to the Free Software			*
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA	*
*										*
*********************************************************************************
*										*
* Anzeige der generellen Basisinformationen in der Ansicht Basisinformation	*
*										*
********************************************************************************}

{$I ../Settings.inc}

unit DXClassicInfoFrame;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  DXContainer, KD4SaveGame, DXDraws,StringConst, Blending, XForce_types,
  DirectDraw, DirectFont;

type

  TDXClassicInfoFrame = class(TDXComponent)
  private
    fBorderColor  : TColor;
    fFrameInfos   : TFrameInfos;
    fBlendColor   : TBlendColor;
    procedure SetBorderColor(const Value: TColor);
    procedure SetFrameInfo(const Value: TFrameInfos);
    procedure DrawEntry(var DrawTop: Integer;Text1: String;Text2: String;Surface: TDirectDrawSurface);
    procedure SetBlendColor(const Value: TBlendColor);
    { Private-Deklarationen }
  protected
    { Protected-Deklarationen }
  public
    constructor Create(Page: TDXPage);override;
    procedure Draw(Surface: TDirectDrawSurface;var Mem: TDDSurfaceDesc);override;
    property BorderColor : TColor read fBorderColor write SetBorderColor;
    property Infos       : TFrameInfos read fFrameInfos write SetFrameInfo;
    property BlendColor  : TBlendColor read fBlendColor write SetBlendColor;
  end;

implementation

uses
  basis_api;

{ TDXClassicInfoFrame }

constructor TDXClassicInfoFrame.Create(Page: TDXPage);
begin
  inherited;
  fBlendColor:=bcMaroon;
end;

procedure TDXClassicInfoFrame.Draw(Surface: TDirectDrawSurface;var Mem: TDDSurfaceDesc);
var
  DrawTop   : Integer;
  Region    : HRGN;
  TempRect  : TRect;
begin
  DrawTop:=Top+27;
  if AlphaElements then
    BlendRoundRect(ClientRect,150,fBlendColor,Surface,20,Rect(Left,Top,Right,Top+19));

  Rectangle(Surface,Mem,Left,Top+19,Right,Top+20,fBorderColor);
  WhiteBStdFont.Draw(Surface,Left+8,Top+4,basis_api_GetSelectedBasis.Name);

  if (ifWohn in Infos) then DrawEntry(DrawTop,AInfoFrame[ifWohn,false],Format(FRoom,[basis_api_BelegterWohnRaum/1,basis_api_WohnRaum/1]),Surface);
  if (ifLager in Infos) then DrawEntry(DrawTop,AInfoFrame[ifLager,false],Format(FLagerRoom,[basis_api_BelegterLagerRaum,basis_api_LagerRaum/1]),Surface);
  if (ifLabor in Infos) then DrawEntry(DrawTop,AInfoFrame[ifLabor,false],Format(FRoom,[basis_api_BelegterLaborRaum/1,basis_api_LaborRaum/1]),Surface);
  if (ifWerk in Infos) then DrawEntry(DrawTop,AInfoFrame[ifWerk,false],Format(FRoom,[basis_api_BelegterWerkRaum/1,basis_api_WerkRaum/1]),Surface);
  if (ifHangar in Infos) then DrawEntry(DrawTop,AInfoFrame[ifHangar,false],Format(FRoom,[basis_api_BelegterHangarRaum/1,basis_api_HangarRaum/1]),Surface);
  TempRect:=ClientRect;
  inc(TempRect.Bottom,20);
  Region:=CreateRoundRectRgn(TempRect.Left,TempRect.Top,TempRect.Right+1,TempRect.Bottom+1,20,20);
  FrameRegion(Region,Rect(Left,Top,Right,Bottom),fBorderColor,Surface);
  DeleteObject(Region);
end;

procedure TDXClassicInfoFrame.DrawEntry(var DrawTop: Integer; Text1,
  Text2: String; Surface: TDirectDrawSurface);
begin
  YellowStdFont.Draw(Surface,Left+8,DrawTop,Text1);
  WhiteStdFont.Draw(Surface,Right-8-WhiteStdFont.TextWidth(Text2),DrawTop,Text2);
  Inc(DrawTop,20);
end;

procedure TDXClassicInfoFrame.SetBlendColor(const Value: TBlendColor);
begin
  fBlendColor := Value;
  Redraw;
end;

procedure TDXClassicInfoFrame.SetBorderColor(const Value: TColor);
begin
  fBorderColor := Container.Surface.ColorMatch(Value);
  Redraw;
end;

procedure TDXClassicInfoFrame.SetFrameInfo(const Value: TFrameInfos);
begin
  fFrameInfos := Value;
  Redraw;
end;

end.
