{********************************************************************************
*										*
* Copyright (C) 2004  X-Force Team - jim_raynor@web.de				*
*										*
* This file is part of X-Force: Fight For Destiny (X-Force).			*
*										*
* X-Force is free software; you can redistribute it and/or modify		*
* it under the terms of the GNU General Public License as published by		*
* the Free Software Foundation; either version 2 of the License, or		*
* (at your option) any later version.						*
*										*
* X-Force is distributed in the hope that it will be useful,			*
* but WITHOUT ANY WARRANTY; without even the implied warranty of		*
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the			*
* GNU General Public License for more details.					*
*										*
* You should have received a copy of the GNU General Public License		*
* along with X-Force; if not, write to the Free Software			*
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA	*
*										*
*********************************************************************************
*										*
* basis_api bildet die Schnittstelle zu einer Basisliste, die bis zu 8 Basen	*
* verwaltet									*
*										*
* WICHTIG!! Bevor die Funktionen genutzt werden k�nnen muss �ber 		*
*           basis_api_init die Basisliste gesetzt werden. Sollte durch 		*
*           savegame_api_init geschehen sein.					*
*										*
********************************************************************************}

{$I ../Settings.inc}

unit EinsatzOptionsDialog;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ModalDialog, DXGroupCaption, DXBitmapButton, DXContainer, XForce_types, Blending,
  DXCheckList, DXCanvas, DXDraws, DirectDraw, KD4Utils, Defines,
  StringConst;

type
  TChoosedOption        = (coNone, coOK, coCancel, coCancelEinsatz, coKeyBoardConfig, coShowMission);

  TEinsatzOptionsDialog = class(TModalDialog)
  private
    GroupHeader       : TDXGroupCaption;
    MessageHeader     : TDXGroupCaption;
    ListBox           : TDXCheckList;
    Canvas            : TDXCanvas;
    OKButton          : TDXBitmapButton;
    CancelButton      : TDXBitmapButton;
    KeyBoardButton    : TDXBitmapButton;
    MissionButton     : TDXBitmapButton;
    EndeButton        : TDXBitmapButton;
    fMessages         : TEinsatzMessages;
    fChoosedOption    : TChoosedOption;

    procedure CanvasRedraw(Sender: TDXComponent; Surface: TDirectDrawSurface;Rect: TRect;var Mem: TDDSurfaceDesc);
    procedure ButtonClick(Sender: TObject);
  protected
    procedure BeforeShow;override;
    { Protected-Deklarationen }
  public
    constructor Create(Page: TDXPage);override;

    property Messages               : TEinsatzMessages read fMessages write fMessages;
    property ChooseOption           : TChoosedOption read fChoosedOption;
    { Public-Deklarationen }
  end;

implementation

{ TEinsatzOptionsDialog }

procedure TEinsatzOptionsDialog.BeforeShow;
var
  Dummy: TEinsatzMessage;
begin
  ListBox.Font:=Font;
  GroupHeader.Font.Style:=[fsBold];
  GroupHeader.Font.Size:=Font.Size+4;
  GroupHeader.FontColor:=clYellow;
  GroupHeader.Caption:=ST0309230015;
  MessageHeader.Font.Style:=[fsBold];
  MessageHeader.FontColor:=clWhite;
  MessageHeader.Caption:=ST0309230016;

  if AlphaElements then
  begin
    MissionButton.FirstColor:=$00800000;
    KeyBoardButton.FirstColor:=$00800000;
    OKButton.FirstColor:=$00400000;
    CancelButton.FirstColor:=$00400000;
  end
  else
  begin
    MissionButton.FirstColor:=$00FF0000;
    KeyBoardButton.FirstColor:=$00FF0000;
    OKButton.FirstColor:=$00800000;
    CancelButton.FirstColor:=$00800000;
  end;
  MissionButton.SecondColor:=clNavy;
  KeyBoardButton.SecondColor:=clNavy;
  OKButton.SecondColor:=clBlue;
  CancelButton.SecondColor:=clBlue;

  MissionButton.BlendColor:=bcBlue;
  KeyBoardButton.BlendColor:=bcBlue;
  OKButton.BlendColor:=bcDarkNavy;
  CancelButton.BlendColor:=bcDarkNavy;

  EndeButton.SecondColor:=clRed;

  for Dummy:=low(TEinsatzMessage) to high(TEinsatzMessage) do
  begin
    ListBox.Checked[Integer(Dummy)]:=Dummy in fMessages;
  end;

  fChoosedOption:=coNone;
end;

procedure TEinsatzOptionsDialog.ButtonClick(Sender: TObject);
var
  Dummy: Integer;
begin
  fChoosedOption:=TChoosedOption(TDXBitmapButton(Sender).Tag);

  if fChoosedOption<>coCancel then
  begin
    // Nachrichteneinstellungen �bernehmen
    fMessages:=[];
    for Dummy:=0 to ListBox.Items.Count-1 do
    begin
      if ListBox.Checked[Dummy] then
        include(fMessages,TEinsatzMessage(Dummy));
    end;
  end;

  ReadyShow;
end;

procedure TEinsatzOptionsDialog.CanvasRedraw(Sender: TDXComponent;
  Surface: TDirectDrawSurface; Rect: TRect; var Mem: TDDSurfaceDesc);
var
  BlendRect: TRect;
begin
  if not AlphaControls then
  begin
    IntersectRect(BlendRect,Rect,CorrectBottomOfRect(ListBox.ClientRect));
    BlendRectangle(BlendRect,200,bcDarkNavy,Surface,Mem);

    BlendRect:=Classes.Rect(Canvas.Left,ListBox.Bottom+1,Canvas.Right,Canvas.Bottom);
    IntersectRect(Rect,Rect,BlendRect);
  end;

  BlendRectangle(CorrectBottomOfRect(Rect),200,bcDarkNavy,Surface,Mem);
  Rectangle(Surface,Mem,BlendRect,bcDarkNavy);
end;

constructor TEinsatzOptionsDialog.Create(Page: TDXPage);
begin
  inherited;

  { �berschrift }
  GroupHeader:=TDXGroupCaption.Create(Page);
  with GroupHeader do
  begin
    Visible:=false;
    BorderColor:=$00400000;
    BlendColor:=bcDarkNavy;
    RoundCorners:=rcTop;
    AlphaValue:=200;
    Width:=323;
    Height:=25;
    Alignment:=taCenter;
  end;
  AddComponent(GroupHeader);

  MessageHeader:=TDXGroupCaption.Create(Page);
  with MessageHeader do
  begin
    Visible:=false;
    BorderColor:=$00400000;
    BlendColor:=bcDarkNavy;
    RoundCorners:=rcNone;
    AlphaValue:=200;
    Width:=GroupHeader.Width;
    Top:=GroupHeader.Bottom+1;
    Height:=22;
    Alignment:=taCenter;
  end;
  AddComponent(MessageHeader);

  Canvas:=TDXCanvas.Create(Page);
  with Canvas do
  begin
    OnPaintRect:=CanvasRedraw;
    Top:=MessageHeader.Bottom+1;
    Width:=MessageHeader.Width;
  end;
  AddComponent(Canvas);

  { Listbox zum Ausw�hlen }
  ListBox:=TDXCheckList.Create(Page);
  with ListBox do
  begin
    ItemHeight:=22;
    Visible:=false;
    RoundCorners:=rcNone;
//    OnDblClick:=ListDblClick;
//    OnKeyPress:=PressKey;
//    OwnerDraw:=true;
//    OnDrawItem:=DrawItem;
    FirstColor:=bcDarkNavy;
    SecondColor:=coScrollPen;
    AlphaValue:=200;
    SetRect(Canvas.Left,Canvas.Top,Canvas.Width,Canvas.Height);
  end;
  AddComponent(ListBox);

  ListBox.AddItem(ST0309230017,'');
  ListBox.AddItem(ST0309230018,'');
  ListBox.AddItem(ST0309230019,'');
  ListBox.AddItem(ST0309230020,'');
  ListBox.AddItem(ST0309230021,'');
  ListBox.AddItem(ST0309230022,'');
  ListBox.AddItem(ST0309230023,'');

  Canvas.Height:=(ListBox.Items.Count*ListBox.ItemHeight)+72;
  ListBox.Height:=Canvas.Height-70;

  { Button zum Abbruch des Einsatzes }
  EndeButton:=TDXBitmapButton.Create(Page);
  with EndeButton do
  begin
    Visible:=false;
    OnClick:=ButtonClick;
    Text:=ST0309230024;
    Font:=Font;
    Font.Color:=clYellow;
    RoundCorners:=rcNone;
    Left:=10;
    Width:=Canvas.Width-20;
    Top:=ListBox.Bottom+38;
    Height:=25;
    Tag:=Integer(coCancelEinsatz);
  end;
  AddComponent(EndeButton);

  { Button zum Aufruf der Missions-Beschreibung }
  MissionButton:=TDXBitmapButton.Create(Page);
  with MissionButton do
  begin
    Visible:=false;
    OnClick:=ButtonClick;
    Text:=ST0410100001;
    Font:=Font;
    Font.Color:=clYellow;
    RoundCorners:=rcNone;
    Left:=10;
    Width:=(EndeButton.Width div 2)-2;
    Top:=ListBox.Bottom+8;
    Height:=25;
    Tag:=Integer(coShowMission);
  end;
  AddComponent(MissionButton);

  { Button zum Aufruf der Tastatureinstellungen }
  KeyBoardButton:=TDXBitmapButton.Create(Page);
  with KeyBoardButton do
  begin
    Visible:=false;
    OnClick:=ButtonClick;
    Text:=ST0309230025;
    Font:=Font;
    Font.Color:=clYellow;
    RoundCorners:=rcNone;
    Left:=13+(EndeButton.Width div 2);
    Width:=(EndeButton.Width div 2)-2;
    Top:=ListBox.Bottom+8;
    Height:=25;
    Tag:=Integer(coKeyBoardConfig);
  end;
  AddComponent(KeyBoardButton);

  { Einstellungen f�r den OK Button }
  OKButton:=TDXBitmapButton.Create(Page);
  with OKButton do
  begin
    Visible:=false;
    OnClick:=ButtonClick;
    Text:=BOKWA;
    Width:=GroupHeader.Width div 2;
    Top:=Canvas.Bottom+1;
    Font:=Font;
    RoundCorners:=rcLeftBottom;
    Height:=28;
    BlendAlpha:=150;
    Tag:=Integer(coOK);
  end;
  AddComponent(OKButton);

  { Einstellungen f�r den Abbrechen Button }
  CancelButton:=TDXBitmapButton.Create(Page);
  with CancelButton do
  begin
    Visible:=false;
    Text:=BCancelWA;
    Width:=GroupHeader.Width div 2;
    Left:=(GroupHeader.Width div 2)+1;
    Top:=Canvas.Bottom+1;
    Font:=Font;
    Height:=28;
    RoundCorners:=rcRightBottom;
    OnClick:=ButtonClick;
    BlendAlpha:=150;
    Tag:=Integer(coCancel);
  end;
  AddComponent(CancelButton);

end;


end.
