{********************************************************************************
*										*
* Copyright (C) 2004  X-Force Team - jim_raynor@web.de				*
*										*
* This file is part of X-Force: Fight For Destiny (X-Force).			*
*										*
* X-Force is free software; you can redistribute it and/or modify		*
* it under the terms of the GNU General Public License as published by		*
* the Free Software Foundation; either version 2 of the License, or		*
* (at your option) any later version.						*
*										*
* X-Force is distributed in the hope that it will be useful,			*
* but WITHOUT ANY WARRANTY; without even the implied warranty of		*
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the			*
* GNU General Public License for more details.					*
*										*
* You should have received a copy of the GNU General Public License		*
* along with X-Force; if not, write to the Free Software			*
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA	*
*										*
*********************************************************************************
*										*
* RaumschiffList verwaltet alle Raumschiffe in einem Spiel.			*
*										*
********************************************************************************}

{$I ../Settings.inc}

unit RaumschiffList;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  XForce_types, DXDraws, DXClass, TraceFile, KD4Utils, LagerListe, Koding, TypInfo,
  UFOList, Defines, math, Loader, EinsatzListe, BasisListe,
  NGTypes, EarthCalcs, OrganisationList, NotifyList, ExtRecord, ProjektRecords,
  ConvertRecords, StringConst;

const
  BasisScanWeite      = 10000;
  SchiffScanWeite     = 5000;
  VerbrauchAnpassung  = 2.3;

  // Events f�r die Raumschiffliste
  EVENT_RAUMSCHIFFLISTADD       : TEventID = 0;
  EVENT_RAUMSCHIFFLISTDELETE    : TEventID = 1;

  // Events f�r Raumschiff
  EVENT_SCHIFFONDESTROY         : TEventID = 0;
  EVENT_SCHIFFONMOVE            : TEventID = 1;
  EVENT_SCHIFFREACHEDDEST       : TEventID = 2;
  EVENT_SCHIFFSHOOTDOWN         : TEventID = 3;
  EVENT_SCHIFFREACHEDHOMEBASE   : TEventID = 4;

type
  TSchiffItemsArray = Array of TItemsInSchiff;

  {$M+}
  TRaumschiff       = class;
  {$M-}
  TScanWorld        = class;

  TRaumschiffList = class(TObject)
  private
    fModels        : Array of TRaumschiffModel;
    fSchiffe       : TList;
    fScanArea      : TScanWorld;
    fClearList     : boolean;

    fShipIcons     : TPictureCollectionItem;
    fShipGraphics  : TPictureCollectionItem;

    fNotifyList    : TNotifyList;

    function GetModelCount: Integer;
    function GetRaumschiffModel(Index: Integer): TRaumschiffModel;
    function GetCount: Integer;
    function GetRaumschiff(Index: Integer): TRaumschiff;
    { Private-Deklarationen }
  protected
    procedure CreateSchiff(Index: Integer);
    procedure AddSchiff(Schiff: TRaumschiff);
    procedure DeleteSchiff(Schiff: TRaumschiff);
    procedure DrawGWZellen(Surface: TDirectDrawSurface;x,y: Integer;WZ: Integer; Rect: TRect);
    procedure Clear;

    procedure NewGameHandler(Sender: TObject);
    { Protected-Deklarationen }
  public
    constructor Create;
    destructor destroy;override;
    procedure SetBitmap(Bitmap: TBitmap; Small: boolean);
    procedure DrawIcon(Surface: TDirectDrawSurface;x,y: Integer;WZ: Integer);

    procedure NextRound(Minuten: Integer);
    procedure NextDay;
    procedure NextWeek;

    procedure Build(Model: Integer);
    procedure LoadFromStream(Stream: TStream);
    procedure SaveToStream(Stream: TStream);
    procedure ClearModels;
    procedure NachLaden;
    procedure UpgradeID(ID: TObject);
    procedure Abgleich;
    function DoTransport(FromPos,ToPos: TFloatPoint;const Item: TLagerItem;Preis: Integer;Kauf: boolean): TRaumschiff;
    function DoAlphatronTransport(FromPos,ToPos: TFloatPoint;Alphatron: Integer): TRaumschiff;
    function SchiffInListe(Schiff: TRaumschiff):boolean;

    function ModelOfID(ID: Cardinal): Integer;
    function SchiffOfID(ID: Integer): TRaumschiff;
    function IndexOfSchiff(Schiff: TRaumschiff): Integer;

    function GetWeeklyCost: Integer; overload;
    function GetWeeklyCost(BasisID: Cardinal): Integer; overload;

    procedure AddForschItem(Projekt: TObject);
    function InScanArea(Pos: TFloatPoint): boolean;
    property Models[Index: Integer]: TRaumschiffModel read GetRaumschiffModel;
    property Schiffe[Index:Integer]: TRaumschiff read GetRaumschiff;default;
    property Scanner       : TScanWorld read fScanArea;
    property ModelCount    : Integer read GetModelCount;
    property Count         : Integer read GetCount;

    property NotifyList    : TNotifyList read fNotifyList;
  end;

  TSchiffState = (ssReady,ssBuild,ssNoMotor,ssNoBenzin,ssHuntUFO,ssFlyToEinsatz,ssFlyToPos,ssFlyToTown,ssAir,ssBackToBasis,ssEskortSchiff);

  TScannerPoints = Array of record
    Position  : TFloatPoint;
    Range     : Integer;
    Color     : TColor;
  end;

  TScanWorld  = class(TObject)
  private
    fSchiffList    : TRaumschiffList;

    fChangeScan    : Boolean;

    fEvents        : Array of record
      Schiff       : TRaumschiff;
      Event        : TEventHandle;
    end;

    procedure SetSchiffList(const Value: TRaumschiffList);
    procedure AddSchiff(SenderSchiff: TObject);
    procedure DeleteSchiff(SenderSchiff: TObject);

    procedure CalcSensorMapFast(PointArray: TScannerPoints);
  public
    constructor Create;
    function AktuScanArea: Boolean;

    procedure ChangeScanArea(Sender: TObject);

    function CheckPos(Pos: TFloatPoint): Boolean;
    property SchiffList : TRaumschiffList read fSchiffList write SetSchiffList;
  end;

  TRaumschiff = class(TObject)
  private
    fListe         : TRaumschiffList;
    fZellen        : Integer;
    fHitPoints     : Integer;
    fAktHitPoi     : Integer;
    fSolSlots      : Integer;
    fAbschuesse    : Integer;
    fLagerPlatz    : Integer;
    fName          : String;
    fID            : Integer;

    // Variablen, die das Spiel des UFOs speichern
    fUFOID         : Cardinal;
    fEinsatzID     : Cardinal;
    fSchiffID      : Cardinal;
    fTownID        : Cardinal;
    fZiel          : TFloatPoint;

    fBasisID       : Cardinal;
    fModel         : Integer;
    fBauZeit       : Integer;
    fSendMessage   : boolean;
    fMotor         : TMotor;
    fWZellen       : Array[0..2] of TWaffenZelle;
    fEZellen       : Array[0..2] of TExtensionZelle;
    fShieldPoints  : Integer;
    fSchiffShield  : Integer;
    fPosition      : TFloatPoint;
    fLoadStat      : Integer;
    fSoldaten      : Integer;
    fItems         : TSchiffItemsArray;
    fXCOM          : boolean;

    fHasDestroyed  : Boolean;
    fHasMove       : boolean;
    fExtendsSlots  : Byte;
    fSensorWeite   : Word;

    // Transport von Ausr�stungen
    fTransportItem : TTransportItem;
    fFrom          : TFloatPoint;
    fMission       : TRaumschiffMission;
    fBuchType      : TKapital;
    fAlphatron     : Integer;

    fLastPosition  : TFloatPoint;
    fNotifyList    : TNotifyList;

    fBasisDestroyEvent : TEventHandle;
    procedure SetWaffenZelle(Index: Integer; const Value: TWaffenZelle);
    function GetModel: TRaumschiffModel;
    function GetStatus: String;
    function GetState: TSchiffState;
    function GetMonthKost: Integer;
    function GetTreibstoff: double;
    function GetWaffenZelle(Index: Integer): TWaffenZelle;
    function GetHasCommands: boolean;
    function GetAbilities: TAbilities;
    function GetZielPosition: TFloatPoint;
    function GetZielBasis: TBasis;
    function GetItemsInSchiff: Integer;
    function GetItems(Index: Integer): TItemsInSchiff;
    function GetLagerRoomBelegt: Integer;
    function GetTransportItem: TLagerItem;
    function GetHomePos: TFloatPoint;
    function GetSensorWeite: Word;
    function GetExtendZelle(Index: Integer): TExtensionZelle;
    function GetShieldInstalliert: boolean;
    function GetMaxShieldPoints: Integer;
    function GetShieldAbwehr: Integer;
    function GetShieldLaden: Integer;

    function IsOneAttributePresent(Attributes: TExtensionPropSet): boolean;
    function SumAttributeValues(Attribute: TExtensionProp): Integer;
    function GetSingleAttributeValue(Index: Integer; Attribute: TExtensionProp): Integer;
    function GetAttributeValue(Attribute: TExtensionProp): Integer;
    procedure GetMaximalAttributeValue(var Original: Integer; Attribute: TExtensionProp);
    procedure GetBoosterAttributeValue(var Original: Integer; Index: Integer; Attribute: TExtensionProp);
    function GetInvertedFractialSumOfAttributeValues(Base: Integer; Attribute: TExtensionProp): Extended;
    function GetExtensionIndexOfTheShield: Integer;

    // Events
    procedure HomeBaseDestroyed(Sender: TObject);

    procedure HomeBaseReached(Sender: TObject);

    function BenzinVerbrauchen(Liter: double): boolean;
    function GetCruisingRange: Integer;
  protected
    procedure UpgradeID(ID: Cardinal);
    procedure AbgleichWithModel(const Model: TRaumschiffModel);
    function CreateName: String;
    function CreateID: Integer;
    function IndexOfMunition(Zelle: Integer): Integer;

    procedure DefaultCreate(Liste: TRaumschiffList);
    procedure SetModel(Model: Integer);
    procedure SetTransporterSettings(Liste: TRaumschiffList);
  public
    constructor Create(Liste: TRaumschiffList);overload;
    constructor Create(Liste: TRaumschiffList;Model: Integer);overload;
    constructor CreateBuild(Liste: TRaumschiffList;Model: Integer);
    constructor CreateTransport(Liste: TRaumschiffList;Item: TLagerItem;Preis: Integer;FromPos,ToPos: TFloatPoint; Kauf: boolean);overload;
    constructor CreateTransport(Liste: TRaumschiffList;Alphatron: Integer;FromPos,ToPos: TFloatPoint);overload;
    destructor Destroy;override;

    procedure RechargeShield(Time: Integer; Full: boolean=false);
    function ZelleNachLaden(Zelle: Integer): TZelleNachladenError;
    function Tanken(Minuten: Integer = 0): boolean;
    procedure Repair(Minuten: Integer);

    procedure SetMunition(Zelle: Integer;Schuss: Integer);

    procedure SaveToStream(Stream: TStream);
    procedure LoadFromStream(Stream: TStream);

    procedure DrawView(Surface: TDirectDrawSurface;X,Y,Weite,Hoehe: Integer;Rect: TRect);

    procedure NextDay;

    function GenugTreibstoff(km: Integer): boolean;overload;
    function GenugTreibstoff(Destination: TFloatPoint): Boolean;overload;

    function BerechneVerbrauch(km: Integer): double;

    procedure CancelBuild;
    procedure SellSchiff;

    procedure AktuFreeSlots;

    // Zerst�rt das Raumschiff, ohne das der Hangarplatz freigegeben wurde
    procedure DestroyShip;

    // Commandos geben
    procedure BackToBase;

    procedure HuntingUFO(ID: Cardinal);
    procedure FlyToEinsatz(EinsatzID: Cardinal);
    procedure FlyToTown(TownID: Cardinal);
    procedure FlyToPos(Pos: TFloatPoint);
    procedure EskortSchiff(ID: Cardinal);

    // Basis Funktionen
    procedure SetHomeBase(Basis: TBasis);
    function GetHomeBasis: TBasis;
    function InSelectedBasis : boolean;
    function IsInHomeBase: Boolean;

    { Komponenten setzen }
    function SetzeMotor(Index: Integer):boolean;
    function SetzeWaffe(Index: Integer;Zelle: Integer):boolean;
    function SetzeExtension(Index: Integer;Zelle: Integer): boolean;

    procedure SetSoldaten(Index: Integer;Slot: Byte);
    function GetSoldat(Slot: Byte; out Soldat: PSoldatInfo): Integer;

    { Komponenten ausbauen }
    procedure MotorAusbauen;
    procedure WaffeAusbauen(Zelle: Integer);
    procedure ExtensionAusbauen(Zelle: Integer);

    function AttributeImproved(Attribute: TExtensionProp): boolean;

    function ChangeItem(ID: Cardinal; Count: Integer;VomLager: boolean = true): Integer;
    procedure DeleteItem(ID: Cardinal);
    function AddrOfSchiffItemIndex(Index: Integer): PItemsInSchiff;
    function AddrOfSchiffItemID(ID: Cardinal): PItemsInSchiff;

    procedure DoAbschuss;
    function DoTreffer(Strength: Integer;var Absorb: Integer; UFO: TUFO=nil): boolean;

    function GetMaxMunition(Zelle: Integer): Integer;

    function GetSchaden: Integer;
    function GetRepairTime: Integer;

    function BuildDays: Integer;

    // Zielangaben
    function Einsatz: TEinsatz;
    function HuntedUFO: TUFO;
    function EskortedSchiff: TRaumschiff;
    function Town: TTown;

    function DoMove(Minuten: Integer): boolean;
    function EntfernungToZiel: Integer;
    function GetZielName: String;
    function IsOverTown(Town: TTown): Boolean;

    { Index der Eingebauten Komponenten ermitteln }
    function GetMotorIndex: Integer;
    function GetWaffenIndex(Zelle: Integer): Integer;
    function GetExtendIndex(Zelle: Integer): Integer;

    procedure VernichteRaumschiff(FromUFO : TUFO = nil);
    procedure ReachedDestination;

    property Soldaten                      : Integer read fSoldaten;
    property ID                            : Integer read fID;
    property SoldatenSlots                 : Integer read fSolSlots;
    property BasisID                       : Cardinal read fBasisID;
    property Model                         : TRaumschiffModel read GetModel;
    property WaffenZellen                  : Integer read fZellen;
    property ExtendsSlots                  : Byte read fExtendsSlots;
    property HitPoints                     : Integer read fHitPoints;
    property AktHitPoints                  : Integer read fAktHitPoi;
    property Abilities                     : TAbilities read GetAbilities;
    property Abschuesse                    : Integer read fAbschuesse;
    property MaxShieldPoints               : Integer read GetMaxShieldPoints;
    property ShieldPoints                  : Integer read fShieldPoints;
    property ShieldAbwehr                  : Integer read GetShieldAbwehr;
    property ShieldLaden                   : Integer read GetShieldLaden;
    property StatusText                    : String  read GetStatus;
    property SensorWeite                   : Word read GetSensorWeite;
    property Status                        : TSchiffState read GetState;
    property MonthKost                     : Integer read GetMonthKost;

    property TreibStoff                    : double read GetTreibstoff;
    property CruisingRange                 : Integer read GetCruisingRange;
    
    property Motor                         : TMotor read fMotor write fMotor;
    property ShieldInstalliert             : boolean read GetShieldInstalliert;
    property Position                      : TFloatPoint read fPosition write fPosition;
    property ZielPosition                  : TFloatPoint read GetZielPosition;
    property WaffenZelle[Index: Integer]   : TWaffenZelle read GetWaffenZelle write SetWaffenZelle;
    property ExtendZelle[Index: Integer]   : TExtensionZelle read GetExtendZelle;
    property Liste                         : TRaumschiffList read fListe;

    property ItemCount                     : Integer read GetItemsInSchiff;
    property Items[Index: Integer]         : TItemsInSchiff read GetItems;

    property LagerRoomBelegt               : Integer read GetLagerRoomBelegt;

    // Eigenschaften Transporter
    property XCOMSchiff                    : boolean read fXCOM;
    property MissionType                   : TRaumschiffMission read fMission;
    property Item                          : TLagerItem read GetTransportItem;
    property Ziel                          : TFloatPoint read fZiel write FlyToPos;
    property Alphatron                     : Integer read fAlphatron;

    property PositionBeforeMove            : TFloatPoint read fLastPosition;

    property NotifyList                    : TNotifyList read fNotifyList;
  published
    function WriteToScriptString: String;
    class function ReadFromScriptString(ObjStr: String): TObject;

    property Name                          : String read fName write fName;
  end;

implementation

{ TRaumschiffList }

uses
  DXTakticScreen, country_api, lager_api, basis_api, array_utils, savegame_api,
  forsch_api, ufo_api, town_api, soldaten_api, einsatz_api, game_api,
  raumschiff_api, GlobeRenderer;

var
  TRaumschiffListRecord  : TExtRecordDefinition;
  TRaumschiffRecord      : TExtRecordDefinition;

procedure TRaumschiffList.Abgleich;
var
  Projects  : TRecordArray;
  Entry     : TForschProject;
  Dummy     : Integer;
  Dummy2    : Integer;
  Index     : Integer;
begin
  Projects:=savegame_api_GameSetGetProjects;
  for Dummy:=0 to high(Projects) do
  begin
    Entry:=RecordToProject(Projects[Dummy]);

    if Entry.TypeID<>ptRaumschiff then
      continue;

    Index:=ModelOfID(Entry.ID);
    if Index<>-1 then
    begin
      // Hier m�ssen in Zukunft neue Eigenschaften aus Entry nach fModels �bernommen werden

      for Dummy2:=0 to Count-1 do
        Schiffe[Dummy2].AbgleichWithModel(fModels[Index]);
    end;
  end;
end;

procedure TRaumschiffList.AddForschItem(Projekt: TObject);
var
  PProjekt: PForschProject;
begin
  Assert(Projekt<>nil);
  PProjekt:=PForschProject(Projekt);

  if PProjekt.TypeID=ptRaumschiff then
  begin
    SetLength(fModels,ModelCount+1);
    with fModels[ModelCount-1] do
    begin
      Name:=PProjekt.Name;
      ID:=PProjekt.ID;
      KaufPreis:=PProjekt.KaufPreis;
      VerKaufPreis:=PProjekt.VerKaufPreis;
      WaffenZellen:=PProjekt.WaffenZellen;
      HitPoints:=PProjekt.HitPoints;
      LagerPlatz:=PProjekt.LagerPlatz;
      Soldaten:=PProjekt.Soldaten;
      BauZeit:=PProjekt.BauZeit;
      ExtendsSlots:=PProjekt.ExtendSlots;
      SensorWeite:=PProjekt.SensorWeite;
      Info:=PProjekt.Info;
    end;
  end;
end;

procedure TRaumschiffList.AddSchiff(Schiff: TRaumschiff);
begin
  fSchiffe.Add(Schiff);

  fNotifyList.CallEvents(EVENT_RAUMSCHIFFLISTADD,Schiff);
end;

procedure TRaumschiffList.Build(Model: Integer);
begin
  TRaumschiff.CreateBuild(Self,Model);
end;

procedure TRaumschiffList.Clear;
var
  Schiff: TRaumschiff;
  Dummy : Integer;
begin
  // Da der Hangar freigegeben wird, wenn ein Raumschiff zerst�rt wird,
  // und dieses noch in der Schiffsliste ist muss es vor dem Free
  // aus der Liste genommen werden
  for Dummy:=fSchiffe.Count-1 downto 0 do
  begin
    Schiff:=TRaumschiff(fSchiffe[Dummy]);
    fSchiffe.Delete(Dummy);
    Schiff.Free;
  end;

  fSchiffe.Clear;
end;

procedure TRaumschiffList.ClearModels;
begin
  SetLength(fModels,0);
end;

constructor TRaumschiffList.Create;
begin
  fClearList:=false;
  fShipIcons := game_api_CreateImageListItem;
  fShipIcons.Transparent:=true;
  fShipIcons.TransparentColor:=clGreen;
  fShipIcons.SystemMemory:=true;

  fShipGraphics := game_api_CreateImageListItem;
  fShipGraphics.Transparent:=false;
  fShipGraphics.TransparentColor:=clFuchsia;
  fShipGraphics.SystemMemory:=true;

  fSchiffe:=TList.Create;

  fNotifyList:=TNotifyList.Create;

  forsch_api_RegisterProjektEndHandler(AddForschItem);
  forsch_api_RegisterUpgradeEndHandler(UpgradeID);


  savegame_api_RegisterCustomSaveHandler(SaveToStream,LoadFromStream,nil,$49F689E0);
  savegame_api_RegisterNewGameHandler(NewGameHandler);

  fScanArea:=TScanWorld.Create;
  fScanArea.SchiffList:=Self;

  savegame_api_RegisterStartGameHandler(fScanArea.ChangeScanArea);

  raumschiff_api_init(Self);
end;

procedure TRaumschiffList.CreateSchiff(Index: Integer);
begin
//  basis_api_NeedHangarRaum(basis_api_GetMainBasis.ID);

  with TRaumschiff.Create(Self,Index) do
  begin
    SetHomeBase(basis_api_GetSelectedBasis);
    Position:=basis_api_GetSelectedBasis.BasisPos;
  end;
end;

procedure TRaumschiffList.DeleteSchiff(Schiff: TRaumschiff);
var
  Ind: Integer;
begin
  // Hangarraum darf nur freigegeben werden, wenn das Raumschiff noch in der
  // Liste steht. Wenn nicht l�uft die Prozedure TRaumschiffList.Clear ab
  Ind:=fSchiffe.IndexOf(Schiff);
  if Ind<>-1 then
  begin
    if (Schiff.XCOMSchiff) and (Schiff.GetHomeBasis<>nil) then
      basis_api_FreeHangarRaum(Schiff.fBasisID);

    fSchiffe.Delete(Ind);
    
    fNotifyList.CallEvents(EVENT_RAUMSCHIFFLISTDELETE,Schiff);
  end;

end;

destructor TRaumschiffList.destroy;
begin
  Clear;
  fSchiffe.Free;
  fScanArea.Free;
  fNotifyList.Free;
  inherited;
end;

procedure TRaumschiffList.NextRound(Minuten: Integer);
var
  Dummy : Integer;
  Anzahl: Integer;
begin
  for Dummy:=0 to Count-1 do
    Schiffe[Dummy].fHasMove:=false;

  Anzahl:=Count;
  Dummy:=0;
  while (Dummy<Anzahl) do
  begin
    Schiffe[Dummy].DoMove(Minuten);
    if Anzahl<>Count then
    begin
      Anzahl:=Count;
      Dummy:=0;
    end
    else
      inc(Dummy)
  end;
end;

function TRaumschiffList.DoTransport(FromPos, ToPos: TFloatPoint;
  const Item: TLagerItem;Preis: Integer;Kauf: boolean): TRaumschiff;
begin
  result:=TRaumschiff.CreateTransport(Self,Item,Preis,FromPos,ToPos,Kauf);
end;

procedure TRaumschiffList.DrawGWZellen(Surface: TDirectDrawSurface; x, y,
  WZ: Integer;Rect: TRect);
begin
  Surface.Canvas.Release;
  OffSetRect(Rect,fShipGraphics.PatternRects[WZ].Left,fShipGraphics.PatternRects[WZ].Top);
  Surface.Draw(x,y,Rect,fShipGraphics.PatternSurfaces[WZ],true);
end;

procedure TRaumschiffList.DrawIcon(Surface: TDirectDrawSurface; x,
  y: Integer; WZ: Integer);
begin
  Surface.Canvas.Release;
  Surface.Draw(x,y,fShipIcons.PatternRects[WZ],fShipIcons.PatternSurfaces[WZ],true);
end;

function TRaumschiffList.GetCount: Integer;
begin
  result:=fSchiffe.Count;
end;

function TRaumschiffList.GetModelCount: Integer;
begin
  result:=length(fModels);
end;

function TRaumschiffList.GetRaumschiff(Index: Integer): TRaumschiff;
begin
  result:=nil;
  if (Index<0) or (Index>=Count) then
    exit;
  result:=TRaumschiff(fSchiffe[Index]);
end;

function TRaumschiffList.GetRaumschiffModel(Index: Integer): TRaumschiffModel;
begin
  result:=fModels[Index];
end;

function TRaumschiffList.ModelOfID(ID: Cardinal): Integer;
var
  Dummy: Integer;
begin
  result:=-1;
  for Dummy:=0 to ModelCount-1 do
  begin
    if fModels[Dummy].ID=ID then
    begin
      result:=Dummy;
      exit;
    end;
  end;
end;

function TRaumschiffList.InScanArea(Pos: TFloatPoint): boolean;
begin
  result:=fScanArea.CheckPos(Pos);
end;

procedure TRaumschiffList.LoadFromStream(Stream: TStream);
Var
  Count  : Integer;
  Dummy  : Integer;
  Rec    : TExtRecord;
begin
  Clear;

  Rec:=TExtRecord.Create(TRaumschiffListRecord);
  Rec.LoadFromStream(Stream);

  with Rec.GetRecordList('Models') do
  begin
    SetLength(fModels,Count);
    for Dummy:=0 to high(fModels) do
      fModels[Dummy]:=RecordToRaumschiffModel(Item[Dummy]);
  end;
  Count:=Rec.GetInteger('Count');
  Rec.Free;

  for Dummy:=0 to Count-1 do
  begin
    with TRaumschiff.Create(Self) do
      LoadFromStream(Stream);
  end;

  Abgleich;
end;

procedure TRaumschiffList.NachLaden;
var
  Dummy   : Integer;
  Dummy2  : Integer;
  Error   : TZelleNachladenError;
begin
  for Dummy:=0 to Count-1 do
  begin
    with Schiffe[Dummy] do
    begin
      if sabCanTanken in Abilities then
      begin
        for Dummy2:=0 to fZellen-1 do
        begin
          if (fWZellen[Dummy2].Installiert) then
          begin
            Error:=ZelleNachLaden(Dummy2);
            if Error=zneOK then
              fWZellen[Dummy2].SendMessage:=false
            else
            begin
              if not fWZellen[Dummy2].SendMessage then
              begin
                savegame_api_Message(ZelleNachladenErrorText[Error],lmNachLaden);
                fWZellen[Dummy2].SendMessage:=true;
              end;
            end;
          end;
        end;
      end;
    end;
  end;
end;

procedure TRaumschiffList.NextDay;
var
  Dummy: Integer;
begin
  for Dummy:=0 to Count-1 do
    TRaumschiff(fSchiffe[Dummy]).NextDay;
end;

procedure TRaumschiffList.NextWeek;
var
  Dummy: Integer;
begin
  for Dummy:=0 to Count-1 do
  begin
    if not savegame_api_NeedMoney(TRaumschiff(fSchiffe[Dummy]).MonthKost,kbRB) then
     // SCHIFFSKOSTEN
      ;
  end;
end;

procedure TRaumschiffList.SaveToStream(Stream: TStream);
var
  Dummy  : Integer;
  Rec    : TExtRecord;
begin
  Rec:=TExtRecord.Create(TRaumschiffListRecord);

  with Rec.GetRecordList('Models') do
  begin
    for Dummy:=0 to ModelCount-1 do
      Add(RaumschiffModelToRecord(fModels[Dummy]));
  end;
  Rec.SetInteger('Count',fSchiffe.Count);
  Rec.SaveToStream(Stream);
  Rec.Free;

  // Raumschiffe speichern
  for Dummy:=0 to Count-1 do
    TRaumschiff(fSchiffe[Dummy]).SaveToStream(Stream);
end;

function TRaumschiffList.SchiffInListe(Schiff: TRaumschiff): boolean;
begin
  result:=(fSchiffe.IndexOf(Schiff)<>-1);
end;

procedure TRaumschiffList.SetBitmap(Bitmap: TBitmap; Small: boolean);
begin
  if Small then
  begin
    fShipIcons.Picture.Bitmap:=Bitmap;
    fShipIcons.TransparentColor:=Bitmap.Canvas.Pixels[0,0];
    fShipIcons.PatternHeight:=32;
    fShipIcons.PatternWidth:=32;
    fShipIcons.Restore;
  end
  else
  begin
    fShipGraphics.Picture.Bitmap:=Bitmap;
    fShipGraphics.TransparentColor:=Bitmap.Canvas.Pixels[0,0];
    fShipGraphics.PatternHeight:=250;
    fShipGraphics.PatternWidth:=250;
    fShipGraphics.Restore;
  end;
end;

procedure TRaumschiffList.UpgradeID(ID: TObject);
var
  Dummy: Integer;
begin
  // Alle Eingebauten Ausr�stungen der Raumschiffe verbessern
  for Dummy:=0 to Count-1 do
  begin
    Schiffe[Dummy].UpgradeID(Cardinal(ID));
  end;
end;

function TRaumschiffList.SchiffOfID(ID: Integer): TRaumschiff;
var
  Dummy: Integer;
begin
  result:=nil;
  for Dummy:=0 to Count-1 do
  begin
    if Schiffe[Dummy].ID=ID then
    begin
      result:=Schiffe[Dummy];
      exit;
    end;
  end;
end;

function TRaumschiffList.IndexOfSchiff(Schiff: TRaumschiff): Integer;
begin
  result:=fSchiffe.IndexOf(Schiff);
end;

function TRaumschiffList.GetWeeklyCost: Integer;
var
  Dummy: Integer;
begin
  result:=0;
  for Dummy:=0 to Count-1 do
    inc(result,Schiffe[Dummy].MonthKost);
end;

function TRaumschiffList.GetWeeklyCost(BasisID: Cardinal): Integer;
var
  Dummy: Integer;
begin
  result:=0;
  for Dummy:=0 to Count-1 do
    if Schiffe[Dummy].BasisID=BasisID then
      inc(result,Schiffe[Dummy].MonthKost);
end;

procedure TRaumschiffList.NewGameHandler(Sender: TObject);
var
  Dummy    : Integer;
  Temp     : TForschProject;
  Projects : TRecordArray;
begin
  Projects:=savegame_api_GameSetGetProjects;

  Clear;
  SetLength(fModels,0);

  for Dummy:=0 to high(Projects) do
  begin
    Temp:=RecordToProject(Projects[Dummy]);
    if (Temp.TypeID=ptRaumschiff) and forsch_api_isStartProject(Temp) then
    begin
      AddForschItem(TObject(Addr(Temp)));
      while Temp.Anzahl>0 do
      begin
        CreateSchiff(high(fModels));
        dec(Temp.Anzahl);
      end;
    end;
  end;
end;

function TRaumschiffList.DoAlphatronTransport(FromPos, ToPos: TFloatPoint;
  Alphatron: Integer): TRaumschiff;
begin
  result:=TRaumschiff.CreateTransport(Self,Alphatron,FromPos,ToPos);
end;

{ TRaumschiff }

function TRaumschiff.ChangeItem(ID: Cardinal; Count: Integer; VomLager: boolean): Integer;
var
  SchiffItem  : PItemsInSchiff;
  Temp        : TItemsInSchiff;
  Room        : Integer;
  Anzahl      : Integer;
  Item        : PLagerItem;
  LagerV      : Integer;
begin
  result:=0;
  if Model.LagerPlatz=0 then
    exit;

  if Count=0 then
    exit;

  SchiffItem:=AddrOfSchiffItemID(ID);
  if SchiffItem=nil then
  begin
    Temp.ID:=ID;
    SetLength(fItems,length(fItems)+1);
    fItems[high(fItems)]:=Temp;
    //AddArray(Addr(fItems),TypeInfo(TSchiffItemsArray),Addr(Temp));
    SchiffItem:=AddrOfSchiffItemIndex(high(fItems));
  end;

  Item:=lager_api_GetItem(ID);

  // Pr�fen, ob genug Ausr�stung im Lager vorhanden ist
  // nat�rlich nur, wenn Ausr�stung aus dem Lager geholt wird
  if VomLager and (Count>0) then
  begin
    Anzahl:=lager_api_GetItemCountInBase(fBasisID,ID);

    if (Anzahl<Count) then
      Count:=Anzahl;
  end;

  LagerV:=round(Item.LagerV*10);
  if (Count>0) then
  begin
    // freien Lagerraum ermitteln (Berechnung des Lagerraumes immer mit Integers
    // und dem Faktor 10
    if LagerV>0 then
    begin
      Room:=(Model.LagerPlatz*10)-GetLagerRoomBelegt;
      Count:=min(Count,Room div LagerV);
    end;

    if VomLager then
      Assert(lager_api_DeleteItem(fBasisID,ID,Count));

    inc(SchiffItem.Anzahl,Count);
    inc(SchiffItem.Verbrauch,Count*LagerV);

    result:=Count;
  end
  else if (Count<0) then
  begin
    Count:=-Count;
    Count:=min(Count,SchiffItem.Anzahl);
    if VomLager then
      Count:=lager_api_PutItems(fBasisID,ID,Count);

    dec(SchiffItem.Anzahl,Count);
    dec(SchiffItem.Verbrauch,LagerV*Count);

    result:=-Count;
  end;
end;

procedure TRaumschiff.AktuFreeSlots;
var
  Dummy  : Integer;
  Soldat : PSoldatInfo;
begin
  fSoldaten:=0;
  for Dummy:=0 to SoldatenSlots-1 do
  begin
    if GetSoldat(Dummy,Soldat)>-1 then
      inc(fSoldaten);
  end;
end;

function TRaumschiff.BenzinVerbrauchen(Liter: double): boolean;
begin
  result:=true;
  if not fXCOM then
    exit;

  if fMotor.Liter<Liter then
  begin
    result:=false;
    exit;
  end;
  fMotor.Liter:=fMotor.Liter-Liter;
  result:=true;
end;

function TRaumschiff.BuildDays: Integer;
begin
  if Status=ssBuild then
    result:=fBauZeit
  else
    result:=-1;
end;

procedure TRaumschiff.CancelBuild;
begin
  if not (Status=ssBuild) then
    exit;
  Self.Free;
end;

constructor TRaumschiff.Create(Liste: TRaumschiffList; Model: Integer);
begin
  DefaultCreate(Liste);

  SetModel(Model);
end;

constructor TRaumschiff.CreateBuild(Liste: TRaumschiffList; Model: Integer);
begin
  DefaultCreate(Liste);

  SetHomeBase(basis_api_GetSelectedBasis);

  fPosition:=basis_api_GetSelectedBasis.BasisPos;

  SetModel(Model);

  {$IFDEF INSTANTBUILD}
  fBauZeit:=0;
  {$ELSE}
  fBauZeit:=Liste.Models[Model].BauZeit;
  {$ENDIF}
end;

function TRaumschiff.CreateID: Integer;
var
  IDOK  : boolean;
  Dummy : Integer;
begin
  result:=0;
  IDOK:=false;
  while (not IDOK) do
  begin
    IDOK:=true;
    result:=random(high(Integer));
    if result=0 then
    begin
      IDOK:=false;
    end
    else
    begin
      for Dummy:=0 to fListe.Count-1 do
      begin
        if fListe[Dummy].ID=result then
        begin
          IDOK:=false;
          break;
        end;
      end;
    end;
  end;
end;

function TRaumschiff.CreateName: String;
var
  Zahl: Integer;
begin
  Zahl:=random(89999)+10000;
  result:='NCC - '+IntToStr(Zahl);
end;

constructor TRaumschiff.CreateTransport(Liste: TRaumschiffList;
  Item: TLagerItem;Preis: Integer;FromPos,ToPos: TFloatPoint; Kauf: boolean);
begin
  SetTransporterSettings(Liste);

  fTransportItem.ID:=Item.ID;
  fTransportItem.Anzahl:=Item.Anzahl;
  fTransportItem.Preis:=Preis;

  fPosition:=FromPos;
  fZiel:=ToPos;
  fFrom:=FromPos;

  if Kauf then
  begin
    fMission:=rmKauf;
    fBuchType:=kbLK;
  end
  else
  begin
    fMission:=rmVerkauf;
    fBuchType:=kbLVK;
  end;
end;

procedure TRaumschiff.DeleteItem(ID: Cardinal);
var
  Dummy: Integer;
  Index: Integer;
  Count: Integer;
begin
  Index:=-1;
  for Dummy:=0 to length(fItems)-1 do
  begin
    if fItems[Dummy].ID=ID then
    begin
      Index:=Dummy;
      break;
    end;
  end;
  if Index<>-1 then
  begin
    Count:=lager_api_PutItems(fBasisID,ID,fItems[Index].Anzahl);
    dec(fItems[Index].Anzahl,Count);

    if fItems[Index].Anzahl=0 then
      DeleteArray(Addr(fItems),TypeInfo(TSchiffItemsArray),Index);

  end;
end;

destructor TRaumschiff.Destroy;
begin
  fListe.DeleteSchiff(Self);

  fNotifyList.CallEvents(EVENT_SCHIFFONDESTROY,Self);
  fNotifyList.Free;
  inherited;
end;

procedure TRaumschiff.DoAbschuss;
begin
  inc(fAbschuesse);
end;

function TRaumschiff.DoMove(Minuten: Integer): boolean;
var
  Pt        : TFloatPoint;
  Einhe     : double;
  Kilom     : Integer;
  IsRange   : Integer;
  Verbrauch : double;

  procedure SaveBeladung;
  var
    Dummy: Integer;
  begin
    if not fXCOM then
      exit;

    for Dummy:=0 to ItemCount-1 do
      fItems[Dummy].Wanted:=fItems[Dummy].Anzahl;
  end;

begin
  result:=true;
  if fHasMove then
    exit;

  fLastPosition:=fPosition;
  fHasMove:=true;

  // Raumschiff nachtanken
  if sabCanTanken in Abilities then
    Tanken(Minuten);

  // Raumschiff reparieren
  if sabRepair in Abilities then
    Repair(Minuten);

  if EskortedSchiff<>nil then
  begin
    if not EskortedSchiff.DoMove(Minuten) then
      result:=false;
  end;

  // Zielposition ermitteln
  Pt:=GetZielPosition;
  if Pt.X=-1 then
    // Raumschiff hat kein Flugziel
    exit;

  // Beim Abflug von der Basis, wird gespeichert wieviele Items im Raumschiff
  // beim Abflug sind. Nach R�ckkehr in der Basis wird, das "Lager" des Raumschiffes
  // soweit aufger�umt, dass am Ende die gleiche Ausr�stung, wie bei Flugbeginn
  // im Raumschiff ist
  if IsInHomeBase then
    SaveBeladung;

  // Berechnung der m�glichen Kilometer, die das Raumschiff zur�cklegen kann
  Kilom:=EarthEntfernung(fPosition,Pt);
  Einhe:=(fMotor.PpS*Minuten*15);
  Kilom:=min(round(Einhe),Kilom);

  // Pr�fen, ob genug Treibstoff f�r die maximale Distanz zur Verf�gung steht
  if (not GenugTreibstoff(Kilom)) and (status=ssAir) then
  begin
    // Befehl zur�cknehmen und Raumschiff zur Basis zur�ckschicken
    HuntingUFO(0);
    savegame_api_Message(Format(MRueckkehr,[Name]),lmTreibstoffMangel);

    // Das sollte hier keine Probleme machen
    exit;
  end;

  if EarthCalcFlight(fPosition,Pt,Kilom,IsRange) then
  begin
    // Raumschiff hat sich nicht bewegt -> Trotzdem ein wenig Sprit verbrauchen
    if IsRange=0 then
      IsRange:=Kilom div 20;

    if HuntedUFO<>nil then
      HuntedUFO.Stop;

    if Town<>nil then
      Town.Protection(Self,Minuten);

    // Raumschiff ist zur Basis zur�ckgekehrt
    if IsInHomeBase and (Status=ssReady) then
      fNotifyList.CallEvents(EVENT_SCHIFFREACHEDHOMEBASE,Self);
  end;
  fNotifyList.CallEvents(EVENT_SCHIFFONMOVE,Self);

  Verbrauch:=BerechneVerbrauch(IsRange);
  if not BenzinVerbrauchen(Verbrauch) then
  begin
    // Raumschiff ist aufgrund von Treibstoffmangel abgest�rzt
    savegame_api_Message(Format(MAbsturz,[Name]),lmTreibstoffMangel);
    savegame_api_BuchPunkte(-250,pbRaumschiffZerstoert);
    result:=false;
    Free;
    exit;
  end;

  // Hier folgen nur noch Sachen f�r einen Transporter
  if XCOMSchiff then exit;

  if SamePoint(fPosition,fZiel) then
  begin
    ReachedDestination;
    result:=false;
  end;
end;

function TRaumschiff.GetShieldInstalliert: boolean;
begin
  Result := GetExtensionIndexOfTheShield>=0;
end;

function TRaumschiff.GetMaxShieldPoints: Integer;
begin
  Result := GetAttributeValue(epShieldHitpoints);
end;

function TRaumschiff.GetShieldAbwehr: Integer;
begin
  Result := GetAttributeValue(epShieldDefence);
end;

function TRaumschiff.GetShieldLaden: Integer;
begin
  Result := GetAttributeValue(epShieldReloadTime);
end;

function TRaumschiff.DoTreffer(Strength: Integer;var Absorb: Integer; UFO: TUFO): boolean;
var
  ShieldSchutz: Integer;
begin
  result:=false;
  ShieldSchutz:=round(Strength/100*ShieldAbwehr);
  ShieldSchutz:=min(ShieldSchutz,fShieldPoints);
  dec(fShieldPoints,ShieldSchutz);
  Absorb:=round(ShieldSchutz/Strength*100);
  dec(fAktHitPoi,(Strength-ShieldSchutz));
  if fAktHitPoi<=0 then
  begin
    VernichteRaumschiff(UFO);
    result:=true;
  end;
end;

procedure TRaumschiff.DrawView(Surface: TDirectDrawSurface; X, Y,Weite,Hoehe: Integer;Rect: TRect);
var
  SX,SY       : Integer;
begin
  SX:=X+(Weite shr 1)-125+Rect.Left;
  SY:=Y+Rect.Top;
  fListe.DrawGWZellen(Surface,SX,SY,fZellen,Rect);
end;

function TRaumschiff.Einsatz: TEinsatz;
begin
  result:=einsatz_api_GetEinsatz(fEinsatzID);
end;

function TRaumschiff.EntfernungToZiel: Integer;
begin
//  result:=CalculateEntfern(fPosition,GetZielPosition);
  result:=round(EarthEntfernung(fPosition,GetZielPosition));
end;

procedure TRaumschiff.EskortSchiff(ID: Cardinal);
begin
  if ID=0 then
  begin
    BackToBase;
    exit;
  end;
  
  if fListe.SchiffOfID(ID)=self then
    exit;

  fZiel:=FloatPoint(-1,-1);
  fEinsatzID:=0;
  fTownID:=0;
  fSchiffID:=ID;
  fUFOID:=0;
end;

function TRaumschiff.EskortedSchiff: TRaumschiff;
begin
  result:=raumschiff_api_GetRaumschiff(fSchiffID);
end;

procedure TRaumschiff.FlyToEinsatz(EinsatzID: Cardinal);
begin
  if EinsatzID=0 then
  begin
    BackToBase;
    exit;
  end;

  if not GenugTreibstoff(einsatz_api_GetEinsatz(EinsatzID,true).Position) then
  begin
    savegame_api_Message(ST0410110001,lmTreibstoffMangel);
    exit;
  end;
  
  fZiel:=FloatPoint(-1,-1);
  fUFOID:=0;
  fSchiffID:=0;
  fTownID:=0;
  fEinsatzID:=EinsatzID;
end;

procedure TRaumschiff.FlyToPos(Pos: TFloatPoint);
begin
  if not GenugTreibstoff(Pos) then
  begin
    savegame_api_Message(ST0410110001,lmTreibstoffMangel);
    exit;
  end;
  
  fZiel:=Pos;
  fEinsatzID:=0;
  fSchiffID:=0;
  fTownID:=0;
  fUFOID:=0;
end;

function TRaumschiff.GenugTreibstoff(km: Integer): boolean;
var
  Entf    : Integer;
  Benzinv : double;
begin
  // Bei Transportern oder Raumschiffen ohne Homebase kann keine Pr�fung erfolgen
  if (not fXCom) or (fBasisID=0) then
  begin
    result:=true;
    exit;
  end;

  result:=false;
  if fMotor.Installiert then
  begin
    result:=true;
    Entf:=round(EarthEntfernung(fPosition,GetHomeBasis.BasisPos));
    Benzinv:=BerechneVerbrauch(Entf);

    // 500 km Reserver einbauen
    if Benzinv+BerechneVerbrauch(km*2)>fMotor.Liter then
      result:=false;
  end;
end;

function TRaumschiff.GetAbilities: TAbilities;
var
  State : TSchiffState;
  Dummy : Integer;
  Weap  : boolean;
begin
  AktuFreeSlots;
  result:=[];
  if fXCOM then
  begin
    State:=Status;
    if State=ssBuild then
    begin
      include(result,sabCancelBuild);
      if fBauZeit=Model.BauZeit then
        include(result,sabCancelWithOutRequest);
    end;
    if State in [ssReady,ssNoMotor,ssNoBenzin,ssHuntUFO,ssFlyToEinsatz,ssFlyToPos,ssEskortSchiff,ssFlyToTown] then
    begin
      if Treibstoff<100 then
        include(result,sabCanTanken);
      include(result,sabSelling);
      include(result,sabEquipt);
    end;
    if fMotor.Installiert and (Treibstoff>0) then
    begin
      include(result,sabFlight);
      include(result,sabChooseTarget);
      Weap:=false;
      for Dummy:=0 to fZellen-1 do
      begin
        if fWZellen[Dummy].Installiert then Weap:=true;
      end;
      if Weap then include(result,sabHuntUFO);
      if (fSoldaten<>0) then include(result,sabBodenKampf);
    end;
    if GetHasCommands then include(result,sabClearCommand);
    if (fAktHitPoi<fHitPoints) and (sabEquipt in result) then
      include(result,sabRepair);
  end
  else
  begin
    include(result,sabFlight);
  end;
end;

function TRaumschiff.GetHasCommands: boolean;
begin
  result:=(Status in [ssFlyToEinsatz,ssHuntUFO,ssAir,ssFlyToPos,ssEskortSchiff]);
end;

function TRaumschiff.GetItems(Index: Integer): TItemsInSchiff;
begin
  result:=fItems[Index];
end;

function TRaumschiff.GetItemsInSchiff: Integer;
begin
  result:=length(FItems);
end;

function TRaumschiff.GetLagerRoomBelegt: Integer;
var
  Dummy: Integer;
begin
  result:=0;
  for Dummy:=0 to length(fItems)-1 do
  begin
    result:=result+fItems[Dummy].Verbrauch;
  end;
end;

function TRaumschiff.GetMaxMunition(Zelle: Integer): Integer;
var
  Index: Integer;
begin
  if fWZellen[Zelle].WaffType<>wtLaser then
  begin
    Index:=IndexOfMunition(Zelle);
    if Index=-1 then
      result:=fWZellen[Zelle].Munition
    else
      result:=lager_api_getItem(Index).Munition;
  end
  else
  begin
    result:=fWZellen[Zelle].Munition;
  end;
end;

function TRaumschiff.GetModel: TRaumschiffModel;
begin
  result:=fListe.Models[fModel];
end;

function TRaumschiff.GetMonthKost: Integer;
begin
  if Status<>ssBuild then
    result:=round((Model.KaufPreis div 48)/Model.BauZeit)
  else
    result:=0;
end;

function TRaumschiff.GetMotorIndex: Integer;
begin
  result:=-1;
  if fMotor.Installiert then
    result:=lager_api_GetItemIndex(Motor.ID);
end;

function TRaumschiff.GetSchaden: Integer;
begin
  result:=100-round((fAktHitPoi/fHitPoints*100));
end;

function TRaumschiff.GetSoldat(Slot: Byte; out Soldat: PSoldatInfo): Integer;
begin
  result:=soldaten_api_GetSoldatInRaumschiff(ID,Slot,Soldat);
end;

function TRaumschiff.GetState: TSchiffState;
var
  HomePos: TFloatPoint;
begin
  result:=ssReady;
  if fBauzeit>0 then result:=ssBuild
  else if (not fMotor.Installiert) then result:=ssNoMotor
  else if fMotor.Liter=0 then result:=ssNoBenzin
  else
  begin
    if fBasisID<>0 then
    begin
      HomePos:=GetHomePos;
      if not SamePoint(fPosition,HomePos) then
      begin
        if (fZiel.X<>-1) or (HuntedUFO<>nil) or (Einsatz<>nil) or (EskortedSchiff<>nil) or (Town<>nil) then
          result:=ssAir
        else
          result:=ssBackTobasis;
        exit;
      end
    end
    else
    begin
      // Ist keine Homebase vorhanden, ist das Raumschiff immer in der Luft
      result:=ssAir;
      exit;
    end;

    if ufo_api_GetUFO(fUFOID)<>nil then result:=ssHuntUFO
    else if Einsatz<>nil then result:=ssFlyToEinsatz
    else if fZiel.X<>-1 then result:=ssFlyToPos
    else if Town<>nil then result:=ssFlyToTown
    else if fListe.SchiffOfID(fSchiffID)<>nil then result:=ssEskortSchiff;
  end;
end;

function TRaumschiff.GetStatus: String;
begin
  if fXCOm then
  begin
    case Status of
      ssReady        : result:=STBereit;
      ssBuild        : if fBauZeit=1 then result:=Format(STBauDay,[fBauzeit]) else result:=Format(STBauDays,[fBauzeit]);
      ssNoMotor      : result:=STNoMotor;
      ssNoBenzin     : result:=STNoBenzin;
      ssAir          : result:=STEinsatz;
      ssFlyToEinsatz : result:=STFlyEinsatz;
      ssFlyToPos     : result:=STFlyToPos;
      ssFlyToTown    : result:=ST0309270015;
      ssBackTobasis  : result:=STBackToBasis;
      ssHuntUFO      : result:=STHuntUFO;
      ssEskortSchiff : result:=STEskort;
    end;
  end
  else
  begin
    case fMission of
      rmKauf                 : result:=STEinkauf;
      rmVerkauf              : result:=STVerkauf;
      rmTransferAlphatron    : result:='Lieferung von Alphatron';
    end;
  end;
end;

function TRaumschiff.GetTransportItem: TLagerItem;
begin
  if not XCOMSchiff then
  begin
    result:=lager_api_GetItem(fTransportItem.ID)^;
    result.Anzahl:=fTransportItem.Anzahl;
  end;
end;

function TRaumschiff.GetTreibstoff: double;
begin
  result:=0;
  if Status in [ssReady,ssHuntUFO,ssAir,ssBackToBasis,ssFlyToEinsatz,ssFlyToPos,ssFlyToTown,ssEskortSchiff] then
    result:=fMotor.Liter/fMotor.MaxLiter*100;
end;

function TRaumschiff.GetWaffenIndex(Zelle: Integer): Integer;
begin
  result:=-1;
  if (Zelle<0) or (Zelle>=fZellen) then exit;
  if fWZellen[Zelle].Installiert then
    result:=lager_api_GetItemIndex(fWZellen[Zelle].ID);
end;

function TRaumschiff.GetWaffenZelle(Index: Integer): TWaffenZelle;
begin
  if (Index<0) or (Index>2) then exit;
  result:=fWZellen[Index];
end;

function TRaumschiff.GetZielPosition: TFloatPoint;
begin
  result.X:=-1;
       if not XCOMSchiff then result:=fZiel
  else if HuntedUFO<>nil then result:=HuntedUFO.Position
  else if Einsatz<>nil then result:=Einsatz.Position
  else if (Status in [ssBackToBasis]) then result:=GetHomePos
  else if fZiel.X<>-1 then result:=fZiel
  else if Town<>nil then result:=Town.Position
  else if EskortedSchiff<>nil then result:=EskortedSchiff.Position;
end;

function TRaumschiff.HuntedUFO: TUFO;
begin
  result:=ufo_api_GetUFO(fUFOID);
end;

procedure TRaumschiff.HuntingUFO(ID: Cardinal);
begin
  if ID=0 then
  begin
    BackToBase;
    exit;
  end;

  fZiel:=FloatPoint(-1,-1);
  fEinsatzID:=0;
  fUFOID:=ID;
  fTownID:=0;
  fSchiffID:=0;
end;

function TRaumschiff.IndexOfMunition(Zelle: Integer): Integer;
begin
  result:=fWZellen[Zelle].MunIndex;
  if result<>-1 then
  begin
    if lager_api_GetItem(result).Munfor<>fWZellen[Zelle].ID then
      result:=-1;
  end;
  // Korrekte Munition ist nicht gespeichert
  if result=-1 then
  begin
    result:=lager_api_GetRMunitionIndex(fWZellen[Zelle].ID);
    fWZellen[Zelle].MunIndex:=result;
  end;
end;

procedure TRaumschiff.LoadFromStream(Stream: TStream);
var
  Dummy   : Integer;
  Rec     : TExtRecord;
begin
  Rec:=TExtRecord.Create(TRaumschiffRecord);
  Rec.LoadFromStream(Stream);

  fXCOM:=Rec.GetBoolean('XCOM-Ship');

  fName:=Rec.GetString('Name');
  fID:=Rec.GetInteger('ID');
  fModel:=Rec.GetInteger('Model');
  fZellen:=Rec.GetInteger('WZellen');
  fHitPoints:=Rec.GetInteger('TotalHitPoints');
  fSolSlots:=Rec.GetInteger('Soldaten');
  fLagerPlatz:=Rec.GetInteger('LagerPlatz');
  fSendMessage:=Rec.GetBoolean('WasSendTankMessage');
  fAbschuesse:=Rec.GetInteger('Abschuesse');

  fUFOID:=Rec.GetCardinal('UFOID');
  fEinsatzID:=Rec.GetCardinal('EinsatzID');
  fSchiffID:=Rec.GetInteger('SchiffID');
  fBasisID:=Rec.GetCardinal('BasisID');
  fTownID:=Rec.GetCardinal('TownID');

  fAktHitPoi:=Rec.GetInteger('AktHitpoints');
  fShieldPoints:=Rec.GetInteger('ShieldPoints');
  fExtendsSlots:=Rec.GetInteger('ExtensionCount');
  fSensorWeite:=Rec.GetInteger('SensorWeite');
  fBauZeit:=Rec.GetInteger('BauZeit');

  fPosition.X:=Rec.GetDouble('Position.X');
  fPosition.Y:=Rec.GetDouble('Position.Y');

  fZiel.X:=Rec.GetDouble('Ziel.X');
  fZiel.Y:=Rec.GetDouble('Ziel.Y');

  fMotor:=RecordToRaumschiffMotor(Rec.GetRecordList('Motor').Item[0]);

  with Rec.GetRecordList('WaffenZellen') do
  begin
    for Dummy:=0 to fZellen-1 do
      fWZellen[Dummy]:=RecordToWaffenzelle(Item[Dummy]);
  end;

  with Rec.GetRecordList('ItemCount') do
  begin
    SetLength(fItems,Count);
    for Dummy:=0 to high(fItems) do
    begin
      fItems[Dummy].ID:=Item[Dummy].GetCardinal('ID');
      fItems[Dummy].Anzahl:=Item[Dummy].GetInteger('Anzahl');
      fItems[Dummy].Wanted:=Item[Dummy].GetInteger('Wanted');
      fItems[Dummy].Verbrauch:=Item[Dummy].GetInteger('Verbrauch');
    end;
  end;

  with Rec.GetRecordList('Extensions') do
  begin
    for Dummy:=0 to fExtendsSlots-1 do
      fEZellen[Dummy]:=RecordToRaumschiffExtension(Item[Dummy]);
  end;

  if not fXCOM then
  begin
    fTransportItem.ID:=Rec.GetCardinal('Transport:Item.ID');
    fTransportItem.Anzahl:=Rec.GetInteger('Transport:Item.Anzahl');
    fTransportItem.Preis:=Rec.GetInteger('Transport:Item.Preis');

    fFrom.X:=Rec.GetDouble('Transport:From.X');
    fFrom.Y:=Rec.GetDouble('Transport:From.Y');

    fMission:=TRaumschiffMission(Rec.GetInteger('Transport:Mission'));
    fBuchType:=TKapital(Rec.GetInteger('Transport:BuchType'));

    fAlphatron:=Rec.GetInteger('Transport:Alphatron');
  end
  else
  begin
    {$IFNDEF NOHOMEBASE}
    fBasisDestroyEvent:=basis_api_GetBasisFromID(fBasisID).NotifyList.RegisterEvent(EVENT_BASISVERNICHTET,HomeBaseDestroyed);
    {$ELSE}
    fBasisID:=0;
    {$ENDIF}
  end;

  fSchiffShield:=ShieldLaden;

  Rec.Free;
  AktuFreeSlots;
end;

procedure TRaumschiff.MotorAusbauen;
begin
  if not (sabEquipt in Abilities) then
    exit;

  if fMotor.Installiert then
  begin
    // es erfolgt keine Informationen, wenn nicht genug Platz in der Basis ist
    lager_api_PutItem(fBasisID,fMotor.ID);
    
    fMotor.Installiert:=false;
    fMotor.MaxLiter:=0;
    fMotor.ID:=0;
    HuntingUFO(0);
    Tanken;
  end;
end;

procedure TRaumschiff.NextDay;
begin
  if Status=ssBuild then
  begin
    if savegame_api_NeedMoney(round(Model.KaufPreis/Model.BauZeit),kbRB) then
    begin
      dec(fBauZeit);
      if fBauZeit=0 then
        savegame_api_Message(Format(MBauBeendet,[fName,Model.Name]),lmSchiffEnd);
    end;
  end;
end;

procedure TRaumschiff.RechargeShield(Time: Integer;Full: boolean);
var
  Add: Integer;
begin
  if Full then
  begin
    fSchiffShield:=ShieldLaden;
    fShieldPoints:=MaxShieldPoints;
  end
  else
  begin
    if (ShieldPoints<MaxShieldPoints) then
    begin
      dec(fSchiffShield,Time);
      while (fSchiffShield<0) do
      begin
        inc(fLoadStat);
        if fLoadStat>(MaxShieldPoints div 20) then
        begin
          Add:=min(fLoadStat,MaxShieldPoints-fShieldPoints);
          inc(fShieldPoints,Add);
          dec(fLoadStat,Add);
        end;
        inc(fSchiffShield,ShieldLaden);
      end;
    end;
  end;
end;

procedure TRaumschiff.SaveToStream(Stream: TStream);
var
  Dummy  : Integer;
  Rec    : TExtRecord;
begin
  Rec:=TExtRecord.Create(TRaumschiffRecord);

  // Allgemeine Eigenschaften
  Rec.SetBoolean('XCOM-Ship',fXCOM);
  Rec.SetString('Name',fName);
  Rec.SetInteger('ID',fID);
  Rec.SetInteger('Model',fModel);
  Rec.SetInteger('WZellen',fZellen);
  Rec.SetInteger('TotalHitPoints',fHitPoints);
  Rec.SetInteger('Soldaten',fSolSlots);
  Rec.SetInteger('LagerPlatz',fLagerPlatz);
  Rec.SetInteger('Abschuesse',fAbschuesse);
  Rec.SetInteger('BauZeit',fBauZeit);
  Rec.SetBoolean('WasSendTankMessage',fSendMessage);
  Rec.SetInteger('AktHitpoints',fAktHitPoi);
  Rec.SetInteger('ShieldPoints',fShieldPoints);
  Rec.SetInteger('ExtensionCount',fExtendsSlots);
  Rec.SetInteger('SensorWeite',fSensorWeite);

  Rec.SetCardinal('UFOID',fUFOID);
  Rec.SetInteger('SchiffID',fSchiffID);
  Rec.SetCardinal('BasisID',fBasisID);
  Rec.SetCardinal('EinsatzID',fEinsatzID);
  Rec.SetCardinal('TownID',fTownID);

  Rec.GetRecordList('Motor').Add(RaumschiffMotorToRecord(fMotor));

  Rec.SetDouble('Position.X',fPosition.X);
  Rec.SetDouble('Position.Y',fPosition.Y);

  Rec.SetDouble('Ziel.X',fZiel.X);
  Rec.SetDouble('Ziel.Y',fZiel.Y);

  with Rec.GetRecordList('WaffenZellen') do
  begin
    for Dummy:=0 to fZellen-1 do
      Add(WaffenzelleToRecord(fWZellen[Dummy]));
  end;

  // Ausr�stung speichern
  with Rec.GetRecordList('ItemCount') do
  begin
    for Dummy:=0 to high(fItems) do
    begin
      with Add do
      begin
        SetCardinal('ID',fItems[Dummy].ID);
        SetInteger('Anzahl',fItems[Dummy].Anzahl);
        SetInteger('Wanted',fItems[Dummy].Wanted);
        SetInteger('Verbrauch',fItems[Dummy].Verbrauch);
      end;
    end;
  end;

  // Erweiterungsslots speichern
  with Rec.GetRecordList('Extensions') do
  begin
    for Dummy:=0 to fExtendsSlots-1 do
      Add(RaumschiffExtensionToRecord(fEZellen[Dummy]));
  end;

  if not fXCOM then
  begin
    Rec.SetCardinal('Transport:Item.ID',fTransportItem.ID);
    Rec.SetInteger('Transport:Item.Anzahl',fTransportItem.Anzahl);
    Rec.SetInteger('Transport:Item.Preis',fTransportItem.Preis);

    Rec.SetDouble('Transport:From.X',fFrom.X);
    Rec.SetDouble('Transport:From.Y',fFrom.Y);

    Rec.SetInteger('Transport:Mission',Integer(fMission));
    Rec.SetInteger('Transport:BuchType',Integer(fBuchType));

    Rec.SetInteger('Transport:Alphatron',fAlphatron);
  end;

  Rec.SaveToStream(Stream);
  Rec.Free;
end;

procedure TRaumschiff.SellSchiff;
var
  Dummy  : Integer;
  Item   : Integer;
begin
  MotorAusbauen;

  // Nach dem Ausbau des Motors und aufruf von Tanken, wird das Benzin wieder
  // verkauft
  Tanken;

  // Waffen und Erweiterungsslots ausbauen
  for Dummy:=0 to 2 do
  begin
    WaffeAusbauen(Dummy);
    ExtensionAusbauen(Dummy);
  end;

  // Ausr�stung in die Basis transferieren (ist kein Platz mehr ist di Ausr�stung
  // verloren
  Item:=0;
  while ItemCount>Item do
  begin
    DeleteItem(fItems[Item].ID);
    inc(Item);
  end;

  savegame_api_FreeMoney(Model.VerKaufPreis,kbRV);

  Free;
end;

procedure TRaumschiff.SetMunition(Zelle, Schuss: Integer);
begin
  if (Zelle<0) or (Zelle>fZellen) then exit;
  fWZellen[Zelle].Munition:=Schuss;
end;

procedure TRaumschiff.SetSoldaten(Index: Integer; Slot: Byte);
var
  Soldat : PSoldatInfo;
  OIndex : Integer;
begin
  if not (sabEquipt in Abilities) then
    exit;

  OIndex:=GetSoldat(Slot,Soldat);
  if OIndex>-1 then
  begin
    soldaten_api_RemoveRaumschiff(OIndex);
  end;
  soldaten_api_AssignRaumschiff(Index,ID,Slot);
  AktuFreeSlots;
end;

procedure TRaumschiff.SetWaffenZelle(Index: Integer;
  const Value: TWaffenZelle);
begin
  fWZellen[Index]:=Value;
end;

function TRaumschiff.SetzeMotor(Index: Integer):boolean;
var
  Item: PLagerItem;
begin
  result:=false;

  if not (sabEquipt in Abilities) then
    exit;

  Item:=lager_api_GetItem(Index);

  // Sicherheitspr�fungen
  Assert(Item<>nil);
  if (Item.TypeID<>ptMotor) then
    raise Exception.Create(Format(ENoMotor,[Item.Name]));

  // Gleicher Motor bereits installiert
  if (fMotor.Installiert) and (Item.ID=fMotor.ID) then
    exit;

  if not lager_api_DeleteItem(fBasisID,Item.ID) then
    raise ENotEnoughItems.Create(Format(ENotItems,[Item.Name]));

  try
    MotorAusbauen;
  except
    // Der Alte Motor muss wieder eingebaut werden
    lager_api_PutItem(fBasisID,Item.ID);
    raise;
  end;

  fMotor.Installiert:=true;
  fMotor.Name:=Item.Name;
  fMotor.MaxLiter:=Item.Munition;
  fMotor.ID:=Item.ID;
  fMotor.Verbrauch:=Item.Strength;
  fMotor.Duesen:=Item.Verfolgung;
  fMotor.PpS:=Item.PixPerSec;

  Tanken;
  result:=true;
end;

function TRaumschiff.SetzeWaffe(Index, Zelle: Integer): boolean;
var
  MuniIndex: Integer;
  Error    : TZelleNachladenError;
  Item     : PLagerItem;
begin
  result:=false;

  if not (sabEquipt in Abilities) then
    exit;

  Item:=lager_api_GetItem(Index);
  Assert(Item<>nil);
  if Item.TypeID<>ptRWaffe then
    raise Exception.Create(Format(ENoWaffe,[Item.Name]));

  // Waffe bereits eingebaut
  if (fWZellen[Zelle].Installiert) and (Item.ID=fWZellen[Zelle].ID) then
    exit;

  MuniIndex:=0;
  if Item.WaffType<>wtLaser then
  begin
    MuniIndex:=lager_api_GetRMunitionIndex(Item.ID);
    if MuniIndex=-1 then
      raise Exception.Create(ENoMunition);
  end;

  if not lager_api_DeleteItem(fBasisID,Item.ID) then
    raise ENotEnoughItems.Create(Format(ENotItems,[Item.Name]));

  try
    WaffeAusbauen(Zelle);
  except
    // Alte Waffe wieder in die Basis
    lager_api_PutItem(fBasisID,Item.ID);
    raise;
  end;

  fWZellen[Zelle].Installiert:=true;
  fWZellen[Zelle].Name:=Item.Name;
  fWZellen[Zelle].ID:=Item.ID;
  fWZellen[Zelle].MunIndex:=MuniIndex;
  fWZellen[Zelle].Munition:=0;
  fWZellen[Zelle].WaffType:=Item.WaffType;
  fWZellen[Zelle].Strength:=Item.Strength;
  fWZellen[Zelle].PixPerSec:=Item.PixPerSec;
  fWZellen[Zelle].Laden:=Item.Laden;
  fWZellen[Zelle].Verfolgung:=Item.Verfolgung;
  fWZellen[Zelle].Reichweite:=Item.Reichweite;
  fWZellen[Zelle].ReserveMunition:=0;

  Error:=ZelleNachLaden(Zelle);
  if Error<>zneOK then
    raise Exception.Create(ZelleNachladenErrorText[Error]);

  result:=true;
end;

function TRaumschiff.Tanken(Minuten: Integer = 0): boolean;
var
  Enough  : boolean;
  Liter   : double;
begin
  if fMotor.MaxLiter>fMotor.Liter then
  begin
    if Minuten<>0 then
      Liter:=min(round((fMotor.MaxLiter*0.02)*Minuten),fMotor.MaxLiter-fMotor.Liter)
    else
      Liter:=fMotor.MaxLiter-fMotor.Liter;

    Enough:=false;
    while ((not Enough) and (Liter>0)) do
    begin
      Enough:=savegame_api_NeedMoney(round(Liter*Benzinkosten),kbTrK);

      if not Enough then
        Liter:=Liter-1;
    end;
    if not Enough then
    begin
      result:=false;
      // Nachricht absenden, wenn kein Geld zum Nachtanken verf�gbar ist
      if not fSendMessage then
      begin
        savegame_api_Message(ENoMoneyToTank,lmNachLaden);
        fSendMessage:=true;
      end;
      exit;
    end;
    fMotor.Liter:=fMotor.Liter+Liter;
    if round(fMotor.Liter)=fMotor.MaxLiter then
      fMotor.Liter:=fMotor.MaxLiter;

  end
  else
  begin
    // Benzin verkaufen (Ausbau des Motors)
    Liter:=fMotor.Liter-fMotor.MaxLiter;
    if Liter>0 then
      savegame_api_FreeMoney(round(Liter*Benzinkosten),kbTrK);

    fMotor.Liter:=fMotor.MaxLiter;
  end;
  result:=true;
  fSendMessage:=false;
end;

procedure TRaumschiff.UpgradeID(ID: Cardinal);
var
  Item      : TLagerItem;
  Dummy     : Integer;
  ExtDummy  : Integer;
begin
  Item:=lager_api_GetItem(ID)^;
  if fMotor.Installiert and (fMotor.ID=ID) then
  begin
    fMotor.MaxLiter:=Item.Munition;
    fMotor.Verbrauch:=Item.Strength;
    fMotor.Duesen:=Item.Verfolgung;
    fMotor.PpS:=Item.PixPerSec;
  end;
  for Dummy:=0 to fZellen-1 do
  begin
    if fWZellen[Dummy].Installiert and (fWZellen[Dummy].ID=ID) then
    begin
      fWZellen[Dummy].Strength:=Item.Strength;
      fWZellen[Dummy].PixPerSec:=Item.PixPerSec;
      fWZellen[Dummy].Laden:=Item.Laden;
      fWZellen[Dummy].Verfolgung:=Item.Verfolgung;
    end;
  end;

  for Dummy:=0 to ExtendsSlots-1 do
  begin
    if fEZellen[Dummy].Installiert and (fEZellen[Dummy].ID=ID) then
    begin
      with fEZellen[Dummy] do
      begin
        for ExtDummy:=0 to ExtCount-1 do
        begin
          Extensions[ExtDummy]:=Item.Extensions[ExtDummy];
        end;
      end;
    end;
  end;
end;

procedure TRaumschiff.WaffeAusbauen(Zelle: Integer);
var
  Index: Integer;
begin
  if not (sabEquipt in Abilities) then
    exit;

  if fWZellen[Zelle].Installiert then
  begin
    // es erfolgt keine Informationen, wenn nicht genug Platz in der Basis ist
    lager_api_PutItem(fBasisID,fWZellen[Zelle].ID);
    Index:=IndexOfMunition(Zelle);
    if (Index<>-1) and (fWZellen[Zelle].Munition=lager_api_GetItem(Index).Munition) then
      // es erfolgt keine Informationen, wenn nicht genug Platz in der Basis ist
      lager_api_PutItems(fBasisID,lager_api_GetItem(Index).ID,1);
  end;
  
  fWZellen[Zelle].Installiert:=false;
  fWZellen[Zelle].Munition:=0;
  fWZellen[Zelle].ID:=0;
  fWZellen[Zelle].MunIndex:=-1;
  fWZellen[Zelle].ReserveMunition:=0;
end;

function TRaumschiff.ZelleNachLaden(Zelle: Integer): TZelleNachladenError;
var
  Index : Integer;
  Item  : TLagerItem;
  Schuss: Integer;

  procedure ReserverMunitionBenutzen;
  begin
    Schuss:=Item.Munition-fWZellen[Zelle].Munition; // Ben�tigte Munition berechnen

    Schuss:=min(Schuss,fWZellen[Zelle].ReserveMunition);

    inc(fWZellen[Zelle].Munition,Schuss);
    dec(fWZellen[Zelle].ReserveMunition,Schuss);
  end;

begin
  result:=zneOK;
  if not fWZellen[Zelle].Installiert then
  begin
    result:=zneNichtInstalliert;
    exit;
  end;

  if fWZellen[Zelle].WaffType=wtLaser then
  begin
    fWZellen[Zelle].Munition:=lager_api_GetItem(fWZellen[Zelle].ID).Munition;
    exit;
  end;

  // Gibt es eine Munition zu der Waffe
  Index:=IndexOfMunition(Zelle);
  if Index=-1 then
  begin
    result:=zneNoMunition;
    exit;
  end;

  Item:=lager_api_GetItem(Index)^;
  Item.Anzahl:=lager_api_GetItemCountInBase(fBasisID,Index);

  // Muss �berhaupt nachgeladen werden
  if fWZellen[Zelle].Munition=Item.Munition then
    exit;

  // Reserve Munition aufbrauchen
  ReserverMunitionBenutzen;

  // Reserve Munition hat gereicht (ist ja was �briggeblieben)
  if fWZellen[Zelle].ReserveMunition>0 then
    exit;

  // Munition noch im Lager vorhanden und benutzen
  if (Item.Anzahl>0) then
  begin
    Assert(lager_api_DeleteItem(fBasisID,Item.ID));

    // Nu kann nochmal
    fWZellen[Zelle].ReserveMunition:=Item.Munition;
    ReserverMunitionBenutzen;
  end
  else
  begin
    if (fWZellen[Zelle].Munition=0) then
      result:=zneNotImLager;
  end;
end;

function TRaumschiff.InSelectedBasis: boolean;
begin
  result:=fBasisID=basis_api_GetSelectedBasis.ID;
end;

function TRaumschiff.GetHomePos: TFloatPoint;
var
  Basis: TBasis;
begin
  Basis:=basis_api_GetBasisFromID(fBasisID);
  if Basis=nil then
    result.x:=-1
  else
    result:=Basis.BasisPos;
end;

function TRaumschiff.GetZielBasis: TBasis;
var
  Dummy: Integer;
begin
  result:=nil;
  if fXCOM then exit;

  for Dummy:=0 to basis_api_GetBasisCount-1 do
  begin
    if SamePoint(basis_api_GetBasisFromIndex(Dummy).BasisPos,fZiel) then
    begin
      result:=basis_api_GetBasisFromIndex(Dummy);
      break;
    end;
  end;
  Assert(result<>nil,'Keine Zielbasis gefunden');
end;

function TRaumschiff.GetHomeBasis: TBasis;
begin
  if fXCOM then
  begin
    if fBasisID<>0 then
      result:=basis_api_GetBasisFromID(fBasisID)
    else
      result:=nil;
  end
  else
    result:=GetZielBasis;
end;

function TRaumschiff.GetZielName: String;
begin
  if fXCom then
  begin
    result:='';
    exit;
  end;
  case fMission of
    rmKauf              : result:=GetZielBasis.Name;
    rmVerkauf           : result:=country_api_LandOverPos(fZiel);
    rmTransferAlphatron : result:=GetZielBasis.Name;
  end;
end;

procedure TRaumschiff.SetHomeBase(Basis: TBasis);
var
  fOldBase: Cardinal;
  Dummy   : Integer;
  Soldat  : PSoldatInfo;
  Index   : Integer;
begin
  // Sicherstellen, dass die korrekte Anzahl an Soldaten im Raumschiff gez�hlt
  // wurde
  AktuFreeSlots;

  if GetHomeBasis = Basis then
    exit;

  fOldBase:=fBasisID;

  if (fOldbase<>0) then
    basis_api_GetBasisFromID(fOldBase).NotifyList.RemoveEvent(fBasisDestroyEvent);

  if Basis<>nil then
  begin

    // Pr�fen, ob in der neuen Basis genug Platz f�r alle Soldaten ist
    if basis_api_FreierWohnRaum(Basis.ID)<fSoldaten then
      raise Exception.Create(Format(ST0409270001,[Name,Basis.Name]));

    // Hangarraum in der neuen Basis belegen
    if not Basis.NeedHangar then
      raise Exception.Create(Format(ECantChangeBase,[Name,Basis.Name]));

    // Soldaten der neuen Basis zuordnen
    for Dummy:=0 to SoldatenSlots-1 do
    begin
      Index:=GetSoldat(Dummy,Soldat);
      if Index<>-1 then
        soldaten_api_ChangeBase(Index,Basis.ID);
    end;

    fBasisID:=Basis.ID;

    fBasisDestroyEvent:=Basis.NotifyList.RegisterEvent(EVENT_BASISVERNICHTET,HomeBaseDestroyed);
  end
  else
    fBasisID:=0;

  // Hangarraum der alten Basis freigeben
  if fOldBase<>0 then
    basis_api_FreeHangarRaum(fOldBase);
end;

function TRaumschiff.AddrOfSchiffItemIndex(Index: Integer): PItemsInSchiff;
begin
  result:=Addr(fItems[Index]);
end;

function TRaumschiff.AddrOfSchiffItemID(ID: Cardinal): PItemsInSchiff;
var
  Dummy: Integer;
begin
  if length(fItems)=0 then
  begin
    result:=nil;
    exit;
  end;

  result:=Addr(fItems[0]);
  for Dummy:=0 to high(fItems) do
  begin
    if result.ID=ID then
      exit;
    inc(result);
  end;
  result:=nil;
end;

procedure TRaumschiff.AbgleichWithModel(const Model: TRaumschiffModel);
begin
  // Pr�fung, ob das �berhaupt das richtige Modell ist
  if Model.ID<>Self.Model.ID then
    exit;

  // Hier m�ssen in Zukunft neue Eigenschaften aus Model ins Raumschiff �bernommen werden
end;

{ Ermittelt die Sensorreichweite des Raumschiffes }
function TRaumschiff.GetSensorWeite: Word;
begin
  Result := GetAttributeValue(epSensor); 
end;

function TRaumschiff.GetExtendZelle(Index: Integer): TExtensionZelle;
begin
  result.Installiert:=false;
  if (Index<0) or (Index>=fExtendsSlots) then exit;
  result:=fEZellen[Index];
end;

function TRaumschiff.SetzeExtension(Index: Integer;Zelle: Integer): boolean;
var
  Dummy       : Integer;
  Item        : PLagerItem;
  IsShield    : boolean;
  ShieldIndex : Integer;
  ExtSet      : TExtensionPropSet;
begin
  result:=false;

  if not (sabEquipt in Abilities) then
    exit;

  Item:=lager_api_GetItem(Index);
  Assert(Item<>nil);
  if Item.TypeID<>ptExtension then
    raise Exception.Create(Format(ENoExtension,[Item.Name]));

  if Item.ID=fEZellen[Zelle].ID then
    exit;

  ExtSet := [];
  for Dummy:=0 to Item.ExtCount-1 do
    if Item.Extensions[Dummy].Prop in ShieldExtensions then
      ExtSet := ExtSet + [Item.Extensions[Dummy].Prop];

  IsShield := ExtSet >= ShieldExtensions;

  ShieldIndex := GetExtensionIndexOfTheShield;
  if IsShield and (ShieldIndex>=0) and (ShieldIndex<>Zelle) then
    raise Exception.Create(EOnlyOneShieldAllowed);
    
  if not lager_api_DeleteItem(fBasisID,Item.ID) then
    raise ENotEnoughItems.Create(Format(ENotItems,[Item.Name]));

  try
    ExtensionAusbauen(Zelle);
  except
    // Alte Erweiterung wieder in die Basis
    lager_api_PutItem(fBasisID,Item.ID);
    raise;
  end;

  with fEZellen[Zelle] do
  begin
    Installiert:=true;
    ID:=Item.ID;
    ExtCount:=Item.ExtCount;
    for Dummy:=0 to ExtCount-1 do
      Extensions[Dummy]:=Item.Extensions[Dummy];
  end;

  //Kein Schild, aber eine Schilderweiterung und ein Schild ist schon vorhanden
  if (not IsShield) and GetShieldInstalliert then
    for Dummy:=0 to Item.ExtCount-1 do
      if Item.Extensions[Dummy].Prop in ShieldExtensions then
        IsShield := true;

  if IsShield then
    fShieldPoints := GetMaxShieldPoints;

  result:=true;
end;

procedure TRaumschiff.ExtensionAusbauen(Zelle: Integer);
var
  Dummy    : Integer;
  IsShield : boolean;
begin
  if not (sabEquipt in Abilities) then
    exit;

  if fEZellen[Zelle].Installiert then
  begin
    // es erfolgt keine Informationen, wenn nicht genug Platz in der Basis ist
    lager_api_PutItem(fBasisID,fEZellen[Zelle].ID);

    IsShield := false;
    for Dummy := 0 to fEZellen[Zelle].ExtCount-1 do
      IsShield := IsShield or (fEZellen[Zelle].Extensions[Dummy].Prop in ShieldExtensions);

    fEZellen[Zelle].Installiert:=false;
    fEZellen[Zelle].ID:=0;
    fEZellen[Zelle].ExtCount:=0;

    if IsShield then
      fShieldPoints := GetMaxShieldPoints;
  end;
end;

function TRaumschiff.GetExtendIndex(Zelle: Integer): Integer;
begin
  result:=-1;
  if (Zelle<0) or (Zelle>=fExtendsSlots) then exit;
  if fEZellen[Zelle].Installiert then
    result:=lager_api_GetItemIndex(fEZellen[Zelle].ID);
end;

function TRaumschiff.AttributeImproved(Attribute: TExtensionProp): boolean;
var
  OrgValue : Integer;
begin
  //Default-Werte setzen
  Result:=false;
  OrgValue:=0;

  // Orginal Wert ermitteln, falls notwendig
  case Attribute of
    epShieldHitpoints,
      epShieldDefence,
      epShieldReloadTime:
      begin
        if GetShieldInstalliert then
          OrgValue:=GetSingleAttributeValue(GetExtensionIndexOfTheShield, Attribute);
      end;
  end;

  //�nderung des Standartwertes ermitteln
  case Attribute of
    epSensor : Result:=fSensorWeite<>SensorWeite;
    epShieldHitpoints : Result:=OrgValue<>MaxShieldPoints;
    epShieldDefence : Result:=OrgValue<>ShieldAbwehr;
    epShieldReloadTime : Result:=OrgValue<>ShieldLaden;
  end;
end;

function TRaumschiff.IsOneAttributePresent(Attributes: TExtensionPropSet): boolean;
var
  Dummy  : Integer;
  ExtZae : Integer;
begin
  for Dummy:=0 to fExtendsSlots-1 do
  begin
    if fEZellen[Dummy].Installiert then
    begin
      with fEZellen[Dummy] do
      begin
        for ExtZae:=0 to ExtCount-1 do
        begin
          with Extensions[ExtZae] do
          begin
            if (Prop in Attributes) and (Value>0) then begin
              Result := True;
              exit;
            end;
          end;
        end;
      end;
    end;
  end;
  Result := False;
end;

function TRaumschiff.GetSingleAttributeValue(Index: Integer; Attribute: TExtensionProp): Integer;
var
  ExtZae : Integer;
begin
  with fEZellen[Index] do
  begin
    if fEZellen[Index].Installiert then
    begin
      for ExtZae:=0 to ExtCount-1 do
      begin
        with Extensions[ExtZae] do
        begin
          if Prop=Attribute then
          begin
            Result:=Value;
            exit;
          end;
        end;
      end;
    end;
  end;
  Result:=0;
end;

procedure TRaumschiff.GetMaximalAttributeValue(var Original: Integer;
  Attribute: TExtensionProp);
var
  Dummy  : Integer;
  ExtZae : Integer;
begin
  for Dummy:=0 to fExtendsSlots-1 do
  begin
    if fEZellen[Dummy].Installiert then
    begin
      with fEZellen[Dummy] do
      begin
        for ExtZae:=0 to ExtCount-1 do
        begin
          with Extensions[ExtZae] do
          begin
            if Prop=Attribute then
              Original:=max(Original,Value);
          end;
        end;
      end;
    end;
  end;
end;

procedure TRaumschiff.FlyToTown(TownID: Cardinal);
begin
  if TownID=0 then
  begin
    BackToBase;
    exit;
  end;

  if not GenugTreibstoff(town_api_getTown(TownID,true).Position) then
  begin
    savegame_api_Message(ST0410110001,lmTreibstoffMangel);
    exit;
  end;

  fZiel:=FloatPoint(-1,-1);
  fUFOID:=0;
  fSchiffID:=0;
  fTownID:=TownID;
  fEinsatzID:=0;
end;

function TRaumschiff.Town: TTown;
begin
  result:=town_api_GetTown(fTownID);
end;

procedure TRaumschiff.Repair(Minuten: Integer);
begin
  if fAktHitPoi<fHitPoints then
  begin
    if savegame_api_NeedMoney(Minuten*8,kbRB) then
      fAktHitPoi:=min(fHitPoints,fAktHitPoi+(Minuten*4));
  end;
end;

procedure TRaumschiff.HomeBaseDestroyed(Sender: TObject);
begin
  if IsInHomeBase then
    VernichteRaumschiff
  else
    SetHomeBase(nil);
end;

procedure TRaumschiff.VernichteRaumschiff(FromUFO: TUFO);
var
  SolCount : Integer;
  Dummy    : Integer;
  Index    : Integer;
  Soldat   : PSoldatInfo;
  Text     : String;
begin
  if fHasDestroyed then
    exit;
    
  fHasDestroyed:=true;

  fNotifyList.CallEvents(EVENT_SCHIFFSHOOTDOWN,Self);
  SolCount:=0;

  // Bei der Vernichtung eines Raumschiffes werden alle Soldaten im Raumschiff
  // get�tet. Dazu wird einfach deren Gesundheit auf -1 gesetzt. Durch CheckSoldaten
  // werden alle Soldaten aus der Liste gel�scht, deren Gesundheit kleiner als 0
  for Dummy:=0 to SoldatenSlots-1 do
  begin
    Index:=GetSoldat(Dummy,Soldat);
    if Index<>-1 then
    begin
      Soldat.Ges:=-1;
      inc(SolCount);
      savegame_api_BuchPunkte(-30,pbSoldatGetoetet);
    end;
  end;
  soldaten_api_CheckSoldaten;

  if FromUFO=nil then
    Text:=Format(ST0309270016,[Name])
  else
    Text:=Format(MDestruction,[Name,FromUFO.Name]);
  if SolCount>0 then
  begin
    Text:=Text+Format(MSolDies,[SolCount]);
  end;
  savegame_api_Message(Text,lmUFOs,FromUFO);

  if fXCOM then
    savegame_api_BuchPunkte(-250,pbRaumschiffZerstoert);

  Free;
end;

function TRaumschiff.IsInHomeBase: Boolean;
begin
  result:=false;
  if (fBasisID=0) or (not fXCom) then
    exit;

  if (GetHomePos.X=fPosition.X) and (GetHomePos.Y=fPosition.Y) then
    result:=true;
end;

function TRaumschiff.IsOverTown(Town: TTown): Boolean;
begin
  if Town=nil then
    result:=false
  else
    result:=(round(fPosition.X)=round(Town.Position.X)) and
            (round(fPosition.Y)=round(Town.Position.Y));
end;

procedure TRaumschiff.ReachedDestination;
var
  Item    : TLagerItem;
  Message : String;
  Count   : Integer;
begin
  // Raumschiff erreicht sein Ziel
  if not fXCOM then
  begin
    case fMission of
      rmKauf :
      begin
        Item:=lager_api_GetItem(fTransportItem.ID)^;

        Message:=Format(MLieferung,[fTransportItem.Anzahl,Item.Name,GetZielName]);

        Count:=lager_api_PutItems(GetZielBasis.ID,fTransportItem.ID,fTransportItem.Anzahl);

        if Count<>fTransportItem.Anzahl then
        begin
          fTransportItem.Anzahl:=fTransportItem.Anzahl-Count;
          fZiel:=fFrom;
          fMission:=rmVerkauf;
          fBuchType:=kbLK;
          Message:=Message+' '+Format(MNoRoomForLie,[fTransportItem.Anzahl]);
          savegame_api_Message(Message,lmAngebote);

          // Exit, da sonst der Transporter zerst�rt wird
          exit;
        end;

        savegame_api_Message(Message,lmAngebote);
      end;
      rmVerkauf:
      begin
        Item:=lager_api_GetItem(fTransportItem.ID)^;

        savegame_api_FreeMoney(fTransportItem.Anzahl*fTransportItem.Preis,fBuchType);

        if fTransportItem.Preis=0 then
          savegame_api_Message(Format(MLieferung,[fTransportItem.Anzahl,Item.Name,GetZielName]),lmAngebote)
        else
          savegame_api_Message(Format(MVerkauf,[fTransportItem.Anzahl,Item.Name,(fTransportItem.Anzahl*fTransportItem.Preis)/1]),lmAngebote);
      end;
      rmTransferAlphatron:
      begin
        savegame_api_FreeAlphatron(fAlphatron,GetZielBasis.ID);

        savegame_api_Message(Format(ST0412200004,[fAlphatron,GetZielbasis.Name]),lmAlphatronTransport);
      end;
    end;
  end;

  fNotifyList.CallEvents(EVENT_SCHIFFREACHEDDEST,Self);
  Free;
end;

class function TRaumschiff.ReadFromScriptString(ObjStr: String): TObject;
begin
  result:=raumschiff_api_GetRaumschiff(StrToInt(ObjStr));
end;

function TRaumschiff.WriteToScriptString: String;
begin
  result:=IntToStr(ID);
end;

function TRaumschiff.BerechneVerbrauch(km: Integer): double;
begin
  result:=((km/1000)*fMotor.Verbrauch)/VerbrauchAnpassung;
end;

function TRaumschiff.GenugTreibstoff(Destination: TFloatPoint): Boolean;
var
  Entf    : Integer;
  Benzinv : double;
begin
  // Bei Transportern oder Raumschiffen ohne Homebase kann keine Pr�fung erfolgen
  if (not fXCom) or (fBasisID=0) then
  begin
    result:=true;
    exit;
  end;

  result:=false;
  if fMotor.Installiert then
  begin
    result:=true;
    // Von der aktuellen Position zum Ziel
    Entf:=round(EarthEntfernung(fPosition,Destination));

    // Vom Ziel zur�ck zur Basis
    Entf:=Entf+round(EarthEntfernung(Destination,GetHomePos));
    // 500 km Reserver einbauen
    Benzinv:=BerechneVerbrauch(Entf+1000);

    if Benzinv>fMotor.Liter then
      result:=false;
  end;
end;

constructor TRaumschiff.CreateTransport(Liste: TRaumschiffList;
  Alphatron: Integer; FromPos, ToPos: TFloatPoint);
begin
  SetTransporterSettings(Liste);

  fPosition:=FromPos;
  fZiel:=ToPos;
  fFrom:=FromPos;

  fMission:=rmTransferAlphatron;
  fAlphatron:=Alphatron;
end;

procedure TRaumschiff.SetTransporterSettings(Liste: TRaumschiffList);
begin
  DefaultCreate(Liste);

  fName:=PreTransporter+IntToHex(random(High(Word)),4);

  FillChar(fMotor,SizeOf(fMotor),#0);
  fMotor.Installiert:=true;
  fMotor.MaxLiter:=1000;
  fMotor.Liter:=1000;
  fMotor.Verbrauch:=5;
  fMotor.PpS:=5;

  fAktHitPoi:=1000;
  fHitPoints:=1000;
  fSensorWeite:=2500;
  fXCOM:=false;
end;

constructor TRaumschiff.Create(Liste: TRaumschiffList);
begin
  DefaultCreate(Liste);
end;

procedure TRaumschiff.DefaultCreate(Liste: TRaumschiffList);
begin
  fListe:=Liste;

  fNotifyList:=TNotifyList.Create;

  fNotifyList.RegisterEvent(EVENT_SCHIFFREACHEDHOMEBASE,HomeBaseReached);

  Liste.AddSchiff(Self);
  fZiel:=FloatPoint(-1,-1);
  fListe:=Liste;
  fAbschuesse:=0;
  fUFOID:=0;
  fXCOM:=true;
  fHasDestroyed:=false;
  
  fName:=CreateName;
  fID:=CreateID;
end;

procedure TRaumschiff.SetModel(Model: Integer);
begin
  fModel:=Model;
  with Liste.Models[Model] do
  begin
    fZellen:=WaffenZellen;
    fHitPoints:=HitPoints;
    fAktHitPoi:=HitPoints;
    fSolSlots:=Soldaten;
    fLagerPlatz:=LagerPlatz;
    fExtendsSlots:=ExtendsSlots;
    fSensorWeite:=SensorWeite;
  end;
end;

function TRaumschiff.GetRepairTime: Integer;
begin
  result:=(fHitPoints-fAktHitPoi) div 4;
end;

procedure TRaumschiff.DestroyShip;
begin
  fBasisID:=0;
  Free;
end;

function TRaumschiff.GetExtensionIndexOfTheShield: Integer;
var
  Dummy  : Integer;
  ExtZae : Integer;
  ExtSet : TExtensionPropSet;
begin
  Result := -1;
  for Dummy:=0 to fExtendsSlots-1 do
  begin
    with fEZellen[Dummy] do
    begin
      ExtSet := [];
      for ExtZae:=0 to ExtCount-1 do
      begin
        with Extensions[ExtZae] do
        begin
          if (Prop in ShieldExtensions) and (Value>0) then
          begin
            ExtSet := ExtSet + [Prop];
          end;
        end;
      end;

      // Wenn eine Extension gefunden wird, die wenigstens alle 3
      // Schildeigenschaften hat, dann handelt es sich um ein Schild
      if ExtSet >= ShieldExtensions then
      begin
        Result := Dummy;
        exit;
      end;
    end;
  end;
end;

function TRaumschiff.SumAttributeValues(
  Attribute: TExtensionProp): Integer;
var
  Dummy  : Integer;
  ExtZae : Integer;
begin
  Result := 0;
  for Dummy:=0 to fExtendsSlots-1 do
  begin
    if fEZellen[Dummy].Installiert then
    begin
      with fEZellen[Dummy] do
      begin
        for ExtZae:=0 to ExtCount-1 do
        begin
          with Extensions[ExtZae] do
          begin
            if Prop=Attribute then
              Inc(Result, Value);
          end;
        end;
      end;
    end;
  end;
end;

procedure TRaumschiff.GetBoosterAttributeValue(var Original: Integer;
  Index: Integer; Attribute: TExtensionProp);
var
  Dummy  : Integer;
  ExtZae : Integer;
begin
  for Dummy:=0 to fExtendsSlots-1 do
  begin
    if (Dummy<>Index) and (fEZellen[Dummy].Installiert) then
    begin
      with fEZellen[Dummy] do
      begin
        for ExtZae:=0 to ExtCount-1 do
        begin
          with Extensions[ExtZae] do
          begin
            //Falls ein Wert vorhanden und ungleich 0 ist, dann ein Bonus dazurechnen
            if (Prop=Attribute) and (Value<>0) then
              Original := Round(Original * ((Value+100)/100));
          end;
        end;
      end;
    end;
  end;
end;

function TRaumschiff.GetInvertedFractialSumOfAttributeValues(Base: Integer;
  Attribute: TExtensionProp): Extended;
var
  Dummy  : Integer;
  ExtZae : Integer;
begin
  Result := 0;
  for Dummy:=0 to fExtendsSlots-1 do
  begin
    if fEZellen[Dummy].Installiert then
    begin
      with fEZellen[Dummy] do
      begin
        for ExtZae:=0 to ExtCount-1 do
        begin
          with Extensions[ExtZae] do
          begin
            if (Prop=Attribute) and (Value<>0) then
              Result := Result + (Base/Value);
          end;
        end;
      end;
    end;
  end;
end;

function TRaumschiff.GetCruisingRange: Integer;
begin
  result:=round(((fMotor.Liter*VerbrauchAnpassung)/fMotor.Verbrauch)*1000);
end;

function TRaumschiff.GetAttributeValue(Attribute: TExtensionProp): Integer;
var
  ShieldExtension: Integer;
  
begin
  case Attribute of
    epSensor :
      begin
        { Kontrolliert die Erweiterungen nach epSensor    }
        { gr��te Sensoreichweite wird �bernommen          }
        Result:=fSensorWeite;
        GetMaximalAttributeValue(Result, epSensor);
      end;

    epShieldHitpoints :
      begin
        if GetExtensionIndexOfTheShield>=0 then
          Result := SumAttributeValues(epShieldHitpoints)
        else
          Result := 0;
      end;

    epShieldDefence :
      begin
        ShieldExtension := GetExtensionIndexOfTheShield;
        if ShieldExtension>=0 then
        begin
          { Hole den Wert f�r Schildabwehr von der Schild-Erweiterung }
          Result := GetSingleAttributeValue(ShieldExtension, epShieldDefence);
          { Berechne die Verbesserung der Abwehr aus anderen Erweiterungen }
          GetBoosterAttributeValue(Result, ShieldExtension, epShieldDefence);
        end else
          Result := 0;
      end;

    epShieldReloadTime :
      begin
        Result := 0;
        if GetExtensionIndexOfTheShield>=0 then
        begin
          //H�chste Aufladezeit holen
          GetMaximalAttributeValue(Result, epShieldReloadTime);
          //Bruchteile summieren (z.B. 100ms, 200ms und 300ms ergeben dann 300/100+300/200+300/300=5,5)
          Result := Round(Result / GetInvertedFractialSumOfAttributeValues(Result, epShieldReloadTime));
        end;
      end;
  end;
end;

procedure TRaumschiff.HomeBaseReached(Sender: TObject);

  procedure CheckLadeRaum;
  var
    Dummy     : Integer;
    Mess      : String;
    Anzahl    : Integer;
    EntlOK    : Boolean;
    BelaOK    : Boolean;
    Item      : PLagerItem;
    Soldat    : PSoldatInfo;
    Index     : Integer;
  begin
    // Bei Transportern darf nicht der Laderaum automatisch geleert werden
    if not fXCOM then
      exit;

    Mess:=Format(ST0410050001,[Name]);
    EntlOK:=true;
    BelaOK:=true;
    for Dummy:=ItemCount-1 downto 0 do
    begin
      if fItems[Dummy].Wanted<fItems[Dummy].Anzahl then
      begin
        // Ausr�stung muss ausgeladen werden
        Anzahl:=fItems[Dummy].Wanted-fItems[Dummy].Anzahl;
        Anzahl:=Anzahl-ChangeItem(fItems[Dummy].ID,Anzahl);
        if (Anzahl<>0) and EntlOK then
        begin
          EntlOK:=false;
          Mess:=Mess+#13#10+ST0410050003;
        end;
      end;
      if fItems[Dummy].Wanted>fItems[Dummy].Anzahl then
      begin
        // Ausr�stung muss eingeladen werden
        Anzahl:=fItems[Dummy].Wanted-fItems[Dummy].Anzahl;
        Anzahl:=Anzahl-ChangeItem(fItems[Dummy].ID,Anzahl);
        if (Anzahl<>0) then
        begin
          if BelaOK then
          begin
            BelaOK:=false;
            Mess:=Mess+#13#10+ST0410050004;
          end;

          Item:=lager_api_GetItem(fItems[Dummy].ID);
          Mess:=Format('%s'#13#10+ST0410050005,[Mess,Item.Name,Anzahl]);
        end;
      end;
    end;

    if not (BelaOK and EntlOK) then
      savegame_api_Message(Mess,lmNachLaden,Self);

    // Soldaten wieder ausr�sten
    for Dummy:=0 to SoldatenSlots do
    begin
      Index:=GetSoldat(Dummy,Soldat);
      if Index<>-1 then
        soldaten_api_GetManager(Index).Reequip;
    end;
  end;

begin
  CheckLadeRaum;
end;

procedure TRaumschiff.BackToBase;
begin
  if (fZiel.X=-1) and (fEinsatzID=0) and (fUFOID=0) and (fTownID=0) and (fSchiffID=0) then
    exit;
    
  fZiel:=FloatPoint(-1,-1);
  fEinsatzID:=0;
  fUFOID:=0;
  fTownID:=0;
  fSchiffID:=0;

  if Status=ssReady then
    fNotifyList.CallEvents(EVENT_SCHIFFREACHEDHOMEBASE,Self);
end;

{ TScanWorld }

procedure TScanWorld.AddSchiff(SenderSchiff: TObject);
begin
  SetLength(fEvents,length(fEvents)+1);
  with fEvents[high(fEvents)] do
  begin
    Schiff:=TRaumschiff(SenderSchiff);
    Event:=TRaumschiff(SenderSchiff).NotifyList.RegisterEvent(EVENT_SCHIFFONMOVE,ChangeScanArea);
  end;
end;

function TScanWorld.AktuScanArea: Boolean;
var
  Dummy  : Integer;
  Points : TScannerPoints;
begin
  result:=fChangeScan;
  if not fChangeScan then
    exit;
  fChangeScan:=false;

  SetLength(Points,0);
  for Dummy:=0 to basis_api_GetBasisCount-1 do
  begin
    with basis_api_GetBasisFromIndex(Dummy) do
    begin
      if SensorWidth>0 then
      begin
        SetLength(Points,length(Points)+1);
        with Points[high(Points)] do
        begin
          Position:=BasisPos;
          Range:=SensorWidth;
          Color:=clNavy;
        end;
      end;
    end;
  end;

  for Dummy:=0 to raumschiff_api_GetRaumschiffCount-1 do
  begin
    with raumschiff_api_GetIndexedRaumschiff(Dummy) do
    begin
      if XCOMSchiff and (Status in [ssAir,ssBackToBasis]) then
      begin
        SetLength(Points,length(Points)+1);
        with Points[high(Points)] do
        begin
          Position:=raumschiff_api_GetIndexedRaumschiff(Dummy).Position;
          Range:=SensorWeite;
          Color:=clBlue;
        end;
      end;
    end;
  end;

  for Dummy:=0 to basis_api_GetBasisCount-1 do
  begin
    with basis_api_GetBasisFromIndex(Dummy) do
    begin
      if AbwehrRange>0 then
      begin
        SetLength(Points,length(Points)+1);
        with Points[high(Points)] do
        begin
          Position:=BasisPos;
          Range:=AbwehrRange;
          Color:=clRed;
        end;
      end;
    end;
  end;

  CalcSensorMapFast(Points);
end;

function TScanWorld.CheckPos(Pos: TFloatPoint): Boolean;
var
  Dummy: Integer;
begin
  for Dummy:=0 to basis_api_GetBasisCount-1 do
  begin
    if EarthEntfernung(basis_api_GetBasisFromIndex(Dummy).BasisPos,Pos)<=basis_api_GetBasisFromIndex(Dummy).SensorWidth then
    begin
      result:=true;
      exit;
    end;
  end;
  for Dummy:=0 to fSchiffList.Count-1 do
  begin
    with fSchiffList[Dummy] do
    begin
      if XCOMSchiff and (Status in [ssAir,ssBackToBasis]) then
      begin
        if EarthEntfernung(Position,Pos)<=SensorWeite then
        begin
          result:=true;
          exit;
        end;
      end;
    end;
  end;
  result:=false;
end;

constructor TScanWorld.Create;
begin
  basis_api_RegisterNewBaseHandler(ChangeScanArea);
  basis_api_RegisterDestroyBaseHandler(ChangeScanArea);

  fChangeScan:=false;
end;

procedure TScanWorld.DeleteSchiff(SenderSchiff: TObject);
var
  Dummy: Integer;
begin
  for Dummy:=0 to high(fEvents) do
  begin
    if fEvents[Dummy].Schiff=SenderSchiff then
      TRaumschiff(SenderSchiff).NotifyList.RemoveEvent(fEvents[Dummy].Event);
  end;

  fChangeScan:=true;
end;

procedure TScanWorld.ChangeScanArea(Sender: TObject);
begin
  fChangeScan:=true;
end;

procedure TScanWorld.SetSchiffList(const Value: TRaumschiffList);
begin
  fSchiffList := Value;
  fSchiffList.NotifyList.RegisterEvent(EVENT_RAUMSCHIFFLISTADD,AddSchiff);
  fSchiffList.NotifyList.RegisterEvent(EVENT_RAUMSCHIFFLISTDELETE,DeleteSchiff);
end;

procedure TScanWorld.CalcSensorMapFast(PointArray: TScannerPoints);
var
  Dummy : Integer;
begin
  gGlobeRenderer.ClearSensorFields;

  for Dummy:=0 to high(PointArray) do
  begin
    gGlobeRenderer.CreateSensorField(PointArray[Dummy].Position,PointArray[Dummy].Range,PointArray[Dummy].Color);
  end;
end;

var
  Temp  : TExtRecordDefinition;

initialization

  TRaumschiffListRecord:=TExtRecordDefinition.Create('TRaumschiffListRecord');
  TRaumschiffListRecord.AddRecordList('Models',RecordRaumschiffModel);
  TRaumschiffListRecord.AddInteger('Count',0,high(Integer));

  TRaumschiffRecord:=TExtRecordDefinition.Create('TRaumschiffRecord');
  TRaumschiffRecord.AddBoolean('XCOM-Ship');
  TRaumschiffRecord.AddString('Name',50);
  TRaumschiffRecord.AddInteger('ID',low(Integer),high(Integer));
  TRaumschiffRecord.AddInteger('Model',low(Integer),high(Integer));
  TRaumschiffRecord.AddInteger('WZellen',low(Integer),high(Integer));
  TRaumschiffRecord.AddInteger('TotalHitPoints',low(Integer),high(Integer));
  TRaumschiffRecord.AddInteger('Soldaten',low(Integer),high(Integer));
  TRaumschiffRecord.AddInteger('LagerPlatz',low(Integer),high(Integer));
  TRaumschiffRecord.AddInteger('Abschuesse',low(Integer),high(Integer));
  TRaumschiffRecord.AddInteger('BauZeit',low(Integer),high(Integer));
  TRaumschiffRecord.AddBoolean('WasSendTankMessage');
  TRaumschiffRecord.AddInteger('AktHitpoints',low(Integer),high(Integer));

  TRaumschiffRecord.AddCardinal('UFOID',0,high(Cardinal));
  TRaumschiffRecord.AddInteger('SchiffID',low(Integer),high(Integer));
  TRaumschiffRecord.AddCardinal('BasisID',0,high(Cardinal));
  TRaumschiffRecord.AddCardinal('TownID',0,high(Cardinal));
  TRaumschiffRecord.AddCardinal('EinsatzID',0,high(Cardinal));
  
  TRaumschiffRecord.AddDouble('Position.X',-360,360,10);
  TRaumschiffRecord.AddDouble('Position.Y',-360,360,10);
  TRaumschiffRecord.AddDouble('Ziel.X',-360,360,10);
  TRaumschiffRecord.AddDouble('Ziel.Y',-360,360,10);
  TRaumschiffRecord.AddInteger('ShieldPoints',0,high(Integer));
  TRaumschiffRecord.AddInteger('SensorWeite',0,high(Integer));
  TRaumschiffRecord.AddInteger('ExtensionCount',0,high(Integer));

  TRaumschiffRecord.AddRecordList('Shield',RecordRaumschiffShield);
  TRaumschiffRecord.AddRecordList('Motor',RecordRaumschiffMotor);
  TRaumschiffRecord.AddRecordList('WaffenZellen',RecordWaffenzelle);

  Temp:=TExtRecordDefinition.Create('ItemCount');
  Temp.AddCardinal('ID',0,high(Cardinal));
  Temp.AddInteger('Anzahl',0,high(Integer));
  Temp.AddInteger('Verbrauch',0,high(Integer));
  Temp.AddInteger('Wanted',0,high(Integer));

  TRaumschiffRecord.AddRecordList('ItemCount',Temp);

  TRaumschiffRecord.AddRecordList('Extensions',RecordRaumschiffExtension);

  TRaumschiffRecord.AddCardinal('Transport:Item.ID',0,high(Cardinal));
  TRaumschiffRecord.AddInteger('Transport:Item.Anzahl',0,high(Integer));
  TRaumschiffRecord.AddInteger('Transport:Item.Preis',0,high(Integer));

  TRaumschiffRecord.AddDouble('Transport:From.X',-360,360,10);
  TRaumschiffRecord.AddDouble('Transport:From.Y',-360,360,10);

  TRaumschiffRecord.AddInteger('Transport:Mission',0,high(Integer));
  TRaumschiffRecord.AddInteger('Transport:BuchType',0,high(Integer));
  TRaumschiffRecord.AddInteger('Transport:Alphatron',0,high(Integer));

finalization
  TRaumschiffRecord.Free;
end.
