{********************************************************************************
*										*
* Copyright (C) 2004  X-Force Team - jim_raynor@web.de				*
*										*
* This file is part of X-Force: Fight For Destiny (X-Force).			*
*										*
* X-Force is free software; you can redistribute it and/or modify		*
* it under the terms of the GNU General Public License as published by		*
* the Free Software Foundation; either version 2 of the License, or		*
* (at your option) any later version.						*
*										*
* X-Force is distributed in the hope that it will be useful,			*
* but WITHOUT ANY WARRANTY; without even the implied warranty of		*
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the			*
* GNU General Public License for more details.					*
*										*
* You should have received a copy of the GNU General Public License		*
* along with X-Force; if not, write to the Free Software			*
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA	*
*										*
*********************************************************************************
*										*
* Komplette Engine f�r den Bodeneinsatz						*
*										*
********************************************************************************}

{$I ../Settings.inc}

unit DXISOTileGroup;

interface

uses
  XForce_Types, TileGroup, Classes, Windows, Graphics, DXDraws, IDHashMap,
  SysUtils;

type
  TDXISOTileGroup = class
  private
    fDXDraws     : TDXDraw;
    fTSet        : TList;

    fUsedTiles   : Array of TUsedTileInfo;
    fUsedTileSets: TStringList;

    fTileInfos   : TTileInformationArray;
    fWallInfos   : TWallInformationArray;

  public
    constructor Create(DXDraws: TDXDraw);
    destructor Destroy;override;

    procedure LoadTiles;
    procedure ReadUsedTiles(Stream: TStream);

    procedure Clear;

    property TileSurfaces: TList read fTSet write fTSet;

    function GetWallInfos(Index: Integer): TWallInformation;
    function GetTileInfos(Index: Integer): TTileInformation;

    procedure DrawFloor(Surface: TDirectDrawSurface;XPos,YPos: Integer;Index, Alpha: Integer);

    procedure DrawMauer(Surface: TDirectDrawSurface;XPos,YPos: Integer;Index, Alpha: Integer;Cut3D, Transparent: Boolean; WallIndex: Integer);

    function CheckWall(Index: Integer; X,Z: Integer; Left: Boolean): Integer;
  end;

implementation

uses
  DirectDraw, ISOTools, Defines, KD4Utils, Blending, EinsatzIntro;

{ TDXISOTileGroup }

function TDXISOTileGroup.CheckWall(Index, X, Z: Integer;
  Left: Boolean): Integer;
var
  ImageIndex: Integer;

  function Check(TileSet: Integer): Boolean;
  var
    Col: TColor;
  begin
    Col:=TDirectDrawSurface(fTSet[TileSet]).Pixels[(Index*WallWidth)+X,WallHeight-Z-2];

    result:=Col=TDirectDrawSurface(fTSet[TileSet]).TransparentColor;
  end;

begin
  Assert((Index>=0) and (Index<=high(fWallInfos)));

  result:=0;

  if (X<0) or (X>=WallWidth) or (Z<0) or (Z>WallHeight) then
    exit;
    
  if Left then
  begin
    ImageIndex:=LeftIndex;
    Z:=Z+(X div 2);
    inc(X,Wall3DEffekt);
  end
  else
  begin
    ImageIndex:=RightIndex;
    Z:=Z+((HalfTileWidth-X) div 2);
  end;

  if not Check(ImageIndex) then
    result:=BarnessPower[fWallInfos[Index].Barness];
end;

procedure TDXISOTileGroup.Clear;
var
  Dummy: Integer;
begin
  for Dummy:=0 to fTSet.Count-1 do
  begin
    // Muss auf 1,1 gesetzt werden (bei 0,0 passiert nichts)
    TDirectDrawSurface(fTSet[Dummy]).SetSize(1,1);
  end;
end;

constructor TDXISOTileGroup.Create(DXDraws: TDXDraw);

  function CreateTileSurface: TDirectDrawSurface;
  begin
    result:=TDirectDrawSurface.Create(DXDraws.DDraw);
    result.SystemMemory:=true;
  end;

begin
  fDXDraws:=DXDraws;

  fTSet:=TList.Create;

  fTSet.Add(CreateTileSurface);
  fTSet.Add(CreateTileSurface);
  fTSet.Add(CreateTileSurface);
  fTSet.Add(CreateTileSurface);

  fUsedTileSets:=TStringList.Create;
end;

destructor TDXISOTileGroup.Destroy;
var
  Dummy: Integer;
begin
  for Dummy:=0 to fTSet.Count-1 do
    TObject(fTSet[Dummy]).Free;

  fTSet.Free;
  fUsedTileSets.Free;
  inherited;
end;

procedure TDXISOTileGroup.DrawFloor(Surface: TDirectDrawSurface; XPos,
  YPos, Index, Alpha: Integer);
begin
  Surface.Draw(xPos,YPos,Rect(Index*TileWidth,Alpha*TileHeight,(Index+1)*TileWidth,(Alpha+1)*TileHeight),TDirectDrawSurface(fTSet[FloorIndex]));
end;

procedure TDXISOTileGroup.DrawMauer(Surface: TDirectDrawSurface; XPos,
  YPos, Index, Alpha: Integer; Cut3D, Transparent: Boolean;
  WallIndex: Integer);
var
  LeftWall: Integer;
  WallRect: TRect;
begin
  LeftWall:=Alpha*WallHeight;
  WallRect:=Rect(Index*WallWidth,LeftWall,Index*WallWidth+WallWidth,LeftWall+Wallheight);

  {$IFNDEF NOCUT3D}
  if Cut3D then
    dec(WallRect.Right,Wall3DEffekt);
  {$ENDIF}

  if Transparent then
    DrawTransAlpha(Surface,xpos,ypos,WallRect,TDirectDrawSurface(fTSet[WallIndex]),128,TDirectDrawSurface(fTSet[WallIndex]).TransparentColor)
  else
    if (fWallInfos[Index].DrawWindow) then
      DrawWallWindow(Surface,xpos,ypos,WallRect,TDirectDrawSurface(fTSet[WallIndex]),fWallInfos[Index].WindowColor)
    else
      Surface.Draw(xPos,yPos,WallRect,TDirectDrawSurface(fTSet[WallIndex]));

end;

function TDXISOTileGroup.GetTileInfos(Index: Integer): TTileInformation;
begin
  Assert((Index>=0) and (Index<=high(fTileInfos)),Format('TileIndex: %d',[Index]));
  result:=fTileInfos[Index];
end;

function TDXISOTileGroup.GetWallInfos(Index: Integer): TWallInformation;
begin
  Assert((Index>=0) and (Index<=high(fWallInfos)),Format('WallIndex: %d',[Index]));
  result:=fWallInfos[Index];
end;

procedure TDXISOTileGroup.LoadTiles;
var
  Groups  : Array of TTileGroup;
  OwnTile : TTileGroup;
  Dummy   : Integer;
  Bitmap  : TBitmap;

  procedure CreateBlendedSurface(Index: Integer);
  var
    BitHei  : Integer;
    Dummy   : Integer;
    DDMem   : TDDSurfaceDesc;
  begin
    with TDirectDrawSurface(fTSet[Index]) do
    begin
      if Bitmap.Width=0 then
        SetSize(0,0)
      else
      begin
        BitHei:=Bitmap.Height;
        SetSize(Bitmap.Width,BitHei*8);
        for Dummy:=1 to 8 do
        begin
          Canvas.Draw(0,(Dummy-1)*BitHei,Bitmap);
          Canvas.Release;
          Assert(Lock(DDMem));
          BlendTileSet(Rect(0,(Dummy-1)*BitHei,Width,(Dummy*BitHei)-1),255-((Dummy*32)-1),TDirectDrawSurface(fTSet[Index]),DDMem);
          Unlock;
        end;
        
        IncEinsatzLoadProgress;

        TransparentColor:=Pixels[0,0];
        Restore;
      end;
    end;
  end;

  function FindRealWallIndex(TileSetIndex, Index: Integer): Integer;
  var
    Dummy: Integer;
  begin
    result:=-1;
    if Index=-1 then
      exit;
    for Dummy:=0 to high(fUsedTiles) do
    begin
      if (fUsedTiles[Dummy].Typ=ttWall) and (fUsedTiles[Dummy].TileSetIndex=TileSetIndex) and (fUsedTiles[Dummy].Index=Index) then
      begin
        result:=fUsedTiles[Dummy].OwnIndex;
        exit;
      end;
    end;
  end;
begin
  if fUsedTilesets.Count=0 then
    exit;

  // Alle Tilesets erstellen
  SetLength(Groups,fUsedTilesets.Count);
  for Dummy:=0 to fUsedTilesets.Count-1 do
  begin
    Groups[Dummy]:=TTileGroup.Create(nil);
    Groups[Dummy].DXDraw:=fDXDraws;
    Groups[Dummy].LoadFromFile('data\Tilesets\'+fUsedTilesets[Dummy]+'.t3d');

    IncEinsatzLoadProgress;
  end;
  OwnTile:=TTileGroup.Create(nil);
  OwnTile.DXDraw:=fDXDraws;

  Bitmap:=TBitmap.Create;
  for Dummy:=0 to high(fUsedTiles) do
  begin
    with fUsedTiles[Dummy] do
    begin
      case Typ of
        ttFloor:
        begin
          Bitmap.Width:=TileWidth;
          Bitmap.Height:=TileHeight;
          Groups[TileSetIndex].DrawFloorCanvas(Bitmap.Canvas,0,0,Index);
          OwnTile.AddFloor(Bitmap);
          OwnIndex:=OwnTile.FloorCount-1;
          OwnTile.TileInformation(OwnIndex)^:=Groups[TileSetIndex].TileInformation(Index)^;
        end;
        ttWall:
        begin
          Bitmap.Width:=WallWidth;
          Bitmap.Height:=WallHeight;
          Groups[TileSetIndex].DrawWallLeftCanvas(Bitmap.Canvas,0,0,Index);
          OwnTile.AddWall(Bitmap);
          Groups[TileSetIndex].DrawWallRightCanvas(Bitmap.Canvas,0,0,Index);
          OwnIndex:=OwnTile.WallLeftCount-1;
          OwnTile.ReplaceWallRight(Bitmap,OwnIndex);
          OwnTile.WallInformation(OwnIndex)^:=Groups[TileSetIndex].WallInformation(Index)^;

          with OwnTile.WallInformation(OwnIndex)^ do
          begin
            Mirror:=FindRealWallIndex(TileSetIndex,Mirror);
            BackSide:=FindRealWallIndex(TileSetIndex,BackSide);
            Border:=FindRealWallIndex(TileSetIndex,Border);
            Destroyed:=FindRealWallIndex(TileSetIndex,Destroyed);
          end;

        end;
      end;
    end;
  end;

  Bitmap.Width:=0;

  // Tiles �bernehmen
  Bitmap.Width:=TileWidth*OwnTile.FloorCount;
  Bitmap.Height:=TileHeight;

  SetLength(fTileInfos,OwnTile.FloorCount);
  for Dummy:=0 to OwnTile.FloorCount-1 do
  begin
    OwnTile.DrawFloorCanvas(Bitmap.Canvas,Dummy*TileWidth,0,Dummy);
    fTileInfos[Dummy]:=OwnTile.TileInformation(Dummy)^;
  end;
  IncEinsatzLoadProgress;
  CreateBlendedSurface(FloorIndex);

  // Mauern Links �bernehmen
  Bitmap.Width:=WallWidth*OwnTile.WallLeftCount;
  Bitmap.Height:=WallHeight;
  SetLength(fWallInfos,OwnTile.WallLeftCount);
  for Dummy:=0 to OwnTile.WallLeftCount-1 do
  begin
    OwnTile.DrawWallLeftCanvas(Bitmap.Canvas,Dummy*WallWidth,0,Dummy);
    fWallInfos[Dummy]:=OwnTile.WallInformation(Dummy)^;
  end;
  IncEinsatzLoadProgress;
  CreateBlendedSurface(LeftIndex);

  // Mauern Rechts �bernehmen
  Bitmap.Width:=WallWidth*OwnTile.WallRightCount;
  Bitmap.Height:=WallHeight;
  for Dummy:=0 to OwnTile.WallLeftCount-1 do
  begin
    OwnTile.DrawWallRightCanvas(Bitmap.Canvas,Dummy*WallWidth,0,Dummy);
  end;
  IncEinsatzLoadProgress;
  CreateBlendedSurface(RightIndex);

  // Alles freigeben
  Bitmap.Free;
  OwnTile.Free;
  for Dummy:=0 to fUsedTilesets.Count-1 do
    Groups[Dummy].Free;
end;

procedure TDXISOTileGroup.ReadUsedTiles(Stream: TStream);

  procedure ReadTileInfoArray(Typ: TTileType);
  var
    Count: Integer;
    Dummy: Integer;

    TileSet : Integer;
    Start   : Integer;
  begin
    Stream.Read(Count,SizeOf(Count));
    Start:=length(fUsedTiles);
    SetLength(fUsedTiles,length(fUsedTiles)+Count);
    for Dummy:=0 to Count-1 do
    begin
      Stream.Read(fUsedTiles[Start].ID,sizeOf(Cardinal));
      Stream.Read(fUsedTiles[Start].TileSetIndex,SizeOf(Cardinal));
      Stream.Read(fUsedTiles[Start].Index,SizeOf(Integer));

      fUsedTiles[Start].OwnIndex:=Dummy;
      fUsedTiles[Start].Typ:=Typ;

      inc(Start);

    end;
  end;

begin
  SetLength(fUsedTiles,0);
  fUsedTileSets.Text:=ReadString(Stream);

  ReadTileInfoArray(ttFloor);
  ReadTileInfoArray(ttWall);

  LoadTiles;
end;

end.
