{********************************************************************************
*										*
* Copyright (C) 2004  X-Force Team - jim_raynor@web.de				*
*										*
* This file is part of X-Force: Fight For Destiny (X-Force).			*
*										*
* X-Force is free software; you can redistribute it and/or modify		*
* it under the terms of the GNU General Public License as published by		*
* the Free Software Foundation; either version 2 of the License, or		*
* (at your option) any later version.						*
*										*
* X-Force is distributed in the hope that it will be useful,			*
* but WITHOUT ANY WARRANTY; without even the implied warranty of		*
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the			*
* GNU General Public License for more details.					*
*										*
* You should have received a copy of the GNU General Public License		*
* along with X-Force; if not, write to the Free Software			*
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA	*
*										*
*********************************************************************************
*										*
* Minimap f�r den Bodeneinsatz							*
*										*
********************************************************************************}

{$I ../Settings.inc}

unit DXISOMiniMap;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  DXContainer, DXDraws, DXISOEngine, Blending, DirectDraw, DXInput, TraceFile,
  XForce_types, math, DirectFont, KD4Utils, GameFigure, ISOObjects, Defines;

type
  TDXISOMiniMap = class(TDXComponent)
  private
    fISOEngine   : TDXISOEngine;
    fInput       : TDXInput;
    fOnHide      : TNotifyEvent;
    fOnShow      : TNotifyEvent;
    fTemp        : TDirectDrawSurface;
    fSList       : TList;
    fDRect       : TRect;
    fMapInfo     : Array of Array of PMapTileEntry;
    fFigures     : Array of Array of PTileState;
    fYOff        : Integer;
    fXOff        : Integer;
    fHeight      : Integer;
    fWidth       : Integer;
    fInfoTexts   : Array of TMiniMapInfoText;
    fShowInfo    : boolean;
    fNeedRedraw  : Boolean;

    // Weiches Scrolling
    fXSTempo     : Integer;
    fYSTempo     : Integer;

    function GetTileRect(Point: TPoint): TRect;
    procedure CenterTo(Pos: TPoint);
    procedure SetXOff(const Value: Integer);
    procedure SetYOff(const Value: Integer);
    function GetMaxYOffSet: Integer;
    function GetMaxXOffSet: Integer;
    function GetMinXOffSet: Integer;
  protected
    function PerformFrame(Sender: TObject;Frames: Integer): boolean;
    procedure AktuMap;
    procedure ChangeView(X,Y: Integer);
    procedure ScrollBy(X,Y: Integer);

    { Infotext Funktionen }
    function AddInfoText(IText: String; IRect: TRect; IFigure: TGameFigure = nil): Integer;
    procedure DeleteAllInfoText;

    procedure RenderFloor(X,Y: Integer; Surface: TDirectDrawSurface;var Mem: TDDSurfaceDesc;XPos,YPos: Integer);
    procedure RenderWall(X,Y: Integer; Surface: TDirectDrawSurface;var Mem: TDDSurfaceDesc;XPos,YPos: Integer);
  public
    constructor Create(Page: TDXPage);override;

    procedure Redraw;override;
    procedure Draw(Surface: TDirectDrawSurface ;var Mem: TDDSurfaceDesc);override;
    procedure DrawRect(Rect: TRect;Surface: TDirectDrawSurface);
    procedure RedrawRect(DrawRect: TRect; Surface: TDirectDrawSurface; var Mem: TDDSurfaceDesc);override;
    procedure DblClick(Button: TMouseButton;x,y: Integer);override;
    procedure ChangeUnitSelection(Figure: TGameFigure);
    procedure Show;
    procedure Hide;

    property ISOEngine  : TDXISOEngine read fISOEngine write fISOEngine;
    property OnShow     : TNotifyEvent read fOnShow write fOnShow;
    property OnHide     : TNotifyEvent read fOnHide write fOnHide;
    property XOff       : Integer read fXOff write SetXOff;
    property YOff       : Integer read fYOff write SetYOff;
  end;

const
  Faktor      : Integer = 4;

  FigureRects : Array[TFigureStatus] of TRect = {fsFriendly } ((Left: 501;Top:   0;Right: 508;Bottom:   7),
                                                {fsNeutral  }  (Left: 501;Top:   7;Right: 508;Bottom:   14),
                                                {fsEnemy    }  (Left: 501;Top:  14;Right: 508;Bottom:   21),
                                                {fsObject   }  (Left:   0;Top:   0;Right:   0;Bottom:    0),
                                                {fsNone     }  (Left:   0;Top:   0;Right:   0;Bottom:    0));
  SelectedFigure : TRect = (Left: 501;Top: 28;Right: 508;Bottom: 35);

  ItemRect       : TRect = (Left: 503;Top: 23;Right: 506;Bottom: 26);

  WallTLeft      : TRect = (Left: 468;Top:  8;Right: 484;Bottom: 16);
  WallTRight     : TRect = (Left: 468;Top: 16;Right: 484;Bottom: 24);
  WallBLeft      : TRect = (Left: 468;Top: 24;Right: 484;Bottom: 32);
  WallBRight     : TRect = (Left: 468;Top: 32;Right: 484;Bottom: 40);

  DoorTLeft      : TRect = (Left: 484;Top:  8;Right: 500;Bottom: 16);
  DoorTRight     : TRect = (Left: 484;Top: 16;Right: 500;Bottom: 24);
  DoorBLeft      : TRect = (Left: 484;Top: 24;Right: 500;Bottom: 32);
  DoorBRight     : TRect = (Left: 484;Top: 32;Right: 500;Bottom: 40);

  MiniHideRect   : TRect = (Left: 468;Top:  0;Right: 484;Bottom:  8);

implementation

uses
  game_api;

var
  InfoTextID  : Integer = 0;

{ TDXISOMiniMap }

function TDXISOMiniMap.AddInfoText(IText: String; IRect: TRect;
  IFigure: TGameFigure): Integer;
var
  Dummy: Integer;
begin
  for Dummy:=0 to High(fInfoTexts) do
  begin
    with fInfoTexts[Dummy] do
    begin
      if not Draw then
      begin
        ID:=InfoTextID;
        Draw:=true;
        Text:=IText;
        Rect:=IRect;
        Figure:=IFigure;
        result:=InfoTextID;
        inc(InfoTextID);
        exit;
      end;
    end;
  end;
  Dummy:=length(fInfoTexts);
  SetLength(fInfoTexts,Dummy+5);
  with fInfoTexts[Dummy] do
  begin
    ID:=InfoTextID;
    Draw:=true;
    Text:=IText;
    Rect:=IRect;
    Figure:=IFigure;
  end;
  result:=InfoTextID;
  inc(InfoTextID);
end;

procedure TDXISOMiniMap.AktuMap;
var
  Map  : TISOMap;
  X,Y  : Integer;
begin
  Map:=fIsoEngine.ISOMap;
  fHeight:=Map.Height;
  fWidth:=Map.Width;
  SetLength(fMapInfo,fWidth,fHeight);
  SetLength(FFigures,fWidth,fHeight);
  for X:=0 to fWidth-1 do
  begin
    for Y:=0 to fHeight-1 do
    begin
      fMapInfo[X,Y]:=Map.TileEntry(X,Y);
      fFigures[X,Y]:=Map.TileState(X,Y);
    end;
  end;
  fSList:=fISOEngine.GetSurfaceList;
  fTemp:=fISOEngine.GetTempSurface;
  CenterTo(Map.TileAtPos(Width div 2,Height div 2));
  DrawRect(ClientRect,fTemp);
end;

procedure TDXISOMiniMap.CenterTo(Pos: TPoint);
var
  TileRect : TRect;
  TilePos  : TPoint;
begin
  TileRect:=GetTileRect(Pos);
  TilePos.X:=TileRect.Left+((TileRect.Right-TileRect.Left) div 2);
  TilePos.Y:=TileRect.Top+((TileRect.Bottom-TileRect.Top) div 2);
  ScrollBy(TilePos.X-(Width shr 1),TilePos.Y-(Height shr 1));
end;

procedure TDXISOMiniMap.ChangeUnitSelection(Figure: TGameFigure);
begin
  if Visible then
  begin
    DrawRect(GetTileRect(Point(Figure.XPos,Figure.YPos)),fTemp);
    Redraw;
  end;
end;

procedure TDXISOMiniMap.ChangeView(X, Y: Integer);
begin
  Container.Lock;
  fISOEngine.ISOMap.XOff:=((fXOff-X+(Width div (Faktor div 2)))*Faktor);
  fISOEngine.ISOMap.YOff:=((fYOff-Y+(Height div (Faktor*2)))*Faktor);
  Container.Unlock;
  Redraw;
end;

constructor TDXISOMiniMap.Create(Page: TDXPage);
begin
  inherited;
  fInput:=game_api_GetDirectInput;
end;

procedure TDXISOMiniMap.DblClick(Button: TMouseButton; x, y: Integer);
begin
  if Button=mbLeft then
  begin
    ChangeView(X,Y);
    Hide;
  end;
end;

procedure TDXISOMiniMap.DeleteAllInfoText;
var
  Dummy  : Integer;
begin
  for Dummy:=0 to High(fInfoTexts) do fInfoTexts[Dummy].Draw:=false;
end;

procedure TDXISOMiniMap.Draw(Surface: TDirectDrawSurface;
  var Mem: TDDSurfaceDesc);
begin
  ReDrawRect(ClientRect,Surface,Mem);
end;

procedure TDXISOMiniMap.DrawRect(Rect: TRect; Surface: TDirectDrawSurface);
var
  X,Y       : integer;
  XPos,YPos : integer;
  Mem       : TDDSurfaceDesc;
begin
  if IsRectEmpty(Rect) then
    exit;

  fDRect:=Rect;
  Surface.FillRect(Rect,0);
  Surface.Lock(Mem);
  Surface.Unlock;
  Surface.ClippingRect:=Rect;

  YPos:=0;
  Y:=0;
  while (YPos<fHeight) and (y+yoff<=Rect.Bottom+13) do
  begin
    x:=((Width div 2)-(YPos*((TileWidth div Faktor) div 2)));
    XPos:=0;
    while (XPos<fWidth) and (x+xoff<Rect.Right) do
    begin
      if (x+xoff>Rect.Left-(TileWidth div Faktor)) and (y+yoff>Rect.Top-(TileHeight div Faktor)) and
         (x+xoff<Rect.Right) and (y+yoff-(TileWidth div Faktor)<Rect.Bottom) then
      begin
        RenderFloor(XPos,YPos,Surface,Mem,X+XOff,Y+YOff);
      end;
      x:=x+((TileWidth div Faktor) div 2);
      y:=y+((TileHeight div Faktor) div 2);
      inc(XPos);
    end;
    inc(ypos);
    y:=(YPOS*((TileHeight div Faktor) div 2));
  end;
  
  YPos:=0;
  Y:=0;
  while (YPos<fHeight) and (y+yoff<=Rect.Bottom+13) do
  begin
    x:=((Width div 2)-(YPos*((TileWidth div Faktor) div 2)));
    XPos:=0;
    while (XPos<fWidth) and (x+xoff<Rect.Right) do
    begin
      if (x+xoff>Rect.Left-(TileWidth div Faktor)) and (y+yoff>Rect.Top-(TileHeight div Faktor)) and
         (x+xoff<Rect.Right) and (y+yoff-(TileWidth div Faktor)<Rect.Bottom) then
      begin
        RenderWall(XPos,YPos,Surface,Mem,X+XOff,Y+YOff);
      end;
      x:=x+((TileWidth div Faktor) div 2);
      y:=y+((TileHeight div Faktor) div 2);
      inc(XPos);
    end;
    inc(ypos);
    y:=(YPOS*((TileHeight div Faktor) div 2));
  end;
  Surface.ClearClipRect;
end;

function TDXISOMiniMap.GetMaxXOffSet: Integer;
begin
  result:=(fWidth*((TileWidth div Faktor) div 2))-round(Width*1.5)+(TileWidth div Faktor);
end;

function TDXISOMiniMap.GetMaxYOffSet: Integer;
var
  yTemp: Integer;
begin
  yTemp:=Ceil((fWidth+fHeight)/2);
  result:=(yTemp*((TileWidth div Faktor) div 2))-Height+((TileWidth div Faktor) div 2);
end;

function TDXISOMiniMap.GetMinXOffSet: Integer;
begin
  result:=-(fHeight*((TileWidth div Faktor) div 2))+(Width div ((TileHeight div Faktor) div 2))+60;
end;

function TDXISOMiniMap.GetTileRect(Point: TPoint): TRect;
begin
  result.Left:=(((Width div 2)-((((TileWidth div Faktor) div 2)*(-Point.X))+(Point.Y*((TileWidth div Faktor) div 2))))+fXOff);
  result.Right:=Result.Left+(TileWidth div Faktor);
  result.Bottom:=(((Point.X+Point.Y)*((TileWidth div Faktor) div 4))+fYOff+((TileWidth div Faktor) div 2))+1;
  result.Top:=result.Bottom-(TileHeight div Faktor);
end;

procedure TDXISOMiniMap.Hide;
begin
  Container.Lock;
  fISOEngine.AktuTempSurface;
  fISOEngine.Visible:=true;
  Visible:=false;
  Container.UnLock;
  if Assigned(fOnHide) then fOnHide(Self);
  Container.RedrawArea(fISOEngine.ClientRect,Container.Surface);
  Container.DeleteFrameFunction(PerformFrame,Self);
end;

function TDXISOMiniMap.PerformFrame(Sender: TObject;
  Frames: Integer): boolean;
var
  NeedRedraw      : boolean;
  Figure          : TGameFigure;
  NameRect        : TRect;
  TileRect        : TRect;
  TextWidth       : Integer;
  Dummy           : Integer;
begin
  result:=false;
  if (not Visible) or (Container.ActivePage<>Parent) then exit;
  NeedRedraw:=false;
  fInput.Update;
{  if isButton31 in fInput.States then
  begin
    fInput.States:=fInput.States-[isButton31];
    Hide;
    exit;
  end;}
  if (isButton30 in fInput.States) then
  begin
    if not fShowInfo then
    begin
      for Dummy:=0 to fISOEngine.Figures.Count-1 do
      begin
        Figure:=fISOEngine.Figures[Dummy];
        if fISOEngine.ISOMap.CanDrawFigure(Figure) then
        begin
          TileRect:=GetTileRect(Point(Figure.XPos,Figure.YPos));
          TextWidth:=WhiteStdFont.TextWidth(Figure.Name);
          NameRect.Left:=(TileRect.Left+4)-(TextWidth div 2);
          NameRect.Right:=(NameRect.Left+TextWidth+6);
          NameRect.Top:=TileRect.Bottom+2;
          NameRect.Bottom:=TileRect.Bottom+20;
          AddInfoText(Figure.Name,NameRect);
        end;
      end;
      fShowInfo:=true;
      NeedRedraw:=true;
    end;
  end
  else if (fShowInfo) then
  begin
    DeleteAllInfoText;
    NeedRedraw:=true;
    fShowInfo:=false;
  end;
  
  if (Container.MousePos.x<1) or (isButton24 in fInput.States) then
    fXSTempo:=max(-24,fXSTempo-2)
  else if (Container.MousePos.x>ScreenWidth-2)  or (isButton25 in fInput.States) then
    fXSTempo:=min(24,fXSTempo+2)
  else
  begin
    if fXSTempo>0 then fXSTempo:=max(0,fXSTempo-2)
    else if fXSTempo<0 then fXSTempo:=min(0,fXSTempo+2);
  end;

  if (Container.MousePos.Y<1) or (isButton26 in fInput.States) then
    fYSTempo:=max(-24,fYSTempo-2)
  else if (Container.MousePos.Y>ScreenHeight-2) or (isButton27 in fInput.States) then
    fYSTempo:=min(24,fYSTempo+2)
  else
  begin
    if fYSTempo>0 then fYSTempo:=max(0,fYSTempo-2)
    else if fYSTempo<0 then fYSTempo:=min(0,fYSTempo+2);
  end;

  if (fXSTempo<>0) or (fYSTempo<>0) then
  begin
    ScrollBy(fXSTempo,fYSTempo);
    NeedRedraw:=true;
  end;
{  if (isButton24 in fInput.States) or (Container.MousePos.X<1) then XOffSet:=-24;
  if (isButton25 in fInput.States) or (Container.MousePos.X>638) then XOffSet:=24;
  if (isButton26 in fInput.States) or (Container.MousePos.Y<1) then YOffSet:=-12;
  if (isButton27 in fInput.States) or (Container.MousePos.Y>478) then YOffSet:=12;
  if (XOffSet<>0) or (YOffSet<>0) then
  begin
    ScrollBy(XOffSet,YOffSet);
    NeedRedraw:=true;
  end;}
  if (GetKeyState(VK_LBUTTON)<0) and (IsCaptured or PtInRect(ClientRect,Container.MousePos)) then
  begin
    Container.Lock;
    ChangeView(Container.MousePos.X,Container.MousePos.Y);
    Container.UnLock;
    NeedRedraw:=true;
  end;
  if fNeedRedraw or NeedRedraw then
  begin
    Redraw;
    result:=true;
  end;
end;

procedure TDXISOMiniMap.Redraw;
begin
  if Container.IsLoadGame then
  begin
    fNeedRedraw:=true;
    exit;
  end;
  if Visible then Container.RedrawBlackControl(Self,Container.Surface);
  fNeedRedraw:=false;
end;

procedure TDXISOMiniMap.ReDrawRect(DrawRect: TRect;
  Surface: TDirectDrawSurface; var Mem: TDDSurfaceDesc);
var
  XISO  : Integer;
  YISO  : Integer;
  Dummy : Integer;
begin
  Surface.ClippingRect:=DrawRect;
  Surface.Draw(Left,Top,fTemp,false);

  // Auswahlrechteck
  XISO:=Xoff-(fISOEngine.ISOMap.xOff div Faktor)+(Width div (Faktor div 2))-(Width div (Faktor*2));
  YISO:=YOff-(fISOEngine.ISOMap.YOff div Faktor);
  Rectangle(Surface,Mem,XISO,YISO,XISO+(Width div Faktor),YISO+(Height div Faktor),bcWhite);

  // Infotexte
  for Dummy:=0 to High(fInfoTexts) do
  begin
    with fInfoTexts[Dummy] do
    begin
      if Draw then
      begin
        BlendRectangle(CorrectBottomOfRect(Rect),150,bcDisabled,Surface,Mem);
        Rectangle(Surface,Mem,Rect,bcDisabled);
        WhiteStdFont.Draw(Surface,Rect.Left+2,Rect.Top+2,Text);
      end;
    end;
  end;

  Surface.ClearClipRect;
end;

procedure TDXISOMiniMap.RenderFloor(X, Y: Integer;
  Surface: TDirectDrawSurface; var Mem: TDDSurfaceDesc; XPos,
  YPos: Integer);
var
  Alpha: Integer;
begin
//  if not OverlapRect(Rect(Xpos-1,Ypos-1,Xpos+49,YPos+29),fDRect) then exit;
  with fMapInfo[X,Y]^ do
  begin
    { Zeichnen des Bodens }
    if AlphaV.Discover then
      Alpha:=max(0,min(7,AlphaV.All-1))
    else
      Alpha:=-1;
    if Alpha>-1 then
      Surface.StretchDraw(Rect(xPos,YPos,XPos+((TileWidth div Faktor)),Ypos+((TileHeight div Faktor))),Rect(Floor*TileWidth,Alpha*TileHeight,(Floor+1)*TileWidth,(Alpha+1)*TileHeight),TDirectDrawSurface(fISOEngine.ISOMap.GetTileSurface(FloorIndex)))
    else
      Surface.Draw(xPos,YPos,MiniHideRect,TDirectDrawSurface(fSList[0]));
  end;
end;

procedure TDXISOMiniMap.RenderWall(X, Y: Integer;
  Surface: TDirectDrawSurface; var Mem: TDDSurfaceDesc; XPos,
  YPos: Integer);
var
  Alpha: Integer;
  Dummy: Integer;
  DrawS: boolean;
begin
  with fMapInfo[X,Y]^ do
  begin
    { Zeichnen der oberen rechten Mauer}
    if (TRight<>-1) and (not ISOEngine.ISOMap.TileGroup.GetWallInfos(TRight).IsEcke) then
    begin
      Alpha:=-1;
      if PTopRight<>nil then
      begin
        if PTopRight.AlphaV.Discover or AlphaV.Discover then
          Alpha:=max(0,min(7,max(PTopRight.AlphaV.All,AlphaV.All)-1));
      end
      else if AlphaV.Discover then
        Alpha:=max(0,min(7,AlphaV.All-1));

      if Alpha>-1 then
      begin
        if DoorInfo.TRightW=wbDoor then
          Surface.Draw(XPos,YPos,DoorTRight,TDirectDrawSurface(fSList[0]))
        else
          Surface.Draw(XPos,YPos,WallTRight,TDirectDrawSurface(fSList[0]));
      end;
    end;
    { Zeichnen der oberen linken Mauer}
    if (TLeft<>-1) and (not ISOEngine.ISOMap.TileGroup.GetWallInfos(TLeft).IsEcke) then
    begin
      Alpha:=-1;
      if PTopLeft<>nil then
      begin
        if PTopLeft.AlphaV.Discover or AlphaV.Discover then
          Alpha:=max(0,min(7,max(PTopLeft.AlphaV.All,AlphaV.All)-1));
      end
      else if AlphaV.Discover then
        Alpha:=max(0,min(7,AlphaV.All-1));

      if Alpha>-1 then
      begin
        if DoorInfo.TLeftW=wbDoor then
          Surface.Draw(XPos,YPos,DoorTLeft,TDirectDrawSurface(fSList[0]))
        else
          Surface.Draw(XPos,YPos,WallTLeft,TDirectDrawSurface(fSList[0]));
      end;
    end;
    { Zeichnen der unteren linken Mauer}
    if (BLeft<>-1) and (not ISOEngine.ISOMap.TileGroup.GetWallInfos(BLeft).IsEcke) then
    begin
      Alpha:=-1;
      if PBottomLeft<>nil then
      begin
        if PBottomLeft.AlphaV.Discover or AlphaV.Discover then
          Alpha:=max(0,min(7,max(PBottomLeft.AlphaV.All,AlphaV.All)-1));
      end
      else if AlphaV.Discover then
        Alpha:=max(0,min(7,AlphaV.All-1));

      if Alpha>-1 then
      begin
        if DoorInfo.BLeftW=wbDoor then
          Surface.Draw(XPos,YPos,DoorBLeft,TDirectDrawSurface(fSList[0]))
        else
          Surface.Draw(XPos,YPos,WallBLeft,TDirectDrawSurface(fSList[0]));
      end;
    end;
    { Zeichnen der unteren rechten Mauer}
    if (BRight<>-1) and (not ISOEngine.ISOMap.TileGroup.GetWallInfos(BRight).IsEcke) then
    begin
      Alpha:=-1;
      if PBottomRight<>nil then
      begin
        if PBottomRight.AlphaV.Discover or AlphaV.Discover then
          Alpha:=max(0,min(7,max(PBottomRight.AlphaV.All,AlphaV.All)-1));
      end
      else if AlphaV.Discover then
        Alpha:=max(0,min(7,AlphaV.All-1));

      if Alpha>-1 then
      begin
        if DoorInfo.BRightW=wbDoor then
          Surface.Draw(XPos,YPos,DoorBRight,TDirectDrawSurface(fSList[0]))
        else
          Surface.Draw(XPos,YPos,WallBRight,TDirectDrawSurface(fSList[0]));
      end;
    end;
  end;

  with fFigures[X,Y]^ do
  begin
    DrawS:=false;
    if (ISOEngine.ISOMap.CanDrawFigure(Figure)) and (not Figure.IsDead) then
    begin
      if (Figure.XPos=X) and (Figure.YPos=Y) then
      begin
        Surface.Draw(XPos+3,YPos,FigureRects[Figure.FigureStatus],TDirectDrawSurface(fSList[0]));
        if Figure.Selected then
          Surface.Draw(XPos+3,YPos,SelectedFigure,TDirectDrawSurface(fSList[0]));
        DrawS:=true;
      end;
    end;
    
    for Dummy:=0 to ObjectCount-1 do
    begin
      if (Objects[Dummy] is TISOItem) then
      begin
        Surface.Draw(XPos+5,YPos+2,ItemRect,TDirectDrawSurface(fSList[0]));
        if not DrawS then
          Rectangle(Surface,Mem,XPos+5,YPos+2,Xpos+9,YPos+6,bcBlack);
        break;
      end;
    end;
  end;
end;

procedure TDXISOMiniMap.ScrollBy(X, Y: Integer);
var
  OldX   : Integer;
  OldY   : Integer;
  Dummy  : Integer;

  procedure ScrollSurface;
  var
    ScrollRect: TRect;
    XRedraw   : TRect;
    YRedraw   : TRect;
  begin
    ScrollRect:=Rect(0,0,Width,Height);
    XRedraw:=Rect(0,0,0,Height);
    YRedraw:=Rect(0,0,Width,0);
    if X>0 then
    begin
      XRedraw.Left:=Width-X;
      XRedraw.Right:=Width;
    end
    else if X<0 then
    begin
      XRedraw.Left:=0;
      XRedraw.Right:=-X;
    end;
    if Y>0 then
    begin
      YRedraw.Top:=Height-Y;
      YRedraw.Bottom:=Height;
    end
    else if Y<0 then
    begin
      YRedraw.Top:=0;
      YRedraw.Bottom:=-Y;
    end;
    ScrollDC(fTemp.Canvas.Handle,-X,-Y,ScrollRect,ScrollRect,0,nil);
    fTemp.Canvas.Release;
    DrawRect(XRedraw,fTemp);
    DrawRect(YRedraw,fTemp);
  end;

begin
  OldX:=XOff;
  OldY:=YOff;
  XOff:=OldX-X;
  YOff:=OldY-Y;
  X:=OldX-XOff;
  Y:=OldY-YOff;
  if (X=0) and (Y=0) then exit;
  for Dummy:=0 to High(fInfoTexts) do
  begin
    if fInfoTexts[Dummy].Draw then
      OffsetRect(fInfoTexts[Dummy].Rect,-X,-Y);
  end;
  ScrollSurface;
end;

procedure TDXISOMiniMap.SetXOff(const Value: Integer);
begin
  fxOff := min(max(Value,-GetMaxXOffSet-Width),-GetMinXOffSet-50);
end;

procedure TDXISOMiniMap.SetYOff(const Value: Integer);
begin
  fyOff := min(max(Value,-GetMaxYOffSet),15);
end;

procedure TDXISOMiniMap.Show;
begin
  Container.Lock;
  AktuMap;
  fShowInfo:=false;
  DeleteAllInfoText;
  Container.AddFrameFunction(PerformFrame,Self,25);
  fISOEngine.Visible:=False;
  Visible:=true;
  Container.UnLock;
  if Assigned(fOnShow) then
    fOnShow(Self);
  Container.RedrawArea(fISOEngine.ClientRect,Container.Surface);
end;

end.
