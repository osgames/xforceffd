{********************************************************************************
*										*
* Copyright (C) 2004  X-Force Team - jim_raynor@web.de				*
*										*
* This file is part of X-Force: Fight For Destiny (X-Force).			*
*										*
* X-Force is free software; you can redistribute it and/or modify		*
* it under the terms of the GNU General Public License as published by		*
* the Free Software Foundation; either version 2 of the License, or		*
* (at your option) any later version.						*
*										*
* X-Force is distributed in the hope that it will be useful,			*
* but WITHOUT ANY WARRANTY; without even the implied warranty of		*
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the			*
* GNU General Public License for more details.					*
*										*
* You should have received a copy of the GNU General Public License		*
* along with X-Force; if not, write to the Free Software			*
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA	*
*										*
*********************************************************************************
*										*
* Komponente zum Ausr�sten der Raumschiffe (Waffen, Motoren, Schilde ...)	*
*										*
********************************************************************************}

{$I ../Settings.inc}

unit DXSchiffViewer;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  DXContainer, DXDraws, RaumschiffList, Blending, NGTypes, XForce_types, KD4Utils,
  TraceFile, Defines, DirectDraw, DirectFont, StringConst;

type
  TSchiffKomponent = record
    Draw        : boolean;
    Position    : TPoint;
    Rect        : TRect;
    Komponente  : THighLightKomponent;
    KompIndex   : Byte;
  end;

  TRaumschiffType = (rtNone,rtWaffen,rtShield,rtMotor,rtExtension,rtAll);

  TSchiffVorlage = record
    WZellen : Array[0..2] of TSchiffKomponent;
    Motor   : TSchiffKomponent;
    Shield  : TSchiffKomponent;
  end;


type
  TDXSchiffViewer = class(TDXComponent)
  private
    fRaumschiff       : TRaumschiff;
    fHighLight        : TSchiffZelle;
    fClickKomponent   : TClickKomponent;
    fOverKomponent    : TOverKomponent;
    fVorlagen         : Array[0..3] of TSchiffVorlage;
    fDrawRect         : TRect;
    fTopMargin        : Integer;
    fHebeVor          : TRaumschiffType;
    procedure SetRaumschiff(const Value: TRaumschiff);
    function KomponentOverPoint(Point: TPoint): TSchiffZelle;
    procedure SetTopMargin(const Value: Integer);
    procedure DrawRaumschiffe0Zelle(Surface: TDirectDrawSurface;const Mem: TDDSurfaceDesc);
    procedure DrawRaumschiffe1Zelle(Surface: TDirectDrawSurface;const Mem: TDDSurfaceDesc);
    procedure DrawRaumschiffe2Zelle(Surface: TDirectDrawSurface;const Mem: TDDSurfaceDesc);
    procedure DrawRaumschiffe3Zelle(Surface: TDirectDrawSurface;const Mem: TDDSurfaceDesc);
    procedure SetHebeVor(const Value: TRaumschiffType);
    procedure DrawExtendsSlots(Surface: TDirectDrawSurface;const Mem: TDDSurfaceDesc);
    { Private-Deklarationen }
  protected
    procedure DrawKomponent(Surface: TDirectDrawSurface;Komponent: TSchiffKomponent);
    procedure DrawHebevor(Surface: TDirectDrawSurface;const Mem: TDDSurfaceDesc;Komponent: TSchiffKomponent);
    procedure InitArray;
    procedure CreateMouseRegion;override;
    { Protected-Deklarationen }
  public
    { Public-Deklarationen }
    constructor Create(Page: TDXPage);override;
    procedure MouseMove(X,Y: Integer);override;
    procedure MouseDown(Button: TMouseButton;X,Y: Integer);override;
    procedure Draw(Surface: TDirectDrawSurface;var Mem: TDDSurfaceDesc);override;
    procedure MouseLeave;override;
    procedure ReDrawRect(DrawRect: TRect;Surface: TDirectDrawSurface;var Mem: TDDSurfaceDesc);override;
    function GetObjektRect(const Zelle: TSchiffZelle): TRect;
    property Schiff             : TRaumschiff read fRaumschiff write SetRaumschiff;
    property OnClickKomponent   : TClickKomponent read fClickKomponent write fClickKomponent;
    property OnOverKomponent    : TOverKomponent read fOverKomponent write fOverKomponent;
    property TopMargin          : Integer read fTopMargin write SetTopMargin;
    property HebeVor            : TRaumschiffType read fHebeVor write SetHebeVor;
  end;

implementation

uses
  lager_api;

{ TDXSchiffViewer }

constructor TDXSchiffViewer.Create(Page: TDXPage);
begin
  inherited;
  fRaumschiff:=nil;
  fTopMargin:=80;
  InitArray;
  fHighLight.Komponent:=hlkNone;
  fHebeVor:=rtNone;
end;

procedure TDXSchiffViewer.CreateMouseRegion;
begin
  fMouseRegion:=CreateRoundRectRgn(Left,Top,Right,Bottom,20,20);
end;

procedure TDXSchiffViewer.Draw(Surface: TDirectDrawSurface;var Mem: TDDSurfaceDesc);
begin
  RedrawRect(ClientRect,Surface,Mem);
end;

procedure TDXSchiffViewer.DrawHebevor(Surface: TDirectDrawSurface;const Mem: TDDSurfaceDesc;Komponent: TSchiffKomponent);
var
  Zeichnen : boolean;
  SX,SY    : Integer;
  TempRect : TRect;
begin
  if not (HebeVor=rtAll) then
  begin
    Zeichnen:=false;
    case Komponent.Komponente of
      hlkMotor             : if HebeVor=rtMotor then Zeichnen:=true;
      //hlkShield            : if HebeVor=rtShield then Zeichnen:=true;
      hlkWZelle            : if HebeVor=rtWaffen then Zeichnen:=true;
    end;
    if not Zeichnen then exit;
  end;
  TempRect:=Komponent.Rect;
  SX:=Left+(Width shr 1)-125;
  SY:=Top+fTopMargin;
  OffSetRect(TempRect,SX,SY);
  if Enabled then
    Frame3D(Surface,Mem,TempRect.Left,TempRect.Top,TempRect.Right,TempRect.Bottom,bcMaroon,bcRed)
  else
    Frame3D(Surface,Mem,TempRect.Left,TempRect.Top,TempRect.Right,TempRect.Bottom,bcGray,bcStdGray);
end;

procedure TDXSchiffViewer.DrawKomponent(Surface: TDirectDrawSurface;Komponent: TSchiffKomponent);
var
  SX,SY    : Integer;
  ImgPosX  : Integer;
  ImgPosY  : Integer;
begin
  if not Komponent.Draw then exit;
  SX:=Left+(Width shr 1)-125;
  SY:=Top+fTopMargin;
  ImgPosX:=SX+Komponent.Position.X;
  ImgPosY:=SY+Komponent.Position.Y;
  case Komponent.Komponente of
    hlkMotor  :
      if fRaumschiff.Motor.Installiert then
        lager_api_DrawLargeImage(IILLMotor,Surface,ImgPosX,ImgPosY)
      else
        lager_api_DrawLargeImage(IILLMotor,Surface,ImgPosX,ImgPosY,true);
    {
    hlkShield :
      if fRaumschiff.ShieldInstalliert then
        lager_api_DrawLargeImage(IILLShield,Surface,ImgPosX,ImgPosY)
      else
        lager_api_DrawLargeImage(IILLShield,Surface,ImgPosX,ImgPosY,true);
    }
    hlkWZelle:
      if fRaumschiff.WaffenZelle[Komponent.KompIndex].Installiert then
        lager_api_DrawLargeImage(TypeIDToIndex(ptRWaffe,fRaumschiff.WaffenZelle[Komponent.KompIndex].WaffType,1),Surface,ImgPosX,ImgPosY)
      else
        lager_api_DrawLargeImage(IILLRWaffeP,Surface,ImgPosX,ImgPosY,true);
  end;
end;

procedure TDXSchiffViewer.DrawRaumschiffe0Zelle(Surface: TDirectDrawSurface;const Mem: TDDSurfaceDesc);
begin
  DrawKomponent(Surface,fVorlagen[fRaumschiff.WaffenZellen].Motor);
  fRaumschiff.DrawView(Surface,Left,Top+fTopMargin,Width,Height,Rect(0,0,250,250));
//  DrawKomponent(Surface,fVorlagen[fRaumschiff.WaffenZellen].Shield);
  { Rahmen zeichnen f�r hervorgehobene Komponenten }
  DrawHebevor(Surface,Mem,fVorlagen[fRaumschiff.WaffenZellen].Motor);
//  DrawHebevor(Surface,Mem,fVorlagen[fRaumschiff.WaffenZellen].Shield);
end;

procedure TDXSchiffViewer.DrawRaumschiffe1Zelle(Surface: TDirectDrawSurface;const Mem: TDDSurfaceDesc);
begin
  DrawKomponent(Surface,fVorlagen[fRaumschiff.WaffenZellen].Motor);
  DrawKomponent(Surface,fVorlagen[fRaumschiff.WaffenZellen].WZellen[0]);
  fRaumschiff.DrawView(Surface,Left,Top+fTopMargin,Width,Height,Rect(0,0,250,250));
//  DrawKomponent(Surface,fVorlagen[fRaumschiff.WaffenZellen].Shield);
  { Rahmen zeichnen f�r hervorgehobene Komponenten }
  DrawHebevor(Surface,Mem,fVorlagen[fRaumschiff.WaffenZellen].Motor);
//  DrawHebevor(Surface,Mem,fVorlagen[fRaumschiff.WaffenZellen].Shield);
  DrawHebevor(Surface,Mem,fVorlagen[fRaumschiff.WaffenZellen].WZellen[0]);
end;

procedure TDXSchiffViewer.DrawRaumschiffe2Zelle(Surface: TDirectDrawSurface;const Mem: TDDSurfaceDesc);
begin
  DrawKomponent(Surface,fVorlagen[fRaumschiff.WaffenZellen].Motor);
  fRaumschiff.DrawView(Surface,Left,Top+fTopMargin,Width,Height,Rect(0,0,250,250));
  DrawKomponent(Surface,fVorlagen[fRaumschiff.WaffenZellen].WZellen[0]);
  DrawKomponent(Surface,fVorlagen[fRaumschiff.WaffenZellen].WZellen[1]);
  fRaumschiff.DrawView(Surface,Left,Top+fTopMargin,Width,Height,Rect(78,137,163,172));
//  DrawKomponent(Surface,fVorlagen[fRaumschiff.WaffenZellen].Shield);
  { Rahmen zeichnen f�r hervorgehobene Komponenten }
  DrawHebevor(Surface,Mem,fVorlagen[fRaumschiff.WaffenZellen].Motor);
//  DrawHebevor(Surface,Mem,fVorlagen[fRaumschiff.WaffenZellen].Shield);
  DrawHebevor(Surface,Mem,fVorlagen[fRaumschiff.WaffenZellen].WZellen[0]);
  DrawHebevor(Surface,Mem,fVorlagen[fRaumschiff.WaffenZellen].WZellen[1]);
end;

procedure TDXSchiffViewer.DrawRaumschiffe3Zelle(Surface: TDirectDrawSurface;const Mem: TDDSurfaceDesc);
begin
  DrawKomponent(Surface,fVorlagen[fRaumschiff.WaffenZellen].WZellen[0]);
  DrawKomponent(Surface,fVorlagen[fRaumschiff.WaffenZellen].WZellen[1]);
  DrawKomponent(Surface,fVorlagen[fRaumschiff.WaffenZellen].Motor);
  fRaumschiff.DrawView(Surface,Left,Top+fTopMargin,Width,Height,Rect(0,0,250,250));
  DrawKomponent(Surface,fVorlagen[fRaumschiff.WaffenZellen].WZellen[2]);
  fRaumschiff.DrawView(Surface,Left,Top+fTopMargin,Width,Height,Rect(100,190,142,212));
//  DrawKomponent(Surface,fVorlagen[fRaumschiff.WaffenZellen].Shield);
  { Rahmen zeichnen f�r hervorgehobene Komponenten }
  DrawHebevor(Surface,Mem,fVorlagen[fRaumschiff.WaffenZellen].Motor);
//  DrawHebevor(Surface,Mem,fVorlagen[fRaumschiff.WaffenZellen].Shield);
  DrawHebevor(Surface,Mem,fVorlagen[fRaumschiff.WaffenZellen].WZellen[0]);
  DrawHebevor(Surface,Mem,fVorlagen[fRaumschiff.WaffenZellen].WZellen[1]);
  DrawHebevor(Surface,Mem,fVorlagen[fRaumschiff.WaffenZellen].WZellen[2]);
end;

function TDXSchiffViewer.GetObjektRect(const Zelle: TSchiffZelle): TRect;
begin
  result:=Rect(0,0,0,0);
  case Zelle.Komponent of
    hlkMotor  : result:=fVorlagen[fRaumschiff.WaffenZellen].Motor.Rect;
    hlkWZelle : result:=fVorlagen[fRaumschiff.WaffenZellen].WZellen[Zelle.KompIndex].Rect;
    //hlkShield : result:=fVorlagen[fRaumschiff.WaffenZellen].Shield.Rect;
    hlkESlot  :
    begin
      result:=Rect(Right-47,Top+30+(Zelle.KompIndex*40),Right-11,Top+66+(Zelle.KompIndex*40));
      exit;
    end;
    hlkNone   : exit;
  end;
  OffsetRect(result,Left+(Width shr 1)-125,Top+fTopMargin);
end;

procedure TDXSchiffViewer.InitArray;
var
  Dummy  : Integer;
  ZelZae : Integer;
begin
  for Dummy:=0 to 3 do
  begin
    with fVorlagen[Dummy] do
    begin
      for ZelZae:=0 to 2 do
      begin
        WZellen[ZelZae].Draw:=Dummy>ZelZae;
        WZellen[ZelZae].KompIndex:=ZelZae;
        WZellen[ZelZae].Komponente:=hlkWZelle;
      end;
      Motor.Draw:=true;
//      Shield.Draw:=true;
      Motor.Komponente:=hlkMotor;
//      Shield.Komponente:=hlkShield;
      Motor.KompIndex:=0;
//      Shield.KompIndex:=0;
    end;
  end;
  { Abweichungen f�r Transporter }
//  fVorlagen[0].Shield.Position:=Point(95,100);
  fVorlagen[0].Motor.Position:=Point(95,200);
  fVorlagen[1].Motor.Position:=Point(101,218);
//  fVorlagen[1].Shield.Position:=Point(101,175);
  { 1 Waffezelle }
  fVorlagen[1].WZellen[0].Position:=Point(102,105);
  { 2 Waffezellen }
  fVorlagen[2].WZellen[0].Position:=Point(80,110);
  fVorlagen[2].WZellen[1].Position:=Point(130,110);
//  fVorlagen[2].Shield.Position:=Point(102,176);
  fVorlagen[2].Motor.Position:=Point(102,210);
  { 3 Waffezelle }
  fVorlagen[3].WZellen[0].Position:=Point(50,135);
  fVorlagen[3].WZellen[1].Position:=Point(160,135);
  fVorlagen[3].WZellen[2].Position:=Point(105,150);
  fVorlagen[3].Motor.Position:=Point(103,223);
//  fVorlagen[3].Shield.Position:=Point(103,120);
  for Dummy:=0 to 3 do
  begin
    with fVorlagen[Dummy] do
    begin
      Motor.Rect:=Rect(Motor.Position.x,Motor.Position.y,Motor.Position.x+39,Motor.Position.Y+39);
//      Shield.Rect:=Rect(Shield.Position.x,Shield.Position.y,Shield.Position.x+39,Shield.Position.Y+29);
      for ZelZae:=0 to 2 do
      begin
        with WZellen[ZelZae] do
          Rect:=Classes.Rect(Position.X,Position.y,Position.x+35,Position.Y+62);
      end;
    end;
  end;
end;

function TDXSchiffViewer.KomponentOverPoint(Point: TPoint): TSchiffZelle;
var
  SX,SY : Integer;
  Dummy : Integer;

  function CheckKomponent(Komponent: TSchiffKomponent): boolean;
  var
    TempRect: TRect;
  begin
    if not Komponent.Draw then
    begin
      result:=false;
      exit;
    end;
    TempRect:=Komponent.Rect;
    OffsetRect(TempRect,SX,SY);
    if PtInRect(TempRect,Point) then
    begin
      KomponentOverPoint.Komponent:=Komponent.Komponente;
      KomponentOverPoint.KompIndex:=Komponent.KompIndex;
      result:=true;
    end
    else
      result:=false;
  end;

begin
  SX:=(Width shr 1)-125;
  SY:=fTopMargin;
  result.Komponent:=hlkNone;
  result.KompIndex:=0;
  if CheckKomponent(fVorlagen[fRaumschiff.WaffenZellen].Motor) then exit;
//  if CheckKomponent(fVorlagen[fRaumschiff.WaffenZellen].Shield) then exit;
  for Dummy:=0 to 2 do
  begin
    if CheckKomponent(fVorlagen[fRaumschiff.WaffenZellen].WZellen[Dummy]) then exit;
  end;
  for Dummy:=0 to fRaumschiff.ExtendsSlots-1 do
  begin
    if PtInRect(Rect(Width-47,30+(Dummy*40),Width-11,66+(Dummy*40)),Point) then
    begin
      Result.Komponent:=hlkESlot;
      Result.KompIndex:=Dummy;
      exit;
    end;
  end;
end;

procedure TDXSchiffViewer.MouseLeave;
begin
  inherited;
  if fHighLight.Komponent<>hlkNone then
  begin
    fHighLight.Komponent:=hlkNone;
    Redraw;
    if Assigned(fOverKomponent) then fOverKomponent(Self,fHighLight);
  end;
end;

procedure TDXSchiffViewer.MouseMove(X, Y: Integer);
var
  Old         : TSchiffZelle;
begin
  Old:=fHighLight;
  fHighLight:=KomponentOverPoint(Point(X,Y));
  if (fHighLight.Komponent<>Old.Komponent) or ((fHighLight.Komponent in [hlkWZelle,hlkESlot]) and (fHighLight.KompIndex<>Old.KompIndex)) then
  begin
    if Assigned(fOverKomponent) then fOverKomponent(Self,fHighLight);
  end;
end;

procedure TDXSchiffViewer.MouseDown(Button: TMouseButton; X, Y: Integer);
begin
  if fHighLight.Komponent<>hlkNone then
    if Assigned(fClickKomponent) then fClickKomponent(Self,Button,fHighLight);
  inherited;
end;

procedure TDXSchiffViewer.ReDrawRect(DrawRect: TRect; Surface: TDirectDrawSurface;
    var Mem: TDDSurfaceDesc);
var
  Text: String;
begin
  fDrawRect:=DrawRect;
  if AlphaElements then
  begin
    BlendRectangle(Rect(Left,Top,Right,Top+22),100,bcMaroon,Surface,Mem);
  end;
  FramingRect(Surface,Mem,ClientRect,[cRightBottom],11,bcMaroon);
  HLine(Surface,Mem,Left+1,Right-1,Top+22,bcMaroon);

  if fRaumschiff=nil then
    exit;

  case fRaumschiff.WaffenZellen of
    0 : DrawRaumschiffe0Zelle(Surface,Mem);
    1 : DrawRaumschiffe1Zelle(Surface,Mem);
    2 : DrawRaumschiffe2Zelle(Surface,Mem);
    3 : DrawRaumschiffe3Zelle(Surface,Mem);
  end;
  DrawExtendsSlots(Surface,Mem);
  WhiteBStdFont.Draw(Surface,Left+8,Top+5,fRaumschiff.Model.Name);
  YellowStdFont.Draw(Surface,Left+8,Top+25,IAHitPoints);
  Text:=Format(FRoom,[fRaumschiff.AktHitPoints/1,fRaumschiff.HitPoints/1]);
  YellowStdFont.Draw(Surface,Left+128-YellowStdFont.TextWidth(Text),Top+25,Text);
  Text:=ZahlString(FWaffenZelle,FWaffenZellen,fRaumschiff.WaffenZellen);
  YellowStdFont.Draw(Surface,Right-8-YellowStdFont.TextWidth(Text),Top+5,Text);
  if fRaumschiff.WaffenZellen>0 then
  begin
    YellowStdFont.Draw(Surface,Left+8,Top+45,IITreffer);
    Text:=IntToStr(fRaumschiff.Abschuesse);
    YellowStdFont.Draw(Surface,Left+128-YellowStdFont.TextWidth(Text),Top+45,Text);
  end;
end;

procedure TDXSchiffViewer.SetHebeVor(const Value: TRaumschiffType);
begin
  if fHebeVor=Value then exit;
  fHebeVor := Value;
  Redraw;
end;

procedure TDXSchiffViewer.SetRaumschiff(const Value: TRaumschiff);
begin
  fRaumschiff := Value;

  Enabled:=(fRaumschiff<>nil) and (sabEquipt in fRaumschiff.Abilities);

  Redraw;
end;

procedure TDXSchiffViewer.SetTopMargin(const Value: Integer);
begin
  fTopMargin := Value;
  Redraw;
end;

procedure TDXSchiffViewer.DrawExtendsSlots(Surface: TDirectDrawSurface;
  const Mem: TDDSurfaceDesc);
var
  Dummy : Integer;
  Color : TBlendColor;

  procedure DrawSlot(Index: Integer;Rect: TRect);
  var
    Image: Integer;
  begin
    if AlphaElements then
      BlendRectangle(ResizeRect(Rect,1),100,Color,Surface,Mem);

    Rectangle(Surface,Mem,Rect,Color);
    if fRaumschiff.ExtendZelle[Index].Installiert then
    begin
      Image:=lager_api_GetItem(fRaumschiff.GetExtendIndex(Index)).ImageIndex;
      lager_api_DrawItem(Image,Surface,Rect.Left+2,Rect.Top+2);
    end;
  end;

begin
  if not Enabled then
    Color:=bcStdGray
  else if fHebeVor=rtExtension then
    Color:=bcRed
  else
    Color:=bcMaroon;
  for Dummy:=0 to fRaumschiff.ExtendsSlots-1 do
  begin
    DrawSlot(Dummy,Rect(Right-47,Top+30+(Dummy*40),Right-11,Top+66+(Dummy*40)));
  end;
end;

end.
