{********************************************************************************
*										*
* Copyright (C) 2004  X-Force Team - jim_raynor@web.de				*
*										*
* This file is part of X-Force: Fight For Destiny (X-Force).			*
*										*
* X-Force is free software; you can redistribute it and/or modify		*
* it under the terms of the GNU General Public License as published by		*
* the Free Software Foundation; either version 2 of the License, or		*
* (at your option) any later version.						*
*										*
* X-Force is distributed in the hope that it will be useful,			*
* but WITHOUT ANY WARRANTY; without even the implied warranty of		*
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the			*
* GNU General Public License for more details.					*
*										*
* You should have received a copy of the GNU General Public License		*
* along with X-Force; if not, write to the Free Software			*
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA	*
*										*
*********************************************************************************
*										*
* basis_api bildet die Schnittstelle zu einer Basisliste, die bis zu 8 Basen	*
* verwaltet									*
*										*
* WICHTIG!! Bevor die Funktionen genutzt werden k�nnen muss �ber 		*
*           basis_api_init die Basisliste gesetzt werden. Sollte durch 		*
*           savegame_api_init geschehen sein.					*
*										*
********************************************************************************}

{$I ../Settings.inc}

unit DXFrameBox;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  DXDraws, DXContainer, Blending, XForce_types, TraceFile, NGTypes, DXClass,
  DXGroupCaption, KD4Utils, DirectDraw, DirectFont;

type
  TDXFrameBox = class(TDXComponent)
  private
    fColor         : TColor;
    fBlend         : boolean;
    fBlendColor    : TBlendColor;
    fCorners       : TRoundCorners;
    fCorner        : TCorners;
    fCaption       : String;
    fDrawCaption   : boolean;
    fOnDrawCaption: TDXDrawHeadRowEvent;
    procedure SetColor(const Value: TColor);
    procedure SetBlend(const Value: boolean);
    procedure SetBlendColor(const Value: TBlendColor);
    procedure SetCorners(const Value: TRoundCorners);
    function GetFrameRect: TRect;
    procedure SetCaption(const Value: String);
    procedure SetDrawCaption(const Value: TDXDrawHeadRowEvent);
    { Private-Deklarationen }
  protected
    procedure CreateMouseRegion;override;
    { Protected-Deklarationen }
  public
    constructor Create(Page: TDXPage);override;
    procedure ReDrawRect(DrawRect: TRect;Surface: TDirectDrawSurface;var Mem: TDDSurfaceDesc);override;
    procedure Draw(Surface: TDirectDrawSurface;var Mem: TDDSurfaceDesc);override;
    property BorderColor  : TColor write SetColor;
    property Blending     : boolean read fBlend write SetBlend;
    property BlendColor   : TBlendColor read fBlendColor write SetBlendColor;
    property RoundCorners : TRoundCorners read fCorners write SetCorners;
    property Caption      : String read fCaption write SetCaption;
    property DrawCaption  : TDXDrawHeadRowEvent read fOnDrawCaption write SetDrawCaption;
  end;

implementation

{ TDXFrameBox }

constructor TDXFrameBox.Create(Page: TDXPage);
begin
  inherited;
  RoundCorners:=rcAll;
  fCaption:='';
  fDrawCaption:=false;
  fBlendColor:=bcMaroon;
end;

procedure TDXFrameBox.CreateMouseRegion;
var
  FrameRect: TRect;
  RoundRGN : HRGN;
begin
  FrameRect:=GetFrameRect;
  RoundRGN:=CreateRoundRectRgn(FrameRect.Left,FrameRect.Top,FrameRect.Right+1,FrameRect.Bottom+1,5,5);
  fMouseRegion:=CreateRectRgn(Left,Top,Right,Bottom);
  CombineRgn(fMouseRegion,fMouseRegion,RoundRGN,RGN_AND);
  DeleteObject(RoundRGN);
end;

procedure TDXFrameBox.Draw(Surface: TDirectDrawSurface;var Mem: TDDSurfaceDesc);
begin
  RedrawRect(ClientRect,Surface,Mem);
end;

function TDXFrameBox.GetFrameRect: TRect;
begin
  result:=ClientRect;
  if fCorners in [rcLeft,rcLeftTop,rcLeftBottom,rcNone] then inc(result.Right,20);
  if fCorners in [rcTop,rcLeftTop,rcRightTop,rcNone] then inc(result.Bottom,20);
  if fCorners in [rcRight,rcRightTop,rcRightBottom,rcNone] then dec(result.Left,20);
  if fCorners in [rcBottom,rcLeftBottom,rcRightBottom,rcNone] then dec(result.Top,20);
end;

procedure TDXFrameBox.ReDrawRect(DrawRect: TRect; Surface: TDirectDrawSurface;
    var Mem: TDDSurfaceDesc);
var
  TempCorner: TCorners;
begin
  IntersectRect(DrawRect,DrawRect,Rect(Left,Top,Right,Top+20));
//  Canvas:=Surface.Canvas;
//  Canvas.Pen.Color:=fColor;
//  Canvas.Brush.Style:=bsClear;
  if fDrawCaption then
  begin
    if not IsRectEmpty(DrawRect) then
    begin
      TempCorner:=fCorner;
      Exclude(TempCorner,cLeftBottom);
      Exclude(TempCorner,cRightBottom);
      if AlphaElements then
        BlendRoundRect(DrawRect,150,fBlendColor,Surface,Mem,11,TempCorner,DrawRect);
      HLine(Surface,Mem,Left,Right,Top+20,fColor);
      if not Assigned(fOnDrawCaption) then
        WhiteBStdFont.Draw(Surface,Left+6,Top+3,fCaption)
      else
        fOnDrawCaption(Self,Surface,Rect(Left,Top,Right,Top+20));
    end;
  end
  else
  begin
    if Blending and AlphaElements then
      BlendRoundRect(ClientRect,100,fBlendColor,Surface,Mem,11,fCorner,ClientRect);
  end;
  FramingRect(Surface,Mem,ClientRect,fCorner,11,fColor);
//  TempRect:=GetFrameRect;
//  Region:=CreateRoundRectRgn(TempRect.Left,TempRect.Top,TempRect.Right+1,TempRect.Bottom+1,20,20);
//  FrameRegion(Region,ClientRect,fColor,Surface);
//  DeleteObject(Region);
end;

procedure TDXFrameBox.SetBlend(const Value: boolean);
begin
  fBlend := Value;
  Redraw;
end;

procedure TDXFrameBox.SetBlendColor(const Value: TBlendColor);
begin
  fBlendColor := Value;
  Redraw;
end;

procedure TDXFrameBox.SetCaption(const Value: String);
begin
  fCaption := Value;
  fDrawCaption:=Value<>'';
end;

procedure TDXFrameBox.SetColor(const Value: TColor);
begin

  fColor := Container.Surface.ColorMatch(Value);
  Redraw;
end;

procedure TDXFrameBox.SetCorners(const Value: TRoundCorners);
begin
  fCorners := Value;
  fCorner:=[];
  if fCorners in [rcLeft,rcTop,rcLeftTop,rcAll] then include(fCorner,cLeftTop);
  if fCorners in [rcRight,rcTop,rcRightTop,rcAll] then include(fCorner,cRightTop);
  if fCorners in [rcLeft,rcBottom,rcLeftBottom,rcAll] then include(fCorner,cLeftBottom);
  if fCorners in [rcRight,rcBottom,rcRightBottom,rcAll] then include(fCorner,cRightBottom);
  RecreateMouseRegion;
  Redraw;
end;

procedure TDXFrameBox.SetDrawCaption(const Value: TDXDrawHeadRowEvent);
begin
  fOnDrawCaption := Value;
  fDrawCaption:=Assigned(fOnDrawCaption);
end;

end.
