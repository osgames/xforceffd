{********************************************************************************
*										*
* Copyright (C) 2004  X-Force Team - jim_raynor@web.de				*
*										*
* This file is part of X-Force: Fight For Destiny (X-Force).			*
*										*
* X-Force is free software; you can redistribute it and/or modify		*
* it under the terms of the GNU General Public License as published by		*
* the Free Software Foundation; either version 2 of the License, or		*
* (at your option) any later version.						*
*										*
* X-Force is distributed in the hope that it will be useful,			*
* but WITHOUT ANY WARRANTY; without even the implied warranty of		*
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the			*
* GNU General Public License for more details.					*
*										*
* You should have received a copy of the GNU General Public License		*
* along with X-Force; if not, write to the Free Software			*
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA	*
*										*
*********************************************************************************
*										*
* Alles was zum Raumschiffkampf geh�rt						*
*										*
********************************************************************************}

{$I ../Settings.inc}

unit DXUFOKampf;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  DXContainer, DXDraws, DXInput, DXClass, Blending,DirectDraw,DirectFont,
  RaumschiffList,UFOList, KD4Utils, XForce_types, TraceFile,math,DIB, Defines,
  AGFUnit, ArchivFile, Direct3D, AGFImageList, ColorBars, StringConst;

const
  Size            : Integer = 50;
  HalfSize        : Integer = 25;
  SurSize         : Integer = 70;
  HalfSurSize     : Integer = 35;
  FrameTime       : Integer = 25;
  ShieldWidth     : Integer = 100;
  ShieldHeight    : Integer = 100;
  ShieldImages    : Integer = 31;
  ExploSize       : Integer = 100;
  ExploFrames     : Integer = 90;
  LastFrame       : Integer = 42;
  ClipStarRect    : TRect   = (Left: -319;Top: -239;Right: 319;Bottom: 239);

var
  KRLeft          : String;
  KRRight         : String;
  KSchub          : String;
  KMLeft          : String;
  KMRight         : String;
  KZelle1         : String;
  KZelle2         : String;
  KZelle3         : String;

type
  TStar          = record
    XPos         : Double;
    YPos         : Double;
    ZOrder       : Double;
    Farbe        : Integer;
    Moving       : boolean;
    XMove        : double;
    YMove        : double;
    Pulsar       : boolean;
    FromFar      : Integer;
    ToFar        : Integer;
    Increase     : boolean;
  end;

  TWaffInfo = record
    Munition     : Integer;
    MaxMuni      : Integer;
    Remain       : Integer;
    Strength     : Integer;
    LadeZeit     : Integer;
    NachLadeZeit : Integer;
    WaffTyp      : TWaffenType;
    LaserWaffe   : boolean;
    Reichweite   : Integer;
  end;

  TWaffenZellenInfo = record
    Angle             : Integer;
    RadiusFromMiddle  : Integer;
    MinWinkel         : Integer;
    MaxWinkel         : Integer;
    Waffe             : TWaffenZelle;
  end;

  TSchussStatistik = record
    Abgegeben : Integer;
    Getroffen : Integer;
  end;

  TGesamtStatistik = record
    UFO       : TSchussStatistik;
    WZellen   : Integer;
    Gesamt    : TSchussStatistik;
    Zellen    : Array[1..3] of TSchussStatistik;
  end;

type
  TDXUFOKampf   = class;
  TAnimSprite   = class;
  TWaffenSprite = class;
  TSchiffSprite = class;
  TSchiffTyp    = (eoUFO,eoRaumschiff);

  TTrefferEvent       = procedure(Sender: TWaffenSprite;var Destroy: boolean) of object;
  TOnShootEvent       = procedure(Sender: TSchiffSprite;Zelle : Integer) of object;

  TSchiffSprite = class(TObject)
  private
    fDrawRect     : TRect;
    fX,fY         : double;
    fAngle        : double;
    fAngleBes     : Integer;
    fUFOKampf     : TDXUFOKampf;
    fBeschl       : Integer;
    fDueBeschl    : Integer;
    fZiel         : boolean;
    fMaxBeschl    : Integer;
    fDuesen       : boolean;
    FrameBox      : TRect;
    fDestroyed    : boolean;
    fVisible      : boolean;
    fSchaden      : Integer;
    fTyp          : TSchiffTyp;
    fOnTreffer    : TTrefferEvent;
    Source        : TDirectDrawSurface;
    Prepared      : TDirectDrawSurface;
    fZoom         : double;
    fAlpha        : Integer;
    fOnShoot      : TOnShootEvent;
    fZellen       : Array of TWaffenZellenInfo;
    fEnemy        : TSchiffSprite;
    procedure CalcFrameBox;
    procedure SetX(const Value: double);
    procedure SetY(const Value: double);
    procedure CalcPrepared;
    function GetPos: TFloatPoint;
    function GetX: double;
    function GetY: double;
    function GetWinkel: Integer;
    procedure SetZoom(const Value: double);
  public
    constructor Create(UFOKampf: TDXUFOKampf);
    procedure Draw(Surface: TDirectDrawSurface;const Mem: TDDSurfaceDesc; Left,Top: Integer);
    procedure SetBitmapRect(Surface: TDirectDrawSurface;Rect: TRect);
    procedure DoUFOShoot;
    procedure RotateLeft;
    procedure RotateRight;
    procedure MoveLeft;
    procedure MoveRight;
    procedure Schub;
    procedure NoSchub;
    procedure NoRotate;
    procedure NoMove;
    procedure SetStartPos(X,Y,Angle: Integer);
    procedure DoTreffer(Sender: TWaffenSprite;var Destroy: boolean);
    procedure Restore;
    procedure OffSet(X,Y: Integer);
    procedure DoShoot(Angle: Integer; Zelle: Integer);
    procedure CalcNeededOffSet(var OffX,OffY: Integer);
    function DoCollisonDetect(X,Y: Integer): boolean;

    procedure GenerateSchiffBitmap(Target: TPoint; Dest: TDirectDrawSurface; DestRect: TRect);

    procedure AddWaffenZellenInfo(Zellen: TWaffenZellenInfo);
    procedure ClearWaffenZellen;

    property Winkel         : Integer read GetWinkel;
    property AlphaValue     : Integer read fAlpha write fAlpha;
    property DrawZielPunkt  : boolean read fZiel write fZiel;
    property MaxBeschl      : Integer read fMaxBeschl write fMaxBeschl;
    property X              : double read GetX write SetX;
    property Y              : double read GetY write SetY;
    property Duesen         : boolean read fDuesen write fDuesen;
    property Pos            : TFloatPoint read GetPos;
    property Destroyed      : boolean read fDestroyed write fDestroyed;
    property Visible        : boolean read fVisible write fVisible;
    property Schaden        : Integer read fSchaden write fSchaden;
    property Typ            : TSchiffTyp read fTyp write fTyp;
    property BoundingBox    : TRect read FrameBox;
    property Zoom           : double read fZoom write SetZoom;
    property OnTreffer      : TTrefferEvent read fOnTreffer write fOnTreffer;
    property OnShoot        : TOnShootEvent read fOnShoot write fOnShoot;
    property Enemy          : TSchiffSprite read fEnemy write fEnemy;
  end;

  TMoveArt = record
    Schub  : boolean;
    Rotate : boolean;
    Move   : boolean;
  end;

  TFrameChange        = procedure(Sender: TAnimSprite;Frame: Integer) of object;

  TSchiffKI = class(TObject)
  private
    fSchiff          : TSchiffSprite;
    fEnemy           : TSchiffSprite;
    fMoveArt         : TMoveArt;
    fAusWeichCounter : Integer;
    fPause           : Integer;
    fLeft            : boolean;
    fManover         : TThreadMethod;
    Count            : Integer;
    procedure Rotate(Angle: Integer);
    procedure GibSchub;
    procedure Rotiere(Links: boolean);
    procedure Move(Links: boolean);
    procedure InitMoveArt;
    procedure Attack(Weite: Integer);
    procedure DoMoveArt;
    procedure CorrectAngle;
    procedure SearchManover;
    procedure AbstandGewinnen;
    { Man�verprozeduren }
    procedure SelbstMord;
    procedure Ausweichen;
    procedure Flucht;
  public
    constructor Create(Schiff: TSchiffSprite;Enemy: TSchiffSprite);
    procedure DoRound;
    procedure Reset;
    property Schiff  : TSchiffSprite read fSchiff write fSchiff;
    property Enemy   : TSchiffSprite read fEnemy write fEnemy;
  end;

  TAnimSprite   = class(TObject)
  private
    fUFOKampf    : TDXUFOKampf;
    fCount       : Integer;
    fImage       : Integer;
    fAngle       : Integer;
    fY           : double;
    fX           : double;
    fAnimTime    : Integer;
    fNextFrame   : Integer;
    fOnChange    : TFrameChange;
    fData        : Integer;
    fZoom        : double;
    fSource      : TAGFImage;
    procedure SetAnimTime(const Value: Integer);
    function GetX: Integer;
    function GetY: Integer;
    procedure SetX(const Value: Integer);
    procedure SetY(const Value: Integer);
  protected
    procedure SetZoom(const Value: double);virtual;
    procedure DoNextFrame;virtual;
    procedure AnimationSet;virtual; // wird aufgerufen, nachdem SetBitmap durchgef�hrt wurde
  public
    constructor Create(DXDraw: TDXDraw; UFOKampf: TDXUFOKampf);virtual;
    destructor destroy;override;
    procedure Draw(Surface: TDirectDrawSurface;Left,Top: Integer);virtual;
    procedure SetBitmap(Source: TAGFImage; Images: Integer; Zoom: double);virtual;
    procedure OffSet(X,Y: Integer);virtual;
    function Timer(Lag: Integer): boolean;virtual;

    property X        : Integer read GetX write SetX;
    property Y        : Integer read GetY write SetY;
    property Angle    : Integer read fAngle write fAngle;
    property AnimTime : Integer read fAnimTime write SetAnimTime;
    property Frame    : Integer read fImage;
    property OnChange : TFrameChange read fOnChange write fOnChange;
    property Data     : Integer read fData write fData;
    property Zoom     : double read fZoom write SetZoom;

    property Source   : TAGFImage read fSource write fSource;
  end;

  TShieldSprite = class(TAnimSprite)
  private
    fBlendColor  : TBlendColor;
    fAlpha       : Integer;
    fFollowSprite: TSchiffSprite;
    fFrame       : Integer;
  protected
    procedure SetZoom(const Value: double);override;
    procedure DoNextFrame;override;
  public
    constructor Create(DXDraw: TDXDraw; UFOKampf: TDXUFOKampf);override;
    procedure Draw(Surface: TDirectDrawSurface;Left,Top: Integer);override;
    property Color        : TBlendColor read fBlendColor write fBlendColor;
    property FollowSprite : TSchiffSprite read fFollowSprite write fFollowSprite;
  end;

  TExplosionSprite = class(TAnimSprite)
  private
    fAlpha    : double;  // Alpha Value der Explosion
    fAutoZoom : Boolean;
  protected
    procedure SetZoom(const Value: double);override;
    procedure DoNextFrame;override;
  public
    constructor Create(DXDraw: TDXDraw; UFOKampf: TDXUFOKampf);override;
    destructor destroy;override;
    procedure Draw(Surface: TDirectDrawSurface;Left,Top: Integer);override;
    procedure SetZoomedSurface(Surface: TDirectDrawSurface);
  end;

  TWaffenSprite = class(TObject)
  private
    fUFOKampf    : TDXUFOKampf;
    fSchiff      : TSchiffSprite;
    fStrength    : Integer;
    fVerfolgung  : boolean;
    fZiel        : TSchiffSprite;
    fWaffType    : TWaffenType;
    fZelle       : Integer;
    fZoom        : double;
    fReichweite  : double;
    function GetWinkel: Integer;
    procedure SetWinkel(const Value: Integer);
    function GetX: Integer;
    function GetY: Integer;
    function GetPos: TFloatPoint;
  protected
    fBeschl      : double;
    fMaxGeschw   : Integer;
    fAngle       : double;
    fX,fY        : double;
    procedure SetX(const Value: Integer);virtual;
    procedure SetY(const Value: Integer);virtual;
    procedure SetMaxGeschw(const Value: Integer);virtual;
    procedure IncBeschleunigung;virtual;
    procedure SetZoom(const Value: double);virtual;
  public
    constructor Create(DXDraw: TDXDraw; UFOKampf: TDXUFOKampf);virtual;
    destructor destroy;override;
    procedure Draw(Surface: TDirectDrawSurface;const Mem: TDDSurfaceDesc; Left,Top: Integer);virtual;
    procedure GenerateBitmap;virtual;
    procedure OffSet(X,Y: Integer);virtual;
    function DoMove: boolean;virtual;
    property Winkel     : Integer read GetWinkel write SetWinkel;
    property X          : Integer read GetX write SetX;
    property Y          : Integer read GetY write SetY;
    property PPS        : Integer read fMaxGeschw write SetMaxGeschw;
    property Schiff     : TSchiffSprite read fSchiff write fSchiff;
    property Strength   : Integer read fStrength write fStrength;
    property Verfolgung : boolean read fVerfolgung write fVerfolgung;
    property Ziel       : TSchiffSprite read fZiel write fZiel;
    property WaffType   : TWaffenType read fWaffType write fWaffType;
    property WZelle     : Integer read fZelle write fZelle;
    property Zoom       : double read fZoom write SetZoom;
    property Reichweite : double read fReichweite write fReichweite;
    property Pos        : TFloatPoint read GetPos;
  end;

  TLaserSprite = class(TWaffenSprite)
  private
    fSchweifLength : Integer;
    fFrom          : TFloatPoint;
  protected
    procedure SetMaxGeschw(const Value: Integer);override;
    procedure SetX(const Value: Integer);override;
    procedure SetY(const Value: Integer);override;
    procedure SetZoom(const Value: double);override;
  public
    constructor Create(DXDraw: TDXDraw; UFOKampf: TDXUFOKampf); override;
    procedure OffSet(X,Y: Integer);override;
    procedure Draw(Surface: TDirectDrawSurface;const Mem: TDDSurfaceDesc; Left,Top: Integer);override;
    function DoMove: boolean;override;
  end;

  TRaketenSprite = class(TWaffenSprite)
  private
    fImage  : TDirectDrawSurface;
  protected
    procedure IncBeschleunigung;override;
    procedure SetMaxGeschw(const Value: Integer);override;
  public
    constructor Create(DXDraw: TDXDraw; UFOKampf: TDXUFOKampf); override;
    destructor Destroy; override;
    procedure GenerateBitmap;override;
    procedure Draw(Surface: TDirectDrawSurface;const Mem: TDDSurfaceDesc; Left,Top: Integer);override;
    function DoMove: boolean;override;
  end;

  TProjektilSprite = class(TWaffenSprite)
  private
    fImage  : TDirectDrawSurface;
  protected
    procedure SetMaxGeschw(const Value: Integer);override;
  public
    constructor Create(DXDraw: TDXDraw; UFOKampf: TDXUFOKampf); override;
    destructor Destroy; override;
    procedure GenerateBitmap;override;
    procedure Draw(Surface: TDirectDrawSurface;const Mem: TDDSurfaceDesc; Left,Top: Integer);override;
  end;

  TStarsColor = record
    Center : TBlendColor;
    Aussen : TBlendColor;
  end;

  TStarField = class(TObject)
  private
    Stars       : Array of TStar;
    fStars      : Integer;
    Colors      : Array[0..255] of TStarsColor;
    fZoom       : double;
    fHeight     : Integer;
    fWidth      : Integer;
    procedure SetZoom(const Value: double);
  public
    constructor Create;
    procedure DoTick;
    procedure Init(Stars: Integer);
    procedure InitColor(Surface: TDirectDrawSurface);
    procedure Move(X,Y: Integer);
    procedure DrawStarField(Surface: TDirectDrawSurface;const Mem: TDDSurfaceDesc);
    property Zoom: double read fZoom write SetZoom;

    property Width: Integer read fWidth write fWidth;
    property Height: Integer read fHeight write fHeight;
  end;

  TDXUFOKampf = class(TDXComponent)
  private
    fInput         : TDXInput;
    fRName         : String;
    fUName         : String;
    fExit          : boolean;
    fQuit          : boolean;
    fRaumschiff    : TSchiffSprite;
    fUFO           : TSchiffSprite;
    fDXImage       : TDXImageList;
    fSchuss        : TList;
    fSprites       : TList;
    fSchiff        : TRaumschiff;
    fUFOObject     : TUFO;
    fUFOTime       : Integer;
    fSieger        : Integer;
    fShiSurface    : TDirectDrawSurface;
    fExpSurface    : TDirectDrawSurface;
    fUFOKI         : TSchiffKI;  // KI f�rs UFOS
    fSchiffKI      : TSchiffKI;  // Unterst�zung zur Steuerung des Raumschiffes
    fIntro         : Integer;
    fTime          : Integer;
    fGreenValue    : Integer;
    fExploEnded    : boolean;
    fGame          : boolean;
    fSchiffHitpoi  : Integer;
    fUFOHitPoi     : Integer;
    fSShieldPoi    : Integer;
    fUShieldPoi    : Integer;
    fStatistik     : TGesamtStatistik;
    fStarField     : TStarField;
    fZoom          : double;
    UFODesFont     : TDirectFont;
    RauDesFont     : TDirectFont;
    YelBolFont     : TDirectFont;
    StatisFont     : TDirectFont;
    {$IFDEF DRAWFRAME}
    fFrames        : Integer;
    fFrameTime     : Cardinal;
    flast          : Cardinal;
    {$ENDIF}
    fAGFFiles      : TAGFImageList;
    procedure AddSchuss(Schuss: TWaffenSprite);
    procedure DeleteSchuss(Schuss: TWaffenSprite);
    procedure AddSprite(Sprite: TAnimSprite);
    procedure DeleteSprite(Sprite: TAnimSprite);
    procedure SetSchiff(const Value: TRaumschiff);
    procedure InitRaumschiffBitmap(Target: TPoint);
    procedure InitUFOBitmap;
    procedure UFOTreffer(Sender: TWaffenSprite;var Destroy: boolean);
    procedure RaumschiffTreffer(Sender: TWaffenSprite;var Destroy: boolean);
    procedure SetUFOObject(const Value: TUFO);
    procedure SetZoom(const Value: double);

    procedure OnRaumschiffShoot(Sender: TSchiffSprite; Zelle: Integer);
    { Private-Deklarationen }
  protected
    function PerformFrame(Sender: TObject;Frames: Integer): boolean;
    procedure DoShoot(Angle,Zelle: Integer);
    procedure DrawWaffen(Surface: TDirectDrawSurface);
    procedure AnimFrame(Sender: TAnimSprite;Frame: Integer);

    procedure RestoreSurfaces;
    procedure InitBitmaps;

    procedure OffSet(X,Y: Integer);
    procedure CalcOffSet;

    procedure SetWaffenZellen;

    { Protected-Deklarationen }
  public
    fZellen        : Array of TWaffInfo;
    constructor Create(Page: TDXPage);override;
    destructor destroy;override;
    procedure SetBitmap(Bitmap: TBitmap;Image: Integer);
    procedure SetShieldMask(Bitmap: TBitmap);
    procedure Draw(Surface: TDirectDrawSurface;var Mem: TDDSurfaceDesc);override;
    procedure ReDrawRect(DrawRect: TRect;Surface: TDirectDrawSurface;var Mem: TDDSurfaceDesc);override;
    procedure DoUFOShoot;
    procedure StartGame;
    procedure Init;

    procedure Resize(var NewLeft,NewTop,NewWidth,NewHeight: Integer);override;

    function GetAGFImage(Name: String): TAGFImage;
    function GetSchiffSprite(Pos: TPoint): TSchiffSprite;

    property Raumschiff    : TRaumschiff read fSchiff write SetSchiff;
    property UFO           : TUFO read fUFOObject write SetUFOObject;
    property Zoom          : double read fZoom write SetZoom;
  end;

//  procedure DrawLifeBar(Surface: TDirectDrawSurface;X,Y: Integer; Percent: double);

var
  ShowStarField     : boolean = true;
  FreeLook          : boolean = false;

implementation

uses NGTypes, game_api, ui_utils, game_utils;

function NormalizeWinkel(Winkel: Integer): Integer;
begin
  if Winkel>180 then
    result:=Winkel-360
  else if Winkel<-180 then
    result:=Winkel+360
  else
    result:=Winkel;
end;

{ TDXUFOKampf }

procedure TDXUFOKampf.AddSchuss(Schuss: TWaffenSprite);
begin
  fSchuss.Add(Schuss);
end;

procedure TDXUFOKampf.AddSprite(Sprite: TAnimSprite);
begin
   fSprites.Add(Sprite);
end;

procedure TDXUFOKampf.AnimFrame(Sender: TAnimSprite; Frame: Integer);
begin
  { wenn das 45. Frame bei einer Explosion erreicht ist Objekt ausblenden }
  if Frame=ExploFrames-1 then fExploEnded:=true;
  if (TSchiffTyp(Sender.Data)=eoUFO) then
  begin
    if fUFO.Visible then
    begin
      fUFO.AlphaValue:=fUFO.AlphaValue-6;
      if (Frame=LastFrame) then fUFO.Visible:=false;
    end;
  end;
  if (TSchiffTyp(Sender.Data)=eoRaumschiff) then
  begin
    if fRaumschiff.Visible then
    begin
      fRaumschiff.AlphaValue:=fRaumschiff.AlphaValue-6;
      if (Frame=LastFrame) then fRaumschiff.Visible:=false;
    end;
  end;
end;

procedure TDXUFOKampf.CalcOffSet;
var
  ROffX,ROffY: Integer;
  UOffX,UOffY: Integer;
  OffX,OffY  : Integer;
begin
  ROffX:=0;ROffY:=0;
  UOffX:=0;UOffY:=0;
  fUFO.CalcNeededOffset(UOffX,UOffY);
  fRaumschiff.CalcNeededOffset(ROffX,ROffY);
  if PtInRect(Rect(40,0,Width-40,Width),Point(round(fRaumschiff.X+UOffX),round(fRaumschiff.Y))) then OffX:=UOffX else OffX:=ROffX;
  if PtInRect(Rect(0,40,Height,Height-40),Point(round(fRaumschiff.X),round(fRaumschiff.Y+UOffY))) then OffY:=UOffY else OffY:=ROffY;
  if (OffX<>0) or (OffY<>0) then OffSet(OffX,OffY);
  {$IFNDEF NOZOOM}
  Zoom:=200/((CalculateEntfern(fRaumschiff.GetPos,fUFO.GetPos)));
  {$ELSE}
  Zoom:=0.625;
  {$ENDIF}
end;

constructor TDXUFOKampf.Create(Page: TDXPage);
var
  Item : TPictureCollectionItem;
  Font : TFont;
begin
  inherited;
  Font:=TFont.Create;
  Font.Name:=coFontName;
  Font.Size:=25;
  Font.Color:=clLime;
  UFODesFont:=FontEngine.FindDirectFont(Font,clGreen);
  Font.Color:=clRed;
  RauDesFont:=FontEngine.FindDirectFont(Font,clMaroon);
  Font.Size:=15;
  Font.Color:=clWhite;
  StatisFont:=FontEngine.FindDirectFont(Font,clBlack);
  Font.Size:=coFontSize;
  Font.Color:=clYellow;
  Font.Style:=[fsBold];
  YelBolFont:=FontEngine.FindDirectFont(Font,clBlack);
  Font.Free;
  fStarField:=TStarField.Create;
  fStarField.InitColor(Container.Surface);
  fZoom:=0.625;
  fRaumschiff:=TSchiffSprite.Create(Self);
  fRaumschiff.DrawZielPunkt:=true;
  fRaumschiff.Typ:=eoRaumschiff;
  fRaumschiff.OnTreffer:=RaumschiffTreffer;
  fRaumschiff.OnShoot:=OnRaumschiffShoot;
  fUFO:=TSchiffSprite.Create(Self);
  fUFO.Typ:=eoUFO;
  fUFO.OnTreffer:=UFOTreffer;
  fDXImage:=TDXImageList.Create(nil);
  fDXImage.DXDraw:=Container;
  { ItemIndex 0 - Raumschiff/UFos}
  Item := TPictureCollectionItem.Create(fDXImage.Items);
  Item.Transparent:=false;
  Item.TransparentColor:=0;
  Item.PatternHeight:=Size;
  Item.PatternWidth:=Size;
  Item.SystemMemory:=true;
  { ItemIndex 1 - Waffen}
  Item := TPictureCollectionItem.Create(fDXImage.Items);
  Item.Transparent:=true;
  Item.TransparentColor:=0;
  Item.PatternHeight:=17;
  Item.PatternWidth:=10;
  Item.SystemMemory:=false;
  { ItemIndex 2 }
  Item := TPictureCollectionItem.Create(fDXImage.Items);
  Item.Transparent:=true;
  Item.TransparentColor:=0;
  Item.SystemMemory:=false;

  { ItemIndex 4 - Raketenschweif }
  Item := TPictureCollectionItem.Create(fDXImage.Items);
  Item.SystemMemory:=false;
  Item.Restore;

  { Raketenschweif - gezoomt }
  fExpSurface:=TDirectDrawSurface.Create(Container.DDraw);
  fExpSurface.SystemMemory:=false;
  { SchutzShield Maske}
  fShiSurface:=TDirectDrawSurface.Create(Container.DDraw);
  fShiSurface.SystemMemory:=false;
  fShiSurface.SetSize(ShieldWidth*ShieldImages,ShieldHeight);
  fSchuss:=TList.Create;
  fSprites:=TList.Create;

  fUFOKI:=TSchiffKI.Create(fUFO,fRaumschiff);
  fSchiffKI:=TSchiffKI.Create(fRaumschiff,fUFO);

  DirectDraw:=true;

  fAGFFiles:=TAGFImageList.Create;
  fAGFFiles.Container:=Container;
  fAGFFiles.AddImage(fDataFile,'RaketenSchweif');
  fAGFFiles.AddImage(fDataFile,'SchutzSchild');
  fAGFFiles.AddImage(fDataFile,'Explosion');

  fAGFFiles.LoadImages;

  fInput:=game_api_GetDirectInput;
end;

procedure TDXUFOKampf.DeleteSchuss(Schuss: TWaffenSprite);
var
  Index: Integer;
begin
  Index:=fSchuss.IndexOf(Schuss);
  if Index<>-1 then fSchuss.Delete(Index);
end;

procedure TDXUFOKampf.DeleteSprite(Sprite: TAnimSprite);
var
  Index: Integer;
begin
  Index:=fSprites.IndexOf(Sprite);
  if Index<>-1 then fSprites.Delete(Index);
end;

destructor TDXUFOKampf.destroy;
begin
  try
    while fSchuss.Count>0 do TWaffenSprite(fSchuss[0]).Free;
    while fSprites.Count>0 do TAnimSprite(fSprites[0]).Free;
  except
  end;
  fSchuss.Free;
  fSprites.Free;
  fDXImage.Free;
  fUFOKI.Free;
  fUFO.Free;
  fRaumschiff.Free;
  fStarField.Free;
  fAGFFiles.Free;
  fSchiffKI.Free;
  Container.DeleteRestoreFunction(RestoreSurfaces);
  inherited;
end;

procedure TDXUFOKampf.DoShoot(Angle,Zelle: Integer);
begin
  if fZellen[Zelle].Munition=0 then exit;
  if fZellen[Zelle].Remain>0 then exit;
  fRaumschiff.DoShoot(Angle,Zelle);
end;

procedure TDXUFOKampf.DoUFOShoot;
var
  Schuss    : TLaserSprite;
  Winkel    : Integer;
  Winkel256 : Integer;
  Abweichung: Integer;
begin
  if fUFOTime>0 then exit;
  Winkel:=fUFO.Winkel;

  Abweichung:=random(random((120-fUFOObject.Model.Treffsicherheit) div 4));
  if random(200)>100 then
    Abweichung:=-Abweichung;
  inc(Winkel,Abweichung);

  if Winkel>359 then dec(Winkel,360);
  if WInkel<0 then inc(Winkel,360);

  Schuss:=TLaserSprite.Create(Container,Self);
  Winkel256:=round(Winkel/360*256);
  Schuss.Zoom:=fZoom;
  Schuss.X:=round(fUFO.X)+round(Sin256(Winkel256)*12);
  Schuss.Y:=round(fUFO.Y)+round(-Cos256(Winkel256)*12);
  Schuss.Winkel:=Winkel;
  Schuss.Schiff:=fUFO;
  Schuss.PPS:=8;
  Schuss.WaffType:=wtLaser;
  Schuss.fStrength:=fUFOObject.Model.Angriff;
  Schuss.GenerateBitmap;
  Schuss.Verfolgung:=false;
  Inc(fStatistik.UFO.Abgegeben);
  fUFOTime:=UFOShootTime;
end;

procedure TDXUFOKampf.Draw(Surface: TDirectDrawSurface;var Mem: TDDSurfaceDesc);
begin
  RedrawRect(ClientRect,Surface,Mem);
end;

procedure TDXUFOKampf.DrawWaffen(Surface: TDirectDrawSurface);
var
  Text            : String;
  TempBot,TempWid : Integer;
  TempRig         : Integer;
  TempWidHalf     : Integer;
  TextWidHalf     : Integer;
begin
  TempBot:=Bottom;
  TempWid:=Width;
  TempRig:=Right;
  TempWidHalf:=TempWid shr 1;
  if fSchiff.WaffenZellen>1 then
  begin
    if fSchiff.WaffenZelle[0].Installiert then
      YellowStdFont.Draw(Surface,Left+10,TempBot-50,fSchiff.WaffenZelle[0].Name);
  end
  else
  begin
    if fSchiff.WaffenZelle[0].Installiert then
    begin
      Text:=fSchiff.WaffenZelle[0].Name;
      TextWidHalf:=WhiteStdFont.TextWidth(Text) shr 1;
      YellowStdFont.Draw(Surface,Left+TempWidHalf-TextWidHalf,TempBot-50,fSchiff.WaffenZelle[0].Name);
    end;
  end;
  if fSchiff.WaffenZellen>1 then
  begin
    if fSchiff.WaffenZelle[1].Installiert then
    begin
      Text:=fSchiff.WaffenZelle[1].Name;
      YellowStdFont.Draw(Surface,TempRig-10-YellowStdFont.TextWidth(Text),TempBot-50,Text);
    end;
  end;
  if fSchiff.WaffenZellen>2 then
  begin
    if fSchiff.WaffenZelle[2].Installiert then
    begin
      Text:=fSchiff.WaffenZelle[2].Name;
      TextWidHalf:=WhiteStdFont.TextWidth(Text) shr 1;
      YellowStdFont.Draw(Surface,Left+TempWidHalf-TextWidHalf,TempBot-50,fSchiff.WaffenZelle[2].Name);
    end;
  end;
  YellowStdFont.Draw(Surface,Left+10,Top+10,fSchiff.Name);
  YellowStdFont.Draw(Surface,TempRig-10-YellowStdFont.TextWidth(fUFOObject.Name),Top+10,fUFOObject.Name);
  if fSchiff.WaffenZellen=1 then
  begin
    if fSchiff.WaffenZelle[0].Installiert then
    begin
      { Waffen Zelle 1 bei einer Waffe (Zentriert) }
      with fZellen[0] do
      begin
        DrawWeaponTimeBar(Surface,Left+TempWidHalf-50,TempBot-15,round((LadeZeit-Remain)/LadeZeit*100));
        DrawWeaponMunitionBar(Surface,Left+TempWidHalf-50,TempBot-25,round(Munition/MaxMuni*100));
      end;
    end;
  end
  else
  begin
    if fSchiff.WaffenZelle[0].Installiert then
    begin
      { Waffen Zelle 1 bei mehr als 1 Waffe (Links) }
      with fZellen[0] do
      begin
        DrawWeaponTimeBar(Surface,Left+10,TempBot-15,round((LadeZeit-Remain)/LadeZeit*100));
        DrawWeaponMunitionBar(Surface,Left+10,TempBot-25,round(Munition/MaxMuni*100));
      end;
    end;
    if (fSchiff.WaffenZellen>1) then
    begin
      if fSchiff.WaffenZelle[1].Installiert then
      begin
        { Waffen Zelle 2 (Rechts) }
        with fZellen[1] do
        begin
          DrawWeaponTimeBar(Surface,TempRig-110,TempBot-15,round((LadeZeit-Remain)/LadeZeit*100));
          DrawWeaponMunitionBar(Surface,TempRig-110,TempBot-25,round(Munition/MaxMuni*100));
        end;
      end;
      if (fSchiff.WaffenZellen>2) and fSchiff.WaffenZelle[2].Installiert then
      begin
        { Waffen Zelle 3 (Zentriert) }
        with fZellen[2] do
        begin
          DrawWeaponTimeBar(Surface,Left+TempWidHalf-50,TempBot-15,round((LadeZeit-Remain)/LadeZeit*100));
          DrawWeaponMunitionBar(Surface,Left+TempWidHalf-50,TempBot-25,round(Munition/MaxMuni*100));
        end;
      end;
    end;
  end;
  { Energieanzeige}

  { Hitpoints Raumschiff }
  DrawLifeBar(Surface,Left+10,Top+30,round(fSchiffHitpoi/fSchiff.HitPoints*100));
  { Hitpoints UFO }
  DrawLifeBar(Surface,TempRig-210,Top+30,round(fUFOHitpoi/fUFOObject.Model.HitPoints*100));
  { Schild Raumschiff }
  if fSchiff.ShieldInstalliert then
    DrawShieldBar(Surface,Left+10,Top+42,round(fSShieldPoi/fSchiff.MaxShieldPoints*100));
    { Schild UFO }
  if fUFOObject.Model.Shield>0 then
    DrawShieldBar(Surface,TempRig-210,Top+42,round(fUShieldPoi/fUFOObject.Model.Shield*100));
end;

function TDXUFOKampf.GetAGFImage(Name: String): TAGFImage;
begin
  result:=fAGFFiles.GetAGFImage(Name);
end;

function TDXUFOKampf.GetSchiffSprite(Pos: TPoint): TSchiffSprite;
begin
  result:=nil;
  if PtInRect(fRaumschiff.BoundingBox,Pos) then result:=fRaumschiff
  else if PtInRect(fUFO.BoundingBox,Pos) then result:=fUFO;
end;

procedure TDXUFOKampf.Init;
begin
  FillChar(fStatistik,SizeOf(fStatistik),#0);
  fIntro:=3;
  fTime:=1000;
  fGreenValue:=255;
  fExit:=false;
  fExploEnded:=false;
  fQuit:=false;
  fGame:=true;
  fUFO.Destroyed:=false;
  fUFO.Schaden:=100-round(fUFOObject.HitPoints/fUFOObject.Model.HitPoints*100);
  fUFO.Duesen:=true;
  fRaumschiff.Destroyed:=false;
  fRaumschiff.SetBitmapRect(fDXImage.Items[0].PatternSurfaces[0],fDXImage.Items[0].PatternRects[0]);
  fRaumschiff.Duesen:=fSchiff.Motor.Duesen;
  fRaumschiff.Visible:=true;
  fRaumschiff.Enemy:=fUFO;

  // Infos zu den Waffenzellen setzen
  SetWaffenZellen;

  fUFO.SetBitmapRect(fDXImage.Items[0].PatternSurfaces[1],fDXImage.Items[0].PatternRects[1]);
  fUFO.MaxBeschl:=fUFOObject.Model.Pps;
  fUFO.Visible:=true;
  fUFO.Enemy:=fRaumschiff;
  Zoom:=0.625;
  fSchiffHitpoi:=fSchiff.AktHitPoints;
  fSShieldPoi:=fSchiff.ShieldPoints;
  fUShieldPoi:=fUFOObject.ShieldPoints;
  fUFOHitPoi:=fUFOObject.HitPoints;
  fUFOTime:=0;
  fUFOKI.Reset;
  while fSchuss.Count>0 do
    TWaffenSprite(fSchuss[0]).Free;
  while fSprites.Count>0 do
    TAnimSprite(fSprites[0]).Free;
  fRaumschiff.SetStartPos(50,random(Height-80)+40,64);
  fUFO.SetStartPos(Width-50,random(Height-80)+40,300);

  InitBitmaps;
  
  {$IFNDEF NOZOOM}
  Zoom:=200/((CalculateEntfern(fRaumschiff.GetPos,fUFO.GetPos)));
  {$ELSE}
  Zoom:=0.625;
  {$ENDIF}
  fStarField.Init(1800);

  { Tastenbelegung �bernehmen }
  ui_utils_ChangeKeyAssign(fInput,isLeft,KeyConfiguration.UFOKeys.RLeft);
  ui_utils_ChangeKeyAssign(fInput,isRight,KeyConfiguration.UFOKeys.RRight);
  ui_utils_ChangeKeyAssign(fInput,isUp,KeyConfiguration.UFOKeys.GibSchub);
  ui_utils_ChangeKeyAssign(fInput,isButton1,KeyConfiguration.UFOKeys.WZelle1);
  ui_utils_ChangeKeyAssign(fInput,isButton2,KeyConfiguration.UFOKeys.WZelle2);
  ui_utils_ChangeKeyAssign(fInput,isButton3,KeyConfiguration.UFOKeys.WZelle3);
  ui_utils_ChangeKeyAssign(fInput,isButton5,KeyConfiguration.UFOKeys.MRight);
  ui_utils_ChangeKeyAssign(fInput,isButton6,KeyConfiguration.UFOKeys.MLeft);

  KRLeft:=ui_utils_GetText(KeyConfiguration.UFOKeys.RLeft);
  KRRight:=ui_utils_GetText(KeyConfiguration.UFOKeys.RRight);
  KSchub:=ui_utils_GetText(KeyConfiguration.UFOKeys.GibSchub);
  KMLeft:=ui_utils_GetText(KeyConfiguration.UFOKeys.MLeft);
  KMRight:=ui_utils_GetText(KeyConfiguration.UFOKeys.MRight);
  KZelle1:=ui_utils_GetText(KeyConfiguration.UFOKeys.WZelle1);
  KZelle2:=ui_utils_GetText(KeyConfiguration.UFOKeys.WZelle2);
  KZelle3:=ui_utils_GetText(KeyConfiguration.UFOKeys.WZelle3);

  Container.AddRestoreFunction(RestoreSurfaces);
end;

procedure TDXUFOKampf.InitBitmaps;
var
  Dummy: Integer;
begin
  InitBarSurface(Container);

  InitRaumschiffBitmap(Container.MousePos);
  InitUFOBitmap;

  fRaumschiff.SetBitmapRect(fDXImage.Items[0].PatternSurfaces[0],fDXImage.Items[0].PatternRects[0]);
  fUFO.SetBitmapRect(fDXImage.Items[0].PatternSurfaces[1],fDXImage.Items[0].PatternRects[1]);

  fUFO.Restore;
  fRaumschiff.Restore;

  for Dummy:=0 to fSchuss.Count-1 do
    TWaffenSprite(fSchuss[Dummy]).GenerateBitmap;

end;

procedure TDXUFOKampf.InitRaumschiffBitmap(Target: TPoint);
var
  Dest     : TDirectDrawSurface;
  Source   : TDirectDrawSurface;
  TempRect : TRect;

  procedure DrawSchiffRect(Rect: TRect);
  var
    X,Y      : Integer;
  begin
    X:=Rect.Left;
    Y:=Rect.Top;
    TempRect:=fDXImage.Items[0].PatternRects[fSchiff.WaffenZellen+1];
    OffSetRect(Rect,TempRect.Left,TempRect.Top);
    Dest.Draw(X,Y,Rect,Source,true);
  end;

  procedure DrawWaffe(Typ: TWaffenType;X,Y: Integer);
  var
    Index  : Integer;
    Winkel : Integer;
  begin
    Index:=0;
    case Typ of
      wtProjektil : Index:=0;
      wtLaser     : Index:=1;
      wtRaketen   : Index:=2;
    end;
//    Dest.Draw(X,Y,fDXImage.Items[1].PatternRects[Index],fDXImage.Items[1].PatternSurfaces[Index],true);
    Winkel:=CalculateWinkel(Target,Point(round(fRaumschiff.X),round(fRaumschiff.Y)));
    Winkel:=NormalizeWinkel(Winkel+(360-fRaumschiff.Winkel));
    Dest.DrawRotate(X+5,Y+8,10,17,fDXImage.Items[1].PatternRects[Index],fDXImage.Items[1].PatternSurfaces[Index],0.5,0.5,true,Winkel*256 div 360);
  end;

begin
  if not ((fSchiff=nil) or fRaumschiff.Destroyed) then
  begin
    Dest:=fDXImage.Items[0].PatternSurfaces[0];
    Source:=fDXImage.Items[0].PatternSurfaces[fSchiff.WaffenZellen+1];
    Source.FillRect(fDXImage.Items[0].PatternRects[0],0);
    case fSchiff.WaffenZellen of
      1:
      begin
        if fSchiff.WaffenZelle[0].Installiert then DrawWaffe(fZellen[0].WaffTyp,22,19);
        DrawSchiffRect(Rect(0,0,Size,Size));
      end;
      2:
      begin
        DrawSchiffRect(Rect(0,0,Size,Size));
        if fSchiff.WaffenZelle[0].Installiert then DrawWaffe(fZellen[0].WaffTyp,17,22);
        if fSchiff.WaffenZelle[1].Installiert then DrawWaffe(fZellen[1].WaffTyp,26,22);
        DrawSchiffRect(Rect(16,27,30,35));
      end;
      3:
      begin
        if fSchiff.WaffenZelle[0].Installiert then DrawWaffe(fZellen[0].WaffTyp,11,24);
        if fSchiff.WaffenZelle[1].Installiert then DrawWaffe(fZellen[1].WaffTyp,31,24);
        DrawSchiffRect(Rect(0,0,Size,Size));
        if fSchiff.WaffenZelle[2].Installiert then DrawWaffe(fZellen[2].WaffTyp,21,30);
        DrawSchiffRect(Rect(21,36,26,44));
      end;
    end;
  end;
end;

procedure TDXUFOKampf.InitUFOBitmap;
var
  Dest     : TDirectDrawSurface;
  Source   : TDirectDrawSurface;
  TempRect : TRect;

begin
  if not ((fUFOObject=nil) or fUFO.Destroyed) then
  begin
    Dest:=fDXImage.Items[0].PatternSurfaces[1];
    Source:=fDXImage.Items[0].PatternSurfaces[fUFOObject.Size+5];
    Source.FillRect(fDXImage.Items[0].PatternRects[1],0);
    TempRect:=fDXImage.Items[0].PatternRects[1];
    Dest.Draw(TempRect.Left,TempRect.Top,fDXImage.Items[0].PatternRects[fUFOObject.Size+5],Source,true);
  end;
end;

procedure TDXUFOKampf.OffSet(X, Y: Integer);
var
  Dummy: Integer;
begin
  fRaumschiff.OffSet(X,Y);
  fUFO.OffSet(X,Y);
  if ShowStarField then fStarField.Move(X,Y);
  for Dummy:=0 to fSchuss.Count-1 do TWaffenSprite(fSchuss[Dummy]).OffSet(X,Y);
  for Dummy:=0 to fSprites.Count-1 do TAnimSprite(fSprites[Dummy]).OffSet(X,Y);
end;

procedure TDXUFOKampf.OnRaumschiffShoot(Sender: TSchiffSprite;
  Zelle: Integer);
begin
  // Statistik
  inc(fStatistik.Zellen[Zelle+1].Abgegeben);
  inc(fStatistik.Gesamt.Abgegeben);

  with fZellen[Zelle] do
  begin
    Munition:=Munition-1;
    Remain:=LadeZeit;
  end;
end;

function TDXUFOKampf.PerformFrame(Sender: TObject;Frames: Integer): boolean;
var
  SchiffRotate : boolean;
  SchiffMove   : boolean;
  Dummy        : Integer;
  Redraw       : boolean;
  Count        : Integer;
  Winkel       : Integer;
begin
  Redraw:=false;
  {$IFDEF DRAWFRAME}
  fFrameTime:=GetTickCount-fLast;
  fLast:=GetTickCount;
  fFrames:=Frames;
  {$ENDIF}
  result:=false;
  fInput.Update;
  if (isButton32 in fInput.States) then
  begin
    fInput.States := fInput.States - [isButton32];
    Container.MessageLock(false);
    Container.DeleteFrameFunction(PerformFrame,Self);
//    Container.ShowCursor(true);
//    Container.MoveMessage:=true;
    Container.UnLockAll;
    game_api_MessageBox(Format(MTastInfo,[KRLeft,KRRight,KSchub,KMLeft,KMRight,KZelle1,KZelle2,KZelle3]),CTastInfo);
    Container.LockAll;
//    Container.MoveMessage:=false;
//    Container.ShowCursor(false);
    Container.MessageLock(true);
    Container.AddFrameFunction(PerformFrame,Self,FrameTime);
    exit;
  end;
  if fIntro>0 then
  begin
    while (Frames>0) do
    begin
      dec(Frames);
      dec(fTime,FrameTime);
      if fIntro=3 then dec(fGreenValue,6) else dec(fGreenValue,12);
      if fTime<=0 then
      begin
        dec(fIntro);
        inc(fTime,500);
      end;
      Redraw:=true;
      result:=true;
    end;
    if Redraw then Container.RedrawBlackControl(Self,Container.Surface);
    exit;
  end;
  Container.Lock;
  while Frames>0 do
  begin
    if ShowStarField then fStarField.DoTick;
    if not fExit then
    begin
           if fSchiffHitpoi>fSchiff.AktHitPoints then fSchiffHitpoi:=max(fSchiff.AktHitPoints,fSchiffHitPoi-max(1,fSchiff.HitPoints div 100))
      else if fSchiffHitpoi<fSchiff.AktHitPoints then fSchiffHitpoi:=min(fSchiff.AktHitPoints,fSchiffHitPoi+max(1,fSchiff.HitPoints div 100));
           if fUFOHitPoi>fUFOObject.HitPoints then fUFOHitPoi:=max(fUFOObject.HitPoints,fUFOHitPoi-max(1,fUFOObject.Model.HitPoints div 100))
      else if fUFOHitPoi<fUFOObject.HitPoints then fUFOHitPoi:=min(fUFOObject.HitPoints,fUFOHitPoi+max(1,fUFOObject.Model.HitPoints div 100));
           if fSShieldPoi>fSchiff.ShieldPoints then fSShieldPoi:=max(fSchiff.ShieldPoints,fSShieldPoi-max(1,fSchiff.MaxShieldPoints div 200))
      else if fSShieldPoi<fSchiff.ShieldPoints then fSShieldPoi:=min(fSchiff.ShieldPoints,fSShieldPoi+max(1,fSchiff.MaxShieldPoints div 200));
           if fUShieldPoi>fUFOObject.ShieldPoints then fUShieldPoi:=max(fUFOObject.ShieldPoints,fUShieldPoi-max(1,fUFOObject.Model.Shield div 200))
      else if fUShieldPoi<fUFOObject.ShieldPoints then fUShieldPoi:=min(fUFOObject.ShieldPoints,fUShieldPoi+max(1,fUFOObject.Model.Shield div 200));

      fSchiff.RechargeShield(AnimTime);
      fUFOObject.RechargeShield(AnimTime);

      SchiffRotate:=false;
      SchiffMove:=false;
      fSchiffKI.InitMoveArt;

      // Raumschiff rotieren
      Winkel:=CalculateWinkel(PointToFloat(Container.MousePos),FloatPoint(fRaumSchiff.X,fRaumschiff.Y));
      if FreeLook then
        fSchiffKI.Rotate(Winkel*256 div 360)
      else
      begin
        if (isLeft in fInput.States) then
          fSchiffKI.Rotiere(true);

        if (isRight in fInput.States) and not SchiffRotate then
          fSchiffKI.Rotiere(false);

        if (isUp in fInput.States) then
          fSchiffKI.GibSchub;
      end;

      // Abschiessen der Waffen
      if GetKeyState(VK_RBUTTON)<0 then
      begin
        if fSchiff.WaffenZelle[0].Installiert then DoShoot(Winkel,0);
        if (fSchiff.WaffenZellen>1) and fSchiff.WaffenZelle[1].Installiert then DoShoot(Winkel,1);
        if (fSchiff.WaffenZellen>2) and fSchiff.WaffenZelle[2].Installiert then DoShoot(Winkel,2);
      end;

      if FreeLook and (GetKeyState(VK_LBUTTON)<0) then
        if (CalculateEntfern(PointToFloat(Container.MousePos),FloatPoint(fRaumSchiff.X,fRaumschiff.Y)))>25 then
          fSchiffKI.GibSchub;
      if (isButton5 in fInput.States) then
      begin
        fSchiffKI.Move(true);
      end;
      if (isButton6 in fInput.States) and not SchiffMove then
      begin
        fSchiffKI.Move(false);
      end;
      fSchiffKI.DoMoveArt;
{      if not SchiffRotate then fRaumschiff.NoRotate;
      if not SchiffSchub then fRaumschiff.NoSchub;
      if not SchiffMove then fRaumschiff.NoMove;}
      {$IFNDEF NOUFOMOVE}
      fUFOKI.DoRound;
      {$ENDIF}
      CalcOffSet;
      if fUFOTime>0 then
        dec(fUFOTime,AnimTime);
      for Dummy:=0 to length(fZellen)-1 do
      begin
        with fZellen[Dummy] do
        begin
          if Remain>0 then
          begin
            dec(Remain,AnimTime);
            if Remain<0 then Remain:=0;
          end;
          if LaserWaffe then
          begin
            if Munition<MaxMuni then
            begin
              dec(NachLadeZeit,AnimTime);
              if NachLadeZeit<=0 then
              begin
                inc(Munition);
                inc(NachLadeZeit,Strength*2);
              end;
            end;
          end;
        end;
      end;
      if (not OverlapRect(fRaumschiff.BoundingBox,ClientRect)) or (not OverlapRect(fUFO.BoundingBox,ClientRect)) then
      begin
        fSieger:=0;
        fExit:=true;
        fExploEnded:=true;
      end;
    end
    else if fExploEnded then
    begin
      fInput.Update;
      if (isButton31 in fInput.States) then fQuit:=true;
    end;
    Redraw:=true;
    for Dummy:=0 to fSprites.Count-1 do
    begin
      TAnimSprite(fSprites[Dummy]).Zoom:=fZoom;
    end;
    Count:=fSprites.Count;
    Dummy:=0;
    while Dummy<Count do
    begin
      if not TAnimSprite(fSprites[Dummy]).Timer(AnimTime) then inc(Dummy) else dec(Count);
    end;
    Dummy:=0;
    Count:=fSchuss.Count;
    while (Dummy<Count) do
    begin
      if TWaffenSprite(fSchuss[Dummy]).DoMove then inc(Dummy) else dec(Count);
    end;
    dec(Frames);
    result:=true;
  end;
  Container.UnLock;
  if Redraw then Self.Redraw;
end;

procedure TDXUFOKampf.RaumschiffTreffer(Sender: TWaffenSprite;
  var Destroy: boolean);
var
  Absorb: Integer;
begin
  { Wird ausgef�hrt wenn das Raumschiff von einem Objekt Getroffen wurde }
  if Sender.Schiff=fUFO then
  begin
    { Raumschiff vom UFO getroffen. Treffer f�r UFO erh�hen }
    inc(fStatistik.UFO.Getroffen);
  end;
  { Schaden am Raumschiff verursachen }
  Destroy:=fSchiff.DoTreffer(Sender.Strength,Absorb,fUFOObject);
  if (not Destroy) and (Absorb>0) and (fSchiff.MaxShieldPoints>0) then
  begin
    with TShieldSprite.Create(Container,Self) do
    begin
      SetBitmap(GetAGFImage('SchutzSchild'),ShieldImages,fZoom);
      fX:=fRaumschiff.X;
      fY:=fRaumschiff.Y;
      FrameTime:=25;
      FollowSprite:=fRaumschiff;
      Angle:=round(Sender.fAngle)-128;
      Color:=LineWinColorTable[(100*fSchiff.ShieldPoints div fSchiff.MaxShieldPoints)];
    end;
  end;
  if Destroy then
  begin
    { Wenn das Raumschiff zerst�rt wurde, Kampf beenden }
    fExit:=true;
    fRaumschiff.Destroyed:=true;    // Raumschiff wurde zerst�rt
    fSieger:=2;                     // Sieger auf 2 (UFO) setzen
  end;
end;

procedure TDXUFOKampf.ReDrawRect(DrawRect: TRect; Surface: TDirectDrawSurface;
  var Mem: TDDSurfaceDesc);
var
  Dummy   : Integer;
  TextTop : Integer;
  Text    : String;

  procedure DrawBilanz(TextTop: Integer;Text: String;Statistik: TSchussStatistik);
  begin
    YelBolFont.Draw(Surface,Left+150,TextTop,Text);
    Text:=IntToStr(Statistik.Abgegeben);
    WhiteStdFont.Draw(Surface,Left+330-(WhiteStdFont.TextWidth(Text) shr 1),TextTop,Text);
    if Statistik.Getroffen>0 then
      Text:=Format(ST0502050001,[Statistik.Getroffen,round(Statistik.Getroffen/Statistik.Abgegeben*100)])
    else
      Text:='---';
    WhiteStdFont.Draw(Surface,Left+430-(WhiteStdFont.TextWidth(Text) shr 1),TextTop,Text);
    if Statistik.Abgegeben-Statistik.Getroffen>0 then
      Text:=Format(ST0502050001,[Statistik.Abgegeben-Statistik.Getroffen,round((Statistik.Abgegeben-Statistik.Getroffen)/Statistik.Abgegeben*100)])
    else
      Text:='---';
    WhiteStdFont.Draw(Surface,Left+530-(WhiteStdFont.TextWidth(Text) shr 1),TextTop,Text);
  end;

begin
  Surface.Fill(0);
  Surface.Lock(Mem);
  Surface.Unlock;
  if ShowStarField then
    fStarField.DrawStarField(Surface,Mem);
  if fIntro>0 then
  begin
    With Surface.Canvas do
    begin
      Brush.Style:=bsClear;
      Font.Name:='Arial';
      Font.Color:=RGB(0,fGreenValue,0);
      Font.Size:=40;
      case fIntro of
        3 : Text:=HStart3;
        2 : Text:=HStart2;
        1 : Text:=HStart1;
      end;
      TextOut(Left+(Width shr 1)-(TextWidth(Text) shr 1),Top+150,Text);
      Font:=Self.Font;
      Release;
    end;
  end;

//  Container.PowerDraw.Lock(Surface);
//  Container.PowerDraw.PowerD3D.BeginScene;
  for Dummy:=0 to fSchuss.Count-1 do
    TWaffenSprite(fSchuss[Dummy]).Draw(Surface,Mem,Left,Top);
//  Container.PowerDraw.Unlock;

  fUFO.CalcPrepared;
  fUFO.Draw(Surface,Mem,Left,Top);

  fRaumschiff.GenerateSchiffBitmap(Container.MousePos,fDXImage.Items[0].PatternSurfaces[0],fDXImage.Items[0].PatternRects[0]);
  fRaumschiff.Draw(Surface,Mem,Left,Top);

//  Container.PowerDraw.Lock(Surface);
  for Dummy:=0 to fSprites.Count-1 do
    TAnimSprite(fSprites[Dummy]).Draw(Surface,Left,Top);
///  Container.PowerDraw.PowerD3D.EndScene;
//  Container.PowerDraw.Unlock;

  if not fExit then
  begin
    { Nur Zeichnen wenn der Kampf noch l�uft }
    DrawWaffen(Surface);
  end
  else
  begin
    if fExploEnded then
    begin
      { Zeichnen der Auswertung am Ende eines Kampfes }
      if fUFO.Destroyed then
      begin
        UFODesFont.Draw(Surface,Left+(Width shr 1)-(UFODesFont.TextWidth(LGewonnen) shr 1),Top+50,LGewonnen);
      end;
      if fRaumschiff.Destroyed then
      begin
        RauDesFont.Draw(Surface,Left+(Width shr 1)-(RauDesFont.TextWidth(LVerloren) shr 1),Top+90,LVerloren);
      end;
      WhiteStdFont.Draw(Surface,Left+(Width shr 1)-(WhiteStdFont.TextWidth(LEscToEnd) shr 1),Bottom-50,LEscToEnd);
      { Statistik zeichnen }
      StatisFont.Draw(Surface,Left+(Width shr 1)-(StatisFont.TextWidth(LStatistik) shr 1),Top+170,LStatistik);
      YelBolFont.Draw(Surface,Left+330-(YelBolFont.TextWidth(LGeschossen) shr 1),Top+225,LGeschossen);
      YelBolFont.Draw(Surface,Left+430-(YelBolFont.TextWidth(LTreffer) shr 1),Top+225,LTreffer);
      YelBolFont.Draw(Surface,Left+530-(YelBolFont.TextWidth(LVerfehlt) shr 1),Top+225,LVerfehlt);
      TextTop:=Top+250;
      for Dummy:=1 to fStatistik.WZellen do
      begin
        DrawBilanz(TextTop,Format(IAZelle,[Dummy]),fStatistik.Zellen[Dummy]);
        inc(TextTop,20);
      end;
      DrawBilanz(TextTop+10,Format(LRGesamt,[fRName]),fStatistik.Gesamt);
      DrawBilanz(TextTop+30,fUName+':',fStatistik.UFO);
    end;
  end;
  {$IFDEF DRAWFRAME}
  Canvas.Font.Color:=clWhite;
  Canvas.TextOut(Left+10,Top+60,IntToStr(fFrames));
  Canvas.TextOut(Left+10,Top+80,IntToStr(fFrameTime));
  {$ENDIF}
  {$IFDEF DRAWDEBUG}
  Canvas.TextOut(Left+10,Top+100,IntToStr(CalculateEntfern(fRaumschiff.Pos,fUFO.Pos)));
  {$ENDIF}
  {$IFDEF SHOWMEMORY}
  WhiteStdFont.Draw(Surface,Left+10,Top+120,IntToStr(GetProcessMemory));
  {$ENDIF}
end;

procedure TDXUFOKampf.Resize(var NewLeft, NewTop, NewWidth,
  NewHeight: Integer);
begin
  inherited;
  fStarField.Width:=NewWidth;
  fStarField.Height:=NewHeight;
end;

procedure TDXUFOKampf.RestoreSurfaces;
begin
  InitBitmaps;

  fAGFFiles.LoadImages;
end;

procedure TDXUFOKampf.SetBitmap(Bitmap: TBitmap; Image: Integer);
begin
  fDXImage.Items[Image].Transparent:=true;
  fDXImage.Items[Image].TransparentColor:=Bitmap.Canvas.Pixels[0,0];
  fDXImage.Items[Image].Picture.Bitmap:=Bitmap;
  fDXImage.Items[Image].Restore;
end;

procedure TDXUFOKampf.SetSchiff(const Value: TRaumschiff);
var
  Dummy: Integer;
begin
  fSchiff := Value;
  fRName:=Value.Name;
  SetLength(fZellen,0);
  SetLength(fZellen,fSchiff.WaffenZellen);
  for Dummy:=0 to fSchiff.WaffenZellen-1 do
  begin
    with fSchiff.WaffenZelle[Dummy] do
    begin
      fZellen[Dummy].Munition:=Munition;
      fZellen[Dummy].LadeZeit:=Laden;
      fZellen[Dummy].WaffTyp:=WaffType;
      fZellen[Dummy].LaserWaffe:=WaffType=wtLaser;
      fZellen[Dummy].MaxMuni:=fSchiff.GetMaxMunition(Dummy);
      fZellen[Dummy].NachLadeZeit:=Strength*2;
      fZellen[Dummy].Strength:=Strength;
      fZellen[Dummy].Reichweite:=Reichweite;
    end;
  end;
  fRaumschiff.MaxBeschl:=fSchiff.Motor.PPS;
end;

procedure TDXUFOKampf.SetShieldMask(Bitmap: TBitmap);
begin
  fShiSurface.Canvas.Draw(0,0,Bitmap);
  fShiSurface.Canvas.Release;
end;

procedure TDXUFOKampf.SetUFOObject(const Value: TUFO);
begin
  fUFOObject := Value;
  fUName:=Value.Name;
end;

procedure TDXUFOKampf.SetWaffenZellen;

  function BuildWaffenZellenInfo(Angle, Radius,Min,Max: Integer; Waffe: TWaffenZelle): TWaffenZellenInfo;
  begin
    result.Angle:=Angle;
    result.RadiusFromMiddle:=Radius;
    result.MinWinkel:=Min;
    result.MaxWinkel:=Max;
    result.Waffe:=Waffe;
  end;

begin
  fRaumschiff.ClearWaffenZellen;
  case fSchiff.WaffenZellen of
    1:
    begin
      fRaumschiff.AddWaffenZellenInfo(BuildWaffenZellenInfo(0,0,-180,180,fSchiff.WaffenZelle[0]));
    end;
    2:
    begin
      fRaumschiff.AddWaffenZellenInfo(BuildWaffenZellenInfo(205,12,-45,90,fSchiff.WaffenZelle[0]));
      fRaumschiff.AddWaffenZellenInfo(BuildWaffenZellenInfo(155,12,-90,45,fSchiff.WaffenZelle[1]));
    end;
    3:
    begin
      fRaumschiff.AddWaffenZellenInfo(BuildWaffenZellenInfo(225,12,-90,45,fSchiff.WaffenZelle[0]));
      fRaumschiff.AddWaffenZellenInfo(BuildWaffenZellenInfo(135,12,-45,90,fSchiff.WaffenZelle[1]));
      fRaumschiff.AddWaffenZellenInfo(BuildWaffenZellenInfo(0,12,-45,45,fSchiff.WaffenZelle[2]));
    end;
  end;
end;

procedure TDXUFOKampf.SetZoom(const Value: double);
var
  Dummy  : Integer;
  fHeigh : Integer;
  fWidth : Integer;
begin
  if round(fZoom*10)=round(Value*10) then exit;
  fZoom := max(min(Value,1),0.5);
  fRaumschiff.Zoom:=fZoom;
  fUFO.Zoom:=fZoom;
  fWidth:=round((fDXImage.Items[3].Width div 16)*fZoom);
  fHeigh:=round(fDXImage.Items[3].Height*fZoom);
  fExpSurface.SetSize(fWidth*16,round(fDXImage.Items[3].Height*fZoom));
  fDXImage.Items[3].Transparent:=false;
  fDXImage.Items[3].StretchDraw(fExpSurface,Rect(0,0,fWidth*16,fHeigh),0);
//  fExpSurfae.StretchDraw(Rect(0,0,round(fDXImage.Items[4].Width*fZoom),round(fDXImage.Items[4].Height*fZoom)),Rect(0,0,fDXImage.Items[4].Width,fDXImage.Items[4].Height),fDXImage.Items[4].PatternSurfaces[0],false);
  for Dummy:=0 to fSchuss.Count-1 do
    TWaffenSprite(fSchuss[Dummy]).Zoom:=fZoom;
  for Dummy:=0 to fSprites.Count-1 do
    TAnimSprite(fSprites[Dummy]).Zoom:=fZoom;
//  if fSShieldSprite<>nil then fSShieldSprite.Zoom:=fZoom;
//  if fUShieldSprite<>nil then fUShieldSprite.Zoom:=fZoom;
  if ShowStarField then
    fStarField.Zoom:=fZoom;
end;

procedure TDXUFOKampf.StartGame;
var
  Dummy: Integer;
begin
  Container.AddFrameFunction(PerformFrame,Self,FrameTime);
  {$IFDEF DRAWFRAME}
  fLast:=GetTickCount;
  {$ENDIF}
  fStatistik.WZellen:=fSchiff.WaffenZellen;

  { Kampfausf�hrung }
  while not (fExit and fQuit) do
    Application.HandleMessage;

  // Aufr�umen
  Container.DeleteFrameFunction(PerformFrame,Self);
  fGame:=false;
  while fSchuss.Count>0 do TWaffenSprite(fSchuss[0]).Free;
  while fSprites.Count>0 do TAnimSprite(fSprites[0]).Free;
  for Dummy:=0 to Length(fZellen)-1 do
  begin
    if not fZellen[Dummy].LaserWaffe then fSchiff.SetMunition(Dummy,fZellen[Dummy].Munition);
  end;
  if fSieger<>2 then fSchiff.RechargeShield(0,true);
  if fSieger=1 then fSchiff.DoAbschuss;
  if fSieger<>1 then fUFOObject.RechargeShield(0,true);

  Container.DeleteRestoreFunction(RestoreSurfaces);

  // AGF-Bilder freigeben
//  fAGFFiles.FreeImages;
end;

procedure TDXUFOKampf.UFOTreffer(Sender: TWaffenSprite;
  var Destroy: boolean);
var
  Absorb: Integer;
begin
  { Wird ausgef�hrt wenn das UFO von einem Objekt Getroffen wurde }
  if Sender.Schiff=fRaumSchiff then
  begin
    { UFO vom Raumschiff getroffen. Treffer f�r Raumschiff erh�hen }
    inc(fStatistik.Zellen[Sender.WZelle].Getroffen);
    inc(fStatistik.Gesamt.Getroffen);
  end;
  { Schaden am UFO verursachen }
  Destroy:=fUFOObject.DoTreffer(Sender.Strength,Sender.WaffType,Absorb);
  if (not Destroy) and (Absorb>0) and (fUFOObject.Model.Shield>0) then
  begin
    with TShieldSprite.Create(Container,Self) do
    begin
      SetBitmap(GetAGFImage('SchutzSchild'),ShieldImages,fZoom);
      fX:=fUFO.X;
      fY:=fUFO.Y;
      FrameTime:=25;
      Angle:=round(Sender.fAngle)-128;
      FollowSprite:=fUFO;
      Color:=LineWinColorTable[100-((100*fUFOObject.ShieldPoints) div fUFOObject.Model.Shield)];
    end;
  end;
  if Destroy then
  begin
    { Wenn das UFO zerst�rt wurde, Kampf beenden }
    fExit:=true;
    fUFO.Destroyed:=true;           // Raumschiff wurde zerst�rt
    fSieger:=1;                     // Sieger auf 1 (Raumschiff) setzen
    fUFO.Schaden:=100;              // Schaden des UFOs auf 100 % setzen (f�r die KI)
  end
  else
    { Schaden neu berechnen ( Flucht bei der KI ) }
    fUFO.Schaden:=100-round(fUFOObject.HitPoints/fUFOObject.Model.HitPoints*100);
end;

{ TSchiffSprite }

constructor TSchiffSprite.Create(UFOKampf: TDXUFOKampf);
begin
  fBeschl:=0;
  fZoom:=0.5;
  fSchaden:=0;
  fX:=100;
  fY:=100;
  fAngle:=0;
  fUFOKampf:=UFOKampf;
  Prepared:=TDirectDrawSurface.Create(UFOKampf.Container.DDraw);
  Prepared.SystemMemory:=true;
  Prepared.SetSize(SurSize,SurSize);
  fAlpha:=255;
end;

procedure TSchiffSprite.Draw(Surface: TDirectDrawSurface;const Mem: TDDSurfaceDesc; Left,
  Top: Integer);

  procedure DrawZiel;
  var
    BasisX,BasisY: Integer;
  begin
    BasisX:=Left+round(X)+round(Sin256(round(fAngle))*100);
    BasisY:=Top+round(Y)-round(Cos256(round(fAngle))*100);
    try
      if PtInRect(ResizeRect(fUFOKampf.ClientRect,2),Point(BasisX,BasisY)) then
      begin
        PWord(Integer(Mem.lpSurface)+(BasisY+1)*Mem.lPitch+(BasisX shl 1))^ := bcRed;
        PWord(Integer(Mem.lpSurface)+(BasisY-1)*Mem.lPitch+(BasisX shl 1))^ := bcRed;
        PWord(Integer(Mem.lpSurface)+BasisY*Mem.lPitch+((BasisX+1) shl 1))^ := bcRed;
        PWord(Integer(Mem.lpSurface)+BasisY*Mem.lPitch+((BasisX-1) shl 1))^ := bcRed;
      end;
    except
    end;
  end;

begin
  if not fVisible then exit;
  if fAlpha=255 then
    Surface.Draw(Left+round(X)-HalfSurSize,Top+round(Y)-HalfSurSize,Prepared)
  else
    DrawTransAlpha(Surface,Left+round(X)-HalfSurSize,Top+round(Y)-HalfSurSize,Rect(0,0,SurSize,SurSize),Prepared,fAlpha,bcBlack);
  if fZiel then DrawZiel;
  {$IFDEF DRAWDEBUG}
  Surface.Pixels[Left+X,Top+Y]:=bcRed;
  {$ENDIF}
  {$IFDEF DRAWFRAMEBOX}
  Surface.Canvas.Rectangle(FrameBox);
  Surface.Canvas.Release;
  {$ENDIF}
//  GlobalFile.Write('fAngle',fAngle);
//  fUFOKampf.Container.PowerDraw.PowerD3D.Rotate(fUFOKampf.fSchiffe,Left+round(X),Top+round(Y),50,50,2,fAngle+64);
end;

procedure TSchiffSprite.RotateLeft;
begin
  if fAngleBes>-fMaxBeschl then dec(fAngleBes);
  fAngle:=fAngle+fAngleBes;
  if fAngle<0 then fAngle:=fAngle+256;
  if fAngle>255 then fAngle:=fAngle-256;
  CalcFrameBox;
  CalcPrepared;
end;

procedure TSchiffSprite.RotateRight;
begin
  if fAngleBes<fMaxBeschl then inc(fAngleBes);
  fAngle:=fAngle+fAngleBes;
  if fAngle<0 then fAngle:=fAngle+256;
  if fAngle>255 then fAngle:=fAngle-256;
  CalcFrameBox;
  CalcPrepared;
end;

procedure TSchiffSprite.NoSchub;
begin
  if fBeschl>0 then dec(fBeschl);
  fX:=fX+Sin256(round(fAngle))*fBeschl*fZoom;
  fY:=fY-Cos256(round(fAngle))*fBeschl*fZoom;
  CalcFrameBox;
end;

procedure TSchiffSprite.SetBitmapRect(Surface: TDirectDrawSurface;Rect: TRect);
begin
  fDrawRect:=Rect;
  Source:=Surface;
end;

function TSchiffSprite.GetWinkel: Integer;
begin
  result:=round(fAngle/256*360);
end;

procedure TSchiffSprite.Schub;
begin
  if fBeschl<fMaxBeschl then inc(fBeschl);
  fX:=fX+Sin256(round(fAngle))*(fBeschl*fZoom);
  fY:=fY-Cos256(round(fAngle))*(fBeschl*fZoom);
  CalcFrameBox;
end;

procedure TSchiffSprite.NoRotate;
begin
  if fAngleBes>0 then dec(fAngleBes) else if fAngleBes<0 then inc(fAngleBes);
  if fAngle<0 then fAngle:=fAngle+256;
  if fAngle>255 then fAngle:=fAngle-256;
  fAngle:=fAngle+fAngleBes;
  CalcFrameBox;
  CalcPrepared;
end;

procedure TSchiffSprite.SetStartPos(X, Y, Angle: Integer);
begin
  fX:=X;
  fY:=Y;
//  fAngle:=Angle;
  fAngle:=0;
  fAlpha:=255;
  fBeschl:=0;
  fAngleBes:=0;
  fDueBeschl:=0;
  CalcPrepared;
  CalcFrameBox;
end;

procedure TSchiffSprite.CalcFrameBox;
var
  NewWinkel: Integer;
  X,Y: Integer;
  Winkel256:Integer;
begin
  NewWinkel:=Winkel+315;
  if NewWinkel>359 then dec(NewWinkel,360);
  Winkel256:=round(NewWinkel/360*256);
  X:=fUFOKampf.Left+round(Self.X)+round(Sin256(Winkel256)*35);
  Y:=fUFOKampf.Top+round(Self.Y)-round(Cos256(Winkel256)*35);
  FrameBox.Left:=X;
  FrameBox.Top:=Y;
  FrameBox.Bottom:=Y;
  FrameBox.Right:=X;
  NewWinkel:=Winkel+45;
  if NewWinkel>359 then dec(NewWinkel,360);
  Winkel256:=round(NewWinkel/360*256);
  X:=fUFOKampf.Left+round(Self.X)+round(Sin256(Winkel256)*30);
  Y:=fUFOKampf.Top+round(Self.Y)-round(Cos256(Winkel256)*30);
  FrameBox.Left:=min(X,FrameBox.Left);
  FrameBox.Top:=min(Y,FrameBox.Top);
  FrameBox.Bottom:=max(Y,FrameBox.Bottom);
  FrameBox.Right:=max(X,FrameBox.Right);
  NewWinkel:=Winkel+135;
  if NewWinkel>359 then dec(NewWinkel,360);
  Winkel256:=round(NewWinkel/360*256);
  X:=fUFOKampf.Left+round(Self.X)+round(Sin256(Winkel256)*30);
  Y:=fUFOKampf.Top+round(Self.Y)-round(Cos256(Winkel256)*30);
  FrameBox.Left:=min(X,FrameBox.Left);
  FrameBox.Top:=min(Y,FrameBox.Top);
  FrameBox.Bottom:=max(Y,FrameBox.Bottom);
  FrameBox.Right:=max(X,FrameBox.Right);
  NewWinkel:=Winkel+225;
  if NewWinkel>359 then dec(NewWinkel,360);
  Winkel256:=round(NewWinkel/360*256);
  X:=fUFOKampf.Left+round(Self.X)+round(Sin256(Winkel256)*30);
  Y:=fUFOKampf.Top+round(Self.Y)-round(Cos256(Winkel256)*30);
  FrameBox.Left:=min(X,FrameBox.Left);
  FrameBox.Top:=min(Y,FrameBox.Top);
  FrameBox.Bottom:=max(Y,FrameBox.Bottom);
  FrameBox.Right:=max(X,FrameBox.Right);
end;

procedure TSchiffSprite.DoTreffer(Sender: TWaffenSprite;
  var Destroy: boolean);
begin
  if Assigned(fOnTreffer) then fOnTreffer(Sender,Destroy);
end;

procedure TSchiffSprite.MoveLeft;
begin
  if not fDuesen then exit;
  if fDueBeschl>0 then dec(fDueBeschl);
  if fDueBeschl>-fMaxBeschl then dec(fDueBeschl);
  fX:=fX+Sin256(round(fAngle)-64)*(fDueBeschl*fZoom);
  fY:=fY-Cos256(round(fAngle)-64)*(fDueBeschl*fZoom);
  CalcFrameBox;
end;

procedure TSchiffSprite.MoveRight;
begin
  if not fDuesen then exit;
  if fDueBeschl<0 then inc(fDueBeschl);
  if fDueBeschl<fMaxBeschl then inc(fDueBeschl);
  fX:=fX+Sin256(round(fAngle)-64)*(fDueBeschl*fZoom);
  fY:=fY-Cos256(round(fAngle)-64)*(fDueBeschl*fZoom);
  CalcFrameBox;
end;

procedure TSchiffSprite.NoMove;
begin
  if fDueBeschl>0 then
    dec(fDueBeschl)
  else if fDueBeschl<0 then
    inc(fDueBeschl);
  fX:=fX+Sin256(round(fAngle)-64)*(fDueBeschl*fZoom);
  fY:=fY-Cos256(round(fAngle)-64)*(fDueBeschl*fZoom);
  CalcFrameBox;
end;

procedure TSchiffSprite.DoUFOShoot;
begin
  fUFOKampf.DoUFOShoot;
end;

function TSchiffSprite.GetPos: TFloatPoint;
begin
  result.X:=(fUFOKampf.Width div 2)-((((fUFOKampf.Width div 2)-X)*0.5)/fZoom);
  result.Y:=(fUFOKampf.Height div 2)-((((fUFOKampf.Height div 2)-Y)*0.5)/fZoom);
end;

function TSchiffSprite.GetX: double;
begin
  result:=fX;
end;

function TSchiffSprite.GetY: double;
begin
  result:=fY;
end;

procedure TSchiffSprite.SetX(const Value: double);
begin
  fX:=Value;
end;

procedure TSchiffSprite.SetY(const Value: double);
begin
  fY:=Value;
end;

procedure TSchiffSprite.CalcPrepared;
begin
  Assert(round(Size*fZoom)<>0);
  Prepared.Fill(0);
  Prepared.DrawRotate(HalfSurSize,HalfSurSize,round(Size*fZoom),round(Size*fZoom),fDrawRect,Source,0.5,0.5,false,round(fAngle));
end;

function TSchiffSprite.DoCollisonDetect(X, Y: Integer): boolean;
begin
  result:=(Prepared.Pixels[X,Y]<>0);
end;

procedure TSchiffSprite.Restore;
begin
  Source.Restore;
  Prepared.Restore;
  CalcPrepared;
end;

procedure TSchiffSprite.OffSet(X, Y: Integer);
begin
  fx:=fx+(X*fZoom);
  fY:=fY+(Y*fZoom);
  CalcFrameBox;
end;

procedure TSchiffSprite.CalcNeededOffSet(var OffX, OffY: Integer);
begin
  if X>fUFOKampf.Width-40 then OffX:=(fUFOKampf.Width-40)-round(X);
  if Y>fUFOKampf.Height-40 then OffY:=(fUFOKampf.Height-40)-round(Y);
  if X<40 then OffX:=40-round(X);
  if Y<40 then OffY:=40-round(Y);
end;

procedure TSchiffSprite.SetZoom(const Value: double);
var
  OldZoom: double;
begin
  OldZoom:=fZoom;
  fZoom := Value;
  fX:=(fUFOKampf.Width div 2)-((((fUFOKampf.Width div 2)-X)*fZoom)/OldZoom);
  fY:=(fUFOKampf.Height div 2)-((((fUFOKampf.Height div 2)-Y)*fZoom)/OldZoom);
//  CalcPrepared;
  CalcFrameBox;
end;

procedure TSchiffSprite.DoShoot(Angle, Zelle: Integer);
var
  Winkel    : Integer;
  Winkel256 : Integer;
  StartPos  : TPoint;
  Schuss    : TWaffenSprite;

begin
  // Darf nur im Bereich von -180..180 liegen
  if (NormalizeWinkel(Angle-Self.Winkel)>fZellen[Zelle].MaxWinkel) or (NormalizeWinkel(Angle-Self.Winkel)<fZellen[Zelle].MinWinkel) then
    exit;

  Winkel:=Angle+fZellen[Zelle].Angle;
  if Winkel>359 then
    dec(Winkel,360);

  // Startposition ermitteln
  if fZellen[Zelle].RadiusFromMiddle>0 then
  begin
    Winkel256:=round(Winkel/360*256);
    StartPos.X:=round(X)+round(Sin256(Winkel256)*12*fZoom);
    StartPos.Y:=round(Y)+round(-Cos256(Winkel256)*12*fZoom);
  end
  else
  begin
    StartPos.X:=round(X);
    StartPos.Y:=round(Y);
  end;

  // Schusssprite erstellen
  case fZellen[Zelle].Waffe.WaffType of
    wtProjektil : Schuss:=TProjektilSprite.Create(fUFOKampf.Container,fUFOKampf);
    wtRaketen   : Schuss:=TRaketenSprite.Create(fUFOKampf.Container,fUFOKampf);
    wtLaser     : Schuss:=TLaserSprite.Create(fUFOKampf.Container,fUFOKampf);
    else
      raise Exception.Create('Ung�ltiger Waffentyp');
  end;
  with Schuss do
  begin
    Zoom:=Self.fZoom;
    X:=StartPos.X;
    Y:=StartPos.Y;
    Winkel:=Angle;
    Schiff:=Self;
    PPS:=fZellen[Zelle].Waffe.PixPerSec;
    fStrength:=fZellen[Zelle].Waffe.Strength;
    Verfolgung:=fZellen[Zelle].Waffe.Verfolgung;
    WaffType:=fZellen[Zelle].Waffe.WaffType;
    Reichweite:=min(5000,fZellen[Zelle].Waffe.Reichweite);
    Ziel:=Enemy;
    WZelle:=Zelle+1;
  end;
  Schuss.GenerateBitmap;

  if Assigned(fOnShoot) then
    fOnShoot(Self,Zelle);
end;

procedure TSchiffSprite.AddWaffenZellenInfo(Zellen: TWaffenZellenInfo);
begin
  SetLength(fZellen,length(fZellen)+1);
  fZellen[high(fZellen)]:=Zellen;
end;

procedure TSchiffSprite.ClearWaffenZellen;
begin
  SetLength(fZellen,0);
end;

procedure TSchiffSprite.GenerateSchiffBitmap(Target: TPoint; Dest: TDirectDrawSurface; DestRect: TRect);
var
//  Source   : TDirectDrawSurface;
  TempRect : TRect;

  procedure DrawSchiffRect(Rect: TRect);
  var
    X,Y      : Integer;
  begin
    X:=Rect.Left;
    Y:=Rect.Top;
    TempRect:=fUFOKampf.fDXImage.Items[0].PatternRects[length(fZellen) +1];
    OffSetRect(Rect,TempRect.Left,TempRect.Top);
    Dest.Draw(X,Y,Rect,Source,true);
  end;

  procedure DrawWaffe(Waffe: Integer;X,Y: Integer);
  var
    Index  : Integer;
    Winkel : Integer;
  begin
    Index:=0;
    case fZellen[Waffe].Waffe.WaffType of
      wtProjektil : Index:=0;
      wtLaser     : Index:=1;
      wtRaketen   : Index:=2;
    end;
//    Dest.Draw(X,Y,fDXImage.Items[1].PatternRects[Index],fDXImage.Items[1].PatternSurfaces[Index],true);
    Winkel:=CalculateWinkel(Target,Point(round(Self.X)+X-HalfSize,round(Self.Y)+Y-HalfSize));
    Winkel:=NormalizeWinkel(Winkel-Self.Winkel);
    Winkel:=max(min(Winkel,fZellen[Waffe].MaxWinkel),fZellen[Waffe].MinWinkel);
    Dest.DrawRotate(X+5,Y+8,10,17,fUFOKampf.fDXImage.Items[1].PatternRects[Index],fUFOKampf.fDXImage.Items[1].PatternSurfaces[Index],0.5,0.5,true,Winkel*256 div 360);
  end;

begin
  if not Destroyed then
  begin
    Dest.FillRect(DestRect,0);
    case length(fZellen) of
      1:
      begin
        if fZellen[0].Waffe.Installiert then DrawWaffe(0,22,19);
        DrawSchiffRect(Rect(0,0,Size,Size));
      end;
      2:
      begin
        DrawSchiffRect(Rect(0,0,Size,Size));
        if fZellen[0].Waffe.Installiert then DrawWaffe(0,17,22);
        if fZellen[1].Waffe.Installiert then DrawWaffe(1,26,22);
        DrawSchiffRect(Rect(16,27,30,35));
      end;
      3:
      begin
        if fZellen[0].Waffe.Installiert then DrawWaffe(0,11,24);
        if fZellen[1].Waffe.Installiert then DrawWaffe(1,31,24);
        DrawSchiffRect(Rect(0,0,Size,Size));
        if fZellen[2].Waffe.Installiert then DrawWaffe(2,21,30);
        DrawSchiffRect(Rect(21,36,26,44));
      end;
    end;
  end;

  CalcPrepared;
end;

{ TWaffenSprite }

constructor TWaffenSprite.Create(DXDraw: TDXDraw; UFOKampf: TDXUFOKampf);
begin
  fAngle:=0;
  fReichweite:=1000;
  fBeschl:=1;
  fZoom:=1;
  fX:=0;
  fY:=0;
  fUFOKampf:=UFOKampf;
  if UFOKampf<>nil then UFOKampf.AddSchuss(Self);
  fSchiff:=nil;
end;

destructor TWaffenSprite.destroy;
begin
  fUFOKampf.DeleteSchuss(Self);
  inherited;
end;

function TWaffenSprite.DoMove: boolean;
var
  OffSetRect  : TRect;
  Dummy       : Integer;
  ZielWinkel  : Integer;
  IstWinkel   : Integer;
  AbstandL    : Integer;
  AbstandR    : Integer;
  ZielObjekt  : TSchiffSprite;
  Destroyed   : boolean;
  XAdd,YAdd   : double;
begin
  IncBeschleunigung;
  if fBeschl>fMaxGeschw then fBeschl:=fMaxGeschw;
  if (fVerfolgung) and (not fZiel.Destroyed) then
  begin
    ZielWinkel:=CalculateWinkel(fZiel.Pos,Pos);
    IstWinkel:=Winkel;
    if IstWinkel>ZielWinkel then AbstandL:=abs(IstWinkel-360-ZielWinkel) else AbstandL:=(ZielWinkel-IstWinkel);
    if IstWinkel<ZielWinkel then AbstandR:=(IstWinkel+360)-ZielWinkel else AbstandR:=(IstWinkel-ZielWinkel);
    if AbstandL<AbstandR then
    begin
      if CalculateAbstand(round(IstWinkel/360*256),round(ZielWinkel/360*256))>round(fBeschl/2) then
        Winkel:=Winkel+round(fBeschl/2)
      else
        Winkel:=ZielWinkel;
    end
    else
    begin
      if CalculateAbstand(round(IstWinkel/360*256),round(ZielWinkel/360*256))>round(fBeschl/2) then
        Winkel:=Winkel-round(fBeschl/2)
      else
        Winkel:=ZielWinkel;
    end;
  end;
  result:=true;
  XAdd:=(Sin256(round(fAngle))/1.5)*fZoom;
  YAdd:=(-Cos256(round(fAngle))/1.5)*fZoom;
  OffSetRect:=Rect(0,0,fUFOKampf.Width,fUFOKampf.Height);
  for Dummy:=1 to round(fBeschl) do
  begin
    fReichweite:=fReichweite-1;
    if fReichweite<0 then
    begin
      result:=false;
      Free;
      exit;
    end;
    fX:=fX+XAdd;
    fY:=fY+YAdd;
    if (fX<0) or (fX>fUFOKampf.Width) or (fY<0) or (fY>fUFOKampf.Height) then continue;
    ZielObjekt:=fUFOKampf.GetSchiffSprite(Point(X,Y));
    if (not (ZielObjekt=nil)) and (ZielObjekt<>fSchiff) and (not ZielObjekt.Destroyed) then
    begin
      if ZielObjekt.DoCollisonDetect(X-round(ZielObjekt.X)+HalfSurSize,Y-round(ZielObjekt.Y)+HalfSurSize) then
      begin
        Destroyed:=false;
        ZielObjekt.DoTreffer(Self,Destroyed);
        {$IFNDEF DRAWDEBUG}
        if Destroyed then
        begin
          if ZielObjekt=Schiff then
            fUFOKampf.Container.PlayMovie('videos/ufokampf_lose.avi',Rect(160,120,320,240))
          else
            fUFOKampf.Container.PlayMovie('videos/ufokampf_win.avi',Rect(160,120,320,240));
            
          fUFOKampf.Container.PlaySound(SExplosion);
          with TExplosionSprite.Create(fUFOKampf.Container,fUFOKampf) do
          begin
            SetBitmap(fUFOKampf.GetAGFImage('Explosion'),ExploFrames,fUFOKampf.Zoom);
//            Source:=nil;
            X:=round(ZielObjekt.X);
            Y:=round(ZielObjekt.Y);
            Angle:=ZielObjekt.Winkel;
            Data:=Integer(ZielObjekt.Typ);
            OnChange:=fUFOKampf.AnimFrame;
            AnimTime:=25;
          end;
        end;
        {$ENDIF}
        result:=false;
        Free;
        exit;
      end;
    end;
  end;
end;

procedure TWaffenSprite.Draw(Surface: TDirectDrawSurface; const Mem: TDDSurfaceDesc; Left,
  Top: Integer);
begin
end;

procedure TWaffenSprite.GenerateBitmap;
begin
end;

function TWaffenSprite.GetPos: TFloatPoint;
begin
  result.X:=(fUFOKampf.Width div 2)-((((fUFOKampf.Width div 2)-X)*0.5)/fZoom);
  result.Y:=(fUFOKampf.Height div 2)-((((fUFOKampf.Height div 2)-Y)*0.5)/fZoom);
end;

function TWaffenSprite.GetWinkel: Integer;
begin
  result:=round(fAngle/256*360);
end;

function TWaffenSprite.GetX: Integer;
begin
  result:=round(fX);
end;

function TWaffenSprite.GetY: Integer;
begin
  result:=round(fY);
end;

procedure TWaffenSprite.IncBeschleunigung;
begin
  fBeschl:=min(fMaxGeschw,fBeschl+0.3);
end;

procedure TWaffenSprite.OffSet(X, Y: Integer);
begin
  fX:=fX+(X*fZoom);
  fY:=fY+(Y*fZoom);
end;

procedure TWaffenSprite.SetMaxGeschw(const Value: Integer);
begin
  fMaxGeschw := Value;
end;

procedure TWaffenSprite.SetWinkel(const Value: Integer);
var
  NewAngle: Integer;
begin
  NewAngle:=Value;
  while NewAngle>360 do dec(NewAngle,360);
  fAngle:=NewAngle/360*256;
end;

procedure TWaffenSprite.SetX(const Value: Integer);
begin
  fx:=Value;
end;

procedure TWaffenSprite.SetY(const Value: Integer);
begin
  fy:=Value;
end;

procedure TWaffenSprite.SetZoom(const Value: double);
var
  OldZoom: double;
begin
  OldZoom:=fZoom;
  fZoom := Value;
  fX:=(fUFOKampf.Width div 2)-(((fUFOKampf.Width div 2)-fX)*(fZoom/OldZoom));
  fY:=(fUFOKampf.Height div 2)-(((fUFOKampf.Height div 2)-fY)*(fZoom/OldZoom));
end;

{ TAnimSprite }

procedure TAnimSprite.AnimationSet;
begin
end;

constructor TAnimSprite.Create(DXDraw: TDXDraw; UFOKampf: TDXUFOKampf);
begin
  fUFOKampf:=UFOKampf;
  UFOKampf.AddSprite(Self);
  fImage:=0;
  fZoom:=1;
end;

destructor TAnimSprite.destroy;
begin
  fUFOKampf.DeleteSprite(Self);
  inherited;
end;

procedure TAnimSprite.DoNextFrame;
begin
  if Assigned(fOnChange) then fOnChange(Self,fImage);
end;

procedure TAnimSprite.Draw(Surface: TDirectDrawSurface; Left,
  Top: Integer);
begin
//  Surface.DrawRotate(Left+X,Top+Y,fImageWidth,fImageHeight,Rect((fImage*fImageWidth),0,((fImage+1)*fImageWidth),fImageHeight),fSurface,0.5,0.5,true,fAngle);
end;

function TAnimSprite.GetX: Integer;
begin
  result:=round(fX);
end;

function TAnimSprite.GetY: Integer;
begin
  result:=round(fY);
end;

procedure TAnimSprite.OffSet(X, Y: Integer);
begin
  fX:=fX+(X*fZoom);
  fY:=fY+(Y*fZoom);
end;

procedure TAnimSprite.SetAnimTime(const Value: Integer);
begin
  fAnimTime := Value;
  fNextFrame:=Value;
end;

procedure TAnimSprite.SetBitmap(Source: TAGFImage; Images: Integer; Zoom: double);
begin
//  fSurface.SetSize(Rect.Right-Rect.Left,Rect.Bottom-Rect.Top);
//  fSurface.Draw(0,0,Rect,Surface,false);
//  fSurface.TransparentColor:=fSurface.ColorMatch(clBlack);
//  fImageWidth:=fSurface.Width div Images;
//  try
//    fImageHeight:=fSurface.Height;
//  except
//  end;
  fSource:=Source;
  fCount:=Images;
  fImage:=0;
  AnimationSet;
  fZoom:=Zoom;
end;

procedure TAnimSprite.SetX(const Value: Integer);
begin
  fX:=Value;
end;

procedure TAnimSprite.SetY(const Value: Integer);
begin
  fY:=Value;
end;

procedure TAnimSprite.SetZoom(const Value: double);
var
  OldZoom: double;
begin
  OldZoom:=fZoom;
  fZoom := Value;
  fX:=(fUFOKampf.Width div 2)-(((fUFOKampf.Width div 2)-fX)*(fZoom/OldZoom));
  fY:=(fUFOKampf.Height div 2)-(((fUFOKampf.Height div 2)-fY)*(fZoom/OldZoom));
end;

function TAnimSprite.Timer(Lag: Integer): boolean;
begin
  result:=false;
  dec(fNextFrame,Lag);
  if fNextFrame<=0 then
  begin
    inc(fNextFrame,fAnimTime);
    inc(fImage,1);
    if fImage>=fCount then
    begin
      { Wenn das Letzte Frame erreicht ist, Objekt freigeben }
      result:=true;
      Free;
      exit;
    end;
    DoNextFrame;
  end;
end;

{ TSchiffKI }

procedure TSchiffKI.AbstandGewinnen;
var
  FluchtWinkel: Integer;
begin
  // Auf Fluchtkurs gehen
  FluchtWinkel:=CalculateWinkel(fEnemy.GetPos,fSchiff.GetPos)-160;
  if FluchtWinkel<0 then inc(FluchtWinkel,360);
  Rotate(round(FluchtWinkel/360*256));
  // Gas geben
  GibSchub;
  if (CalculateEntfern(fEnemy.GetPos,fSchiff.GetPos))>400 then
  begin
    SearchManover;
  end;
end;

procedure TSchiffKI.Attack;
begin
  // Wenn das UFO genau auf das Raumschiff zielt, dann Schub geben
  if abs(CalculateAbstand(CalculateWinkel(fEnemy.GetPos,fSchiff.GetPos),fSchiff.Winkel))<3 then
  begin
    if CalculateEntfern(fEnemy.GetPos,fSchiff.GetPos)>Weite then GibSchub;
  end;
end;

procedure TSchiffKI.Ausweichen;
begin
  if fAusWeichCounter=-1 then
  begin
    dec(fPause);
    CorrectAngle;
    Attack(150);
    if fPause>0 then exit;
    fPause:=20;
    fAusweichCounter:=min(fSchiff.fMaxBeschl*2,15);
    fLeft:=not fLeft;
  end;
  dec(fAusweichCounter);
  CorrectAngle;
  Move(fLeft);
  Attack(150);
end;

procedure TSchiffKI.CorrectAngle;
var
  Winkel: Integer;
begin
  Winkel:=round(CalculateWinkel(fEnemy.GetPos,fSchiff.GetPos)/360*256);
  Rotate(Winkel);
end;

constructor TSchiffKI.Create(Schiff, Enemy: TSchiffSprite);
begin
  fSchiff:=Schiff;
  fPause:=15;
  fEnemy:=Enemy;
  Reset;
end;

procedure TSchiffKI.DoMoveArt;
begin
  if not fMoveArt.Schub then fSchiff.NoSchub;
  if not fMoveArt.Rotate then fSchiff.NoRotate;
  if not fMoveArt.Move then fSchiff.NoMove;
end;

procedure TSchiffKI.DoRound;
begin
  inc(Count);
  if Count=100 then
    SearchManover;
  InitMoveArt;

{  // Wenn der Schaden gr��er als 90 % dann die Flucht ergreifen
  if fSchiff.Schaden>=90 then
  begin
    fManover:=Flucht;
  end;}
  if not Assigned(fManover) then SearchManover;
  fManover;
  DoMoveArt;
  // Nur schiessen wenn das UFO genau auf das Raumschiff zielt
  if CalculateAbstand(CalculateWinkel(fEnemy.GetPos,fSchiff.GetPos),fSchiff.Winkel)=0 then fSchiff.DoUFOShoot;
end;

procedure TSchiffKI.Flucht;
var
  FluchtWinkel: Integer;
begin
  // Auf Fluchtkurs gehen
  FluchtWinkel:=CalculateWinkel(fEnemy.GetPos,fSchiff.GetPos)-160;
  if FluchtWinkel<0 then inc(FluchtWinkel,360);
  Rotate(round(FluchtWinkel/360*256));
  // Gas geben
  GibSchub;
end;

procedure TSchiffKI.GibSchub;
begin
  // Nur Schub geben, falls in dieser Runde noch kein Schub durchgef�hrt wurde
  if not fMoveArt.Schub then
  begin
    fSchiff.Schub;
    fMoveArt.Schub:=true;
  end;
end;

procedure TSchiffKI.InitMoveArt;
begin
  fMoveArt.Schub:=false;
  fMoveArt.Rotate:=false;
  fMoveArt.Move:=false;
end;

procedure TSchiffKI.Move(Links: boolean);
begin
  if not fMoveArt.Move then
  begin
    if Links then fSchiff.MoveLeft else fSchiff.MoveRight;
    fMoveArt.Move:=true;
  end;
end;

procedure TSchiffKI.Reset;
begin
  fLeft:=boolean(random(2));
  fAusweichCounter:=fSchiff.MaxBeschl div 2;
  fManover:=nil;
end;

procedure TSchiffKI.Rotate(Angle: Integer);
var
  AbstandL     : Integer;
  AbstandR     : Integer;
  AngleSchub   : Integer;
  SchiffAngle  : Integer;
  BesAngle     : Integer;
  Abstand      : Integer;
begin
  AngleSchub:=0;
  if fSchiff.fAngleBes<>0 then
  begin
    BesAngle:=abs(fSchiff.fAngleBes);
    if Odd(BesAngle) then AngleSchub:=BesAngle*((BesAngle-1) shr 1)
      else AngleSchub:=BesAngle*((BesAngle-2) shr 1)+(BesAngle shr 1);
    if fSchiff.fAngleBes<0 then AngleSchub:=-AngleSchub;
  end;
  SchiffAngle:=round(fSchiff.fAngle)+AngleSchub;
  if Angle>SchiffAngle then AbstandL:=abs(Angle-256-SchiffAngle) else AbstandL:=(SchiffAngle-Angle);
  if Angle<SchiffAngle then AbstandR:=(Angle+256)-SchiffAngle else AbstandR:=(Angle-SchiffAngle);
  if AbstandL<AbstandR then
  begin
    if not fMoveArt.Rotate then
    begin
      Abstand:=CalculateAbstand(SchiffAngle,Angle);
      if (Abstand>abs(AngleSchub)+abs(fSchiff.fAngleBes)) then
      begin
        Rotiere(true);
      end;
    end;
  end
  else if (AbstandR or AbstandL)<>0 then
  begin
    if not fMoveArt.Rotate then
    begin
      Abstand:=CalculateAbstand(SchiffAngle,Angle);
      if (Abstand>abs(AngleSchub)+abs(fSchiff.fAngleBes)) then
      begin
        Rotiere(false);
      end;
    end;
  end;
end;

procedure TSchiffKI.Rotiere(Links: boolean);
begin
  if not fMoveArt.Rotate then
  begin
    if Links then fSchiff.RotateLeft else fSchiff.RotateRight;
    fMoveArt.Rotate:=true;
  end;
end;

procedure TSchiffKI.SearchManover;
begin
  Count:=0;
  case random(100) of
    0..33   : fManover:=Ausweichen;
    34..66  : fManover:=SelbstMord;
    67..100 : fManover:=AbstandGewinnen;
  end;
end;

procedure TSchiffKI.SelbstMord;
begin
  // das UFO zum Raumschiff drehen und hinfliegen
  CorrectAngle;
  Attack(100);
end;

{ TLaserSprite }

constructor TLaserSprite.Create(DXDraw: TDXDraw; UFOKampf: TDXUFOKampf);
begin
  inherited;
  fFrom.X:=-1;
  fSchweifLength:=50;
  fFrom.Y:=-1;
  UFOKampf.Container.PlaySound(SLaserFire);
end;

function TLaserSprite.DoMove: boolean;
begin
  result:=inherited DoMove;
  if result then
  begin
    if fSchweifLength>0 then dec(fSchweifLength,round(fBeschl))
    else
    begin
      fFrom.X:=fFrom.X+(Sin256(round(fAngle))/1.5*fZoom*fBeschl);
      fFrom.Y:=fFrom.Y+(-Cos256(round(fAngle))/1.5*fZoom*fBeschl);
    end;
  end;
end;

procedure TLaserSprite.Draw(Surface: TDirectDrawSurface; const Mem: TDDSurfaceDesc; Left,Top: Integer);
begin
//  AALine(Surface,Mem,round(fFrom.X),round(fFrom.Y),Left+X,Top+Y,LaserColTable[(Strength div 15)]);
  fUFOKampf.Container.PowerDraw.Lock(Surface);
  fUFOKampf.Container.PowerDraw.WuLine(round(fFrom.X),round(fFrom.Y),Left+X,Top+Y,LaserColTable[(Strength div 15)],255);
  fUFOKampf.Container.PowerDraw.Unlock;
end;

procedure TLaserSprite.OffSet(X, Y: Integer);
begin
  inherited;
  fFrom.X:=fFrom.X+(X*fZoom);
  fFrom.Y:=fFrom.Y+(Y*fZoom);
end;

procedure TLaserSprite.SetMaxGeschw(const Value: Integer);
begin
  fMaxGeschw := Value*6;
  fBeschl:=Value*6;
end;

procedure TLaserSprite.SetX(const Value: Integer);
begin
  inherited;
  if fFrom.X=-1 then
    fFrom.X:=Value;
end;

procedure TLaserSprite.SetY(const Value: Integer);
begin
  inherited;
  if fFrom.Y=-1 then
    fFrom.Y:=Value;
end;

procedure TLaserSprite.SetZoom(const Value: double);
var
  OldZoom: double;
begin
  OldZoom:=fZoom;
  inherited;
  if fFrom.X<>-1 then
  begin
    fFrom.X:=(fUFOKampf.Width div 2)-(((fUFOKampf.Width div 2)-fFrom.X)*(fZoom/OldZoom));
    fFrom.Y:=(fUFOKampf.Height div 2)-(((fUFOKampf.Height div 2)-fFrom.Y)*(fZoom/OldZoom));
  end;
end;

{ TExplosionSprite }

constructor TExplosionSprite.Create(DXDraw: TDXDraw;
  UFOKampf: TDXUFOKampf);
begin
  inherited;
  fAlpha:=0;
  fZoom:=0.1;
  fAutoZoom:=true;
end;

destructor TExplosionSprite.destroy;
begin
//  if not fAutoZoom then fSurface:=nil;
  inherited;
end;

procedure TExplosionSprite.DoNextFrame;
begin
  inherited;
//  fAlpha:=min(fAlpha+fAlphaInc,255);
//  fSurface.Fill(0);
//  fSurface.DrawRotate(fImageWidth div 2,fImageHeight div 2,round(fZoom*fImageWidth),round(fZoom*fImageHeight),Rect(fImage*fImageWidth,fImageHeight*fPattern,(fImage+1)*fImageWidth,fImageHeight*(fPattern+1)),fSource,0.5,0.5,false,fAngle);
end;

procedure TExplosionSprite.Draw(Surface: TDirectDrawSurface; Left,
  Top: Integer);
var
  StartX,StartY  : Integer;
begin
  if fSource=nil then exit;
  StartX:=GetX+Left;             // Absolute X-Position
  StartY:=GetY+Top;             // Absolute Y-Position
//  GlobalFile.Write('Width',fSource.D3DTexture.Texture[0].TextureFormat.dwWidth);
//  );
  fUFOKampf.Container.PowerDraw.Lock(Surface);
  fUFOKampf.Container.PowerDraw.RotateAdd(fSource,StartX,StartY,fImage,fAngle,round(255*fZoom));
  fUFOKampf.Container.PowerDraw.UnLock;
//  fUFOKampf.Container.PowerDraw.PowerD3D.RotateAdd(fSource,StartX,StartY,fImage,fAngle,round(256*fZoom));
end;

procedure TExplosionSprite.SetZoom(const Value: double);
begin
//  if Value=fZoom then exit;
  inherited;
//  fImageWidth:=round(fZoom*fOrSize);
//  fImageHeight:=fImageWidth;
{  if fAutoZoom then
  begin
    fSurface.SystemMemory:=false;
    fSurface.SetSize(fImageWidth*fCount,fImageHeight);
//    fSurface.Fill($FFFF);
    fSurface.ClippingRect:=fSurface.ClientRect;
    fSurface.StretchDraw(fSurface.ClientRect,Rect(0,fPattern*fOrSize,fSource.Width,(fPattern+1)*fOrSize),fSource,false);
  end;}
end;

procedure TExplosionSprite.SetZoomedSurface(Surface: TDirectDrawSurface);
begin
  fAutoZoom:=false;
//  fSurface.Free;
//  fSurface:=Surface;
end;

{ TRaketenSprite }

constructor TRaketenSprite.Create(DXDraw: TDXDraw; UFOKampf: TDXUFOKampf);
begin
  inherited;
  UFOKampf.Container.PlaySound(SRocketFire);
  fImage:=TDirectDrawSurface.Create(DXDraw.DDraw);
  fImage.SystemMemory:=true;
  fImage.TransparentColor:=0;
  fImage.SetSize(17,17);
end;

destructor TRaketenSprite.Destroy;
begin
  fImage.Free;
  inherited;
end;

function TRaketenSprite.DoMove: boolean;
begin
  with TExplosionSprite.Create(fUFOKampf.Container,fUFOKampf) do
  begin
    SetBitmap(fUFOKampf.GetAGFImage('RaketenSchweif'),16,fUFOKampf.Zoom);
    X:=round(Self.fX);
    Y:=round(Self.fY);
    AnimTime:=25;
//    SetZoomedSurface(fUFOKampf.fExpSurface);
//    GlobalFile.Write('Zoom',true,Integer(Source));
  end;
  result:=inherited DoMove;
end;

procedure TRaketenSprite.Draw(Surface: TDirectDrawSurface;const Mem: TDDSurfaceDesc; Left,
  Top: Integer);
begin
  Surface.DrawRotate(Left+round(fX),Top+round(fY),17,17,Rect(0,0,17,17),fImage,0.5,0.5,true,round(fAngle));
end;

procedure TRaketenSprite.GenerateBitmap;
begin
  fImage.Draw(5,0,fUFOKampf.fDXImage.Items[1].PatternRects[3],fUFOKampf.fDXImage.Items[1].PatternSurfaces[3]);
end;

procedure TRaketenSprite.IncBeschleunigung;
begin
  fBeschl:=fBeschl+0.15;
end;

procedure TRaketenSprite.SetMaxGeschw(const Value: Integer);
begin
  fMaxGeschw := Value;
  fBeschl:=Value div 2;
end;

{ TProjektilSprite }

constructor TProjektilSprite.Create(DXDraw: TDXDraw;
  UFOKampf: TDXUFOKampf);
begin
  inherited;
  fImage:=TDirectDrawSurface.Create(DXDraw.DDraw);
  fImage.SystemMemory:=true;
  fImage.SetSize(4,4);
end;

destructor TProjektilSprite.Destroy;
begin
  inherited;
  fImage.Free;
end;

procedure TProjektilSprite.Draw(Surface: TDirectDrawSurface;const Mem: TDDSurfaceDesc; Left,
  Top: Integer);
begin
  Surface.Draw(Left+round(fX)-2,Top+round(fY)-2,fImage);
end;

procedure TProjektilSprite.GenerateBitmap;
begin
  fImage.Pixels[1,0]:=bcWhite;
  fImage.Pixels[0,1]:=bcWhite;
  fImage.Pixels[1,1]:=bcWhite;
  fImage.Pixels[2,1]:=bcWhite;
  fImage.Pixels[1,2]:=bcWhite;
end;

procedure TProjektilSprite.SetMaxGeschw(const Value: Integer);
begin
  fMaxGeschw := Value;
  fBeschl := Value;
end;

{ TShieldSprite }

constructor TShieldSprite.Create(DXDraw: TDXDraw; UFOKampf: TDXUFOKampf);
begin
  inherited;
  fAlpha:=0;
  fAngle:=0;
  fFrame:=0;
end;

procedure TShieldSprite.DoNextFrame;
begin
  inherited;
  inc(fFrame);
end;

procedure TShieldSprite.Draw(Surface: TDirectDrawSurface; Left,
  Top: Integer);
begin
//  fUFOKampf.Container.PowerDraw.PowerD3D.RotateAdd(fUFOKampf.fSchield,Left+X-(ShieldHeight div 2),Top+Y-(ShieldHeight div 2),fFrame,bcGreen,bcRed,bcYellow,bcWhite,fAngle,128);
//  fUFOKampf.Container.PowerDraw.Circle(200,200,10,100,Color,255);
  assert(fSource<>nil,'Grafik f�r Schutzschild fehlerhaft');
  fUFOKampf.Container.PowerDraw.Lock(Surface);
  fUFOKampf.Container.PowerDraw.RotateAddCol(fSource,Left+X,Top+Y,fFrame,fAngle,round(255*fZoom),Color);
  fUFOKampf.Container.PowerDraw.Unlock;
//  DrawShieldEffect(Surface,Left+X-(ShieldHeight div 2),Top+Y-(ShieldHeight div 2),Rect(0,0,ShieldHeight-1,ShieldHeight-1),fSurface,Color,255);
end;

procedure TShieldSprite.SetZoom(const Value: double);
begin
  inherited;
  if fFollowSprite=nil then exit;
  fX:=fFollowSprite.X;
  fY:=fFollowSprite.Y;
end;

{ TStarField }

constructor TStarField.Create;
begin
  fZoom:=1;
end;

procedure TStarField.DoTick;
var
  Dummy : Integer;

  procedure MoveStar(var Star: TStar);
  begin
    with Star do
    begin
      if Moving then
      begin
        XPos:=XPos+XMove;
        YPos:=YPos+YMove;
        if XPos<-639 then XPos:=XPos+1280;
        if XPos>639 then XPos:=XPos-1280;
        if YPos<-479 then YPos:=YPos+960;
        if YPos>479 then YPos:=YPos-960;
      end;
      if Pulsar then
      begin
        if Increase then
        begin
          inc(Farbe);
          if Farbe>FromFar then
          begin
            Farbe:=FromFar;
            Increase:=false;
          end;
        end
        else
        begin
          dec(Farbe);
          if Farbe<ToFar then
          begin
            Farbe:=ToFar;
            Increase:=true;
          end;
        end;
      end;
    end;
  end;

begin
  for Dummy:=fStars-1 downto 0 do
  begin
    MoveStar(Stars[Dummy]);
  end;
end;

procedure TStarField.DrawStarField(Surface: TDirectDrawSurface;const Mem: TDDSurfaceDesc);
var
  Dummy : Integer;
  X     : Integer;
  Y     : Integer;
  LineP : Integer;
  LPit  : Integer;

  procedure DrawStar32(XPos,YPos: Integer;Farbe: Integer);
  var
    Pointer1: Integer;
    LineP   : Integer;
  begin
    if not PtInRect(Rect(-((Width div 2)-1),-((Height div 2)-1),(Width div 2)-1,((Height div 2)-1)),Point(XPos,YPos)) then exit;
    inc(XPos,(Width div 2));
    inc(YPos,(Height div 2));
    LineP:=Integer(Mem.lpSurface)+(YPos*Mem.lPitch)+(XPos shl 2);
    with Colors[Farbe] do
    begin
      Pointer1:=LineP;
      PLongWord(Pointer1)^:=Center or (PLongWord(Pointer1)^ and $FF000000);
      Pointer1:=LineP-3;
      PLongWord(Pointer1)^:=Aussen or (PLongWord(Pointer1)^ and $FF000000);
      Pointer1:=LineP+3;
      PLongWord(Pointer1)^:=Aussen or (PLongWord(Pointer1)^ and $FF000000);
      Pointer1:=LineP-Mem.LPitch;
      PLongWord(Pointer1)^:=Aussen or (PLongWord(Pointer1)^ and $FF000000);
      Pointer1:=LineP+Mem.LPitch;
      PLongWord(Pointer1)^:=Aussen or (PLongWord(Pointer1)^ and $FF000000);
    end;
  end;

begin
  if Mode32Bit then
  begin
    for Dummy:=0 to fStars-1 do
    begin
      with Stars[Dummy] do DrawStar32(round(Xpos*fZoom),round(Ypos*fZoom),Farbe);
    end;
  end
  else
  begin
    LPit:=Mem.lPitch;
    for Dummy:=0 to fStars-1 do
    begin
      with Stars[Dummy] do
      begin
        X:=round(Xpos*fZoom);
        Y:=round(Ypos*fZoom);
        if PtInRect(ClipStarRect,Point(X,Y)) then
        begin
          inc(X,(Width div 2));
          inc(Y,(Height div 2));
          LineP:=Integer(Mem.lpSurface)+(Y*Mem.lPitch)+(X shl 1);
          with Colors[Farbe] do
          begin
            PWord(LineP)^:=Center;
            PWord(LineP+2)^:=Aussen;
            PWord(LineP-2)^:=Aussen;
            PWord(LineP+LPit)^:=Aussen;
            PWord(LineP-LPit)^:=Aussen;
          end;
        end;
      end;
    end;
  end;
end;

procedure TStarField.Init(Stars: Integer);
var
  Dummy  : Integer;
begin
  fStars:=Stars;
  SetLength(Self.Stars,fStars);
  for Dummy:=0 to Stars-1 do
  begin
    with Self.Stars[Dummy] do
    begin
      XPos:=fWidth-random(fWidth*2);
      YPos:=fHeight-random(fHeight*2);
      ZOrder:=random;
      Farbe:=random(255);
      Moving:=RandomChance(5);
      if Moving then
      begin
        XMove:=1-(random*2);
        YMove:=1-(random*2);
      end;
      Pulsar:=RandomChance(50);
      if Pulsar then
      begin
        FromFar:=Farbe;
        ToFar:=random(50);
        Increase:=false;
      end;
    end;
  end;
end;

procedure TStarField.InitColor(Surface: TDirectDrawSurface);
var
  Dummy: Integer;
  R,G,B: Integer;
  RMask: Cardinal;
  BMask: Cardinal;
  GMask: Cardinal;
  GCou : Cardinal;
begin
  RMask:=Surface.SurfaceDesc.ddpfPixelFormat.dwRBitMask;
  BMask:=Surface.SurfaceDesc.ddpfPixelFormat.dwBBitMask;
  GMask:=Surface.SurfaceDesc.ddpfPixelFormat.dwGBitMask;
  if RMask=$0000F800 then GCou:=6 else GCou:=5;
  for Dummy:=0 to 255 do
  begin
    R:=min(Dummy+Random(50),255);
    G:=Dummy;
    B:=min(Dummy+random(50),255);
    if Mode32Bit then
    begin
      Colors[Dummy].Center:=(R shl 16) or (G shl 8) or B;
      Colors[Dummy].Aussen:=((R shl 14) and RMask) or ((G shl 6) and GMask) or ((B shr 2) and BMask);
    end
    else
    begin
      Colors[Dummy].Center:=((R shl (GCou+2)) and RMask) or ((G shl (GCou-3)) and GMask) or ((B shr 3) and BMask);
      Colors[Dummy].Aussen:=((R shl (GCou)) and RMask) or ((G shl (GCou-5)) and GMask) or ((B shr 5) and BMask);
    end
  end;
end;

procedure TStarField.Move(X, Y: Integer);
var
  Dummy: Integer;
begin
  for Dummy:=0 to fStars-1 do
  begin
    with Stars[Dummy] do
    begin
      XPos:=XPos+X*ZOrder;
      YPos:=YPos+Y*ZOrder;
      if XPos<-639 then XPos:=XPos+1280;
      if XPos>639 then XPos:=XPos-1280;
      if YPos<-479 then YPos:=YPos+960;
      if YPos>479 then YPos:=YPos-960;
    end;
  end;
end;

procedure TStarField.SetZoom(const Value: double);
begin
  if fZoom=max(0.5,min(Value,1)) then exit;
  fZoom := max(min(Value,1),0.5);
end;

end.

