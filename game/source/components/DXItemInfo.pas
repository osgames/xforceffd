{********************************************************************************
*										*
* Copyright (C) 2004  X-Force Team - jim_raynor@web.de				*
*										*
* This file is part of X-Force: Fight For Destiny (X-Force).			*
*										*
* X-Force is free software; you can redistribute it and/or modify		*
* it under the terms of the GNU General Public License as published by		*
* the Free Software Foundation; either version 2 of the License, or		*
* (at your option) any later version.						*
*										*
* X-Force is distributed in the hope that it will be useful,			*
* but WITHOUT ANY WARRANTY; without even the implied warranty of		*
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the			*
* GNU General Public License for more details.					*
*										*
* You should have received a copy of the GNU General Public License		*
* along with X-Force; if not, write to the Free Software			*
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA	*
*										*
*********************************************************************************
*										*
* Anzeige der Informationen zu Ausrüstungen, Soldaten, ALiens ....		*
*										*
********************************************************************************}

{$I ../Settings.inc}

unit DXItemInfo;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  DXContainer, DXDraws, XForce_types, NGTypes, KD4Utils, math, Defines, Blending,
  TraceFile, RaumschiffList, UFOList, DirectDraw, DXBitmapButton, DXTextViewer,
  DXScrollBar, DirectFont, GameFigureManager, NotifyList, string_utils,
  BasisListe, StringConst;

type
  TAlphaTable = Array[0..14] of Integer;
const
  AlphaValues: Array[boolean] of TAlphaTable = ((64,96,128,160,192,224,240,255,240,224,192,160,128,96,64),
                                                  (32,48,64,80,96,112,120,128,120,112,96,80,64,48,32));

  LineSkip  = 5;
  EntrySkip = 18;

type
  TDrawLineProc      = procedure(var DrawTop: Integer) of object;
  TDrawEntryProc     = procedure(var DrawTop: Integer;Text1: String;Text2: String) of object;
  TDrawPropertyProc  = procedure(var DrawTop: Integer;Text1: String;Text2: String;prop: double;Increased: boolean) of object;

  TDXItemInfo = class(TDXComponent)
  private
    fCaption        : String;
    fColor          : TColor;
    fInfoType       : TInfoType;
    fSoldat         : TSoldatInfo;

    fSoldatManager  : TGameFigureManager;
    fSoldatEvent    : TEventHandle;
    fSoldatDestroy  : TEventHandle;

    fLagerItem      : TLagerItem;
    fInfos          : TInfos;
    fProduktion     : TProduktion;
    fEinrichtung    : TEinrichtung;
    fEinStatus      : boolean;
    fOrganisation   : TLand;
    fBasis          : TBasis;
    fBasisStatus    : TBasis;
    fModel          : TRaumschiffModel;
    fRaumschiff     : TRaumschiff;
    fUFOModel       : TUFOModel;
    fUFOKampf       : boolean;
    fUFO            : TUFO;
    fProjekt        : TForschungen;
    fName           : String;
    fBlendColor     : TBlendColor;
    fCorners        : TRoundCorners;
    fCorner         : TCorners;
    fItemValid      : boolean;
    fItemUpgrade    : TItemUpgrade;
    fInfoText       : boolean;
    fInfoTextButton : TDXBitmapButton;
    fTextViewer     : TDXTextViewer;
    fAlien          : TAlien;
    fScrollBar      : TDXScrollBar;
    fScrollVisible  : boolean;
    fEntryRect      : TRect;
    NeedHeight      : Integer;
    fBeginnEntry    : Integer;

    fStrings        : TStringArray;
    fSurface        : TDirectDrawSurface;
    fMem            : TDDSurfaceDesc;
    fDBoldFont      : TDirectFont;
    fImprovedFont   : TDirectFont;
    fValueFont      : TDirectFont;
    fShowInfoButton : Boolean;
    fWasLine        : Boolean;

    // Eigenschaften verbessert ?
    fIncMaxGes      : Boolean;
    fIncTimeUnits   : Boolean;
    fIncSicht       : Boolean;
    fIncPSIAn       : Boolean;
    fIncPSIAb       : Boolean;
    fIncTreff       : Boolean;
    fIncKraft       : Boolean;
    fIncIQ          : Boolean;
    fIncReact       : Boolean;

    DrawEntry       : TDrawEntryProc;
    DrawLine        : TDrawLineProc;
    DrawProperty    : TDrawPropertyProc;

    procedure SetCaption(const Value: String);
    procedure SetColor(const Value: TColor);
    procedure SetInfoType(const Value: TInfoType);
    procedure SetLagerItem(const Value: TLagerItem);
    procedure SetInfos(const Value: TInfos);
    procedure SetProduktion(const Value: TProduktion);
    procedure SetEinrichtung(const Value: TEinrichtung);
    procedure SetEinStatus(const Value: boolean);
    procedure SetOrganisation(const Value: TLand);
    procedure SetModel(const Value: TRaumschiffModel);
    procedure SetRaumschiff(const Value: TRaumschiff);
    procedure SetUFOModel(const Value: TUFOModel);
    procedure SetUFO(const Value: TUFO);
    procedure SetBasis(const Value: TBasis);
    procedure SetBasisStatus(const Value: TBasis);

    procedure SetCorners(const Value: TRoundCorners);
    procedure SetItemValid(const Value: boolean);
    procedure SetItemUpgrade(const Value: TItemUpgrade);
    procedure SetBlendColor(const Value: TBlendColor);
    procedure InfoButtonClick(Sender: TObject);
    procedure SetInfoText(InfoText: boolean);
    function GetFrameRect: TRect;
    procedure SetAlien(const Value: TAlien);
    procedure OnScroll(Sender: TObject);
    procedure InfoText(Description: String);

    procedure ChangeKapazitat(Sender: TObject);
    procedure ManagerDestroy(Sender: TObject);
    { Private-Deklarationen }
  protected
    procedure DrawSoldatInfo(Surface: TDirectDrawSurface);
    procedure DrawLagerItem(Surface: TDirectDrawSurface);
    procedure DrawItemUpgrade(Surface: TDirectDrawSurface);
    procedure DrawProduktion(Surface: TDirectDrawSurface);
    procedure DrawEinrichtung(Surface: TDirectDrawSurface);
    procedure DrawOrganisation(Surface: TDirectDrawSurface);
    procedure DrawModel(Surface: TDirectDrawSurface);
    procedure DrawRaumschiff(Surface: TDirectDrawSurface);
    procedure DrawUFOModel(Surface: TDirectDrawSurface);
    procedure DrawUFO(Surface: TDirectDrawSurface);
    procedure DrawAlien(Surface: TDirectDrawSurface);
    procedure DrawProjekt(Surface: TDirectDrawSurface);
    procedure DrawBasis(Surface: TDirectDrawSurface);
    procedure DrawBasisStatus(Surface: TDirectDrawSurface);

    procedure DrawOrganStatus(Surface: TDirectDrawSurface;X,Y, Width: Integer;Friendly: Integer);

    procedure SetComponentHint(const Value: String);override;
    procedure CreateMouseRegion;override;
    procedure FontChange(Sender: TObject);override;
    procedure CalcEntryRect;
    procedure DoScroll(Direction: TScrollDirection; Pos: TPoint);override;
    procedure EnumLagerForOrganisation;
    procedure EnumMunitionenForWaffe;
    { Zeichnen }
    procedure DefDrawEntry(var DrawTop: Integer;Text1: String;Text2: String);
    procedure DefDrawLine(var DrawTop: Integer);
    procedure DefDrawProperty(var DrawTop: Integer;Text1: String;Text2: String;prop: double;Increased: boolean);

    { Berechnen der Größe }
    procedure EnumDrawEntry(var DrawTop: Integer;Text1: String;Text2: String);
    procedure EnumDrawLine(var DrawTop: Integer);
    procedure EnumDrawProperty(var DrawTop: Integer;Text1: String;Text2: String;prop: double;Increased: boolean);

    procedure DrawStrings(var DrawTop: Integer; Titel: String);
    { Scrollbar Berechnungen }
    procedure CalculateScrollBar;
    { Protected-Deklarationen }
  public
    constructor Create(Page: TDXPage);override;
    destructor destroy;override;
    procedure Draw(Surface: TDirectDrawSurface;var Mem: TDDSurfaceDesc);override;
    procedure Resize(var NewLeft,NewTop,NewWidth,NewHeight: Integer);override;
    procedure VisibleChange;override;

    procedure ShowSoldatInfo(var Value: TSoldatInfo; Manager: TGameFigureManager = nil);
    procedure SetProjekt(const Value: TForschungen;Name: String);

    property Caption        : String read fCaption write SetCaption;
    property BorderColor    : TColor write SetColor;
    property InfoType       : TInfoType read fInfoType write SetInfoType;
    property Soldat         : TSoldatInfo read fSoldat;
    property Item           : TLagerItem read fLagerItem write SetLagerItem;
    property ItemUpgrade    : TItemUpgrade read fItemUpgrade write SetItemUpgrade;
    property Production     : TProduktion read fProduktion write SetProduktion;
    property Organisation   : TLand read fOrganisation write SetOrganisation;
    property Einrichtung    : TEinrichtung read fEinrichtung write SetEinrichtung;
    property Basis          : TBasis read fBasis write SetBasis;
    property BasisStatus    : TBasis read fBasisStatus write SetBasisStatus;
    property RaumschiffTyp  : TRaumschiffModel read fModel write SetModel;
    property Raumschiff     : TRaumschiff read fRaumschiff write SetRaumschiff;
    property Alien          : TAlien read fAlien write SetAlien;
    property UFOKampf       : boolean read fUFOKampf write fUFOKampf;
    property UFOModel       : TUFOModel read fUFOModel write SetUFOModel;
    property UFO            : TUFO read fUFO write SetUFO;
    property EinStatus      : boolean read fEinStatus write SetEinStatus;
    property Infos          : TInfos read fInfos write SetInfos;
    property BlendColor     : TBlendColor read fBlendColor write SetBlendColor;
    property RoundCorners   : TRoundCorners read fCorners write SetCorners;
    property ItemValid      : boolean read fItemValid write SetItemValid;

    property ShowInfoButton : Boolean read fShowInfoButton write fShowInfoButton;
  end;

  TColorSheme      = (csRed,csGreen,csBlue);

  TDXPopupItemInfo = class(TDXComponent)
  private
    fSoldat         : TSoldatInfo;
    fInfoType       : TInfoType;
    Surface         : TDirectDrawSurface;
    GlobalMem       : TDDSurfaceDesc;

    fBlendColor     : TBlendColor;
    fBColor         : TColor;
    fLagerItem      : TLagerItem;
    fMunition       : Integer;
    fMaxMuni        : Integer;
    fLeftOffSet     : Integer;
    fTopOffSet      : Integer;
    DrawEntry       : TDrawEntryProc;
    DrawLine        : TDrawLineProc;

    NewHeight       : Integer;
    NewWidth        : Integer;

    fAlpha          : Integer;
    fStatus         : Integer;
    fReserveMuni    : Integer;
    fShowNameOnly: Boolean;
    procedure SetSoldat(const Value: TSoldatInfo);
    procedure SetLagerItem(const Value: TLagerItem);

    procedure AktuTempSurface;
    function AddAlpha(Sender: TObject;Frames: Integer): boolean;
    function DeleteAlpha(Sender: TObject;Frames: Integer): boolean;

    procedure SetBColor(const Value: TColor);
    procedure SetNameOnly(const Value: Boolean);
  protected
    { Zeichnen }
    procedure DefDrawEntry(var DrawTop: Integer;Text1: String;Text2: String);
    procedure DefDrawLine(var DrawTop: Integer);
    { Höhe Berechnen }
    procedure EnumDrawEntry(var DrawTop: Integer;Text1: String;Text2: String);
    procedure EnumDrawLine(var DrawTop: Integer);

    procedure DrawLagerItem;
    procedure DrawSoldat;

    procedure Restore;override;
    procedure SetColor(Color: TColorSheme);
  public
    constructor Create(Page: TDXPage);override;
    destructor destroy;override;
    procedure Draw(Surface: TDirectDrawSurface;var Mem: TDDSurfaceDesc);override;
    procedure SetAlignRect(const Rect: TRect);
    procedure VisibleChange;override;
    procedure UnVisible(Reset: boolean = false);

    procedure Show(Color: TColorSheme;Rect: TRect;Soldat: TSoldatInfo);overload;
    procedure Show(Color: TColorSheme;Rect: TRect;Item: TLagerItem);overload;

    property Soldat      : TSoldatInfo read fSoldat write SetSoldat;
    property Item        : TLagerItem read fLagerItem write SetLagerItem;
    
    property Munition    : Integer read fMunition write fMunition;
    property ReserveMuni : Integer read fReserveMuni write fReserveMuni;
    property MaxMunition : Integer read fMaxMuni write fMaxMuni;

    property ShowNameOnly: Boolean read fShowNameOnly write SetNameOnly;

    property BorderColor : TColor write SetBColor;
    property BlendColor  : TBlendColor read fBlendColor write fBlendColor;
  end;

  TSoldatRang = record
    NeededEP  : Integer;
  end;

  function GetSoldatenRang(EP: Integer): String;

var
  PopupTempSurface: TDirectDrawSurface;

  RangTable : Array[0..9] of TSoldatRang = ((NeededEP: 50000),
                                            (NeededEP: 37000),
                                            (NeededEP: 26000),
                                            (NeededEP: 19000),
                                            (NeededEP: 13000),
                                            (NeededEP: 8000),
                                            (NeededEP: 5000),
                                            (NeededEP: 2500),
                                            (NeededEP: 1000),
                                            (NeededEP: -1));

implementation

uses country_api, lager_api, soldaten_api, basis_api, raumschiff_api, ufo_api,
  alien_api, werkstatt_api, game_utils, forsch_api;

function GetSoldatenRang(EP: Integer): String;
var
  Dummy: Integer;
begin
  result:='';
  for Dummy:=0 to high(RangTable) do
  begin
    if EP>RangTable[Dummy].NeededEP then
    begin
      result:=RangTableName[Dummy];
      exit;
    end;
  end;
end;
{ TDXItemInfo }

procedure TDXItemInfo.CalcEntryRect;
begin
  fEntryRect:=Rect(Left,Top+fBeginnEntry+1,Right,Bottom-1);

  if fInfoTextButton.Visible then
    dec(fEntryRect.Bottom,27);

  if fScrollVisible then dec(fEntryRect.Right,17);
end;

procedure TDXItemInfo.CalculateScrollBar;

  procedure SetScrollBar;
  var
    HasSize: Integer;
  begin
    dec(NeedHeight,LineSkip);
    HasSize:=Height-fBeginnEntry;
    if fInfoTextButton.Visible then
      dec(HasSize,28);
    if NeedHeight>HasSize then
    begin
      Container.LoadGame(true);
      fScrollVisible:=true;
      fScrollBar.Top:=fBeginnEntry+Top;
      fScrollBar.Visible:=Visible;
      fScrollBar.Value:=0;
      fScrollBar.LargeChange:=fScrollBar.Height;
      fScrollBar.Height:=HasSize;
      fScrollBar.Max:=NeedHeight-HasSize;
      if fInfoTextButton.Visible then
        fScrollBar.RoundCorners:=rcNone
      else
      begin
        if RoundCorners in [rcAll,rcRight,rcRightBottom,rcBottom] then
          fScrollBar.RoundCorners:=rcRightBottom
        else
          fScrollBar.RoundCorners:=rcNone;
      end;
      Container.LoadGame(false);
    end
    else
    begin
      fScrollVisible:=false;
      fScrollBar.Visible:=false;
    end;
  end;

begin
  NeedHeight:=0;
  DrawEntry:=EnumDrawEntry;
  DrawLine:=EnumDrawLine;
  DrawProperty:=EnumDrawProperty;
  case fInfoType of
    itEinrichtung  : DrawEinrichtung(nil);
    itOrganisation : DrawOrganisation(nil);
    itLagerItem    : DrawLagerItem(nil);
    itSoldat       : DrawSoldatInfo(nil);
    itProjekt      : DrawProjekt(nil);
  end;
  DrawEntry:=DefDrawEntry;
  DrawLine:=DefDrawLine;
  DrawProperty:=DefDrawProperty;
  SetScrollBar;
end;

constructor TDXItemInfo.Create(Page: TDXPage);
begin
  inherited;
  fBlendColor:=bcMaroon;
  fInfoText:=false;

  fInfoTextButton:=TDXBitmapButton.Create(Page);
  fInfoTextButton.Visible:=false;
  fInfoTextButton.OnClick:=InfoButtonClick;
  fInfoTextButton.Text:=BInfoText;
  fInfoTextButton.Hint:=HInfoText;

  fTextViewer:=TDXTextViewer.Create(Page);
  fTextViewer.BlendColor:=bcMaroon;
  fTextViewer.RoundCorners:=rcNone;
  fTextViewer.Visible:=false;
  fTextViewer.LeftMargin:=8;
  fTextViewer.Topmargin:=5;

  fItemValid:=false;
  RoundCorners:=rcAll;
  fScrollBar:=TDXScrollBar.Create(Page);
  fScrollBar.Visible:=false;
  fScrollBar.Kind:=sbVertical;
  fScrollBar.RoundCorners:=rcNone;
  fScrollBar.FirstColor:=coScrollBrush;
  fScrollBar.SecondColor:=coScrollPen;
  fScrollBar.SmallChange:=18;
  fScrollVisible:=false;
  fScrollBar.OnChange:=OnScroll;

  DrawEntry:=DefDrawEntry;
  DrawLine:=DefDrawLine;
  DrawProperty:=DefDrawProperty;

  fValueFont:=WhiteStdFont;

  fShowInfoButton:=true;
end;

procedure TDXItemInfo.CreateMouseRegion;
var
  FrameRect: TRect;
  RoundRGN : HRGN;
begin
  FrameRect:=GetFrameRect;
  RoundRGN:=CreateRoundRectRgn(FrameRect.Left,FrameRect.Top,FrameRect.Right+1,FrameRect.Bottom+1,20,20);
  fMouseRegion:=CreateRectRgn(Left,Top,Right,Bottom);
  CombineRgn(fMouseRegion,fMouseRegion,RoundRGN,RGN_AND);
  DeleteObject(RoundRGN);
end;

destructor TDXItemInfo.destroy;
begin
  if fSoldatManager<>nil then
  begin
    fSoldatManager.NotifyList.RemoveEvent(fSoldatEvent);
    fSoldatManager.NotifyList.RemoveEvent(fSoldatDestroy);
  end;

  fScrollBar.Free;
  inherited;
end;

procedure TDXItemInfo.Draw(Surface: TDirectDrawSurface;var Mem: TDDSurfaceDesc);
begin
  if AlphaElements then BlendRoundRect(ClientRect,150,fBlendColor,Surface,Mem,11,fCorner,Rect(Left,Top,Right,Top+22));
  if AlphaControls then
  begin
    if fInfoTextButton.Visible then
      BlendRoundRect(ClientRect,125,fBlendColor,Surface,Mem,11,fCorner,Rect(Left,Top+22,Right,Bottom-fInfoTextButton.Height))
    else
      BlendRoundRect(ClientRect,125,fBlendColor,Surface,Mem,11,fCorner,Rect(Left,Top+22,Right,Bottom));
  end;
  HLine(Surface,Mem,Left+1,Right-1,Top+22,fColor);
  fDBoldFont.Draw(Surface,Left+8+((Width-16) shr 1)-(fDBoldFont.TextWidth(fCaption) shr 1),Top+5,fCaption);
  FramingRect(Surface,Mem,ClientRect,fCorner,11,fColor);
  if not fItemValid then exit;
  CalcEntryRect;
  fSurface:=Surface;
  fMem:=Mem;
  fWasLine:=false;
  case fInfoType of
    itSoldat        : DrawSoldatInfo(Surface);
    itLagerItem     : DrawLagerItem(Surface);
    itUpgrade       : DrawItemUpgrade(Surface);
    itProduktion    : DrawProduktion(Surface);
    itEinrichtung   : DrawEinrichtung(Surface);
    itOrganisation  : DrawOrganisation(Surface);
    itModel         : DrawModel(Surface);
    itRaumschiff    : DrawRaumschiff(Surface);
    itUFOModel      : DrawUFOModel(Surface);
    itUFO           : DrawUFO(Surface);
    itAlien         : DrawAlien(Surface);
    itProjekt       : DrawProjekt(Surface);
    itBasis         : DrawBasis(Surface);
    itBasisStatus   : DrawBasisStatus(Surface);
  end;
end;

procedure TDXItemInfo.DrawAlien(Surface: TDirectDrawSurface);
var
  DrawTop : Integer;
  Text    : String;
begin
  fBeginnEntry:=67;
  DrawTop:=Top+67;
  alien_api_DrawIcon(Surface,Left+8,Top+28,fAlien.Image);
  Text:=fAlien.Name;
  YellowStdFont.Draw(Surface,Left+8+((Width-16) shr 1)-(YellowStdFont.TextWidth(Text) shr 1),Top+28,Text);
  if fTextViewer.Visible then exit;
  DrawLine(DrawTop);
  if not Alien.Autopsie then
  begin
    DrawEntry(DrawTop,ST0311300001,'');
    exit;
  end;
  DrawEntry(DrawTop,IAHitPoints,Format(FInteger,[fAlien.Ges]));
  {$IFDEF SHOWEP}
  DrawEntry(DrawTop,'Erfahrungspunkte',Format(FInteger,[fAlien.EP]));
  {$ENDIF}
  DrawEntry(DrawTop,IAPanzer,Format(FInteger,[fAlien.Pan]));
  DrawEntry(DrawTop,ST0309220009,Format(FPercent,[fAlien.Power]));
  DrawEntry(DrawTop,IAPSIAn,Format(FPercent,[fAlien.PSAn]));
  DrawEntry(DrawTop,IAPSIAb,Format(FPercent,[fAlien.PSAb]));
  DrawEntry(DrawTop,IAZeiteinheiten,Format(FInteger,[fAlien.Zei]));
  DrawEntry(DrawTop,IATreffSicherh,Format(FPercent,[fAlien.Treff]));
  DrawEntry(DrawTop,ST0311250001,Format(FInteger,[fAlien.Sicht]));
  DrawEntry(DrawTop,ST0403080001,Format(FInteger,[fAlien.IQ]));
  DrawEntry(DrawTop,ST0403310001,Format(FInteger,[fAlien.Reaktion]));
  DrawLine(DrawTop);
end;

procedure TDXItemInfo.DrawEinrichtung(Surface: TDirectDrawSurface);
var
  DrawTop: Integer;
begin
  if Surface<>nil then
  begin
    basis_api_DrawIcon(Surface,Left+8,Top+28);
    YellowStdFont.Draw(Surface,Left+8+((Width-16) shr 1)-(YellowStdFont.TextWidth(fEinrichtung.Name) shr 1),Top+28,fEinrichtung.Name);
    WhiteStdFont.Draw(Surface,Left+8+((Width-16) shr 1)-(WhiteStdFont.TextWidth(RoomTypNames[fEinrichtung.RoomTyp]) shr 1),Top+48,RoomTypNames[fEinrichtung.RoomTyp]);
  end;
  fBeginnEntry:=67;
  DrawTop:=Top+67;
  if fTextViewer.Visible then exit;
  DrawLine(DrawTop);
  if fEinStatus then
  begin
    if Einrichtung.days>0 then
    begin
      DrawEntry(DrawTop,IAStatus,ZahlString(STBauDay,STBauDays,Einrichtung.days));
      if Einrichtung.ActualValue>0 then
        DrawEntry(DrawTop,ST0501290001,Format(FCredits,[Einrichtung.ActualValue/1]));
      DrawLine(DrawTop);
      exit;
    end;
//    DrawEntry(DrawTop,IAKostperMonth,Format(FCredits,[(Einrichtung.KaufPreis/Einrichtung.BauZeit)/52]));
//    DrawLine(DrawTop);
    if Einrichtung.ActualValue>0 then
      DrawEntry(DrawTop,ST0501290001,Format(FCredits,[Einrichtung.ActualValue/1]));
    DrawEntry(DrawTop,IAHitPoints,Format(FRoom,[Einrichtung.AktHitpoints/1,Einrichtung.Hitpoints/1]));
    DrawLine(DrawTop);
  end
  else
  begin
    DrawEntry(DrawTop,IIKaufPreis,Format(FCredits,[Einrichtung.KaufPreis/1]));
    DrawEntry(DrawTop,IABauZeit,Format(Fdays,[Einrichtung.BauZeit]));

    DrawEntry(DrawTop,IAHitPoints,Format(FFloat,[Einrichtung.Hitpoints/1]));

//    DrawEntry(DrawTop,IABKostperDay,Format(FCredits,[Einrichtung.KaufPreis/Einrichtung.BauZeit]));
//    DrawEntry(DrawTop,IAKostperMonth,Format(FCredits,[(Einrichtung.KaufPreis/Einrichtung.BauZeit)/52]));
    DrawLine(DrawTop);
  end;
  if Einrichtung.LagerRaum>0 then
  begin
    if fEinStatus then
      DrawEntry(DrawTop,LBLagerRaum,Format(FLAuslastung,[Einrichtung.LagerBelegt,Einrichtung.LagerRaum,round(Einrichtung.LagerBelegt/Einrichtung.LagerRaum*100)]))
    else
      DrawEntry(DrawTop,LBLagerRaum,Format(FInteger,[round(Einrichtung.LagerRaum)]))
  end;
  if Einrichtung.AlphatronStorage>0 then
    DrawEntry(DrawTop,ST0501110001,Format(FFloat,[Einrichtung.AlphatronStorage/1]));

  DrawLine(DrawTop);

  case Einrichtung.RoomTyp of
    rtBaseRooms :
    begin
      if Einrichtung.WerkRaum>0 then
      begin
        if fEinStatus then
          DrawEntry(DrawTop,LBWerkRaum,Format(FAuslastung,[Einrichtung.WerkBelegt,Einrichtung.WerkRaum,round(Einrichtung.WerkBelegt/Einrichtung.WerkRaum*100)]))
        else
          DrawEntry(DrawTop,LBWerkRaum,Format(FInteger,[Einrichtung.WerkRaum]));
      end;
      if Einrichtung.LaborRaum>0 then
      begin
        if fEinStatus then
          DrawEntry(DrawTop,LBLaborRaum,Format(FAuslastung,[Einrichtung.LaborBelegt,Einrichtung.LaborRaum,round(Einrichtung.LaborBelegt/Einrichtung.LaborRaum*100)]))
        else
          DrawEntry(DrawTop,LBLaborRaum,Format(FInteger,[Einrichtung.LaborRaum]));
      end;
    end;
    rtDefense:
    begin
      DrawEntry(DrawTop,IAAbwehr,Format(FInteger,[Einrichtung.Abwehr]));
      DrawEntry(DrawTop,IARange,Format(FKMetres,[Einrichtung.Reichweite/1])+' - '+Format(FKMetres,[Einrichtung.maxReichweite/1]));
      DrawEntry(DrawTop,IATreffSicherh,Format(FFloat0Percent,[Einrichtung.Treffsicherheit/1]));
      DrawEntry(DrawTop,IAFrequenz,Format(FShootFreqent,[Einrichtung.Frequenz]));
      DrawEntry(DrawTop,IAArt,game_utils_WaffTypeToStr(Einrichtung.WaffenArt));
    end;
    rtSensor:
    begin
      DrawEntry(DrawTop,IASensorRange,Format(FKMetres,[Einrichtung.SensorWidth/1]));
    end;
    rtDefenseShield:
    begin
      if fEinStatus then
        DrawEntry(DrawTop,IAShieldPoints,Format(FRoom,[Einrichtung.AktShieldPoints/1,Einrichtung.ShieldStrength/1]))
      else
        DrawEntry(DrawTop,IAShieldPoints,Format(FFloat,[Einrichtung.ShieldStrength/1]))
    end;
    rtLivingRoom:
    begin
      if EInrichtung.Quartiere>0 then
        DrawEntry(DrawTop,LBWohnRaum,Format(FAuslastung,[Einrichtung.WohnBelegt,Einrichtung.Quartiere,round(Einrichtung.WohnBelegt/Einrichtung.Quartiere*100)]))
      else
        DrawEntry(DrawTop,LBWohnRaum,Format(FInteger,[Einrichtung.Quartiere]))
    end;
    rtHangar:
    begin
      if Einrichtung.SmallSchiff>0 then
        DrawEntry(DrawTop,IASmallSchiff,Format(FAuslastung,[Einrichtung.SmallBelegt,Einrichtung.SmallSchiff,round((Einrichtung.SmallBelegt/Einrichtung.SmallSchiff)*100)]))
      else
        DrawEntry(DrawTop,IASmallSchiff,Format(FInteger,[Einrichtung.SmallSchiff]));
    end;
  end;
  DrawLine(DrawTop);
end;

procedure TDXItemInfo.DefDrawEntry(var DrawTop: Integer; Text1, Text2: String);
var
  PaintTop: Integer;
begin
  if fScrollVisible then
    PaintTop:=DrawTop-fScrollBar.Value
  else
    PaintTop:=DrawTop;

  YellowStdFont.DrawInRect(fSurface,fEntryRect,Left+8,PaintTop,Text1);
  if fScrollBar.Visible then
    fValueFont.DrawInRect(fSurface,fEntryRect,Right-25-fValueFont.TextWidth(Text2),PaintTop,Text2)
  else
    fValueFont.DrawInRect(fSurface,fEntryRect,Right-8-fValueFont.TextWidth(Text2),PaintTop,Text2);
  fValueFont:=WhiteStdFont;
  Inc(DrawTop,EntrySkip);

  fWasLine:=false;
end;

procedure TDXItemInfo.DrawItemUpgrade(Surface: TDirectDrawSurface);
var
  DrawTop : Integer;
  Text    : String;
  Line    : boolean;
  Dummy   : Integer;
  Item    : PLagerItem;
begin
  fBeginnEntry:=67;
  DrawTop:=Top+67;

  lager_api_DrawItem(fItemUpgrade.Old.ImageIndex,Surface,Left+8,Top+28);
  
  Text:=fItemUpgrade.Old.Name;
  YellowStdFont.Draw(Surface,Left+8+((Width-16) shr 1)-(YellowStdFont.TextWidth(Text) shr 1),Top+28,Text);
  Text:=game_utils_TypeToStr(fItemUpgrade.Old.TypeId);
  Text:=Format(FULevel,[Text,fItemUpgrade.Old.Level,fItemUpgrade.New.Level]);
  WhiteStdFont.Draw(Surface,Left+8+((Width-16) shr 1)-(WhiteStdFont.TextWidth(Text) shr 1),Top+45,Text);
  DrawLine(DrawTop);
  if fItemUpgrade.Old.Land<>-1 then
  begin
    country_api_DrawSymbol(Surface,Right-50,Top+32,fItemUpgrade.Old.Land);

    Text:=country_api_GetCountryName(fItemUpgrade.Old.Land);
    DrawEntry(DrawTop,IAMadeIn,Text);
    Line:=true;

    if not fItemUpgrade.Old.HerstellBar then
    begin
      DrawEntry(DrawTop,IANoConstruct,EmptyStr);
      Line:=true;
    end;
    if Line then DrawLine(DrawTop);
  end;
  if not fItemUpgrade.Old.Useable then
  begin
    DrawEntry(DrawTop,ST0310270001,'');
    exit;
  end;
  DrawEntry(DrawTop,IAForschTime,Format(FStunden,[fItemUpgrade.Time/1]));
  DrawLine(DrawTop);
  DrawEntry(DrawTop,IIKaufPreis,Format(FCredits,[fItemUpgrade.Old.KaufPreis/1]));
  DrawEntry(DrawTop,IAVerKaufPreis,Format(FCredits,[fItemUpgrade.Old.VerKaufPreis/1]));
  DrawLine(DrawTop);
  DrawEntry(DrawTop,IALagerVer,Format(FUFloat1,[fItemUpgrade.Old.LagerV,fItemUpgrade.New.LagerV]));

  if (fItemUpgrade.Old.TypeID in [ptWaffe,ptGranate,ptMine,ptPanzerung,ptSensor,ptGuertel]) then
    DrawEntry(DrawTop,IAGewicht,Format(FUFloat1,[fItemUpgrade.Old.Gewicht,fItemUpgrade.New.Gewicht]));

  if (fItemUpgrade.Old.TypeID in [ptWaffe,ptMunition,ptRWaffe,ptRMunition]) then
  begin
    Text:=game_utils_WaffTypeToStr(fItemUpgrade.Old.WaffType);
    DrawEntry(DrawTop,IAArt,Text);
  end;

  if (fItemUpgrade.Old.TypeID in [ptWaffe,ptGranate,ptRWaffe]) then
  begin
    if fItemUpgrade.Old.Strength=0 then
    begin
      DrawEntry(DrawTop,IAStrengthW,IAAbhMunition);
    end
    else
    begin
      DrawEntry(DrawTop,IAStrengthW,Format(FUInteger,[fItemUpgrade.Old.Strength,fItemUpgrade.New.Strength]));
      if fItemUpgrade.Old.TypeID=ptWaffe then
        DrawEntry(DrawTop,ST0309220009,Format(FUPercent,[fItemUpgrade.Old.Power,fItemUpgrade.New.Power]));
    end;
  end;
  if (fItemUpgrade.Old.TypeID in [ptMunition,ptRMunition]) then
  begin
    Item:=lager_api_GetItem(fItemUpgrade.Old.Munfor,false);
    if Item<>nil then
      DrawEntry(DrawTop,IAWeapon,Item.Name);
    
    if (fItemUpgrade.Old.Strength=0) or (fItemUpgrade.Old.TypeID=ptRMunition) then
      Text:=IAAbhWaffe
    else
      Text:=Format(FUInteger,[fItemUpgrade.Old.Strength,fItemUpgrade.New.Strength]);

    DrawEntry(DrawTop,IAStrengthM,Text);

    if fItemUpgrade.Old.Strength>0 then
      DrawEntry(DrawTop,ST0309220009,Format(FUPercent,[fItemUpgrade.Old.Power,fItemUpgrade.New.Power]));

    DrawEntry(DrawTop,IASchuss,Format(FUInteger,[fItemUpgrade.Old.Munition,fItemUpgrade.New.Munition]));

  end;
  if (fItemUpgrade.Old.TypeID=ptWaffe) and (fItemUpgrade.Old.WaffType<>wtShortRange) then
  begin
//    DrawEntry(DrawTop,IAFrequenz,Format(FUInteger,[fItemUpgrade.Old.Laden,fItemUpgrade.New.Laden]));
    DrawEntry(DrawTop,ST0311230005,Format(FUInteger,[fItemUpgrade.Old.Genauigkeit,fItemUpgrade.New.Genauigkeit]));
  end;
  if (fItemUpgrade.Old.TypeID=ptMine) then
  begin
    DrawEntry(DrawTop,IAStrengthMine,Format(FUInteger,[fItemUpgrade.Old.Strength,fItemUpgrade.New.Strength]));
    DrawEntry(DrawTop,ST0309220010,Format(FUInteger,[fItemUpgrade.Old.Reichweite,fItemUpgrade.New.Reichweite]));
  end;
  if (fItemUpgrade.Old.TypeID in [ptWaffe,ptRWaffe]) and (fItemUpgrade.Old.WaffType=wtLaser) then
    DrawEntry(DrawTop,IASchuss,Format(FUInteger,[fItemUpgrade.Old.Munition,fItemUpgrade.New.Munition]));
  if (fItemUpgrade.Old.TypeID=ptRWaffe) then
  begin
    DrawEntry(DrawTop,IARange,Format(FUMetres,[fItemUpgrade.Old.Reichweite/1,fItemUpgrade.New.Reichweite/1]));
    DrawEntry(DrawTop,IANachLade,Format(FUMSeconds,[fItemUpgrade.Old.Laden,fItemUpgrade.New.Laden]));
    if fItemUpgrade.Old.Verfolgung then Text:=LYes else Text:=LNo;
    if fItemUpgrade.New.Verfolgung then
      Text:=Format(FUString,[Text,LYes])
    else
      Text:=Format(FUString,[Text,LNo]);
    DrawEntry(DrawTop,IAVerfolg,Text);
  end;
  if (fItemUpgrade.Old.TypeID=ptSensor) then
    DrawEntry(DrawTop,IASensorRange,Format(FUInteger,[fItemUpgrade.Old.Strength,fItemUpgrade.New.Strength]));
  if (fItemUpgrade.Old.TypeID=ptGuertel) then
    DrawEntry(DrawTop,ST0309220011,Format(FUInteger,[fItemUpgrade.Old.Strength,fItemUpgrade.New.Strength]));
  if (fItemUpgrade.Old.TypeID=ptPanzerung) then
    DrawEntry(DrawTop,IAPanzer,Format(FUInteger,[fItemUpgrade.Old.Panzerung,fItemUpgrade.New.Panzerung]));
  if (fItemUpgrade.Old.TypeID=ptMotor) then
  begin
    DrawEntry(DrawTop,IAKapazitat,Format(FULiter,[fItemUpgrade.Old.Munition,fItemUpgrade.New.Munition]));
    DrawEntry(DrawTop,IAVerbrauch,Format(FULiterperkm,[fItemUpgrade.Old.Strength,fItemUpgrade.New.Strength]));
    DrawEntry(DrawTop,LPPS,Format(FUInteger,[fItemUpgrade.Old.PixPerSec,fItemUpgrade.New.PixPerSec]));
    if fItemUpgrade.Old.Verfolgung then Text:=LYes else Text:=LNo;
    if fItemUpgrade.New.Verfolgung then
      Text:=Format(FUString,[Text,LYes])
    else
      Text:=Format(FUString,[Text,LNo]);
    DrawEntry(DrawTop,IADuesen,Text);
  end;
  if (fItemUpgrade.New.TypeID=ptShield) then
  begin
    DrawEntry(DrawTop,IAShieldPoints,Format(FUInteger,[fItemUpgrade.Old.Strength,fItemUpgrade.New.Strength]));
    DrawEntry(DrawTop,IADefend,Format(FUPercent,[fItemUpgrade.Old.Panzerung,fItemUpgrade.New.Panzerung]));
    DrawEntry(DrawTop,IAAufLade,Format(FUMSeconds,[fItemUpgrade.Old.Laden,fItemUpgrade.New.Laden]));
  end;
  if (fItemUpgrade.New.TypeID=ptExtension) then
  begin
    DrawLine(DrawTop);
    DrawEntry(DrawTop,IAExtensions,'');
    for Dummy:=0 to fItemUpgrade.New.ExtCount-1 do
    begin
      with fItemUpgrade.New.Extensions[Dummy] do
      begin
        if Value<>0 then
        begin
          DrawEntry(DrawTop, game_utils_NameOfExtension(fItemUpgrade.New.Extensions[Dummy].Prop),
            game_utils_FormatedExtensionDiff(fItemUpgrade.Old.Extensions[Dummy], fItemUpgrade.New.Extensions[Dummy]));
        end;
      end;
    end;
  end;
  DrawLine(DrawTop);
end;

procedure TDXItemInfo.DrawLagerItem(Surface: TDirectDrawSurface);
var
  DrawTop : Integer;
  Text    : String;
  Dummy   : Integer;
  Item    : PLagerItem;
begin
  fBeginnEntry:=67;
  DrawTop:=Top+67;
  if Surface<>nil then
  begin
    lager_api_DrawItem(fLagerItem.ImageIndex,Surface,Left+8,Top+28);

    Text:=fLagerItem.Name;
    YellowStdFont.Draw(Surface,Left+8+((Width-16) shr 1)-(YellowStdFont.TextWidth(Text) shr 1),Top+28,Text);
    Text:=game_utils_TypeToStr(fLagerItem.TypeId);
    Text:=Format(IALagerItem,[Text,fLagerItem.Level]);
    WhiteStdFont.Draw(Surface,Left+8+((Width-16) shr 1)-(WhiteStdFont.TextWidth(Text) shr 1),Top+45,Text);
    if fLagerItem.Land<>-1 then
    begin
      country_api_DrawSymbol(Surface,Right-50,Top+32,fLagerItem.Land);
    end;
  end;
  if fTextViewer.Visible then exit;

  if not fLagerItem.Useable then
  begin
    DrawEntry(DrawTop,ST0310270001,'');
    exit;
  end;

  DrawLine(DrawTop);
  if fLagerItem.Land<>-1 then
  begin
    Text:=country_api_GetCountryName(fLagerItem.Land);
    DrawEntry(DrawTop,IAMadeIn,Text);
    DrawLine(DrawTop);
  end;
  if (inPreis in fInfos) then
  begin
    if not fLagerItem.AlienItem then
    begin
      DrawEntry(DrawTop,IIKaufPreis,Format(FCredits,[fLagerItem.KaufPreis/1]));

      if fLagerItem.VerKaufPreis>0 then
        DrawEntry(DrawTop,IAVerKaufPreis,Format(FCredits,[fLagerItem.VerKaufPreis/1]))
      else
        DrawEntry(DrawTop,ST0401120004,'');
    end;
  end;
  if (inProduktion in fInfos) then
  begin
    DrawLine(DrawTop);
    if not fLagerItem.AlienItem then
    begin
      if fLagerItem.HerstellBar then
      begin
        DrawEntry(DrawTop,IAProdKost,Format(FQuarringAlphatron, [fLagerItem.ManuAlphatron / 1]));
        DrawEntry(DrawTop,IATStunden,Format(FFloat,[fLagerItem.ProdTime/1]));
      end
      else
      begin
        DrawEntry(DrawTop,IANoConstruct,EmptyStr);
        DrawLine(DrawTop);
      end;
    end;
    if lager_api_GetRecyclingAlphatron(fLagerItem)>0 then
      DrawEntry(DrawTop,CR0507210001,Format(FAlphatron,[lager_api_GetRecyclingAlphatron(fLagerItem)]));
      
    DrawLine(DrawTop);
  end;
  if (fLagerItem.TypeID in [ptWaffe,ptMunition,ptRWaffe,ptRMunition]) then
  begin
    Text:=game_utils_WaffTypeToStr(fLagerItem.WaffType);
    DrawEntry(DrawTop,IAArt,Text);
  end;
  if fLagerItem.TypeID=ptWaffe then
  begin
    if fLagerItem.Einhand then
      DrawEntry(DrawTop,IAWaffType,IIOneHandWeapon)
    else
      DrawEntry(DrawTop,IAWaffType,IITwoHandWeapon);
  end;
  if (inAnzahl in fInfos) then DrawEntry(DrawTop,IACount,Format(FInteger,[fLagerItem.Anzahl]));
  if (inLager in fInfos) then DrawEntry(DrawTop,IALagerVer,Format(FDezimal1,[fLagerItem.LagerV]));
  if (inGewicht in fInfos) and (fLagerItem.TypeID in [ptWaffe,ptMunition,ptPanzerung,ptSensor,ptGranate,ptMine]) then
     DrawEntry(DrawTop,IAGewicht,Format(FDezimal1,[fLagerItem.Gewicht]));
  if (fLagerItem.TypeID=ptWaffe) and (inStrength in fInfos) then
  begin
    if fLagerItem.Strength=0 then
    begin
      DrawEntry(DrawTop,IAStrengthW,IAAbhMunition);
    end
    else
    begin
      DrawEntry(DrawTop,IAStrengthW,Format(FFloat,[fLagerItem.Strength/1]));
      DrawEntry(DrawTop,ST0309220009,Format(FPercent,[fLagerItem.Power]));
    end;
    if fLagerItem.WaffType<>wtShortRange then
    begin
      DrawEntry(DrawTop,ST0311230005,Format(FInteger,[fLagerItem.Genauigkeit]));

      if stAuto in fLagerItem.Schuss then
        DrawEntry(DrawTop,ST0309220012,Format(FFloat,[fLagerItem.TimeUnits*TimeUnitsMultiplikator[stAuto]/1]));
      if stSpontan in fLagerItem.Schuss then
        DrawEntry(DrawTop,ST0409280001,Format(FFloat,[fLagerItem.TimeUnits*TimeUnitsMultiplikator[stSpontan]/1]));
      if stGezielt in fLagerItem.Schuss then
        DrawEntry(DrawTop,ST0409280002,Format(FFloat,[fLagerItem.TimeUnits*TimeUnitsMultiplikator[stGezielt]/1]));
    end
    else
    begin
//      DrawEntry(DrawTop,ST0401120002,Format(FMSeconds,[fLagerItem.Laden]));
      DrawEntry(DrawTop,ST0401120003,Format(FFloat,[fLagerItem.TimeUnits/1]));
    end;
  end;
  if (fLagerItem.TypeID=ptGranate) and (inStrength in fInfos) then
  begin
    DrawEntry(DrawTop,IAStrengthW,Format(FFloat,[fLagerItem.Strength/1]));
    DrawEntry(DrawTop,ST0309220010,Format(FInteger,[fLagerItem.Reichweite]));
  end;
  if (fLagerItem.TypeID=ptGuertel) and (inStrength in fInfos) then
  begin
    DrawEntry(DrawTop,ST0309220011,Format(FInteger,[fLagerItem.Strength]));
  end;
  if (inMotor in fInfos) and (fLagerItem.TypeID=ptMotor) then
  begin
    DrawEntry(DrawTop,IAKapazitat,Format(FLiter,[fLagerItem.Munition/1]));
    DrawEntry(DrawTop,IAVerbrauch,Format(FLiterperkm,[fLagerItem.Strength]));
    DrawEntry(DrawTop,LPPS,Format(FInteger,[fLagerItem.PixPerSec]));
    if fLagerItem.Verfolgung then Text:=LYes else Text:=LNo;
    DrawEntry(DrawTop,IADuesen,Text);
  end;
  if (fLagerItem.TypeID=ptRWaffe) and (inStrength in fInfos) then
  begin
    DrawEntry(DrawTop,IAStrengthW,Format(FFloat,[fLagerItem.Strength/1]));
    DrawEntry(DrawTop,IARange,Format(fMetres,[fLagerItem.Reichweite/1]));
  end;
  if (fLagerItem.TypeID=ptMine) and (inStrength in fInfos) then
  begin
    DrawEntry(DrawTop,IAStrengthMine,Format(FInteger,[fLagerItem.Strength]));
    DrawEntry(DrawTop,ST0309220010,Format(FFloat,[fLagerItem.Reichweite/1]));
  end;
  if (fLagerItem.TypeID=ptSensor) and (inStrength in fInfos) then
    DrawEntry(DrawTop,IASensorRange,Format(FInteger,[fLagerItem.Strength]));
  if (fLagerItem.TypeID in [ptMunition,ptRMunition]) and (inStrength in fInfos) then
  begin
    Item:=lager_api_GetItem(fLagerItem.Munfor,false);

    if Item=nil then
      DrawEntry(DrawTop,IAWeapon,LNoWeapon)
    else
      DrawEntry(DrawTop,IAWeapon,Item.Name);

    if (fLagerItem.Strength=0) or (fLagerItem.TypeID=ptRMunition) then
      Text:=IAAbhWaffe
    else
      Text:=Format(FInteger,[fLagerItem.Strength]);

    DrawEntry(DrawTop,IAStrengthM,Text);

    if (fLagerItem.TypeID=ptMunition) and (fLagerItem.Strength>0) then
      DrawEntry(DrawTop,ST0309220009,Format(FPercent,[fLagerItem.Power]));

    DrawEntry(DrawTop,IASchuss,Format(FInteger,[fLagerItem.Munition]));
  end;
  if (inStrength in fInfos) and (fLagerItem.TypeID=ptMunition) and (fLagerItem.WaffType=wtRaketen) then
    DrawEntry(DrawTop,ST0310120001,Format(ST0310120002,[fLagerItem.Explosion]));
  if (inStrength in fInfos) and (fLagerItem.TypeID in [ptWaffe,ptRWaffe]) and (fLagerItem.WaffType=wtLaser) then
  begin
    DrawEntry(DrawTop,IASchuss,Format(FInteger,[fLagerItem.Munition]));
  end;
  if (fLagerItem.TypeID=ptPanzerung) and (inPanzerung in fInfos) then
    DrawEntry(DrawTop,IAPanzer,Format(FInteger,[fLagerItem.Panzerung]));
  if (fLagerItem.TypeID=ptRWaffe) and (inRInfos in fInfos) then
  begin
    DrawEntry(DrawTop,IANachLade,Format(FMSeconds,[fLagerItem.Laden]));
    if fLagerItem.Verfolgung then Text:=LYes else Text:=LNo;
    DrawEntry(DrawTop,IAVerfolg,Text);
  end;
  if (fLagerItem.TypeID=ptShield) and (inRInfos in fInfos) then
  begin
    DrawEntry(DrawTop,IAShieldPoints,Format(FInteger,[fLagerItem.Strength]));
    DrawEntry(DrawTop,IADefend,Format(FPercent,[fLagerItem.Panzerung]));
    DrawEntry(DrawTop,IAAufLade,Format(FMSeconds,[fLagerItem.Laden]));
  end;
  if (fLagerItem.TypeID in SoldatenTypes) and (fLagerItem.NeedIQ>0) then
  begin
    DrawEntry(DrawTop,ST0403190001,Format(FInteger,[fLagerItem.NeedIQ]));
  end;
  if (fLagerItem.TypeID=ptExtension) then
  begin
    DrawLine(DrawTop);
    DrawEntry(DrawTop,IAExtensions,'');
    for Dummy:=0 to fLagerItem.ExtCount-1 do
    begin
      with fLagerItem.Extensions[Dummy] do
      begin
        if Value<>0 then
        begin
          DrawEntry(DrawTop,game_utils_NameOfExtension(Prop),game_utils_FormatedExtension(fLagerItem.Extensions[Dummy]));
        end;
      end;
    end;
  end;
  DrawLine(DrawTop);

  DrawStrings(DrawTop,ST0309220013);
end;

procedure TDXItemInfo.DefDrawLine(var DrawTop: Integer);
var
  PaintTop: Integer;
begin
  if fWasLine then
    exit;

  if fScrollVisible and not (DrawTop=Top+67) then
    PaintTop:=DrawTop-fScrollBar.Value
  else
    PaintTop:=DrawTop;
  if ((PaintTop>=fEntryRect.Top-1) and (PaintTop<=fEntryRect.Bottom)) then
  begin
    if fScrollBar.Visible then
      HLine(fSurface,fMem,Left+1,Right-17,PaintTop,fColor)
    else
      HLine(fSurface,fMem,Left+1,Right-1,PaintTop,fColor);
  end;
  inc(DrawTop,LineSkip);

  fWasLine:=true;
end;

procedure TDXItemInfo.DrawModel(Surface: TDirectDrawSurface);
var
  DrawTop : Integer;
begin
  fBeginnEntry:=67;
  DrawTop:=Top+67;
  raumschiff_api_DrawIcon(Surface,Left+8,Top+28,fModel.WaffenZellen);
  YellowStdFont.Draw(Surface,Left+8+((Width-16) shr 1)-(YellowStdFont.TextWidth(fModel.Name) shr 1),Top+28,fModel.Name);
  if fTextViewer.Visible then exit;
  DrawLine(DrawTop);
  DrawEntry(DrawTop,IIKaufPreis,Format(FCredits,[fModel.Kaufpreis/1]));
  DrawEntry(DrawTop,IAVerKaufPreis,Format(FCredits,[fModel.VerKaufpreis/1]));
  DrawEntry(DrawTop,IABauZeit,Format(FInteger,[fModel.BauZeit]));
  DrawEntry(DrawTop,IABKostperDay,Format(FCredits,[fModel.KaufPreis/fModel.BauZeit]));
  DrawEntry(DrawTop,IAKostperMonth,Format(FCredits,[round((fModel.KaufPreis div 48)/fModel.BauZeit)/1]));
  DrawLine(DrawTop);
  DrawEntry(DrawTop,IAWaffenZ,Format(FInteger,[fModel.WaffenZellen]));
  DrawEntry(DrawTop,IAExtendsSlots,Format(FInteger,[fModel.ExtendsSlots]));
  DrawEntry(DrawTop,IASensorRange,Format(FFloat,[fModel.SensorWeite/1]));
  DrawEntry(DrawTop,IAHitPoints,Format(FFloat,[fModel.HitPoints/1]));
  DrawEntry(DrawTop,LBLagerRaum,Format(FInteger,[fModel.LagerPlatz]));
  DrawEntry(DrawTop,IASoldaten,Format(FInteger,[fModel.Soldaten]));
  DrawLine(DrawTop);
end;

procedure TDXItemInfo.DrawOrganisation(Surface: TDirectDrawSurface);
var
  DrawTop : Integer;
  Text    : String;
begin
  fBeginnEntry:=67;
  DrawTop:=Top+67;
  if Surface<>nil then
  begin
    country_api_DrawSymbol(Surface,Left+8,Top+35,fOrganisation.Image);
    YellowStdFont.Draw(Surface,Left+8+((Width-16) shr 1)-(YellowStdFont.TextWidth(Organisation.Name) shr 1),Top+35,Organisation.Name);
  end;
  DrawLine(DrawTop);
//  DrawEntry(DrawTop,IABudget,Format(FCredits,[Organisation.Budget/1]));
  case OrganStatus(fOrganisation.Friendly) of
    osAllianz    : Text:=IAAlliance;
    osFriendly   : Text:=IAFriendly;
    osNeutral    : Text:=IANeutral;
    osUnFriendly : Text:=IAUnFriendly;
    osEnemy      : Text:=IAEnemy;
  end;
  DrawEntry(DrawTop,IAVertrauen,Text);
  begin
    DrawEntry(DrawTop,'-','+');
    if Surface<>nil then
    begin
      DrawOrganStatus(Surface,Left+8,DrawTop,Width-16,fOrganisation.Friendly)
    end
    else
      inc(NeedHeight,17);
    DrawTop:=DrawTop+17;
  end;
  DrawEntry(DrawTop,IAMitglied,Format(FFloat,[Organisation.Einwohner/1]));
  DrawEntry(DrawTop,LSchadKost,Format(FFloat,[Organisation.SchErsatz/1]));
  DrawLine(DrawTop);

  DrawStrings(DrawTop,IALandSpezifik);
end;

procedure TDXItemInfo.DrawOrganStatus(Surface: TDirectDrawSurface;X,Y, Width: Integer; Friendly: Integer);
var
  Links  : Integer;
  Oben   : Integer;
  Mem    : TDDSurfaceDesc;
  Full   : boolean;
  Zeiger : Cardinal;
  RMask  : Cardinal;
  BMask  : Cardinal;
  GMask  : Cardinal;
  RValue : Cardinal;
  BValue : Cardinal;
  GValue : Cardinal;
  RWert  : Cardinal;
  BWert  : Cardinal;
  GWert  : Cardinal;
  Farbe  : Cardinal;

  function GeneriereFarbStufe(Alpha: Cardinal): TBlendColor;
  begin
    RWert:=((RValue*Alpha) shr 8) and RMask;
    BWert:=((BValue*Alpha) shr 8) and BMask;
    GWert:=((GValue*Alpha) shr 8) and GMask;
    result:=RWert or BWert or GWert;
  end;

begin
  if fScrollVisible then
    Y:=Y-fScrollBar.Value
  else
    Y:=Y;
  if fScrollBar.Visible then Width:=Width-16;
  Surface.Lock(Mem);
  RMask:=Mem.ddpfPixelFormat.dwRBitMask;
  BMask:=Mem.ddpfPixelFormat.dwBBitMask;
  GMask:=Mem.ddpfPixelFormat.dwGBitMask;
  if Mode32Bit then
  begin
    for Links:=0 to Width-1 do
    begin
      Farbe:=LineColorTable[100-round(Links/Width*100)];
      Full:=round(Friendly/100*Width)<Links;
      Zeiger:=Integer(Mem.lpSurface)+(Y*Mem.lPitch)+((X+Links) shl 2);
      RValue:=RMask and Farbe;
      BValue:=BMask and Farbe;
      GValue:=GMask and Farbe;
      for Oben:=0 to 14 do
      begin
        if Y+Oben>=fEntryRect.Top then
        begin
          PLongWord(Zeiger)^:=GeneriereFarbStufe(AlphaValues[Full][Oben]);
        end;
        inc(Zeiger,Mem.lPitch)
      end;
    end;
  end
  else
  begin
    for Links:=0 to Width-1 do
    begin
      Farbe:=LineColorTable[100-round(Links/Width*100)];
      Full:=round(Friendly/100*Width)<Links;
      Zeiger:=Integer(Mem.lpSurface)+(Y*Mem.lPitch)+((X+Links) shl 1);
      RValue:=RMask and Farbe;
      BValue:=BMask and Farbe;
      GValue:=GMask and Farbe;
      for Oben:=0 to 14 do
      begin
        if Y+Oben>=fEntryRect.Top then
        begin
          PWord(Zeiger)^:=GeneriereFarbStufe(AlphaValues[Full][Oben]);
        end;
        inc(Zeiger,Mem.lPitch)
      end;
    end;
  end;
  Surface.Unlock;
end;

procedure TDXItemInfo.DrawProduktion(Surface: TDirectDrawSurface);
var
  Item      : TLagerItem;
  DrawTop   : Integer;
  Text      : String;
  Techniker : Integer;
  Strength  : Integer;
  runden    : Integer;
begin
  fBeginnEntry:=67;
  DrawTop:=Top+67;
  Item:=lager_api_GetItem(fProduktion.ItemID)^;

  lager_api_DrawItem(Item.ImageIndex,Surface,Left+8,Top+28);
  
  Text:=Item.Name;
  YellowStdFont.Draw(Surface,Left+8+((Width-16) shr 1)-(YellowStdFont.TextWidth(Text) shr 1),Top+28,Text);
  DrawLine(DrawTop);
  Text:=game_utils_TypeToStr(Item.TypeId);
  Text:=Format(IALagerItem,[Text,Item.Level]);
  WhiteStdFont.Draw(Surface,Left+8+((Width-16) shr 1)-(WhiteStdFont.TextWidth(Text) shr 1),Top+45,Text);
  Techniker:=0;
  Strength:=0;
  werkstatt_api_GetProductInfo(fProduktion.ID,Techniker,Strength);
  if fProduktion.Anzahl=101 then
    DrawEntry(DrawTop,LCount,LUnbegrentzt)
  else
    DrawEntry(DrawTop,LCount,Format(FInteger,[fProduktion.Anzahl]));
  if fProduktion.Product then
  begin
    DrawEntry(DrawTop,IAStatus,IAHerstellung);
    if Strength>0 then
    begin
      runden:=ceil((fProduktion.Hour*60)/((Strength/100)*1.25));
      DrawEntry(DrawTop,IANoch,IntToTime(runden));
      if fProduktion.Anzahl<101 then
      begin
        runden:=ceil(((fProduktion.Hour+(fProduktion.Gesamt*(fProduktion.Anzahl-1)))*60)/((Strength/100)*1.25));
        DrawEntry(DrawTop,ST0312210001,IntToTime(runden));
      end;
    end;
  end
  else
  begin
    DrawEntry(DrawTop,IAStatus,IACollection);
    DrawEntry(DrawTop,IANeedAlphatron,Format(FQuarringAlphatron, [fProduktion.Kost/1]));
    DrawEntry(DrawTop,IAReserved,Format(FQuarringAlphatron, [fProduktion.Reserved/1]));
    DrawEntry(DrawTop,IANoch,Format(FQuarringAlphatron, [(fProduktion.Kost-fProduktion.Reserved)/1]));
  end;
  DrawEntry(DrawTop,IATechniker,Format(FInteger,[Techniker]));
  DrawEntry(DrawTop,IAFaehig,Format(FFloat,[Strength/1]));
  DrawLine(DrawTop);
end;

procedure TDXItemInfo.DrawRaumschiff(Surface: TDirectDrawSurface);
var
  Dummy   : Integer;
  DrawTop : Integer;
  Text    : String;
begin
  if fRaumschiff=nil then exit;
  fBeginnEntry:=67;
  DrawTop:=Top+67;
  raumschiff_api_DrawIcon(Surface,Left+8,Top+28,fRaumschiff.WaffenZellen);
  Text:=fRaumschiff.Name;
  YellowStdFont.Draw(Surface,Left+8+((Width-16) shr 1)-(YellowStdFont.TextWidth(Text) shr 1),Top+28,Text);
  Text:=fRaumschiff.Model.Name;
  WhiteStdFont.Draw(Surface,Left+8+((Width-16) shr 1)-(WhiteStdFont.TextWidth(Text) shr 1),Top+45,Text);
  DrawLine(DrawTop);
  if not fUFOKampf then
  begin
    DrawEntry(DrawTop,IAStatus,fRaumschiff.StatusText);
    if fRaumschiff.Status=ssHuntUFO then
      DrawEntry(DrawTop,IAZiel,fRaumschiff.HuntedUFO.Name);
    DrawEntry(DrawTop,IAVerKaufPreis,Format(FCredits,[fRaumschiff.Model.VerKaufPreis/1]));
    DrawEntry(DrawTop,IAKostperMonth,Format(FCredits,[fRaumschiff.MonthKost/1]));
    DrawLine(DrawTop);
    if Raumschiff.Status<>ssBuild then
    begin
      DrawEntry(DrawTop,IAWaffenZ,Format(FInteger,[fRaumschiff.WaffenZellen]));
      DrawEntry(DrawTop,IAExtendsSlots,Format(FInteger,[fRaumschiff.ExtendsSlots]));
      if fRaumschiff.AttributeImproved(epSensor) then
        fValueFont:=fImprovedFont;
      DrawEntry(DrawTop,IASensorRange,Format(FFloat,[fRaumschiff.SensorWeite/1]));

      DrawLine(DrawTop);
      DrawEntry(DrawTop,IAHitPoints,Format(FRoom,[fRaumschiff.AktHitpoints/1,fRaumschiff.Model.HitPoints/1]));
      if fRaumschiff.AttributeImproved(epShieldHitpoints) then
        fValueFont:=fImprovedFont;
      DrawEntry(DrawTop,IAShieldPoints,Format(FRoom,[fRaumschiff.ShieldPoints/1, fRaumschiff.MaxShieldPoints/1]));
      if fRaumschiff.AttributeImproved(epShieldDefence) then
        fValueFont:=fImprovedFont;
      DrawEntry(DrawTop,IADefend,Format(FPercent,[fRaumschiff.ShieldAbwehr]));
      if fRaumschiff.AttributeImproved(epShieldReloadTime) then
        fValueFont:=fImprovedFont;
      DrawEntry(DrawTop,IAAufLade,Format(FMSeconds,[fRaumschiff.ShieldLaden]));
      DrawLine(DrawTop);
      
      if sabRepair in fRaumschiff.Abilities then
        DrawEntry(DrawTop,ST0502220001,IntToTime(fRaumschiff.GetRepairTime));
        
      DrawEntry(DrawTop,LBLagerRaum,Format(FInteger,[fRaumschiff.Model.LagerPlatz]));
      DrawEntry(DrawTop,IASoldaten,Format(FInteger,[fRaumschiff.Model.Soldaten]));
      if fRaumschiff.WaffenZellen>0 then
        DrawEntry(DrawTop,IITreffer,Format(FInteger,[fRaumschiff.Abschuesse]));
      if Raumschiff.Status<>ssNoMotor then
      begin
         DrawEntry(DrawTop,IABenzin,Format(FFloatPercent,[fRaumschiff.Treibstoff]));
      end;
    end
    else
    begin
      DrawEntry(DrawTop,IABKostperDay,Format(FCredits,[fRaumschiff.Model.KaufPreis/fRaumschiff.Model.BauZeit]));
    end;
    DrawLine(DrawTop);
  end
  else
  begin
    DrawEntry(DrawTop,IAHitPoints,Format(FRoom,[fRaumschiff.AktHitpoints/1,fRaumschiff.HitPoints/1]));
    if fRaumschiff.Motor.Duesen then Text:=LYes else Text:=LNo;
    DrawEntry(DrawTop,IADuesen,Text);
    DrawLine(DrawTop);
    if fRaumschiff.ShieldInstalliert then
    begin
      DrawEntry(DrawTop,IAShieldPoints,Format(FInteger,[fRaumschiff.MaxShieldPoints]));
      DrawEntry(DrawTop,IADefend,Format(FPercent,[fRaumschiff.ShieldAbwehr]));
      DrawEntry(DrawTop,IAAufLade,Format(FMSeconds,[fRaumschiff.ShieldLaden]));
      DrawLine(DrawTop);
    end;
    for Dummy:=0 to 2 do
    begin
      if fRaumschiff.WaffenZelle[Dummy].Installiert then
      begin

        // Waffenname
        DrawEntry(DrawTop,Format(IAZelle,[Dummy+1]),fRaumschiff.WaffenZelle[Dummy].Name);

        // Waffenart (Waffenstärke)
        Text:=game_utils_WaffTypeToStr(fRaumschiff.WaffenZelle[Dummy].WaffType)+Format(' ('+FInteger+')',[fRaumschiff.WaffenZelle[Dummy].Strength]);
        DrawEntry(DrawTop,ST0402170001,Text);

        // Munition
        DrawEntry(DrawTop,IASchuss,Format(FInteger,[fRaumschiff.WaffenZelle[Dummy].Munition]));

        DrawLine(DrawTop);
      end;
    end;
  end;
end;

procedure TDXItemInfo.DrawSoldatInfo(Surface: TDirectDrawSurface);
var
  DrawTop : Integer;
  Rang    : String;
begin
  fBeginnEntry:=75;
  DrawTop:=Top+75;
  if Surface<>nil then
  begin
    soldaten_api_DrawDXSoldat(fSoldat,Surface,fMem,Left+8,Top+26,true);
    Rang:=GetSoldatenRang(fSoldat.EP)+' - '+fSoldat.ClassName;
    WhiteBStdFont.Draw(Surface,Left+((Width) shr 1)-(WhiteStdFont.TextWidth(Rang) shr 1),Top+54,Rang);
    WhiteStdFont.Draw(Surface,Left+((Width) shr 1)-(WhiteStdFont.TextWidth(fSoldat.Name) shr 1),Top+30,fSoldat.Name);
  end;
  DrawLine(DrawTop);
  {$IFDEF SHOWEP}
  DrawEntry(DrawTop,'Erfahrungspunkte:',Format(FFloat,[fSoldat.EP/1]));
  {$ENDIF}
  DrawEntry(DrawTop,ST0311160001,'');

  // Eigenschaften zeichnen
  DrawProperty(DrawTop,IIGesundheit,Format('%d / %d',[trunc(fSoldat.Ges),trunc(fSoldat.MaxGes)]),fSoldat.MaxGes,fIncMaxGes);
  if fSoldatManager=nil then
    DrawProperty(DrawTop,IITimeUnits,Format(FInteger,[trunc(fSoldat.Zeit)]),fSoldat.Zeit,fIncTimeUnits)
  else
    DrawProperty(DrawTop,IITimeUnits,Format('(%d) %d',[fSoldatManager.ZEKapazitat,fSoldatManager.UnitZeitEinheiten]),fSoldat.Zeit,fIncTimeUnits);
  DrawProperty(DrawTop,ST0311170002,Format(FInteger,[trunc(fSoldat.Sicht)]),fSoldat.Sicht,fIncSicht);
  DrawProperty(DrawTop,IITreff,Format(FInteger,[trunc(fSoldat.Treff)]),fSoldat.Treff,fIncTreff);
  DrawProperty(DrawTop,IIKraft,Format(FInteger,[trunc(fSoldat.Kraft)]),fSoldat.Kraft,fIncKraft);
  DrawProperty(DrawTop,IIPsiAn,Format(FInteger,[trunc(fSoldat.PsiAn)]),fSoldat.PsiAn,fIncPSIAn);
  DrawProperty(DrawTop,IIPsiAb,Format(FInteger,[trunc(fSoldat.PsiAb)]),fSoldat.PsiAb,fIncPSIAb);
  DrawProperty(DrawTop,ST0403080001,Format(FInteger,[trunc(fSoldat.IQ)]),fSoldat.IQ,fIncIQ);
  DrawProperty(DrawTop,ST0403310001,Format(FInteger,[trunc(fSoldat.React)]),fSoldat.React,fIncReact);

  DrawLine(DrawTop);
  if fSoldat.LeftHand then
    DrawEntry(DrawTop,IIHandStatus,IILeftHand)
  else
    DrawEntry(DrawTop,IIHandStatus,IIRightHand);
  DrawLine(DrawTop);
  DrawEntry(DrawTop,IITag,Format(FFloat,[fSoldat.Tag/1]));
  DrawEntry(DrawTop,IIEinsatz,Format(FInteger,[fSoldat.Einsatz]));
  DrawEntry(DrawTop,IITreffer,Format(FInteger,[fSoldat.Treffer]));
  DrawLine(DrawTop);
  DrawEntry(DrawTop,IIWert,Format(FFloat,[GetSoldatWert(fSoldat)/1]));
  DrawLine(DrawTop);
end;

procedure TDXItemInfo.DrawUFO(Surface: TDirectDrawSurface);
var
  DrawTop : Integer;
  Text    : String;
begin
  if fUFO=nil then exit;
  fBeginnEntry:=67;
  DrawTop:=Top+67;
  fUFO.Draw(Surface,Left+8,Top+28);
  Text:=fUFO.Name;
  YellowStdFont.Draw(Surface,Left+8+((Width-16) shr 1)-(YellowStdFont.TextWidth(Text) shr 1),Top+28,Text);
  Text:=fUFO.Model.Name;
  WhiteStdFont.Draw(Surface,Left+8+((Width-16) shr 1)-(WhiteStdFont.TextWidth(Text) shr 1),Top+45,Text);
  DrawLine(DrawTop);
  DrawEntry(DrawTop,IAHitPoints,Format(FRoom,[fUFO.Hitpoints/1,fUFO.Model.HitPoints/1]));
  DrawLine(DrawTop);
  if fUFO.OverLand then
    DrawEntry(DrawTop,LLand,'')
  else
    DrawEntry(DrawTop,LWasser,'');
  DrawLine(DrawTop);
  DrawEntry(DrawTop,IAShieldPoints,Format(FFloat,[fUFO.Model.Shield/1]));
  DrawEntry(DrawTop,IAPrDefend,Format(FPercent,[fUFO.Model.ShieldType.Projektil]));
  DrawEntry(DrawTop,IARaDefend,Format(FPercent,[fUFO.Model.ShieldType.Rakete]));
  DrawEntry(DrawTop,IALaDefend,Format(FPercent,[fUFO.Model.ShieldType.Laser]));
  DrawEntry(DrawTop,IAAufLade,Format(FMSeconds,[fUFO.Model.ShieldType.Laden]));
  DrawLine(DrawTop);
  DrawEntry(DrawTop,IAStrengthW,Format(FFloat,[fUFO.Model.Angriff/1]));
  DrawEntry(DrawTop,IITreff,Format(FPercent,[fUFO.Model.Treffsicherheit]));
  DrawEntry(DrawTop,ST0501080005,Format(FPoints,[fUFO.Model.Points/1]));
  DrawEntry(DrawTop,ST0309220014,Format(FInteger,[fUFO.Besatzung]));
  DrawLine(DrawTop);
end;

procedure TDXItemInfo.DrawUFOModel(Surface: TDirectDrawSurface);
var
  DrawTop : Integer;
  Text    : String;
begin
  fBeginnEntry:=67;
  DrawTop:=Top+67;
  Text:=fUFOModel.Name;
  ufo_api_DrawIcon(Surface,Left+8,Top+28,fUFOModel.Image);
  YellowStdFont.Draw(Surface,Left+8+((Width-16) shr 1)-(YellowStdFont.TextWidth(Text) shr 1),Top+28,Text);
  if fTextViewer.Visible then exit;
  DrawLine(DrawTop);
  DrawEntry(DrawTop,IAHitPoints,Format(FFloat,[fUFOModel.HitPoints/1]));
  DrawLine(DrawTop);
  DrawEntry(DrawTop,IAShieldPoints,Format(FFloat,[fUFOModel.Shield/1]));
  DrawEntry(DrawTop,IAPrDefend,Format(FPercent,[fUFOModel.ShieldType.Projektil]));
  DrawEntry(DrawTop,IARaDefend,Format(FPercent,[fUFOModel.ShieldType.Rakete]));
  DrawEntry(DrawTop,IALaDefend,Format(FPercent,[fUFOModel.ShieldType.Laser]));
  DrawEntry(DrawTop,IAAufLade,Format(FMSeconds,[fUFOModel.ShieldType.Laden]));
  DrawLine(DrawTop);
  DrawEntry(DrawTop,IAStrengthW,Format(FFloat,[fUFOModel.Angriff/1]));
  DrawEntry(DrawTop,IITreff,Format(FPercent,[fUFOModel.Treffsicherheit]));
  DrawEntry(DrawTop,ST0501080005,Format(FPoints,[fUFOModel.Points/1]));
  DrawEntry(DrawTop,LPPS,Format(FInteger,[fUFOModel.PPS]));
  DrawEntry(DrawTop,ST0309220014,Format('%.0n - %.0n',[fUFOModel.MinBesatz/1,fUFOModel.MinBesatz+fUFOModel.ZusBesatz/1]));
  DrawLine(DrawTop);
end;

procedure TDXItemInfo.FontChange(Sender: TObject);
begin
  Container.Lock;
  fInfoTextButton.Font:=Font;
  fInfoTextButton.Font.Color:=clWhite;
  fTextViewer.Font:=Font;
  Container.UnLock;
  Font.OnChange:=nil;
  Font.Color:=clWhite;
  Font.Style:=[fsBold];
  fDBoldFont:=FontEngine.FindDirectFont(Font,clBlack);
  Font.Style:=[];
  Font.Color:=$00A03000;
  fImprovedFont:=FontEngine.FindDirectFont(Font,clBlack);
  Font.Color:=clWhite;
  Font.OnChange:=FontChange;
  inherited;
end;

function TDXItemInfo.GetFrameRect: TRect;
begin
  result:=ClientRect;
  if fCorners in [rcLeft,rcLeftTop,rcLeftBottom,rcNone] then inc(result.Right,20);
  if fCorners in [rcTop,rcLeftTop,rcRightTop,rcNone] then inc(result.Bottom,20);
  if fCorners in [rcRight,rcRightTop,rcRightBottom,rcNone] then dec(result.Left,20);
  if fCorners in [rcBottom,rcLeftBottom,rcRightBottom,rcNone] then dec(result.Top,20);
end;

procedure TDXItemInfo.InfoButtonClick(Sender: TObject);
begin
  Container.Lock;
  fTextViewer.Visible:=not fInfoTextButton.HighLight;

  fInfoTextButton.HighLight:=not fInfoTextButton.HighLight;

  if fInfoTextButton.Highlight=true then
  begin
    fInfoTextButton.Text:=BInfoDaten;
    fInfoTextButton.Hint:=HInfoDaten;
  end
  else
  begin
    fInfoTextButton.Text:=BInfoText;
    fInfoTextButton.Hint:=HInfoText;
  end;

  fScrollBar.Visible:=not fInfoTextButton.HighLight and fScrollVisible;
  Container.UnLock;
  Redraw;
end;

procedure TDXItemInfo.OnScroll(Sender: TObject);
begin
  Container.RedrawArea(fEntryRect,Container.Surface);
end;

procedure TDXItemInfo.Resize(var NewLeft, NewTop, NewWidth,
  NewHeight: Integer);
begin
  fInfoTextButton.SetRect(NewLeft,NewTop+NewHeight-28,NewWidth,28);
  fTextViewer.SetRect(NewLeft,NewTop+67,NewWidth,NewHeight-95);
  fScrollBar.SetRect((NewLeft+NewWidth)-17,NewTop+67,17,NewHeight-95);
end;

procedure TDXItemInfo.SetAlien(const Value: TAlien);
begin
  fAlien:=Value;
  fInfoType:=itAlien;
  fItemValid:=true;
  Container.Lock;
  InfoText(fAlien.Info);
  fScrollVisible:=false;
  if fScrollBar.Visible then fScrollBar.Visible:=false;
  Container.UnLock;
  Redraw;
end;

procedure TDXItemInfo.SetBlendColor(const Value: TBlendColor);
begin
  fBlendColor := Value;
  fInfoTextButton.BlendColor:=Value;
  fTextViewer.BlendColor:=Value;
  fScrollBar.FirstColor:=Value;
  Redraw;
end;

procedure TDXItemInfo.SetCaption(const Value: String);
begin
  fCaption := Value;
  Redraw;
end;

procedure TDXItemInfo.SetColor(const Value: TColor);
begin
  fColor := Container.Surface.ColorMatch(Value);
  fInfoTextButton.FirstColor:=Value;
  if Value=clMaroon then
  begin
    fInfoTextButton.SecondColor:=clRed;
  end
  else if Value=clNavy then
  begin
    fInfoTextButton.SecondColor:=clBlue;
  end;
  fTextViewer.BorderColor:=Value;
  fScrollBar.SecondColor:=Value;
  Redraw;
end;

procedure TDXItemInfo.SetComponentHint(const Value: String);
begin
  inherited;
  fTextViewer.Hint:=Value;
  fScrollBar.Hint:=Value;
end;

procedure TDXItemInfo.SetCorners(const Value: TRoundCorners);
begin
  fCorners := Value;
  case Value of
    rcAll,rcBottom        : fInfoTextButton.RoundCorners:=rcBottom;
    rcRight,rcRightBottom : fInfoTextButton.RoundCorners:=rcRightBottom;
    rcLeft,rcLeftBottom   : fInfoTextButton.RoundCorners:=rcLeftBottom;
    else fInfoTextButton.RoundCorners:=rcNone;
  end;
  fCorner:=[];
  if fCorners in [rcLeft,rcTop,rcLeftTop,rcAll] then include(fCorner,cLeftTop);
  if fCorners in [rcRight,rcTop,rcRightTop,rcAll] then include(fCorner,cRightTop);
  if fCorners in [rcLeft,rcBottom,rcLeftBottom,rcAll] then include(fCorner,cLeftBottom);
  if fCorners in [rcRight,rcBottom,rcRightBottom,rcAll] then include(fCorner,cRightBottom);
  CreateMouseRegion;
  Redraw;
end;

procedure TDXItemInfo.SetEinrichtung(const Value: TEinrichtung);
begin
  fEinrichtung := Value;
  fInfoType:=itEinrichtung;
  fItemValid:=true;
  if fEinStatus then
  begin
    SetInfoText(false);
  end
  else
  begin
    Container.Lock;
    InfoText(fEinrichtung.Info);
    CalculateScrollBar;
    Container.UnLock;
  end;
  Redraw;
end;

procedure TDXItemInfo.SetEinStatus(const Value: boolean);
begin
  fEinStatus := Value;
  if fInfoType=itEinrichtung then Redraw;
end;

procedure TDXItemInfo.SetInfos(const Value: TInfos);
begin
  fInfos := Value;
  Redraw;
end;

procedure TDXItemInfo.SetInfoText(InfoText: boolean);
begin
  fInfoText:=InfoText;
  if fInfoText then
  begin
    Container.Lock;
    if Visible then fInfoTextButton.Visible:=true;
    if fInfoTextButton.HighLight then fTextViewer.Visible:=true;
    fTextViewer.SetRect(Left,Top+67,Width,Height-95);
    fTextViewer.RoundCorners:=rcNone;
    Container.UnLock;
  end
  else
  begin
    Container.Lock;
    fInfoTextButton.Visible:=false;
    fTextViewer.Visible:=false;
    Container.UnLock;
  end;
end;

procedure TDXItemInfo.SetInfoType(const Value: TInfoType);
begin
  fInfoType := Value;
  Redraw;
end;

procedure TDXItemInfo.SetItemUpgrade(const Value: TItemUpgrade);
begin
  fItemUpgrade := Value;
  fInfoType:=itUpgrade;
  fItemValid:=true;
  SetInfoText(false);
  fScrollVisible:=false;
  if fScrollBar.Visible then fScrollBar.Visible:=false;
  Redraw;
end;

procedure TDXItemInfo.SetItemValid(const Value: boolean);
begin
  fItemValid := Value;
  SetInfoText(false);
  Redraw;
end;

procedure TDXItemInfo.SetLagerItem(const Value: TLagerItem);
begin
  fLagerItem := Value;
  fInfoType:=itLagerItem;
  fItemValid:=true;
  Container.Lock;

  if fLagerItem.Useable then
    InfoText(fLagerItem.Info)
  else
    InfoText(forsch_api_GetProject(fLagerItem.ID).ResearchInfo);

  fScrollVisible:=false;
  EnumMunitionenForWaffe;
  CalculateScrollBar;
  Container.UnLock;
  Redraw;
end;

procedure TDXItemInfo.SetModel(const Value: TRaumschiffModel);
begin
  fModel := Value;
  fInfoType:=itModel;
  fItemValid:=true;
  Container.Lock;
  InfoText(fModel.Info);
  fScrollVisible:=false;
  if fScrollBar.Visible then fScrollBar.Visible:=false;
  Container.UnLock;
  Redraw;
end;

procedure TDXItemInfo.SetOrganisation(const Value: TLand);
begin
  fOrganisation := Value;
  fInfoType:=itOrganisation;
  fItemValid:=true;
  SetInfoText(false);
  fScrollVisible:=false;
  EnumLagerForOrganisation;
  CalculateScrollBar;
  Redraw;
end;

procedure TDXItemInfo.SetProduktion(const Value: TProduktion);
begin
  fProduktion := Value;
  fInfoType:=itProduktion;
  fItemValid:=true;
  SetInfoText(false);
  fScrollVisible:=false;
  if fScrollBar.Visible then fScrollBar.Visible:=false;
  Redraw;
end;

procedure TDXItemInfo.SetRaumschiff(const Value: TRaumschiff);
begin
  fRaumschiff := Value;
  if fRaumschiff=nil then
  begin
    fItemValid:=false;
    exit;
  end;
  fItemValid:=true;
  fInfoType:=itRaumschiff;
  fScrollVisible:=false;
  if fScrollBar.Visible then fScrollBar.Visible:=false;
  SetInfoText(false);
  Redraw;
end;

procedure TDXItemInfo.ShowSoldatInfo(var Value: TSoldatInfo; Manager: TGameFigureManager);
begin
  fSoldat := Value;
  if fSoldatManager<>nil then
  begin
    fSoldatManager.NotifyList.RemoveEvent(fSoldatEvent);
    fSoldatManager.NotifyList.RemoveEvent(fSoldatDestroy);
  end;

  fSoldatManager:=Manager;

  if fSoldatManager<>nil then
  begin
    fSoldatEvent:=fSoldatManager.NotifyList.RegisterEvent(EVENT_MANAGER_ONCHANGE,ChangeKapazitat);
    fSoldatDestroy:=fSoldatManager.NotifyList.RegisterEvent(EVENT_MANAGER_ONDESTROY,ManagerDestroy);
  end;

  fIncMaxGes:=trunc(fSoldat.MaxGes)>trunc(fSoldat.LastMaxGes);
  fIncSicht:=trunc(fSoldat.Sicht)>trunc(fSoldat.LastSicht);
  fIncTimeUnits:=trunc(fSoldat.Zeit)>trunc(fSoldat.LastZeit);
  fIncPSIAn:=trunc(fSoldat.PSIAn)>trunc(fSoldat.LastPSIAn);
  fIncPSIAb:=trunc(fSoldat.PSIAb)>trunc(fSoldat.LastPSIAb);
  fIncTreff:=trunc(fSoldat.Treff)>trunc(fSoldat.LastTreff);
  fIncKraft:=trunc(fSoldat.Kraft)>trunc(fSoldat.LastKraft);
  fIncIQ:=trunc(fSoldat.IQ)>trunc(fSoldat.LastIQ);
  fIncReact:=trunc(fSoldat.React)>trunc(fSoldat.LastReact);

  Value.LastMaxGes:=Value.MaxGes;
  Value.LastZeit:=Value.Zeit;
  Value.LastPSIAn:=Value.PSIAn;
  Value.LastPSIAb:=Value.PSIAb;
  Value.LastTreff:=Value.Treff;
  Value.LastKraft:=Value.Kraft;
  Value.LastSicht:=Value.Sicht;
  Value.LastIQ:=Value.IQ;
  Value.LastReact:=Value.React;

  fInfoType:=itSoldat;
  fItemValid:=true;
  SetInfoText(false);
  fScrollVisible:=false;
  CalculateScrollBar;
  Redraw;
end;

procedure TDXItemInfo.SetUFO(const Value: TUFO);
begin
  fUFO := Value;
  fInfoType:=itUFO;
  fItemValid:=true;
  SetInfoText(false);
  fScrollVisible:=false;
  if fScrollBar.Visible then fScrollBar.Visible:=false;
  Redraw;
end;

procedure TDXItemInfo.SetUFOModel(const Value: TUFOModel);
begin
  fUFOModel := Value;
  fInfoType:=itUFOModel;
  fItemValid:=true;
  Container.Lock;
  InfoText(fUFOModel.Info);
  fScrollVisible:=false;
  if fScrollBar.Visible then fScrollBar.Visible:=false;
  Container.UnLock;
  Redraw;
end;

procedure TDXItemInfo.VisibleChange;
begin
  Container.Lock;
  if Visible then
  begin
    fInfoTextButton.Visible:=fInfoText;
    fTextViewer.Visible:=fInfoTextButton.Visible and fInfoTextButton.HighLight;
  end
  else
  begin
    fInfoTextButton.Visible:=false;
    fTextViewer.Visible:=false;
  end;
  fScrollBar.Visible:=Visible and fScrollVisible;
  Container.UnLock;
end;

procedure TDXItemInfo.EnumDrawEntry(var DrawTop: Integer; Text1,
  Text2: String);
begin
  inc(NeedHeight,EntrySkip);
  fWasLine:=false;
end;

procedure TDXItemInfo.EnumDrawLine(var DrawTop: Integer);
begin
  if fWasLine then
    exit;

  inc(NeedHeight,LineSkip);
  fWasLine:=true;
end;

procedure TDXItemInfo.DoScroll(Direction: TScrollDirection; Pos: TPoint);
begin
  if not fScrollBar.Visible then exit;
  if not PtInRect(fEntryRect,Pos) then exit;
  if Direction=sdUp then fScrollBar.Value:=(fScrollBar.Value-fScrollBar.SmallChange shl 1);
  if Direction=sdDown then fScrollBar.Value:=(fScrollBar.Value+fScrollBar.SmallChange shl 1);
end;

procedure TDXItemInfo.EnumLagerForOrganisation;
begin
  lager_api_EnumLandItems(fStrings,fOrganisation.InfoIndex);
end;

procedure TDXItemInfo.EnumMunitionenForWaffe;
begin
  lager_api_EnumMunitionen(fStrings,fLagerItem.ID);
end;

procedure TDXItemInfo.InfoText(Description: String);
begin
  if (not fShowInfoButton) or (Description=EmptyStr) then
  begin
    SetInfoText(False);
  end
  else
  begin
    SetInfoText(true);
    fTextViewer.Text:=Description;
  end;
end;

procedure TDXItemInfo.DefDrawProperty(var DrawTop: Integer; Text1,
  Text2: String; prop: double;Increased: boolean);
var
  PaintTop   : Integer;
  PaintRight : Integer;
begin
  if fScrollVisible then
  begin
    PaintTop:=DrawTop-fScrollBar.Value;
    PaintRight:=Right-16;
  end
  else
  begin
    PaintTop:=DrawTop;
    PaintRight:=Right;
  end;

  HLine(fSurface,fMem,Left+1,PaintRight-2,PaintTop,fColor);

  VLine(fSurface,fMem,PaintTop,PaintTop+19,Left+21,fColor);
  VLine(fSurface,fMem,PaintTop,PaintTop+19,PaintRight-50,fColor);

  BlendRectangle(Rect(Left+22,PaintTop+1,Left+22+round((((PaintRight-50)-(Left+22)))*frac(Prop)),PaintTop+19),128,bcBlack,fSurface,fMem);

  if Increased then
    fSurface.Draw(Left+3,PaintTop+3,Rect(106,43,122,57),Container.ImageList.Items[1].PatternSurfaces[0]);
  YellowStdFont.DrawInRect(fSurface,fEntryRect,Left+25,PaintTop+4,Text1);
  fValueFont.DrawInRect(fSurface,fEntryRect,PaintRight-8-fValueFont.TextWidth(Text2),PaintTop+4,Text2);

  Inc(DrawTop,20);
end;

procedure TDXItemInfo.EnumDrawProperty(var DrawTop: Integer; Text1,
  Text2: String; prop: double;Increased: boolean);
begin
  inc(NeedHeight,20);
end;

procedure TDXItemInfo.ChangeKapazitat(Sender: TObject);
begin
  if (fInfoType=itSoldat) and (Sender=fSoldatManager) then
  begin
    Container.UnLockAll;
    Container.IncLock;
    Redraw;
    Container.DecLock;
    Container.LockAll;
  end;
end;

procedure TDXItemInfo.SetProjekt(const Value: TForschungen;Name: String);
begin
  fProjekt := Value;
  fName := Name;
  fInfoType:=itProjekt;
  fItemValid:=true;

  Container.Lock;
  InfoText(Format(ST0401200004+' '+FStunden+#13#10#13#10'%s',[fProjekt.Hour,fProjekt.Info]));
  SetInfoText(false);
  fTextViewer.Visible:=true;
  fTextViewer.SetRect(Left,Top+67,Width,Height-67);
  fTextViewer.RoundCorners:=rcBottom;
  CalculateScrollBar;
  Container.UnLock;
  Redraw;
end;

procedure TDXItemInfo.DrawProjekt(Surface: TDirectDrawSurface);
var
  DrawTop  : Integer;
  Text     : String;
begin
  fBeginnEntry:=67;
  DrawTop:=Top+67;
  if Surface<>nil then
  begin
    Text:=fName;
    YellowStdFont.Draw(Surface,Left+8+((Width-16) shr 1)-(YellowStdFont.TextWidth(Text) shr 1),Top+28,Text);
    Text:=ST0401200004;
    WhiteStdFont.Draw(Surface,Left+8+((Width-16) shr 1)-(WhiteStdFont.TextWidth(Text) shr 1),Top+45,Text);
  end;
  if fTextViewer.Visible then exit;
  DrawLine(DrawTop);
  DrawEntry(DrawTop,IAForschTime,Format(FStunden,[fProjekt.Gesamt]));
end;

procedure TDXItemInfo.ManagerDestroy(Sender: TObject);
begin
  fSoldatManager:=nil;
end;

procedure TDXItemInfo.DrawStrings(var DrawTop: Integer; Titel: String);
var
  Dummy: Integer;
begin
  if length(fStrings)>0 then
  begin
    DrawEntry(DrawTop,Titel,'');
    for Dummy:=0 to high(fStrings) do
      DrawEntry(DrawTop,fStrings[Dummy],'');
    DrawLine(DrawTop);
  end;
end;

procedure TDXItemInfo.SetBasis(const Value: TBasis);
begin
  fBasis := Value;
  fInfoType:=itBasis;
  fItemValid:=fBasis<>nil;
  SetInfoText(false);
  fScrollVisible:=false;
  if fScrollBar.Visible then
    fScrollBar.Visible:=false;
  Redraw;
end;

procedure TDXItemInfo.SetBasisStatus(const Value: TBasis);
begin
  fBasisStatus := Value;
  fInfoType:=itBasisStatus;
  fItemValid:=fBasisStatus<>nil;
  SetInfoText(false);
  fScrollVisible:=false;
  if fScrollBar.Visible then
    fScrollBar.Visible:=false;
  Redraw;
end;

procedure TDXItemInfo.DrawBasis(Surface: TDirectDrawSurface);
var
  DrawTop: Integer;

  procedure DrawRoom(Room: String; FormatStr: String; Belegt,Gesamt: double);
  begin
    if Gesamt>0 then
    begin
      DrawEntry(DrawTop,Room,Format(FormatStr,[Belegt,Gesamt]));
    end;
  end;
begin
  fBeginnEntry:=67;
  DrawTop:=Top+67;
  if Surface<>nil then
  begin
    YellowBStdFont.Draw(Surface,Left+8+((Width-16) shr 1)-(YellowBStdFont.TextWidth(Basis.Name) shr 1),Top+28,Basis.Name);
    WhiteStdFont.Draw(Surface,Left+8+((Width-16) shr 1)-(WhiteStdFont.TextWidth(BaseNames[Basis.BaseType]) shr 1),Top+48,BaseNames[Basis.BaseType]);
  end;
  DrawLine(DrawTop);
  DrawEntry(DrawTop,ST0501080004,Format(FCredits,[Basis.RunningCostWeek]));
  DrawLine(DrawTop);
  DrawEntry(DrawTop,IASensorRange,Format(FKMetres,[Basis.SensorWidth/1]));
  
  if Basis.ShieldStrength>0 then
    DrawEntry(DrawTop,IAShieldPoints,Format(FFloat,[Basis.ShieldStrength/1]));

  DrawEntry(DrawTop,ST0501110001,Format(FFloat,[Basis.AlphatronStorage/1]));
  DrawLine(DrawTop);
  if Basis.BaseType=btAlphatronMine then
  begin
    DrawEntry(DrawTop,ST0501080002,Format(FDezimal2,[Basis.AlphatronPerHour]));
    DrawEntry(DrawTop,ST0501080003,Format(FFloatPercent,[Basis.AlphatronMiningEffectiveness*100]));
    DrawLine(DrawTop);
  end;

  DrawRoom(LBLagerRaum,FLagerRoom,Basis.LagerBelegt,Basis.LagerRaum);
  DrawRoom(LBWohnRaum,FRoom,Basis.WohnBelegt,Basis.WohnRaum);
  DrawRoom(LBLaborRaum,FRoom,Basis.LaborBelegt,Basis.LaborRaum);
  DrawRoom(LBWerkRaum,FRoom,Basis.WerkBelegt,Basis.WerkRaum);
  DrawRoom(IASmallSchiff,FRoom,Basis.HangerBelegt,Basis.HangerRaum);
end;

procedure TDXItemInfo.DrawBasisStatus(Surface: TDirectDrawSurface);
var
  DrawTop: Integer;

  procedure DrawRoom(Room: String; FormatStr: String; Belegt,Gesamt: double);
  begin
    if Gesamt>0 then
    begin
      DrawEntry(DrawTop,Room,Format(FormatStr,[Belegt,Gesamt]));
    end;
  end;
begin
  fBeginnEntry:=67;
  DrawTop:=Top+67;
  if Surface<>nil then
  begin
    YellowBStdFont.Draw(Surface,Left+8+((Width-16) shr 1)-(YellowBStdFont.TextWidth(BasisStatus.Name) shr 1),Top+28,BasisStatus.Name);
    WhiteStdFont.Draw(Surface,Left+8+((Width-16) shr 1)-(WhiteStdFont.TextWidth(BaseNames[BasisStatus.BaseType]) shr 1),Top+48,BaseNames[BasisStatus.BaseType]);
  end;
  DrawLine(DrawTop);
  DrawEntry(DrawTop,ST0501080004,Format(FCredits,[BasisStatus.RunningCostWeek]));
  DrawLine(DrawTop);
  DrawEntry(DrawTop,IASensorRange,Format(FKMetres,[BasisStatus.SensorWidth/1]));
  DrawEntry(DrawTop,MK0506050001,Format(FKMetres,[BasisStatus.AbwehrRange/1]));

  if BasisStatus.ShieldStrength>0 then
    DrawEntry(DrawTop,IAShieldPoints,Format(FFloat,[BasisStatus.ShieldStrength/1]));

  DrawEntry(DrawTop,ST0501110001,Format(FFloat,[BasisStatus.AlphatronStorage/1]));
  DrawLine(DrawTop);
  if BasisStatus.BaseType=btAlphatronMine then
  begin
    DrawEntry(DrawTop,ST0501080002,Format(FDezimal2,[BasisStatus.AlphatronPerHour]));
    DrawEntry(DrawTop,ST0501080003,Format(FFloatPercent,[BasisStatus.AlphatronMiningEffectiveness*100]));
    DrawLine(DrawTop);
  end;

  DrawRoom(LBLagerRaum,FLagerRoom,BasisStatus.LagerBelegt,BasisStatus.LagerRaum);
  DrawRoom(LBWohnRaum,FRoom,BasisStatus.WohnBelegt,BasisStatus.WohnRaum);
  DrawRoom(LBLaborRaum,FRoom,BasisStatus.LaborBelegt,BasisStatus.LaborRaum);
  DrawRoom(LBWerkRaum,FRoom,BasisStatus.WerkBelegt,BasisStatus.WerkRaum);
  DrawRoom(IASmallSchiff,FRoom,BasisStatus.HangerBelegt,BasisStatus.HangerRaum);

  DrawLine(DrawTop);
  DrawEntry(DrawTop,LSolGehalt,Format(FCredits,[soldaten_api_GetWeeklySold(BasisStatus.ID)/1]));
  DrawEntry(DrawTop,LForGehalt,Format(FCredits,[forsch_api_GetWeeklySold(BasisStatus.ID)/1]));
  DrawEntry(DrawTop,LHerGehalt,Format(FCredits,[werkstatt_api_GetWeeklySold(BasisStatus.ID)/1]));
  DrawEntry(DrawTop,LRauBau,Format(FCredits,[raumschiff_api_GetWeeklyCost(BasisStatus.ID)/1]));

  DrawLine(DrawTop);
  DrawEntry(DrawTop,IASoldaten,Format(FInteger,[soldaten_api_GetSoldatenCount(BasisStatus.ID)]));
  DrawEntry(DrawTop,LWissenschaf,Format(FIntFahig,[
    forsch_api_GetForscherCount(BasisStatus.ID),
    forsch_api_GetAllStrength(BasisStatus.ID)/1]));
  DrawEntry(DrawTop,IATechniker,Format(FIntFahig,[
    werkstatt_api_GetTechnikerCount(BasisStatus.ID),
    werkstatt_api_GetAllStrength(BasisStatus.ID)/1]));
  DrawEntry(DrawTop,LEinrichtungen,Format(FInteger,[BasisStatus.Count]));
end;

{ TDXPopupItemInfo }

constructor TDXPopupItemInfo.Create(Page: TDXPage);
begin
  inherited;
  Visible:=false;
  fAlpha:=0;
  fMunition:=-1;
  fInfoType:=itNone;
  Enabled:=false;
  fBColor:=bcLime;
  fBlendColor:=bcGreen;
  DrawEntry:=DefDrawEntry;
  DrawLine:=DefDrawLine;

  fShowNameOnly:=false;
end;

destructor TDXPopupItemInfo.destroy;
begin
  inherited;
end;

procedure TDXPopupItemInfo.Draw(Surface: TDirectDrawSurface;var Mem: TDDSurfaceDesc);
begin
  Self.Surface:=Surface;
  if AlphaElements then
  begin
    BlendRectangle(CorrectBottomOfRect(ClientRect),fAlpha*200 div 255,fBlendColor,Surface);
//    Surface.Draw(Left,Top,PopupTempSurface,false);
    if fAlpha<>255 then
      DrawTransAlpha(Surface,Left,Top,Rect(0,0,Width,Height),PopupTempSurface,fAlpha,$1234)
    else
    begin
      Surface.Draw(Left,Top,PopupTempSurface);
    end;
  end
  else
  begin
    Surface.FillRect(ClientRect,fBlendColor);
    Surface.Lock(Mem);
    Surface.UnLock;
    Rectangle(Surface,Mem,ClientRect,fBColor);
    GlobalMem:=Mem;
    fLeftOffSet:=Left;
    fTopOffSet:=Top;
    case fInfoType of
      itSoldat    : DrawSoldat;
      itLagerItem : DrawLagerItem;
    end;
  end;
end;

procedure TDXPopupItemInfo.DefDrawEntry(var DrawTop: Integer; Text1,
  Text2: String);
begin
  YellowStdFont.Draw(Surface,fLeftOffSet+8,DrawTop,Text1);
  WhiteStdFont.Draw(Surface,fLeftOffSet+Width-8-WhiteStdFont.TextWidth(Text2),DrawTop,Text2);
  Inc(DrawTop,EntrySkip);
end;

procedure TDXPopupItemInfo.DrawLagerItem;
var
  Text    : String;
  DrawTop : Integer;
  Dummy   : Integer;
begin
  if Surface<>nil then
  begin
    if not fShowNameOnly then
    begin
      DrawTop:=fTopOffSet+47;
      lager_api_DrawItem(fLagerItem.ImageIndex,Surface,fLeftOffSet+8,fTopOffSet+8);
      Text:=fLagerItem.Name;
      YellowStdFont.Draw(Surface,fLeftOffSet+48,fTopOffSet+8,Text);
    end
    else
    begin
      Text:=fLagerItem.Name;
      YellowStdFont.Draw(Surface,fLeftOffSet+8,fTopOffSet+4,Text);
      exit;
    end;

    Text:=game_utils_TypeToStr(fLagerItem.TypeId);
    Text:=Format(IALagerItem,[Text,fLagerItem.Level]);
    WhiteStdFont.Draw(Surface,fLeftOffSet+48,fTopOffSet+25,Text);
  end;

  DrawLine(DrawTop);

  // noch nicht erforschte Alienausrüstung ?
  if not fLagerItem.Useable then
  begin
    DrawEntry(DrawTop,ST0310270001,'');
    exit;
  end;

  // Waffenart
  if (fLagerItem.TypeID in [ptWaffe,ptMunition,ptRWaffe,ptRMunition]) then
  begin
    Text:=game_utils_WaffTypeToStr(fLagerItem.WaffType);
    DrawEntry(DrawTop,IAArt,Text);
  end;

  // Einhand-/Zweihandwaffe
  if fLagerItem.TypeID=ptWaffe then
  begin
    if fLagerItem.Einhand then
      DrawEntry(DrawTop,IAWaffType,IIOneHandWeapon)
    else
      DrawEntry(DrawTop,IAWaffType,IITwoHandWeapon);
  end;

  // Gewicht
  if (fLagerItem.TypeID in [ptWaffe,ptMunition,ptPanzerung,ptSensor,ptGranate,ptMine,ptGuertel]) then
  begin
    if (fMunition=-1) then
      DrawEntry(DrawTop,IAGewicht,Format(FDezimal1,[fLagerItem.Gewicht]))
    else
      DrawEntry(DrawTop,IAGewicht,Format(FDezimal1,[GetMunitionGewicht(fLagerItem.Gewicht,fLagerItem.Munition,fMunition)]))
  end;

  // Lagerverbrauch
  if (fLagerItem.TypeID in LagerItemType) then
     DrawEntry(DrawTop,IALagerVer,Format(FDezimal1,[fLagerItem.LagerV]));

  // Angriffstärke und Zeiteinheiten
  if (fLagerItem.TypeID in [ptWaffe,ptGranate,ptRWaffe]) then
  begin
    if fLagerItem.Strength=0 then
      DrawEntry(DrawTop,IAStrengthW,IAAbhMunition)
    else
    begin
      DrawEntry(DrawTop,IAStrengthW,Format(FInteger,[fLagerItem.Strength]));
      if (fLagerItem.TypeID=ptWaffe) then
        DrawEntry(DrawTop,ST0309220009,Format(FPercent,[fLagerItem.Power]));
    end;
    if fLagerItem.TypeID=ptWaffe then
    begin
      if fLagerItem.WaffType<>wtShortRange then
      begin
//        DrawEntry(DrawTop,IAFrequenz,Format(FMSeconds,[fLagerItem.Laden]));
        DrawEntry(DrawTop,ST0311230005,Format(FInteger,[fLagerItem.Genauigkeit]));
        if stAuto in fLagerItem.Schuss then
          DrawEntry(DrawTop,ST0309220012,Format(FFloat,[fLagerItem.TimeUnits*TimeUnitsMultiplikator[stAuto]/1]));
        if stSpontan in fLagerItem.Schuss then
          DrawEntry(DrawTop,ST0409280001,Format(FFloat,[fLagerItem.TimeUnits*TimeUnitsMultiplikator[stSpontan]/1]));
        if stGezielt in fLagerItem.Schuss then
          DrawEntry(DrawTop,ST0409280002,Format(FFloat,[fLagerItem.TimeUnits*TimeUnitsMultiplikator[stGezielt]/1]));
      end
      else
      begin
//        DrawEntry(DrawTop,ST0401120002,Format(FMSeconds,[fLagerItem.Laden]));
        DrawEntry(DrawTop,ST0401120003,Format(FFloat,[fLagerItem.TimeUnits/1]));
      end;
    end;
  end;
  
  if (fLagerItem.TypeID in [ptMunition]) then
  begin
    Text:=lager_api_GetItem(fLagerItem.Munfor).Name;
    DrawEntry(DrawTop,IAWeapon,Text);
    if fLagerItem.Strength=0 then
      DrawEntry(DrawTop,IAStrengthM,IAAbhWaffe)
    else
    begin
      DrawEntry(DrawTop,IAStrengthM,Format(FInteger,[fLagerItem.Strength]));
      DrawEntry(DrawTop,ST0309220009,Format(FPercent,[fLagerItem.Power]));
    end;
    if fMunition=-1 then
      DrawEntry(DrawTop,IASchuss,Format(FFloat,[fLagerItem.Munition/1]))
    else
      DrawEntry(DrawTop,IASchuss,Format(FRoom,[fMunition/1,fMaxMuni/1]))
  end;

  if (fLagerItem.TypeID=ptMotor) then
  begin
    DrawEntry(DrawTop,IAKapazitat,Format(FLiter,[fLagerItem.Munition/1]));
    if fMunition<>-1 then
      DrawEntry(DrawTop,IAInhalt,Format(FPercent,[fMunition]));
    DrawEntry(DrawTop,IAVerbrauch,Format(FLiterperkm,[fLagerItem.Strength]));
    DrawEntry(DrawTop,LPPS,Format(FInteger,[fLagerItem.PixPerSec]));
    if fLagerItem.Verfolgung then Text:=LYes else Text:=LNo;
    DrawEntry(DrawTop,IADuesen,Text);
  end;

  if (fLagerItem.TypeID=ptMine) then
  begin
    DrawEntry(DrawTop,IAStrengthMine,Format(FInteger,[fLagerItem.Strength]));
    DrawEntry(DrawTop,IARange,Format(FFloat,[fLagerItem.Reichweite/1]));
  end;

  if (fLagerItem.TypeID=ptGuertel) then
  begin
    DrawEntry(DrawTop,ST0309220011,Format(FInteger,[fLagerItem.Strength]));
  end;

  if (fLagerItem.TypeID=ptSensor) then
    DrawEntry(DrawTop,IASensorRange,Format(FInteger,[fLagerItem.Strength]));

  if (fLagerItem.TypeID in [ptRWaffe]) then
  begin
    if fMunition<>-1 then
      DrawEntry(DrawTop,IASchuss,Format(FRoom,[fMunition/1,fMaxMuni/1]))
    else if fMaxMuni<>-1 then
      DrawEntry(DrawTop,IASchuss,Format(FFloat,[fMaxMuni/1]))
    else
      DrawEntry(DrawTop,IASchuss,LNoMuntion);

    if fReserveMuni>0 then
      DrawEntry(DrawTop,ST0401230001,Format(FFloat,[fReserveMuni/1]));
    DrawEntry(DrawTop,IARange,Format(fMetres,[fLagerItem.Reichweite/1]));
  end;

  if (fLagerItem.TypeID in [ptWaffe]) then
  begin
    if fLagerItem.WaffType=wtLaser then
      DrawEntry(DrawTop,IASchuss,Format(FFloat,[fLagerItem.Munition/1]))
  end;

  if (fLagerItem.TypeID=ptPanzerung) then
    DrawEntry(DrawTop,IAPanzer,Format(FInteger,[fLagerItem.Panzerung]));

  if (fLagerItem.TypeID=ptRWaffe) then
  begin
    DrawEntry(DrawTop,IANachLade,Format(FMSeconds,[fLagerItem.Laden]));
    if fLagerItem.Verfolgung then Text:=LYes else Text:=LNo;
    DrawEntry(DrawTop,IAVerfolg,Text);
  end;

  if (fLagerItem.TypeID=ptShield) then
  begin
    DrawEntry(DrawTop,IAShieldPoints,Format(FInteger,[fLagerItem.Strength]));
    DrawEntry(DrawTop,IADefend,Format(FPercent,[fLagerItem.Panzerung]));
    DrawEntry(DrawTop,IAAufLade,Format(FMSeconds,[fLagerItem.Laden]));
  end;

  if (fLagerItem.TypeID in SoldatenTypes) then
    if fLagerItem.NeedIQ>0 then
      DrawEntry(DrawTop,ST0403190001,Format(FInteger,[fLagerItem.NeedIQ]));

  if (fLagerItem.TypeID=ptExtension) then
  begin
    for Dummy:=0 to fLagerItem.ExtCount-1 do
    begin
      with fLagerItem.Extensions[Dummy] do
      begin
        if Value<>0 then
        begin
          DrawEntry(DrawTop,game_utils_NameOfExtension(Prop),game_utils_FormatedExtension(fLagerItem.Extensions[Dummy]));
        end;
      end;
    end;
  end;

end;

procedure TDXPopupItemInfo.DefDrawLine(var DrawTop: Integer);
begin
  if DrawTop>Bottom then exit;
  HLine(Surface,GlobalMem,fLeftOffSet+1,fLeftOffSet+Width-1,DrawTop,fBColor);
  inc(DrawTop,LineSkip);
end;

procedure TDXPopupItemInfo.DrawSoldat;
var
  DrawTop: Integer;
begin
  DrawTop:=fTopOffSet+26;
  YellowStdFont.Draw(Surface,fLeftOffSet+8,fTopOffSet+8,fSoldat.Name);
  DrawLine(DrawTop);
  DrawEntry(DrawTop,IIGesundheit,Format(FRoom,[fSoldat.Ges,fSoldat.MaxGes]));
  DrawEntry(DrawTop,ST0311170002,Format(FFloat,[fSoldat.Sicht]));
  DrawEntry(DrawTop,IITimeUnits,Format(FFloat,[fSoldat.Zeit]));
  DrawEntry(DrawTop,IIPsiAn,Format(FFloat,[fSoldat.PsiAn]));
  DrawEntry(DrawTop,IIPsiAb,Format(FFloat,[fSoldat.PsiAb]));
  DrawEntry(DrawTop,IITreff,Format(FFloat,[fSoldat.Treff]));
  DrawEntry(DrawTop,IIKraft,Format(FFloat,[fSoldat.Kraft]));
  DrawEntry(DrawTop,ST0403080001,Format(FFloat,[fSoldat.IQ]));
  DrawEntry(DrawTop,ST0403310001,Format(FFloat,[fSoldat.React]));
end;

procedure TDXPopupItemInfo.SetAlignRect(const Rect: TRect);
begin
  Top:=Rect.Bottom;
  if Bottom>ScreenHeight then Top:=Rect.Top-Height;
  Left:=Rect.Left;
  if Right>ScreenWidth then Left:=Rect.Right-Width;
  if Width<Rect.Right-Rect.Left then Width:=Rect.Right-Rect.Left;
  if AlphaElements then
    AktuTempSurface;
end;

procedure TDXPopupItemInfo.SetLagerItem(const Value: TLagerItem);
var
  Text      : String;
begin
  fLagerItem := Value;
  fInfoType:=itLagerItem;

  if not fShowNameOnly then
  begin
    // Größe dynamisch ermitteln
    NewHeight:=47;
    NewWidth:=YellowStdFont.TextWidth(fLagerItem.Name);
    Text:=Format(IALagerItem,[game_utils_TypeToStr(fLagerItem.TypeId),fLagerItem.Level]);
    NewWidth:=max(YellowStdFont.TextWidth(Text),NewWidth);
    inc(NewWidth,56);

    DrawEntry:=EnumDrawEntry;
    DrawLine:=EnumDrawLine;
    Surface:=nil;
    DrawLagerItem;
    DrawEntry:=DefDrawEntry;
    DrawLine:=DefDrawLine;

    SetRect(0,0,max(NewWidth,180),NewHeight);
  end
  else
    SetRect(0,0,YellowStdFont.TextWidth(fLagerItem.Name)+16,24);

  Redraw;
end;

procedure TDXPopupItemInfo.SetSoldat(const Value: TSoldatInfo);
begin
  fSoldat := Value;
  fInfoType:=itSoldat;
  SetRect(0,0,115,138);
  Redraw;
end;

procedure TDXPopupItemInfo.EnumDrawEntry(var DrawTop: Integer; Text1,
  Text2: String);
begin
  inc(NewHeight,EntrySkip);

  NewWidth:=max(NewWidth,YellowStdFont.TextWidth(Text1)+WhiteStdFont.TextWidth(Text2)+24);
end;

procedure TDXPopupItemInfo.EnumDrawLine(var DrawTop: Integer);
begin
  inc(NewHeight,LineSkip);
end;

procedure TDXPopupItemInfo.AktuTempSurface;
var
  Mem: TDDSurfaceDesc;
begin
  if PopupTempSurface=nil then
  begin
    { Temporäres Surface anlegen }
    PopupTempSurface:=TDirectDrawSurface.Create(Container.DDraw);
    PopupTempSurface.SystemMemory:=true;
  end;
  PopupTempSurface.SetSize(Width,Height);
  PopupTempSurface.TransparentColor:=$1234;
  PopupTempSurface.Fill($1234);
  PopupTempSurface.Lock(Mem);
  PopupTempSurface.Unlock;
  Surface:=PopupTempSurface;
  GlobalMem:=Mem;
  Rectangle(PopupTempSurface,Mem,0,0,Width,Height,fBColor);
  fLeftOffSet:=0;
  fTopOffSet:=0;
  case fInfoType of
    itSoldat    : DrawSoldat;
    itLagerItem : DrawLagerItem;
  end;
end;

procedure TDXPopupItemInfo.Restore;
begin
  if AlphaElements then
  begin
    PopupTempSurface.Restore;
    AktuTempSurface;
  end;
end;

procedure TDXPopupItemInfo.SetBColor(const Value: TColor);
begin
  fBColor := Container.Surface.ColorMatch(Value);
end;

function TDXPopupItemInfo.AddAlpha(Sender: TObject;
  Frames: Integer): boolean;
begin
  result:=true;
  inc(fAlpha,30);
  if fAlpha>255 then
  begin
    fAlpha:=255;
    Container.DeleteFrameFunction(AddAlpha,Self);
  end;
  Redraw;
end;

function TDXPopupItemInfo.DeleteAlpha(Sender: TObject;
  Frames: Integer): boolean;
begin
  result:=true;
  dec(fAlpha,30);
  if fAlpha<0 then
  begin
    fAlpha:=0;
    Visible:=false;
    Container.DeleteFrameFunction(DeleteAlpha,Self);
    exit;
  end;
  Redraw;
end;

procedure TDXPopupItemInfo.VisibleChange;
begin
  if (not AlphaElements) or (fStatus=3) then exit;
  if not Visible then
  begin
    if fAlpha<>0 then
    begin
      fStatus:=1;
      Visible:=true;
      fStatus:=0;
      Container.DeleteFrameFunction(AddAlpha,Self);
      Container.AddFrameFunction(DeleteAlpha,Self,25);
    end;
  end
  else
  begin
    if (fAlpha<>255) and (fStatus<>1) then
    begin
      Container.DeleteFrameFunction(DeleteAlpha,Self);
      Container.AddFrameFunction(AddAlpha,Self,25);
    end;
  end;
end;

procedure TDXPopupItemInfo.UnVisible(Reset: boolean);
begin
  fStatus:=3;
  Visible:=false;
  fStatus:=0;
  Container.DeleteFrameFunction(AddAlpha,Self);
  Container.DeleteFrameFunction(DeleteAlpha,Self);
  if Reset then fAlpha:=0;
end;

procedure TDXPopupItemInfo.Show(Color: TColorSheme;Rect: TRect; Soldat: TSoldatInfo);
begin
  Container.IncLock;
  UnVisible;
  Container.LoadGame(true);
  SetColor(Color);
  Self.Soldat:=Soldat;
  SetAlignRect(Rect);
  Container.LoadGame(false);
  Container.DecLock;
  Visible:=true;
end;

procedure TDXPopupItemInfo.Show(Color: TColorSheme;Rect: TRect; Item: TLagerItem);
begin
  Container.IncLock;
  UnVisible;
  Container.LoadGame(true);
  SetColor(Color);
  Self.Item:=Item;
  SetAlignRect(Rect);
  Container.LoadGame(false);
  Container.DecLock;
  Visible:=true;
end;

procedure TDXPopupItemInfo.SetColor(Color: TColorSheme);
begin
  case Color of
    csRed   : begin BorderColor:=clRed  ;BlendColor:=bcMaroon; end;
    csGreen : begin BorderColor:=clLime ;BlendColor:=bcGreen;  end;
    csBlue  : begin BorderColor:=clBlue ;BlendColor:=bcNavy;   end;
  end;
end;

procedure TDXPopupItemInfo.SetNameOnly(const Value: Boolean);
begin
  fShowNameOnly := Value;
end;

end.
