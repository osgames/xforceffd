{********************************************************************************
*										*
* Copyright (C) 2004  X-Force Team - jim_raynor@web.de				*
*										*
* This file is part of X-Force: Fight For Destiny (X-Force).			*
*										*
* X-Force is free software; you can redistribute it and/or modify		*
* it under the terms of the GNU General Public License as published by		*
* the Free Software Foundation; either version 2 of the License, or		*
* (at your option) any later version.						*
*										*
* X-Force is distributed in the hope that it will be useful,			*
* but WITHOUT ANY WARRANTY; without even the implied warranty of		*
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the			*
* GNU General Public License for more details.					*
*										*
* You should have received a copy of the GNU General Public License		*
* along with X-Force; if not, write to the Free Software			*
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA	*
*										*
*********************************************************************************
*										*
* Verwaltet die KI im Bodeneinsatz						*
*										*
********************************************************************************}

{$I ../Settings.inc}

unit EinsatzKI;

interface

uses
  GameFigure, Classes, TraceFile, XForce_types, Windows, PathFinder, Forms,
  ISOMessages, KD4Utils, Defines, NotifyList;

type
  TUnitKI       = class;

  TUnitSettingMode = (usmRandomGroups,usmCloseQuartersGroups);

  TGroupCommand = (lcNone,lcSelbstMord,lcGruppieren,lcGroupAttack);
  TGroup        = record
    Command     : TGroupCommand;
    UnitsKI     : Array[0..GroupSize-1] of TUnitKI;
  end;

  TSpawnPoint = record
    Point     : TPoint;
    RoomNr    : Integer;
  end;

  TSpawnPoints = Array of TSpawnPoint;

  TGroupKI = class
  private
    fKIList           : TList;
    fTarget           : TGameFigure;
    fEnemyListe       : TList;
    fFormations       : TList;
    fMapSize          : TSize;
    fEchtzeit         : Boolean;
    fGroups           : Array of TGroup;

    fGroup            : Integer;
    fGroupIndex       : Integer;
    fUnitList         : TList;
    fSettingMode      : TUnitSettingMode;
    fStartingPoint    : TPoint;

    fLastUnit         : TGameFigure;

    fForbiddenZone    : TPoint;
    fForbiddenRange   : Integer;
    fReserveTU        : Integer;

    fSpawnPoints      : TSpawnPoints;
    fNotUsedSpawns    : TSpawnPoints;
    fLastSpawnRoom    : Integer;

    procedure SetMapSize(const Value: TSize);
  protected
    procedure FindVisibleEnemys;
    procedure SelbstMordKommando(Group: Integer);
    procedure Gruppieren(Group: Integer);

    procedure GroupAttack(Group: Integer;Figure: TGameFigure);

    function GroupCommandFinished(Group: Integer): Boolean;
  public
    constructor Create;
    destructor Destroy;override;
    procedure Clear(Echtzeit: boolean);
    procedure AddUnit(Figure: TGameFigure);

    procedure BeginNextRound;
    procedure DoUnitRound;
    procedure ZugSideChange;

    procedure SetFormationList(List: TList);
    procedure AddSpawnPoint(Point: TPoint);

    procedure SetForbiddenZone(Point: TPoint; Range: Integer);

    property MapSize         : TSize read fMapSize write SetMapSize;

    property UnitSettingMode : TUnitSettingMode read fSettingMode write fSettingMode;
    property StartingPoint   : TPoint read fStartingPoint;

    property UnitList        : TList read fUnitList write fUnitList;

    property ReserveTUWhenNoSeeing : Integer read fReserveTU write fReserveTU;
  end;

  TUnitKI = class
  private
    fFigure           : TGameFigure;
    fUnitPos          : TPoint;
    fTarget           : TGameFigure;
    fTime             : Integer;
    fGroupKI          : TGroupKI;
    fGroup            : Integer;

    fOnSee            : TEventHandle;
    fOnDestroy        : TEventHandle;
    fOnWayBlocked     : TEventHandle;
    fOnUnderFire      : TEventHandle;

    fCommand          : (kicNone,kicAttack,kicMove);

    fFigureAI       : TFigureAI;

    procedure SetFigure(const Value: TGameFigure);

    procedure OnSeeUnit(SeeUnit: TObject);
    procedure OnDestroyUnit(SeeUnit: TObject);
    procedure OnWayToDestBlocked(Figure: TObject);
    procedure OnUnderFire(Obj: TObject);

    procedure ClearEvents;
  protected
    procedure WaitAndCenterWhileMove;
  public
    constructor Create(GroupKI: TGroupKI);
    destructor destroy;override;
    procedure SetKillTarget(KillUnit: TGameFigure);
    procedure SetSammelPoint(Point: TPoint);
    function HasCommand: Boolean;
    procedure DoAction;
    function HasTarget: Boolean;
    function IsEinsatzFaehig: boolean;

    procedure DoTreffer(Figure: TGameFigure);

    property Figure: TGameFigure read fFigure write SetFigure;
    property Group : Integer read fGroup write fGroup;

    property FigureAI            : TFigureAI read fFigureAI write fFigureAI;
  end;


implementation

uses
  array_utils, DXIsoEngine;

{ TGroupKI }

procedure TGroupKI.AddUnit(Figure: TGameFigure);
var
  KI          : TUnitKI;
  Pos         : TPoint;
  Dummy       : Integer;

  procedure GotoNextSpawnRoom;
  var
    Dummy: Integer;
  begin
    for Dummy:=high(fSpawnPoints) downto 0 do
    begin
      if (fSpawnPoints[Dummy].RoomNr=fLastSpawnRoom) then
      begin
        SetLength(fNotUsedSpawns,length(fNotUsedSpawns)+1);
        fNotUsedSpawns[high(fNotUsedSpawns)]:=fSpawnPoints[Dummy];

        DeleteArray(Addr(fSpawnPoints),TypeInfo(TSpawnPoints),Dummy);
      end;
    end;

    fLastSpawnRoom:=-1;
  end;

  function GetSpawnPointInRoom(Room: Integer; var Point: TPoint): Boolean;
  var
    Dummy: Integer;
    Points : Array of Integer;
    Index  : Integer;
  begin
    SetLength(Points,length(fSpawnPoints));
    Index:=0;
    for Dummy:=0 to high(fSpawnPoints) do
    begin
      if fSpawnPoints[Dummy].RoomNr=Room then
      begin
        Points[Index]:=Dummy;
        inc(Index);
      end;
    end;

    if Index=0 then
      result:=false
    else
    begin
      result:=true;
      Index:=random(Index);
      Point:=fSpawnPoints[Points[Index]].Point;
      DeleteArray(Addr(fSpawnPoints),TypeInfo(TSpawnPoints),Points[Index]);
    end;
  end;

  procedure NewGroup;
  begin
    if fGroupIndex=0 then
      exit;
      
    inc(fGroup);
    SetLength(fGroups,fGroup+1);
    fGroupIndex:=0;
    GotoNextSpawnRoom
  end;

begin
  KI:=TUnitKI.Create(Self);
  KI.Figure:=Figure;
  Figure.EinsatzKI:=KI;
  
  fKIList.Add(KI);

  inc(fGroupIndex);

  if fGroupIndex=GroupSize then
    NewGroup;

  if not GetSpawnPointInRoom(fLastSpawnRoom,Pos) then
  begin
    NewGroup;
    // Keine Spawnpunkte mehr vorhanden
    if length(fSpawnPoints)=0 then
    begin
      if length(fNotUsedSpawns)=0 then
      begin
        Dummy:=5000;
        repeat
          Pos:=Point(random(MapSize.cx),random(MapSize.cy));
          dec(Dummy);
          if Dummy=0 then
            break;
        until CalculateEntfern(Pos,fForbiddenZone)>fForbiddenRange;
      end
      else
      begin
        Dummy:=random(high(fNotUsedSpawns));
        Assert((Dummy>=0) and (Dummy<=high(fNotUsedSpawns)));
        Pos:=fNotUsedSpawns[Dummy].Point;
        DeleteArray(Addr(fNotUsedSpawns),TypeInfo(TSpawnPoints),Dummy);
      end;
    end
    else
    begin
      Dummy:=random(high(fSpawnPoints));
      Assert((Dummy>=0) and (Dummy<=high(fSpawnPoints)));
      fLastSpawnRoom:=fSpawnPoints[Dummy].RoomNr;
      Assert(GetSpawnPointInRoom(fLastSpawnRoom,Pos));
    end;
  end;

  repeat
    if not PtInRect(Rect(0,0,MapSize.cx,MapSize.cy),Pos) then
    begin
      Pos.X:=random(MapSize.cx);
      Pos.Y:=random(MapSize.cy);
    end;

    Figure.XPos:=Pos.X;
    Figure.YPos:=Pos.Y;

    // Beim nächsten durchlauf auf jeden Fall eine neue Position ermitteln
    Pos.X:=-1;
  until Figure.SetToGameField;

  fGroups[fGroup].UnitsKI[fGroupIndex]:=KI;
  KI.Group:=fGroup;
end;

procedure TGroupKI.Clear(Echtzeit: boolean);
begin
  while fKIList.Count>0 do
  begin
    TObject(fKIList[0]).Free;
    fKiList.Delete(0);
  end;
  fEchtzeit:=Echtzeit;

  SetLength(fGroups,0);

  fGroup:=-1;
  fGroupIndex:=GroupSize-1;

  SetLength(fSpawnPoints,0);
  SetLength(fNotUsedSpawns,0);
  fLastSpawnRoom:=-1;
end;

constructor TGroupKI.Create;
begin
  fKIList:=TList.Create;
  fEnemyListe:=TList.Create;
  fEchtzeit:=true;
  fSettingMode:=usmRandomGroups;
end;

destructor TGroupKI.Destroy;
begin
  Clear(fEchtzeit);
  fKIList.Free;
  fEnemyListe.Free;
  inherited;
end;

procedure TGroupKI.DoUnitRound;
var
  Figure     : TGameFigure;
begin
  if (fUnitList.Count>0) then
  begin
    Figure:=TGameFigure(fUnitList[0]);

    if Figure.FigureStatus<>fsEnemy then
      exit;

    if Figure.IsDead then
    begin
      fKIList.Remove(Figure);
      fUnitList.Remove(Figure);
      SendVMessage(vmEngineRedraw);
      exit;
    end;

    Assert(Figure.EinsatzKI<>nil);
    TUnitKI(Figure.EinsatzKI).DoAction;

    if fLastUnit<>Figure then
    begin
      fLastUnit:=Figure;

      rec.Figure:=Figure;
      SendVMessage(vmKameraVerfolgung);
    end;

    if Figure.RoundFinished then
      fUnitList.Delete(0);

  end;

end;

procedure TGroupKI.FindVisibleEnemys;
var
  Dummy   : Integer;
  Units   : Integer;
  Figure  : TGameFigure;
begin
  for Dummy:=0 to fKIList.Count-1 do
  begin
    with TUnitKI(fKIList[Dummy]).Figure do
    begin
      for Units:=0 to SeeCount-1 do
      begin
        Figure:=SeeUnit[Units];
        if (Figure<>nil) and (fEnemyListe.IndexOf(Figure)=-1) then
        begin
          fEnemyListe.Add(Figure);
        end;
      end;
    end;
  end;
end;

procedure TGroupKI.GroupAttack(Group: Integer; Figure: TGameFigure);
var
  Dummy: Integer;
begin
  with fGroups[Group] do
  begin
    if Command=lcGroupAttack then
      exit;
    Command:=lcGroupAttack;

    for Dummy:=0 to high(UnitsKI) do
    begin
      if (UnitsKI[Dummy]<>nil) and (UnitsKI[Dummy].IsEinsatzFaehig) then
        UnitsKI[Dummy].SetKillTarget(Figure);
    end;
  end;
end;

function TGroupKI.GroupCommandFinished(Group: Integer): Boolean;
var
  Dummy: Integer;
begin
  result:=true;
  with fGroups[Group] do
  begin
    for Dummy:=0 to high(UnitsKI) do
    begin
      if UnitsKI[Dummy]=nil then
        exit;

      // Sobald eine Einheit der Gruppe ein Kommando hat, ist die Gruppe noch
      // beschäftigt
      if UnitsKI[Dummy].HasCommand then
      begin
        result:=false;
        exit;
      end;
    end;
  end;
end;

procedure TGroupKI.Gruppieren(Group: Integer);
var
  Formation: TGroupFormation;
  Dummy    : Integer;
begin
  with fGroups[Group] do
  begin
    if Command=lcGruppieren then
      exit;
    Command:=lcGruppieren;

    // Sammelpunkt und Formation ermitteln
    Formation:=TGroupFormation(fFormations[random(fFormations.Count)]);
    Formation.SetzePosition(Point(random(fMapSize.cx),random(fMapSize.cy)),Point(random(fMapSize.cx),random(fMapSize.cy)),fMapSize);

    for Dummy:=0 to high(UnitsKI) do
    begin
      if (UnitsKI[Dummy]<>nil) and (UnitsKI[Dummy].IsEinsatzFaehig) then
        UnitsKI[Dummy].SetSammelPoint(Formation.GetNextZiel);
    end;
  end;
end;

procedure TGroupKI.BeginNextRound;
var
  Dummy: Integer;
begin
  Assert(fUnitList<>nil);

  fEnemyListe.Clear;
  FindVisibleEnemys;

  if fEnemyListe.Count>0 then
  begin
    for Dummy:=0 to high(fGroups) do
    begin
      if RandomChance(100 div length(fGroups)) then
        SelbstMordKommando(Dummy);
    end;
  end
  else
  begin
    for Dummy:=0 to high(fGroups) do
    begin
      if random(200)=1 then
        fGroups[Dummy].Command:=lcNone;  // Neue Ziele Suchen

      // Weist der Gruppen ein neues Ziel zu, wenn sie sich formiert haben
      if GroupCommandFinished(Dummy) then
        fGroups[Dummy].Command:=lcNone;  // Neue Ziele Suchen

      Gruppieren(Dummy);
    end;
  end;

  fLastUnit:=nil;
end;

procedure TGroupKI.SelbstMordKommando(Group: Integer);
var
  Dummy: Integer;
begin
  with fGroups[Group] do
  begin
    if Command=lcSelbstMord then
      exit;
    Command:=lcSelbstMord;

    if fEnemyListe.Count>0 then
    begin
      fTarget:=fEnemyListe[0];
      for Dummy:=0 to high(UnitsKI) do
      begin
        if UnitsKI[Dummy]<>nil then
        begin
          UnitsKI[Dummy].SetKillTarget(fTarget);
        end;
      end;
    end;
  end;
end;

procedure TGroupKI.SetForbiddenZone(Point: TPoint; Range: Integer);
begin
  fForbiddenZone:=Point;
  fForbiddenRange:=Range;
end;

procedure TGroupKI.SetFormationList(List: TList);
begin
  fFormations:=List;
end;

procedure TGroupKI.SetMapSize(const Value: TSize);
begin
  fMapSize := Value;

  if fSettingMode=usmCloseQuartersGroups then
  begin
    fStartingPoint.X:=random(Value.cx);
    fStartingPoint.Y:=random(Value.cy);
  end;
end;

procedure TGroupKI.ZugSideChange;
begin
  fLastUnit:=nil;
end;

procedure TGroupKI.AddSpawnPoint(Point: TPoint);
begin
  SetLength(fSpawnPoints,length(fSpawnPoints)+1);
  fSpawnPoints[high(fSpawnPoints)].Point:=Point;
  fSpawnPoints[high(fSpawnPoints)].RoomNr:=(Point.X div 15)+((Point.Y div 15)*(fMapSize.cx div 15));
end;

{ TUnitKI }

procedure TUnitKI.ClearEvents;
begin
  if fFigure<>nil then
  begin
    fFigure.NotifyList.RemoveEvent(fOnSee);
    fFigure.NotifyList.RemoveEvent(fOnDestroy);
    fFigure.NotifyList.RemoveEvent(fOnWayBlocked);
    fFigure.NotifyList.RemoveEvent(fOnUnderFire);
  end;
end;

constructor TUnitKI.Create(GroupKI: TGroupKI);
begin
  fCommand:=kicNone;
  fGroupKI:=GroupKI;
end;

destructor TUnitKI.destroy;
begin
  ClearEvents;
  inherited;
end;

procedure TUnitKI.DoAction;
begin
  Assert(Figure<>nil);
  if HasTarget then
  begin
    if Figure.CanSeeUnit(fTarget) then
    begin
      Figure.ShootToUnit(fTarget);
    end
    else
    begin
      if (Figure.Ziel.X<>fUnitPos.X) or (Figure.Ziel.Y<>fUnitPos.Y) then
      begin
        if not Figure.MoveTo(Point(fUnitPos.X,fUnitPos.Y)) then
          fTarget:=nil;
      end;
    end;
  end
  else if fCommand=kicMove then
  begin
    if (Figure.Ziel.X<>fUnitPos.X) or (Figure.Ziel.Y<>fUnitPos.Y) then
    begin
      if not Figure.MoveTo(Point(fUnitPos.X,fUnitPos.Y)) then
      begin
        fCommand:=kicNone;
      end;
    end;
    if (Figure.XPos=fUnitPos.X) and (Figure.YPos=fUnitPos.Y) then
      fCommand:=kicNone
    else
      Figure.GiveOK;
  end
  else
  begin
    fTarget:=nil;
    dec(fTime);
    if fTime<0 then
    begin
      Figure.ViewDirection:=TViewDirection(random(Ord(high(TViewDirection))));
      inc(fTime,random(5)+5);
    end;
  end;

  if fFigure.SeeCount=0 then
    fFigure.ReservedTimeUnits:=fGroupKI.ReserveTUWhenNoSeeing;

  fFigure.GiveOK;
end;

procedure TUnitKI.DoTreffer(Figure: TGameFigure);
begin
  SetKillTarget(Figure);
end;

function TUnitKI.HasCommand: Boolean;
begin
  result:=(fCommand<>kicNone);
end;

function TUnitKI.HasTarget: Boolean;
begin
  result:=(fCommand=kicattack) and (fTarget<>nil) and (fTarget.IsOnField) and (not fTarget.IsDead);
end;

function TUnitKI.IsEinsatzFaehig: boolean;
begin
  result:=not (Figure.IsDead);
end;

procedure TUnitKI.OnDestroyUnit(SeeUnit: TObject);
begin
  fFigure:=nil;
end;

procedure TUnitKI.OnSeeUnit(SeeUnit: TObject);
begin
  Assert(fFigure<>nil);
  fFigure.StopMove;
  if (fFigureAI<>faiAgressiv) then
    exit;

  SetKillTarget(TGameFigure(SeeUnit));
  fFigure.ReservedTimeUnits:=0;
end;

procedure TUnitKI.OnUnderFire(Obj: TObject);
begin
  Assert(Obj is TISOObject);

  if not HasTarget then
    fFigure.ViewTo(TISOObject(Obj).GetStartPoint); 
end;

procedure TUnitKI.OnWayToDestBlocked(Figure: TObject);
begin
  fCommand:=kicNone;
end;

procedure TUnitKI.SetFigure(const Value: TGameFigure);
begin
  ClearEvents;

  Assert(Value<>nil);

  fFigure := Value;

  fOnSee:=fFigure.NotifyList.RegisterEvent(EVENT_FIGURE_SEEUNIT,OnSeeUnit);
  fOnDestroy:=fFigure.NotifyList.RegisterEvent(EVENT_FIGURE_ONDESTROY,OnDestroyUnit);
  fOnWayBlocked:=fFigure.NotifyList.RegisterEvent(EVENT_FIGURE_WAYTODESTBLOCKED,OnWayToDestBlocked);
  fOnUnderFire:=fFigure.NotifyList.RegisterEvent(EVENT_FIGURE_UNDERFIRE,OnUnderFire);

  if fFigure.FigureStatus=fsEnemy then
    fFigureAI:=faiAgressiv
  else
    fFigureAI:=faiNormal;
end;

procedure TUnitKI.SetKillTarget(KillUnit: TGameFigure);
begin
  if HasTarget then
  begin
    if fFigure.CanSeeUnit(fTarget) and (CalculateEntfern(fTarget.Position,fFigure.Position)<CalculateEntfern(KillUnit.Position,fFigure.Position)) then
      KillUnit:=nil;
  end;

  if KillUnit<>nil then
  begin
    fCommand:=kicAttack;
    fTarget:=KillUnit;
    fUnitPos:=Point(KillUnit.XPos,KillUnit.YPos);
    fGroupKI.GroupAttack(fGroup,KillUnit);
  end;
end;

procedure TUnitKI.SetSammelPoint(Point: TPoint);
begin
  fCommand:=kicMove;
  fUnitPos:=Point;
end;

procedure TUnitKI.WaitAndCenterWhileMove;
begin
end;

end.
