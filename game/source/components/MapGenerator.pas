{********************************************************************************
*										*
* Copyright (C) 2004  X-Force Team - jim_raynor@web.de				*
*										*
* This file is part of X-Force: Fight For Destiny (X-Force).			*
*										*
* X-Force is free software; you can redistribute it and/or modify		*
* it under the terms of the GNU General Public License as published by		*
* the Free Software Foundation; either version 2 of the License, or		*
* (at your option) any later version.						*
*										*
* X-Force is distributed in the hope that it will be useful,			*
* but WITHOUT ANY WARRANTY; without even the implied warranty of		*
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the			*
* GNU General Public License for more details.					*
*										*
* You should have received a copy of the GNU General Public License		*
* along with X-Force; if not, write to the Free Software			*
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA	*
*										*
*********************************************************************************
*										*
* MapGenerator stellt die Funktionalit�t zur Verf�gung, um Karten zum 		*
* Bodeneinsatz per Skript erstellen zu lassen					*
*										*
********************************************************************************}

{$I ../Settings.inc}

unit MapGenerator;

interface

uses
  Windows, Classes, Sysutils, NGtypes, XForce_Types, KD4Utils, upsCompiler,
  upsRunTime,  uPSDebugger, uPSUtils, IDHashMap;

type
  TMapGenerator = class(TObject)
  private
    fRoomList     : TList;
    fSetTileFunc  : TSetTileProcedure;
    fSetMapSize   : TSetMapSizeProcedure;

    fMapHeight    : Integer;
    fMapWidth     : Integer;

    fCompiler     : TPSPascalCompiler;
    fScriptRunTime: TPSDebugExec;

    fRooms        : Array of Array of String;

    fValid        : Boolean;
    fGenerated    : Boolean;

    fUsedTiles    : Array of TUsedTileInfo;

    fSkripts      : Array of String;

    fHashMap      : TIDHashMap;

    function FindRoom(Name: String): TObject;
    function GetMapHeight: Integer;
    function GetMapWidth: Integer;

    procedure CopyRoomToPos(X,Y: Integer; Room: String);
    procedure FillFreeRooms(Room: String);
    function CheckMapPos(X, Y: Integer): boolean;

    procedure ReadUsedTiles(Stream: TStream);

    procedure SetMapSize(Width: Integer; Height: Integer);

    procedure Clear;
  public
    constructor Create;
    destructor Destroy;override;

    function Generate(ForUnits: Integer): Boolean;

    // Generiere Karte, aus dem geladenen Skript
    function GenerateFromSkript(Width,Height: Integer): Boolean;

    // Erstelle eine Karte und nutze ein Skript aus fSkripts
    function GenerateRandom(Width, Height: Integer): Boolean;

    function Compile(Skript: String): Boolean;

    function GetRoomAtPos(X,Y: Integer): String;

    function GetTileIndex(ID: Cardinal; out Index: Integer): Boolean;

    procedure LoadMapData(Stream: TStream);

    property SetTileFunc   : TSetTileProcedure read fSetTileFunc write fSetTileFunc;
    property OnSetMapSize  : TSetMapSizeProcedure read fSetMapSize write fSetMapSize;

    property Compiler    : TPSPascalCompiler read fCompiler;
    property SkriptEngine: TPSDebugExec read fScriptRunTime;

    property MapHeight   : Integer read GetMapHeight;
    property MapWidth    : Integer read GetMapWidth;
  end;

  TMapRoom = class(TObject)
  private
    fRoom      : TRoomInfo;
    fGenerator : TMapGenerator;
    fRoomName  : String;
    procedure SetTile(x, y: Integer; const Value: TMapTileEntry);
    function GetTile(x, y: Integer): TMapTileEntry;
    function GetHeight: Integer;
    function GetWidth: Integer;
  public
    constructor Create(Generator: TMapGenerator);
    procedure LoadFromStream(Stream: TStream);
    procedure LoadTiles(Stream: TStream; ID: Cardinal);

    { Kopieren in die Engine }
    procedure NormalCopy(X,Y: Integer; SetTileFunc: TSetTileProcedure);
    procedure SetRoomToPos(X,Y: Integer; SetTileFunc: TSetTileProcedure);

    property Tiles[x,y: Integer]: TMapTileEntry read GetTile write SetTile;
    property Width: Integer read GetWidth;
    property Height: Integer read GetHeight;
    property RoomName: String read fRoomName;
  end;

const
  RoomWidth  = 15;
  RoomHeight = 15;

implementation

uses
  script_utils, Map_Utils, EinsatzIntro;

{ TMapGenerator }

function CheckExports(Sender: TPSPascalCompiler; Proc: TPSInternalProcedure; const ProcDecl: string): Boolean;

  function CheckScriptFunktion(Params: Array of TPSBaseType; NeededDecl: String): Boolean;
  begin
    result:=true;
    if not ExportCheck(Sender, Proc, Params, []) then
    begin
      Sender.MakeError('', ecCustomError, 'Funktionskopf f�r '+Proc.Name+' muss  '+NeededDecl+' entsprechen.');
      result:=false;
    end
  end;

begin
  result:=true;
  if Proc.Name='GENERATEMAP' then
    result:=CheckScriptFunktion([0],'procedure GenerateMap;')
end;

var
  g_Compiler: TPSPascalCompiler;
  g_Exec    : TPSExec;
  Map     : TMapGenerator;

procedure RegisterExterns;

  procedure RegisterScriptFunction(Ptr: Pointer; const Name: String; const Decl: string);
  begin
    Assert((g_Compiler<>nil) or (g_Exec<>nil));
    if g_Compiler <> nil then
      g_Compiler.AddDelphiFunction(Decl);

    if g_Exec <> nil then
      g_Exec.RegisterDelphiFunction(Ptr,Name,cdRegister);
  end;

  procedure RegisterScriptMethod(SlfPtr: Pointer; Ptr: Pointer; const Name: String; const Decl: string;Conv: TPSCallingConvention = cdRegister);
  begin
    Assert((g_Compiler<>nil) or (g_Exec<>nil));
    if g_Compiler <> nil then
      g_Compiler.AddDelphiFunction(Decl);

    if g_Exec <> nil then
      g_Exec.RegisterDelphiMethod(SlfPtr,Ptr,Name,Conv);
  end;

begin
//  RegisterScriptMethod(MissionObj,@TMission.RegisterUFOShootDown,'register_ufo_shootdown','procedure register_ufo_shootdown(Event: TUFOProcedure;UFO: TUFO);',cdStdCall);
  RegisterScriptMethod(Map,@TMapGenerator.GetMapWidth,'getmapWidth','function GetMapWidth: Integer;');
  RegisterScriptMethod(Map,@TMapGenerator.GetMapHeight,'getmapheight','function GetMapHeight: Integer;');
  RegisterScriptMethod(Map,@TMapGenerator.GetRoomAtPos,'getroomatpos','function GetRoomAtPos(X,Y: Integer): String;');
  RegisterScriptMethod(Map,@TMapGenerator.CopyRoomToPos,'copyroomtopos','procedure CopyRoomToPos(X,Y: Integer; Room: String);');
  RegisterScriptMethod(Map,@TMapGenerator.FillFreeRooms,'fillfreerooms','procedure fillfreerooms(Room: String);');

  // random kann nicht direkt importiert werden, deshalb umweg �ber script_utils
  RegisterScriptFunction(@script_utils_random,'random','function random(Range: Integer): Integer');
  RegisterScriptFunction(@script_utils_inc,'inc','procedure inc(var int: Integer)');
  RegisterScriptFunction(@script_utils_dec,'dec','procedure dec(var int: Integer)');

  if g_Compiler <> nil then
  begin
    g_Compiler.AddUsedVariableN('MapWidth','Integer');
    g_Compiler.AddUsedVariableN('MapHeight','Integer');
  end;
end;

function RegisterCompiler(Sender: TPSPascalCompiler; const Name: string): Boolean;
begin
  result:=false;
  if Name='SYSTEM' then
  begin
    g_Exec:=nil;
    g_Compiler:=Sender;

    RegisterExterns;

    result:=true;
  end;
end;

function TMapGenerator.CheckMapPos(X,Y: Integer): boolean;
begin
  result:=(X>=0) and (Y>=0) and (X<fMapWidth) and (Y<fMapHeight);
end;

procedure TMapGenerator.Clear;
var
  Dummy: Integer;
begin
  for Dummy:=0 to fRoomList.Count-1 do
    TMapRoom(fRoomList[Dummy]).Free;

  fRoomList.Clear;

  fHashMap.ClearList;

  SetLength(fSkripts,0);
end;

function TMapGenerator.Compile(Skript: String): Boolean;
var
  compscript: String;
  compdebug : String;
begin
  fGenerated:=false;
  if not fCompiler.Compile(Skript) then
  begin
    fValid:=false;
    result:=false;

    exit;
  end;
  Assert(fCompiler.GetOutput(compscript));
  Assert(fCompiler.GetDebugOutput(compdebug));

  Assert(fScriptRunTime.LoadData(compscript));
  fScriptRunTime.LoadDebugData(compdebug);

  fValid:=true;
  result:=true;
end;

procedure TMapGenerator.CopyRoomToPos(X, Y: Integer; Room: String);
begin
  if CheckMapPos(X,Y) then
  begin
    fRooms[X,Y]:=Room;
    if fRoomList.Count>0 then
      TMapRoom(FindRoom(Room)).NormalCopy(X*RoomWidth,Y*RoomHeight,fSetTileFunc);
  end;
end;

constructor TMapGenerator.Create;
begin
  fCompiler:=TPSPascalCompiler.Create;
  fCompiler.AllowNoBegin:=true;
  fCompiler.BooleanShortCircuit:=true;
  fCompiler.OnUses:=RegisterCompiler;
  fCompiler.OnExportCheck:=CheckExports;

  fScriptRunTime:=TPSDebugExec.Create;
  fScriptRunTime.DebugEnabled:=false;

  g_Compiler:=nil;
  g_Exec:=fScriptRunTime;
  Map:=Self;
  RegisterExterns;

  fScriptRunTime.Id:=Self;

  fValid:=false;
  fGenerated:=false;

  fRoomList:=TList.Create;

  fHashMap:=TIDHashMap.Create;
end;

destructor TMapGenerator.Destroy;
begin
  Clear;
  fHashMap.Free;
  fRoomList.Free;
  fCompiler.Free;
  fScriptRunTime.Free;
  inherited;
end;

procedure TMapGenerator.FillFreeRooms(Room: String);
var
  X,Y: Integer;
begin
  for X:=0 to fMapWidth-1 do
  begin
    for Y:=0 to fMapHeight-1 do
    begin
      if fRooms[X,Y]='' then
      begin
        fRooms[X,Y]:=Room;
        if fRoomList.Count>0 then
          TMapRoom(FindRoom(Room)).NormalCopy(X*RoomWidth,Y*RoomHeight,fSetTileFunc);
      end;
    end;
  end;
end;

function TMapGenerator.FindRoom(Name: String): TObject;
var
  Dummy      : Integer;
  MatchRooms : Array of Integer;
  Index      : Integer;
begin
  SetLength(MatchRooms,fRoomList.Count);
  Index:=0;
  for Dummy:=0 to fRoomList.Count-1 do
  begin
    if Matchstrings(TMapRoom(fRoomList[Dummy]).RoomName,Name) then
    begin
      MatchRooms[Index]:=Dummy;
      inc(Index);
    end;
  end;

  if Index=0 then
    raise Exception.Create('Kein Raum entspricht den angegeben Suchstring '+Name);

  result:=TMapRoom(fRoomList[MatchRooms[random(Index)]]);
end;

function TMapGenerator.GenerateFromSkript(Width, Height: Integer): Boolean;
var
  Proc : Cardinal;
  List : TPSList;
begin
  SetMapSize(Width,Height);

  result:=false;

  SetLength(fRooms,0,0);
  SetLength(fRooms,Width,Height);

  List:=TPSList.Create;
  Proc:=fScriptRuntime.GetProc('GenerateMap');
  if Proc=InvalidVal then
    exit;

  fScriptRunTime.RunProc(List,Proc);

  List.Free;

  fGenerated:=true;

  result:=true;
end;

function TMapGenerator.Generate(ForUnits: Integer): Boolean;
var
  Size     : TSize;
  Proc     : Cardinal;
  ParaList : TPSList;

  function GetRandomSize: TSize;
  begin
    {$IFDEF SMALLMAP}
    result.cx:=2;
    result.cy:=2;
    {$ELSE}
    if (ForUnits<6) then
    begin
      result.cx:=random(2)+2;
      result.cy:=random(2)+2;
    end
    else if (ForUnits<12) then
    begin
      result.cx:=random(3)+2;
      result.cy:=random(3)+2;
    end
    else if (ForUnits<24) then
    begin
      result.cx:=random(4)+3;
      result.cy:=random(4)+3;
    end
    else if (ForUnits<40) then
    begin
      result.cx:=random(4)+3;
      result.cy:=random(4)+3;
    end
    else
    begin
      result.cx:=random(4)+3;
      result.cy:=random(4)+3;
    end
    {$ENDIF}
  end;

begin
  if (not fValid) and (length(fSkripts)=0) then
  begin
    Size:=GetRandomSize;

    result:=GenerateRandom(Size.cx,Size.cy);
  end
  else
  begin
    if not fValid then
      Assert(Compile(fSkripts[random(length(fSkripts))]));

    // Karten Gr��e ermitteln
    Size.cx:=-1;
    Size.cy:=-1;

    Proc:=fScriptRuntime.GetProc('CalculateMapSize');
    if Proc<>InvalidVal then
    begin
      VSetInt(fScriptRunTime.GetVar2('MapWidth'),Size.cx);
      VSetInt(fScriptRunTime.GetVar2('MapHeight'),Size.cy);

      ParaList:=script_utils_constArrayToPSList(fScriptRunTime,fScriptRuntime.GetProcNo(Proc) as TPSInternalProcRec,[ForUnits]);

      fScriptRunTime.RunProc(ParaList,Proc);

      script_utils_ClearPSList(ParaList);

      FreeAndNil(ParaList);

      if fScriptRunTime.Status=isNotLoaded then
      begin
        result:=false;
        exit;
      end;

      Size.cx:=VGetInt(fScriptRunTime.GetVar2('MapWidth'));
      Size.cy:=VGetInt(fScriptRunTime.GetVar2('MapHeight'));
    end;

    if (Size.cx<1) or (Size.cy<1) then
      Size:=GetRandomSize;

    result:=GenerateFromSkript(Size.cx,Size.cy);

    fValid:=false;
  end;
end;

function TMapGenerator.GenerateRandom(Width, Height: Integer): Boolean;
var
  TakeX : Integer;
  TakeY : Integer;
begin
  Assert(fRoomList.Count>0);
  Assert(Assigned(fSetTileFunc));

  SetMapSize(Width,Height);

  result:=false;

  for TakeX:=0 to Width-1 do
  begin
    for TakeY:=0 to Height-1 do
    begin
      TMapRoom(fRoomList[random(fRoomList.Count)]).SetRoomToPos((TakeX*15),(TakeY*15),fSetTileFunc);
    end;
  end;
end;

function TMapGenerator.GetMapHeight: Integer;
begin
  result:=fMapHeight;
end;

function TMapGenerator.GetMapWidth: Integer;
begin
  result:=fMapWidth;
end;

function TMapGenerator.GetRoomAtPos(X, Y: Integer): String;
begin
  if CheckMapPos(X,Y) then
    result:=fRooms[X,Y]
  else
    result:='';
end;

function TMapGenerator.GetTileIndex(ID: Cardinal;
  out Index: Integer): Boolean;
begin
  result:=fHashMap.FindKey(ID,Index);
end;

procedure TMapGenerator.LoadMapData(Stream: TStream);
var
  ID     : Cardinal;
  Count  : Integer;
  Dummy  : Integer;
  Room   : TMapRoom;
begin
  Clear;

  Stream.Read(ID,SizeOf(ID));
  if not ValidMapVersion(ID) then
    raise Exception.Create('Ung�ltige Karte');

  ReadUsedTiles(Stream);

  IncEinsatzLoadProgress;

  Stream.Read(Count,SizeOf(Count));
  for Dummy:=0 to Count-1 do
  begin
    Room:=TMapRoom.Create(Self);
    Room.LoadFromStream(Stream);
    fRoomList.Add(Room);

    Room.LoadTiles(Stream,ID);
  end;

  IncEinsatzLoadProgress;

  // Skripte lesen
  Stream.Read(ID,sizeOf(ID));
  if ID=MapScriptVersion then
  begin
    Stream.Read(Count,SizeOf(Count));
    SetLength(fSkripts,Count);
    for Dummy:=0 to Count-1 do
    begin
      ReadString(Stream);
      fSkripts[Dummy]:=ReadString(Stream);
    end;
  end;
end;

procedure TMapGenerator.ReadUsedTiles(Stream: TStream);
var
  Dummy: String;

  procedure ReadTileInfoArray(Typ: TTileType);
  var
    Count: Integer;
    Dummy: Integer;

    Start   : Integer;
  begin
    Stream.Read(Count,SizeOf(Count));
    Start:=length(fUsedTiles);
    SetLength(fUsedTiles,length(fUsedTiles)+Count);
    for Dummy:=0 to Count-1 do
    begin
      Stream.Read(fUsedTiles[Start].ID,sizeOf(Cardinal));
      Stream.Read(fUsedTiles[Start].TileSetIndex,SizeOf(Cardinal));
      Stream.Read(fUsedTiles[Start].Index,SizeOf(Integer));

      fUsedTiles[Start].OwnIndex:=Dummy;
      fUsedTiles[Start].Typ:=Typ;

      fHashMap.InsertID(fUsedTiles[Start].ID,Dummy);

      inc(Start);

    end;
  end;

begin
  SetLength(fUsedTiles,0);
  Dummy:=ReadString(Stream);

  ReadTileInfoArray(ttFloor);
  ReadTileInfoArray(ttWall);
end;

procedure TMapGenerator.SetMapSize(Width, Height: Integer);
begin
  fMapWidth:=Width;
  fMapHeight:=Height;

  if Assigned(fSetMapSize) then
    fSetMapSize(Width*RoomWidth,Height*RoomHeight);
end;

{ TMapRoom }

constructor TMapRoom.Create(Generator: TMapGenerator);
begin
  fGenerator:=Generator;
end;

function TMapRoom.GetHeight: Integer;
begin
  result:=fRoom.Height;
end;

function TMapRoom.GetTile(x, y: Integer): TMapTileEntry;
begin
  result:=fRoom.Tiles[x,y];
end;

function TMapRoom.GetWidth: Integer;
begin
  result:=fRoom.Width;
end;

procedure TMapRoom.LoadFromStream(Stream: TStream);
var
  Name  : String;
  Wi,Hi : Integer;
begin
  Name:=ReadString(Stream);
  Stream.Read(Wi,SizeOf(Integer));
  Stream.Read(Hi,SizeOf(Integer));
  fRoomName:=Name;
  fRoom.Name:=Name;
  fRoom.Width:=Wi;
  fRoom.Height:=Hi;
//  Stream.Read(fRoom.CanRotate,SizeOf(boolean));
  fRoom.CanRotate:=true;
  SetLength(fRoom.Tiles,Wi,Hi);
end;

procedure TMapRoom.LoadTiles(Stream: TStream; ID: Cardinal);
var
  X,Y         : Integer;
  HasHotSpots : Boolean;

  function ReadFloor: Integer;
  var
    ID    : Cardinal;
  begin
    Stream.Read(ID,SizeOf(Cardinal));
    result:=-1;
    Assert(fGenerator.GetTileIndex(ID,result));
  end;

  function ReadWall: Integer;
  var
    ID    : Cardinal;
  begin
    Stream.Read(ID,SizeOf(Cardinal));
    result:=-1;
    if ID=0 then
      exit;
    Assert(fGenerator.GetTileIndex(ID,result));
  end;

begin
  HasHotSpots:=MapVersionHasInfo(ID,miHotSpots);
  for X:=0 to fRoom.Width-1 do
    for Y:=0 to fRoom.Height-1 do
    begin
      with fRoom.Tiles[X,Y] do
      begin
        Floor:=ReadFloor;
        BLeft:=ReadWall;
        Bright:=ReadWall;
        TLeft:=ReadWall;
        TRight:=ReadWall;

        if HasHotSpots then
          Stream.Read(HotSpot,sizeOf(HotSpot))
        else
          HotSpot:=0;

        Init:=false;
        AlphaV.All:=0;
        AlphaV.Objekt:=0;
        AlphaV.ATLeft:=-1;
        AlphaV.ATRight:=-1;
        AlphaV.ABLeft:=-1;
        AlphaV.ABRight:=-1;
        Obj:=-1;
      end;
    end;
end;

procedure TMapRoom.NormalCopy(X, Y: Integer;
  SetTileFunc: TSetTileProcedure);
var
  X1,Y1      : Integer;
  X2,Y2      : Integer;
begin
  X2:=X;
  for X1:=0 to fRoom.Width-1 do
  begin
    Y2:=Y;
    for Y1:=0 to fRoom.Height-1 do
    begin
      SetTileFunc(X2,Y2,fRoom.Tiles[X1,Y1]);
      inc(Y2);
    end;
    inc(X2);
  end;
end;

procedure TMapRoom.SetRoomToPos(X, Y: Integer; SetTileFunc: TSetTileProcedure);
begin
{  case random(125) of
      0.. 24  : MirrorHCopy(X,Y,SetTileFunc);
     25.. 49  : RotateLeftCopy(X,Y,SetTileFunc);
     50.. 74  : MirrorVCopy(X,Y,SetTileFunc);
     75.. 99  : RotateRightCopy(X,Y,SetTileFunc);
    100..124  : NormalCopy(X,Y,SetTileFunc);
  end;}

  NormalCopy(X,Y,SetTileFunc);
end;

procedure TMapRoom.SetTile(x, y: Integer; const Value: TMapTileEntry);
begin
  fRoom.Tiles[x,y]:=Value;
end;

end.
