{********************************************************************************
*										*
* Copyright (C) 2004  X-Force Team - jim_raynor@web.de				*
*										*
* This file is part of X-Force: Fight For Destiny (X-Force).			*
*										*
* X-Force is free software; you can redistribute it and/or modify		*
* it under the terms of the GNU General Public License as published by		*
* the Free Software Foundation; either version 2 of the License, or		*
* (at your option) any later version.						*
*										*
* X-Force is distributed in the hope that it will be useful,			*
* but WITHOUT ANY WARRANTY; without even the implied warranty of		*
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the			*
* GNU General Public License for more details.					*
*										*
* You should have received a copy of the GNU General Public License		*
* along with X-Force; if not, write to the Free Software			*
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA	*
*										*
*********************************************************************************
*										*
* basis_api bildet die Schnittstelle zu einer Basisliste, die bis zu 8 Basen	*
* verwaltet									*
*										*
* WICHTIG!! Bevor die Funktionen genutzt werden k�nnen muss �ber 		*
*           basis_api_init die Basisliste gesetzt werden. Sollte durch 		*
*           savegame_api_init geschehen sein.					*
*										*
********************************************************************************}

{$I ../Settings.inc}

unit DXMessage;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  DXContainer, DXDraws, DXBitmapButton, math, Blending, TraceFile, XForce_types,
  DirectDraw, DirectFont,Defines, KD4Utils, ModalDialog, DXGroupCaption,
  DXTextViewer, StringConst;

const
  MinHeight : Integer = 73;
  MaxHeight : Integer = 275;
  MinWidthQ : Integer = 220;
  MinWidthM : Integer = 150;
  MaxWidth  : Integer = 350;

type
  TDXMessage = class(TModalDialog)
  private
    GroupHeader   : TDXGroupCaption;
    TextViewer    : TDXTextViewer;
    YesButton     : TDXBitmapButton;
    NoButton      : TDXBitmapButton;
    OKButton      : TDXBitmapButton;

    fMinWidth     : Integer;
    procedure SetAccColor(const Value: TColor);
    function GetAccColor: TColor;
    { Private-Deklarationen }
  protected
    { Protected-Deklarationen }
    procedure ButtonClick(Sender: TObject);

    procedure CalculateSize;
    procedure BeforeShow;override;
  public
    constructor Create(Page: TDXPage);override;
    destructor destroy;override;

    function Show(Text: String;Caption: String;Question: boolean): TModalResult;overload;
    function Show(Text: String;Caption,FirstButton,SecondButton: String): TModalResult;overload;

    property AccerlateColor    : TColor read GetAccColor write SetAccColor;
  end;


implementation




{ TDXMessage }

constructor TDXMessage.Create(Page: TDXPage);
var
  Font: TFont;
begin
  inherited;

  { Einstellungen f�r Eingabefeld }
  GroupHeader:=TDXGroupCaption.Create(Page);
  with GroupHeader do
  begin
    Visible:=false;
    BorderColor:=$00400000;
    BlendColor:=bcDarkNavy;
    RoundCorners:=rcTop;
    Width:=233;
    Height:=25;
    Alignment:=taCenter;
  end;
  AddComponent(GroupHeader);

  { Einstellungen f�r den TextViewer }
  TextViewer:=TDXTextViewer.Create(Page);
  with TextViewer do
  begin
    Visible:=false;
    BlendColor:=bcMaroon;
    RoundCorners:=rcNone;
    LeftMargin:=15;
    Topmargin:=8;
    Top:=GroupHeader.Bottom+1;
    Blending:=true;
    BlendValue:=200;
    BorderColor:=$00400000;
    BlendColor:=bcDarkNavy;

    // Schriften Definieren
    RegisterFont('white',WhiteStdFont);
    RegisterFont('yellow',YellowStdFont);
    RegisterFont('green',GreenStdFont);
    RegisterFont('red',RedStdFont);
    RegisterFont('lime',LimeStdFont);
    RegisterFont('blue',BlueStdFont);
    RegisterFont('maroon',MaroonStdFont);
    RegisterFont('yellowbold',YellowBStdFont);
    RegisterFont('whitebold',WhiteBStdFont);
  end;
  AddComponent(TextViewer);
  AddComponent(TextViewer.ScrollBar);

  { Einstellungen f�r den Ja Button }
  YesButton:=TDXBitmapButton.Create(Page);
  with YesButton do
  begin
    Visible:=false;
    Text:=BYes;
    Height:=28;
    RoundCorners:=rcLeftBottom;
    OnClick:=ButtonClick;
    BlendAlpha:=150;
    FirstColor:=$00400000;
    SecondColor:=clBlue;
    BlendColor:=bcDarkNavy;
  end;

  { Einstellungen f�r den Nein Button }
  NoButton:=TDXBitmapButton.Create(Page);
  with NoButton do
  begin
    Visible:=false;
    Text:=BNo;
    Height:=28;
    RoundCorners:=rcRightBottom;
    OnClick:=ButtonClick;
    BlendAlpha:=150;
    FirstColor:=$00400000;
    SecondColor:=clBlue;
    BlendColor:=bcDarkNavy;
  end;

  { Einstellungen f�r den OK Button }
  OKButton:=TDXBitmapButton.Create(Page);
  with OKButton do
  begin
    Visible:=false;
    Text:=BOK;
    Height:=28;
    RoundCorners:=rcBottom;
    OnClick:=ButtonClick;
    BlendAlpha:=150;
    FirstColor:=$00400000;
    SecondColor:=clBlue;
    BlendColor:=bcDarkNavy;
  end;

end;

procedure TDXMessage.ButtonClick(Sender: TObject);
begin
  if Sender=OKButton then fResult:=1
  else if Sender=YesButton then fResult:=2
  else if Sender=NoButton then fResult:=3;

  ReadyShow;
end;

function TDXMessage.Show(Text: String;Caption: String;Question: boolean): TModalResult;
var
  FocusTemp : TDXComponent;
  maxW      : Integer;
  Dummy     : Integer;
  DFont     : TDirectFont;
  Page      : TDXPage;
begin
  if not fPage.Container.CanMessage then
    exit;

  TextViewer.Width:=MaxWidth;
  TextViewer.Text:=Text;
  GroupHeader.Caption:=Caption;

  if Question then
    fMinWidth:=minWidthQ
  else
    fMinWidth:=minWidthM;

  CalculateSize;

  Page:=fPage.Container.ActivePage;

  if Question then
  begin
    AddComponent(NoButton);
    AddComponent(YesButton);

    Page.RegisterHotKey(NoButton.HotKey,NoButton);
    Page.RegisterHotKey(YesButton.HotKey,YesButton);
    Page.RegisterHotKey(#13,YesButton);
    Page.RegisterHotKey(#27,NoButton);
  end
  else
  begin
    AddComponent(OKButton);
    Page.RegisterHotKey(OKButton.HotKey,YesButton);
    Page.RegisterHotKey(#13,OKButton);
    Page.RegisterHotKey(#27,OKButton);
  end;

  case ShowModal of
    1: result:=mrOk;
    2: result:=mrYes;
    3: result:=mrNo;
    else
      result:=mrCancel;
  end;

  if Question then
  begin
    Page.DeleteHotKey(NoButton.HotKey,NoButton);
    Page.DeleteHotKey(YesButton.HotKey,YesButton);
    Page.DeleteHotKey(#13,YesButton);
    Page.DeleteHotKey(#27,NoButton);
  end
  else
  begin
    Page.DeleteHotKey(OKButton.HotKey,YesButton);
    Page.DeleteHotKey(#13,OKButton);
    Page.DeleteHotKey(#27,OKButton);
  end;

  DeleteComponent(OKButton);
  DeleteComponent(NoButton);
  DeleteComponent(YesButton);
end;

function TDXMessage.GetAccColor: TColor;
begin
  Result:=OKButton.AccerlateColor;
end;

procedure TDXMessage.SetAccColor(const Value: TColor);
begin
  OKButton.AccerlateColor:=Value;
  NoButton.AccerlateColor:=Value;
  YesButton.AccerlateColor:=Value;
end;

function TDXMessage.Show(Text, Caption, FirstButton,
  SecondButton: String): TModalResult;
begin
  YesButton.Text:=FirstButton;
  NoButton.Text:=SecondButton;
  result:=Show(Text,Caption,True);
  YesButton.Text:=BYes;
  NoButton.Text:=BNo;
end;

destructor TDXMessage.destroy;
begin
  NoButton.Free;
  YesButton.Free;
  OKButton.Free;
  inherited;
end;

procedure TDXMessage.CalculateSize;
var
  maxW      : Integer;
  Dummy     : Integer;
  DFont     : TDirectFont;
begin
  // Gr��e des Dialoges ermitteln
  DFont:=FontEngine.FindDirectFont(GroupHeader.Font);
  maxW:=max(DFont.TextWidth(GroupHeader.Caption),fMinWidth);

  DFont:=WhiteStdFont;
  TextViewer.Height:=min(MaxHeight,(TextViewer.Lines.Count*(DFont.TextHeight('gf')+3))+(TextViewer.TopMargin*2)+(2-3));

  for Dummy:=0 to TextViewer.Lines.Count-1 do
    maxW:=max(DFont.TextWidth(TextViewer.Lines[dummy]),maxW);

  inc(maxW,(TextViewer.LeftMargin*2));

  if TextViewer.ScrollBar.Max>0 then
    inc(maxW,16);

  // Objekte correkt anordnen
  TextViewer.Width:=maxW;
  GroupHeader.Width:=maxW;

  YesButton.Top:=TextViewer.Bottom+1;
  NoButton.Top:=TextViewer.Bottom+1;
  OKButton.Top:=TextViewer.Bottom+1;

  YesButton.Width:=(maxW shr 1)-1;
  NoButton.Left:=YesButton.Right+1;

  if maxW mod 2=1 then
    NoButton.Width:=(maxW shr 1)+1
  else
    NoButton.Width:=(maxW shr 1);

  OKButton.Width:=maxW;
end;

procedure TDXMessage.BeforeShow;
begin
  inherited;

  GroupHeader.Font.Style:=[fsBold];
  GroupHeader.Font.Size:=Font.Size+4;
  GroupHeader.Font.Color:=clYellow;
  GroupHeader.FontColor:=clYellow;
end;

end.
