{********************************************************************************
*										*
* Copyright (C) 2004  X-Force Team - jim_raynor@web.de				*
*										*
* This file is part of X-Force: Fight For Destiny (X-Force).			*
*										*
* X-Force is free software; you can redistribute it and/or modify		*
* it under the terms of the GNU General Public License as published by		*
* the Free Software Foundation; either version 2 of the License, or		*
* (at your option) any later version.						*
*										*
* X-Force is distributed in the hope that it will be useful,			*
* but WITHOUT ANY WARRANTY; without even the implied warranty of		*
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the			*
* GNU General Public License for more details.					*
*										*
* You should have received a copy of the GNU General Public License		*
* along with X-Force; if not, write to the Free Software			*
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA	*
*										*
*********************************************************************************
*										*
* Diese Unit stellt eine Textfeld zur Verf�gung mit der man lange Texte		*
* anzeigen lassen kann. Die Vertikale Scrollbar wird angezeigt, sobald 		*
* sie ben�tigt wird. Eine Horizontale Scrollbar ist nicht vorgesehen, da die	*
* Texte automatisch umgebrochen werden.						*
*										*
********************************************************************************}

{$I ../Settings.inc}

unit DXTextViewer;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  DXContainer, DXDraws, XForce_types, Blending, DXScrollBar, Kd4Utils, TraceFile,
  DirectDraw, DirectFont;

type
  TDXTextViewer = class(TDXComponent)
  private
    fCorners          : TRoundCorners;
    fOriginalWidth    : Integer;
    fBorderColor      : TColor;
    fText             : String;
    fList             : TStringList;
    fScrollBar        : TDXScrollBar;
    fShowScroll       : boolean;
    fLeftMargin       : Integer;
    fTopMargin        : Integer;
    fCorner           : TCorners;
    fBlending         : Boolean;
    fFonts            : Array of TDirectFont;
    fFontNames        : Array of String;
    fAktuScrollBars   : Integer;
    fOriginalCorners  : TRoundCorners;
    fShowBorder       : Boolean;
    fBlendValue       : Integer;
    procedure SetCorners(const Value: TRoundCorners);
    function GetFrameRect: TRect;
    function GetTextHeight: Integer;
    procedure SetBorderColor(const Value: TColor);
    procedure SetText(const Value: String);
    procedure CalculateTextFluss(Text: String;MaxWidth: integer);
    procedure AktuScrollBar;
    procedure OnScroll(Sender: TObject);
    function GetBlendColor: TBlendColor;
    procedure SetBlendColor(const Value: TBlendColor);
    { Private-Deklarationen }
  protected
    procedure CreateMouseRegion;override;
    procedure SetComponentHint(const Value: String);override;
    procedure DoScroll(Direction: TScrollDirection; Pos: TPoint);override;
    procedure FontChange(Sender: TObject);override;
    { Protected-Deklarationen }
  public
    constructor Create(Page: TDXPage);override;
    destructor destroy;override;
    procedure Draw(Surface: TDirectDrawSurface;var Mem: TDDSurfaceDesc);override;
    procedure Redraw;override;
    procedure VisibleChange;override;
    procedure RegisterFont(Name: String; Font: TDirectFont);
    procedure Resize(var NewLeft, NewTop, NewWidth, NewHeight: Integer);override;

    property RoundCorners : TRoundCorners read fCorners write SetCorners;
    property BlendColor   : TBlendColor read GetBlendColor write SetBlendColor;
    property Blending     : Boolean read fBlending write fBlending;
    property BlendValue   : Integer read fBlendValue write fBlendValue;
    property BorderColor  : TColor read fBorderColor write SetBorderColor;
    property LeftMargin   : Integer read fLeftMargin write fLeftMargin;
    property TopMargin    : Integer read fTopMargin write fTopMargin;
    property Text         : String write SetText;
    property Lines        : TStringList read fList;
    property ShowBorder   : Boolean read fShowBorder write fShowBorder;

    property ScrollBar    : TDXScrollBar read fScrollBar;
    { Public-Deklarationen }
  end;

implementation

{ TDXTextViewer }

procedure TDXTextViewer.AktuScrollBar;
var
  NeedScrollBar: boolean;
begin
  NeedScrollBar:=GetTextHeight>Height-(fTopMargin*2);
  inc(fAktuScrollBars);
  if NeedScrollBar and (not fShowScroll) then
  begin
    if fOriginalCorners=rcBottom then
      SetCorners(rcLeftBottom)
    else if fOriginalCorners=rcTop then
      SetCorners(rcLeftTop)
    else if fOriginalCorners=rcAll then
      SetCorners(rcLeft);
    fScrollBar.Visible:=true and Visible;
    fShowScroll:=true;
    Width:=fOriginalWidth;
    fScrollBar.SmallChange:=-Font.Height+3;
    fScrollBar.LargeChange:=Height;
    fScrollBar.min:=0;
    fScrollBar.Max:=GetTextHeight-Height+(fTopMargin*2);
    fScrollBar.Value:=0;
  end
  else if (not NeedScrollBar) and fShowScroll then
  begin
    SetCorners(fOriginalCorners);
    fScrollBar.Value:=0;
    fScrollBar.Max:=0;
    fScrollBar.Visible:=false;
    fShowScroll:=false;
    Width:=fOriginalWidth;
  end;
  dec(fAktuScrollBars);
end;

procedure TDXTextViewer.CalculateTextFluss(Text: String;
  MaxWidth: integer);
var
  Dummy     : Integer;
  Line      : String;
  Word      : String;
  Ch        : Char;
  LineWidth : Integer;
  Added     : boolean;
  Bis       : Integer;
  Break     : boolean;
  LastFont  : Char;
  Font      : TDirectFont;

  procedure AddLine(Line: String);
  var
    Dummy: Integer;
  begin
    if LastFont=#0 then
      fList.Add(Line)
    else
      fList.Add(LastFont+Line);

    // Letzte Schrift ermitteln
    for Dummy:=length(Line) downto 1 do
    begin
      if Line[Dummy]<#32 then
      begin
        LastFont:=Line[Dummy];
        exit;
      end;
    end;
  end;

begin
  Assert(length(fFonts)>0);
  Font:=fFonts[0];

  if Font=nil then
    Font:=WhiteStdFont;

  LastFont:=#0;
  Text:=StringReplace(Text,#13#10,#255#5#254,[rfReplaceAll]);
  Text:=StringReplace(Trim(Text),#10,#255#5#254,[rfReplaceAll]);

  for Dummy:=0 to high(fFontNames) do
  begin
    Text:=StringReplace(Text,fFontNames[Dummy],Char(Dummy+2),[rfReplaceAll]);
  end;

  fList.Clear;
  Added:=false;
  LineWidth:=0;
  Bis:=length(Text);
  break:=false;
  Dummy:=1;
  while (Dummy<=Bis) do
  begin
    Assert(Font<>nil);
    Ch:=Text[Dummy];
    if (ch=#255) and (Dummy<(Bis-1)) and (Text[Dummy+1]=#5)  and (Text[Dummy+2]=#254)then
    begin
      inc(Dummy,2);
      break:=true;
      LineWidth:=LineWidth+Font.TextWidth(Word);
      if LineWidth>MaxWidth then
      begin
        AddLine(Line);
        Line:='';
      end;
      Line:=Line+Word;
      Word:='';
    end
    else
    begin
      if (Ch<#32) and ((Integer(Ch)-1)<high(fFonts)) then
        Font:=fFonts[Integer(Ch)-1];
      Word:=Word+Ch;
    end;
    Added:=false;
    if (Ch=' ') or break then
    begin
      LineWidth:=LineWidth+Font.TextWidth(Word);
      if (LineWidth>MaxWidth) or break then
      begin
        AddLine(Line);
        LineWidth:=Font.TextWidth(Word);
        Line:='';
        Added:=true;
        break:=false;
      end;
      Line:=Line+Word;
      Word:='';
    end
    else if (Font.TextWidth(Word)>MaxWidth) then
    begin
      AddLine(Copy(Word,0,length(Word)-1));
      LineWidth:=0;
      Word:=ch;
      Line:='';
    end;
    inc(Dummy);
  end;
  if not Added then
  begin
    LineWidth:=LineWidth+Font.TextWidth(Word);
    if LineWidth>MaxWidth then
    begin
      AddLine(Line);
      Line:='';
    end;
    Line:=Line+Word;
    AddLine(Line);
  end;
  AktuScrollBar;
end;

constructor TDXTextViewer.Create(Page: TDXPage);
begin
  inherited;
  fList:=TStringList.Create;
  fTopMargin:=3;
  fLeftMargin:=3;
  fShowScroll:=false;
  fScrollBar:=TDXScrollBar.Create(Page);
  fScrollBar.Visible:=false;
  fScrollBar.Kind:=sbVertical;
  fScrollBar.RoundCorners:=rcRight;
  fScrollBar.OnChange:=OnScroll;
  fBlending:=false;
  fShowBorder:=true;

  SetLength(fFonts,1);

  fAktuScrollBars:=0;
  fBlendValue:=100;
end;

procedure TDXTextViewer.CreateMouseRegion;
var
  FrameRect: TRect;
  RoundRGN : HRGN;
begin
  FrameRect:=GetFrameRect;
  RoundRGN:=CreateRoundRectRgn(FrameRect.Left,FrameRect.Top,FrameRect.Right+1,FrameRect.Bottom+1,20,20);
  fMouseRegion:=CreateRectRgn(Left,Top,Right,Bottom);
  CombineRgn(fMouseRegion,fMouseRegion,RoundRGN,RGN_AND);
  DeleteObject(RoundRGN);
end;

destructor TDXTextViewer.destroy;
begin
  fList.Free;
  inherited;
end;

procedure TDXTextViewer.DoScroll(Direction: TScrollDirection; Pos: TPoint);
begin
  if not fScrollBar.Visible then exit;
  if Direction=sdUp then fScrollBar.Value:=fScrollBar.Value-(fScrollBar.SmallChange shl 1);
  if Direction=sdDown then fScrollBar.Value:=fScrollBar.Value+(fScrollBar.SmallChange shl 1);
end;

procedure TDXTextViewer.Draw(Surface: TDirectDrawSurface;var Mem: TDDSurfaceDesc);
var
  TempRect : TRect;
  Dummy    : Integer;
  TextTop  : Integer;
  TextLeft : Integer;
begin
  TempRect:=ClientRect;
  TempRect.Right:=TempRect.Left+fOriginalWidth;
  
  if fBlending and AlphaElements then
    BlendRoundRect(TempRect,fBlendValue,fScrollBar.FirstColor,Surface,Mem,11,fCorner,ClientRect);

  if fShowBorder then
    FramingRect(Surface,Mem,TempRect,fCorner,11,fBorderColor);
//  BlendFrameRoundRect(ClientRect,100,fScrollBar.FirstColor,Surface,15,

  TempRect:=GetFrameRect;
  TextTop:=Top+fTopMargin-fScrollBar.Value;
  TempRect:=ClientRect;
  inc(TempRect.Left,fLeftMargin);
  dec(TempRect.Right,fLeftMargin);
  inc(TempRect.Top,fTopMargin);
  dec(TempRect.Bottom,fTopMargin);
  TextLeft:=Left+fLeftMargin;
  Surface.ClippingRect:=ResizeRect(ClientRect,2);
  for Dummy:=0 to fList.Count-1 do
  begin
//    TextRect(Canvas,TempRect,TextLeft,TextTop,fList[Dummy],Font.Color);
    FontEngine.DynamicText(Surface,TextLeft,TextTop,fList[Dummy],fFonts);
    inc(TextTop,-Font.Height+3);
  end;
  Surface.ClearClipRect;
end;

procedure TDXTextViewer.FontChange(Sender: TObject);
begin
  fFonts[0]:=FontEngine.FindDirectFont(Font,clBlack);
end;

function TDXTextViewer.GetBlendColor: TBlendColor;
begin
  result:=fScrollBar.FirstColor;
end;

function TDXTextViewer.GetFrameRect: TRect;
begin
  result:=Bounds(Left,Top,fOriginalWidth,Height);
  if fCorners in [rcLeft,rcLeftTop,rcLeftBottom,rcNone] then inc(result.Right,20);
  if fCorners in [rcTop,rcLeftTop,rcRightTop,rcNone] then inc(result.Bottom,20);
  if fCorners in [rcRight,rcRightTop,rcRightBottom,rcNone] then dec(result.Left,20);
  if fCorners in [rcBottom,rcLeftBottom,rcRightBottom,rcNone] then dec(result.Top,20);
end;

function TDXTextViewer.GetTextHeight: Integer;
begin
  result:=fList.Count*(-Font.Height+3);
end;

procedure TDXTextViewer.OnScroll(Sender: TObject);
begin
  Container.RedrawArea(ClientRect,Container.Surface);
end;

procedure TDXTextViewer.Redraw;
begin
  if Visible then
    Container.RedrawArea(Bounds(Left,Top,fOriginalWidth,Height),Container.Surface);
end;

procedure TDXTextViewer.RegisterFont(Name: String; Font: TDirectFont);
begin
  SetLength(fFontNames,length(fFontNames)+1);
  fFontNames[High(fFontNames)]:='<'+Name+'>';

  SetLength(fFonts,length(fFonts)+1);
  fFonts[High(fFonts)]:=Font;
end;

procedure TDXTextViewer.Resize(var NewLeft, NewTop, NewWidth,
  NewHeight: Integer);
begin
  Container.IncLock;
  fOriginalWidth:=NewWidth;
  if fScrollBar.Visible then
  begin
    fScrollBar.SetRect(NewLeft+NewWidth-17,NewTop,17,NewHeight);
    NewWidth:=fOriginalWidth-16;
  end
  else
  begin
    NewWidth:=fOriginalWidth;
    fScrollBar.SetRect(0,0,0,0);
    fScrollBar.Value:=0;
  end;
  CalculateTextFluss(fText,NewWidth-(fLeftMargin*2));
  Container.DecLock;
end;

procedure TDXTextViewer.SetBlendColor(const Value: TBlendColor);
begin
  fScrollBar.FirstColor:=Value;
end;

procedure TDXTextViewer.SetBorderColor(const Value: TColor);
begin
  fBorderColor := Container.Surface.ColorMatch(Value);
  fScrollBar.SecondColor := Value;
  Redraw;
end;

procedure TDXTextViewer.SetComponentHint(const Value: String);
begin
  inherited;
  fScrollBar.Hint:=Value;
end;

procedure TDXTextViewer.SetCorners(const Value: TRoundCorners);
begin
  if fAktuScrollBars=0 then
  begin
    fOriginalCorners:=Value;
  end;
  fCorners := Value;
  case fOriginalCorners of
    rcLeft,rcLeftTop,rcLeftBottom,rcNone : fScrollBar.RoundCorners:=rcNone;
    rcAll,rcRight                        : fScrollBar.RoundCorners:=rcRight;
    rcBottom                             : fScrollBar.RoundCorners:=rcRightBottom;
    rcTop                                : fScrollBar.RoundCorners:=rcRightTop;
    rcRightTop,rcRightBottom             : fScrollBar.RoundCorners:=Value;
  end;
  fCorners := Value;
  fCorner:=[];
  if fCorners in [rcLeft,rcTop,rcLeftTop,rcAll] then include(fCorner,cLeftTop);
  if fCorners in [rcRight,rcTop,rcRightTop,rcAll] then include(fCorner,cRightTop);
  if fCorners in [rcLeft,rcBottom,rcLeftBottom,rcAll] then include(fCorner,cLeftBottom);
  if fCorners in [rcRight,rcBottom,rcRightBottom,rcAll] then include(fCorner,cRightBottom);
  RecreateMouseRegion;
  Redraw;
end;

procedure TDXTextViewer.SetText(const Value: String);
begin
  Container.Lock;
  fText:=Value;
{  if not Visible then
  begin
    Container.UnLock;
    exit;
  end;}
  CalculateTextFluss('',Width-(fLeftMargin*2));
  CalculateTextFluss(fText,Width-(fLeftMargin*2));
  Container.UnLock;
  Redraw;
end;

procedure TDXTextViewer.VisibleChange;
begin
  if Visible then
  begin
    fShowScroll:=false;
    CalculateTextFluss(fText,Width-(fLeftMargin*2));
//    Width:=fOriginalWidth
  end
  else fScrollBar.Visible:=false;
end;

end.
