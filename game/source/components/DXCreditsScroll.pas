{********************************************************************************
*										*
* Copyright (C) 2004  X-Force Team - jim_raynor@web.de				*
*										*
* This file is part of X-Force: Fight For Destiny (X-Force).			*
*										*
* X-Force is free software; you can redistribute it and/or modify		*
* it under the terms of the GNU General Public License as published by		*
* the Free Software Foundation; either version 2 of the License, or		*
* (at your option) any later version.						*
*										*
* X-Force is distributed in the hope that it will be useful,			*
* but WITHOUT ANY WARRANTY; without even the implied warranty of		*
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the			*
* GNU General Public License for more details.					*
*										*
* You should have received a copy of the GNU General Public License		*
* along with X-Force; if not, write to the Free Software			*
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA	*
*										*
*********************************************************************************
*										*
* Komponente zum Anzeigen der Mitwirkenden					*
*										*
********************************************************************************}

{$I ../Settings.inc}

unit DXCreditsScroll;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  DXContainer, DXDraws, extctrls, DirectDraw, Defines, DirectFont, TraceFile;

type
  TCreditLine = record
    Token     : String;
    OffSet    : Integer;
    FontSize  : Integer;
    HeadLine  : boolean;
    Font      : TDirectFont;
    Text      : String;
    Color     : TColor;
    Style     : TFontStyles;
    Justify   : TAlignment;
  end;

  TDXCreditsScroll = class(TDXComponent)
  private
    fStream     : TStream;
    fTop        : Integer;
    Lines       : Array of TCreditLine;
    EndCredit   : boolean;
    fPlay       : boolean;
    fOnStop     : TNotifyEvent;
    { Private-Deklarationen }
  protected
    function PerformFrame(Sender: TObject;Frames: Integer): boolean;
    { Protected-Deklarationen }
  public
    procedure Draw(Surface: TDirectDrawSurface;var Mem: TDDSurfaceDesc);override;
    procedure Show;
    procedure Start;
    procedure Stop;
    property Stream :TStream read fStream write fStream;
    property OnStop : TNotifyEvent read fOnStop write fOnStop;
  end;

implementation

{ TDXCreditsScroll }

procedure TDXCreditsScroll.Draw(Surface: TDirectDrawSurface;var Mem: TDDSurfaceDesc);
var
  Dummy   : Integer;
  y       : Integer;
  x       : Integer;
begin
  y:=fTop+Top;
  for Dummy:=0 to Length(Lines)-1 do
  begin
    inc(y,Lines[Dummy].OffSet);
    if Lines[Dummy].Text<>'' then
    begin
      if (y>Top-Lines[Dummy].Font.TextHeight('')-5) and (y<Bottom) then
      begin
        x:=Left+((Width shr 1)-(Lines[Dummy].Font.TextWidth(Lines[Dummy].Text) shr 1))-1;

        if (y>Top+50) and (y<Bottom-50-Lines[Dummy].Font.TextHeight('')) then
          Lines[Dummy].Font.Draw(Surface,x,y-1,Lines[Dummy].Text)
        else if (y<=Top+50) then
          Lines[Dummy].Font.DrawAlpha(Surface,x,y-1,Lines[Dummy].Text,round((y-Top)*5.12))
        else if (y>=Bottom-50-Lines[Dummy].Font.TextHeight('')) then
          Lines[Dummy].Font.DrawAlpha(Surface,x,y-1,Lines[Dummy].Text,255-round((50-(Bottom-y-Lines[Dummy].Font.TextHeight('')))*5.12))
      end
      else if Y>Bottom then
        exit;
    end;
    inc(y,Lines[Dummy].Font.TextHeight(''));
  end;
  if (y<Top-Lines[0].Font.TextHeight('')) then EndCredit:=true;
end;

function TDXCreditsScroll.PerformFrame(Sender: TObject;Frames: Integer): boolean;
begin
  if (GetKeyState(VK_ESCAPE)<0) or EndCredit then
  begin
    Stop;
    result:=false;
    exit;
  end;
  dec(fTop,Frames);
  Redraw;
  result:=true;
end;

procedure TDXCreditsScroll.Show;
begin
  Container.MessageLock(true);
  Container.AddFrameFunction(PerformFrame,nil,37);
  fPlay:=true;
  EndCredit:=false;
  fTop:=Height;
end;

procedure TDXCreditsScroll.Start;
var
  Text      : String;
  Token     : String;
  Zeichen   : Char;
  EndParse  : boolean;
  Tok       : boolean;
  Index     : Integer;
  Font      : TFont;
  Dummy     : Integer;
  Left      : Boolean;
begin
  if not Assigned(Stream) then exit;
  fTop:=Bottom;
  SetLength(Lines,0);
  Stream.Position:=0;
  Index:=-1;
  EndParse:=false;
  Tok:=false;
  Text:='';
  while not EndParse do
  begin
    if Stream.Read(Zeichen,1)=0 then Endparse:=true;
    if Zeichen='<' then
    begin
      if Text<>'' then
      begin
        if Lines[Index].Token='caption' then
          Lines[Index].Text:='=== '+Uppercase(Text)+' ==='
        else
          Lines[Index].Text:=Text;
        Text:='';
      end;
      inc(Index);
      SetLength(Lines,Index+1);
      Tok:=true;
    end
    else if Zeichen='>' then
    begin
           if Token='end' then EndParse:=true
      else if Token='caption' then
      begin
        Lines[Index].OffSet:=20;
        Lines[Index].FontSize:=14;
        Lines[Index].Color:=$654321;
        Lines[Index].Style:=[fsBold];
        Lines[Index].Justify:=taCenter;
      end
      else if Token='text' then
      begin
        Lines[Index].HeadLine:=false;
        Lines[Index].FontSize:=10;
        Lines[Index].Color:=clYellow;
      end
      else if Token='header' then
      begin
        Lines[Index].HeadLine:=true;
        Lines[Index].FontSize:=18;
        Lines[Index].Color:=clYellow;
        Lines[Index].Justify:=taCenter;
      end
      else if Token='name' then
      begin
        Lines[Index].HeadLine:=false;
        Lines[Index].FontSize:=12;
        Lines[Index].Color:=$C0C0C0;
      end;
      Lines[Index].Token:=Token;
      Tok:=false;
      Token:='';
    end
    else
    begin
      if (Zeichen>=#32) and (Zeichen<=#255) then
      begin
        if not Tok then Text:=Text+Zeichen
        else Token:=Token+Zeichen;
      end;
    end;
  end;
  Font:=TFont.Create;
  Font.Color:=clWhite;
  Font.Name:=coFontName;
  Left:=false;
  for dummy:=0 to length(Lines)-1 do
  begin
    Font.Size:=Lines[Dummy].FontSize;
    Font.Style:=Lines[Dummy].Style;
    Font.Color:=Lines[Dummy].Color;
    Lines[Dummy].Font:=FontEngine.FindDirectFont(Font,clBlack);
    if Lines[Dummy].Justify<>taCenter then
    begin
      if Left then
        Lines[Dummy].Justify:=taLeftJustify
      else
        Lines[Dummy].Justify:=taRightJustify;

      Left:=not Left;
    end;
  end;
  Font.Free;
end;

procedure TDXCreditsScroll.Stop;
begin
  if not fPlay then exit;
  Container.DeleteFrameFunction(PerformFrame,nil);
  EndCredit:=true;
  fPlay:=false;
  Container.MessageLock(false);
  if Assigned(fOnStop) then fOnStop(Self);
end;

end.
