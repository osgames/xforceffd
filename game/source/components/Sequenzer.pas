{********************************************************************************
*										*
* Copyright (C) 2004  X-Force Team - jim_raynor@web.de				*
*										*
* This file is part of X-Force: Fight For Destiny (X-Force).			*
*										*
* X-Force is free software; you can redistribute it and/or modify		*
* it under the terms of the GNU General Public License as published by		*
* the Free Software Foundation; either version 2 of the License, or		*
* (at your option) any later version.						*
*										*
* X-Force is distributed in the hope that it will be useful,			*
* but WITHOUT ANY WARRANTY; without even the implied warranty of		*
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the			*
* GNU General Public License for more details.					*
*										*
* You should have received a copy of the GNU General Public License		*
* along with X-Force; if not, write to the Free Software			*
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA	*
*										*
*********************************************************************************
*										*
* Sequenzer bietet die m�glichkeit gleichm��ig Zufallszahlen zu erstellen.	*
* �ber SetHighest wird der gr��tm�gliche Wert, der zur�ckgegeben wird,		*
* angegeben (ACHTUNG!! Nicht zu gro� machen).Der kleinste wird ist immer 0	*
* 										*
* Mit SetToleranz wird angegeben, wie viel Vorsprung ein Wert haben darf.       *
* Beispiel: Toleranz 1. Eine Zahl taucht erst ein zweites mal auf, wenn im      *
*           angebenen Bereich alle Zahlen ein mal dran waren.                   *
*           Bei Toleranz 2 darf eine Zahl erst zum dritten Mal auftauchen, wenn *
*           alle anderen Zahlen bereits aufgetaucht sind.			*
*										*
* Wird h�ufig verwendet, um Symbole f�r UFOs oder Aliens gleichm��ig zu		*
* verteilen.									*
*										*
********************************************************************************}

{$I ../Settings.inc}

unit Sequenzer;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  math;

type
  TSequenzer = class(TObject)
  private
    fArray    : Array of Integer;
    fMax      : Integer;
    fToleranz : Integer;
    { Private-Deklarationen }
  protected
    { Protected-Deklarationen }
  public
    constructor Create;
    procedure Reset;
    procedure SetHighest(max: Integer);
    procedure SetToleranz(Toleranz: Integer);
    function GetNumber: Integer;
    { Public-Deklarationen }
  end;

implementation

{ TSequenzer }

constructor TSequenzer.Create;
begin
  fmax:=-1;
  fToleranz:=1;
end;

function TSequenzer.GetNumber: Integer;
var
  Numbers: Array of Integer;
  Anzahl : Integer;
  rand   : Integer;

  procedure SearchNumbers;
  var
    Dummy  : Integer;
    minAuf : Integer;
  begin
    minAuf:=fArray[0];
    for Dummy:=1 to fMax-1 do
    begin
      minAuf:=min(minAuf,fArray[Dummy]);
    end;
    Anzahl:=0;
    minAuf:=minAuf+fToleranz-1;
    for Dummy:=0 to fMax-1 do
    begin
      if fArray[Dummy]<=minAuf then
      begin
        SetLength(Numbers,Anzahl+1);
        Numbers[Anzahl]:=Dummy;
        inc(Anzahl);
      end;
    end;
  end;

begin
  if fMax<1 then
  begin
    result:=-1;
    exit;
  end;
  SearchNumbers;
  rand:=random(Anzahl);
  result:=Numbers[rand];
  inc(fArray[result]);
  SetLength(Numbers,0);
end;

procedure TSequenzer.Reset;
var
  Dummy: Integer;
begin
  for DUmmy:=0 to fMax-1 do
  begin
    fArray[Dummy]:=0;
  end;
end;

procedure TSequenzer.SetHighest(max: Integer);
begin
  randomize;
  SetLength(fArray,0);
  SetLength(fArray,max+1);
  fMax:=max+1;
  Reset;
end;

procedure TSequenzer.SetToleranz(Toleranz: Integer);
begin
  if Toleranz<1 then exit;
  fToleranz:=Toleranz;
end;

end.
 
