Der Quellcode zu X-Force: Fight For Destiny wurde bisher erfolgreich unter folgenden
Delphi-Versionen kompiliert:

Delphi 5 Enterprise
Delphi 5 Enterprise mit Service Pack 1
Delphi 6 Personal
Delphi 6 Enterprise

Zur Einrichtung m�ssen folgende Schritte unternommen werden:

1. Aktuellste Version von X-Force installieren und per Online-Updater die wichtigen Updates
   ziehen.
   
   Als weiteres sollten die aktuellsten Dateien aus dem bin-Verzeichnis des CVS Archives
   installiert werden.

2. Quellcode herunterladen (CVS-Verzeichnis Source) und in ein beliebiges Verzeichnis speichern.
   Verzeichnisstruktur muss dabei beibehalten werden.
   
3. Jedi Code Library (JCL) installieren. Der Download befindet sich hier:
   http://homepages.borland.com/jedi/jcl/page30.html
   
   Nach dem Entpacken des Zip-Archives die Installationsanleitung der JCL befolgen.
   
3a) Pascal Script von http://www.remobjects.com/download.asp?id={23BEE1E9-0933-4C1A-BF69-C98EAEF5296F}&nodownloadinfo=now 
    herunterladen und installieren.
    
3b) Beachte die readme.txt innerhalb des changethirdparty Verzeichnisses

3c) Utility Library von http://philo.de/xml/downloads.shtml herunterladen und installieren.
   
4. Delphi starten

4a) Das Sourceverzeichnis von Pascal Script muss als Bibliothekpfad aufgenommen werden.

5. Projektgruppe f�r die entsprechende Delphi Version �ffnen (xforce_delphix.bpg)

5a. Projektverwaltung anzeigen (Ansicht->Projektverwaltung) (wenn nicht sichtbar)

6. Package delphix_forx.dpr in der Projektverwaltung aktivieren und installieren.
7. Package kd4dx_delphix.dpr in der Projektverwaltung aktivieren und installieren.

8. Projekt X-Force aktivieren
9. Projektoptionen aufrufen
  unter Verzeichnisse/Bedingungen folgende Optionen anpassen
    a) Ausgabeverzeichnis: Hier muss nun das Installationsverzeichnis von X-Force
       angegeben werden, damit die EXE direkt dort erstellt wird.

10. Projekt X-Force kompilieren und hoffen.
11. Wenn alles geklappt, m�sste X-Force kompiliert sein und du kannst mit den Arbeiten
    beginnen.
    
    Empfehlenswert ist es unter Tools->Debugger-Optionen->Sprachexceptions die Option 
    "bei Delphi-Exceptions stoppen" zu deaktivieren. Dadurch wird verhindert, dass X-Force
    stehen bleibt, sobald eine Exception auftritt.

    Und noch ein wichtiger Tipp: Aktiviert das Auto-Speichern. Nichts ist schlimmer als ein
    Absturz nach dem man l�ngere Zeit Programmiert hat und nicht gespeichert hat.