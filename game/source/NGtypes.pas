{********************************************************************************
*										*
* Copyright (C) 2004  X-Force Team - jim_raynor@web.de				*
*										*
* This file is part of X-Force: Fight For Destiny (X-Force).			*
*										*
* X-Force is free software; you can redistribute it and/or modify		*
* it under the terms of the GNU General Public License as published by		*
* the Free Software Foundation; either version 2 of the License, or		*
* (at your option) any later version.						*
*										*
* X-Force is distributed in the hope that it will be useful,			*
* but WITHOUT ANY WARRANTY; without even the implied warranty of		*
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the			*
* GNU General Public License for more details.					*
*										*
* You should have received a copy of the GNU General Public License		*
* along with X-Force; if not, write to the Free Software			*
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA	*
*										*
*********************************************************************************
*										*
* Definierte Events								*
*										*
********************************************************************************}

{$I ../Settings.inc}

unit NGtypes;

interface

uses
  DXContainer, DXDraws, Windows, Forms, XForce_types, controls, Classes, DirectDraw;

type
  { Eventdefinitionen }
  TCanChangeEvent     = procedure(Sender: TDXComponent;var CanChange: boolean) of object;
  TDXOwnerDrawEvent   = procedure(Sender: TDXComponent;Surface: TDirectDrawSurface;const Mem: TDDSurfaceDesc;Rect: TRect; Index: Integer;Selected: boolean) of object;
  TDXDrawHeadRowEvent = procedure(Sender: TDXComponent;Surface: TDirectDrawSurface;Rect: TRect) of object;
  TQueryText          = procedure(Caption: String;MaxLength: Integer;var Text:String) of object;
  TDrawSoldatEvent    = procedure(const Soldat: TSoldatInfo;Surface: TDirectDrawSurface;const Mem: TDDSurfaceDesc; x, y: Integer; Ges: boolean=true) of object;
  TDrawLagerEvent     = procedure(Image: Integer;Surface: TDirectDrawSurface;x,y: Integer;Index: Integer=0;Shadow: boolean=false) of object;
  TDrawOrganEvent     = procedure(Surface: TDirectDrawSurface;x,y: Integer;Index: Integer) of object;
  TDrawIconEvent      = procedure(Surface: TDirectDrawSurface;x,y: Integer) of object;
  TDrawSchiffEvent    = procedure(Surface: TDirectDrawSurface;x,y: Integer;WZ: Integer) of object;
  TDrawAlienEvent     = procedure(Index: Integer;Surface: TDirectDrawSurface;X,Y: Integer) of object;
  TGetItemInfo        = procedure(ID: Cardinal;var Item: TLagerItem) of object;
  TGetProduktInfo     = procedure(ID: Cardinal;var Techniker: Integer;var Strength: Integer) of object;
  TShowUFOPadie       = procedure(EntryType: TUFOPadieEntry;Index: Integer) of object;
  TShowOrgan          = procedure(Organ: Integer) of object;
  TClickKomponent     = procedure(Sender: TObject; Button: TMouseButton;Zelle: TSchiffZelle) of object;
  TGetSpinName        = procedure(Value: Integer;var Text: String) of object;
  TOverKomponent      = procedure(Sender: TObject; Zelle: TSchiffZelle) of object;
  TDXDrawEvent        = procedure(Sender: TDXComponent;Surface: TDirectDrawSurface;var Mem: TDDSurfaceDesc) of object;
  TDXDrawRectEvent    = procedure(Sender: TDXComponent;Surface: TDirectDrawSurface;Rect: TRect;var Mem: TDDSurfaceDesc) of object;
  TDXChangeWaffe      = procedure(Zelle: Integer) of Object;
  TEnumProjektFunc    = procedure(Projekt: TEndedForschProjekt) of Object;
  TClickSlot          = procedure(Slot: Integer; Button: TMouseButton) of Object;
  TOverSlot           = procedure(Slot: Integer; HighLight: boolean; Rect: TRect;Soldat: TSoldatInfo) of Object;
  TOverItem           = procedure(Index: Integer; Rect: TRect) of object;
  TShowPopupInfo      = procedure(ID: Cardinal; Rect: TRect) of object;
  TButtonState        = (bsLeft,bsRight,bsNone);
  TCanChangeHotKey    = procedure(Sender: TObject;HotKey: Char; var CanChange: boolean) of object;
  TItemChangeEvent    = procedure(ID: Cardinal;Count: Integer) of object;
  TDeleteItem         = procedure(ID: Cardinal) of object;
  TGetSelectedItem    = procedure(var Index: Integer) of object;
  TEnumStringList     = procedure(Strings: TStringList;Data: Cardinal) of object;
  TBooleanProcedure   = procedure(Bool: Boolean) of object;
  TMissionEvent       = procedure(Event: TEreignissType;ObjektID: Cardinal) of object;
  TNeedItemEvent      = procedure(ID: Cardinal;var HasItem: boolean; Anzahl: Integer = 1) of object;
  TFreeItemEvent      = procedure(ID: Cardinal; Anzahl: Integer = 1) of object;
  TMessageEvent       = procedure(Message: String) of object;
  TEnumItem           = procedure(SID: Cardinal; Slot: TSoldatConfigSlot;Schuesse: Integer = -1;Anzahl: Integer = 1;ZusData: Pointer = nil) of object;
  TEnumItems          = procedure(Manager: Pointer;CallBack: TEnumItem) of object;
  TGetRaumschiff      = procedure(Manager: Pointer;var RSchiff: Pointer) of object;
  TCheckAction        = function(Manager: Pointer;const FromSlot,ToSlot: TItemSlot): TDragActionError of object;
  TFindPersonEvent    = procedure(Index: Integer; BasisID: Cardinal; WatchType: TWatchType) of object;

  TDrawSmallSolEvent  = procedure(const Soldat: TSoldatInfo;Surface: TDirectDrawSurface;const Mem: TDDSurfaceDesc; x, y: Integer;ZeitEinh: Integer; Ges: boolean = true;MaxZeit: Integer = 0) of object;

  // Kommunikation eines GameFigureManagers mit anderen Objekten
  TConfigCommunication= procedure(Auftrag: Integer;var Param: Pointer; Point: TPoint; Manager: TObject = nil) of object;

  TSetTileProcedure = procedure(X,Y: Integer; const Tile: TMapTileEntry) of object;

  TSetMapSizeProcedure = procedure(MapWidth, MapHeight: Integer) of object;

implementation

end.

