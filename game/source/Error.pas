{********************************************************************************
*										*
* Copyright (C) 2004  X-Force Team - jim_raynor@web.de				*
*										*
* This file is part of X-Force: Fight For Destiny (X-Force).			*
*										*
* X-Force is free software; you can redistribute it and/or modify		*
* it under the terms of the GNU General Public License as published by		*
* the Free Software Foundation; either version 2 of the License, or		*
* (at your option) any later version.						*
*										*
* X-Force is distributed in the hope that it will be useful,			*
* but WITHOUT ANY WARRANTY; without even the implied warranty of		*
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the			*
* GNU General Public License for more details.					*
*										*
* You should have received a copy of the GNU General Public License		*
* along with X-Force; if not, write to the Free Software			*
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA	*
*										*
*********************************************************************************
*										*
* Fehlerdialog									*
*										*
********************************************************************************}

{$I ../Settings.inc}

unit Error;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, Mapi,ShellApi, ExtCtrls;

type
  TErrorDlg = class(TForm)
    Label1: TLabel;
    MessageLabel: TLabel;
    Panel1: TPanel;
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    BitBtn3: TBitBtn;
    BitBtn4: TBitBtn;
    Panel3: TPanel;
    ErrorUnderLine: TPanel;
    Panel6: TPanel;
    Label5: TLabel;
    procedure BitBtn3Click(Sender: TObject);
    procedure BitBtn4Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private-Deklarationen }
  public
    { Public-Deklarationen }
  end;

var
  ErrorDlg: TErrorDlg;

implementation

uses
  ErrorDescription, frmMain;

function SendMail: Boolean;
var
  MapiMsg: TMapiMessage;
  Recips: TMapiRecipDesc;
  Files : TMapiFileDesc;
begin
  with Recips do begin
    ulReserved := 0;
    ulRecipClass := MAPI_TO;
    lpszName := PChar('jim_Raynor@web.de');
    lpszAddress := nil;
    ulEIDSize := 0;
    lpEntryID := nil;
  end;
  with Files do
  begin
    ulReserved := 0;
    flFlags:=0;
    nPosition:=0;
    lpszPathName:=PChar(IncludeTrailingBackslash(ExtractFilePath(Application.ExeName))+ErrorFileName);
    lpszFileName:=nil;
    lpFileType:=PChar('.txt');
  end;
  with MapiMsg do begin
    ulReserved := 0;

// Betreff-Zeile
    lpszSubject := PChar('Bug-Report '+DateTimeToStr(Now));

// Text der Mail
    lpszNoteText := PChar('Folgender Fehler trat bei X-Force auf.'#13#10#13#10+frmErrorDescription.Description.Text);
    lpszMessageType := nil;
    lpszDateReceived := nil;
    lpszConversationID := nil;
    flFlags := 0;

// Informationen �ber den Sender der Mail (TMapiRecipDescr)
    lpOriginator := nil;

// Anzahl der Empf�nger;
    nRecipCount := 1;

// Array der Empf�nger (hier nur einer) (TMapiRecipDescr)
    lpRecips := @Recips;

// Anzahl anh�ngender Dateien
    nFileCount := 1;

// Angeh�ngte Dateien (TMapiFileDescr)
    lpFiles := @Files;
  end;
  result:=MapiSendMail(0, 0, MapiMsg, MAPI_DIALOG or MAPI_LOGON_UI or MAPI_NEW_SESSION, 0)=SUCCESS_SUCCESS;
end;

{$R *.DFM}

procedure TErrorDlg.BitBtn3Click(Sender: TObject);
begin
  if frmErrorDescription=nil then
    frmErrorDescription:=TfrmErrorDescription.Create(Application);

  frmErrorDescription.ShowModal;

  if not SendMail then
    MessageDlg('Failed to open mail client. Please send file '+IncludeTrailingBackslash(ExtractFilePath(Application.ExeName))+frmMain.ErrorFileName+' to jim_raynor@web.de',mtError,[mbOK],0);
end;

procedure TErrorDlg.BitBtn4Click(Sender: TObject);
begin
  ShellExecute(0,'open',PChar(IncludeTrailingBackslash(ExtractFilePath(Application.ExeName))+frmMain.ErrorFileName),nil,nil,SW_SHOW);
end;

procedure TErrorDlg.FormShow(Sender: TObject);
begin
//  ClientHeight:=Label4.Top+Label4.Height+Panel1.Height;
  ShowWindow(Handle,SW_SHOWNORMAL);
end;

procedure TErrorDlg.FormCreate(Sender: TObject);
begin
  ErrorUnderLine.Width:=Label1.Width;
end;

end.
