// Diese $DEFINE Anweisung bestimmt ob KEINE Exception
// ausgel�st wird, wenn der Endtag ( </bla> ) nicht vorhanden ist!
// Damit k�nnen auch HTML-Seiten geparst werden, allerdings sind
// die Ergebnisse nicht immer vorherzusehen, da diese Seiten keine
// XML-Struktur haben. 
{$DEFINE thaxml_nostrikt}
unit ThaXML;
(****
 *
 *  ThaXML Library
 *  (c) 2002 Andreas Resch "ThaBaker"
 *  version: 1.0
 *  Dat.:    12.01.2003
 *
 **
 *
 *  Mit dieser Unit soll der Umgang mit einfachen
 *  XML-Dateien erleichtert werden, die keine
 *  Validierung ben�tigen.
 *
 **
 *
 *  Die Ver�ffentlichung dieser Sourcen erfolgt
 *  in der Hoffnung, dass es Ihnen von Nutzen sein
 *  wird, aber OHNE JEDE GEW�HRLEISTUNG - sogar
 *  ohne die implizite Gew�hrleistung der MARKTREIFE
 *  oder der EIGNUNG F�R EINEN BESTIMMTEN ZWECK.
 *
 ****
 *
 ****
**)

interface

uses
  classes, SysUtils,
  ThaLib;

type
  EXMLException = class( Exception);
  EXML = EXMLException;

  (* TXMLBase
   ***
   * Grundklasse, wird zur Verwaltung eingef�hrt
   ***
   *)
  TXMLBase = class
  end;
  (*--*)

  (* forward-decleration *)
  TXMLTag = class;
  (*--*)

  (* TXMLText
   ***
   * speichert einen Textabschnitt
   ***
   *)
  TXMLText = class(TXMLBase)
  private
    fText: string;
  public
    Constructor create( AParent: TXMLTag); overload;
    Constructor create( AParent: TXMLTag; AText: string); overload;
    property Text : string read fText write fText;
  end;
  (*--*)

  (* TXMLAttributes
   ***
   * -speichert die Attributes eines XML-Tag
   * -sch�tzt vor zwei gleichnamigen Attributen
   * -bietet viele von TStrings bekannten Methoden
   ***
   *)
  TXMLAttributes = class
  private
    fItems : TStringList;
    function GetAttribute(Name: string): string;
    function GetName(Idx: integer): string;
    procedure SetAttribute(Name: string; const Value: string);
    procedure SetName(Idx: integer; const Value: string);
  public
    Constructor create;
    Destructor  Destroy; override;

    procedure AssignTo( Dest: TXMLTag);

    procedure Add( Name, Value : string);
    procedure Delete( Name: string); overload;
    procedure Delete( Idx: integer); overload;
    procedure Clear;
    function  IndexOf( Name: string) : integer;

    function  Exists( Name: string) : boolean;

    function  Count : integer;

    procedure SetData( const Text: string);
    function  GetData : string;

    // gibt die Namen zur�ck
    property Names[ Idx: integer] : string read GetName write SetName;
    // gibt den Value-string zur�ck
    property Attributes[ Name: string] : string read GetAttribute write SetAttribute; default;
  end;
  (*--*)

  (* TXMLItems
   ***
   * -speichert die Textabschnitte (TXMLBase) und
   *  die Kind-Tags (TXMLTag)
   * -wenn ein Element gesetzt wird, wird das alte automatisch freigegeben!
   *  andernfalls muss Unbind() verwendet werden!
   ***
   *)
  TXMLItems = class
  private
    fItems : TList;
    function GetItem(Idx: integer): TXMLBase;
    function GetTag(Idx: integer): TXMLTag;
    function GetText(Idx: integer): TXMLText;
    procedure SetItem(Idx: integer; const Value: TXMLBase);
    procedure SetTag(Idx: integer; const Value: TXMLTag);
    procedure SetText(Idx: integer; const Value: TXMLText);
    function GetTagByCaption(Caption: string): TXMLTag;
    procedure SetTagByCaption(Caption: string; const Value: TXMLTag);
  public
    Constructor create; virtual;
    Destructor  Destroy; override;

    procedure AssignTo( Dest: TXMLTag);

    procedure Add( Value: TXMLBase);
    procedure AddText( Value: string);
    procedure Insert( Index: integer; Value: TXMLBase);
    procedure Delete( Index: integer);
    procedure Unbind( Index: integer);
    procedure Exchange( Index1, Index2: integer);
    procedure Clear;

    function  ExistsTag( const Caption: string) : boolean;

    function IndexOf( Value: pointer) : integer;
    function IndexOfText( const Text: string) : integer;
    function IndexOfTag( const Caption: string) : integer;

    function Count : integer;
    function TextCount : integer;
    function TagCount : integer;

    (* array auf ALLE Bausteine (sowohl Text als auch Tags)
     * idx: absolut
     *)
    property Items[ Idx: integer] : TXMLBase read GetItem write SetItem; default;

    (* array auf Textabschnitte
     * idx: nur Textabschnitte werden gez�hlt!
     *)
    property Text[ Idx: integer] : TXMLText read GetText write SetText;

    (* array auf Kind-Tags
     * idx: nur Kind-Tags werden gez�hlt!
     *)
     property Tags[ Caption: string] : TXMLTag read GetTagByCaption write SetTagByCaption;
     property TagsByIdx[ Idx: integer] : TXMLTag read GetTag write SetTag;
  end;
  (*--*)

  (* TXMLTag
   * Klasse f�r einen XML-Tag:
   * Tag-Caption, Attribute, Kind-Tags, Textabschnitte
   *)
  TXMLTag = class( TXMLBase)
  private
    fAttributes : TXMLAttributes;
    fItems   : TXMLItems;
    fCaption: string;
    function GetValue: string;
  public
    Constructor create( AOwner: TXMLTag); overload;
    Constructor create( AOwner: TXMLTag; ACaption : string); overload;
    Constructor create( AOwner: TXMLTag; ACaption, AValue : string); overload;
    Destructor  Destroy; override;

    function Copy : TXMLTag;

    property Caption : string read fCaption write fCaption;
    property Items   : TXMLItems read fItems;
    property Attributes : TXMLAttributes read fAttributes{ write SetAttributes};
    property Value   : string read GetValue;
  end;
  (*--*)

  (* TXMLCodec
   * (soll!) de-/encodiert Unicode-zeichen...
   *)
  TXMLCodec = class( TComponent)
    function Decode( const Text: string) : string; virtual;
    function Encode( const Text: string) : string; virtual;
  end;

  (* TXMLParser
   ***
   * parst und speichert eine XML Datei
   ***
   *)
  TXMLParser = class( TComponent)
  private
    fCodec: TXMLCodec;
  public
    Constructor create( AOwner: TComponent); override;
    
    function ParseXMLText( const Text : string; root : TXMLTag) : boolean;
    function ParseXMLFile( FileName : string; root : TXMLTag) : boolean;

    procedure SaveToXMLStrings( root : TXMLBase; Dest: TStrings; spacing: integer=0; VisibleRoot: boolean=false);
    procedure SaveToXML( root : TXMLBase; FileName : string; VisibleRoot: boolean=true; standalone: boolean=true);

  published
    property Codec: TXMLCodec read fCodec write fCodec;
  end;
  (*--*)


const
  // xml datei header
  xmlVersion1_0         = '<?xml version="1.0"?>';
  xmlVersion1Standalone = '<?xml version="1.0" standalone="yes"?>';
  xmlVersionEx          = '<?xml version="%s" %s?>';

  // fehlermeldungen
  cInvalidXML = '"%s" ist keine XML-Datei! (Version 1.0)';
  cMissingTag = 'Fehler in "%s".'#13#10'Tag %s fehlt!';

  // typkennungen
  _OP_Integer = 'ObjectPascal.Integer.32bit';
  _OP_Int64   = 'ObjectPascal.Integer.64bit';
  _OP_Double  = 'ObjectPascal.Double';
  _OP_Extended = 'ObjectPascal.Extended';

// CreateTagWithValue
// erstellt einen Tag mit �bergebener Caption und Value
function CreateTagWithValue( AOwner: TXMLTag; const Caption, Value: string) : TXMLTag;


// standard-typen
function XMLCreateInteger( AParent: TXMLTag; const Value: integer; const caption: string='integer'; StoreAsAttribute: boolean = false) : TXMLTag;
function XMLCreateInt64( AParent: TXMLTag; const Value: int64; const caption: string='int64'; StoreAsAttribute: boolean = false) : TXMLTag;
function XMLCreateDouble( AParent: TXMLTag; const Value: double; const caption: string='double'; StoreAsAttribute: boolean = false) : TXMLTag;
function XMLCreateExtended( AParent: TXMLTag; const Value: extended; const caption: string='extended'; StoreAsAttribute: boolean = false) : TXMLTag;

function XMLLoadInteger( Item: TXMLTag) : integer;
function XMLLoadInt64( Item: TXMLTag) : int64;
function XMLLoadDouble( Item: TXMLTag) : double;
function XMLLoadExtended( Item: TXMLTag) : extended;
//-----

procedure Register;


implementation

function CreateTagWithValue( AOwner: TXMLTag; const Caption, Value: string) : TXMLTag;
begin
  result := TXMLTag.create( AOwner, Caption);
  result.Items.AddText( value);
end;

function XMLCreateInteger( AParent: TXMLTag; const Value: integer; const caption: string='integer'; StoreAsAttribute: boolean = false) : TXMLTag;
begin
  result := TXMLTag.create( AParent, Caption);
  result.Attributes.Add( 'type', _OP_Integer);
  if StoreAsAttribute then
    result.Attributes.Add( 'value', IntToStr( value))
  else TXMLText.create( result, IntToStr( value));
end;

function XMLCreateInt64( AParent: TXMLTag; const Value: int64; const caption: string='int64'; StoreAsAttribute: boolean = false) : TXMLTag;
begin
  result := TXMLTag.create( AParent, Caption);
  result.Attributes.Add( 'type', _OP_Int64);
  if StoreAsAttribute then
    result.Attributes.Add( 'value', IntToStr( value))
  else TXMLText.create( result, IntToStr( value));
end;

function XMLCreateDouble( AParent: TXMLTag; const Value: double; const caption: string='double'; StoreAsAttribute: boolean = false) : TXMLTag;
begin
  result := TXMLTag.create( AParent, Caption);
  result.Attributes.Add( 'type', _OP_Double);
  if StoreAsAttribute then
    result.Attributes.Add( 'value', FloatToStr( value))
  else TXMLText.create( result, FloatToStr( value));
end;

function XMLCreateExtended( AParent: TXMLTag; const Value: extended; const caption: string='extended'; StoreAsAttribute: boolean = false) : TXMLTag;
begin
  result := TXMLTag.create( AParent, Caption);
  result.Attributes.Add( 'type', _OP_Extended);
  if StoreAsAttribute then
    result.Attributes.Add( 'value', FloatToStr( value))
  else TXMLText.create( result, FloatToStr( value));
end;

function XMLLoadInteger( Item: TXMLTag) : integer;
begin
  if Item.Attributes.Exists( 'type') then
    if UpperComp( Item.Attributes[ 'type'], _OP_Integer) then
    begin
      if Item.Attributes.Exists( 'value') then
        result := StrToInt( Item.Attributes[ 'value'])
      else result := StrToInt( Item.Value);
    end else
      raise EXMLException.create( 'Fehler beim Laden eines Integers');
end;

function XMLLoadInt64( Item: TXMLTag) : int64;
begin
  if Item.Attributes.Exists( 'type') then
    if UpperComp( Item.Attributes[ 'type'], _OP_Int64) then
    begin
      if Item.Attributes.Exists( 'value') then
        result := StrToInt64( Item.Attributes[ 'value'])
      else result := StrToInt64( Item.Value);
    end else
      raise EXMLException.create( 'Fehler beim Laden eines Int64');
end;

function XMLLoadDouble( Item: TXMLTag) : double;
begin
  if Item.Attributes.Exists( 'type') then
    if UpperComp( Item.Attributes[ 'type'], _OP_Double) then
    begin
      if Item.Attributes.Exists( 'value') then
        result := StrToFloat( Item.Attributes[ 'value'])
      else result := StrToFloat( Item.Value);
    end else
      raise EXMLException.create( 'Fehler beim Laden eines Doubles');
end;

function XMLLoadExtended( Item: TXMLTag) : extended;
begin
  if Item.Attributes.Exists( 'type') then
    if UpperComp( Item.Attributes[ 'type'], _OP_Extended) then
    begin
      if Item.Attributes.Exists( 'value') then
        result := StrToFloat( Item.Attributes[ 'value'])
      else result := StrToFloat( Item.Value);
    end else
      raise EXMLException.create( 'Fehler beim Laden eines Extended');
end;


procedure Register;
begin
  RegisterComponents( 'XML', [ TXMLParser, TXMLCodec]);
end;

{ TXMLText }

constructor TXMLText.create( AParent: TXMLTag);
begin
  inherited create;
  fText := '';
  if Assigned( AParent) then
    AParent.Items.Add( self);
end;

constructor TXMLText.create( AParent: TXMLTag; AText: string);
begin
  create( AParent);
  fText := AText;
end;

{ TXMLAttributes }

procedure TXMLAttributes.Add(Name, Value: string);
var i : integer;
begin
  // replace " with '
  Value := Replace( Value, '"', '''', true);
  // suchen, ob schon vorhanden
  i := fItems.IndexOfName( Name);
  if i = -1 then
    fItems.Add( Name + '=' + Value + '')
  else // ersetzen
    fItems.Values[ Name] := Value;
end;

procedure TXMLAttributes.Clear;
begin
  fItems.Clear;
end;

function TXMLAttributes.Count: integer;
begin
  result := fItems.Count;
end;

constructor TXMLAttributes.create;
begin
  inherited;
  fItems := TStringList.create;
end;

procedure TXMLAttributes.Delete(Idx: integer);
begin
  fItems.Delete( Idx);
end;

procedure TXMLAttributes.Delete(Name: string);
var i : integer;
begin
  i := fItems.IndexOfName( Name);
  if not( i = 0) then
    fItems.Delete( i);
end;

destructor TXMLAttributes.Destroy;
begin
  fItems.Free;
  inherited;
end;

function TXMLAttributes.GetAttribute(Name: string): string;
begin
  result := fItems.Values[ Name];
end;

function TXMLAttributes.GetName(Idx: integer): string;
begin
  result := fItems.Names[ Idx];
end;

function TXMLAttributes.IndexOf(Name: string): integer;
begin
  result := fItems.IndexOfName( Name);
end;

procedure TXMLAttributes.SetAttribute(Name: string; const Value: string);
var i : integer;
    a : string;
begin
  i := IndexOf( Name);
  if i <> -1 then
  begin
    a := Replace( Value, '"', '''', true);
    fItems.Values[ Name] := a;
  end;
end;

procedure TXMLAttributes.SetName(Idx: integer; const Value: string);
var a : string;
begin
  if Exists( Value) then Exit;
  a := fItems[ Idx];
  a := Value + '=' + SplitStr( a, '=', false, true);
  fItems[ Idx] := a;
end;

procedure TXMLAttributes.SetData(const Text: string);
(*
 * a: working string
 * b: safe
 * c: working
 *)
var a, b, c : string;
    i       : integer;
begin
  Clear;
  a := Text;

  while a <> '' do
  begin
    b := Trim( SplitStr( a, ' ', false, true));
    // a auf text vor erstem ' ' setzen
    a := Trim( SplitStr( a, ' ', true, false));

    // c h�lt value
    c := Trim( SplitStr( a, '=', false, true));
    // a h�lt name
    a := Trim( SplitStr( a, '=', true));

    // umschliessende '"' l�schen
    if Copy( c, 1, 1) = '"' then
      c := Copy( c, 2, Length( c));
    if Copy( c, Length( c), 1) = '"' then
      c := Copy( c, 1, Length( c) -1);

    Add( a, c);

    // Kreis schliessen
    a := b;
  end;
end;

function TXMLAttributes.Exists(Name: string): boolean;
begin
  result := not( IndexOf( Name) = -1);
end;

function TXMLAttributes.GetData: string;
var i : integer;
begin
  result := '';
  for i := 0 to fItems.Count -1 do
    result := result + fItems.Names[i] + '="' + fItems.Values[ fItems.Names[i]] + '" ';// fItems[i] + ' ';
end;

procedure TXMLAttributes.AssignTo(Dest: TXMLTag);
begin
  Dest.fAttributes.fItems.Assign( self.fItems);
end;

{ TXMLItems }

procedure TXMLItems.Add(Value: TXMLBase);
begin
  fItems.Add( Value);
end;

procedure TXMLItems.AddText(Value: string);
begin
  Add( TXMLText.create( nil, Value));
end;

procedure TXMLItems.AssignTo(Dest: TXMLTag);
var
  i : integer;
  p : TXMLBase;
begin
  for i := 0 to self.fItems.Count -1 do
  begin
    p := fItems[i];
    if p is TXMLText then
      Dest.Items.AddText( (p as TXMLText).Text)
    else Dest.Items.Add( (p as TXMLTag).Copy);
  end;
end;

procedure TXMLItems.Clear;
var p : TXMLBase;
begin
  while fItems.Count > 0 do
  begin
    p := TXMLBase( fItems[0]);
    fItems.Delete( 0);
    p.Free;
  end;
end;

function TXMLItems.Count: integer;
begin
  result := fItems.Count;
end;

constructor TXMLItems.create;
begin
  inherited;
  fItems := TList.create;
end;

procedure TXMLItems.Delete(Index: integer);
var p : TXMLBase;
begin
  p := fItems[ Index];
  fItems.Delete( Index);
  p.Free;
end;

destructor TXMLItems.Destroy;
begin
  inherited;
  Clear;
  fItems.Free;
end;

procedure TXMLItems.Exchange(Index1, Index2: integer);
begin
  fItems.Exchange( Index1, Index2);
end;

function TXMLItems.ExistsTag(const Caption: string): boolean;
begin
  result := not( IndexOfTag( Caption) = -1);
end;

function TXMLItems.GetItem(Idx: integer): TXMLBase;
begin
  result := fItems[ Idx];
end;

function TXMLItems.GetTag(Idx: integer): TXMLTag;
var p : TXMLBase;
    i, c : integer;
begin
  c := -1;
  for i := 0 to fItems.Count -1 do
  begin
    p := TXMLBase( fItems[i]);
    if p is TXMLTag then
      Inc( c);
    if c = Idx then
      Break;
  end;
  if c = Idx then result := TXMLTag( fItems[i])
  else result := nil;
end;

function TXMLItems.GetTagByCaption(Caption: string): TXMLTag;
var p : TXMLBase;
    l : TLoop;
    i : integer;
begin
  if fItems.Count = 0 then l := FAILED
  else l := CONTINUE;

  i := 0;

  while l = CONTINUE do
  begin
    p := fItems[i];
    if (p is TXMLTag) then
      if UpperComp( (p as TXMLTag).Caption, Caption) then
        l := SUCCESS;
    if l = CONTINUE then
      Inc( i);
    if i = fItems.Count then
      l := FAILED;
  end;

  if l = SUCCESS then
    result := (p as TXMLTag)
  else result := nil;
end;

function TXMLItems.GetText(Idx: integer): TXMLText;
var p : TXMLBase;
    i, c : integer;
begin
  c := -1;
  for i := 0 to fItems.Count -1 do
  begin
    p := TXMLBase( fItems[i]);
    if p is TXMLText then
      Inc( c);
    if c = Idx then
      Break;
  end;
  if c = Idx then result := TXMLText( fItems[i])
  else result := nil;
end;

function TXMLItems.IndexOf(Value: pointer): integer;
begin
  result := fItems.IndexOf( Value);
end;

function TXMLItems.IndexOfTag(const Caption: string): integer;
var p : TXMLBase;
    l : TLoop;
    i : integer;
begin
  if fItems.Count = 0 then l := FAILED
  else l := CONTINUE;

  i := 0;

  while l = CONTINUE do
  begin
    p := fItems[i];
    if (p is TXMLTag) then
      if UpperComp( (p as TXMLTag).Caption, Caption) then
        l := SUCCESS;
    if l = CONTINUE then
      Inc( i);
    if i = fItems.Count then
      l := FAILED;
  end;

  if l = SUCCESS then
    result := i
  else result := -1;
end;

function TXMLItems.IndexOfText(const Text: string): integer;
var p : TXMLBase;
    l : TLoop;
    i : integer;
begin
  if fItems.Count = 0 then l := FAILED
  else l := CONTINUE;

  i := 0;

  while l = CONTINUE do
  begin
    p := fItems[i];
    if (p is TXMLText) then
      if UpperComp( (p as TXMLText).Text, Text) then
        l := SUCCESS;
    if l = CONTINUE then
      Inc( i);
    if i = fItems.Count then
      l := FAILED;
  end;

  if l = SUCCESS then
    result := i
  else result := -1;
end;

procedure TXMLItems.Insert(Index: integer; Value: TXMLBase);
begin
  fItems.Insert( Index, Value);
end;

procedure TXMLItems.SetItem(Idx: integer; const Value: TXMLBase);
var p : TXMLBase;
begin
  p := GetItem( Idx);
  // free altes Object
  p.Free;
  // set  to new
  fItems[ Idx] := Value;
end;

procedure TXMLItems.SetTag(Idx: integer; const Value: TXMLTag);
var p : TXMLBase;
    i, c : integer;
begin
  c := -1;
  for i := 0 to fItems.Count -1 do
  begin
    p := TXMLBase( fItems[i]);
    if ( p is TXMLTag) then
      Inc( c);
    if c = Idx then
      Break;
  end;
  if c = Idx then
  begin
    p.Free;
    fItems[i] := Value;
  end;
end;

procedure TXMLItems.SetTagByCaption(Caption: string; const Value: TXMLTag);
begin

end;

procedure TXMLItems.SetText(Idx: integer; const Value: TXMLText);
var p : TXMLBase;
    i, c : integer;
begin
  c := -1;
  for i := 0 to fItems.Count -1 do
  begin
    p := TXMLBase( fItems[i]);
    if (p is TXMLText) then
      Inc( c);
    if c = Idx then
      Break;
  end;
  if c = Idx then
  begin
    p.Free;
    fItems[i] := Value;
  end;
end;

function TXMLItems.TagCount: integer;
var p : TXMLBase;
    i, c : integer;
begin
  c := 0;
  for i := 0 to fItems.Count -1 do
  begin
    p := TXMLBase( fItems[i]);
    if ( p is TXMLTag) then
      Inc( c);
  end;
  result := c;
end;

function TXMLItems.TextCount: integer;
var p : TXMLBase;
    i, c : integer;
begin
  c := 0;
  for i := 0 to fItems.Count -1 do
  begin
    p := TXMLBase( fItems[i]);
    if (p is TXMLText) then
      Inc( c);
  end;
  result := c;
end;

procedure TXMLItems.Unbind(Index: integer);
begin
  fItems.Delete( Index);
end;

{ TXMLTag }

constructor TXMLTag.create( AOwner: TXMLTag);
begin
  inherited create;
  fItems := TXMLItems.create;
  fAttributes := TXMLAttributes.create;
  if Assigned( AOwner) then
    AOwner.Items.Add( self);
end;

constructor TXMLTag.create( AOwner: TXMLTag; ACaption: string);
begin
  create( AOwner);
  fCaption := ACaption;
end;

function TXMLTag.Copy: TXMLTag;
begin
  result := TXMLTag.create( nil, fCaption);
  Items.AssignTo( result);
  Attributes.AssignTo( result);
end;

constructor TXMLTag.create(AOwner: TXMLTag; ACaption, AValue: string);
begin
  create( AOwner);
  fCaption := ACaption;
  fItems.AddText( AValue);
end;

destructor TXMLTag.Destroy;
begin
  inherited;
  fItems.Free;
  fAttributes.Free;
end;

function TXMLTag.GetValue: string;
var i : integer;
    b : TXMLBase;
begin
  result := '';
  for i := 0 to fItems.Count -1 do
  begin
    b := fItems[ i];
    if (b is TXMLText) then
      result := result + (b as TXMLText).Text;
  end;
end;


{ ------------------ }


function PosEndTag( Text: string; TagName: string; FromPos: integer = 0) : integer;
var
  OpenCount : integer;
begin
  OpenCount := 1;
  FromPos := 0;
  // correct FromPos (see loop)
//  FromPos := FromPos -1;
  while (OpenCount > 0) and
        (Length( Text) > 0) and
        (FromPos < Length( Text)) do
  begin
    // increase FromPos
    FromPos := FromPos +1;
    // recourse tag opening...
    if UpperComp( '<'+TagName, Copy( Text, FromPos, Length( TagName) +1)) then
      Inc( OpenCount); // increase counter
    // tag closed
    if UpperComp( '</'+TagName, Copy( Text, FromPos, Length( TagName) +2)) then
      Dec( OpenCount); // decrease counter
  end;
  result := FromPos;
end;

{
function ParseXMLText( text : string; root : TXMLTag) : boolean;
var
  Tag       : TXMLTag;

  Caption, Attributes, Value : string;

  a, b, c, d : string;

  i, j     : integer;

  closed  : boolean;
begin
  a := text;
  result := false;

  while Length( a) <> 0 do
  try
    i := Pos( '<', a); // find <??? aaa>
    if i = 0 then
    begin
      // add text
      root.Items.Add( TXMLText.create( a));
      // clear text
      a := '';
    end
    else begin
      // text before tag
      b := Copy( a, 1, i -1);
      if b <> '' then
      begin
        // there is text => add text
        root.Items.Add( TXMLText.create( b));
      end;

      // true if tag is closed by "/>"
      closed := false;

      // copy text from "i" ( i = Pos( '<', a) ;)
      a := Copy( a, i, Length( a));

      // search for tag end
      j := Pos( '>', a);
      // copy tag content
      b := Trim( Copy( a, 2, j -2));
      // hold rest
      a := Trim( Copy( a, j+1, Length( a)));

      // test if tag is closed by "/>"
      if Copy( b, Length( b), 1) = '/' then
      begin
        // cut "/" at the end
        b := Copy( b, 1, Length( b) -1);
        // set flag
        closed := true;
      end;
      // test if tag opens with "!"
      if Copy( b, 1, 1) = '!' then
      begin
        // cut "!" at beginning
        b := Copy( b, 2, Length( b));
        // set flag
        closed := true;
      end;

      // search for caption seperator
      i := Pos( ' ', b);
      if i > 0 then
      begin
        Caption := Trim( Copy( b, 1, i));
        Attributes := Trim( Copy( b, i, Length( b)));
      end else
        Caption := b;

      if closed then
      begin
        // tag is closed
        // create tag
        Tag := TXMLTag.create( Caption);
        // set Attributes
        Tag.Attributes.SetData( Attributes);
        // add to root
        root.Items.Add( p);
      end else
      begin
        // search for closing tag
        i := PosEndTag( a, Caption);
        if i = 0 then
        begin
          // no closing tag found
          ShowMessage( 'Abschlie�ender Tag fehlt! ' + a);
          Exit;
        end;

        // copy text between
        value := Copy( a, 1, i -1);
        // copy rest
        // workaround to find ">"
        a := Copy( a, i, Length( a));
        i := Pos( '>', a);
        a := Copy( a, i +1, Length( a));

        // create new item
        p := TXMLTag.create( Caption);
        // set Attributes
        p.Attributes.SetData( Attributes);

        // add to root
        root.Items.Add( p);

        // parse value-text
        ParseXMLText( Value, p);

      end;
    end;
  except
    result := false;
  end;
  result := true;
end;

function ParseXMLFile( FileName : string; root : TXMLTag) : boolean;
var
  a, b : string;
  f    : TextFile;
  i    : integer;
begin
  result := false;
  AssignFile( f, FileName);
  try
   try
    Reset( f);
    b := '';

    while not EoF( f) do
    begin
      ReadLn( f, a);
      b := b + Trim( a);
    end;

    result := ParseXMLText( b, root);
   finally
    CloseFile( f);
   end;
  except
  end;
end;
}


{ TXMLParser }

constructor TXMLParser.create( AOwner: TComponent);
begin
  inherited;
  fCodec := nil;
end;

function TXMLParser.ParseXMLFile(FileName: string; root: TXMLTag): boolean;
var
  a, b : string;
  f    : TextFile;
  i    : integer;
begin
  result := false;
  AssignFile( f, FileName);
//  try
   try
    Reset( f);
    a := '';
    while (a = '') and
          (not EoF( f)) do
    begin
      ReadLn(f, a);
      if ( Pos( '<?xml', a) = 0 ) then
        a := '';
    end;
    if EoF( f) then
      raise EXMLException.create( Format( '"%s" ist keine g�ltige XML-Datei!', [ FileName]));

    b := '';

    while not EoF( f) do
    begin
      ReadLn( f, a);
      b := b + Trim( a);
    end;

    result := ParseXMLText( b, root);

   finally
    CloseFile( f);
   end;
{  except
  end;}
end;

function TXMLParser.ParseXMLText(const Text: string;
  root: TXMLTag): boolean;
var
  Tag       : TXMLTag;

  Caption, Attributes, Value : string;

  a, b, c, d : string;

  i, j     : integer;

  closed  : boolean;
begin

  a := text;
  a := Replace( a, #10, '', true);
  a := Replace( a, #13, '', true);
  result := false;

  while Length( a) <> 0 do
  try
    i := Pos( '<', a); // find <??? aaa>
    if i = 0 then
    begin
      // decode text
      if Assigned( fCodec) then
        a := fCodec.Decode( a);
      // add text
      TXMLText.create( root, a);
      // clear text
      a := '';
    end
    else begin
      // text before tag
      b := Trim( Copy( a, 1, i -1));
      if b <> '' then
      begin
        // there is text => add text
        // decode text
        if Assigned( fCodec) then
           a := fCodec.Decode( a);
        TXMLText.create( root, b);
      end;

      // true if tag is closed by "/>"
      closed := false;

      // copy text from "i" ( i = Pos( '<', a) ;)
      a := Copy( a, i, Length( a));

      // search for tag end
      j := Pos( '>', a);
      // copy tag content
      b := Trim( Copy( a, 2, j -2));
      // hold rest
      a := Trim( Copy( a, j+1, Length( a)));

      // test if tag is closed by "/>"
      if Copy( b, Length( b), 1) = '/' then
      begin
        // cut "/" at the end
        b := Copy( b, 1, Length( b) -1);
        // set flag
        closed := true;
      end;

      // test if tag opens with "!"
      if Copy( b, 1, 1) = '!' then
      begin
        // cut "!" at beginning
        b := Copy( b, 2, Length( b));
        // set flag
        closed := true;
      end else
        // test if tag opens with "?"
        // <?preprocessor ?>
        if Copy( b, 1, 1) = '?' then
        begin
          // cut "?" at beginning
          b := Copy( b, 2, Length( b));
          // cut "?" at end
          if b[Length( b)] = '?' then
            b := Copy( b, 1, Length( b) -1); 
          // set flag
          closed := true;
        end;

      // search for caption seperator
      i := Pos( ' ', b);
      if i > 0 then
      begin
        Caption := Trim( Copy( b, 1, i));
        Attributes := Trim( Copy( b, i, Length( b)));
      end else
        Caption := b;

      if closed then
      begin
        // tag is closed
        // create tag
        Tag := TXMLTag.create( root, Caption);
        // set Attributes
        Tag.Attributes.SetData( Attributes);
      end else
      begin
        // search for closing tag
        i := PosEndTag( a, Caption);
        {$IFNDEF thaxml_nostrikt}
        if i = 0 then
          // no closing tag found
          raise EXMLException.create( 'Abschlie�ender Tag fehlt! ' + a);
        {$ENDIF}

        // copy text between
        value := Copy( a, 1, i -1);
        // copy rest
        // workaround to find ">"
        a := Copy( a, i, Length( a));
        i := Pos( '>', a);
        if i = 0 then a := '';
        a := Copy( a, i +1, Length( a));

        // create new item
        Tag := TXMLTag.create( root, Caption);
        // set Attributes
        Tag.Attributes.SetData( Attributes);

        // parse value-text
        ParseXMLText( Value, Tag);

      end;
    end;

    result := true;
  except
    result := false;
    raise;
  end;
end;

procedure TXMLParser.SaveToXML(root: TXMLBase; FileName: string; VisibleRoot: boolean=true; standalone: boolean=true);
var s : TStringList;
    IsTag: boolean;
begin
  s := TStringList.create;
  try
    if standalone then
      s.Add( Format( xmlVersionEx, ['1.0', 'standalone="yes"']))
    else s.Add(  xmlVersion1_0);

    IsTag := root is TXMLTag;

    if (not IsTag) then
      SaveToXMLStrings( root, s, 0)
    else SaveToXMLStrings( (root as TXMLTag), s, 0, VisibleRoot);

    s.SaveToFile( FileName);
  finally
    s.Free;
  end;
end;

procedure TXMLParser.SaveToXMLStrings(root: TXMLBase; Dest: TStrings; spacing: integer=0; VisibleRoot: boolean=false);
var _sp : string;
    a, b, c : string;
    i, j : integer;

    base    : TXMLBase;
    Tag, p  : TXMLTag;
    Text    : TXMLText;
begin
  _sp := '';

  for i := 1 to spacing do _sp := _sp + ' ';

  if root is TXMLText then
    Dest.Add( _sp + (root as TXMLText).Text)
  else begin

    Tag := (root as TXMLTag);
    a   := _sp;

    // root element sichern
    if VisibleRoot then
    begin
      // �bergeordneten eintrag erstellen
      p := TXMLTag.create( nil);
      try
        // root als child
        p.Items.Add( root);
        // sichern
        SaveToXMLStrings( p, Dest, spacing, false);
      finally
        // achtung: p.Free gibt alle childs mit frei!
        p.Items.Unbind( 0);
        p.Free;
      end;
      Exit;
    end;

    for i := 0 to Tag.Items.Count -1 do
    begin
      base := Tag.Items[i];
      if base is TXMLText then
      begin
        // encode text
        if Assigned( fCodec) then
          Dest.Add( a + fCodec.Encode( (base as TXMLText).Text))
        else Dest.Add( a + (base as TXMLText).Text);
      end
      else begin
        p := (base as TXMLTag);
        b := '<' + p.Caption + ' ';
        b := Trim( b + p.Attributes.GetData);
        if p.Items.Count = 0 then
        begin
          b := b + ' />';
          Dest.Add( a + b);
        end
        else begin
          b := b + '>';

          // c wird nur verwendet wenn keine Kind-tags vorhanden sind!
          // encode Text
          if Assigned( fCodec) then
            c := b + fCodec.Encode( p.Value) + '</' + p.Caption + '>'
          else c := b + p.Value + '</' + p.Caption + '>';

          if (p.Items.TagCount = 0) and
             (Length( c) < 81) then
            Dest.Add( a + c)
          else begin
            Dest.Add( a + b);
            SaveToXMLStrings( p, Dest, spacing + 2);
            Dest.Add( a + '</' + p.Caption + '>');
          end;
        end;
      end;
    end;
  end;
end;

{ TXMLCodec }

function TXMLCodec.Decode(const Text: string): string;
begin
  result := DecodeHTMLString( Text);
end;

function TXMLCodec.Encode(const Text: string): string;
begin
  result := ValidateStringToHTML( Text);
end;

end.
