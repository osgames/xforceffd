unit thalib;
(****
 *  ThaBaker library
 *  (c) 2001
 *  version 0.5.1

 *  ****

 *  this unit extends the SysUtils unit

 *  ****

 *  the unit is provided "as it is" and you use
 *  it on your own risk. the author is not liable
 *  for any damages that are caused by this code!

 *  ****

**)

interface

USES
    Classes, windows, graphics, SysUtils;

CONST
     // loop consts
     FAILED             = -1;
     SUCCESS            =  0;
     CONTINUE           =  1;

{
 damit ist folgendes BSP m�glich:
 l(* TLoop *) := CONTINUE;
 while Boolean( l) (* oder l = CONTINUE )* do
 ...
}

type
    // loop type
    TLoop = -1..1; //( FAILED, SUCCESS, CONTINUE);

var
  _OSVersionInfo : TOSVersionInfo;
  _AppFolder     : string;

// compares two strings without low-high-case
function UpperComp( str1, str2 : string) : boolean;

function SplitStr( value, seperator : string; before : boolean; ForceEmpty : boolean = false) : string;

function RPos( const Substr, Str : string) : integer;
procedure Switch( var Value1, Value2: integer);

// returns the path without a '\', if existing
function GetPath( Path : string) : string;

// see function names
function BoolTo01( value : boolean) : string;
function BoolToStr( value : boolean) : string;
function StrToBool( value : string) : boolean;

// splits a string to multiple strings
// BSP: value1;value2;value3
procedure SplitStrs( value : string; AssignTo : TStrings; Seperator : string = '"');

// wertet einen string aus, indem Werte in Hochkommas
// als zusammengeh�rig interpretiert werden
// BSP: "FileName1" "FileName2" ...
function BuiltStrs( Values : TStrings) : string;

// ersetzt einen string in einem string durch einen neuen
function Replace( value, what, new: string; all: boolean; NoCase: boolean = true) : string;

// "Write" -�quivalent: values ist typenlos, d.h. alles ist m�glich
function MsgBoxStr( const values : array of const) : string;

// testet Betriebssysteminfo
function IsWin98 : boolean;
function IsWin2000 : boolean;

// gibt ein object frei und setzt obj auf nil
function SafeNil( var obj) : boolean;

function LoadUnicodeText(const FileName: string): WideString;

function ConvertSpecialChar(Key : Char) : String;
function DecodeSpecialChar( value: string) : char;
function ValidateStringToHTML(Html : String) : String;
function DecodeHTMLString( HTML: string) : string;

function PictureToRGN(Picture: TPicture): hRgn;
function GetWinDirectory: string;

implementation

function UpperComp( str1, str2 : string) : boolean;
begin
     result := UpperCase( str1) = UpperCase( str2);
end;

function SplitStr( value, seperator : string; before : boolean; ForceEmpty : boolean = false) : string;
var i : integer;
begin
     i := Pos( seperator, value);
     if i < 1 then
     begin
       if ForceEmpty then
         result := ''
       else result := value;
     end
     else
       if before then
          result := Copy( value, 1, i -1)
       else result := Copy( value, i + Length( seperator), Length( value));
end;

function GetPath( Path : string) : string;
begin
  if Path = '' then
    result := '.'
  else
    if Path[ Length( Path)] = '\' then
      result := Copy( Path, 1, Length( Path) -1)
    else result := Path;
end;

function BoolToStr( value : boolean) : string;
begin
  if value then
     result := 'TRUE'
  else result := 'FALSE';
end;

function BoolTo01( value : boolean) : string;
begin
  if value then
     result := '1'
  else result := '0';
end;

function StrToBool( value : string) : boolean;
begin
  result := UpperComp( value, 'TRUE');
end;

procedure SplitStrs( value : string; AssignTo : TStrings; Seperator : string = '"');
var a, b : string;
begin
  a := Trim( value);
  b := '';
  while (a <> '') and
        (a <> b)   do
  begin
    b := ThaLib.SplitStr( a, Seperator, true, false);
    AssignTo.Add( b);
    a := ThaLib.SplitStr( a, Seperator, false, true);
    a := Trim( a);
  end;
end;

function BuiltStrs( Values : TStrings) : string;
var i : integer;
begin
  if Values.Count > 0 then
    result := Values[0]
  else result := '';
  for i := 1 to Values.Count -1 do
    result := result + '; ' + Values[i];
  result := Trim(result);
end;

function Replace( value, what, new: string; all: boolean; NoCase: boolean = true) : string;
var i : integer;
    a : string;
begin
  a := value;
  if NoCase then
    what := UpperCase( what);

  if NoCase then
    i := Pos( what, UpperCase( a))
  else i := Pos( what, a);
  while i > 0 do
  begin
    System.Delete( a, i, Length( what));
    System.Insert( new, a, i);
    if all then
    begin
      if NoCase then
        i := Pos( what, UpperCase( a))
      else i := Pos( what, a);
    end
    else i := 0;
  end;
  result := a;
end;

function MsgBoxStr( const values : array of const) : string;
var i : integer;
begin
  // copied from help topic "Variante offene Array-Parameter"
  Result := '';
  for i := 0 to High(Values) do
  with Values[i] do
  try
    case VType of
      vtInteger:    Result := Result + IntToStr(VInteger);
      vtBoolean:    if VBoolean then
                      Result := Result + 'TRUE'
                    else Result := result + 'FALSE';  
      vtChar:       Result := Result + VChar;
      vtExtended:   Result := Result + FloatToStr(VExtended^);

      vtString:     Result := Result + VString^;
      vtPChar:      Result := Result + VPChar;
      vtObject:     Result := Result + VObject.ClassName;
      vtClass:      Result := Result + VClass.ClassName;
      vtAnsiString: Result := Result + string(VAnsiString);
      vtCurrency:   Result := Result + CurrToStr(VCurrency^);
      vtVariant:    Result := Result + string(VVariant^);
      vtInt64:      Result := Result + IntToStr(VInt64^);
    end;
  except
  end;
end;

function IsWin98 : boolean;
begin
  result := (_OSVersionInfo.dwPlatformId = VER_PLATFORM_WIN32_WINDOWS) and
            (_OSVersionInfo.dwMajorVersion > 3);
end;

function IsWin2000 : boolean;
begin
  result := (_OSVersionInfo.dwPlatformId = VER_PLATFORM_WIN32_NT) and
            (_OSVersionInfo.dwMajorVersion > 4);
end;

function SafeNil( var obj) : boolean;
begin
  result := false;
  try
    IUnknown( obj) := nil;
    result := true;
  except
  end;
end;

function RPos( const Substr, Str : string) : integer;
var i : integer;
    a: string;
begin
  i := Length( Str) - Length( SubStr)+1;
  while (not UpperComp( SubStr, Copy( Str, i, Length( SubStr)))) and
        (i > -1) do
    Dec( i);
  result := i;
end;

procedure Switch( var Value1, Value2: integer);
var t : integer;
begin
  t := Value1;
  Value1 := Value2;
  Value2 := t;
end;

function LoadUnicodeText(const FileName: string): WideString;
var
  L: TFileStream;
  W: Word;
  S: string;
  I: Integer;
begin
  L := TFileStream.Create(FileName, fmOpenRead or fmShareDenyNone);
  try
    L.Read(W, SizeOf(W));
    case W of
      $FEFF: //UTF-16 little endian
        if L.Size > L.Position then
        begin
          SetLength(Result, (L.Size - L.Position) div 2);
          L.Read(Result[1], L.Size - L.Position);
        end;
      $FFFE: //UTF-16 big endian
        if L.Size > L.Position then
        begin
          SetLength(Result, (L.Size - L.Position) div 2);
          L.Read(Result[1], L.Size - L.Position);
          for I := 1 to Length(Result) do
            Result[I] := WideChar(Swap(Word(Result[I]))); //This can be optimized.
        end;
      else
        begin
          L.Seek(0, soFromBeginning);
          SetLength(S, L.Size);
          if L.Size > 0 then L.Read(S[1], L.Size);
          Result := S;
        end;
    end;
  finally
    L.Free;
  end;
end;

function ConvertSpecialChar(Key : Char) : String;

begin

  case Ord(Key) of

    000..031,

    033, 035..037, 039,

//    039..059,

    091..096,

    123..159 : result := '#'+IntToStr(Ord(key));  



    034 : result := 'quot';

    038 : result := 'amp';

    060 : result := 'lt';

    062 : result := 'gt';

    160 : result := 'nbsp';

    161 : result := 'iexcl';

    162 : result := 'cent';

    163 : result := 'pund';

    164 : result := 'curren';

    165 : result := 'yen';

    166 : result := 'brvbar';

    167 : result := 'sect';

    168 : result := 'uml';

    169 : result := 'copy';

    170 : result := 'ordf';

    171 : result := 'laquo';

    172 : result := 'not';

    173 : result := 'shy';

    174 : result := 'reg';

    175 : result := 'macr';



    176 : result := 'deg';

    177 : result := 'plusmn';

    178 : result := 'sup2';

    179 : result := 'sup3';

    180 : result := 'acute';

    181 : result := 'micro';

    182 : result := 'para';

    183 : result := 'middot';

    184 : result := 'cedil';

    185 : result := 'sup1';

    186 : result := 'ordm';

    187 : result := 'raquo';

    188 : result := 'frac14';

    189 : result := 'frac12';

    190 : result := 'frac43';

    191 : result := 'iquest';

    192 : result := 'Agrave';

    193 : result := 'Aacute';

    194 : result := 'Acirc';

    195 : result := 'Atilde';

    196 : result := 'Auml';

    197 : result := 'Aring';

    198 : result := 'Aelig';

    199 : result := 'Ccedil';

    200 : result := 'Egrave';

    201 : result := 'Eacute';

    202 : result := 'Ecirc';

    203 : result := 'Euml';

    204 : result := 'Igrave';

    205 : result := 'Iacute';

    206 : result := 'Icirc';

    207 : result := 'Iuml';

    208 : result := 'ETH';

    209 : result := 'Ntilde';

    210 : result := 'Ograve';

    211 : result := 'Oacute';

    212 : result := 'Ocirc';

    213 : result := 'Otilde';

    214 : result := 'Ouml';

    215 : result := 'times';

    216 : result := 'Oslash';

    217 : result := 'Ugrave';

    218 : result := 'Uacute';

    219 : result := 'Ucirc';

    220 : result := 'Uuml';

    221 : result := 'Yacute';

    222 : result := 'THORN';

    223 : result := 'szlig';

    224 : result := 'agrave';

    225 : result := 'aacute';

    226 : result := 'acirc';

    227 : result := 'atilde';

    228 : result := 'auml';

    229 : result := 'aring';

    230 : result := 'aelig';

    231 : result := 'ccedil';

    232 : result := 'egrave';

    233 : result := 'eacute';

    234 : result := 'ecirc';

    235 : result := 'euml';

    236 : result := 'igrave';



    237 : result := 'iacute';

    238 : result := 'icirc';

    239 : result := 'iuml';

    240 : result := 'eth';

    241 : result := 'ntilde';

    242 : result := 'ograve';

    243 : result := 'oacute';

    244 : result := 'ocirc';

    245 : result := 'otilde';

    246 : result := 'ouml';

    247 : result := 'divide';

    248 : result := 'oslash';

    249 : result := 'ugrave';

    250 : result := 'uacute';

    251 : result := 'ucirc';

    252 : result := 'uuml';

    253 : result := 'yacute';

    254 : result := 'thorn';

    255 : result := 'yuml';

   else

   begin

     result := key;

     exit;

   end;

 end;

 result := '&'+result+';';

end;

function DecodeSpecialChar( value: string) : char;
begin
  result := #0;
  value := AnsiUpperCase( value);
  if value = '' then Exit;

  if value[1] = '&' then
    value := Copy( value, 2, Length( value));
  if value[Length(value)] = ';' then
    value := Copy( value, 1, Length( value) -1);

  if value[1] = '#' then
  begin
    result := Chr( StrToInt( Copy( value, 2, Length( value))));
    Exit;
  end;

  if value='QUOT' then result := Chr(034);
  if value='AMP' then result := Chr(038);
  if value='LT' then result := Chr(060);
  if value='GT' then result := Chr(062);
  if value='EURO' then result := Chr(128);
  if value='NBSP' then result := Chr(160);
  if value='IEXCL' then result := Chr(161);
  if value='CENT' then result := Chr(162);
  if value='PUND' then result := Chr(163);
  if value='CURREN' then result := Chr(164);
  if value='YEN' then result := Chr(165);
  if value='BRVBAR' then result := Chr(166);
  if value='SECT' then result := Chr(167);
  if value='UML' then result := Chr(168);
  if value='COPY' then result := Chr(169);
  if value='ORDF' then result := Chr(170);
  if value='LAQUO' then result := Chr(171);
  if value='NOT' then result := Chr(172);
  if value='SHY' then result := Chr(173);
  if value='REG' then result := Chr(174);
  if value='MACR' then result := Chr(175);
  if value='DEG' then result := Chr(176);
  if value='PLUSMN' then result := Chr(177);
  if value='SUP2' then result := Chr(178);
  if value='SUP3' then result := Chr(179);
  if value='ACUTE' then result := Chr(180);
  if value='MICRO' then result := Chr(181);
  if value='PARA' then result := Chr(182);
  if value='MIDDOT' then result := Chr(183);
  if value='CEDIL' then result := Chr(184);
  if value='SUP1' then result := Chr(185);
  if value='ORDM' then result := Chr(186);
  if value='RAQUO' then result := Chr(187);
  if value='FRAC14' then result := Chr(188);
  if value='FRAC12' then result := Chr(189);
  if value='FRAC43' then result := Chr(190);
  if value='IQUEST' then result := Chr(191);
  if value='AGRAVE' then result := Chr(192);
  if value='AACUTE' then result := Chr(193);
  if value='ACIRC' then result := Chr(194);
  if value='ATILDE' then result := Chr(195);
  if value='AUML' then result := Chr(196);
  if value='ARING' then result := Chr(197);
  if value='AELIG' then result := Chr(198);
  if value='CCEDIL' then result := Chr(199);
  if value='EGRAVE' then result := Chr(200);
  if value='EACUTE' then result := Chr(201);
  if value='ECIRC' then result := Chr(202);
  if value='EUML' then result := Chr(203);
  if value='IGRAVE' then result := Chr(204);
  if value='IACUTE' then result := Chr(205);
  if value='ICIRC' then result := Chr(206);
  if value='IUML' then result := Chr(207);
  if value='ETH' then result := Chr(208);
  if value='NTILDE' then result := Chr(209);
  if value='OGRAVE' then result := Chr(210);
  if value='OACUTE' then result := Chr(211);
  if value='OCIRC' then result := Chr(212);
  if value='OTILDE' then result := Chr(213);
  if value='OUML' then result := Chr(214);
  if value='TIMES' then result := Chr(215);
  if value='OSLASH' then result := Chr(216);
  if value='UGRAVE' then result := Chr(217);
  if value='UACUTE' then result := Chr(218);
  if value='UCIRC' then result := Chr(219);
  if value='UUML' then result := Chr(220);
  if value='YACUTE' then result := Chr(221);
  if value='THORN' then result := Chr(222);
  if value='SZLIG' then result := Chr(223);
  if value='AGRAVE' then result := Chr(224);
  if value='AACUTE' then result := Chr(225);
  if value='ACIRC' then result := Chr(226);
  if value='ATILDE' then result := Chr(227);
  if value='AUML' then result := Chr(228);
  if value='ARING' then result := Chr(229);
  if value='AELIG' then result := Chr(230);
  if value='CCEDIL' then result := Chr(231);
  if value='EGRAVE' then result := Chr(232);
  if value='EACUTE' then result := Chr(233);
  if value='ECIRC' then result := Chr(234);
  if value='EUML' then result := Chr(235);
  if value='IGRAVE' then result := Chr(236);
  if value='IACUTE' then result := Chr(237);
  if value='ICIRC' then result := Chr(238);
  if value='IUML' then result := Chr(239);
  if value='ETH' then result := Chr(240);
  if value='NTILDE' then result := Chr(241);
  if value='OGRAVE' then result := Chr(242);
  if value='OACUTE' then result := Chr(243);
  if value='OCIRC' then result := Chr(244);
  if value='OTILDE' then result := Chr(245);
  if value='OUML' then result := Chr(246);
  if value='DIVIDE' then result := Chr(247);
  if value='OSLASH' then result := Chr(248);
  if value='UGRAVE' then result := Chr(249);
  if value='UACUTE' then result := Chr(250);
  if value='UCIRC' then result := Chr(251);
  if value='UUML' then result := Chr(252);
  if value='YACUTE' then result := Chr(253);
  if value='THORN' then result := Chr(254);
  if value='YUML' then result := Chr(255);
end;

function ValidateStringToHTML(Html : String) : String;

var i : Integer;

    ch : Char;

begin

  result := '';

  for i := 1 to Length(Html) do

  begin

    result := result+ConvertSpecialChar(Html[i]);

  end;

end;

function DecodeHTMLString( HTML: string) : string;
var
  i : integer;
  a, b : string;
  c    : char;
begin
  i := Pos( '&', HTML);
  a := '';
  while i > 0 do
  begin
    a := a + SplitStr( HTML, '&', true);
    HTML := SplitStr( HTML, '&', false, true);
    b := SplitStr( HTML, ';', true, true);
    HTML := SplitStr( HTML, ';', false, true);

    c := DecodeSpecialChar( b);
    if c <> #0 then a := a + c;

    i := Pos( '&', HTML);
  end;
  result := a;
  if HTML <> '' then result := result + HTML;
end;

function PictureToRGN(Picture: TPicture): hRgn;
var
  B: TBitmap;
  C: Byte;
  R: hRgn;
  P: PByte;
  S,E,Y: Integer;
begin
  B := TBitmap.Create;
  try
    B.HandleType := bmDIB;
    B.PixelFormat := pf24Bit;
    B.Width := Picture.Width;
    B.Height := Picture.Height;
    B.Canvas.Draw(0, 0, Picture.Graphic);
    B.Mask(B.TransparentColor);
    B.PixelFormat := pf8Bit;
    C := PByte(B.Scanline[0])^;
    Result := CreateRectRgn(0, 0, B.Width, B.Height);
    for Y := B.Height-1 downto 0 do
    begin
      P := B.ScanLine[Y];
      S := 0;
      E := 0;
      repeat
        while (P^ = C) and (E < B.Width) do
        begin
          Inc(P);
          Inc(E);
        end;
        R := CreateRectRgn(S, Y, E, Y+1);
        try
          CombineRgn(Result, Result, R, RGN_DIFF);
        finally
          DeleteObject(R);
        end;
        while (P^ <> C) and (E < B.Width) do
        begin
          Inc(P);
          Inc(E);
        end;
        S := E;
      until E >= B.Width;
      if S <> E then
      begin
        R := CreateRectRgn(S, Y, E, Y+1);
        try
          CombineRgn(Result, Result, R, RGN_DIFF);
        finally
          DeleteObject(R);
        end;
      end;
    end;
  finally
    B.Free;
  end;
end;

function GetWinDirectory: string;
var p: array[0..1023] of char;
begin
  Fillchar(p, sizeof(p), #0);
  GetWindowsDirectory(p, 1024);
  Result := string(p);
end;


begin
  _AppFolder := ExtractFileDir( ParamStr( 0));
  FillChar( _OSVersionInfo, SizeOf( _OSVersionInfo), 0);
  _OSVersionInfo.dwOSVersionInfoSize := SizeOf( _OSVersionInfo);
  GetVersionEx( _OSVersionInfo);
end.
