unit AGFUnit;
{
 AGF (Advanced Graphics Format) image handling unit, version 1.1 by Lifepower (lifepower@operamail.com)
 Last updated: 18 Sep 2001

  ---------------------------------------------------------------------------
                    AGFImage unit v1.1 - Turbo Edition
                         Written by Lifepower
                        (lifepower@operamail.com)
  ---------------------------------------------------------------------------
                     *** DISCLAIMER OF WARRANTY ***
 The author disclaims all warranties relating to this product,
whenever expressed or implied, including without any limitation,
any implied warranties of merchantability or fitness for a particular
purpose.

 The author will not be liable for any special, incidental, consequential,
indirect or similar damages due to loss of data, damage of hardware or
any other reason, even if the author was advised of the possibility of
such loss or damage.
 ------------------------------------------------------------------------
 The library and its source code are released as Freeware. You may use it
freely in your non-commercial products and re-distribute the library, as
long as its source and documentation are left intact.
 In order to use this library in a commercial project you are to contact
the author for a special agreement and disclaimer.
 ------------------------------------------------------------------------
 This version of library is released exclusively for:

  Turbo - The source for game development with Borland/Inprise compilers
                    (http://turbo.gamedev.net)
}
interface
{$R-,S-}
Uses DirectDraw, DXDraws, Direct3D,  DXClass, FastDIB, Classes, TraceFile;
//---------------------------------------------------------------------------
// T4Vertex - used with TAGFImage.SelectD3DTexture
//---------------------------------------------------------------------------
type T4Vertex = Array[0..3] of TD3DTLVERTEX;
//---------------------------------------------------------------------------
// T4Mesh - record for PowerDraw library convenience
//---------------------------------------------------------------------------
     T4Mesh = Array[0..3] of record
                              Ang: Double;
                              Rad: Double;
                             end;
//---------------------------------------------------------------------------
// TAGFTextureD3D - texture storage used with Direct3D
//---------------------------------------------------------------------------
     TAGFTextureD3D = class
                       public
                        Texture: Array of TDirect3DTexture2;

                        constructor Create();
                        destructor Destroy;override;
                      end;
//---------------------------------------------------------------------------
// TAGFTexture - texture storage used with software emulation
//---------------------------------------------------------------------------
     TAGFTexture = class
                    public
                     Texture: Array of TFastDIB;

                     constructor Create();
                     destructor Destroy;override;
                    end;
//---------------------------------------------------------------------------
// TAGFImage component
//---------------------------------------------------------------------------
     TAGFImage = class
                  private
                   FDDraw: TDXDraw;
                   PWidth, PHeight, TWidth, THeight, Patterns: Integer;
                   ImgPerW, ImgPerH, ImgPerText: Integer;
                   Textures: Integer;
                   FD3DActive: Boolean;
                   TextureD3D: TAGFTextureD3D;
                   TextureAGF: TAGFTexture;
                   FActive: Boolean;
                   FBits: Integer;
                   fName: String;
                   fPattern: Integer;
                   fSelected: Integer;
                  public
                   // determines if Direct3D is used
                   property D3DActive: Boolean read FD3DActive;

                   // Direct3D texture reference
                   property D3DTexture: TAGFTextureD3D read TextureD3D;

                   // Software emulation texture reference
                   property AGFTexture: TAGFTexture read TextureAGF;

                   // determines if the image has been loaded and initialized
                   property Active: Boolean read FActive;

                   // determines the bit count of the image
                   property BitCount: Integer read FBits;

                   // determines the size of animation pattern in the image
                   property PatternWidth: Integer read PWidth;
                   property PatternHeight: Integer read PHeight;

                   // creates the component. hardware variable determines if Direct3D is to be used
                   constructor Create(DDraw: TDXDraw; Hardware: Boolean);

                   destructor Destroy;override;

                   { loads AGF image from disk
                       error codes:
                          -1  =  Cannot open file
                          -2  =  Not AGF file
                          -3  =  Invalid AGF file version
                          -4  =  Error reading from file
                          -5  =  No textures found in AGF file
                          -6  =  Block read error
                          -7  =  Block extraction error
                          -8  =  Image already loaded }
                   function LoadFromStream(Stream: TStream): Integer;

                   { returns the texture number and position based on pattern (for software renderer)
                       error code:
                          -1  =  No valid AGF image loaded }
                   function SelectAGFTexture(Pattern: Integer; out TexNum, SrcXpos, SrcYpos: Integer): Integer;

                   { sets active the requested texture pattern for Direct3D renderer
                       error code:
                          -1  =  No valid AGF image loaded }
                   function SelectD3DTexture(Pattern: Integer; var Vertex: T4Vertex): Integer;

                   property Name : String read fName write fName;

                   property Pattern : Integer read fPattern;
                 end;
//---------------------------------------------------------------------------
// Creates a mesh coordinates to be used with PowerDraw library
//---------------------------------------------------------------------------
procedure CreateT4Mesh(Height, Width: Integer; var Mesh: T4Mesh);
//---------------------------------------------------------------------------
implementation
Uses ZLib, Dialogs, Forms, SYSUtils, Graphics;

//---------------------------------------------------------------------------
constructor TAGFTextureD3D.Create();
begin
 inherited;

 SetLength(Texture, 0);
end;

//---------------------------------------------------------------------------
constructor TAGFTexture.Create();
begin
 inherited;

 SetLength(Texture, 0);
end;

//---------------------------------------------------------------------------
constructor TAGFImage.Create(DDraw: TDXDraw; Hardware: Boolean);
begin
 inherited Create;

 FDDraw:= DDraw;

 FD3DActive:= Hardware;

 if (FD3DActive) then
  begin
   TextureD3D:= TAGFTextureD3D.Create();
   TextureAGF:= nil;
  end else
  begin
   TextureAGF:= TAGFTexture.Create();
   TextureD3D:= nil;
  end;

 FBits:= FDDraw.Surface.SurfaceDesc.ddpfPixelFormat.dwRGBBitCount;

 FActive:= False;

 Patterns:= 0;
 Textures:= 0;
 fSelected:=-1;
end;

//---------------------------------------------------------------------------
destructor TAGFImage.Destroy;
begin
  if TextureD3D<>nil then
    TextureD3D.Free;

  if TextureAGF<>nil then
    TextureAGF.Free;
  inherited;
end;

function TAGFImage.LoadFromStream(Stream: TStream): Integer;
type TPakID = Array[0..2] of Char;
Const AGFID: TPakID = ('A', 'G', 'F');
      AGFVersion: Byte = $11;
Var
//  InF: File;
  InBuf: Array[0..65535] of Byte;
  OutBuf: Pointer;
  InSize, OutSize, I, J, IWidth, IHeight, NBlocks: Integer;
  ID: TPakID;
  Ver: Byte;
  FDIB: TFastDIB;
  FPic: TBitmap;
  P: Pointer;
begin
  if (FActive) then
  begin
    Result:= -8; // already loaded
    Exit;
  end;

  Stream.Read(ID,3);
  if (ID <> AGFID) then
  begin
    Result:= -2;// not an AGF file
    Exit;
  end;
  Stream.Read(Ver,1);
  if (Ver <> AGFVersion) then
  begin
    Result:= -3;// invalid file version
    Exit;
  end;

 Stream.Read(PWidth,2);
 Stream.Read(PHeight,2);
 Stream.Read(TWidth,2);
 Stream.Read(THeight,2);
 Stream.Read(Patterns,4);

 fPattern:=Patterns;
 ImgPerW:= TWidth div PWidth;
 ImgPerH:= THeight div PHeight;
 ImgPerText:= ImgPerW * ImgPerH;
 Textures:= Patterns div ImgPerText;
 if (Patterns mod ImgPerText <> 0) then Inc(Textures);
 if (Textures = 0) then
 begin
   Result:= -5;// no textures found in AGF file
   Exit;
 end;

 if (FD3DActive) then SetLength(TextureD3D.Texture, Textures) else SetLength(TextureAGF.Texture, Textures);

 for I:=0 to Textures - 1 do
 begin
   IWidth:= 0; IHeight:= 0;
   Stream.Read(IWidth,2);
   Stream.Read(IHeight,2);

   FDIB:= TFastDIB.Create();
   FDIB.SetSize(IWidth, IHeight, 32);
   P:= Pointer(FDIB.Bits);

   NBlocks:= 0;
   Stream.Read(NBlocks,2);
   for J:=1 to NBlocks do
   begin
     InSize:= 0;
     Stream.Read(InSize,2);
     Stream.Read(InBuf,InSize);
//     Stream.Read(InBuf,InSize);

     DeCompressBuf(@InBuf, InSize, 0, OutBuf, OutSize);
     Move(OutBuf^, P^, OutSize);
     Inc(Integer(P), OutSize);
     FreeMem(OutBuf);
   end;

   if (FD3DActive) then
    begin
     FPic:= TBitmap.Create();
     FPic.PixelFormat:= pf32bit;
     FPic.Width:= IWidth;
     FPic.Height:= IHeight;
     FDIB.Draw(FPic.Canvas.Handle, 0, 0);

     TextureD3D.Texture[I]:= TDirect3DTexture2.Create(FDDraw, FPic, False);
     FPic.Free();
    end else
    begin
     TextureAGF.Texture[I]:= TFastDIB.Create();
     Case FDDraw.Surface.BitCount of
      16: TextureAGF.Texture[I].SetSizeEx(IWidth, -IHeight, 16, 5, 6, 5);
      32: TextureAGF.Texture[I].SetSize(IWidth, -IHeight, 32);
      else begin
            Result:= -6; // invalid destination bit count
            Exit;
           end;
     end;

     FDIB.Draw(TextureAGF.Texture[I].hdc, 0, 0);
    end;
   FDIB.Free();
  end;

 Result:= 0;
 FActive:= True;
end;

//---------------------------------------------------------------------------
function TAGFImage.SelectAGFTexture(Pattern: Integer; out TexNum, SrcXpos, SrcYpos: Integer): Integer;
begin
 if (not FActive)or(Patterns = 0)or(Textures = 0)or(FD3DActive) then
  begin
   Result:= -1;// No valid AGF image loaded
   Exit;
  end;
 if (Pattern < 0) then Pattern:= 0;
 if (Pattern > Patterns - 1) then Pattern:= Patterns - 1;

 TexNum:= Pattern div ImgPerText;
 Pattern:= Pattern mod ImgPerText;

 SrcXpos:= (Pattern mod ImgPerW) * PWidth;
 SrcYpos:= (Pattern div ImgPerW) * PHeight;

 Result:= 0;
end;

//---------------------------------------------------------------------------
function TAGFImage.SelectD3DTexture(Pattern: Integer; var Vertex: T4Vertex): Integer;
Var Text, SrcX, SrcY: Integer;
    FloatX1, FloatY1, FloatX2, FloatY2: Double;
begin
 if (not FActive)or(Patterns = 0)or(Textures = 0)or(not FD3DActive) then
  begin
   Result:= -1;// No valid AGF image loaded
   Exit;
  end;
 if (Pattern < 0) then Pattern:= 0;
 if (Pattern > Patterns - 1) then Pattern:= Patterns - 1;

 Text:= Pattern div ImgPerText;
 if fSelected<>Pattern then
 begin
   FDDraw.D3DDevice7.SetTexture(0, TextureD3D.Texture[Text].Surface.IDDSurface7);
   fSelected:=Pattern;
 end;

 Pattern:= Pattern mod ImgPerText;
 SrcX:= ((Pattern mod ImgPerW) * PWidth) + 1;
 SrcY:= ((Pattern div ImgPerW) * PHeight) + 1;
 FloatX1:= SrcX / TextureD3D.Texture[Text].Surface.Width;
 FloatY1:= SrcY / TextureD3D.Texture[Text].Surface.Height;
 FloatX2:= (SrcX + PWidth - 1) / TextureD3D.Texture[Text].Surface.Width;
 FloatY2:= (SrcY + PHeight - 1) / TextureD3D.Texture[Text].Surface.Height;

 Vertex[0].tu:= FloatX1;
 Vertex[0].tv:= FloatY1;
 Vertex[1].tu:= FloatX2;
 Vertex[1].tv:= FloatY1;
 Vertex[2].tu:= FloatX1;
 Vertex[2].tv:= FloatY2;
 Vertex[3].tu:= FloatX2;
 Vertex[3].tv:= FloatY2;
 Result:= 0;
end;

//---------------------------------------------------------------------------
procedure CreateT4Mesh(Height, Width: Integer; var Mesh: T4Mesh);
procedure GetAngle(X1, Y1, X2, Y2: Integer; var Angle: Double);
begin
 if (X1 = X2) then
  begin
   if (Y1 > Y2) then Angle:= 0 else Angle:= Pi;
   Exit;
  end;
 Angle:= ArcTan((Y2 - Y1) / (X2 - X1));
 Angle:= Pi - Angle;
 if (Y2 > Y1) then Angle:= Angle + Pi;
 if (Y2 = Y1) then
  if (X1 < X2) then Angle:= Pi else Angle:= 0;

 while (Angle < 0) do Angle:= Angle + (2 * Pi);
 while (Angle > 2 * Pi) do Angle:= Angle - (2 * Pi);
end;
Var Delta, Ang: Double;
    CentX, CentY: Integer;
begin
 CentX:= Width div 2;
 CentY:= Height div 2;
 Delta:= Sqrt(Sqr(CentX) + Sqr(CentY));
 GetAngle(CentX, CentY, 0, 0, Ang);
 Mesh[0].Ang:= Ang;
 Mesh[0].Rad:= Delta;
 Delta:=Sqrt(Sqr(CentX - Width) + Sqr(CentY));
 GetAngle(CentX, CentY, Width, 0, Ang);
 Mesh[1].Ang:= Ang;
 Mesh[1].Rad:= Delta;
 Delta:=Sqrt(Sqr(CentX) + Sqr(CentY - Height));
 GetAngle(CentX, CentY, 0, Height, Ang);
 Mesh[2].Ang:= Ang;
 Mesh[2].Rad:= Delta;
 Delta:=Sqrt(Sqr(CentX - Width) + Sqr(CentY - Height));
 GetAngle(CentX, CentY, Width, Height, Ang);
 Mesh[3].Ang:= Ang;
 Mesh[3].Rad:= Delta;
end;

//---------------------------------------------------------------------------
destructor TAGFTextureD3D.Destroy;
var
  Dummy: Integer;
begin
  for DUmmy:=0 to High(Texture) do
    Texture[Dummy].Free;
  SetLength(Texture,0);
  inherited;
end;

destructor TAGFTexture.Destroy;
var
  Dummy: Integer;
begin
  for Dummy:=0 to High(Texture) do
    Texture[Dummy].Free;
  SetLength(Texture,0);
  inherited;
end;

end.
