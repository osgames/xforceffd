PowerDraw Tools v1.4 - Turbo Edition
---------------------------------------------------------
Written by Lifepower
E-mail: lifepower@operamail.com

This package contains the following:

1) PowerDraw v1.4 component featuring:

 a) Hardware acceleration for 2D effects using Direct3D
 b) Software rendering for the same 2D effects (some
   are MMX accelerated, thanks to Gordon Cowie)
 c) Routines like RenderAdd, RenderMul, Rotate and others.

2) AGFUnit v1.1 component which is used with PowerDraw

3) FastTile v1.2 program which can tile several images
  into one large bitmap and animate such bitmaps

4) AGF2Bmp v1.1 program which converts normal bitmaps
  into AGF image files, to be used with PowerDraw

5) Examples with sources
----------------------------------------------------------
 This package is quick written and still has some issues.
If someone encounters any bugs or adds any new routines,
please contact the author.
