unit PowerDraw;
{
 PowerDraw library, version 1.4 by Lifepower (lifepower@operamail.com)
 Last updated: 18 Sep 2001
 
  ---------------------------------------------------------------------------
                       PowerDraw v1.4 - Turbo Edition
                         Written by Lifepower
                        (lifepower@operamail.com)
  ---------------------------------------------------------------------------
                     *** DISCLAIMER OF WARRANTY ***
 The author disclaims all warranties relating to this product,
whenever expressed or implied, including without any limitation,
any implied warranties of merchantability or fitness for a particular
purpose.

 The author will not be liable for any special, incidental, consequential,
indirect or similar damages due to loss of data, damage of hardware or
any other reason, even if the author was advised of the possibility of
such loss or damage.
 ------------------------------------------------------------------------
 The library and its source code are released as Freeware. You may use it
freely in your non-commercial products and re-distribute the library, as
long as its source and documentation are left intact.
 In order to use this library in a commercial project you are to contact
the author for a special agreement and disclaimer.
 ------------------------------------------------------------------------
 This version of library is released exclusively for:

  Turbo - The source for game development with Borland/Inprise compilers
                    (http://turbo.gamedev.net)
}
interface
Uses DirectDraw, Direct3D, DXDraws, DXClass, AGFUnit, Classes, Windows, SYSUtils, FastDIB;
//---------------------------------------------------------------------------
// constans used for TPowerD3D.SetTextureFilter
//---------------------------------------------------------------------------
const tfNone = 0;
      tfLinear = 1;
//---------------------------------------------------------------------------
// T4MeshEx - used for internal mesh caching
//---------------------------------------------------------------------------
type T4MeshEx = record
                 Mesh: T4Mesh;
                 Width, Height: Integer;
                end;
//---------------------------------------------------------------------------
// TPowerD3D component version 1.1
//---------------------------------------------------------------------------
     TPowerD3D = class
                  private
                   FDDraw: TDXDraw;
                   Vertex: T4Vertex;
                   SinTable,
                   CosTable: Array[0..1023] of Integer;
                   Meshes: Array[0..2047] of T4MeshEx;
                   MaxMesh: Integer;

                   function FindMesh(Width, Height: Integer): Integer;
                  public
                   // returns a number of total meshes pre-cached
                   property TotMeshes: Integer read MaxMesh;

                   // creates the component
                   constructor Create(DDraw: TDXDraw);

                   // clears mesh cache
                   procedure ClearMeshes();

                   // use BeginScene() before starting rendering
                   procedure BeginScene(Surface: TDirectDrawSurface = nil);

                   // use EndScene() after all images have been rendered
                   procedure EndScene();

                   // sets the texture antialiasing filter
                   procedure SetTextureFilter(Filter: Integer);

                   // texture mapping routines (with no special effects appied)
                   procedure TextureMap(Image: TAGFImage; X1, Y1, X2, Y2, X3, Y3, X4, Y4, Pattern: Integer);overload;
                   procedure TextureMap(Image: TAGFImage; X1, Y1, X2, Y2, X3, Y3, X4, Y4, Pattern, L1, L2, L3, L4: Integer);overload;

                   // texture mapping routines with Additive blending
                   procedure TextureAdd(Image: TAGFImage; X1, Y1, X2, Y2, X3, Y3, X4, Y4, Pattern: Integer);overload;
                   procedure TextureAdd(Image: TAGFImage; X1, Y1, X2, Y2, X3, Y3, X4, Y4, Pattern, L1, L2, L3, L4: Integer);overload;

                   // rotation routines (mesh is required and no special effects apply)
                   procedure RotateMesh(Image: TAGFImage; Xp, Yp, Rx, Ry: Integer; Mesh: T4Mesh; Pattern: Integer; Angle: Double);overload;
                   procedure RotateMesh(Image: TAGFImage; Xp, Yp, Rx, Ry: Integer; Mesh: T4Mesh; Pattern, L1, L2, L3, L4: Integer; Angle: Double);overload;

                   // rotation routines (mesh is required and additive blend used)
                   procedure RotateMeshAdd(Image: TAGFImage; Xp, Yp, Rx, Ry: Integer; Mesh: T4Mesh; Pattern: Integer; Angle: Double);overload;
                   procedure RotateMeshAdd(Image: TAGFImage; Xp, Yp, Rx, Ry: Integer; Mesh: T4Mesh; Pattern, L1, L2, L3, L4: Integer; Angle: Double);overload;

                   // rotation routine (mesh is required, additive blend used and all lights are set with COLOR)
                   procedure RotateMeshAdd_cl(Image: TAGFImage; Xp, Yp, Rx, Ry: Integer; Mesh: T4Mesh; Pattern, C1, C2, C3, C4: Integer; Angle: Double);

                   // texture mapping routines that multiply the destination screen with the texture
                   procedure TextureMul(Image: TAGFImage; X1, Y1, X2, Y2, X3, Y3, X4, Y4, Pattern: Integer);overload;
                   procedure TextureMul(Image: TAGFImage; X1, Y1, X2, Y2, X3, Y3, X4, Y4, Pattern, L1, L2, L3, L4: Integer);overload;

                   // rotation routine (the mesh is generated automatically and pre-cached)
                   procedure Rotate(Image: TAGFImage; Xp, Yp, RadiusX, RadiusY, Pattern: Integer; Angle: Double);

                   // rotation additive routines
                   procedure RotateAdd(Image: TAGFImage; Xp, Yp, Pattern: Integer; Angle: Double; Scale: Integer);overload;
                   procedure RotateAdd(Image: TAGFImage; Xp, Yp, Pattern, C1, C2, C3, C4: Integer; Angle: Double; Scale: Integer);overload;
                  end;
//---------------------------------------------------------------------------
// TPowerGraph component version 1.2
//---------------------------------------------------------------------------
     TPowerGraph = class
                    private
                     FDDraw: TDXDraw;
                     FPowerD3D: TPowerD3D;
                     FClipRect: TRect;
                     FSurface: IDirectDrawSurface4;
                     FSurfaceDesc: TDDSurfaceDesc2;
                     LockRect: TRect;
                     BitCount: Integer;
                     FD3DActive: Boolean;
                     FAntialias: Boolean;

                     procedure PutPixel16(X, Y: Integer; Color: Word);
                     function GetPixel16(X, Y: Integer): Integer;
                     procedure PutPixel32(X, Y: Integer; Color: Integer);
                     function GetPixel32(X, Y: Integer): Integer;
                     procedure AddPixel16(X, Y, Color: Integer);
                     procedure AddPixel32(X, Y, Color: Integer);
                     procedure BlendPixel16(X, Y, Color, Opacity: Integer);
                     procedure BlendPixel32(X, Y, Color, Opacity: Integer);

                     function ClipCoords(var SrcX, SrcY, DstX, DstY, Width, Height: Integer): Boolean;
                     procedure SetAntialias(Value: Boolean);

                    public
                     // determines the clipping rectangle. does not work with Direct3D
                     property ClipRect : TRect read FClipRect write FClipRect;

                     // references to TPowerD3D component
                     property PowerD3D : TPowerD3D read FPowerD3D;

                     // determines if Direct3D is used
                     property D3DActive : Boolean read FD3DActive;

                     // determines if antialiasing is used for rotate effects
                     property AntiAliasing : Boolean read FAntialias write SetAntialias;

                     // creates the component. hardware determines if Direct3D is to be used
                     constructor Create(DDraw: TDXDraw; Hardware: Boolean);

                     // used for software rendering routines.
                     // !!! USE FOR ANY RENDERING ROUTINE in Software Emulation mode
                     function Lock(Surface:TDirectDrawSurface):Boolean;
                     procedure Unlock();

                     // Pixel rendering routines. Need surface to be locked
                     procedure PutPixel(X, Y, Color: Integer);
                     function GetPixel(X, Y: Integer): Integer;
                     procedure AddPixel(X, Y, Color: Integer);
                     procedure BlendPixel(X, Y, Color, Opacity: Integer);

                     procedure Line(X1, Y1, X2, Y2, Color: Integer);
                     procedure BlendLine(X1, Y1, X2, Y2, Color, Opacity: Integer);
                     procedure WuLine(X1, Y1, X2, Y2, Color, Opacity: Integer);

                     procedure Circle(Xpos, Ypos, Radius, Vertices, Color, Opacity: Integer);

                     { Generic rendering routines (use AGF unit):
                        1) In Software Emulation mode they need DDraw surface to be locked first (use Lock)
                        2) In Hardware mode using Direct3D you need to call PowerD3D.BeginScene() and PowerD3D.EndScene

                       The routines get hardware acceleration when Hardware flag is set (upon initialization)
                       and Direct3D-supporting video card is present.

                       In software mode these routines get EXTREME speed, when the destination surface is
                       located in System Memory.

                       To manually use D3D or AGF functions, make calls to PowerD3D property of
                       through PowerSEUnit.
                     }

                     // additive render routine
                     procedure RenderAdd(Image: TAGFImage; Xpos, Ypos, Pattern: Integer);

                     // multiply render routine
                     procedure RenderMul(Image: TAGFImage; Xpos, Ypos, Pattern: Integer);

                     { additive rotation routines
                         limitations:
                           1) The quality of the output is dependent on Antialiasing method
                           2) In software mode the method is rather slow, compared to RenderAdd and RenderMul }
                     procedure RotateAdd(Image: TAGFImage; Xpos, Ypos, Pattern, Angle, Scale: Integer); overload;
                     procedure RotateAdd(Image: TAGFImage; Xpos, Ypos, Xrad, Yrad, Pattern, Angle, AngleEx, Scale: Integer); overload;

                     { additive custom rotation routine - before the image is rendered
                       it's multiplied with the given color
                         same limitations apply as of RotateAdd }
                     procedure RotateAddCol(Image: TAGFImage; Xpos, Ypos, Pattern, Angle, Scale, Color: Integer);
                   end;
//---------------------------------------------------------------------------
// 24 (888 mask) <-> 16 (565 mask) bit conversion routines
//---------------------------------------------------------------------------
function Conv24to16(Color: Integer): Word;
function Conv16to24(Color: Word): Integer;
implementation
uses PowerSEUnit, Dialogs;

//---------------------------------------------------------------------------
function Conv24to16(Color: Integer): Word; register;
asm
 mov ecx, eax

 shl eax, 24
 shr eax, 27
 shl eax, 11

 mov edx, ecx

 shl edx, 16
 shr edx, 26
 shl edx, 5

 or eax, edx

 mov edx, ecx

 shl edx, 8
 shr edx, 27

 or eax, edx
end;

//---------------------------------------------------------------------------
function Conv16to24(Color: Word): Integer; register;
asm
 xor edx, edx
 mov dx, ax

 mov eax, edx
 shl eax, 27
 shr eax, 8

 mov ecx, edx
 shr ecx, 5
 shl ecx, 26
 shr ecx, 16
 or eax, ecx

 mov ecx, edx
 shr ecx, 11
 shl ecx, 27
 shr ecx, 24
 or eax, ecx
end;

//---------------------------------------------------------------------------
constructor TPowerGraph.Create(DDraw: TDXDraw; Hardware: Boolean);
begin
 inherited Create;

 FDDraw:= DDraw;
 FD3DActive:= Hardware;

 if (FD3DActive) then
  begin
   FPowerD3D:= TPowerD3D.Create(DDraw);
  end else
  begin
   FPowerD3D:= nil;
  end;

 AntiAliasing:= True;
end;

//---------------------------------------------------------------------------
function TPowerGraph.Lock(Surface: TDirectDrawSurface): Boolean;
var
  Error: HRESULT;
begin
 Result:= True;

 BitCount:= Surface.SurfaceDesc.ddpfPixelFormat.dwRGBBitCount;
 if (BitCount <> 16)and(BitCount <> 32) then
  begin
   Result:= False;
   Exit;
  end;

 FSurface:= Surface.ISurface4;
 FSurfaceDesc.dwSize:= SizeOf(TDDSurfaceDesc2);
 LockRect:= Rect(0, 0, Surface.Width, Surface.Height);

 FClipRect.Left:= 0;
 FClipRect.Top:= 0;
 FClipRect.Right:= Surface.Width - 1;
 FClipRect.Bottom:= Surface.Height - 1;

 Error:=FSurface.Lock(@LockRect, FSurfaceDesc, DDLOCK_SURFACEMEMORYPTR or DDLOCK_DONOTWAIT, 0);
 if Error <> DD_OK then
   raise Exception.Create(DDErrorString(Error));
end;

//---------------------------------------------------------------------------
procedure TPowerGraph.Unlock();
begin
 FSurface.Unlock(@LockRect);
 FSurface:= nil;
end;

//---------------------------------------------------------------------------
procedure TPowerGraph.PutPixel(X, Y: Integer; Color: Integer);
begin
 if (X < ClipRect.Left)or(Y < ClipRect.Top)or(X > ClipRect.Right - 1)or(Y > ClipRect.Bottom - 1) then Exit;

 Case BitCount of
  16: PutPixel16(X, Y, Conv24to16(Color));
  32: PutPixel32(X, Y, Color);
 end;
end;

//---------------------------------------------------------------------------
function TPowerGraph.GetPixel(X,Y:Integer):Integer;
begin
 if (X < ClipRect.Left)or(Y < ClipRect.Top)or(X > ClipRect.Right - 1)or(Y > ClipRect.Bottom - 1) then
  begin
   Result:= $000000;
   Exit;
  end;
 Case BitCount of
  16:Result:= Conv16to24(GetPixel16(X, Y));
  32:Result:= GetPixel32(X, Y);
  else Result:= 0;
 end;
end;

//---------------------------------------------------------------------------
procedure TPowerGraph.PutPixel16(X, Y: Integer; Color: Word);
begin
 PWord(Integer(FSurfaceDesc.lpSurface) + (FSurfaceDesc.lPitch * Y) + (X shl 1))^:= Color;
end;

//---------------------------------------------------------------------------
procedure TPowerGraph.PutPixel32(X, Y: Integer; Color: Integer);
Var Offs: Integer;
begin
 Offs:= Integer(FSurfaceDesc.lpSurface) + (FSurfaceDesc.lPitch * Y) + (X shl 2);
 asm
  push ebx
  mov ecx, Offs
  mov eax, Color
  mov ebx, eax
  shr ebx, 16
  and ebx, 0FFh
  xchg al, ah
  shl eax, 8
  and eax, 0FFFF00h
  or eax, ebx
  mov [ecx], eax
  pop ebx
 end;
end;

//---------------------------------------------------------------------------
function TPowerGraph.GetPixel16(X, Y: Integer): Integer;
begin
 Result:= PWord(Integer(FSurfaceDesc.lpSurface) + (FSurfaceDesc.lPitch * Y) + (X shl 1))^;
end;

//---------------------------------------------------------------------------
function TPowerGraph.GetPixel32(X, Y: Integer): Integer;
begin
 Result:= PDWord(Integer(FSurfaceDesc.lpSurface) + (FSurfaceDesc.lPitch * Y) + (X shl 2))^;
end;

//---------------------------------------------------------------------------
procedure TPowerGraph.AddPixel(X, Y, Color: Integer);
begin
 if (X < ClipRect.Left)or(Y < ClipRect.Top)or(X > ClipRect.Right - 1)or(Y > ClipRect.Bottom - 1) then Exit;
 Case BitCount of
  16: AddPixel16(X, Y, Conv24to16(Color));
  32: AddPixel32(X, Y, Color);
 end;
end;

//---------------------------------------------------------------------------
procedure TPowerGraph.AddPixel16(X, Y, Color: Integer);
Var Offs:Integer;
begin
 Offs:= Integer(FSurfaceDesc.lpSurface) + (FSurfaceDesc.lPitch * Y) + (X shl 1);
 asm
  push edx
  push ebx
  push esi
  push edi

  mov esi, Offs
  xor edx, edx
  mov dx, [esi]
  mov edi, Color
  and edi, 0FFFFh
  xor ecx, ecx
  mov eax, edx
  mov ebx, edi
  and eax, 01Fh
  and ebx, 01Fh
  add eax, ebx
  cmp eax, 01Fh
  jbe @Skip1
  mov eax, 01Fh
 @Skip1:
  mov ecx,eax

  mov eax, edx
  mov ebx, edi
  shr eax, 5
  shr ebx, 5
  and eax, 03Fh
  and ebx, 03Fh
  add eax, ebx
  cmp eax, 03Fh
  jbe @Skip2
  mov eax, 03Fh
 @Skip2:
  shl eax, 5
  add ecx, eax

  mov eax, edx
  mov ebx, edi
  shr eax, 11
  shr ebx, 11
  and eax, 01Fh
  and ebx, 01Fh
  add eax, ebx
  cmp eax, 01Fh
  jbe @Skip3
  mov eax, 01Fh
 @Skip3:
  shl eax, 11
  add ecx, eax

  mov [esi], cx

  pop edi
  pop esi
  pop ebx
  pop edx
 end;
end;

//---------------------------------------------------------------------------
procedure TPowerGraph.AddPixel32(X, Y, Color: Integer);
Var Offs: Integer;
begin
 Offs:= Integer(FSurfaceDesc.lPSurface) + FSurfaceDesc.lPitch * Y + (X shl 2);
 asm
  push ebx
  push edx
  push esi
  push edi
  mov edi, Offs

  mov ebx, [edi]
  mov edx, ebx
  mov esi, Color
  mov eax, esi
  shr eax, 16
  and eax, 000000FFh
  and ebx, 000000FFh
  add eax, ebx
  cmp eax, 000000FFh
  jbe @Skip1
  mov eax, 000000FFh
 @Skip1:
  mov ecx, eax

  mov ebx, [edi]
  mov eax, esi
  shr eax, 8
  shr ebx, 8
  and eax, 000000FFh
  and ebx, 000000FFh
  add eax, ebx
  cmp eax, 000000FFh
  jbe @Skip2
  mov eax, 000000FFh
 @Skip2:
  shl eax, 8
  or ecx, eax

  mov ebx, [edi]
  mov eax, esi
{  shr eax, 16}
  shr ebx, 16
  and eax, 000000FFh
  and ebx, 000000FFh
  add eax, ebx
  cmp eax, 000000FFh
  jbe @Skip3
  mov eax, 000000FFh
 @Skip3:
  shl eax, 16
  or ecx, eax

  mov [edi], ecx

  pop edi
  pop esi
  pop edx
  pop ebx
 end;
end;

//---------------------------------------------------------------------------
procedure TPowerGraph.BlendPixel(X, Y, Color, Opacity: Integer);
begin
 if (X < ClipRect.Left)or(Y < ClipRect.Top)or(X > ClipRect.Right - 1)or(Y > ClipRect.Bottom - 1) then Exit;
 Case BitCount of
  16: BlendPixel16(X, Y, Color , Opacity);
  32: BlendPixel32(X, Y, Color, Opacity);
 end;
end;

//---------------------------------------------------------------------------
procedure TPowerGraph.BlendPixel16(X, Y, Color, Opacity: Integer);
Var Offs, _esp:Integer;
begin
 Offs:= Integer(FSurfaceDesc.lpSurface) + (FSurfaceDesc.lPitch * Y) + (X shl 1);
 asm
  push edx
  push ebx
  push esi
  push edi
  mov _esp, esp

  mov esp, Offs        // ESP - offset
  mov ecx, Opacity
  mov edx, 0FFh
  sub edx, ecx         // EDX - dest multiplier
  xor eax, eax
  mov ax, [esp]
  mov esi, eax         // ESI - second color
  xor edi, edi         // EDI - dest color

  mov eax, Color
  mov ebx, esi
  and eax, 01Fh
  and ebx, 01Fh
  imul eax, ecx
  imul ebx, edx
  shr eax, 8
  shr ebx, 8
  add eax, ebx
  or edi, eax

  mov eax, Color
  mov ebx, esi
  shr eax, 5
  shr ebx, 5
  and eax, 03Fh
  and ebx, 03Fh
  imul eax, ecx
  imul ebx, edx
  shr eax, 8
  shr ebx, 8
  add eax, ebx
  shl eax, 5
  or edi, eax

  mov eax, Color
  mov ebx, esi
  shr eax, 11
  shr ebx, 11
  and eax, 01Fh
  and ebx, 01Fh
  imul eax, ecx
  imul ebx, edx
  shr eax, 8
  shr ebx, 8
  add eax, ebx
  shl eax, 11
  or eax, edi

  mov [esp], ax

  mov esp, _esp
  pop edi
  pop esi
  pop ebx
  pop edx
 end;
end;

//---------------------------------------------------------------------------
procedure TPowerGraph.BlendPixel32(X, Y, Color, Opacity: Integer);
Var Offs, _esp: Integer;
begin
 Offs:= Integer(FSurfaceDesc.lpSurface) + (FSurfaceDesc.lpitch * Y) + (X shl 2);
 Opacity:= Opacity and $FF;
 asm
  push ebx
  push edx
  push esi
  push edi
  mov _esp, esp

  mov esp, Offs            // ESP - destination offset
  mov ecx, Opacity         // ECX - opacity
  mov eax, 0FFh
  sub eax, ecx
  mov edx, eax             // EAX - dest opacity
  mov eax, [esp]
  mov esi, eax             // ESI - source color
  xor edi, edi             // EDI - dest color

  mov eax, esi
  mov ebx, Color
 { shr eax, 16}
  shr ebx, 16
  and eax, 0FFh
  and ebx, 0FFh
  imul eax, edx
  imul ebx, ecx
  shr eax, 8
  shr ebx, 8
  add eax, ebx
  mov edi, eax

  mov eax, esi
  mov ebx, Color
  shr eax, 8
  shr ebx, 8
  and eax, 0FFh
  and ebx, 0FFh
  imul eax, edx
  imul ebx, ecx
  shr eax, 8
  shr ebx, 8
  add eax, ebx
  shl eax, 8
  or edi, eax

  mov eax, esi
  mov ebx, Color
  shr eax, 16
{  shr ebx, 16}
  and eax, 0FFh
  and ebx, 0FFh
  imul eax, edx
  imul ebx, ecx
  shr eax, 8
  shr ebx, 8
  add eax, ebx
  shl eax, 16
  or edi, eax

  mov [esp], edi

  mov esp, _esp
  pop edi
  pop esi
  pop edx
  pop ebx
 end;
end;

//---------------------------------------------------------------------------
procedure TPowerGraph.Line(X1, Y1, X2, Y2, Color: Integer);
Var I, DeltaX, DeltaY, NumPixels,
    D, DInc1, DInc2, X, XInc1, XInc2,
    Y, YInc1, YInc2: Integer;
begin
 DeltaX:= Abs(X2 - X1);
 DeltaY:= Abs(Y2 - Y1);
 if (DeltaX >= DeltaY) then
  begin
   NumPixels:= DeltaX + 1;
   D:= (DeltaY shl 1) - DeltaX;
   DInc1:= DeltaY shl 1;
   DInc2:= (DeltaY - DeltaX) shl 1;
   XInc1:= 1;XInc2:= 1;
   YInc1:= 0;YInc2:= 1;
  end else
  begin
   NumPixels:= DeltaY + 1;
   D:= (DeltaX shl 1) - DeltaY;
   DInc1:= DeltaX shl 1;
   DInc2:= (DeltaX - DeltaY) shl 1;
   XInc1:= 0; XInc2:= 1;
   YInc1:= 1; YInc2:= 1;
  end;
 if (X1 > X2) then
  begin
   XInc1:= -XInc1;
   XInc2:= -XInc2;
  end;
 if (Y1 > Y2) then
  begin
   YInc1:= -YInc1;
   YInc2:= -YInc2;
  end;
 X:= X1; Y:= Y1;
 for I:=1 to NumPixels do
  begin
   PutPixel(X, Y, Color);
   if (D < 0) then
    begin
     D:= D + DInc1;
     X:= X + XInc1;
     Y:= Y + YInc1;
    end else
    begin
     D:= D + DInc2;
     X:= X + XInc2;
     Y:= Y + YInc2;
    end;
  end;
end;

//---------------------------------------------------------------------------
procedure TPowerGraph.BlendLine(X1, Y1, X2, Y2, Color, Opacity: Integer);
Var I, DeltaX, DeltaY, NumPixels,
    D, DInc1, DInc2, X, XInc1, XInc2,
    Y, YInc1, YInc2: Integer;
begin
 DeltaX:= Abs(X2 - X1);
 DeltaY:= Abs(Y2 - Y1);
 if (DeltaX >= DeltaY) then
  begin
   NumPixels:= DeltaX + 1;
   D:= (DeltaY shl 1) - DeltaX;
   DInc1:= DeltaY shl 1;
   DInc2:= (DeltaY - DeltaX) shl 1;
   XInc1:= 1;XInc2:= 1;
   YInc1:= 0;YInc2:= 1;
  end else
  begin
   NumPixels:= DeltaY + 1;
   D:= (DeltaX shl 1) - DeltaY;
   DInc1:= DeltaX shl 1;
   DInc2:= (DeltaX - DeltaY) shl 1;
   XInc1:= 0; XInc2:= 1;
   YInc1:= 1; YInc2:= 1;
  end;
 if (X1 > X2) then
  begin
   XInc1:= -XInc1;
   XInc2:= -XInc2;
  end;
 if (Y1 > Y2) then
  begin
   YInc1:= -YInc1;
   YInc2:= -YInc2;
  end;
 X:= X1; Y:= Y1;
 for I:=1 to NumPixels do
  begin
   BlendPixel(X, Y, Color, Opacity);
   if (D < 0) then
    begin
     D:= D + DInc1;
     X:= X + XInc1;
     Y:= Y + YInc1;
    end else
    begin
     D:= D + DInc2;
     X:= X + XInc2;
     Y:= Y + YInc2;
    end;
  end;
end;

//---------------------------------------------------------------------------
procedure TPowerGraph.WuLine(X1, Y1, X2, Y2, Color, Opacity: Integer);
Var Dx, Dy, D, S, Ci, Ea, Ec: Integer;
begin
 if (Y1 = Y2)or(X1 = X2) then BlendLine(X1, Y1, X2, Y2, Color, Opacity) else
  begin
   if Y1 > Y2 then
    begin
     D:= Y1; Y1:= Y2; Y2:=D;
     D:= X1; X1:= X2; X2:=D;
    end;

   Dx:=X2 - X1;
   Dy:=Y2 - Y1;

   if Dx > -1 then S:=1 else
    begin
     S:= -1;
     Dx:= -Dx;
    end;
   Ec:=0;

   BlendPixel(X1, Y1, Color, Opacity);

   if Dy > Dx then
    begin
     Ea:=(Dx shl 16) div Dy;
     While Dy > 1 do
      begin
       Dec(Dy);
       D:= Ec;
       Inc(Ec, Ea);
       Ec:=Ec and $FFFF;
       if Ec <= D then Inc(X1, S);
       Inc(Y1);
       Ci:=Ec shr 8;

       BlendPixel(X1 + S, Y1, Color, (Opacity * Ci) shr 8);
       BlendPixel(X1, Y1, Color, (Opacity * (256 - Ci)) shr 8);
     end;
    end else
    begin
     Ea:=(Dy shl 16) div Dx;
     While Dx > 1 do
      begin
       Dec(Dx);
       D:= Ec;
       Inc(Ec, Ea);
       Ec:=Ec and $FFFF;
       if Ec <= D then Inc(Y1);
       Inc(X1, S);
       Ci:=Ec shr 8;

       BlendPixel(X1, Y1 + 1, Color, (Opacity * Ci) shr 8);
       BlendPixel(X1, Y1, Color, (Opacity * (256 - Ci)) shr 8);
     end;
    end;
   BlendPixel(X2, Y2, Color, Opacity);
  end;
end;

//---------------------------------------------------------------------------
procedure TPowerGraph.Circle(Xpos, Ypos, Radius, Vertices, Color, Opacity: Integer);
Var I, Xo, Yo, Xp, Yp: Integer;
begin
 Xp:= Xpos + Radius;
 Yp:= Ypos;
 for I:= 1 to Vertices do
  begin
   Xo:= Xp; Yo:= Yp;
   Xp:= Round(Xpos + (Radius * Cos(I * 2 * Pi / Vertices)));
   Yp:= Round(Ypos + (Radius * Sin(I * 2 * Pi / Vertices)));
   WuLine(Xo, Yo, Xp, Yp, Color, Opacity);
  end;
end;

//---------------------------------------------------------------------------
function TPowerGraph.ClipCoords(var SrcX, SrcY, DstX, DstY, Width, Height: Integer): Boolean;
begin
 Result:= False;

 if (DstX < ClipRect.Left) then
  begin
   SrcX:= SrcX +(ClipRect.Left - DstX);
   Width:= Width - (ClipRect.Left - DstX);
   DstX:= ClipRect.Left;
   if (Width < 1) then Exit;
  end;

 if (DstY < ClipRect.Top) then
  begin
   SrcY:= SrcY + (ClipRect.Top - DstY);
   Height:= Height - (ClipRect.Top - DstY);
   DstY:= ClipRect.Top;
   if (Height < 1) then Exit;
  end;

 if (DstX + Width > ClipRect.Right - 1) then
  begin
   Width:= ClipRect.Right - DstX - 1;
   if (Width < 1) then Exit;
  end;

 if (DstY + Height > ClipRect.Bottom - 1) then
  begin
   Height:= ClipRect.Bottom - DstY - 1;
   if (Height < 1) then Exit;
  end;

 Result:= True;
end;

//---------------------------------------------------------------------------
procedure TPowerGraph.SetAntialias(Value: Boolean);
begin
 if (FD3DActive) then
  begin
   if (Value) then
    FPowerD3D.SetTextureFilter(tfLinear)
     else FPowerD3D.SetTextureFilter(tfNone);
   end;

 FAntialias:= Value;
end;

//---------------------------------------------------------------------------
procedure TPowerGraph.RenderAdd(Image: TAGFImage; Xpos, Ypos, Pattern: Integer);
Var TexNum, SrcX, SrcY, PWidth, PHeight: Integer;
begin
  Assert(Image<>nil);
 PWidth:= Image.PatternWidth;
 PHeight:= Image.PatternHeight;

 if (FD3DActive) then
  begin
   FPowerD3D.TextureAdd(Image, Xpos, Ypos, Xpos + PWidth, Ypos, Xpos, Ypos + PHeight, Xpos + PWidth, Ypos + PHeight, Pattern);
  end else
  begin
   if (Image.SelectAGFTexture(Pattern, TexNum, SrcX, SrcY) <> 0) then Exit;

   if (not ClipCoords(SrcX, SrcY, Xpos, Ypos, PWidth, PHeight)) then Exit;

   Case BitCount of
    16: AddBlend16(Image.AGFTexture.Texture[TexNum], Bounds(SrcX, SrcY, PWidth, PHeight), Integer(FSurfaceDesc.lpSurface), Integer(FSurfaceDesc.lPitch), Xpos, Ypos);
    32: AddBlend32(Image.AGFTexture.Texture[TexNum], Bounds(SrcX, SrcY, PWidth, PHeight), Integer(FSurfaceDesc.lpSurface), Integer(FSurfaceDesc.lPitch), Xpos, Ypos);
   end;
  end;
end;

//---------------------------------------------------------------------------
procedure TPowerGraph.RenderMul(Image: TAGFImage; Xpos, Ypos, Pattern: Integer);
Var TexNum, SrcX, SrcY, PWidth, PHeight: Integer;
begin
 PWidth:= Image.PatternWidth;
 PHeight:= Image.PatternHeight;

 if (FD3DActive) then
  begin
   FPowerD3D.TextureMul(Image, Xpos, Ypos, Xpos + PWidth, Ypos, Xpos, Ypos + PHeight, Xpos + PWidth, Ypos + PHeight, Pattern);
  end else
  begin
   if (Image.SelectAGFTexture(Pattern, TexNum, SrcX, SrcY) <> 0) then Exit;

   if (not ClipCoords(SrcX, SrcY, Xpos, Ypos, PWidth, PHeight)) then Exit;

   Case BitCount of
    16: MulBlend16(Image.AGFTexture.Texture[TexNum], Bounds(SrcX, SrcY, PWidth, PHeight), Integer(FSurfaceDesc.lpSurface), Integer(FSurfaceDesc.lPitch), Xpos, Ypos);
    32: MulBlend32(Image.AGFTexture.Texture[TexNum], Bounds(SrcX, SrcY, PWidth, PHeight), Integer(FSurfaceDesc.lpSurface), Integer(FSurfaceDesc.lPitch), Xpos, Ypos);
   end;
  end;
end;

//---------------------------------------------------------------------------
procedure TPowerGraph.RotateAdd(Image: TAGFImage; Xpos, Ypos, Pattern, Angle, Scale: Integer);
Var TexNum, SrcX, SrcY, PWidth, PHeight: Integer;
begin
 PWidth:= Image.PatternWidth;
 PHeight:= Image.PatternHeight;

 if (fD3DActive) then
  begin
   FPowerD3D.RotateAdd(Image, Xpos, Ypos, Pattern, Angle, Scale);
  end else
  begin
   if (Image.SelectAGFTexture(Pattern, TexNum, SrcX, SrcY) <> 0) then Exit;

   if (FAntialias) then
    Case BitCount of
     16: AddRotate16_AA(Image.AGFTexture.Texture[TexNum], Bounds(SrcX, SrcY, PWidth, PHeight), Integer(FSurfaceDesc.lpSurface), Integer(FSurfaceDesc.lPitch), Xpos, Ypos, (64 - Angle) and $FF, Scale, ClipRect);
     32: AddRotate32_AA(Image.AGFTexture.Texture[TexNum], Bounds(SrcX, SrcY, PWidth, PHeight), Integer(FSurfaceDesc.lpSurface), Integer(FSurfaceDesc.lPitch), Xpos, Ypos, (64 - Angle) and $FF, Scale, ClipRect);
    end else
    Case BitCount of
     16: AddRotate16(Image.AGFTexture.Texture[TexNum], Bounds(SrcX, SrcY, PWidth, PHeight), Integer(FSurfaceDesc.lpSurface), Integer(FSurfaceDesc.lPitch), Xpos, Ypos, (64 - Angle) and $FF, Scale, ClipRect);
     32: AddRotate32(Image.AGFTexture.Texture[TexNum], Bounds(SrcX, SrcY, PWidth, PHeight), Integer(FSurfaceDesc.lpSurface), Integer(FSurfaceDesc.lPitch), Xpos, Ypos, (64 - Angle) and $FF, Scale, ClipRect);
    end;
  end;
end;

//---------------------------------------------------------------------------
procedure TPowerGraph.RotateAdd(Image: TAGFImage; Xpos, Ypos, Xrad, Yrad, Pattern, Angle, AngleEx, Scale: Integer);
Var TexNum, SrcX, SrcY, PWidth, PHeight: Integer;
    RAng, RAng2: Single;
begin
 PWidth:= Image.PatternWidth;
 PHeight:= Image.PatternHeight;

 RAng:= (Angle * Pi) / 128;
 RAng2:= (AngleEx * Pi) / 128;
 Xpos:= Xpos + Round(Cos(RAng + RAng2) * Xrad);
 Ypos:= Ypos + Round(Sin(RAng + RAng2) * Yrad);

 if (FD3DActive) then
  begin
   FPowerD3D.RotateAdd(Image, Xpos, Ypos, Pattern, Angle, Scale);
  end else
  begin
   if (Image.SelectAGFTexture(Pattern, TexNum, SrcX, SrcY) <> 0) then Exit;

   if (FAntialias) then
    Case BitCount of
     16: AddRotate16_AA(Image.AGFTexture.Texture[TexNum], Bounds(SrcX, SrcY, PWidth, PHeight), Integer(FSurfaceDesc.lpSurface), Integer(FSurfaceDesc.lPitch), Xpos, Ypos, (64 - Angle) and $FF, Scale, ClipRect);
     32: AddRotate32_AA(Image.AGFTexture.Texture[TexNum], Bounds(SrcX, SrcY, PWidth, PHeight), Integer(FSurfaceDesc.lpSurface), Integer(FSurfaceDesc.lPitch), Xpos, Ypos, (64 - Angle) and $FF, Scale, ClipRect);
    end else
    Case BitCount of
     16: AddRotate16(Image.AGFTexture.Texture[TexNum], Bounds(SrcX, SrcY, PWidth, PHeight), Integer(FSurfaceDesc.lpSurface), Integer(FSurfaceDesc.lPitch), Xpos, Ypos, (64 - Angle) and $FF, Scale, ClipRect);
     32: AddRotate32(Image.AGFTexture.Texture[TexNum], Bounds(SrcX, SrcY, PWidth, PHeight), Integer(FSurfaceDesc.lpSurface), Integer(FSurfaceDesc.lPitch), Xpos, Ypos, (64 - Angle) and $FF, Scale, ClipRect);
    end;
  end;
end;

//---------------------------------------------------------------------------
procedure TPowerGraph.RotateAddCol(Image: TAGFImage; Xpos, Ypos, Pattern, Angle, Scale, Color: Integer);
Var TexNum, SrcX, SrcY, PWidth, PHeight: Integer;
begin
 PWidth:= Image.PatternWidth;
 PHeight:= Image.PatternHeight;

 if (FD3DActive) then
  begin
   FPowerD3D.RotateAdd(Image, Xpos, Ypos, Pattern, Color, Color, Color, Color, Angle, Scale);
  end else
  begin
   if (Image.SelectAGFTexture(Pattern, TexNum, SrcX, SrcY) <> 0) then Exit;

   if (FAntialias) then
    Case BitCount of
     16: AddRotate16_AA_M1(Image.AGFTexture.Texture[TexNum], Bounds(SrcX, SrcY, PWidth, PHeight), Integer(FSurfaceDesc.lpSurface), Integer(FSurfaceDesc.lPitch), Xpos, Ypos, (64 - Angle) and $FF, Scale, Color, ClipRect);
     32: AddRotate32_AA_M1(Image.AGFTexture.Texture[TexNum], Bounds(SrcX, SrcY, PWidth, PHeight), Integer(FSurfaceDesc.lpSurface), Integer(FSurfaceDesc.lPitch), Xpos, Ypos, (64 - Angle) and $FF, Scale, Color, ClipRect);
    end else
    Case BitCount of
     16: AddRotate16_M1(Image.AGFTexture.Texture[TexNum], Bounds(SrcX, SrcY, PWidth, PHeight), Integer(FSurfaceDesc.lpSurface), Integer(FSurfaceDesc.lPitch), Xpos, Ypos, (64 - Angle) and $FF, Scale, Color, ClipRect);
     32: AddRotate32_M1(Image.AGFTexture.Texture[TexNum], Bounds(SrcX, SrcY, PWidth, PHeight), Integer(FSurfaceDesc.lpSurface), Integer(FSurfaceDesc.lPitch), Xpos, Ypos, (64 - Angle) and $FF, Scale, Color, ClipRect);
    end;
  end;
end;

//---------------------------------------------------------------------------
constructor TPowerD3D.Create(DDraw: TDXDraw);
Var I: Integer;
begin
 inherited Create;

 FDDraw:= DDraw;
 for I:=0 to 1023 do
  begin
   SinTable[I]:= Round(Sin((I*Pi)/512)*65356);
   CosTable[I]:= Round(Cos((I*Pi)/512)*65356);
  end;
 Vertex[0].Specular:= D3DRGB(1.0,1.0,1.0);
 Vertex[1].Specular:= D3DRGB(1.0,1.0,1.0);
 Vertex[2].Specular:= D3DRGB(1.0,1.0,1.0);
 Vertex[3].Specular:= D3DRGB(1.0,1.0,1.0);
 Vertex[0].rhw:= 1.0;
 Vertex[1].rhw:= 1.0;
 Vertex[2].rhw:= 1.0;
 Vertex[3].rhw:= 1.0;
 FillChar(Meshes, SizeOf(Meshes), 0);
 MaxMesh:= 0;
end;

//---------------------------------------------------------------------------
function TPowerD3D.FindMesh(Width, Height: Integer): Integer;
Var I: Integer;
begin
 for I:=1 to MaxMesh do
  begin
   if (Meshes[I-1].Width = Width)and(Meshes[I-1].Height = Height) then
    begin
     Result:= I-1;
     Exit;
    end;
  end;
 Result:= -1;
end;

//---------------------------------------------------------------------------
procedure TPowerD3D.ClearMeshes();
begin
 FillChar(Meshes, SizeOf(Meshes), 0);
 MaxMesh:= 0;
end;

//---------------------------------------------------------------------------
procedure TPowerD3D.BeginScene(Surface: TDirectDrawSurface = nil);
begin
 asm
  FINIT
 end;
 FDDraw.D3DDevice7.BeginScene();
 if Surface<>nil then
   fDDraw.D3DDevice7.SetRenderTarget(Surface.ISurface7,0);
end;

//---------------------------------------------------------------------------
procedure TPowerD3D.EndScene();
begin
 fDDraw.D3DDevice7.SetRenderTarget(fDDraw.Surface.ISurface7,0);
 FDDraw.D3DDevice7.EndScene();
 asm
  FINIT
 end;
end;

//---------------------------------------------------------------------------
procedure TPowerD3D.SetTextureFilter(Filter: Integer);
begin
 Case Filter of
  tfLinear: begin
             FDDraw.D3DDevice7.SetTextureStageState(0, D3DTSS_MAGFILTER, DWord(D3DTFG_LINEAR));
             FDDraw.D3DDevice7.SetTextureStageState(0, D3DTSS_MINFILTER, DWord(D3DTFN_LINEAR));
             FDDraw.D3DDevice7.SetTextureStageState(0, D3DTSS_MIPFILTER, DWord(D3DTFN_LINEAR));
            end;
  else begin
        FDDraw.D3DDevice7.SetTextureStageState(0, D3DTSS_MAGFILTER, DWord(D3DTFG_POINT));
        FDDraw.D3DDevice7.SetTextureStageState(0, D3DTSS_MINFILTER, DWord(D3DTFN_POINT));
        FDDraw.D3DDevice7.SetTextureStageState(0, D3DTSS_MIPFILTER, DWord(D3DTFN_POINT));
       end;
 end;
end;

//---------------------------------------------------------------------------
procedure TPowerD3D.TextureMap(Image: TAGFImage; X1, Y1, X2, Y2, X3, Y3, X4, Y4, Pattern: Integer);
begin
 FDDraw.D3DDevice7.SetRenderState(D3DRENDERSTATE_ALPHABLENDENABLE, 0);

 Vertex[0].Color:= D3DRGB(1.0, 1.0, 1.0);
 Vertex[1].Color:= D3DRGB(1.0, 1.0, 1.0);
 Vertex[2].Color:= D3DRGB(1.0, 1.0, 1.0);
 Vertex[3].Color:= D3DRGB(1.0, 1.0, 1.0);
 Image.SelectD3DTexture(Pattern, Vertex);

 Vertex[0].sx:= X1;
 Vertex[0].sy:= Y1;
 Vertex[1].sx:= X2;
 Vertex[1].sy:= Y2;
 Vertex[2].sx:= X3;
 Vertex[2].sy:= Y3;
 Vertex[3].sx:= X4;
 Vertex[3].sy:= Y4;
 FDDraw.D3DDevice7.DrawPrimitive(D3DPT_TRIANGLESTRIP, D3DFVF_TLVERTEX, Vertex, 4, D3DDP_WAIT);
end;

//---------------------------------------------------------------------------
procedure TPowerD3D.TextureMap(Image: TAGFImage; X1, Y1, X2, Y2, X3, Y3, X4, Y4, Pattern, L1, L2, L3, L4: Integer);
begin
 FDDraw.D3DDevice7.SetRenderState(D3DRENDERSTATE_ALPHABLENDENABLE, 0);

 Vertex[0].Color:= D3DRGB(L1 / 256, L1 / 256, L1 / 256);
 Vertex[1].Color:= D3DRGB(L2 / 256, L2 / 256, L2 / 256);
 Vertex[2].Color:= D3DRGB(L3 / 256, L3 / 256, L3 / 256);
 Vertex[3].Color:= D3DRGB(L4 / 256, L4 / 256, L4 / 256);
 Image.SelectD3DTexture(Pattern, Vertex);

 Vertex[0].sx:= X1;
 Vertex[0].sy:= Y1;
 Vertex[1].sx:= X2;
 Vertex[1].sy:= Y2;
 Vertex[2].sx:= X3;
 Vertex[2].sy:= Y3;
 Vertex[3].sx:= X4;
 Vertex[3].sy:= Y4;
 FDDraw.D3DDevice7.DrawPrimitive(D3DPT_TRIANGLESTRIP, D3DFVF_TLVERTEX, Vertex, 4, D3DDP_WAIT);
end;

//---------------------------------------------------------------------------
procedure TPowerD3D.TextureAdd(Image: TAGFImage; X1, Y1, X2, Y2, X3, Y3, X4, Y4, Pattern: Integer);
begin
 FDDraw.D3DDevice7.SetRenderState(D3DRENDERSTATE_ALPHABLENDENABLE, 1);
 FDDraw.D3DDevice7.SetRenderState(D3DRENDERSTATE_SRCBLEND, Integer(D3DBLEND_ONE));
 FDDraw.D3DDevice7.SetRenderState(D3DRENDERSTATE_DESTBLEND, Integer(D3DBLEND_ONE));

 Vertex[0].Color:= D3DRGB(1.0, 1.0, 1.0);
 Vertex[1].Color:= D3DRGB(1.0, 1.0, 1.0);
 Vertex[2].Color:= D3DRGB(1.0, 1.0, 1.0);
 Vertex[3].Color:= D3DRGB(1.0, 1.0, 1.0);
 Image.SelectD3DTexture(Pattern, Vertex);

 Vertex[0].sx:= X1;
 Vertex[0].sy:= Y1;
 Vertex[1].sx:= X2;
 Vertex[1].sy:= Y2;
 Vertex[2].sx:= X3;
 Vertex[2].sy:= Y3;
 Vertex[3].sx:= X4;
 Vertex[3].sy:= Y4;
 FDDraw.D3DDevice7.DrawPrimitive(D3DPT_TRIANGLESTRIP, D3DFVF_TLVERTEX, Vertex, 4, D3DDP_DONOTLIGHT or D3DDP_DONOTCLIP);
end;

//---------------------------------------------------------------------------
procedure TPowerD3D.TextureAdd(Image: TAGFImage; X1, Y1, X2, Y2, X3, Y3, X4, Y4, Pattern, L1, L2, L3, L4: Integer);
begin
 FDDraw.D3DDevice7.SetRenderState(D3DRENDERSTATE_ALPHABLENDENABLE, 1);
 FDDraw.D3DDevice7.SetRenderState(D3DRENDERSTATE_SRCBLEND, Integer(D3DBLEND_ONE));
 FDDraw.D3DDevice7.SetRenderState(D3DRENDERSTATE_DESTBLEND, Integer(D3DBLEND_ONE));

 Vertex[0].Color:= D3DRGB(L1 / 256, L1 / 256, L1 / 256);
 Vertex[1].Color:= D3DRGB(L2 / 256, L2 / 256, L2 / 256);
 Vertex[2].Color:= D3DRGB(L3 / 256, L3 / 256, L3 / 256);
 Vertex[3].Color:= D3DRGB(L4 / 256, L4 / 256, L4 / 256);
 Image.SelectD3DTexture(Pattern, Vertex);

 Vertex[0].sx:= X1;
 Vertex[0].sy:= Y1;
 Vertex[1].sx:= X2;
 Vertex[1].sy:= Y2;
 Vertex[2].sx:= X3;
 Vertex[2].sy:= Y3;
 Vertex[3].sx:= X4;
 Vertex[3].sy:= Y4;
 FDDraw.D3DDevice7.DrawPrimitive(D3DPT_TRIANGLESTRIP, D3DFVF_TLVERTEX, Vertex, 4, D3DDP_WAIT);
end;

//---------------------------------------------------------------------------
procedure TPowerD3D.TextureMul(Image: TAGFImage; X1, Y1, X2, Y2, X3, Y3, X4, Y4, Pattern: Integer);
begin
 FDDraw.D3DDevice7.SetRenderState(D3DRENDERSTATE_ALPHABLENDENABLE, 1);
 FDDraw.D3DDevice7.SetRenderState(D3DRENDERSTATE_SRCBLEND, Integer(D3DBLEND_ZERO));
 FDDraw.D3DDevice7.SetRenderState(D3DRENDERSTATE_DESTBLEND, Integer(D3DBLEND_SRCCOLOR));

 Vertex[0].Color:= D3DRGB(1.0, 1.0, 1.0);
 Vertex[1].Color:= D3DRGB(1.0, 1.0, 1.0);
 Vertex[2].Color:= D3DRGB(1.0, 1.0, 1.0);
 Vertex[3].Color:= D3DRGB(1.0, 1.0, 1.0);
 Image.SelectD3DTexture(Pattern, Vertex);

 Vertex[0].sx:= X1;
 Vertex[0].sy:= Y1;
 Vertex[1].sx:= X2;
 Vertex[1].sy:= Y2;
 Vertex[2].sx:= X3;
 Vertex[2].sy:= Y3;
 Vertex[3].sx:= X4;
 Vertex[3].sy:= Y4;
 FDDraw.D3DDevice7.DrawPrimitive(D3DPT_TRIANGLESTRIP, D3DFVF_TLVERTEX, Vertex, 4, D3DDP_WAIT);
end;

//---------------------------------------------------------------------------
procedure TPowerD3D.TextureMul(Image: TAGFImage; X1, Y1, X2, Y2, X3, Y3, X4, Y4, Pattern, L1, L2, L3, L4: Integer);
begin
 FDDraw.D3DDevice7.SetRenderState(D3DRENDERSTATE_ALPHABLENDENABLE, 1);
 FDDraw.D3DDevice7.SetRenderState(D3DRENDERSTATE_SRCBLEND, Integer(D3DBLEND_ZERO));
 FDDraw.D3DDevice7.SetRenderState(D3DRENDERSTATE_DESTBLEND, Integer(D3DBLEND_SRCCOLOR));

 Vertex[0].Color:= D3DRGB(L1 / 256, L1 / 256, L1 / 256);
 Vertex[1].Color:= D3DRGB(L2 / 256, L2 / 256, L2 / 256);
 Vertex[2].Color:= D3DRGB(L3 / 256, L3 / 256, L3 / 256);
 Vertex[3].Color:= D3DRGB(L4 / 256, L4 / 256, L4 / 256);
 Image.SelectD3DTexture(Pattern, Vertex);

 Vertex[0].sx:= X1;
 Vertex[0].sy:= Y1;
 Vertex[1].sx:= X2;
 Vertex[1].sy:= Y2;
 Vertex[2].sx:= X3;
 Vertex[2].sy:= Y3;
 Vertex[3].sx:= X4;
 Vertex[3].sy:= Y4;
 FDDraw.D3DDevice7.DrawPrimitive(D3DPT_TRIANGLESTRIP, D3DFVF_TLVERTEX, Vertex, 4, D3DDP_WAIT);
end;

//---------------------------------------------------------------------------
procedure TPowerD3D.Rotate(Image: TAGFImage; Xp, Yp, RadiusX, RadiusY, Pattern: Integer; Angle: Double);
Var Mesh: Integer;
begin
 Mesh:= FindMesh(RadiusX, RadiusY);
 if Mesh=-1 then
  begin
   if MaxMesh>=2047 then
    raise Exception.Create('PowerGraph: Mesh # overflow');
   CreateT4Mesh(RadiusY, RadiusX, Meshes[MaxMesh].Mesh);
   Mesh:= MaxMesh;
   Inc(MaxMesh);
  end;
 RotateMesh(Image, Xp, Yp, 0, 0, Meshes[Mesh].Mesh, Pattern, Angle);
end;

//---------------------------------------------------------------------------
procedure TPowerD3D.RotateAdd(Image: TAGFImage; Xp, Yp, Pattern: Integer; Angle: Double; Scale: Integer);
Var Mesh, RadiusX, RadiusY: Integer;
begin
 RadiusX:= (Image.PatternWidth * Scale) div 256;
 RadiusY:= (Image.PatternHeight * Scale) div 256;

 Mesh:= FindMesh(RadiusX, RadiusY);
 if (Mesh = -1) then
  begin
   if (MaxMesh >= 2047) then ClearMeshes();
   MaxMesh:= 0;

   CreateT4Mesh(RadiusX, RadiusY, Meshes[MaxMesh].Mesh);
   Meshes[MaxMesh].Width:= RadiusX;
   Meshes[MaxMesh].Height:= RadiusY;

   Mesh:= MaxMesh;
   Inc(MaxMesh);
  end;
 RotateMeshAdd(Image, Xp, Yp, 0, 0, Meshes[Mesh].Mesh, Pattern, Angle);
end;

//---------------------------------------------------------------------------
procedure TPowerD3D.RotateAdd(Image: TAGFImage; Xp, Yp, Pattern, C1, C2, C3, C4: Integer; Angle: Double; Scale: Integer);
Var Mesh, RadiusX, RadiusY: Integer;
begin
 RadiusX:= (Image.PatternWidth * Scale) div 256;
 RadiusY:= (Image.PatternHeight * Scale) div 256;

 Mesh:= FindMesh(RadiusX, RadiusY);
 if (Mesh = -1) then
  begin
   if (MaxMesh >= 2047) then ClearMeshes();
   MaxMesh:= 0;

   CreateT4Mesh(RadiusY, RadiusX, Meshes[MaxMesh].Mesh);
   Meshes[MaxMesh].Width:= RadiusX;
   Meshes[MaxMesh].Height:= RadiusY;

   Mesh:= MaxMesh;
   Inc(MaxMesh);
  end;
 RotateMeshAdd_cl(Image, Xp, Yp, 0, 0, Meshes[Mesh].Mesh, Pattern, C1, C2, C3, C4, Angle);
end;

//---------------------------------------------------------------------------
procedure TPowerD3D.RotateMesh(Image: TAGFImage; Xp, Yp, Rx, Ry: Integer; Mesh: T4Mesh; Pattern: Integer;Angle: Double);
Var RAng: Double;
begin
 FDDraw.D3DDevice7.SetRenderState(D3DRENDERSTATE_ALPHABLENDENABLE, 0);
 FDDraw.D3DDevice7.SetRenderState(D3DRENDERSTATE_COLORKEYENABLE,Integer(True));

{ FDDraw.D3DDevice7.SetRenderState(D3DRENDERSTATE_SRCBLEND, Integer(D3DBLEND_ONE));
 FDDraw.D3DDevice7.SetRenderState(D3DRENDERSTATE_DESTBLEND, Integer(D3DBLEND_ONE));}

{ FDDraw.D3DDevice7.SetTextureStageState(0, D3DTSS_MAGFILTER, dword(D3DTFG_LINEAR));
 FDDraw.D3DDevice7.SetTextureStageState(0, D3DTSS_MINFILTER, dword(D3DTFG_LINEAR));}

 Vertex[0].Color:= D3DRGB(1.0, 1.0, 1.0);
 Vertex[1].Color:= D3DRGB(1.0, 1.0, 1.0);
 Vertex[2].Color:= D3DRGB(1.0, 1.0, 1.0);
 Vertex[3].Color:= D3DRGB(1.0, 1.0, 1.0);
 Image.SelectD3DTexture(Pattern, Vertex);
 RAng:=(Angle * 2 * Pi) / 256;
 Xp:= Xp + Round(Cos(RAng) * Rx);
 Yp:= Yp + Round(Sin(RAng) * Ry);
 Vertex[0].sx:= Xp + Round(Cos(RAng + Mesh[0].Ang) * Mesh[0].Rad);
 Vertex[0].sy:= Yp + Round(Sin(RAng + Mesh[0].Ang) * Mesh[0].Rad);
 Vertex[1].sx:= Xp + Round(Cos(RAng + Mesh[1].Ang) * Mesh[1].Rad);
 Vertex[1].sy:= Yp + Round(Sin(RAng + Mesh[1].Ang) * Mesh[1].Rad);
 Vertex[2].sx:= Xp + Round(Cos(RAng + Mesh[2].Ang) * Mesh[2].Rad);
 Vertex[2].sy:= Yp + Round(Sin(RAng + Mesh[2].Ang) * Mesh[2].Rad);
 Vertex[3].sx:= Xp + Round(Cos(RAng + Mesh[3].Ang) * Mesh[3].Rad);
 Vertex[3].sy:= Yp + Round(Sin(RAng + Mesh[3].Ang) * Mesh[3].Rad);
 FDDraw.D3DDevice7.DrawPrimitive(D3DPT_TRIANGLESTRIP, D3DFVF_TLVERTEX, Vertex, 4, D3DDP_WAIT);
end;

//---------------------------------------------------------------------------
procedure TPowerD3D.RotateMesh(Image: TAGFImage; Xp, Yp, Rx, Ry: Integer; Mesh: T4Mesh; Pattern, L1, L2, L3, L4: Integer; Angle: Double);
Var RAng: Double;
begin
 FDDraw.D3DDevice7.SetRenderState(D3DRENDERSTATE_ALPHABLENDENABLE, 0);
 FDDraw.D3DDevice7.SetRenderState(D3DRENDERSTATE_COLORKEYENABLE, Integer(True));

 Vertex[0].Color:= D3DRGB(L1 / 256, L1 / 256, L1 / 256);
 Vertex[1].Color:= D3DRGB(L2 / 256, L2 / 256, L2 / 256);
 Vertex[2].Color:= D3DRGB(L3 / 256, L3 / 256, L3 / 256);
 Vertex[3].Color:= D3DRGB(L4 / 256, L4 / 256, L4 / 256);
 Image.SelectD3DTexture(Pattern, Vertex);
 RAng:=(Angle * 2 * Pi) / 256;
 Xp:= Xp + Round(Cos(RAng) * Rx);
 Yp:= Yp + Round(Sin(RAng) * Ry);
 Vertex[0].sx:= Xp + Round(Cos(RAng + Mesh[0].Ang) * Mesh[0].Rad);
 Vertex[0].sy:= Yp + Round(Sin(RAng + Mesh[0].Ang) * Mesh[0].Rad);
 Vertex[1].sx:= Xp + Round(Cos(RAng + Mesh[1].Ang) * Mesh[1].Rad);
 Vertex[1].sy:= Yp + Round(Sin(RAng + Mesh[1].Ang) * Mesh[1].Rad);
 Vertex[2].sx:= Xp + Round(Cos(RAng + Mesh[2].Ang) * Mesh[2].Rad);
 Vertex[2].sy:= Yp + Round(Sin(RAng + Mesh[2].Ang) * Mesh[2].Rad);
 Vertex[3].sx:= Xp + Round(Cos(RAng + Mesh[3].Ang) * Mesh[3].Rad);
 Vertex[3].sy:= Yp + Round(Sin(RAng + Mesh[3].Ang) * Mesh[3].Rad);
 FDDraw.D3DDevice7.DrawPrimitive(D3DPT_TRIANGLESTRIP, D3DFVF_TLVERTEX, Vertex, 4, D3DDP_WAIT);
end;

//---------------------------------------------------------------------------
procedure TPowerD3D.RotateMeshAdd(Image: TAGFImage; Xp, Yp, Rx, Ry: Integer; Mesh: T4Mesh; Pattern: Integer; Angle: Double);
Var RAng: Double;
begin
 FDDraw.D3DDevice7.SetRenderState(D3DRENDERSTATE_ALPHABLENDENABLE, 1);
 FDDraw.D3DDevice7.SetRenderState(D3DRENDERSTATE_SRCBLEND, Integer(D3DBLEND_ONE));
 FDDraw.D3DDevice7.SetRenderState(D3DRENDERSTATE_DESTBLEND, Integer(D3DBLEND_ONE));

 Vertex[0].Color:= D3DRGB(1.0, 1.0, 1.0);
 Vertex[1].Color:= D3DRGB(1.0, 1.0, 1.0);
 Vertex[2].Color:= D3DRGB(1.0, 1.0, 1.0);
 Vertex[3].Color:= D3DRGB(1.0, 1.0, 1.0);
 Image.SelectD3DTexture(Pattern, Vertex);
 RAng:= (Angle * 2 * Pi) / 256;
 Xp:= Xp + Round(Cos(RAng) * Rx);
 Yp:= Yp + Round(Sin(RAng) * Ry);
 Vertex[0].sx:= Xp + Round(Cos(RAng + Mesh[0].Ang) * Mesh[0].Rad);
 Vertex[0].sy:= Yp + Round(Sin(RAng + Mesh[0].Ang) * Mesh[0].Rad);
 Vertex[1].sx:= Xp + Round(Cos(RAng + Mesh[1].Ang) * Mesh[1].Rad);
 Vertex[1].sy:= Yp + Round(Sin(RAng + Mesh[1].Ang) * Mesh[1].Rad);
 Vertex[2].sx:= Xp + Round(Cos(RAng + Mesh[2].Ang) * Mesh[2].Rad);
 Vertex[2].sy:= Yp + Round(Sin(RAng + Mesh[2].Ang) * Mesh[2].Rad);
 Vertex[3].sx:= Xp + Round(Cos(RAng + Mesh[3].Ang) * Mesh[3].Rad);
 Vertex[3].sy:= Yp + Round(Sin(RAng + Mesh[3].Ang) * Mesh[3].Rad);
 FDDraw.D3DDevice7.DrawPrimitive(D3DPT_TRIANGLESTRIP, D3DFVF_TLVERTEX, Vertex, 4, D3DDP_WAIT);
end;

//---------------------------------------------------------------------------
procedure TPowerD3D.RotateMeshAdd(Image: TAGFImage; Xp, Yp, Rx, Ry: Integer; Mesh: T4Mesh; Pattern, L1, L2, L3, L4: Integer; Angle: Double);
Var RAng: Double;
begin
 FDDraw.D3DDevice7.SetRenderState(D3DRENDERSTATE_ALPHABLENDENABLE, 1);
 FDDraw.D3DDevice7.SetRenderState(D3DRENDERSTATE_SRCBLEND, Integer(D3DBLEND_ONE));
 FDDraw.D3DDevice7.SetRenderState(D3DRENDERSTATE_DESTBLEND, Integer(D3DBLEND_ONE));

 Vertex[0].Color:= D3DRGB(L1 / 256, L1 / 256, L1 / 256);
 Vertex[1].Color:= D3DRGB(L2 / 256, L2 / 256, L2 / 256);
 Vertex[2].Color:= D3DRGB(L3 / 256, L3 / 256, L3 / 256);
 Vertex[3].Color:= D3DRGB(L4 / 256, L4 / 256, L4 / 256);
 Image.SelectD3DTexture(Pattern, Vertex);
 RAng:= (Angle * 2 * Pi) / 256;
 Xp:= Xp + Round(Cos(RAng) * Rx);
 Yp:= Yp + Round(Sin(RAng) * Ry);
 Vertex[0].sx:= Xp + Round(Cos(RAng + Mesh[0].Ang) * Mesh[0].Rad);
 Vertex[0].sy:= Yp + Round(Sin(RAng + Mesh[0].Ang) * Mesh[0].Rad);
 Vertex[1].sx:= Xp + Round(Cos(RAng + Mesh[1].Ang) * Mesh[1].Rad);
 Vertex[1].sy:= Yp + Round(Sin(RAng + Mesh[1].Ang) * Mesh[1].Rad);
 Vertex[2].sx:= Xp + Round(Cos(RAng + Mesh[2].Ang) * Mesh[2].Rad);
 Vertex[2].sy:= Yp + Round(Sin(RAng + Mesh[2].Ang) * Mesh[2].Rad);
 Vertex[3].sx:= Xp + Round(Cos(RAng + Mesh[3].Ang) * Mesh[3].Rad);
 Vertex[3].sy:= Yp + Round(Sin(RAng + Mesh[3].Ang) * Mesh[3].Rad);
 FDDraw.D3DDevice7.DrawPrimitive(D3DPT_TRIANGLESTRIP, D3DFVF_TLVERTEX, Vertex, 4, D3DDP_WAIT);
end;

//---------------------------------------------------------------------------
procedure TPowerD3D.RotateMeshAdd_cl(Image: TAGFImage; Xp, Yp, Rx, Ry: Integer; Mesh: T4Mesh; Pattern, C1, C2, C3, C4: Integer; Angle: Double);
Var RAng: Double;
begin
 FDDraw.D3DDevice7.SetRenderState(D3DRENDERSTATE_ALPHABLENDENABLE, 1);
 FDDraw.D3DDevice7.SetRenderState(D3DRENDERSTATE_SRCBLEND, Integer(D3DBLEND_ONE));
 FDDraw.D3DDevice7.SetRenderState(D3DRENDERSTATE_DESTBLEND, Integer(D3DBLEND_ONE));

 Vertex[0].Color:= D3DRGB((C1 and $FF) / 255, ((C1 shr 8) and $FF) / 255, ((C1 shr 16) and $FF) / 255);
 Vertex[1].Color:= D3DRGB((C2 and $FF) / 255, ((C2 shr 8) and $FF) / 255, ((C2 shr 16) and $FF) / 255);
 Vertex[2].Color:= D3DRGB((C3 and $FF) / 255, ((C3 shr 8) and $FF) / 255, ((C3 shr 16) and $FF) / 255);
 Vertex[3].Color:= D3DRGB((C4 and $FF) / 255, ((C4 shr 8) and $FF) / 255, ((C4 shr 16) and $FF) / 255);
 Image.SelectD3DTexture(Pattern, Vertex);
 RAng:=(Angle * 2 * Pi) / 256;
 Xp:= Xp + Round(Cos(RAng) * Rx);
 Yp:= Yp + Round(Sin(RAng) * Ry);
 Vertex[0].sx:= Xp + Round(Cos(RAng + Mesh[0].Ang) * Mesh[0].Rad);
 Vertex[0].sy:= Yp + Round(Sin(RAng + Mesh[0].Ang) * Mesh[0].Rad);
 Vertex[1].sx:= Xp + Round(Cos(RAng + Mesh[1].Ang) * Mesh[1].Rad);
 Vertex[1].sy:= Yp + Round(Sin(RAng + Mesh[1].Ang) * Mesh[1].Rad);
 Vertex[2].sx:= Xp + Round(Cos(RAng + Mesh[2].Ang) * Mesh[2].Rad);
 Vertex[2].sy:= Yp + Round(Sin(RAng + Mesh[2].Ang) * Mesh[2].Rad);
 Vertex[3].sx:= Xp + Round(Cos(RAng + Mesh[3].Ang) * Mesh[3].Rad);
 Vertex[3].sy:= Yp + Round(Sin(RAng + Mesh[3].Ang) * Mesh[3].Rad);
 FDDraw.D3DDevice7.DrawPrimitive(D3DPT_TRIANGLESTRIP, D3DFVF_TLVERTEX, Vertex, 4, D3DDP_WAIT);
end;

//---------------------------------------------------------------------------
end.
