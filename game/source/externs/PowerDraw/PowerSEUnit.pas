unit PowerSEUnit;
//---------------------------------------------------------------------------
// PowerSEUnit - version 1.0 by Lifepower (lifepower@operamail.com)
//  16 / 32 bit Add/Mull Blending routiines are copyright of Gordon Cowie
//---------------------------------------------------------------------------
interface
uses Windows, FastDIB;

//---------------------------------------------------------------------------
// MMX routines by Gordon Cowie
//---------------------------------------------------------------------------
procedure AddBlend16(Src: TFastDIB; SrcRect: TRect; DstOfs, DstPitch, DstXpos, DstYpos: Integer);
procedure AddBlend32(Src: TFastDIB; SrcRect: TRect; DstOfs, DstPitch, DstXpos, DstYpos: Integer);
procedure MulBlend16(Src: TFastDIB; SrcRect: TRect; DstOfs, DstPitch, DstXpos, DstYpos: Integer);
procedure MulBlend32(Src: TFastDIB; SrcRect: TRect; DstOfs, DstPitch, DstXpos, DstYpos: Integer);

//---------------------------------------------------------------------------
// Rotating routines by Lifepower
//---------------------------------------------------------------------------
procedure AddRotate16_AA(Src: TFastDIB; SrcRect: TRect; DestOfs, DstPitch, Xpos, Ypos, Angle, Scale: Integer; ClipRect: TRect);
procedure AddRotate16(Src: TFastDIB; SrcRect: TRect; DestOfs, DstPitch, Xpos, Ypos, Angle, Scale: Integer; ClipRect: TRect);
procedure AddRotate32_AA(Src: TFastDIB; SrcRect: TRect; DestOfs, DstPitch, Xpos, Ypos, Angle, Scale: Integer; ClipRect: TRect);
procedure AddRotate32(Src: TFastDIB; SrcRect: TRect; DestOfs, DstPitch, Xpos, Ypos, Angle, Scale: Integer; ClipRect: TRect);

procedure AddRotate16_AA_M1(Src: TFastDIB; SrcRect: TRect; DestOfs, DstPitch, Xpos, Ypos, Angle, Scale, Color: Integer; ClipRect: TRect);
procedure AddRotate16_M1(Src: TFastDIB; SrcRect: TRect; DestOfs, DstPitch, Xpos, Ypos, Angle, Scale, Color: Integer; ClipRect: TRect);
procedure AddRotate32_AA_M1(Src: TFastDIB; SrcRect: TRect; DestOfs, DstPitch, Xpos, Ypos, Angle, Scale, Color: Integer; ClipRect: TRect);
procedure AddRotate32_M1(Src: TFastDIB; SrcRect: TRect; DestOfs, DstPitch, Xpos, Ypos, Angle, Scale, Color: Integer; ClipRect: TRect);

implementation

procedure AddBlend16(Src: TFastDIB; SrcRect: TRect; DstOfs, DstPitch, DstXpos, DstYpos: Integer);
type
  TAddMem16REG = array[0..235]of Byte; PAddMem16REG =^TAddMem16REG;
  TAddMem16MMX = array[0..346]of Byte; PAddMem16MMX =^TAddMem16MMX;
  TAddMemProc = procedure(Dst,Src1,Src2:Pointer;Size:Integer);
const
  AddMem16REG: TAddMem16REG =
  ($55,$8B,$EC,$83,$C4,$F4,$53,$57,$56,$89,$D7,$89,$CE,$8B,$4D,$08,$51,$83,$E1,
   $FC,$0F,$84,$99,$00,$00,$00,$01,$C1,$89,$4D,$08,$8B,$0F,$8B,$1E,$81,$E1,$1F,
   $00,$1F,$00,$81,$E3,$1F,$00,$1F,$00,$01,$D9,$89,$CB,$81,$E3,$20,$00,$20,$00,
   $C1,$EB,$05,$6B,$DB,$1F,$09,$D9,$8B,$17,$8B,$1E,$81,$E2,$E0,$07,$E0,$07,$81,
   $E3,$E0,$07,$E0,$07,$C1,$EA,$05,$C1,$EB,$05,$01,$DA,$89,$D3,$81,$E3,$40,$00,
   $40,$00,$C1,$EB,$06,$6B,$DB,$3F,$09,$DA,$C1,$E2,$05,$09,$D1,$8B,$17,$8B,$1E,
   $81,$E2,$00,$F8,$00,$F8,$81,$E3,$00,$F8,$00,$F8,$C1,$EA,$0B,$C1,$EB,$0B,$01,
   $DA,$89,$D3,$81,$E3,$20,$00,$20,$00,$C1,$EB,$05,$6B,$DB,$1F,$09,$DA,$C1,$E2,
   $0B,$09,$D1,$89,$08,$83,$C7,$04,$83,$C6,$04,$83,$C0,$04,$3B,$45,$08,$0F,$85,
   $70,$FF,$FF,$FF,$39,$EF,$74,$29,$59,$83,$E1,$03,$D1,$E9,$74,$28,$66,$8B,$0F,
   $66,$8B,$1E,$89,$4D,$FC,$89,$5D,$F8,$8D,$7D,$FC,$8D,$75,$F8,$50,$8D,$45,$F4,
   $8D,$58,$04,$89,$5D,$08,$E9,$43,$FF,$FF,$FF,$58,$8B,$5D,$F4,$66,$89,$18,$5E,
   $5F,$5B,$8B,$E5,$5D,$C2,$04,$00);

  AddMem16MMX: TAddMem16MMX =
  ($55,$8B,$EC,$83,$C4,$E0,$57,$56,$C7,$45,$FC,$1F,$00,$1F,$00,$C7,$45,$F8,$1F,
   $00,$1F,$00,$C7,$45,$F4,$E0,$07,$E0,$07,$C7,$45,$F0,$E0,$07,$E0,$07,$C7,$45,
   $EC,$00,$F8,$00,$F8,$C7,$45,$E8,$00,$F8,$00,$F8,$C7,$45,$E4,$00,$00,$00,$00,
   $C7,$45,$E0,$00,$00,$00,$00,$0F,$6F,$B5,$F0,$FF,$FF,$FF,$0F,$71,$D6,$05,$0F,
   $6F,$BD,$E8,$FF,$FF,$FF,$0F,$71,$D7,$0B,$89,$D7,$89,$CE,$8B,$4D,$08,$51,$C1,
   $E9,$03,$0F,$84,$A6,$00,$00,$00,$0F,$6F,$07,$0F,$6F,$0E,$0F,$6F,$D0,$0F,$6F,
   $D9,$0F,$6F,$E0,$0F,$6F,$E9,$0F,$DB,$85,$F8,$FF,$FF,$FF,$0F,$DB,$8D,$F8,$FF,
   $FF,$FF,$0F,$DD,$C1,$0F,$6F,$C8,$0F,$65,$8D,$F8,$FF,$FF,$FF,$0F,$71,$D1,$0B,
   $0F,$EB,$C1,$0F,$DB,$95,$F0,$FF,$FF,$FF,$0F,$DB,$9D,$F0,$FF,$FF,$FF,$0F,$71,
   $D2,$05,$0F,$71,$D3,$05,$0F,$DD,$D3,$0F,$6F,$DA,$0F,$65,$DE,$0F,$71,$D3,$0A,
   $0F,$EB,$D3,$0F,$71,$F2,$05,$0F,$EB,$C2,$0F,$DB,$A5,$E8,$FF,$FF,$FF,$0F,$DB,
   $AD,$E8,$FF,$FF,$FF,$0F,$71,$D4,$0B,$0F,$71,$D5,$0B,$0F,$DD,$E5,$0F,$6F,$EC,
   $0F,$65,$EF,$0F,$71,$D5,$0B,$0F,$EB,$E5,$0F,$71,$F4,$0B,$0F,$EB,$C4,$0F,$7F,
   $00,$83,$C7,$08,$83,$C6,$08,$83,$C0,$08,$49,$0F,$85,$5F,$FF,$FF,$FF,$83,$FF,
   $00,$74,$3B,$59,$83,$E1,$07,$D1,$E9,$74,$3C,$89,$CA,$57,$8D,$7D,$E0,$66,$F3,
   $A5,$0F,$6F,$8D,$E0,$FF,$FF,$FF,$5E,$89,$D1,$8D,$7D,$E0,$66,$F3,$A5,$0F,$6F,
   $85,$E0,$FF,$FF,$FF,$50,$8D,$45,$E0,$BF,$F8,$FF,$FF,$FF,$B9,$01,$00,$00,$00,
   $E9,$25,$FF,$FF,$FF,$5F,$8D,$75,$E0,$89,$D1,$66,$F3,$A5,$0F,$77,$5E,$5F,$8B,
   $E5,$5D,$C2,$04,$00);
var
  i: Integer;
  Code: PLine8;
  SrcOfs, SrcPitch, SrcXpos, SrcWidth, SrcHeight: Integer;
  DstPtr: Pointer;
  SrcPtr: Pointer;
begin
  if cfMMX in CPUInfo.Features then
  begin
    GetMem(Code,SizeOf(TAddMem16MMX));
    PAddMem16MMX(Code)^:=AddMem16MMX;

    i:=Src.BMask shl 16 or Src.BMask;
    PDWord(@Code[11])^:=i;
    PDWord(@Code[18])^:=i;
    i:=Src.GMask shl 16 or Src.GMask;
    PDWord(@Code[25])^:=i;
    PDWord(@Code[32])^:=i;
    i:=Src.RMask shl 16 or Src.RMask;
    PDWord(@Code[39])^:=i;
    PDWord(@Code[46])^:=i;
    Code[74]:=Src.GShl;
    Code[85]:=Src.RShl;
    Code[151]:=16-Src.Bpb;
    Code[172]:=Src.GShl;
    Code[176]:=Src.GShl;
    Code[189]:=16-Src.Bpg;
    Code[196]:=Src.GShl;
    Code[217]:=Src.RShl;
    Code[221]:=Src.RShl;
    Code[234]:=16-Src.Bpr;
    Code[241]:=Src.RShl;
  end else
  begin
    GetMem(Code,SizeOf(TAddMem16REG));
    PAddMem16REG(Code)^:=AddMem16REG;

    i:=Src.BMask shl 16 or Src.BMask;
    PDWord(@Code[37])^:=i;
    PDWord(@Code[43])^:=i;
    PDWord(@Code[53])^:=(i+i)and(not i);
    i:=Src.GMask shl 16 or Src.GMask;
    PDWord(@Code[71])^:=i;
    PDWord(@Code[77])^:=i;
    i:=i shr Src.GShl;
    PDWord(@Code[93])^:=(i+i)and(not i);
    i:=Src.RMask shl 16 or Src.RMask;
    PDWord(@Code[116])^:=i;
    PDWord(@Code[122])^:=i;
    i:=i shr Src.RShl;
    PDWord(@Code[138])^:=(i+i)and(not i);
    Code[59]:=Src.Bpb;
    Code[62]:=(1 shl Src.Bpb)-1;
    Code[83]:=Src.GShl;
    Code[86]:=Src.GShl;
    Code[99]:=Src.Bpg;
    Code[102]:=(1 shl Src.Bpg)-1;
    Code[107]:=Src.GShl;
    Code[128]:=Src.RShl;
    Code[131]:=Src.RShl;
    Code[144]:=Src.Bpr;
    Code[147]:=(1 shl Src.Bpr)-1;
    Code[152]:=Src.RShl;
  end;

  SrcOfs:= Integer(Src.Bits);
  SrcPitch:= Src.BWidth;
  SrcXpos:= SrcRect.Left;
  SrcWidth:= SrcRect.Right - SrcRect.Left;
  SrcHeight:= SrcRect.Bottom - SrcRect.Top;

  for i:=0 to SrcHeight - 1 do
   begin
    SrcPtr:= Pointer(SrcOfs + ((I + SrcRect.Top) * SrcPitch) + (SrcXpos * 2));
    DstPtr:= Pointer(DstOfs + ((I + DstYpos) * DstPitch) + (DstXpos * 2));
    TAddMemProc(Code)(DstPtr, SrcPtr, DstPtr, SrcWidth * 2);
   end;

  FreeMem(Code);
end;

procedure AddMem8(Dst,Src1,Src2:Pointer;Size:Integer);
// Dst  = eax
// Src1 = edx
// Src2 = ecx
// Size = [ebp + 8]
var
  s: Integer;
asm
  push ebx
  push edi
  push esi

  mov edi,edx
  mov esi,ecx
  mov ecx,[ebp+8]
  push ecx
  mov s,esp
  mov esp,eax
  and ecx,$FFFFFFFC
  jz @skip

  add ecx,eax
  mov [ebp+8],ecx

  @dwords:
    mov eax,[edi]
    mov ebx,[esi]
    mov ecx,eax
    mov edx,ebx

    and eax,$00FF00FF
    and ebx,$00FF00FF
    add eax,ebx
    mov ebx,eax
    and ebx,$01000100
    shr ebx,8
    imul ebx,$FF
    or  eax,ebx

    and ecx,$FF00FF00
    and edx,$FF00FF00
    shr ecx,8
    shr edx,8
    add ecx,edx
    mov edx,ecx
    and edx,$01000100
    shr edx,8
    imul edx,$FF
    or  ecx,edx

    shl ecx,8
    or eax,ecx
    mov [esp],eax

    add edi,4
    add esi,4
    add esp,4
    cmp esp,[ebp+8]
  jne @dwords

  @skip:
  mov eax,esp
  mov esp,s
  pop ecx
  and ecx,11b
  jz @exit

  @bytes:
    movzx ebx,Byte([esi])
    movzx edx,Byte([edi])
    add ebx,edx
    mov edx,ebx
    and edx,$0100
    sub edx,$0100
    xor edx,-1
    shr edx,8
    or  ebx,edx
    mov [eax],bl
    inc esi
    inc edi
    inc eax
    dec ecx
  jnz @bytes

  @exit:
  pop esi
  pop edi
  pop ebx
end;

procedure AddMem8MMX(Dst,Src1,Src2:Pointer;Size:Integer);
// Dst  = eax
// Src1 = edx
// Src2 = ecx
// Size = [ebp + 8]
asm
  push ebx
  push edi
  push esi

  mov edi,edx
  mov esi,ecx
  mov ecx,[ebp+8]
  push ecx
  shr ecx,3
  jz @skip

  @quads:
    db $0F,$6F,$07  /// movq    mm0,[edi]
    db $0F,$6F,$0E  /// movq    mm1,[esi]
    db $0F,$DC,$C1  /// paddusb mm0,mm1
    db $0F,$7F,$00  /// movq    [eax],mm0
    add edi,8
    add esi,8
    add eax,8
    dec ecx
  jnz @quads

  db $0F,$77 // emms

  @skip:
  pop ecx
  and ecx,111b
  jz  @exit

  @bytes:
    movzx ebx,Byte([esi])
    movzx edx,Byte([edi])
    add ebx,edx
    mov edx,ebx
    and edx,$0100
    sub edx,$0100
    xor edx,-1
    shr edx,8
    or  ebx,edx
    mov [eax],bl

    inc edi
    inc esi
    inc eax
    dec ecx
  jnz @bytes

  @exit:
  pop esi
  pop edi
  pop ebx
end;

procedure AddBlend32(Src: TFastDIB; SrcRect: TRect; DstOfs, DstPitch, DstXpos, DstYpos: Integer);
type TAddMemProc = procedure(Dst,Src1,Src2:Pointer;Size:Integer);
var i: Integer;
    AddMem: TAddMemProc;
    SrcOfs, SrcPitch, SrcXpos, SrcWidth, SrcHeight: Integer;
    DstPtr: Pointer;
    SrcPtr: Pointer;

begin
 if cfMMX in CPUInfo.Features then AddMem:=AddMem8MMX else AddMem:=AddMem8;

 SrcOfs:= Integer(Src.Bits);
 SrcPitch:= Src.BWidth;
 SrcXpos:= SrcRect.Left;
 SrcWidth:= SrcRect.Right - SrcRect.Left;
 SrcHeight:= SrcRect.Bottom - SrcRect.Top;

  for i:=0 to SrcHeight - 1 do
   begin
    SrcPtr:= Pointer(SrcOfs + ((I + SrcRect.Top) * SrcPitch) + (SrcXpos * 4));
    DstPtr:= Pointer(DstOfs + ((I + DstYpos) * DstPitch) + (DstXpos * 4));
    AddMem(DstPtr, SrcPtr, DstPtr, SrcWidth * 4);
   end;
end;

procedure MulBlend16(Src: TFastDIB; SrcRect: TRect; DstOfs, DstPitch, DstXpos, DstYpos: Integer);
type
  TMulMem16REG = array[0..150]of Byte; PMulMem16REG =^TMulMem16REG;
  TMulMem16MMX = array[0..294]of Byte; PMulMem16MMX =^TMulMem16MMX;
  TMulMemProc = procedure(Dst,Src1,Src2:Pointer;Size:Integer);
const
  MulMem16REG: TMulMem16REG =
  ($55,$8B,$EC,$83,$C4,$F4,$53,$57,$56,$89,$65,$FC,$89,$D7,$89,$CE,$89,$C4,$8B,
   $4D,$08,$D1,$E9,$74,$72,$0F,$B7,$1F,$0F,$B7,$16,$89,$5D,$F8,$89,$55,$F4,$81,
   $E3,$1F,$00,$00,$00,$81,$E2,$1F,$00,$00,$00,$0F,$AF,$DA,$C1,$EB,$05,$8B,$45,
   $F8,$8B,$55,$F4,$25,$E0,$07,$00,$00,$81,$E2,$E0,$07,$00,$00,$C1,$E8,$05,$C1,
   $EA,$05,$0F,$AF,$C2,$C1,$E8,$06,$C1,$E0,$05,$09,$C3,$8B,$45,$F8,$8B,$55,$F4,
   $25,$00,$F8,$00,$00,$81,$E2,$00,$F8,$00,$00,$C1,$E8,$0B,$C1,$EA,$0B,$0F,$AF,
   $C2,$C1,$E8,$05,$C1,$E0,$0B,$09,$C3,$66,$89,$1C,$24,$83,$C7,$02,$83,$C6,$02,
   $83,$C4,$02,$49,$75,$8E,$8B,$65,$FC,$5E,$5F,$5B,$8B,$E5,$5D,$C2,$04,$00);

  MulMem16MMX: TMulMem16MMX =
  ($55,$8B,$EC,$83,$C4,$E0,$57,$56,$C7,$45,$FC,$00,$F8,$00,$F8,$C7,$45,$F8,$00,
   $F8,$00,$F8,$C7,$45,$F4,$E0,$07,$E0,$07,$C7,$45,$F0,$E0,$07,$E0,$07,$C7,$45,
   $EC,$1F,$00,$1F,$00,$C7,$45,$E8,$1F,$00,$1F,$00,$C7,$45,$E4,$00,$00,$00,$00,
   $C7,$45,$E0,$00,$00,$00,$00,$89,$D7,$89,$CE,$8B,$4D,$08,$51,$C1,$E9,$03,$0F,
   $84,$83,$00,$00,$00,$0F,$6F,$07,$0F,$6F,$0E,$0F,$6F,$D0,$0F,$6F,$D8,$0F,$6F,
   $E1,$0F,$6F,$E9,$0F,$DB,$85,$E8,$FF,$FF,$FF,$0F,$DB,$8D,$E8,$FF,$FF,$FF,$0F,
   $D5,$C1,$0F,$71,$D0,$05,$0F,$DB,$95,$F0,$FF,$FF,$FF,$0F,$DB,$A5,$F0,$FF,$FF,
   $FF,$0F,$71,$D2,$05,$0F,$71,$D4,$05,$0F,$D5,$D4,$0F,$71,$D2,$06,$0F,$71,$F2,
   $05,$0F,$EB,$C2,$0F,$DB,$9D,$F8,$FF,$FF,$FF,$0F,$DB,$AD,$F8,$FF,$FF,$FF,$0F,
   $71,$D3,$0B,$0F,$71,$D5,$0B,$0F,$D5,$DD,$0F,$71,$D3,$05,$0F,$71,$F3,$0B,$0F,
   $EB,$C3,$0F,$7F,$00,$83,$C7,$08,$83,$C6,$08,$83,$C0,$08,$49,$75,$82,$83,$FF,
   $00,$74,$3E,$59,$83,$E1,$07,$D1,$E9,$74,$41,$89,$CA,$57,$89,$EF,$83,$EF,$20,
   $66,$F3,$A5,$0F,$6F,$8D,$E0,$FF,$FF,$FF,$5E,$29,$D7,$89,$D1,$66,$F3,$A5,$0F,
   $6F,$85,$E0,$FF,$FF,$FF,$B9,$01,$00,$00,$00,$BF,$F8,$FF,$FF,$FF,$50,$89,$E8,
   $83,$E8,$20,$E9,$45,$FF,$FF,$FF,$89,$D1,$5F,$89,$EE,$83,$EE,$20,$66,$F3,$A5,
   $0F,$77,$5E,$5F,$8B,$E5,$5D,$C2,$04,$00);
var
  i: Integer;
  Code: PLine8;
  SrcOfs, SrcPitch, SrcXpos, SrcWidth, SrcHeight: Integer;
  DstPtr: Pointer;
  SrcPtr: Pointer;

begin
  if cfMMX in CPUInfo.Features then
  begin
    GetMem(Code,SizeOf(TMulMem16MMX));
    PMulMem16MMX(Code)^:=MulMem16MMX;

    i:=Src.RMask shl 16 or Src.RMask;
    PDWord(@Code[11])^:=i;
    PDWord(@Code[18])^:=i;
    i:=Src.GMask shl 16 or Src.GMask;
    PDWord(@Code[25])^:=i;
    PDWord(@Code[32])^:=i;
    i:=Src.BMask shl 16 or Src.BMask;
    PDWord(@Code[39])^:=i;
    PDWord(@Code[46])^:=i;
    Code[119]:=Src.Bpb;
    Code[137]:=Src.GShl;
    Code[141]:=Src.GShl;
    Code[148]:=Src.Bpg;
    Code[152]:=Src.GShl;
    Code[173]:=Src.RShl;
    Code[177]:=Src.RShl;
    Code[184]:=Src.Bpr;
    Code[188]:=Src.RShl;
  end else
  begin
    GetMem(Code,SizeOf(TMulMem16REG));
    PMulMem16REG(Code)^:=MulMem16REG;

    PDWord(@Code[39])^:=Src.BMask;
    PDWord(@Code[45])^:=Src.BMask;
    PDWord(@Code[62])^:=Src.GMask;
    PDWord(@Code[68])^:=Src.GMask;
    PDWord(@Code[96])^:=Src.RMask;
    PDWord(@Code[102])^:=Src.RMask;
    Code[54]:=Src.Bpb;
    Code[74]:=Src.GShl;
    Code[77]:=Src.GShl;
    Code[83]:=Src.Bpg;
    Code[86]:=Src.GShl;
    Code[108]:=Src.RShl;
    Code[111]:=Src.RShl;
    Code[117]:=Src.Bpr;
    Code[120]:=Src.RShl;
  end;

  SrcOfs:= Integer(Src.Bits);
  SrcPitch:= Src.BWidth;
  SrcXpos:= SrcRect.Left;
  SrcWidth:= SrcRect.Right - SrcRect.Left;
  SrcHeight:= SrcRect.Bottom - SrcRect.Top;

  for i:=0 to SrcHeight - 1 do
   begin
    SrcPtr:= Pointer(SrcOfs + ((I + SrcRect.Top) * SrcPitch) + (SrcXpos * 2));
    DstPtr:= Pointer(DstOfs + ((I + DstYpos) * DstPitch) + (DstXpos * 2));
    TMulMemProc(Code)(DstPtr, SrcPtr, DstPtr, SrcWidth * 2);
   end;

  FreeMem(Code);
end;

procedure MulMem8(Dst,Src1,Src2:Pointer;Size:Integer);
// Dst  = eax
// Src1 = edx
// Src2 = ecx
// Size = [ebp + 8]
asm
  push ebx
  push edi
  push esi

  mov edi,edx
  mov esi,ecx
  mov ecx,[ebp+8]
  cmp ecx,0
  je @exit

  @bytes:
    movzx ebx,Byte([edi])
    movzx edx,Byte([esi])
    imul ebx,edx
    shr ebx,8
    mov [eax],bl
    inc edi
    inc esi
    inc eax
    dec ecx
  jnz @bytes

  @exit:
  pop esi
  pop edi
  pop ebx
end;

procedure MulMem8MMX(Dst,Src1,Src2:Pointer;Size:Integer);
// Dst  = eax
// Src1 = edx
// Src2 = ecx
// Size = [ebp + 8]
asm
  push ebx
  push edi
  push esi

  mov edi,edx
  mov esi,ecx
  mov ecx,[ebp+8]
  push ecx
  shr ecx,2
  jz @skip

  db $0F,$EF,$D2       /// pxor mm2,mm2

  @dwords:
    db $0F,$6E,$07       /// movd       mm0,[edi]
    db $0F,$6E,$0E       /// movd       mm1,[esi]
    db $0F,$60,$C2       /// punpcklbw  mm0,mm2
    db $0F,$60,$CA       /// punpcklbw  mm1,mm2
    db $0F,$D5,$C1       /// pmullw     mm0,mm1
    db $0F,$71,$D0,$08   /// psrlw      mm0,8
    db $0F,$67,$C0       /// packuswb   mm0,mm0
    db $0F,$7E,$00       /// movd       [eax],mm0

    add edi,4
    add esi,4
    add eax,4
    dec ecx
  jnz @dwords

  db $0F,$77 // emms

  @skip:
  pop ecx
  and ecx,11b
  jz @exit

  @bytes:
    movzx ebx,Byte([edi])
    movzx edx,Byte([esi])
    imul ebx,edx
    shr ebx,8
    mov [eax],bl
    inc edi
    inc esi
    inc eax
    dec ecx
  jnz @bytes

  @exit:
  pop esi
  pop edi
  pop ebx
end;

procedure MulBlend32(Src: TFastDIB; SrcRect: TRect; DstOfs, DstPitch, DstXpos, DstYpos: Integer);
type TMulMemProc = procedure(Dst,Src1,Src2:Pointer;Size:Integer);
var i: Integer;
    MulMem: TMulMemProc;
    SrcOfs, SrcPitch, SrcXpos, SrcWidth, SrcHeight: Integer;
    DstPtr: Pointer;
    SrcPtr: Pointer;
begin
  if cfMMX in CPUInfo.Features then
    MulMem:=MulMem8MMX else MulMem:=MulMem8;

 SrcOfs:= Integer(Src.Bits);
 SrcPitch:= Src.BWidth;
 SrcXpos:= SrcRect.Left;
 SrcWidth:= SrcRect.Right - SrcRect.Left;
 SrcHeight:= SrcRect.Bottom - SrcRect.Top;

 for i:=0 to SrcHeight - 1 do
  begin
   SrcPtr:= Pointer(SrcOfs + ((I + SrcRect.Top) * SrcPitch) + (SrcXpos * 4));
   DstPtr:= Pointer(DstOfs + ((I + DstYpos) * DstPitch) + (DstXpos * 4));
   MulMem(DstPtr, SrcPtr, DstPtr, SrcWidth * 4);
  end;
end;

procedure AddRotate16_AA(Src: TFastDIB; SrcRect: TRect; DestOfs, DstPitch, Xpos, Ypos, Angle, Scale: Integer; ClipRect: TRect);
Const Sqrt3 = 1.732050808;
Var HiLength, ImgX, ImgY, ImgW, ImgH: Integer;
    SrcOfs, SrcBWidth, DstOfs, DstAdd, J, I, SinVal, CosVal, xDiff, yDiff: Integer;
    SrcX, SrcY, StartX, StartY, SrcWidth, SrcHeight, DestWidth, DestHeight: Integer;
    Xfloat, Yfloat, Xint, Yint, Xadd, Yadd, Xfrac, Yfrac, CenterX, CenterY: Integer;
    _esp: Integer;
begin
 { getting source rectangle }
 SrcX:= SrcRect.Left;
 SrcY:= SrcRect.Top;
 SrcWidth:= SrcRect.Right - SrcX;
 SrcHeight:= SrcRect.Bottom - SrcY;
 CenterX:= SrcWidth shr 1;
 CenterY:= SrcHeight shr 1;
 { calculating destination area size }
 HiLength:= SrcWidth;
 if (SrcHeight > HiLength) then HiLength:= SrcHeight;
 HiLength:= (HiLength * Scale) div 256;
 HiLength:= HiLength + Round((Sqrt3 * HiLength) / 4);
 { destination width & height }
 DestWidth:= HiLength;
 DestHeight:= HiLength;
 { centering rotating image inside destination }
 xDiff:=DestWidth shr 1;
 yDiff:=DestHeight shr 1;
 { centering destination image }
 Xpos:= Xpos - (DestWidth shr 1);
 Ypos:= Ypos - (DestHeight shr 1);
 { starting offsets in source image }
 StartX:= 0; StartY:= 0;
 { performing clipping }
 if (Xpos < ClipRect.Left) then
  begin
   StartX:= StartX + (ClipRect.Left - Xpos);
   DestWidth:= DestWidth - (ClipRect.Left - Xpos);
   Xpos:= ClipRect.Left;
   if (DestWidth < 1) then Exit;
  end;
 if (Ypos < ClipRect.Top) then
  begin
   StartY:= StartY + (ClipRect.Top - Ypos);
   DestHeight:= DestHeight - (ClipRect.Top - Ypos);
   Ypos:= ClipRect.Top;
   if (DestHeight < 1) then Exit;
  end;
 if (Xpos + DestWidth > ClipRect.Right - 1) then
  begin
   DestWidth:= ClipRect.Right - Xpos - 1;
   if (DestWidth < 1) then Exit;
  end;
 if (Ypos + DestHeight > ClipRect.Bottom - 1) then
  begin
   DestHeight:= ClipRect.Bottom - Ypos - 1;
   if (DestHeight < 1) then Exit;
  end;

 SrcBWidth:= Src.BWidth;
 SrcOfs:= Integer(Src.Bits) + (SrcBWidth * SrcY) + (SrcX shl 1);

 DstOfs:= DestOfs + (DstPitch * Ypos) + (Xpos shl 1);
 DstAdd:= DstPitch - (DestWidth shl 1);

 SinVal:= Round((Sin(Angle * Pi / 128) / Scale) * 65536 * 256);
 CosVal:= Round((Cos(Angle * Pi / 128) / Scale) * 65536 * 256);
 Xadd:= (CenterX shl 16) - (SinVal * (StartY - yDiff)) + (CosVal * (StartX - xDiff));
 Yadd:= (CenterY shl 16) + (CosVal * (StartY - yDiff)) + (SinVal * (StartX - xDiff));

 for J:=0 to DestHeight - 1 do
  begin
   Xfloat:= Xadd - (SinVal * J);
   Yfloat:= Yadd + (CosVal * J);
   for I:=0 to DestWidth - 1 do
    begin
     Xint:= SmallInt(Xfloat shr 16);
     Yint:= SmallInt(Yfloat shr 16);
     if (Xint >= 0)and(Yint >= 0)and(Xint < SrcWidth - 1)and(Yint < SrcHeight - 1) then
      begin
       Xfrac:= Xfloat and $FFFF;
       Yfrac:= Yfloat and $FFFF;
       asm
        push ebx
        push esi
        push edi
        mov _esp,esp

        mov esi, SrcOfs
        mov eax, Yint
        mov ebx, SrcBWidth
        imul eax, ebx
        add esi, eax
        mov eax, Xint
        shl eax, 1
        add esi, eax
        mov edi, esi
        add edi, SrcBWidth
        { ESI - Top pixel, EDI - Bottom pixel }

        xor eax,eax
        xor ebx,ebx
        xor ecx,ecx
        xor edx,edx
        { BLUE }
        mov bx,[esi]
        and bx,001Fh
        mov edx,ebx
        mov ax,[esi+2]
        and ax,001Fh
        sub eax,ebx
        mov ebx,Xfrac
        imul eax,ebx
        sar eax,16
        add eax,edx

        mov ecx,eax

        mov bx,[edi]
        and bx,001Fh
        mov edx,ebx
        mov ax,[edi+2]
        and ax,001Fh
        sub eax,ebx
        mov ebx,Xfrac
        imul eax,ebx
        sar eax,16
        add eax,edx

        sub eax,ecx
        mov ebx, Yfrac
        imul eax,ebx
        sar eax,16
        add eax,ecx
        mov esp,eax

        { GREEN }
        mov bx,[esi]
        shr bx,5
        and bx,003Fh
        mov edx,ebx
        mov ax,[esi+2]
        shr ax,5
        and ax,003Fh
        sub eax,ebx
        mov ebx,Xfrac
        imul eax,ebx
        sar eax,16
        add eax,edx

        mov ecx,eax

        mov bx,[edi]
        shr bx,5
        and bx,003Fh
        mov edx,ebx
        mov ax,[edi+2]
        shr ax,5
        and ax,003Fh
        sub eax,ebx
        mov ebx,Xfrac
        imul eax,ebx
        sar eax,16
        add eax,edx

        sub eax,ecx
        mov ebx, Yfrac
        imul eax,ebx
        sar eax,16
        add eax,ecx
        shl eax,5
        or esp,eax

        { RED }
        mov bx,[esi]
        shr bx,11
        and bx,001Fh
        mov edx,ebx
        mov ax,[esi+2]
        shr ax,11
        and ax,001Fh
        sub eax,ebx
        mov ebx,Xfrac
        imul eax,ebx
        sar eax,16
        add eax,edx

        mov ecx,eax

        mov bx,[edi]
        shr bx,11
        and bx,001Fh
        mov edx,ebx
        mov ax,[edi+2]
        shr ax,11
        and ax,001Fh
        sub eax,ebx
        mov ebx,Xfrac
        imul eax,ebx
        sar eax,16
        add eax,edx

        sub eax,ecx
        mov ebx, Yfrac
        imul eax,ebx
        sar eax,16
        add eax,ecx
        shl eax,11
        or eax,esp

        mov edi, DstOfs

        mov esp, eax          // ESP - source color
        mov bx, [edi]
        mov dx, bx            // EDX - destination color

        and ax, 001Fh         // Adding RED
        and bx, 001Fh
        add ax, bx

        cmp ax, 001Fh
        jb @Skip1
        mov ax, 001Fh
       @Skip1:
        mov cx, ax

        mov eax, esp          // Adding GREEN
        mov bx, dx
        shr ax, 5
        shr bx, 5
        and ax, 003Fh
        and bx, 003Fh
        add ax,bx

        cmp ax, 003Fh
        jb @Skip2
        mov ax, 003Fh
       @Skip2:
        shl ax,5
        or cx, ax

        mov eax, esp          // Adding BLUE
        mov bx, dx
        shr ax, 11
        shr bx, 11
        and ax, 001Fh
        and bx, 001Fh
        add ax,bx

        cmp ax, 001Fh
        jb @Skip3
        mov ax, 001Fh
       @Skip3:
        shl ax,11
        or cx, ax

        mov [edi],cx

        mov esp,_esp
        pop edi
        pop esi
        pop ebx
       end;
      end;
     Inc(DstOfs, 2);
     Inc(Xfloat, CosVal);
     Inc(Yfloat, SinVal);
    end;
   Inc(DstOfs, DstAdd);
  end;
end;

procedure AddRotate16(Src: TFastDIB; SrcRect: TRect; DestOfs, DstPitch, Xpos, Ypos, Angle, Scale: Integer; ClipRect: TRect);
Const Sqrt3=1.732050808;
Var HiLength:Integer;
    SrcOfs,SrcBWidth,DstOfs,DstAdd,SinVal,CosVal,xDiff,yDiff:Integer;
    SrcX,SrcY,StartX,StartY,SrcWidth,SrcHeight,DestWidth,DestHeight:Integer;
    Xfloat,Yfloat,Xadd,Yadd,CenterX,CenterY,WorkX, WorkY:Integer;
    _esp:Integer;
begin
 { getting source rectangle }
 SrcX:= SrcRect.Left;
 SrcY:= SrcRect.Top;
 SrcWidth:= SrcRect.Right - SrcX;
 SrcHeight:= SrcRect.Bottom - SrcY;
 CenterX:= SrcWidth shr 1;
 CenterY:= SrcHeight shr 1;
 { calculating destination area size }
 HiLength:= SrcWidth;
 if (SrcHeight > HiLength) then HiLength:= SrcHeight;
 HiLength:= (HiLength * Scale) div 256;
 HiLength:= HiLength + Round((Sqrt3 * HiLength) / 4);
 { destination width & height }
 DestWidth:= HiLength;
 DestHeight:= HiLength;
 { centering rotating image inside destination }
 xDiff:=DestWidth shr 1;
 yDiff:=DestHeight shr 1;
 { centering destination image }
 Xpos:= Xpos - (DestWidth shr 1);
 Ypos:= Ypos - (DestHeight shr 1);
 { starting offsets in source image }
 StartX:= 0; StartY:= 0;
 { performing clipping }
 if (Xpos < ClipRect.Left) then
  begin
   StartX:= StartX + (ClipRect.Left - Xpos);
   DestWidth:= DestWidth - (ClipRect.Left - Xpos);
   Xpos:= ClipRect.Left;
   if (DestWidth < 1) then Exit;
  end;
 if (Ypos < ClipRect.Top) then
  begin
   StartY:= StartY + (ClipRect.Top - Ypos);
   DestHeight:= DestHeight - (ClipRect.Top - Ypos);
   Ypos:= ClipRect.Top;
   if (DestHeight < 1) then Exit;
  end;
 if (Xpos + DestWidth > ClipRect.Right - 1) then
  begin
   DestWidth:= ClipRect.Right - Xpos - 1;
   if (DestWidth < 1) then Exit;
  end;
 if (Ypos + DestHeight > ClipRect.Bottom - 1) then
  begin
   DestHeight:= ClipRect.Bottom - Ypos - 1;
   if (DestHeight < 1) then Exit;
  end;

 SrcBWidth:= Src.BWidth;
 SrcOfs:= Integer(Src.Bits) + (SrcBWidth * SrcY) + (SrcX shl 1);

 DstOfs:= DestOfs + (DstPitch * Ypos) + (Xpos shl 1);
 DstAdd:= DstPitch - (DestWidth shl 1);

 SinVal:= Round((Sin(Angle * Pi / 128) / Scale) * 65536 * 256);
 CosVal:= Round((Cos(Angle * Pi / 128) / Scale) * 65536 * 256);
 Xadd:= (CenterX shl 16) - (SinVal * (StartY - yDiff)) + (CosVal * (StartX - xDiff));
 Yadd:= (CenterY shl 16) + (CosVal * (StartY - yDiff)) + (SinVal * (StartX - xDiff));
 WorkY:= 0;

 asm
  push ebx
  push esi
  push edi
  mov _esp, esp
  mov esp, DstOfs

 @YLoop:
  mov edx, WorkY
  mov eax, SinVal
  imul eax, edx
  mov ebx, eax
  mov eax, Xadd
  sub eax, ebx
  mov Xfloat, eax

  mov eax, CosVal
  imul eax, edx
  mov ebx, eax
  mov eax, Yadd
  add eax, ebx
  mov Yfloat, eax

  mov eax, DestWidth
  mov WorkX, eax
 @XLoop:
  mov edx, Xfloat
  shr edx, 16
  cmp edx, SrcWidth
  jae @SkipDraw
  mov eax, Yfloat
  shr eax, 16
  cmp eax, SrcHeight
  jae @SkipDraw

  mov esi, eax
  mov ebx, SrcBWidth
  imul esi, ebx
  add esi, SrcOfs
  shl edx, 1
  add esi, edx

  xor eax, eax
  mov ax, [esi]
  mov esi, eax

  mov dx, [esp]
  mov bx, dx
  and ax, 01Fh
  and bx, 01Fh
  add ax, bx
  cmp ax, 01Fh
  jb @Skip1
  mov ax, 01Fh
 @Skip1:
  mov cx, ax

  mov eax, esi
  mov bx, dx
  shr ax, 5
  shr bx, 5
  and ax, 03Fh
  and bx, 03Fh
  add ax, bx
  cmp ax, 03Fh
  jb @Skip2
  mov ax, 03Fh
 @Skip2:
  shl ax, 5
  or cx, ax

  mov eax, esi
  mov bx, dx
  shr ax, 11
  shr bx, 11
  and ax, 01Fh
  and bx, 01Fh
  add ax, bx
  cmp ax, 01Fh
  jb @Skip3
  mov ax, 01Fh
 @Skip3:
  shl ax, 11
  or cx, ax

  mov [esp], cx
 @SkipDraw:

  add esp, 2
  mov eax, CosVal
  add Xfloat, eax
  mov eax, SinVal
  add Yfloat, eax

  dec WorkX
  jnz @XLoop

  add esp, DstAdd

  inc WorkY
  dec DestHeight
  jnz @YLoop

  mov esp, _esp
  pop edi
  pop esi
  pop ebx
 end;
end;

procedure AddRotate32_AA(Src: TFastDIB; SrcRect: TRect; DestOfs, DstPitch, Xpos, Ypos, Angle, Scale: Integer; ClipRect: TRect);
Const Sqrt3 = 1.732050808;
Var HiLength, ImgX, ImgY, ImgW, ImgH: Integer;
    SrcOfs, SrcBWidth, DstOfs, DstAdd, J, I, SinVal, CosVal, xDiff, yDiff: Integer;
    SrcX, SrcY, StartX, StartY, SrcWidth, SrcHeight, DestWidth, DestHeight: Integer;
    Xfloat, Yfloat, Xint, Yint, Xadd, Yadd, Xfrac, Yfrac, CenterX, CenterY: Integer;
    _esp: Integer;
begin
 { getting source rectangle }
 SrcX:= SrcRect.Left;
 SrcY:= SrcRect.Top;
 SrcWidth:= SrcRect.Right - SrcX;
 SrcHeight:= SrcRect.Bottom - SrcY;
 CenterX:= SrcWidth shr 1;
 CenterY:= SrcHeight shr 1;
 { calculating destination area size }
 HiLength:= SrcWidth;
 if (SrcHeight > HiLength) then HiLength:= SrcHeight;
 HiLength:= (HiLength * Scale) div 256;
 HiLength:= HiLength + Round((Sqrt3 * HiLength) / 4);
 { destination width & height }
 DestWidth:= HiLength;
 DestHeight:= HiLength;
 { centering rotating image inside destination }
 xDiff:=DestWidth shr 1;
 yDiff:=DestHeight shr 1;
 { centering destination image }
 Xpos:= Xpos - (DestWidth shr 1);
 Ypos:= Ypos - (DestHeight shr 1);
 { starting offsets in source image }
 StartX:= 0; StartY:= 0;
 { performing clipping }
 if (Xpos < ClipRect.Left) then
  begin
   StartX:= StartX + (ClipRect.Left - Xpos);
   DestWidth:= DestWidth - (ClipRect.Left - Xpos);
   Xpos:= ClipRect.Left;
   if (DestWidth < 1) then Exit;
  end;
 if (Ypos < ClipRect.Top) then
  begin
   StartY:= StartY + (ClipRect.Top - Ypos);
   DestHeight:= DestHeight - (ClipRect.Top - Ypos);
   Ypos:= ClipRect.Top;
   if (DestHeight < 1) then Exit;
  end;
 if (Xpos + DestWidth > ClipRect.Right - 1) then
  begin
   DestWidth:= ClipRect.Right - Xpos - 1;
   if (DestWidth < 1) then Exit;
  end;
 if (Ypos + DestHeight > ClipRect.Bottom - 1) then
  begin
   DestHeight:= ClipRect.Bottom - Ypos - 1;
   if (DestHeight < 1) then Exit;
  end;

 SrcBWidth:= Src.BWidth;
 SrcOfs:= Integer(Src.Bits) + (SrcBWidth * SrcY) + (SrcX shl 2);

 DstOfs:= DestOfs + (DstPitch * Ypos) + (Xpos shl 2);
 DstAdd:= DstPitch - (DestWidth shl 2);

 SinVal:= Round((Sin(Angle * Pi / 128) / Scale) * 65536 * 256);
 CosVal:= Round((Cos(Angle * Pi / 128) / Scale) * 65536 * 256);
 Xadd:= (CenterX shl 16) - (SinVal * (StartY - yDiff)) + (CosVal * (StartX - xDiff));
 Yadd:= (CenterY shl 16) + (CosVal * (StartY - yDiff)) + (SinVal * (StartX - xDiff));

 for J:=0 to DestHeight - 1 do
  begin
   Xfloat:= Xadd - (SinVal * J);
   Yfloat:= Yadd + (CosVal * J);
   for I:=0 to DestWidth - 1 do
    begin
     Xint:= SmallInt(Xfloat shr 16);
     Yint:= SmallInt(Yfloat shr 16);
     if (Xint >= 0)and(Yint >= 0)and(Xint < SrcWidth - 1)and(Yint < SrcHeight - 1) then
      begin
       Xfrac:= Xfloat and $FFFF;
       Yfrac:= Yfloat and $FFFF;
       asm
        push ebx
        push esi
        push edi
        mov _esp,esp

        mov esi, SrcOfs
        mov eax, Yint
        mov ebx, SrcBWidth
        imul eax, ebx
        add esi, eax
        mov eax, Xint
        shl eax, 2
        add esi, eax
        mov edi, esi
        add edi, SrcBWidth
        { ESI - Top pixel, EDI - Bottom pixel }

        xor eax, eax
        xor ebx, ebx
        xor ecx, ecx
        xor edx, edx
        { BLUE }
        mov ebx, [esi]
        and ebx, 00FFh
        mov edx, ebx
        mov eax, [esi + 4]
        and eax, 00FFh
        sub eax, ebx
        mov ebx, Xfrac
        imul eax, ebx
        sar eax, 16
        add eax, edx

        mov ecx, eax

        mov ebx, [edi]
        and ebx, 00FFh
        mov edx, ebx
        mov eax, [edi + 4]
        and eax, 00FFh
        sub eax, ebx
        mov ebx, Xfrac
        imul eax, ebx
        sar eax, 16
        add eax, edx

        sub eax, ecx
        mov ebx, Yfrac
        imul eax, ebx
        sar eax, 16
        add eax, ecx
        mov esp, eax

        { GREEN }
        mov ebx, [esi]
        shr ebx, 8
        and ebx, 00FFh
        mov edx, ebx
        mov eax, [esi + 4]
        shr eax, 8
        and eax, 00FFh
        sub eax, ebx
        mov ebx, Xfrac
        imul eax, ebx
        sar eax, 16
        add eax, edx

        mov ecx, eax

        mov ebx, [edi]
        shr ebx, 8
        and ebx, 00FFh
        mov edx, ebx
        mov eax, [edi+4]
        shr eax, 8
        and eax, 00FFh
        sub eax, ebx
        mov ebx, Xfrac
        imul eax, ebx
        sar eax, 16
        add eax, edx

        sub eax, ecx
        mov ebx, Yfrac
        imul eax, ebx
        sar eax, 16
        add eax, ecx
        shl eax, 8
        or esp, eax

        { RED }
        mov ebx, [esi]
        shr ebx, 16
        and ebx, 00FFh
        mov edx, ebx
        mov eax, [esi+4]
        shr eax, 16
        and eax, 00FFh
        sub eax, ebx
        mov ebx, Xfrac
        imul eax, ebx
        sar eax, 16
        add eax, edx

        mov ecx, eax

        mov ebx, [edi]
        shr ebx, 16
        and ebx, 00FFh
        mov edx, ebx
        mov eax, [edi+4]
        shr eax, 16
        and eax, 00FFh
        sub eax, ebx
        mov ebx, Xfrac
        imul eax, ebx
        sar eax, 16
        add eax, edx

        sub eax, ecx
        mov ebx, Yfrac
        imul eax, ebx
        sar eax, 16
        add eax, ecx
        shl eax, 16
        or eax, esp

        mov edi, DstOfs

        mov esp, eax          // ESP - source color
        mov ebx, [edi]
        mov edx, ebx            // EDX - destination color

        and eax, 00FFh         // Adding RED
        and ebx, 00FFh
        add eax, ebx

        cmp eax, 00FFh
        jb @Skip1
        mov eax, 00FFh
       @Skip1:
        mov ecx, eax

        mov eax, esp          // Adding GREEN
        mov ebx, edx
        shr eax, 8
        shr ebx, 8
        and eax, 00FFh
        and ebx, 00FFh
        add eax, ebx

        cmp eax, 00FFh
        jb @Skip2
        mov eax, 00FFh
       @Skip2:
        shl eax, 8
        or ecx, eax

        mov eax, esp          // Adding BLUE
        mov ebx, edx
        shr eax, 16
        shr ebx, 16
        and eax, 00FFh
        and ebx, 00FFh
        add eax, ebx

        cmp eax, 00FFh
        jb @Skip3
        mov eax, 00FFh
       @Skip3:
        shl eax, 16
        or ecx, eax

        mov [edi], ecx

        mov esp,_esp
        pop edi
        pop esi
        pop ebx
       end;
      end;
     Inc(DstOfs, 4);
     Inc(Xfloat, CosVal);
     Inc(Yfloat, SinVal);
    end;
   Inc(DstOfs, DstAdd);
  end;
end;

procedure AddRotate32(Src: TFastDIB; SrcRect: TRect; DestOfs, DstPitch, Xpos, Ypos, Angle, Scale: Integer; ClipRect: TRect);
Const Sqrt3=1.732050808;
Var HiLength:Integer;
    SrcOfs,SrcBWidth,DstOfs,DstAdd,SinVal,CosVal,xDiff,yDiff:Integer;
    SrcX,SrcY,StartX,StartY,SrcWidth,SrcHeight,DestWidth,DestHeight:Integer;
    Xfloat,Yfloat,Xadd,Yadd,CenterX,CenterY,WorkX, WorkY:Integer;
    _esp:Integer;
begin
 { getting source rectangle }
 SrcX:= SrcRect.Left;
 SrcY:= SrcRect.Top;
 SrcWidth:= SrcRect.Right - SrcX;
 SrcHeight:= SrcRect.Bottom - SrcY;
 CenterX:= SrcWidth shr 1;
 CenterY:= SrcHeight shr 1;
 { calculating destination area size }
 HiLength:= SrcWidth;
 if (SrcHeight > HiLength) then HiLength:= SrcHeight;
 HiLength:= (HiLength * Scale) div 256;
 HiLength:= HiLength + Round((Sqrt3 * HiLength) / 4);
 { destination width & height }
 DestWidth:= HiLength;
 DestHeight:= HiLength;
 { centering rotating image inside destination }
 xDiff:=DestWidth shr 1;
 yDiff:=DestHeight shr 1;
 { centering destination image }
 Xpos:= Xpos - (DestWidth shr 1);
 Ypos:= Ypos - (DestHeight shr 1);
 { starting offsets in source image }
 StartX:= 0; StartY:= 0;
 { performing clipping }
 if (Xpos < ClipRect.Left) then
  begin
   StartX:= StartX + (ClipRect.Left - Xpos);
   DestWidth:= DestWidth - (ClipRect.Left - Xpos);
   Xpos:= ClipRect.Left;
   if (DestWidth < 1) then Exit;
  end;
 if (Ypos < ClipRect.Top) then
  begin
   StartY:= StartY + (ClipRect.Top - Ypos);
   DestHeight:= DestHeight - (ClipRect.Top - Ypos);
   Ypos:= ClipRect.Top;
   if (DestHeight < 1) then Exit;
  end;
 if (Xpos + DestWidth > ClipRect.Right - 1) then
  begin
   DestWidth:= ClipRect.Right - Xpos - 1;
   if (DestWidth < 1) then Exit;
  end;
 if (Ypos + DestHeight > ClipRect.Bottom - 1) then
  begin
   DestHeight:= ClipRect.Bottom - Ypos - 1;
   if (DestHeight < 1) then Exit;
  end;

 SrcBWidth:= Src.BWidth;
 SrcOfs:= Integer(Src.Bits) + (SrcBWidth * SrcY) + (SrcX shl 2);

 DstOfs:= DestOfs + (DstPitch * Ypos) + (Xpos shl 2);
 DstAdd:= DstPitch - (DestWidth shl 2);

 SinVal:= Round((Sin(Angle * Pi / 128) / Scale) * 65536 * 256);
 CosVal:= Round((Cos(Angle * Pi / 128) / Scale) * 65536 * 256);
 Xadd:= (CenterX shl 16) - (SinVal * (StartY - yDiff)) + (CosVal * (StartX - xDiff));
 Yadd:= (CenterY shl 16) + (CosVal * (StartY - yDiff)) + (SinVal * (StartX - xDiff));
 WorkY:= 0;

 asm
  push ebx
  push esi
  push edi
  mov _esp, esp
  mov esp, DstOfs

 @YLoop:
  mov edx, WorkY
  mov eax, SinVal
  imul eax, edx
  mov ebx, eax
  mov eax, Xadd
  sub eax, ebx
  mov Xfloat, eax

  mov eax, CosVal
  imul eax, edx
  mov ebx, eax
  mov eax, Yadd
  add eax, ebx
  mov Yfloat, eax

  mov eax, DestWidth
  mov WorkX, eax
 @XLoop:
  mov edx, Xfloat
  shr edx, 16
  cmp edx, SrcWidth
  jae @SkipDraw
  mov eax, Yfloat
  shr eax, 16
  cmp eax, SrcHeight
  jae @SkipDraw

  mov esi, eax
  mov ebx, SrcBWidth
  imul esi, ebx
  add esi, SrcOfs
  shl edx, 2
  add esi, edx

  xor eax, eax
  mov eax, [esi]
  mov esi, eax

  mov edx, [esp]
  mov ebx, edx

  and eax, 0FFh
  and ebx, 0FFh
  add eax, ebx
  cmp eax, 0FFh
  jb @Skip1
  mov eax, 0FFh
 @Skip1:
  mov ecx, eax

  mov eax, esi
  mov ebx, edx

  shr eax, 8
  shr ebx, 8
  and eax, 0FFh
  and ebx, 0FFh
  add eax, ebx
  cmp eax, 0FFh
  jb @Skip2
  mov eax, 0FFh
 @Skip2:
  shl eax, 8
  or ecx, eax

  mov eax, esi
  mov ebx, edx

  shr eax, 16
  shr ebx, 16
  and eax, 0FFh
  and ebx, 0FFh
  add eax, ebx
  cmp eax, 0FFh
  jb @Skip3
  mov eax, 0FFh
 @Skip3:
  shl eax, 16
  or ecx, eax

  mov [esp], ecx
 @SkipDraw:

  add esp, 4
  mov eax, CosVal
  add Xfloat, eax
  mov eax, SinVal
  add Yfloat, eax

  dec WorkX
  jnz @XLoop

  add esp, DstAdd

  inc WorkY
  dec DestHeight
  jnz @YLoop

  mov esp, _esp
  pop edi
  pop esi
  pop ebx
 end;
end;

procedure AddRotate16_AA_M1(Src: TFastDIB; SrcRect: TRect; DestOfs, DstPitch, Xpos, Ypos, Angle, Scale, Color: Integer; ClipRect: TRect);
Const Sqrt3 = 1.732050808;
Var HiLength, ImgX, ImgY, ImgW, ImgH: Integer;
    SrcOfs, SrcBWidth, DstOfs, DstAdd, J, I, SinVal, CosVal, xDiff, yDiff: Integer;
    SrcX, SrcY, StartX, StartY, SrcWidth, SrcHeight, DestWidth, DestHeight: Integer;
    Xfloat, Yfloat, Xint, Yint, Xadd, Yadd, Xfrac, Yfrac, CenterX, CenterY: Integer;
    _esp: Integer;
    Rv, Gv, Bv: Byte;
begin
 { getting source rectangle }
 SrcX:= SrcRect.Left;
 SrcY:= SrcRect.Top;
 SrcWidth:= SrcRect.Right - SrcX;
 SrcHeight:= SrcRect.Bottom - SrcY;
 CenterX:= SrcWidth shr 1;
 CenterY:= SrcHeight shr 1;
 { calculating destination area size }
 HiLength:= SrcWidth;
 if (SrcHeight > HiLength) then HiLength:= SrcHeight;
 HiLength:= (HiLength * Scale) div 256;
 HiLength:= HiLength + Round((Sqrt3 * HiLength) / 4);
 { destination width & height }
 DestWidth:= HiLength;
 DestHeight:= HiLength;
 { centering rotating image inside destination }
 xDiff:= DestWidth shr 1;
 yDiff:= DestHeight shr 1;
 { centering destination image }
 Xpos:= Xpos - (DestWidth shr 1);
 Ypos:= Ypos - (DestHeight shr 1);
 { starting offsets in source image }
 StartX:= 0; StartY:= 0;
 { performing clipping }
 if (Xpos < ClipRect.Left) then
  begin
   StartX:= StartX + (ClipRect.Left - Xpos);
   DestWidth:= DestWidth - (ClipRect.Left - Xpos);
   Xpos:= ClipRect.Left;
   if (DestWidth < 1) then Exit;
  end;
 if (Ypos < ClipRect.Top) then
  begin
   StartY:= StartY + (ClipRect.Top - Ypos);
   DestHeight:= DestHeight - (ClipRect.Top - Ypos);
   Ypos:= ClipRect.Top;
   if (DestHeight < 1) then Exit;
  end;
 if (Xpos + DestWidth > ClipRect.Right - 1) then
  begin
   DestWidth:= ClipRect.Right - Xpos - 1;
   if (DestWidth < 1) then Exit;
  end;
 if (Ypos + DestHeight > ClipRect.Bottom - 1) then
  begin
   DestHeight:= ClipRect.Bottom - Ypos - 1;
   if (DestHeight < 1) then Exit;
  end;

 Rv:= Color and $FF;
 Gv:= (Color shr 8) and $FF;
 Bv:= (Color shr 16) and $FF;

 SrcBWidth:= Src.BWidth;
 SrcOfs:= Integer(Src.Bits) + (SrcBWidth * SrcY) + (SrcX shl 1);

 DstOfs:= DestOfs + (DstPitch * Ypos) + (Xpos shl 1);
 DstAdd:= DstPitch - (DestWidth shl 1);

 SinVal:= Round((Sin(Angle * Pi / 128) / Scale) * 65536 * 256);
 CosVal:= Round((Cos(Angle * Pi / 128) / Scale) * 65536 * 256);
 Xadd:= (CenterX shl 16) - (SinVal * (StartY - yDiff)) + (CosVal * (StartX - xDiff));
 Yadd:= (CenterY shl 16) + (CosVal * (StartY - yDiff)) + (SinVal * (StartX - xDiff));

 for J:=0 to DestHeight - 1 do
  begin
   Xfloat:= Xadd - (SinVal * J);
   Yfloat:= Yadd + (CosVal * J);
   for I:=0 to DestWidth - 1 do
    begin
     Xint:= SmallInt(Xfloat shr 16);
     Yint:= SmallInt(Yfloat shr 16);
     if (Xint >= 0)and(Yint >= 0)and(Xint < SrcWidth - 1)and(Yint < SrcHeight - 1) then
      begin
       Xfrac:= Xfloat and $FFFF;
       Yfrac:= Yfloat and $FFFF;
       asm
        push ebx
        push esi
        push edi
        mov _esp, esp

        mov esi, SrcOfs
        mov eax, Yint
        mov ebx, SrcBWidth
        imul eax, ebx
        add esi, eax
        mov eax, Xint
        shl eax, 1
        add esi, eax
        mov edi, esi
        add edi, SrcBWidth
        { ESI - Top pixel, EDI - Bottom pixel }

        xor eax,eax
        xor ebx,ebx
        xor ecx,ecx
        xor edx,edx
        { RED }
        mov bx,[esi]
        and bx,001Fh
        mov edx,ebx
        mov ax,[esi+2]
        and ax,001Fh
        sub eax,ebx
        mov ebx,Xfrac
        imul eax,ebx
        sar eax,16
        add eax,edx

        mov ecx,eax

        mov bx,[edi]
        and bx,001Fh
        mov edx,ebx
        mov ax,[edi+2]
        and ax,001Fh
        sub eax,ebx
        mov ebx,Xfrac
        imul eax,ebx
        sar eax,16
        add eax,edx

        sub eax,ecx
        mov ebx, Yfrac
        imul eax,ebx
        sar eax, 16
        add eax, ecx

        mul Bv
        shr eax, 8

        mov esp,eax

        { GREEN }
        mov bx,[esi]
        shr bx,5
        and bx,003Fh
        mov edx,ebx
        mov ax,[esi+2]
        shr ax,5
        and ax,003Fh
        sub eax,ebx
        mov ebx,Xfrac
        imul eax,ebx
        sar eax,16
        add eax,edx

        mov ecx,eax

        mov bx,[edi]
        shr bx,5
        and bx,003Fh
        mov edx,ebx
        mov ax,[edi+2]
        shr ax,5
        and ax,003Fh
        sub eax,ebx
        mov ebx,Xfrac
        imul eax,ebx
        sar eax,16
        add eax,edx

        sub eax,ecx
        mov ebx, Yfrac
        imul eax,ebx
        sar eax,16
        add eax,ecx

        mul Gv
        shr eax, 8

        shl eax, 5
        or esp, eax

        { BLUE }
        mov bx,[esi]
        shr bx,11
        and bx,001Fh
        mov edx,ebx
        mov ax,[esi+2]
        shr ax,11
        and ax,001Fh
        sub eax,ebx
        mov ebx,Xfrac
        imul eax,ebx
        sar eax,16
        add eax,edx

        mov ecx,eax

        mov bx,[edi]
        shr bx,11
        and bx,001Fh
        mov edx,ebx
        mov ax,[edi+2]
        shr ax,11
        and ax,001Fh
        sub eax,ebx
        mov ebx,Xfrac
        imul eax,ebx
        sar eax,16
        add eax,edx

        sub eax,ecx
        mov ebx, Yfrac
        imul eax,ebx
        sar eax,16
        add eax,ecx

        mul Rv
        shr eax, 8

        shl eax,11
        or eax,esp

        mov edi, DstOfs

        mov esp, eax          // ESP - source color
        mov bx, [edi]
        mov dx, bx            // EDX - destination color

        and ax, 001Fh         // Adding RED
        and bx, 001Fh
        add ax, bx

        cmp ax, 001Fh
        jb @Skip1
        mov ax, 001Fh
       @Skip1:
        mov cx, ax

        mov eax, esp          // Adding GREEN
        mov bx, dx
        shr ax, 5
        shr bx, 5
        and ax, 003Fh
        and bx, 003Fh
        add ax,bx

        cmp ax, 003Fh
        jb @Skip2
        mov ax, 003Fh
       @Skip2:
        shl ax,5
        or cx, ax

        mov eax, esp          // Adding BLUE
        mov bx, dx
        shr ax, 11
        shr bx, 11
        and ax, 001Fh
        and bx, 001Fh
        add ax,bx

        cmp ax, 001Fh
        jb @Skip3
        mov ax, 001Fh
       @Skip3:
        shl ax,11
        or cx, ax

        mov [edi],cx

        mov esp,_esp
        pop edi
        pop esi
        pop ebx
       end;
      end;
     Inc(DstOfs, 2);
     Inc(Xfloat, CosVal);
     Inc(Yfloat, SinVal);
    end;
   Inc(DstOfs, DstAdd);
  end;
end;

procedure AddRotate16_M1(Src: TFastDIB; SrcRect: TRect; DestOfs, DstPitch, Xpos, Ypos, Angle, Scale, Color: Integer; ClipRect: TRect);
Const Sqrt3=1.732050808;
Var HiLength:Integer;
    SrcOfs,SrcBWidth,DstOfs,DstAdd,SinVal,CosVal,xDiff,yDiff:Integer;
    SrcX,SrcY,StartX,StartY,SrcWidth,SrcHeight,DestWidth,DestHeight:Integer;
    Xfloat,Yfloat,Xadd,Yadd,CenterX,CenterY,WorkX, WorkY:Integer;
    _esp:Integer;
    Rv, Gv, Bv: Byte;
begin
 { getting source rectangle }
 SrcX:= SrcRect.Left;
 SrcY:= SrcRect.Top;
 SrcWidth:= SrcRect.Right - SrcX;
 SrcHeight:= SrcRect.Bottom - SrcY;
 CenterX:= SrcWidth shr 1;
 CenterY:= SrcHeight shr 1;
 { calculating destination area size }
 HiLength:= SrcWidth;
 if (SrcHeight > HiLength) then HiLength:= SrcHeight;
 HiLength:= (HiLength * Scale) div 256;
 HiLength:= HiLength + Round((Sqrt3 * HiLength) / 4);
 { destination width & height }
 DestWidth:= HiLength;
 DestHeight:= HiLength;
 { centering rotating image inside destination }
 xDiff:=DestWidth shr 1;
 yDiff:=DestHeight shr 1;
 { centering destination image }
 Xpos:= Xpos - (DestWidth shr 1);
 Ypos:= Ypos - (DestHeight shr 1);
 { starting offsets in source image }
 StartX:= 0; StartY:= 0;
 { performing clipping }
 if (Xpos < ClipRect.Left) then
  begin
   StartX:= StartX + (ClipRect.Left - Xpos);
   DestWidth:= DestWidth - (ClipRect.Left - Xpos);
   Xpos:= ClipRect.Left;
   if (DestWidth < 1) then Exit;
  end;
 if (Ypos < ClipRect.Top) then
  begin
   StartY:= StartY + (ClipRect.Top - Ypos);
   DestHeight:= DestHeight - (ClipRect.Top - Ypos);
   Ypos:= ClipRect.Top;
   if (DestHeight < 1) then Exit;
  end;
 if (Xpos + DestWidth > ClipRect.Right - 1) then
  begin
   DestWidth:= ClipRect.Right - Xpos - 1;
   if (DestWidth < 1) then Exit;
  end;
 if (Ypos + DestHeight > ClipRect.Bottom - 1) then
  begin
   DestHeight:= ClipRect.Bottom - Ypos - 1;
   if (DestHeight < 1) then Exit;
  end;

 Rv:= Color and $FF;
 Gv:= (Color shr 8) and $FF;
 Bv:= (Color shr 16) and $FF;

 SrcBWidth:= Src.BWidth;
 SrcOfs:= Integer(Src.Bits) + (SrcBWidth * SrcY) + (SrcX shl 1);

 DstOfs:= DestOfs + (DstPitch * Ypos) + (Xpos shl 1);
 DstAdd:= DstPitch - (DestWidth shl 1);

 SinVal:= Round((Sin(Angle * Pi / 128) / Scale) * 65536 * 256);
 CosVal:= Round((Cos(Angle * Pi / 128) / Scale) * 65536 * 256);
 Xadd:= (CenterX shl 16) - (SinVal * (StartY - yDiff)) + (CosVal * (StartX - xDiff));
 Yadd:= (CenterY shl 16) + (CosVal * (StartY - yDiff)) + (SinVal * (StartX - xDiff));
 WorkY:= 0;

 asm
  push ebx
  push esi
  push edi
  mov _esp, esp
  mov esp, DstOfs

 @YLoop:
  mov edx, WorkY
  mov eax, SinVal
  imul eax, edx
  mov ebx, eax
  mov eax, Xadd
  sub eax, ebx
  mov Xfloat, eax

  mov eax, CosVal
  imul eax, edx
  mov ebx, eax
  mov eax, Yadd
  add eax, ebx
  mov Yfloat, eax

  mov eax, DestWidth
  mov WorkX, eax
 @XLoop:
  mov edx, Xfloat
  shr edx, 16
  cmp edx, SrcWidth
  jae @SkipDraw
  mov eax, Yfloat
  shr eax, 16
  cmp eax, SrcHeight
  jae @SkipDraw

  mov esi, eax
  mov ebx, SrcBWidth
  imul esi, ebx
  add esi, SrcOfs
  shl edx, 1
  add esi, edx

  xor eax, eax
  mov ax, [esi]
  mov esi, eax

  mov dx, [esp]
  mov bx, dx
  and ax, 01Fh

  mul Bv
  shr ax, 8

  and bx, 01Fh
  add ax, bx
  cmp ax, 01Fh
  jb @Skip1
  mov ax, 01Fh
 @Skip1:

  mov cx, ax

  mov eax, esi
  mov bx, dx
  shr ax, 5
  shr bx, 5
  and ax, 03Fh

  mul Gv
  shr ax, 8

  and bx, 03Fh
  add ax, bx
  cmp ax, 03Fh
  jb @Skip2
  mov ax, 03Fh
 @Skip2:
  shl ax, 5
  or cx, ax

  mov eax, esi
  mov bx, dx
  shr ax, 11
  shr bx, 11
  and ax, 01Fh

  mul Rv
  shr ax, 8

  and bx, 01Fh
  add ax, bx
  cmp ax, 01Fh
  jb @Skip3
  mov ax, 01Fh
 @Skip3:
  shl ax, 11
  or cx, ax

  mov [esp], cx
 @SkipDraw:

  add esp, 2
  mov eax, CosVal
  add Xfloat, eax
  mov eax, SinVal
  add Yfloat, eax

  dec WorkX
  jnz @XLoop

  add esp, DstAdd

  inc WorkY
  dec DestHeight
  jnz @YLoop

  mov esp, _esp
  pop edi
  pop esi
  pop ebx
 end;
end;

procedure AddRotate32_AA_M1(Src: TFastDIB; SrcRect: TRect; DestOfs, DstPitch, Xpos, Ypos, Angle, Scale, Color: Integer; ClipRect: TRect);
Const Sqrt3 = 1.732050808;
Var HiLength, ImgX, ImgY, ImgW, ImgH: Integer;
    SrcOfs, SrcBWidth, DstOfs, DstAdd, J, I, SinVal, CosVal, xDiff, yDiff: Integer;
    SrcX, SrcY, StartX, StartY, SrcWidth, SrcHeight, DestWidth, DestHeight: Integer;
    Xfloat, Yfloat, Xint, Yint, Xadd, Yadd, Xfrac, Yfrac, CenterX, CenterY: Integer;
    _esp: Integer;
    Rv, Gv, Bv: Byte;
begin

 { getting source rectangle }
 SrcX:= SrcRect.Left;
 SrcY:= SrcRect.Top;
 SrcWidth:= SrcRect.Right - SrcX;
 SrcHeight:= SrcRect.Bottom - SrcY;
 CenterX:= SrcWidth shr 1;
 CenterY:= SrcHeight shr 1;
 { calculating destination area size }
 HiLength:= SrcWidth;
 if (SrcHeight > HiLength) then HiLength:= SrcHeight;
 HiLength:= (HiLength * Scale) div 256;
 HiLength:= HiLength + Round((Sqrt3 * HiLength) / 4);
 { destination width & height }
 DestWidth:= HiLength;
 DestHeight:= HiLength;
 { centering rotating image inside destination }
 xDiff:=DestWidth shr 1;
 yDiff:=DestHeight shr 1;
 { centering destination image }
 Xpos:= Xpos - (DestWidth shr 1);
 Ypos:= Ypos - (DestHeight shr 1);
 { starting offsets in source image }
 StartX:= 0; StartY:= 0;
 { performing clipping }
 if (Xpos < ClipRect.Left) then
  begin
   StartX:= StartX + (ClipRect.Left - Xpos);
   DestWidth:= DestWidth - (ClipRect.Left - Xpos);
   Xpos:= ClipRect.Left;
   if (DestWidth < 1) then Exit;
  end;
 if (Ypos < ClipRect.Top) then
  begin
   StartY:= StartY + (ClipRect.Top - Ypos);
   DestHeight:= DestHeight - (ClipRect.Top - Ypos);
   Ypos:= ClipRect.Top;
   if (DestHeight < 1) then Exit;
  end;
 if (Xpos + DestWidth > ClipRect.Right - 1) then
  begin
   DestWidth:= ClipRect.Right - Xpos - 1;
   if (DestWidth < 1) then Exit;
  end;
 if (Ypos + DestHeight > ClipRect.Bottom - 1) then
  begin
   DestHeight:= ClipRect.Bottom - Ypos - 1;
   if (DestHeight < 1) then Exit;
  end;

 Rv:= Color and $FF;
 Gv:= (Color shr 8) and $FF;
 Bv:= (Color shr 16) and $FF;

 Assert(Src.Bits<>nil,'Fehler in Powerdraw');
 SrcBWidth:= Src.BWidth;
 SrcOfs:= Integer(Src.Bits) + (SrcBWidth * SrcY) + (SrcX shl 2);

 DstOfs:= DestOfs + (DstPitch * Ypos) + (Xpos shl 2);
 DstAdd:= DstPitch - (DestWidth shl 2);

 SinVal:= Round((Sin(Angle * Pi / 128) / Scale) * 65536 * 256);
 CosVal:= Round((Cos(Angle * Pi / 128) / Scale) * 65536 * 256);
 Xadd:= (CenterX shl 16) - (SinVal * (StartY - yDiff)) + (CosVal * (StartX - xDiff));
 Yadd:= (CenterY shl 16) + (CosVal * (StartY - yDiff)) + (SinVal * (StartX - xDiff));

 for J:=0 to DestHeight - 1 do
  begin
   Xfloat:= Xadd - (SinVal * J);
   Yfloat:= Yadd + (CosVal * J);
   for I:=0 to DestWidth - 1 do
    begin
     Xint:= SmallInt(Xfloat shr 16);
     Yint:= SmallInt(Yfloat shr 16);
     if (Xint >= 0)and(Yint >= 0)and(Xint < SrcWidth - 1)and(Yint < SrcHeight - 1) then
      begin
       Xfrac:= Xfloat and $FFFF;
       Yfrac:= Yfloat and $FFFF;
       asm
        push ebx
        push esi
        push edi
        mov _esp,esp

        mov esi, SrcOfs
        mov eax, Yint
        mov ebx, SrcBWidth
        imul eax, ebx
        add esi, eax
        mov eax, Xint
        shl eax, 2
        add esi, eax
        mov edi, esi
        add edi, SrcBWidth
        { ESI - Top pixel, EDI - Bottom pixel }

        xor eax, eax
        xor ebx, ebx
        xor ecx, ecx
        xor edx, edx
        { BLUE }
        mov ebx, [esi]
        and ebx, 00FFh
        mov edx, ebx
        mov eax, [esi + 4]
        and eax, 00FFh
        sub eax, ebx
        mov ebx, Xfrac
        imul eax, ebx
        sar eax, 16
        add eax, edx

        mov ecx, eax

        mov ebx, [edi]
        and ebx, 00FFh
        mov edx, ebx
        mov eax, [edi + 4]
        and eax, 00FFh
        sub eax, ebx
        mov ebx, Xfrac
        imul eax, ebx
        sar eax, 16
        add eax, edx

        sub eax, ecx
        mov ebx, Yfrac
        imul eax, ebx
        sar eax, 16
        add eax, ecx

        mul Bv
        shr eax, 8

        mov esp, eax

        { GREEN }
        mov ebx, [esi]
        shr ebx, 8
        and ebx, 00FFh
        mov edx, ebx
        mov eax, [esi + 4]
        shr eax, 8
        and eax, 00FFh
        sub eax, ebx
        mov ebx, Xfrac
        imul eax, ebx
        sar eax, 16
        add eax, edx

        mov ecx, eax

        mov ebx, [edi]
        shr ebx, 8
        and ebx, 00FFh
        mov edx, ebx
        mov eax, [edi+4]
        shr eax, 8
        and eax, 00FFh
        sub eax, ebx
        mov ebx, Xfrac
        imul eax, ebx
        sar eax, 16
        add eax, edx

        sub eax, ecx
        mov ebx, Yfrac
        imul eax, ebx
        sar eax, 16
        add eax, ecx

        mul Gv
        shr eax, 8

        shl eax, 8
        or esp, eax

        { RED }
        mov ebx, [esi]
        shr ebx, 16
        and ebx, 00FFh
        mov edx, ebx
        mov eax, [esi+4]
        shr eax, 16
        and eax, 00FFh
        sub eax, ebx
        mov ebx, Xfrac
        imul eax, ebx
        sar eax, 16
        add eax, edx

        mov ecx, eax

        mov ebx, [edi]
        shr ebx, 16
        and ebx, 00FFh
        mov edx, ebx
        mov eax, [edi+4]
        shr eax, 16
        and eax, 00FFh
        sub eax, ebx
        mov ebx, Xfrac
        imul eax, ebx
        sar eax, 16
        add eax, edx

        sub eax, ecx
        mov ebx, Yfrac
        imul eax, ebx
        sar eax, 16
        add eax, ecx

        mul Rv
        shr eax, 8

        shl eax, 16
        or eax, esp

        mov edi, DstOfs

        mov esp, eax          // ESP - source color
        mov ebx, [edi]
        mov edx, ebx            // EDX - destination color

        and eax, 00FFh         // Adding RED
        and ebx, 00FFh
        add eax, ebx

        cmp eax, 00FFh
        jb @Skip1
        mov eax, 00FFh
       @Skip1:
        mov ecx, eax

        mov eax, esp          // Adding GREEN
        mov ebx, edx
        shr eax, 8
        shr ebx, 8
        and eax, 00FFh
        and ebx, 00FFh
        add eax, ebx

        cmp eax, 00FFh
        jb @Skip2
        mov eax, 00FFh
       @Skip2:
        shl eax, 8
        or ecx, eax

        mov eax, esp          // Adding BLUE
        mov ebx, edx
        shr eax, 16
        shr ebx, 16
        and eax, 00FFh
        and ebx, 00FFh
        add eax, ebx

        cmp eax, 00FFh
        jb @Skip3
        mov eax, 00FFh
       @Skip3:
        shl eax, 16
        or ecx, eax

        mov [edi], ecx

        mov esp,_esp
        pop edi
        pop esi
        pop ebx
       end;
      end;
     Inc(DstOfs, 4);
     Inc(Xfloat, CosVal);
     Inc(Yfloat, SinVal);
    end;
   Inc(DstOfs, DstAdd);
  end;
end;

procedure AddRotate32_M1(Src: TFastDIB; SrcRect: TRect; DestOfs, DstPitch, Xpos, Ypos, Angle, Scale, Color: Integer; ClipRect: TRect);
Const Sqrt3=1.732050808;
Var HiLength:Integer;
    SrcOfs,SrcBWidth,DstOfs,DstAdd,SinVal,CosVal,xDiff,yDiff:Integer;
    SrcX,SrcY,StartX,StartY,SrcWidth,SrcHeight,DestWidth,DestHeight:Integer;
    Xfloat,Yfloat,Xadd,Yadd,CenterX,CenterY,WorkX, WorkY:Integer;
    _esp:Integer;
    Rv, Gv, Bv: Byte;
begin
 { getting source rectangle }
 SrcX:= SrcRect.Left;
 SrcY:= SrcRect.Top;
 SrcWidth:= SrcRect.Right - SrcX;
 SrcHeight:= SrcRect.Bottom - SrcY;
 CenterX:= SrcWidth shr 1;
 CenterY:= SrcHeight shr 1;
 { calculating destination area size }
 HiLength:= SrcWidth;
 if (SrcHeight > HiLength) then HiLength:= SrcHeight;
 HiLength:= (HiLength * Scale) div 256;
 HiLength:= HiLength + Round((Sqrt3 * HiLength) / 4);
 { destination width & height }
 DestWidth:= HiLength;
 DestHeight:= HiLength;
 { centering rotating image inside destination }
 xDiff:=DestWidth shr 1;
 yDiff:=DestHeight shr 1;
 { centering destination image }
 Xpos:= Xpos - (DestWidth shr 1);
 Ypos:= Ypos - (DestHeight shr 1);
 { starting offsets in source image }
 StartX:= 0; StartY:= 0;
 { performing clipping }
 if (Xpos < ClipRect.Left) then
  begin
   StartX:= StartX + (ClipRect.Left - Xpos);
   DestWidth:= DestWidth - (ClipRect.Left - Xpos);
   Xpos:= ClipRect.Left;
   if (DestWidth < 1) then Exit;
  end;
 if (Ypos < ClipRect.Top) then
  begin
   StartY:= StartY + (ClipRect.Top - Ypos);
   DestHeight:= DestHeight - (ClipRect.Top - Ypos);
   Ypos:= ClipRect.Top;
   if (DestHeight < 1) then Exit;
  end;
 if (Xpos + DestWidth > ClipRect.Right - 1) then
  begin
   DestWidth:= ClipRect.Right - Xpos - 1;
   if (DestWidth < 1) then Exit;
  end;
 if (Ypos + DestHeight > ClipRect.Bottom - 1) then
  begin
   DestHeight:= ClipRect.Bottom - Ypos - 1;
   if (DestHeight < 1) then Exit;
  end;

 Rv:= Color and $FF;
 Gv:= (Color shr 8) and $FF;
 Bv:= (Color shr 16) and $FF;

 SrcBWidth:= Src.BWidth;
 SrcOfs:= Integer(Src.Bits) + (SrcBWidth * SrcY) + (SrcX shl 2);

 DstOfs:= DestOfs + (DstPitch * Ypos) + (Xpos shl 2);
 DstAdd:= DstPitch - (DestWidth shl 2);

 SinVal:= Round((Sin(Angle * Pi / 128) / Scale) * 65536 * 256);
 CosVal:= Round((Cos(Angle * Pi / 128) / Scale) * 65536 * 256);
 Xadd:= (CenterX shl 16) - (SinVal * (StartY - yDiff)) + (CosVal * (StartX - xDiff));
 Yadd:= (CenterY shl 16) + (CosVal * (StartY - yDiff)) + (SinVal * (StartX - xDiff));
 WorkY:= 0;

 asm
  push ebx
  push esi
  push edi
  mov _esp, esp
  mov esp, DstOfs

 @YLoop:
  mov edx, WorkY
  mov eax, SinVal
  imul eax, edx
  mov ebx, eax
  mov eax, Xadd
  sub eax, ebx
  mov Xfloat, eax

  mov eax, CosVal
  imul eax, edx
  mov ebx, eax
  mov eax, Yadd
  add eax, ebx
  mov Yfloat, eax

  mov eax, DestWidth
  mov WorkX, eax
 @XLoop:
  mov edx, Xfloat
  shr edx, 16
  cmp edx, SrcWidth
  jae @SkipDraw
  mov eax, Yfloat
  shr eax, 16
  cmp eax, SrcHeight
  jae @SkipDraw

  mov esi, eax
  mov ebx, SrcBWidth
  imul esi, ebx
  add esi, SrcOfs
  shl edx, 2
  add esi, edx

  xor eax, eax
  mov eax, [esi]
  mov esi, eax

  mov edx, [esp]
  mov ebx, edx

  and eax, 0FFh

  mul Bv
  shr eax, 8

  and ebx, 0FFh
  add eax, ebx
  cmp eax, 0FFh
  jb @Skip1
  mov eax, 0FFh
 @Skip1:
  mov ecx, eax

  mov eax, esi
  mov ebx, edx

  shr eax, 8
  shr ebx, 8
  and eax, 0FFh

  mul Gv
  shr eax, 8

  and ebx, 0FFh
  add eax, ebx
  cmp eax, 0FFh
  jb @Skip2
  mov eax, 0FFh
 @Skip2:
  shl eax, 8
  or ecx, eax

  mov eax, esi
  mov ebx, edx

  shr eax, 16
  shr ebx, 16
  and eax, 0FFh

  mul Rv
  shr eax, 8

  and ebx, 0FFh
  add eax, ebx
  cmp eax, 0FFh
  jb @Skip3
  mov eax, 0FFh
 @Skip3:
  shl eax, 16
  or ecx, eax

  mov [esp], ecx
 @SkipDraw:

  add esp, 4
  mov eax, CosVal
  add Xfloat, eax
  mov eax, SinVal
  add Yfloat, eax

  dec WorkX
  jnz @XLoop

  add esp, DstAdd

  inc WorkY
  dec DestHeight
  jnz @YLoop

  mov esp, _esp
  pop edi
  pop esi
  pop ebx
 end;
end;


end.
