// BASSWMA 1.8 Delphi API
// -----------------------------
// This unit is a Modified version of BASSWMA.pas written by Mr. Ian Luck, to
// support dynamic loading & unloading.
//    rewriter : Silhwan Hyun

Unit RT_BASSWMA;

interface

uses windows, rt_bass;

const
  // Additional error codes returned by BASS_WMA_GetErrorCode
  BASS_ERROR_WMA_LICENSE     = 1000;    // the file is protected

  // Additional flags for use with BASS_WMA_EncodeOpenFile/Network
  BASS_WMA_ENCODE_TAGS       = $10000; // set tags in the WMA encoding
  BASS_WMA_ENCODE_SCRIPT     = $20000; // set script (mid-stream tags) in the WMA encoding

  // Additional flag for use with BASS_WMA_EncodeGetRates
  BASS_WMA_ENCODE_RATES_VBR  = $10000; // get available VBR quality settings

type
  HWMENCODE = DWORD;		// WMA encoding handle


  CLIENTCONNECTPROC = procedure(handle:HWMENCODE; connect:BOOL; ip:PChar; user:DWORD); stdcall;
  {
    Client connection notification callback function.
    handle : The encoder
    connect: TRUE=client is connecting, FALSE=disconnecting
    ip     : The client's IP (xxx.xxx.xxx.xxx:port)
    user   : The 'user' parameter value given when calling BASS_EncodeSetNotify */
  }


var

 BASS_WMA_StreamCreateFile : function(mem:BOOL; fl:pointer; offset,length,flags:DWORD): HSTREAM; stdcall;
{
  Create a sample stream from a WMA file (or URL).
  mem    : TRUE = Stream file from memory
  fl     : Filename (mem=FALSE) or memory location (mem=TRUE)
  offset : ignored (set to 0)
  length : Data length (only used if mem=TRUE)
  flags  : Flags (BASS_SAMPLE_LOOP/BASS_SAMPLE_3D/BASS_SAMPLE_FX/BASS_STREAM_DECODE)
  RETURN : The created stream's handle (NULL=error)
}

 BASS_WMA_ChannelSetSync : function(handle,atype:DWORD; param:QWORD; proc:SYNCPROC; user:DWORD): HSYNC; stdcall;
{
  Setup a sync on a WMA channel. Multiple syncs may be used per channel.
  handle : Channel handle
  atype  : Sync type (BASS_SYNC_xxx type & flags)
  param  : Sync parameters (see the BASS_SYNC_xxx type description)
  proc   : User defined callback function
  user   : The 'user' value passed to the callback function
  RETURN : Sync handle (NULL=error)
}

 BASS_WMA_GetIWMReader : function(handle:HSTREAM): Pointer; stdcall;
{
  Retrieve the IWMReader interface of a WMA stream. This allows direct
  access to the WMFSDK functions.
  handle : Channel handle
  RETURN : Pointer to the IWMReader object interface (NULL=error)
}

 BASS_WMA_EncodeGetRates : function(freq,flags:DWORD): Pointer; stdcall;
{
  Retrieve a list of the encoding bitrates available for a
  specified input sample format.
  freq   : Sampling rate
  flags  : BASS_SAMPLE_MONO/BASS_WMA_ENCODE_RATES_VBR flags
  RETURN : Pointer to an array of bitrates, terminated by a 0 (NULL=error)
}
 BASS_WMA_EncodeOpenFile : function(freq,flags,bitrate:DWORD; fname:PChar): HWMENCODE; stdcall;
{
  Initialize WMA encoding to a file.
  freq   : Sampling rate
  flags  : BASS_SAMPLE_MONO/BASS_WMA_ENCODE_TAGS/BASS_WMA_ENCODE_SCRIPT flags
  bitrate: Encoding bitrate
  fname  : Filename
  RETURN : The created encoder's handle (0=error)
}
 BASS_WMA_EncodeOpenNetwork : function(freq,flags,bitrate,port,clients:DWORD): HWMENCODE; stdcall;
{
  Initialize WMA encoding to the network.
  freq   : Sampling rate
  flags  : BASS_SAMPLE_MONO/BASS_WMA_ENCODE_TAGS/BASS_WMA_ENCODE_SCRIPT flags
  bitrate: Encoding bitrate
  port   : Port number for clients to conenct to (0=let system choose)
  clients: Maximum number of clients that can connect
  RETURN : The created encoder's handle (0=error)
}
 BASS_WMA_EncodeGetPort : function(handle:HWMENCODE): DWORD; stdcall;
{
  Retrieve the port for clients to connect to a network encoder.
  handle : Encoder handle
  RETURN : The port number for clients to connect to (0=error)
}
 BASS_WMA_EncodeSetNotify : function(handle:HWMENCODE; proc:CLIENTCONNECTPROC; user:DWORD): BOOL; stdcall;
{
  Set a client connection notification callback.
  handle : Encoder handle
  proc   : User defined notification function
  user   : The 'user' value passed to the callback function
}
 BASS_WMA_EncodeGetClients : function(handle:HWMENCODE): DWORD; stdcall;
{
  Retrieve the number of clients connected.
  handle : Encoder handle
  RETURN : The number of clients (-1=error)
}
 BASS_WMA_EncodeSetTag : function(handle:HWMENCODE; tag,text:PChar): BOOL; stdcall;
{
  Set a tag. Requires that the BASS_WMA_ENCODE_TAGS flag was used in
  the BASS_WMA_EncodeOpenFile/Network call.
  handle : Encoder handle
  tag    : The tag (NULL=no more tags)
  text   : The tag's text
}
 BASS_WMA_EncodeWrite : function(handle:HWMENCODE; buffer:Pointer; length:DWORD): BOOL; stdcall;
{
  Encode sample data and write it to the file or network.
  handle : Encoder handle
  buffer : Buffer containing the sample data
  length : Number of bytes in the buffer
}
 BASS_WMA_EncodeClose : procedure(handle:HWMENCODE); stdcall;
{
  Finish encoding and close the file or network port.
  handle : Encoder handle
}

var BASSWMA_Handle : Thandle = 0;

function Load_BASSWMADLL(const dllpath : string; const dllfilename : string) : boolean;
procedure Unload_BASSWMADLL;

implementation

function Load_BASSWMADLL(const dllpath : string; const dllfilename : string) : boolean;
var
   DirPath : string;
   pt : pchar;
   oldmode : integer;
begin
   result := false;
   if BASSWMA_Handle <> 0 then // is it already there ?
   begin
      result := true;
      exit;
   end;

  // go & load the dll
   if (dllpath[length(dllpath)] <> '\') then
      DirPath := dllpath + '\'
   else
      DirPath := dllpath; // path with \ or without... add a \ if there isn't one...
   pt := pchar(DirPath + dllfilename); // now cast it a bit
   oldmode := SetErrorMode($8001);
   BASSWMA_Handle := LoadLibrary(pt);  // obtain the handle we want
   SetErrorMode(oldmode);

   if BASSWMA_Handle = 0 then          // dll was not loaded
      exit;

   @BASS_WMA_StreamCreateFile := GetProcAddress(BASSWMA_Handle, 'BASS_WMA_StreamCreateFile');
   @BASS_WMA_ChannelSetSync := GetProcAddress(BASSWMA_Handle, 'BASS_WMA_ChannelSetSync');
   @BASS_WMA_GetIWMReader := GetProcAddress(BASSWMA_Handle, 'BASS_WMA_GetIWMReader');
   @BASS_WMA_EncodeGetRates := GetProcAddress(BASSWMA_Handle, 'BASS_WMA_EncodeGetRates');
   @BASS_WMA_EncodeOpenFile := GetProcAddress(BASSWMA_Handle, 'BASS_WMA_EncodeOpenFile');
   @BASS_WMA_EncodeOpenNetwork := GetProcAddress(BASSWMA_Handle, 'BASS_WMA_EncodeOpenNetwork');
   @BASS_WMA_EncodeGetPort := GetProcAddress(BASSWMA_Handle, 'BASS_WMA_EncodeGetPort');
   @BASS_WMA_EncodeSetNotify := GetProcAddress(BASSWMA_Handle, 'BASS_WMA_EncodeSetNotify');
   @BASS_WMA_EncodeGetClients := GetProcAddress(BASSWMA_Handle, 'BASS_WMA_EncodeGetClients');
   @BASS_WMA_EncodeSetTag := GetProcAddress(BASSWMA_Handle, 'BASS_WMA_EncodeSetTag');
   @BASS_WMA_EncodeWrite := GetProcAddress(BASSWMA_Handle, 'BASS_WMA_EncodeWrite');
   @BASS_WMA_EncodeClose := GetProcAddress(BASSWMA_Handle, 'BASS_WMA_EncodeClose');

 // check if everything is linked in correctly
   if (@BASS_WMA_StreamCreateFile = nil) or
      (@BASS_WMA_ChannelSetSync = nil) or
      (@BASS_WMA_GetIWMReader = nil) or
      (@BASS_WMA_EncodeGetRates = nil) or
      (@BASS_WMA_EncodeOpenFile = nil) or
      (@BASS_WMA_EncodeOpenNetwork = nil) or
      (@BASS_WMA_EncodeGetPort = nil) or
      (@BASS_WMA_EncodeSetNotify = nil) or
      (@BASS_WMA_EncodeGetClients = nil) or
      (@BASS_WMA_EncodeSetTag = nil) or
      (@BASS_WMA_EncodeWrite = nil) or
      (@BASS_WMA_EncodeClose = nil) then
   begin
      FreeLibrary(BASSWMA_Handle);
      BASSWMA_Handle := 0;
   end else
      result := true;
end;

procedure Unload_BASSWMADLL;
begin
   if BASSWMA_Handle <> 0 then
      FreeLibrary(BASSWMA_Handle);

   BASSWMA_Handle := 0;
end;

end.
