// Unit PluginConfig Ver 1.0
//
//  This unit takes charge of configuring Winamp input plug-ins for TBASSPlayer
//
//    written by Silhwan Hyun  hsh@chollian.net
//
//
// Ver 1.1                         15 Feb 2003
//   Changed the goRangeSelect property of PluginGrid from true to false
//       (bug fix : to prohibit multiple selection of plug-ins to unload)
//
// Ver 1.0                         30 Jan 2003
//   - Initial release
//

unit PluginConfig;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Grids, PluginCtrl;

const
   WM_PluginFirst_Changed = WM_USER + 109;
type
  TPluginConfigForm = class(TForm)
    PluginList: TListBox;
    Label1: TLabel;
    Label2: TLabel;
    PluginGrid: TStringGrid;
    Label3: TLabel;
    btnLoad: TButton;
    btnUnload: TButton;
    btnAbout: TButton;
    btnConfigure: TButton;
    btnClose: TButton;
    Label4: TLabel;
    Label5: TLabel;
    PluginFirst: TCheckBox;
    Label6: TLabel;
    Label7: TLabel;
    procedure ClearPluginGrid;
    procedure FillPluginGrid;
    procedure btnCloseClick(Sender: TObject);
    procedure btnLoadClick(Sender: TObject);
    procedure btnUnloadClick(Sender: TObject);
    procedure btnConfigureClick(Sender: TObject);
    procedure btnAboutClick(Sender: TObject);
    procedure PluginFirstClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

{var
  PluginConfigForm: TPluginConfigForm; }

  procedure SetMessageHandle(MessageHandle : hwnd);

implementation

{$R *.DFM}


var
   LoadCounter : integer;
   CurSelection : TGridRect;
   IndexNum : integer;
   PluginName : string;
   MainMessageHandle : hwnd;

procedure SetMessageHandle(MessageHandle : hwnd);
begin
   MainMessageHandle := MessageHandle;
end;

procedure TPluginConfigForm.ClearPluginGrid;
var
   i : integer;
begin
   with PluginGrid do
   begin
     Perform(WM_SETREDRAW, 0, 0);  // block visual updates
     try
       for i := FixedRows to RowCount - 1 do
         Rows[i].Clear;
     finally
       Perform(WM_SETREDRAW, 1, 0);
       Invalidate;
     end;
   end;
end;

procedure TPluginConfigForm.FillPluginGrid;
var
   i : integer;
   WinampPlugin : TPlugin;
   WinampPluginInfo : TPluginInfo;
begin
   LoadCounter := 0;
   for i := 0 to MaxPluginNum - 1 do
   begin
      WinampPlugin := GetPlugin(i);
      if WinampPlugin <> nil then     // APlugin = nil -> empty element
      begin
         WinampPluginInfo := GetPluginInfo(i);
         inc(LoadCounter);
         PluginGrid.Cells[0, LoadCounter] := WinampPluginInfo.Name;
         PluginGrid.Cells[1, LoadCounter] := WinampPluginInfo.Description;
         PluginGrid.Cells[2, LoadCounter] := WinampPluginInfo.FileExtensions;
      end;
   end;
end;

procedure TPluginConfigForm.btnCloseClick(Sender: TObject);
begin
   Close;
end;


procedure TPluginConfigForm.btnLoadClick(Sender: TObject);
const
   msg1 = 'No plug-in to load';
   msg2 = 'Select plug-in to load first';
   msg3 = 'Load denied : Output plug-in is not ready';
   msg4 = 'Load denied : Already loaded before';
   msg5 = 'Load denied : No space to load';
   msg6 = 'Load denied : Unknown Error';
   msg7 = 'Load denied : Not valid Winamp 2 input plug-in';
var
   WinampPluginInfo : TPluginInfo;
begin
   if PluginList.Items.Count = 0 then
   begin
      Application.MessageBox(msg1, 'Confirm', MB_OK);
      exit;
   end;
   if PluginList.ItemIndex = -1 then
   begin
      Application.MessageBox(msg2, 'Confirm', MB_OK);
      exit;
   end;

   IndexNum := PluginList.ItemIndex;
   PluginName := PluginList.Items[IndexNum];
   case LoadWinampPlugin(PluginName) of
      0 : begin      // 0 : No error
             inc(LoadCounter);
             IndexNum := GetPluginIndex(PluginName);
             WinampPluginInfo := GetPluginInfo(IndexNum);
             PluginGrid.Cells[0, LoadCounter] := WinampPluginInfo.Name;
             PluginGrid.Cells[1, LoadCounter] := WinampPluginInfo.Description;
             PluginGrid.Cells[2, LoadCounter] := WinampPluginInfo.FileExtensions;
          end;
      ERROR_OMOD_NOTREADY : Application.MessageBox(msg3, 'Confirm', MB_OK);
      ERROR_LOADED_BEFORE : Application.MessageBox(msg4, 'Confirm', MB_OK);
      ERROR_LOADED_FULL   : Application.MessageBox(msg5, 'Confirm', MB_OK);
      ERROR_CANNOT_LOAD   : Application.MessageBox(msg6, 'Confirm', MB_OK);
      ERROR_INVALID_PLUGIN : Application.MessageBox(msg7, 'Confirm', MB_OK);
   end;
end;


procedure TPluginConfigForm.btnUnloadClick(Sender: TObject);
const
   msg1 = 'No plug-ins are loaded';
   msg2 = 'Select plug-in to unload first';
   msg3 = 'Cannot unload : Unknown Error';
   msg4 = 'Cannot unload : Plug-in is not loaded';
   msg5 = 'Cannot unload : Plug-in is in use';

begin
   if LoadCounter = 0 then
   begin
      Application.MessageBox(msg1, 'Confirm', MB_OK);
      exit;
   end;

   CurSelection := PluginGrid.Selection;
   if CurSelection.Top = -1 then
   begin
      Application.MessageBox(msg2, 'Confirm', MB_OK);
      exit;
   end;

   PluginName := PluginGrid.Cells[0, CurSelection.Top];
   if PluginName = '' then
   begin
      Application.MessageBox(msg2, 'Confirm', MB_OK);
      exit;
   end;

   case UnloadWinampPlugin(PluginName) of
      0 : begin
            if CurSelection.Top = LoadCounter then   // the last item in PluginGrid ?
            begin
               PluginGrid.Cells[0, LoadCounter] := '';
               PluginGrid.Cells[1, LoadCounter] := '';
               PluginGrid.Cells[2, LoadCounter] := '';
               dec(LoadCounter);
            end else
            begin
               ClearPluginGrid;
               FillPluginGrid;
            end;
         end;
      ERROR_CANNOT_UNLOAD : Application.MessageBox(msg3, 'Confirm', MB_OK);
      ERROR_NOT_LOADED : Application.MessageBox(msg4, 'Confirm', MB_OK);
      ERROR_IN_USE     : Application.MessageBox(msg5, 'Confirm', MB_OK);
   end;
end;


procedure TPluginConfigForm.btnAboutClick(Sender: TObject);
const
   msg1 = 'Select plug-in to query first';
   msg2 = 'No plug-ins to query are loaded';
var
   WinampPlugin : TPlugin;
   PluginNum : integer;
begin
   if LoadCounter = 0 then
   begin
      Application.MessageBox(msg2, 'Confirm', MB_OK);
      exit;
   end;

   CurSelection := PluginGrid.Selection;
   if CurSelection.Top = -1 then
   begin
      Application.MessageBox(msg1, 'Confirm', MB_OK);
      exit;
   end;
   PluginName := PluginGrid.Cells[0, CurSelection.Top];
   if PluginName = '' then
   begin
      Application.MessageBox(msg1, 'Confirm', MB_OK);
      exit;
   end;

   PluginNum := GetPluginIndex(PluginName);
   WinampPlugin := GetPlugin(PluginNum);
   WinampPlugin.About(Self.Handle);
end;

procedure TPluginConfigForm.btnConfigureClick(Sender: TObject);
const
   msg1 = 'Select plug-in to configure first';
   msg2 = 'No plug-ins to configure are loaded';
var
   WinampPlugin : TPlugin;
   PluginNum : integer;
begin
   if LoadCounter = 0 then
   begin
      Application.MessageBox(msg2, 'Confirm', MB_OK);
      exit;
   end;

   CurSelection := PluginGrid.Selection;
   if CurSelection.Top = -1 then
   begin
      Application.MessageBox(msg1, 'Confirm', MB_OK);
      exit;
   end;
   PluginName := PluginGrid.Cells[0, CurSelection.Top];
   if PluginName = '' then
   begin
      Application.MessageBox(msg1, 'Confirm', MB_OK);
      exit;
   end;

   PluginNum := GetPluginIndex(PluginName);
   WinampPlugin := GetPlugin(PluginNum);
   WinampPlugin.config(Self.Handle);
end;

procedure TPluginConfigForm.PluginFirstClick(Sender: TObject);
var
   msgPluginFirst : longint;
begin
   if PluginFirst.Checked then
      msgPluginFirst := 1
   else
      msgPluginFirst := 0;

 // I do not know the method how to hand over a value of Form's element to its parent's
 //  (in this case PluginConfigForm.Checked -> TBASSPlayer.FPluginFirst)
 // So I simply use PostMessage API call. 
   PostMessage(MainMessageHandle, WM_PluginFirst_Changed, 0, msgPluginFirst);
end;

end.
