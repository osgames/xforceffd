// Component TBASSPlayer Ver 1.3
//
//    written by Silhwan Hyun  hsh@chollian.net
//
//
// Revision history
//
// Ver 1.3                        5 Apr 2003
//   ( Based on bass.dll/basswma.dll Ver 1.8 )
//  Added a function and a procedure to run Winamp Visualization plug-ins
//   - function RunVisoutProgram(VismodName : string; VismodNo : word) : boolean;
//   - procedure QuitVisoutProgram;
//  Modified function GetVolume and procedure SetVolume to reflect/adjust the
//   volume level of Winamp plug-in's output
//  Added property EchoLevel & ReverbLevel to adjust the intensity of sound effect
//
// Ver 1.2                        5 Feb 2003
//   ( Based on bass.dll/basswma.dll Ver 1.7 )
//  Added functions & properties to use Winamp 2 input plug-ins.
//   Now TBASSPlayer can play stream files which BASS sound system cannot decode
//   if provided appropriate Winamp input plug-in.
//  Changed the loading method of bass.dll & basswma.dll from static loading to
//   dynamic loading
//
//   - Modified function : GetStreamInfo
//   - New properties : BASSWMAReady, Version, NativeFileExts, PluginFileExts, CanUsePlugin,
//                      SupportedBy, PluginFirst, OnPluginStartPlay, UnloadButEnabled
//   - Renamed property : DLLVersionStr -> BASSDLLVer
//   - New  procedure : ShowPluginConfigForm (-> used to show PluginConfigForm,
//                        which is defined in sub unit PluginConfig.pas)
//   - New functions in sub unit PluginCtrl which you can call in main program
//     if you want to manage Winamp input plug-ins directly(= not using PluginConfigForm).
//       GetPluginIndex(PluginName : string) : integer;
//       GetPlugin(PluginNum : integer) : TPlugin;
//       GetPluginInfo(PluginNum : integer) : TPluginInfo;
//       LoadWinampPlugin(PluginName : string) : integer;
//       UnloadWinampPlugin(PluginName : string) : integer;
//  * include "PluginCtrl" in uses clause of main program to use above functions
//
//  * The items which does not affect external program are not mentioned here
//
// Ver 1.1                         8 Dec 2002
//   ( Based on bass.dll/basswma.dll Ver 1.7 )
//   - New function : GetChannelFFTData (for custom processing of FFT data)
//   - Added field at record TStreamInfo : Year
//   - Added property : EQBands (for custom setting of equalizer bands)
//   - Renamed property : EQGains ( <- EqPreset )
//   - New type identifiers : TBandData, TEQBands
//   - Renamed type identifier : TEQGains ( <- TEqPreset )
//   - Updated procedure : TimerFFTTimer (for better output of spectrum visualization)
//
// Ver 1.0                         4 Aug 2002
//   ( Based on bass.dll/basswma.dll Ver 1.6 )
//   - Initial release

unit BASSPlayer;

interface

uses
  Windows, Messages, SysUtils, Classes, Forms, Controls, StdCtrls, ExtCtrls,
  RT_BASS, MPEGAudio, OggVorbis, WAVFile, Dialogs, IDHashMap;

const
   Ver = '1.2';
   NumFFTBands = 25;     // Number of bands for spectrum visualization
   NumEQBands  = 10;     // Number of bands for equalizer
   EQBandWidth  : array[0..NumEQBands-1] of float =
                  (4, 4, 4, 4, 5, 6, 5, 4, 3, 3);
   EQFreq : array[0..NumEQBands-1] of float =
                  (80, 170, 310, 600, 1000, 3000, 6000, 10000, 12000, 14000);
   NoDX8Msg : pchar = 'DirectX Ver 8 are not installed.' + #13#10 +
                      'Sound effects except flanger are disabled.';

   WM_GetToEnd      = WM_USER + 107;
   WM_NewFFTData    = WM_USER + 108;

type
  TSoundEffects = set of (Flanger, Equalizer, Echo, Reverb);
  TStreamInfo = record
     FileName : string;
     FileSize : DWORD;        // File size in byte
     SampleRate : LongInt;    // Sampling rate in Hz
     BitRate : LongInt;       // Bit Rate in KBPS
     Duration : DWORD;        // Song duration in mili second
     Channels : Word;         // 1- Mono, 2 - Stereo

     Title : string;
     Artist : string;
     Album : string;
     Year : string;
     Genre : string;
     GenreID : byte;
     Track : byte;
     Comment : string;
  end;

  TSample = record
    Sample   : HSample;
    Name     : String;
  end;

  TFFTData = array [0..512] of Single;
  TBandOut = array[0..NumFFTBands-1] of word;
  TPlayerMode = (plmStandby, plmReady, plmStopped, plmPlaying, plmPaused);
  TChannelType = (Channel_NotOpened, Channel_Stream, Channel_Music);
  TSupportedBy = (Both, BASSNative, WinampPlugin, None);
  TEQGains = array [0..NumEQBands-1] of float;
  TBandData = record
     CenterFreq : float;
     Bandwidth  : float;
  end;
  TEQBands = record
     Bands : word;     // Number of equalizer bands (min : 0, max : 10)
     BandPara : array[0..NumEQBands-1] of TBandData;
  end;

  TNotifyEvent = procedure(Sender: TObject) of object;
  TNotifyNewFFTDataEvent = procedure(Sender: TObject; BandOut : TBandOut) of object;

  TRequestNextFile = procedure(var FileName: String; var Loop: Boolean) of object;

  TBASSPlayer = class(TComponent)
  private
    { Private declarations }
    AppHWND     : THandle;
    TimerFFT    : TTimer;

    ChannelType   : TChannelType;
    Channel       : DWORD;
    MessageHandle : hwnd;

    MessageHandle2 : hwnd;
    NowStarting : boolean;
 // Some input plug-ins do not need output plug-in. i.e., some input plug-ins
 // sound out decoded data directly.
 // UsesOutputPlugin is used to know if input plug-in needs output plug-in.
 // Use this instead of input plug-in's UsesOutPlug property because some
 // input plug-ins report wrong UsesOutPlug value.

    BASSDLLLoaded : boolean;
    BassInfoParam : BASS_INFO;
    EQParam       : BASS_FXPARAMEQ;
    EchoParam     : BASS_FXECHO;
    ReverbParam   : BASS_FXREVERB;
    FSoundEffects : TSoundEffects;
    SyncHandle    : HSYNC;
    fladspHandle  : HDSP;
    EQHandle      : array [0..NumEQBands-1] of HFX;
    EchoHandle    : HFX;
    ReverbHandle  : HFX;

    BandOut: TBandOut;
    ShareMemPointer : pointer;
    fHashMap        : TIDHashMap;

    MPEG        : TMPEGaudio;
    Vorbis      : TOggVorbis;
    WAV         : TWAVFile;

    GoVisOut : boolean;

    FBASSReady      : boolean;
    FVersionStr     : string;
    FDLLVersionStr  : string;
    FStreamName     : string;
    FSupportedBy    : TSupportedBy;
    FStreamInfo     : TStreamInfo;
    FMusicVolume    : integer;
    FStreamVolume   : integer;
    FDX8EffectReady : boolean;
    FEchoLevel      : word;
    FReverbLevel    : word;
    FEQGains        : TEQGains;
    FEQBands        : TEQBands;
    FPlayerMode     : TPlayerMode;
    FUnloadButEnabled : boolean;

    FOnPlayEnd         : TNotifyEvent;
    FOnNewFFTData      : TNotifyNewFFTDataEvent;
    fRequestNextFile   : TRequestNextFile;

    procedure ClearEffectHandle;                   // * New at Ver 1.2
    function  GetStreamInfo2(StreamName : string;  // * New at Ver 1.2
                            var StreamInfo : TStreamInfo;
                            var SupportedBy : TSupportedBy;
                            var PluginNum : integer) : boolean;
    function  GetVolume : integer;            // * Modified at Ver 1.3
    procedure SetVersionStr(Value : string);
    procedure SetDLLVersionStr(Value : string);
    procedure SetDX8EffectReady(Value : boolean);
    procedure SetVolume(Value : integer);     // * Modified at Ver 1.3
    procedure SetEchoLevel(Value : word);     // * New at Ver 1.3
    procedure SetReverbLevel(Value : word);   // * New at Ver 1.3
    procedure SetSoundEffect(Value : TSoundEffects);
    procedure SetEQGains(Value : TEQGains);
    procedure SetEQBands(Value : TEQBands);
    procedure SetUnloadBut(Value : boolean);   // * New at Ver 1.2
    function  GetDLLVersionStr : string;
    function  GetNativeFileExts : string;      // * New at Ver 1.2
    procedure PausePlay;
    procedure ResumePlay;
    procedure Restart;
    procedure TimerFFTTimer(Sender: TObject);
    procedure ProcMessage(var Msg: TMessage);
    function  CurrentPosition : DWORD;
    procedure SetPosition(Value : DWORD);
    procedure Error(msg: string);
    function GetSampleVolume: Integer;
    procedure SetSampleVolume(const Value: Integer);

  public
    { Public declarations }
    constructor Create(AOwner: TComponent); override;
    destructor  Destroy; override;

    function  GetStreamInfo(StreamName : string;        // * Modified at Ver 1.2
                            var StreamInfo : TStreamInfo;
                            var SupportedBy : TSupportedBy) : boolean;
    function  Open(StreamName : string) : boolean;

    function  PlayLength : DWORD;
    function  GetChannelFFTData(PFFTData : pointer; FFTFlag : DWORD) : boolean;

    procedure Play(Loop: Boolean = false);
    procedure FadeTo(Filename: String;Loop: Boolean = false);
    procedure Stop;
    procedure Pause(pAction : boolean);
    procedure Close;

    property  EQGains : TEQGains read FEQGains write SetEQGains;
    property  EQBands : TEQBands read FEQBands write SetEQBands;
    property  PlayerReady : boolean read FBASSReady;
    property  SupportedBy : TSupportedBy read FSupportedBy;  // * New at Ver 1.2
    property  StreamInfo : TStreamInfo read FStreamInfo;
    property  Mode : TPlayerMode read FPlayerMode;
    property  Position : DWORD read CurrentPosition write SetPosition;
    property  StreamName : string read FStreamName;

    property  Volume : integer read GetVolume write SetVolume;  // 0(minimum output) ~ 100(maximum output)
    property  SoundEffects : TSoundEffects read FSoundEffects write SetSoundEffect;

  // EchoLevel & ReverbLevel : 0(minimum sound effect) ~ 32(maximum sound effect)
    property  EchoLevel : word read FEchoLevel write SetEchoLevel;        // * New at Ver 1.3
    property  ReverbLevel : word read FReverbLevel write SetReverbLevel;  // * New at Ver 1.3

    property  NativeFileExts : string read GetNativeFileExts; // * New at Ver 1.2
    property  UnloadButEnabled : boolean read FUnloadButEnabled write SetUnloadBut; // * New at Ver 1.2

    // Sampleprozeduren
    function ExistsSample(ID: Cardinal): boolean;

    function AddSample(ID: Cardinal; Stream: TMemoryStream): Boolean;
    procedure RemoveSample(ID: Cardinal);
    function PlaySample(ID: Cardinal): Boolean;

    procedure StopSample(ID: Cardinal);

    property SampleVolume: Integer read GetSampleVolume write SetSampleVolume;
  published
    { Published declarations }
    property  Version : string read FVersionStr write SetVersionStr;    // * New at Ver 1.2
    property  BASSDLLVer : string read FDLLVersionStr write SetDLLVersionStr; // * Renamed at Ver 1.2
    property  DX8EffectReady : boolean read FDX8EffectReady write SetDX8EffectReady;
    property  OnNewFFTData : TNotifyNewFFTDataEvent read FOnNewFFTData write FOnNewFFTData;
    property  OnPlayEnd : TNotifyEvent read FOnPlayEnd write FOnPlayEnd;
    property  OnRequestNextFile : TRequestNextFile read fRequestNextFile write fRequestNextFile;
  end;

  procedure Register;

var
  FadingTime : Integer = 1500;

implementation


procedure WinProcessMessages;
// Allow Windows to process other system messages
var
    ProcMsg  :  TMsg;
begin
    while PeekMessage(ProcMsg, 0, 0, 0, PM_REMOVE) do begin
      if (ProcMsg.Message = WM_QUIT) then Exit;
      TranslateMessage(ProcMsg);
      DispatchMessage(ProcMsg);
    end;
end;


// Following statements for flanger effect is adopted from DTMain.pas of BASS16
// package

const
   FLABUFLEN = 350;         // buffer length for flanger effect

var
   // Variables for DSP (flanger effect) implementation
   flabuf : array[0..FLABUFLEN-1, 0..2] of SmallInt;  // buffer
   flapos : Integer;         // cur.pos
   flas, flasinc : FLOAT;    // sweep pos/min/max/inc

function fmod(a, b: FLOAT): FLOAT;
begin
   Result := a - (b * Trunc(a / b));
end;

function Clip(a: Integer): Integer;
begin
   if a <= -32768 then
      a := -32768
   else if a >= 32767 then
      a := 32767;

   Result := a;
end;

procedure Flange(handle: HSYNC; channel: DWORD; buffer: Pointer; length, user: DWORD); stdcall;
var
  lc, rc: SmallInt;
  p1, p2, s: Integer;
  d: ^DWORD;
  f: FLOAT;
begin
  d := buffer;
  while (length > 0) do
  begin
    lc := LOWORD(d^); rc := HIWORD(d^);
    p1 := (flapos + Trunc(flas)) mod FLABUFLEN;
    p2 := (p1 + 1) mod FLABUFLEN;
    f := fmod(flas, 1.0);
    s := lc + Trunc(((1.0-f) * flabuf[p1, 0]) + (f * flabuf[p2, 0]));
    flabuf[flapos, 0] := lc;
    lc := Clip(s);
    s := rc + Trunc(((1.0-f) * flabuf[p1, 1]) + (f * flabuf[p2, 1]));
    flabuf[flapos, 1] := rc;
    rc := Clip(s);
    d^ := MakeLong(lc, rc);
    Inc(d);
    Inc(flapos);
    if (flapos = FLABUFLEN) then flapos := 0;
    flas := flas + flasinc;
    if (flas < 0) or (flas > FLABUFLEN) then
      flasinc := -flasinc;
    length := length - 4;
  end;
end;

// This procedure is called at the end of playing stream file
procedure SyncProc(SyncHandle : HSYNC; Channel, data, user : DWORD); stdcall;
begin
   PostMessage(User, WM_GetToEnd, 0, 0);
end;

procedure SyncProcChannelFree(SyncHandle : HSYNC; Channel, data, user : DWORD); stdcall;
begin
  BASS_StreamFree(Channel);
end;

procedure SyncProcBeforeEnd(SyncHandle : HSYNC; Channel, data, user : DWORD); stdcall;
var
  Player: TBASSPlayer;
  FileName: String;
  Loop: Boolean;
begin
//  BASS_StreamFree(Channel);
  Player:=TBassPlayer(user);
  if Assigned(Player.OnRequestNextFile) then
  begin
    FileName:='';
    Loop:=false;
    Player.OnRequestNextFile(FileName,Loop);
    if FileName<>'' then
      Player.FadeTo(FileName,Loop);
  end;
end;

procedure TBASSPlayer.Error(msg: string);
var
  s: string;
begin
  s := msg + #13#10 + '(error code: ' + IntToStr(BASS_ErrorGetCode) + ')';
//  MessageBox(Application.handle, PChar(s), 'Error', MB_ICONERROR or MB_OK);
  raise Exception.Create(s);
end;

procedure TBASSPlayer.ClearEffectHandle;
var
   i : integer;
begin
   fladspHandle := 0;
   if FDX8EffectReady then
   begin
      for i := 0 to (NumEQBands-1) do
          EQHandle[i] := 0;
      EchoHandle := 0;
      ReverbHandle := 0;
   end;
end;

function TBASSPlayer.PlayLength : DWORD;
var
   SongLength : int64;
   FloatLen : FLOAT;
begin
   result := 0;
   case ChannelType of
      Channel_NotOpened : exit;
      Channel_Music  : SongLength := BASS_MusicGetLength(Channel, true);
      Channel_Stream : SongLength := BASS_StreamGetLength(Channel);
   end;

   if SongLength < 0 then
      SongLength := 0;

   FloatLen := BASS_ChannelBytes2Seconds(Channel, SongLength);
   result := round(1000 * FloatLen);   // sec -> milli sec
end;


function TBASSPlayer.CurrentPosition : DWORD;
var
   SongPos : int64;
   FloatPos : FLOAT;
begin
   result := 0;
   case ChannelType of
      Channel_NotOpened : exit;
      Channel_Music  : exit;   // not implemented yet
      Channel_Stream : SongPos:=BASS_ChannelGetPosition(Channel);
   end;
   if SongPos < 0 then
      SongPos := 0;

   FloatPos := BASS_ChannelBytes2Seconds(Channel, SongPos);
   result := round(1000 * FloatPos);     // sec -> milli sec
end;


procedure TBASSPlayer.SetPosition(Value : DWORD);
var
   SongPos : int64;
   wCycle : integer;
begin
   if ChannelType = Channel_NotOpened then
      exit;

   if Value > FStreamInfo.Duration then
      exit;

   if NowStarting then
      exit;

   SongPos := BASS_ChannelSeconds2Bytes(Channel, Value / 1000);
   case ChannelType of
      Channel_NotOpened : exit;
      Channel_Music   : exit;   // not implemented yet
      Channel_Stream  : BASS_ChannelSetPosition(Channel,SongPos);
   end;
end;


function TBASSPlayer.GetVolume : integer;
begin
   if ChannelType = Channel_Music then
      result := FMusicVolume
   else
      result := FStreamVolume;
end;

procedure TBASSPlayer.SetVersionStr(Value : string);
begin
   // nothing to do : only to show FVersionStr at form design
end;

procedure TBASSPlayer.SetDLLVersionStr(Value : string);
begin
   // nothing to do : only to show FDLLVersionStr at form design
end;

procedure TBASSPlayer.SetDX8EffectReady(Value : boolean);
begin
   // nothing to do : only to show FDX8EffectReady at form design
end;

procedure TBASSPlayer.SetVolume(Value : integer);
var
   tmpValue : integer;
begin
   if Value > 100 then
      tmpValue := 100
   else if Value < -1 then
      tmpValue := 0
   else
      tmpValue := Value;

 // Adjust the volume according ChannelType
   if ChannelType = Channel_Music then
   begin
      BASS_SetConfig(BASS_CONFIG_GVOL_MUSIC,tmpValue);
      FMusicVolume := tmpValue;
   end else begin
      BASS_SetConfig(BASS_CONFIG_GVOL_STREAM, tmpValue);
      FStreamVolume := tmpValue;
   end;
end;

procedure TBASSPlayer.SetEQGains(Value : TEQGains);
var
   i : integer;
begin
   if not FDX8EffectReady then
      exit;

   for i := 0 to (NumEQBands - 1) do
   begin
      if Value[i] > 15.0 then
         FEQGains[i] := 15.0
      else if Value[i] < -15.0 then
         FEQGains[i] := -15.0
      else
         FEQGains[i] := Value[i];

      if not (Equalizer in FSoundEffects) then
         Continue;

      if EQHandle[i] = 0 then
         Continue;

      BASS_FXGetParameters(EQHandle[i], @EQParam);

   // Avoid redundant operation
      if abs(EQParam.fGain - FEQGains[i]) < 0.001 then   // consider round-off error
         Continue;

      EQParam.fBandwidth := FEQBands.BandPara[i].Bandwidth;
      EQParam.fCenter := FEQBands.BandPara[i].CenterFreq;
      EQParam.fGain:= FEQGains[i];
      BASS_FXSetParameters(EQHandle[i], @EQParam);
   end;
end;

procedure TBASSPlayer.SetEchoLevel(Value : word);
begin
   if Value > 32 then
      exit;

   FEchoLevel := Value;
   if Echo in FSoundEffects then
   begin
       if EchoHandle <> 0 then
       begin
         if BASS_FXGetParameters(EchoHandle, @EchoParam) then
         begin
            EchoParam.fWetDryMix := FEchoLevel * 1.2{30.0};
            EchoParam.fFeedBack := 30.0;
            BASS_FXSetParameters(EchoHandle, @EchoParam)
         end;
       end;
   end;
end;

procedure TBASSPlayer.SetReverbLevel(Value : word);
begin
   if Value > 32 then
      exit;

   FReverbLevel := Value;
   if Reverb in FSoundEffects then
   begin
      if ReverbHandle <> 0 then
       begin
         if BASS_FXGetParameters(ReverbHandle, @ReverbParam) then
         begin
            ReverbParam.fInGain := 0.0;
            ReverbParam.fReverbMix := FReverbLevel * 0.5 - 16.0;
            ReverbParam.fReverbTime := 1000.0;
            ReverbParam.fHighFreqRTRatio := 0.1;
            BASS_FXSetParameters(ReverbHandle, @ReverbParam)
         end;
       end;
   end;
end;

procedure TBASSPlayer.SetEQBands(Value : TEQBands);
var
   i : integer;
begin
   FEQBands.Bands := Value.Bands;
   if FEQBands.Bands > NumEQBands then
      FEQBands.Bands := NumEQBands;

   for i := 0 to (FEQBands.Bands-1) do
   begin
      FEQBands.BandPara[i].CenterFreq := Value.BandPara[i].CenterFreq;
      FEQBands.BandPara[i].Bandwidth := Value.BandPara[i].Bandwidth;
   end;

   if Equalizer in FSoundEffects then
     for i := 0 to (FEQBands.Bands-1) do
     begin
        if EQHandle[i] = 0 then
           EQHandle[i] := BASS_ChannelSetFX(Channel, BASS_FX_PARAMEQ,0);
        EQParam.fGain := FEQGains[i];
        EQParam.fBandwidth := FEQBands.BandPara[i].Bandwidth;
        EQParam.fCenter := FEQBands.BandPara[i].CenterFreq;
        BASS_FXSetParameters(EQHandle[i], @EQParam);
     end;

   for i := FEQBands.Bands to (NumEQBands-1) do
   begin
      if EQHandle[i] <> 0 then
         if BASS_ChannelRemoveFX(Channel, EQHandle[i]) then
            EQHandle[i] := 0;
   end;
end;

procedure TBASSPlayer.SetSoundEffect(Value : TSoundEffects);
var
//   tmpPaused : boolean;
   i : integer;
begin
   FSoundEffects := Value;

   if ChannelType = Channel_NotOpened then
      exit;

   if Flanger in Value then
   begin
      if fladspHandle = 0 then
      begin
         FillChar(flabuf, SizeOf(flabuf), 0);
         flapos := 0;
         flas := FLABUFLEN / 2;
         flasinc := 0.002;
         fladspHandle := BASS_ChannelSetDSP(Channel, Flange, 0,0 );
      end;
   end
   else
      if fladspHandle <> 0 then
         if BASS_ChannelRemoveDSP(Channel, fladspHandle) then
            fladspHandle := 0;

   if not FDX8EffectReady then
      exit;

// DX8 effects can be applied at any time with BASS ver 1.7
{  if BASS_ChannelIsActive(Channel) = BASS_ACTIVE_PLAYING then
   begin
      PausePlay;
      tmpPaused := true;
   end else
      tmpPaused := false; }

   if Equalizer in Value then
   for i := 0 to (FEQBands.Bands-1) do
   begin
      if EQHandle[i] = 0 then
         EQHandle[i] := BASS_ChannelSetFX(Channel, BASS_FX_PARAMEQ,0);
      EQParam.fGain := FEQGains[i];
      EQParam.fBandwidth := FEQBands.BandPara[i].Bandwidth;
      EQParam.fCenter := FEQBands.BandPara[i].CenterFreq;
      BASS_FXSetParameters(EQHandle[i], @EQParam);
   end else
   for i := 0 to (NumEQBands-1) do
   begin
      if EQHandle[i] <> 0 then
         if BASS_ChannelRemoveFX(Channel, EQHandle[i]) then
            EQHandle[i] := 0;
   end;

   if Echo in Value then
   begin
       if EchoHandle = 0 then
          EchoHandle := BASS_ChannelSetFX(Channel, BASS_FX_ECHO,0);
       if EchoHandle <> 0 then
       begin
         if BASS_FXGetParameters(EchoHandle, @EchoParam) then
         begin
            EchoParam.fWetDryMix := FEchoLevel * 1.2{30.0};
            EchoParam.fFeedBack := 30.0;
            BASS_FXSetParameters(EchoHandle, @EchoParam)
         end;
       end;
   end else
      if EchoHandle <> 0 then
         if BASS_ChannelRemoveFX(Channel, EchoHandle) then
            EchoHandle := 0;

   if Reverb in Value then
   begin
       if ReverbHandle = 0 then
          ReverbHandle := BASS_ChannelSetFX(Channel, BASS_FX_REVERB,0);
       if ReverbHandle <> 0 then
       begin
         if BASS_FXGetParameters(ReverbHandle, @ReverbParam) then
         begin
            ReverbParam.fInGain := 0.0;
            ReverbParam.fReverbMix := FReverbLevel * 0.5 - 16.0;
            ReverbParam.fReverbTime := 1000.0;
            ReverbParam.fHighFreqRTRatio := 0.1;
            BASS_FXSetParameters(ReverbHandle, @ReverbParam)
         end;
       end;
   end else
      if ReverbHandle <> 0 then
         if BASS_ChannelRemoveFX(Channel, ReverbHandle) then
            ReverbHandle := 0;

 {  if tmpPaused then
      ResumePlay;  }
end;

// This procedure is used to inhibit or to allow unloading of Winamp
// plug-ins.
//   SetUnloadBut(false) -> inhibit,  SetUnloadBut(true) -> allow
procedure TBASSPlayer.SetUnloadBut(Value : boolean);
begin
   FUnloadButEnabled := value;
end;


constructor TBASSPlayer.Create(AOwner: TComponent);
var
   i : integer;
   MusicVol, SampleVol, StreamVol : DWORD;
   BASSVersion : DWORD;
begin
   inherited Create(AOwner);

   FVersionStr := Ver;
   FBASSReady := false;
   ChannelType := Channel_NotOpened;
   FSoundEffects := [];
   FPlayerMode := plmStandby;
   FStreamName :='';
   FStreamInfo.FileName := '';
   FStreamInfo.SampleRate := 0;
   FSupportedBy := None;

   if not (csDesigning in ComponentState) then
   begin
     AppHWND := Application.Handle;
     MessageHandle := AllocateHWnd(ProcMessage);
     TimerFFT := TTimer.Create(nil);
     TimerFFT.Interval := 33;
     TimerFFT.Enabled := false;
     TimerFFT.OnTimer := TimerFFTTimer;
   end;

{   BASSDLLLoaded := Load_BASSDLL('bass.dll');
   if not BASSDLLLoaded then
   begin
      if (csDesigning in ComponentState) then
         FDLLVersionStr := 'N.A.(Copy bass.dll in <directory Delphi installed>\Bin to get version)'
      else
         FDLLVersionStr := '';
      exit;
   end;}

   BASSVersion := BASS_GetVersion;
   if (BASSVersion <> MakeLong(2,1)) then
   begin
     if not (csDesigning in ComponentState) then
        Error('BASS version is not 2 or higher');
     exit;
   end;

 // setup output - default device, 44100hz, stereo, 16 bits
   if not BASS_Init(1, 44100, 0, AppHWND,nil) then
   begin
      if not (csDesigning in ComponentState) then
         Error('Can''t initialize device');
      exit;
   end;

   if BASS_Start then
      FBASSReady := true
   else
      exit;

   BassInfoParam.size := SizeOf(BassInfoParam);
   BASS_GetInfo(BassInfoParam);

 // BASS_ChannelSetFX requires DirectX version 8 or higher
   if BassInfoParam.dsver >= 8 then
   begin
      FDX8EffectReady := true;
   end else begin
      if not (csDesigning in ComponentState) then
         MessageBox(AppHWND, NoDX8Msg, 'Warning', MB_ICONWARNING or MB_OK);
      FDX8EffectReady := false;
   end;

   FEQBands.Bands := NumEQBands;
   for i := 0 to (NumEQBands-1) do
   begin
      FEQGains[i] := 0;
      FEQBands.BandPara[i].CenterFreq := EQFreq[i];
      FEQBands.BandPara[i].Bandwidth := EQBandWidth[i];
   end;
   FEchoLevel := 16;
   FReverbLevel := 16;

   FMusicVolume := BASS_GetConfig(BASS_CONFIG_GVOL_MUSIC);
   FStreamVolume := BASS_GetConfig(BASS_CONFIG_GVOL_STREAM);
   FDLLVersionStr := GetDLLVersionStr;

   if not (csDesigning in ComponentState) then
   begin
      MPEG := TMPEGaudio.Create;
      Vorbis := TOggVorbis.Create;
      WAV := TWAVFile.Create;
   end;

  fHashMap:=TIDHashMap.Create;
end;


destructor  TBASSPlayer.Destroy;
begin
  if (FPlayerMode = plmPlaying) then
      Stop;

   if BASSDLLLoaded then
   begin
      BASS_Free;
//      Unload_BASSDLL;
   end;

   if not (csDesigning in ComponentState) then
   begin
     TimerFFT.Free;
     MPEG.Free;
     Vorbis.Free;
     WAV.Free;
     if MessageHandle <> 0 then
        DeallocateHWnd(MessageHandle);

   end;

   fHashMap.Free;
   
   inherited Destroy;
end;

// Get the version information as string
function TBASSPlayer.GetDLLVersionStr : string;
var
   VersionNum : DWORD;
begin
   if not FBASSReady then
   begin
      result := '';
      exit;
   end;

   VersionNum := BASS_GetVersion;
   result := intToStr(LoWord(VersionNum)) + '.' + intToStr(HiWord(VersionNum));
end;

function TBASSPlayer.GetNativeFileExts : string;
begin
   if FBASSReady then
      result := '*.wav;*.mp3;*.ogg;'
   else
      result := '';
end;

function TBASSPlayer.GetStreamInfo2(StreamName : string;
                       var StreamInfo : TStreamInfo;
                       var SupportedBy : TSupportedBy;
                       var PluginNum : integer) : boolean;
var
   i : integer;
   s, ExtCode : string;
   _file : array[0..255] of char;
   _title : array[0..255] of char;
   _length_in_ms : integer;
begin
   result := false;

   with StreamInfo do
   begin
      FileName := '';
      SampleRate := 0;
      BitRate := 0;
      Duration := 0;
      Title := '';
      Artist := '';
      Album := '';
      Year := '';
      Genre := '';
      GenreID := 0;
      Track := 0;
      Comment := '';
      Channels := 0;
   end;

//   StreamInfo.FileName := ExtractFileName(StreamName);
   StreamInfo.FileName := StreamName;  // * Changed at Ver 1.3
   SupportedBy := None;
   PluginNum := -1;
   ExtCode := UpperCase(ExtractFileExt(StreamName));

 // Check if native file types
   if ExtCode = '.WAV' then
   begin
      WAV.ReadFromFile(StreamName);
      if WAV.Valid then
      begin
         with StreamInfo do
         begin
            FileSize := WAV.FileSize;
            SampleRate := WAV.SampleRate;
            Duration := round(WAV.Duration * 1000);
            Channels := WAV.ChannelModeID;
         end;
         if FBASSReady then
            SupportedBy := BASSNative;
         result := true;
      end;
   end else if ExtCode = '.MP3' then
   begin
      MPEG.ReadFromFile(StreamName);
      if MPEG.Valid then
      begin
         with StreamInfo do
         begin
            FileSize := MPEG.FileLength;
            SampleRate := MPEG.SampleRate;
            BitRate := MPEG.BitRate;
            Duration := round(MPEG.Duration * 1000);

            if MPEG.ID3v1.Exists then
            begin
               Title := MPEG.ID3v1.Title;
               Artist := MPEG.ID3v1.Artist;
               Album := MPEG.ID3v1.Album;
               Year := MPEG.ID3v1.Year;
               Genre := MPEG.ID3v1.Genre;
               GenreID := MPEG.ID3v1.GenreID;
               Track := MPEG.ID3v1.Track;
               Comment := MPEG.ID3v1.Comment;
            end else if MPEG.ID3v2.Exists then
            begin
               Title := MPEG.ID3v2.Title;
               Artist := MPEG.ID3v2.Artist;
               Album := MPEG.ID3v2.Album;
               Year := MPEG.ID3v2.Year;
               Genre := MPEG.ID3v2.Genre;
               Track := MPEG.ID3v2.Track;
               Comment := MPEG.ID3v2.Comment;
            end;

            if MPEG.ChannelMode = 'Mono' {MPEG_CM_MONO} then
               Channels := 1
            else
               Channels := 2;
         end;
         if FBASSReady then
            SupportedBy := BASSNative;
         result := true;
      end;
   end else if ExtCode = '.OGG' then
   begin
      Vorbis.ReadFromFile(StreamName);
      if Vorbis.Valid then
      begin
         with StreamInfo do
         begin
            FileSize := Vorbis.FileSize;
            SampleRate := Vorbis.SampleRate;
            BitRate := Vorbis.BitRate;
            Duration := round(Vorbis.Duration * 1000);
            Title := Vorbis.Title;
            Artist := Vorbis.Artist;
            Album := Vorbis.Album;
            Year := Vorbis.Date;
            Genre := Vorbis.Genre;
            Channels := Vorbis.ChannelModeID;
            Track := Vorbis.Track;
            Comment := Vorbis.Comment;
         end;
         if FBASSReady then
            SupportedBy := BASSNative;
         result := true;
      end;
   end;
end;

function TBASSPlayer.GetStreamInfo(StreamName : string;
                       var StreamInfo : TStreamInfo;
                       var SupportedBy : TSupportedBy) : boolean;
var
   dumNum : integer;
begin
   result := GetStreamInfo2(StreamName, StreamInfo, SupportedBy, dumNum);
end;


procedure TBASSPlayer.ProcMessage(var Msg: TMessage);
begin
   case Msg.Msg of
      WM_NewFFTData    : if Assigned(FOnNewFFTData) then
                            FOnNewFFTData(Self, BandOut);
      WM_GetToEnd      : begin
                            FPlayerMode := plmStopped;
                            if Assigned(FOnPlayEnd) then
                               FOnPlayEnd(Self);
                         end;
      WM_QueryEndSession : Msg.Result := 1;    // Allow system termination
   end;
end;


function TBASSPlayer.Open(StreamName : string) : boolean;
var
   StreamInfo : TStreamInfo;
   SupportedBy : TSupportedBy;
   PluginNum : integer;
   chattr: Integer;
   Strukt: BASS_CHANNELINFO;
begin
   result := false;

   if not FBASSReady then
   begin
      Application.MessageBox('Player is not ready !', 'Confirm', MB_OK);
      exit;
   end;

   if not GetStreamInfo2(StreamName, StreamInfo, SupportedBy, PluginNum) then
   begin
      Error('Invalid stream file(' + StreamName + ') !');
      exit;
   end;

   // release channel
   case ChannelType of
      Channel_Music : BASS_MusicFree(Channel);
      Channel_Stream: BASS_StreamFree(Channel);
   end;

    Channel := BASS_StreamCreateFile(FALSE, PChar(StreamName), 0, 0, 0{BASS_SAMPLE_FX});
    if (Channel <> 0) then
       ChannelType := Channel_Stream
    else
    begin
      Channel := BASS_MusicLoad(FALSE, PChar(StreamName), 0, 0, BASS_MUSIC_CALCLEN,0);  // * changed at Ver 1.3 (added flag BASS_MUSIC_CALCLEN)
      if (Channel <> 0) then
        ChannelType := Channel_Music
      else
        ChannelType := Channel_NotOpened;
    end;

    if (Channel = 0) then  // not a playable file
    begin
       Error('Can''t play the file');
       Exit;
    end;

    BASS_ChannelGetInfo(Channel,Strukt);
    chattr := Strukt.flags and ({BASS_SAMPLE_MONO or} BASS_SAMPLE_8BITS);
    if chattr > 0 then
    begin
    // does not support 16-bit sources.
       BASS_MusicFree(Channel);
       BASS_StreamFree(Channel);
       Error('Sound card does not support 16-bit sources.');
       Exit;
    end;

  // Reset previous handle values
   ClearEffectHandle;

  // Apply equalyzation and sound effects
   SetSoundEffect(FSoundEffects);

   FStreamName := StreamName;
   FStreamInfo := StreamInfo;
   FSupportedBy := SupportedBy;
   FPlayerMode := plmReady;

   result := true;
end;


procedure TBASSPlayer.Play(Loop: Boolean);
var
   StartOK : boolean;
   wCycle : integer;
begin
   if NowStarting then   // avoid duplicate Play operation
      exit
   else if ChannelType = Channel_NotOpened then
      exit
   else if FPlayerMode = plmPlaying then
      exit
   else if FPlayerMode = plmPaused then
   begin
      ResumePlay;
      exit;
   end else if FPlayerMode = plmStopped then
   begin
      SetPosition(0);   // Set position to starting point
      Restart;
      exit;
   end;

   StartOK := false;

 // play an opened channel
   case ChannelType of
      Channel_Music : StartOK := BASS_ChannelPlay(Channel,true);
      Channel_Stream :
      begin
        if Loop then
          Bass_ChannelSetFlags(Channel,BASS_SAMPLE_LOOP)
        else
          Bass_ChannelSetFlags(Channel,0);

        BASS_ChannelPlay(Channel,true);
      end;
   end;

   if StartOK then
   begin
      SyncHandle := BASS_ChannelSetSync(Channel, BASS_SYNC_END, 0, @SyncProc, MessageHandle);
      FPlayerMode := plmPlaying;
      if Assigned(FOnNewFFTData) then
        TimerFFT.Enabled := true;

     //
     if not Loop then
     begin
       BASS_ChannelSetSync(Channel,BASS_SYNC_POS or BASS_SYNC_ONETIME,BASS_StreamGetLength(Channel)-BASS_ChannelSeconds2Bytes(Channel,FadingTime/1000),SyncProcBeforeEnd,Integer(Self));
     end;
   end;
end;

procedure TBASSPlayer.PausePlay;
begin
   if FPlayerMode <> plmPlaying then
      exit;

   BASS_ChannelPause(Channel);

   FPlayerMode := plmPaused;
end;

procedure TBASSPlayer.ResumePlay;
begin
   if FPlayerMode <> plmPaused then
      exit;

   BASS_ChannelPlay(Channel,false);

   FPlayerMode := plmPlaying;
end;

procedure TBASSPlayer.Pause(pAction : boolean);
begin
   if pAction then
      PausePlay
   else
      ResumePlay;
end;

procedure TBASSPlayer.Stop;
begin
   if FPlayerMode <> plmPlaying then
      exit;

   BASS_ChannelStop(Channel);

   FPlayerMode := plmStopped;
end;

procedure TBASSPlayer.Restart;
var
   StartOK : boolean;
   wCycle : integer;
begin
   if FPlayerMode <> plmStopped then
      exit;
   if NowStarting then
      exit;
   StartOK := false;

  // play a opened channel
   case ChannelType of
       Channel_Music : StartOK := BASS_ChannelPlay(Channel,true);
       Channel_Stream : StartOK := BASS_ChannelPlay(Channel, true);
   end;

   if StartOK then
   begin
      FPlayerMode := plmPlaying;
   end;
end;


procedure TBASSPlayer.Close;
begin
   case ChannelType of
      Channel_Music : BASS_MusicFree(Channel);
      Channel_Stream : BASS_StreamFree(Channel);
   end;

   ChannelType := Channel_NotOpened;
   FPlayerMode := plmStandby;
   FStreamName :='';
   FSupportedBy := None;
   FStreamInfo.FileName := '';
   FStreamInfo.SampleRate := 0;
end;


function TBASSPlayer.GetChannelFFTData(PFFTData : pointer; FFTFlag : DWORD) : boolean;
begin
   result := false;

   if BASS_ChannelIsActive(Channel) <> BASS_ACTIVE_PLAYING then
      exit;

   if (FFTFlag = BASS_DATA_FFT512) or (FFTFlag = BASS_DATA_FFT1024) or
      (FFTFlag = BASS_DATA_FFT2048) then
      if BASS_ChannelGetData(Channel, PFFTData, FFTFlag) <> $ffffffff then
         result := true;
end;


// If you have any idea on spectrum visualization for better result,
// please let me know.
procedure TBASSPlayer.TimerFFTTimer(Sender: TObject);
const
 // Classify FFTData[x] by the number of required bands for spectrum visualization.
 // FreqCoef defines the last index number of FFTData[x] for each band.
   FreqCoef : array[0..NumFFTBands - 1] of word =
               (  1,  2,    3,   6,       // narrow interval for low freq. range
                 12,  18,  24,  30,  36,  // normal interval for middle freq. range
                 42,  48,  54,  60,  66,
                 72,  78,  84,  90,  96,
                102, 108,
                120, 132, 156, 180 );     // wide interval for high freq. range

  // If the sampling rate is (standard) 44100Hz then the frequency of FFTData[x]
  // is 44100 / 512 * x, so the representative(or maximum) frequency of each
  // band is as followings
  //  BandOut[0] : 86.1Hz     BandOut[1] : 172.3Hz    BandOut[2] : 344.5Hz
  //  BandOut[3] : 516.8Hz    BandOut[4] : 1033.6Hz   BandOut[5] : 1550.4Hz
  //  . . . (516.8Hz interval upto BandOut[20]) . .   BandOut[20] : 9302.2Hz
  //  BandOut[21] : 10336Hz   BandOut[22] : 11370Hz   BandOut[23] : 13437Hz
  //  BandOut[24] : 15503Hz   . . very high frequency range is excluded

   Boost = 0.15; // Factor to intensify high frequency bands
   Scale = 80;   // Factor to calibrate the output range of BandOut[x]
                 //   ( approximately from 0 to 24 ).
var
   FFTData : TFFTData;
   NewBandOut : TBandOut;
   StartIndex : integer;
   i, k : integer;
   tmpIntensity : double;

   SpectrumData: array[0..2048] of float;
   WaveformData: array[0..1151] of DWORD;
   p1 : PDWORD;
   p2 : PBYTE;

   freq, dum1 : dword;
   dum : integer;

   Strukt: BASS_CHANNELINFO;
begin
   if BASS_ChannelIsActive(Channel) <> BASS_ACTIVE_PLAYING then
   begin
      TimerFFT.Enabled := false;
      exit;
   end;

   if BASS_ChannelGetData(Channel, @FFTData, BASS_DATA_FFT512) = $ffffffff then
   begin
      for i := 0 to (NumFFTBands - 1) do
         BandOut[i] := 0;
      exit;
   end;

   TimerFFT.Enabled := false;  // Avoid duplicate timer interrupt

   for i := 0 to (NumFFTBands - 1) do
   begin
       if i = 0 then
          StartIndex := 1
       else
          StartIndex := FreqCoef[i-1] + 1;

    // tmpIntensity <= the greatest value in classified FFTData[x]'s
       tmpIntensity := 0;
       for k := StartIndex to FreqCoef[i] do
           if FFTData[k] > tmpIntensity then
              tmpIntensity := FFTData[k];

       NewBandOut[i] := round(tmpIntensity * (1 + i * Boost) * Scale);

    // Decrease the value of BandOut[i] smoothly for better looking
       if NewBandOut[i] > BandOut[i] then
          BandOut[i] := NewBandOut[i]
       else
          if BandOut[i] >= 2 then
             dec(BandOut[i], 2)
          else
             BandOut[i] := 0;

    // for the case that original NewBandOut[i] is smaller than BandOut[i] by 1
       if NewBandOut[i] > BandOut[i] then
          BandOut[i] := NewBandOut[i];
   end;

 //  if not GoVisOut then
      PostMessage(MessageHandle, WM_NewFFTData, 0, 0);

   if not GoVisOut then
   begin
      TimerFFT.Enabled := true;
      exit;
   end;

   BASS_ChannelGetAttributes(Channel, freq, dum1, dum);
   p1 := ShareMemPointer;
   p1^ := freq;
   inc(p1, 1);
   p1^ := 2;  // Channels
   inc(p1, 1);
   p2 := pointer(p1);

   FillChar(SpectrumData, SizeOf(SpectrumData), 0);
   FillChar(WaveformData, SizeOf(waveformData), 0);

 // for stereo FFT, use BASS_DATA_FFT2048S instead of BASS_DATA_FFT2048
 //   bass_channelgetdata(Channel, @spectrumdata, BASS_DATA_FFT2048S);
   bass_channelgetdata(Channel, @spectrumdata, BASS_DATA_FFT2048);
   for i := 1 To 576 do
   begin
     p2^ := trunc(SpectrumData[i] * 255);
     inc(p2);
   end;
   for i := 1 To 576 do
   begin
     p2^ := trunc(SpectrumData[i] * 255);
     inc(p2);
   end;

   BASS_ChannelGetInfo(Channel,Strukt);
   dum1 := Strukt.flags;
   if ((dum1 and BASS_SAMPLE_8BITS) = BASS_SAMPLE_8BITS) or
      ((dum1 and BASS_SAMPLE_MONO) = BASS_SAMPLE_MONO) then
   begin
     bass_channelgetdata(Channel, @waveformdata, 1152{=576*2});
     for i := 0 to 575 do
     begin
       p2^ := waveformdata[i] div 256;
       inc(p2);
     end;
     for i := 0 to 575 do
     begin
       p2^ := waveformdata[i] div 256;
       inc(p2);
     end;
   end else
   begin
      bass_channelgetdata(Channel, @waveformdata, 2304{=576*4});
      for i := 0 to 575 do
      begin
        p2^ := waveformdata[i*2] div 256;
        inc(p2);
      end;
      for i := 0 to 575 do
      begin
        p2^ := waveformdata[i*2+1] div 256;
        inc(p2);
      end;
   end;

 // Inform "VisOut.exe" that visualization data is ready
   TimerFFT.Enabled := true;
end;


procedure TBASSPlayer.FadeTo(Filename: String; Loop: Boolean);
var
  ChannelInfo: BASS_CHANNELINFO;
begin
  BASS_ChannelSetSync(Channel,BASS_SYNC_SLIDE,0,SyncProcChannelFree,0);
  BASS_ChannelSlideAttributes(Channel,-1,0,-101,FadingTime);
  Channel:=0;
  Open(FileName);
  BASS_ChannelSetAttributes(Channel,0,0,-101);
  BASS_ChannelPlay(Channel,true);
  BASS_ChannelSlideAttributes(Channel,-1,100,-101,FadingTime);
  BASS_ChannelGetInfo(Channel,ChannelInfo);
  if not Loop then
  begin
    BASS_ChannelSetSync(Channel,BASS_SYNC_POS or BASS_SYNC_ONETIME,BASS_StreamGetLength(Channel)-BASS_ChannelSeconds2Bytes(Channel,FadingTime/1000),SyncProcBeforeEnd,Integer(Self));
    BASS_ChannelSetFlags(Channel,ChannelInfo.flags and (not BASS_SAMPLE_LOOP));
  end
  else
    BASS_ChannelSetFlags(Channel,ChannelInfo.flags or BASS_SAMPLE_LOOP);
end;

procedure Register;
begin
   RegisterComponents('Samples', [TBASSPlayer]);
end;

function TBASSPlayer.AddSample(ID: Cardinal; Stream: TMemoryStream): Boolean;
var
  Sample: HSAMPLE;
begin
  Sample:=BASS_SampleLoad(true,Stream.Memory,0,Stream.Size,10,BASS_SAMPLE_OVER_POS);

  result:=Sample<>0;

  if result then
    fHashMap.InsertID(ID,Sample);

end;

function TBASSPlayer.ExistsSample(ID: Cardinal): boolean;
var
  Key: Integer;
begin
  result:=fHashMap.FindKey(ID,Key);
end;

function TBASSPlayer.PlaySample(ID: Cardinal): Boolean;
var
  Key: HSample;
begin
  result:=true;
  if fHashMap.FindKey(ID,Integer(Key)) then
    BASS_ChannelPlay(BASS_SampleGetChannel(Key,false),true)
  else
    result:=false;
end;

procedure TBASSPlayer.StopSample(ID: Cardinal);
var
  Key: HSample;
begin
  if fHashMap.FindKey(ID,Integer(Key)) then
    BASS_SampleStop(Key);
end;

function TBASSPlayer.GetSampleVolume: Integer;
begin
  result:=BASS_GetConfig(BASS_CONFIG_GVOL_SAMPLE);
end;

procedure TBASSPlayer.SetSampleVolume(const Value: Integer);
var
  tmpValue : integer;
begin
  if Value > 100 then
    tmpValue := 100
  else if Value < -1 then
    tmpValue := 0
  else
    tmpValue := Value;

  BASS_SetConfig(BASS_CONFIG_GVOL_SAMPLE,tmpValue);
end;

procedure TBASSPlayer.RemoveSample(ID: Cardinal);
var
  Key: Integer;
begin
  fHashMap.FindKey(ID,Key);

  BASS_SampleFree(Key);

  fHashMap.DeleteKey(ID);
end;

end.
