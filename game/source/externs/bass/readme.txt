I am not a native English speaker.
Please be patient for my clumsy expressions.

TBASSPlayer is a stream file player component which can play WAV, MP3, WMA and Ogg Vorbis files using BASS sound system (bass.dll & basswma.dll).
TBASSPlayer also can play other stream file types using Winamp 2 input plug-ins.
Although BASS sound system is an excellent audio library, its native decoding ability is limited in some popular types of stream files.
With TBASSPlayer, you can use Winamp 2 input plug-ins to play stream files which BASS sound system cannot handle.
TBASSPlayer can load upto 8 Winamp 2 input plug-ins simultaneously.
TBASSPlayer also supports Winamp 2 visualization plug-ins with a seperate visualizer program.

Please read included BASS.txt and visit http://www.un4seen.com/ for futher information on BASS sound system.

The Delphi unit files used to make TBASSPlayer are listed below.
  - BASSPlayer.pas : main unit
  - PluginCtrl.pas : sub unit to manage Winamp 2 input plug-ins and to interface
                     BASS sound system to Winamp 2 input plug-in
  - PluginConfig.pas, PluginConfig.dfm : sub unit to help easier managing of Winamp 2 input
                     plug-ins by supplying visual dialog form.
  - ioplug.pas     : Winamp i/o plugin header adaption for Delphi/Pascal written
                     by Mr. Christian Zbinden
  - MPEGAudio.pas, ID3v1.pas, ID3v2.pas, OggVorbis.pas, WMAFile.pas and WAVFile.pas :
                   written by Mr. Jurgen Faul, used to retrieve information on stream file
                   ( Visit http://jfaul.de/atl/ for futher information on these files)
  - RT_BASS.pas, RT_BASSWMA.pas : These units are modified version of BASS.pas and BASSWMA.pas
                   written by Mr. Ian Luck.
                   I have changed the originals just a little to adapt BASSPlayer.pas.
                   These units are used for interfacing TBASSPlayer to bass.dll & basswma.dll.
  - fisSharedMemory.pas : A product of First Internet Software House 2000
                   I have modified the originals just a little to adapt TBASSPlayer.
                   This unit(component) is used to share memory between TBASSPlayer and the
                   visualizer program, VisOut.exe.
  - appexec.pas : written by Patrick Brisacier and Jean-Fabien Connault
                  This unit(component) is used as a launcher of visualizer program(VisOut.exe).

The Delphi files to make the visualizer program(VisOut.exe) are listed below.
  - VisOut.dpr : project file
  - VisOutIntf.pas, VisOutIntf.dfm : main unit files
You should install TfisSharedMemory component(fisSharedMemory.pas) to compile VisOut.dpr.

The Delphi files to make the demo program(BASSPlay.exe) are listed below.
  - BASSPlay.dpr : project file
  - BassTest.pas, BassTest.dfm : main unit files
  - VisPluginCtrl.pas, VisPluginCtrl.dfm : sub unit files to manage Winamp 2 visualization
                  plug-ins.

I have also included two units(components), which I used for the demo project 'BASSPlay.dpr' to show the usage of TBASSPlayer.
  - Slider.pas(TSlider) : written by Mr. AO ROSNO (?)
  - Knob.pas(TKnob) : written by Mr. Gary Bradley Hardman


Note : The executable program which uses BASS sound system requires that bass.dll &
       basswma.dll should be installed in the directory where the program resides in.
       It is also necessary that bass.dll should be installed in <directory Delphi installed>\Bin
       to get correct values of some properties at design time, prior to putting
       TBASSPlayer component on a form. (not necessary if you create TBASSPlayer
       using "Create" method in program - e.g. BASSPlayer1 := TBASSplayer.Create;)
  
You do not need install TBASSPlayer to compile BASSPlay.dpr, because the main program of BASSPlay.dpr uses 'Create' method to create TBASSPlayer.
But you must install above TSlider and TKnob prior to compiling BASSPlay.dpr.
TBASSPlayer has been successfully tested on Delphi 4.

The programs written by me are freeware.
Please inform me if you have any advice or improvement on TBASSPlayer.
 
                   5 Apr 2003

 Silhwan Hyun, hsh@chollian.net      

ps) If you have the header file of a Winamp 3 input plug-in, please let me know.
    I need it to make TBASSPlayer accept Winamp 3 input plug-ins also. 