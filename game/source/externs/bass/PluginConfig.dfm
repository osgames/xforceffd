�
 TPLUGINCONFIGFORM 0�  TPF0TPluginConfigFormPluginConfigFormLeftyTop� BorderStylebsDialogCaptionPlug-in ConfigurationClientHeightRClientWidthFColor	clBtnFaceFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style OldCreateOrderPositionpoDesktopCenterPixelsPerInch`
TextHeight TLabelLabel1Left,ToptWidth0HeightCaptionPlug-insFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFont  TLabelLabel2LeftTopWidth�HeightCaptionDNote : Plug-ins must reside in <program_directory>\Plugins directoryFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFont  TLabelLabel3Left.ToptWidthbHeightCaptionLoaded Plug-insFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFont  TLabelLabel4Left>TopWidth� HeightCaption#Only Winamp 2 version is acceptableFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFont  TLabelLabel5Left?Top1Width� HeightCaption Maximum 8 plug-ins can be loadedFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFont  TLabelLabel6LeftrTopIWidth�HeightCaptionH( if checked then Winamp plug-in is applied for the file types which canFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFont  TLabelLabel7LeftzTopZWidthHeightCaption+be played by both BASS and Winamp plug-in )Font.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFont  TListBox
PluginListLeftTop� WidthyHeight� ImeName�ѱ��� �Է� �ý��� (IME 2000)
ItemHeightTabOrder   TStringGrid
PluginGridLeft� Top� Width�Height� ColCountDefaultRowHeight
FixedColorclWhite	FixedCols RowCount	OptionsgoFixedVertLinegoFixedHorzLine
goVertLine
goHorzLinegoColSizinggoRowSelect TabOrder	ColWidthsT� �    TButtonbtnLoadLeft)Top/WidthKHeightCaptionLoadTabOrderOnClickbtnLoadClick  TButton	btnUnloadLeft� Top/WidthKHeightCaptionUnloadTabOrderOnClickbtnUnloadClick  TButtonbtnAboutLeftPTop/WidthKHeightCaptionAboutTabOrderOnClickbtnAboutClick  TButtonbtnConfigureLeft�Top/WidthKHeightCaption	ConfigureTabOrderOnClickbtnConfigureClick  TButtonbtnCloseLeft�TopWidthKHeightCaptionCloseTabOrderOnClickbtnCloseClick  	TCheckBoxPluginFirstLeftTopJWidthXHeightCaptionPlugin FirstFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrderOnClickPluginFirstClick   