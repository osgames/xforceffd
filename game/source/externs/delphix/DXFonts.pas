unit DXFonts;

interface
// Modified by Michael Wilson 2/05/2001
// - re-added redundant assignment to Offset
// Modified by Marcus Knight 19/12/2000
// - replaces all referaces to 'pos' with 'AnsiPos' <- faster
// - replaces all referaces to 'uppercase' with 'Ansiuppercase' <- faster
// - Now only uppercases outside the loop
// - Fixed the non-virtual contructor
// - renamed & moved Offset to private(fOffSet), and added the property OffSet
// - Commented out the redundant assignment to Offset<- not needed, as Offset is now a readonly property
// - Added the Notification method to catch when the image list is destroyed
// - removed DXclasses from used list
uses
  SysUtils, Classes, Controls, DXDraws;

const              
  Alphabet = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ"at  0123456789<>=()-''!_+\/{}^&%.=$#���?*';

type
  TDXFont = class(TComponent)
  private
    FDXImageList: TDXImageList;
    FFont: string;
    FFontIndex: Integer;
    FOffset: integer; // renamed from Offset -> fOffset
    procedure SetFont(const Value: string);
    procedure SetFontIndex(const Value: Integer);
  Protected
    Procedure Notification(AComponent: TComponent; Operation: TOperation); Override; // added
  public
    constructor Create(AOwner: TComponent); override; // Modified
    destructor Destroy; override;
    procedure TextOut(DirectDrawSurface: TDirectDrawSurface; X, Y: Integer; const Text: string);
    Property Offset : integer read FOffset write FOffset; // added
  published
    property Font: string read FFont write SetFont;
    property FontIndex: Integer read FFontIndex write SetFontIndex;
    property DXImageList: TDXImageList read FDXImageList write FDXImageList;
  end;

Procedure Register;
implementation

Procedure Register;
begin
  RegisterComponents('DelphiX',[TDXFont]);
end;

constructor TDXFont.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
end;

destructor TDXFont.Destroy;
begin
  inherited Destroy;
end;

// Added
Procedure TDXFont.Notification(AComponent: TComponent; Operation: TOperation);
begin
inherited Notification(AComponent, Operation);
if (Operation = opRemove) and (AComponent = FDXImageList) then
  begin
  FDXImageList := nil;
  end;
end; {Notification}

procedure TDXFont.SetFont(const Value: string);
begin
  FFont := Value;
  if assigned(FDXImageList) then
  begin
    FFontIndex := FDXImageList.items.IndexOf(FFont); { find font once }
    fOffset := FDXImageList.Items[FFontIndex].PatternWidth; // Modified
  end;
end;

procedure TDXFont.SetFontIndex(const Value: Integer);
begin
  FFontIndex := Value;
  if assigned(FDXImageList) then
  begin
    FFont := FDXImageList.Items[FFontIndex].Name;
    fOffset := FDXImageList.Items[FFontIndex].PatternWidth; // Modified
  end;
end;

procedure TDXFont.TextOut(DirectDrawSurface: TDirectDrawSurface; X, Y: Integer; const Text: string);
var
  loop, letter: integer;
  UpperText : string; // added
begin
  if not assigned(FDXImageList) then
    exit;
  Offset := FDXImageList.Items[FFontIndex].PatternWidth;
  UpperText := AnsiUppercase(text);  // added
  for loop := 1 to Length(UpperText) do
  begin
    letter := AnsiPos(UpperText[loop], Alphabet) - 1; // Modified
    if letter < 0 then letter := 30;
    FDXImageList.items[FFontIndex].Draw(DirectDrawSurface, x + Offset * loop, y, letter);
  end; { loop }
end;

end.

