{
TDIBEffect is a TDIB descendant
with Some Effects like AntiAlias, Contrast,
Lightness, Saturation, GaussianBlur, Mosaic,
Twist, Splitlight, Trace, Emboss, etc.
Works with 24bit color DIBs.

This component is based on TProEffectImage component version 1.0 by
Written By Babak Sateli (babak_sateli@yahoo.com, http://raveland.netfirms.com)

and modified by (c) 2004 Jaro Benes
for DelphiX use.

Demo was modifies into DXForm with function like  original

DISCLAIMER
This component is provided AS-IS without any warranty of any kind, either express or
implied. This component is freeware and can be used in any software product.
}
unit DXDIBEffect;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Math, DIB;

Type
  TFilterTypeResample = (ftrBox, ftrTriangle,ftrHermite, ftrBell, ftrBSpline,
    ftrLanczos3, ftrMitchell);
Const
  DefaultFilterRadius: Array[TFilterTypeResample] of Single = (0.5,1,1,1.5,2,3,2);

type
  TDIBEffect = class(TDIB)
  private
    { Private declarations }
  protected
    { Protected declarations }
  public
    { Public declarations }
  published
    Procedure Effect_Invert;
    Procedure Effect_AddColorNoise   (Amount:Integer);
    Procedure Effect_AddMonoNoise    (Amount:Integer);
    Procedure Effect_AntiAlias;
    Procedure Effect_Contrast        (Amount:Integer);
    Procedure Effect_FishEye         (Amount:Integer);
    Procedure Effect_GrayScale;
    Procedure Effect_Lightness       (Amount:Integer);
    Procedure Effect_Darkness        (Amount:Integer);
    Procedure Effect_Saturation      (Amount:Integer);
    Procedure Effect_SplitBlur       (Amount:Integer);
    Procedure Effect_GaussianBlur    (Amount:Integer);
    Procedure Effect_Mosaic          (Size:Integer);
    Procedure Effect_Twist           (Amount:Integer);
    Procedure Effect_Splitlight      (Amount:integer);
    Procedure Effect_Tile            (Amount: integer);
    Procedure Effect_SpotLight       (Amount: integer; Spot: TRect);
    Procedure Effect_Trace           (Amount: integer);
    Procedure Effect_Emboss;
    Procedure Effect_Solorize        (Amount: integer);
    Procedure Effect_Posterize       (Amount: integer);
    Procedure Effect_Brightness      (Amount: integer);
    Procedure Effect_Resample        (AmountX,AmountY: integer; TypeResample: TFilterTypeResample);
    Procedure Effect_Colorize (ForeColor, BackColor:TColor);
    { Published declarations }
  end;


implementation


procedure PicInvert(src: TDIB);
var w,h,x,y:integer;
    p:pbytearray;
begin
  w:=src.width;
  h:=src.height;
  //src.PixelFormat :=pf24bit;
  src.BitCount := 24;
  for y:=0 to h-1 do
  begin
    p:=src.scanline[y];
    for x:=0 to w-1 do
    begin
      p[x*3]:= not p[x*3];
      p[x*3+1]:= not p[x*3+1];
      p[x*3+2]:= not p[x*3+2];
    end;
  end;
end;

function IntToByte(i:Integer):Byte;
begin
  if      i>255 then Result:=255
  else if i<0   then Result:=0
  else               Result:=i;
end;

procedure AddColorNoise(var clip: TDIB; Amount: Integer);
var
  p0:pbytearray;
  x,y,r,g,b: Integer;
begin
  for y:=0 to clip.Height-1 do
  begin
    p0:=clip.ScanLine [y];
    for x:=0 to clip.Width-1 do
    begin
      r:=p0[x*3]+(Random(Amount)-(Amount shr 1));
      g:=p0[x*3+1]+(Random(Amount)-(Amount shr 1));
      b:=p0[x*3+2]+(Random(Amount)-(Amount shr 1));
      p0[x*3]:=IntToByte(r);
      p0[x*3+1]:=IntToByte(g);
      p0[x*3+2]:=IntToByte(b);
    end;
  end;
end;

procedure _AddMonoNoise(var clip: TDIB; Amount: Integer);
var
p0:pbytearray;
x,y,a,r,g,b: Integer;
begin
  for y:=0 to clip.Height-1 do
  begin
    p0:=clip.scanline[y];
    for x:=0 to clip.Width-1 do
    begin
      a:=Random(Amount)-(Amount shr 1);
      r:=p0[x*3]+a;
      g:=p0[x*3+1]+a;
      b:=p0[x*3+2]+a;
      p0[x*3]:=IntToByte(r);
      p0[x*3+1]:=IntToByte(g);
      p0[x*3+2]:=IntToByte(b);
    end;
  end;
end;

procedure AntiAliasRect(clip: TDIB; XOrigin, YOrigin, XFinal, YFinal: Integer);
var Memo,x,y: Integer; (* Composantes primaires des points environnants *)
    p0,p1,p2:pbytearray;
begin
  if XFinal<XOrigin then begin Memo:=XOrigin; XOrigin:=XFinal; XFinal:=Memo; end;  (* Inversion des valeurs   *)
  if YFinal<YOrigin then begin Memo:=YOrigin; YOrigin:=YFinal; YFinal:=Memo; end;  (* si diff�rence n�gative*)
  XOrigin:=max(1,XOrigin);
  YOrigin:=max(1,YOrigin);
  XFinal:=min(clip.width-2,XFinal);
  YFinal:=min(clip.height-2,YFinal);
  clip.BitCount := 24;
  for y:=YOrigin to YFinal do begin
    p0:=clip.ScanLine [y-1];
    p1:=clip.scanline [y];
    p2:=clip.ScanLine [y+1];
    for x:=XOrigin to XFinal do begin
      p1[x*3]:=(p0[x*3]+p2[x*3]+p1[(x-1)*3]+p1[(x+1)*3])div 4;
      p1[x*3+1]:=(p0[x*3+1]+p2[x*3+1]+p1[(x-1)*3+1]+p1[(x+1)*3+1])div 4;
      p1[x*3+2]:=(p0[x*3+2]+p2[x*3+2]+p1[(x-1)*3+2]+p1[(x+1)*3+2])div 4;
    end;
  end;
end;

procedure AntiAlias(clip: TDIB);
begin
  AntiAliasRect(clip,0,0,clip.width,clip.height);
end;

procedure _Contrast(var clip: TDIB; Amount: Integer);
var
  p0:pbytearray;
  rg,gg,bg,r,g,b,x,y:  Integer;
begin
  for y:=0 to clip.Height-1 do
  begin
    p0:=clip.scanline[y];
    for x:=0 to clip.Width-1 do
    begin
      r:=p0[x*3];
      g:=p0[x*3+1];
      b:=p0[x*3+2];
      rg:=(Abs(127-r)*Amount)div 255;
      gg:=(Abs(127-g)*Amount)div 255;
      bg:=(Abs(127-b)*Amount)div 255;
      if r>127 then r:=r+rg else r:=r-rg;
      if g>127 then g:=g+gg else g:=g-gg;
      if b>127 then b:=b+bg else b:=b-bg;
      p0[x*3]:=IntToByte(r);
      p0[x*3+1]:=IntToByte(g);
      p0[x*3+2]:=IntToByte(b);
    end;
  end;
end;

procedure _FishEye(var Bmp, Dst: TDIB; Amount: Extended);
var
  xmid,ymid              : Single;
  fx,fy                  : Single;
  r1, r2                 : Single;
  ifx, ify               : integer;
  dx, dy                 : Single;
  rmax                   : Single;
  ty, tx                 : Integer;
  weight_x, weight_y     : array[0..1] of Single;
  weight                 : Single;
  new_red, new_green     : Integer;
  new_blue               : Integer;
  total_red, total_green : Single;
  total_blue             : Single;
  ix, iy                 : Integer;
  sli, slo               : PByteArray;
begin
  xmid := Bmp.Width/2;
  ymid := Bmp.Height/2;
  rmax := Dst.Width * Amount;

  for ty := 0 to Dst.Height - 1 do begin
    for tx := 0 to Dst.Width - 1 do begin
      dx := tx - xmid;
      dy := ty - ymid;
      r1 := Sqrt(dx * dx + dy * dy);
      if r1 = 0 then begin
        fx := xmid;
        fy := ymid;
      end
      else begin
        r2 := rmax / 2 * (1 / (1 - r1/rmax) - 1);
        fx := dx * r2 / r1 + xmid;
        fy := dy * r2 / r1 + ymid;
      end;
      ify := Trunc(fy);
      ifx := Trunc(fx);
                // Calculate the weights.
      if fy >= 0  then begin
        weight_y[1] := fy - ify;
        weight_y[0] := 1 - weight_y[1];
      end else begin
        weight_y[0] := -(fy - ify);
        weight_y[1] := 1 - weight_y[0];
      end;
      if fx >= 0 then begin
        weight_x[1] := fx - ifx;
        weight_x[0] := 1 - weight_x[1];
      end else begin
        weight_x[0] := -(fx - ifx);
        Weight_x[1] := 1 - weight_x[0];
      end;

      if ifx < 0 then
        ifx := Bmp.Width-1-(-ifx mod Bmp.Width)
      else if ifx > Bmp.Width-1  then
        ifx := ifx mod Bmp.Width;
      if ify < 0 then
        ify := Bmp.Height-1-(-ify mod Bmp.Height)
      else if ify > Bmp.Height-1 then
        ify := ify mod Bmp.Height;

      total_red   := 0.0;
      total_green := 0.0;
      total_blue  := 0.0;
      for ix := 0 to 1 do begin
        for iy := 0 to 1 do begin
          if ify + iy < Bmp.Height then
            sli := Bmp.scanline[ify + iy]
          else
            sli := Bmp.scanline[Bmp.Height - ify - iy];
          if ifx + ix < Bmp.Width then begin
            new_red := sli[(ifx + ix)*3];
            new_green := sli[(ifx + ix)*3+1];
            new_blue := sli[(ifx + ix)*3+2];
          end
          else begin
            new_red := sli[(Bmp.Width - ifx - ix)*3];
            new_green := sli[(Bmp.Width - ifx - ix)*3+1];
            new_blue := sli[(Bmp.Width - ifx - ix)*3+2];
          end;
          weight := weight_x[ix] * weight_y[iy];
          total_red   := total_red   + new_red   * weight;
          total_green := total_green + new_green * weight;
          total_blue  := total_blue  + new_blue  * weight;
        end;
      end;
      slo := Dst.scanline[ty];
      slo[tx*3] := Round(total_red);
      slo[tx*3+1] := Round(total_green);
      slo[tx*3+2] := Round(total_blue);

    end;
  end;
end;

procedure GrayScale(var clip: TDIB);
var
  p0:pbytearray;
  Gray,x,y: Integer;
begin
  for y:=0 to clip.Height-1 do begin
    p0:=clip.scanline[y];
    for x:=0 to clip.Width-1 do begin
      Gray:=Round(p0[x*3]*0.3+p0[x*3+1]*0.59+p0[x*3+2]*0.11);
      p0[x*3]:=Gray;
      p0[x*3+1]:=Gray;
      p0[x*3+2]:=Gray;
    end;
  end;
end;


procedure _Lightness(var clip: TDIB; Amount: Integer);
var
  p0:pbytearray;
  r,g,b,x,y: Integer;
begin
  for y:=0 to clip.Height-1 do begin
    p0:=clip.scanline[y];
    for x:=0 to clip.Width-1 do begin
      r:=p0[x*3];
      g:=p0[x*3+1];
      b:=p0[x*3+2];
      p0[x*3]:=IntToByte(r+((255-r)*Amount)div 255);
      p0[x*3+1]:=IntToByte(g+((255-g)*Amount)div 255);
      p0[x*3+2]:=IntToByte(b+((255-b)*Amount)div 255);
    end;
  end;
end;

procedure Darkness(var src: TDIB; Amount: integer);
var
  p0:pbytearray;
  r,g,b,x,y: Integer;
begin
  src.BitCount := 24;
  for y:=0 to src.Height-1 do begin
    p0:=src.scanline[y];
    for x:=0 to src.Width-1 do begin
      r:=p0[x*3];
      g:=p0[x*3+1];
      b:=p0[x*3+2];
      p0[x*3]:=IntToByte(r-((r)*Amount)div 255);
      p0[x*3+1]:=IntToByte(g-((g)*Amount)div 255);
      p0[x*3+2]:=IntToByte(b-((b)*Amount)div 255);
    end;
  end;
end;


procedure _Saturation(var clip: TDIB; Amount: Integer);
var
  p0:pbytearray;
  Gray,r,g,b,x,y: Integer;
begin
  for y:=0 to clip.Height-1 do begin
    p0:=clip.scanline[y];
    for x:=0 to clip.Width-1 do begin
      r:=p0[x*3];
      g:=p0[x*3+1];
      b:=p0[x*3+2];
      Gray:=(r+g+b)div 3;
      p0[x*3]:=IntToByte(Gray+(((r-Gray)*Amount)div 255));
      p0[x*3+1]:=IntToByte(Gray+(((g-Gray)*Amount)div 255));
      p0[x*3+2]:=IntToByte(Gray+(((b-Gray)*Amount)div 255));
    end;
  end;
end;

procedure SmoothResize(var Src, Dst: TDIB);
var
  x,y,xP,yP,
  yP2,xP2:     Integer;
  Read,Read2:  PByteArray;
  t,z,z2,iz2:  Integer;
  pc:PBytearray;
  w1,w2,w3,w4: Integer;
  Col1r,col1g,col1b,Col2r,col2g,col2b:   byte;
begin
  xP2:=((src.Width-1)shl 15)div Dst.Width;
  yP2:=((src.Height-1)shl 15)div Dst.Height;
  yP:=0;
  for y:=0 to Dst.Height-1 do begin
    xP:=0;
    Read:=src.ScanLine[yP shr 15];
    if yP shr 16<src.Height-1 then
      Read2:=src.ScanLine [yP shr 15+1]
    else
      Read2:=src.ScanLine [yP shr 15];
    pc:=Dst.scanline[y];
    z2:=yP and $7FFF;
    iz2:=$8000-z2;
    for x:=0 to Dst.Width-1 do begin
      t:=xP shr 15;
      Col1r:=Read[t*3];
      Col1g:=Read[t*3+1];
      Col1b:=Read[t*3+2];
      Col2r:=Read2[t*3];
      Col2g:=Read2[t*3+1];
      Col2b:=Read2[t*3+2];
      z:=xP and $7FFF;
      w2:=(z*iz2)shr 15;
      w1:=iz2-w2;
      w4:=(z*z2)shr 15;
      w3:=z2-w4;
      pc[x*3+2]:=
        (Col1b*w1+Read[(t+1)*3+2]*w2+
         Col2b*w3+Read2[(t+1)*3+2]*w4)shr 15;
      pc[x*3+1]:=
        (Col1g*w1+Read[(t+1)*3+1]*w2+
         Col2g*w3+Read2[(t+1)*3+1]*w4)shr 15;
      pc[x*3]:=
        (Col1r*w1+Read2[(t+1)*3]*w2+
         Col2r*w3+Read2[(t+1)*3]*w4)shr 15;
      Inc(xP,xP2);
    end;
    Inc(yP,yP2);
  end;
end;

function TrimInt(i, Min, Max: Integer): Integer;
begin
  if      i>Max then Result:=Max
  else if i<Min then Result:=Min
  else               Result:=i;
end;

procedure SmoothRotate(var Src, Dst: TDIB; cx, cy: Integer;
  Angle: Extended);
type
 TFColor  = record b,g,r:Byte end;
var
  Top, Bottom, Left, Right, eww,nsw, fx,fy, wx,wy: Extended;
  cAngle, sAngle:   Double;
  xDiff, yDiff, ifx,ify, px,py, ix,iy, x,y: Integer;
  nw,ne, sw,se: TFColor;
  P1,P2,P3:Pbytearray;
begin
  Angle:=angle;
  Angle:=-Angle*Pi/180;
  sAngle:=Sin(Angle);
  cAngle:=Cos(Angle);
  xDiff:=(Dst.Width-Src.Width)div 2;
  yDiff:=(Dst.Height-Src.Height)div 2;
  for y:=0 to Dst.Height-1 do begin
    P3:=Dst.scanline[y];
    py:=2*(y-cy)+1;
    for x:=0 to Dst.Width-1 do begin
      px:=2*(x-cx)+1;
      fx:=(((px*cAngle-py*sAngle)-1)/ 2+cx)-xDiff;
      fy:=(((px*sAngle+py*cAngle)-1)/ 2+cy)-yDiff;
      ifx:=Round(fx);
      ify:=Round(fy);

      if(ifx>-1)and(ifx<Src.Width)and(ify>-1)and(ify<Src.Height)then begin
        eww:=fx-ifx;
        nsw:=fy-ify;
        iy:=TrimInt(ify+1,0,Src.Height-1);
        ix:=TrimInt(ifx+1,0,Src.Width-1);
        P1:=Src.scanline[ify];
        P2:=Src.scanline[iy];
        nw.r:=P1[ifx*3];
        nw.g:=P1[ifx*3+1];
        nw.b:=P1[ifx*3+2];
        ne.r:=P1[ix*3];
        ne.g:=P1[ix*3+1];
        ne.b:=P1[ix*3+2];
        sw.r:=P2[ifx*3];
        sw.g:=P2[ifx*3+1];
        sw.b:=P2[ifx*3+2];
        se.r:=P2[ix*3];
        se.g:=P2[ix*3+1];
        se.b:=P2[ix*3+2];

        Top:=nw.b+eww*(ne.b-nw.b);
        Bottom:=sw.b+eww*(se.b-sw.b);
        P3[x*3+2]:=IntToByte(Round(Top+nsw*(Bottom-Top)));

        Top:=nw.g+eww*(ne.g-nw.g);
        Bottom:=sw.g+eww*(se.g-sw.g);
        P3[x*3+1]:=IntToByte(Round(Top+nsw*(Bottom-Top)));

        Top:=nw.r+eww*(ne.r-nw.r);
        Bottom:=sw.r+eww*(se.r-sw.r);
        P3[x*3]:=IntToByte(Round(Top+nsw*(Bottom-Top)));
      end;
    end;
  end;
end;


procedure _SplitBlur(var clip: TDIB; Amount: integer);
var
  p0,p1,p2: pbytearray;
  cx,x,y: Integer;
  Buf: array[0..3,0..2] of byte;
begin
  if Amount=0 then Exit;
  for y:=0 to clip.Height-1 do begin
    p0:=clip.scanline[y];
    if y-Amount<0         then p1:=clip.scanline[y]
    else {y-Amount>0}          p1:=clip.ScanLine[y-Amount];
    if y+Amount<clip.Height    then p2:=clip.ScanLine[y+Amount]
    else {y+Amount>=Height}    p2:=clip.ScanLine[clip.Height-y];

    for x:=0 to clip.Width-1 do begin
      if x-Amount<0     then cx:=x
      else {x-Amount>0}      cx:=x-Amount;
      Buf[0,0]:=p1[cx*3];
      Buf[0,1]:=p1[cx*3+1];
      Buf[0,2]:=p1[cx*3+2];
      Buf[1,0]:=p2[cx*3];
      Buf[1,1]:=p2[cx*3+1];
      Buf[1,2]:=p2[cx*3+2];
      if x+Amount<clip.Width     then cx:=x+Amount
      else {x+Amount>=Width}     cx:=clip.Width-x;
      Buf[2,0]:=p1[cx*3];
      Buf[2,1]:=p1[cx*3+1];
      Buf[2,2]:=p1[cx*3+2];
      Buf[3,0]:=p2[cx*3];
      Buf[3,1]:=p2[cx*3+1];
      Buf[3,2]:=p2[cx*3+2];
      p0[x*3]:=(Buf[0,0]+Buf[1,0]+Buf[2,0]+Buf[3,0])shr 2;
      p0[x*3+1]:=(Buf[0,1]+Buf[1,1]+Buf[2,1]+Buf[3,1])shr 2;
      p0[x*3+2]:=(Buf[0,2]+Buf[1,2]+Buf[2,2]+Buf[3,2])shr 2;
    end;
  end;
end;

procedure GaussianBlur(var clip: TDIB; Amount: integer);
var
  i: Integer;
begin
  for i:=Amount downto 0 do
    _SplitBlur(clip,3);
end;

procedure Mosaic(var Bm:TDIB;size:Integer);
var
  x,y,i,j:integer;
  p1,p2:pbytearray;
  r,g,b:byte;
begin
  y:=0;
  repeat
    p1:=bm.scanline[y];
    x:=0;
    repeat
      j:=1;
      repeat
        p2:=bm.scanline[y];
        x:=0;
        repeat
          r:=p1[x*3];
          g:=p1[x*3+1];
          b:=p1[x*3+2];
          i:=1;
          repeat
            p2[x*3]:=r;
            p2[x*3+1]:=g;
            p2[x*3+2]:=b;
            inc(x);
            inc(i);
          until (x>=bm.width) or (i>size);
        until x>=bm.width;
        inc(j);
        inc(y);
      until (y>=bm.height) or (j>size);
    until (y>=bm.height) or (x>=bm.width);
  until y>=bm.height;
end;


procedure _Twist(var Bmp, Dst: TDIB; Amount: integer);
var
  fxmid, fymid : Single;
  txmid, tymid : Single;
  fx,fy : Single;
  tx2, ty2 : Single;
  r : Single;
  theta : Single;
  ifx, ify : integer;
  dx, dy : Single;
  OFFSET : Single;
  ty, tx : Integer;
  weight_x, weight_y : array[0..1] of Single;
  weight : Single;
  new_red, new_green : Integer;
  new_blue : Integer;
  total_red, total_green : Single;
  total_blue : Single;
  ix, iy : Integer;
  sli, slo : PBytearray;

  function ArcTan2(xt,yt : Single): Single;
  begin
    if xt = 0 then
      if yt > 0 then
        Result := Pi/2
      else
        Result := -(Pi/2)
    else begin
      Result := ArcTan(yt/xt);
      if xt < 0 then
        Result := Pi + ArcTan(yt/xt);
    end;
  end;

begin
  OFFSET := -(Pi/2);
  dx := Bmp.Width - 1;
  dy := Bmp.Height - 1;
  r := Sqrt(dx * dx + dy * dy);
  tx2 := r;
  ty2 := r;
  txmid := (Bmp.Width-1)/2;    //Adjust these to move center of rotation
  tymid := (Bmp.Height-1)/2;   //Adjust these to move ......
  fxmid := (Bmp.Width-1)/2;
  fymid := (Bmp.Height-1)/2;
  if tx2 >= Bmp.Width then tx2 := Bmp.Width-1;
  if ty2 >= Bmp.Height then ty2 := Bmp.Height-1;

  for ty := 0 to Round(ty2) do begin
    for tx := 0 to Round(tx2) do begin
      dx := tx - txmid;
      dy := ty - tymid;
      r := Sqrt(dx * dx + dy * dy);
      if r = 0 then begin
        fx := 0;
        fy := 0;
      end
      else begin
        theta := ArcTan2(dx,dy) - r/Amount - OFFSET;
        fx := r * Cos(theta);
        fy := r * Sin(theta);
      end;
      fx := fx + fxmid;
      fy := fy + fymid;

      ify := Trunc(fy);
      ifx := Trunc(fx);
                // Calculate the weights.
      if fy >= 0  then begin
        weight_y[1] := fy - ify;
        weight_y[0] := 1 - weight_y[1];
      end else begin
        weight_y[0] := -(fy - ify);
        weight_y[1] := 1 - weight_y[0];
      end;
      if fx >= 0 then begin
        weight_x[1] := fx - ifx;
        weight_x[0] := 1 - weight_x[1];
      end else begin
        weight_x[0] := -(fx - ifx);
        Weight_x[1] := 1 - weight_x[0];
      end;

      if ifx < 0 then
        ifx := Bmp.Width-1-(-ifx mod Bmp.Width)
      else if ifx > Bmp.Width-1  then
        ifx := ifx mod Bmp.Width;
      if ify < 0 then
        ify := Bmp.Height-1-(-ify mod Bmp.Height)
      else if ify > Bmp.Height-1 then
        ify := ify mod Bmp.Height;

      total_red   := 0.0;
      total_green := 0.0;
      total_blue  := 0.0;
      for ix := 0 to 1 do begin
        for iy := 0 to 1 do begin
          if ify + iy < Bmp.Height then
            sli := Bmp.scanline[ify + iy]
          else
            sli := Bmp.scanline[Bmp.Height - ify - iy];
          if ifx + ix < Bmp.Width then begin
            new_red := sli[(ifx + ix)*3];
            new_green := sli[(ifx + ix)*3+1];
            new_blue := sli[(ifx + ix)*3+2];
          end
          else begin
            new_red := sli[(Bmp.Width - ifx - ix)*3];
            new_green := sli[(Bmp.Width - ifx - ix)*3+1];
            new_blue := sli[(Bmp.Width - ifx - ix)*3+2];
          end;
          weight := weight_x[ix] * weight_y[iy];
          total_red   := total_red   + new_red   * weight;
          total_green := total_green + new_green * weight;
          total_blue  := total_blue  + new_blue  * weight;
        end;
      end;
      slo := Dst.scanline[ty];
      slo[tx*3] := Round(total_red);
      slo[tx*3+1] := Round(total_green);
      slo[tx*3+2] := Round(total_blue);
    end;
  end;
end;

Procedure Splitlight (var clip:TDIB;amount:integer);
var 
  x,y,i:integer;
  p1:pbytearray;

  function sinpixs(a:integer):integer;
  begin
    result:=variant(sin(a/255*pi/2)*255);
  end;
begin
  for i:=1 to amount do
    for y:=0 to clip.height-1 do begin
      p1:=clip.scanline[y];
      for x:=0 to clip.width-1 do begin
        p1[x*3]:=sinpixs(p1[x*3]);
        p1[x*3+1]:=sinpixs(p1[x*3+1]);
        p1[x*3+2]:=sinpixs(p1[x*3+2]);
      end;
    end;
end;


procedure Tile(src, dst: TDIB; amount: integer);
var 
  w,h,w2,h2,i,j:integer;
  bm:TDIB;
begin
  w:=src.width;
  h:=src.height;
  dst.width:=w;
  dst.height:=h;
  dst.Canvas.draw(0,0,src);
  if (amount<=0) or ((w div amount)<5)or ((h div amount)<5) then exit;
  h2:=h div amount;
  w2:=w div amount;
  bm:=TDIB.create;
  bm.width:=w2;
  bm.height:=h2;
  bm.BitCount := 24;
  smoothresize(src,bm);
  for j:=0 to amount-1 do
    for i:=0 to amount-1 do
      dst.canvas.Draw (i*w2,j*h2,bm);
  bm.free;
end;

procedure SpotLight (var src: TDIB; Amount: integer; Spot: TRect);
var 
  bm:TDIB;
  w,h:integer;
begin
  Darkness(src,amount);
  w:=src.Width;
  h:=src.Height ;
  bm:=TDIB.create;
  bm.width:=w;
  bm.height:=h;
  bm.canvas.Brush.color:=clblack;
  bm.canvas.FillRect (rect(0,0,w,h));
  bm.canvas.brush.Color :=clwhite;
  bm.canvas.Ellipse (Spot.left,spot.top,spot.right,spot.bottom);
  bm.transparent:=true;
  src.Canvas.CopyMode := cmSrcAnd; {as transparentcolor for white}
  src.Canvas.Draw (0,0,bm);
  bm.free;
end;

procedure Trace (src:TDIB;intensity:integer);
var
  x,y,i : integer;
  P1,P2,P3,P4 : PByteArray;
  tb,TraceB:byte;
  hasb:boolean;
  bitmap:TDIB;
begin
  bitmap:=TDIB.create;
  bitmap.width:=src.width;
  bitmap.height:=src.height;
  bitmap.canvas.draw(0,0,src);
  bitmap.BitCount := 8;
  src.BitCount := 24;
  hasb:=false;
  TraceB:=$00;
  for i:=1 to Intensity do begin
    for y := 0 to BitMap.height -2 do begin
      P1 := BitMap.ScanLine[y];
      P2 := BitMap.scanline[y+1];
      P3 := src.scanline[y];
      P4 := src.scanline[y+1];
      x:=0;
      repeat
        if p1[x]<>p1[x+1] then begin
           if not hasb then begin
             tb:=p1[x+1];
             hasb:=true;
             p3[x*3]:=TraceB;
             p3[x*3+1]:=TraceB;
             p3[x*3+2]:=TraceB;
             end
             else begin
             if p1[x]<>tb then
                 begin
                 p3[x*3]:=TraceB;
                 p3[x*3+1]:=TraceB;
                 p3[x*3+2]:=TraceB;
                 end
               else
                 begin
                 p3[(x+1)*3]:=TraceB;
                 p3[(x+1)*3+1]:=TraceB;
                 p3[(x+1)*3+1]:=TraceB;
                 end;
             end;
           end;
        if p1[x]<>p2[x] then begin
           if not hasb then begin
             tb:=p2[x];
             hasb:=true;
             p3[x*3]:=TraceB;
             p3[x*3+1]:=TraceB;
             p3[x*3+2]:=TraceB;
             end
             else begin
             if p1[x]<>tb then
                 begin
                 p3[x*3]:=TraceB;
                 p3[x*3+1]:=TraceB;
                 p3[x*3+2]:=TraceB;
                 end
               else
                 begin
                 p4[x*3]:=TraceB;
                 p4[x*3+1]:=TraceB;
                 p4[x*3+2]:=TraceB;
                 end;
             end;
           end;
      inc(x);
      until x>=(BitMap.width -2);
    end;
    if i>1 then
    for y := BitMap.height -1 downto 1 do begin
      P1 := BitMap.ScanLine[y];
      P2 := BitMap.scanline[y-1];
      P3 := src.scanline[y];
      P4 := src.scanline [y-1];
      x:=Bitmap.width-1;
      repeat
        if p1[x]<>p1[x-1] then begin
           if not hasb then begin
             tb:=p1[x-1];
             hasb:=true;
             p3[x*3]:=TraceB;
             p3[x*3+1]:=TraceB;
             p3[x*3+2]:=TraceB;
             end
             else begin
             if p1[x]<>tb then
                 begin
                 p3[x*3]:=TraceB;
                 p3[x*3+1]:=TraceB;
                 p3[x*3+2]:=TraceB;
                 end
               else
                 begin
                 p3[(x-1)*3]:=TraceB;
                 p3[(x-1)*3+1]:=TraceB;
                 p3[(x-1)*3+2]:=TraceB;
                 end;
             end;
           end;
        if p1[x]<>p2[x] then begin
           if not hasb then begin
             tb:=p2[x];
             hasb:=true;
             p3[x*3]:=TraceB;
             p3[x*3+1]:=TraceB;
             p3[x*3+2]:=TraceB;
             end
             else begin
             if p1[x]<>tb then
                 begin
                 p3[x*3]:=TraceB;
                 p3[x*3+1]:=TraceB;
                 p3[x*3+2]:=TraceB;
                 end
               else
                 begin
                 p4[x*3]:=TraceB;
                 p4[x*3+1]:=TraceB;
                 p4[x*3+2]:=TraceB;
                 end;
             end;
           end;
      dec(x);
      until x<=1;
    end;
  end;
  bitmap.free;
end;


procedure _Emboss(var Bmp:TDIB);
var
  x,y:   Integer;
  p1,p2: Pbytearray;
begin
  for y:=0 to Bmp.Height-2 do begin
    p1:=bmp.scanline[y];
    p2:=bmp.scanline[y+1];
    for x:=0 to Bmp.Width-4 do begin
      p1[x*3]:=(p1[x*3]+(p2[(x+3)*3] xor $FF))shr 1;
      p1[x*3+1]:=(p1[x*3+1]+(p2[(x+3)*3+1] xor $FF))shr 1;
      p1[x*3+2]:=(p1[x*3+2]+(p2[(x+3)*3+2] xor $FF))shr 1;
    end;
  end;
end;

procedure Solorize(src, dst: TDIB; amount: integer);
var 
  w,h,x,y:integer;
  ps,pd:pbytearray;
  c:integer;
begin
  w:=src.width;
  h:=src.height;
  src.BitCount := 24;
  dst.BitCount := 24;
  for y:=0 to h-1 do begin
    ps:=src.scanline[y];
    pd:=dst.scanline[y];
    for x:=0 to w-1 do begin
      c:=(ps[x*3]+ps[x*3+1]+ps[x*3+2]) div 3;
      if c>amount then begin
        pd[x*3]:= 255-ps[x*3];
        pd[x*3+1]:=255-ps[x*3+1];
        pd[x*3+2]:=255-ps[x*3+2];
      end
      else begin
        pd[x*3]:=ps[x*3];
        pd[x*3+1]:=ps[x*3+1];
        pd[x*3+2]:=ps[x*3+2];
      end;
    end;
  end;
end;

procedure Posterize(src, dst: TDIB; amount: integer);
var 
  w,h,x,y:integer;
  ps,pd:pbytearray;
begin
  w:=src.width;
  h:=src.height;
  src.BitCount := 24;
  dst.BitCount := 24;
  for y:=0 to h-1 do begin
    ps:=src.scanline[y];
    pd:=dst.scanline[y];
    for x:=0 to w-1 do begin
      pd[x*3]:= round(ps[x*3]/amount)*amount;
      pd[x*3+1]:=round(ps[x*3+1]/amount)*amount;
      pd[x*3+2]:=round(ps[x*3+2]/amount)*amount;
    end;
  end;
end;

procedure Brightness(src, dst: TDIB; level: Integer);
const
  MaxPixelCount   =  32768;
type
  pRGBArray  =  ^TRGBArray;
  TRGBArray  =  ARRAY[0..MaxPixelCount-1] OF TRGBTriple;
var
  i, j, value: integer;
  OrigRow, DestRow: pRGBArray;
begin
  // get brightness increment value
  value := level;
  src.BitCount := 24;
  dst.BitCount := 24;
  // for each row of pixels
  for i := 0 to src.Height - 1 do begin
    OrigRow := src.ScanLine[i];
    DestRow := dst.ScanLine[i];
    // for each pixel in row
    for j := 0 to src.Width - 1 do begin
      // add brightness value to pixel's RGB values
      if value > 0 then begin
        // RGB values must be less than 256
        DestRow[j].rgbtRed := Min(255, OrigRow[j].rgbtRed + value);
        DestRow[j].rgbtGreen := Min(255, OrigRow[j].rgbtGreen + value);
        DestRow[j].rgbtBlue := Min(255, OrigRow[j].rgbtBlue + value);
      end else begin
        // RGB values must be greater or equal than 0
        DestRow[j].rgbtRed := Max(0, OrigRow[j].rgbtRed + value);
        DestRow[j].rgbtGreen := Max(0, OrigRow[j].rgbtGreen + value);
        DestRow[j].rgbtBlue := Max(0, OrigRow[j].rgbtBlue + value);
      end;
    end;
  end;
end;

//----------------------
//-------------------------
//----------------------

procedure TDIBEffect.Effect_Invert;
Begin
  PicInvert (Self);
end;

Procedure TDIBEffect.Effect_AddColorNoise (Amount:Integer);
Var BB:TDIB;
Begin
  BB := TDIB.Create;
  BB.BitCount := 24;
  BB.Assign (Self);
  AddColorNoise (bb,Amount);
  Self.Assign (BB);
  BB.Free;
end;

Procedure TDIBEffect.Effect_AddMonoNoise (Amount:Integer);
Var BB:TDIB;
Begin
  BB := TDIB.Create;
  BB.BitCount := 24;
  BB.Assign (Self);
  _AddMonoNoise (bb,Amount);
  Self.Assign (BB);
  BB.Free;
end;

procedure TDIBEffect.Effect_AntiAlias;
Begin
  AntiAlias (Self);
end;

Procedure TDIBEffect.Effect_Contrast (Amount:Integer);
Var BB:TDIB;
Begin
  BB := TDIB.Create;
  BB.BitCount := 24;
  BB.Assign (Self);
  _Contrast (bb,Amount);
  Self.Assign (BB);
  BB.Free;
end;

Procedure TDIBEffect.Effect_FishEye (Amount:Integer);
Var BB1,BB2:TDIB;
Begin
  BB1 := TDIB.Create;
  BB1.BitCount := 24;
  BB1.Assign (Self);
  BB2 := TDIB.Create;
  BB2.BitCount := 24;
  BB2.Assign (BB1);
  _FishEye (BB1,BB2,Amount);
  Self.Assign (BB2);
  BB1.Free;
  BB2.Free;
end;

Procedure TDIBEffect.Effect_GrayScale;
Var BB:TDIB;
Begin
  BB := TDIB.Create;
  BB.BitCount := 24;
  BB.Assign (Self);
  GrayScale (BB);
  Self.Assign (BB);
  BB.Free;
end;

Procedure TDIBEffect.Effect_Lightness (Amount:Integer);
Var BB:TDIB;
Begin
  BB := TDIB.Create;
  BB.BitCount := 24;
  BB.Assign (Self);
  _Lightness (BB,Amount);
  Self.Assign (BB);
  BB.Free;
end;

Procedure TDIBEffect.Effect_Darkness (Amount:Integer);
Var BB:TDIB;
Begin
  BB := TDIB.Create;
  BB.BitCount := 24;
  BB.Assign (Self);
  Darkness (BB,Amount);
  Self.Assign (BB);
  BB.Free;
end;

Procedure TDIBEffect.Effect_Saturation (Amount:Integer);
Var BB:TDIB;
Begin
  BB := TDIB.Create;
  BB.BitCount := 24;
  BB.Assign (Self);
  _Saturation (BB,Amount);
  Self.Assign (BB);
  BB.Free;
end;

Procedure TDIBEffect.Effect_SplitBlur (Amount:Integer);
Var BB:TDIB;
Begin
  BB := TDIB.Create;
  BB.BitCount := 24;
  BB.Assign (Self);
  _SplitBlur (BB,Amount);
  Self.Assign (BB);
  BB.Free;
end;

Procedure TDIBEffect.Effect_GaussianBlur (Amount:Integer);
Var BB:TDIB;
Begin
  BB := TDIB.Create;
  BB.BitCount := 24;
  BB.BitCount := 24;
  BB.Assign (Self);
  GaussianBlur (BB,Amount);
  Self.Assign (BB);
  BB.Free;
end;

Procedure TDIBEffect.Effect_Mosaic (Size:Integer);
Var BB:TDIB;
Begin
  BB := TDIB.Create;
  BB.BitCount := 24;
  BB.Assign (Self);
  Mosaic (BB,Size);
  Self.Assign (BB);
  BB.Free;
end;

Procedure TDIBEffect.Effect_Twist (Amount:Integer);
Var BB1,BB2:TDIB;
Begin
  BB1 := TDIB.Create;
  BB1.BitCount := 24;
  BB1.Assign (Self);
  BB2 := TDIB.Create;
  BB2.BitCount := 24;
  BB2.Assign (BB1);
  _Twist (BB1,BB2,Amount);
  Self.Assign (BB2);
  BB1.Free;
  BB2.Free;
end;

Procedure TDIBEffect.Effect_Trace (Amount: integer);
Var BB1,BB2:TDIB;
Begin
  BB1 := TDIB.Create;
  BB1.BitCount := 24;
  BB1.Assign (Self);
  BB2 := TDIB.Create;
  BB2.BitCount := 24;
  BB2.Assign (BB1);
  Trace (BB2,Amount);
  Self.Assign (BB2);
  BB1.Free;
  BB2.Free;
end;

procedure TDIBEffect.Effect_Splitlight (Amount:integer);
Var BB1{,BB2}:TDIB;
Begin
  BB1 := TDIB.Create;
  BB1.BitCount := 24;
  BB1.Assign (Self);
//  BB2 := TDIB.Create;
//  BB2.BitCount := 24;
//  BB2.Assign (BB1);
  Splitlight (BB1,Amount);
  Self.Assign (BB1);
  BB1.Free;
//  BB2.Free;
end;

Procedure TDIBEffect.Effect_Tile (Amount: integer);
Var BB1,BB2:TDIB;
Begin
  BB1 := TDIB.Create;
  BB1.BitCount := 24;
  BB1.Assign (Self);
  BB2 := TDIB.Create;
  BB2.BitCount := 24;
  BB2.Assign (BB1);
  Tile (BB1,BB2,Amount);
  Self.Assign (BB2);
  BB1.Free;
  BB2.Free;
end;

Procedure TDIBEffect.Effect_SpotLight (Amount: integer; Spot: TRect);
Var BB1,BB2:TDIB;
Begin
  BB1 := TDIB.Create;
  BB1.BitCount := 24;
  BB1.Assign (Self);
  BB2 := TDIB.Create;
  BB2.BitCount := 24;
  BB2.Assign (BB1);
  SpotLight (BB2,Amount,Spot);
  Self.Assign (BB2);
  BB1.Free;
  BB2.Free;
end;

Procedure TDIBEffect.Effect_Emboss;
Var BB1,BB2:TDIB;
Begin
  BB1 := TDIB.Create;
  BB1.BitCount := 24;
  BB1.Assign (Self);
  BB2 := TDIB.Create;
  BB2.BitCount := 24;
  BB2.Assign (BB1);
  _Emboss (BB2);
  Self.Assign (BB2);
  BB1.Free;
  BB2.Free;
end;

Procedure TDIBEffect.Effect_Solorize (Amount: integer);
Var BB1,BB2:TDIB;
Begin
  BB1 := TDIB.Create;
  BB1.BitCount := 24;
  BB1.Assign (Self);
  BB2 := TDIB.Create;
  BB2.BitCount := 24;
  BB2.Assign (BB1);
  Solorize (BB1,BB2,Amount);
  Self.Assign (BB2);
  BB1.Free;
  BB2.Free;
end;


Procedure TDIBEffect.Effect_Posterize (Amount: integer);
Var BB1,BB2:TDIB;
Begin
  BB1 := TDIB.Create;
  BB1.BitCount := 24;
  BB1.Assign (Self);
  BB2 := TDIB.Create;
  BB2.BitCount := 24;
  BB2.Assign (BB1);
  Posterize (BB1,BB2,Amount);
  Self.Assign (BB2);
  BB1.Free;
  BB2.Free;
end;

Procedure TDIBEffect.Effect_Brightness (Amount: integer);
Var BB1,BB2:TDIB;
Begin
  BB1 := TDIB.Create;
  BB1.BitCount := 24;
  BB1.Assign (Self);
  BB2 := TDIB.Create;
  BB2.BitCount := 24;
  BB2.Assign (BB1);
  Brightness(BB1, BB2, Amount);
  Self.Assign (BB2);
  BB1.Free;
  BB2.Free;
end;

{$DEFINE USE_SCANLINE}

procedure Resample(Src, Dst: TDIB; filtertype: TFilterTypeResample; fwidth: single);
// -----------------------------------------------------------------------------
//
//			Filter functions
//
// -----------------------------------------------------------------------------

// Hermite filter
function HermiteFilter(Value: Single): Single;
begin
  // f(t) = 2|t|^3 - 3|t|^2 + 1, -1 <= t <= 1
  if (Value < 0.0) then
    Value := -Value;
  if (Value < 1.0) then
    Result := (2.0 * Value - 3.0) * Sqr(Value) + 1.0
  else
    Result := 0.0;
end;

// Box filter
// a.k.a. "Nearest Neighbour" filter
// anme: I have not been able to get acceptable
//       results with this filter for subsampling.
function BoxFilter(Value: Single): Single;
begin
  if (Value > -0.5) and (Value <= 0.5) then
    Result := 1.0
  else
    Result := 0.0;
end;

// Triangle filter
// a.k.a. "Linear" or "Bilinear" filter
function TriangleFilter(Value: Single): Single;
begin
  if (Value < 0.0) then
    Value := -Value;
  if (Value < 1.0) then
    Result := 1.0 - Value
  else
    Result := 0.0;
end;

// Bell filter
function BellFilter(Value: Single): Single;
begin
  if (Value < 0.0) then
    Value := -Value;
  if (Value < 0.5) then
    Result := 0.75 - Sqr(Value)
  else if (Value < 1.5) then
  begin
    Value := Value - 1.5;
    Result := 0.5 * Sqr(Value);
  end else
    Result := 0.0;
end;

// B-spline filter
function SplineFilter(Value: Single): Single;
var
  tt			: single;
begin
  if (Value < 0.0) then
    Value := -Value;
  if (Value < 1.0) then
  begin
    tt := Sqr(Value);
    Result := 0.5*tt*Value - tt + 2.0 / 3.0;
  end else if (Value < 2.0) then
  begin
    Value := 2.0 - Value;
    Result := 1.0/6.0 * Sqr(Value) * Value;
  end else
    Result := 0.0;
end;

// Lanczos3 filter
function Lanczos3Filter(Value: Single): Single;
  function SinC(Value: Single): Single;
  begin
    if (Value <> 0.0) then
    begin
      Value := Value * Pi;
      Result := sin(Value) / Value
    end else
      Result := 1.0;
  end;
begin
  if (Value < 0.0) then
    Value := -Value;
  if (Value < 3.0) then
    Result := SinC(Value) * SinC(Value / 3.0)
  else
    Result := 0.0;
end;

function MitchellFilter(Value: Single): Single;
const
  B		= (1.0 / 3.0);
  C		= (1.0 / 3.0);
var
  tt			: single;
begin
  if (Value < 0.0) then
    Value := -Value;
  tt := Sqr(Value);
  if (Value < 1.0) then
  begin
    Value := (((12.0 - 9.0 * B - 6.0 * C) * (Value * tt))
      + ((-18.0 + 12.0 * B + 6.0 * C) * tt)
      + (6.0 - 2 * B));
    Result := Value / 6.0;
  end else
  if (Value < 2.0) then
  begin
    Value := (((-1.0 * B - 6.0 * C) * (Value * tt))
      + ((6.0 * B + 30.0 * C) * tt)
      + ((-12.0 * B - 48.0 * C) * Value)
      + (8.0 * B + 24 * C));
    Result := Value / 6.0;
  end else
    Result := 0.0;
end;

// -----------------------------------------------------------------------------
//
//			Interpolator
//
// -----------------------------------------------------------------------------
type
  // Contributor for a pixel
  TContributor = record
    pixel: integer;		// Source pixel
    weight: single;		// Pixel weight
  end;

  TContributorList = array[0..0] of TContributor;
  PContributorList = ^TContributorList;

  // List of source pixels contributing to a destination pixel
  TCList = record
    n		: integer;
    p		: PContributorList;
  end;

  TCListList = array[0..0] of TCList;
  PCListList = ^TCListList;

  TRGB = packed record
    r, g, b	: single;
  end;

  // Physical bitmap pixel
  TColorRGB = packed record
    r, g, b	: BYTE;
  end;
  PColorRGB = ^TColorRGB;

  // Physical bitmap scanline (row)
  TRGBList = packed array[0..0] of TColorRGB;
  PRGBList = ^TRGBList;



var
  xscale, yscale	: single;		// Zoom scale factors
  i, j, k		: integer;		// Loop variables
  center		: single;		// Filter calculation variables
  width, fscale, weight	: single;		// Filter calculation variables
  left, right		: integer;		// Filter calculation variables
  n			: integer;		// Pixel number
  Work			: TDIB;
  contrib		: PCListList;
  rgb			: TRGB;
  color			: TColorRGB;
{$IFDEF USE_SCANLINE}
  SourceLine		,
  DestLine		: PRGBList;
  SourcePixel		,
  DestPixel		: PColorRGB;
  Delta			,
  DestDelta		: integer;
{$ENDIF}
  SrcWidth		,
  SrcHeight		,
  DstWidth		,
  DstHeight		: integer;

  function Color2RGB(Color: TColor): TColorRGB;
  begin
    Result.r := Color AND $000000FF;
    Result.g := (Color AND $0000FF00) SHR 8;
    Result.b := (Color AND $00FF0000) SHR 16;
  end;

  function RGB2Color(Color: TColorRGB): TColor;
  begin
    Result := Color.r OR (Color.g SHL 8) OR (Color.b SHL 16);
  end;

begin
  DstWidth := Dst.Width;
  DstHeight := Dst.Height;
  SrcWidth := Src.Width;
  SrcHeight := Src.Height;
  if (SrcWidth < 1) or (SrcHeight < 1) then
    raise Exception.Create('Source bitmap too small');

  // Create intermediate image to hold horizontal zoom
  Work := TDIB.Create;
  try
    Work.Height := SrcHeight;
    Work.Width := DstWidth;
    // xscale := DstWidth / SrcWidth;
    // yscale := DstHeight / SrcHeight;
    // Improvement suggested by David Ullrich:
    if (SrcWidth = 1) then
      xscale:= DstWidth / SrcWidth
    else
      xscale:= (DstWidth - 1) / (SrcWidth - 1);
    if (SrcHeight = 1) then
      yscale:= DstHeight / SrcHeight
    else
      yscale:= (DstHeight - 1) / (SrcHeight - 1);
    // This implementation only works on 24-bit images because it uses
    // TDIB.Scanline
{$IFDEF USE_SCANLINE}
    //Src.PixelFormat := pf24bit;
    Src.BitCount := 24;
    //Dst.PixelFormat := Src.PixelFormat;
    dst.BitCount := 24;
    //Work.PixelFormat := Src.PixelFormat;
    work.BitCount := 24;
{$ENDIF}

    // --------------------------------------------
    // Pre-calculate filter contributions for a row
    // -----------------------------------------------
    GetMem(contrib, DstWidth* sizeof(TCList));
    // Horizontal sub-sampling
    // Scales from bigger to smaller width
    if (xscale < 1.0) then
    begin
      width := fwidth / xscale;
      fscale := 1.0 / xscale;
      for i := 0 to DstWidth-1 do
      begin
        contrib^[i].n := 0;
        GetMem(contrib^[i].p, trunc(width * 2.0 + 1) * sizeof(TContributor));
        center := i / xscale;
        // Original code:
        // left := ceil(center - width);
        // right := floor(center + width);
        left := floor(center - width);
        right := ceil(center + width);
        for j := left to right do
        begin
          Case filtertype of
            ftrBox       : weight := boxfilter((center - j) / fscale) / fscale;
            ftrTriangle  : weight := trianglefilter((center - j) / fscale) / fscale;
            ftrHermite   : weight := hermitefilter((center - j) / fscale) / fscale;
            ftrBell      : weight := bellfilter((center - j) / fscale) / fscale;
            ftrBSpline   : weight := splinefilter((center - j) / fscale) / fscale;
            ftrLanczos3  : weight := Lanczos3filter((center - j) / fscale) / fscale;
            ftrMitchell  : weight := Mitchellfilter((center - j) / fscale) / fscale;
          end;
          if (weight = 0.0) then
            continue;
          if (j < 0) then
            n := -j
          else if (j >= SrcWidth) then
            n := SrcWidth - j + SrcWidth - 1
          else
            n := j;
          k := contrib^[i].n;
          contrib^[i].n := contrib^[i].n + 1;
          contrib^[i].p^[k].pixel := n;
          contrib^[i].p^[k].weight := weight;
        end;
      end;
    end else
    // Horizontal super-sampling
    // Scales from smaller to bigger width
    begin
      for i := 0 to DstWidth-1 do
      begin
        contrib^[i].n := 0;
        GetMem(contrib^[i].p, trunc(fwidth * 2.0 + 1) * sizeof(TContributor));
        center := i / xscale;
        // Original code:
        // left := ceil(center - fwidth);
        // right := floor(center + fwidth);
        left := floor(center - fwidth);
        right := ceil(center + fwidth);
        for j := left to right do
        begin
          Case filtertype of
            ftrBox       : weight := boxfilter(center - j);
            ftrTriangle  : weight := trianglefilter(center - j);
            ftrHermite   : weight := hermitefilter(center - j);
            ftrBell      : weight := bellfilter(center - j);
            ftrBSpline   : weight := splinefilter(center - j);
            ftrLanczos3  : weight := Lanczos3filter(center - j);
            ftrMitchell  : weight := Mitchellfilter(center - j);
          end;
          if (weight = 0.0) then
            continue;
          if (j < 0) then
            n := -j
          else if (j >= SrcWidth) then
            n := SrcWidth - j + SrcWidth - 1
          else
            n := j;
          k := contrib^[i].n;
          contrib^[i].n := contrib^[i].n + 1;
          contrib^[i].p^[k].pixel := n;
          contrib^[i].p^[k].weight := weight;
        end;
      end;
    end;

    // ----------------------------------------------------
    // Apply filter to sample horizontally from Src to Work
    // ----------------------------------------------------
    for k := 0 to SrcHeight-1 do
    begin
{$IFDEF USE_SCANLINE}
      SourceLine := Src.ScanLine[k];
      DestPixel := Work.ScanLine[k];
{$ENDIF}
      for i := 0 to DstWidth-1 do
      begin
        rgb.r := 0.0;
        rgb.g := 0.0;
        rgb.b := 0.0;
        for j := 0 to contrib^[i].n-1 do
        begin
{$IFDEF USE_SCANLINE}
          color := SourceLine^[contrib^[i].p^[j].pixel];
{$ELSE}
          color := Color2RGB(Src.Canvas.Pixels[contrib^[i].p^[j].pixel, k]);
{$ENDIF}
          weight := contrib^[i].p^[j].weight;
          if (weight = 0.0) then
            continue;
          rgb.r := rgb.r + color.r * weight;
          rgb.g := rgb.g + color.g * weight;
          rgb.b := rgb.b + color.b * weight;
        end;
        if (rgb.r > 255.0) then
          color.r := 255
        else if (rgb.r < 0.0) then
          color.r := 0
        else
          color.r := round(rgb.r);
        if (rgb.g > 255.0) then
          color.g := 255
        else if (rgb.g < 0.0) then
          color.g := 0
        else
          color.g := round(rgb.g);
        if (rgb.b > 255.0) then
          color.b := 255
        else if (rgb.b < 0.0) then
          color.b := 0
        else
          color.b := round(rgb.b);
{$IFDEF USE_SCANLINE}
        // Set new pixel value
        DestPixel^ := color;
        // Move on to next column
        inc(DestPixel);
{$ELSE}
        Work.Canvas.Pixels[i, k] := RGB2Color(color);
{$ENDIF}
      end;
    end;

    // Free the memory allocated for horizontal filter weights
    for i := 0 to DstWidth-1 do
      FreeMem(contrib^[i].p);

    FreeMem(contrib);

    // -----------------------------------------------
    // Pre-calculate filter contributions for a column
    // -----------------------------------------------
    GetMem(contrib, DstHeight* sizeof(TCList));
    // Vertical sub-sampling
    // Scales from bigger to smaller height
    if (yscale < 1.0) then
    begin
      width := fwidth / yscale;
      fscale := 1.0 / yscale;
      for i := 0 to DstHeight-1 do
      begin
        contrib^[i].n := 0;
        GetMem(contrib^[i].p, trunc(width * 2.0 + 1) * sizeof(TContributor));
        center := i / yscale;
        // Original code:
        // left := ceil(center - width);
        // right := floor(center + width);
        left := floor(center - width);
        right := ceil(center + width);
        for j := left to right do
        begin
          Case filtertype of
            ftrBox       : weight := boxfilter((center - j) / fscale) / fscale;
            ftrTriangle  : weight := trianglefilter((center - j) / fscale) / fscale;
            ftrHermite   : weight := hermitefilter((center - j) / fscale) / fscale;
            ftrBell      : weight := bellfilter((center - j) / fscale) / fscale;
            ftrBSpline   : weight := splinefilter((center - j) / fscale) / fscale;
            ftrLanczos3  : weight := Lanczos3filter((center - j) / fscale) / fscale;
            ftrMitchell  : weight := Mitchellfilter((center - j) / fscale) / fscale;
          end;
          if (weight = 0.0) then
            continue;
          if (j < 0) then
            n := -j
          else if (j >= SrcHeight) then
            n := SrcHeight - j + SrcHeight - 1
          else
            n := j;
          k := contrib^[i].n;
          contrib^[i].n := contrib^[i].n + 1;
          contrib^[i].p^[k].pixel := n;
          contrib^[i].p^[k].weight := weight;
        end;
      end
    end else
    // Vertical super-sampling
    // Scales from smaller to bigger height
    begin
      for i := 0 to DstHeight-1 do
      begin
        contrib^[i].n := 0;
        GetMem(contrib^[i].p, trunc(fwidth * 2.0 + 1) * sizeof(TContributor));
        center := i / yscale;
        // Original code:
        // left := ceil(center - fwidth);
        // right := floor(center + fwidth);
        left := floor(center - fwidth);
        right := ceil(center + fwidth);
        for j := left to right do
        begin
          Case filtertype of
            ftrBox       : weight := boxfilter(center - j);
            ftrTriangle  : weight := trianglefilter(center - j);
            ftrHermite   : weight := hermitefilter(center - j);
            ftrBell      : weight := bellfilter(center - j);
            ftrBSpline   : weight := splinefilter(center - j);
            ftrLanczos3  : weight := Lanczos3filter(center - j);
            ftrMitchell  : weight := Mitchellfilter(center - j);
          end;
          if (weight = 0.0) then
            continue;
          if (j < 0) then
            n := -j
          else if (j >= SrcHeight) then
            n := SrcHeight - j + SrcHeight - 1
          else
            n := j;
          k := contrib^[i].n;
          contrib^[i].n := contrib^[i].n + 1;
          contrib^[i].p^[k].pixel := n;
          contrib^[i].p^[k].weight := weight;
        end;
      end;
    end;

    // --------------------------------------------------
    // Apply filter to sample vertically from Work to Dst
    // --------------------------------------------------
{$IFDEF USE_SCANLINE}
    SourceLine := Work.ScanLine[0];
    Delta := integer(Work.ScanLine[1]) - integer(SourceLine);
    DestLine := Dst.ScanLine[0];
    DestDelta := integer(Dst.ScanLine[1]) - integer(DestLine);
{$ENDIF}
    for k := 0 to DstWidth-1 do
    begin
{$IFDEF USE_SCANLINE}
      DestPixel := pointer(DestLine);
{$ENDIF}
      for i := 0 to DstHeight-1 do
      begin
        rgb.r := 0;
        rgb.g := 0;
        rgb.b := 0;
        // weight := 0.0;
        for j := 0 to contrib^[i].n-1 do
        begin
{$IFDEF USE_SCANLINE}
          color := PColorRGB(integer(SourceLine)+contrib^[i].p^[j].pixel*Delta)^;
{$ELSE}
          color := Color2RGB(Work.Canvas.Pixels[k, contrib^[i].p^[j].pixel]);
{$ENDIF}
          weight := contrib^[i].p^[j].weight;
          if (weight = 0.0) then
            continue;
          rgb.r := rgb.r + color.r * weight;
          rgb.g := rgb.g + color.g * weight;
          rgb.b := rgb.b + color.b * weight;
        end;
        if (rgb.r > 255.0) then
          color.r := 255
        else if (rgb.r < 0.0) then
          color.r := 0
        else
          color.r := round(rgb.r);
        if (rgb.g > 255.0) then
          color.g := 255
        else if (rgb.g < 0.0) then
          color.g := 0
        else
          color.g := round(rgb.g);
        if (rgb.b > 255.0) then
          color.b := 255
        else if (rgb.b < 0.0) then
          color.b := 0
        else
          color.b := round(rgb.b);
{$IFDEF USE_SCANLINE}
        DestPixel^ := color;
        inc(integer(DestPixel), DestDelta);
{$ELSE}
        Dst.Canvas.Pixels[k, i] := RGB2Color(color);
{$ENDIF}
      end;
{$IFDEF USE_SCANLINE}
      Inc(SourceLine, 1);
      Inc(DestLine, 1);
{$ENDIF}
    end;

    // Free the memory allocated for vertical filter weights
    for i := 0 to DstHeight-1 do
      FreeMem(contrib^[i].p);

    FreeMem(contrib);

  finally
    Work.Free;
  end;
end;

Procedure TDIBEffect.Effect_Resample (AmountX,AmountY: integer; TypeResample: TFilterTypeResample);
Var BB1,BB2:TDIB;
Begin
  BB1 := TDIB.Create;
  BB1.BitCount := 24;
  BB1.Assign (Self);
  BB2 := TDIB.Create;
  BB2.SetSize(AmountX,AmountY,24);
  //BB2.Assign (BB1);
  Resample(BB1,BB2,TypeResample,DefaultFilterRadius[TypeResample]);
  Self.Assign (BB2);
  BB1.Free;
  BB2.Free;
end;

procedure Colorize(src, dst: TDIB; iForeColor, iBackColor: TColor);
{for monochromatic picture change colors}
  procedure InvertBitmap(Bmp: TDIB);
  begin
    Bmp.Canvas.CopyMode := cmDstInvert;
    Bmp.Canvas.CopyRect(rect(0, 0, Bmp.Width, Bmp.Height),
                       Bmp.Canvas, rect(0, 0, Bmp.Width, Bmp.Height));
  end;
Var
  fColor:      TColor;
  fForeColor:  TColor;
  fForeDither: Boolean;
  fSkipCreate: Boolean;
  fBmpMade:    Boolean;
  lTempBitmap:   TDIB;
  lTempBitmap2:  TDIB;
  lDitherBitmap: TDIB;
  lCRect:        TRect;
  x, y, w, h:    Integer;
begin
  {--}
  fColor := iBackColor;;
  fForeColor := iForeColor;

  w := src.Width;
  h := src.Height;
  lTempBitmap := TDIB.Create;
  lTempBitmap.SetSize(w, h, 24);
  lTempBitmap2 := TDIB.Create;
  lTempBitmap2.SetSize(w, h, 24);
  lCRect := rect(0, 0, w, h);
  with lTempBitmap.Canvas do begin
     Brush.Style := bsSolid;
     Brush.Color := iBackColor;
     FillRect(lCRect);
     CopyMode := cmSrcInvert;
     CopyRect(lCRect, src.Canvas, lCRect);
     InvertBitmap(src);
     CopyMode := cmSrcPaint;
     CopyRect(lCRect, src.Canvas, lCRect);
     InvertBitmap(lTempBitmap);
     CopyMode := cmSrcInvert;
     CopyRect(lCRect, src.Canvas, lCRect);
     InvertBitmap(src);
  end;
  with lTempBitmap2.Canvas do begin
     Brush.Style := bsSolid;
     Brush.Color := clBlack;
     FillRect(lCRect);
     if fForeDither then begin
        InvertBitmap(src);
        lDitherBitmap := TDIB.Create;
        lDitherBitmap.SetSize(8, 8, 24);
        with lDitherBitmap.Canvas do begin
           for x := 0 to 7 do
              for y := 0 to 7 do
                 if ((x mod 2 = 0) and (y mod 2 > 0)) or
                    ((x mod 2 > 0) and (y mod 2 = 0)) then
                       pixels[x,y] := fForeColor
                 else
                       pixels[x,y] := iBackColor;
        end;
        Brush.Bitmap.Assign(lDitherBitmap);
     end
     else begin
        Brush.Style := bsSolid;
        Brush.Color := fForeColor;
     end;
     if not fForeDither then
       InvertBitmap(src);
     CopyMode := cmPatPaint;
     CopyRect(lCRect, src.Canvas, lCRect);
     if fForeDither then lDitherBitmap.Free;
     CopyMode := cmSrcInvert;
     CopyRect(lCRect, src.Canvas, lCRect);
  end;
  lTempBitmap.Canvas.CopyMode := cmSrcInvert;
  lTempBitmap.Canvas.Copyrect(lCRect, lTempBitmap2.Canvas, lCRect);
  InvertBitmap(src);
  lTempBitmap.Canvas.CopyMode := cmSrcErase;
  lTempBitmap.Canvas.Copyrect(lCRect, src.Canvas, lCRect);
  InvertBitmap(src);
  lTempBitmap.Canvas.CopyMode := cmSrcInvert;
  lTempBitmap.Canvas.Copyrect(lCRect, lTempBitmap2.Canvas, lCRect);
  InvertBitmap(lTempBitmap);
  InvertBitmap(src);
  dst.Assign(lTempBitmap);
  lTempBitmap.Free;
  fBmpMade := True;
End;

Procedure TDIBEffect.Effect_Colorize (ForeColor, BackColor:TColor);
Var BB1,BB2:TDIB;
Begin
  BB1 := TDIB.Create;
  BB1.BitCount := 24;
  BB1.Assign (Self);
  BB2 := TDIB.Create;
  Colorize(BB1,BB2,ForeColor, BackColor);
  Self.Assign (BB2);
  BB1.Free;
  BB2.Free;
end;

end.
