// --- c o n F u s i o n ---
// By Joakim Back, www.back.mine.nu
// Huge thanks to Ilkka Tuomioja for helping out with the project.

unit DXFusion; //v 0.95 B

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, DIB, DXClass, DXDraws;

type
  TFilterMode = (fmNormal, fmMix50, fmMix25,fmMix75);

  TLightSource = record
    X, Y: Integer;
    Size1, Size2: Integer;
    Color: TColor;
  end;

  TLightArray = array of TLightsource;

  TMatrixSetting = array [0..9] of Integer;

const

  // 3x3 Matrix Presets.

  msEmboss     : TMatrixSetting=( -1, -1,  0, -1,  6,  1,  0,  1,  1,  6);
  msHardEmboss : TMatrixSetting=( -4, -2, -1, -2, 10,  2, -1,  2,  4,  8);
  msBlur       : TMatrixSetting=(  1,  2,  1,  2,  4,  2 , 1,  2,  1, 16);
  msSharpen    : TMatrixSetting=( -1, -1, -1, -1, 15, -1, -1, -1, -1,  7);
  msEdgeDetect : TMatrixSetting=( -1, -1, -1, -1,  8, -1, -1, -1, -1,  1);

function PosValue(Value: Integer): Integer;

// Basic.

procedure CreateDIBFromBitmap(var DestDIB:TDIB;Bitmap:TBitmap);

// Drawing Methods.

procedure Draw(DestDIB, SrcDIB: TDIB; X, Y, Width, Height,
  SourceX, SourceY: Integer);

procedure DrawTransparent(DestDIB, SrcDIB: TDIB; const X, Y, Width, Height,
  SourceX, SourceY: Integer; const Color: TColor);

procedure DrawShadow(DestDIB, SrcDIB: TDIB; X, Y, Width, Height,
  Frame: Integer;FilterMode:TFilterMode);

procedure DrawDarken(DestDIB, SrcDIB: TDIB; X, Y, Width, Height,
  Frame: Integer);

procedure DrawAdditive(DestDIB, SrcDIB: TDIB; X, Y, Width, Height, Alpha,
  Frame: Integer);

procedure DrawQuickAlpha(DestDIB, SrcDIB: TDIB; const X, Y, Width, Height,
  SourceX, SourceY: Integer; const Color: TColor;FilterMode:TFilterMode);

procedure DrawTranslucent(DestDIB, SrcDIB: TDIB; const X, Y, Width, Height,
  SourceX, SourceY: Integer; const Color: TColor);

procedure DrawMorphed(DestDIB, SrcDIB: TDIB; const X, Y, Width, Height,
  SourceX, SourceY: Integer; const Color: TColor);

procedure DrawAlpha(DestDIB, SrcDIB: TDIB; const X, Y, Width, Height,
  SourceX, SourceY,Alpha: Integer; const Color: TColor);

procedure DrawAlphaMask(DestDIB, SrcDIB, MaskDIB: TDIB; const X, Y, Width,
  Height, SourceX, SourceY: Integer);

procedure DrawAntialias(DestDIB,SrcDIB: TDIB);

procedure Draw3x3Matrix(DestDIB,SrcDIB: TDIB; Setting:TMatrixSetting);

procedure DrawMono(DestDIB, SrcDIB: TDIB; const X, Y, Width, Height,
  SourceX, SourceY:integer; const TransColor,ForeColor,BackColor: TColor);

// One-color Filters.

procedure FilterLine(DestDIB:TDIB; X1, Y1, X2, Y2: Integer; Color: TColor;
  FilterMode: TFilterMode);

procedure FilterRect(DestDIB: TDIB; X, Y, Width, Height: Integer;
  Color: TColor; FilterMode: TFilterMode);

// Lightsource.

procedure InitLight(Count,Detail: Integer);

procedure DrawLights(DestDIB: TDIB; FLight: TLightArray;
  AmbientLight: TColor);

procedure DrawOn(SrcCanvas:TCanvas; Dest : TRect ; DestCanvas : TCanvas ; Xsrc, Ysrc : Integer );  

var
  FLUTDist: array[0..255, 0..255] of Integer;
  LG_COUNT, LG_DETAIL : Integer;

implementation

function PosValue(Value:integer):integer;
begin
  If Value<0 then result:=0 else result:=Value;
end;

procedure CreateDIBFromBitmap(var DestDIB:TDIB;Bitmap:TBitmap);
begin
  DestDIB:=TDIB.Create;
  DestDIB.SetSize(Bitmap.Width,Bitmap.Height,24);
  DestDIB.Canvas.Draw(0,0,Bitmap);
end;

procedure Draw(DestDIB, SrcDIB: TDIB; X, Y, Width, Height,
  SourceX, SourceY: Integer);
begin
  //SrcDIB.Canvas.CopyRect(Bounds(X,Y,Width,Height),DestDIB.Canvas,Bounds(SourceX,SourceY,SrcDIB.Width,SrcDIB.Height));
  DrawOn(SrcDIB.Canvas,Rect(X, Y, Width, Height), DestDIB.Canvas, SourceX, SourceY);
end;

procedure DrawTransparent(DestDIB, SrcDIB: TDIB; const X, Y, Width, Height,
  SourceX, SourceY: Integer; const Color: TColor);
var
  i, j: Integer;
  k1, k2: Integer;
  n: Integer;
  p1, p2: PByteArray;

  Startk1, Startk2: Integer;

  StartY: Integer;
  EndY: Integer;

  DestStartY: Integer;
begin
  Startk1 := 3 * SourceX;
  Startk2 := 3 * X;

  DestStartY := Y - SourceY;

  StartY := SourceY;
  EndY := SourceY + Height;

  if (StartY + DestStartY < 0) then
    StartY := -DestStartY;
  if (EndY + DestStartY > DestDIB.Height) then
    EndY := DestDIB.Height - DestStartY;

  if (StartY < 0) then
    StartY := 0;
  if (EndY > SrcDIB.Height) then
    EndY := SrcDIB.Height;

  for j := StartY to EndY-1 do
  begin
    p1 := DestDIB.Scanline[j + DestStartY];
    p2 := SrcDIB.Scanline[j];

    k1 := Startk1;
    k2 := Startk2;

    for i := SourceX to SourceX + Width - 1 do
    begin
      n := (p2[k1] shl 16) + (p2[k1 + 1] shl 8) + p2[k1 + 2];

      if not (n = Color) then
      begin
        p1[k2] := p2[k1];
        p1[k2 + 1] := p2[k1 + 1];
        p1[k2 + 2] := p2[k1 + 2];
      end;

      k1 := k1 + 3;
      k2 := k2 + 3;
    end;
  end;
end;

procedure DrawShadow(DestDIB, SrcDIB: TDIB; X, Y, Width, Height,
  Frame: Integer;FilterMode:TFilterMode);
var
  i, j: Integer;
  p1, p2: PByte;
  FW: Integer;
begin
  FW := Frame * Width;
  for i := 1 to Height - 1 do
  begin
    p1 := DestDIB.Scanline[i + Y];
    p2 := SrcDIB.Scanline[i];
    inc(p1, 3 * (X + 1));
    inc(p2, 3 * (FW + 1));
    for j := 1 to Width - 1 do
    begin
      if (p2^ = 0) then
      begin
        Case FilterMode of
          fmNormal, fmMix50: begin
            p1^ := p1^ shr 1;               // Blue
            inc(p1);
            p1^ := p1^ shr 1;               // Green
            inc(p1);
            p1^ := p1^ shr 1;               // Red
            inc(p1);
          end;
          fmMix25: begin
            p1^ := p1^ - p1^ shr 2;         // Blue
            inc(p1);
            p1^ := p1^ - p1^ shr 2;         // Green
            inc(p1);
            p1^ := p1^ - p1^ shr 2;         // Red
            inc(p1);
          end;
          fmMix75: begin
            p1^ := p1^ shr 2;               // Blue
            inc(p1);
            p1^ := p1^ shr 2;               // Green
            inc(p1);
            p1^ := p1^ shr 2;               // Red
            inc(p1);
          end;
        end;
      end else inc(p1, 3);	// Not in the loop...
      inc(p2, 3);
    end;
  end;
end;

procedure DrawDarken(DestDIB, SrcDIB: TDIB; X, Y, Width, Height,
  Frame: Integer);
var
  frameoffset, i, j: integer;
  p1, p2: pByte;
  XOffset: Integer;
begin
  frameoffset := 3 * (Frame * Width) + 3;
  XOffset := 3*X + 3;
  For i := 1 to Height - 1 do
  begin
    p1 := DestDIB.Scanline[i + Y];
    p2 := SrcDIB.Scanline[i];
    inc(p1, XOffset);
    inc (p2, frameoffset);
    For j := 1 to Width - 1 do
    begin
      p1^ := (p2^ * p1^) shr 8; // R
      inc(p1);
      inc(p2);
      p1^ := (p2^ * p1^) shr 8; // G
      inc(p1);
      inc(p2);
      p1^ := (p2^ * p1^) shr 8; // B
      inc(p1);
      inc(p2);
    end;
  end;
end;

procedure DrawQuickAlpha(DestDIB, SrcDIB: TDIB; const X, Y, Width, Height,
  SourceX, SourceY: Integer; const Color: TColor;FilterMode:TFilterMode);
var
  i, j: Integer;
  k1, k2: Integer;
  n: Integer;
  p1, p2: PByteArray;
  BitSwitch1,BitSwitch2:boolean;

  Startk1, Startk2: Integer;

  //StartX: Integer;
  //EndX: Integer;

  StartY: Integer;
  EndY: Integer;

  DestStartY: Integer;
  //DestEndY: Integer;
  //DestStartX: Integer;
  //DestEndX: Integer;
begin
  Startk1 := 3 * SourceX;
  Startk2 := 3 * X;

  DestStartY := Y - SourceY;

  StartY := SourceY;
  EndY := SourceY + Height;

  if (StartY + DestStartY < 0) then
    StartY := -DestStartY;
  if (EndY + DestStartY > DestDIB.Height) then
    EndY := DestDIB.Height - DestStartY;

  if (StartY < 0) then
    StartY := 0;
  if (EndY > SrcDIB.Height) then
    EndY := SrcDIB.Height;

  If Odd(Y) then BitSwitch1:=true else BitSwitch1:=false;
  If Odd(X) then BitSwitch2:=true else BitSwitch2:=false;

  for j := StartY to EndY - 1 do
  begin
    BitSwitch1:= not BitSwitch1;
    p1 := DestDIB.Scanline[j + DestStartY];
    p2 := SrcDIB.Scanline[j];

    k1 := Startk1;
    k2 := Startk2;

    for i := SourceX to SourceX + Width - 1 do
    begin
      BitSwitch2:= not BitSwitch2;

      n := (p2[k1] shl 16) + (p2[k1 + 1] shl 8) + p2[k1 + 2];

    case FilterMode of
      fmNormal,fmMix50: if not (n = Color) and (BitSwitch1 xor BitSwitch2) then
      begin
        p1[k2] := p2[k1];
        p1[k2 + 1] := p2[k1 + 1];
        p1[k2 + 2] := p2[k1 + 2];
      end;
      fmMix25: if not (n = Color) and (BitSwitch1 and BitSwitch2) then
      begin
        p1[k2] := p2[k1];
        p1[k2 + 1] := p2[k1 + 1];
        p1[k2 + 2] := p2[k1 + 2];
      end;
      fmMix75: if not (n = Color) and (BitSwitch1 or BitSwitch2) then
      begin
        p1[k2] := p2[k1];
        p1[k2 + 1] := p2[k1 + 1];
        p1[k2 + 2] := p2[k1 + 2];
      end;
    end;

      k1 := k1 + 3;
      k2 := k2 + 3;
    end;
  end;
end;

procedure DrawAdditive(DestDIB, SrcDIB: TDIB; X, Y, Width, Height,Alpha, Frame:
Integer);
var
  frameoffset, i, j, Wid: integer;
  p1, p2: pByte;
begin
  If (Alpha <1) Or (Alpha>256) then Exit;
  Wid := Width shl 1 + Width;
  frameoffset := Wid * Frame;
  For i := 1 to Height - 1 do
  begin
    If (i + Y) > (DestDIB.Height-1) Then Break; //add 25.5.2004 JB.
    p1 := DestDIB.Scanline[i + Y];
    p2 := SrcDIB.Scanline[i];
    inc(p1, X shl 1 + X + 3);
    inc(p2, frameoffset + 3);
    For j := 3 to Wid - 4 do
    begin
      inc(p1^,(Alpha-p1^) * p2^ shr 8);
      inc(p1);
      inc(p2);
    end;
  end;
end;

procedure DrawTranslucent(DestDIB, SrcDIB: TDIB; const X, Y, Width, Height,
  SourceX, SourceY: Integer; const Color: TColor);
var
  i, j: Integer;
  k1, k2: Integer;
  n: Integer;
  p1, p2: PByteArray;

  Startk1, Startk2: Integer;

  //StartX: Integer;
  //EndX: Integer;

  StartY: Integer;
  EndY: Integer;

  DestStartY: Integer;
  //DestEndY: Integer;
  //DestStartX: Integer;
  //DestEndX: Integer;
begin
  Startk1 := 3 * SourceX;
  Startk2 := 3 * X;

  DestStartY := Y - SourceY;

  StartY := SourceY;
  EndY := SourceY + Height;

  if (StartY + DestStartY < 0) then
    StartY := -DestStartY;
  if (EndY + DestStartY > DestDIB.Height) then
    EndY := DestDIB.Height - DestStartY;

  if (StartY < 0) then
    StartY := 0;
  if (EndY > SrcDIB.Height) then
    EndY := SrcDIB.Height;

  for j := StartY to EndY - 1 do
  begin
    p1 := DestDIB.Scanline[j + DestStartY];
    p2 := SrcDIB.Scanline[j];

    k1 := Startk1;
    k2 := Startk2;

    for i := SourceX to SourceX + Width - 1 do
    begin
      n := (p2[k1] shl 16) + (p2[k1 + 1] shl 8) + p2[k1 + 2];

      if not (n = Color) then
      begin
        p1[k2] := (p1[k2]+p2[k1]) shr 1;
        p1[k2 + 1] := (p1[k2+1]+p2[k1+1]) shr 1;
        p1[k2 + 2] := (p1[k2+2]+p2[k1+2]) shr 1;
      end;

      k1 := k1 + 3;
      k2 := k2 + 3;
    end;
  end;
end;

procedure DrawAlpha(DestDIB, SrcDIB: TDIB; const X, Y, Width, Height,
  SourceX, SourceY,Alpha: Integer; const Color: TColor);
var
  i, j: Integer;
  k1, k2: Integer;
  n: Integer;
  p1, p2: PByteArray;

  Startk1, Startk2: Integer;

  //StartX: Integer;
  //EndX: Integer;

  StartY: Integer;
  EndY: Integer;

  DestStartY: Integer;
  //DestEndY: Integer;
begin
  Startk1 := 3 * SourceX;
  Startk2 := 3 * x;

  DestStartY := Y - SourceY;

  StartY := SourceY;
  EndY := SourceY + Height;

  if (EndY + DestStartY > DestDIB.Height) then
    EndY := DestDIB.Height - DestStartY;

  if (EndY > SrcDIB.Height) then
    EndY := SrcDIB.Height;

  if (StartY < 0) then
    StartY := 0;

  if (StartY + DestStartY < 0) then
    StartY := DestStartY;

  for j := StartY to EndY - 1 do
  begin
    p1 := DestDIB.Scanline[j + DestStartY];
    p2 := SrcDIB.Scanline[j];

    k1 := Startk1;
    k2 := Startk2;

    for i := SourceX to SourceX + Width - 1 do
    begin
      n := (p2[k1] shl 16) + (p2[k1 + 1] shl 8) + p2[k1 + 2];

      if not (n = Color) then
      begin
        p1[k2] := (p1[k2]*(256-Alpha)+p2[k1]*Alpha) shr 8;
        p1[k2 + 1] := (p1[k2+1]*(256-Alpha)+p2[k1+1]*Alpha) shr 8;
        p1[k2 + 2] := (p1[k2+2]*(256-Alpha)+p2[k1+2]*Alpha) shr 8;
      end;

      k1 := k1 + 3;
      k2 := k2 + 3;
    end;
  end;
end;

procedure DrawAlphaMask(DestDIB, SrcDIB,MaskDIB: TDIB; const X, Y,
  Width, Height, SourceX, SourceY: Integer);
var
  i, j: Integer;
  k1, k2, k3: Integer;
  p1, p2, p3: PByteArray;

  Startk1, Startk2: Integer;

  //StartX: Integer;
  //EndX: Integer;

  StartY: Integer;
  EndY: Integer;

  DestStartY: Integer;
  //DestEndY: Integer;
begin
  Startk1 := 3 * SourceX;
  Startk2 := 3 * x;

  DestStartY := Y - SourceY;

  StartY := SourceY;
  EndY := SourceY + Height;

  if (EndY + DestStartY > DestDIB.Height) then
    EndY := DestDIB.Height - DestStartY;

  if (EndY > SrcDIB.Height) then
    EndY := SrcDIB.Height;

  if (StartY < 0) then
    StartY := 0;

  if (StartY + DestStartY < 0) then
    StartY := DestStartY;

  for j := StartY to EndY - 1 do
  begin
    p1 := DestDIB.Scanline[j + DestStartY];
    p2 := SrcDIB.Scanline[j];
    p3 := MaskDIB.Scanline[j];

    k1 := Startk1;
    k2 := Startk2;
    k3 := 0;

    for i := SourceX to SourceX + Width - 1 do
    begin
      p1[k2] := (p1[k2]*(256-p3[k3])+p2[k1]*p3[k3]) shr 8;
      p1[k2 + 1] := (p1[k2+1]*(256-p3[k3])+p2[k1+1]*p3[k3]) shr 8;
      p1[k2 + 2] := (p1[k2+2]*(256-p3[k3])+p2[k1+2]*p3[k3]) shr 8;

      k1 := k1 + 3;
      k2 := k2 + 3;
      k3 := k3 + 3;
    end;
  end;
end;

procedure DrawMorphed(DestDIB, SrcDIB: TDIB; const X, Y, Width, Height,
  SourceX, SourceY: Integer; const Color: TColor);
var
  i, j, r, g, b: Integer;
  k1, k2: Integer;
  n: Integer;
  p1, p2: PByteArray;

  Startk1, Startk2: Integer;

  //StartX: Integer;
  //EndX: Integer;

  StartY: Integer;
  EndY: Integer;

  DestStartY: Integer;
  //DestEndY: Integer;
begin
  Startk1 := 3 * SourceX;
  Startk2 := 3 * x;

  DestStartY := Y - SourceY;

  StartY := SourceY;
  EndY := SourceY + Height;

  if (EndY + DestStartY > DestDIB.Height) then
    EndY := DestDIB.Height - DestStartY;

  if (EndY > SrcDIB.Height) then
    EndY := SrcDIB.Height;

  if (StartY < 0) then
    StartY := 0;

  if (StartY + DestStartY < 0) then
    StartY := DestStartY;

  r:=0;
  g:=0;
  b:=0;

  for j := StartY to EndY - 1 do
  begin
    p1 := DestDIB.Scanline[j + DestStartY];
    p2 := SrcDIB.Scanline[j];

    k1 := Startk1;
    k2 := Startk2;

    for i := SourceX to SourceX + Width - 1 do
    begin
      n := (p2[k1] shl 16) + (p2[k1 + 1] shl 8) + p2[k1 + 2];

      if Random(100)<50 then
      begin
        b:=p1[k2];
        g:=p1[k2+1];
        r:=p1[k2+2];
      end;

      if not (n = Color) then
      begin
        p1[k2] := b;
        p1[k2 + 1] := g;
        p1[k2 + 2] := r;
      end;

      k1 := k1 + 3;
      k2 := k2 + 3;
    end;
  end;
end;

procedure DrawMono(DestDIB, SrcDIB: TDIB; const X, Y, Width, Height,
  SourceX, SourceY:integer; const TransColor,ForeColor,BackColor: TColor);
var
  i, j, r1, g1, b1, r2, g2, b2: Integer;
  k1, k2: Integer;
  n: Integer;
  p1, p2: PByteArray;

  Startk1, Startk2: Integer;

  //StartX: Integer;
  //EndX: Integer;

  StartY: Integer;
  EndY: Integer;

  DestStartY: Integer;
  //DestEndY: Integer;
begin
  Startk1 := 3 * SourceX;
  Startk2 := 3 * x;

  DestStartY := Y - SourceY;

  StartY := SourceY;
  EndY := SourceY + Height;

  if (EndY + DestStartY > DestDIB.Height) then
    EndY := DestDIB.Height - DestStartY;

  if (EndY > SrcDIB.Height) then
    EndY := SrcDIB.Height;

  if (StartY < 0) then
    StartY := 0;

  if (StartY + DestStartY < 0) then
    StartY := DestStartY;

  r1:=GetRValue(BackColor);
  g1:=GetGValue(BackColor);
  b1:=GetBValue(BackColor);

  r2:=GetRValue(ForeColor);
  g2:=GetGValue(ForeColor);
  b2:=GetBValue(ForeColor);


  for j := StartY to EndY-1 do
  begin
    p1 := DestDIB.Scanline[j + DestStartY];
    p2 := SrcDIB.Scanline[j];

    k1 := Startk1;
    k2 := Startk2;

    for i := SourceX to SourceX + Width - 1 do
    begin
      n := (p2[k1] shl 16) + (p2[k1 + 1] shl 8) + p2[k1 + 2];

      if (n = TransColor) then
      begin
        p1[k2] := b1;
        p1[k2 + 1] := g1;
        p1[k2 + 2] := r1;
      end else
      begin
        p1[k2] := b2;
        p1[k2 + 1] := g2;
        p1[k2 + 2] := r2;
      end;

      k1 := k1 + 3;
      k2 := k2 + 3;
    end;
  end;
end;

procedure Draw3x3Matrix(DestDIB,SrcDIB: TDIB; Setting:TMatrixSetting);
var i,j,k:integer;
    p1,p2,p3,p4:PByteArray;
begin
  For i:=1 to SrcDIB.Height-2 do
  begin
    p1:=SrcDIB.ScanLine[i-1];
    p2:=SrcDIB.ScanLine[i];
    p3:=SrcDIB.ScanLine[i+1];
    p4:=DestDIB.ScanLine[i];
    For j:=3 to 3*SrcDIB.Width-4 do
    begin
      k:=(p1[j-3]*Setting[0] + p1[j]*Setting[1] + p1[j+3]*Setting[2] +
          p2[j-3]*Setting[3] + p2[j]*Setting[4] + p2[j+3]*Setting[5] +
          p3[j-3]*Setting[6] + p3[j]*Setting[7] + p3[j+3]*Setting[8])
          div Setting[9];
      If k<0 then k:=0;
      If k>255 then k:=255;
      p4[j]:=k;
    end;
  end;
end;

procedure DrawAntialias(DestDIB, SrcDIB: TDIB);
var i,j,k,l,m:integer;
    p1,p2,p3:PByteArray;
begin
  For i:=1 to DestDIB.Height-1 do
  begin
  k:=i shl 1;
  p1:=SrcDIB.Scanline[k];
  p2:=SrcDIB.Scanline[k+1];
  p3:=DestDIB.Scanline[i];
    For j:=1 to DestDIB.Width-1 do
    begin
      m:=3*j;
      l:=m shl 1;
      p3[m]:=(p1[l]+p1[l+3]+p2[l]+p2[l+3]) shr 2;
      p3[m+1]:=(p1[l+1]+p1[l+4]+p2[l+1]+p2[l+4]) shr 2;
      p3[m+2]:=(p1[l+2]+p1[l+5]+p2[l+2]+p2[l+5]) shr 2;
    end;
  end;
end;

procedure FilterLine(DestDIB: TDIB; X1, Y1, X2, Y2: Integer; Color: TColor;
  FilterMode: TFilterMode);
var
  i,j:integer;
  t:TColor;
  r1,g1,b1,r2,g2,b2:integer;
begin
  j:=ROUND(Sqrt(Sqr(ABS(X2-X1))+Sqr(ABS(Y2-Y1))));
  If j<1 then Exit;

  r1:=GetRValue(Color);
  g1:=GetGValue(Color);
  b1:=GetBValue(Color);

  For i:=0 to j do
  begin
    t:=DestDIB.Pixels[X1+((X2-X1)*i div j),Y1+((Y2-Y1)*i div j)];
    r2:=GetRValue(t);
    g2:=GetGValue(t);
    b2:=GetBValue(t);
    case FilterMode of
      fmNormal:t:=RGB(r1+(((256-r1)*r2) shr 8),
                      g1+(((256-g1)*g2) shr 8),
                      b1+(((256-b1)*b2) shr 8));
      fmMix25: t:=RGB((r1+r2*3)shr 2,(g1+g2*3)shr 2,(b1+b2*3)shr 2);
      fmMix50: t:=RGB((r1+r2)shr 1,(g1+g2)shr 1,(b1+b2)shr 1);
      fmMix75: t:=RGB((r1*3+r2)shr 2,(g1*3+g2)shr 2,(b1*3+b2)shr 2);
    end;
    DestDIB.Pixels[X1+((X2-X1)*i div j),Y1+((Y2-Y1)*i div j)]:=t;
  end;
end;

procedure FilterRect(DestDIB: TDIB; X, Y, Width, Height: Integer;
  Color: TColor; FilterMode: TFilterMode);
var
  i, j, r, g, b, C1:integer;
  p1,p2,p3:pByte;
begin
  r:=GetRValue(Color);
  g:=GetGValue(Color);
  b:=GetBValue(Color);

  For i:=0 to Height-1 do
  begin
    p1:=DestDIB.Scanline[i+Y];
    Inc(p1,(3*X));
    For j:=0 to Width-1 do
      begin
        Case FilterMode of
        fmNormal: begin
          p2:=p1;
          Inc(p2);
          p3:=p2;
          Inc(p3);
          C1:=(p1^+p2^+p3^) div 3;

          p1^:=(C1*b) shr 8;
          Inc(p1);
          p1^:=(C1*g) shr 8;
          Inc(p1);
          p1^:=(C1*r) shr 8;
          Inc(p1);
        end;
        fmMix25: begin
          p1^:=(3*p1^+b) shr 2;
          Inc(p1);
          p1^:=(3*p1^+g) shr 2;
          Inc(p1);
          p1^:=(3*p1^+r) shr 2;
          Inc(p1);
        end;
        fmMix50: begin
          p1^:=(p1^+b) shr 1;
          Inc(p1);
          p1^:=(p1^+g) shr 1;
          Inc(p1);
          p1^:=(p1^+r) shr 1;
          Inc(p1);
        end;
        fmMix75: begin
          p1^:=(p1^+3*b) shr 2;
          Inc(p1);
          p1^:=(p1^+3*g) shr 2;
          Inc(p1);
          p1^:=(p1^+3*r) shr 2;
          Inc(p1);
        end;
      end;
    end;
  end;
end;

procedure InitLight(Count,Detail: Integer);
var
  i, j: Integer;
begin
  LG_COUNT:=Count;
  LG_DETAIL:=Detail;

  For i:=0 to 255 do      // Build Lightning LUT
    For j:=0 to 255 do
      FLUTDist[i,j]:=ROUND(Sqrt(Sqr(i*10)+Sqr(j*10)));
end;

procedure DrawLights(DestDIB: TDIB; FLight: TLightArray;
  AmbientLight: TColor);
var
  i,j,l,m,n,o,q,D1,D2,R,G,B,AR,AG,AB:integer;
  P: array of PByteArray;
begin
  SetLength(P,LG_DETAIL);

  AR:=GetRValue(AmbientLight);
  AG:=GetGValue(AmbientLight);
  AB:=GetBValue(AmbientLight);

  For i:=(DestDIB.Height div (LG_DETAIL+1)) downto 1 do
  begin
    For o:=0 to LG_DETAIL do
      P[o]:=DestDIB.Scanline[(LG_DETAIL+1)*i-o];

    For j:=(DestDIB.Width div (LG_DETAIL+1)) downto 1 do
    begin
      R:=AR;
      G:=AG;
      B:=AB;

      For l:=LG_COUNT-1 downto 0 do  // Check the lightsources
      begin
        D1:=ABS(j*(LG_DETAIL+1)-FLight[l].X) div FLight[l].Size1;
        D2:=ABS(i*(LG_DETAIL+1)-FLight[l].Y) div FLight[l].Size2;
        If D1>255 then D1:=255;
        If D2>255 then D2:=255;

        m:=255-FLUTDist[D1,D2];
        If m<0 then m:=0;

        Inc(R,(PosValue(GetRValue(FLight[l].Color)-R)*m shr 8));
        Inc(G,(PosValue(GetGValue(FLight[l].Color)-G)*m shr 8));
        Inc(B,(PosValue(GetBValue(FLight[l].Color)-B)*m shr 8));
      end;

      For q:=LG_DETAIL downto 0 do
      begin
        n:=3*(j*(LG_DETAIL+1)-q);

        For o:=LG_DETAIL downto 0 do
        begin
          P[o][n]:=(P[o][n]*B) shr 8;
          P[o][n+1]:=(P[o][n+1]*G) shr 8;
          P[o][n+2]:=(P[o][n+2]*R) shr 8;
        end;
      end;
    end;
  end;
  SetLength(P,0);
end;

procedure DrawOn(SrcCanvas:TCanvas; Dest : TRect ; DestCanvas : TCanvas ; Xsrc, Ysrc : Integer );
{procedure is supplement of original TDIBUltra function}
Begin
  If Not AsSigned(SrcCanvas) Then Exit;
  If (Xsrc<0) Then
  Begin
    Dec(Dest.Left, Xsrc);
    Inc(Dest.Right  {Width }, Xsrc);
    Xsrc := 0
  End;
  If (Ysrc<0) Then
  Begin
    Dec(Dest.Top,  Ysrc);
    Inc(Dest.Bottom {Height}, Ysrc);
    Ysrc := 0
  End;
  BitBlt( DestCanvas.Handle, Dest.Left, Dest.Top, Dest.Right, Dest.Bottom, SrcCanvas.Handle, Xsrc, Ysrc, SRCCOPY);
End;

end.

