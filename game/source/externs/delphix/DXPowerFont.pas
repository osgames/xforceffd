(*******************************************************************************
 * Unit Name: DXPowerFont.pas
 * Information: Writed By Ramin.S.Zaghi (Based On Wilson's DXFont Unit)
 * Last Changes: Dec 25 2000;
 * Unit Information:
 *     This unit includes a VCL-Component for DelphiX. This component draws the
 *     Character-Strings on a TDirectDrawSurface. This component helps the
 *     progarmmers to using custom fonts and printing texts easily such as
 *     TCanvas.TextOut function...
 * Includes:
 * 1. TDXPowerFontTextOutEffect ==> The kinds of drawing effects.
 *    - teNormal: Uses the Draw function. (Normal output)
 *    - teRotat: Uses the DrawRotate function. (Rotates each character)
 *    - teAlphaBlend: Uses DrawAlpha function. (Blends each character)
 *    - teWaveX: Uses DrawWaveX function. (Adds a Wave effect to the each character)
 *
 * 2. TDXPowerFontTextOutType ==> The kinds of each caracter.
 *    - ttUpperCase: Uppers all characters automaticaly.
 *    - ttLowerCase: Lowers all characters automaticaly.
 *    - ttNormal: Uses all characters with out any converting.
 *
 * 3. TDXPowerFontEffectsParameters ==> Includes the parameters for adding effects to the characters.
 *    - (CenterX, CenterY): The rotating center point.
 *    - (Width, Height): The new size of each character.
 *    - Angle: The angle of rotate.
 *    - AlphaValue: The value of Alpha-Chanel.
 *    - WAmplitude: The Amplitude of Wave function. (See The Help Of DelphiX)
 *    - WLenght: The Lenght Of Wave function. (See The Help Of DelphiX)
 *    - WPhase: The Phase Of Wave function. (See The Help Of DelphiX)
 *
 * 4. TDXPowerFontBeforeTextOutEvent ==> This is an event that occures before
 *    drawing texts on to TDirectDrawSurface object.
 *    - Sender: Retrieves the event caller object.
 *    - Text: Retrieves the text sended text for drawing.
 *      (NOTE: The changes will have effect)
 *    - DoTextOut: The False value means that the TextOut function must be stopped.
 *      (NOTE: The changes will have effect)
 *
 * 5. TDXPowerFontAfterTextOutEvent ==> This is an event that occures after
 *    drawing texts on to TDirectDrawSurface object.
 *    - Sender: Retrieves the event caller object.
 *    - Text: Retrieves the text sended text for drawing.
 *      (NOTE: The changes will not have any effects)
 *
 * 6. TDXPowerFont ==> I sthe main class of PowerFont VCL-Component.
 *    - property Font: string; The name of custom-font's image in the TDXImageList items.
 *    - property FontIndex: Integer; The index of custom-font's image in the TDXImageList items.
 *    - property DXImageList: TDXImageList; The TDXImageList that includes the image of custom-fonts.
 *    - property UseEnterChar: Boolean; When the value of this property is True, The component caculates Enter character.
 *    - property EnterCharacter: String;
 *==>   Note that TDXPowerFont calculates tow kinds of enter character:
 *==>   E1. The Enter character that draws the characters after it self in a new line and after last drawed character, ONLY.
 *==>   E2. The Enter character that draws the characters after it self in a new line such as #13#10 enter code in delphi.
 *==>   Imporatant::
 *==>       (E1) TDXPowerFont uses the first caracter of EnterCharacter string as the first enter caracter (Default value is '|').
 *==>       (E2) and uses the second character as the scond enter caracter (Default value is '<')
 *    - property BeforeTextOut: TDXPowerFontBeforeTextOutEvent; See TDXPowerFontBeforeTextOutEvent.
 *    - property AfterTextOut: TDXPowerFontAfterTextOutEvent; See TDXPowerFontAfterTextOutEvent.
 *    - property Alphabets: string; TDXPowerFont uses this character-string for retrieving the pattern number of each character.
 *    - property TextOutType: TDXPowerFontTextOutType; See TDXPowerFontTextOutType.
 *    - property TextOutEffect: TDXPowerFontTextOutEffect; See TDXPowerFontTextOutEffect.
 *    - property EffectsParameters: TDXPowerFontEffectsParameters; See TDXPowerFontEffectsParameters.
 *
 *    - function TextOut(DirectDrawSurface: TDirectDrawSurface; X, Y: Integer; const Text: string): Boolean;
 *      This function draws/prints the given text on the given TDirectDrawSurface.
 *      - DirectDrawSurface: The surface for drawing text (character-string).
 *      - (X , Y): The first point of outputed text. (Such as X,Y parameters in TCanvas.TextOut function)
 *      - Text: The text for printing.
 *      Return values: This function returns False when an error occured or...
 *    - function TextOutFast(DirectDrawSurface: TDirectDrawSurface; X, Y: Integer; const Text: string): Boolean;
 *      This function works such as TextOut function but,
 *      with out calculating any Parameters/Effects/Enter-Characters/etc...
 *      This function calculates the TextOutType, ONLY.
 *
 * Ramin.S.Zaghi (ramin_zaghi@yahoo.com)
 * (Based on wilson's code for TDXFont VCL-Component/Add-On)
 * (wilson@no2games.com)
 *
 * For more information visit:
 *  www.no2games.com
 *  turbo.gamedev.net
 ******************************************************************************)
unit DXPowerFont;

interface

uses
	Classes, DXDraws; // modified

type
	TDXPowerFontTextOutEffect = (teNormal, teRotat, teAlphaBlend, teWaveX);
	TDXPowerFontTextOutType = (ttUpperCase, ttLowerCase, ttNormal);
	TDXPowerFontBeforeTextOutEvent = procedure(Sender: TObject; var Text: string; var DoTextOut: Boolean) of object;
	TDXPowerFontAfterTextOutEvent = procedure(Sender: TObject; Text: string) of object;

	TDXPowerFontEffectsParameters = class(TPersistent)
	private
		FCenterX: Integer;
		FCenterY: Integer;
		FHeight: Integer;
		FWidth: Integer;
		FAngle: Integer;
		FAlphaValue: Integer;
		FWPhase: Integer;
		FWAmplitude: Integer;
		FWLenght: Integer;
		procedure SetAngle(const Value: Integer);
		procedure SetCenterX(const Value: Integer);
		procedure SetCenterY(const Value: Integer);
		procedure SetHeight(const Value: Integer);
		procedure SetWidth(const Value: Integer);
		procedure SetAlphaValue(const Value: Integer);
		procedure SetWAmplitude(const Value: Integer);
		procedure SetWLenght(const Value: Integer);
		procedure SetWPhase(const Value: Integer);
	published
		property CenterX: Integer read FCenterX write SetCenterX;
		property CenterY: Integer read FCenterY write SetCenterY;
		property Width: Integer read FWidth write SetWidth;
		property Height: Integer read FHeight write SetHeight;
		property Angle: Integer read FAngle write SetAngle;
		property AlphaValue: Integer read FAlphaValue write SetAlphaValue;
		property WAmplitude: Integer read FWAmplitude write SetWAmplitude;
		property WLenght: Integer read FWLenght write SetWLenght;
		property WPhase: Integer read FWPhase write SetWPhase;
	end;

	TDXPowerFont= class(TComponent)
	private
		FDXImageList: TDXImageList;
		FFont: string;
		FFontIndex: Integer;
		FUseEnterChar: Boolean;
        FEnterCharacter: String;
		FAfterTextOut: TDXPowerFontAfterTextOutEvent;
		FBeforeTextOut: TDXPowerFontBeforeTextOutEvent;
		FAlphabets: string;
		FTextOutType: TDXPowerFontTextOutType;
		FTextOutEffect: TDXPowerFontTextOutEffect;
		FEffectsParameters: TDXPowerFontEffectsParameters;
		procedure SetFont(const Value: string);
		procedure SetFontIndex(const Value: Integer);
		procedure SetUseEnterChar(const Value: Boolean);
        procedure SetEnterCharacter(const Value: String);
		procedure SetAlphabets(const Value: string);
		procedure SetTextOutType(const Value: TDXPowerFontTextOutType);
		procedure SetTextOutEffect(const Value: TDXPowerFontTextOutEffect);
		procedure SetEffectsParameters(const Value: TDXPowerFontEffectsParameters);
	published
		property Font: string read FFont write SetFont;
		property FontIndex: Integer read FFontIndex write SetFontIndex;
		property DXImageList: TDXImageList read FDXImageList write FDXImageList;
		property UseEnterChar: Boolean read FUseEnterChar write SetUseEnterChar;
		property EnterCharacter: String read FEnterCharacter write SetEnterCharacter;
		property BeforeTextOut: TDXPowerFontBeforeTextOutEvent read FBeforeTextOut write FBeforeTextOut;
		property AfterTextOut: TDXPowerFontAfterTextOutEvent read FAfterTextOut write FAfterTextOut;
		property Alphabets: string read FAlphabets write SetAlphabets;
		property TextOutType: TDXPowerFontTextOutType read FTextOutType write SetTextOutType;
		property TextOutEffect: TDXPowerFontTextOutEffect read FTextOutEffect write SetTextOutEffect;
		property EffectsParameters: TDXPowerFontEffectsParameters read FEffectsParameters write SetEffectsParameters;
	public
		Offset: integer;
		constructor Create(AOwner: TComponent); override;
		destructor Destroy; override;
		function TextOut(DirectDrawSurface: TDirectDrawSurface; X, Y: Integer; const Text: string): Boolean;
		function TextOutFast(DirectDrawSurface: TDirectDrawSurface; X, Y: Integer; const Text: string): Boolean;
	end;

procedure Register;

implementation
uses Windows,SysUtils; // modified

constructor TDXPowerFont.Create(AOwner: TComponent);
begin
	inherited Create(AOwner);
	FUseEnterChar:= True;
	FEnterCharacter:= '|<';
	FAlphabets:= 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ`1234567890-=~!@#$%^&*()_+[];'',./\{}:"<>?|��� ';
	FTextOutType:= ttNormal;
	FTextOutEffect:= teNormal;
	FEffectsParameters:= TDXPowerFontEffectsParameters.Create;
end;

destructor TDXPowerFont.Destroy;
begin
	inherited Destroy;
end;

procedure TDXPowerFont.SetAlphabets(const Value: string);
begin
	if FDXImageList <> nil then
		if Length(Value) > FDXImageList.Items[FFontIndex].PatternCount- 1 then Exit;
	FAlphabets := Value;
end;

procedure TDXPowerFont.SetEnterCharacter(const Value: String);
begin
	if Length(Value) >= 2 then Exit;
	FEnterCharacter := Value;
end;

procedure TDXPowerFont.SetFont(const Value: string);
begin
	FFont := Value;
	if FDXImageList <> nil then
	begin
		FFontIndex := FDXImageList.Items.IndexOf(FFont); // Find font once...
		Offset := FDXImageList.Items[FFontIndex].PatternWidth;

		FEffectsParameters.Width := FDXImageList.Items[FFontIndex].PatternWidth;
		FEffectsParameters.Height := FDXImageList.Items[FFontIndex].PatternHeight;
	end;
end;

procedure TDXPowerFont.SetFontIndex(const Value: Integer);
begin
	FFontIndex := Value;
	if FDXImageList <> nil then
	begin
		FFont := FDXImageList.Items[FFontIndex].Name;
		Offset := FDXImageList.Items[FFontIndex].PatternWidth;

		FEffectsParameters.Width := FDXImageList.Items[FFontIndex].PatternWidth;
		FEffectsParameters.Height := FDXImageList.Items[FFontIndex].PatternHeight;
	end;
end;

procedure TDXPowerFont.SetEffectsParameters(const Value: TDXPowerFontEffectsParameters);
begin
	FEffectsParameters := Value;
end;

procedure TDXPowerFont.SetTextOutEffect(const Value: TDXPowerFontTextOutEffect);
begin
	FTextOutEffect := Value;
end;

procedure TDXPowerFont.SetTextOutType(const Value: TDXPowerFontTextOutType);
begin
	FTextOutType := Value;
end;

procedure TDXPowerFont.SetUseEnterChar(const Value: Boolean);
begin
	FUseEnterChar := Value;
end;

function TDXPowerFont.TextOutFast(DirectDrawSurface: TDirectDrawSurface; X, Y: Integer; const Text: string): Boolean;
var
	Loop, Letter: Integer;
        txt : string;
begin
	Result:= False;
	if FDXImageList = nil then Exit;
        // modified
	case FTextOutType of
		ttNormal: Txt := Text;
		ttUpperCase: Txt := AnsiUpperCase(Text);
		ttLowerCase: Txt := AnsiLowerCase(Text);
            end;
	Offset := FDXImageList.Items[FFontIndex].PatternWidth;
	Loop := 1;
	while (Loop <= Length(Text)) do
	begin
                Letter := AnsiPos(txt[Loop], FAlphabets);// modified
		if (Letter > 0) and (Letter < FDXImageList.Items[FFontIndex].PatternCount- 1) then
			FDXImageList.Items[FFontIndex].Draw(DirectDrawSurface, X + (Offset * Loop), Y, Letter - 1);
		Inc(Loop);
	end;
	Result:= True;
end;

function TDXPowerFont.TextOut(DirectDrawSurface: TDirectDrawSurface; X, Y: Integer; const Text: string): Boolean;
var
	Loop, Letter: Integer;
	FCalculatedEnters, EnterHeghit, XLoop: Integer;
	DoTextOut: Boolean;
	Txt: string;
	Rect: TRect;
begin
	Result:= False;
	if FDXImageList = nil then Exit;
        Txt:= Text;
	DoTextOut:= True;
	if Assigned(FBeforeTextOut) then FBeforeTextOut(Self, Txt, DoTextOut);
	if not DoTextOut then Exit;
	 // modified
	case FTextOutType of
	        ttNormal: Txt:= Text;
		ttUpperCase: Txt := AnsiUpperCase(Text);
		ttLowerCase: Txt := AnsiLowerCase(Text);
             end;
	Offset := FDXImageList.Items[FFontIndex].PatternWidth;
	FCalculatedEnters:= 0;
	EnterHeghit:= FDXImageList.Items[FFontIndex].PatternHeight;
	XLoop:= 0;
	Loop := 1;
	while (Loop <= Length(Txt)) do
	begin
		if FUseEnterChar then
		begin
			if Txt[Loop]= FEnterCharacter[1] then begin Inc(FCalculatedEnters); Inc(Loop); end;
			if Txt[Loop]= FEnterCharacter[2] then begin Inc(FCalculatedEnters); XLoop:= 0;{-FCalculatedEnters;} Inc(Loop); end;
		end;
                Letter := AnsiPos(Txt[Loop], FAlphabets); // modified

		if (Letter > 0) and (Letter < FDXImageList.Items[FFontIndex].PatternCount- 1) then
			case FTextOutEffect of
				teNormal: FDXImageList.Items[FFontIndex].Draw(DirectDrawSurface, X + (Offset * XLoop), Y + (FCalculatedEnters*EnterHeghit), Letter - 1);
				teRotat: FDXImageList.Items[FFontIndex].DrawRotate(DirectDrawSurface, X + (Offset * XLoop), Y + (FCalculatedEnters*EnterHeghit), FEffectsParameters.Width, FEffectsParameters.Height, Letter - 1, FEffectsParameters.CenterX, FEffectsParameters.CenterY, FEffectsParameters.Angle);
				teAlphaBlend:
					begin
						Rect.Left:= X + (Offset * XLoop);
						Rect.Top:= Y + (FCalculatedEnters*EnterHeghit);
						Rect.Right:= Rect.Left+ FEffectsParameters.Width;
						Rect.Bottom:= Rect.Top+ FEffectsParameters.Height;

						FDXImageList.Items[FFontIndex].DrawAlpha(DirectDrawSurface, Rect , Letter - 1, FEffectsParameters.AlphaValue);
                    end;
				teWaveX: FDXImageList.Items[FFontIndex].DrawWaveX(DirectDrawSurface, X + (Offset * XLoop), Y + (FCalculatedEnters*EnterHeghit), FEffectsParameters.Width, FEffectsParameters.Height, Letter - 1, FEffectsParameters.WAmplitude, FEffectsParameters.WLenght, FEffectsParameters.WPhase);
			end;
		Inc(Loop);
		Inc(XLoop);
	end;
	if Assigned(FAfterTextOut) then FAfterTextOut(Self, Txt);
	Result:= True;
end;

{ TDXPowerFontEffectsParameters }

procedure TDXPowerFontEffectsParameters.SetAlphaValue(
  const Value: Integer);
begin
	FAlphaValue := Value;
end;

procedure TDXPowerFontEffectsParameters.SetAngle(const Value: Integer);
begin
	FAngle := Value;
end;

procedure TDXPowerFontEffectsParameters.SetCenterX(const Value: Integer);
begin
	FCenterX := Value;
end;

procedure TDXPowerFontEffectsParameters.SetCenterY(const Value: Integer);
begin
	FCenterY := Value;
end;

procedure TDXPowerFontEffectsParameters.SetHeight(const Value: Integer);
begin
	FHeight := Value;
end;

procedure TDXPowerFontEffectsParameters.SetWAmplitude(
  const Value: Integer);
begin
	FWAmplitude := Value;
end;

procedure TDXPowerFontEffectsParameters.SetWidth(const Value: Integer);
begin
	FWidth := Value;
end;

procedure TDXPowerFontEffectsParameters.SetWLenght(const Value: Integer);
begin
	FWLenght := Value;
end;

procedure TDXPowerFontEffectsParameters.SetWPhase(const Value: Integer);
begin
	FWPhase := Value;
end;

{ Standards }
procedure Register;
begin
	RegisterComponents('DelphiX', [TDXPowerFont]);
end;

end.
