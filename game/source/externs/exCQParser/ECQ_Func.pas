unit ECQ_Func;

interface

uses
  ECQ_Def, SysUtils;

type
  TFunctionClass = class of TFunction;

  TFunction = class
  published
    class function Float(Param1, Param2: TVariable; _R: TVariable): TVariable; virtual; abstract;
    class function Complex(Param1, Param2: TVariable; _R: TVariable): TVariable; virtual; abstract;
    class function Quad(Param1, Param2: TVariable; _R: TVariable): TVariable; virtual; abstract;

    class function Supports(Mode: TParserMode): Boolean; virtual; abstract;

    class function ParamCount: Integer; virtual; abstract;
  end;

  TFunctionItem = class
  private
    fName: string;
    fFunction: TFunctionClass;

    fPCount: Integer;
    fEAX: Pointer;

    fFloat,
    fComplex,
    fQuad: TECQPFunction;
  public
    constructor Create(AFunction: TFunctionClass; Name: string; O: Boolean = false); virtual;

    property Name: string read fName;

    property Float: TECQPFunction read fFloat;
    property Complex: TECQPFunction read fComplex;
    property Quad: TECQPFunction read fQuad;

    property FunctionClass: TFunctionClass read fFunction;

    property ParamCount: Integer read fPCount;

    property EAX: Pointer read fEAX;
  end;

  TFunctionList = class
  private
    fItems: array of TFunctionItem;

    function GetItem(Index: Integer): TFunctionItem;
    procedure Grow;
  public
    destructor Destroy; override;

    property Items[Index: Integer]: TFunctionItem read GetItem; default;

    procedure Add(Item: TFunctionItem);
    procedure Delete(Index: Integer);

    function Length: Integer;
  end;

implementation

uses
  ECQ_Ops;

constructor TFunctionItem.Create(AFunction: TFunctionClass; Name: string; O: Boolean = false);
var t: TObject;
begin
  inherited Create;
  fName := Name;
  fFunction := AFunction;
  if fFunction.Supports(pmReal) then
    fFloat := fFunction.Float;
  if fFunction.Supports(pmComplex) then
    fComplex := fFunction.Complex;
  if fFunction.Supports(pmQuad) then
    fQuad := fFunction.Quad;
  if not O then
    fPCount := AFunction.ParamCount;
  fEAX := Pointer(AFunction.NewInstance);
  t := fEAX;
  fEAX := Pointer(fEAX^);
  t.Destroy;
end;

destructor TFunctionList.Destroy;
var i: Integer;
begin
  for i := 0 to High(fItems) do
    fItems[i].Free;
  SetLength(fItems, 0);
  inherited;
end;

function TFunctionList.GetItem(Index: Integer): TFunctionItem;
begin
  Result := fItems[Index];
end;

procedure TFunctionList.Grow;
begin
  SetLength(fItems, System.Length(fItems)+1);
end;

procedure TFunctionList.Add(Item: TFunctionItem);
begin
  Grow;
  fItems[High(fItems)] := Item;
end;

procedure TFunctionList.Delete(Index: Integer);
begin
  Move(fItems[Index+1], fItems[Index], System.Length(fItems)-Index);
  SetLength(fItems, High(fItems));
end;

function TFunctionList.Length: Integer;
begin
  Result := System.Length(fItems);
end;

end.
