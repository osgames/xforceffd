unit ECQ_Ops;

interface

uses
  ECQ_Def, ECQ_Func, SysUtils;

type
  TOperatorType = (otUnaryPrefix, otUnaryPostfix, otBinary);

  ENoMulOperand = class(Exception);

  TOperator = class(TFunction)
  private
    fName: Char;
    fFunction: TFunctionClass;

    fType: TOperatorType;
    fPrec: Integer;

    fMul: Boolean;

    fFloat,
    fComplex,
    fQuad: TECQPFunction;
  public
    constructor Create(AFunction: TFunctionClass; Name: Char;
                       OpType: TOperatorType; Precedence: Integer;
                       IsMul: Boolean = False); virtual;

    property Name: Char read fName;
{$WARNINGS OFF}
    property Float: TECQPFunction read fFloat;
    property Complex: TECQPFunction read fComplex;
    property Quad: TECQPFunction read fQuad;
{$WARNINGS ON}
    property OpType: TOperatorType read fType;
    property Precedence: Integer read fPrec;

    property IsMul: Boolean read fMul;

    property FunctionClass: TFunctionClass read fFunction;

    function Supports(Mode: TParserMode): Boolean; reintroduce;
    function ParamCount: Integer; reintroduce;
  end;

  TOperatorList = class
  private
    fItems: array of TOperator;

    function GetItem(Index: Integer): TOperator;
    procedure Grow;
    function GetMul: TOperator;
  public
    destructor Destroy; override;

    property Items[Index: Integer]: TOperator read GetItem; default;

    property Mul: TOperator read GetMul;

    procedure Add(Item: TOperator);
    procedure Delete(Index: Integer);

    function Length: Integer;
  end;

implementation

constructor TOperator.Create(AFunction: TFunctionClass; Name: Char;
                             OpType: TOperatorType; Precedence: Integer;
                             IsMul: Boolean = false);
begin
  inherited Create;
  fName := Name;
  fFunction := AFunction;
  if fFunction.Supports(pmReal) then
    fFloat := fFunction.Float;
  if fFunction.Supports(pmComplex) then
    fComplex := fFunction.Complex;
  if fFunction.Supports(pmQuad) then
    fQuad := fFunction.Quad;
  fType := OpType;
  fPrec := Precedence;
  fMul := IsMul;
end;

destructor TOperatorList.Destroy;
var i: Integer;
begin
  for i := 0 to High(fItems) do
    fItems[i].Free;
  SetLength(fItems, 0);
  inherited;
end;

function TOperatorList.GetItem(Index: Integer): TOperator;
begin
  Result := fItems[Index];
end;

procedure TOperatorList.Grow;
begin
  SetLength(fItems, System.Length(fItems)+1);
end;

procedure TOperatorList.Add(Item: TOperator);
var i: Integer;
begin
  for i := 0 to High(fItems) do
    if (fItems[i].Name = Item.Name) and
       (fItems[i].OpType = Item.OpType) then
    begin
      fItems[i].Free;
      fItems[i] := Item;
      Exit;
    end;
  Grow;
  fItems[High(fItems)] := Item;
end;

procedure TOperatorList.Delete(Index: Integer);
begin
  Move(fItems[Index+1], fItems[Index], System.Length(fItems)-Index);
  SetLength(fItems, High(fItems));
end;

function TOperatorList.Length: Integer;
begin
  Result := System.Length(fItems);
end;

function TOperatorList.GetMul: TOperator;
var i: Integer;
begin
  for i := 0 to High(fItems) do
    if fItems[i].IsMul then
    begin
      Result := fItems[i];
      Exit;
    end;
  raise ENoMulOperand.Create('');
end;

function TOperator.Supports(Mode: TParserMode): Boolean;
begin
  Result := False;
  case Mode of
    pmReal: Result := Assigned(fFloat);
    pmComplex: Result := Assigned(fComplex);
    pmQuad: Result := Assigned(fQuad);
  end;
end;

function TOperator.ParamCount: Integer;
begin
  Result := fFunction.ParamCount;
end;

end.
