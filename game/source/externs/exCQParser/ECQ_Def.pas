unit ECQ_Def;

interface

uses
  SysUtils;

type
  TFloat = Double;

  TParserMode = (pmReal, pmComplex, pmQuad);
     
  TCallType = (ctFun, ctOp, ctNone);

  TOpCallType = (octPrefix, octPostfix, octBinary);

  TSolveMode = (smVM, smRM);

  PVariable = ^TVariable;
  TVariable = class
  private
    fConst: Boolean;

    function GetVal(Index: Integer): TFloat;
    procedure SetVal(Index: Integer; Value: TFloat);
  public
    x, y, z, w: TFloat;

    constructor Create(Constant: Boolean = false; I: Boolean = True); virtual;
    destructor Destroy; override;

    procedure _Set(Value: string);

    property Values[Index: Integer]: TFloat read GetVal write SetVal; default;

    procedure AssignTo(Dest: TVariable);

    property Constant: Boolean read fConst write fConst;
  end;

  TECQPFunction = function(Param1, Param2: TVariable; _R: TVariable): TVariable of object;

  TCompiledExpression = function: TVariable of object;

  TOpType = (opPush, opPop, opCall_P0, opCall_P1, opCall_P2,
             opNOP, opOpen, opClose);

  EWrongFloatSize = class(Exception);

  TVariableStack = class
  private
    fFields: PVariable;
    fSize,
    fTop,
    fSave: Integer;

    fFree: Boolean;

    procedure SetVar(Index: Integer; Src: TVariable);
    function GetVar(Index: Integer): TVariable;
  public
    constructor Create(Size: Integer); virtual;
    destructor Destroy; override;

    procedure Init;

    property Values[Index: Integer]: TVariable read GetVar write SetVar; default;

    procedure AssignTo(Dest: TVariableStack);
    procedure XCHG_TOP;

    property FreeOnDestroy: Boolean read fFree write fFree;

    property Fields: PVariable read fFields;
    property Size: Integer read fSize;
  published
    function Pop: TVariable;
    procedure Push(Value: TVariable);    

    procedure Enter;
    procedure Leave;
  end;
        
implementation

constructor TVariable.Create(Constant: Boolean = false; I: Boolean = True);
begin
  inherited Create;
  fConst := Constant;
end;

destructor TVariable.Destroy;
begin
  inherited;
end;

function TVariable.GetVal(Index: Integer): TFloat;
begin
  case Index of
    0: Result := x;
    1: Result := y;
    2: Result := z;
    3: Result := w;
  else
    Result := 0;
  end;
end;

procedure TVariable.SetVal(Index: Integer; Value: TFloat);
begin
  case Index of
    0: x := Value;
    1: y := Value;
    2: z := Value;
    3: w := Value;
  end;
end;

procedure TVariable.AssignTo(Dest: TVariable);
begin
  Dest.x := x;
  Dest.y := y;
  Dest.z := z;
  Dest.w := w;
  Dest.Constant := Constant;
end;

procedure TVariable._Set(Value: string);
begin
  SetVal(0, StrToFloat(Value));
end;

//=============================================================================

constructor TVariableStack.Create(Size: Integer);
begin
  inherited Create;
  GetMem(fFields, Size*sizeof(TVariable));
  fSize := Size;
  fTop := 0;
  fFree := false;
end;

destructor TVariableStack.Destroy;
var i: Integer;
begin
  if fFree then
    for i := 0 to fSize-1 do
    try
      Values[i].Free;
      Values[i] := nil;
    except
    end;
  FreeMem(fFields);
  inherited;
end;

procedure TVariableStack.SetVar(Index: Integer; Src: TVariable);
begin
  PVariable(Integer(fFields)+Index*sizeof(TVariable))^ := Src;
end;

procedure TVariableStack.AssignTo(Dest: TVariableStack);
begin
  Dest := TVariableStack.Create(fSize);
  Move(fFields^, Dest.fFields^, fSize*sizeof(TVariable));
end;

function TVariableStack.GetVar(Index: Integer): TVariable;
begin
  Result := PVariable(Integer(fFields)+Index*sizeof(TVariable))^;
end;

procedure TVariableStack.Init;
begin
  FillChar(fFields^, fSize*sizeof(TFloat), 0);
  fTop := 0;
end;

function TVariableStack.Pop: TVariable;
begin
  Dec(fTop);
  Result := PVariable(Integer(fFields)+fTop*sizeof(TVariable))^;
end;

procedure TVariableStack.Push(Value: TVariable);
begin
  PVariable(Integer(fFields)+fTop*sizeof(TVariable))^ := Value;
  Inc(fTop);
end;

procedure TVariableStack.XCHG_TOP;
var a: PVariable;
begin
  a := PVariable(Integer(fFields)+(fTop-1)*sizeof(TVariable));
  PVariable(Integer(fFields)+(fTop-1)*sizeof(TVariable))^ :=
    PVariable(Integer(fFields)+(fTop)*sizeof(TVariable))^;
  PVariable(Integer(fFields)+(fTop)*sizeof(TVariable))^ := a^;
end;

procedure TVariableStack.Enter;
begin
  fSave := fTop;
end;

procedure TVariableStack.Leave;
begin
  fTop := fSave;
end;

initialization

end.
