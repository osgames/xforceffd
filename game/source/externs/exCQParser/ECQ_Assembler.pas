unit ECQ_Assembler;

interface

type
  TRegister = (rEAX, rEBX, rECX, rEDX);

  TECQPAssembler = class
  private
    fAssembly : string;
    fCurrent  : Integer;
    fLen      : Integer;
    fChunkSize: Integer;

    procedure Grow;
    procedure PushB(Value: Byte);
    procedure PushI(Value: Integer); overload;
    procedure Inc(By: Integer = 1);

    function bFromr(Reg: TRegister): Byte;

    function GetAssembly: Pointer;
  protected
  public
    constructor Create; virtual;
    destructor Destroy; override;

    procedure Init;

    procedure MOV(Dest: TRegister; Value: Integer); overload;
    procedure MOV(Dest, Value: TRegister); overload;
    procedure MOV_DR(Dest, Src: TRegister);

    procedure SUB(Dest: TRegister; By: Integer);

    procedure CALL(Dest: Pointer);

    procedure RET;

    procedure POP(Dest: TRegister);
    procedure PUSH(Src: TRegister); overload;
    procedure PUSH(Src: Integer); overload;
    procedure PUSH_DR(Src: TRegister);

    procedure X_OR(Src, Dest: TRegister);

    property ChunkSize: Integer read fChunkSize write fChunkSize;
    property Length: Integer read fLen;

    property Assembly: Pointer read GetAssembly;
  end;

implementation

constructor TECQPAssembler.Create;
begin
  inherited Create;
//  fChunkSize := 1024;
  fLen := 0;
//  Initialize(fAssembly);
//  SetLength(fAssembly, fLen);
  fCurrent := 1;
end;

procedure TECQPAssembler.Init;
begin
//  SetLength(fAssembly, 0);
//  Finalize(fAssembly);
//  fLen := fChunkSize;
//  Initialize(fAssembly);
//  SetLength(fAssembly, fLen);
  fAssembly := '';
  fLen := 0;
  fCurrent := 1;
end;

destructor TECQPAssembler.Destroy;
begin
//  SetLength(fAssembly, 0);
//  Finalize(fAssembly);
  inherited;
end;

procedure TECQPAssembler.Grow;
begin
  fLen := fLen + fChunkSize;
//  SetLength(fAssembly, fLen);
end;

procedure TECQPAssembler.Inc(By: Integer = 1);
begin
  fCurrent := fCurrent+By;
end;

procedure TECQPAssembler.PushB(Value: Byte);
begin
  if fLen - fCurrent < sizeof(Value) then
    Grow;

//  Byte(fCurrent^) := Value;
  fAssembly := fAssembly + Chr(Value);
  Inc(sizeof(Byte));
end;

procedure TECQPAssembler.PushI(Value: Integer);
begin
  if fLen - fCurrent < sizeof(Value) then
    Grow;

//  Integer(fCurrent^) := Value;
  fAssembly := fAssembly + '____';
  Move(Value,  fAssembly[fCurrent], sizeof(Value));
  Inc(sizeof(Integer));
end;

procedure TECQPAssembler.MOV(Dest: TRegister; Value: Integer);
begin
  PushB($B8 + bFromr(Dest));
  PushI(Value);
end;

procedure TECQPAssembler.MOV(Dest, Value: TRegister);
begin
  PushB($89);
  PushB(Byte((3 shl 6) or (bFromr(Value) shl 3) or bFromr(Dest)));
end;

procedure TECQPAssembler.CALL(Dest: Pointer);
begin
  PushB($E8);
  PushI(Integer(Dest) - Integer(@fAssembly[fCurrent]) - 4);
end;

procedure TECQPAssembler.RET;
begin
  PushB($C3);
end;

procedure TECQPAssembler.X_OR(Src, Dest: TRegister);
begin
  PushB($31);
  PushB((((3 shl 3) or bFromr(Dest)) shl 3) or bFromr(Src));
end;

procedure TECQPAssembler.POP(Dest: TRegister);
begin
  PushB($58 + bFromr(Dest));
end;

procedure TECQPAssembler.PUSH(Src: TRegister);
begin
  PushB($50 + bFromr(Src));
end;

procedure TECQPAssembler.PUSH(Src: Integer);
begin
  PushB($68);
  PushI(Src);
end;

procedure TECQPAssembler.PUSH_DR(Src: TRegister);
begin
  PushB($FF);
  PushB((((0 shl 3) or 6) shl 3) or bFromr(Src));
end;

procedure TECQPAssembler.SUB(Dest: TRegister; By: Integer);
begin
  PushB($83);
  PushB((((3 shl 3) or 5) shl 3) or bFromr(Dest));
  PushB(By);
end;

procedure TECQPAssembler.MOV_DR(Dest, Src: TRegister);
begin
  PushB($8B);
  PushB((((0 shl 3) or bFromr(Dest)) shl 3) or bFromr(Src));
end;

function TECQPAssembler.bFromr(Reg: TRegister): Byte;
begin
  Result := 255;
  case Reg of
    rEAX: Result := 0;
    rEBX: Result := 3;
    rECX: Result := 1;
    rEDX: Result := 2;
  end;
end;

function TECQPAssembler.GetAssembly: Pointer;
begin
  Result := @fAssembly[1];
end;

end.
