unit ExCQP_Predefs;

interface

uses
  ExCQParser, ECQ_Func, ECQ_Def, ECQ_Ops, Math;

type
  TOperator_Add = class(TFunction)
  published
    class function Float(Param1, Param2: TVariable; _R: TVariable): TVariable; override;
    class function Complex(Param1, Param2: TVariable; _R: TVariable): TVariable; override;
    class function Quad(Param1, Param2: TVariable; _R: TVariable): TVariable; override;

    class function Supports(Mode: TParserMode): Boolean; override;
  end;

  TOperator_Sub = class(TFunction)
  published
    class function Float(Param1, Param2: TVariable; _R: TVariable): TVariable; override;
    class function Complex(Param1, Param2: TVariable; _R: TVariable): TVariable; override;
    class function Quad(Param1, Param2: TVariable; _R: TVariable): TVariable; override;

    class function Supports(Mode: TParserMode): Boolean; override;
  end;

  TOperator_Neg = class(TFunction)
  published
    class function Float(Param1, Param2: TVariable; _R: TVariable): TVariable; override;
    class function Complex(Param1, Param2: TVariable; _R: TVariable): TVariable; override;
    class function Quad(Param1, Param2: TVariable; _R: TVariable): TVariable; override;

    class function Supports(Mode: TParserMode): Boolean; override;
  end;

  TOperator_Mul = class(TFunction)
  published
    class function Float(Param1, Param2: TVariable; _R: TVariable): TVariable; override;
    class function Complex(Param1, Param2: TVariable; _R: TVariable): TVariable; override;
    class function Quad(Param1, Param2: TVariable; _R: TVariable): TVariable; override;

    class function Supports(Mode: TParserMode): Boolean; override;
  end;

  TOperator_Div = class(TFunction)
  published
    class function Float(Param1, Param2: TVariable; _R: TVariable): TVariable; override;
    class function Complex(Param1, Param2: TVariable; _R: TVariable): TVariable; override;
    class function Quad(Param1, Param2: TVariable; _R: TVariable): TVariable; override;

    class function Supports(Mode: TParserMode): Boolean; override;
  end;

  TOperator_Power = class(TFunction)
  published
    class function Float(Param1, Param2: TVariable; _R: TVariable): TVariable; override;
    class function Complex(Param1, Param2: TVariable; _R: TVariable): TVariable; override;
    class function Quad(Param1, Param2: TVariable; _R: TVariable): TVariable; override;

    class function Supports(Mode: TParserMode): Boolean; override;
  end;

  TFunction_Ln = class(TFunction)
  published
    class function Float(Param1, Param2: TVariable; _R: TVariable): TVariable; override;
    class function Complex(Param1, Param2: TVariable; _R: TVariable): TVariable; override;
    class function Quad(Param1, Param2: TVariable; _R: TVariable): TVariable; override;

    class function Supports(Mode: TParserMode): Boolean; override;

    class function ParamCount: Integer; override;
  end;

  TFunction_Sqrt = class(TFunction)
  published
    class function Float(Param1, Param2: TVariable; _R: TVariable): TVariable; override;
    class function Complex(Param1, Param2: TVariable; _R: TVariable): TVariable; override;
    class function Quad(Param1, Param2: TVariable; _R: TVariable): TVariable; override;

    class function Supports(Mode: TParserMode): Boolean; override;

    class function ParamCount: Integer; override;
  end;

  TFunction_Sqr = class(TFunction)
  published
    class function Float(Param1, Param2: TVariable; _R: TVariable): TVariable; override;
    class function Complex(Param1, Param2: TVariable; _R: TVariable): TVariable; override;
    class function Quad(Param1, Param2: TVariable; _R: TVariable): TVariable; override;

    class function Supports(Mode: TParserMode): Boolean; override;

    class function ParamCount: Integer; override;
  end;

  TFunction_Sinh = class(TFunction)
  published
    class function Float(Param1, Param2: TVariable; _R: TVariable): TVariable; override;
    class function Complex(Param1, Param2: TVariable; _R: TVariable): TVariable; override;
    class function Quad(Param1, Param2: TVariable; _R: TVariable): TVariable; override;

    class function Supports(Mode: TParserMode): Boolean; override;

    class function ParamCount: Integer; override;
  end;

  TFunction_Sin = class(TFunction)
  published
    class function Float(Param1, Param2: TVariable; _R: TVariable): TVariable; override;
    class function Complex(Param1, Param2: TVariable; _R: TVariable): TVariable; override;
    class function Quad(Param1, Param2: TVariable; _R: TVariable): TVariable; override;

    class function Supports(Mode: TParserMode): Boolean; override;

    class function ParamCount: Integer; override;
  end;

  TFunction_Cosh = class(TFunction)
  published
    class function Float(Param1, Param2: TVariable; _R: TVariable): TVariable; override;
    class function Complex(Param1, Param2: TVariable; _R: TVariable): TVariable; override;
    class function Quad(Param1, Param2: TVariable; _R: TVariable): TVariable; override;

    class function Supports(Mode: TParserMode): Boolean; override;

    class function ParamCount: Integer; override;
  end;

  TFunction_Cos = class(TFunction)
  published
    class function Float(Param1, Param2: TVariable; _R: TVariable): TVariable; override;
    class function Complex(Param1, Param2: TVariable; _R: TVariable): TVariable; override;
    class function Quad(Param1, Param2: TVariable; _R: TVariable): TVariable; override;

    class function Supports(Mode: TParserMode): Boolean; override;

    class function ParamCount: Integer; override;
  end;

  TFunction_Tanh = class(TFunction)
  published
    class function Float(Param1, Param2: TVariable; _R: TVariable): TVariable; override;
    class function Complex(Param1, Param2: TVariable; _R: TVariable): TVariable; override;
    class function Quad(Param1, Param2: TVariable; _R: TVariable): TVariable; override;

    class function Supports(Mode: TParserMode): Boolean; override;

    class function ParamCount: Integer; override;
  end;

  TFunction_Tan = class(TFunction)
  published
    class function Float(Param1, Param2: TVariable; _R: TVariable): TVariable; override;
    class function Complex(Param1, Param2: TVariable; _R: TVariable): TVariable; override;
    class function Quad(Param1, Param2: TVariable; _R: TVariable): TVariable; override;

    class function Supports(Mode: TParserMode): Boolean; override;

    class function ParamCount: Integer; override;
  end;

  TFunction_Cotanh = class(TFunction)
  published
    class function Float(Param1, Param2: TVariable; _R: TVariable): TVariable; override;
    class function Complex(Param1, Param2: TVariable; _R: TVariable): TVariable; override;
    class function Quad(Param1, Param2: TVariable; _R: TVariable): TVariable; override;

    class function Supports(Mode: TParserMode): Boolean; override;

    class function ParamCount: Integer; override;
  end;

  TFunction_Cotan = class(TFunction)
  published
    class function Float(Param1, Param2: TVariable; _R: TVariable): TVariable; override;
    class function Complex(Param1, Param2: TVariable; _R: TVariable): TVariable; override;
    class function Quad(Param1, Param2: TVariable; _R: TVariable): TVariable; override;

    class function Supports(Mode: TParserMode): Boolean; override;

    class function ParamCount: Integer; override;
  end;

  TFunction_Abs = class(TFunction)
  published
    class function Float(Param1, Param2: TVariable; _R: TVariable): TVariable; override;
    class function Complex(Param1, Param2: TVariable; _R: TVariable): TVariable; override;
    class function Quad(Param1, Param2: TVariable; _R: TVariable): TVariable; override;

    class function Supports(Mode: TParserMode): Boolean; override;

    class function ParamCount: Integer; override;
  end;

  TFunction_Exp = class(TFunction)
  published
    class function Float(Param1, Param2: TVariable; _R: TVariable): TVariable; override;
    class function Complex(Param1, Param2: TVariable; _R: TVariable): TVariable; override;
    class function Quad(Param1, Param2: TVariable; _R: TVariable): TVariable; override;

    class function Supports(Mode: TParserMode): Boolean; override;

    class function ParamCount: Integer; override;
  end;

  TFunction_Frac = class(TFunction)
  published
    class function Float(Param1, Param2: TVariable; _R: TVariable): TVariable; override;
    class function Complex(Param1, Param2: TVariable; _R: TVariable): TVariable; override;
    class function Quad(Param1, Param2: TVariable; _R: TVariable): TVariable; override;

    class function Supports(Mode: TParserMode): Boolean; override;

    class function ParamCount: Integer; override;
  end;

  TFunction_Int = class(TFunction)
  published
    class function Float(Param1, Param2: TVariable; _R: TVariable): TVariable; override;
    class function Complex(Param1, Param2: TVariable; _R: TVariable): TVariable; override;
    class function Quad(Param1, Param2: TVariable; _R: TVariable): TVariable; override;

    class function Supports(Mode: TParserMode): Boolean; override;

    class function ParamCount: Integer; override;
  end;

  TFunction_Round = class(TFunction)
  published
    class function Float(Param1, Param2: TVariable; _R: TVariable): TVariable; override;
    class function Complex(Param1, Param2: TVariable; _R: TVariable): TVariable; override;
    class function Quad(Param1, Param2: TVariable; _R: TVariable): TVariable; override;

    class function Supports(Mode: TParserMode): Boolean; override;

    class function ParamCount: Integer; override;
  end;

  TFunction_Arcsinh = class(TFunction)
  published
    class function Float(Param1, Param2: TVariable; _R: TVariable): TVariable; override;
    class function Complex(Param1, Param2: TVariable; _R: TVariable): TVariable; override;
    class function Quad(Param1, Param2: TVariable; _R: TVariable): TVariable; override;

    class function Supports(Mode: TParserMode): Boolean; override;

    class function ParamCount: Integer; override;
  end;

  TFunction_Arccosh = class(TFunction)
  published
    class function Float(Param1, Param2: TVariable; _R: TVariable): TVariable; override;
    class function Complex(Param1, Param2: TVariable; _R: TVariable): TVariable; override;
    class function Quad(Param1, Param2: TVariable; _R: TVariable): TVariable; override;

    class function Supports(Mode: TParserMode): Boolean; override;

    class function ParamCount: Integer; override;
  end;

  TFunction_Arctanh = class(TFunction)
  published
    class function Float(Param1, Param2: TVariable; _R: TVariable): TVariable; override;
    class function Complex(Param1, Param2: TVariable; _R: TVariable): TVariable; override;
    class function Quad(Param1, Param2: TVariable; _R: TVariable): TVariable; override;

    class function Supports(Mode: TParserMode): Boolean; override;

    class function ParamCount: Integer; override;
  end;

  TFunction_Arcsin = class(TFunction)
  published
    class function Float(Param1, Param2: TVariable; _R: TVariable): TVariable; override;
    class function Complex(Param1, Param2: TVariable; _R: TVariable): TVariable; override;
    class function Quad(Param1, Param2: TVariable; _R: TVariable): TVariable; override;

    class function Supports(Mode: TParserMode): Boolean; override;

    class function ParamCount: Integer; override;
  end;

  TFunction_Arccos = class(TFunction)
  published
    class function Float(Param1, Param2: TVariable; _R: TVariable): TVariable; override;
    class function Complex(Param1, Param2: TVariable; _R: TVariable): TVariable; override;
    class function Quad(Param1, Param2: TVariable; _R: TVariable): TVariable; override;

    class function Supports(Mode: TParserMode): Boolean; override;

    class function ParamCount: Integer; override;
  end;

  TFunction_Arctan = class(TFunction)
  published
    class function Float(Param1, Param2: TVariable; _R: TVariable): TVariable; override;
    class function Complex(Param1, Param2: TVariable; _R: TVariable): TVariable; override;
    class function Quad(Param1, Param2: TVariable; _R: TVariable): TVariable; override;

    class function Supports(Mode: TParserMode): Boolean; override;

    class function ParamCount: Integer; override;
  end;

procedure RegisterTo(Dest: TExCQParser); 

procedure Quat2Comp(const Q : TVariable; var C1, C2 : TVariable);
procedure Comp2Quat(const C1, C2 : TVariable; var Q : TVariable);

function konjC   (const C : TVariable) : TVariable;
function pol2recC(const C : TVariable) : TVariable;
function rec2polC(const C : TVariable) : TVariable;
function argC    (const C : TVariable) : TVariable;
function absC    (const C : TVariable) : TVariable;

implementation

type
  TComplex = record
    x, y: double;
  end;

  TQuat = record
    x, y, z, w: Double;
  end;

function Sign(Value: double) : Integer;
begin
  if Value=0 then
    result:=0
  else if Value<0 then
    result:=-1
  else if Value>0 then
    result:=1;
end;
procedure RegisterTo(Dest: TExCQParser);
begin
  Dest.AddOperator(TOperator_Add, '+', otBinary, 1);
  Dest.AddOperator(TOperator_Sub, '-', otBinary, 1);
  Dest.AddOperator(TFunction_Abs, '+', otUnaryPrefix, 1, False, True);
  Dest.AddOperator(TOperator_Neg, '-', otUnaryPrefix, 1, False, True);
  Dest.AddOperator(TOperator_Mul, '*', otBinary, 2, True);
  Dest.AddOperator(TOperator_Div, '/', otBinary, 2);
  Dest.AddOperator(TOperator_Power, '^', otBinary, 3);

  Dest.AddFunction(TFunction_Ln, 'ln');
  Dest.AddFunction(TFunction_Exp, 'exp');
  Dest.AddFunction(TFunction_Sqrt, 'sqrt');
  Dest.AddFunction(TFunction_Sqr, 'sqr');
  Dest.AddFunction(TFunction_Sinh, 'sinh');
  Dest.AddFunction(TFunction_Sin, 'sin');
  Dest.AddFunction(TFunction_Cosh, 'cosh');
  Dest.AddFunction(TFunction_Cos, 'cos');
  Dest.AddFunction(TFunction_Tanh, 'tanh');
  Dest.AddFunction(TFunction_Tan, 'tan');
  Dest.AddFunction(TFunction_Cotanh, 'cotanh');
  Dest.AddFunction(TFunction_Cotan, 'cotan');
  Dest.AddFunction(TFunction_Abs, 'abs');
  Dest.AddFunction(TFunction_Frac, 'frac');
  Dest.AddFunction(TFunction_Int, 'int');
  Dest.AddFunction(TFunction_Round, 'round');
  Dest.AddFunction(TFunction_Arcsinh, 'arcsinh');
  Dest.AddFunction(TFunction_Arccosh, 'arccosh');
  Dest.AddFunction(TFunction_Arcsin, 'arcsin');
  Dest.AddFunction(TFunction_Arccos, 'arccos');
  Dest.AddFunction(TFunction_Arctan, 'arctan');

  Dest.RegisterVariable('pi', [pi], True);
  Dest.RegisterVariable('e', [Exp(1)], True);
  Dest.RegisterVariable('i', [0, 1], True);
end;

// ---- Misc -------------------------------------------------------------------

procedure Quat2Comp(const Q : TVariable; var C1, C2 : TVariable);
begin
  C1.x := Q.x - Q.w;
  C1.y := Q.y + Q.z;

  C2.x := Q.x + Q.w;
  C2.y := Q.y - Q.z;
end;

procedure Comp2Quat(const C1, C2 : TVariable; var Q : TVariable);
begin
  Q.x := (C1.x + C2.x) * 0.5;
  Q.y := (C1.y + C2.y) * 0.5;
  Q.z := (C1.y - C2.y) * 0.5;
  Q.w := (C2.x - C1.x) * 0.5;
end;

function konjC(const C : TVariable) : TVariable;
begin
  Result := TVariable.Create(True);
  result.x :=  C.x;
  result.y := -C.y;
end;

function pol2recC(const C : TVariable): TVariable;
var
  sn, cs : Extended;
begin
  Result := TVariable.Create(True);
  sincos(C.y, cs, sn);
  result.x := C.x * cs;
  result.y := C.x * sn;
end;

function rec2polC(const C : TVariable) : TVariable;
const PIhalbe : double = pi / 2;
begin
  Result := TVariable.Create(True);
  C.AssignTo(Result);

  result.x := sqrt(sqr(result.x) + sqr(result.y));

  if result.x = 0 then
  begin
    result.y := -PIhalbe;
  end
  else
  begin
    result.y := arctan(result.y / result.x);
    if (result.x < 0)then result.y := result.y + pi;
  end;
end;

function argC(const C : TVariable) : TVariable;
var temp : double;
begin
  Result := TVariable.Create(True);
  result.x := 0;
  temp     := 0;
  if C.x <> 0 then
    temp := ArcTan(Abs(C.y / C.x))
  else
  if C.y <> 0 then
    result.x := pi / 2 * Sign(C.y); //  C.y / Abs(C.y);

  if C.y = 0 then
    if C.x > 0 then
      result.x := 0
    else
      result.x := pi;

  if C.x > 0 then
  begin
    if C.y > 0 then
      result.x :=  temp
    else
    if C.y < 0 then
      result.x := -temp;
  end
else
  if C.x < 0 then
  begin
    if C.y > 0 then
      result.x := pi - temp
    else
    if C.y < 0 then
      result.x := temp - pi;
  end
else
  if C.y = 0 then result.x := 0;

//  result.y := 0;
end;

function _argC(const C : TVariable) : double; overload;
var temp : double;
const pi2: double = pi / 2;
begin
//  Result := _argC;
  result := 0;
  temp     := 0;
  if C.x <> 0 then
    temp := ArcTan(Abs(C.y / C.x))
  else
  if C.y <> 0 then
    result := pi2 * Sign(C.y) //  C.y / Abs(C.y);
else
  if C.y = 0 then
    if C.x > 0 then
      result := 0
    else
      result := pi;

  if C.x > 0 then
  begin
    if C.y > 0 then
      result :=  temp
    else
    if C.y < 0 then
      result := -temp;
  end
else
  if C.x < 0 then
  begin
    if C.y > 0 then
      result := pi - temp
    else
    if C.y < 0 then
      result := temp - pi;
  end
else
  if C.y = 0 then result := 0;

//  result.y := 0;
end;

function _argC(const C : TComplex) : double; overload;
var temp : double;
const pi2: double = pi / 2;
begin
  result := 0;
  temp     := 0;
  if C.x <> 0 then
    temp := ArcTan(Abs(C.y / C.x))
  else
  if C.y <> 0 then
    result := pi2 * Sign(C.y) //  C.y / Abs(C.y);
else
  if C.y = 0 then
    if C.x > 0 then
      result := 0
    else
      result := pi;

  if C.x > 0 then
  begin
    if C.y > 0 then
      result :=  temp
    else
    if C.y < 0 then
      result := -temp;
  end
else
  if C.x < 0 then
  begin
    if C.y > 0 then
      result := pi - temp
    else
    if C.y < 0 then
      result := temp - pi;
  end
else
  if C.y = 0 then result := 0;

//  result.y := 0;
end;

function absC(const C : TVariable) : TVariable;
begin
  Result := TVariable.Create(True);
  result.x := sqrt(sqr(C.x)+sqr(C.y));
  result.y := 0;
end;

procedure SinhCosh(const X: Extended; var sh, ch: Extended);
var inv : double;
begin
  ch  := exp(X) * 0.5;
  inv := 0.25 / ch;
  sh := ch - inv;
  ch := ch + inv;
end;

// ---- Addition ---------------------------------------------------------------

class function TOperator_Add.Float(Param1, Param2: TVariable; _R: TVariable): TVariable;
begin
  Result := _R;
  Result.x := Param1.x + Param2.x;
end;

class function TOperator_Add.Complex(Param1, Param2: TVariable; _R: TVariable): TVariable;
begin
  Result := _R;
  Result.x := Param1.x + Param2.x;
  Result.y := Param1.y + Param2.y;
end;

class function TOperator_Add.Quad(Param1, Param2: TVariable; _R: TVariable): TVariable;
begin
  Result := _R;
  result.x := Param1.x + Param2.x;
  result.y := Param1.y + Param2.y;
  result.z := Param1.z + Param2.z;
  result.w := Param1.w + Param2.w;
end;

class function TOperator_Add.Supports(Mode: TParserMode): Boolean;
begin
  Result := True;
end;

// ---- Substraction -----------------------------------------------------------

class function TOperator_Sub.Float(Param1, Param2: TVariable; _R: TVariable): TVariable;
begin
  Result := _R; 
  Result.x := Param1.x - Param2.x;
end;

class function TOperator_Sub.Complex(Param1, Param2: TVariable; _R: TVariable): TVariable;
begin
  Result := _R;
  Result.x := Param1.x - Param2.x;
  Result.y := Param1.y - Param2.y;
end;

class function TOperator_Sub.Quad(Param1, Param2: TVariable; _R: TVariable): TVariable;
begin
  Result := _R;
  result.x := Param1.x - Param2.x;
  result.y := Param1.y - Param2.y;
  result.z := Param1.z - Param2.z;
  result.w := Param1.w - Param2.w;
end;

class function TOperator_Sub.Supports(Mode: TParserMode): Boolean;
begin
  Result := True;
end;

// ---- Unary - ----------------------------------------------------------------

class function TOperator_Neg.Float(Param1, Param2: TVariable; _R: TVariable): TVariable;
begin
  Result := _R;
  Result.x := -Param1.x;
end;

class function TOperator_Neg.Complex(Param1, Param2: TVariable; _R: TVariable): TVariable;
begin
  Result := _R;
  Result.x := -Param1.x;
  Result.y := -Param1.y;
end;

class function TOperator_Neg.Quad(Param1, Param2: TVariable; _R: TVariable): TVariable;
begin
  Result := _R;
  Result.x := -Param1.x;
  Result.y := -Param1.y;
  Result.z := -Param1.z;
  Result.w := -Param1.w;
end;

class function TOperator_Neg.Supports(Mode: TParserMode): Boolean;
begin
  Result := True;
end;

// ---- Multiplication ---------------------------------------------------------

class function TOperator_Mul.Float(Param1, Param2: TVariable; _R: TVariable): TVariable;
begin
  Result := _R;   
  Result.x := Param1.x * Param2.x;
end;

class function TOperator_Mul.Complex(Param1, Param2: TVariable; _R: TVariable): TVariable;
begin
  Result := _R;
  result.x := Param1.x*Param2.x - Param1.y*Param2.y;
  result.y := Param1.y*Param2.x + Param1.x*Param2.y;
end;

class function TOperator_Mul.Quad(Param1, Param2: TVariable; _R: TVariable): TVariable;
begin
  Result := _R;  
  result.x := Param1.x*Param2.x - Param1.y*Param2.y - Param1.z*Param2.z - Param1.w*Param2.w;
  result.y := Param1.y*Param2.x + Param1.x*Param2.y + Param1.w*Param2.z - Param1.z*Param2.w;
  result.z := Param1.z*Param2.x - Param1.w*Param2.y + Param1.x*Param2.z + Param1.y*Param2.w;
  result.w := Param1.w*Param2.x + Param1.z*Param2.y - Param1.y*Param2.z + Param1.x*Param2.w;
end;

class function TOperator_Mul.Supports(Mode: TParserMode): Boolean;
begin
  Result := True;
end;

// ---- Division ---------------------------------------------------------------

class function TOperator_Div.Float(Param1, Param2: TVariable; _R: TVariable): TVariable;
begin
  Result := _R;   
  Result.x := Param1.x / Param2.x;
end;

class function TOperator_Div.Complex(Param1, Param2: TVariable; _R: TVariable): TVariable;
begin
  Result := _R;
  if Param2.x = 0 then
  begin
    result.x := Param1.x;
    result.y := Param1.y/Param2.x - Param1.x/Param2.y;
  end
  else
  begin
    if Param2.y = 0 then
    begin
      result.x := Param1.x/Param2.x;
      result.y := Param1.y;
    end
    else
    begin
      result.x := Param1.x/Param2.x + Param1.y/Param2.y;
      result.y := Param1.y/Param2.x - Param1.x/Param2.y;
    end;
  end;
end;

class function TOperator_Div.Quad(Param1, Param2: TVariable; _R: TVariable): TVariable;
var xw, yz: Double;
begin
  Result := _R;   
  xw := sqr(Param2.x)-sqr(Param2.w);
  yz := sqr(Param2.y)-sqr(Param2.z);
  result.x := (((Param1.x*Param2.x)-(Param1.w*Param2.w)) / xw) +
              (((Param1.y*Param2.y)-(Param1.z*Param2.z)) / yz);
  result.y := (((Param1.y*Param2.x)+(Param1.z*Param2.w)) / xw) -
              (((Param1.x*Param2.y)+(Param1.w*Param2.z)) / yz);
  result.z := (((Param1.x*Param2.z)+(Param1.w*Param2.y)) / yz) +
              (((Param1.y*Param2.w)+(Param1.z*Param2.x)) / xw);
  result.w := (((Param1.y*Param2.z)-(Param1.z*Param2.y)) / yz) -
              (((Param1.x*Param2.w)-(Param1.w*Param2.x)) / yz);
end;

class function TOperator_Div.Supports(Mode: TParserMode): Boolean;
begin
  Result := True;
end;

// ---- Power(X) ---------------------------------------------------------------

class function TOperator_Power.Float(Param1, Param2: TVariable; _R: TVariable): TVariable;
begin
  Result := _R;
  Result.x := Power(Param1.x, Param2.x);
end;

class function TOperator_Power.Complex(Param1, Param2: TVariable; _R: TVariable): TVariable;   
var
  sn, cs, t: Extended;
begin
  Result := _R;
  sincos(Param2.x*_argC(Param1), sn, cs);
  t := power(sqrt(sqr(Param1.x)+sqr(Param1.y)), Param2.x);
  result.x := t * cs;
  result.y := t * sn;
end;

class function TOperator_Power.Quad(Param1, Param2: TVariable; _R: TVariable): TVariable;  
var c1, c2, c3, c4, _t: tcomplex;            
var
  sn, cs, t: Extended;
begin
  Result := _R;
//  Quat2Comp(Param1, C1, C2);
   
  C1.x := Param1.x - Param1.w;
  C1.y := Param1.y + Param1.z;

  C2.x := Param1.x + Param1.w;
  C2.y := Param1.y - Param1.z;


  _t.x := Param2.x;
  _t.y := 0;
//  C3 := TOperator_Power.Complex(C1, _t);

    sincos(_t.x*_argC(C1), sn, cs);
    t := power(sqrt(sqr(C1.x)+sqr(C1.y)), _t.x);
    C3.x := t * cs;
    C3.y := t * sn;

//  C4 := TOperator_Power.Complex(C2, _t);

    sincos(_t.x*_argC(C2), sn, cs);
    t := power(sqrt(sqr(C2.x)+sqr(C2.y)), _t.x);
    C4.x := t * cs;
    C4.y := t * sn;

//  Comp2Quat(C3, C4, result);
  result.x := (C3.x + C4.x) * 0.5;
  result.y := (C3.y + C4.y) * 0.5;
  result.z := (C3.y - C4.y) * 0.5;
  result.w := (C4.x - C3.x) * 0.5;
end;

class function TOperator_Power.Supports(Mode: TParserMode): Boolean;
begin
  Result := True;
end;

// ---- Ln(X) ------------------------------------------------------------------

class function TFunction_Ln.Float(Param1, Param2: TVariable; _R: TVariable): TVariable;
begin
  Result := _R;
  Result.x := Ln(Param1.x);
end;

class function TFunction_Ln.Complex(Param1, Param2: TVariable; _R: TVariable): TVariable;
begin
  Result := _R;
  try
    result.x := ln(sqrt(sqr(Param1.x)+sqr(Param1.y)));
    result.y := _argC(Param1);
  except
    result.x := 0;
    result.y := 0;
  end;
end;

class function TFunction_Ln.Quad(Param1, Param2: TVariable; _R: TVariable): TVariable;  
var c1, c2, c3, c4: tcomplex;
begin
  Result := _R; 
//  Quat2Comp(Param1, C1, C2);

  C1.x := Param1.x - Param1.w;
  C1.y := Param1.y + Param1.z;

  C2.x := Param1.x + Param1.w;
  C2.y := Param1.y - Param1.z;


//  C3 := TFunction_Ln.Complex(C1, nil);

    try
      C3.x := ln(sqrt(sqr(C1.x)+sqr(C1.y)));
      C3.y := _argC(C1);
    except
      C3.x := 0;
      C3.y := 0;
    end;

//  C4 := TFunction_Ln.Complex(C2, nil);

    try
      C4.x := ln(sqrt(sqr(C2.x)+sqr(C2.y)));
      C4.y := _argC(C2);
    except
      C4.x := 0;
      C4.y := 0;
    end;

//  Comp2Quat(C3, C4, result);
  result.x := (C3.x + C4.x) * 0.5;
  result.y := (C3.y + C4.y) * 0.5;
  result.z := (C3.y - C4.y) * 0.5;
  result.w := (C4.x - C3.x) * 0.5;
end;

class function TFunction_Ln.Supports(Mode: TParserMode): Boolean;
begin
  Result := True;
end;

class function TFunction_Ln.ParamCount: Integer;
begin
  Result := 1;
end;

// ---- Sqrt(X) ----------------------------------------------------------------

class function TFunction_Sqrt.Float(Param1, Param2: TVariable; _R: TVariable): TVariable;
begin
  Result := _R;   
  Result.x := Sqrt(Param1.x);
end;

class function TFunction_Sqrt.Complex(Param1, Param2: TVariable; _R: TVariable): TVariable;
var
  sn, cs, t: Extended;
begin
  Result := _R;
  sincos(0.5*Sqrt(Sqr(Param1.x) + Sqr(Param1.y)), sn, cs);
  t := sqrt(Sqrt(Sqr(Param1.x) + Sqr(Param1.y)));
  result.x := t * cs;
  result.y := t * sn;
end;

class function TFunction_Sqrt.Quad(Param1, Param2: TVariable; _R: TVariable): TVariable;    
var c1, c2, c3, c4: tcomplex;
var
  sn, cs, t: Extended;
begin
  Result := _R; 
//  Quat2Comp(Param1, C1, C2);

  C1.x := Param1.x - Param1.w;
  C1.y := Param1.y + Param1.z;

  C2.x := Param1.x + Param1.w;
  C2.y := Param1.y - Param1.z;


//  C3 := TFunction_Sqrt.Complex(C1, nil);

    sincos(0.5*Sqrt(Sqr(C1.x) + Sqr(C1.y)), sn, cs);
    t := sqrt(Sqrt(Sqr(C1.x) + Sqr(C1.y)));
    C3.x := t * cs;
    C3.y := t * sn;

//  C4 := TFunction_Sqrt.Complex(C2, nil);

    sincos(0.5*Sqrt(Sqr(C2.x) + Sqr(C2.y)), sn, cs);
    t := sqrt(Sqrt(Sqr(C2.x) + Sqr(C2.y)));
    C4.x := t * cs;
    C4.y := t * sn;

//  Comp2Quat(C3, C4, result);
  result.x := (C3.x + C4.x) * 0.5;
  result.y := (C3.y + C4.y) * 0.5;
  result.z := (C3.y - C4.y) * 0.5;
  result.w := (C4.x - C3.x) * 0.5;
end;

class function TFunction_Sqrt.Supports(Mode: TParserMode): Boolean;
begin
  Result := True;
end;

class function TFunction_Sqrt.ParamCount: Integer;
begin
  Result := 1;
end;

// ---- Sqr(X) -----------------------------------------------------------------

class function TFunction_Sqr.Float(Param1, Param2: TVariable; _R: TVariable): TVariable;
begin
  Result := _R;  
  Result.x := Sqr(Param1.x);
end;

class function TFunction_Sqr.Complex(Param1, Param2: TVariable; _R: TVariable): TVariable;
begin
  Result := _R;
  result.x := sqr(Param1.x)-sqr(Param1.y);
  result.y := 2*Param1.x*Param1.y;
end;

class function TFunction_Sqr.Quad(Param1, Param2: TVariable; _R: TVariable): TVariable;
begin
  Result := _R;  
  result.x := sqr(Param1.x) - sqr(Param1.y) - sqr(Param1.z) - sqr(Param1.w);
  result.y := Param1.x*Param1.y * 2;
  result.z := Param1.z*Param1.x * 2;
  result.w := Param1.w*Param1.x * 2;
end;

class function TFunction_Sqr.Supports(Mode: TParserMode): Boolean;
begin
  Result := True;
end;

class function TFunction_Sqr.ParamCount: Integer;
begin
  Result := 1;
end;

// ---- Sinh(X) ----------------------------------------------------------------

class function TFunction_Sinh.Float(Param1, Param2: TVariable; _R: TVariable): TVariable;
begin
  Result := _R;  
  Result.x := Sinh(Param1.x);
end;

class function TFunction_Sinh.Complex(Param1, Param2: TVariable; _R: TVariable): TVariable;
var
  sn, cs : Extended;
  sh, ch, inv: Extended;
begin
  Result := _R;
  sincos(Param1.y, sn, cs);
//  sinhcosh(Param1.x, sh, ch);

    ch  := exp(Param1.x) * 0.5;
    inv := 0.25 / ch;
    sh := ch - inv;
    ch := ch + inv;

  result.x := sh * cs;
  result.y := ch * sn;
end;

class function TFunction_Sinh.Quad(Param1, Param2: TVariable; _R: TVariable): TVariable;   
var c1, c2, c3, c4: tcomplex;   
var
  sn, cs : Extended;
  sh, ch, inv: Extended;
begin
  Result := _R;  
//  Quat2Comp(Param1, C1, C2);

  C1.x := Param1.x - Param1.w;
  C1.y := Param1.y + Param1.z;

  C2.x := Param1.x + Param1.w;
  C2.y := Param1.y - Param1.z;


//  C3 := TFunction_Sinh.Complex(C1, nil);

    sincos(C1.y, sn, cs);
//    sinhcosh(C1.x, sh, ch);

      ch  := exp(C1.x) * 0.5;
      inv := 0.25 / ch;
      sh := ch - inv;
      ch := ch + inv;

    C3.x := sh * cs;
    C3.y := ch * sn;

//  C4 := TFunction_Sinh.Complex(C2, nil);

    sincos(C2.y, sn, cs);
//    sinhcosh(C2.x, sh, ch);

      ch  := exp(C2.x) * 0.5;
      inv := 0.25 / ch;
      sh := ch - inv;
      ch := ch + inv;

    C4.x := sh * cs;
    C4.y := ch * sn;

//  Comp2Quat(C3, C4, result);
  result.x := (C3.x + C4.x) * 0.5;
  result.y := (C3.y + C4.y) * 0.5;
  result.z := (C3.y - C4.y) * 0.5;
  result.w := (C4.x - C3.x) * 0.5;
end;

class function TFunction_Sinh.Supports(Mode: TParserMode): Boolean;
begin
  Result := True;
end;

class function TFunction_Sinh.ParamCount: Integer;
begin
  Result := 1;
end;

// ---- Sin(X) -----------------------------------------------------------------

class function TFunction_Sin.Float(Param1, Param2: TVariable; _R: TVariable): TVariable;
begin
  Result := _R;  
  Result.x := Sin(Param1.x);
end;

class function TFunction_Sin.Complex(Param1, Param2: TVariable; _R: TVariable): TVariable; 
var
  sn, cs : Extended;
  sh, ch, inv: Extended;
begin
  Result := _R;
  sincos(Param1.x, sn, cs);
//  sinhcosh(Param1.y, sh, ch);

    ch  := exp(Param1.y) * 0.5;
    inv := 0.25 / ch;
    sh := ch - inv;
    ch := ch + inv;

  result.x := sn*ch;
  result.y := cs*sh;
end;

class function TFunction_Sin.Quad(Param1, Param2: TVariable; _R: TVariable): TVariable; 
var c1, c2, c3, c4: tcomplex;   
var
  sn, cs : Extended;
  sh, ch, inv: Extended;
begin
  Result := _R;
//  Quat2Comp(Param1, C1, C2);

  C1.x := Param1.x - Param1.w;
  C1.y := Param1.y + Param1.z;

  C2.x := Param1.x + Param1.w;
  C2.y := Param1.y - Param1.z;


//  C3 := TFunction_Sin.Complex(C1, nil);

    sincos(C1.x, sn, cs);
//    sinhcosh(C1.y, sh, ch);

      ch  := exp(C1.y) * 0.5;
      inv := 0.25 / ch;
      sh := ch - inv;
      ch := ch + inv;

    C3.x := sn*ch;
    C3.y := cs*sh;

//  C4 := TFunction_Sin.Complex(C2, nil);

    sincos(C2.x, sn, cs);
//    sinhcosh(C2.y, sh, ch);

      ch  := exp(C2.y) * 0.5;
      inv := 0.25 / ch;
      sh := ch - inv;
      ch := ch + inv;

    C4.x := sn*ch;
    C4.y := cs*sh;

//  Comp2Quat(C3, C4, result);
  result.x := (C3.x + C4.x) * 0.5;
  result.y := (C3.y + C4.y) * 0.5;
  result.z := (C3.y - C4.y) * 0.5;
  result.w := (C4.x - C3.x) * 0.5;
end;

class function TFunction_Sin.Supports(Mode: TParserMode): Boolean;
begin
  Result := True;
end;

class function TFunction_Sin.ParamCount: Integer;
begin
  Result := 1;
end;

// ---- Cosh(X) ----------------------------------------------------------------

class function TFunction_Cosh.Float(Param1, Param2: TVariable; _R: TVariable): TVariable;
begin
  Result := _R;  
  Result.x := Cosh(Param1.x);
end;

class function TFunction_Cosh.Complex(Param1, Param2: TVariable; _R: TVariable): TVariable;
var
  sn, cs : Extended;
  sh, ch, inv: Extended;
begin
  Result := _R;
  sincos(Param1.y, sn, cs);
//  sinhcosh(Param1.x, sh, ch);  

    ch  := exp(Param1.x) * 0.5;
    inv := 0.25 / ch;
    sh := ch - inv;
    ch := ch + inv;

  result.x := ch * cs;
  result.y := sh * sn;
end;

class function TFunction_Cosh.Quad(Param1, Param2: TVariable; _R: TVariable): TVariable;    
var c1, c2, c3, c4: tcomplex;   
var
  sn, cs : Extended;
  sh, ch, inv: Extended;
begin
  Result := _R;   
//  Quat2Comp(Param1, C1, C2);

  C1.x := Param1.x - Param1.w;
  C1.y := Param1.y + Param1.z;

  C2.x := Param1.x + Param1.w;
  C2.y := Param1.y - Param1.z;


//  C3 := TFunction_Cosh.Complex(C1, nil);

    sincos(C1.y, sn, cs);
//    sinhcosh(C1.x, sh, ch);

      ch  := exp(C1.x) * 0.5;
      inv := 0.25 / ch;
      sh := ch - inv;
      ch := ch + inv;

    C3.x := ch * cs;
    C3.y := sh * sn;

//  C4 := TFunction_Cosh.Complex(C2, nil);

    sincos(C2.y, sn, cs);
//    sinhcosh(C2.x, sh, ch);

      ch  := exp(C2.x) * 0.5;
      inv := 0.25 / ch;
      sh := ch - inv;
      ch := ch + inv;

    C4.x := ch * cs;
    C4.y := sh * sn;

//  Comp2Quat(C3, C4, result);
  result.x := (C3.x + C4.x) * 0.5;
  result.y := (C3.y + C4.y) * 0.5;
  result.z := (C3.y - C4.y) * 0.5;
  result.w := (C4.x - C3.x) * 0.5;
end;

class function TFunction_Cosh.Supports(Mode: TParserMode): Boolean;
begin
  Result := True;
end;

class function TFunction_Cosh.ParamCount: Integer;
begin
  Result := 1;
end;

// ---- Cos(X) -----------------------------------------------------------------

class function TFunction_Cos.Float(Param1, Param2: TVariable; _R: TVariable): TVariable;
begin
  Result := _R;
  Result.x := Cos(Param1.x);
end;

class function TFunction_Cos.Complex(Param1, Param2: TVariable; _R: TVariable): TVariable; 
var
  sn, cs : Extended;
  sh, ch, inv: Extended;
begin
  Result := _R;
  sincos(Param1.x, sn, cs);
//  sinhcosh(Param1.y, sh, ch);  

    ch  := exp(Param1.y) * 0.5;
    inv := 0.25 / ch;
    sh := ch - inv;
    ch := ch + inv;

  result.x :=   cs * ch;
  result.y := -(sn * sh);
end;

class function TFunction_Cos.Quad(Param1, Param2: TVariable; _R: TVariable): TVariable;
var c1, c2, c3, c4: tcomplex;     
var
  sn, cs : Extended;
  sh, ch, inv: Extended;
begin
  Result := _R;    
//  Quat2Comp(Param1, C1, C2);

  C1.x := Param1.x - Param1.w;
  C1.y := Param1.y + Param1.z;

  C2.x := Param1.x + Param1.w;
  C2.y := Param1.y - Param1.z;


//  C3 := TFunction_Cos.Complex(C1, nil);

    sincos(C1.x, sn, cs);
//    sinhcosh(C1.y, sh, ch);

      ch  := exp(C1.y) * 0.5;
      inv := 0.25 / ch;
      sh := ch - inv;
      ch := ch + inv;

    C3.x :=   cs * ch;
    C3.y := -(sn * sh);

//  C4 := TFunction_Cos.Complex(C2, nil);

    sincos(C2.x, sn, cs);
//    sinhcosh(C2.y, sh, ch);

      ch  := exp(C2.y) * 0.5;
      inv := 0.25 / ch;
      sh := ch - inv;
      ch := ch + inv;

    C4.x :=   cs * ch;
    C4.y := -(sn * sh);

//  Comp2Quat(C3, C4, result);
  result.x := (C3.x + C4.x) * 0.5;
  result.y := (C3.y + C4.y) * 0.5;
  result.z := (C3.y - C4.y) * 0.5;
  result.w := (C4.x - C3.x) * 0.5;
end;

class function TFunction_Cos.Supports(Mode: TParserMode): Boolean;
begin
  Result := True;
end;

class function TFunction_Cos.ParamCount: Integer;
begin
  Result := 1;
end;

// ---- Tanh(X) ----------------------------------------------------------------

class function TFunction_Tanh.Float(Param1, Param2: TVariable; _R: TVariable): TVariable;
begin
  Result := _R;  
  Result.x := Tanh(Param1.x);
end;

class function TFunction_Tanh.Complex(Param1, Param2: TVariable; _R: TVariable): TVariable;
var
  sn, cs : Extended;
  sh, ch, t, inv: Extended;
begin
  Result := _R;
  sincos(2*Param1.y, sn, cs);
//  sinhcosh(2*Param1.x, sh, ch);

    ch  := exp(2*Param1.x) * 0.5;
    inv := 0.25 / ch;
    sh := ch - inv;
    ch := ch + inv;

  t := ch + cs;
  result.x := sh / t;
  result.y := sn / t;
end;

class function TFunction_Tanh.Quad(Param1, Param2: TVariable; _R: TVariable): TVariable; 
var c1, c2, c3, c4: tcomplex;    
var
  sn, cs : Extended;
  sh, ch, t, inv: Extended;
begin
  Result := _R; 
//  Quat2Comp(Param1, C1, C2);

  C1.x := Param1.x - Param1.w;
  C1.y := Param1.y + Param1.z;

  C2.x := Param1.x + Param1.w;
  C2.y := Param1.y - Param1.z;


//  C3 := TFunction_Tanh.Complex(C1, nil);

    sincos(2*C1.y, sn, cs);
//    sinhcosh(2*C1.x, sh, ch);

      ch  := exp(2*C1.x) * 0.5;
      inv := 0.25 / ch;
      sh := ch - inv;
      ch := ch + inv;

    t := ch + cs;
    C3.x := sh / t;
    C3.y := sn / t;

//  C4 := TFunction_Tanh.Complex(C2, nil);

    sincos(2*C2.y, sn, cs);
//    sinhcosh(2*C2.x, sh, ch);

      ch  := exp(2*C2.x) * 0.5;
      inv := 0.25 / ch;
      sh := ch - inv;
      ch := ch + inv;

    t := ch + cs;
    C4.x := sh / t;
    C4.y := sn / t;

//  Comp2Quat(C3, C4, result);
  result.x := (C3.x + C4.x) * 0.5;
  result.y := (C3.y + C4.y) * 0.5;
  result.z := (C3.y - C4.y) * 0.5;
  result.w := (C4.x - C3.x) * 0.5;
end;

class function TFunction_Tanh.Supports(Mode: TParserMode): Boolean;
begin
  Result := True;
end;

class function TFunction_Tanh.ParamCount: Integer;
begin
  Result := 1;
end;

// ---- Tan(X) -----------------------------------------------------------------

class function TFunction_Tan.Float(Param1, Param2: TVariable; _R: TVariable): TVariable;
begin
  Result := _R;  
  Result.x := Tan(Param1.x);
end;

class function TFunction_Tan.Complex(Param1, Param2: TVariable; _R: TVariable): TVariable;                  
var
  sn, cs : Extended;
  sh, ch, t, inv: Extended;
begin
  Result := _R;
  sincos(2*Param1.x, sn, cs);
//  sinhcosh(2*Param1.y, sh, ch); 

    ch  := exp(2*Param1.y) * 0.5;
    inv := 0.25 / ch;
    sh := ch - inv;
    ch := ch + inv;

  t := cs + ch;
  result.x := sn / t;
  result.y := sh / t;
end;

class function TFunction_Tan.Quad(Param1, Param2: TVariable; _R: TVariable): TVariable; 
var c1, c2, c3, c4: tcomplex;  
var
  sn, cs : Extended;
  sh, ch, t, inv: Extended;
begin
  Result := _R;  
//  Quat2Comp(Param1, C1, C2);

  C1.x := Param1.x - Param1.w;
  C1.y := Param1.y + Param1.z;

  C2.x := Param1.x + Param1.w;
  C2.y := Param1.y - Param1.z;


//  C3 := TFunction_Tan.Complex(C1, nil);

    sincos(2*C1.x, sn, cs);
//    sinhcosh(2*C1.y, sh, ch);

      ch  := exp(2*C1.y) * 0.5;
      inv := 0.25 / ch;
      sh := ch - inv;
      ch := ch + inv;

    t := cs + ch;
    C3.x := sn / t;
    C3.y := sh / t;

//  C4 := TFunction_Tan.Complex(C2, nil);

    sincos(2*C2.x, sn, cs);
//    sinhcosh(2*C2.y, sh, ch);

      ch  := exp(2*C2.y) * 0.5;
      inv := 0.25 / ch;
      sh := ch - inv;
      ch := ch + inv;

    t := cs + ch;
    C4.x := sn / t;
    C4.y := sh / t;

//  Comp2Quat(C3, C4, result);
  result.x := (C3.x + C4.x) * 0.5;
  result.y := (C3.y + C4.y) * 0.5;
  result.z := (C3.y - C4.y) * 0.5;
  result.w := (C4.x - C3.x) * 0.5;
end;

class function TFunction_Tan.Supports(Mode: TParserMode): Boolean;
begin
  Result := True;
end;

class function TFunction_Tan.ParamCount: Integer;
begin
  Result := 1;
end;

// ---- Cotanh(X) --------------------------------------------------------------

class function TFunction_Cotanh.Float(Param1, Param2: TVariable; _R: TVariable): TVariable;
begin
  Assert(false);
  Result := _R;
//  Result.x := Coth(Param1.x);
end;

class function TFunction_Cotanh.Complex(Param1, Param2: TVariable; _R: TVariable): TVariable;               
var
  sn, cs : Extended;
  sh, ch, t, inv: Extended;
begin
  Result := _R;  
  sincos(2*Param1.y, sn, cs);
//  sinhcosh(2*Param1.x, sh, ch);

    ch  := exp(2*Param1.x) * 0.5;
    inv := 0.25 / ch;
    sh := ch - inv;
    ch := ch + inv;

  t := ch - cs;
  result.x := sh / t;
  result.y := sn / t;
end;

class function TFunction_Cotanh.Quad(Param1, Param2: TVariable; _R: TVariable): TVariable; 
var c1, c2, c3, c4: tcomplex;   
var
  sn, cs : Extended;
  sh, ch, t, inv: Extended;
begin
  Result := _R;
//  Quat2Comp(Param1, C1, C2);

  C1.x := Param1.x - Param1.w;
  C1.y := Param1.y + Param1.z;

  C2.x := Param1.x + Param1.w;
  C2.y := Param1.y - Param1.z;


//  C3 := TFunction_Cotanh.Complex(C1, nil);

    sincos(2*C1.y, sn, cs);
//    sinhcosh(2*C1.x, sh, ch);

      ch  := exp(2*C1.x) * 0.5;
      inv := 0.25 / ch;
      sh := ch - inv;
      ch := ch + inv;

    t := ch - cs;
    C3.x := sh / t;
    C3.y := sn / t;

//  C4 := TFunction_Cotanh.Complex(C2, nil);

    sincos(2*C2.y, sn, cs);
//    sinhcosh(2*C2.x, sh, ch);

      ch  := exp(2*C2.x) * 0.5;
      inv := 0.25 / ch;
      sh := ch - inv;
      ch := ch + inv;

    t := ch - cs;
    C4.x := sh / t;
    C4.y := sn / t;

//  Comp2Quat(C3, C4, result);
  result.x := (C3.x + C4.x) * 0.5;
  result.y := (C3.y + C4.y) * 0.5;
  result.z := (C3.y - C4.y) * 0.5;
  result.w := (C4.x - C3.x) * 0.5;
end;

class function TFunction_Cotanh.Supports(Mode: TParserMode): Boolean;
begin
  Result := True;
end;

class function TFunction_Cotanh.ParamCount: Integer;
begin
  Result := 1;
end;

// ---- Cotan(X) ---------------------------------------------------------------

class function TFunction_Cotan.Float(Param1, Param2: TVariable; _R: TVariable): TVariable;
begin
  Result := _R;
  Result.x := Cotan(Param1.x);
end;

class function TFunction_Cotan.Complex(Param1, Param2: TVariable; _R: TVariable): TVariable;                
var
  sn, cs : Extended;
  sh, ch, t, inv: Extended;
begin
  Result := _R;
  sincos(2*Param1.x, sn, cs);
//  sinhcosh(2*Param1.y, sh, ch); 

    ch  := exp(2*Param1.y) * 0.5;
    inv := 0.25 / ch;
    sh := ch - inv;
    ch := ch + inv;

  t := ch - cs;
  result.x :=   sn / t;
  result.y := -(sh / t);
end;

class function TFunction_Cotan.Quad(Param1, Param2: TVariable; _R: TVariable): TVariable;   
var c1, c2, c3, c4: tcomplex;      
var
  sn, cs : Extended;
  sh, ch, t, inv: Extended;
begin
  Result := _R;   
//  Quat2Comp(Param1, C1, C2);

  C1.x := Param1.x - Param1.w;
  C1.y := Param1.y + Param1.z;

  C2.x := Param1.x + Param1.w;
  C2.y := Param1.y - Param1.z;


//  C3 := TFunction_Cotan.Complex(C1, nil);

    sincos(2*C1.x, sn, cs);
//    sinhcosh(2*C1.y, sh, ch);

      ch  := exp(2*C1.y) * 0.5;
      inv := 0.25 / ch;
      sh := ch - inv;
      ch := ch + inv;

    t := ch - cs;
    C3.x :=   sn / t;
    C3.y := -(sh / t);

//  C4 := TFunction_Cotan.Complex(C2, nil);

    sincos(2*C2.x, sn, cs);
//    sinhcosh(2*C2.y, sh, ch);

      ch  := exp(2*C2.y) * 0.5;
      inv := 0.25 / ch;
      sh := ch - inv;
      ch := ch + inv;

    t := ch - cs;
    C4.x :=   sn / t;
    C4.y := -(sh / t);

//  Comp2Quat(C3, C4, result);
  result.x := (C3.x + C4.x) * 0.5;
  result.y := (C3.y + C4.y) * 0.5;
  result.z := (C3.y - C4.y) * 0.5;
  result.w := (C4.x - C3.x) * 0.5;
end;

class function TFunction_Cotan.Supports(Mode: TParserMode): Boolean;
begin
  Result := True;
end;

class function TFunction_Cotan.ParamCount: Integer;
begin
  Result := 1;
end;

// ---- Abs(X) -----------------------------------------------------------------

class function TFunction_Abs.Float(Param1, Param2: TVariable; _R: TVariable): TVariable;
begin
  Result := _R;   
  Result.x := Abs(Param1.x);
end;

class function TFunction_Abs.Complex(Param1, Param2: TVariable; _R: TVariable): TVariable;
begin
  Result := _R;   
  result.x := sqrt(sqr(Param1.x)+sqr(Param1.y));
  result.y := 0;
end;

class function TFunction_Abs.Quad(Param1, Param2: TVariable; _R: TVariable): TVariable;
begin
  Result := _R; 
  result.x := sqrt(sqr(Param1.x)+sqr(Param1.y)+sqr(Param1.z)+sqr(Param1.w));
  result.y := 0;
  result.z := 0;
  result.w := 0;
end;

class function TFunction_Abs.Supports(Mode: TParserMode): Boolean;
begin
  Result := True;
end;

class function TFunction_Abs.ParamCount: Integer;
begin
  Result := 1;
end;

// ---- Exp(X) -----------------------------------------------------------------

class function TFunction_Exp.Float(Param1, Param2: TVariable; _R: TVariable): TVariable;
begin
  Result := _R;
  Result.x := Exp(Param1.x);
end;

class function TFunction_Exp.Complex(Param1, Param2: TVariable; _R: TVariable): TVariable;
var
  sn, cs, t: Extended;
begin
  Result := _R;
  sincos(Param1.y, sn, cs);
  t := exp(Param1.x);
  result.x := t * cs;
  result.y := t * sn;
end;

class function TFunction_Exp.Quad(Param1, Param2: TVariable; _R: TVariable): TVariable;
var c1, c2, c3, c4: tcomplex;
var
  sn, cs : Extended;
  t: Extended;
begin
  Result := _R;
//  Quat2Comp(Param1, C1, C2);

  C1.x := Param1.x - Param1.w;
  C1.y := Param1.y + Param1.z;

  C2.x := Param1.x + Param1.w;
  C2.y := Param1.y - Param1.z;


//  C3 := TFunction_Exp.Complex(C1, nil);

    sincos(C1.y, sn, cs);
    t := exp(C1.x);
    C3.x := t * cs;
    C3.y := t * sn;

//  C4 := TFunction_Exp.Complex(C2, nil);

    sincos(C2.y, sn, cs);
    t := exp(C2.x);
    C4.x := t * cs;
    C4.y := t * sn;

//  Comp2Quat(C3, C4, result);
  result.x := (C3.x + C4.x) * 0.5;
  result.y := (C3.y + C4.y) * 0.5;
  result.z := (C3.y - C4.y) * 0.5;
  result.w := (C4.x - C3.x) * 0.5;
end;

class function TFunction_Exp.Supports(Mode: TParserMode): Boolean;
begin
  Result := True;
end;

class function TFunction_Exp.ParamCount: Integer;
begin
  Result := 1;
end;

// ---- Frac(X) ----------------------------------------------------------------

class function TFunction_Frac.Float(Param1, Param2: TVariable; _R: TVariable): TVariable;
begin
  Result := _R;
  Result.x := Frac(Param1.x);
end;

class function TFunction_Frac.Complex(Param1, Param2: TVariable; _R: TVariable): TVariable;
begin
  Result := _R;
  Result.x := Frac(Param1.x);
  Result.y := Frac(Param1.y);
end;

class function TFunction_Frac.Quad(Param1, Param2: TVariable; _R: TVariable): TVariable;
begin
  Result := _R;              
  Result.x := Frac(Param1.x);
  Result.y := Frac(Param1.y);
  Result.z := Frac(Param1.z);
  Result.w := Frac(Param1.w);
end;

class function TFunction_Frac.Supports(Mode: TParserMode): Boolean;
begin
  Result := True;
end;

class function TFunction_Frac.ParamCount: Integer;
begin
  Result := 1;
end;

// ---- Int(X) -----------------------------------------------------------------

class function TFunction_Int.Float(Param1, Param2: TVariable; _R: TVariable): TVariable;
begin
  Result := _R;
  Result.x := Int(Param1.x);
end;

class function TFunction_Int.Complex(Param1, Param2: TVariable; _R: TVariable): TVariable;
begin
  Result := _R;
  Result.x := Int(Param1.x);
  Result.y := Int(Param1.y);
end;

class function TFunction_Int.Quad(Param1, Param2: TVariable; _R: TVariable): TVariable;
begin
  Result := _R;
  Result.x := Int(Param1.x);
  Result.y := Int(Param1.y);
  Result.z := Int(Param1.z);
  Result.w := Int(Param1.w);
end;

class function TFunction_Int.Supports(Mode: TParserMode): Boolean;
begin
  Result := True;
end;

class function TFunction_Int.ParamCount: Integer;
begin
  Result := 1;
end;

// ---- Round(X) ---------------------------------------------------------------

class function TFunction_Round.Float(Param1, Param2: TVariable; _R: TVariable): TVariable;
begin
  Result := _R;
  Result.x := Round(Param1.x);
end;

class function TFunction_Round.Complex(Param1, Param2: TVariable; _R: TVariable): TVariable;
begin
  Result := _R;
  Result.x := Round(Param1.x);
  Result.y := Round(Param1.y);
end;

class function TFunction_Round.Quad(Param1, Param2: TVariable; _R: TVariable): TVariable;
begin
  Result := _R;
  Result.x := Round(Param1.x);
  Result.y := Round(Param1.y);
  Result.z := Round(Param1.z);
  Result.w := Round(Param1.w);
end;

class function TFunction_Round.Supports(Mode: TParserMode): Boolean;
begin
  Result := True;
end;

class function TFunction_Round.ParamCount: Integer;
begin
  Result := 1;
end;

// ---- Arcsinh(X) -------------------------------------------------------------

class function TFunction_Arcsinh.Float(Param1, Param2: TVariable; _R: TVariable): TVariable;
begin
  Result := _R;
  Result.x := Arcsinh(Param1.x);
end;

class function TFunction_Arcsinh.Complex(Param1, Param2: TVariable; _R: TVariable): TVariable; 
var
  sn, cs, t: Extended;
  c1: TComplex;
begin
  Result := _R;

  // Sqr(Param1) + 1
    sincos(0.5*Sqrt(Sqr(Param1.x) + Sqr(Param1.y)), sn, cs);
    t := sqrt(Sqrt(Sqr(Param1.x) + Sqr(Param1.y)));
    c1.x := t * cs + 1;
    c1.y := t * sn;

  // Param1 + Sqrt(_1)
    sincos(0.5*Sqrt(Sqr(c1.x) + Sqr(c1.y)), sn, cs);
    t := sqrt(Sqrt(Sqr(c1.x) + Sqr(c1.y)));
    c1.x := t * cs + Param1.x;
    c1.y := t * sn + Param1.y;

  // Ln(_2)
  try
    result.x := ln(sqrt(sqr(c1.x)+sqr(c1.y)));
    result.y := _argC(c1);
  except
    result.x := 0;
    result.y := 0;
  end;
end;

class function TFunction_Arcsinh.Quad(Param1, Param2: TVariable; _R: TVariable): TVariable;
begin
  Result := nil;
end;

class function TFunction_Arcsinh.Supports(Mode: TParserMode): Boolean;
begin
  Result := Mode <> pmQuad;
end;

class function TFunction_Arcsinh.ParamCount: Integer;
begin
  Result := 1;
end;

// ---- Arccosh(X) -------------------------------------------------------------

class function TFunction_Arccosh.Float(Param1, Param2: TVariable; _R: TVariable): TVariable;
begin
  Result := _R;
  Result.x := Arccosh(Param1.x);
end;

class function TFunction_Arccosh.Complex(Param1, Param2: TVariable; _R: TVariable): TVariable;
var
  sn, cs, t: Extended;
  c1: TComplex;
begin
  Result := _R;

  // Sqr(Param1) - 1
    sincos(0.5*Sqrt(Sqr(Param1.x) + Sqr(Param1.y)), sn, cs);
    t := sqrt(Sqrt(Sqr(Param1.x) + Sqr(Param1.y)));
    c1.x := t * cs - 1;
    c1.y := t * sn;

  // Param1 + Sqrt(_1)
    sincos(0.5*Sqrt(Sqr(c1.x) + Sqr(c1.y)), sn, cs);
    t := sqrt(Sqrt(Sqr(c1.x) + Sqr(c1.y)));
    c1.x := t * cs + Param1.x;
    c1.y := t * sn + Param1.y;

  // Ln(_2)
  try
    result.x := ln(sqrt(sqr(c1.x)+sqr(c1.y)));
    result.y := _argC(c1);
  except
    result.x := 0;
    result.y := 0;
  end;
end;

class function TFunction_Arccosh.Quad(Param1, Param2: TVariable; _R: TVariable): TVariable;
begin
  Result := nil;
end;

class function TFunction_Arccosh.Supports(Mode: TParserMode): Boolean;
begin
  Result := Mode <> pmQuad;
end;

class function TFunction_Arccosh.ParamCount: Integer;
begin
  Result := 1;
end;

// ---- Arctanh(X) -------------------------------------------------------------

class function TFunction_Arctanh.Float(Param1, Param2: TVariable; _R: TVariable): TVariable;
begin
  Result := _R;
  Result.x := Arctanh(Param1.x);
end;

class function TFunction_Arctanh.Complex(Param1, Param2: TVariable; _R: TVariable): TVariable;
var
  c1, c2: TComplex;
begin
  Result := _R;

  // 1 + Param1
    c1.x := 1 + Param1.x;
    c1.y := Param1.y;

  // 1 - Param1
    c2.x := 1 - Param1.x;
    c2.y := Param1.y;

  // _1 / _2   
    c1.x :=  c1.x/c2.x + c1.y/c2.y;
    c1.y :=  c1.y/c2.x - c1.x/c2.y;

  // Ln(_3)
  try
    result.x := ln(sqrt(sqr(c1.x)+sqr(c1.y)));
    result.y := _argC(c1);
  except
    result.x := 0;
    result.y := 0;
  end;
end;

class function TFunction_Arctanh.Quad(Param1, Param2: TVariable; _R: TVariable): TVariable;
begin
  Result := nil;
end;

class function TFunction_Arctanh.Supports(Mode: TParserMode): Boolean;
begin
  Result := Mode <> pmQuad;
end;

class function TFunction_Arctanh.ParamCount: Integer;
begin
  Result := 1;
end;

// ---- Arcsin(X) -------------------------------------------------------------

class function TFunction_Arcsin.Float(Param1, Param2: TVariable; _R: TVariable): TVariable;
begin
  Result := _R;
  Result.x := Arcsin(Param1.x);
end;

class function TFunction_Arcsin.Complex(Param1, Param2: TVariable; _R: TVariable): TVariable;
var
  sn, cs, t: Extended;
  c1, c2: TComplex;
begin
  Result := _R;

  // 1 - Param1^2
    c1.x := 1 - sqr(Param1.x)-sqr(Param1.y);
    c1.y := 2*Param1.x*Param1.y;

  // Sqrt(_1)
    sincos(0.5*Sqrt(Sqr(c1.x) + Sqr(c1.y)), sn, cs);
    t := sqrt(Sqrt(Sqr(c1.x) + Sqr(c1.y)));
    c1.x := t * cs;
    c1.y := t * sn;

  // i * Param1
    c2.x := - Param1.y;
    c2.y := Param1.x;

  // _2 + _1
    c1.x := c1.x + c2.x;
    c1.y := c1.y + c2.y;

  // Ln(_3)
    try
      c1.x := ln(sqrt(sqr(c1.x)+sqr(c1.y)));
      c1.y := _argC(c1);
    except
      c1.x := 0;
      c1.y := 0;
    end;

  // -i * _4
  result.x := c1.y;
  result.y := -c1.x;
end;

class function TFunction_Arcsin.Quad(Param1, Param2: TVariable; _R: TVariable): TVariable;
begin
  Result := nil;
end;

class function TFunction_Arcsin.Supports(Mode: TParserMode): Boolean;
begin
  Result := Mode <> pmQuad;
end;

class function TFunction_Arcsin.ParamCount: Integer;
begin
  Result := 1;
end;

// ---- Arccos(X) -------------------------------------------------------------

class function TFunction_Arccos.Float(Param1, Param2: TVariable; _R: TVariable): TVariable;
begin
  Result := _R;
  Result.x := Arccos(Param1.x);
end;

class function TFunction_Arccos.Complex(Param1, Param2: TVariable; _R: TVariable): TVariable;
var
  sn, cs, t: Extended;
  c1: TComplex;
begin
  Result := _R;

  // Param1^2 - 1
    c1.x := sqr(Param1.x)-sqr(Param1.y) - 1;
    c1.y := 2*Param1.x*Param1.y;

  // Sqrt(_1)
    sincos(0.5*Sqrt(Sqr(c1.x) + Sqr(c1.y)), sn, cs);
    t := sqrt(Sqrt(Sqr(c1.x) + Sqr(c1.y)));
    c1.x := t * cs;
    c1.y := t * sn;

  // Param1 + _2
    c1.x := Param1.x + c1.x;
    c1.y := Param1.y + c1.y;

  // Ln(_3)
    try
      c1.x := ln(sqrt(sqr(c1.x)+sqr(c1.y)));
      c1.y := _argC(c1);
    except
      c1.x := 0;
      c1.y := 0;
    end;

  // -i * _4
    result.x := c1.y;
    result.y := -c1.x;
end;

class function TFunction_Arccos.Quad(Param1, Param2: TVariable; _R: TVariable): TVariable;
begin
  Result := nil;
end;

class function TFunction_Arccos.Supports(Mode: TParserMode): Boolean;
begin
  Result := Mode <> pmQuad;
end;

class function TFunction_Arccos.ParamCount: Integer;
begin
  Result := 1;
end;

// ---- Arctan(X) -------------------------------------------------------------

class function TFunction_Arctan.Float(Param1, Param2: TVariable; _R: TVariable): TVariable;
begin
  Result := _R;
  Result.x := Arctan(Param1.x);
end;

class function TFunction_Arctan.Complex(Param1, Param2: TVariable; _R: TVariable): TVariable;
var
  sn, cs, t: Extended;
  c1: TComplex;
begin
  Result := _R;

  // Param1^2 - 1
    c1.x := sqr(Param1.x)-sqr(Param1.y) - 1;
    c1.y := 2*Param1.x*Param1.y;

  // Sqrt(_1)
    sincos(0.5*Sqrt(Sqr(c1.x) + Sqr(c1.y)), sn, cs);
    t := sqrt(Sqrt(Sqr(c1.x) + Sqr(c1.y)));
    c1.x := t * cs;
    c1.y := t * sn;

  // Param1 + _2
    c1.x := Param1.x + c1.x;
    c1.y := Param1.y + c1.y;

  // Ln(_3)
    try
      c1.x := ln(sqrt(sqr(c1.x)+sqr(c1.y)));
      c1.y := _argC(c1);
    except
      c1.x := 0;
      c1.y := 0;
    end;

  // -i * _4
    result.x := c1.y;
    result.y := -c1.x;
end;

class function TFunction_Arctan.Quad(Param1, Param2: TVariable; _R: TVariable): TVariable;
begin
  Result := nil;
end;

class function TFunction_Arctan.Supports(Mode: TParserMode): Boolean;
begin
  Result := Mode <> pmQuad;
end;

class function TFunction_Arctan.ParamCount: Integer;
begin
  Result := 1;
end;

end.
