unit ExCQParser;

interface

uses
  ECQ_Def, ECQ_Func, ECQ_Varlist, ECQ_Ops, ECQ_Assembler, SysUtils;

type
  TPriority = 1..High(Integer)-3;

type
  TAtomicOp = class
    _Func: TECQPFunction;
    _Class: TFunctionClass;
    OpType: TOpType;
    EAX: Integer;

    Next: TAtomicOp;
  end;

  TOperatorContext = record
    Callee: TECQPFunction;
    _Class: TFunctionClass;
    CallType: TCallType;
    OpCallType: TOpCallType;
    EAX: Integer;
    OpType: TOpType;
    Priority: Integer;
  end;

  TOCArray = array of TOperatorContext;
  TNumArray = array of TVariable;

  TParseRec = record
    OpStack: TOCArray;
    NumStack: TNumArray;
  end;

  TErrorType = (etNumberExpected, etUnknownIdentifier, etOther);

  EInvalidExpression = class(Exception)
  private
    fPos: Integer;
    fString: string;
    fErr: TErrorType;
  public
    property ErrorPos: Integer read fPos;
    property Formula: string read fString;
    property ErrorType: TErrorType read fErr;
  end;

  EModeUnsupported = class(Exception);

  TExCQParser = class(TObject)
  private
    fFunctions: TFunctionList;
    fVariables: TVariableList;
    fOperators: TOperatorList;

    fOtherVars: TVariableList;

    fOperations: TAtomicOp;

    fEvalStack,
    fRealStack: TVariableStack;

    fMode: TParserMode;
    fSolveMode: TSolveMode;

    fAssembler: TECQPAssembler;

    fExpressionCode: TCompiledExpression;

    fExpendableVars: array of TVariable;
    fExpendableStackTop: Integer;

    fPreSolve: Boolean;

    fFormula: string;
    fCurPos: Integer;

    function Extract(F: TFunctionClass): TECQPFunction; overload;
    function Extract(O: TOperator): Integer; overload;

    function _Parse(S: string): TParseRec;
    function _Serialize(Context: TParseRec): TAtomicOp;
    function _PreSolve(P: TParseRec): TParseRec;
    function _ParaClean(R: TParseRec): TParseRec;

    function New: TVariable;
    procedure Clear;
    procedure FreeOps;
  public             
    constructor Create; virtual;
    destructor Destroy; override;

    property Functions: TFunctionList read fFunctions;
    property Variables: TVariableList read fVariables;
    property Operators: TOperatorList read fOperators;

    procedure AddFunction(AFunction: TFunctionClass; Name: string);
    procedure DelFunction(Name: string);

    procedure AddOperator(AOperator: TFunctionClass; Name: Char;
                          OpType: TOperatorType; Precedence: TPriority;
                          IsMul: Boolean = false; UsePri: Boolean = False);
    procedure DelOperator(Name: Char; OpType: TOperatorType);

    procedure RegisterVariable(VarName: string; Value: array of TFloat; Constant: Boolean = false); overload;
    procedure RegisterVariable(VarName: string; Constant: Boolean = false); overload;
    procedure UnRegisterVariable(VarName: string);

    procedure SetVariable(VarName: string; Value: array of TFloat); overload;
    procedure SetVariable(VarName: string; Value: TVariable); overload;
    function GetVariable(VarName: string): TVariable;

    procedure AdjustMode(New: TParserMode);

    property Mode: TParserMode read fMode write AdjustMode;

    procedure Parse(S: string);

    function Solve: TVariable;

    function Call: TVariable;

    property SolveMode: TSolveMode read fSolveMode write fSolveMode;

    procedure Compile;

    property PreSolve: Boolean read fPreSolve write fPreSolve;
  end;

implementation

uses
  ExCQP_Predefs;

const
  FUNCTION_PRI = High(Integer);
  PREFIX_PRI = FUNCTION_PRI-1;
  POSTFIX_PRI = PREFIX_PRI-1;
  CLOSE_PRI = Low(Integer);

constructor TExCQParser.Create;
begin
  inherited;
  fFunctions := TFunctionList.Create;
  fVariables := TVariableList.Create;
  fOperators := TOperatorList.Create;
  fOtherVars := TVariableList.Create;    
  fAssembler := TECQPAssembler.Create; 
  ExCQP_Predefs.RegisterTo(Self);
  fMode := pmReal;
  fSolveMode := smVM;
  fExpendableStackTop := 0;
  fPreSolve := True;
end;

destructor TExCQParser.Destroy;
begin
  fFunctions.Free;
  fVariables.Free;
  fOperators.Free;
  fOtherVars.Free;
  fRealStack.Free;
  fEvalStack.Free;
  fAssembler.Free;
  Clear;
  FreeOps;
  inherited;
end;

procedure TExCQParser.AddFunction(AFunction: TFunctionClass; Name: string);
begin
  fFunctions.Add(
    TFunctionItem.Create(AFunction, Name)
  );
end;

procedure TExCQParser.AddOperator(AOperator: TFunctionClass; Name: Char;
                                  OpType: TOperatorType; Precedence: TPriority;
                                  IsMul: Boolean = false; UsePri: Boolean = False);
begin
  if not UsePri then
    case OpType of
      otUnaryPrefix: Integer(Precedence) := PREFIX_PRI;
      otUnaryPostfix: Integer(Precedence) := POSTFIX_PRI;
    end;
  fOperators.Add(
    TOperator.Create(AOperator, Name, OpType, Precedence, IsMul)
  );
end;

procedure TExCQParser.RegisterVariable(VarName: string; Value: array of TFloat; Constant: Boolean = false);
var i, varLimit: Integer; Item: TVariable;
begin
  Item := TVariable.Create(Constant);
  if Length(Value) > 4 then
    varLimit := 3
  else
    varLimit := High(Value);

  for i := 0 to varLimit do
    Item[i] := Value[i];

  fVariables.Add(
    TVariableItem.Create(Item, VarName)
  );
end;

procedure TExCQParser.RegisterVariable(VarName: string; Constant: Boolean = false); 
begin
  RegisterVariable(VarName, [], Constant);
end;

procedure TExCQParser.UnRegisterVariable(VarName: string);
var i: Integer;
begin
  for i := 0 to fVariables.Length-1 do
    if AnsiSameText(fVariables[i].Name, VarName) then
    begin
      fVariables.Delete(i);
      Exit;
    end;
end;

procedure TExCQParser.DelFunction(Name: string);
var i: Integer;
begin
  for i := 0 to fFunctions.Length-1 do
    if AnsiSameText(fFunctions[i].Name, Name) then
    begin
      fFunctions.Delete(i);
      Exit;
    end;
end;

procedure TExCQParser.DelOperator(Name: Char; OpType: TOperatorType);
var i: Integer;
begin
  for i := 0 to fOperators.Length-1 do
    if AnsiSameText(fOperators[i].Name, Name) and (fOperators[i].OpType = OpType) then
    begin
      fOperators.Delete(i);
      Exit;
    end;
end;

procedure TExCQParser.SetVariable(VarName: string; Value: array of TFloat);
var i, x: Integer;
begin
  for i := 0 to fVariables.Length-1 do
    if AnsiSameText(fVariables[i].Name, VarName) then
    begin
      for x := 0 to High(Value) do
        fVariables[i].Variable[x] := Value[x];
      Exit;
    end;
  RegisterVariable(VarName, Value);
end;

procedure TExCQParser.SetVariable(VarName: string; Value: TVariable);
var i: Integer;
begin
  for i := 0 to fVariables.Length-1 do
    if AnsiSameText(fVariables[i].Name, VarName) then
    begin
      Value.AssignTo(fVariables[i].Variable);
      Exit;
    end;
end;

function TExCQParser.GetVariable(VarName: string): TVariable;
var i: integer;
begin
  Result := nil;
  for i := 0 to fVariables.Length-1 do
    if AnsiSameText(fVariables[i].Name, VarName) then
    begin
      Result := fVariables[i].Variable;
      Exit;
    end;
end;

function TExCQParser._Parse(S: string): TParseRec;
var
  i, NumDiff, Num_Pre, PC, x, z, PDiff: Integer;
  TempVar: TVariable;
  NumStack: TNumArray;
  OpStack: TOCArray;
  Op: TOperator;
  Context: TOperatorContext;
  _Result, R: TParseRec;
  Temp: string;
  Worked, Need: Boolean;

  procedure InternalDelete(var S: String; Start, Count: Integer);
  begin
    PDiff := Count;
    fCurPos := fCurPos + Count;
    Delete(S, Start, Count);
  end;

  procedure Clean;
  begin
    SetLength(OpStack, 0);
    SetLength(NumStack, 0);
  end;

  procedure Kill(ET: TErrorType);
  var E: EInvalidExpression;
  begin
    Clean;
    E := EInvalidExpression.Create('Invalid Expression');
    E.fString := fFormula;
    E.fPos := fCurPos;
    E.fErr := ET;
    raise E;
  end;

  procedure AppendNum(V: TVariable);
  begin
    SetLength(NumStack, Length(NumStack)+1);
    NumStack[High(NumStack)] := V;
  end;

  procedure AppendOp(O: TOperatorContext);
  begin
    SetLength(OpStack, Length(OpStack)+1);
    OpStack[High(OpStack)] := O;
  end;

  function Diff:Integer;
  begin
    Result:= Length(OpStack) - Length(NumStack);
  end;

  procedure DoOp(OpChar: Char);
  var
    i: Integer;
    OpClass: set of TOperatorType;
  begin
    if (NumDiff < 0) or (Diff < 0) then // Postfix / Binary
      OpClass := [otUnaryPostfix, otBinary]
    else                                // Prefix
      OpClass := [otUnaryPrefix];
    Op := nil;
    for i := 0 to fOperators.Length-1 do
      if (fOperators[i].OpType in OpClass) and (OpChar = fOperators[i].Name) then
      begin
        Op := fOperators[i];
        Break;
      end;
    if Op = nil then Kill(etUnknownIdentifier);
    Context.Callee := Extract(Op.FunctionClass);
    Context._Class := Op.FunctionClass;
    Context.EAX := Extract(Op);
    Context.CallType := ctOp;
    Context.Priority := Op.Precedence;
    case fOperators[i].OpType of
      otBinary:
        begin
          Context.OpType := opCall_P2;
          Context.OpCallType := octBinary;
          Inc(NumDiff);
        end;
      otUnaryPrefix:
        begin
          Context.OpType := opCall_P1;
          Context.OpCallType := octPrefix;
        end;
      otUnaryPostfix:
        begin
          Context.OpType := opCall_P1;
          Context.OpCallType := octPostfix;
        end;
    end;
    AppendOp(Context);
  end;

  function HasOp(Name: Char): Boolean;
  var
    i: Integer;
    OpClass: set of TOperatorType;
  begin
    Result := False;   
    if (NumDiff < 0) or (Diff < 0) then // Postfix / Binary
      OpClass := [otUnaryPostfix, otBinary]
    else                                // Prefix
      OpClass := [otUnaryPrefix];
    for i := 0 to fOperators.Length-1 do
      if (fOperators[i].OpType in OpClass) and (Name = fOperators[i].Name) then
      begin
        Result := True;
        Exit;
      end;
  end;

  procedure Append(var R: TParseRec);
  var x: Integer;
  begin
    for x := 0 to High(R.NumStack) do
      AppendNum(R.NumStack[x]);
    for x := 0 to High(R.OpStack) do
      AppendOp(R.OpStack[x]);
    SetLength(R.OpStack, 0);
    SetLength(R.NumStack, 0);
  end;

  procedure NumDiff0;
  begin
    Context.Callee := Extract(fOperators.Mul.FunctionClass);
    Context.EAX := Extract(fOperators.Mul);
    Context.OpType := opCall_P2;
    Context.Priority := fOperators.Mul.Precedence;
    Context.CallType := ctOp;
    Context._Class := fOperators.Mul.FunctionClass;
    AppendOp(Context);
    Inc(NumDiff);
  end;

begin
  NumDiff := 0;
  Num_Pre := 0;
  Worked := True;
  Need := False;
  if S = '' then
    Kill(etOther);
  while S <> '' do
  begin
    if not Worked then Kill(etOther);
    Worked := False;
    case S[1] of
      '0'..'9':
        begin
          for i := 1 to Length(S) do
            if not (S[i+1] in ['0'..'9', DecimalSeparator]) then Break;
          TempVar := TVariable.Create(true);
          TempVar._Set(Copy(S, 1, i));
          InternalDelete(S, 1, i);
          AppendNum(TempVar);
          fOtherVars.Add(
            TVariableItem.Create(TempVar, IntToStr(fOtherVars.Length))
          ); 

          if NumDiff < 0 then
            NumDiff0;

          Dec(NumDiff);
          Num_Pre := 0;

          Worked := True;
          Need := False;
        end;
    else
      if S[1] = '(' then
      begin
        PC := 1;
        for i := 2 to Length(S) do
        begin
          case S[i] of
            '(': Inc(PC);
            ')': Dec(PC);
          end;
          if PC = 0 then
          begin
            SetLength(R.OpStack, 0);
            SetLength(R.NumStack, 0);
            SetLength(_Result.OpStack, 0);
            SetLength(_Result.NumStack, 0); 

            Inc(fCurPos);

            _Result := _Parse(Copy(S, 2, i-2));
            R := _ParaClean(_Result);
            _Result := _ParaClean(_PreSolve(R));   

            if NumDiff < 0 then
              NumDiff0;

            Context.OpType := opOpen;
            Context.CallType := ctNone;
            Context.Priority := FUNCTION_PRI;
            AppendOp(Context);

            Append(_Result);

            Context.OpType := opClose;
            Context.CallType := ctNone;
            Context.Priority := CLOSE_PRI;
            AppendOp(Context);

            Num_Pre := 0;

            Delete(S, 1, i);
            Inc(fCurPos);

            Dec(NumDiff);

            Worked := True;
            Need := False;

            Break;
          end;
        end;
      end
      else
      begin
        for i := 1 to Length(S) do
          if not (S[i] in ['a'..'z', 'A'..'Z', '0'..'9', '_']) then Break;
{$B-}
        if (i = 1) or (HasOp(S[1]) and ((S[2] in ['(', '0'..'9']) or (HasOp(S[2])) or (S[2] = ''))) then // Operator
        begin
          if Need then Kill(etNumberExpected);
          DoOp(S[1]);
          InternalDelete(S, 1, 1);
          Worked := True;
          Need := OpStack[High(OpStack)].Priority = POSTFIX_PRI;
        end
        else          // Function / Variable
        begin
          if S[i] = '(' then // Function
          begin
            Temp := Copy(S, 1, i-1);
            Op := nil;
            for i := 0 to fFunctions.Length-1 do
              if AnsiSameText(fFunctions[i].Name, Temp) then
              begin
                Op := fOperators.Mul;
                Context.Callee := Extract(fFunctions[i].FunctionClass);
                Context.CallType := ctFun;
                Context._Class := fFunctions[i].FunctionClass;
                case fFunctions[i].ParamCount of
                  0: Context.OpType := opCall_P0;
                  1: Context.OpType := opCall_P1;
                  2: Context.OpType := opCall_P2;
                end;
                case fFunctions[i].ParamCount of
                  0: begin
                       x := Pos(')', S);
                       Delete(S, 1, x);
                     end;
                  1: begin
                       x := Pos('(', S);
                       Delete(S, 1, x);

                       PC := 1;
                       for z := 1 to Length(S) do
                       begin
                         case S[z] of
                           '(': Inc(PC);
                           ')': Dec(PC);
                         end;

                         if PC = 0 then
                         begin
                           Temp := Copy(S, 1, z-1);
                           InternalDelete(S, 1, z);
                           _Result := _Parse(Temp);
                           _Result := _ParaClean(_PreSolve(_ParaClean(_Result)));
                           Break;
                         end;
                       end;

                       if PC <> 0 then Kill(etOther);
                     end;
                  2: begin
                       x := Pos('(', S);
                       Delete(S, 1, x);

                       PC := 1;
                       for z := 1 to Length(S) do
                       begin
                         case S[z] of
                           '(': Inc(PC);
                           ')': Dec(PC);
                           ',': if PC = 1 then PC := 0;
                         end;

                         if PC = 0 then
                         begin
                           Temp := Copy(S, 1, z-1);
                           InternalDelete(S, 1, z);
                           _Result := _Parse(Temp);

                           Context.OpType := opOpen;
                           Context.CallType := ctNone;
                           Context.Priority := FUNCTION_PRI;
                           AppendOp(Context);

                           _Result := _ParaClean(_PreSolve(_ParaClean(_Result)));
                           Append(_Result);

                           Context.OpType := opClose;
                           Context.Priority := CLOSE_PRI;
                           AppendOp(Context);
                           Break;
                         end;
                       end;
                       if PC <> 0 then Kill(etOther);

                       PC := 1;
                       for z := 1 to Length(S) do
                       begin
                         case S[z] of
                           '(': Inc(PC);
                           ')': Dec(PC);
                         end;

                         if PC = 0 then
                         begin
                           Temp := Copy(S, 1, z-1);
                           InternalDelete(S, 1, z);
                           Context.OpType := opOpen;
                           Context.CallType := ctNone;
                           Context.Priority := FUNCTION_PRI;
                           AppendOp(Context);
                           _Result := _Parse(Temp); 
                           _Result := _ParaClean(_PreSolve(_ParaClean(_Result)));
                           Context.OpType := opClose;
                           Context.Priority := CLOSE_PRI;
                           AppendOp(Context);
                           Break;
                         end;
                       end;

                       if PC <> 0 then Kill(etOther);
                     end;
                end;
                Context.Priority := FUNCTION_PRI;
                AppendOp(Context);
                           Append(_Result);
                Context.OpType := opClose;
                AppendOp(Context);

                if NumDiff < 0 then
                  NumDiff0;

                Dec(NumDiff);
                Num_Pre := 0;

                Worked := true;
                Need := False;

                Break;
              end;
            if Op = nil then Kill(etUnknownIdentifier);
          end
          else // Variable
          begin
            Temp := Copy(S, 1, i-1);
            TempVar := nil;
            for i := 0 to fVariables.Length-1 do
              if AnsiSameText(fVariables[i].Name, Temp) then
              begin
                TempVar := fVariables[i].Variable;
                Break;
              end;
            if TempVar = nil then Kill(etUnknownIdentifier);
            InternalDelete(S, 1, Length(Temp));
            
            AppendNum(TempVar);
            
            if NumDiff < 0 then
              NumDiff0;

            Dec(NumDiff);
            Num_Pre := 0;    

            Worked := true;
            Need := False;
          end;
        end;
      end;
    end;
  end;

  if (NumDiff > 0) or (Num_Pre <> 0) or Need then
    if Need then
      Kill(etNumberExpected)
    else
      Kill(etOther);

  Result.OpStack := OpStack;
  Result.NumStack := NumStack;
end;

function TExCQParser._PreSolve(P: TParseRec): TParseRec;
var
  PS: array of Integer;
  NS: TNumArray;
  i, x, j, min, nc, z, y: Integer;
  b: boolean;

  procedure DelNum(From: Integer);
  var j: Integer;
  begin
    if From+1 <= High(P.NumStack) then
      for j := From to Length(P.NumStack)-From-1 do
        P.NumStack[j] := P.NumStack[j+1];
    SetLength(P.NumStack, High(P.NumStack));
  end;

  procedure DelOp(From, Count: Integer);
  var j: Integer;
  begin
    if From+Count <= High(P.OpStack) then
      for j := 0 to Length(P.OpStack)-Count-1 do
        P.OpStack[j+From] := P.OpStack[j+From+Count];
    SetLength(P.OpStack, Length(P.OpStack)-Count);
  end;

label
  p0, p1, p2;
begin
  if (Length(P.OpStack) = 0) or not fPreSolve then
  begin
    Result := P;
    Exit;
  end;

  for i := 0 to High(P.OpStack) do
  begin
    b := True;
    for x := 0 to High(PS) do
      if PS[x] = P.OpStack[i].Priority then
      begin
        b := False;
        Break;
      end;

    if b then
    begin
      SetLength(PS, Length(PS)+1);
      PS[High(PS)] := P.OpStack[i].Priority;
    end;
  end;

  for i := 0 to High(PS)-1 do
  begin
    min := i;
    for j := i+1 to High(PS) do
      if (PS[j] < PS[min]) then min := j;
    j := PS[i];
    PS[i] := PS[min];
    PS[min] := j;
  end;

  for i := High(PS) downto 0 do
  begin
p0:
    j := 0;
    nc := -1;
    for x := 0 to High(P.OpStack) do
    begin
      case P.OpStack[x].OpType of
        opOpen: Inc(j);
        opClose: Dec(j);
      end;
      if not (P.OpStack[x].OpType in [opOpen, opClose]) and
         (nc < High(P.NumStack)) and (P.OpStack[x].OpCallType <> octPostfix) then
        Inc(nc);
{$B-}
      if (j = 0) and (P.OpStack[x].Priority = PS[i]) and
         (P.OpStack[x].OpType = opCall_P0) then
      begin
        b := P.OpStack[x].CallType = ctFun;
        Dec(j, Integer(b));

        if P.OpStack[x].OpType = opOpen then
          Continue;

        SetLength(NS, Length(NS)+1);
        Move(P.NumStack[0], NS[0], nc);
        NS[nc+1] := P.OpStack[x].Callee(nil, nil, New);
        Move(P.NumStack[nc], NS[nc+1], Length(NS)-nc);
        P.NumStack := NS;

        DelOp(x, Integer(b)+1);

        goto p0;
      end;
    end;

p1:
    j := 0;
    nc := -1;
    for x := 0 to High(P.OpStack) do
    begin
      case P.OpStack[x].OpType of
        opOpen: Inc(j);
        opClose: Dec(j);
      end;
      if not (P.OpStack[x].OpType in [opOpen, opClose]) and
         (nc < High(P.NumStack)) and (P.OpStack[x].OpCallType <> octPostfix) then
        Inc(nc);
{$B-}
      if (j = 0) and (P.NumStack[nc].Constant) and (P.OpStack[x].Priority = PS[i])
        and (P.OpStack[x].OpType = opCall_P1) and P.NumStack[nc].Constant then
      begin
        b := P.OpStack[x].CallType = ctFun; 
        Dec(j, Integer(b));

        z := 0;
        y := 0;
        for min := x to High(P.OpStack) do
        begin
          if (P.OpStack[min].CallType = ctFun) and (P.OpStack[min].OpType <> opClose) then
            Inc(z);
          case P.OpStack[min].OpType of
            opOpen: Inc(z);
            opClose: Dec(z);
          end;
          if z = 0 then Break;
          Inc(y);
        end;

        if (y <> 1) and b then
          Continue;

        if P.OpStack[x].OpType = opOpen then
          Continue;

        P.NumStack[nc] := P.OpStack[x].Callee(P.NumStack[nc], nil, New);

        DelOp(x, Integer(b)+1);

        goto p1;
      end;
    end;

p2:
    j := 0;
    nc := -1;
    for x := 0 to High(P.OpStack) do
    begin
      case P.OpStack[x].OpType of
        opOpen: Inc(j);
        opClose: Dec(j);
      end;
      if not (P.OpStack[x].OpType in [opOpen, opClose]) and
         (nc < High(P.NumStack)) and (P.OpStack[x].OpCallType <> octPostfix) then
        Inc(nc);
{$B-}
      if (j = 0) and (P.NumStack[nc].Constant) and (P.OpStack[x].Priority = PS[i])
        and (P.OpStack[x].OpType = opCall_P2) and P.NumStack[nc].Constant and
        P.NumStack[nc+1].Constant then
      begin
        if (nc <> High(P.NumStack)) then
          if not P.NumStack[nc].Constant then
            Continue;

        b := P.OpStack[x].CallType = ctFun;
        Dec(j, Integer(b)); 

        z := 0;
        y := 0;
        for min := x to High(P.OpStack) do
        begin
          if (P.OpStack[min].CallType = ctFun) and (P.OpStack[min].OpType <> opClose) then
            Inc(z);
          case P.OpStack[min].OpType of
            opOpen: Inc(z);
            opClose: Dec(z);
          end;
          if z = 0 then Break;
          Inc(y);
        end;

        if ((y <> 2) and b) or (P.OpStack[x].OpType = opOpen) then
          Continue;

        P.NumStack[nc] := P.OpStack[x].Callee(P.NumStack[nc], P.NumStack[nc+1], New);

        DelNum(nc+1);

        DelOp(x, Integer(b)+1);

        goto p2;
      end;
    end;
  end;
  Result := P;
end;

function TExCQParser.Extract(F: TFunctionClass): TECQPFunction;
begin           
  if not F.Supports(fMode) then
    raise EModeUnsupported.Create('Parsing Mode unsupported by ' + F.ClassName);
  case fMode of
    pmReal: Result := F.Float;
    pmComplex: Result := F.Complex;
    pmQuad: Result := F.Quad;
  end;
end;

function TExCQParser.Extract(O: TOperator): Integer;
begin
  with TFunctionItem.Create(O.FunctionClass, '', true) do
  begin
    Result := Integer(EAX);
    Free;
  end;
end;

function TExCQParser._Serialize(Context: TParseRec): TAtomicOp;
var
  Op, Stream, StreamStart: TAtomicOp;
  i: Integer;
  HadOp: Boolean;

  procedure Serialize(Ops: TOCArray; Call: Boolean = false);
  var
    i, osTop, x, z: Integer;
    OpDump: TOCArray;
    Dont: Boolean;
    varcount: Integer;

    procedure AppendStream(S: TAtomicOp);
    begin
      if StreamStart <> nil then
      begin
        Stream.Next := S;
        Stream := Stream.Next;
      end
      else
      begin
        Stream := S;
        StreamStart := Stream;
      end;
      case S.OpType of
        opPop: Inc(varcount);
        opCall_P2: Dec(varcount);
      end;
    end;

    function Pop: TOperatorContext;
    begin
      Result := OpDump[osTop];
      SetLength(OpDump, osTop);
      Dec(osTop);
    end;

    function Current: TOperatorContext;
    begin
      if (Length(OpDump) > osTop) and (osTop >= 0) then
        Result := OpDump[osTop];
    end;

    procedure Push(OC: TOperatorContext);
    begin
      Inc(osTop);
      if High(OpDump) < osTOp then
        SetLength(OpDump, osTop+1);
      OpDump[osTop] := OC;
      HadOp := True;
    end;

    procedure E1;
    begin
      if ((varcount < 1) and (Current.OpType = opCall_P1)) or
         (( varcount < 2) and (Current.OpType = opCall_P2)) then
      begin
        Op := TAtomicOp.Create;
        Op.OpType := opPop;
        AppendStream(Op);
      end;

      Op := TAtomicOp.Create;
      Op.OpType := Current.OpType;
      Op._Func := Current.Callee;
      Op.EAX := Current.EAX;
      Op._Class := Current._Class;
      AppendStream(Op);
      Pop;
    end;

    procedure FlushOps;
    begin
      if (i <> High(Ops)) and (Ops <> nil) then
      begin
        if Ops[i+1].Priority <= Current.Priority then
        begin
          while (Length(OpDump) > 0) and (Current.Priority >= Ops[i+1].Priority) do
            E1;
        end;
      end
      else
      begin
        while Length(OpDump) > 0 do
          E1;
      end;
    end;

  begin
    osTop := -1;
    i := 0;
    varcount := 0;
    if Ops <> nil then
    begin
      if (Ops[0].OpType <> opOpen) and (Ops[0].CallType <> ctFun) and Call then
      begin
        Op := TAtomicOp.Create;
        Op.OpType := opPop;
        AppendStream(Op);
      end;
    end
    else
      begin
        Op := TAtomicOp.Create;
        Op.OpType := opPop;
        AppendStream(Op);
      end;
    while i <= High(Ops) do
    begin
      Dont := False;
      case Ops[i].OpType of
        opCall_P0:
          begin
            Push(Ops[i]);
          end;
        opCall_P1:
          begin
            Push(Ops[i]);
            if Ops[i].CallType = ctOp then
            begin
              Op := TAtomicOp.Create;
              Op.OpType := opPop;
              AppendStream(Op);
            end
            else
            begin
              z := 0;
              for x := i to High(Ops) do
              begin
                case Ops[x].OpType of
                  opOpen: Inc(z);
                  opClose: Dec(z);
                else
                  if Ops[x].CallType = ctFun then
                    Inc(z);
                end;

                if z = 0 then
                begin
                  Serialize(Copy(Ops, i+1, x-i-1));
                  Inc(varcount);
                  i := x;
                  Break;
                end;
              end;
            end;
          end;
        opCall_P2:
          begin
            Push(Ops[i]);
            if Ops[i].CallType = ctOp then
            begin
              if i <> High(Ops) then
                if Ops[i+1].OpType = opOpen then
                begin
                  Dont := True;
                end
                else
              else
              begin
                Op := TAtomicOp.Create;
                Op.OpType := opPop;
                AppendStream(Op);
              end;
            end
            else
            begin
              z := 0;
              for x := i to High(Ops) do
              begin
                case Ops[x].OpType of
                  opOpen: Inc(z);
                  opClose: Dec(z);  
                else
                  if Ops[x].CallType = ctFun then
                    Inc(z);
                end;

                if z = 0 then
                begin
                  Serialize(Copy(Ops, i+1, x-i-1));
                  Inc(varcount);
                  i := x;
                  Break;
                end;
              end;
              z := 0;
              for x := i to High(Ops) do
              begin
                case Ops[x].OpType of
                  opOpen: Inc(z);
                  opClose: Dec(z); 
                else
                  if Ops[x].CallType = ctFun then
                    Inc(z);
                end;

                if z = 0 then
                begin
                  Serialize(Copy(Ops, i+1, x-i-1));
                  Inc(varcount);
                  i := x;
                  Break;
                end;
              end;
            end;
          end;
        opOpen:
          begin
            z := 0;
            for x := i to High(Ops) do
            begin
              case Ops[x].OpType of
                opOpen: Inc(z);
                opClose: Dec(z);
              else
                if Ops[x].CallType = ctFun then
                  Inc(z);
              end;

              if z = 0 then
              begin
                Serialize(Copy(Ops, i+1, x-i-1), True);
                Inc(varcount);
                i := x;
                Break;
              end;
            end;
          end;
      end;
      if not Dont then
        FlushOps;
      Inc(i);
    end;
    FlushOps;
  end;

begin
  fEvalStack.Free;
  fRealStack.Free;

  FreeOps;

  fEvalStack := TVariableStack.Create(Length(Context.NumStack));
  fRealStack := TVariableStack.Create(Length(Context.NumStack));
  for i := High(Context.NumStack) downto 0 do
    fRealStack.Push(Context.NumStack[i]);

  HadOp := False;

  Stream := nil;
  StreamStart := nil;

  if Length(Context.OpStack) = 0 then
  begin
    Result := TAtomicOp.Create;
    Result.OpType := opPop;
  end
  else
  begin
    Serialize(Context.OpStack, True);
    Result := StreamStart;
    if StreamStart.Next = nil then
      Exit;
  end;

  SetLength(fExpendableVars, Length(Context.OpStack));

  if Length(Context.OpStack) <> 0 then
    for i := 0 to Length(Context.OpStack)-1 do
      fExpendableVars[i] := TVariable.Create(True, False);
end;

function TExCQParser._ParaClean(R: TParseRec): TParseRec;
var i: Integer;
begin
  SetLength(Result.OpStack, 0);
  Result.NumStack := R.NumStack;
  for i := 0 to High(R.OpStack) do
  begin
{$B-}
    if ((R.OpStack[i].OpType = opOpen) and (R.OpStack[i+1].OpType = opClose)) or
       (((i <> 0) and (R.OpStack[i-1].OpType = opOpen) and (R.OpStack[i].OpType = opClose))) then
    else
    begin
      SetLength(Result.OpStack, Length(Result.OpStack)+1);
      Result.OpStack[High(Result.OpStack)] := R.OpStack[i];
    end;
  end;
end;

procedure TExCQParser.Parse(S: string);
var Result: TParseRec;
begin
  Clear;
  fFormula := StringReplace(S, ' ', '', [rfReplaceAll]);
  fCurPos := 0;
  Result := _Parse(fFormula);
  Result := _ParaClean(Result);
  Result := _PreSolve(Result);
  Result := _ParaClean(Result);
  fOperations := _Serialize(Result);
  if fSolveMode = smRM then
    Compile;
end;

function TExCQParser.Solve: TVariable;
var C: TAtomicOp;
begin
  if fSolveMode = smRM then
  begin
    Result := fExpressionCode;
    Exit;
  end;

  fRealStack.Enter;

  fExpendableStackTop := 0;
  
  C := fOperations;

  while C <> nil do
  begin
    case C.OpType of
      opPop: fEvalStack.Push(fRealStack.Pop);
      opCall_P0: fEvalStack.Push(C._Func(nil, nil, New));
      opCall_P1: fEvalStack.Push(C._Func(fEvalStack.Pop, nil, New));
      opCall_P2: fEvalStack.Push(C._Func(fEvalStack.Pop, fEvalStack.Pop, New));
    end;
    C := C.Next;
  end;

  fRealStack.Leave;
  Result := fEvalStack.Pop;
end;

function TExCQParser.Call: TVariable;
asm
  push ebx
  push ecx
  push edx
  call dword ptr [eax].TExCQParser.fExpressionCode
  pop edx
  pop ecx
  pop ebx
end;

procedure TExCQParser.Compile;
var
  C: TAtomicOp;
  HasData, Has2Data: Boolean;

  function NextOpCall: Integer;
  begin
    if C.Next = nil then
       Result := 2
    else
      if C.Next.OpType in [opCall_P1, opCall_P2] then
        Result := 1
      else
        Result := 0;
  end;

  function Next2OpCall: Integer;
  begin
    if C.Next = nil then
      Result := 0
    else
      with C.Next do
      if Next = nil then
         Result := 2
      else
        if Next.OpType = opCall_P2 then
          Result := 1
        else
          Result := 0;
  end;

begin
  if fOperations = nil then
    Exit;
  with fAssembler do
  begin

    Init;

    C := fOperations;

    HasData := False;
    Has2Data := False;     

    MOV(rEBX, Integer(fRealStack.Fields)+fRealStack.Size*4-4);

    while C <> nil do
    begin
      case C.OpType of
        opPop:
          begin
            case NextOpCall of
              0: begin
                   if Next2OpCall = 1 then
                   begin
                     MOV_DR(rEDX, rEBX);
                     Has2Data := True;
                   end
                   else
                   begin
                     PUSH_DR(rEBX);
                     HasData := False;
                   end;
                 end;
              1: begin
                   if C.Next.OpType = opCall_P1 then
                     MOV_DR(rEDX, rEBX)
                   else
                     MOV_DR(rECX, rEBX);
                   HasData := True;
                 end;
              2: begin
                   MOV_DR(rEAX, rEBX);
                 end;
            end;
            if NextOpCall <> 2 then
              SUB(rEBX, 4);
          end;
        opCall_P0:
          begin
            X_OR(rECX, rECX);
            X_OR(rEDX, rEDX);
            MOV(rEAX, C.EAX);
            PUSH(Integer(New));
            case fMode of
              pmReal: CALL(C._Class.MethodAddress('Float'));
              pmComplex: CALL(C._Class.MethodAddress('Complex'));
              pmQuad: CALL(C._Class.MethodAddress('Quad'));
            end;
            case NextOpCall of
              0: begin
                   if Next2OpCall = 1 then
                   begin
                     MOV(rEDX, rEAX);
                     Has2Data := True;
                   end
                   else
                   begin
                     PUSH(rEAX);
                     HasData := False;
                   end;
                 end;
              1: begin
                   if C.Next.OpType = opCall_P1 then
                     MOV(rEDX, rEAX)
                   else
                     MOV(rECX, rEAX);
                   HasData := True;
                 end;
            end;
          end;
        opCall_P1:
          begin
            if not HasData then
              POP(rEDX);
            HasData := False;
            X_OR(rECX, rECX);
            MOV(rEAX, C.EAX);
            PUSH(Integer(New));
            case fMode of
              pmReal: CALL(C._Class.MethodAddress('Float'));
              pmComplex: CALL(C._Class.MethodAddress('Complex'));
              pmQuad: CALL(C._Class.MethodAddress('Quad'));
            end;
            case NextOpCall of
              0: begin
                   if Next2OpCall = 1 then
                   begin
                     MOV(rEDX, rEAX);
                     Has2Data := True;
                   end
                   else
                   begin
                     PUSH(rEAX);
                     HasData := False;
                   end;
                 end;
              1: begin
                   if C.Next.OpType = opCall_P1 then
                     MOV(rEDX, rEAX)
                   else
                     MOV(rECX, rEAX);
                   HasData := True;
                 end;
            end;
          end;
        opCall_P2:
          begin    
            if not HasData then
              POP(rECX);
            if not Has2Data then
              POP(rEDX);
            HasData := False;
            Has2Data := False;
            MOV(rEAX, C.EAX);
            PUSH(Integer(New));
            case fMode of
              pmReal: CALL(C._Class.MethodAddress('Float'));
              pmComplex: CALL(C._Class.MethodAddress('Complex'));
              pmQuad: CALL(C._Class.MethodAddress('Quad'));
            end;
            case NextOpCall of
              0: begin
                   if Next2OpCall = 1 then
                   begin
                     MOV(rEDX, rEAX);
                     Has2Data := True;
                   end
                   else
                   begin
                     PUSH(rEAX);
                     HasData := False;
                   end;
                 end;
              1: begin
                   if C.Next.OpType = opCall_P1 then
                     MOV(rEDX, rEAX)
                   else
                     MOV(rECX, rEAX);
                   HasData := True;
                 end;
            end;
          end;
      end;
      C := C.Next;
    end;

    RET;

    @fExpressionCode := fAssembler.Assembly;

  end;
end;

procedure TExCQParser.FreeOps;
var
  tao: TAtomicOp;
  t: TAtomicOp;
begin
  tao := fOperations;
  while tao <> nil do
  begin
    t := tao;
    tao := tao.Next;
    t.Free;
  end;
end;

function TExCQParser.New: TVariable;
begin
  if fExpendableStackTop = Length(fExpendableVars) then
  begin
    Result := TVariable.Create(True);
    fOtherVars.Add(
      TVariableItem.Create(Result, '')
    );
  end
  else
  begin
    Result := fExpendableVars[fExpendableStackTop];
    inc(fExpendableStackTop);
  end;
end;

procedure TExCQParser.Clear;
var i: Integer;
begin
  for i := 0 to High(fExpendableVars) do
  try
    fExpendableVars[i].Free;
  finally
    fExpendableVars[i] := nil;
  end;
  Finalize(fExpendableVars);
  fExpendableStackTop := 0;
end;

procedure TExCQParser.AdjustMode(New: TParserMode);
var C: TAtomicOp;
begin
  fMode := New;
  C := fOperations;
  while C <> nil do
  begin
    if C.OpType in [opCall_P0, opCall_P1, opCall_P2] then
      C._Func := Extract(C._Class);
    C := C.Next;
  end;
  if fSolveMode = smRM then
    Compile;
end;

end.
