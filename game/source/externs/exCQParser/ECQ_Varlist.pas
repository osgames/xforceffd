unit ECQ_Varlist;

interface

uses
  ECQ_Def, SysUtils;

type
  TVariableItem = class
  private
    fName: string;
    fVariable: TVariable;
  public
    constructor Create(Variable: TVariable; Name: string); virtual;
    destructor Destroy; override;

    property Variable: TVariable read fVariable write fVariable;
    property Name: string read fName write fName;
  end;

  TVariableList = class
  private
    fItems: array of TVariableItem;

    function GetItem(Index: Integer): TVariableItem;
    procedure SetItem(Index: Integer; New: TVariableItem);
    procedure Grow;
  public
    destructor Destroy; override; 

    property Items[Index: Integer]: TVariableItem read GetItem write SetItem; default;

    procedure Add(Item: TVariableItem);
    procedure Delete(Index: Integer);

    function Length: Integer;
  end;

implementation

constructor TVariableItem.Create(Variable: TVariable; Name: string);
begin
  inherited Create;
  fName := Name;
  fVariable := Variable;
end;

destructor TVariableList.Destroy;
var i: Integer;
begin
  for i := 0 to Length-1 do
  try
    Items[i].Free;
  finally
    Items[i] := nil;
  end;   
  SetLength(fItems, 0);
  inherited;
end;

function TVariableList.GetItem(Index: Integer): TVariableItem;
begin
  Result := fItems[Index];
end;

procedure TVariableList.SetItem(Index: Integer; New: TVariableItem);
begin
  fItems[Index] := New;
end;

procedure TVariableList.Grow;
begin
  SetLength(fItems, System.Length(fItems)+1);
end;

procedure TVariableList.Add(Item: TVariableItem);
begin
  Grow;
  fItems[High(fItems)] := Item;
end;

procedure TVariableList.Delete(Index: Integer);
begin
  Move(fItems[Index+1], fItems[Index], System.Length(fItems)-Index);
  SetLength(fItems, High(fItems));
end;

function TVariableList.Length: Integer;
begin
  Result := System.Length(fItems);
end;

destructor TVariableItem.Destroy;
begin
  fVariable.Free;
  inherited;
end;

end.
