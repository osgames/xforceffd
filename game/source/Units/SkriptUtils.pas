{********************************************************************************
*										*
* Copyright (C) 2004  X-Force Team - jim_raynor@web.de				*
*										*
* This file is part of X-Force: Fight For Destiny (X-Force).			*
*										*
* X-Force is free software; you can redistribute it and/or modify		*
* it under the terms of the GNU General Public License as published by		*
* the Free Software Foundation; either version 2 of the License, or		*
* (at your option) any later version.						*
*										*
* X-Force is distributed in the hope that it will be useful,			*
* but WITHOUT ANY WARRANTY; without even the implied warranty of		*
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the			*
* GNU General Public License for more details.					*
*										*
* You should have received a copy of the GNU General Public License		*
* along with X-Force; if not, write to the Free Software			*
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA	*
*										*
*********************************************************************************
*										*
* Utilities um f�r die Skriptsprache						*
*										*
********************************************************************************}

{$I ../Settings.inc}

unit SkriptUtils;

interface

uses
  MissionSkriptEngine, XForce_types, Dialogs, SysUtils, TypInfo, TraceFile,Classes,
  Windows;

function HashCommandName(Name: String): Cardinal;
function GetLaenderName(Index: Integer): String;

function ToHexString(Ptr: Pointer; Size: Integer): String;
function SaveToString(Text: String): String;
function ReadFromString(var Text: String): String;
procedure HexToMem(var Hex: String;Ptr: Pointer; Size: Integer);
procedure CheckObject(Value: TValue;Obj: String);

procedure RegisterMissionCommands(Engine: TMissionSkriptEngine; Editor: Boolean = true);

procedure RegisterMapCommands(Engine: TMissionSkriptEngine; Editor: Boolean = true);

implementation

const HashTable: Array[Byte] of Cardinal =
  ($646379E0,$1BD5A767,$30BDE508,$4A11DB2A,$58B373D6,$1DF86135,$61A9BE0D,$52C43048,
   $1102956D,$41856123,$36D175B4,$646C5E86,$031374A3,$58A19531,$45A5B1F9,$2FD5A3E4,
   $5ED64778,$5ED60B5F,$130BB8E0,$35925E62,$0F9EE3EF,$571DF1AD,$21A37066,$255A0C00,
   $10FAC005,$4C14961B,$2AAD5E8C,$12874ABE,$6D26F1BA,$204366A9,$6790A951,$7BB5D89C,
   $5B262F10,$7C8FF157,$720116B7,$4380939A,$1276CE07,$4AC9E425,$60C50CBD,$585F79B8,
   $0A68C49D,$4A590D13,$671B9163,$69A5A8F6,$7B13A8D2,$6D0B5A21,$5CC24AA9,$279F5F54,
   $15ECB0A9,$4A02D94F,$349B7E90,$2CCFFAD2,$0BFCB21F,$14C3B89D,$07F41316,$4E4FF970,
   $17B62335,$2662460B,$7C698E3B,$314AF92E,$606B19EA,$46D0EF99,$6A701601,$239DB80C,
   $2D634C41,$044E4347,$4A787067,$5594140A,$7E921036,$06F2EF15,$0EB6036E,$32C70B28,
   $40EC5BCC,$6E5FC103,$2F84D514,$381ABB66,$745EC502,$678BA711,$506F8B59,$44DC62C4,
   $396381D9,$48B1AF3F,$03D56C40,$05005F42,$5C38684E,$215F078D,$53305DC5,$32802EE0,
   $4CA0FDFB,$51FAE5EB,$0BD86F9E,$49C02A1A,$33530089,$21362AB2,$7DA6DF7C,$6366D170,
   $0C8C9D37,$698FF217,$1D685C7A,$1E913A67,$0A2F8205,$7228A21D,$3356E498,$3B595AFD,
   $7F957CF3,$19F940C4,$316795D6,$4700C932,$305E7C01,$19D9740A,$1568AE34,$7E86BB08,
   $155E8D2F,$152581F0,$543F8BB2,$5EDE067E,$05ABDE7D,$1F045076,$0C46AC50,$18C32195,
   $51CCBDEB,$7A4D659B,$5CCBAE0E,$3E32224B,$7F059979,$7A0EE761,$60AD4EEC,$1F7CBEA1,
   $08B39BC8,$6A196CEA,$6E004C96,$6E3B9CF5,$7BC8E8CD,$3D6B0608,$5A7BC22C,$11F640E3,
   $29283846,$0505B563,$51BFD8F1,$5C2C04B9,$792041A4,$34A25C39,$6C85731F,$06F7BFA0,
   $62A98022,$6C798CAE,$2C663D6D,$711BEB25,$27FF71C0,$14ACBCC5,$0AE185DB,$0E4D0D4C,
   $50C0B47E,$0CCD027B,$6524BA69,$0D264C12,$758D065C,$1DF113D1,$0E796917,$6B4F6D77,
   $3CC3455A,$626B46C6,$2ED33FE5,$0642D77E,$465F6F78,$591F915C,$7B7E0CD3,$6EB39023,
   $64F8A2B6,$7A798992,$77EBBDE1,$64933D69,$21DF1D14,$45026568,$0CA2610F,$01B82550,
   $2A5A3C92,$5796FADE,$624A245D,$39232DD6,$4F3DBFF4,$0EDB55CB,$70E5DCFB,$065382EE,
   $3E9CCAAB,$42EC6359,$1CA858C2,$372205CC,$350FD101,$451FDB07,$5ECF6727,$4881E5CA,
   $6B5E28F6,$29B26AD5,$56426E2D,$559020E8,$4C10C88C,$5428E0C3,$5ED173D3,$1C74D526,
   $2D6845C3,$711E2AD1,$2A3B1E1A,$13814084,$32F2D699,$363156FF,$37D2B300,$576DC102,
   $6CC2510E,$5613934D,$50C61885,$30B7D4A0,$6A422B24,$36B62DBB,$4F03D4AB,$0E20195E,
   $16AD7ADB,$17989449,$14C10D72,$72484D3C,$5524F630,$763654F7,$3A9F88D8,$72714E3A,
   $7264F326,$1E951DC5,$1A73ACDE,$04591A58,$221B67BD,$75F2BCB3,$7CAA7F83,$7938CF96,
   $2D931FC1,$4E4FA6C9,$23E2ABF4,$1BBFAFC9,$2BAE54EF,$65B368AF,$48000D72,$72878F3E,
   $04654D39,$0B9C6A22,$45DBDCAC,$087C8361,$0538D6E7,$602BD287,$51AD7EAA,$5B0BA556,
   $2F3758B5,$6B82938D,$7E165BC8,$240B6EED,$7CD7A0A3,$442AB333,$16E09206,$05E67623);


function HashCommandName(Name: String): Cardinal;
var
  Dummy: Integer;
begin
  Name:=lowercase(Name);
  result:=0;
  for Dummy:=1 to length(Name) do
  begin
    result:=(HashTable[Byte(Name[Dummy])] xor result);
    asm
      ROL Result,2
    end;
  end;
end;

function GetLaenderName(Index: Integer): String;
begin
  case Index of
    0 : result:='Amerika';
    1 : result:='Kanada';
    2 : result:='Europ�ische Union';
    3 : result:='Gr�nland';
    4 : result:='Mexico';
    5 : result:='Australien';
    6 : result:='Russland';
    7 : result:='S�dafrika';
    8 : result:='China';
    9 : result:='Brasilien';
    10: result:='Indonesion';
    11: result:='Indien';
    12: result:='Japan';
    13: result:='Saudi-Arabien';
    14: result:='Algerien';
    15: result:='Argentinien';
    16: result:='Neuseeland';
    17: result:='Peru';
    18: result:='Antarktis';
    19: result:='Kasachstan';
    20: result:='Madagaskar';
    21: result:='Thailand';
    22: result:='Marshall-Inseln';
    23: result:='Mikronesien';
    24: result:='�gypten';
  end;
end;

procedure RegisterMissionCommands(Engine: TMissionSkriptEngine; Editor: Boolean);
var
  Modell      : TRecordDiscriptor;
  UFO         : TRecordDiscriptor;
  Event       : TRecordDiscriptor;
  Einsatz     : TRecordDiscriptor;
  Temp        : TRecordDiscriptor;
  Item        : TRecordDiscriptor;
  Country     : TRecordDiscriptor;
  Transporter : TRecordDiscriptor;
  Basis       : TRecordDiscriptor;
  Einrichtung : TRecordDiscriptor;
  Dummy       : Integer;

  procedure AddEnum(TypeInfo: PTypeInfo);
  var
    Data  : PTypeData;
    Dummy : Integer;
  begin
    Data:=GetTypeData(TypeInfo);
    for Dummy:=Data.MinValue to Data.MaxValue do
    begin
      Engine.AddVariable(GetEnumName(TypeInfo,Dummy),IntToStr(Dummy),vtZahl,TypeInfo.Name);
    end;
  end;

begin
(*  Engine.AddVariable('false','false',vtBoolean);
  Engine.AddVariable('true','true',vtBoolean);

  // UFO
  UFO:=Engine.NewRecord('TUFO');
  UFO.ExternalSize:=sizeOf(Integer);
  UFO.AddMember('Name',vtString,BuildMemberSource(mstMethodCall));
  UFO.AddMember('Typ',vtString,BuildMemberSource(mstMethodCall),true);
  UFO.AddEvent('OnDestroy',BuildMemberSource(mstMethodCall));
  UFO.AddMethod('HuntObject',1,vtNone);
  UFO.AddMethod('SetPosition',2,vtNone);

  // UFOModell
  Modell:=Engine.NewRecord('TUFOModel');
  Modell.ExternalSize:=SizeOf(TUFOModel);
  Modell.AddMember('Name',vtString,BuildMemberSource(mstShortString,0,30));
  Modell.AddMember('Hitpoints',vtZahl,BuildMemberSource(mstWord,35));
  Modell.AddMember('ProjektilAbwehr',vtZahl,BuildMemberSource(mstByte,39));
  Modell.AddMember('RaketenAbwehr',vtZahl,BuildMemberSource(mstByte,40));
  Modell.AddMember('LaserAbwehr',vtZahl,BuildMemberSource(mstByte,41));
  Modell.AddMethod('CreateUFO',0,vtRecord,UFO);

  UFO.AddMember('Model',vtRecord,BuildMemberSource(mstMethodCall),true,Modell);

  // Ausr�stungsliste
  AddEnum(TypeInfo(TWaffenType));

  // TProjektType
  for Dummy:=Integer(low(TProjektType)) to Integer(high(TProjektType)) do
    if TProjektType(Dummy) in LagerItemType then
      Engine.AddVariable(GetEnumName(TypeInfo(TProjektType),Dummy),IntToStr(Dummy),vtZahl,'TProjektType');

// Ausr�stung
  Item:=Engine.NewRecord('TItem');
  if Editor then
  begin
    Item.ExternalSize:=SizeOf(TForschProject);
    Item.AddMember('Name',vtString,BuildMemberSource(mstShortString,0,50),true);
    Item.AddMember('Kaufpreis',vtZahl,BuildMemberSource(mstInteger,79),true);
    Item.AddMember('Verkaufspreis',vtZahl,BuildMemberSource(mstInteger,83),true);
    Item.AddMember('ItemTyp',vtZahl,BuildMemberSource(mstByte,PTypeInfo(TypeInfo(TProjektType))^.Name,91),true);
    Item.AddMember('Count',vtZahl,BuildMemberSource(mstInteger,134),true);
  end
  else
  begin
    Item.ExternalSize:=SizeOf(TLagerItem);
    Item.AddMember('Name',vtString,BuildMemberSource(mstShortString,0,50),true);
    Item.AddMember('Kaufpreis',vtZahl,BuildMemberSource(mstInteger,70),true);
    Item.AddMember('Verkaufspreis',vtZahl,BuildMemberSource(mstInteger,74),true);
    Item.AddMember('ItemTyp',vtZahl,BuildMemberSource(mstByte,PTypeInfo(TypeInfo(TProjektType))^.Name,82),true);
    Item.AddMember('Count',vtZahl,BuildMemberSource(mstWord,51),true);
  end;
  Item.AddMethod('RemoveItems|Count',1,vtBoolean);

  Temp:=Engine.NewRecord('TItemList',false);
  Temp.AddMember('Count',vtZahl,BuildMemberSource(mstMethodCall),true);
  Temp.AddMethod('Items|Index',1,vtRecord,Item);

  Engine.AddVariable('ItemList','',vtRecord,'Verwaltung',Temp);

  // L�nderliste
  Country:=Engine.NewRecord('TCountry');
  Country.ExternalSize:=SizeOf(TLand);
  Country.AddMember('Name',vtString,BuildMemberSource(mstLongString,0),true);
  Country.AddMember('Index',vtZahl,BuildMemberSource(mstByte,9),true);
  Country.AddMember('Confident',vtZahl,BuildMemberSource(mstByte,10),true);
  Country.AddMethod('ChangeConfident|Confident',1,vtNone);
  Country.AddMethod('GetRandomX',0,vtZahl);
  Country.AddMethod('GetRandomY',0,vtZahl);

  Temp:=Engine.NewRecord('TCountryList',false);
  Temp.AddMember('Count',vtZahl,BuildMemberSource(mstMethodCall),true);
  Temp.AddMethod('Countries|Index',1,vtRecord,Country);

  Engine.AddVariable('CountryList','',vtRecord,'Verwaltung',Temp);

  // Transporter
  Transporter:=Engine.NewRecord('TTransporter');
  if Editor then
  begin
    Transporter.ExternalSize:=SizeOf(Cardinal);
    Transporter.AddMember('Name',vtString,BuildMemberSource(mstMethodCall));
    Transporter.AddEvent('OnDestroy',BuildMemberSource(mstMethodCall));
    Transporter.AddEvent('OnDestinationReached',BuildMemberSource(mstMethodCall));
  end
  else
  begin
    Transporter.ExternalSize:=SizeOf(TMissionTransporter);
    Transporter.AddEvent('OnDestroy',BuildMemberSource(mstLongString,8));
    Transporter.AddEvent('OnDestinationReached',BuildMemberSource(mstLongString,12));
  end;
  Transporter.AddMethod('TransportFrom|Item,Count,Country',3,vtNone);
  Transporter.AddMethod('TransportTo|Item,Count,Country',3,vtNone);
//  Transporter.AddMethod('BuyFrom',3,vtNone);
//  Transporter.AddMethod('SellTo',3,vtNone);

  // Ereignisse
  Event:=Engine.NewRecord('TEvent');
  Event.ExternalSize:=sizeOf(TEreigniss);
  Event.AddMember('Active',vtBoolean,BuildMemberSource(mstBoolean,4));
  Event.AddMember('CalledEvent',vtString,BuildMemberSource(mstLongString,8));
  Event.AddMember('Minutes',vtZahl,BuildMemberSource(mstInteger,12));
  Event.AddMember('Repeated',vtBoolean,BuildMemberSource(mstBoolean,16));

  Einsatz:=Engine.NewRecord('TEinsatz');
  Einsatz.ExternalSize:=sizeOf(Cardinal);
  Einsatz.AddMethod('AddRandomAlien',0,vtNone);
  Einsatz.AddMethod('Start',0,vtBoolean);
  Einsatz.AddMethod('GetCountry',0,vtRecord,Country);
  Einsatz.AddEvent('OnWin',BuildMemberSource(mstMethodCall));
  Einsatz.AddEvent('OnTimeUp',BuildMemberSource(mstMethodCall));
  Einsatz.AddMember('Name',vtString,BuildMemberSource(mstMethodCall));
  Einsatz.AddMember('RemainTime',vtZahl,BuildMemberSource(mstMethodCall));
  Einsatz.AddMember('Description',vtString,BuildMemberSource(mstMethodCall));
  Einsatz.AddMember('Objectives',vtString,BuildMemberSource(mstMethodCall));
  Einsatz.AddMember('AlienCount',vtZahl,BuildMemberSource(mstMethodCall),true);

  // Eins�tze
  Temp:=Engine.NewRecord('TEinsatzList',false);
  Temp.AddMethod('CreateEinsatz',0,vtRecord,Einsatz);

  Engine.AddVariable('EinsatzList','',vtRecord,'Verwaltung',Temp);

  // Basis
  Basis:=Engine.NewRecord('TBasis');
  if Editor then
    Basis.ExternalSize:=sizeOf(String)
  else
    Basis.ExternalSize:=sizeOf(Cardinal);
  Basis.AddMethod('AddRoom|Room',1,vtNone);
//  Basis.AddMember('Name',vtString,BuildMemberSource(mstMethodCall),true);

  Temp:=Engine.NewRecord('TBasisList',false);
  Temp.ExternalSize:=sizeOf(Cardinal);
  Temp.AddMethod('MainBase',0,vtRecord,Basis);

  Engine.AddVariable('BasisList','',vtRecord,'Verwaltung',Temp);

  Einrichtung:=Engine.NewRecord('TRoom');
  Einrichtung.ExternalSize:=SizeOf(TEinrichtung);
  Einrichtung.AddMember('Name',vtString,BuildMemberSource(mstShortString,0,50));
  Einrichtung.AddMember('Price',vtZahl,BuildmemberSource(mstInteger,56));
  Einrichtung.AddMember('Hangar',vtZahl,BuildMemberSource(mstWord,60));
  Einrichtung.AddMember('Quarters',vtZahl,BuildMemberSource(mstWord,88));
  Einrichtung.AddMember('WorkshopSpace',vtZahl,BuildMemberSource(mstWord,92));
  Einrichtung.AddMember('LabSpace',vtZahl,BuildMemberSource(mstWord,98));
  Einrichtung.AddMember('Alphatron',vtZahl,BuildMemberSource(mstWord,102));


  { Funktionen }
  Engine.AddFunc('Message|Text',1,vtNone);
  Engine.AddFunc('Question|Text',1,vtBoolean);
  Engine.AddFunc('Random|Bereich',1,vtZahl);
  Engine.AddFunc('AendereUnterst�tzung',2,vtNone);
  Engine.AddFunc('AendereVertrauen',2,vtNone);
  Engine.AddFunc('ChangeCredits|Credits',1,vtNone);
  Engine.AddFunc('MissionLoose',0,vtNone);
  Engine.AddFunc('MissionWin',0,vtNone);
  Engine.AddFunc('MissionAbort',0,vtNone);
  Engine.AddFunc('RegisterEvent|Event,Minutes',2,vtRecord,Event);
  Engine.AddFunc('CalculatePercent|Value,Percent',2,vtZahl);
  Engine.AddFunc('SetMissionType|Missionsziel',1,vtNone);
  Engine.AddFunc('CreateUFO',0,vtRecord,UFO);
  Engine.AddFunc('CreateTransporter',0,vtRecord,Transporter);
  Engine.AddFunc('Format1|FormatStr,String1',2,vtString);
  Engine.AddFunc('Format2|FormatStr,String1,String2',3,vtString);
  Engine.AddFunc('Format3|FormatStr,String1,String2,String3',4,vtString);
  {$IFDEF DEBUG}
  Engine.AddFunc('HexDump',1,vtNone);
  {$ENDIF}

  Engine.AddVariable('LandAnzahl','25',vtZahl);
  { Init Standard L�ndernummern }
  Engine.AddVariable('Amerika','0',vtZahl,'L�nder');
  Engine.AddVariable('Kanada','1',vtZahl,'L�nder');
  Engine.AddVariable('Europa','2',vtZahl,'L�nder');
  Engine.AddVariable('Groenland','3',vtZahl,'L�nder');
  Engine.AddVariable('Mexico','4',vtZahl,'L�nder');
  Engine.AddVariable('Australien','5',vtZahl,'L�nder');
  Engine.AddVariable('Russland','6',vtZahl,'L�nder');
  Engine.AddVariable('Suedafrika','7',vtZahl,'L�nder');
  Engine.AddVariable('China','8',vtZahl,'L�nder');
  Engine.AddVariable('Brasilien','9',vtZahl,'L�nder');
  Engine.AddVariable('Indonesion','10',vtZahl,'L�nder');
  Engine.AddVariable('Indien','11',vtZahl,'L�nder');
  Engine.AddVariable('Japan','12',vtZahl,'L�nder');
  Engine.AddVariable('SaudiArabien','13',vtZahl,'L�nder');
  Engine.AddVariable('Algerien','14',vtZahl,'L�nder');
  Engine.AddVariable('Argentinien','15',vtZahl,'L�nder');
  Engine.AddVariable('Neuseeland','16',vtZahl,'L�nder');
  Engine.AddVariable('Peru','17',vtZahl,'L�nder');
  Engine.AddVariable('Antarktis','18',vtZahl,'L�nder');
  Engine.AddVariable('Kasachstan','19',vtZahl,'L�nder');
  Engine.AddVariable('Madagaskar','20',vtZahl,'L�nder');
  Engine.AddVariable('Thailand','21',vtZahl,'L�nder');
  Engine.AddVariable('MarshallInseln','22',vtZahl,'L�nder');
  Engine.AddVariable('Mikronesien','23',vtZahl,'L�nder');
  Engine.AddVariable('Aegypten','24',vtZahl,'L�nder');

  { Konstanten f�r SetzeMissionsziel }
  Engine.AddVariable('mtUser','0',vtZahl,'Missionsziel');
  Engine.AddVariable('mtDestroyUFOs','1',vtZahl,'Missionsziel');
*)
end;

procedure RegisterMapCommands(Engine: TMissionSkriptEngine; Editor: Boolean = true);
begin
  Engine.AddVariable('false','false',vtBoolean);
  Engine.AddVariable('true','true',vtBoolean);

  Engine.AddFunc('Random|Bereich',1,vtZahl);
  Engine.AddFunc('GetMapWidth',0,vtZahl);
  Engine.AddFunc('GetMapHeight',0,vtZahl);
  Engine.AddFunc('CopyRoomTo',3,vtNone);
  Engine.AddFunc('GetRoomAtPos',2,vtString);
  Engine.AddFunc('FillFreeRooms',1,vtNone);
end;

function ToHexString(Ptr: Pointer; Size: Integer): String;
var
  Dummy: Integer;
begin
  for Dummy:=1 to Size do
  begin
    result:=result+IntToHex(PByte(Ptr)^,2);
    inc(PByte(Ptr));
  end;
end;

function SaveToString(Text: String): String;
begin
  result:=IntToStr(Length(Text))+','+Text;
end;

function ReadFromString(var Text: String): String;
var
  Len: Integer;
begin
  Len:=StrToInt(copy(Text,1,Pos(',',Text)-1));
  delete(Text,1,Pos(',',Text));
  result:=copy(Text,1,Len);
  delete(Text,1,Len);
end;

procedure HexToMem(var Hex: String;Ptr: Pointer; Size: Integer);
var
  Dummy: Integer;
begin
  for Dummy:=1 to Size do
  begin
    PByte(Ptr)^:=StrToInt('$'+Copy(Hex,1,2));
    Delete(Hex,1,2);
    inc(PByte(Ptr));
  end;
end;

procedure CheckObject(Value: TValue;Obj: String);
begin
  if LowerCase(Value.AsRecord.RecordTyp.Name)<>LowerCase(Obj) then
    raise ERunTimeError.Create('Objekt vom Typ '+Obj+' erwartet, aber Objekt vom Typ '+Value.AsRecord.RecordTyp.Name+' gefunden.');
end;

end.
