{********************************************************************************
*										*
* Copyright (C) 2004  X-Force Team - jim_raynor@web.de				*
*										*
* This file is part of X-Force: Fight For Destiny (X-Force).			*
*										*
* X-Force is free software; you can redistribute it and/or modify		*
* it under the terms of the GNU General Public License as published by		*
* the Free Software Foundation; either version 2 of the License, or		*
* (at your option) any later version.						*
*										*
* X-Force is distributed in the hope that it will be useful,			*
* but WITHOUT ANY WARRANTY; without even the implied warranty of		*
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the			*
* GNU General Public License for more details.					*
*										*
* You should have received a copy of the GNU General Public License		*
* along with X-Force; if not, write to the Free Software			*
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA	*
*										*
*********************************************************************************
*										*
* Funktionen um Berechnung mit Erdkoordinaten durchzuf�hren (zum Beispiel	*
* entfernung)									*
*										*
********************************************************************************}

{$I ../Settings.inc}

unit EarthCalcs;

interface

uses XForce_types, math, SysUtils, D3DUtils, TraceFile, Windows;

type
  TPositionCallBack = procedure(Point: TFloatPoint; Data: Pointer;var Stop: boolean);
// Entfernung von zwei Punkten auf der Erde
function EarthEntfernung(VonPoint: TFloatPoint; ToPoint: TFloatPoint): Integer;

procedure EarthCalcRoute(VonPoint: TFloatPoint; ToPoint: TFloatPoint; Steps: double; Data: Pointer; CallBack: TPositionCallBack);
function EarthCalcFlight(var Position: TFloatPoint; ToPoint: TFloatPoint; CanRange: Integer; out IsRange : Integer): boolean;

implementation

procedure LimitPoint(var Pt: TFloatPoint);
begin
  if Pt.Y<=0 then
    Pt.Y:=0.05;
  if Pt.Y>=180-G_EPSILON then
    Pt.Y:=179.95;
end;

function EarthEntfernung(VonPoint: TFloatPoint; ToPoint: TFloatPoint): Integer;
var
  VonBreite, VonLaenge: double;
  ToBreite, ToLaenge  : double;
  distance : double;
begin
  VonLaenge:=abs(VonPoint.X-180)*Pi/180;
  VonBreite:=abs(VonPoint.Y-90)*Pi/180;
  ToLaenge:=abs(ToPoint.X-180)*Pi/180;
  ToBreite:=abs(ToPoint.Y-90)*Pi/180;

  if VonPoint.Y<90 then
    VonBreite:=-VonBreite;

  if ToPoint.Y<90 then
    ToBreite:=-ToBreite;

  if VonPoint.X<180 then
    VonLaenge:=-VonLaenge;
    
  if ToPoint.X<180 then
    ToLaenge:=-ToLaenge;

  distance:=arccos(sin(VonBreite)*sin(ToBreite)+cos(VonBreite)*cos(ToBreite)*cos(ToLaenge - VonLaenge));

  result:=round(distance * 6371);
end;

procedure Call(Point: TFloatPoint;const CallBack: TPositionCallBack;const Data: Pointer;var Stop: Boolean);
begin
  if Point.X<0 then
    Point.X:=Point.X+360
  else if Point.X>360 then
    Point.X:=Point.X-360;
  CallBack(Point,Data,Stop);
end;

procedure CallLine(FromPoint: TFloatPoint; ToPoint: TFloatPoint;Steps: double;const CallBack: TPositionCallBack;const Data: Pointer;var Stop: Boolean);
var
  XStep  : double;
  YStep  : double;
  Ende   : boolean;
  Init   : Integer;
begin
  Init:=round(Steps);
  repeat
    YStep:=(ToPoint.Y-FromPoint.Y)/Steps;
    Steps:=Steps+1;
  until abs(YStep)<(5/Init);

  if D3DMath_IsZero(YStep,G_EPSILON) then
  begin
    if YStep>0 then
      YStep:=G_EPSILON
    else
      YStep:=-G_EPSILON;
  end;

  XStep:=(ToPoint.X-FromPoint.X)/((ToPoint.Y-FromPoint.y)/YStep);

  repeat
//      Winkel:=Winkel+XStep;
    if YStep<0 then
      Ende:=FromPoint.Y<=ToPoint.Y
    else
      Ende:=FromPoint.Y>=ToPoint.Y;

    if Ende then
      FromPoint.Y:=ToPoint.Y;

    Call(FromPoint,CallBack,Data,Stop);

    FromPoint.X:=FromPoint.X+XStep;
    FromPoint.Y:=FromPoint.Y+YStep;
  until Stop or Ende;
end;

procedure EarthCalcRoute(VonPoint: TFloatPoint; ToPoint: TFloatPoint; Steps: double; Data: Pointer; CallBack: TPositionCallBack);
var
  Point  : TFloatPoint;

  Winkel : double;
  Bis    : double;
  Incr   : double;
  CX,CZ  : double;
  T13,T14,T15: double;
  T5,T6  : double;
  V5,V6  : double;
  U5,U6  : double;
  Stop   : boolean;
  Ende   : boolean;

  LastPoint : TFloatPoint;
  Phis,LmbS: double;
  SinPhiS,CosPhiS,SinLmbS,CosLmbS : double;
  PhiZ,LmbZ: double;
  SinPhiZ,CosPhiZ,SinLmbZ,CosLmbZ : double;

  Tan : double;

begin
  // Von
  LimitPoint(VonPoint);
  PhiS:=DegToRad(VonPoint.Y-90);
  LmbS:=DegToRad(180-(360-VonPoint.X));
  SinPhiS:=sin(PhiS);
  CosPhiS:=cos(PhiS);
  SinLmbS:=sin(LmbS);
  CosLmbS:=cos(LmbS);

  // Bis
  LimitPoint(ToPoint);
  PhiZ:=DegToRad(ToPoint.Y-90);
  LmbZ:=DegToRad(180-(360-ToPoint.X));
  SinPhiZ:=sin(PhiZ);
  CosPhiZ:=cos(PhiZ);
  SinLmbZ:=sin(LmbZ);
  CosLmbZ:=cos(LmbZ);

  T5:=CosPhiS*CosLmbS;
  T6:=CosPhiZ*CosLmbZ;
  V5:=CosPhiS*SinLmbS;
  V6:=CosPhiZ*SinLmbZ;
  U5:=SinPhiS;
  U6:=SinPhiZ;

  T13:=U5*V6-V5*U6;
  T14:=V5*T6-T5*V6;
  T15:=T5*U6-U5*T6;

  // Vermutlich liegen beide Punkte �bereinander
  if T14=0 then
  begin
    if (VonPoint.X=ToPoint.X) and (VonPoint.Y<>ToPoint.Y) then
      CallLine(VonPoint,ToPoint,Steps,CallBack,Data,Stop);
    exit;
  end;
  CX:=T13/T14;
  CZ:=T15/T14;

  Winkel:=VonPoint.X;
  Bis:=ToPoint.X;
  if Winkel-Bis<-180 then
    Bis:=Bis-360
  else if Winkel-Bis>180 then
    Bis:=Bis+360;

  Incr:=1/Steps;

  if (Incr>abs(VonPoint.X-ToPoint.X)) then
    Incr:=(ToPoint.X-VonPoint.X)/Steps
  else
  begin
    if Bis<Winkel then
      Incr:=-Incr;
  end;

  Stop:=false;
  if D3DMath_IsZero(Incr,G_EPSILON*2) then
  begin
    CallLine(VonPoint,ToPoint,Steps,CallBack,Data,Stop);
    exit;
  end;
  LastPoint:=VonPoint;
  repeat
    if Incr<0 then
      Ende:=Winkel<Bis
    else
      Ende:=Winkel>Bis;

    if Ende then
      Winkel:=Bis;

    Point.X:=Winkel;
    Point.Y:=90+-RadToDeg(ArcTan(-(CX*cos(DegToRad(Winkel))+CZ*Sin(DegToRad(Winkel)))));

    Winkel:=Winkel+Incr;

    if (abs(Point.Y-LastPoint.Y)>1) then
    begin
      CallLine(LastPoint,Point,Steps,CallBack,Data,Stop);
    end
    else
      Call(Point,CallBack,Data,Stop);

    LastPoint:=Point;
  until Stop or Ende;
//  Call(Point);
end;


type
  PCalcFlightData = ^TCalcFlightData;
  TCalcFlightData = record
    Position  : TFloatPoint;
    CanRange  : Integer;
    IsRange   : Integer;
    First     : boolean;
  end;

procedure CallBackEarthCalcFlight(Point: TFloatPoint; DataPtr: Pointer;var Stop: boolean);
var
  Entf: Integer;
  Data: PCalcFlightData;
begin
  Data:=DataPtr;
  if Data.First then
  begin
    Data.First:=false;
    exit;
  end;

  Entf:=EarthEntfernung(Data.Position,Point);
  if Entf=0 then
    exit;
    
  if Entf>Data.CanRange then
  begin
    Stop:=true;
    exit;
  end;

  inc(Data.IsRange,Entf);
  dec(Data.CanRange,Entf);
  Data.Position:=Point;
end;

function EarthCalcFlight(var Position: TFloatPoint; ToPoint: TFloatPoint; CanRange: Integer; out IsRange : Integer): boolean;
var
  Entf      : Integer;
  Data      : TCalcFlightData;
  Steps     : Integer;
begin
  Steps:=3;
  result:=false;
  LimitPoint(Position);
//  LimitPoint(ToPoint);
  // Pr�fen, ob das Objekt schon dicht genug an den Zielkoordinaten ist
  Entf:=EarthEntfernung(Position,ToPoint);
  if Entf<=CanRange then
  begin
    Position:=ToPoint;
    IsRange:=round(Entf);
    result:=true;
    exit;
  end;

  Data.Position:=Position;
  Data.CanRange:=CanRange;
  Data.IsRange:=0;

  repeat
    Data.First:=true;
    EarthCalcRoute(Data.Position,ToPoint,Steps,@Data,CallBackEarthCalcFlight);
    Steps:=Steps+15;
  until (Steps>150) or (Data.CanRange=0);

  Position:=Data.Position;
  IsRange:=Data.IsRange;
end;

end.
