unit register_scripttypes;

interface

uses uPSUtils, uPSCompiler, uPSRunTime;

function RegisterCompiler(Sender: TPSPascalCompiler; const Name: string): Boolean;

function RegisterRunTimeEngine(RunTime: TPSexec): TPSRuntimeClassImporter;

type TOnUsesUnit = function(NeededUnitName: String; out Text: String): Boolean of object;

var
  registeredClasses   : Array of TClass;
  ScriptEngineVersion : Cardinal;
  OnUsesUnit          : TOnUsesUnit;

implementation

uses
  lager_api, script_api, game_api, country_api, sysutils, MissionList,
  uPSC_std, uPSR_std, uPSC_classes, uPSR_classes, script_utils, UFOList,
  ufo_api, Classes, string_utils, savegame_api, EinsatzListe, einsatz_api,
  alien_api, basis_api, BasisListe, raumschiff_api, RaumschiffList,
  script_propertyhelpers, KD4Utils, NotifyList, EarthCalcs, earth_api;

var
  Compiler   : TPSPascalCompiler;
  Exec       : TPSExec;

// Hier werden alle Funktionen definiert, die �ber die Skriptsprache genutzt werden k�nnen
procedure RegisterExterns;
var
  MissionObj: Pointer;
  MissionList : Pointer;

  procedure RegisterScriptFunction(Ptr: Pointer; const Decl: string);
  var
    Name: String;
  begin
    Assert((Compiler<>nil) or (Exec<>nil));
    if Compiler <> nil then
      Compiler.AddDelphiFunction(Decl);

    if Pos('(',Decl)=0 then
      Name:=Copy(Decl,Pos(' ',Decl)+1,Pos(':',Decl)-Pos(' ',Decl)-1)
    else
      Name:=Copy(Decl,Pos(' ',Decl)+1,Pos('(',Decl)-Pos(' ',Decl)-1);

    if Exec <> nil then
      Exec.RegisterDelphiFunction(Ptr,Name,cdRegister);
  end;

  procedure RegisterScriptMethod(SlfPtr: Pointer; Ptr: Pointer; const Decl: string;Conv: TPSCallingConvention = cdRegister);
  var
    Name: String;
  begin
    Assert((Compiler<>nil) or (Exec<>nil));
    if Compiler <> nil then
      Compiler.AddDelphiFunction(Decl);

    if Pos('(',Decl)=0 then
      Name:=Copy(Decl,Pos(' ',Decl)+1,Pos(':',Decl)-Pos(' ',Decl)-1)
    else
      Name:=Copy(Decl,Pos(' ',Decl)+1,Pos('(',Decl)-Pos(' ',Decl)-1);

    if Exec <> nil then
      Exec.RegisterDelphiMethod(SlfPtr,Ptr,Name,Conv);
  end;

begin
  if Exec <> nil then
  begin
    MissionObj:=Exec.Id;
    MissionList:=TMission(MissionObj).MissionList;
  end
  else
  begin
    MissionObj:=nil;
    MissionList:=nil;
  end;

  // Funktionen Lager/Ausr�stungen
  RegisterScriptFunction(@lager_api_Count,'function lager_api_Count: Integer;');
  RegisterScriptFunction(@lager_api_GetItemInfo,'function lager_api_GetItem(Index: Integer): TLagerItem;');
  RegisterScriptFunction(@lager_api_GetItemByID,'function lager_api_GetItemByID(ID: Cardinal): TLagerItem;');
  RegisterScriptFunction(@lager_api_CheckItem,'function lager_api_CheckItem(ID: Cardinal): Boolean;');
  RegisterScriptFunction(@lager_api_GetItemCountInBase,'function lager_api_GetItemCountInBase(BaseID: Cardinal; ID: Cardinal): Integer;');
  RegisterScriptFunction(@lager_api_ItemVisible,'function lager_api_ItemVisible(Item: TLagerItem): Boolean;');
  RegisterScriptFunction(@lager_api_PutItems,'function lager_api_PutItems(BaseID: Cardinal; ItemID: Cardinal; Count: Integer): Integer;');
  RegisterScriptFunction(@lager_api_DeleteItem,'function lager_api_DeleteItem(BaseID: Cardinal; ItemID: Cardinal; Count: Integer): Boolean;');

  // Funktionen Countries
  RegisterScriptFunction(@country_api_GetCountryCount,'function country_api_count: Integer');
  RegisterScriptFunction(@country_api_GetRandomLandPosition,'function country_api_GetRandomLandPosition(Index: Integer;OverLand: Boolean): TFloatPoint');
  RegisterScriptFunction(@script_api.country_api_GetCountry,'function country_api_getCountry(Index: Integer): TCountry');
  RegisterScriptFunction(@country_api_ChangeZufriedenheit,'procedure country_api_changeconfident(Index: Integer; Confident: Integer);');
  RegisterScriptFunction(@country_api_ChangeBudget,'procedure country_api_ChangeBudget(Land: Integer; Budget: Integer);');

  // Funktionen Eins�tze
  RegisterScriptFunction(@einsatz_api_generateeinsatz,'function einsatz_api_generateeinsatz: TEinsatz');

  // Funktionen Aliens
  RegisterScriptFunction(@script_api.alien_api_GetRandomAlien,'function alien_api_GetRandomAlien: TAlien');
  RegisterScriptFunction(@script_api.alien_api_GetAlien,'function alien_api_GetAlien(ID: Cardinal): TAlien');

  // Funktionen UFOs
  RegisterScriptFunction(@ufo_api_CreateUFO,'function ufo_api_CreateUFO: TUFO');
  RegisterScriptFunction(@ufo_api_CreateUFOFromModel,'function ufo_api_CreateUFOFromModel(Model: TUFOModel): TUFO');
  RegisterScriptFunction(@ufo_api_GetUFOModelCount,'function ufo_api_GetUFOModelCount: Integer');
  RegisterScriptFunction(@ufo_api_GetUFOModelByIndex,'function ufo_api_GetUFOModelByIndex(Index: Integer; out Model: TUFOModel): Boolean;');
  RegisterScriptFunction(@ufo_api_GetUFOModelByID,'function ufo_api_GetUFOModelByID(ID: Cardinal; out Model: TUFOModel): Boolean;');

  // Funktionen Basis
  RegisterScriptFunction(@basis_api_GetMainBasis,'function basis_api_GetMainBase: TBasis');
  RegisterScriptFunction(@basis_api_GetBasisCount,'function basis_api_GetBasisCount: Integer;');
  RegisterScriptFunction(@basis_api_GetBasisFromIndex,'function basis_api_GetBasis(Index: Integer): TBasis;');

  // Funktionen Raumschiffe
  RegisterScriptFunction(@script_api.raumschiff_api_DoTransportToBase,'function raumschiff_api_DoTransportToBase(FromCountry: TCountry; ToBase: TBasis; Item: TLagerItem; Count: Integer): TRaumschiff;');
  RegisterScriptFunction(@script_api.raumschiff_api_DoTransportToCountry,'function raumschiff_api_DoTransportToCountry(FromBase: TBasis; ToCountry: TCountry; Item: TLagerItem; Count: Integer): TRaumschiff;');
  RegisterScriptFunction(@raumschiff_api_DoAlphatronTransport,'function raumschiff_api_DoAlphatronTransport(FromPos,ToPos: TFloatPoint;Alphatron: Integer): TRaumschiff;');

  // Funktionen Missionen
  RegisterScriptMethod(MissionList,@TMissionList.SetSkriptStatus,'procedure mission_api_ChangeActiveState(ScriptName: string; Active: Boolean)');
  RegisterScriptMethod(MissionList,@TMissionList.StartMissionFromSkriptName,'procedure mission_api_StartSkript(ScriptName: string)');
  RegisterScriptMethod(MissionList,@TMissionList.ClearUserTrigger,'procedure mission_api_ClearTrigger(TriggerName: string)');

  // Funktionen Savegame
  RegisterScriptFunction(@savegame_api_NeedMoney,'function savegame_api_NeedMoney(Money: Int64; BuchType: TKapital; raiseException: boolean): Boolean;');
  RegisterScriptFunction(@savegame_api_FreeMoney,'procedure savegame_api_FreeMoney(Money: Int64; BuchType: TKapital);');

  RegisterScriptFunction(@savegame_api_NeedAlphatron,'function savegame_api_NeedAlphatron(Alphatron: Integer; BasisID: Cardinal): Integer;');
  RegisterScriptFunction(@savegame_api_FreeAlphatron,'procedure savegame_api_FreeAlphatron(Alphatron: Integer; BasisID: Cardinal);');
  RegisterScriptFunction(@savegame_api_GetGameDifficult,'function savegame_api_GetGameDifficult: TGameDifficult;');
  RegisterScriptFunction(@savegame_api_Message,'procedure savegame_api_Message(Message: String; MType: TLongMessage; Objekt: TObject);');

  RegisterScriptFunction(@savegame_api_GetDate,'function savegame_api_GetDate: TKD4Date;');

  // Funktionen earth_api
  RegisterScriptFunction(@EarthEntfernung,'function earth_api_Distance(VonPoint: TFloatPoint; ToPoint: TFloatPoint): Integer;');
  RegisterScriptFunction(@earth_api_RandomEarthPoint,'function earth_api_randomPoint(OverLand: Boolean): TFloatPoint;');
  RegisterScriptFunction(@earth_api_PointOverLand,'function earth_api_PointOverLand(Position: TFloatPoint): Boolean;');

  // Funktionen Game
  RegisterScriptFunction(@game_api_Question,'function game_api_Question(Message: String; Caption: String): Boolean;');
  RegisterScriptFunction(@script_utils_MessageBox,'procedure game_api_MessageBox(Message: String);');
  RegisterScriptFunction(@game_api_QueryText,'function game_api_QueryText(Caption: String; MaxLength: Integer; var Text: String): Boolean;');
  RegisterScriptFunction(@game_api_ChooseItemBox,'function game_api_ChooseItemBox(Caption: String; List: TStrings): Integer;');
  RegisterScriptFunction(@game_api_getConstantValue,'function game_api_getConstantValue(Constant: String): String;');

  // Funktionen Ereignisse
  RegisterScriptMethod(MissionObj,@TMission.MissionAbort,'procedure mission_abort()');
  RegisterScriptMethod(MissionObj,@TMission.MissionLoose,'procedure mission_loose()');
  RegisterScriptMethod(MissionObj,@TMission.MissionWin,'procedure mission_win()');

  RegisterScriptMethod(MissionObj,@TMission.RegisterEvent,'function event_register(Proc: TProcedure; Minutes: Integer): TEvent',cdStdCall);

  RegisterScriptMethod(MissionObj,@TMission.RegisterUFO,'procedure register_ufo(UFO: TUFO);');
  RegisterScriptMethod(MissionObj,@TMission.RegisterUFOShootDown,'procedure register_ufo_shootdown(Event: TUFOProcedure;UFO: TUFO);',cdStdCall);
  RegisterScriptMethod(MissionObj,@TMission.RegisterUFOEscape,'procedure register_ufo_escape(Event: TUFOProcedure;UFO: TUFO);',cdStdCall);
  RegisterScriptMethod(MissionObj,@TMission.RegisterUFODiscovered,'procedure register_ufo_discover(Event: TUFOProcedure;UFO: TUFO);',cdStdCall);

  RegisterScriptMethod(MissionObj,@TMission.RegisterEinsatzWin,'procedure register_einsatz_win(Event: TEinsatzProcedure;Einsatz: TEinsatz);',cdStdCall);
  RegisterScriptMethod(MissionObj,@TMission.RegisterEinsatzTimeUp,'procedure register_einsatz_timeup(Event: TEinsatzProcedure;Einsatz: TEinsatz);',cdStdCall);

  RegisterScriptMethod(MissionObj,@TMission.RegisterRaumschiffShootDown,'procedure register_raumschiff_shootdown(Event: TSchiffProcedure;Raumschiff: TRaumschiff);',cdStdCall);
  RegisterScriptMethod(MissionObj,@TMission.RegisterRaumschiffReachedDest,'procedure register_raumschiff_reached(Event: TSchiffProcedure;Raumschiff: TRaumschiff);',cdStdCall);

  // Utilities
  RegisterScriptFunction(@Format,'function Format(const Format: string; const Args: array of const): string;');
  RegisterScriptFunction(@FloatPoint,'function FloatPoint(x,y: double): TFloatPoint;');

  // random kann nicht direkt importiert werden, deshalb umweg �ber script_utils
  RegisterScriptFunction(@script_utils_random,'function random(Range: Integer): Integer');

  RegisterScriptFunction(@script_utils_inc,'procedure inc(var int: Integer)');
  RegisterScriptFunction(@script_utils_dec,'procedure dec(var int: Integer)');

  RegisterScriptFunction(@FreeAndNil,'procedure FreeAndNil(var Ref: TObject);');

  Compiler:=nil;
  Exec:=nil;
end;

// Hier werden die Objecte Registriert
procedure CompilerRegisterObjects(Comp: TPSPascalCompiler);

  function RegisterClass(parent: String; aClass: TClass): TPSCompileTimeClass;
  begin
    if aClass.ClassInfo = nil then
    begin
      raise Exception.Create('Achtung!! Keine RTTI-Informationen f�r Klasse '+aClass.ClassName+' vorhanden. Bitte schalter {$M+} aktivieren bei der ersten Deklaration vornehmen.');
      exit;
    end;

    if aClass.MethodAddress('ReadFromScriptString')=nil then
      raise Exception.Create('Achtung!! Klasse '+aClass.ClassName+' ben�tigt eine procedure ReadFromScriptString(ScriptString: String)');

    if aClass.MethodAddress('WriteToScriptString')=nil then
      raise Exception.Create('Achtung!! Klasse '+aClass.ClassName+' ben�tigt eine function WriteToScriptString: String');

    result:=Comp.AddClass(Comp.FindClass(parent),aClass);
    result.RegisterPublishedProperties;

    SetLength(registeredClasses,length(registeredClasses)+1);
    registeredClasses[high(registeredClasses)]:=aClass;
  end;

  function GetClass(ClassName: String): TPSCompileTimeClass;
  begin
    result:=Comp.FindClass(ClassName);
    Assert(result<>nil);
  end;

begin
  SIRegisterTObject(Comp);
  SIRegisterTPersistent(Comp);
  SIRegisterTStrings(Comp,false);
  SIRegisterTStringList(Comp);

  RegisterClass('TObject',TEvent);
  RegisterClass('TObject',TEinsatz);
  RegisterClass('TObject',TBasis);
  RegisterClass('TObject',TRaumschiff);
  RegisterClass('TObject',TUFO);

  // Spezielle Eigenschaften / Methoden registrieren
  with GetClass('TEinsatz') do
  begin
    RegisterProperty('Position','TFloatPoint',iptRW);
    RegisterMethod('procedure AddAlien(Alien: TAlien)');
    RegisterMethod('function Start: Boolean');
  end;

  with GetClass('TBasis') do
  begin
    RegisterProperty('Position','TFloatPoint',iptr);
//    RegisterMethod('procedure AddRoom(Room: TRoom);');
  end;

  with GetClass('TRaumschiff') do
  begin
    RegisterMethod('procedure ShootDown(From: TUFO)');
  end;

  with GetClass('TUFO') do
  begin
    RegisterMethod('procedure SetPosition(X,Y: double)');
    RegisterMethod('procedure ShootDown;');
    RegisterMethod('procedure Escape;');
    RegisterMethod('procedure HuntSchiff(Schiff: TRaumschiff)');
    RegisterMethod('procedure AttackBase(Base: TBasis)');
    RegisterMethod('function GetAlien(Index: Integer): TAlien');

    RegisterProperty('Model','TUFOModel',iptRW);
    RegisterProperty('Position','TFloatPoint',iptR);
  end;

end;

// Objekte f�r die RuntimeEngine
procedure RunTimeRegisterObjects(CI: TPSRuntimeClassImporter);
begin
  RIRegisterTObject(CI);
  RIRegisterTPersistent(CI);
  RIRegisterTStrings(CI,false);
  RIRegisterTStringList(CI);

  // X-Force Objekte
  CI.Add(TEvent);

  with CI.Add(TUFO) do
  begin
    RegisterMethod(@TUFO.SetPosition,'SetPosition');

    RegisterMethod(@TUFO.Abschuss,'ShootDown');
    RegisterMethod(@TUFO.Escape,'Escape');
    RegisterMethod(@TUFO.HuntObject,'HuntSchiff');
    RegisterMethod(@TUFO.HuntObject,'AttackBase');
    RegisterMethod(@TUFO.GetAlien,'GetAlien');

    RegisterPropertyHelper(@TUFOModel_R,@TUFOModel_W,'Model');
    RegisterPropertyHelper(@TUFOPosition_R,nil,'Position');
  end;

  with CI.Add(TEinsatz) do
  begin
    RegisterMethod(@TEinsatz.AddAlien,'AddAlien');
    RegisterMethod(@TEinsatz.CheckEinsatz,'Start');

    RegisterPropertyHelper(@TEinsatzPosition_R,@TEinsatzPosition_W,'Position');
  end;

  with CI.Add(TBasis) do
  begin
    // Funktion durch das Basisbaukonzept Obsloet
//    RegisterMethod(@TBasis.AddEinrichtung,'AddRoom');
    RegisterPropertyHelper(@TBasisBasisPos_R,nil,'Position');
  end;

  with CI.Add(TRaumschiff) do
  begin
    RegisterMethod(@TRaumschiff.VernichteRaumschiff,'ShootDown');
  end;
end;

function RegisterCompiler(Sender: TPSPascalCompiler; const Name: string): Boolean;
var
  Types           : TStringList;
  TypeName        : String;
  TypeDefinition  : String;
  Line            : String;
  UnitText        : String;

  procedure RegisterType(TypeName,Definition: String);
  begin
    with Sender.AddTypeS(TypeName,Definition) do
    begin
      ExportName:=true;
      DeclarePos:=$FFFFFFFF;
    end;
  end;

begin
  if Name='SYSTEM' then
  begin
    Types:=TStringList.Create;
    Types.Text:=string_utils_LoadFromFile(IncludeTrailingBackslash(ExtractFilePath(ParamStr(0)))+'scripttypes.txt');

    ScriptEngineVersion:=HashString(Types.Text);

    while (Types.Count>0) do
    begin
      Line:=Types[0];
      if (length(Line)>0) and ((Line[1] in ['a'..'z','A'..'Z'])) then
      begin
        TypeName:=trim(copy(Line,1,Pos('=',Line)-1));
        TypeDefinition:=trim(copy(Line,Pos('=',Line)+1,length(Line)));
        Types.Delete(0);
        while (Types.Count>0) and (length(trim(Types[0]))>0) do
        begin
          TypeDefinition:=TypeDefinition+' '+Types[0];
          Types.Delete(0);
        end;
        RegisterType(TypeName,TypeDefinition);
//        game_api_MessageBox(TypeName+'='+TypeDefinition,'Test');
      end;
      if Types.Count>0 then
        Types.Delete(0);
    end;
    Types.Free;

    // GlobalVariablen
      // Missionsname
    Sender.AddUsedVariableN('MissionName','String');
    Sender.AddUsedVariableN('MissionType','TWinMode');

    // Missionsobjekte registrieren
    CompilerRegisterObjects(Sender);

    RegisterType('TUFOProcedure','procedure(UFO: TUFO)');
    RegisterType('TEinsatzProcedure','procedure(Einsatz: TEinsatz)');
    RegisterType('TSchiffProcedure','procedure(Raumschiff: TRaumschiff)');

    Compiler:=Sender;

    // Funktionen registrieren
    RegisterExterns;

    result:=true;
  end
  else
  begin
    result:=false;
    UnitText:='';
    if Assigned(OnUsesUnit) then
      result:=OnUsesUnit(Name,UnitText);

    if not result then
      exit;

    result:=Sender.Compile(UnitText)
  end;
end;

function RegisterRunTimeEngine(RunTime: TPSexec): TPSRuntimeClassImporter;
var
  Importer: TPSRuntimeClassImporter;
begin
  Exec:=RunTime;
  RegisterExterns;
  Importer:=TPSRuntimeClassImporter.CreateAndRegister(Runtime,false);
  RuntimeRegisterObjects(Importer);
  result:=Importer;
end;

end.
