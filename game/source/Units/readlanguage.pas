{********************************************************************************
*										*
* Copyright (C) 2004  X-Force Team - jim_raynor@web.de				*
*										*
* This file is part of X-Force: Fight For Destiny (X-Force).			*
*										*
* X-Force is free software; you can redistribute it and/or modify		*
* it under the terms of the GNU General Public License as published by		*
* the Free Software Foundation; either version 2 of the License, or		*
* (at your option) any later version.						*
*										*
* X-Force is distributed in the hope that it will be useful,			*
* but WITHOUT ANY WARRANTY; without even the implied warranty of		*
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the			*
* GNU General Public License for more details.					*
*										*
* You should have received a copy of the GNU General Public License		*
* along with X-Force; if not, write to the Free Software			*
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA	*
*										*
*********************************************************************************
*										*
* Liest die Sprachdateien ein							*
*										*
********************************************************************************}

{$I ../Settings.inc}

unit ReadLanguage;

interface

uses Classes, SysUtils, XForce_types, Dialogs, Windows, TraceFile, Forms;
                
function ReadLanguageFile(Language: String): boolean;

function GetLanguageFile(Prefix: string): string;

function ReplaceTags(Value: string): string;

var
  g_GameConstants: TStringList;

implementation

uses
  StringConst, Defines, UnicodeHelper;

var
  StringList     : TStringList;
  MissingStrings : String;

function GetLanguageFile(Prefix: string): string;
begin
  Result := IncludeTrailingBackSlash(ExtractFilePath(Application.ExeName))+'language/'+Prefix+Defines.Language+'.dat';
end;

function ReplaceTags(Value: string): string;
begin
  Result := StringReplace(Value, '%LGameName%', LGameName, [rfReplaceAll, rfIgnoreCase]);
  Result := StringReplace(Result, '%IDAlphatron%', IDAlphatron, [rfReplaceAll, rfIgnoreCase]);
  Result := StringReplace(Result, '%IDGeldMittel%', IDGeldMittel, [rfReplaceAll, rfIgnoreCase]);
  Result := StringReplace(Result, '\n',#10, [rfReplaceAll, rfIgnoreCase]);
  Result := StringReplace(Result, '\r',#13, [rfReplaceAll, rfIgnoreCase]);
end;

function GetValue(Name: String): String;
var
  Index: Integer;
begin
  Index:=StringList.IndexOfName(Name);
  if Index=-1 then
  begin
    MissingStrings:=MissingStrings+Name+#13#10;
    result:='Missing';
  end
//    raise Exception.CreateFmt(,[Name]);
  else
  begin
    result:=StringList.Strings[Index];

    if Pos('(old)',result)>0 then
      result:=Copy(result,Pos('(old)',result)+6,length(result));

    if Pos('(new)',result)>0 then
      result:=StringReplace(result,'(new) ','',[rfReplaceAll,rfIgnoreCase]);

    g_GameConstants.Add(result);

    result:=copy(result,Length(Name)+2,MaxInt);
    StringList.Delete(Index);
  end;
end;

function ReadLanguageFile(Language: String): boolean;
var
  Dummy    : Integer;
  Time     : Cardinal;
  FileName : String;
begin
  if g_GameConstants=nil then
    g_GameConstants:=TStringList.Create;

  g_GameConstants.Clear;

  Defines.Language:=Language;
  FileName := GetLanguageFile('');

  StringList:=TStringList.Create;

  Result := FileExists(FileName);
  if Result then
  begin
    FileToStringList(FileName, StringList);

    Time:=GetTickCount;
    for Dummy:=0 to StringList.Count-1 do
      StringList[Dummy] := ReplaceTags(StringList[Dummy]);

    {$I readlanguage.inc}
    if MissingStrings<>'' then
    begin
      MessageBeep(MB_ICONERROR);
      MessageDlg(Format('Some Strings couldn''t find in language file: '#13#10'%s',[MissingStrings]),mtError,[mbOK],0);
    end;

    StringList.Free;
    GlobalFile.Write('Time',GetTickCount-Time);
  end;
end;

initialization

finalization
  if g_GameConstants<>nil then
    FreeAndNil(g_GameConstants);
end.

