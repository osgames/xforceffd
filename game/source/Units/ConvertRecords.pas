{********************************************************************************
*										*
* Copyright (C) 2004  X-Force Team - jim_raynor@web.de				*
*										*
* This file is part of X-Force: Fight For Destiny (X-Force).			*
*										*
* X-Force is free software; you can redistribute it and/or modify		*
* it under the terms of the GNU General Public License as published by		*
* the Free Software Foundation; either version 2 of the License, or		*
* (at your option) any later version.						*
*										*
* X-Force is distributed in the hope that it will be useful,			*
* but WITHOUT ANY WARRANTY; without even the implied warranty of		*
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the			*
* GNU General Public License for more details.					*
*										*
* You should have received a copy of the GNU General Public License		*
* along with X-Force; if not, write to the Free Software			*
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA	*
*										*
*********************************************************************************
*										*
* Konvertierfunktionen zwischen ExtRecords und den entsprechenden normalen	*
* records									*
*										*
********************************************************************************}

{$I ../Settings.inc}

unit ConvertRecords;

interface
{ Regul�rer Ausdruck - ExtRecord to Record Funktionen }
{ Suchen            : ^.+Add\([^(]+\)('\([^']+\)'.+$ }
{ Ersetzen ToExt    :     result.Set\1('\2',\2);}
{ Ersetzen ToRecord :     \2:=Rec.Get\1('\2');}

{ Regul�rer Ausdruck - ExtRecordeigenschaften erstellen }
{ Suchen            : ^[^a-z^A-Z]+\([a-zA-Z]+\).+:[^a-z^A-Z]+\([a-zA-Z]+\); }
{ Ersetzen          :     Add\2('\1',);}

uses XForce_types, ExtRecord, Math;

function LagerItemToRecord(const Item: TLagerItem): TExtRecord;
function RecordToLagerItem(Rec: TExtRecord): TLagerItem;

function ForschungenToRecord(const Forschung: TForschungen): TExtRecord;
function RecordToForschungen(Rec: TExtRecord): TForschungen;

function SoldatInfoToRecord(const SoldatInfo: TSoldatInfo): TExtRecord;
function RecordToSoldatInfo(Rec: TExtRecord): TSoldatInfo;

function SoldatWaffeToRecord(const SoldatWaffe: TSoldatWaffe): TExtRecord;
function RecordToSoldatWaffe(Rec: TExtRecord): TSoldatWaffe;

function SoldatPanzerungToRecord(const SoldatPanzerung: TSoldatPanzerung): TExtRecord;
function RecordToSoldatPanzerung(Rec: TExtRecord): TSoldatPanzerung;

function SoldatGuertelToRecord(const SoldatGuertel: TSoldatGuertel): TExtRecord;
function RecordToSoldatGuertel(Rec: TExtRecord): TSoldatGuertel;

function PatentToRecord(const Patent: TPatent): TExtRecord;
function RecordToPatent(Rec: TExtRecord): TPatent;

function MissionUFOToRecord(const UFO: TMissionUFO): TExtRecord;
function RecordToMissionUFO(Rec: TExtRecord): TMissionUFO;

function MissionSchiffToRecord(const Trans: TMissionRaumschiff): TExtRecord;
function RecordToMissionSchiff(Rec: TExtRecord): TMissionRaumschiff;

function MissionEinsatzToRecord(const Einsatz: TMissionEinsatz): TExtRecord;
function RecordToMissionEinsatz(Rec: TExtRecord): TMissionEinsatz;

function LagerAuftragToRecord(const Auftrag: TAuftrag): TExtRecord;
function RecordToLagerAuftrag(Rec: TExtRecord): TAuftrag;

function LagerAngebotToRecord(const Angebot: TAngebot): TExtRecord;
function RecordToLagerAngebot(Rec: TExtRecord): TAngebot;

function WaffenzelleToRecord(const Waffenzelle: TWaffenzelle): TExtRecord;
function RecordToWaffenzelle(Rec: TExtRecord): TWaffenzelle;

function EinrichtungToRecord(const Einrichtung: TEinrichtung): TExtRecord;
function RecordToEinrichtung(Rec: TExtRecord): TEinrichtung;

function ForscherToRecord(const Forscher: TForscher): TExtRecord;
function RecordToForscher(Rec: TExtRecord): TForscher;

function EndedForschProjectToRecord(const EndedForschProject: TEndedForschProjekt): TExtRecord;
function RecordToEndedForschProject(Rec: TExtRecord): TEndedForschProjekt;

function WatchMarktSettingsToRecord(const WatchMarktSettings: TWatchMarktSettings): TExtRecord;
function RecordToWatchMarktSettings(Rec: TExtRecord): TWatchMarktSettings;

function LandToRecord(const Land: TLand): TExtRecord;
function RecordToLand(Rec: TExtRecord): TLand;

function TechnikerToRecord(const Techniker: TTechniker): TExtRecord;
function RecordToTechniker(Rec: TExtRecord): TTechniker;

function ProduktionToRecord(const Produktion: TProduktion): TExtRecord;
function RecordToProduktion(Rec: TExtRecord): TProduktion;

function RaumschiffModelToRecord(const RaumschiffModel: TRaumschiffModel): TExtRecord;
function RecordToRaumschiffModel(Rec: TExtRecord): TRaumschiffModel;

function RaumschiffShieldToRecord(const RaumschiffShield: TShield): TExtRecord;
function RecordToRaumschiffShield(Rec: TExtRecord): TShield;

function RaumschiffMotorToRecord(const RaumschiffMotor: TMotor): TExtRecord;
function RecordToRaumschiffMotor(Rec: TExtRecord): TMotor;

function RaumschiffExtensionToRecord(const RaumschiffExtension: TExtensionZelle): TExtRecord;
function RecordToRaumschiffExtension(Rec: TExtRecord): TExtensionZelle;

function BackPackItemToRecord(const BackPackItem: TBackPackItem): TExtRecord;
function RecordToBackPackItem(Rec: TExtRecord): TBackPackItem;

function MissionDataToRecord(const MissionData: TMissionData): TExtRecord;
function RecordToMissionData(Rec: TExtRecord): TMissionData;

var
  RecordLagerListe            : TExtRecordDefinition;
  RecordForschung             : TExtRecordDefinition;
  RecordSoldatInfo            : TExtRecordDefinition;
  RecordSoldatWaffe           : TExtRecordDefinition;
  RecordSoldatPanzerung       : TExtRecordDefinition;
  RecordSoldatGuertel         : TExtRecordDefinition;
  RecordPatent                : TExtRecordDefinition;
  RecordMissionUFOs           : TExtRecordDefinition;
  RecordMissionSchiff         : TExtRecordDefinition;
  RecordMissionEinsatz        : TExtRecordDefinition;
  RecordMissionEvent          : TExtRecordDefinition;
  RecordLagerAuftrag          : TExtRecordDefinition;
  RecordLagerAngebot          : TExtRecordDefinition;
  RecordWaffenzelle           : TExtRecordDefinition;
  RecordEinrichtung           : TExtRecordDefinition;
  RecordForscher              : TExtRecordDefinition;
  RecordEndedForschProject    : TExtRecordDefinition;
  RecordWatchMarktSettings    : TExtRecordDefinition;
  RecordLand                  : TExtRecordDefinition;
  RecordTechniker             : TExtRecordDefinition;
  RecordProduktion            : TExtRecordDefinition;
  RecordRaumschiffModel       : TExtRecordDefinition;
  RecordRaumschiffShield      : TExtRecordDefinition;
  RecordRaumschiffMotor       : TExtRecordDefinition;
  RecordRaumschiffExtension   : TExtRecordDefinition;
  RecordBackPackItem          : TExtRecordDefinition;

implementation

uses
  ObjektRecords, Defines, SysUtils, typinfo, KD4Utils;

function LagerItemToRecord(const Item: TLagerItem): TExtRecord;
var
  Dummy: Integer;
  DummySchussType : TSchussType;
  tmpName         : String;
begin
  Result:=TExtRecord.Create(RecordLagerListe);
  with Item do
  begin
    Result.SetString('Name',Name);
    Result.SetInteger('Anzahl',Anzahl);
    Result.SetInteger('Level',Level);
    Result.SetCardinal('ID',ID);

    Result.Setdouble('LagerV',LagerV);
    Result.SetInteger('WaffType',Integer(WaffType));
    Result.Setdouble('Gewicht',Gewicht);

    Result.SetInteger('KaufPreis',KaufPreis);
    Result.SetInteger('VerKaufPreis',VerKaufPreis);
    result.SetInteger('Alphatron',ManuAlphatron);
    result.SetInteger('ProdTime',ProdTime);

    Result.SetInteger('Munition',Munition);
    Result.SetInteger('TypeID',Integer(TypeID));
    Result.SetCardinal('Strength',Strength);

    for DummySchussType := low(TSchussType) to high(TSchussType) do
    begin
      tmpName:='SchussType['+GetEnumName(TypeInfO(TSchussType), Ord(DummySchussType))+']';

      if result.ValueExists(tmpName) then
        Result.SetBoolean(tmpName,(DummySchussType in Schuss));
    end;

    Result.SetCardinal('Munfor',Munfor);
    Result.SetInteger('Panzerung',Panzerung);
    Result.SetInteger('PixPerSec',PixPerSec);

    Result.SetInteger('Laden',Laden);
    Result.SetInteger('Frequenz',Laden);
    result.SetInteger('Genauigkeit',Genauigkeit);

    Result.SetBoolean('Verfolgung',Verfolgung);
    Result.SetInteger('ImageIndex',ImageIndex);
    Result.SetInteger('Land',Land);

    Result.SetBoolean('HerstellBar',HerstellBar);
    Result.SetInteger('Reichweite',Reichweite);
    Result.SetBoolean('Einhand',Einhand);
    Result.SetInteger('Power',Power);
    Result.SetInteger('TimeUnits',TimeUnits);
    Result.SetString('Info',Info);

    with result.GetRecordList('Extensions') do
    begin
      for Dummy:=0 to ExtCount-1 do
      begin
        with Add do
        begin
          SetInteger('Prop',Ord(Extensions[Dummy].Prop));
          SetInteger('Value',Extensions[Dummy].Value);
        end;
      end;
    end;

    result.SetString('UFOPaedieImage',UFOPaedieImage);
    result.SetInteger('Explosion',Explosion);
    result.SetBoolean('UseAble',UseAble);
    result.SetBoolean('PublicPlan',PublicPlan);

    result.SetInteger('AlienChance',AlienChance);

    result.SetInteger('NeedIQ',NeedIQ);

    result.SetBoolean('Visible',Visible);
    result.SetBoolean('AlienItem',AlienItem);
    result.SetInteger('ShootSurPlus',ShootSurPlus);

    Result.SetDouble('ResearchedDate', ResearchedDate);

    result.SetString('ShootingSound',SoundShoot);
  end;
end;

function RecordToLagerItem(Rec: TExtRecord): TLagerItem;
var
  Dummy: Integer;
  DummySchussType : TSchussType;
  tmpName         : String;
begin
  with result do
  begin
    Name:=Rec.GetString('Name');
    Anzahl:=Rec.GetInteger('Anzahl');
    Level:=Rec.GetInteger('Level');
    ID:=Rec.GetCardinal('ID');

    LagerV:=Rec.Getdouble('LagerV');
    WaffType:=TWaffenType(Rec.GetInteger('WaffType'));
    Gewicht:=Rec.Getdouble('Gewicht');

    KaufPreis:=Rec.GetInteger('KaufPreis');
    VerKaufPreis:=Rec.GetInteger('VerKaufPreis');
    ManuAlphatron:=Rec.GetInteger('Alphatron');
    ProdTime:=Rec.GetInteger('ProdTime');

    Munition:=Rec.GetInteger('Munition');
    TypeID:=TProjektType(Rec.GetInteger('TypeID'));
    Strength:=Rec.GetCardinal('Strength');

    Schuss:=[];
    for DummySchussType := low(TSchussType) to high(TSchussType) do
    begin
      tmpName:='SchussType['+GetEnumName(TypeInfO(TSchussType), Ord(DummySchussType))+']';

      if Rec.ValueExists(tmpName) and Rec.GetBoolean(tmpName) then
        Include(Schuss,DummySchussType);
    end;

    // Schusstypen korrigieren
    Schuss:=CheckSchussType(Result.TypeID,Result.WaffType,result.Schuss);

    Munfor:=Rec.GetCardinal('Munfor');
    Panzerung:=Rec.GetInteger('Panzerung');
    PixPerSec:=Rec.GetInteger('PixPerSec');

    if TypeID=ptRWaffe  then
      Laden:=Rec.GetInteger('Frequenz')
    else
      Laden:=Rec.GetInteger('Laden');

    Genauigkeit:=Rec.GetInteger('Genauigkeit');

    Verfolgung:=Rec.GetBoolean('Verfolgung');
    ImageIndex:=Rec.GetInteger('ImageIndex');
    Land:=Rec.GetInteger('Land');

    HerstellBar:=Rec.GetBoolean('HerstellBar');
    Reichweite:=Rec.GetInteger('Reichweite');
    Einhand:=Rec.GetBoolean('Einhand');
    Power:=Rec.GetInteger('Power');
    TimeUnits:=Rec.GetInteger('TimeUnits');
    Info:=Rec.GetString('Info');
    UFOPaedieImage:=Rec.GetString('UFOPaedieImage');

    with Rec.GetRecordList('Extensions') do
    begin
      ExtCount:=Count;
      for Dummy:=0 to Count-1 do
      begin
        with Item[Dummy] do
        begin
          Extensions[Dummy].Prop:=TExtensionProp(GetInteger('Prop'));
          Extensions[Dummy].Value:=GetInteger('Value');
        end;
      end;
    end;

    Explosion:=Rec.GetInteger('Explosion');
    Useable:=Rec.GetBoolean('UseAble');
    PublicPlan:=Rec.GetBoolean('PublicPlan');

    AlienChance:=Rec.GetInteger('AlienChance');

    NeedIQ:=Rec.GetInteger('NeedIQ');

    Visible:=Rec.GetBoolean('Visible');
    AlienItem:=Rec.GetBoolean('AlienItem');
    ShootSurPlus:=Rec.GetInteger('ShootSurPlus');

    ResearchedDate:=Rec.GetDouble('ResearchedDate');

    SoundShoot:=Rec.GetString('ShootingSound');
  end;
end;

function ForschungenToRecord(const Forschung: TForschungen): TExtRecord;
begin
  Result:=TExtRecord.Create(RecordForschung);
  with Forschung do
  begin
    Result.SetCardinal('ID',ID);
    Result.Setdouble('Hour',Hour);
    Result.Setdouble('Gesamt',Gesamt);
    Result.SetInteger('TypeID',Integer(TypeID));
    Result.SetBoolean('Update',Update);
    Result.SetBoolean('ResearchItem',ResearchItem);
    Result.SetCardinal('BasisID',BasisID);
    Result.SetString('Info',Info);
    Result.SetInteger('ResearchCount',ResearchCount);
    Result.SetInteger('ResearchType',Integer(ResearchType));
    Result.SetBoolean('Started',Started);
  end;
end;

function RecordToForschungen(Rec: TExtRecord): TForschungen;
begin
  with result do
  begin
    ID:=Rec.GetCardinal('ID');
    Hour:=Rec.GetDouble('Hour');
    Gesamt:=Rec.GetDouble('Gesamt');
    TypeID:=TProjektType(Rec.GetInteger('TypeID'));
    Update:=Rec.GetBoolean('Update');
    ResearchItem:=Rec.GetBoolean('ResearchItem');
    BasisID:=Rec.GetCardinal('BasisID');
    Info:=Rec.GetString('Info');
    ResearchCount:=Rec.GetInteger('ResearchCount');
    ResearchType:=TForschType(Rec.GetInteger('ResearchType'));
    Started:=Rec.GetBoolean('Started');
  end;
end;

function SoldatInfoToRecord(const SoldatInfo: TSoldatInfo): TExtRecord;
begin
  Result:=TExtRecord.Create(RecordSoldatInfo);
  with SoldatInfo do
  begin
    Result.SetString('Name',Name);
    Result.SetDouble('Ges',Ges);
    Result.SetInteger('EP',EP);

    Result.SetInteger('Image',Image);
    Result.SetInteger('Raum',Raum);
    Result.SetInteger('Slot',Slot);
    Result.SetCardinal('BasisID',BasisID);
    Result.SetBoolean('LeftHand',LeftHand);
    Result.SetBoolean('Train',Train);
    Result.SetInteger('Tag',Tag);
    Result.SetInteger('Einsatz',Einsatz);
    Result.SetInteger('Treffer',Treffer);
    Result.SetInteger('TrainTime',TrainTime);
    Result.SetString('ClassName',ClassName);

    Result.SetDouble('MaxGes',MaxGes);
    Result.SetDouble('PSIAn',PSIAn);
    Result.SetDouble('PSIAb',PSIAb);
    Result.SetDouble('Zeit',Zeit);
    Result.SetDouble('Treff',Treff);
    Result.SetDouble('Kraft',Kraft);
    Result.SetDouble('Sicht',Sicht);
    Result.SetDouble('IQ',IQ);
    Result.SetDouble('React',React);

    Result.SetDouble('LastMaxGes',LastMaxGes);
    Result.SetDouble('LastPSIAn',LastPSIAn);
    Result.SetDouble('LastPSIAb',LastPSIAb);
    Result.SetDouble('LastZeit',LastZeit);
    Result.SetDouble('LastTreff',LastTreff);
    Result.SetDouble('LastKraft',LastKraft);
    Result.SetDouble('LastSicht',LastSicht);
    Result.SetDouble('LastIQ',LastIQ);
    Result.SetDouble('LastReact',LastReact);

    result.SetInteger('FaehigGes',FaehigGes);
    result.SetInteger('FaehigPSIAn',FaehigPSIAn);
    result.SetInteger('FaehigPSIAb',FaehigPSIAb);
    result.SetInteger('FaehigZeit',FaehigZeit);
    result.SetInteger('FaehigTreff',FaehigTreff);
    result.SetInteger('FaehigKraft',FaehigKraft);
    result.SetInteger('FaehigSicht',FaehigSicht);
    result.SetInteger('FaehigIQ',FaehigIQ);
    result.SetInteger('FaehigReact',FaehigReact);
  end;
end;

function RecordToSoldatInfo(Rec: TExtRecord): TSoldatInfo;
begin
  with result do
  begin
    Name:=Rec.GetString('Name');
    Ges:=Rec.GetDouble('Ges');
    EP:=Rec.GetInteger('EP');

    Image:=Rec.GetInteger('Image');
    Raum:=Rec.GetInteger('Raum');
    Slot:=Rec.GetInteger('Slot');
    BasisID:=Rec.GetCardinal('BasisID');
    LeftHand:=Rec.GetBoolean('LeftHand');
    Train:=Rec.GetBoolean('Train');
    Tag:=Rec.GetInteger('Tag');
    Einsatz:=Rec.GetInteger('Einsatz');
    Treffer:=Rec.GetInteger('Treffer');
    TrainTime:=Rec.GetInteger('TrainTime');

    ClassName:=Rec.GetString('ClassName');

    MaxGes:=Rec.GetDouble('MaxGes');
    PSIAn:=Rec.GetDouble('PSIAn');
    PSIAb:=Rec.GetDouble('PSIAb');
    Zeit:=Rec.GetDouble('Zeit');
    Treff:=Rec.GetDouble('Treff');
    Kraft:=Rec.GetDouble('Kraft');
    Sicht:=Rec.GetDouble('Sicht');
    IQ:=Rec.GetDouble('IQ');
    React:=Rec.GetDouble('React');

    FaehigGes:=Rec.GetInteger('FaehigGes');
    FaehigPSIAn:=Rec.GetInteger('FaehigPSIAn');
    FaehigPSIAb:=Rec.GetInteger('FaehigPSIAb');
    FaehigZeit:=Rec.GetInteger('FaehigZeit');
    FaehigTreff:=Rec.GetInteger('FaehigTreff');
    FaehigKraft:=Rec.GetInteger('FaehigKraft');
    FaehigSicht:=Rec.GetInteger('FaehigSicht');
    FaehigIQ:=Rec.GetInteger('FaehigIQ');
    FaehigReact:=Rec.GetInteger('FaehigReact');

    LastMaxGes:=Rec.GetDouble('LastMaxGes');
    LastPSIAn:=Rec.GetDouble('LastPSIAn');
    LastPSIAb:=Rec.GetDouble('LastPSIAb');
    LastZeit:=Rec.GetDouble('LastZeit');
    LastTreff:=Rec.GetDouble('LastTreff');
    LastKraft:=Rec.GetDouble('LastKraft');
    LastSicht:=Rec.GetDouble('LastSicht');
    LastIQ:=Rec.GetDouble('LastIQ');
    LastReact:=Rec.GetDouble('LastReact');
  end;
end;

function SoldatWaffeToRecord(const SoldatWaffe: TSoldatWaffe): TExtRecord;
begin
  Result:=TExtRecord.Create(RecordSoldatWaffe);
  with SoldatWaffe do
  begin
    result.SetBoolean('Gesetzt',Gesetzt);
    result.SetCardinal('ID',ID);
    result.SetString('Name',Name);
    result.SetInteger('Image',Image);
    result.SetInteger('TypeID',Integer(TypeID));
    result.SetInteger('WaffenArt',Integer(WaffenArt));
    result.SetDouble('Gewicht',Gewicht);
    result.SetBoolean('Zweihand',Zweihand);
    result.SetBoolean('MunGesetzt',MunGesetzt);
    result.SetInteger('MunImage',MunImage);
    result.SetCardinal('MunID',MunID);
    result.SetInteger('Schuesse',Schuesse);
    result.SetInteger('TimeLeft',TimeLeft);
    result.SetInteger('Strength',Strength);
    result.SetInteger('Power',Power);
    result.SetInteger('SchussMax',SchussMax);
    result.SetInteger('SchussArt',Integer(SchussArt));
    result.SetInteger('TimeUnits',TimeUnits);
    result.SetInteger('Genauigkeit',Genauigkeit);
    result.SetInteger('Frequenz',Ladezeit);
    result.SetDouble('MunGewicht',MunGewicht);
  end;
end;

function RecordToSoldatWaffe(Rec: TExtRecord): TSoldatWaffe;
begin
  with result do
  begin
    Gesetzt:=Rec.GetBoolean('Gesetzt');
    ID:=Rec.GetCardinal('ID');
    Name:=Rec.GetString('Name');
    Image:=Rec.GetInteger('Image');
    TypeID:=TProjektType(Rec.GetInteger('TypeID'));
    WaffenArt:=TWaffenType(Rec.GetInteger('WaffenArt'));
    Gewicht:=Rec.GetDouble('Gewicht');
    Zweihand:=Rec.GetBoolean('Zweihand');
    MunGesetzt:=Rec.GetBoolean('MunGesetzt');
    MunImage:=Rec.GetInteger('MunImage');
    MunID:=Rec.GetCardinal('MunID');
    Schuesse:=Rec.GetInteger('Schuesse');
    TimeLeft:=Rec.GetInteger('TimeLeft');
    Strength:=Rec.GetInteger('Strength');
    Power:=Rec.GetInteger('Power');
    SchussMax:=Rec.GetInteger('SchussMax');
    SchussArt:=TSchussType(Rec.GetInteger('SchussArt'));
    TimeUnits:=Rec.GetInteger('TimeUnits');
    Genauigkeit:=Rec.GetInteger('Genauigkeit');
    Ladezeit:=Rec.GetInteger('Frequenz');
    MunGewicht:=Rec.GetDouble('MunGewicht');
  end;
end;

function SoldatPanzerungToRecord(const SoldatPanzerung: TSoldatPanzerung): TExtRecord;
begin
  Result:=TExtRecord.Create(RecordSoldatPanzerung);
  with SoldatPanzerung do
  begin
    result.Setboolean('Gesetzt',Gesetzt);
    result.SetCardinal('ID',ID);
    result.SetString('Name',Name);
    result.SetInteger('Image',Image);
    result.Setdouble('Gewicht',Gewicht);
  end;
end;

function RecordToSoldatPanzerung(Rec: TExtRecord): TSoldatPanzerung;
begin
  with result do
  begin
    ID:=Rec.GetCardinal('ID');
    Gesetzt:=Rec.Getboolean('Gesetzt');
    Name:=Rec.GetString('Name');
    Image:=Rec.GetInteger('Image');
    Gewicht:=Rec.Getdouble('Gewicht');
  end;
end;

function SoldatGuertelToRecord(const SoldatGuertel: TSoldatGuertel): TExtRecord;
begin
  Result:=TExtRecord.Create(RecordSoldatGuertel);
  with SoldatGuertel do
  begin
    result.Setboolean('Gesetzt',Gesetzt);
    result.SetCardinal('ID',ID);
    result.SetString('Name',Name);
    result.Setdouble('Gewicht',Gewicht);
    result.SetInteger('Slots',Slots);
  end;
end;

function RecordToSoldatGuertel(Rec: TExtRecord): TSoldatGuertel;
begin
  with result do
  begin
    ID:=Rec.GetCardinal('ID');
    Gesetzt:=Rec.Getboolean('Gesetzt');
    Name:=Rec.GetString('Name');
    Gewicht:=Rec.Getdouble('Gewicht');
    Slots:=Rec.GetInteger('Slots');
  end;
end;

function PatentToRecord(const Patent: TPatent): TExtRecord;
begin
  Result:=TExtRecord.Create(RecordPatent);
  with Patent do
  begin
    result.SetInteger('Gebuehr',Gebuehr);
    result.SetInteger('Laufzeit',Laufzeit);
    result.SetInteger('Woche',Woche);

    result.SetInteger('Zeit',Zeit);
    result.SetDouble('Anstieg',Anstieg);
    result.SetInteger('Satt',Satt);
  end;
end;

function RecordToPatent(Rec: TExtRecord): TPatent;
begin
  with result do
  begin
    Gebuehr:=Rec.GetInteger('Gebuehr');
    Laufzeit:=Rec.GetInteger('Laufzeit');
    Woche:=Rec.GetInteger('Woche');
    Zeit:=Rec.GetInteger('Zeit');
    Anstieg:=Rec.GetDouble('Anstieg');
    Satt:=Rec.GetInteger('Satt');
  end;
end;

function MissionUFOToRecord(const UFO: TMissionUFO): TExtRecord;
begin
  result:=TExtRecord.Create(RecordMissionUFOs);
  with UFO do
  begin
    result.SetCardinal('ID',UFOID);
    Result.SetBoolean('Deleted',Delete);
    result.SetString('OnShootDown',OnShootDown);
    result.SetString('OnEscape',OnEscape);
    result.SetString('OnDiscovered',OnDiscovered);
  end;
end;

function RecordToMissionUFO(Rec: TExtRecord): TMissionUFO;
begin
  with result do
  begin
    UFOID:=Rec.GetCardinal('ID');
    Delete:=Rec.GetBoolean('Delete');
    OnShootDown:=Rec.GetString('OnShootDown');
    OnEscape:=Rec.GetString('OnEscape');
    OnDiscovered:=Rec.GetString('OnDiscovered');
  end;
end;

function MissionSchiffToRecord(const Trans: TMissionRaumschiff): TExtRecord;
begin
  result:=TExtRecord.Create(RecordMissionSchiff);
  with Trans do
  begin
    result.SetInteger('SchiffID',SchiffID);
    result.SetString('OnDestroy',OnDestroy);
    result.SetString('OnReached',OnReached);
  end;
end;

function RecordToMissionSchiff(Rec: TExtRecord): TMissionRaumschiff;
begin
  with result do
  begin
    SchiffID:=Rec.GetInteger('SchiffID');
    OnDestroy:=Rec.GetString('OnDestroy');
    OnReached:=Rec.GetString('OnReached');
  end;
end;

function MissionEinsatzToRecord(const Einsatz: TMissionEinsatz): TExtRecord;
begin
  result:=TExtRecord.Create(RecordMissionEinsatz);
  with Einsatz do
  begin
    result.SetCardinal('ID',ID);
    result.SetString('OnWin',OnWin);
    result.SetString('OnTimeUp',OnTimeUp);
  end;
end;

function RecordToMissionEinsatz(Rec: TExtRecord): TMissionEinsatz;
begin
{  with result do
  begin
    ID:=Rec.GetCardinal('ID');
    Einsatz:=nil;
    OnWin:=Rec.GetString('OnWin');
    OnTimeUp:=Rec.GetString('OnTimeUp');
    NotifyHandleWin:=0;
    NotifyHandleDestroy:=0;
  end;}
end;

function LagerAuftragToRecord(const Auftrag: TAuftrag): TExtRecord;
begin
  result:=TExtRecord.Create(RecordLagerAuftrag);
  with Auftrag do
  begin
    result.SetCardinal('ID',ID);
    result.Setboolean('Kauf',Kauf);
    result.SetCardinal('Item',Item);
    result.SetInteger('Preis',Preis);
    result.SetInteger('Anzahl',Anzahl);
    result.SetCardinal('BasisID',BasisID);
  end;
end;

function RecordToLagerAuftrag(Rec: TExtRecord): TAuftrag;
begin
  with result do
  begin
    ID:=Rec.GetCardinal('ID');
    Kauf:=Rec.Getboolean('Kauf');
    Item:=Rec.GetCardinal('Item');
    Preis:=Rec.GetInteger('Preis');
    Anzahl:=Rec.GetInteger('Anzahl');
    BasisID:=Rec.GetCardinal('BasisID');
  end;
end;

function LagerAngebotToRecord(const Angebot: TAngebot): TExtRecord;
begin
  result:=TExtRecord.Create(RecordLagerAngebot);
  with Angebot do
  begin
    result.SetCardinal('ID',ID);
    result.SetInteger('Organisation',Organisation);
    result.SetInteger('Anzahl',Anzahl);
    result.SetInteger('Preis',Preis);
    result.SetInteger('Rounds',Rounds);
  end;
end;

function RecordToLagerAngebot(Rec: TExtRecord): TAngebot;
begin
  with result do
  begin
    ID:=Rec.GetCardinal('ID');
    Organisation:=Rec.GetInteger('Organisation');
    Anzahl:=Rec.GetInteger('Anzahl');
    Preis:=Rec.GetInteger('Preis');
    Rounds:=Rec.GetInteger('Rounds');
  end;
end;

function WaffenzelleToRecord(const Waffenzelle: TWaffenzelle): TExtRecord;
begin
  result:=TExtRecord.Create(RecordWaffenzelle);
  with Waffenzelle do
  begin
    result.Setboolean('Installiert',Installiert);
    result.SetString('Name',Name);
    result.SetCardinal('ID',ID);
    result.SetInteger('MunIndex',MunIndex);
    result.SetCardinal('Munition',Munition);
    result.SetInteger('WaffType',Integer(WaffType));
    result.SetCardinal('Strength',Strength);
    result.SetInteger('PixPerSec',PixPerSec);
    result.SetInteger('Laden',Laden);
    result.Setboolean('SendMessage',SendMessage);
    result.Setboolean('Verfolgung',Verfolgung);
    result.SetInteger('Reichweite',Reichweite);
    result.SetInteger('ReserveMunition',ReserveMunition);
  end;
end;

function RecordToWaffenzelle(Rec: TExtRecord): TWaffenzelle;
begin
  with result do
  begin
    Installiert:=Rec.Getboolean('Installiert');
    Name:=Rec.GetString('Name');
    ID:=Rec.GetCardinal('ID');
    MunIndex:=Rec.GetInteger('MunIndex');
    Munition:=Rec.GetCardinal('Munition');
    WaffType:=TWaffenType(Rec.GetInteger('WaffType'));
    Strength:=Rec.GetCardinal('Strength');
    PixPerSec:=Rec.GetInteger('PixPerSec');
    Laden:=Rec.GetInteger('Laden');
    SendMessage:=Rec.Getboolean('SendMessage');
    Verfolgung:=Rec.Getboolean('Verfolgung');
    Reichweite:=Rec.GetInteger('Reichweite');
    ReserveMunition:=Rec.GetInteger('ReserveMunition');
  end;
end;

function EinrichtungToRecord(const Einrichtung: TEinrichtung): TExtRecord;
begin
  result:=TExtRecord.Create(RecordEinrichtung);
  with Einrichtung do
  begin
    result.SetString('Name',Name);
    result.SetCardinal('ID',ID);
    result.SetInteger('KaufPreis',KaufPreis);
    result.SetInteger('SmallSchiff',SmallSchiff);
    result.SetInteger('SmallBelegt',SmallBelegt);
    result.SetInteger('LargeSchiff',LargeSchiff);
    result.Setdouble('LagerRaum',LagerRaum);
    result.Setdouble('LagerBelegt',LagerBelegt);
    result.SetInteger('Quartiere',Quartiere);
    result.SetInteger('WohnBelegt',WohnBelegt);
    result.SetInteger('WerkRaum',WerkRaum);
    result.SetInteger('WerkBelegt',WerkBelegt);
    result.SetInteger('Abwehr',Abwehr);
    result.SetInteger('LaborRaum',LaborRaum);
    result.SetInteger('LaborBelegt',LaborBelegt);
    result.SetInteger('days',days);
    result.SetInteger('BauZeit',BauZeit);
    result.SetInteger('Reichweite',Reichweite);
    result.SetInteger('Frequenz',Frequenz);
    result.SetCardinal('UFOID',UFOID);
    result.SetString('Info',Info);
    
    result.SetString('BuildImageBase',ImageBase);
    result.SetString('BuildImageUnder',ImageUnder);

    result.SetInteger('UnitWidth',Width);
    result.SetInteger('UnitHeight',Height);
    result.SetInteger('RoomTyp',Integer(RoomTyp));

    result.SetInteger('SensorWidth',SensorWidth);
    result.SetInteger('ShieldStrength',ShieldStrength);
    result.SetInteger('AktShieldPoints',AktShieldPoints);
    result.SetInteger('Treffsicherheit',Treffsicherheit);
    result.SetInteger('maxReichweite',maxReichweite);
    result.SetInteger('WaffenArt',Integer(WaffenArt));

    result.SetInteger('BuildFloor',Position.BuildFloor);

    result.SetInteger('HitPoints',Hitpoints);
    result.SetInteger('AktHitPoints',AktHitpoints);

    result.SetInteger('AlphatronStorage',AlphatronStorage);

    result.SetInteger('XPos',Position.X);
    result.SetInteger('YPos',Position.Y);

    result.SetCardinal('RoomID',RoomID);
    result.SetCardinal('BaseID',BaseID);

    result.SetInteger('ActualValue',ActualValue);
    result.SetInteger('DaysToDecValue',DaysToDecValue);
  end;
end;

function RecordToEinrichtung(Rec: TExtRecord): TEinrichtung;
begin
  with result do
  begin
    Name:=Rec.GetString('Name');
    ID:=Rec.GetCardinal('ID');
    KaufPreis:=Rec.GetInteger('KaufPreis');
    SmallSchiff:=Rec.GetInteger('SmallSchiff');
    SmallBelegt:=Rec.GetInteger('SmallBelegt');
    LargeSchiff:=Rec.GetInteger('LargeSchiff');
    LagerRaum:=Rec.Getdouble('LagerRaum');
    LagerBelegt:=Rec.Getdouble('LagerBelegt');
    Quartiere:=Rec.GetInteger('Quartiere');
    WohnBelegt:=Rec.GetInteger('WohnBelegt');
    WerkRaum:=Rec.GetInteger('WerkRaum');
    WerkBelegt:=Rec.GetInteger('WerkBelegt');
    Abwehr:=Rec.GetInteger('Abwehr');
    LaborRaum:=Rec.GetInteger('LaborRaum');
    LaborBelegt:=Rec.GetInteger('LaborBelegt');
    days:=Rec.GetInteger('days');
    BauZeit:=Rec.GetInteger('BauZeit');
    Reichweite:=Rec.GetInteger('Reichweite');
    Frequenz:=Rec.GetInteger('Frequenz');
    UFOID:=Rec.GetCardinal('UFOID');
    Info:=Rec.GetString('Info');
    ImageBase:=Rec.GetString('BuildImageBase');
    ImageUnder:=Rec.GetString('BuildImageUnder');
    Width:=Rec.GetInteger('UnitWidth');
    Height:=Rec.GetInteger('UnitHeight');

    RoomTyp:=TRoomTyp(Rec.GetInteger('RoomTyp'));

    SensorWidth:=Rec.GetInteger('SensorWidth');
    ShieldStrength:=Rec.GetInteger('ShieldStrength');
    AktShieldPoints:=Rec.GetInteger('AktShieldPoints');
    Treffsicherheit:=Rec.GetInteger('Treffsicherheit');
    maxReichweite:=Rec.GetInteger('maxReichweite');
    WaffenArt:=TWaffenType(Rec.GetInteger('WaffenArt'));

    Hitpoints:=Rec.GetInteger('HitPoints');
    AktHitpoints:=Rec.GetInteger('AktHitPoints');

    AlphatronStorage:=Rec.GetInteger('AlphatronStorage');

    Position.BuildFloor:=Rec.GetInteger('BuildFloor');
    Position.X:=Rec.GetInteger('XPos');
    Position.Y:=Rec.GetInteger('YPos');

    RoomID:=Rec.GetCardinal('RoomID');
    BaseID:=Rec.GetCardinal('BaseID');

    ActualValue:=Rec.GetInteger('ActualValue');
    DaysToDecValue:=Rec.GetInteger('DaysToDecValue');
  end;
end;

function ForscherToRecord(const Forscher: TForscher): TExtRecord;
begin
  result:=TExtRecord.Create(RecordForscher);
  with Forscher do
  begin
    result.SetString('Name',Name);
    result.SetDouble('Strength',Strength);
    result.SetInteger('BitNr',BitNr);
    result.SetInteger('Days',Days);
    result.SetCardinal('ID',ID);
    result.Setboolean('Train',Train);
    result.SetCardinal('BasisID',BasisID);
    Result.SetInteger('TrainTime', TrainTime);
    Result.SetInteger('ForschTime', ForschTime);
  end;
end;

function RecordToForscher(Rec: TExtRecord): TForscher;
begin
  with result do
  begin
    Name:=Rec.GetString('Name');
    Strength:=Rec.GetDouble('Strength');
    BitNr:=Rec.GetInteger('BitNr');
    Days:=Rec.GetInteger('Days');
    ID:=Rec.GetCardinal('ID');
    Train:=Rec.Getboolean('Train');
    BasisID:=Rec.GetCardinal('BasisID');
    TrainTime := Rec.GetInteger('TrainTime');
    ForschTime := Rec.GetInteger('ForschTime');
  end;
end;

function EndedForschProjectToRecord(const EndedForschProject: TEndedForschProjekt): TExtRecord;
begin
  result:=TExtRecord.Create(RecordEndedForschProject);
  with EndedForschProject do
  begin
    result.SetCardinal('ID',ID);
    result.SetCardinal('ParentID',ParentID);
    result.SetInteger('TypeID',Integer(TypeID));
    result.SetString('Name',Name);
  end;
end;

function RecordToEndedForschProject(Rec: TExtRecord): TEndedForschProjekt;
begin
  with result do
  begin
    ID:=Rec.GetCardinal('ID');
    ParentID:=Rec.GetCardinal('ParentID');
    TypeID:=TProjektType(Rec.GetInteger('TypeID'));
    Name:=Rec.GetString('Name');
  end;
end;

function WatchMarktSettingsToRecord(const WatchMarktSettings: TWatchMarktSettings): TExtRecord;
begin
  result:=TExtRecord.Create(RecordWatchMarktSettings);
  with WatchMarktSettings do
  begin
    result.SetCardinal('BasisID',BasisID);
    result.SetBoolean('Active',Active);
    result.SetInteger('minFaehigkeiten',minFaehigkeiten);
    result.SetInteger('WatchType',Integer(WatchType));
  end;
end;

function RecordToWatchMarktSettings(Rec: TExtRecord): TWatchMarktSettings;
begin
  with result do
  begin
    BasisID:=Rec.GetCardinal('BasisID');
    Active:=Rec.GetBoolean('Active');
    minFaehigkeiten:=Rec.GetInteger('minFaehigkeiten');
    WatchType:=TWatchType(Rec.GetInteger('WatchType'));
  end;
end;

function LandToRecord(const Land: TLand): TExtRecord;
begin
  result:=TExtRecord.Create(RecordLand);
  with Land do
  begin
    result.SetString('Name',Name);
    result.SetCardinal('ID',ID);
    result.SetInteger('Image',Image);
    result.SetInteger('InfoIndex',InfoIndex);
    result.SetInteger('Friendly',Friendly);
    result.SetInteger('UFOHours',UFOHours);
    result.SetInteger('Einwohner',Einwohner);
    result.SetInteger('Budget',Budget);
    result.SetInteger('Zufriedenheit',Zufriedenheit);
    result.SetInteger('SchErsatz',SchErsatz);
    result.SetInteger('Towns',Towns);
  end;
end;

function RecordToLand(Rec: TExtRecord): TLand;
begin
  with result do
  begin
    Name:=Rec.GetString('Name');
    ID:=Rec.GetCardinal('ID');
    Image:=Rec.GetInteger('Image');
    InfoIndex:=Rec.GetInteger('InfoIndex');
    Friendly:=Rec.GetInteger('Friendly');
    UFOHours:=Rec.GetInteger('UFOHours');
    Einwohner:=Rec.GetInteger('Einwohner');
    Budget:=Rec.GetInteger('Budget');
    Zufriedenheit:=Rec.GetInteger('Zufriedenheit');
    SchErsatz:=Rec.GetInteger('SchErsatz');
    Towns:=Rec.GetInteger('Towns');
  end;
end;

function TechnikerToRecord(const Techniker: TTechniker): TExtRecord;
begin
  result:=TExtRecord.Create(RecordTechniker);
  with Techniker do
  begin
    result.SetCardinal('TechnikerID',TechnikerID);
    result.SetString('Name',Name);
    result.Setdouble('Strength',Strength);
    result.SetInteger('BitNr',BitNr);
    result.SetInteger('Days',Days);
    result.Setboolean('Ausbil',Ausbil);
    result.SetCardinal('ID',ProjektID);
    result.SetCardinal('BasisID',BasisID);
    result.SetBoolean('AlphatronMining',AlphatronMining);
    result.SetBoolean('Repair',Repair);
    Result.SetInteger('TrainTime', TrainTime);
    Result.SetInteger('ProdTime', ProdTime);
  end;
end;

function RecordToTechniker(Rec: TExtRecord): TTechniker;
begin
  with result do
  begin
    TechnikerID:=Rec.GetCardinal('TechnikerID');
    Name:=Rec.GetString('Name');
    Strength:=Rec.Getdouble('Strength');
    BitNr:=Rec.GetInteger('BitNr');
    Days:=Rec.GetInteger('Days');
    Ausbil:=Rec.Getboolean('Ausbil');
    ProjektID:=Rec.GetCardinal('ID');
    BasisID:=Rec.GetCardinal('BasisID');
    AlphatronMining:=Rec.GetBoolean('AlphatronMining');
    Repair:=Rec.GetBoolean('Repair');
    TrainTime:=Rec.GetInteger('TrainTime');
    ProdTime:=Rec.GetInteger('ProdTime');
  end;
end;

function ProduktionToRecord(const Produktion: TProduktion): TExtRecord;
begin
  result:=TExtRecord.Create(RecordProduktion);
  with Produktion do
  begin
    result.SetCardinal('ID',ID);
    result.SetCardinal('ItemID',ItemID);
    result.SetInteger('Kost',Kost);
    result.SetInteger('Reserved',Reserved);
    result.Setdouble('LagerV',LagerV);
    result.SetInteger('Anzahl',Anzahl);
    result.SetInteger('Gesamt',Gesamt);
    result.Setdouble('Hour',Hour);
    result.Setboolean('Product',Product);
    result.SetCardinal('BasisID',BasisID);
  end;
end;

function RecordToProduktion(Rec: TExtRecord): TProduktion;
begin
  with result do
  begin
    ID:=Rec.GetCardinal('ID');
    ItemID:=Rec.GetCardinal('ItemID');
    Kost:=Rec.GetInteger('Kost');
    Reserved:=Rec.GetInteger('Reserved');
    LagerV:=Rec.Getdouble('LagerV');
    Anzahl:=Rec.GetInteger('Anzahl');
    Gesamt:=Rec.GetInteger('Gesamt');
    Hour:=Rec.Getdouble('Hour');
    Product:=Rec.Getboolean('Product');
    BasisID:=Rec.GetCardinal('BasisID');
  end;
end;

function RaumschiffModelToRecord(const RaumschiffModel: TRaumschiffModel): TExtRecord;
begin
  result:=TExtRecord.Create(RecordRaumschiffModel);
  with RaumschiffModel do
  begin
    result.SetString('Name',Name);
    result.SetCardinal('ID',ID);
    result.SetCardinal('KaufPreis',KaufPreis);
    result.SetCardinal('VerKaufPreis',VerKaufPreis);
    result.SetInteger('WaffenZellen',WaffenZellen);
    result.SetInteger('HitPoints',HitPoints);
    result.SetInteger('LagerPlatz',LagerPlatz);
    result.SetInteger('Soldaten',Soldaten);
    result.SetInteger('BauZeit',BauZeit);
    result.SetInteger('ExtendsSlots',ExtendsSlots);
    result.SetInteger('SensorWeite',SensorWeite);
    result.SetString('Info',Info);
  end;
end;

function RecordToRaumschiffModel(Rec: TExtRecord): TRaumschiffModel;
begin
  with result do
  begin
    Name:=Rec.GetString('Name');
    ID:=Rec.GetCardinal('ID');
    KaufPreis:=Rec.GetCardinal('KaufPreis');
    VerKaufPreis:=Rec.GetCardinal('VerKaufPreis');
    WaffenZellen:=Rec.GetInteger('WaffenZellen');
    HitPoints:=Rec.GetInteger('HitPoints');
    LagerPlatz:=Rec.GetInteger('LagerPlatz');
    Soldaten:=Rec.GetInteger('Soldaten');
    BauZeit:=Rec.GetInteger('BauZeit');
    ExtendsSlots:=Rec.GetInteger('ExtendsSlots');
    SensorWeite:=Rec.GetInteger('SensorWeite');
    Info:=Rec.GetString('Info');
  end;
end;

function RaumschiffShieldToRecord(const RaumschiffShield: TShield): TExtRecord;
begin
  result:=TExtRecord.Create(RecordRaumschiffShield);
  with RaumschiffShield do
  begin
    result.SetBoolean('Installiert',Installiert);
    result.SetString('Name',Name);
    result.SetCardinal('ID',ID);
    result.SetInteger('Points',Points);
    result.SetInteger('Abwehr',Abwehr);
    result.SetInteger('Laden',Laden);
  end;
end;

function RecordToRaumschiffShield(Rec: TExtRecord): TShield;
begin
  with result do
  begin
    Installiert:=Rec.GetBoolean('Installiert');
    Name:=Rec.GetString('Name');
    ID:=Rec.GetCardinal('ID');
    Points:=Rec.GetInteger('Points');
    Abwehr:=Rec.GetInteger('Abwehr');
    Laden:=Rec.GetInteger('Laden');
  end;
end;

function RaumschiffMotorToRecord(const RaumschiffMotor: TMotor): TExtRecord;
begin
  result:=TExtRecord.Create(RecordRaumschiffMotor);
  with RaumschiffMotor do
  begin
    result.SetBoolean('Installiert',Installiert);
    result.SetString('Name',Name);
    result.SetCardinal('ID',ID);
    result.SetInteger('MaxLiter',MaxLiter);
    result.Setdouble('Liter',Liter);
    result.SetInteger('Verbrauch',Verbrauch);
    result.Setboolean('Duesen',Duesen);
    result.SetInteger('PpS',PpS);
  end;
end;

function RecordToRaumschiffMotor(Rec: TExtRecord): TMotor;
begin
  with result do
  begin
    Installiert:=Rec.GetBoolean('Installiert');
    Name:=Rec.GetString('Name');
    ID:=Rec.GetCardinal('ID');
    MaxLiter:=Rec.GetInteger('MaxLiter');
    Liter:=Rec.Getdouble('Liter');
    Verbrauch:=Rec.GetInteger('Verbrauch');
    Duesen:=Rec.Getboolean('Duesen');
    PpS:=Rec.GetInteger('PpS');
  end;
end;

function RaumschiffExtensionToRecord(const RaumschiffExtension: TExtensionZelle): TExtRecord;
var
  Dummy: Integer;
begin
  result:=TExtRecord.Create(RecordRaumschiffExtension);
  result.SetBoolean('Installiert',RaumschiffExtension.Installiert);
  result.SetCardinal('ID',RaumschiffExtension.ID);
  with result.GetRecordList('Properties') do
  begin
    for Dummy:=0 to RaumschiffExtension.ExtCount-1 do
    begin
      with Add do
      begin
        SetInteger('Prop',Integer(RaumschiffExtension.Extensions[Dummy].Prop));
        SetInteger('Value',Integer(RaumschiffExtension.Extensions[Dummy].Value));
      end;
    end;
  end;
end;

function RecordToRaumschiffExtension(Rec: TExtRecord): TExtensionZelle;
var
  Dummy: Integer;
begin
  result.Installiert:=Rec.GetBoolean('Installiert');
  result.ID:=Rec.GetCardinal('ID');
  with Rec.GetRecordList('Properties') do
  begin
    result.ExtCount:=Count;
    for Dummy:=0 to Count-1 do
    begin
      with Item[Dummy] do
      begin
        Result.Extensions[Dummy].Prop:=TExtensionProp(GetInteger('Prop'));
        Result.Extensions[Dummy].Value:=GetInteger('Value');
      end;
    end;
  end;
end;

function BackPackItemToRecord(const BackPackItem: TBackPackItem): TExtRecord;
begin
  result:=TExtRecord.Create(RecordBackPackItem);
  with BackPackItem do
  begin
    result.SetBoolean('IsSet',IsSet);
    result.SetCardinal('ID',ID);
    result.SetInteger('Schuesse',Schuesse);
    result.SetDouble('Gewicht',Gewicht);
  end;
end;

function RecordToBackPackItem(Rec: TExtRecord): TBackPackItem;
begin
  with result do
  begin
    IsSet:=Rec.GetBoolean('IsSet');
    ID:=Rec.GetCardinal('ID');
    Schuesse:=Rec.GetInteger('Schuesse');
    Gewicht:=Rec.GetDouble('Gewicht');
  end;
end;

function MissionDataToRecord(const MissionData: TMissionData): TExtRecord;
var
  Dummy: Integer;
begin
  result:=TExtRecord.Create(RecordMissionSkript);
  with MissionData do
  begin
    result.SetCardinal('ID',ID);
    result.SetInteger('Typ',Integer(Typ));
    result.SetString('Name',Name);
    result.SetBoolean('Active',Active);
    result.SetString('Skript',Skript);

    for Dummy:=0 to high(Triggers) do
    begin
      with result.GetRecordList('Triggers').Add do
      begin
        SetInteger('TriggerTyp',Integer(MissionData.Triggers[Dummy].TriggerTyp));

        SetBoolean('Negation',MissionData.Triggers[Dummy].Negation);

        SetCardinal('CarParam1',MissionData.Triggers[Dummy].CarParam1);
        SetInteger('IntParam1',MissionData.Triggers[Dummy].IntParam1);
        SetInteger('IntParam2',MissionData.Triggers[Dummy].IntParam2);
        SetString('StringParam1',MissionData.Triggers[Dummy].StringParam1);

        SetBoolean('Cleared',MissionData.Triggers[Dummy].Cleared);
      end;
    end;
  end;
end;

function RecordToMissionData(Rec: TExtRecord): TMissionData;
var
  Dummy: Integer;
begin
  with result do
  begin
    ID:=Rec.GetCardinal('ID');
    Typ:=TMissionSkriptTyp(Rec.GetInteger('Typ'));
    Name:=Rec.GetString('Name');
    Active:=Rec.GetBoolean('Active');
    Skript:=Rec.GetString('Skript');

    with Rec.GetRecordList('Triggers') do
    begin
      SetLength(Triggers,Count);
      for Dummy:=0 to high(Triggers) do
      begin
        Triggers[Dummy].TriggerTyp:=TTriggerTyp(Item[Dummy].GetInteger('TriggerTyp'));

        Triggers[Dummy].Negation:=Item[Dummy].GetBoolean('Negation');

        Triggers[Dummy].CarParam1:=Item[Dummy].GetCardinal('CarParam1');
        Triggers[Dummy].IntParam1:=Item[Dummy].GetInteger('IntParam1');
        Triggers[Dummy].IntParam2:=Item[Dummy].GetInteger('IntParam2');
        Triggers[Dummy].StringParam1:=Item[Dummy].GetString('StringParam1');

        Triggers[Dummy].Cleared:=Item[Dummy].GetBoolean('Cleared');
      end;
    end;
  end;
end;

var
  Temp: TExtRecordDefinition;

initialization
  Temp:=TExtRecordDefinition.Create('ExtensionProperty');
  Temp.ParentDescription:=true;
  Temp.AddInteger('Prop',0,high(Integer));
  Temp.AddInteger('Value',low(Integer),high(Integer));

  RecordLagerListe:=TExtRecordDefinition.Create('LagerItem');
  with RecordLagerListe do
  begin
    AddString('Name',50);
    AddInteger('Anzahl',0,high(Integer));
    AddInteger('Level',0,high(Integer));
    AddCardinal('ID',0,high(Cardinal));
    AddDouble('LagerV',0.1,10000,1);
    AddInteger('WaffType',0,Integer(high(TWaffenType)));
    AddDouble('Gewicht',0.1,10000,1);
    AddInteger('KaufPreis',0,high(Integer));
    AddInteger('VerKaufPreis',0,high(Integer));
    AddInteger('Munition',0,high(Integer));
    AddInteger('TypeID',0,Integer(high(TProjektType)));
    AddCardinal('Strength',0,high(Cardinal));

    AddBoolean('SchussType[stAuto]');
    AddBoolean('SchussType[stSpontan]');
    AddBoolean('SchussType[stGezielt]');
    AddBoolean('SchussType[stSchlag]');
    AddBoolean('SchussType[stStossen]');
    AddBoolean('SchussType[stSchwingen]');

    AddLoadLink('Auto','SchussType[stAuto]');
    AddLoadLink('Spontan','SchussType[stSpontan]');
    AddLoadLink('Gezielt','SchussType[stGezielt]');
    AddLoadLink('Schlag','SchussType[stSchlag]');

    AddCardinal('Munfor',0,high(Cardinal));
    AddInteger('Panzerung',0,Integer(high(Integer)));
    AddInteger('PixPerSec',0,Integer(high(Integer)));
    AddInteger('Laden',0,Integer(high(Integer)));
    AddBoolean('Verfolgung');
    AddInteger('ImageIndex',0,Integer(high(Integer)));
    AddInteger('Land',-1,Integer(high(Integer)));
    AddBoolean('HerstellBar');
    AddInteger('Reichweite',0,Integer(high(Integer)));
    AddBoolean('Einhand',false);
    AddInteger('Power',0,Integer(high(Integer)));
    AddInteger('TimeUnits',0,Integer(high(Integer)));
    AddString('Info',100000);
    AddRecordList('Extensions',Temp);
    AddString('UFOPaedieImage',100);
    AddInteger('Explosion',1,15,4);
    AddBoolean('UseAble',true);
    AddBoolean('PublicPlan',true);
    AddInteger('Genauigkeit',0,100,75);
    AddInteger('Frequenz',0,Integer(high(Integer)),250);
    AddInteger('Alphatron',0,Integer(high(Integer)),-1);
    AddInteger('ProdTime',0,Integer(high(Integer)),-1);
    AddInteger('AlienChance',0,100,25);
    AddInteger('NeedIQ',0,200,50);
    AddBoolean('Visible',true);
    AddBoolean('AlienItem',false);
    AddInteger('ShootSurPlus',0,high(Integer));
    AddDouble('ResearchedDate',0,MaxDouble, 200); //Komma-Wert sollte eigentlich maximal Gro� sein - wird es aber verwendet?!?

    AddString('ShootingSound',100,'default\Sound:ShootWeapon');
  end;

  RecordForschung:=TExtRecordDefinition.Create('Forschungen');
  with RecordForschung do
  begin
    AddCardinal('ID',0,High(Cardinal));
    AddDouble('Hour',0,10000000,0);
    AddDouble('Gesamt',0,10000000,0);
    AddInteger('TypeID',0,Integer(High(TProjektType)));
    AddBoolean('Update',false);
    AddBoolean('ResearchItem',false);
    AddCardinal('BasisID',0,High(Cardinal));
    AddString('Info',100000);
    AddInteger('ResearchCount',0,High(Integer));
    AddInteger('ResearchType',0,High(Integer),Integer(ftNone));
    AddBoolean('Started',false);
  end;

  RecordSoldatInfo:=TExtRecordDefinition.Create('SoldatInfo');
  with RecordSoldatInfo do
  begin
    AddString('Name',50);
    AddDouble('Ges',0,500,10);
    AddInteger('EP',0,High(Integer));

    AddDouble('MaxGes',0,500,10);
    AddDouble('PSIAn',0,500,10);
    AddDouble('PSIAb',0,500,10);
    AddDouble('Zeit',0,500,10);
    AddDouble('Treff',0,500,10);
    AddDouble('Kraft',0,500,10);
    AddDouble('Sicht',-1,500,10,-1);
    AddDouble('IQ',-1,500,10,-1);
    AddDouble('React',-1,500,10,-1);

    AddDouble('LastMaxGes',0,500,10);
    AddDouble('LastPSIAn',0,500,10);
    AddDouble('LastPSIAb',0,500,10);
    AddDouble('LastZeit',0,500,10);
    AddDouble('LastTreff',0,500,10);
    AddDouble('LastKraft',0,500,10);
    AddDouble('LastSicht',-1,500,10,-1);
    AddDouble('LastIQ',-1,500,10,-1);
    AddDouble('LastReact',-1,500,10,-1);

    AddInteger('Image',0,1000);
    AddInteger('Raum',low(Integer),high(Integer));
    AddInteger('Slot',low(Integer),high(Integer));
    AddCardinal('BasisID',0,High(Cardinal));
    AddBoolean('LeftHand');
    AddBoolean('Train');
    AddInteger('Tag',0,high(Integer));
    AddInteger('Einsatz',0,high(Integer));
    AddInteger('Treffer',0,high(Integer));
    AddInteger('TrainTime',0,high(Integer));

    AddString('ClassName',50);
    AddInteger('FaehigGes',-1,100,-1);
    AddInteger('FaehigPSIAn',-1,100,-1);
    AddInteger('FaehigPSIAb',-1,100,-1);
    AddInteger('FaehigZeit',-1,100,-1);
    AddInteger('FaehigTreff',-1,100,-1);
    AddInteger('FaehigKraft',-1,100,-1);
    AddInteger('FaehigSicht',-1,100,-1);
    AddInteger('FaehigIQ',-1,100,-1);
    AddInteger('FaehigReact',-1,100,-1);
  end;

  RecordSoldatWaffe:=TExtRecordDefinition.Create('SoldatWaffe');
  with RecordSoldatWaffe do
  begin
    AddBoolean('Gesetzt',false);
    AddCardinal('ID',0,high(Cardinal));
    AddString('Name',50);
    AddInteger('Image',0,high(Integer));
    AddInteger('TypeID',0,Integer(high(TProjektType)));
    AddInteger('WaffenArt',0,Integer(high(TWaffenType)));
    AddDouble('Gewicht',0,1000,1);
    AddBoolean('Zweihand');
    AddBoolean('MunGesetzt',false);
    AddInteger('MunImage',0,high(Integer));
    AddCardinal('MunID',0,high(Cardinal));
    AddInteger('Schuesse',0,high(Integer));
    AddInteger('TimeLeft',0,high(Integer));
    AddInteger('Strength',0,high(Integer));
    AddInteger('Power',0,high(Integer));
    AddInteger('SchussMax',0,high(Integer));
    AddInteger('SchussArt',0,Integer(high(TSchussType)));
    AddInteger('TimeUnits',0,high(Byte));
    AddInteger('Genauigkeit',0,100,75);
    AddInteger('Frequenz',0,high(Integer),250);
    AddDouble('MunGewicht',0,1000,1);
  end;

  RecordSoldatPanzerung:=TExtRecordDefinition.Create('SoldatPanzerung');
  with RecordSoldatPanzerung do
  begin
    Addboolean('Gesetzt');
    AddCardinal('ID',0,high(Cardinal));
    AddString('Name',50);
    AddInteger('Image',-1,high(Integer));
    Adddouble('Gewicht',0,10000,10);
  end;

  RecordSoldatGuertel:=TExtRecordDefinition.Create('SoldatGuertel');
  with RecordSoldatGuertel do
  begin
    Addboolean('Gesetzt');
    AddCardinal('ID',0,high(Cardinal));
    AddString('Name',50);
    Adddouble('Gewicht',0,10000,10);
    AddInteger('Slots',0,high(Integer));
  end;

  RecordPatent:=TExtRecordDefinition.Create('Patent');
  with RecordPatent do
  begin
    AddInteger('Laufzeit',0,high(Integer),0);

    AddInteger('Woche',0,high(Integer),0);
    AddInteger('Satt',0,high(Integer),0);

    AddInteger('Gebuehr',0,high(Integer),0);
    AddInteger('Zeit',0,high(Integer),0);
    AddDouble('Anstieg',0,5,5);
  end;

  RecordMissionEvent:=TExtRecordDefinition.Create('MissionEvent');
  with RecordMissionEvent do
  begin
    AddCardinal('ID',0,high(Cardinal));
    AddBoolean('Aktiv');
    AddString('SkriptEvent',5000);
    AddInteger('Interval',0,high(Integer));
    AddBoolean('Repeated');
    AddInteger('MinutesDone',0,high(Integer));
  end;

  RecordMissionUFOs:=TExtRecordDefinition.Create('MissionUFO');
  with RecordMissionUFOs do
  begin
    AddCardinal('ID',0,high(Cardinal));
    AddBoolean('Deleted');
    AddString('OnShootDown',50);
    AddString('OnEscape',50);
    AddString('OnDiscovered',50);
  end;

  RecordMissionSchiff:=TExtRecordDefinition.Create('MissionRaumschiff');
  with RecordMissionSchiff do
  begin
    AddInteger('SchiffID',0,high(Integer));
    AddString('OnDestroy',50);
    AddString('OnReached',50);
  end;

  RecordMissionEinsatz:=TExtRecordDefinition.Create('MissionEinsatz');
  with RecordMissionEinsatz do
  begin
    AddCardinal('ID',0,high(Cardinal));
    AddString('OnWin',50);
    AddString('OnTimeUp',50);
  end;

  RecordLagerAuftrag:=TExtRecordDefinition.Create('LagerAuftrag');
  with RecordLagerAuftrag do
  begin
    AddCardinal('ID',0,high(Cardinal));
    Addboolean('Kauf');
    AddCardinal('Item',0,high(Cardinal));
    AddInteger('Preis',0,high(Integer));
    AddInteger('Anzahl',0,high(Integer));
    AddCardinal('BasisID',0,high(Cardinal));
  end;

  RecordWaffenzelle:=TExtRecordDefinition.Create('Waffenzelle');
  with RecordWaffenzelle do
  begin
    Addboolean('Installiert');
    AddString('Name',50);
    AddCardinal('ID',0,high(Cardinal));
    AddInteger('MunIndex',-1,high(Integer));
    AddCardinal('Munition',0,high(Cardinal));
    AddInteger('WaffType',0,Integer(high(TWaffenType)));  
    AddCardinal('Strength',0,high(Cardinal));
    AddInteger('PixPerSec',0,high(Integer));
    AddInteger('Laden',0,high(Integer));
    Addboolean('SendMessage');
    Addboolean('Verfolgung');
    AddInteger('Reichweite',0,high(Integer));
    AddInteger('ReserveMunition',0,high(Integer));
  end;

  RecordEinrichtung:=TExtRecordDefinition.Create('Einrichtung');
  with RecordEinrichtung do
  begin
    AddString('Name',50);
    AddCardinal('ID',0,high(Cardinal));
    AddInteger('KaufPreis',0,high(Integer));
    AddInteger('SmallSchiff',0,high(Integer));
    AddInteger('SmallBelegt',0,high(Integer));
    AddInteger('LargeSchiff',0,high(Integer));
    Adddouble('LagerRaum',0,high(Integer),1);
    Adddouble('LagerBelegt',0,high(Integer),1);
    AddInteger('Quartiere',0,high(Integer));
    AddInteger('WohnBelegt',0,high(Integer));
    AddInteger('WerkRaum',0,high(Integer));
    AddInteger('WerkBelegt',0,high(Integer));
    AddInteger('Abwehr',0,high(Integer));
    AddInteger('LaborRaum',0,high(Integer));
    AddInteger('LaborBelegt',0,high(Integer));
    AddInteger('MineAlphatron',0,high(Integer));
    AddInteger('days',0,high(Integer));
    AddInteger('BauZeit',0,high(Integer));
    Addboolean('DeActive');
    AddInteger('Frequenz',0,high(Integer));
    AddCardinal('UFOID',0,high(Cardinal));
    AddString('Info',100000);
    AddString('BuildImageBase',500);
    AddString('BuildImageUnder',500);

    AddInteger('UnitWidth',1,4);
    AddInteger('UnitHeight',1,4);

    AddInteger('Reichweite',0,high(Integer));
    AddInteger('RoomTyp',0,Integer(high(TRoomTyp)));

    AddInteger('SensorWidth',0,high(Integer));
    AddInteger('ShieldStrength',0,high(Integer));
    AddInteger('AktShieldPoints',0,high(Integer));
    AddInteger('Treffsicherheit',0,100);
    AddInteger('maxReichweite',0,high(Integer));
    AddInteger('WaffenArt',Integer(low(TWaffenType)),Integer(high(TWaffenType)));

    AddInteger('BuildFloor',0,MaxFloors);

    AddInteger('HitPoints',0,high(Integer),50000);
    AddInteger('AktHitpoints',0,high(Integer),50000);

    AddInteger('AlphatronStorage',0,high(Integer),0);

    AddInteger('XPos',0,high(Integer));
    AddInteger('YPos',0,high(Integer));

    AddCardinal('RoomID',0,high(Cardinal));
    AddCardinal('BaseID',0,high(Cardinal));

    AddInteger('ActualValue',0,high(Integer));
    AddInteger('DaysToDecValue',0,high(Integer));
  end;

  RecordForscher:=TExtRecordDefinition.Create('Forscher');
  with RecordForscher do
  begin
    AddString('Name',50);
    AddDouble('Strength',0,100,10);
    AddInteger('BitNr',0,high(Integer));
    AddInteger('Days',0,high(Integer));
    AddCardinal('ID',0,high(Cardinal));
    Addboolean('Train');
    AddCardinal('BasisID',0,high(Cardinal));
    AddInteger('TrainTime',0,high(Integer));
    AddInteger('ForschTime',0,high(Integer));
  end;

  RecordEndedForschProject:=TExtRecordDefinition.Create('EndedForschProject');
  with RecordEndedForschProject do
  begin
    AddCardinal('ID',0,high(Cardinal));
    AddCardinal('ParentID',0,high(Cardinal));
    AddInteger('TypeID',0,Integer(high(TProjektType)));
    AddString('Name',50);
  end;

  RecordWatchMarktSettings:=TExtRecordDefinition.Create('WatchMarktSettings');
  with RecordWatchMarktSettings do
  begin
    AddCardinal('BasisID',0,high(Cardinal));
    AddBoolean('Active');
    AddInteger('minFaehigkeiten',0,high(Integer));
    AddInteger('WatchType',0,Integer(high(TWatchType)));
  end;

  RecordLagerAngebot:=TExtRecordDefinition.Create('LagerAngebot');
  with RecordLagerAngebot do
  begin
    AddCardinal('ID',0,high(Cardinal));
    AddInteger('Organisation',0,high(Integer));
    AddInteger('Anzahl',0,high(Integer));
    AddInteger('Preis',0,high(Integer));
    AddInteger('Rounds',0,high(Integer));
  end;

  RecordLand:=TExtRecordDefinition.Create('Land');
  with RecordLand do
  begin
    AddString('Name',50);
    AddCardinal('ID',0,high(Cardinal));
    AddInteger('Image',0,high(Integer));
    AddInteger('InfoIndex',0,high(Integer));
    AddInteger('Friendly',0,high(Integer));
    AddInteger('UFOHours',0,high(Integer));
    AddInteger('Einwohner',0,high(Integer));
    AddInteger('Budget',low(Integer),high(Integer));
    AddInteger('Zufriedenheit',low(Integer),high(Integer));
    AddInteger('SchErsatz',low(Integer),high(Integer));
    AddInteger('Towns',low(Integer),high(Integer));
  end;

  RecordTechniker:=TExtRecordDefinition.Create('Techniker');
  with RecordTechniker do
  begin
    AddCardinal('TechnikerID',0,high(Cardinal));
    AddString('Name',50);
    Adddouble('Strength',0,150,10);
    AddInteger('BitNr',0,high(Integer));
    AddInteger('Days',0,high(Integer));
    Addboolean('Ausbil');
    AddCardinal('ID',0,high(Cardinal));
    AddCardinal('BasisID',0,high(Cardinal));
    AddBoolean('AlphatronMining',false);
    AddBoolean('Repair',false);
    AddInteger('TrainTime',0,high(Integer));
    AddInteger('ProdTime',0,high(Integer));
  end;

  RecordProduktion:=TExtRecordDefinition.Create('Produktion');
  with RecordProduktion do
  begin
    AddCardinal('ID',0,high(Cardinal));
    AddCardinal('ItemID',0,high(Cardinal));
    AddInteger('Kost',0,high(Integer));
    AddInteger('Reserved',0,high(Integer));
    Adddouble('LagerV',0,1000000,4);
    AddInteger('Anzahl',0,high(Integer));
    AddInteger('Gesamt',0,high(Integer));
    Adddouble('Hour',0,10000000,10);
    Addboolean('Product');
    AddCardinal('BasisID',0,high(Cardinal));
  end;

  RecordRaumschiffModel:=TExtRecordDefinition.Create('RaumschiffModel');
  with RecordRaumschiffModel do
  begin
    AddString('Name',50);
    AddCardinal('ID',0,high(Cardinal));
    AddCardinal('KaufPreis',0,high(Cardinal));
    AddCardinal('VerKaufPreis',0,high(Cardinal));
    AddInteger('WaffenZellen',0,high(Integer));
    AddInteger('HitPoints',0,high(Integer));
    AddInteger('LagerPlatz',0,high(Integer));
    AddInteger('Soldaten',0,high(Integer));
    AddInteger('BauZeit',0,high(Integer));
    AddInteger('ExtendsSlots',0,high(Integer));
    AddInteger('SensorWeite',0,high(Integer));
    AddString('Info',1000000);
  end;

  RecordRaumschiffShield:=TExtRecordDefinition.Create('RaumschiffShield');
  with RecordRaumschiffShield do
  begin
    AddBoolean('Installiert');
    AddString('Name',50);
    AddCardinal('ID',0,high(Cardinal));
    AddInteger('Points',0,high(Integer));
    AddInteger('Abwehr',0,high(Integer));
    AddInteger('Laden',low(Integer),high(Integer));
  end;

  RecordRaumschiffMotor:=TExtRecordDefinition.Create('RaumschiffMotor');
  with RecordRaumschiffMotor do
  begin
    AddBoolean('Installiert');
    AddString('Name',50);
    AddCardinal('ID',0,high(Cardinal));
    AddInteger('MaxLiter',low(Integer),high(Integer));
    Adddouble('Liter',0,10000000,10);
    AddInteger('Verbrauch',low(Integer),high(Integer));
    Addboolean('Duesen');
    AddInteger('PpS',low(Integer),high(Integer));
  end;

  Temp:=TExtRecordDefinition.Create('ExtensionProperty');
  Temp.ParentDescription:=true;
  Temp.AddInteger('Prop',0,high(Integer));
  Temp.AddInteger('Value',low(Integer),high(Integer));

  RecordRaumschiffExtension:=TExtRecordDefinition.Create('RaumschiffExtension');
  with RecordRaumschiffExtension do
  begin
    AddBoolean('Installiert');
    AddCardinal('ID',0,high(Cardinal));
    AddRecordList('Properties',Temp);
  end;

  RecordBackPackItem:=TExtRecordDefinition.Create('BackPackItem');
  with RecordBackPackItem do
  begin
    AddBoolean('IsSet');
    AddCardinal('ID',0,high(Cardinal));
    AddInteger('Schuesse',-1,high(Integer));
    AddDouble('Gewicht',0,10000,10);
  end;

finalization
  RecordLagerListe.Free;
  RecordForschung.Free;
  RecordSoldatInfo.Free;
  RecordSoldatWaffe.Free;
  RecordSoldatPanzerung.Free;
  RecordSoldatGuertel.Free;
  RecordPatent.Free;
  RecordMissionUFOs.Free;
  RecordMissionSchiff.Free;
  RecordMissionEinsatz.Free;
  RecordMissionEvent.Free;
  RecordLagerAuftrag.Free;
  RecordLagerAngebot.Free;
  RecordWaffenzelle.Free;
  RecordEinrichtung.Free;
  RecordForscher.Free;
  RecordEndedForschProject.Free;
  RecordWatchMarktSettings.Free;
  RecordLand.Free;
  RecordTechniker.Free;
  RecordProduktion.Free;
  RecordRaumschiffModel.Free;
  RecordRaumschiffShield.Free;
  RecordRaumschiffMotor.Free;
  RecordRaumschiffExtension.Free;
  RecordBackPackItem.Free;
end.
