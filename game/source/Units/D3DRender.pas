{********************************************************************************
*										*
* Copyright (C) 2004  X-Force Team - jim_raynor@web.de				*
*										*
* This file is part of X-Force: Fight For Destiny (X-Force).			*
*										*
* X-Force is free software; you can redistribute it and/or modify		*
* it under the terms of the GNU General Public License as published by		*
* the Free Software Foundation; either version 2 of the License, or		*
* (at your option) any later version.						*
*										*
* X-Force is distributed in the hope that it will be useful,			*
* but WITHOUT ANY WARRANTY; without even the implied warranty of		*
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the			*
* GNU General Public License for more details.					*
*										*
* You should have received a copy of the GNU General Public License		*
* along with X-Force; if not, write to the Free Software			*
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA	*
*										*
*********************************************************************************
*										*
* 3D-Render Utilities								*
*										*
********************************************************************************}

{$I ../Settings.inc}

unit D3DRender;

interface

uses DXDraws, Classes, Windows, Direct3D, Graphics, D3DUtils, Blending, math, Direct3DRM;

const
  D3DFVF_DIFFUSEVERTEX = D3DFVF_XYZRHW or D3DFVF_DIFFUSE;

procedure D3DRenderAlpha(DXDraw: TCustomDXDraw; X,Y: Integer; SrcRect: TRect;Alpha: Byte);
procedure D3DBlendRectangle(DXDraw: TCustomDXDraw; Rect: TRect;Color: Cardinal; Alpha: Byte);
procedure D3DEnabledAlpha(DXDraw: TCustomDXDraw);
procedure D3DDisabledAlpha(DXDraw: TCustomDXDraw);

procedure CheckDirectXResult(Result: HResult);

var
  Enabled3DAlphaBlend: Boolean = true;

implementation

uses SysUtils, DirectDraw;

type
  T4Vertex         = Array[0..3] of TD3DTLVERTEX;
  TDiffuseVertices = Array[0..3] of packed record
                       sx   : TD3DValue;             (* Homogeneous coordinates *)
                       sy   : TD3DValue;
                       sz   : TD3DValue;
                       rhw  : TD3DValue;
                       Color: TD3DColor;
                     end;

var
  f4Vertex         : T4Vertex;
  fDiffuseVertices : TDiffuseVertices;

procedure D3DEnabledAlpha(DXDraw: TCustomDXDraw);
begin
  if not Enabled3DAlphaBlend then
    exit;
    
  DXDraw.D3DDevice7.SetRenderState(D3DRENDERSTATE_ALPHABLENDENABLE,CARDINAL(TRUE));
  DXDraw.D3DDevice7.SetRenderState(D3DRENDERSTATE_SRCBLEND,CARDINAL(D3DBLEND_SRCALPHA));
  DXDraw.D3DDevice7.SetRenderState(D3DRENDERSTATE_DESTBLEND,CARDINAL(D3DBLEND_INVSRCALPHA));

  DXDraw.D3DDevice7.SetTextureStageState(0,D3DTSS_ALPHAARG1,D3DTA_DIFFUSE);
end;

procedure D3DDisabledAlpha(DXDraw: TCustomDXDraw);
begin
  if not Enabled3DAlphaBlend then
    exit;
    
  DXDraw.D3DDevice7.SetRenderState(D3DRENDERSTATE_ALPHABLENDENABLE,CARDINAL(FALSE));
end;

procedure D3DRenderAlpha(DXDraw: TCustomDXDraw; X,Y: Integer; SrcRect: TRect;Alpha: Byte);
begin
  Alpha:=max(0,min(255,round(AlphaBlendFaktor*Alpha)));
  D3DEnabledAlpha(DXDraw);

  f4Vertex[0].color:=$FFFFFF or (Alpha shl 24);
  f4Vertex[0].sx:=X;
  f4Vertex[0].sy:=Y;
  f4Vertex[0].tu:=SrcRect.Left/255;
  f4Vertex[0].tv:=SrcRect.Top/255;

  f4Vertex[1].color:=$FFFFFF or (Alpha shl 24);
  f4Vertex[1].sx:=X+abs(SrcRect.Right-SrcRect.Left);
  f4Vertex[1].sy:=Y;
  f4Vertex[1].tu:=SrcRect.Right/255;
  f4Vertex[1].tv:=SrcRect.Top/255;

  f4Vertex[2].color:=$FFFFFF or (Alpha shl 24);
  f4Vertex[2].sx:=X;
  f4Vertex[2].sy:=Y+abs(SrcRect.Bottom-SrcRect.Top);
  f4Vertex[2].tu:=SrcRect.Left/255;
  f4Vertex[2].tv:=SrcRect.Bottom/255;

  f4Vertex[3].color:=$FFFFFF or (Alpha shl 24);
  f4Vertex[3].sx:=X+abs(SrcRect.Right-SrcRect.Left);
  f4Vertex[3].sy:=Y+abs(SrcRect.Bottom-SrcRect.Top);
  f4Vertex[3].tu:=SrcRect.Right/255;
  f4Vertex[3].tv:=SrcRect.Bottom/255;

  DXDraw.D3DDevice7.DrawPrimitive(D3DPT_TRIANGLESTRIP,D3DFVF_TLVERTEX,f4Vertex,4,0);

  D3DDisabledAlpha(DXDraw);
end;

procedure D3DBlendRectangle(DXDraw: TCustomDXDraw; Rect: TRect;Color: Cardinal; Alpha: Byte);
var
  Text: IDirectDrawSurface7;
begin
  Alpha:=max(0,min(255,round(AlphaBlendFaktor*Alpha)));
  D3DEnabledAlpha(DXDraw);

  DXDraw.D3DDevice7.GetTexture(0,Text);
  DXDraw.D3DDevice7.SetTexture(0,nil);
  fDiffuseVertices[0].color:=Color or (Alpha shl 24);
  fDiffuseVertices[0].sx:=Rect.Left;
  fDiffuseVertices[0].sy:=Rect.Top;

  fDiffuseVertices[1].color:=Color or (Alpha shl 24);
  fDiffuseVertices[1].sx:=Rect.Right;
  fDiffuseVertices[1].sy:=Rect.Top;

  fDiffuseVertices[2].color:=Color or (Alpha shl 24);
  fDiffuseVertices[2].sx:=Rect.Left;
  fDiffuseVertices[2].sy:=Rect.Bottom;

  fDiffuseVertices[3].color:=Color or (Alpha shl 24);
  fDiffuseVertices[3].sx:=Rect.Right;
  fDiffuseVertices[3].sy:=Rect.Bottom;

  DXDraw.D3DDevice7.DrawPrimitive(D3DPT_TRIANGLESTRIP,D3DFVF_DIFFUSEVERTEX,fDiffuseVertices,4,0);

  DXDraw.D3DDevice7.SetTexture(0,Text);
  D3DDisabledAlpha(DXDraw);
end;

procedure CheckDirectXResult(Result: HResult);
var
  Mess: String;
begin
  if Result<>DD_OK then
  begin
    Mess:=D3DErrorString(Result);
    if Mess='Unrecognized Error' then
      Mess:=DDErrorString(Result);
    if Mess='Unrecognized Error' then
      Mess:=D3DRMErrorString(Result);

    raise EDXDrawError.Create(Mess);
  end;
end;

initialization

  fDiffuseVertices[0].rhw:=1;
  fDiffuseVertices[1].rhw:=1;
  fDiffuseVertices[2].rhw:=1;
  fDiffuseVertices[3].rhw:=1;

  f4Vertex[0].rhw:=1;
  f4Vertex[1].rhw:=1;
  f4Vertex[2].rhw:=1;
  f4Vertex[3].rhw:=1;
  f4Vertex[0].Specular:= $FFFFFF;
  f4Vertex[1].Specular:= $FFFFFF;
  f4Vertex[2].Specular:= $FFFFFF;
  f4Vertex[3].Specular:= $FFFFFF;

end.
