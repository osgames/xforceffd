{********************************************************************************
*										*
* Copyright (C) 2004  X-Force Team - jim_raynor@web.de				*
*										*
* This file is part of X-Force: Fight For Destiny (X-Force).			*
*										*
* X-Force is free software; you can redistribute it and/or modify		*
* it under the terms of the GNU General Public License as published by		*
* the Free Software Foundation; either version 2 of the License, or		*
* (at your option) any later version.						*
*										*
* X-Force is distributed in the hope that it will be useful,			*
* but WITHOUT ANY WARRANTY; without even the implied warranty of		*
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the			*
* GNU General Public License for more details.					*
*										*
* You should have received a copy of the GNU General Public License		*
* along with X-Force; if not, write to the Free Software			*
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA	*
*										*
*********************************************************************************
*										*
* Definiert Extrecords f�r alles was erforscht werden kann und stellt		*
* gleichzeit Funktionen zum Konvertieren nach TForschProject zur Verf�gung	*
*										*
********************************************************************************}

{$I ../Settings.inc}

unit ProjektRecords;

interface

uses
  ExtRecord, XForce_types, Classes, typInfo, Sysutils, Windows, math;

type
  TParentArray       = Array of Cardinal;

var
  RecordTechnologie     : TExtRecordDefinition;

  RecordRaumschiff      : TExtRecordDefinition;
  RecordBasis           : TExtRecordDefinition;

  RecordExtension       : TExtRecordDefinition;
  RecordGranate         : TExtRecordDefinition;
  RecordGuertel         : TExtRecordDefinition;
  RecordMine            : TExtRecordDefinition;
  RecordPanzerung       : TExtRecordDefinition;
  RecordSchild          : TExtRecordDefinition;
  RecordSensor          : TExtRecordDefinition;
  RecordTriebwerk       : TExtRecordDefinition;
  RecordWaffe           : TExtRecordDefinition;
  RecordMunition        : TExtRecordDefinition;
  RecordRWaffe          : TExtRecordDefinition;
  RecordRMunition       : TExtRecordDefinition;


function GenerateFromProjekt(const Projekt: TForschProject): TExtRecord;
function RecordToProject(Rec: TExtRecord): TForschProject;

function GetDefinitionFromType(TypeID: TProjektType): TExtRecordDefinition;

procedure SaveProjectRecord(ExtRecord: TExtRecord; Stream: TStream);
function ReadProjectRecord(Stream: TStream): TExtRecord;

procedure GetParentArray(Rec: TExtRecord; var Parents: TParentArray);
procedure SetParentArray(Rec: TExtRecord; var Parents: TParentArray);

implementation

uses
  KD4Utils;

var
  ProjectRecordList: TList;

function GenerateDefaultProjektRecord(Name: String): TExtRecordDefinition;
begin
  result:=TExtRecordDefinition.Create(Name);
  result.AddCardinal('ID',0,high(Cardinal),random(high(Cardinal)),'Name=ID|Category=Allgemein|NoEdit=true');
  result.AddInteger('TypeID',0,Integer(High(TProjektType)),0,'Visible=false');
  result.AddString('Name',100000,'Unbenannt','Category=Allgemein|Custom=ItemName');
  result.AddString('Info',1000000,'','Name=UFO-P�die|Custom=InfoText|Category=Beschreibungen');
  result.AddString('ResearchInfo',10000000,'','Name=Forschung|Custom=InfoText|Category=Beschreibungen');
  result.AddBoolean('Start',true,'Name=Startausr�stung|Visible=false');
  result.AddInteger('Ver',0,1080,0,'Category=Allgemein|Name=Verf�gbar nach|Einheit=Tage');
  result.AddInteger('Stunden',0,100000,500,'Category=Allgemein|Einheit=Stunden|Name=Forschungszeit|Start=false');
  result.AddInteger('Anzahl',0,300,0,'Category=Allgemein|Name=Anzahl|Start=true');
  result.AddInteger('ParentCount',0,1000000,0,'Visible=false|NoEdit=true');
  result.AddBinary('Parents','Visible=false');

  ProjectRecordList.Add(result);
end;

function GenerateDefaultItemRecord(Name: String): TExtRecordDefinition;
begin
  result:=GenerateDefaultProjektRecord(Name);
  result.AddInteger('KaufPreis',100,10000000,100,'Name=Kaufpreis|Einheit=Credits|Category=Allgemein');
  result.AddInteger('VerKaufPreis',0,10000000,100,'Name=Verkaufspreis|Einheit=Credits|Category=Allgemein');
  result.AddInteger('Alphatron',0,2000000,500,'Name=Herstellungskosten|Einheit=Alphatron|Category=Allgemein');
  result.AddInteger('ProdTime',0,100000,500,'Name=Herstellungszeit|Einheit=Stunden|Category=Allgemein');
  result.AddInteger('ImageIndex',-1,5000,-1,'Name=Symbol|Custom=Symbol|Category=Allgemein|NoCheck=true');
  result.AddInteger('Land',-1,25,-1,'Name=Herstellerland|Custom=Land|Category=Allgemein|Start=true');
  result.AddString('UFOPaedieImage',100,'','Name=UFOP�die Bild|Custom=UFOPaedieImage|Category=Allgemein');
  result.AddBoolean('Herstellbar',true,'Name=Herstellbar|Custom=Land|Category=Allgemein|Start=true|Land=true');
  result.AddBoolean('AlienItem',false,'Visible=false');
  result.AddInteger('ResearchItems',1,500,1,'Name=f�r Forschung ben�tigt|Category=Alienausr�stung|Start=false|Alien=true');
  result.AddInteger('AlienChance',0,100,25,'Name=H�ufigkeit|Category=Alienausr�stung|Start=false|Alien=true|Einheit=%');
end;

function GenerateDefaultSoldatItemRecord(Name: String): TExtRecordDefinition;
begin
  result:=GenerateDefaultItemRecord(Name);
  result.AddDouble('LagerV',0.1,15,1,0.5,'Name=Lagerverbrauch|Category='+Name);
  result.AddDouble('Gewicht',0.1,100,1,1,'Name=Gewicht|Einheit=kg|Category='+Name);
  result.AddInteger('NeededIQ',0,200,25,'Name=ben�tigte Intelligenz|Category=Allgemein|Custom=IQ');
end;

procedure GenerateTechnologieRecordDesc;
begin
  RecordTechnologie:=GenerateDefaultProjektRecord('Technologie');
  RecordTechnologie.AddString('UFOPaedieImage',100,'','Name=UFOP�die Bild|Custom=UFOPaedieImage|Category=Allgemein');
  RecordTechnologie.AddInteger('Patengebuehr',0,1000000,0,'Name=Patentgeb�hr|Category=Patent');
  RecordTechnologie.AddInteger('PatenZeit',0,100,0,'Name=Patentzeitraum|Einheit=Wochen|Category=Patent');
  RecordTechnologie.AddInteger('Patenanstieg',0,100,0,'Name=Anstieg pro Woche|Einheit=%|Category=Patent');
  RecordTechnologie.AddInteger('PatenSatt',0,100,0,'Name=S�ttigung erreicht nach|Einheit=Wochen|Category=Patent');
end;

procedure GenerateRaumschiffRecordDesc;
begin
  RecordRaumschiff:=GenerateDefaultProjektRecord('Raumschiff');
  RecordRaumschiff.AddInteger('KaufPreis',5000,50000000,100,'Name=Kaufpreis|Einheit=Credits|Category=Allgemein');
  RecordRaumschiff.AddInteger('VerKaufPreis',0,50000000,100,'Name=Verkaufspreis|Einheit=Credits|Category=Allgemein');
  RecordRaumschiff.AddInteger('HitPoints',100,10000,250,'Name=Hitpoints|Category=Eigenschaften');
  RecordRaumschiff.AddInteger('WaffenZellen',0,3,0,'Name=Waffenzellen|Category=Eigenschaften');
  RecordRaumschiff.AddInteger('LagerPlatz',0,300,0,'Name=Lagerraum|Category=Eigenschaften');
  RecordRaumschiff.AddInteger('Soldaten',0,16,0,'Name=Soldaten|Category=Eigenschaften');
  RecordRaumschiff.AddInteger('BauZeit',3,40,20,'Name=Bauzeit|Category=Allgemein|Einheit=Tage');
  RecordRaumschiff.AddInteger('ExtendSlots',0,3,0,'Name=Erweiterungsslots|Category=Eigenschaften');
  RecordRaumschiff.AddInteger('SensorWeite',100,7500,500,'Name=Sensorreichweite|Einheit=km|Category=Eigenschaften');
end;

procedure GenerateExtensionRecordDesc;
begin
  RecordExtension:=GenerateDefaultItemRecord('Erweiterung');
  RecordExtension.AddDouble('LagerV',0,5,1,0.5,'Name=Lagerverbrauch|Category=Erweiterung');
  RecordExtension.AddInteger('Ext[epSensor]',0,15000,0,'Name=Sensorreichweite|Einheit=km|Category=Sensor');
  RecordExtension.AddInteger('Ext[epShieldHitpoints]',0,5000,0,'Name=Hitpoints|Category=Schutzschild');
  RecordExtension.AddInteger('Ext[epShieldDefence]',0,100,0,'Name=Abwehr|Einheit=%|Category=Schutzschild');
  RecordExtension.AddInteger('Ext[epShieldReloadTime]',0,1500,0,'Name=Aufladezeit|Einheit=ms|Category=Schutzschild');

  // Fr�her war Ext[epSensor] = Sensor
  RecordExtension.AddLoadLink('Sensor','Ext[epSensor]');
end;

procedure GenerateGranatenRecordDesc;
begin
  RecordGranate:=GenerateDefaultSoldatItemRecord('Granate');
  RecordGranate.AddInteger('Strength',1,300,50,'Name=Explosionsst�rke|Category=Granate');
  RecordGranate.AddInteger('Reichweite',3,20,5,'Name=Explosionsradius|Einheit=Felder|Category=Granate');
end;

procedure GenerateGuertelRecordDesc;
begin
  RecordGuertel:=GenerateDefaultSoldatItemRecord('G�rtel');
  RecordGuertel.AddInteger('Strength',1,12,2,'Name=Munitionsslots|Category=G�rtel');
end;

procedure GenerateMineRecordDesc;
begin
  RecordMine:=GenerateDefaultSoldatItemRecord('Mine');
  RecordMine.AddInteger('Strength',1,300,50,'Name=Explosionsst�rke|Category=Granate');
  RecordMine.AddInteger('Reichweite',2,15,5,'Name=Explosionsradius|Einheit=Felder|Category=Granate');
end;

procedure GeneratePanzerungRecordDesc;
begin
  RecordPanzerung:=GenerateDefaultSoldatItemRecord('Panzerung');
  RecordPanzerung.AddInteger('Panzerung',1,80,10,'Name=Panzerung|Category=Panzerung');
end;

procedure GenerateSchildRecordDesc;
begin
  RecordSchild:=GenerateDefaultItemRecord('Schutzschild');
  RecordSchild.AddDouble('LagerV',0,15,1,0.5,'Name=Lagerverbrauch|Category=Schutzschild');
  RecordSchild.AddInteger('Strength',1,5000,500,'Name=Hitpoints|Category=Schutzschild');
  RecordSchild.AddInteger('Panzerung',1,100,10,'Name=Abwehr|Einheit=%|Category=Schutzschild');
  RecordSchild.AddInteger('Laden',50,1500,250,'Name=Aufladezeit|Einheit=ms|Category=Schutzschild');
end;

procedure GenerateSensorRecordDesc;
begin
  RecordSensor:=GenerateDefaultSoldatItemRecord('Sensor');
  RecordSensor.AddInteger('Strength',0,50,10,'Name=Sensorreichweite|Category=Sensor|Einheit=Felder');
end;

procedure GenerateTriebwerkRecordDesc;
begin
  RecordTriebwerk:=GenerateDefaultItemRecord('Triebwerk');
  RecordTriebwerk.AddDouble('LagerV',0,15,1,0.5,'Name=Lagerverbrauch|Category=Triebwerk');
  RecordTriebwerk.AddInteger('Strength',1,150,6,'Name=Verbrauch|Einheit=Liter/1000km|Category=Triebwerk');
  RecordTriebwerk.AddInteger('Munition',100,30000,500,'Name=Tankvolumen|Category=Triebwerk');
  RecordTriebwerk.AddInteger('PixPerSec',1,15,6,'Name=Geschwindigkeit|Category=Triebwerk');
  RecordTriebwerk.AddBoolean('Verfolgung',false,'Name=Man�vrierd�sen|Category=Triebwerk');
end;

procedure GenerateWaffeRecordDesc;
begin
  RecordWaffe:=GenerateDefaultSoldatItemRecord('Waffe');
  RecordWaffe.AddBoolean('Einhand',true,'Name=Einhandwaffe|Category=Waffe');
  RecordWaffe.AddInteger('TimeUnits',1,20,5,'Name=Zeiteinheiten|Category=Waffe|Einheit=pro Schuss');
  RecordWaffe.AddInteger('WaffType',0,Integer(high(TWaffenType)),0,'Name=Waffenart|Category=Angriff|Custom=WaffenType');
  RecordWaffe.AddInteger('Strength',1,1000,0,'Name=Angriffsst�rke|Category=Angriff|Strength=true|NoCheck=true');
  RecordWaffe.AddInteger('Power',1,100,0,'Name=Durchschlagskraft|Category=Angriff|Strength=true|NoCheck=true');
  RecordWaffe.AddInteger('Munition',1,3000,0,'Name=Munition|Category=Angriff|Munition=true|NoCheck=true');

  RecordWaffe.AddString('ShootingSound',100,'default\Sound:ShootWeapon','Name=Abschuss|Category=Sounds|Custom=Sound');

  RecordWaffe.AddBoolean('SchussType[stAuto]',true,'Name=Automatischer Schuss|Category=Schussarten|Fernkampf=true');
  RecordWaffe.AddBoolean('SchussType[stSpontan]',true,'Name=Spontaner Schuss|Category=Schussarten|Fernkampf=true');
  RecordWaffe.AddBoolean('SchussType[stGezielt]',true,'Name=Gezielter Schuss|Category=Schussarten|Fernkampf=true');
  RecordWaffe.AddBoolean('SchussType[stSchlag]',false,'Name=Schlag|Category=Schussarten|Nahkampf=true');
  RecordWaffe.AddBoolean('SchussType[stSchwingen]',false,'Name=Schwingen|Category=Schussarten|Nahkampf=true');
  RecordWaffe.AddBoolean('SchussType[stStossen]',false,'Name=Stossen|Category=Schussarten|Nahkampf=true');

  RecordWaffe.AddLoadLink('Auto','SchussType[stAuto]');
  RecordWaffe.AddLoadLink('Spontan','SchussType[stSpontan]');
  RecordWaffe.AddLoadLink('Gezielt','SchussType[stGezielt]');
  RecordWaffe.AddLoadLink('Schlag','SchussType[stSchlag]');

  RecordWaffe.AddInteger('EffRange',1,30,8,'Name=Effektive Reichweite|Einheit=Felder|Category=Angriff|Fernkampf=true');
  RecordWaffe.AddInteger('Frequenz',25,10000,250,'Name=Schussfrequenz|Einheit=ms|Category=Angriff|Custom=Divisor|Divisor=25|Visible=false');
end;

procedure GenerateMunitionRecordDesc;
begin
  RecordMunition:=GenerateDefaultSoldatItemRecord('Munition');
  RecordMunition.AddCardinal('MunFor',0,high(Cardinal),0,'Name=Munition f�r|Category=geh�rt zu|Custom=Waffe');
  RecordMunition.AddInteger('WaffType',0,Integer(high(TWaffenType)),0,'Name=Waffenart|Category=geh�rt zu|Custom=WaffenType|NoEdit=true');
  RecordMunition.AddInteger('Strength',1,1000,50,'Name=Angriffsst�rke|Category=Angriff|Laser=true');
  RecordMunition.AddInteger('Power',1,100,50,'Name=Durchschlagskraft|Category=Angriff|Laser=true');
  RecordMunition.AddInteger('Munition',1,3000,50,'Name=Munition|Category=Angriff|Laser=true');
  RecordMunition.AddInteger('ExplosionSize',1,15,4,'Name=Explosionsreichweite|Einheit=Felder|Category=Munition|NoCheck=true|Raketen=true');
end;

procedure GenerateRWaffeRecordDesc;
begin
  RecordRWaffe:=GenerateDefaultItemRecord('Raumschiff-Waffe');
  RecordRWaffe.AddDouble('LagerV',0,15,1,0.5,'Name=Lagerverbrauch|Category=Flugfahrzeug-Waffe');
  RecordRWaffe.AddInteger('WaffType',0,Integer(high(TWaffenType)),0,'Name=Waffenart|Category=Angriff|Custom=WaffenType');
  RecordRWaffe.AddInteger('Strength',1,1000,50,'Name=Angriffsst�rke|Category=Angriff');
  RecordRWaffe.AddInteger('Munition',1,5000,50,'Name=Munition|Category=Angriff|Laser=true|NoCheck=true');
  RecordRWaffe.AddInteger('PixPerSec',1,30,10,'Name=Projektilgeschwindigkeit|Category=Angriff');
  RecordRWaffe.AddInteger('Laden',100,10000,50,'Name=Nachladezeit|Einheit=ms|Category=Angriff');
  RecordRWaffe.AddBoolean('Verfolgung',false,'Name=Zielsuche|Category=Angriff');
  RecordRWaffe.AddInteger('Reichweite',200,10000,1000,'Name=Reichweite|Einheit=m|Category=Angriff');
end;

procedure GenerateRMunitionRecordDesc;
begin
  RecordRMunition:=GenerateDefaultItemRecord('Raumschiff-Munition');
  RecordRMunition.AddDouble('LagerV',0,15,1,0.5,'Name=Lagerverbrauch|Category=Flugfahrzeug-Munition');
  RecordRMunition.AddCardinal('MunFor',0,high(Cardinal),0,'Name=Munition f�r|Category=geh�rt zu|Custom=Waffe');
  RecordRMunition.AddInteger('WaffType',0,Integer(high(TWaffenType)),0,'Name=Waffenart|Category=geh�rt zu|Custom=WaffenType|NoEdit=true');
  RecordRMunition.AddInteger('Strength',1,1000,50,'Name=Angriffsst�rke|Category=Angriff|NoEdit=true');
  RecordRMunition.AddInteger('Munition',1,500,50,'Name=Munition|Category=Angriff');
end;

procedure GenerateBasisRecordDesc;
begin
  // CustomParameter GroundFloor = Oberirdisch
  // CustomParameter Basement    = Unterirdisch
  RecordBasis:=GenerateDefaultProjektRecord('Einrichtung');

  RecordBasis.AddInteger('Hitpoints',1,1000000,50000,'Category=Basisbau|Name=Hitpoints');

  RecordBasis.AddInteger('RoomTyp',0,5,0,'Name=Einrichtungstyp|Category=Basisbau|Custom=RoomTyp');
  RecordBasis.AddString('GrafikBasement',200,'','Category=Basisbau|Custom=BasisBauImage|Name=Grafik Obergeschoss|Basement=true');
  RecordBasis.AddString('GrafikUnderground',200,'','Category=Basisbau|Custom=BasisBauImage|Name=Grafik Untergeschoss|Underground=true');

  RecordBasis.AddInteger('KaufPreis',10000,20000000,100000,'Name=Kaufpreis|Category=Allgemein|Einheit=Credits');
  RecordBasis.AddInteger('BauZeit',3,100,20,'Name=Bauzeit|Category=Allgemein|Einheit=Tage');

  RecordBasis.AddInteger('LagerRaum',0,150,0,'Name=Lagerraum|Category=Lager');
  RecordBasis.AddInteger('AlphatronStorage',0,1000000,0,'Name=Alphatronlagerplatz|Category=Lager|Einheit=Einheiten');

  RecordBasis.AddInteger('SensorWidth',0,20000,0,'Name=Sensorreichweite|Einheit=km|Category=Sensor|RoomTyp2=true');

  RecordBasis.AddInteger('Quartiere',0,150,0,'Name=Quartiere|Category=Quartiere|RoomTyp4=true');

  RecordBasis.AddInteger('WerkRaum',0,150,0,'Name=Werkraum|Category=Basiseinrichtung|RoomTyp0=true');
  RecordBasis.AddInteger('LaborRaum',0,150,0,'Name=Laborraum|Category=Basiseinrichtung|RoomTyp0=true');

  RecordBasis.AddInteger('ShieldStrength',0,150000,0,'Name=Schildpunkte|Category=Schild|RoomTyp3=true');

  RecordBasis.AddInteger('SmallSchiff',0,5,0,'Name=Hangarpl�tze|Category=Hangar|RoomTyp5=true');

  RecordBasis.AddInteger('Abwehr',1,1500,0,'Name=Abwehrst�rke|Category=Abwehreinrichtung|GroundFloor=true|RoomTyp1=true|Abwehr=true|NoCheck=true');
  RecordBasis.AddInteger('Treffsicherheit',1,100,25,'Name=Treffsicherheit|Category=Abwehreinrichtung|Einheit=%|RoomTyp1=true|Abwehr=true|NoCheck=true');
  RecordBasis.AddInteger('minReichweite',0,5000,500,'Name=min. Abwehrreichweite|Category=Abwehreinrichtung|Einheit=km|RoomTyp1=true|Abwehr=true|NoCheck=true');
  RecordBasis.AddInteger('maxReichweite',1,5000,500,'Name=max. Abwehrreichweite|Category=Abwehreinrichtung|Einheit=km|RoomTyp1=true|Abwehr=true|NoCheck=true');
  RecordBasis.AddInteger('PixPerSec',1,20,10,'Name=Schussfrequenz|Category=Abwehreinrichtung|Einheit=Schuss/Minute|RoomTyp1=true|Abwehr=true|NoCheck=true');
  RecordBasis.AddInteger('WaffenArt',Integer(low(TWaffenType)),Integer(high(TWaffenType)),0,'Name=Waffenart|Category=Abwehreinrichtung|Custom=WaffenType');

  RecordBasis.AddInteger('UnitWidth',0,4,1,'Visible=false');
  RecordBasis.AddInteger('UnitHeight',0,4,1,'Visible=false');

end;

function GenerateFromProjekt(const Projekt: TForschProject): TExtRecord;
var
  Dummy          : Integer;
  DummySchussType: TSchussType;
  Name           : String;

  function SetDefaultSettings(Definition: TExtRecordDefinition): TExtRecord;
  var
    Parents: TParentArray;
  begin
    result:=TExtRecord.Create(Definition);
    with result do
    begin
      SetCardinal('ID',Projekt.ID);
      SetInteger('TypeID',Integer(Projekt.TypeID));
      SetString('Name',Projekt.Name);
      SetString('Info',Projekt.Info);
      SetString('ResearchInfo',Projekt.ResearchInfo);
      SetBoolean('Start',Projekt.Start);
      SetInteger('Ver',Projekt.Ver);
      SetInteger('Stunden',Projekt.Stunden);
      SetInteger('Anzahl',Projekt.Anzahl);
    end;
    Parents:=TParentArray(Projekt.Parents);
    SetParentArray(result,Parents);
  end;

  function SetItemSettings(Definition: TExtRecordDefinition): TExtRecord;
  begin
    result:=SetDefaultSettings(Definition);
    with result do
    begin
      SetInteger('KaufPreis',Projekt.KaufPreis);
      SetInteger('VerKaufPreis',Projekt.VerKaufPreis);
      SetInteger('ImageIndex',Projekt.ImageIndex);
      SetInteger('Land',Projekt.Land);
      SetBoolean('Herstellbar',Projekt.Herstellbar);

      if ValueExists('LagerV') then
        SetDouble('LagerV',Projekt.LagerV);

      if ValueExists('Gewicht') then
        SetDouble('Gewicht',Projekt.Gewicht);

      if ValueExists('Strength') then
        SetInteger('Strength',Projekt.Strength);

      if ValueExists('NeededIQ') then
        SetInteger('NeededIQ',Projekt.NeedIQ);

      SetString('UFOPaedieImage',Projekt.UFOPaedieImage);

      SetBoolean('AlienItem',Projekt.AlienItem);
      SetInteger('ResearchItems',Projekt.ResearchCount);
      SetInteger('Alphatron',Projekt.ManuAlphatron);
      SetInteger('ProdTime',Projekt.ProdTime);
      SetInteger('AlienChance',Projekt.AlienChance);
    end;
  end;

begin
  case Projekt.TypeID of
    ptNone:
    begin
      result:=SetDefaultSettings(RecordTechnologie);
      with Result do
      begin
        SetInteger('Patengebuehr',Projekt.Patentgebuehr);
        SetInteger('PatenZeit',Projekt.PatentZeit);
        SetInteger('Patenanstieg',Projekt.Patentanstieg);
        SetInteger('PatenSatt',Projekt.PatentSatt);
        SetString('UFOPaedieImage',Projekt.UFOPaedieImage);
      end;
    end;
    ptEinrichtung:
    begin
      result:=SetDefaultSettings(RecordBasis);
      with result do
      begin
        SetInteger('KaufPreis',Projekt.KaufPreis);
        SetInteger('BauZeit',Projekt.BauZeit);

        SetInteger('RoomTyp',Integer(Projekt.RoomTyp));

        SetInteger('LagerRaum',Projekt.LagerRaum);
        SetInteger('AlphatronStorage',Projekt.AlphatronStorage);

        SetInteger('WerkRaum',Projekt.WerkRaum);
        SetInteger('Quartiere',Projekt.Quartiere);
        SetInteger('LaborRaum',Projekt.LaborRaum);
        SetInteger('Abwehr',Projekt.Abwehr);
        SetInteger('SmallSchiff',Projekt.SmallSchiff);
        SetInteger('minReichweite',Projekt.Reichweite);
        SetInteger('PixPerSec',Projekt.PixPerSec);

        SetString('GrafikBasement',Projekt.BuildImageBase);
        SetString('GrafikUnderground',Projekt.BuildImageUnder);

        SetInteger('UnitWidth',Projekt.UnitWidth);
        SetInteger('UnitHeight',Projekt.UnitHeight);
        SetInteger('SensorWidth',Projekt.SensorWidth);
        SetInteger('ShieldStrength',Projekt.ShieldStrength);
        SetInteger('Treffsicherheit',Projekt.Treffsicherheit);
        SetInteger('maxReichweite',Projekt.maxReichweite);
        SetInteger('WaffenArt',Integer(Projekt.WaffType));
        SetInteger('Hitpoints',Projekt.RoomHitPoints);
      end;
    end;
    ptRaumSchiff:
    begin
      result:=SetDefaultSettings(RecordRaumschiff);
      with result do
      begin
        SetInteger('KaufPreis',Projekt.KaufPreis);
        SetInteger('VerKaufPreis',Projekt.VerKaufPreis);
        SetInteger('HitPoints',Projekt.HitPoints);
        SetInteger('WaffenZellen',Projekt.WaffenZellen);
        SetInteger('LagerPlatz',Projekt.LagerPlatz);
        SetInteger('Soldaten',Projekt.Soldaten);
        SetInteger('BauZeit',Projekt.BauZeit);
        SetInteger('ExtendSlots',Projekt.ExtendSlots);
        SetInteger('SensorWeite',Projekt.SensorWeite);
      end;
    end;
    ptExtension:
    begin
      result:=SetItemSettings(RecordExtension);
      for Dummy := 0 to Projekt.ExtCount-1 do
      begin
        Result.SetInteger(
          'Ext[' + GetEnumName(TypeInfO(TExtensionProp), Ord(Projekt.Extensions[Dummy].Prop)) + ']',
          Projekt.Extensions[Dummy].Value);
      end;
    end;
    ptGranate,ptMine:
    begin
      if Projekt.TypeID=ptGranate then
        result:=SetItemSettings(RecordGranate)
      else
        result:=SetItemSettings(RecordMine);

      result.SetInteger('Reichweite',Projekt.Reichweite);
    end;
    ptGuertel:  result:=SetItemSettings(RecordGuertel);
    ptPanzerung:
    begin
      result:=SetItemSettings(RecordPanzerung);
      result.SetInteger('Panzerung',Projekt.Panzerung);
    end;
    ptShield:
    begin
      result:=SetItemSettings(RecordSchild);
      result.SetInteger('Strength',Projekt.Strength);
      result.SetInteger('Panzerung',Projekt.Panzerung);
      result.SetInteger('Laden',Projekt.Laden);
    end;
    ptSensor: result:=SetItemSettings(RecordSensor);
    ptMotor:
    begin
      result:=SetItemSettings(RecordTriebwerk);
      result.SetInteger('Munition',Projekt.Munition);
      result.SetInteger('PixPerSec',Projekt.PixPerSec);
      result.SetBoolean('Verfolgung',Projekt.Verfolgung);
    end;
    ptWaffe:
    begin
      result:=SetItemSettings(RecordWaffe);
      result.SetBoolean('Einhand',Projekt.Einhand);
      result.SetInteger('TimeUnits',Projekt.TimeUnits);
      result.SetInteger('WaffType',Integer(Projekt.WaffType));

      if Projekt.WaffType=wtLaser then
      begin
        result.SetInteger('Strength',Projekt.Strength);
        result.SetInteger('Munition',Projekt.Munition);
        result.SetInteger('Power',Projekt.Power);
      end
      else if Projekt.WaffType=wtShortRange then
      begin
        result.SetInteger('Strength',Projekt.Strength);
        result.SetInteger('Power',Projekt.Power);
      end
      else
      begin
        result.SetInteger('Strength',0);
        result.SetInteger('Munition',0);
        result.SetInteger('Power',0);
      end;

      for DummySchussType := low(TSchussType) to high(TSchussType) do
      begin
        Name:='SchussType['+GetEnumName(TypeInfO(TSchussType), Ord(DummySchussType))+']';

        if result.ValueExists(Name) then
          Result.SetBoolean(Name,(DummySchussType in Projekt.Schuss));
      end;

      result.SetInteger('Frequenz',Projekt.Laden);
      result.SetInteger('EffRange',Projekt.Genauigkeit);

      result.SetString('ShootingSound',Projekt.ShootSound);
    end;
    ptMunition:
    begin
      result:=SetItemSettings(RecordMunition);
      result.SetInteger('WaffType',Integer(Projekt.WaffType));
      result.SetInteger('Strength',Projekt.Strength);
      result.SetInteger('Munition',Projekt.Munition);
      result.SetInteger('Power',Projekt.Power);
      result.SetCardinal('MunFor',Projekt.MunFor);
      result.SetInteger('ExplosionSize',Projekt.Explosion);
    end;
    ptRWaffe:
    begin
      result:=SetItemSettings(RecordRWaffe);
      result.SetInteger('WaffType',Integer(Projekt.WaffType));
      result.SetInteger('Strength',Projekt.Strength);
      result.SetInteger('Munition',Projekt.Munition);
      result.SetInteger('PixPerSec',Projekt.PixPerSec);
      result.SetInteger('Laden',Projekt.Laden);
      result.SetBoolean('Verfolgung',Projekt.Verfolgung);
      result.SetInteger('Reichweite',Projekt.Reichweite);
    end;
    ptRMunition:
    begin
      result:=SetItemSettings(RecordRMunition);
      result.SetInteger('WaffType',Integer(Projekt.WaffType));
      result.SetInteger('Strength',Projekt.Strength);
      result.SetInteger('Munition',Projekt.Munition);
      result.SetCardinal('MunFor',Projekt.MunFor);
    end;
    else
      raise Exception.Create('Ung�ltiger projekttype :'+GetEnumName(TypeInfo(TProjektType),Ord(Projekt.TypeID)));
  end;
end;

function RecordToProject(Rec: TExtRecord): TForschProject;
var
  Dummy             : Integer;
  DummyExtensionProp: TExtensionProp;
  DummySchussType   : TSchussType;
  Name              : String;

  procedure SetDefaultSettings;
  begin
    with rec do
    begin
      result.ID:=GetCardinal('ID');
      result.TypeID:=TProjektType(GetInteger('TypeID'));
      result.Name:=GetString('Name');
      result.Info:=GetString('Info');
      result.ResearchInfo:=GetString('ResearchInfo');
      result.Length:=length(result.Info);
      result.Start:=GetBoolean('Start');
      result.Ver:=GetInteger('Ver');
      result.Stunden:=GetInteger('Stunden');
      result.Anzahl:=GetInteger('Anzahl');
    end;
    GetParentArray(rec,TParentArray(result.Parents));
    result.AlienItem:=false;
  end;

  procedure SetItemSettings;
  begin
    with rec do
    begin
      result.KaufPreis:=GetInteger('KaufPreis');
      result.VerKaufPreis:=GetInteger('VerKaufPreis');
      result.ImageIndex:=GetInteger('ImageIndex');
      result.Land:=GetInteger('Land');
      result.Herstellbar:=GetBoolean('Herstellbar');

      if ValueExists('LagerV') then
        result.LagerV:=GetDouble('LagerV');

      if ValueExists('Gewicht') then
        result.Gewicht:=GetDouble('Gewicht');

      if ValueExists('Strength') then
        result.Strength:=GetInteger('Strength');

      if ValueExists('NeededIQ') then
        result.NeedIQ:=GetInteger('NeededIQ');

      result.UFOPaedieImage:=GetString('UFOPaedieImage');

      result.AlienItem:=GetBoolean('AlienItem');
      result.ResearchCount:=GetInteger('ResearchItems');
      result.ManuAlphatron:=GetInteger('Alphatron');
      result.ProdTime:=GetInteger('ProdTime');
      Result.AlienChance:=GetInteger('AlienChance');
    end;
  end;

begin
  SetDefaultSettings;
  case result.TypeID of
    ptNone:
    begin
      with rec do
      begin
        result.Patentgebuehr:=GetInteger('Patengebuehr');
        result.Patentzeit:=GetInteger('PatenZeit');
        result.Patentanstieg:=GetInteger('Patenanstieg');
        result.PatentSatt:=GetInteger('PatenSatt');
        result.UFOPaedieImage:=GetString('UFOPaedieImage');
      end;
    end;
    ptEinrichtung:
    begin
      with rec do
      begin
        result.KaufPreis:=GetInteger('KaufPreis');
        result.BauZeit:=GetInteger('BauZeit');

        result.RoomTyp:=TRoomTyp(GetInteger('RoomTyp'));

        result.LagerRaum:=GetInteger('LagerRaum');
        result.AlphatronStorage:=GetInteger('AlphatronStorage');

        result.WerkRaum:=GetInteger('WerkRaum');
        result.Quartiere:=GetInteger('Quartiere');
        result.LaborRaum:=GetInteger('LaborRaum');
        result.Abwehr:=GetInteger('Abwehr');
        result.SmallSchiff:=GetInteger('SmallSchiff');
        result.Reichweite:=GetInteger('minReichweite');
        result.PixPerSec:=GetInteger('PixPerSec');
        result.BuildImageBase:=GetString('GrafikBasement');
        result.BuildImageUnder:=GetString('GrafikUnderground');
        result.UnitWidth:=GetInteger('UnitWidth');
        result.UnitHeight:=GetInteger('UnitHeight');

        result.SensorWidth:=GetInteger('SensorWidth');
        result.ShieldStrength:=GetInteger('ShieldStrength');
        result.Treffsicherheit:=GetInteger('Treffsicherheit');
        result.maxReichweite:=GetInteger('maxReichweite');
        result.WaffType:=TWaffenType(GetInteger('WaffenArt'));
        result.RoomHitPoints:=GetInteger('Hitpoints');
      end;
    end;
    ptRaumSchiff:
    begin
      with rec do
      begin
        result.KaufPreis:=GetInteger('KaufPreis');
        result.VerKaufPreis:=GetInteger('VerKaufPreis');
        result.HitPoints:=GetInteger('HitPoints');
        result.WaffenZellen:=GetInteger('WaffenZellen');
        result.LagerPlatz:=GetInteger('LagerPlatz');
        result.Soldaten:=GetInteger('Soldaten');
        result.BauZeit:=GetInteger('BauZeit');
        result.ExtendSlots:=GetInteger('ExtendSlots');
        result.SensorWeite:=GetInteger('SensorWeite');
      end;
    end;
    ptExtension:
    begin
      SetItemSettings;
      result.ExtCount:=0;
      for DummyExtensionProp := Low(TExtensionProp) to High(TExtensionProp) do
      begin
        Dummy := Rec.GetInteger('Ext[' + GetEnumName(TypeInfO(TExtensionProp), Ord(DummyExtensionProp)) + ']');
        if Dummy>0 then
        begin
          Result.Extensions[Result.ExtCount].Prop:=DummyExtensionProp;
          Result.Extensions[Result.ExtCount].Value:=Dummy;
          Inc(Result.ExtCount);
        end;
      end;
    end;
    ptGranate,ptMine:
    begin
      SetItemSettings;

      result.Reichweite:=Rec.GetInteger('Reichweite');
    end;
    ptGuertel:  SetItemSettings;
    ptPanzerung:
    begin
      SetItemSettings;
      result.Panzerung:=Rec.GetInteger('Panzerung');
    end;
    ptShield:
    begin
      SetItemSettings;
      Result.ExtCount := 3;
      Result.Extensions[0].Prop:=epShieldHitpoints;
      Result.Extensions[0].Value:=Rec.GetInteger('Strength');
      Result.Extensions[1].Prop:=epShieldDefence;
      Result.Extensions[1].Value:=Rec.GetInteger('Panzerung');
      Result.Extensions[2].Prop:=epShieldReloadTime;
      Result.Extensions[2].Value:=Rec.GetInteger('Laden');
      Result.TypeID := ptExtension;
    end;
    ptSensor: SetItemSettings;
    ptMotor:
    begin
      SetItemSettings;
      result.Munition:=Rec.GetInteger('Munition');
      result.PixPerSec:=Rec.GetInteger('PixPerSec');
      result.Verfolgung:=Rec.GetBoolean('Verfolgung');
    end;
    ptWaffe:
    begin
      SetItemSettings;
      result.Einhand:=Rec.GetBoolean('Einhand');
      result.TimeUnits:=Rec.GetInteger('TimeUnits');
      result.WaffType:=TWaffenType(Rec.GetInteger('WaffType'));

      if result.WaffType=wtLaser then
      begin
        result.Strength:=Rec.GetInteger('Strength');
        result.Munition:=Rec.GetInteger('Munition');
        result.Power:=Rec.GetInteger('Power');
      end
      else if result.WaffType=wtShortRange then
      begin
        result.Strength:=Rec.GetInteger('Strength');
        result.Power:=Rec.GetInteger('Power');
      end
      else
      begin
        result.Strength:=0;
        result.Munition:=0;
        result.Power:=0;
      end;
      result.Schuss:=[];
      for DummySchussType := low(TSchussType) to high(TSchussType) do
      begin
        Name:='SchussType['+GetEnumName(TypeInfO(TSchussType), Ord(DummySchussType))+']';

        if Rec.ValueExists(Name) and Rec.GetBoolean(Name) then
          Include(result.Schuss,DummySchussType);
      end;

      result.Laden:=Rec.GetInteger('Frequenz');
      result.Genauigkeit:=Rec.GetInteger('EffRange');

      result.ShootSound:=Rec.GetString('ShootingSound');
    end;
    ptMunition:
    begin
      SetItemSettings;
      result.WaffType:=TWaffenType(Rec.GetInteger('WaffType'));
      result.Strength:=Rec.GetInteger('Strength');
      result.Munition:=Rec.GetInteger('Munition');
      result.Power:=Rec.GetInteger('Power');
      result.MunFor:=Rec.GetCardinal('MunFor');
      result.Explosion:=Rec.GetInteger('ExplosionSize');
    end;
    ptRWaffe:
    begin
      SetItemSettings;
      result.WaffType:=TWaffenType(Rec.GetInteger('WaffType'));
      result.Strength:=Rec.GetInteger('Strength');
      result.Munition:=Rec.GetInteger('Munition');
      result.PixPerSec:=Rec.GetInteger('PixPerSec');
      result.Verfolgung:=Rec.GetBoolean('Verfolgung');
      result.Laden:=Rec.GetInteger('Laden');
      result.Reichweite:=Rec.GetInteger('Reichweite');
    end;
    ptRMunition:
    begin
      SetItemSettings;
      result.WaffType:=TWaffenType(Rec.GetInteger('WaffType'));
      result.Strength:=Rec.GetInteger('Strength');
      result.Munition:=Rec.GetInteger('Munition');
      result.MunFor:=Rec.GetCardinal('MunFor');
    end;
    else
      raise Exception.Create('Ung�ltiger projekttype :'+GetEnumName(TypeInfo(TProjektType),Ord(result.TypeID)));
  end;

  // Korrektur der Schusstypen
  result.Schuss:=CheckSchussType(Result.TypeID,Result.WaffType,result.Schuss);
end;

function GetDefinitionFromType(TypeID: TProjektType): TExtRecordDefinition;
begin
  case TypeID of
    ptWaffe       : result:=RecordWaffe;
    ptMunition    : result:=RecordMunition;
    ptRWaffe      : result:=RecordRWaffe;
    ptEinrichtung : result:=RecordBasis;
    ptPanzerung   : result:=RecordPanzerung;
    ptRaumSchiff  : result:=RecordRaumschiff;
    ptRMunition   : result:=RecordRMunition;
    ptGranate     : result:=RecordGranate;
    ptMotor       : result:=RecordTriebwerk;
    ptMine        : result:=RecordMine;
    ptNone        : result:=RecordTechnologie;
    ptSensor      : result:=RecordSensor;
    ptShield      : result:=RecordSchild;
    ptExtension   : result:=RecordExtension;
    ptGuertel     : result:=RecordGuertel;
    else
      raise Exception.Create('Ung�ltiger projekttype :'+GetEnumName(TypeInfo(TProjektType),Ord(TypeID)));
  end;
end;

procedure SaveProjectRecord(ExtRecord: TExtRecord; Stream: TStream);
var
  Buffer: Cardinal;
begin
  Buffer:=ExtRecord.RecordDefinition.RecordHash;
  Stream.Write(Buffer,sizeOf(Buffer));

  ExtRecord.SaveToStream(Stream);
end;

function ReadProjectRecord(Stream: TStream): TExtRecord;
var
  Dummy: Integer;
  Buffer: Cardinal;
begin
  result:=nil;
  Stream.Read(Buffer,SizeOf(Buffer));

  for Dummy:=0 to ProjectRecordList.Count-1 do
  begin
    if TExtRecordDefinition(ProjectRecordList[Dummy]).RecordHash=Buffer then
    begin
      result:=TExtRecord.Create(TExtRecordDefinition(ProjectRecordList[Dummy]));
      result.LoadFromStream(Stream);
      exit;
    end;
  end;
end;

procedure GetParentArray(Rec: TExtRecord; var Parents: TParentArray);
begin
  if not Rec.ValueExists('ParentCount') then
  begin
    SetLength(Parents,0);
    exit;
  end;
  SetLength(Parents,rec.GetInteger('ParentCount'));

  CopyMemory(Addr(Parents[0]),Rec.GetBinary('Parents'),min(SizeOf(Cardinal)*Rec.GetInteger('ParentCount'),rec.Values[rec.GetValueIndex('Parents')].binaryLength));
end;

procedure SetParentArray(Rec: TExtRecord; var Parents: TParentArray);
var
  Mem: Pointer;
begin
  Rec.SetInteger('ParentCount',length(Parents));
  Mem:=GetMemory(sizeOf(Cardinal)*length(Parents));
  CopyMemory(Mem,Addr(Parents[0]),sizeOf(Cardinal)*length(Parents));
  Rec.SetBinary('Parents',sizeOf(Cardinal)*length(Parents),mem);
  FreeMemory(Mem);
end;

initialization
  ProjectRecordList:=TList.Create;

  GenerateTechnologieRecordDesc;

  GenerateRaumschiffRecordDesc;
  GenerateBasisRecordDesc;

  // Ausr�stungen
  GenerateExtensionRecordDesc;
  GenerateGranatenRecordDesc;
  GenerateGuertelRecordDesc;
  GenerateMineRecordDesc;
  GeneratePanzerungRecordDesc;
  GenerateSchildRecordDesc;
  GenerateSensorRecordDesc;
  GenerateTriebwerkRecordDesc;
  GenerateWaffeRecordDesc;
  GenerateMunitionRecordDesc;
  GenerateRWaffeRecordDesc;
  GenerateRMunitionRecordDesc;
finalization

  RecordTechnologie.Free;

  RecordRaumschiff.Free;
  RecordBasis.Free;

  RecordExtension.Free;
  RecordGranate.Free;
  RecordGuertel.Free;
  RecordMine.Free;
  RecordPanzerung.Free;
  RecordSchild.Free;
  RecordSensor.Free;
  RecordTriebwerk.Free;
  RecordWaffe.Free;
  RecordMunition.Free;
  RecordRWaffe.Free;
  RecordRMunition.Free;

  ProjectRecordList.Free;
end.
