{********************************************************************************
*										*
* Copyright (C) 2004  X-Force Team - jim_raynor@web.de				*
*										*
* This file is part of X-Force: Fight For Destiny (X-Force).			*
*										*
* X-Force is free software; you can redistribute it and/or modify		*
* it under the terms of the GNU General Public License as published by		*
* the Free Software Foundation; either version 2 of the License, or		*
* (at your option) any later version.						*
*										*
* X-Force is distributed in the hope that it will be useful,			*
* but WITHOUT ANY WARRANTY; without even the implied warranty of		*
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the			*
* GNU General Public License for more details.					*
*										*
* You should have received a copy of the GNU General Public License		*
* along with X-Force; if not, write to the Free Software			*
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA	*
*										*
*********************************************************************************
*										*
* Paar Funktionen um Streams zu packen und zu "verschlüsseln"			*
*										*
********************************************************************************}

{$I ../Settings.inc}

unit Koding;

interface

uses Classes, math, Dialogs, SysUtils, Windows;

procedure Encode(Input: TStream;OutPut: TStream);
procedure Decode(Input: TStream;OutPut: TStream);
procedure Pack(Input: TStream;OutPut: TStream);
procedure UnPack(Input: TStream;OutPut: TStream);

implementation

procedure Encode(Input: TStream;OutPut: TStream);
var
  Temp: Cardinal;
  Size: Integer;
  StillKode: Integer;
  m: TMemoryStream;
begin
  Input.Position:=0;
  OutPut.Size:=0;
  m:=TMemoryStream.Create;
  pack(Input,m);
  Size:=m.Size;
  Temp:=size;
  asm
    ROL temp,13
  end;
  StillKode:=Size;
  Output.Write(Temp,sizeOf(Integer));
  while StillKode>0 do
  begin
    Temp:=0;
    m.Read(temp,min(sizeOf(Integer),StillKode));
    asm
      ROL temp,9
    end;
    inc(Temp,Size);
    OutPut.Write(temp,sizeOf(Integer));
    dec(StillKode,sizeOf(Integer));
  end;
  Input.Position:=0;
  OutPut.Position:=0;
  m.Free;
end;

procedure Decode(Input: TStream;OutPut: TStream);
var
  Temp: Cardinal;
  Size: Integer;
  StillKode: Integer;
  m: TMemoryStream;
begin
  Input.Position:=0;
  OutPut.Position:=0;
  Temp:=0;
  m:=TMemoryStream.Create;
  Input.Read(temp,sizeOf(Integer));
  asm
    ROR Temp,13
  end;
  Size:=Temp;
  StillKode:=Size;
  while StillKode>0 do
  begin
    Temp:=0;
    Input.Read(temp,sizeOf(Integer));
    dec(Temp,Size);
    asm
      ROR temp,9
    end;
    m.Write(temp,min(sizeOf(Integer),StillKode));
    dec(StillKode,sizeOf(Integer));
  end;
  Input.Position:=0;
  OutPut.Position:=0;
  UnPack(m,OutPut);
  OutPut.Position:=0;
  m.Free;
end;

procedure Pack(Input: TStream;OutPut: TStream);
var
  StatusByte: Byte;
  WriteBuff: Array of Char;
  ReadChar: Char;
  AktChar: Char;
  EndFile: boolean;


  function GetByte(var Byte: Char): boolean;
  begin
    if Input.Read(Byte,1)=0 then result:=true else result:=false;
  end;

  procedure AddByteToArray(Ch: Char);
  begin
    SetLength(WriteBuff,Length(WriteBuff)+1);
    WriteBuff[length(WriteBuff)-1]:=Ch;
  end;

  procedure WriteBuffer;
  var
    Dummy: Integer;
  begin
    OutPut.Write(StatusByte,1);
    for Dummy:=0 to length(WriteBuff)-1 do OutPut.Write(WriteBuff[Dummy],1);
    SetLength(WriteBuff,0);
  end;


begin
  Input.Position:=0;
  OutPut.Size:=0;
  GetByte(AktChar);
  GetByte(ReadChar);
  StatusByte:=1;
  EndFile:=false;
  repeat
  begin
    if AktChar=ReadChar then
    begin
      AddByteToArray(AktChar);
      StatusByte:=2;
      EndFile:=GetByte(ReadChar);
      while (AktChar=ReadChar) and not EndFile and (StatusByte<120) do
      begin
        inc(StatusByte);
        EndFile:=GetByte(ReadChar);
      end;
      WriteBuffer;
    end
    else
    begin
      StatusByte:=0;
      while (AktChar<>ReadChar) and (StatusByte<120) do
      begin
        inc(StatusByte);
        AddByteToArray(AktChar);
        AktChar:=ReadChar;
        if EndFile then break;
        EndFile:=GetByte(ReadChar);
      end;
      AddByteToArray(AktChar);
      inc(StatusByte);
      StatusByte:=Statusbyte or 128;
      WriteBuffer;
    end;
    if not Endfile then
    begin
      AktChar:=ReadChar;
      EndFile:=GetByte(ReadChar);
      if EndFile then
      begin
        AddByteToArray(AktChar);
        StatusByte:=129;
        WriteBuffer;
      end;
    end;
  end
  until EndFile;
  Input.Position:=0;
  OutPut.Position:=0;
end;

procedure UnPack(Input: TStream;OutPut: TStream);
var
  Token  : Byte;
  Ch     : Char;
  Dummy  : Integer;
begin
  Input.Position:=0;
  OutPut.Position:=0;
  repeat
    Input.Read(Token,1);
    if Token and 128=0 then
    begin
      Input.Read(Ch,1);
      for Dummy:=1 to Token do outPut.Write(Ch,1);
    end
    else
    begin
      Token:=Token and 127;
      for Dummy:=1 to Token do
      begin
        Input.Read(Ch,1);
        OutPut.Write(Ch,1);
      end;
    end;
  until Input.Position=Input.Size;
  Input.Position:=0;
  OutPut.Position:=0;
end;

end.
