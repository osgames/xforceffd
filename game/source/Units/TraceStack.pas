{********************************************************************************
*										*
* Copyright (C) 2004  X-Force Team - jim_raynor@web.de				*
*										*
* This file is part of X-Force: Fight For Destiny (X-Force).			*
*										*
* X-Force is free software; you can redistribute it and/or modify		*
* it under the terms of the GNU General Public License as published by		*
* the Free Software Foundation; either version 2 of the License, or		*
* (at your option) any later version.						*
*										*
* X-Force is distributed in the hope that it will be useful,			*
* but WITHOUT ANY WARRANTY; without even the implied warranty of		*
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the			*
* GNU General Public License for more details.					*
*										*
* You should have received a copy of the GNU General Public License		*
* along with X-Force; if not, write to the Free Software			*
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA	*
*										*
*********************************************************************************
*										*
* basis_api bildet die Schnittstelle zu einer Basisliste, die bis zu 8 Basen	*
* verwaltet									*
*										*
* WICHTIG!! Bevor die Funktionen genutzt werden k�nnen muss �ber 		*
*           basis_api_init die Basisliste gesetzt werden. Sollte durch 		*
*           savegame_api_init geschehen sein.					*
*										*
********************************************************************************}

{$I ../Settings.inc}

unit TraceStack;

interface

uses classes,Sysutils,math;

procedure PushAddress;
procedure WriteLastPointer;

var
  Stack        : Array[0..100] of Integer;
  Error        : boolean;
  StackPos     : Integer;
  NoChange     : boolean;
  LastSuccess  : Integer;
  TracingStack : boolean=false;

implementation

uses TraceFile;

procedure PushAddress;
begin
  asm
   mov ebx,ebp
  end;
  if StackPos<0 then
    asm MOV ECX,0 end
  else
    asm MOV ECX,[StackPos]  end;
  asm
    mov eax,[ebx+4]
    sub eax,$05
    mov EDX,ecx
    mov [edx*4+offset Stack],eax
    inc ECX
    MOV STACKPos,ECX
    MOV LastSuccess,ECX
  end;
end;

procedure WriteLastPointer;
begin
  GlobalFile.Write('WriteLastPointer',true,Stack[StackPos-1]);
end;

initialization
  LastSuccess:=0;
  Error:=false;
  StackPos:=0;
  NoChange:=false;
end.
