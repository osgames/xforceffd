{********************************************************************************
*										*
* Copyright (C) 2004  X-Force Team - jim_raynor@web.de				*
*										*
* This file is part of X-Force: Fight For Destiny (X-Force).			*
*										*
* X-Force is free software; you can redistribute it and/or modify		*
* it under the terms of the GNU General Public License as published by		*
* the Free Software Foundation; either version 2 of the License, or		*
* (at your option) any later version.						*
*										*
* X-Force is distributed in the hope that it will be useful,			*
* but WITHOUT ANY WARRANTY; without even the implied warranty of		*
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the			*
* GNU General Public License for more details.					*
*										*
* You should have received a copy of the GNU General Public License		*
* along with X-Force; if not, write to the Free Software			*
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA	*
*										*
*********************************************************************************
*										*
* Definiert ExtRecords zu Objekten wie Aliens und UFOs und stellt entsprechende	*
* konvertierfunktionen zur Verf�gung						*
*										*
********************************************************************************}

{$I ../Settings.inc}

unit ObjektRecords;

interface

{
  Regul�re Ausdruck zum Suchen :.+Add\(.+\)('\([^']+\)'.+
  Ersetzen ExtRecord to Record :    \2:=Rec.Get\1('\2');
  Ersetzen Record to ExtRecord :    result.Set\1('\2',\2);
}
uses
  ExtRecord, XForce_types;

var
  RecordAlien          : TExtRecordDefinition;
  RecordUFOModel       : TExtRecordDefinition;
  ModelData            : TExtRecordDefinition;
  RecordAlienInUFO     : TExtRecordDefinition;
  RecordMissionSkript  : TExtRecordDefinition;




function AlienToExtRecord(const Alien: TAlien): TExtRecord;
function ExtRecordToAlien(Rec: TExtRecord): TAlien;

function UFOToExtRecord(const UFO: TUFOModel): TExtRecord;
function ExtRecordToUFO(Rec: TExtRecord): TUFOModel;

implementation

procedure GenerateAlienRecordDesc;
begin
  RecordAlienInUFO:=TExtRecordDefinition.Create('AlienInUFO');
  RecordAlienInUFO.AddCardinal('ID',0,high(Cardinal));
  RecordAlienInUFO.AddInteger('Chance',0,100,0);

  RecordAlien:=TExtRecordDefinition.Create('Alien');
  RecordAlien.AddCardinal('ID',0,high(Cardinal),0,'Category=Allgemein|NoEdit=true');
  RecordAlien.AddString('Name',100000,'Unbenannt','Category=Allgemein|Custom=ItemName');
  RecordAlien.AddInteger('Ges',5,500,50,'Name=Lebenspunkte|Category=Physis');
  RecordAlien.AddInteger('Pan',0,100,20,'Name=Panzerung|Category=Physis');
  RecordAlien.AddInteger('Zei',10,100,50,'Name=Zeiteinheiten|Category=Physis');
  RecordAlien.AddInteger('Ver',0,1080,0,'Name=Angriff nach|Einheit=Tagen|Category=Allgemein');
  RecordAlien.AddString('Info',10000000,'','Name=UFO-P�die|Custom=InfoText|Category=Beschreibungen');
  RecordAlien.AddString('ResearchInfo',10000000,'','Name=Forschung|Custom=InfoText|Category=Beschreibungen');
  RecordAlien.AddInteger('Image',0,1080,0,'Name=Symbol|Category=Alien|Visible=false');
  RecordAlien.AddInteger('Power',0,100,50,'Name=Durchschlagskraft|Category=Angriff|Visible=false');
  RecordAlien.AddInteger('Treff',0,100,50,'Name=Treffsicherheit|Category=Angriff');
  RecordAlien.AddInteger('PSAn',0,60,0,'Name=PSI-Angriff|Category=Geist');
  RecordAlien.AddInteger('PSAb',0,100,0,'Name=PSI-Abwehr|Category=Geist');
  RecordAlien.AddInteger('EP',0,10000,100,'Name=Erfahrungspunkte|Category=Allgemein');
  RecordAlien.AddInteger('AutopTime',0,10000,1500,'Name=Autopsiezeit|Einheit=Stunden|Category=Allgemein');
  RecordAlien.AddInteger('Sicht',5,40,20,'Name=Sichtweite|Category=Physis');
  RecordAlien.AddInteger('IQ',0,200,50,'Name=Intelligenz|Category=Geist|Custom=IQ');
  RecordAlien.AddInteger('Reaktion',0,100,50,'Name=Reaktion|Category=Geist');
  RecordAlien.AddBoolean('Autopsie',false,'Visible=false');
end;

procedure GenerateUFORecordDesc;
begin
  RecordUFOModel:=TExtRecordDefinition.Create('UFO');
  RecordUFOModel.AddCardinal('ID',0,high(Cardinal),0,'Category=Allgemein|NoEdit=true');
  RecordUFOModel.AddString('Name',100000,'Unbenannt','Category=Allgemein|Custom=ItemName');
  RecordUFOModel.AddInteger('HitPoints',5,7500,250,'Name=Hitpoints|Category=Allgemein');
  RecordUFOModel.AddInteger('Shield',0,2000,100,'Name=Schildpunkte|Category=Schutzschild');
  RecordUFOModel.AddInteger('Projektil',0,100,10,'Name=Projektil-Abwehr|Einheit=%|Category=Schutzschild');
  RecordUFOModel.AddInteger('Rakete',0,100,10,'Name=Raketen-Abwehr|Einheit=%|Category=Schutzschild');
  RecordUFOModel.AddInteger('Laser',0,100,10,'Name=Laser-Abwehr|Einheit=%|Category=Schutzschild');
  RecordUFOModel.AddInteger('Laden',50,2000,100,'Name=Aufladezeit|Einheit=ms|Category=Schutzschild');
  RecordUFOModel.AddInteger('MinBesatz',0,30,5,'Name=min. Besatzung|Category=Besatzung');
  RecordUFOModel.AddInteger('ZusBesatz',0,20,5,'Name=zus. Besatzung|Category=Besatzung');
  RecordUFOModel.AddInteger('Angriff',1,1500,50,'Name=Angriffst�rke|Category=Allgemein');
  RecordUFOModel.AddInteger('Pps',1,20,5,'Name=Geschwindigkeit|Category=Allgemein');
  RecordUFOModel.AddInteger('Ver',0,1080,0,'Name=Angriff nach|Einheit=Tagen|Category=Allgemein');
  RecordUFOModel.AddInteger('Image',0,high(Integer),0,'Visible=false');
  RecordUFOModel.AddBoolean('FirstMes',false,'Visible=false');
  RecordUFOModel.AddInteger('Treffsicherheit',0,100,50,'Einheit=%|Category=Allgemein');
  RecordUFOModel.AddString('Info',10000000,'','Name=UFO-P�die|Custom=InfoText|Category=Beschreibungen');
  RecordUFOModel.AddString('ResearchInfo',10000000,'','Name=Forschung|Custom=InfoText|Category=Beschreibungen|Visible=false');
  RecordUFOModel.AddInteger('Probability',0,100,100,'Einheit=%|Name=Wahrscheinlichkeit|Category=Allgemein');
  RecordUFOModel.AddRecordList('Aliens',RecordAlienInUFO,'Custom=Aliens|Category=Besatzung');
  RecordUFOModel.AddInteger('Points',0,50000,100,'Name=Punkte f�r Abschuss|Category=Allgemein');
  RecordUFOModel.AddInteger('BaseAttackStrength',0,100000,100,'Name=Angriff|Category=Basisangriff');
  RecordUFOModel.AddInteger('BaseAttackRange',0,1500,100,'Name=Reichweite|Einheit=km|Category=Basisangriff');
end;

procedure GenerateSkriptRecordDesc;
var
  Temp: TExtRecordDefinition;
begin
  RecordMissionSkript:=TExtRecordDefinition.Create('MissionSkript');
  RecordMissionSkript.AddString('Name',100000,'Unbenannt','Category=Allgemein');
  RecordMissionSkript.AddCardinal('ID',0,high(Cardinal),0,'Visible=false');
  RecordMissionSkript.AddString('Skript',100000,'program Name;'#13#10#13#10'begin'#13#10'  MissionName:=''Name'';'#13#10'  MissionType:=mzUFO;'#13#10'end.','Category=Allgemein|Custom=MissionSkript');

  Temp:=TExtRecordDefinition.Create('SkriptTrigger');
  Temp.ParentDescription:=true;
  Temp.AddInteger('TriggerTyp',0,100000,0);
  Temp.AddBoolean('Negation',false);
  Temp.AddInteger('IntParam1',low(Integer),high(Integer),0);
  Temp.AddInteger('IntParam2',low(Integer),high(Integer),0);
  Temp.AddCardinal('CarParam1',0,high(Cardinal),0);
  Temp.AddString('StringParam1',50);
  Temp.AddBoolean('Cleared',false);

  RecordMissionSkript.AddBoolean('Active',true,'Visible=false');
  RecordMissionSkript.AddRecordList('Triggers',Temp,'Category=Allgemein|Name=Bedigungen|Custom=MissionSkriptTrigger');
  RecordMissionSkript.AddInteger('Typ',0,2,1,'Visible=false');
end;

function AlienToExtRecord(const Alien: TAlien): TExtRecord;
begin
  result:=TExtRecord.Create(RecordAlien);
  with Alien do
  begin
    result.SetString('Name',Name);
    result.SetCardinal('ID',ID);
    result.SetInteger('Ges',Ges);
    result.SetInteger('Pan',Pan);
    result.SetInteger('PSAn',PSAn);
    result.SetInteger('PSAb',PSAb);
    result.SetInteger('Zei',Zei);
    result.SetInteger('Ver',Ver);
    result.SetString('Info',Info);
    result.SetString('ResearchInfo',ResearchInfo);
    result.SetInteger('Image',Image);
    result.SetInteger('Power',Power);
    result.SetInteger('Treff',Treff);
    result.SetInteger('EP',EP);
    result.SetInteger('Sicht',Sicht);
    result.SetBoolean('Autopsie',Autopsie);
    result.SetInteger('AutopTime',AutopTime);
    result.SetInteger('Reaktion',Reaktion);
    result.SetInteger('IQ',IQ);
  end;
end;

function ExtRecordToAlien(Rec: TExtRecord): TAlien;
begin
  with Result do
  begin
    Name:=Rec.GetString('Name');
    ID:=Rec.GetCardinal('ID');
    Ges:=Rec.GetInteger('Ges');
    Pan:=Rec.GetInteger('Pan');
    PSAn:=Rec.GetInteger('PSAn');
    PSAb:=Rec.GetInteger('PSAb');
    Zei:=Rec.GetInteger('Zei');
    Ver:=Rec.GetInteger('Ver');
    Info:=Rec.GetString('Info');
    ResearchInfo:=Rec.GetString('ResearchInfo');
    Image:=Rec.GetInteger('Image');
    Power:=Rec.GetInteger('Power');
    Treff:=Rec.GetInteger('Treff');
    EP:=Rec.GetInteger('EP');
    Sicht:=Rec.GetInteger('Sicht');
    Autopsie:=Rec.GetBoolean('Autopsie');
    AutopTime:=Rec.GetInteger('AutopTime');
    IQ:=Rec.GetInteger('IQ');
    Reaktion:=Rec.GetInteger('Reaktion');
  end;
end;

function UFOToExtRecord(const UFO: TUFOModel): TExtRecord;
var
  Dummy: Integer;
begin
  result:=TExtRecord.Create(RecordUFOModel);
  with UFO do
  begin
    result.SetString('Name',Name);
    result.SetCardinal('ID',ID);
    result.SetInteger('HitPoints',HitPoints);
    result.SetInteger('Shield',Shield);
    result.SetInteger('Projektil',ShieldType.Projektil);
    result.SetInteger('Rakete',ShieldType.Rakete);
    result.SetInteger('Laser',ShieldType.Laser);
    result.SetInteger('Laden',ShieldType.Laden);
    result.SetInteger('MinBesatz',MinBesatz);
    result.SetInteger('ZusBesatz',ZusBesatz);
    result.SetInteger('Angriff',Angriff);
    result.SetInteger('Pps',Pps);
    result.SetInteger('Ver',Ver);
    result.SetInteger('Image',Image);
    result.SetBoolean('FirstMes',FirstMes);
    result.SetInteger('Treffsicherheit',Treffsicherheit);
    result.SetString('Info',Info);
    result.SetInteger('Probability',Probability);

    with result.GetRecordList('Aliens') do
    begin
      for Dummy:=0 to high(Aliens) do
      begin
        with Add do
        begin
          SetCardinal('ID',Aliens[Dummy].AlienID);
          SetInteger('Chance',Aliens[Dummy].Chance);
        end;
      end;
    end;

    result.SetInteger('Points',Points);
    result.SetInteger('BaseAttackStrength',BaseAttackStrength);
    result.SetInteger('BaseAttackRange',BaseAttackRange);
  end;
end;

function ExtRecordToUFO(Rec: TExtRecord): TUFOModel;
var
  Dummy: Integer;
begin
  with Result do
  begin
    Name:=Rec.GetString('Name');
    ID:=Rec.GetCardinal('ID');
    HitPoints:=Rec.GetInteger('HitPoints');
    Shield:=Rec.GetInteger('Shield');
    ShieldType.Projektil:=Rec.GetInteger('Projektil');
    ShieldType.Rakete:=Rec.GetInteger('Rakete');
    ShieldType.Laser:=Rec.GetInteger('Laser');
    ShieldType.Laden:=Rec.GetInteger('Laden');
    MinBesatz:=Rec.GetInteger('MinBesatz');
    ZusBesatz:=Rec.GetInteger('ZusBesatz');
    Angriff:=Rec.GetInteger('Angriff');
    Pps:=Rec.GetInteger('Pps');
    Ver:=Rec.GetInteger('Ver');
    Image:=Rec.GetInteger('Image');
    FirstMes:=Rec.GetBoolean('FirstMes');
    Treffsicherheit:=Rec.GetInteger('Treffsicherheit');
    Info:=Rec.GetString('Info');
    Probability:=Rec.GetInteger('Probability');

    // Aliens
    with Rec.GetRecordList('Aliens') do
    begin
      SetLength(Aliens,Count);
      for Dummy:=0 to Count-1 do
      begin
        Aliens[Dummy].AlienID:=Item[Dummy].GetCardinal('ID');
        Aliens[Dummy].Chance:=Item[Dummy].GetInteger('Chance');
      end;
    end;

    Points:=Rec.GetInteger('Points');
    BaseAttackStrength:=Rec.GetInteger('BaseAttackStrength');
    BaseAttackRange:=Rec.GetInteger('BaseAttackRange');
  end;
end;

initialization
  GenerateAlienRecordDesc;
  GenerateUFORecordDesc;
  GenerateSkriptRecordDesc;

  ModelData:=TExtRecordDefinition.Create('ModelData');
  ModelData.AddString('imageset',1000);
  ModelData.AddInteger('torsosize',0,50);
  ModelData.AddInteger('headsize',0,50);
  ModelData.AddInteger('footsize',0,50);
  ModelData.AddInteger('footheight',0,high(Integer));
  ModelData.AddInteger('torsoheight',0,high(Integer));
  ModelData.AddInteger('headheight',0,high(Integer));
  ModelData.AddInteger('selectionwidth',0,high(Integer));
  ModelData.AddInteger('selectionheight',0,high(Integer));
  ModelData.AddInteger('aniheight',0,high(Integer));
  ModelData.AddInteger('aniwidth',0,high(Integer));
  ModelData.AddInteger('anioffsetx',0,high(Integer));
  ModelData.AddInteger('anioffsety',0,high(Integer));

finalization
  RecordAlien.Free;
  RecordUFOModel.Free;
  ModelData.Free;
  RecordAlienInUFO.Free;
  RecordMissionSkript.Free;

end.
