{********************************************************************************
*										*
* Copyright (C) 2004  X-Force Team - jim_raynor@web.de				*
*										*
* This file is part of X-Force: Fight For Destiny (X-Force).			*
*										*
* X-Force is free software; you can redistribute it and/or modify		*
* it under the terms of the GNU General Public License as published by		*
* the Free Software Foundation; either version 2 of the License, or		*
* (at your option) any later version.						*
*										*
* X-Force is distributed in the hope that it will be useful,			*
* but WITHOUT ANY WARRANTY; without even the implied warranty of		*
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the			*
* GNU General Public License for more details.					*
*										*
* You should have received a copy of the GNU General Public License		*
* along with X-Force; if not, write to the Free Software			*
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA	*
*										*
*********************************************************************************
*										*
* Hier werden sollten alle Texte als Variablen definiert werden, die in den	*
* Sprachdateien auftauchen.							*
*										*
********************************************************************************}

{$I ../Settings.inc}

unit StringConst;


interface

uses XForce_types, Classes;

const
  { Identifikationstexte }
  LGameName       = 'X-Force - Fight for Destiny';
  LShortName      = 'XForce';
  LEXEName        = 'XForce.exe';
  IDAlphatron      = 'Alphatron';
  IDGeldMittel    = 'Credits';

var
  { Texte, die mehrmals vorkommen und sich nicht auf ein Formular beziehen }
  HExit           : string;
  HKapital        : string;
  HAlphatron      : string;
  HEscToEnd       : string;

  { Beschreibungen f�r alle Komponenten im Soldaten Formular }
  HSoldatenList   : string;
  HSoldatView     : string;
  HSellSoldat     : string;
  HBuySoldatList  : string;
  HBuySoldat      : string;
  HTrainSoldat    : string;
  HFreeSoldat     : string;
  HFreeAllSoldat  : string;
  HTrainAllSoldat : string;
  HRenameSoldat   : string;

  { Beschreibungen f�r alle Komponenten im Forschungs Formular }
  HProjektList    : string;
  HForscherList   : string;
  HAusbildung     : string;
  HFFree          : string;
  HAusbildungAll  : string;
  HFFreeAll       : string;
  HAusbildungFree : string;
  HZuweisen       : string;
  HZuweisenAll    : string;
  HZuweisenFree   : string;
  HHistorie       : string;
  LUpgradeName    : string;
  LUpgradeStarted : string;
  BHistorie       : string;
  MNoUFOPadie     : string;

  { Beschreibungen f�r alle Komponenten im Lager Formular }
  HItemList       : string;
  HItemInfo       : string;
  HKaufBar        : string;
  HBuyItem        : string;
  HSellItem       : string;
  HAuftrag        : string;
  HSSuche         : string;
  HFAll           : string;
  HFWaf           : string;
  HFGra           : string;
  HFMin           : string;
  HFSen           : string;
  HFMot           : string;
  HFPan           : string;
  BFAll           : string;
  BFWaf           : string;
  BFGra           : string;
  BFMin           : string;
  BFSen           : string;
  BFMot           : string;
  BFPan           : string;
  BSSuche         : string;
  FAnzahl         : string;

  { Beschreibungen f�r alle Komponenten im Auftrags Formular }
  HStonieren      : string;
  HAuftragsList   : string;
  HAngebotsList   : string;
  HAcceptAngebot  : string;

  { Beschreibungen f�r alle Komponenten in der Werkstatt }
  MChangeProjekt  : string;
  MendlessProjekt : string;
  LProdukt        : string;
  FHerstellen     : string;
  LEndLessProdukt : string;
  HNewProjekt     : string;
  HItemProjektList: string;
  HTechniker      : string;
  HKaufTech       : string;
  HTrainTech      : string;
  HFreeTech       : string;
  HFTrainTech     : string;
  HATrainTech     : string;
  HAFreeTech      : string;
  HHersProjList   : string;
  HBegin          : string;
  HChangeProjekt  : string;
  HCancelProj     : string;
  HSetHProj       : string;
  HProduktInfo    : string;
  HAnzBar         : string;
  BbCancel        : string;
  MCancelEndProd  : string;
  MCancelProdukt  : string;
  CCancelProdukt  : string;

  { Beschreibungen f�r alle Komponenten in der UFOP�die }
  HUFOItems       : string;
  HUFOItemInfo    : string;
  HNextEntry      : string;
  HPrevEntry      : string;
  HNextCapt       : string;
  HPrevCapt       : string;
  HKategorie      : string;
  HKatList        : string;
  FEintrag        : string;
  FEintrage       : string;
  LKategorie      : string;
  FKategorie      : string;
  FKategorien     : string;

  { Texte, die bei Exceptions benutzt werden }
  EInvalidSaveGame: string;
  ENoMoney        : string;
  ENoMoneyToTank  : string;
  ENoAlphatron    : string;
  ENoRoom         : string;
  ENoLabRoom      : string;
  EInvalidBasis   : string;
  ENoWerRoom      : string;
  ENoHangar       : string;
  ENoTrainNeed    : string;
  ENoLagerRoom    : string;
  ENoLagerToProd  : string;
  EProdCancel     : string;
  ENoRoomAtStart  : string;
  ENoAliensInSet  : string;
  ENoUFOsInSet    : string;
  ENoAliensInSave : string;
  ENoBenzin       : string;
  ENoMotor        : string;
  ENoShield       : string;
  ENoExtension    : string;
  ENoWaffe        : string;
  ETrainCancel    : string;
  ENoItems        : string;
  ENotItems       : string;
  ENoItemsToIns   : string;
  ENoRoomToDel    : string;
  ENoRoomToIns    : string;
  EItemInProd     : string;
  EEndItemInProd  : string;
  ENoConstruction : string;
  ENoUpgrade      : string;
  EInvPlayer      : string;
  EInvGameName    : string;
  EGameSetNotFound: string;
  EWrongGameSet   : string;
  SFileNotFound   : string;
  SInvFileSize    : string;
  SNoPlayer       : string;
  SInvSaveGame    : string;
  EOnlyOneShieldAllowed: string;


  { Hints zu MessageBox Objekten }
  HSaveOK         : string;
  HEditSaveGame   : string;
  HListSaveGame   : string;

  { Texte zur Beschriftung von Labels und Buttons }
  BSelector       : string;
  LFinanz         : string;
  LMessage        : string;
  LSoundSett      : string;
  LGraphicSett    : string;
  LVolumeBar      : string;
  LMVolumeBar     : string;
  LNoSound        : string;
  LNoMusic        : string;
  LPanBar         : string;
  LMiddle         : string;
  LVerwaltung     : string;
  LInfo           : string;
  LInfoFrame      : string;
  LMaxMessages    : string;
  LMonthStat      : string;
  LPunkteStat     : string;
  LYourSold       : string;
  LYourForsch     : string;
  LItem           : string;
  LEinrichtung    : string;
  LProduktion     : string;
  LRaumschiff     : string;
  LTechnologie    : string;
  LUFOs           : string;
  LAliens         : string;
  LItems          : string;
  LYourTechnik    : string;
  LArbeit         : string;
  LStarShip       : string;
  LNoCommand      : string;
  LTraining       : string;
  LKranken        : string;
  LNoProjekt      : string;
  LProjekte       : string;
  LNoWeapon       : string;
  LCount          : string;
  LUnbegrentzt    : string;
  LYes            : string;
  LNo             : string;
  LAuftrag        : string;
  LBasis          : string;
  LTrainLabel     : string;
  LSonstiges      : string;
  LNewItems       : string;
  LConstruction   : string;
  LNewProjekts    : string;
  LWeek           : string;

  { Labels in der Monatsbilanz }
  LVorMonth       : string;
  LNachMonth      : string;
  LGewinne        : string;
  LVerluste       : string;
  LBilanz         : string;
  LShortBilanz    : string;
  LVerlust        : string;
  LGewinn         : string;
  LSolVer         : string;
  LSolEnt         : string;
  LSolKauf        : string;
  LSolGehalt      : string;
  LForVer         : string;
  LForEnt         : string;
  LForKauf        : string;
  LForGehalt      : string;
  LHerVer         : string;
  LHerEnt         : string;
  LHerKauf        : string;
  LHerGehalt      : string;
  LAusVer         : string;
  LAusKauf        : string;
  LEinGewin       : string;
  LBasBau         : string;
  LRauBau         : string;
  LForGewin       : string;
  LBudget         : string;
  LSpeGewin       : string;
  LTrainKost      : string;
  LSchadKost      : string;
  LTreibKost      : string;
  LRauVerkauf     : string;

  { Labels in der Punktebilanz }
  LPForsch        : string;
  LPUFOWeg        : string;
  LPUFOAbge       : string;
  LPSchiffAbge    : string;
  LPUpgrade       : string;
  LPSGetoetet     : string;

  { Labels Basisinformationen }
  LBWohnRaum      : string;
  LBLagerRaum     : string;
  LBLaborRaum     : string;
  LBWerkRaum      : string;
  FAnzeige        : string;
  FFAnzeige       : string;
  FIntFahig       : string;
  LBMonthKost     : string;
  LBBasisBau      : string;
  LBBasisKost     : string;
  LCGesamt        : string;
  LCBelegt        : string;
  LCFrei          : string;
  LCAuslastung    : string;
  LBasisInfos     : string;
  LWissenschaf    : string;
  LEinrichtungen  : string;
  LMiningAlphatron: string;

  { Labels Auftr�ge }
  LAAuftrag       : string;
  LAAuftrage      : string;
  LAKauf          : string;
  LAVerkauf       : string;
  LACAngebot      : string;
  LAAngebot       : string;
  LAAngebote      : string;
  FCount          : string;
  LACancel        : string;
  LAAccept        : string;
  LARounds        : string;
  LARound         : string;

  { Buttons Allgemein }
  BCancelWA       : string;
  BOKWA           : string;
  BCancel         : string;
  BOK             : string;
  BClose          : string;
  BCloseWA        : string;
  BBuy            : string;
  BYes            : string;
  BNo             : string;
  BSell           : string;
  BRecycle        : string;
  BHire           : string;
  BFire           : string;
  BKategorie      : string;

  { Schwierigkeitsgrade }
  SVeryEasy       : string;
  SEasy           : string;
  SNormal         : string;
  SDifficult      : string;
  SVeryDifficult  : string;

  { Buttons Soldaten }
  BSTraining      : string;
  BSFree          : string;
  BSAllTraining   : string;
  BSAllFree       : string;
  BSRename        : string;

  { Text auf der LaborSeite }
  HUpgrade        : string;
  BLZuweisen      : string;
  BLZuweisenAll   : string;
  BLZuweisenFree  : string;
  BLAusbilden     : string;
  BLFree          : string;
  BLAusbildenAll  : string;
  BLFreeAll       : string;
  BLAusbildenFree : string;
  BUpgrade        : string;
  FLevel          : string;
  LUpgrade        : string;

  { Buttons Werkstatt }
  BWNewProjekt    : string;
  BWStart         : string;
  BWChange        : string;
  BBConstruction  : string;
  HBConstruction  : string;
  MBConstruction  : string;
  CBConstruction  : string;

  { FormatierungsStrings }
  FPercent          : string;
  FFloat0Percent    : string;
  FFloatPercent     : string;
  FDay              : string;
  FDays             : string;
  FInteger          : string;
  FIntegerx         : string;
  FPoints           : string;
  FLongDate         : string;
  FShortDate        : string;
  FUFOName          : string;
  FFloat            : string;
  FMetres           : string;
  FKMetres          : string;
  FDezimal1         : string;
  FDezimal2         : string;
  FSoldaten         : string;
  FSoldat           : string;
  FForsch           : string;
  FTechniker        : string;
  FAlphatron        : string;
  FFaehig           : string;
  FShootFreqent     : string;
  FStunden          : string;
  FHours            : string;
  FProjekt          : string;
  FProjekte         : string;
  FWert             : string;
  FItems            : string;
  FLager            : string;
  FLiter            : string;
  FLiterperkm       : string;
  FMSeconds         : string;
  FFloatMSeconds    : string;
  FCredits          : string;
  FQuarringAlphatron: string;
  FKauf             : string;
  FVerKauf          : string;
  FWNeededAlphatron : string;

{ Allgemeine Texte }
  IVersion        : string;
  ICopyRight      : string;
  HCancelButton   : string;
  HOKButton       : string;
  HCloseButton    : string;

{ Texte in den Einstellungen }
  CBSpend         : string;
  CBAngebot       : string;
  CBTrainEnd      : string;
  CBTrainCancel   : string;
  CBForschEnd     : string;
  CBProdCancel    : string;
  CBProdEnd       : string;
  CBPension       : string;
  CBBuildEnd      : string;
  CBSchiffEnd     : string;
  CBOrgan         : string;
  CBUFOs          : string;
  CBLaden         : string;
  CBMangel        : string;
  CBNewItems      : string;
  CBMission       : string;
  CBFastDelete    : string;
  CBAnimation     : string;
  CBTransEnergy   : string;
  CBAlpha         : string;
  CBAlphaControls : string;
  CBFading        : string;
  CBAnimatedMaus  : string;
  CBMouseShadow   : string;
  CBStarField     : string;
  HNOrgan         : string;
  HNMonthBilanz   : string;
  HNSpend         : string;
  HNAngebot       : string;
  HNTrainEnd      : string;
  HNTrainCancel   : string;
  HNForschEnd     : string;
  HNProdEnd       : string;
  HNProdCancel    : string;
  HNPension       : string;
  HNAnimation     : string;
  HNFastDelete    : string;
  HNBuildEnd      : string;
  HNUFOs          : string;
  HNSchiffEnd     : string;
  HNMission       : string;
  HNLaden         : string;
  HNMangel        : string;
  HNNewItems      : string;
  HNAlpha         : string;
  HNAlphaControls : string;
  HNFading        : string;
  HNAnimatedMaus  : string;
  HNMouseShadow   : string;
  HNStarField     : string;
  HNTransEnergy   : string;
  HVolumeBar      : string;
  HMVolumeBar     : string;
  HMaxMessage     : string;
  HDeactAll       : string;
  HActAll         : string;
  BDeactAll       : string;
  BActAll         : string;
  CAllgemein      : string;
  CBAlwaysFast    : string;
  HAlwaysFast     : string;

{ Texte in ItemInfo }
  FLAuslastung    : string;
  FAuslastung     : string;
  BInfoText       : string;
  HInfoText       : string;
  BInfoDaten      : string;
  HInfoDaten      : string;
  IIYourSoldat    : string;
  IIMarktSoldat   : string;
  IIGesundheit    : string;
  IIPsiAn         : string;
  IIPsiAb         : string;
  IITimeUnits     : string;
  IIHandStatus    : string;
  IILeftHand      : string;
  IIRightHand     : string;
  IITreff         : string;
  IIKraft         : string;
  IITag           : string;
  IIEinsatz       : string;
  IITreffer       : string;
  IIWert          : string;
  IIKaufPreis     : string;
  IAMunition      : string;
  IAWaffe         : string;
  IAArt           : string;
  IAMine          : string;
  IAGranate       : string;
  IAPanzerung     : string;
  IAMotor         : string;
  IASensor        : string;
  IAShield        : string;
  IAExtension     : string;
  IIOneHandWeapon : string;
  IITwoHandWeapon : string;
  IARWaffe        : string;
  IARMunition     : string;
  IARaketen       : string;
  IAProjektil     : string;
  IANoConstruct   : string;
  IAMadeIn        : string;
  IALaser         : string;
  IAChemisch      : string;
  IAVerKaufPreis  : string;
  IAProdKost      : string;
  IATStunden      : string;
  IACount         : string;
  IALagerVer      : string;
  IAFrequenz      : string;
  IAGewicht       : string;
  IAStrengthW     : string;
  IARange         : string;
  IAStrengthM     : string;
  IASchuss        : string;
  IAStrengthMine  : string;
  IADuesen        : string;
  IAAbhMunition   : string;
  IAAbhWaffe      : string;
  IAKapazitat     : string;
  IAInhalt        : string;
  IAVerbrauch     : string;
  IASensorRange   : string;
  IAWeapon        : string;
  IAPanzer        : string;
  IANachLade      : string;
  IAVerfolg       : string;
  IAShieldPoints  : string;
  IADefend        : string;
  IAAufLade       : string;
  IAStatus        : string;
  IAHerstellung   : string;
  IACollection    : string;
  IANeedAlphatron : string;
  IAReserved      : string;
  IATechniker     : string;
  IAFaehig        : string;
  IANoch          : string;
  IAWaffType      : string;
  IAExtendsSlots  : string;
  IABauZeit       : string;
  IASmallSchiff   : string;
  IAAbwehr        : string;
  IAExtensions    : string;
  IAAlphatron     : string;
  IABKostperDay   : string;
  IAKostperMonth  : string;
  IAMitglied      : string;
  IAVertrauen     : string;
  IAAlliance      : string;
  IAFriendly      : string;
  IANeutral       : string;
  IAUnFriendly    : string;
  IAEnemy         : string;
  IAWaffenZ       : string;
  IAZelle         : string;
  IAHitPoints     : string;
  IASoldaten      : string;
  IABenzin        : string;
  IAPrDefend      : string;
  IARaDefend      : string;
  IALaDefend      : string;
  IAMinBes        : string;
  IAZusBes        : string;
  IAZiel          : string;
  IALagerItem     : string;
  IAAngriff       : string;
  IAPSIAn         : string;
  IAPSIAb         : string;
  IAZeiteinheiten : string;
  IATreffSicherh  : string;
  IALandSpezifik  : string;
  LNoMuntion      : string;

{ Namen von Erweiterungen }
  LExtSensor      : string;

{ Strings bei Upgrades }
  IAForschTime    : string;
  FULevel         : string;
  FUFloat         : string;
  FUFloat1        : string;
  FUInteger       : string;
  FUString        : string;
  FUMSeconds      : string;
  FUMetres        : string;
  FULiter         : string;
  FULiterperkm    : string;
  FUPercent       : string;

{ Texte im InfoFrame }
  LAlphatron      : string;
  LKapital        : string;
  FRoom           : string;
  FLagerRoom      : string;
  FICredits       : string;
  FIAlphatron     : string;
  CChooseBase     : string;
  AInfoFrame      : Array[ifLager..ifHangar,boolean] of String;
  AInfoShortFrame : Array[ifLager..ifHangar] of String;
  AInfoFormat     : Array[ifLager..ifHangar] of String;

{ �berarbeitung Texte im Hauptmen� }
  HLoadGame       : string;
  HDelete         : string;
  HHighScore      : string;
  HSinglePlayer   : string;
  HKampfSimu      : string;
  HMultiPlayer    : string;
  HNewGame        : string;
  HCredits        : string;
  HBack           : string;
  HSettings       : string;
  HPlayerLoad     : string;
  HPlayerDelete   : string;
  HSelectGame     : string;
  HLoadButton     : string;
  HDeleteGame     : string;
  HDeletePlayer   : string;
  HKeyBoard       : string;
  HMissionsTest   : string;
  HMissionStart   : string;
  MTestModus      : string;
  MNoMultiplayer  : string;
  MOverwriteShip  : string;
  CChooseShip     : string;
  BSinglePlayer   : string;
  BMultiPlayer    : string;
  BLoadGame       : string;
  BLoadGameS      : string;
  BDeleteGameS    : string;
  BDeleteGame     : string;
  BNewGame        : string;
  BKeyBoard       : string;
  BKampfSimu      : string;
  BBack           : string;
  BLoadLastGame   : string;
  BMissionsTest   : string;
  BMissionStart   : string;
  BHighScore      : string;
  BSettings       : string;
  BCredits        : string;
  BExitGame       : string;
  BDeletePlayer   : string;
  LPlayers        : string;
  LSkripte        : string;
  FMission        : string;
  FMissions       : string;
  LHighscore      : string;
  FPlayers        : string;
  FSaveGame       : string;
  FSaveGames      : string;
  LSaveGame       : string;
  LSinglePlayer   : string;
  LMultiPlayer    : string;
  CSaveGame       : string;
  FDate           : string;
  LShadow         : string;
  LShadowLow      : string;
  LShadowMedium   : string;
  LShadowHigh     : string;

  { Texte f�r Tipps und Tricks }
  BLastTipp       : string;
  BNextTipp       : string;
  HLastTipp       : string;
  HNextTipp       : string;
  LTipp           : string;


{ Texte Neues Spiel }
  HStartGame      : string;
  HNameEdit       : string;
  HLevelBox       : string;
  HForschTimeEdit : string;
  HProdTimeEdit   : string;
  HAlienDay       : string;
  HUFODay         : string;
  HSoldatCount    : string;
  HForschCount    : string;
  HTechnikCount   : string;
  HPlayerSelect   : string;
  HWohnRaumBel    : string;
  HGameSet        : string;
  HInfoGameSet    : string;
  HChooseGameSet  : string;
  HCancelSS       : string;
  BStartGameWA    : string;
  LPlayerEdit     : string;
  LHersEdit       : string;
  LForsEdit       : string;
  LStartKaptial   : string;
  LAlienDayEdit   : string;
  LUFODayEdit     : string;
  LLevelEdit      : string;
  LGameSet        : string;
  LStartSettings  : string;
  CChooseGameSet  : string;
  EInvalidPos     : string;
  CBasisPos       : string;

{ Texte, die in Nachrichtenboxen angezeigt werden }
  CQuestion       : string;
  CInformation    : string;
  CHinweis        : string;
  CError          : string;
  CUnknownError   : string;
  MError          : string;
  CDeleteGame     : string;
  CLoadGame       : string;
  CGameEnd        : string;
  CCancelAction   : string;
  CChangeProjekt  : string;
  CDeleteMessage  : string;
  CSettings       : string;
  CRename         : string;
  MExitQuestion   : string;
  MDeletePlayer   : string;
  MNewPlayer      : string;
  MDeleteGame     : string;
  MNoLagForProd   : string;
  MNoTabForProd   : string;
  MProduktionEnd  : string;
  MOverwrite      : string;
  MGameNotSaved   : string;
  MGameNotLoaded  : string;
  MRecreate       : string;
  MSpendKap       : string;
  MSpendTab       : string;
  MWelcome        : string;
  MNewAngebot     : string;
  MForschEnd      : string;
  MUpgradeEnd     : string;
  MTrainEnd       : string;
  MSellAuftrag    : string;
  MBuyAuftrag     : string;
  MCHersStart     : string;
  MHersStart      : string;
  MNoDeaktivate   : string;
  MBauEnd         : string;
  MAbsturz        : string;
  MRueckkehr      : string;
  MNoWeapons      : string;
  MNoSoldaten     : string;

{ Texte in der Kommandozentrale }
  HSaveGame       : string;
  HOptions        : string;
  HSoldaten       : string;
  HMainMenu       : string;
  HNextRound      : string;
  HMonthStat      : string;
  HPunkteStat     : string;
  HForschung      : string;
  HLager          : string;
  HMessages       : string;
  HUfoPaedie      : string;
  HWerkstatt      : string;
  HBaseInfos      : string;
  HArbeitsmarkt   : string;
  HBasis          : string;
  HOrganisation   : string;
  HSchiffe        : string;
  HUFOS           : string;
  HFHilfe         : string;
  HVerwaltung     : string;
  HTaktik         : string;
  HSlow           : string;
  HNormal         : string;
  HFast           : string;
  HVeryFast       : string;
  HPause          : string;
  LPoints         : string;
  LDate           : string;
  BMainMenu       : string;
  BVerwaltung     : string;
  BTaktik         : string;
  BSaveGame       : string;
  BLongSave       : string;
  BOptions        : string;
  BNextRound      : string;
  BMonthStat      : string;
  BFHilfe         : string;
  BPunkteStat     : string;
  BSoldaten       : string;
  BSchiffe        : string;
  BUFOs           : string;
  BBasisInfo      : string;
  BLabor          : string;
  BWerkStatt      : string;
  BLager          : string;
  BUfoPadie       : string;
  BAuftrag        : string;
  BVerMarkt       : string;
  BVerBasis       : string;
  BVerOrgan       : string;

{ Texte im Arbeitsmarkt }
  BTechniker      : string;
  BForscher       : string;
  LZivilist       : string;
  HAMTechniker    : string;
  HAMForscher     : string;
  HAMTechList     : string;
  HAMForsList     : string;
  HAMKTechList    : string;
  HAMKForsList    : string;
  FAMZivilist     : string;
  FAMZivilisten   : string;
  HAMBuyZivi      : string;
  HAMSellTech     : string;
  HAMSellFors     : string;
  HRenameTech     : string;
  HRenameFors     : string;

{ Texte in der Basisverwaltung }
  ETooManyBases   : string;
  ENoMoneyBasis   : string;
  HEinrichtungen  : string;
  HBauEinrichtung : string;
  HBuild          : string;
  HAktiv          : string;
  HDeaktiv        : string;
  HCancelBuild    : string;
  HBauAbbrechen   : string;
  HEinrichtInfo   : string;
  HNewBasis       : string;
  HRename         : string;
  CBauAbbrechen   : string;
  LYourBasis      : string;
  LEinricht       : string;
  FEinrichtung    : string;
  FEinrichtungen  : string;
  FBauZeit        : string;
  LAktiviert      : string;
  LDeaktiviert    : string;
  CUEinricht      : string;
  BAktiv          : string;
  BDeaktiv        : string;
  BNewBasis       : string;
  BBuild          : string;

{ Texte in der Organisationsverwaltung }
  HOrganInfo      : string;
  HOrganList      : string;
  HAllOrgan       : string;
  HAllianzOrgan   : string;
  HFriendlyOrgan  : string;
  HNeutralOrgan   : string;
  HUnFriendlyOrgan: string;
  HEnemyOrgan     : string;
  HSortName       : string;
  HSortKapi       : string;
  HSortMitg       : string;
  HSortStat       : string;
  LOrgan          : string;
  FOrgan          : string;
  FOrgans         : string;
  IIOrganisation  : string;
  LAllOrgan       : string;
  LAllianzOrgan   : string;
  LFriendlyOrgan  : string;
  LNeutralOrgan   : string;
  LUnFriendlyOrgan: string;
  LEnemyOrgan     : string;
  LsortName       : string;
  LSortKapi       : string;
  LSortMitg       : string;
  LSortStat       : string;

{ Konstanten VerRaumschiffe }
  HBuildSchiff    : string;
  HTypListe       : string;
  HRaumschiffInfo : string;
  HRCancelBuild   : string;
  HRenameSchiff   : string;
  HSellSchiff     : string;
  HSchiffList     : string;
  HSetting        : string;
  MSellSchiff     : string;
  MNoLagerRoom    : string;
  CSellSchiff     : string;
  BSetting        : string;
  LModels         : string;
  FTyp            : string;
  FTyps           : string;
  FSchiff         : string;
  FSchiffe        : string;
  LModel          : string;
  LUFOModel       : string;
  LRaumschiffe    : string;
  LRKomponent     : string;

{ Konstanten EinsatzListe }
  FEinsatzName    : string;
  
{ Konstanten VerRaumschiff }
  CBestatigung    : string;
  ESoldatEinsatz  : string;
  MKompAusbauen   : string;
  MTMotor         : string;
  MTShield        : string;
  MTWaffe         : string;
  MTSchiff        : string;
  MTZelle         : string;
  HWaffButt       : string;
  HAusButt        : string;
  HMotorButt      : string;
  HSolButt        : string;
  HInfoButt       : string;
  HSMotor         : string;
  HSShield        : string;
  HMotoren        : string;
  HWaffen         : string;
  HRSoldaten      : string;
  HNextSchiff     : string;
  HPrevSchiff     : string;
  HWZellen        : string;
  HClickHere      : string;
  HMotorAusbauen  : string;
  HExtendAusbauen : string;
  HShieldAusbauen : string;
  HWaffeAusbauen  : string;
  HMotorEinbauen  : string;
  HExtEinbauen    : string;
  HWaffeEinbauen  : string;
  HShieldEinbauen : string;
  HWrongPlace     : string;
  HSoldatConfig   : string;
  HAusruestung    : string;
  BInfos          : string;
  BWaffen         : string;
  BMotor          : string;
  LNot            : string;
  LPlatzFor       : string;
  LVerfuegbar     : string;
  BAusrust        : string;
  LWaffen         : string;
  FWaffe          : string;
  FWaffen         : string;
  LMotor          : string;
  FMotor          : string;
  FMotoren        : string;
  BSMotor         : string;
  BWZellen        : string;
  LWZelle         : string;
  LAusruest       : string;
  LAusruests      : string;
  LFree           : string;
  LPlatzBelegt    : string;
  FInSchiffRight  : string;
  FInLager        : string;


{ KD4SaveGame Konstanten }
  HPension        : string;
  SSoldat         : string;
  SForscher       : string;
  STechniker      : string;

{ Organisationsstatus�nderung }
  OAChangeStat    : string;
  OASteigung      : string;
  OASenkung       : string;

{ Raumschiffkomponente }
  STBereit        : string;
  STNoMotor       : string;
  STNoBenzin      : string;
  STEinsatz       : string;
  STFlyEinsatz    : string;
  STFlyToPos      : string;
  STBackToBasis   : string;
  STNoWeapon      : string;
  STHuntUFO       : string;
  STEskort        : string;
  STEinkauf       : string;
  STVerkauf       : string;
  STBauDay        : string;
  STBauDays       : string;
  MBauBeendet     : string;
  EZelleFrei      : string;
  ENoMunition     : string;
  ENotImLager     : string;
  FMotorInfo      : string;
  FSchuss         : string;
  FWaffenZelle    : string;
  FWaffenZellen   : string;
  ECantChangeBase : string;
  MLieferung      : string;
  MVerkauf        : string;
  MDestruction    : string;
  MSolDies        : string;
  MNoRoomForLie   : string;
  PreTransporter  : string;

  { Einsatzseite }
  LEntdecktam      : string;
  LBesatzung       : string;
  LSeeingAliens    : string;
  BJagd            : string;
  BBackToBasis     : string;
  BUFOAbfangen     : string;
  BEinsatzAbfangen : string;
  LEUFO            : string;
  FUFO             : string;
  FUFOs            : string;
  LSchaden         : string;
  HBackToBasis     : string;
  HJagd            : string;
  HUFOAbfangen     : string;
  HEinsatzAbfangen : string;
  LEntfernung      : string;
  LZeitEntdeck     : string;
  LInBasis         : string;
  LEinsatzorder    : string;
  LLadung          : string;
  MAbfangen        : string;
  CMDNoCommand     : string;
  CMDFlyToPos      : string;
  CMDEskorte       : string;
  CMDEskortiert    : string;
  CMDUFOdestroy    : string;
  CMDFlyEinsatz    : string;
  CMDBacktoBasis   : string;

  { Konstanten UFO Liste }
  MUFOInSicht     : string;
  MFirstSichtung  : string;
  MOVer           : string;
  MUFOWeg         : string;
  LVerfolgungskurs: string;

  { UFOKampf }
  HCancelKampf    : string;
  HStartKampf     : string;
  HEchtZeit       : string;
  HRunden         : string;
  HMissInfo       : string;
  HAusruesten     : string;
  HAusruestenEin  : string;
  BStartKampf     : string;
  BEchtZeit       : string;
  BMissInfo       : string;
  BPause          : string;
  BRoundEnd       : string;
  BRunden         : string;
  BAusruesten     : string;
  LGewonnen       : string;
  LVerloren       : string;
  LStatistik      : string;
  LGeschossen     : string;
  LTreffer        : string;
  LVerfehlt       : string;
  LRGesamt        : string;
  LEscToEnd       : string;
  CTastInfo       : string;
  HStart3         : string;
  HStart2         : string;
  HStart1         : string;
  MTastInfo       : string;
  LHelpInfo       : string;

  { Hilfe-Seite }
  LHelpEntrys     : string;

  { InfoFinanzHilfe }
  LFHilfe         : string;
  LKalkulation    : string;
  LFPunkte        : string;
  FMultiplikator  : string;
  LZFassung       : string;

  { Pager }
  LMessages       : string;
  FMessage        : string;
  FMessages       : string;

  { Kampfsimulator }
  HSchiffName     : string;
  HUFOName        : string;
  HWeaponName     : string;
  HSchiffModell   : string;
  HShieldModel    : string;
  HMotorModel     : string;
  HWeaponModel    : string;
  HSSchiffList    : string;
  HSSchieldList   : string;
  HSMotorList     : string;
  HSUFOList       : string;
  HSWaffenList    : string;
  HHitPoints      : string;
  HWaffenBox      : string;
  HUFOHitPoints   : string;
  HUFOShieldPoints: string;
  HUFOAttack      : string;
  HUFOTreff       : string;
  HWaffenzellen   : string;
  HAttackEdit     : string;
  HSchussEdit     : string;
  HTypeEdit       : string;
  HLadeEdit       : string;
  HFolgeEdit      : string;
  HShieldPoints   : string;
  HAbwehr         : string;
  HPrDefense      : string;
  HRaDefense      : string;
  HLaDefense      : string;
  HLadeZeit       : string;
  HUFOLadeZeit    : string;
  HPPSEdit        : string;
  HWPPSEdit       : string;
  HUFOPPSEdit     : string;
  HDuesenEdit     : string;
  HStartButton    : string;
  HLoad           : string;
  HSave           : string;
  MNoSchiffsSaved : string;
  HChooseSchiffSet: string;
  HChooseUFOSet   : string;
  HUserDefine     : string;
  CSaveName       : string;
  LLoad           : string;
  LSave           : string;
  LYourSchiff     : string;
  LEnemy          : string;
  LSchiffName     : string;
  LNoStandard     : string;
  LSNoUFO         : string;
  LShield         : string;
  LManoevrier     : string;
  LPPS            : string;
  LUFO            : string;
  LSMotor         : string;
  LSchields       : string;
  LSWZelle        : string;
  BUFOEdit        : string;
  BUFOUserButton  : string;
  BUserButton     : string;
  FHitPoints      : string;
  FSchield        : string;
  FSchields       : string;
  FSPoints        : string;
  FDefaultName    : string;
  FSGeschwindig   : string;
  FStrength       : string;
  FSchuesse       : string;
  FWName          : string;
  FSAbwehr        : string;

  { Taktik Screen }
  ILandTime       : string;
  LLand           : string;
  LWasser         : string;
  LSichtungen     : string;
  LVerSchiffe     : string;
  LBasisName      : string;
  LNebenBasis     : string;
  CChooseObjekt   : string;
  HGoToUFOPaedie  : string;
  MTransportZiel  : string;

  { Tastaturoptionen }
  CKeyConfig      : string;
  CUFOKampf       : string;
  CShootzelle     : string;
  CRotateLeft     : string;
  CRotateRight    : string;
  CGibSchub       : string;
  CMoveLeft       : string;
  CMoveRight      : string;
  CBodenEinsatz   : string;

  { Tastatur Labels }
  KLEnter         : string;
  KLBack          : string;
  KLEnd           : string;
  KLHome          : string;
  KLInsert        : string;
  KLDelete        : string;
  KLSpace         : string;
  KLPrior         : string;
  KLNext          : string;
  KLEscape        : string;
  KLLeft          : string;
  KLRight         : string;
  KLUp            : string;
  KLDown          : string;
  KLAfterT        : string;
  KLBeforeX       : string;

  { Punktestatus }
  PSAlliance      : string;
  PSFriendly      : string;
  PSNeutral       : string;
  PSUnFriendly    : string;
  PSEnemy         : string;

  { Texte in DXSchiffItems }
  HSIEntladen     : string;
  HSIBeladen      : string;

  { Hinweistexte im Hauptformular }
  EMResNotFound   : string;
  MKD4NewInstall  : string;
  EUnknownError   : string;
  EDirectXError   : string;
  MInstallDirectX : string;
  EOpenMusik      : string;
  MNoAcessonCD    : string;
  ESendEMail      : string;
  MCDcorrekt      : string;


  { Einsatzbeschreibung }
  ClearMission    : string;
  AlienCount      : string;
  LEinsatzTrupp   : string;
  LVernichtBastard: string;
  LTragLast       : string;

  { Soldaten ausr�sten }
  LLeftHand       : string;
  LLeftHandMuni   : string;
  LRightHand      : string;
  LRightHandMuni  : string;
  LPanzerung      : string;
  MNoItemInSchiff : string;
  MSoldatVoll     : string;
  FInSchiffLeft   : string;
  FInRucksack     : string;

  { Bodeneinsatz }
  EPathNotFound   : string;
  ENoTimeUnits    : string;

  // Neue Texte 21.09.2003
    // Auftragsannahme
  ST0309210001    : string;
  ST0309210002    : string;
  ST0309210003    : string;
  ST0309210004    : string;
  ST0309210005    : string;
  ST0309210006    : string;
  ST0309210007    : string;
  ST0309210008    : string;
  ST0309210009    : string;

    // Basisliste
  ST0309210010    : string;
  ST0309210011    : string;

    // Bodeneinsatz
  ST0309210012    : string;
  ST0309210013    : string;
  ST0309210014    : string;
  ST0309210015    : string;
  ST0309210016    : string;

    // EinsatzPager
  ST0309210017    : string;
  ST0309210018    : string;
  ST0309210019    : string;
  ST0309210020    : string;
  ST0309210021    : string;
  ST0309210022    : string;

    // GameMessage
  ST0309210023    : string;
  ST0309210024    : string;
  ST0309210025    : string;

    // DXInfoFrame
  ST0309210026    : string;
  ST0309210027    : string;
  ST0309210028    : string;

  // Neue Texte 22.09.2003
    // DXISOEngine
  ST0309220001    : string;
  ST0309220002    : string;
  ST0309220003    : string;
  ST0309220004    : string;
  ST0309220005    : string;
  ST0309220006    : string;
  ST0309220007    : string;
  ST0309220008    : string;

    // DXItemInfo
  ST0309220009    : string;
  ST0309220010    : string;
  ST0309220011    : string;
  ST0309220012    : string;
  ST0309220013    : string;
  ST0309220014    : string;

    // DXPointStatus
  ST0309220015    : string;
  ST0309220016    : string;
  ST0309220017    : string;
  ST0309220018    : string;
  ST0309220019    : string;
  ST0309220020    : string;
  ST0309220021    : string;

    // DXSchiffConfig
  ST0309220022    : string;
  ST0309220023    : string;
  ST0309220024    : string;
  ST0309220025    : string;

    // DXSoldatChooser
  ST0309220026    : string;
  ST0309220027    : string;
  ST0309220028    : string;
  ST0309220029    : string;

    // DXSoldatConfig
  ST0309220030    : string;
  ST0309220031    : string;
  ST0309220032    : string;
  ST0309220033    : string;
  ST0309220034    : string;
  ST0309220035    : string;
  ST0309220036    : string;
  ST0309220037    : string;
  ST0309220038    : string;
  ST0309220039    : string;
  ST0309220040    : string;
  ST0309220041    : string;
  ST0309220042    : string;
  ST0309220043    : string;
  ST0309220044    : string;
  ST0309220045    : string;
  ST0309220046    : string;
  ST0309220047    : string;
  ST0309220048    : string;

    // DXTakticScreen
  ST0309220049    : string;
  ST0309220050    : string;
  ST0309220051    : string;
  ST0309220052    : string;

    // DXUIBodenEinsatz
  ST0309220053    : string;
  ST0309220054    : string;
  ST0309220055    : string;
  ST0309220056    : string;
  ST0309220057    : string;
  ST0309220058    : string;
  ST0309220059    : string;
  ST0309220060    : string;
  ST0309220061    : string;
  ST0309220062    : string;
  ST0309220063    : string;
  ST0309220064    : string;
  ST0309220065    : string;
  ST0309220066    : string;
  ST0309220067    : string;
  ST0309220068    : string;
  ST0309220069    : string;
  ST0309220070    : string;
  ST0309220071    : string;
  ST0309220072    : string;
  ST0309220073    : string;
  ST0309220074    : string;
  ST0309220075    : string;
  ST0309220076    : string;
  ST0309220077    : string;
  ST0309220078    : string;
  ST0309220079    : string;
  ST0309220080    : string;
  ST0309220081    : string;
  ST0309220082    : string;
  ST0309220083    : string;
  ST0309220084    : string;
  ST0309220085    : string;
  ST0309220086    : string;
  ST0309220087    : string;
  ST0309220088    : string;
  ST0309220089    : string;
  ST0309220090    : string;
  ST0309220091    : string;
  ST0309220092    : string;
  ST0309220093    : string;
  ST0309220094    : string;

  // Neue Texte 23.09.2003
    // DXISOEngine
  ST0309230001    : string;
  ST0309230002    : string;
  ST0309230003    : string;
  ST0309230004    : string;
  ST0309230005    : string;
  ST0309230006    : string;
  ST0309230007    : string;
  ST0309230008    : string;
  ST0309230009    : string;
  ST0309230010    : string;
  ST0309230011    : string;
  ST0309230012    : string;
  ST0309230013    : string;
  ST0309230014    : string;

    // EinsatzOptionsDialog
  ST0309230015    : string;
  ST0309230016    : string;
  ST0309230017    : string;
  ST0309230018    : string;
  ST0309230019    : string;
  ST0309230020    : string;
  ST0309230021    : string;
  ST0309230022    : string;
  ST0309230023    : string;
  ST0309230024    : string;
  ST0309230025    : string;

    // ForschList
  ST0309230026    : string;
  ST0309230027    : string;
  ST0309230028    : string;
  ST0309230029    : string;
  ST0309230030    : string;
  ST0309230031    : string;

  // Neue Texte 26.09.2003
    // GameFigure
  ST0309260001    : string;
  ST0309260002    : string;
  ST0309260003    : string;
  ST0309260004    : string;
  ST0309260005    : string;
  ST0309260006    : string;
  ST0309260007    : string;
  ST0309260008    : string;

    // InfoUFOPaedie
  ST0309260009    : string;

    // ISOObjects
  ST0309260010    : string;

    // KD4SaveGame
  ST0309260011    : string;
  ST0309260012    : string;
  ST0309260013    : string;
  ST0309260014    : string;
  ST0309260015    : string;
  ST0309260016    : string;

    // KeyBoardOptions
  ST0309260017    : string;
  ST0309260018    : string;
  ST0309260019    : string;
  ST0309260020    : string;
  ST0309260021    : string;
  ST0309260022    : string;
  ST0309260023    : string;
  ST0309260024    : string;
  ST0309260025    : string;
  ST0309260026    : string;
  ST0309260027    : string;
  ST0309260028    : string;
  ST0309260029    : string;

    // LagerListe
  ST0309260030    : string;
  ST0309260031    : string;
  ST0309260032    : string;
  ST0309260033    : string;
  ST0309260034    : string;

    // MainPage
  ST0309260035    : string;
  ST0309260036    : string;
  ST0309260037    : string;
  ST0309260038    : string;
  ST0309260039    : string;
  ST0309260040    : string;
  ST0309260041    : string;
  ST0309260042    : string;
  ST0309260043    : string;
  ST0309260044    : string;
  ST0309260045    : string;
  ST0309260046    : string;

  // Neue Texte 27.09.2003
    // MissionList
  ST0309270001    : string;
  ST0309270002    : string;
  ST0309270003    : string;
  ST0309270004    : string;
  ST0309270005    : string;

    // NewGame
  ST0309270006    : string;

    // OptionsPage
  ST0309270007    : string;
  ST0309270008    : string;

    // OrganisationsList
  ST0309270009    : string;
  ST0309270010    : string;

    // RaumschiffList
  ST0309270011    : string;
  ST0309270012    : string;
  ST0309270013    : string;
  ST0309270014    : string;
  ST0309270015    : string;
  ST0309270016    : string;

    // VerArbeitsmarkt
  ST0309270017    : string;
  ST0309270018    : string;
  ST0309270019    : string;

    // VerAuftrag
  ST0309270020    : string;
  ST0309270021    : string;
  ST0309270022    : string;

    // VerNewProjekt
  ST0309270023    : string;

    // VerSoldatPage
  ST0309270024    : string;
  ST0309270025    : string;
  ST0309270026    : string;

    // WatchMarktDialog
  ST0309270027    : string;
  ST0309270028    : string;
  ST0309270029    : string;
  ST0309270030    : string;
  ST0309270031    : string;
  ST0309270032    : string;
  ST0309270033    : string;
  ST0309270034    : string;
  ST0309270035    : string;
  ST0309270036    : string;
  ST0309270037    : string;
  ST0309270038    : string;
  ST0309280001    : string;
  ST0309280002    : string;
  ST0309290001    : string;
  ST0310070001    : string;
  ST0310070002    : string;
  ST0310070003    : string;
  ST0310070004    : string;
  ST0310070005    : string;
  ST0310070006    : string;
  ST0310090001    : string;
  ST0310090002    : string;
  ST0310090003    : string;
  ST0310120001    : string;
  ST0310120002    : string;
  ST0310180001    : string;
  ST0310180002    : string;
  ST0310250001    : string;
  ST0310250002    : string;
  ST0310250003    : string;
  ST0310250004    : string;
  ST0310260001    : string;
  ST0310260002    : string;
  ST0310270001    : string;
  ST0311020001    : string;
  ST0311020002    : string;
  ST0311040001    : string;
  ST0311040002    : string;
  ST0311040003    : string;
  ST0311060001    : string;
  ST0311060002    : string;
  ST0311060003    : string;
  ST0311060004    : string;
  ST0311070001    : string;
  ST0311070002    : string;
  ST0311070003    : string;
  ST0311070004    : string;
  ST0311070005    : string;
  ST0311070006    : string;
  ST0311070007    : string;
  ST0311070008    : string;
  ST0311070009    : string;
  ST0311080001    : string;
  ST0311080002    : string;
  ST0311080003    : string;
  ST0311080004    : string;
  ST0311150001    : string;
  ST0311160001    : string;
  ST0311170001    : string;
  ST0311170002    : string;
  ST0311200001    : string;
  ST0311200002    : string;
  ST0311200003    : string;
  ST0311200004    : string;
  ST0311210001    : string;
  ST0311210002    : string;
  ST0311230001    : string;
  ST0311230002    : string;
  ST0311230003    : string;
  ST0311230004    : string;
  ST0311230005    : string;
  ST0311250001    : string;
  ST0311280001    : string;
  ST0311300001    : string;
  ST0311300002    : string;
  ST0311300003    : string;
  ST0312070001    : string;
  ST0312070002    : string;
  ST0312070003    : string;
  ST0312070004    : string;
  ST0312070005    : string;
  ST0312110001    : string;
  ST0312160001    : string;
  ST0312170001    : string;
  ST0312170002    : string;
  ST0312170003    : string;
  ST0312170004    : string;
  ST0312210001    : string;
  ST0312260001    : string;

  ST0401120001    : string;
  ST0401120002    : string;
  ST0401120003    : string;
  ST0401120004    : string;
  ST0401120005    : string;
  ST0401200001    : string;
  ST0401200002    : string;
  ST0401200003    : string;
  ST0401200004    : string;
  ST0401200005    : string;
  ST0401200006    : string;
  ST0401200007    : string;
  ST0401210001    : string;
  ST0401210002    : string;
  ST0401230001    : string;
  ST0402040001    : string;
  ST0402040002    : string;
  ST0402040003    : string;
  ST0402040004    : string;
  ST0402140001    : string;
  ST0402160001    : string;
  ST0402160002    : string;
  ST0402170001    : string;
  ST0402290001    : string;
  ST0403080001    : string;
  ST0403190001    : string;
  ST0403260001    : string;
  ST0403260002    : string;
  ST0403260003    : string;
  ST0403260004    : string;
  ST0403310001    : string;

  ST0405250001    : string;

  ST0409110001    : string;
  ST0409260001    : string;
  ST0409270001    : string;
  ST0409270002    : string;
  ST0409280001    : string;
  ST0409280002    : string;

  ST0410050001    : string;
  ST0410050003    : string;
  ST0410050004    : string;
  ST0410050005    : string;
  ST0410080001    : string;
  ST0410100001    : string;
  ST0410110001    : string;
  ST0410220001    : string;
  ST0410220002    : string;
  ST0410250001    : string;
  ST0410250002    : string;
  ST0410250003    : string;
  ST0411280001    : string;
  ST0411280002    : string;
  ST0411300001    : string;
  ST0411300002    : string;
  ST0412070001    : string;
  ST0412100001    : string;
  ST0412100002    : string;
  ST0412200001    : string;
  ST0412200002    : string;
  ST0412200003    : string;
  ST0412200004    : string;
  ST0412200005    : string;
  ST0412200006    : string;
  ST0412200007    : string;
  ST0412200008    : string;
  ST0412230001    : string;
  ST0412270001    : string;
  ST0501020001    : string;
  ST0501020002    : string;
  ST0501030001    : string;
  ST0501040001    : string;
  ST0501040002    : string;
  ST0501040003    : string;
  ST0501080001    : string;
  ST0501080002    : string;
  ST0501080003    : string;
  ST0501080004    : string;
  ST0501080005    : string;
  ST0501080006    : string;
  ST0501080007    : string;
  ST0501080008    : string;
  ST0501110001    : string;
  ST0501130001    : string;
  ST0501130002    : string;
  ST0501130003    : string;
  ST0501130004    : string;
  ST0501230001    : string;
  ST0501230002    : string;
  ST0501230003    : string;
  ST0501230004    : string;
  ST0501230005    : string;
  ST0501240001    : string;
  ST0501250001    : string;
  ST0501250002    : string;
  ST0501270001    : string;                                               
  ST0501290001    : string;
  ST0501300001    : string;
  ST0501300002    : string;
  ST0501310001    : string;
  ST0501310002    : string;
  ST0502050001    : string;
  ST0502070001    : string;
  ST0502080001    : string;
  ST0502080002    : string;
  ST0502080003    : string;
  ST0502080004    : string;
  ST0502080005    : string;
  ST0502110001    : string;
  ST0502130001    : string;
  ST0502140001    : string;
  ST0502160001    : string;
  ST0502200001    : string;
  ST0502200002    : string;
  ST0502220001    : string;
  ST0502240001    : string;
  ST0503060001    : string;
  ST0503060002    : string;
  ST0503080001    : string;
  ST0503100001    : string;
  ST0503110001    : string;
  ST0503170001    : string;
  ST0503220001    : string;
  ST0503220002    : string;
  ST0503220003    : string;
  CR0503310001    : string;
  CR0504050001    : string;
  CR0504050002    : string;
  CR0504100001    : string;
  CR0504140001    : string;
  MK0504160001    : string;
  MK0504160002    : string;
  CR0504280001    : string;
  CR0504280002    : string;
  CR0504280003    : string;
  MK0505030001    : string;
  MK0505030002    : string;
  MK0505040001    : string;
  MK0505040002    : string;
  MK0505040003    : string;
  MK0505040004    : string;
  MK0505040005    : string;
  MK0505050001    : string;
  MK0505050002    : string;
  MK0506050001    : string;
  ST0505260001    : string;
  CR0505290001    : string;
  CR0506060001    : string;
  CR0506270001    : string;
  CR0507020001    : string;
  CR0507210001    : string;
{MARKE: Ende normale Texte}

var
  Country         : Array[1..25] of string;

  SideNames       : Array[TZugSeite] of String;

  CheckBaseErrors : Array[TCheckBasePosResult] of String;
  BaseNames       : Array[TBaseType] of String;
  RoomTypNames    : Array[TRoomTyp] of String;

  BodyRegionNames : Array[TBodyRegion] of String;

  ZelleNachladenErrorText : Array[TZelleNachladenError] of String;

  RangTableName   : Array[0..9] of String;

  TimeModeNames   : Array[0..4] of String;

  PointsInfoText   : Array[TPunkteBuch] of String;
  { Monatsarray }
  AMonth : Array[1..12] of String;
  ADays : Array[1..7] of String;
  AZahl : Array[1..9] of String;

  DropErrors     : Array[TDragActionError] of string;

implementation

end.
