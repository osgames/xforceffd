unit script_propertyhelpers;

interface

uses BasisListe, UFOList, EinsatzListe, XForce_Types;

procedure TBasisBasisPos_R(Self: TBasis; var P: TFloatPoint);

procedure TUFOModel_R(Self: TUFO; var P: TUFOModel);

procedure TUFOModel_W(Self: TUFO; var P: TUFOModel);

procedure TUFOPosition_R(Self: TUFO; var P: TFloatPoint);

procedure TEinsatzPosition_R(Self: TEinsatz; var P: TFloatPoint);

procedure TEinsatzPosition_W(Self: TEinsatz; var P: TFloatPoint);

implementation

procedure TBasisBasisPos_R(Self: TBasis; var P: TFloatPoint);
begin
  P := Self.basisPos;
end;

procedure TUFOModel_R(Self: TUFO; var P: TUFOModel);
begin
  P:=Self.Model;
end;

procedure TUFOModel_W(Self: TUFO; var P: TUFOModel);
begin
  Self.SetModel(P);
end;

procedure TUFOPosition_R(Self: TUFO; var P: TFloatPoint);
begin
  P:=Self.Position;
end;

procedure TEinsatzPosition_R(Self: TEinsatz; var P: TFloatPoint);
begin
  P:=Self.Position;
end;

procedure TEinsatzPosition_W(Self: TEinsatz; var P: TFloatPoint);
begin
  Self.Position:=P;
end;

end.

