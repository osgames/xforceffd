{********************************************************************************
*										*
* Copyright (C) 2004  X-Force Team - jim_raynor@web.de				*
*										*
* This file is part of X-Force: Fight For Destiny (X-Force).			*
*										*
* X-Force is free software; you can redistribute it and/or modify		*
* it under the terms of the GNU General Public License as published by		*
* the Free Software Foundation; either version 2 of the License, or		*
* (at your option) any later version.						*
*										*
* X-Force is distributed in the hope that it will be useful,			*
* but WITHOUT ANY WARRANTY; without even the implied warranty of		*
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the			*
* GNU General Public License for more details.					*
*										*
* You should have received a copy of the GNU General Public License		*
* along with X-Force; if not, write to the Free Software			*
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA	*
*										*
*********************************************************************************
*										*
* Utitlites f�r den Bodeneinsatz						*
*										*
********************************************************************************}

{$I ../Settings.inc}

unit ISOTools;

interface

uses Windows, Classes, Graphics, DXDraws, DirectDraw, math, SysUtils, TraceFile, XForce_types;

// Zeichenfunktionen
procedure DrawWithShadow(Surface: TDirectDrawSurface;X,Y: Integer;SrcRect: TRect;Source: TDirectDrawSurface);
procedure DrawWithOutShadow(Surface: TDirectDrawSurface;X,Y: Integer;SrcRect: TRect;Source: TDirectDrawSurface);
procedure DrawWithOutShadowTrans(Surface: TDirectDrawSurface;X,Y: Integer;SrcRect: TRect;Source: TDirectDrawSurface);

procedure DrawBeamEffekt(Surface: TDirectDrawSurface;X,Y: Integer;SrcRect: TRect;Source: TDirectDrawSurface);

procedure BlendTileSet(Rect: TRect; Alpha: Cardinal; Surface: TDirectDrawSurface;Mem : TDDSurfaceDesc);overload;

procedure DrawWallWindow(Surface: TDirectDrawSurface;X,Y: Integer;SrcRect: TRect;Source: TDirectDrawSurface; Color: TColor);

// Allgemeine Funktionen
function GetDirection(FromPos: TPoint; ToPos: TPoint): TViewDirection;

implementation

uses Blending;

procedure DrawWithShadow(Surface: TDirectDrawSurface;X,Y: Integer;SrcRect: TRect;Source: TDirectDrawSurface);
var
  MemSur            : TDDSurfaceDesc;
  MemSource         : TDDSurfaceDesc;
  PoiSur            : Cardinal;
  PoiSource         : Cardinal;
  relX,relY         : Integer;
  absX,absY         : Integer;
  CurX,CurY         : Integer;
  Height,Width      : Integer;
  ShMask            : Cardinal;
  ClipRect          : TRect;
begin
  if Source=nil then exit;
  if Surface=nil then exit;
  Surface.Canvas.Release;
  ClipRect:=Surface.ClippingRect;
  if not Source.Lock(PRect(nil)^,MemSource) then exit;
  if not Surface.Lock(PRect(nil)^,MemSur) then
  begin
    Source.Unlock;
    exit;
  end;
  AbsY:=Y;
  CurY:=SrcRect.Top;
  Height:=SrcRect.Bottom-SrcRect.Top;
  ShMask:=MemSource.ddpfPixelFormat.dwRBitMask or MemSource.ddpfPixelFormat.dwGBitMask;
  Width:=SrcRect.Right-SrcRect.Left;
  relY:=0;
  if Mode32Bit then
  begin
    while (AbsY<ClipRect.Bottom) and (relY<Height) do
    begin
      relX:=0;
      AbsX:=X;
      CurX:=SrcRect.Left;
      if (AbsY>=ClipRect.Top) then
      begin
        PoiSource:=Integer(MemSource.lpSurface)+(CurY*MemSource.lPitch)+(CurX shl 2);
        PoiSur:=Integer(MemSur.lpSurface)+(AbsY*MemSur.lPitch)+(AbsX shl 2);
        while (AbsX<ClipRect.Right) and (relX<Width) do
        begin
          if AbsX>=ClipRect.Left then
          begin
            Assert((AbsX<ClipRect.Right) and (relX<Width));
            if (PLongWord(PoiSource)^ and $00FFFFFF)<>0 then
            begin
              if (PLongWord(PoiSource)^ and ShMask)=0 then
              begin
                PLongWord(PoiSur)^:=((PLongWord(PoiSur)^ and Mask) shr 1);
              end
              else PInteger(PoiSur)^:=PInteger(PoiSource)^;
            end;
          end;
          inc(PoiSur,4);
          inc(PoiSource,4);
          inc(relX);
          inc(absX);
        end;
      end;
      inc(relY);
      inc(AbsY);
      inc(CurY);
      if CurY>=MemSource.dwHeight then
        break;
    end;
  end
  else
  begin
    while (AbsY<ClipRect.Bottom) and (relY<Height) do
    begin
      relX:=0;
      AbsX:=X;
      CurX:=SrcRect.Left;
      PoiSource:=Integer(MemSource.lpSurface)+(CurY*MemSource.lPitch)+(CurX shl 1);
      PoiSur:=Integer(MemSur.lpSurface)+(AbsY*MemSur.lPitch)+(AbsX shl 1);
      if (AbsY>=ClipRect.Top) then
      begin
        while (AbsX<ClipRect.Right) and (relX<Width) do
        begin
          if AbsX>=ClipRect.Left then
          begin
            if PWord(PoiSource)^<>0 then
            begin
              if (PWord(PoiSource)^ and ShMask)=0 then
              begin
                PWord(PoiSur)^:=(PWord(PoiSur)^ and Mask) shr 1;
              end
              else PWord(PoiSur)^:=PWord(PoiSource)^;
            end;
          end;
          inc(PoiSur,2);
          inc(PoiSource,2);
          inc(relX);
          inc(absX);
        end;
      end;
      inc(relY);
      inc(AbsY);
      inc(CurY);
      if CurY>=MemSource.dwHeight then
        break;
    end;
  end;
  Source.UnLock;
  Surface.UnLock;
end;

procedure DrawWithOutShadow(Surface: TDirectDrawSurface;X,Y: Integer;SrcRect: TRect;Source: TDirectDrawSurface);
var
  MemSur            : TDDSurfaceDesc;
  MemSource         : TDDSurfaceDesc;
  PoiSur            : Cardinal;
  PoiSource         : Cardinal;
  relX,relY         : Integer;
  absX,absY         : Integer;
  CurX,CurY         : Integer;
  Height,Width      : Integer;
  ShMask            : Cardinal;
  ClipRect          : TRect;
begin
  if Source=nil then exit;
  if Surface=nil then exit;
  Surface.Canvas.Release;
  ClipRect:=Surface.ClippingRect;
  if not Source.Lock(PRect(nil)^,MemSource) then exit;
  if not Surface.Lock(PRect(nil)^,MemSur) then
  begin
    Source.Unlock;
    exit;
  end;
  AbsY:=Y;
  CurY:=SrcRect.Top;
  Height:=SrcRect.Bottom-SrcRect.Top;
  ShMask:=MemSource.ddpfPixelFormat.dwRBitMask or MemSource.ddpfPixelFormat.dwGBitMask;
  Width:=SrcRect.Right-SrcRect.Left;
  relY:=0;
  if Mode32Bit then
  begin
    while (AbsY<ClipRect.Bottom) and (relY<Height) do
    begin
      relX:=0;
      AbsX:=X;
      CurX:=SrcRect.Left;
      if (AbsY>=ClipRect.Top) then
      begin
        PoiSource:=Integer(MemSource.lpSurface)+(CurY*MemSource.lPitch)+(CurX shl 2);
        PoiSur:=Integer(MemSur.lpSurface)+(AbsY*MemSur.lPitch)+(AbsX shl 2);
        while (AbsX<ClipRect.Right) and (relX<Width) do
        begin
          if AbsX>=ClipRect.Left then
          begin
            if (PLongWord(PoiSource)^ and $00FFFFFF)<>0 then
            begin
              if (PLongWord(PoiSource)^ and ShMask)<>0 then
                PInteger(PoiSur)^:=PInteger(PoiSource)^;
            end;
          end;
          inc(PoiSur,4);
          inc(PoiSource,4);
          inc(relX);
          inc(absX);
        end;
      end;
      inc(relY);
      inc(AbsY);
      inc(CurY);
      if CurY>=MemSource.dwHeight then
        break;
    end;
  end
  else
  begin
    while (AbsY<ClipRect.Bottom) and (relY<Height) do
    begin
      relX:=0;
      AbsX:=X;
      CurX:=SrcRect.Left;
      PoiSource:=Integer(MemSource.lpSurface)+(CurY*MemSource.lPitch)+(CurX shl 1);
      PoiSur:=Integer(MemSur.lpSurface)+(AbsY*MemSur.lPitch)+(AbsX shl 1);
      if (AbsY>=ClipRect.Top) then
      begin
        while (AbsX<ClipRect.Right) and (relX<Width) do
        begin
          if AbsX>=ClipRect.Left then
          begin
            if PWord(PoiSource)^<>0 then
            begin
              if (PWord(PoiSource)^ and ShMask)<>0 then
                PWord(PoiSur)^:=PWord(PoiSource)^;
            end;
          end;
          inc(PoiSur,2);
          inc(PoiSource,2);
          inc(relX);
          inc(absX);
        end;
      end;
      inc(relY);
      inc(AbsY);
      inc(CurY);
      if CurY>=MemSource.dwHeight then
        break;
    end;
  end;
  Source.UnLock;
  Surface.UnLock;
end;

procedure DrawBeamEffekt(Surface: TDirectDrawSurface;X,Y: Integer;SrcRect: TRect;Source: TDirectDrawSurface);
var
  MemSur            : TDDSurfaceDesc;
  MemSource         : TDDSurfaceDesc;
  PoiSur            : Cardinal;
  PoiSource         : Cardinal;
  relX,relY         : Integer;
  absX,absY         : Integer;
  CurX,CurY         : Integer;
  Height,Width      : Integer;
  ShMask            : Cardinal;
  ClipRect          : TRect;
  RMask             : Cardinal;
  BMask             : Cardinal;
  GMask             : Cardinal;
  ColR,ColB,ColG    : Cardinal;
  RValue            : Cardinal;
  BValue            : Cardinal;
  GValue            : Cardinal;
  UseAlpha          : Cardinal;
  Invert            : Cardinal;
  Alshr             : Integer;
begin
  if Source=nil then exit;
  if Surface=nil then exit;
  Surface.Canvas.Release;
  ClipRect:=Surface.ClippingRect;
  if not Source.Lock(PRect(nil)^,MemSource) then exit;
  if not Surface.Lock(PRect(nil)^,MemSur) then
  begin
    Source.Unlock;
    exit;
  end;
  { Farbmasken anlegen }
  RMask:=MemSur.ddpfPixelFormat.dwRBitMask;
  BMask:=MemSur.ddpfPixelFormat.dwBBitMask;
  GMask:=MemSur.ddpfPixelFormat.dwGBitMask;
  AbsY:=Y;
  CurY:=SrcRect.Top;
  Height:=SrcRect.Bottom-SrcRect.Top;
  Width:=SrcRect.Right-SrcRect.Left;
  relY:=0;
  if Mode32Bit then
  begin
    while (AbsY<ClipRect.Bottom) and (relY<Height) do
    begin
      relX:=0;
      AbsX:=X;
      CurX:=SrcRect.Left;
      if (AbsY>=ClipRect.Top) then
      begin
        PoiSource:=Integer(MemSource.lpSurface)+(CurY*MemSource.lPitch)+(CurX shl 2);
        PoiSur:=Integer(MemSur.lpSurface)+(AbsY*MemSur.lPitch)+(AbsX shl 2);
        while (AbsX<ClipRect.Right) and (relX<Width) do
        begin
          if AbsX>=ClipRect.Left then
          begin
            if (PLongWord(PoiSource)^ and $00FFFFFF)<>0 then
            begin
              { Zielfarbe errechnen }
              UseAlpha:=(PLongWord(PoiSource)^ and BMask) div 2;
              Invert:=255-UseAlpha;
              ColR:=(PLongWord(PoiSource)^ and RMask)*UseAlpha;
              ColG:=(PLongWord(PoiSource)^ and GMask)*UseAlpha;
              ColB:=(PLongWord(PoiSource)^ and BMask)*UseAlpha;
              RValue:=(RMask and ((ColR+(PLongWord(PoiSur)^ and RMask)*Invert) shr 8));
              GValue:=(GMask and ((ColG+(PLongWord(PoiSur)^ and GMask)*Invert) shr 8));
              BValue:=(BMask and ((ColB+(PLongWord(PoiSur)^ and BMask)*Invert) shr 8));
              PLongWord(PoiSur)^:=(RValue or GValue or BValue);
            end;
          end;
          inc(PoiSur,4);
          inc(PoiSource,4);
          inc(relX);
          inc(absX);
        end;
      end;
      inc(relY);
      inc(AbsY);
      inc(CurY);
    end;
  end
  else
  begin
    if GMask=$0000F800 then
      Alshr:=11
    else
      Alshr:=10;
    while (AbsY<ClipRect.Bottom) and (relY<Height) do
    begin
      relX:=0;
      AbsX:=X;
      CurX:=SrcRect.Left;
      if (AbsY>=ClipRect.Top) then
      begin
        PoiSource:=Integer(MemSource.lpSurface)+(CurY*MemSource.lPitch)+(CurX shl 1);
        PoiSur:=Integer(MemSur.lpSurface)+(AbsY*MemSur.lPitch)+(AbsX shl 1);
        while (AbsX<ClipRect.Right) and (relX<Width) do
        begin
          if AbsX>=ClipRect.Left then
          begin
            if PWord(PoiSource)^<>0 then
            begin
              RValue:=(PWord(PoiSource)^ and RMask);
              GValue:=(PWord(PoiSource)^ and GMask);
              BValue:=(PWord(PoiSource)^ and BMask);
              UseAlpha:=BValue shl 3;
              if UseAlpha>255 then UseAlpha:=255;
              Invert:=255-UseAlpha;
              RValue:=(RMask and ((RValue*UseAlpha+(PWord(PoiSur)^ and RMask)*Invert) shr 8));
              GValue:=(GMask and ((GValue*UseAlpha+(PWord(PoiSur)^ and GMask)*Invert) shr 8));
              BValue:=(BMask and ((BValue*UseAlpha+(PWord(PoiSur)^ and BMask)*Invert) shr 8));
              PWord(PoiSur)^:=RValue or GValue or BValue;
            end;
          end;
          inc(PoiSur,2);
          inc(PoiSource,2);
          inc(relX);
          inc(absX);
        end;
      end;
      inc(relY);
      inc(AbsY);
      inc(CurY);
    end;
  end;
  Source.UnLock;
  Surface.UnLock;
end;

function GetDirection(FromPos: TPoint; ToPos: TPoint): TViewDirection;
const
  DirectionArray : Array[TViewDirection] of TPoint =
                      {vdLeft}       ((X:  0; Y:  1),
                      {vdLeftTop}     (X: -1; Y:  1),
                      {vdTop}         (X: -1; Y:  0),
                      {vdRightTop}    (X: -1; Y: -1),
                      {vdRight}       (X:  0; Y: -1),
                      {vdRightBottom} (X:  1; Y: -1),
                      {vdBottom}      (X:  1; Y:  0),
                      {vdLeftBottom}  (X:  1; Y:  1),
                      {vdAll}         (X:  0; Y:  0));
var
  XOffSet,YOffSet: Integer;
  Dummy          : TViewDirection;
begin
  XOffSet:=ToPos.X-FromPos.X;
  YOffSet:=ToPos.Y-FromPos.Y;

  if (XOffSet=0) and (YOffSet=0) then
  begin
    result:=vdTop;
    exit;
  end;

  Assert(Abs(XOffSet)<=1,'Abstand f�r GetDirection zu gro�');
  Assert(Abs(YOffSet)<=1,'Abstand f�r GetDirection zu gro�');

  for Dummy:=low(TViewDirection) to high(TViewDirection) do
  begin
    with DirectionArray[Dummy] do
    begin
      if (X=XoffSet) and (Y=YOffSet) then
      begin
        result:=Dummy;
        exit;
      end;
    end;
  end;
end;

procedure BlendTileSet(Rect: TRect; Alpha: Cardinal; Surface: TDirectDrawSurface;Mem : TDDSurfaceDesc);overload;
var
  RMask,BMask,GMask    : Cardinal;
  Invert               : Cardinal;
  Pointer              : Cardinal;
  Pixels               : Cardinal;
  RValue               : Cardinal;
  BValue               : Cardinal;
  GValue               : Cardinal;
  Weite,Spalte,Zeile   : Integer;
begin
  if Alpha=0 then
    exit;

  ClipRect(Rect,Surface.ClippingRect);
  if IsRectEmpty(Rect) then exit;
  RMask:=Mem.ddpfPixelFormat.dwRBitMask;
  BMask:=Mem.ddpfPixelFormat.dwBBitMask;
  GMask:=Mem.ddpfPixelFormat.dwGBitMask;
  Invert:=255-Alpha;
  Weite:=(Rect.Right-Rect.Left);
  Pixels:=Weite*((Rect.Bottom-Rect.Top)+1);
  Spalte:=1;
  Zeile:=Rect.Top;
  if Mode32Bit then
  begin
    Pointer:=Integer(Mem.lpSurface)+(Zeile*Mem.lPitch)+(Rect.Left shl 2);
    while (Pixels>0) do
    begin
      if Spalte>Weite then
      begin
        inc(Zeile);
        Pointer:=Integer(Mem.lpSurface)+(Zeile*Mem.lPitch)+(Rect.Left shl 2);
        Spalte:=1;
      end;
      if (PLongWord(Pointer)^ and $00FFFFFF)<>bcFuchsia then
      begin
        RValue:=(RMask and (((PLongWord(Pointer)^ and RMask)*Invert) shr 8));
        GValue:=(GMask and (((PLongWord(Pointer)^ and GMask)*Invert) shr 8));
        BValue:=(BMask and (((PLongWord(Pointer)^ and BMask)*Invert) shr 8));
        PLongWord(Pointer)^:=(RValue or GValue or BValue);
      end;
      inc(Pointer,4);
      inc(Spalte);
      dec(Pixels);
    end;
  end
  else
  begin
    Pointer:=Integer(Mem.lpSurface)+(Zeile*Mem.lPitch)+(Rect.Left shl 1);
    while (Pixels>0) do
    begin
      if Spalte>Weite then
      begin
        inc(Zeile);
        Pointer:=Integer(Mem.lpSurface)+(Zeile*Mem.lPitch)+(Rect.Left shl 1);
        Spalte:=1;
      end;
      if (PWord(Pointer)^<>bcFuchsia) then
      begin
        RValue:=(RMask and (((PInteger(Pointer)^ and RMask)*Invert) shr 8));
        GValue:=(GMask and (((PInteger(Pointer)^ and GMask)*Invert) shr 8));
        BValue:=(BMask and (((PInteger(Pointer)^ and BMask)*Invert) shr 8));
        PWord(Pointer)^:=RValue or GValue or BValue;
      end;
      inc(Pointer,2);
      inc(Spalte);
      dec(Pixels);
    end;
  end;
end;

procedure DrawWallWindow(Surface: TDirectDrawSurface;X,Y: Integer;SrcRect: TRect;Source: TDirectDrawSurface;Color: TColor);
var
  MemSur            : TDDSurfaceDesc;
  MemSource         : TDDSurfaceDesc;
  PoiSur            : Cardinal;
  PoiSource         : Cardinal;
  relX,relY         : Integer;
  absX,absY         : Integer;
  CurX,CurY         : Integer;
  RMask,BMask,GMask : Cardinal;
  RValue            : Cardinal;
  BValue            : Cardinal;
  GValue            : Cardinal;
  ColR,ColG,ColB    : Cardinal;
  Height,Width      : Integer;
  UseAlpha,Invert   : Integer;
  ShMask            : Cardinal;
  ClipRect          : TRect;
begin
  if Source=nil then exit;
  if Surface=nil then exit;

  Color:=Surface.ColorMatch(Color);

  Surface.Canvas.Release;
  ClipRect:=Surface.ClippingRect;
  if not Source.Lock(PRect(nil)^,MemSource) then exit;
  if not Surface.Lock(PRect(nil)^,MemSur) then
  begin
    Source.Unlock;
    exit;
  end;
  AbsY:=Y;
  CurY:=SrcRect.Top;
  Height:=SrcRect.Bottom-SrcRect.Top;

  RMask:=MemSource.ddpfPixelFormat.dwRBitMask;
  BMask:=MemSource.ddpfPixelFormat.dwBBitMask;
  GMask:=MemSource.ddpfPixelFormat.dwGBitMask;

  ShMask:=GMask or RMask;
  Width:=SrcRect.Right-SrcRect.Left;
  relY:=0;
  if Mode32Bit then
  begin
    while (AbsY<ClipRect.Bottom) and (relY<Height) do
    begin
      relX:=0;
      AbsX:=X;
      CurX:=SrcRect.Left;
      if (AbsY>=ClipRect.Top) then
      begin
        PoiSource:=Integer(MemSource.lpSurface)+(CurY*MemSource.lPitch)+(CurX shl 2);
        PoiSur:=Integer(MemSur.lpSurface)+(AbsY*MemSur.lPitch)+(AbsX shl 2);
        while (AbsX<ClipRect.Right) and (relX<Width) do
        begin
          if AbsX>=ClipRect.Left then
          begin
            if (PLongWord(PoiSource)^ and $00FFFFFF)<>bcFuchsia then
            begin
              if (PLongWord(PoiSource)^ and ShMask)=0 then
              begin
                { Zielfarbe errechnen }
                UseAlpha:=((((PLongWord(PoiSource)^ and BMask))));
                Invert:=255-UseAlpha;
                ColR:=(Color and RMask)*UseAlpha;
                ColG:=(Color and GMask)*UseAlpha;
                ColB:=(Color and BMask)*UseAlpha;
                RValue:=(RMask and ((ColR+(PLongWord(PoiSur)^ and RMask)*Invert) shr 8));
                GValue:=(GMask and ((ColG+(PLongWord(PoiSur)^ and GMask)*Invert) shr 8));
                BValue:=(BMask and ((ColB+(PLongWord(PoiSur)^ and BMask)*Invert) shr 8));
                PLongWord(PoiSur)^:=(RValue or GValue or BValue);
              end
              else PInteger(PoiSur)^:=PInteger(PoiSource)^;
            end;
          end;
          inc(PoiSur,4);
          inc(PoiSource,4);
          inc(relX);
          inc(absX);
        end;
      end;
      inc(relY);
      inc(AbsY);
      inc(CurY);
      if CurY>=MemSource.dwHeight then
        break;
    end;
  end
  else
  begin
    while (AbsY<ClipRect.Bottom) and (relY<Height) do
    begin
      relX:=0;
      AbsX:=X;
      CurX:=SrcRect.Left;
      PoiSource:=Integer(MemSource.lpSurface)+(CurY*MemSource.lPitch)+(CurX shl 1);
      PoiSur:=Integer(MemSur.lpSurface)+(AbsY*MemSur.lPitch)+(AbsX shl 1);
      if (AbsY>=ClipRect.Top) then
      begin
        while (AbsX<ClipRect.Right) and (relX<Width) do
        begin
          if AbsX>=ClipRect.Left then
          begin
            if PWord(PoiSource)^<>bcFuchsia then
            begin
              if (PWord(PoiSource)^ and ShMask)=0 then
              begin
                { Zielfarbe errechnen }
                UseAlpha:=(PWord(PoiSource)^ and BMask) shl 3;
                Invert:=255-UseAlpha;
                ColR:=(Color and RMask)*UseAlpha;
                ColG:=(Color and GMask)*UseAlpha;
                ColB:=(Color and BMask)*UseAlpha;
                RValue:=(RMask and ((ColR+(PWord(PoiSur)^ and RMask)*Invert) shr 8));
                GValue:=(GMask and ((ColG+(PWord(PoiSur)^ and GMask)*Invert) shr 8));
                BValue:=(BMask and ((ColB+(PWord(PoiSur)^ and BMask)*Invert) shr 8));
                PWord(PoiSur)^:=(RValue or GValue or BValue);
              end
              else
                PWord(PoiSur)^:=PWord(PoiSource)^;
            end;
          end;
          inc(PoiSur,2);
          inc(PoiSource,2);
          inc(relX);
          inc(absX);
        end;
      end;
      inc(relY);
      inc(AbsY);
      inc(CurY);
      if CurY>=MemSource.dwHeight then
        break;
    end;
  end;
  Source.UnLock;
  Surface.UnLock;
end;

procedure DrawWithOutShadowTrans(Surface: TDirectDrawSurface;X,Y: Integer;SrcRect: TRect;Source: TDirectDrawSurface);
var
  MemSur            : TDDSurfaceDesc;
  MemSource         : TDDSurfaceDesc;
  PoiSur            : Cardinal;
  PoiSource         : Cardinal;
  relX,relY         : Integer;
  absX,absY         : Integer;
  CurX,CurY         : Integer;
  Height,Width      : Integer;
  ShMask            : Cardinal;
  ClipRect          : TRect;
begin
  if Source=nil then exit;
  if Surface=nil then exit;
  Surface.Canvas.Release;
  ClipRect:=Surface.ClippingRect;
  if not Source.Lock(PRect(nil)^,MemSource) then exit;
  if not Surface.Lock(PRect(nil)^,MemSur) then
  begin
    Source.Unlock;
    exit;
  end;
  AbsY:=Y;
  CurY:=SrcRect.Top;
  Height:=SrcRect.Bottom-SrcRect.Top;
  ShMask:=MemSource.ddpfPixelFormat.dwRBitMask or MemSource.ddpfPixelFormat.dwGBitMask;
  Width:=SrcRect.Right-SrcRect.Left;
  relY:=0;
  if Mode32Bit then
  begin
    while (AbsY<ClipRect.Bottom) and (relY<Height) do
    begin
      relX:=0;
      AbsX:=X;
      CurX:=SrcRect.Left;
      if (AbsY>=ClipRect.Top) then
      begin
        PoiSource:=Integer(MemSource.lpSurface)+(CurY*MemSource.lPitch)+(CurX shl 2);
        PoiSur:=Integer(MemSur.lpSurface)+(AbsY*MemSur.lPitch)+(AbsX shl 2);
        while (AbsX<ClipRect.Right) and (relX<Width) do
        begin
          if AbsX>=ClipRect.Left then
          begin
            if (PLongWord(PoiSource)^ and $00FFFFFF)<>0 then
            begin
              if (PLongWord(PoiSource)^ and ShMask)<>0 then
                PLongWord(PoiSur)^:=((PLongWord(PoiSur)^ and Mask) shr 1)+((PLongWord(PoiSource)^  and Mask) shr 1);
            end;
          end;
          inc(PoiSur,4);
          inc(PoiSource,4);
          inc(relX);
          inc(absX);
        end;
      end;
      inc(relY);
      inc(AbsY);
      inc(CurY);
      if CurY>=MemSource.dwHeight then
        break;
    end;
  end
  else
  begin
    while (AbsY<ClipRect.Bottom) and (relY<Height) do
    begin
      relX:=0;
      AbsX:=X;
      CurX:=SrcRect.Left;
      PoiSource:=Integer(MemSource.lpSurface)+(CurY*MemSource.lPitch)+(CurX shl 1);
      PoiSur:=Integer(MemSur.lpSurface)+(AbsY*MemSur.lPitch)+(AbsX shl 1);
      if (AbsY>=ClipRect.Top) then
      begin
        while (AbsX<ClipRect.Right) and (relX<Width) do
        begin
          if AbsX>=ClipRect.Left then
          begin
            if PWord(PoiSource)^<>0 then
            begin
              if (PWord(PoiSource)^ and ShMask)<>0 then
              PWord(PoiSur)^:=((PWord(PoiSur)^ and Mask) shr 1) +((PWord(PoiSource)^ and Mask) shr 1);
            end;
          end;
          inc(PoiSur,2);
          inc(PoiSource,2);
          inc(relX);
          inc(absX);
        end;
      end;
      inc(relY);
      inc(AbsY);
      inc(CurY);
      if CurY>=MemSource.dwHeight then
        break;
    end;
  end;
  Source.UnLock;
  Surface.UnLock;
end;

end.
