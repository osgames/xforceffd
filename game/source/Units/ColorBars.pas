{********************************************************************************
*										*
* Copyright (C) 2004  X-Force Team - jim_raynor@web.de				*
*										*
* This file is part of X-Force: Fight For Destiny (X-Force).			*
*										*
* X-Force is free software; you can redistribute it and/or modify		*
* it under the terms of the GNU General Public License as published by		*
* the Free Software Foundation; either version 2 of the License, or		*
* (at your option) any later version.						*
*										*
* X-Force is distributed in the hope that it will be useful,			*
* but WITHOUT ANY WARRANTY; without even the implied warranty of		*
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the			*
* GNU General Public License for more details.					*
*										*
* You should have received a copy of the GNU General Public License		*
* along with X-Force; if not, write to the Free Software			*
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA	*
*										*
*********************************************************************************
*										*
* Erstellt die Farbleisten f�r den Bodeneinsatz					*
*										*
********************************************************************************}

{$I ../Settings.inc}

unit ColorBars;

interface

uses DirectDraw, DXDraws, Blending, Windows, Classes, math;

procedure InitBarSurface(DXDraw: TDXDraw);

procedure DrawWeaponTimeBar(Surface: TDirectDrawSurface;X,Y: Integer; Percent: Integer);
procedure DrawWeaponMunitionBar(Surface: TDirectDrawSurface;X,Y: Integer; Percent: Integer);

procedure DrawLifeBar(Surface: TDirectDrawSurface;X,Y: Integer; Percent: Integer);
procedure DrawShieldBar(Surface: TDirectDrawSurface;X,Y: Integer; Percent: Integer);

implementation

var
  BarSurface        : TDirectDrawSurface;

procedure InitLeisten;
var
  X      : Integer;
  Mem    : TDDSurfaceDesc;
  RMask  : Cardinal;
  BMask  : Cardinal;
  GMask  : Cardinal;
  RValue : Cardinal;
  BValue : Cardinal;
  GValue : Cardinal;
  Farbe  : Cardinal;

  function GeneriereFarbStufe(Alpha: Integer): TBlendColor;
  begin
    RValue:=RMask and Farbe;
    BValue:=BMask and Farbe;
    GValue:=GMask and Farbe;
    RValue:=round(RValue/255*Alpha) and RMask;
    BValue:=round(BValue/255*Alpha) and BMask;
    GValue:=round(GValue/255*Alpha) and GMask;
    result:=RValue or BValue or GValue;
  end;

  procedure SetPixel(X,Y: Integer;Color: TBlendColor);
  var
    Pointer1: Integer;
  begin
    if Mode32Bit then
    begin
      Pointer1:=Integer(Mem.lpSurface)+(Y*Mem.lPitch)+(X shl 2);
      PLongWord(Pointer1)^:=Color;
    end
    else
      PWord(Integer(Mem.lpSurface)+(Y*Mem.lPitch)+(X shl 1))^:=Color;
  end;

begin
  RMask:=BarSurface.SurfaceDesc.ddpfPixelFormat.dwRBitMask;
  BMask:=BarSurface.SurfaceDesc.ddpfPixelFormat.dwBBitMask;
  GMask:=BarSurface.SurfaceDesc.ddpfPixelFormat.dwGBitMask;
  BarSurface.Fill(0);
  BarSurface.Lock(Mem);
  { Energie vorhanden Raumschiff }
  for X:=0 to 199 do
  begin
    Farbe:=LineColorTable[100-round(X/199*100)];
    SetPixel(X,0,GeneriereFarbStufe(64));
    SetPixel(X,1,GeneriereFarbStufe(96));
    SetPixel(X,2,GeneriereFarbStufe(128));
    SetPixel(X,3,GeneriereFarbStufe(192));
    SetPixel(X,4,GeneriereFarbStufe(255));
    SetPixel(X,5,GeneriereFarbStufe(255));
    SetPixel(X,6,GeneriereFarbStufe(192));
    SetPixel(X,7,GeneriereFarbStufe(128));
    SetPixel(X,8,GeneriereFarbStufe(96));
    SetPixel(X,9,GeneriereFarbStufe(64));
  end;
  { Energie Rest Raumschiff }
  for X:=200 to 399 do
  begin
    Farbe:=LineColorTable[100-round((X-200)/199*100)];
    SetPixel(X,0,GeneriereFarbStufe(32));
    SetPixel(X,1,GeneriereFarbStufe(48));
    SetPixel(X,2,GeneriereFarbStufe(64));
    SetPixel(X,3,GeneriereFarbStufe(80));
    SetPixel(X,4,GeneriereFarbStufe(96));
    SetPixel(X,5,GeneriereFarbStufe(96));
    SetPixel(X,6,GeneriereFarbStufe(80));
    SetPixel(X,7,GeneriereFarbStufe(64));
    SetPixel(X,8,GeneriereFarbStufe(48));
    SetPixel(X,9,GeneriereFarbStufe(32));
  end;
  { Energie vorhanden UFO }
  for X:=0 to 199 do
  begin
    Farbe:=LineColorTable[round(X/199*100)];
    SetPixel(X,10,GeneriereFarbStufe(64));
    SetPixel(X,11,GeneriereFarbStufe(96));
    SetPixel(X,12,GeneriereFarbStufe(128));
    SetPixel(X,13,GeneriereFarbStufe(192));
    SetPixel(X,14,GeneriereFarbStufe(255));
    SetPixel(X,15,GeneriereFarbStufe(255));
    SetPixel(X,16,GeneriereFarbStufe(192));
    SetPixel(X,17,GeneriereFarbStufe(128));
    SetPixel(X,18,GeneriereFarbStufe(96));
    SetPixel(X,19,GeneriereFarbStufe(64));
  end;
  { Energie Rest UFO }
  for X:=200 to 399 do
  begin
    Farbe:=LineColorTable[round((X-200)/199*100)];
    SetPixel(X,10,GeneriereFarbStufe(32));
    SetPixel(X,11,GeneriereFarbStufe(48));
    SetPixel(X,12,GeneriereFarbStufe(64));
    SetPixel(X,13,GeneriereFarbStufe(80));
    SetPixel(X,14,GeneriereFarbStufe(96));
    SetPixel(X,15,GeneriereFarbStufe(96));
    SetPixel(X,16,GeneriereFarbStufe(80));
    SetPixel(X,17,GeneriereFarbStufe(64));
    SetPixel(X,18,GeneriereFarbStufe(48));
    SetPixel(X,19,GeneriereFarbStufe(32));
  end;
  { Schild vorhanden Raumschiff }
  for X:=0 to 199 do
  begin
    Farbe:=ShieldColorTable[round(X/199*100)];
    SetPixel(X,20,GeneriereFarbStufe(64));
    SetPixel(X,21,GeneriereFarbStufe(96));
    SetPixel(X,22,GeneriereFarbStufe(128));
    SetPixel(X,23,GeneriereFarbStufe(192));
    SetPixel(X,24,GeneriereFarbStufe(255));
    SetPixel(X,25,GeneriereFarbStufe(255));
    SetPixel(X,26,GeneriereFarbStufe(192));
    SetPixel(X,27,GeneriereFarbStufe(128));
    SetPixel(X,28,GeneriereFarbStufe(96));
    SetPixel(X,29,GeneriereFarbStufe(64));
  end;
  { Schild Rest Raumschiff }
  for X:=200 to 399 do
  begin
    Farbe:=ShieldColorTable[round((X-200)/199*100)];
    SetPixel(X,20,GeneriereFarbStufe(32));
    SetPixel(X,21,GeneriereFarbStufe(48));
    SetPixel(X,22,GeneriereFarbStufe(64));
    SetPixel(X,23,GeneriereFarbStufe(80));
    SetPixel(X,24,GeneriereFarbStufe(96));
    SetPixel(X,25,GeneriereFarbStufe(96));
    SetPixel(X,26,GeneriereFarbStufe(80));
    SetPixel(X,27,GeneriereFarbStufe(64));
    SetPixel(X,28,GeneriereFarbStufe(48));
    SetPixel(X,29,GeneriereFarbStufe(32));
  end;
  { Schild vorhanden UFO }
  for X:=0 to 199 do
  begin
    Farbe:=ShieldColorTable[100-round(X/199*100)];
    SetPixel(X,30,GeneriereFarbStufe(64));
    SetPixel(X,31,GeneriereFarbStufe(96));
    SetPixel(X,32,GeneriereFarbStufe(128));
    SetPixel(X,33,GeneriereFarbStufe(192));
    SetPixel(X,34,GeneriereFarbStufe(255));
    SetPixel(X,35,GeneriereFarbStufe(255));
    SetPixel(X,36,GeneriereFarbStufe(192));
    SetPixel(X,37,GeneriereFarbStufe(128));
    SetPixel(X,38,GeneriereFarbStufe(96));
    SetPixel(X,39,GeneriereFarbStufe(64));
  end;
  { Schild Rest UFO }
  for X:=200 to 399 do
  begin
    Farbe:=ShieldColorTable[100-round((X-200)/199*100)];
    SetPixel(X,30,GeneriereFarbStufe(32));
    SetPixel(X,31,GeneriereFarbStufe(48));
    SetPixel(X,32,GeneriereFarbStufe(64));
    SetPixel(X,33,GeneriereFarbStufe(80));
    SetPixel(X,34,GeneriereFarbStufe(96));
    SetPixel(X,35,GeneriereFarbStufe(96));
    SetPixel(X,36,GeneriereFarbStufe(80));
    SetPixel(X,37,GeneriereFarbStufe(64));
    SetPixel(X,38,GeneriereFarbStufe(48));
    SetPixel(X,39,GeneriereFarbStufe(32));
  end;
  { Aufladezeit Links beendet }
  for X:=0 to 99 do
  begin
    Farbe:=TimeColorTable[round(X/99*100)];
    SetPixel(X,40,GeneriereFarbStufe(128));
    SetPixel(X,41,GeneriereFarbStufe(192));
    SetPixel(X,42,GeneriereFarbStufe(255));
    SetPixel(X,43,GeneriereFarbStufe(192));
    SetPixel(X,44,GeneriereFarbStufe(128));
  end;
  { Aufladezeit Links ben�tigt }
  for X:=100 to 199 do
  begin
    Farbe:=TimeColorTable[round((X-100)/99*100)];
    SetPixel(X,40,GeneriereFarbStufe(48));
    SetPixel(X,41,GeneriereFarbStufe(72));
    SetPixel(X,42,GeneriereFarbStufe(96));
    SetPixel(X,43,GeneriereFarbStufe(72));
    SetPixel(X,44,GeneriereFarbStufe(48));
  end;
  { Aufladezeit Rechts beendet }
  for X:=0 to 99 do
  begin
    Farbe:=TimeColorTable[100-round(X/99*100)];
    SetPixel(X,45,GeneriereFarbStufe(128));
    SetPixel(X,46,GeneriereFarbStufe(192));
    SetPixel(X,47,GeneriereFarbStufe(255));
    SetPixel(X,48,GeneriereFarbStufe(192));
    SetPixel(X,49,GeneriereFarbStufe(128));
  end;
  { Aufladezeit Rechts ben�tigt }
  for X:=100 to 199 do
  begin
    Farbe:=TimeColorTable[100-round((X-100)/99*100)];
    SetPixel(X,45,GeneriereFarbStufe(48));
    SetPixel(X,46,GeneriereFarbStufe(72));
    SetPixel(X,47,GeneriereFarbStufe(96));
    SetPixel(X,48,GeneriereFarbStufe(72));
    SetPixel(X,49,GeneriereFarbStufe(48));
  end;
  { Munition Links beendet }
  for X:=0 to 99 do
  begin
    Farbe:=MuniColorTable[100-round(X/99*100)];
    SetPixel(X,50,GeneriereFarbStufe(128));
    SetPixel(X,51,GeneriereFarbStufe(192));
    SetPixel(X,52,GeneriereFarbStufe(255));
    SetPixel(X,53,GeneriereFarbStufe(192));
    SetPixel(X,54,GeneriereFarbStufe(128));
  end;
  { Munition Links ben�tigt }
  for X:=100 to 199 do
  begin
    Farbe:=MuniColorTable[100-round((X-100)/99*100)];
    SetPixel(X,50,GeneriereFarbStufe(48));
    SetPixel(X,51,GeneriereFarbStufe(72));
    SetPixel(X,52,GeneriereFarbStufe(96));
    SetPixel(X,53,GeneriereFarbStufe(72));
    SetPixel(X,54,GeneriereFarbStufe(48));
  end;
  { Munition Rechts beendet }
  for X:=0 to 99 do
  begin
    Farbe:=MuniColorTable[round(X/99*100)];
    SetPixel(X,55,GeneriereFarbStufe(128));
    SetPixel(X,56,GeneriereFarbStufe(192));
    SetPixel(X,57,GeneriereFarbStufe(255));
    SetPixel(X,58,GeneriereFarbStufe(192));
    SetPixel(X,59,GeneriereFarbStufe(128));
  end;
  { Munition Rechts ben�tigt }
  for X:=100 to 199 do
  begin
    Farbe:=MuniColorTable[round((X-100)/99*100)];
    SetPixel(X,55,GeneriereFarbStufe(48));
    SetPixel(X,56,GeneriereFarbStufe(72));
    SetPixel(X,57,GeneriereFarbStufe(96));
    SetPixel(X,58,GeneriereFarbStufe(72));
    SetPixel(X,59,GeneriereFarbStufe(48));
  end;
  BarSurface.UnLock;
end;

procedure InitBarSurface(DXDraw: TDXDraw);
begin
  if BarSurface<>nil then
    BarSurface.Free;

  { ItemIndex 3 - Leisten}
  BarSurface:=TDirectDrawSurface.Create(DXDraw.DDraw);
  BarSurface.SystemMemory:=true;
  BarSurface.SetSize(400,60);

  InitLeisten;
end;

procedure DrawWeaponTimeBar(Surface: TDirectDrawSurface;X,Y: Integer; Percent: Integer);
begin
  Surface.Draw(X,Y,Rect(0,40,Percent,45),BarSurface,true);
  Surface.Draw(X+Percent,Y,Rect(100+Percent,40,200,45),BarSurface,true);
end;

procedure DrawWeaponMunitionBar(Surface: TDirectDrawSurface;X,Y: Integer; Percent: Integer);
begin
  Surface.Draw(X,Y,Rect(0,50,Percent,55),BarSurface,true);
  Surface.Draw(X+Percent,Y,Rect(100+Percent,50,200,55),BarSurface,true);
end;

procedure DrawLifeBar(Surface: TDirectDrawSurface;X,Y: Integer; Percent: Integer);
var
  TempWid: Integer;
begin
  TempWid:=max(0,Percent*2);
  if TransparentEnergy then
  begin
    DrawTransAlpha(Surface,X,Y,Rect(0,0,TempWid,10),BarSurface,200,bcBlack);
    DrawTransAlpha(Surface,X+TempWid,Y,Rect(TempWid+200,0,400,10),BarSurface,200,bcBlack);
  end
  else
  begin
    Surface.Draw(X,Y,Rect(0,0,TempWid,10),BarSurface,false);
    Surface.Draw(X+TempWid,Y,Rect(TempWid+200,0,400,10),BarSurface,false);
  end;
end;

procedure DrawShieldBar(Surface: TDirectDrawSurface;X,Y: Integer; Percent: Integer);
var
  TempWid: Integer;
begin
  TempWid:=max(0,Percent*2);
  if TransparentEnergy then
  begin
    DrawTransAlpha(Surface,X,Y,Rect(0,20,TempWid,30),BarSurface,200,bcBlack);
    DrawTransAlpha(Surface,X+TempWid,Y,Rect(TempWid+200,0,400,10),BarSurface,200,bcBlack);
  end
  else
  begin
    Surface.Draw(X,Y,Rect(0,20,TempWid,30),BarSurface,false);
    Surface.Draw(X+TempWid,Y,Rect(TempWid+200,20,400,30),BarSurface,false);
  end;
end;

end.
