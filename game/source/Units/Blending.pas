{********************************************************************************
*										*
* Copyright (C) 2004  X-Force Team - jim_raynor@web.de				*
*										*
* This file is part of X-Force: Fight For Destiny (X-Force).			*
*										*
* X-Force is free software; you can redistribute it and/or modify		*
* it under the terms of the GNU General Public License as published by		*
* the Free Software Foundation; either version 2 of the License, or		*
* (at your option) any later version.						*
*										*
* X-Force is distributed in the hope that it will be useful,			*
* but WITHOUT ANY WARRANTY; without even the implied warranty of		*
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the			*
* GNU General Public License for more details.					*
*										*
* You should have received a copy of the GNU General Public License		*
* along with X-Force; if not, write to the Free Software			*
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA	*
*										*
*********************************************************************************
*										*
* Stellt Funktionen mit Alpha-Blending zur Verf�gung				*
*										*
********************************************************************************}

{$I ../Settings.inc}

unit Blending;

interface

uses
  Windows, Classes, Graphics, DXDraws, DirectDraw, math, SysUtils, TraceFile,
  XForce_types, Defines;

type
  TBlendColor    = Cardinal;
  TPutPixel      = procedure(Surface: TDirectDrawSUrface;const Mem: TDDSurfaceDesc; X, Y: Integer; Color: TBlendColor);
  TPutPixelAA    = procedure(Surface: TDirectDrawSurface;const Mem: TDDSurfaceDesc; X, Y: Integer; Color: TBlendColor; Alpha: Integer);

procedure BlendRectangle(Rect: TRect; Alpha: Cardinal; Color: TBlendColor;Surface: TDirectDrawSurface);overload;
procedure BlendRectangle(Rect: TRect; Alpha: Cardinal; Color: TBlendColor;Surface: TDirectDrawSurface;Mem : TDDSurfaceDesc);overload;

procedure BlendPercentBar(Rect: TRect; Alpha: Cardinal; Color1, Color2: TBlendColor;Percent: double; Surface: TDirectDrawSurface;Mem : TDDSurfaceDesc);

procedure BlendRoundRect(Rect: TRect;Alpha: Cardinal;Color: TBlendColor;Surface: TDirectDrawSurface; Radius: Integer;ClipRect: TRect);overload;
procedure BlendRoundRect(Rect: TRect;Alpha: Cardinal;Color: TBlendColor;Surface: TDirectDrawSurface;Mem: TDDSurfaceDesc; Radius: Integer;Corners: TCorners;ClipRect: TRect);overload;
procedure BlendFrameRoundRect(Rect: TRect; Alpha: Cardinal; Color: TBlendColor;Surface: TDirectDrawSurface;Radius: Integer;ClipRect: TRect;FrameRect: TRect;FrameColor: TColor);
procedure DrawShadow(Surface: TDirectDrawSurface;X,Y: Integer;SrcRect: TRect;Source: TDirectDrawSurface;TransparentColor: TBlendColor);
procedure DrawTransAlpha(Surface: TDirectDrawSurface;X,Y: Integer;SrcRect: TRect;Source: TDirectDrawSurface;Alpha: Integer;TransColor: TBlendColor);
procedure DrawShieldEffect(Surface: TDirectDrawSurface;X,Y: Integer;SrcRect: TRect;Source: TDirectDrawSurface;Color: TBlendColor;Alpha: Cardinal);
procedure FrameRegion(Region: HRGN;FrameRect: TRect;Color: TColor;Surface: TDirectDrawSurface);
procedure InitColorTable(RMask,BMask,GMask: Integer);
procedure DrawLineGradiant(Surface: TDirectDrawSurface;Von: TPoint;Zu: TPoint;Min,Max: Integer);
procedure DrawDots(X,Y: Integer; Mem: PDDSurfaceDesc);StdCall;

{ Linien und sonstiges Zeichnen }
procedure HLine(Surface: TDirectDrawSurface;const Mem: TDDSurfaceDesc;FromX,ToX,Y: Integer;Col: TBlendColor);
procedure VLine(Surface: TDirectDrawSurface;const Mem: TDDSurfaceDesc;FromY,ToY,X: Integer;Col: TBlendColor);
procedure Frame3D(Surface: TDirectDrawSurface;const Mem: TDDSurfaceDesc;X1,Y1,X2,Y2: Integer; First,Second: TBlendColor);
procedure Line(Surface: TDirectDrawSurface;const Mem: TDDSurfaceDesc;X1,Y1,X2,Y2: Integer; Col: TBlendColor);
procedure LineAA(Surface: TDirectDrawSurface;const Mem: TDDSurfaceDesc;X1,Y1,X2,Y2: Integer; Col: TBlendColor; AlphaBlend: Integer);
procedure AALine(Surface: TDirectDrawSurface;const Mem: TDDSurfaceDesc;X1,Y1,X2,Y2: Integer; Color: TBlendColor; AlphaBlend: Integer = 255);
procedure Rectangle(Surface: TDirectDrawSurface;const Mem: TDDSurfaceDesc;X1,Y1,X2,Y2: Integer;Col: TBlendColor);overload;
procedure Rectangle(Surface: TDirectDrawSurface;const Mem: TDDSurfaceDesc;Rect: TRect;Col: TBlendColor);overload;
procedure FramingRect(Surface: TDirectDrawSurface;const Mem: TDDSurfaceDesc;Rect: TRect;Corners: TCorners;Radius: Integer;Col: TBlendColor);

var
  LineColorTable    : Array[0..100] of TBlendColor;
  LineWinColorTable : Array[0..100] of TColor;
  LaserColTable     : Array[0..100] of TBlendColor;
  ShieldColorTable  : Array[0..100] of TBlendColor;
  TimeColorTable    : Array[0..100] of TBlendColor;
  MuniColorTable    : Array[0..100] of TBlendColor;
  WerkColorTable    : Array[0..100] of TBlendColor;
  MinYValue         : Integer;
  MaxYValue         : Integer;
  RangeY            : Integer;
  AlphaElements     : boolean = true;
  AlphaControls     : boolean = true;
  TransparentEnergy : boolean = false;
  Mode32Bit         : boolean;
  Mask              : Cardinal;
  AlphaBlendFaktor  : double = 1.0;

  { Farben die im Hauptprogramm initsialisiert werden m�ssen }
  bcBlue       : TBlendColor;
  bcLime       : TBlendColor;
  bcRed        : TBlendColor;
  bcNavy       : TBlendColor;
  bcGreen      : TBlendColor;
  bcDarkGreen  : TBlendColor;
  bcMaroon     : TBlendColor;
  bcOlive      : TBlendColor;
  bcYellow     : TBlendColor;
  bcPurple     : TBlendColor;
  bcFuchsia    : TBlendColor;
  bcAqua       : TBlendColor;
  bcWhite      : TBlendColor;
  bcBlack      : TBlendColor;
  bcTeal       : TBlendColor;
  bcSkyBlue    : TBlendColor;
  bcDarkNavy   : TBlendColor;
  bcGray       : TBlendColor;
  bcStdGray    : TBlendColor;
  bcLightRed   : TBlendColor;
  bcOrange     : TBlendColor;
  bcSilver     : TBlendColor;
  bcDisabled   : TBlendColor;
  { ProjektViewer }
  bcPVOver     : TBlendColor;
  bcPVHighLight: TBlendColor;
  bcPVTechno   : TBlendColor;
  bcPVEinricht : TBlendColor;
  bcPVShip     : TBlendColor;
  pcPVNormal   : TBlendColor;

  coScrollBrush: TBlendColor;

implementation

uses KD4Utils, DXClass;


procedure PutPixel16(Surface: TDirectDrawSurface;const Mem: TDDSurfaceDesc;X,Y: Integer;Col: TBlendColor);
var
  Pointer: Cardinal;              
begin
  if not PtInRect(Surface.ClippingRect,Point(X,Y)) then exit;
  Pointer:=Integer(Mem.lpSurface)+(Y*Mem.lPitch)+(X shl 1);
  PWord(Pointer)^:=Col;
end;

procedure PutPixel32(Surface: TDirectDrawSurface;const Mem: TDDSurfaceDesc;X,Y: Integer;Col: TBlendColor);
var
  Pointer: Cardinal;
begin
  if not PtInRect(Surface.ClippingRect,Point(X,Y)) then exit;
  Pointer:=Integer(Mem.lpSurface)+(Y*Mem.lPitch)+(X shl 2);
  PInteger(Pointer)^:=Col;
end;

procedure PutAAPixel16(Surface: TDirectDrawSUrface;const Mem: TDDSurfaceDesc; X, Y: Integer; Color: TBlendColor; Alpha: Integer);
var
  Bitmap: Integer;
  Invert: Integer;
  RValue               : Cardinal;
  BValue               : Cardinal;
  GValue               : Cardinal;
begin
  if not PtInRect(Surface.ClippingRect,Point(X,Y)) then exit;
  Bitmap := Integer(Mem.lpSurface)+(Mem.Lpitch*Y)+(X shl 1);
  Invert:=255-Alpha;
  RValue:=Mem.ddpfPixelFormat.dwRBitMask and ((((Color and Mem.ddpfPixelFormat.dwRBitMask)*Alpha)+((PWord(Bitmap)^ and Mem.ddpfPixelFormat.dwRBitMask)*Invert)) shr 8);
  GValue:=Mem.ddpfPixelFormat.dwGBitMask and ((((Color and Mem.ddpfPixelFormat.dwGBitMask)*Alpha)+((PWord(Bitmap)^ and Mem.ddpfPixelFormat.dwGBitMask)*Invert)) shr 8);
  BValue:=Mem.ddpfPixelFormat.dwBBitMask and ((((Color and Mem.ddpfPixelFormat.dwBBitMask)*Alpha)+((PWord(Bitmap)^ and Mem.ddpfPixelFormat.dwBBitMask)*Invert)) shr 8);
  PWord(Bitmap)^:=Rvalue or GValue or BValue;
end;

procedure PutAAPixel32(Surface: TDirectDrawSUrface;const Mem: TDDSurfaceDesc; X, Y: Integer; Color: TBlendColor; Alpha: Integer);
var
  Bitmap: Integer;
  Invert: Integer;
  RValue               : Cardinal;
  BValue               : Cardinal;
  GValue               : Cardinal;
begin
  if not PtInRect(Surface.ClippingRect,Point(X,Y)) then exit;
  Bitmap := Integer(Mem.lpSurface)+(Mem.Lpitch*Y)+(X shl 2);
  Invert:=255-Alpha;
  RValue:=Mem.ddpfPixelFormat.dwRBitMask and ((((Color and Mem.ddpfPixelFormat.dwRBitMask)*Alpha)+((PLongWord(Bitmap)^ and Mem.ddpfPixelFormat.dwRBitMask)*Invert)) shr 8);
  GValue:=Mem.ddpfPixelFormat.dwGBitMask and ((((Color and Mem.ddpfPixelFormat.dwGBitMask)*Alpha)+((PLongWord(Bitmap)^ and Mem.ddpfPixelFormat.dwGBitMask)*Invert)) shr 8);
  BValue:=Mem.ddpfPixelFormat.dwBBitMask and ((((Color and Mem.ddpfPixelFormat.dwBBitMask)*Alpha)+((PLongWord(Bitmap)^ and Mem.ddpfPixelFormat.dwBBitMask)*Invert)) shr 8);
  PLongWord(Bitmap)^:=Rvalue or GValue or BValue;
end;

procedure BlendRectangle(Rect: TRect; Alpha: Cardinal; Color: TBlendColor;Surface: TDirectDrawSurface);
var
  RMask,BMask,GMask    : Cardinal;
  Invert               : Cardinal;
  Mem                  : TDDSurfaceDesc;
  Pointer              : Cardinal;
  ColR,ColB,ColG       : Cardinal;
  Pixels               : Cardinal;
  RValue               : Cardinal;
  BValue               : Cardinal;
  GValue               : Cardinal;
  Weite,Spalte,Zeile   : Integer;
begin
  if Alpha=0 then
    exit;
  ClipRect(Rect,Surface.ClippingRect);
  if IsRectEmpty(Rect) then
    exit;
  Surface.Canvas.Release;
  Surface.Lock(Mem);
  RMask:=Mem.ddpfPixelFormat.dwRBitMask;
  BMask:=Mem.ddpfPixelFormat.dwBBitMask;
  GMask:=Mem.ddpfPixelFormat.dwGBitMask;
  Alpha:=max(0,min(255,round(AlphaBlendFaktor*Alpha)));
  Invert:=255-Alpha;
  ColR:=(Color and RMask)*Alpha;
  ColG:=(Color and GMask)*Alpha;
  ColB:=(Color and BMask)*Alpha;
  Weite:=(Rect.Right-Rect.Left);
  Pixels:=Weite*((Rect.Bottom-Rect.Top)+1);
  Spalte:=1;
  Zeile:=Rect.Top;
  if Mode32Bit then
  begin
    Pointer:=Integer(Mem.lpSurface)+(Zeile*Mem.lPitch)+(Rect.Left shl 2);
    while (Pixels>0) do
    begin
      if Spalte>Weite then
      begin
        inc(Zeile);
        if (Zeile>=Surface.ClippingRect.Bottom-1) then
          break;
        Pointer:=Integer(Mem.lpSurface)+(Zeile*Mem.lPitch)+(Rect.Left shl 2);
        Spalte:=1;
      end;
      RValue:=(RMask and ((ColR+(PInteger(Pointer)^ and RMask)*Invert) shr 8));
      GValue:=(GMask and ((ColG+(PInteger(Pointer)^ and GMask)*Invert) shr 8));
      BValue:=(BMask and ((ColB+(PInteger(Pointer)^ and BMask)*Invert) shr 8));
      PLongWord(Pointer)^:=(RValue or GValue or BValue);
      inc(Pointer,4);
      inc(Spalte);
      dec(Pixels);
    end;
  end
  else
  begin
    Pointer:=Integer(Mem.lpSurface)+(Zeile*Mem.lPitch)+(Rect.Left shl 1);
    while (Pixels>0) do
    begin
      if Spalte>Weite then
      begin
        inc(Zeile);
        if (Zeile>=Surface.ClippingRect.Bottom-1) then
          break;
        Pointer:=Integer(Mem.lpSurface)+(Zeile*Mem.lPitch)+(Rect.Left shl 1);
        Spalte:=1;
      end;
      RValue:=(RMask and ((ColR+(PInteger(Pointer)^ and RMask)*Invert) shr 8));
      GValue:=(GMask and ((ColG+(PInteger(Pointer)^ and GMask)*Invert) shr 8));
      BValue:=(BMask and ((ColB+(PInteger(Pointer)^ and BMask)*Invert) shr 8));
      PWord(Pointer)^:=RValue or GValue or BValue;
      inc(Pointer,2);
      inc(Spalte);
      dec(Pixels);
    end;
  end;
  Surface.UnLock;
end;

{ Blendet ein Rechteck auf einem Surface, dass bereits gelocked ist }
procedure BlendRectangle(Rect: TRect; Alpha: Cardinal; Color: TBlendColor;Surface: TDirectDrawSurface;Mem : TDDSurfaceDesc);overload;
var
  RMask,BMask,GMask    : Cardinal;
  Invert               : Cardinal;
  Pointer              : Cardinal;
  ColR,ColB,ColG       : Cardinal;
  Pixels               : Cardinal;
  RValue               : Cardinal;
  BValue               : Cardinal;
  GValue               : Cardinal;
  Weite,Spalte,Zeile   : Integer;
begin
  ClipRect(Rect,Surface.ClippingRect);
  if IsRectEmpty(Rect) then exit;
  RMask:=Mem.ddpfPixelFormat.dwRBitMask;
  BMask:=Mem.ddpfPixelFormat.dwBBitMask;
  GMask:=Mem.ddpfPixelFormat.dwGBitMask;
  Alpha:=max(0,min(255,round(AlphaBlendFaktor*Alpha)));
  Invert:=255-Alpha;
  ColR:=(Color and RMask)*Alpha;
  ColG:=(Color and GMask)*Alpha;
  ColB:=(Color and BMask)*Alpha;
  Weite:=(Rect.Right-Rect.Left);
  Pixels:=Weite*((Rect.Bottom-Rect.Top)+1);
  Spalte:=1;
  Zeile:=Rect.Top;
  if Mode32Bit then
  begin
    Pointer:=Integer(Mem.lpSurface)+(Zeile*Mem.lPitch)+(Rect.Left shl 2);
    while (Pixels>0) do
    begin
      if Spalte>Weite then
      begin
        inc(Zeile);
        if (Zeile>=Surface.ClippingRect.Bottom) then
          break;
        Pointer:=Integer(Mem.lpSurface)+(Zeile*Mem.lPitch)+(Rect.Left shl 2);
        Spalte:=1;
      end;
      RValue:=(RMask and ((ColR+(PInteger(Pointer)^ and RMask)*Invert) shr 8));
      GValue:=(GMask and ((ColG+(PInteger(Pointer)^ and GMask)*Invert) shr 8));
      BValue:=(BMask and ((ColB+(PInteger(Pointer)^ and BMask)*Invert) shr 8));
      PLongWord(Pointer)^:=(RValue or GValue or BValue);
      inc(Pointer,4);
      inc(Spalte);
      dec(Pixels);
    end;
  end
  else
  begin
    Pointer:=Integer(Mem.lpSurface)+(Zeile*Mem.lPitch)+(Rect.Left shl 1);
    while (Pixels>0) do
    begin
      if Spalte>Weite then
      begin
        inc(Zeile);
        if (Zeile>=Surface.ClippingRect.Bottom) then
          break;
        Pointer:=Integer(Mem.lpSurface)+(Zeile*Mem.lPitch)+(Rect.Left shl 1);
        Spalte:=1;
      end;
      RValue:=(RMask and ((ColR+(PInteger(Pointer)^ and RMask)*Invert) shr 8));
      GValue:=(GMask and ((ColG+(PInteger(Pointer)^ and GMask)*Invert) shr 8));
      BValue:=(BMask and ((ColB+(PInteger(Pointer)^ and BMask)*Invert) shr 8));
      PWord(Pointer)^:=RValue or GValue or BValue;
      inc(Pointer,2);
      inc(Spalte);
      dec(Pixels);
    end;
  end;
end;

procedure BlendPercentBar(Rect: TRect; Alpha: Cardinal; Color1, Color2: TBlendColor;Percent: double; Surface: TDirectDrawSurface;Mem : TDDSurfaceDesc);
var
  Wid   : Integer;
  First : Integer;
begin
  Wid:=Rect.Right-Rect.Left;
  First:=round(Wid*Percent);
  Rect.Right:=Rect.Left+First;
  BlendRectangle(Rect,Alpha,Color1,Surface,Mem);

  Rect.Right:=Rect.Left+Wid;
  Rect.Left:=Rect.Left+First;
  BlendRectangle(Rect,Alpha,Color2,Surface,Mem);
end;

procedure BlendRoundRect(Rect: TRect;Alpha: Cardinal;Color: TBlendColor;Surface: TDirectDrawSurface; Radius: Integer;ClipRect: TRect);
var
  Region              : HRGN;
  Mem                 : TDDSurfaceDesc;
  RMask,BMask,GMask   : Cardinal;
  Invert              : Cardinal;
  Pointer             : Cardinal;
  X,Y                 : integer;
  vomRand             : Integer;
  Ende                : Integer;
  ColR,ColB,ColG      : Cardinal;
  RValue              : Cardinal;
  BValue              : Cardinal;
  GValue              : Cardinal;
  YEnd                : Integer;
begin
  if IsRectEmpty(Rect) or IsRectEmpty(ClipRect) then exit;
  CorrectRect(ClipRect);
   InterSectRect(ClipRect,ClipRect,Surface.ClippingRect);
  Region:=CreateRoundRectRgn(Rect.Left,Rect.Top,Rect.Right+1,Rect.Bottom+1,Radius,Radius);
  Surface.Canvas.Release;
  Surface.Lock(PRect(nil)^,Mem);
  RMask:=Mem.ddpfPixelFormat.dwRBitMask;
  BMask:=Mem.ddpfPixelFormat.dwBBitMask;
  GMask:=Mem.ddpfPixelFormat.dwGBitMask;
  Alpha:=max(0,min(255,round(AlphaBlendFaktor*Alpha)));
  Invert:=255-Alpha;
  ColR:=(Color and RMask)*Alpha;
  ColG:=(Color and GMask)*Alpha;
  ColB:=(Color and BMask)*Alpha;
  Y:=max(ClipRect.Top,Rect.Top+1);
  YEnd:=min(ClipRect.Bottom,Rect.Bottom-1);
  if Mode32Bit then
  begin
    While Y<YEnd do
    begin
      X:=Rect.Left;
      Ende:=(ClipRect.Left+((ClipRect.Right-ClipRect.Left) shr 1));
      while (not (PtInRegion(Region,X,Y))) and (X<Ende) do
      begin
        inc(X);
      end;
      if X<Ende then
      begin
        vomRand:=X-Rect.Left;
        X:=max(X,ClipRect.Left);
        Pointer:=Integer(Mem.lpSurface)+(Y*Mem.LPitch)+(X shl 2);
        Ende:=min(ClipRect.Right,Rect.Right-vomRand-1);
        while (X<Ende) do
        begin
          RValue:=(RMask and ((ColR+(PInteger(Pointer)^ and RMask)*Invert) shr 8));
          GValue:=(GMask and ((ColG+(PInteger(Pointer)^ and GMask)*Invert) shr 8));
          BValue:=(BMask and ((ColB+(PInteger(Pointer)^ and BMask)*Invert) shr 8));
          PLongWord(Pointer)^:=(RValue or GValue or BValue);
          inc(Pointer,4);
          inc(X);
        end;
      end;
      inc(Y);
    end;
  end
  else
  begin
    While Y<YEnd do
    begin
      X:=Rect.Left;
      Ende:=(ClipRect.Left+((ClipRect.Right-ClipRect.Left) shr 1));
      while (not (PtInRegion(Region,X,Y))) and (X<Ende) do
      begin
        inc(X);
      end;
      if X<Ende then
      begin
        vomRand:=X-Rect.Left;
        X:=max(X,ClipRect.Left);
        Pointer:=Integer(Mem.lpSurface)+(Y*Mem.LPitch)+(X shl 1);
        Ende:=min(ClipRect.Right,Rect.Right-vomRand-1);
        while (X<Ende) do
        begin
          RValue:=(RMask and ((ColR+(PInteger(Pointer)^ and RMask)*Invert) shr 8));
          GValue:=(GMask and ((ColG+(PInteger(Pointer)^ and GMask)*Invert) shr 8));
          BValue:=(BMask and ((ColB+(PInteger(Pointer)^ and BMask)*Invert) shr 8));
          PWord(Pointer)^:=RValue or GValue or BValue;
          inc(Pointer,2);
          inc(X);
        end;
      end;
      inc(Y);
    end;
  end;
  Surface.UnLock;
  DeleteObject(Region);
end;

{ Blendet ein abgerundetes Rechteck auf einem Surface, dass bereits gelocked ist }
procedure BlendRoundRect(Rect: TRect;Alpha: Cardinal;Color: TBlendColor;Surface: TDirectDrawSurface;Mem: TDDSurfaceDesc; Radius: Integer;Corners: TCorners;ClipRect: TRect);overload;
var
  Invert              : Cardinal;
  Pointer             : Cardinal;
  X,Y                 : integer;
  vomRand             : Integer;
  Ende                : Integer;
  ColR,ColB,ColG      : Cardinal;
  RValue              : Cardinal;
  BValue              : Cardinal;
  GValue              : Cardinal;
  YEnd                : Integer;
  BegCen,EndCen       : Integer;
  FromX,ToX           : Integer;
  majorAxis           : Integer;
  minorAxis           : Integer;
  Line                : Integer;
begin
  if IsRectEmpty(Rect) or IsRectEmpty(ClipRect) then
    exit;
  InterSectRect(ClipRect,ClipRect,Surface.ClippingRect);
  if not OverlapRect(ClipRect,Rect) then
    exit;
  Alpha:=max(0,min(255,round(AlphaBlendFaktor*Alpha)));
  Invert:=255-Alpha;
  if (cRightTop in Corners) then
    dec(Rect.Right);
  ColR:=(Color and Mem.ddpfPixelFormat.dwRBitMask)*Alpha;
  ColG:=(Color and Mem.ddpfPixelFormat.dwGBitMask)*Alpha;
  ColB:=(Color and Mem.ddpfPixelFormat.dwBBitMask)*Alpha;
  if (cLeftTop in Corners) or (cRightTop in Corners) then
  begin
    BegCen:=Rect.Top+Radius+1;
    MajorAxis:=Radius;
    minorAxis:=0;
    for Y:=Rect.Top to Rect.Top+Radius do
    begin
      if cLeftTop in Corners then FromX:=Rect.Left+(Radius-MinorAxis) else FromX:=Rect.Left;
      if cRightTop in Corners then ToX:=Rect.Right-(Radius-MinorAxis) else ToX:=Rect.Right;
      dec(majorAxis);
      minorAxis := round(sqrt((radius*radius) - (majorAxis * majorAxis))+0.5);
      Line:=Y;
      if (Line>=ClipRect.Bottom) or (Line<ClipRect.Top) then continue;
      FromX:=max(ClipRect.Left,FromX);
      ToX:=min(ClipRect.Right,ToX);
      if FromX=ToX then continue;
      if Mode32Bit then
      begin
        Pointer:=Integer(Mem.lpSurface)+(Line*Mem.LPitch)+(FromX shl 2);
        if AlphaElements then
        begin
          while (FromX<ToX) do
          begin
            RValue:=(Mem.ddpfPixelFormat.dwRBitMask and ((ColR+(PInteger(Pointer)^ and Mem.ddpfPixelFormat.dwRBitMask)*Invert) shr 8));
            GValue:=(Mem.ddpfPixelFormat.dwGBitMask and ((ColG+(PInteger(Pointer)^ and Mem.ddpfPixelFormat.dwGBitMask)*Invert) shr 8));
            BValue:=(Mem.ddpfPixelFormat.dwBBitMask and ((ColB+(PInteger(Pointer)^ and Mem.ddpfPixelFormat.dwBBitMask)*Invert) shr 8));
            PLongWord(Pointer)^:=(RValue or GValue or BValue);
            inc(Pointer,4);
            inc(FromX);
          end;
        end
        else
        begin
          while (FromX<ToX) do
          begin
            PLongWord(Pointer)^:=Color;
            inc(Pointer,4);
            inc(FromX);
          end;
        end;
      end
      else
      begin
        Pointer:=Integer(Mem.lpSurface)+(Line*Mem.LPitch)+(FromX*2);
        if AlphaElements then
        begin
          while (FromX<ToX) do
          begin
            RValue:=(Mem.ddpfPixelFormat.dwRBitMask and ((ColR+(PInteger(Pointer)^ and Mem.ddpfPixelFormat.dwRBitMask)*Invert) shr 8));
            GValue:=(Mem.ddpfPixelFormat.dwGBitMask and ((ColG+(PInteger(Pointer)^ and Mem.ddpfPixelFormat.dwGBitMask)*Invert) shr 8));
            BValue:=(Mem.ddpfPixelFormat.dwBBitMask and ((ColB+(PInteger(Pointer)^ and Mem.ddpfPixelFormat.dwBBitMask)*Invert) shr 8));
            PWord(Pointer)^:=RValue or GValue or BValue;
            inc(Pointer,2);
            inc(FromX);
          end;
        end
        else
        begin
          while (FromX<ToX) do
          begin
            PWord(Pointer)^:=Color;
            inc(Pointer,2);
            inc(FromX);
          end;
        end;
      end;
    end;
  end
  else BegCen:=Rect.Top;
  if (cRightTop in Corners) then inc(Rect.Right);
  if (cLeftBottom in Corners) or (cRightBottom in Corners) then
  begin
    EndCen:=Rect.Bottom-Radius-1;
    MajorAxis:=0;
    minorAxis:=Radius;
    for Y:=Rect.Bottom-Radius to Rect.Bottom-1 do
    begin
      if cLeftBottom in Corners then FromX:=Rect.Left+(Radius-MinorAxis) else FromX:=Rect.Left;
      if cRightBottom in Corners then ToX:=Rect.Right-(Radius-MinorAxis) else ToX:=Rect.Right;
      inc(majorAxis);
      minorAxis := round(sqrt((radius*radius) - (majorAxis * majorAxis))-0.5);
      Line:=Y;
      if (Line>=ClipRect.Bottom) or (Line<ClipRect.Top) then continue;
      FromX:=max(ClipRect.Left,FromX);
      ToX:=min(ClipRect.Right,ToX);
      if FromX=ToX then continue;
      if Mode32Bit then
      begin
        Pointer:=Integer(Mem.lpSurface)+(Line*Mem.LPitch)+(FromX shl 2);
        if AlphaElements then
        begin
          while (FromX<ToX) do
          begin
            RValue:=(Mem.ddpfPixelFormat.dwRBitMask and ((ColR+(PInteger(Pointer)^ and Mem.ddpfPixelFormat.dwRBitMask)*Invert) shr 8));
            GValue:=(Mem.ddpfPixelFormat.dwGBitMask and ((ColG+(PInteger(Pointer)^ and Mem.ddpfPixelFormat.dwGBitMask)*Invert) shr 8));
            BValue:=(Mem.ddpfPixelFormat.dwBBitMask and ((ColB+(PInteger(Pointer)^ and Mem.ddpfPixelFormat.dwBBitMask)*Invert) shr 8));
            PLongWord(Pointer)^:=(RValue or GValue or BValue);
            inc(Pointer,4);
            inc(FromX);
          end;
        end
        else
        begin
          while (FromX<ToX) do
          begin
            PLongWord(Pointer)^:=Color;
            inc(Pointer,4);
            inc(FromX);
          end;
        end;
      end
      else
      begin
        Pointer:=Integer(Mem.lpSurface)+(Line*Mem.LPitch)+(FromX*2);
        if AlphaElements then
        begin
          while (FromX<ToX) do
          begin
            RValue:=(Mem.ddpfPixelFormat.dwRBitMask and ((ColR+(PInteger(Pointer)^ and Mem.ddpfPixelFormat.dwRBitMask)*Invert) shr 8));
            GValue:=(Mem.ddpfPixelFormat.dwGBitMask and ((ColG+(PInteger(Pointer)^ and Mem.ddpfPixelFormat.dwGBitMask)*Invert) shr 8));
            BValue:=(Mem.ddpfPixelFormat.dwBBitMask and ((ColB+(PInteger(Pointer)^ and Mem.ddpfPixelFormat.dwBBitMask)*Invert) shr 8));
            PWord(Pointer)^:=RValue or GValue or BValue;
            inc(Pointer,2);
            inc(FromX);
          end;
        end
        else
        begin
          while (FromX<ToX) do
          begin
            PWord(Pointer)^:=Color;
            inc(Pointer,2);
            inc(FromX);
          end;
        end;
      end;
    end;
  end
  else EndCen:=Rect.Bottom-1;
  FromX:=max(ClipRect.Left,Rect.Left);
  ToX:=min(ClipRect.Right,Rect.Right);
  if FromX=ToX then exit;
  for Y:=BegCen to EndCen do
  begin
    Line:=Y;
    if (Line>=ClipRect.Bottom) or (Line<ClipRect.Top) then continue;
    if Mode32Bit then
    begin
      Pointer:=Integer(Mem.lpSurface)+(Line*Mem.LPitch)+(FromX shl 2);
      if AlphaElements then
      begin
        while (FromX<ToX) do
        begin
          RValue:=(Mem.ddpfPixelFormat.dwRBitMask and ((ColR+(PInteger(Pointer)^ and Mem.ddpfPixelFormat.dwRBitMask)*Invert) shr 8));
          GValue:=(Mem.ddpfPixelFormat.dwGBitMask and ((ColG+(PInteger(Pointer)^ and Mem.ddpfPixelFormat.dwGBitMask)*Invert) shr 8));
          BValue:=(Mem.ddpfPixelFormat.dwBBitMask and ((ColB+(PInteger(Pointer)^ and Mem.ddpfPixelFormat.dwBBitMask)*Invert) shr 8));
          PLongWord(Pointer)^:=(RValue or GValue or BValue);
          inc(Pointer,4);
          inc(FromX);
        end;
      end
      else
      begin
        while (FromX<ToX) do
        begin
          PLongWord(Pointer)^:=Color;
          inc(Pointer,4);
          inc(FromX);
        end;
      end;
    end
    else
    begin
      Pointer:=Integer(Mem.lpSurface)+(Line*Mem.LPitch)+(FromX*2);
      if AlphaElements then
      begin
        while (FromX<ToX) do
        begin
          RValue:=(Mem.ddpfPixelFormat.dwRBitMask and ((ColR+(PInteger(Pointer)^ and Mem.ddpfPixelFormat.dwRBitMask)*Invert) shr 8));
          GValue:=(Mem.ddpfPixelFormat.dwGBitMask and ((ColG+(PInteger(Pointer)^ and Mem.ddpfPixelFormat.dwGBitMask)*Invert) shr 8));
          BValue:=(Mem.ddpfPixelFormat.dwBBitMask and ((ColB+(PInteger(Pointer)^ and Mem.ddpfPixelFormat.dwBBitMask)*Invert) shr 8));
          PWord(Pointer)^:=RValue or GValue or BValue;
          inc(Pointer,2);
          inc(FromX);
        end;
      end
      else
      begin
        while (FromX<ToX) do
        begin
          PWord(Pointer)^:=Color;
          inc(Pointer,2);
          inc(FromX);
        end;
      end;
    end;
    FromX:=max(ClipRect.Left,Rect.Left);
    ToX:=min(ClipRect.Right,Rect.Right);
  end;
end;

procedure BlendFrameRoundRect(Rect: TRect; Alpha: Cardinal; Color: TBlendColor;Surface: TDirectDrawSurface;Radius: Integer;ClipRect: TRect;FrameRect: TRect;FrameColor: TColor);
var
  Region            : HRGN;
  Mem               : TDDSurfaceDesc;
  RMask,BMask,GMask : Cardinal;
  Invert            : Cardinal;
  Pointer           : Cardinal;
  X,Y               : Integer;
  vomRand           : Integer;
  Ende              : Integer;
  ColR,ColB,ColG    : Integer;
  RValue            : Cardinal;
  BValue            : Cardinal;
  GValue            : Cardinal;
  YEnd              : Integer;
begin
  if IsRectEmpty(Rect) or IsRectEmpty(ClipRect) then exit;
  CorrectRect(ClipRect);
  Region:=CreateRoundRectRgn(Rect.Left,Rect.Top,Rect.Right+1,Rect.Bottom+1,Radius,Radius);
  Surface.Canvas.Release;
  Surface.Lock(PRect(nil)^,Mem);
  RMask:=Mem.ddpfPixelFormat.dwRBitMask;
  BMask:=Mem.ddpfPixelFormat.dwBBitMask;
  GMask:=Mem.ddpfPixelFormat.dwGBitMask;
  Alpha:=max(0,min(255,round(AlphaBlendFaktor*Alpha)));
  Invert:=255-Alpha;
  ColR:=(Color and RMask)*Alpha;
  ColG:=(Color and GMask)*Alpha;
  ColB:=(Color and BMask)*Alpha;
  Y:=max(ClipRect.Top,Rect.Top+1);
  YEnd:=min(ClipRect.Bottom,Rect.Bottom-1);
  if Mode32Bit then
  begin
    While Y<YEnd do
    begin
      X:=Rect.Left;
      Ende:=(ClipRect.Left+((ClipRect.Right-ClipRect.Left) shr 1));
      while (not (PtInRegion(Region,X,Y))) and (X<Ende) do
      begin
        inc(X);
      end;
      vomRand:=X-Rect.Left;
      X:=max(X,ClipRect.Left);
      Pointer:=Integer(Mem.lpSurface)+(Y*Mem.LPitch)+(X shl 2);
      Ende:=min(ClipRect.Right,Rect.Right-vomRand-1);
      while (X<Ende) do
      begin
        RValue:=(RMask and ((ColR+(PInteger(Pointer)^ and RMask)*Invert) shr 8));
        GValue:=(GMask and ((ColG+(PInteger(Pointer)^ and GMask)*Invert) shr 8));
        BValue:=(BMask and ((ColB+(PInteger(Pointer)^ and BMask)*Invert) shr 8));
        PLongWord(Pointer)^:=(RValue or GValue or BValue);
        inc(Pointer,4);
        inc(X);
      end;
      inc(Y);
    end;
  end
  else
  begin
    While Y<YEnd do
    begin
      X:=Rect.Left;
      Ende:=(ClipRect.Left+((ClipRect.Right-ClipRect.Left) shr 1));
      while (not (PtInRegion(Region,X,Y))) and (X<Ende) do
      begin
        inc(X);
      end;
      vomRand:=X-Rect.Left;
      X:=max(X,ClipRect.Left);
      Pointer:=Integer(Mem.lpSurface)+(Y*Mem.LPitch)+(X shl 1);
      Ende:=min(ClipRect.Right,Rect.Right-vomRand-1);
      while (X<Ende) do
      begin
        RValue:=(RMask and ((ColR+(PInteger(Pointer)^ and RMask)*Invert) shr 8));
        GValue:=(GMask and ((ColG+(PInteger(Pointer)^ and GMask)*Invert) shr 8));
        BValue:=(BMask and ((ColB+(PInteger(Pointer)^ and BMask)*Invert) shr 8));
        PWord(Pointer)^:=RValue or GValue or BValue;
        inc(Pointer,2);
        inc(X);
      end;
      inc(Y);
    end;
  end;
  Surface.UnLock;
  FrameRegion(Region,FrameRect,FrameColor,Surface);
  DeleteObject(Region);
end;

procedure FrameRegion(Region: HRGN;FrameRect: TRect;Color: TColor;Surface: TDirectDrawSurface);
var
  FrameRegion : HRGN;
  TempColor   : TColor;
  Style       : TBrushStyle;
begin
  with FrameRect do
  begin
    FrameRegion:=CreateRectRgn(Left,Top,Right,Bottom);
  end;
  CombineRgn(FrameRegion,Region,FrameRegion,RGN_AND);
  with Surface.Canvas do
  begin
    TempColor:=Brush.Color;
    Style:=Brush.Style;
    Brush.Color:=Color;
    FrameRgn(Handle,FrameRegion,Brush.Handle,1,1);
    Brush.Color:=TempColor;;
    Brush.Style:=Style;
    Release;
  end;
  DeleteObject(FrameRegion);
end;

procedure DrawTransAlpha(Surface: TDirectDrawSurface;X,Y: Integer;SrcRect: TRect;Source: TDirectDrawSurface;Alpha: Integer;TransColor: TBlendColor);
var
  MemSur            : TDDSurfaceDesc;
  MemSource         : TDDSurfaceDesc;
  PoiSur            : Cardinal;
  RMask,BMask,GMask : Cardinal;
  PoiSource         : Cardinal;
  relX,relY         : Integer;
  absX,absY         : Integer;
  CurX,CurY         : Integer;
  Height,Width      : Integer;
  Invert            : Integer;
  RValue            : Cardinal;
  BValue            : Cardinal;
  GValue            : Cardinal;
begin
//  Source.TransparentColor:=TransColor;
//  Surface.DrawAlpha(Rect(X,Y,X+(SrcRect.Right-SrcRect.Left),Y+(SrcRect.Bottom-SrcRect.Top)),SrcRect,Source,true,Alpha);
//  exit;
  Surface.Canvas.Release;
  Source.Lock(PRect(nil)^,MemSource);
  Surface.Lock(PRect(nil)^,MemSur);
  RMask:=MemSource.ddpfPixelFormat.dwRBitMask;
  BMask:=MemSource.ddpfPixelFormat.dwBBitMask;
  GMask:=MemSource.ddpfPixelFormat.dwGBitMask;
  AbsY:=Y;
  Invert:=255-Alpha;
  CurY:=SrcRect.Top;
  Height:=SrcRect.Bottom-SrcRect.Top;
  Width:=SrcRect.Right-SrcRect.Left;
  relY:=0;
  if Mode32Bit then
  begin
    while (AbsY<ScreenHeight) and (relY<Height) do
    begin
      relX:=0;
      AbsX:=X;
      CurX:=SrcRect.Left;
      PoiSource:=Integer(MemSource.lpSurface)+(CurY*MemSource.lPitch)+(CurX shl 2);
      PoiSur:=Integer(MemSur.lpSurface)+(AbsY*MemSur.lPitch)+(AbsX shl 2);
      if (AbsY>=Surface.ClippingRect.Top) and (AbsY<Surface.ClippingRect.Bottom) then
      begin
        while (AbsX<ScreenWidth) and (relX<Width) do
        begin
          if (AbsX>=Surface.ClippingRect.Left) and (AbsX<Surface.ClippingRect.Right) then
          begin
            if (PLongWord(PoiSource)^ and $00FFFFFF)<>TransColor then
            begin
              RValue:=(PLongWord(PoiSource)^ and RMask)*Alpha;
              GValue:=(PLongWord(PoiSource)^ and GMask)*Alpha;
              BValue:=(PLongWord(PoiSource)^ and BMask)*Alpha;
              RValue:=(RMask and ((RValue+(PInteger(PoiSur)^ and RMask)*Invert) shr 8));
              GValue:=(GMask and ((GValue+(PInteger(PoiSur)^ and GMask)*Invert) shr 8));
              BValue:=(BMask and ((BValue+(PInteger(PoiSur)^ and BMask)*Invert) shr 8));
              PLongWord(PoiSur)^:=RValue or GValue or BValue;
            end;
          end;
          inc(PoiSur,4);
          inc(PoiSource,4);
          inc(relX);
          inc(absX);
        end;
      end;
      inc(relY);
      inc(AbsY);
      inc(CurY);
    end;
  end
  else
  begin
    while (AbsY<ScreenHeight) and (relY<Height) do
    begin
      relX:=0;
      AbsX:=X;
      CurX:=SrcRect.Left;
      PoiSource:=Integer(MemSource.lpSurface)+(CurY*MemSource.lPitch)+(CurX shl 1);
      PoiSur:=Integer(MemSur.lpSurface)+(AbsY*MemSur.lPitch)+(AbsX shl 1);
      if (AbsY>=Surface.ClippingRect.Top) and (AbsY<Surface.ClippingRect.Bottom) then
      begin
        while (AbsX<ScreenWidth) and (relX<Width) do
        begin
          if (AbsX>=Surface.ClippingRect.Left) and (AbsX<Surface.ClippingRect.Right) then
          begin
            if PWord(PoiSource)^<>TransColor then
            begin
              RValue:=(PWord(PoiSource)^ and RMask)*Alpha;
              GValue:=(PWord(PoiSource)^ and GMask)*Alpha;
              BValue:=(PWord(PoiSource)^ and BMask)*Alpha;
              RValue:=(RMask and ((RValue+(PWord(PoiSur)^ and RMask)*Invert) shr 8));
              GValue:=(GMask and ((GValue+(PWord(PoiSur)^ and GMask)*Invert) shr 8));
              BValue:=(BMask and ((BValue+(PWord(PoiSur)^ and BMask)*Invert) shr 8));
              PWord(PoiSur)^:=RValue or GValue or BValue;
            end;
          end;
          inc(PoiSur,2);
          inc(PoiSource,2);
          inc(relX);
          inc(absX);
        end;
      end;
      inc(relY);
      inc(AbsY);
      inc(CurY);
    end;
  end;
  Source.UnLock;
  Surface.UnLock;
end;

procedure DrawShadow(Surface: TDirectDrawSurface;X,Y: Integer;SrcRect: TRect;Source: TDirectDrawSurface;TransparentColor: TBlendColor);
var
  MemSur            : TDDSurfaceDesc;
  MemSource         : TDDSurfaceDesc;
  PoiSur            : Cardinal;
  PoiSource         : Cardinal;
  relX,relY         : Integer;
  absX,absY         : Integer;
  CurX,CurY         : Integer;
  Height,Width      : Integer;
begin
  Surface.Canvas.Release;
  Source.Lock(PRect(nil)^,MemSource);
  Surface.Lock(PRect(nil)^,MemSur);
  AbsY:=Y;
  CurY:=SrcRect.Top;
  Height:=SrcRect.Bottom-SrcRect.Top;
  Width:=SrcRect.Right-SrcRect.Left;
  relY:=0;
  if Mode32Bit then
  begin
    while (AbsY<ScreenHeight) and (relY<Height) do
    begin
      relX:=0;
      AbsX:=X;
      CurX:=SrcRect.Left;
      PoiSource:=Integer(MemSource.lpSurface)+(CurY*MemSource.lPitch)+(CurX shl 2);
      PoiSur:=Integer(MemSur.lpSurface)+(AbsY*MemSur.lPitch)+(AbsX shl 2);
      while (AbsX<ScreenWidth) and (relX<Width) do
      begin
        if (PLongWord(PoiSource)^ and $00FFFFFF)<>TransparentColor then
        begin
          if AlphaElements then
          begin
            PLongWord(PoiSur)^:=((PLongWord(PoiSur)^ and Mask) shr 1);
          end
          else PInteger(PoiSur)^:=0;
        end;
        inc(PoiSur,4);
        inc(PoiSource,4);
        inc(relX);
        inc(absX);
      end;
      inc(relY);
      inc(AbsY);
      inc(CurY);
    end;
  end
  else
  begin
    while (AbsY<ScreenHeight) and (relY<Height) do
    begin
      relX:=0;
      AbsX:=X;
      CurX:=SrcRect.Left;
      PoiSource:=Integer(MemSource.lpSurface)+(CurY*MemSource.lPitch)+(CurX shl 1);
      PoiSur:=Integer(MemSur.lpSurface)+(AbsY*MemSur.lPitch)+(AbsX shl 1);
      while (AbsX<ScreenWidth) and (relX<Width) do
      begin
        if PWord(PoiSource)^<>TransparentColor then
        begin
          if AlphaElements then
          begin
            PWord(PoiSur)^:=(PWord(PoiSur)^ and Mask) shr 1;
          end
          else PWord(PoiSur)^:=0;
        end;
        inc(PoiSur,2);
        inc(PoiSource,2);
        inc(relX);
        inc(absX);
      end;
      inc(relY);
      inc(AbsY);
      inc(CurY);
    end;
  end;
  Source.UnLock;
  Surface.UnLock;
end;

procedure DrawShieldEffect(Surface: TDirectDrawSurface;X,Y: Integer;SrcRect: TRect;Source: TDirectDrawSurface;Color: TBlendColor;Alpha: Cardinal);
var
  MemSur            : TDDSurfaceDesc;
  MemSource         : TDDSurfaceDesc;
  PoiSur            : Cardinal;
  PoiSource         : Cardinal;
  relX,relY         : Integer;
  absX,absY         : Integer;
  CurX,CurY         : Integer;
  Height,Width      : Integer;
  Invert            : Cardinal;
  RMask,BMask,GMask : Cardinal;
  RValue            : Cardinal;
  BValue            : Cardinal;
  GValue            : Cardinal;
  ColR,ColG,ColB    : Cardinal;
  UseAlpha          : Cardinal;
  Value             : Cardinal;
  Alshr             : Integer;
begin
  if (Source=nil) or (Surface=nil) then exit;
  Surface.Canvas.Release;
  Source.Lock(PRect(nil)^,MemSource);
  Surface.Lock(PRect(nil)^,MemSur);
  RMask:=MemSource.ddpfPixelFormat.dwRBitMask;
  BMask:=MemSource.ddpfPixelFormat.dwBBitMask;
  GMask:=MemSource.ddpfPixelFormat.dwGBitMask;
  AbsY:=Y;
  CurY:=SrcRect.Top;
  Height:=SrcRect.Bottom-SrcRect.Top;
  Width:=SrcRect.Right-SrcRect.Left;
  relY:=0;
  if not Mode32Bit then
  begin
    // Nur relevant im 16-Bit Modus
    if GMask=$0000F800 then
      Alshr:=8
    else
      Alshr:=7;
  end;
  if Mode32Bit then
  begin
    while (AbsY<ScreenHeight) and (relY<Height) do
    begin
      if AbsY>-1 then
      begin
        relX:=0;
        AbsX:=X;
        CurX:=SrcRect.Left;
        PoiSource:=Integer(MemSource.lpSurface)+(CurY*MemSource.lPitch)+(CurX shl 2);
        PoiSur:=Integer(MemSur.lpSurface)+(AbsY*MemSur.lPitch)+(AbsX shl 2);
        while (AbsX<ScreenWidth) and (relX<Width) do
        begin
          if AbsX>-1 then
          begin
            if (PLongWord(PoiSource)^ and $00FFFFFF)<>0 then
            begin
              { Zielfarbe errechnen }
              UseAlpha:=((((PLongWord(PoiSource)^ and RMask) shr 16))*Alpha) shr 8;
              Invert:=255-UseAlpha;
              ColG:=((PLongWord(PoiSource)^ and GMask)*(Color and GMask) div GMask);
              ColR:=((((PLongWord(PoiSource)^ and RMask) shr 16)*((Color and RMask) shr 16)) shl 8);
              ColB:=(((PLongWord(PoiSource)^ and BMask) shr 8)*((Color and BMask) shr 8));
              RValue:=(RMask and ((((ColR and RMask)*UseAlpha)+(PLongWord(PoiSur)^ and RMask)*Invert) shr 8));
              GValue:=(GMask and ((((ColG and GMask)*UseAlpha)+(PLongWord(PoiSur)^ and GMask)*Invert) shr 8));
              BValue:=(BMask and ((((ColB and BMask)*UseAlpha)+(PLongWord(PoiSur)^ and BMask)*Invert) shr 8));
              PLongWord(PoiSur)^:=(RValue or GValue or BValue);
            end;
          end;
          inc(PoiSur,4);
          inc(PoiSource,4);
          inc(relX);
          inc(absX);
        end;
      end;
      inc(relY);
      inc(AbsY);
      inc(CurY);
    end;
  end
  else
  begin
    ColR:=Color and RMask;
    ColG:=Color and GMask;
    ColB:=Color and BMask;
    while (AbsY<ScreenHeight) and (relY<Height) do
    begin
      if AbsY>-1 then          
      begin
        relX:=0;
        AbsX:=X;
        CurX:=SrcRect.Left;
        PoiSource:=Integer(MemSource.lpSurface)+(CurY*MemSource.lPitch)+(CurX shl 1);
        PoiSur:=Integer(MemSur.lpSurface)+(AbsY*MemSur.lPitch)+(AbsX shl 1);
        while (AbsX<ScreenWidth) and (relX<Width) do
        begin
          if AbsX>-1 then
          begin
            if PWord(PoiSource)^<>0 then
            begin
              { Zielfarbe errechnen }
              UseAlpha:=min(255,((((PWord(PoiSource)^ and RMask) shr Alshr)*Alpha) shr 8));
              Invert:=255-UseAlpha;
              GValue:=((PWord(PoiSource)^ and GMask)*(ColG) div GMask) and GMask;
              RValue:=((PWord(PoiSource)^ and RMask)*(ColR) div RMask) and RMask;
              BValue:=((PWord(PoiSource)^ and BMask)*(ColB) div BMask) and BMask;
              RValue:=(RMask and (((RValue*UseAlpha)+(PWord(PoiSur)^ and RMask)*Invert) shr 8));
              GValue:=(GMask and (((GValue*UseAlpha)+(PWord(PoiSur)^ and GMask)*Invert) shr 8));
              BValue:=(BMask and (((BValue*UseAlpha)+(PWord(PoiSur)^ and BMask)*Invert) shr 8));
              PWord(PoiSur)^:=RValue or GValue or BValue;
            end;
          end;
          inc(PoiSur,2);
          inc(PoiSource,2);
          inc(relX);
          inc(absX);
        end;
      end;
      inc(relY);
      inc(AbsY);
      inc(CurY);
    end;
  end;
  Source.UnLock;
  Surface.UnLock;
end;

procedure InitColorTable(RMask,BMask,GMask: Integer);

  procedure InitGradiant(Von,Zu: Integer;VFarbe,ZFarbe: Cardinal;var ColArray: Array of TBlendColor);
  var
    Dummy       : Integer;
    Schritte    : Integer;
    RValue,RZu  : double;
    BValue,BZu  : double;
    GValue,GZu  : double;
    RAdd        : double;
    BAdd        : double;
    GAdd        : double;
  begin
    Schritte:=Zu-Von;
    RValue:=VFarbe and RMask;
    BValue:=VFarbe and BMask;
    GValue:=VFarbe and GMask;
    RZu:=ZFarbe and RMask;
    BZu:=ZFarbe and BMask;
    GZu:=ZFarbe and GMask;
    RAdd:=(RZu-RValue)/Schritte;
    BAdd:=(BZu-BValue)/Schritte;
    GAdd:=(GZu-GValue)/Schritte;
    for Dummy:=Von to Zu do
    begin
      ColArray[Dummy]:=(round(RValue) and RMask) or (round(BValue) and BMask) or (round(GValue) and GMask);
      RValue:=RValue+RAdd;
      BValue:=BValue+BAdd;
      GValue:=GValue+GAdd;
    end;
  end;

  procedure InitWinGradiant(Von,Zu: Integer;VFarbe,ZFarbe: Cardinal;var ColArray: Array of TColor);
  var
    Dummy       : Integer;
    Schritte    : Integer;
    RValue,RZu  : double;
    BValue,BZu  : double;
    GValue,GZu  : double;
    RAdd        : double;
    BAdd        : double;
    GAdd        : double;
  begin
    Schritte:=Zu-Von;
    RValue:=VFarbe and $0000FF;
    GValue:=VFarbe and $00FF00;
    BValue:=VFarbe and $FF0000;
    RZu:=ZFarbe and $0000FF;
    GZu:=ZFarbe and $00FF00;
    BZu:=ZFarbe and $FF0000;
    RAdd:=(RZu-RValue)/Schritte;
    BAdd:=(BZu-BValue)/Schritte;
    GAdd:=(GZu-GValue)/Schritte;
    for Dummy:=Von to Zu do
    begin
      ColArray[Dummy]:=(round(RValue) and $0000FF) or (round(BValue) and $FF0000) or (round(GValue) and $00FF00);
      RValue:=RValue+RAdd;
      BValue:=BValue+BAdd;
      GValue:=GValue+GAdd;
    end;
  end;

begin
  InitGradiant(0,25,bcGreen,bcLime,LineColorTable);
  InitGradiant(25,50,bcLime,bcYellow,LineColorTable);
  InitGradiant(50,75,bcYellow,bcRed,LineColorTable);
  InitGradiant(75,100,bcRed,bcMaroon,LineColorTable);
  InitGradiant(0,100,bcGreen,bcOrange,LaserColTable);
  InitGradiant(0,100,bcNavy,bcSkyBlue,ShieldColorTable);
  InitGradiant(0,100,bcGray,bcWhite,TimeColorTable);
  InitGradiant(0,100,bcLightRed,bcMaroon,MuniColorTable);
  InitGradiant(0,100,bcRed,bcMaroon,WerkColorTable);

  InitWinGradiant(0,25,clGreen,clLime,LineWinColorTable);
  InitWinGradiant(25,50,clLime,clYellow,LineWinColorTable);
  InitWinGradiant(50,75,clYellow,clRed,LineWinColorTable);
  InitWinGradiant(75,100,clRed,clMaroon,LineWinColorTable);
end;

procedure DrawDots(X,Y: Integer;  Mem: PDDSurfaceDesc);StdCall;
var
  Pointer: Cardinal;
begin
  Pointer:=Integer(Mem.lpSurface)+(Y*Mem.LPitch)+(X shl 1);
  PWord(Pointer)^:=LineColorTable[round(((Y-MinYValue)/RangeY)*100)];
end;

procedure DrawLineGradiant(Surface: TDirectDrawSurface;Von: TPoint;Zu: TPoint;Min,Max: Integer);
var
  Mem: TDDSurfaceDesc;
  x1,x2: Integer;
  y1,y2: Integer;
begin
  x1:=Von.X;
  x2:=Zu.X;
  y1:=Von.Y;
  y2:=Zu.Y;
  MinYValue:=Min;
  MaxYValue:=Max;
  RangeY:=Max-Min;
  Surface.Canvas.Release;
  Surface.Lock(PRect(nil)^,Mem);
  LineDDA(X1,Y1,X2,Y2,@DrawDots,Integer(Addr(Mem)));
  Surface.UnLock;
end;

procedure HLine(Surface: TDirectDrawSurface;const Mem: TDDSurfaceDesc;FromX,ToX,Y: Integer;Col: TBlendColor);
var
  Left,Right : Integer;
  Pixel      : Integer;
  Length     : Integer;
  i          : Integer;
begin
  if (Y < Surface.ClippingRect.Top) then
    exit;
  if (Y >= Surface.ClippingRect.Bottom) then
    exit;
  // Determine which is left and right by value
  if FromX=ToX then exit;
  if (FromX > ToX) then
  begin
    left := ToX;
    right := FromX + 1;
  end
  else
  begin
    left := FromX;
    right := ToX + 1;
  end;
  // Clip the line
  if (left < Surface.ClippingRect.left) then
    Left := Surface.ClippingRect.left;
  if (right > Surface.ClippingRect.right) then
    Right := Surface.ClippingRect.right;
  Length:=Right-Left;
  if Length<=0 then exit;
  Pixel:=Integer(Mem.lpSurface)+(Mem.lPitch*Y);
  if Mode32Bit then
  begin
    inc(Pixel,Left shl 2);
    repeat
      PInteger(Pixel)^ := Col;
      inc(Pixel,4);
      dec(Length);
    until (Length = 0);
  end
  else
  begin
    inc(Pixel,left shl 1);
    i := Length mod 2;
    dec(Length,i);
    while i > 0 do
    begin
      PWord(Pixel)^:=Col;
      inc(Pixel,2);
      dec(i);
    end;
    if (Length > 1) then
    begin
      Col := Col or (Col shl 16);
      repeat
        PInteger(Pixel)^:=Col;
        inc(Pixel,4);
        dec(Length,2);
      until (Length = 0);
    end;
  end;
end;

procedure VLine(Surface: TDirectDrawSurface;const Mem: TDDSurfaceDesc;FromY,ToY,X: Integer;Col: TBlendColor);
var
  Top,Bottom : Integer;
  Pixel      : Integer;
  Length     : Integer;
begin
  if (X < Surface.ClippingRect.Left) then
    exit;
  if (X >= Surface.ClippingRect.Right) then
    exit;
  if FromY=ToY then exit;
  if (FromY > ToY) then
  begin
    Top := ToY;
    Bottom := FromY + 1;
  end
  else
  begin
    Top := FromY;
    Bottom := ToY + 1;
  end;
  // Clip the line
  if (Top < Surface.ClippingRect.Top) then
    Top := Surface.ClippingRect.Top;
  if (Bottom > Surface.ClippingRect.Bottom) then
    Bottom := Surface.ClippingRect.Bottom;
  Length:=Bottom-Top;
  if Length<=0 then exit;
  Pixel:=Integer(Mem.lpSurface)+(Mem.lPitch*Top);
  if Mode32Bit then
  begin
    Pixel:= Pixel+(X shl 2);
    repeat
      PInteger(Pixel)^ := Col;
      inc(Pixel,Mem.lPitch);
      dec(Length);
    until (Length = 0);
  end
  else
  begin
    inc(Pixel,X shl 1);
    repeat
      PWord(Pixel)^ := Col;
      inc(Pixel,Mem.lPitch);
      dec(Length);
    until (Length = 0);
  end;
end;

procedure Line(Surface: TDirectDrawSurface;const Mem: TDDSurfaceDesc;X1,Y1,X2,Y2: Integer; Col: TBlendColor);
var
  xStep    : double;
  yStep    : double;
  X,Y      : double;
  xLength  : Integer;
  yLength  : Integer;
  xCount   : Integer;
  yCount   : Integer;
  temp     : Integer;
  PutPixel : TPutPixel;
begin
  // If the line is horizontal or vertical use the fast version.
  if (X1 = X2) then
  begin
    VLine(Surface,Mem,Y1,Y2,X1,Col);
    exit;
  end
  else if (Y1 = Y2) then
  begin
    HLine(Surface,Mem,X1,X2,Y1,Col);
    exit;
  end;
  if Mode32Bit then PutPixel:=PutPixel32 else PutPixel:=PutPixel16;
  xLength := abs(X2 - X1);
  YLength := abs(Y2 - Y1);
  if (xLength > yLength) then
  begin
    if(X1 > X2) then
    begin
      Temp:=X1;X1:=X2;X2:=Temp;
      Temp:=Y1;Y1:=Y2;Y2:=Temp;
    end;
    yStep := (Y2 - Y1) / (X2 - X1);
    Y := Y1;
    for xCount := X1 To X2 do
    begin
      PutPixel(Surface,Mem,xCount, round(Y), Col);
      Y := Y+yStep;
    end;
  end
  else
  begin
    if(Y1 > Y2) then
    begin
      Temp:=X1;X1:=X2;X2:=Temp;
      Temp:=Y1;Y1:=Y2;Y2:=Temp;
    end;
    xStep := (X2 - X1) / (Y2 - Y1);
    X := X1;
    for yCount := Y1 to Y2 do
    begin
      PutPixel(Surface,Mem,Round(X), yCount, Col);
      X := X+xStep;
    end;
  end;
end;

procedure LineAA(Surface: TDirectDrawSurface;const Mem: TDDSurfaceDesc;X1,Y1,X2,Y2: Integer; Col: TBlendColor; AlphaBlend: Integer);
var
  xStep    : double;
  yStep    : double;
  X,Y      : double;
  xLength  : Integer;
  yLength  : Integer;
  xCount   : Integer;
  yCount   : Integer;
  temp     : Integer;
  PutPixel : TPutPixelAA;
begin
  AlphaBlend:=max(0,min(255,round(AlphaBlendFaktor*AlphaBlend)));
  if AlphaBlend>=255 then
  begin
    Line(Surface,Mem,X1,Y1,X2,Y2,Col);
    exit;
  end;

  if Mode32Bit then PutPixel:=PutAAPixel32 else PutPixel:=PutAAPixel16;
  xLength := abs(X2 - X1);
  YLength := abs(Y2 - Y1);
  if (xLength > yLength) then
  begin
    if(X1 > X2) then
    begin
      Temp:=X1;X1:=X2;X2:=Temp;
      Temp:=Y1;Y1:=Y2;Y2:=Temp;
    end;
    yStep := (Y2 - Y1) / (X2 - X1);
    Y := Y1;
    for xCount := X1 To X2 do
    begin
      PutPixel(Surface,Mem,xCount, round(Y), Col,AlphaBlend);
      Y := Y+yStep;
    end;
  end
  else
  begin
    if(Y1 > Y2) then
    begin
      Temp:=X1;X1:=X2;X2:=Temp;
      Temp:=Y1;Y1:=Y2;Y2:=Temp;
    end;
    if Y2-Y1=0 then
      exit
    else
      xStep := (X2 - X1) / (Y2 - Y1);
    X := X1;
    for yCount := Y1 to Y2 do
    begin
      PutPixel(Surface,Mem,Round(X), yCount, Col, AlphaBlend);
      X := X+xStep;
    end;
  end;
end;

procedure Rectangle(Surface: TDirectDrawSurface;const Mem: TDDSurfaceDesc;X1,Y1,X2,Y2: Integer;Col: TBlendColor);
begin
  if X1=X2 then exit;
  dec(X2);
  dec(Y2);
  HLine(Surface,Mem,X1,X2,Y1,Col);
  HLine(Surface,Mem,X1,X2,Y2,Col);
  VLine(Surface,Mem,Y1,Y2,X1,Col);
  VLine(Surface,Mem,Y1,Y2,X2,Col);
end;

procedure Rectangle(Surface: TDirectDrawSurface;const Mem: TDDSurfaceDesc;Rect: TRect;Col: TBlendColor);
begin
  dec(Rect.Right);
  dec(Rect.Bottom);
  HLine(Surface,Mem,Rect.Left,Rect.Right,Rect.Top,Col);
  HLine(Surface,Mem,Rect.Left,Rect.Right,Rect.Bottom,Col);
  VLine(Surface,Mem,Rect.Top,Rect.Bottom,Rect.Left,Col);
  VLine(Surface,Mem,Rect.Top,Rect.Bottom,Rect.Right,Col);
end;

procedure Frame3D(Surface: TDirectDrawSurface;const Mem: TDDSurfaceDesc;X1,Y1,X2,Y2: Integer; First,Second: TBlendColor);
begin
  dec(X2);
  dec(Y2);
  HLine(Surface,Mem,X1  ,X2  ,Y1   ,First);
  HLine(Surface,Mem,X1+1,X2-1,Y1+1 ,First);
  HLine(Surface,Mem,X1  ,X2  ,Y2   ,Second);
  HLine(Surface,Mem,X1+1,X2-1,Y2-1 ,Second);
  VLine(Surface,Mem,Y1+1,Y2-1,X1   ,First);
  VLine(Surface,Mem,Y1+2,Y2-2,X1+1 ,First);
  VLine(Surface,Mem,Y1+1,Y2-1,X2   ,Second);
  VLine(Surface,Mem,Y1+2,Y2-2,X2-1 ,Second);
end;

procedure FramingRect(Surface: TDirectDrawSurface;const Mem: TDDSurfaceDesc;Rect: TRect;Corners: TCorners;Radius: Integer;Col: TBlendColor);
var
  TopX1,TopX2  : Integer;
  BotX1,BotX2  : Integer;
  RigY1,RigY2  : Integer;
  LefY1,LefY2  : Integer;
  majorAxis    : Integer;
  minorAxis    : Integer;
  radiusSqr    : double;
  PutPixel     : TPutPixel;
begin
  if cLeftTop in Corners then
  begin
    TopX1:=Rect.Left+7;
    LefY1:=Rect.Top+7;
  end
  else
  begin
    TopX1:=Rect.Left;
    LefY1:=Rect.Top+1;
  end;
  if cRightTop in Corners then
  begin
    TopX2:=Rect.Right-7;
    RigY1:=Rect.Top+7;
  end
  else
  begin
    TopX2:=Rect.Right;
    RigY1:=Rect.Top+1;
  end;
  if cLeftBottom in Corners then
  begin
    BotX1:=Rect.Left+7;
    LefY2:=Rect.Bottom-7;
  end
  else
  begin
    BotX1:=Rect.Left;
    LefY2:=Rect.Bottom;
  end;
  if cRightBottom in Corners then
  begin
    BotX2:=Rect.Right-7;
    RigY2:=Rect.Bottom-7;
  end
  else
  begin
    BotX2:=Rect.Right;
    RigY2:=Rect.Bottom;
  end;
  HLine(Surface,Mem,TopX1,TopX2-1,Rect.Top,Col);
  HLine(Surface,Mem,BotX1,BotX2-1,Rect.Bottom-1,Col);
  VLine(Surface,Mem,LefY1,LefY2-1,Rect.Left,Col);
  VLine(Surface,Mem,RigY1,RigY2-1,Rect.Right-1,Col);
  if Corners=[] then exit;
  if Mode32Bit then PutPixel:=PutPixel32 else PutPixel:=PutPixel16;
  majorAxis := 0;
  minorAxis := Radius;
  radiusSqr:=Radius*Radius;
  dec(Rect.Right);
  dec(Rect.Bottom);
  repeat
    if cLeftTop in Corners then
    begin
      PutPixel(Surface,Mem,Rect.Left+Radius-minorAxis, Rect.Top+Radius-majorAxis, Col);
      PutPixel(Surface,Mem,Rect.Left+Radius-majorAxis, Rect.Top+Radius-minorAxis, Col);
    end;
    if cRightTop in Corners then
    begin
      PutPixel(Surface,Mem,Rect.Right-Radius+majorAxis, Rect.Top+Radius-minorAxis, Col);
      PutPixel(Surface,Mem,Rect.Right-Radius+minorAxis, Rect.Top+Radius-majorAxis, Col);
    end;
    if cLeftBottom in Corners then
    begin
      PutPixel(Surface,Mem,Rect.Left+Radius-majorAxis, Rect.Bottom-Radius+minorAxis, Col);
      PutPixel(Surface,Mem,Rect.Left+Radius-minorAxis, Rect.Bottom-Radius+majorAxis, Col);
    end;
    if cRightBottom in Corners then
    begin
      PutPixel(Surface,Mem,Rect.Right-Radius+majorAxis, Rect.Bottom-Radius+minorAxis, Col);
      PutPixel(Surface,Mem,Rect.Right-Radius+minorAxis, Rect.Bottom-Radius+majorAxis, Col);
    end;
    inc(majorAxis);
    minorAxis := round(sqrt(radiusSqr - (majorAxis * majorAxis))+0.5);
  until (majorAxis > minorAxis);
end;

procedure AALine(Surface: TDirectDrawSurface;const Mem: TDDSurfaceDesc;X1,Y1,X2,Y2: Integer; Color: TBlendColor; AlphaBlend: Integer);
var
  intensityShift, errorAdj, errorAcc: Word;
  errorAccTemp, weighting, weightComplimentMask : Word;
  deltaX, deltaY, temp,xDir : Integer;
  PutAAPixel : TPutPixelAA;
  ClipRect   : TRect;
begin
  // Aushalb des Bildschirmes
  ClipRect:=Surface.ClippingRect;
  if (X1<ClipRect.Left) and (X2<ClipRect.Left) then exit;
  if (Y1<ClipRect.Top) and (Y2<ClipRect.Top) then exit;
  if (X1>ClipRect.Right) and (X2>ClipRect.Right) then exit;
  if (Y1>ClipRect.Bottom) and (X2>ClipRect.Bottom) then exit;
  if (X1 = X2) or (Y1 = Y2) then
  begin
    LineAA(Surface,Mem,X1,Y1,X2,Y2,Color,AlphaBlend);
    exit;
  end;

  if Mode32Bit then
    PutAAPixel:=PutAAPixel32
  else
    PutAAPixel:=PutAAPixel16;

  if(Y1 > Y2) then
  begin
    temp := Y1; Y1 := Y2; Y2 := temp;
    temp := X1; X1 := X2; X2 := temp;
  end;
  PutAAPixel(Surface,Mem,X1,Y1,Color, AlphaBlend);
  deltaX:=X2-X1;
  if (deltaX >= 0) then
  begin
    xDir := 1;
  end
  else
  begin
    xDir := -1;
    deltaX := -deltaX;
  end;
  deltaY := Y2 - Y1;
  if deltaX<0 then temp:=-DeltaX else temp:=DeltaX;
  if ( Temp = deltaY) then
  begin
    LineAA(Surface,Mem,X1,Y1,X2,Y2,Color,AlphaBlend);
    exit;
  end;
  errorAcc := 0;
  intensityShift := 8;
  weightComplimentMask := 256 - 1;
  if (deltaY > deltaX) then
  begin
    errorAdj := round((deltaX shl 16) / deltaY);
    while(DeltaY>0) do
    begin
      dec(DeltaY);
      errorAccTemp := errorAcc;
      errorAcc := errorAcc+errorAdj;
      if (errorAcc <= errorAccTemp) then X1 := X1+xDir;
      inc(Y1);
      weighting := errorAcc shr intensityShift;
      PutAAPixel(Surface,Mem,X1,Y1,Color, ((weighting xor weightComplimentMask)*AlphaBlend) div 256);
      PutAAPixel(Surface,Mem,X1 + xDir, Y1, Color, (weighting*AlphaBlend) div 256);
    end;
    PutAAPixel(Surface,Mem,X2,Y2,Color,AlphaBlend);
  end
  else
  begin
    errorAdj := Round((deltaY shl 16) / deltaX);
    while (deltaX>0) do
    begin
      dec(deltaX);
      errorAccTemp := errorAcc;
      errorAcc := errorAcc+errorAdj;
      if (errorAcc <= errorAccTemp) then inc(Y1);
      X1 := X1+xDir;
      weighting := errorAcc shr intensityShift;
      PutAAPixel(Surface,Mem,X1,Y1,Color, ((weighting xor weightComplimentMask)*AlphaBlend) div 256);
      PutAAPixel(Surface,Mem,X1, Y1 + 1, Color, (weighting*AlphaBlend) div 256);
    end;
    PutAAPixel(Surface,Mem,X2,Y2,Color,AlphaBlend);
  end;
end;

end.

