{********************************************************************************
*										*
* Copyright (C) 2004  X-Force Team - jim_raynor@web.de				*
*										*
* This file is part of X-Force: Fight For Destiny (X-Force).			*
*										*
* X-Force is free software; you can redistribute it and/or modify		*
* it under the terms of the GNU General Public License as published by		*
* the Free Software Foundation; either version 2 of the License, or		*
* (at your option) any later version.						*
*										*
* X-Force is distributed in the hope that it will be useful,			*
* but WITHOUT ANY WARRANTY; without even the implied warranty of		*
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the			*
* GNU General Public License for more details.					*
*										*
* You should have received a copy of the GNU General Public License		*
* along with X-Force; if not, write to the Free Software			*
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA	*
*										*
*********************************************************************************
*										*
* Stellt Klassen zur Verf�gung um Spielstandsdaten in unterschiedlichen		*
* laden zu k�nnen. Durch die Benutzung von ExtRecords ist das meistens nicht	*
* mehr erforderlich								*
*										*
********************************************************************************}

{$I ../Settings.inc}

unit Loader;

interface

uses
  XForce_types, Koding, Classes, Sysutils, Defines,math, ExtRecord, ProjektRecords,
  ConvertRecords, ObjektRecords, StringConst;

type
  TProjektLoaderFunc = procedure(var Projekt: TForschProject) of object;
  TProjektLoader = class(TObject)
  private
    fReadStream : TStream;
    fVersion    : Integer;
    fStillRead  : Integer;
    fCount      : Integer;
    procedure SetStream(const Value: TStream);
  public
    constructor Create;
    destructor destroy;override;
    property Stream: TStream write SetStream;
    function GetNextObject(var Rec: TExtRecord): Boolean;
    property Count   : Integer read fCount;
    property Version : Integer read fVersion;
  end;

  TUFOModelLoader = class(TObject)
  private
    fReadStream : TStream;
    fVersion    : Integer;
    fStillRead  : Integer;
    fCount      : Integer;
    fKodiert    : boolean;
    procedure SetStream(const Value: TStream);
  public
    constructor Create;
    destructor destroy;override;
    property Stream: TStream write SetStream;
    function GetNextObject(var UFO: TExtRecord) : boolean;
    property Count : Integer read fCount;
    property Kodiert : boolean read fKodiert write fKodiert;
  end;

  TAlienLoader = class(TObject)
  private
    fReadStream : TStream;
    fVersion    : Integer;
    fStillRead  : Integer;
    fCount      : Integer;
    fKoding     : boolean;
    procedure SetStream(const Value: TStream);
  public
    destructor destroy;override;
    constructor Create;
    property Stream: TStream write SetStream;
    function GetNextObject(var Alien: TExtRecord) : boolean;
    property Count  : Integer read fCount;
    property Koding : boolean read fKoding write fKoding;
  end;

  var
    ReadingLanguage: String = '';

implementation

{ TProjektLoader }

uses
  KD4Utils, string_utils, alien_api;
                                                          
procedure ExtractLanguage(Rec: TExtRecord);

  procedure ApplyProperty(PropertyName: String);
  begin
    if Rec.ValueExists(PropertyName) then
      Rec.SetString(PropertyName,string_utils_getLanguageString(ReadingLanguage,Rec.GetString(PropertyName)));
  end;

begin
  if ReadingLanguage<>'' then
  begin
    ApplyProperty('Name');
    ApplyProperty('Info');
    ApplyProperty('ResearchInfo');
  end;
end;

constructor TProjektLoader.Create;
begin
  fReadStream:=nil;
end;

destructor TProjektLoader.destroy;
begin
  fReadStream.Free;
  inherited;
end;

function TProjektLoader.GetNextObject(var Rec: TExtRecord): Boolean;
var
  tmp: TExtRecord;
begin
  if fStillRead=0 then
  begin
    result:=false;
    exit;
  end;
  result:=true;

  Rec:=ReadProjectRecord(fReadStream);

  Assert(Rec<>nil);

  if Rec.RecordDefinition=RecordSchild then
  begin
    // Schilde werden zu Erweiterungen umgebastelt
    tmp:=TExtRecord.Create(RecordExtension);
    tmp.SetCardinal('ID',Rec.GetCardinal('ID'));
    tmp.SetInteger('TypeID',Integer(ptExtension));
    tmp.SetString('Name',Rec.GetString('Name'));
    tmp.SetString('Info',Rec.GetString('Info'));
    tmp.SetString('ResearchInfo',Rec.GetString('ResearchInfo'));
    tmp.SetBoolean('Start',Rec.GetBoolean('Start'));
    tmp.SetInteger('Ver',Rec.GetInteger('Ver'));
    tmp.SetInteger('Stunden',Rec.GetInteger('Stunden'));
    tmp.SetInteger('Anzahl',Rec.GetInteger('Anzahl'));
    tmp.SetInteger('ParentCount',Rec.GetInteger('ParentCount'));
    tmp.SetBinary('Parents',Rec.GetBinaryLength('Parents'),Rec.GetBinary('Parents'));
    tmp.SetInteger('KaufPreis',Rec.GetInteger('KaufPreis'));
    tmp.SetInteger('VerKaufPreis',Rec.GetInteger('VerKaufPreis'));
    tmp.SetInteger('Alphatron',Rec.GetInteger('Alphatron'));
    tmp.SetInteger('ProdTime',Rec.GetInteger('ProdTime'));
    tmp.SetInteger('ImageIndex',Rec.GetInteger('ImageIndex'));
    tmp.SetInteger('Land',Rec.GetInteger('Land'));
    tmp.SetString('UFOPaedieImage',Rec.GetString('UFOPaedieImage'));
    tmp.SetBoolean('Herstellbar',Rec.GetBoolean('Herstellbar'));
    tmp.SetBoolean('AlienItem',Rec.GetBoolean('AlienItem'));
    tmp.SetInteger('ResearchItems',Rec.GetInteger('ResearchItems'));
    tmp.SetInteger('AlienChance',Rec.GetInteger('AlienChance'));
    tmp.SetDouble('LagerV',Rec.GetDouble('LagerV'));
    tmp.SetInteger('Ext[epShieldHitpoints]',Rec.GetInteger('Strength'));
    tmp.SetInteger('Ext[epShieldDefence]',Rec.GetInteger('Panzerung'));
    tmp.SetInteger('Ext[epShieldReloadTime]',Rec.GetInteger('Laden'));

    Rec.Free;
    Rec:=tmp;
  end;

  // F�r das Spiel die gew�hlte Sprache laden
  ExtractLanguage(Rec);

  dec(fStillRead);
end;

procedure TProjektLoader.SetStream(const Value: TStream);
var
  Header: TEntryHeader;
  Buffer: Cardinal;
begin
  if fReadStream=nil then
    fReadStream:=TMemoryStream.Create;

  Value.Read(Buffer,SizeOf(Buffer));
  if Buffer=$3C3385CD then
  begin
    fReadStream.CopyFrom(Value,0);
  end
  else
    Decode(Value,fReadStream);

  fReadStream.Position:=0;
  fReadStream.Read(Header,SizeOf(TEntryHeader));

  case Header.Version of
    $3C3385CD : fVersion:=13;
    else
      raise EInvalidVersion.Create('Die Forschungsdaten liegen in einem Ung�ltigem Format vor.');
  end;
  fCount:=Header.Entrys;
  fStillRead:=fCount;
end;

{ TUFOModelLoader }

constructor TUFOModelLoader.Create;
begin
  fKodiert:=true;
end;

destructor TUFOModelLoader.destroy;
begin
  if fKodiert then
    fReadStream.Free;

  inherited;
end;

function TUFOModelLoader.GetNextObject(var UFO: TExtRecord): boolean;
begin
  if fStillRead=0 then
  begin
    result:=false;
    exit;
  end;
  result:=true;

  case fVersion of
    5:
    begin
      UFO:=TExtRecord.Create(RecordUFOModel);
      UFO.LoadFromStream(fReadStream);
    end;
  end;

  // F�r das Spiel die gew�hlte Sprache laden
  ExtractLanguage(UFO);

  dec(fStillRead);
end;

procedure TUFOModelLoader.SetStream(const Value: TStream);
var
  Header: TEntryHeader;
  Buffer: Cardinal;
begin
  if Kodiert then
  begin
    if fReadStream=nil then
      fReadStream:=TMemoryStream.Create;

    Value.Read(Buffer,SizeOf(Buffer));
    if Buffer=$18010EC8 then
      fReadStream.CopyFrom(Value,0)
    else
      Decode(Value,fReadStream);

    fReadStream.Position:=0;
  end
  else
  begin
    fReadStream:=Value;
  end;

  fReadStream.Read(Header,SizeOf(TEntryHeader));
  fCount:=Header.Entrys;
  case Header.Version of
    $18010EC8 : fVersion:=5;
    else
      raise EInvalidVersion.Create('TUFOModelLoader liegen in einem Ung�ltigem Format vor.');
  end;
  fStillRead:=fCount;
end;

{ TAlienLoader }

constructor TAlienLoader.Create;
begin
  fKoding:=true;
end;

destructor TAlienLoader.destroy;
begin
  if fKoding then
  fReadStream.Free;
  inherited;
end;

function TAlienLoader.GetNextObject(var Alien: TExtRecord): boolean;
begin
  if fStillRead=0 then
  begin
    result:=false;
    exit;
  end;
  result:=true;
  case fVersion of
    200    :
    begin
      Alien:=TExtRecord.Create(RecordAlien);
      Alien.LoadFromStream(fReadStream);
    end;
  end;

  ExtractLanguage(Alien);

  dec(fStillRead);
end;

procedure TAlienLoader.SetStream(const Value: TStream);
var
  Header: TEntryHeader;
  Buffer: Cardinal;
begin
  if (fReadStream=nil) and fKoding then
    fReadStream:=TMemoryStream.Create;

  if fKoding then
  begin
    Value.Read(Buffer,SizeOf(Buffer));
    if Buffer=$5D0FADFF then
      fReadStream.CopyFrom(Value,0)
    else
      Decode(Value,fReadStream);
      
    fReadStream.Position:=0;
  end
  else
    fReadStream:=Value;

  fReadStream.Read(Header,SizeOf(TEntryHeader));
  case Header.Version of
  // GameSet Daten
    $5D0FADFF : fVersion:=200;

  // Alien Daten
    $3FE0FEF6 : fVersion:=200;
  end;
  fCount:=Header.Entrys;
  fStillRead:=fCount;
end;

end.
