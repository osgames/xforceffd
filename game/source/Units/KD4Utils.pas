{********************************************************************************
*										*
* Copyright (C) 2004  X-Force Team - jim_raynor@web.de				*
*										*
* This file is part of X-Force: Fight For Destiny (X-Force).			*
*										*
* X-Force is free software; you can redistribute it and/or modify		*
* it under the terms of the GNU General Public License as published by		*
* the Free Software Foundation; either version 2 of the License, or		*
* (at your option) any later version.						*
*										*
* X-Force is distributed in the hope that it will be useful,			*
* but WITHOUT ANY WARRANTY; without even the implied warranty of		*
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the			*
* GNU General Public License for more details.					*
*										*
* You should have received a copy of the GNU General Public License		*
* along with X-Force; if not, write to the Free Software			*
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA	*
*										*
*********************************************************************************
*										*
* allgemeine Utilities								*
*										*
********************************************************************************}

{$I ../Settings.inc}

unit KD4Utils;

interface

uses Forms, windows, mmSystem, XForce_types, math, graphics, SysUtils,
     Classes, Registry, FileCtrl, Defines, ComCtrls{$IFDEF SHOWMEMORY},psApi{$ENDIF};

type
  TSortEvent = function(Index1,Index2: Integer;Typ: TFunctionType): Integer;
  TObjSortEvent = function(Index1,Index2: Integer;Typ: TFunctionType): Integer of object;

// ZeitFunktionen
function CalculateTime(var Date: TKD4Date;Minuten: Integer): boolean;

{ Stringfunktionen }
function ExtractVorName(Name: String): String;
function ExtractNachName(Name: String): String;
function Cut(Source: String;CutStart: Integer;CutEnd: Integer): String;
function LowerChar(Zeichen: Char): Char;
function UpperChar(Zeichen: Char): Char;
function IntToTime(Time: Integer): String;
function IntToKB(Int: Cardinal): String;
function GetShortCut(Text: String):Char;
function Stuff(Source: String; Start,length: Integer; Replace: String): String;
function ZahlString(Einzel: String;Mehr: String;Anzahl: Integer): String;
function StripHotKey(Text: String): String;
procedure SaveStringList(StringList: TStrings;Stream: TStream);
procedure LoadStringList(StringList: TStrings;Stream: TStream);
procedure WriteString(Stream: TStream;Str: String);
function ReadString(Stream: TStream): String;
function RemoveLineBreaks(Line: String): String;
function Remove0A(Text: String): String;
procedure CleanStringList(List: TStringList);
function NormalizeText(Text: String): String;

{ Bit-Funktionen }
procedure SetBit(var Int: Integer;Bit: Integer);
procedure ClearBit(var Int: Integer;Bit: Integer);
function GetBit(Int: Integer;Bit: Integer):boolean;

{ Rectecks-Funktionen }
procedure CorrectRect(var Rect: TRect);
procedure HighLightRect(var Rect: TRect);
function ResizeRect(Rect: TRect;OffSet: Integer): TRect;
function OffSetRectangle(Rect: TRect;Left: Integer;Top: Integer): TRect;
function CorrectBottomOfRect(Rect: TRect): TRect;

// TPoint-Funktionen
function PointsSideBySide(Point1: TPoint; Point2: TPoint): Boolean;

procedure ActivateScreenSaver(ActiveState: boolean);
function GetScreenSaver: boolean;
function EnumWindow(Handle: THandle;lParam: LPARAM): boolean;stdCall;
procedure RestoreTopWindows;
procedure MakeScreenShot(Form: TForm);
function GetSoldatWert(Soldat: TSoldatInfo): Integer;
function MinReal(Val1: Single;Val2: Single): Single;
function MaxReal(Val1: Single;Val2: Single): Single;
function OrganStatus(Grad: TFriendlyWert): TOrganisationStatus;
function MakeHeadData(Titel, Einzel, MehrZahl: String): THeadData;
function FloatPoint(X,y: double):TFloatPoint;
function FloatToPoint(Point: TFloatPoint): TPoint;
function PointToFloat(Point: TPoint): TFloatPoint;
function CalculateEntfern(A,B: TFloatPoint): Integer;overload;
function CalculateEntfern(A,B: TPoint): Integer;overload;
function CalculateWinkel(Pt1, Pt2: TFloatPoint): Integer;overload;
function CalculateWinkel(Pt1, Pt2: TPoint): Integer;overload;
function CalculateAbstand(VonAngle, ZielAngle: Integer): Integer;
function TypeIDToIndex(TypeID: TProjektType; WaffType: TWaffenType; Index: Integer = 0): Integer;
procedure QuickSort(L,H: Integer;Compare: TSortEvent);overload;
procedure QuickSort(L,H: Integer;Compare: TObjSortEvent);overload;
function GetDefaultKeys: TAllHotKeys;

// Zufallsfunktionen
function RandomChance(Percent: double): boolean;
function RandomMinMax(min,max: Integer): Integer;
function RandomPercent(Value: Integer; Percent: Integer): Integer;

// Berechnungen
function MinMax(Value,min,max: Integer): Integer;overload;
function MinMax(Value,min,max: double): double;overload;

function GetMunitionGewicht(FullGewicht: double;MaxSchuesse: Integer;Schuesse: Integer): double;

function HashString(Name: String): Cardinal;

{ Pr�ffunktionen }
procedure CheckBorders(Value: Integer; Min, Max: Integer; AssertText : String = '');

{ X-Force spezifisch }
function OpenXForceRegistry(SubFolder: String = ''; CanCreate: Boolean = true): TRegistry;

procedure ConvertForschProjekts(var Projekts: Array of TForschProject);
function ForschProjektToLagerItem(const Forschung: TForschProject): TLagerItem;
function CheckSchussType(TypeID: TProjektType ;WaffenArt: TWaffenType;SchussArt: TSchussTypes): TSchussTypes;

{Vergleichsfunktionen}
function SameSoldat(Soldat1: TSoldatInfo;Soldat2: TSoldatInfo): boolean;
function CompareType(TypeID1,TypeID2: TProjektType): Integer;
function SamePoint(Pos1: TFloatPoint; Pos2: TFloatPoint): boolean;overload;
function SamePoint(Pos1: TPoint; Pos2: TPoint): boolean;overload;
function Matchstrings(Source, pattern: string): Boolean;

{ Savegamefunktionen}
procedure SaveLastSavedGame(Player,SaveGame: String);
function IsValidSaveGameVersion(Version: Integer): boolean;

{ Systemfunktionen }
function ListViewConfHTML(Listview: TListview; outputFile: string; center: Boolean) : Boolean;

{$IFDEF SHOWMEMORY}
function GetProcessMemory: Integer;
{$ENDIF}

type
  TWindState = record
    Handle: HWND;
    Visible: boolean;
  end;

  TSavedInformation = (saGameSet,saAlienList,saEinsatz,saMission,sa32BitLongMessage, saArrayMonthStat, saGameSetLanguage);

function VersionHasInformation(Info: TSavedInformation;Version: Integer): boolean;

var
  TopWindows       : Array of TWindState;
  KeyConfiguration : TAllHotKeys;
  LightDetail      : TLightDetail = ldVierfach;
  ScreenshotDir    : String;

implementation

uses
  savegame_api;

function CalculateTime(var Date: TKD4Date;Minuten: Integer): boolean;
var
  WeekDay: Integer;
  SysDate: TDateTime;
begin
  { Berechnet die n�chste Stunde einer TKD4Date Struktur }
  result:=false;
  inc(Date.Minute,Minuten);
  if Date.Minute>59 then
  begin
    dec(Date.Minute,60);
    inc(Date.Hour);
    if Date.Hour>23 then
    begin
      result:=true;
      dec(Date.Hour,24);
      inc(Date.Day);
      if Date.Day>MonthDays[IsLeapYear(Date.Year)][Date.Month] then
      begin
        Date.Day:=1;
        inc(Date.Month);
        if Date.Month>12 then
        begin
          dec(Date.Month,12);
          inc(Date.Year);
        end;
      end;
      SysDate:=EncodeDate(Date.Year,Date.Month,Date.Day);
      SysDate:=SysDate+0.5;
      WeekDay:=DayOfWeek(SysDate);
      if WeekDay=2 then
      begin
        if (Date.Day<8) and (Date.Month=1) then
          Date.Week:=1
        else
          inc(Date.Week);
      end;
    end;
  end;
end;

function ExtractVorName(Name: String): String;
begin
  { Extrahiert den Vornamen aus einem Namen, allerdings }
  { sollte er kein Leerzeichen enthalten, da er das     }
  { erste Leerzeichen als Ende des Vornamens ansieht    }
  result:=Copy(Name,1,Pos(' ',Name)-1);
end;

function ExtractNachName(Name: String): String;
begin
  { Extrahiert den Nachnamen aus einem Namen. Dazu nimmt }
  { er an das nach dem ersten Leerzeichen der Nachname   }
  { beginnt                                              }
  result:=Copy(Name,Pos(' ',Name)+1,length(Name));
end;

function Cut(Source: String;CutStart: Integer;CutEnd: Integer): String;
begin
  result:=Copy(Source,max(CutStart,1),min(CutEnd-CutStart,length(Source)-CutStart)+1);
end;

function GetShortCut(Text: String):Char;
var
  Dummy: Integer;
  Zeichen: Integer;
begin
  Zeichen:=length(Text);
  if Zeichen<=1 then
  begin
    result:=#0;
    exit;
  end;
  Dummy:=1;
  while (Dummy<Zeichen) and (Text[Dummy]<>'&') do inc(Dummy);
  if (Text[Dummy]<>'&') or (Dummy=Zeichen) then
  begin
    result:=#0;
    exit;
  end;
  result:=LowerChar(Text[Dummy+1]);
end;

function LowerChar(Zeichen: Char): Char;
begin
  result:=Zeichen;
  if (Zeichen>='A') and (Zeichen<='Z') or (Zeichen in ['�','�','�']) then result:=Char(Integer(Zeichen)+32);
end;

function UpperChar(Zeichen: Char): Char;
begin
  result:=Zeichen;
  if (Zeichen>='a') and (Zeichen<='z') or (Zeichen in ['�','�','�'])  then result:=Char(Integer(Zeichen)-32);
end;


procedure ActivateScreenSaver(ActiveState: boolean);
begin
  SystemParametersInfo(SPI_SetScreenSaveActive,uint(ActiveState),nil,0);
end;

function GetScreenSaver: boolean;
var
  Int: UINT;
begin
  Int:=0;
  SystemParametersInfo(SPI_GetScreenSaveActive,Int,nil,0);
  result:=boolean(Int);
end;

function EnumWindow(Handle: THandle; lParam: LPARAM):boolean;
begin
  result:=true;
  if Handle=Application.Handle then exit;
  if (GetWindowLong(Handle,GWL_EXSTYLE) and WS_EX_TOPMOST<>0) and (GetWindowLong(Handle,GWL_EXSTYLE) and WS_EX_APPWINDOW<>0) then
  begin
    SetLength(TopWindows,length(TopWindows)+1);
    TopWindows[Length(TopWIndows)-1].Handle:=Handle;
    TopWindows[Length(TopWIndows)-1].Visible:=IsWindowVisible(Handle);
    ShowWindow(Handle,SW_MINIMIZE);
  end;
end;

procedure RestoreTopWindows;
var
  Dummy: Integer;
begin
  for Dummy:=0 to length(TopWindows)-1 do
  begin
    if TopWindows[Dummy].Visible then ShowWindow(TopWindows[Dummy].Handle,SW_RESTORE);
  end;
end;

procedure MakeScreenShot(Form: TForm);
var
  Bitmap: TBitmap;
  Count: Integer;
  Saved: boolean;
  FileName: String;
begin
  ForceDirectories(ScreenshotDir);
  Bitmap:=TBitmap.Create;
  Bitmap.Width:=Form.ClientWidth;
  Bitmap.Height:=Form.ClientHeight;
  Bitmap.Canvas.CopyRect(Rect(0,0,Form.ClientWidth,Form.ClientHeight),Form.Canvas,Rect(0,0,Form.ClientWidth,Form.ClientHeight));
  Saved:=false;
  Count:=0;
  while not Saved do
  begin
    FileName:=Format('%sScreen%.3d.bmp',[ScreenshotDir,Count]);
    if not FileExists(FileName) then
    begin
      Saved:=true;
      Bitmap.SaveToFile(FileName);
    end;
    inc(Count);
  end;
  Bitmap.Free;
end;

procedure CorrectRect(var Rect: TRect);
begin
  if Rect.Left<0 then Rect.Left:=0;
  if Rect.Top<0 then Rect.Top:=0;
  if Rect.Right>ScreenWidth then Rect.Right:=ScreenWidth;
  if Rect.Bottom>ScreenHeight then Rect.Bottom:=ScreenHeight;
end;

function GetSoldatWert(Soldat: TSoldatInfo): Integer;
begin
  result:=round(Soldat.Maxges*75);
  result:=result+round(Soldat.PSIAn*30);
  result:=result+round(Soldat.PSIAb*30);
  result:=result+round(Soldat.Kraft*40);
  result:=result+round(Soldat.Zeit*75);
  result:=result+round(Soldat.Treff*35);
  result:=result+round(Soldat.Treffer*150);
  result:=result+round(Soldat.Sicht*35);
  result:=result+round(Soldat.IQ*40);
  result:=result+round(Soldat.React*20);
end;

function MinReal(Val1: Single;Val2: Single): Single;
begin
  if Val1>Val2 then result:=Val2 else result:=val1;
end;

function MaxReal(Val1: Single;Val2: Single): Single;
begin
  if Val1<Val2 then result:=Val2 else result:=val1;
end;

function IntToKB(Int: Cardinal): String;
var
  Wert: double;
  Formatter: String;
begin
  Wert:=Int;
  Formatter:='%.0n Byte';
  if Wert>1024 then
  begin
    Wert:=Wert/1024;
    Formatter:='%.1n KByte';
    if Wert>1024 then
    begin
      Wert:=Wert/1024;
      Formatter:='%.2n MByte';
      if Wert>1024 then
      begin
        Wert:=Wert/1024;
        Formatter:='%.3n GByte';
      end;
    end;
  end;
  result:=Format(Formatter,[Wert]);
end;

function OrganStatus(Grad: TFriendlyWert): TOrganisationStatus;
begin
       if Grad<20 then result:=osEnemy
  else if Grad<40 then result:=osUnFriendly
  else if Grad<60 then result:=osNeutral
  else if Grad<80 then result:=osFriendly
  else result:=osAllianz;
end;

function ResizeRect(Rect: TRect;OffSet: Integer): TRect;
begin
  Result:=Rect;
  inc(Result.Left,OffSet);
  inc(Result.Top,OffSet);
  dec(Result.Right,OffSet);
  dec(Result.Bottom,OffSet);
end;

function OffSetRectangle(Rect: TRect;Left: Integer;Top: Integer): TRect;
begin
  result:=Rect;
  inc(result.Left,Left);
  inc(result.Right,Left);
  inc(result.Top,Top);
  inc(result.Bottom,Top);
end;

procedure HighLightRect(var Rect: TRect);
begin
  inc(Rect.Left,1);
  dec(Rect.Right,1);
end;

function SameSoldat(Soldat1: TSoldatInfo;Soldat2: TSoldatInfo): boolean;
begin
  result:=(Soldat1.Name=Soldat2.Name);
  if not result then exit;
  result:=result and (Soldat1.Ges=Soldat2.Ges);
  result:=result and (Soldat1.MaxGes=Soldat2.MaxGes);
  result:=result and (Soldat1.PSIAn=Soldat2.PSIAn);
  result:=result and (Soldat1.PSIAb=Soldat2.PSIAb);
  result:=result and (Soldat1.Zeit=Soldat2.Zeit);
  result:=result and (Soldat1.Treff=Soldat2.Treff);
  result:=result and (Soldat1.Kraft=Soldat2.Kraft);
  result:=result and (Soldat1.Sicht=Soldat2.Sicht);
  result:=result and (Soldat1.IQ=Soldat2.IQ);
  result:=result and (Soldat1.React=Soldat2.React);
end;

function Stuff(Source: String; Start,length: Integer; Replace: String): String;
begin
  delete(Source,Start+1,length);
  insert(Replace,Source,Start+1);
  result:=Source;
end;

function CompareType(TypeID1,TypeID2: TProjektType): Integer;
var
  WertID1,WertID2: Integer;

  function GetWert(ID: TProjektType): Integer;
  begin
    result:=100;
    case ID of
      ptWaffe        : result:=0;
      ptMunition     : result:=1;
      ptGuertel      : result:=2;
      ptMine         : result:=3;
      ptGranate      : result:=4;
      ptSensor       : result:=5;
      ptPanzerung    : result:=6;
      ptRaumSchiff   : result:=7;
      ptRWaffe       : result:=8;
      ptRMunition    : result:=9;
      ptMotor        : result:=10;
      ptShield       : result:=11;
      ptEinrichtung  : result:=12;
      ptOrganisation : result:=13;
      ptExtension    : result:=14;
      ptNone         : result:=15;
    end;
  end;

begin
  WertID1:=GetWert(TypeID1);
  WertID2:=GetWert(TypeID2);
  if WertID1=WertID2 then result:=0
  else if WertID1>WertID2 then result:=1
  else result:=-1;
end;

function ZahlString(Einzel: String;Mehr: String;Anzahl: Integer): String;
begin
  if abs(Anzahl)=1 then result:=Format(Einzel,[Anzahl]) else result:=Format(Mehr,[Anzahl]);
end;

function MakeHeadData(Titel, Einzel, MehrZahl: String): THeadData;
begin
  Result.Titel:=Titel;
  Result.Einzel:=Einzel;
  Result.MehrZahl:=MehrZahl;
end;

function StripHotKey(Text: String): String;
var
  Dummy: Integer;
begin
  for Dummy:=1 to length(Text) do
    if Text[Dummy]<>'&' then result:=result+Text[Dummy];
end;

procedure SetBit(var Int: Integer;Bit: Integer);
var
  Mask: Integer;
begin
  Mask:=1;
  Mask:=Mask shl Bit;
  Int:=Int or Mask;
end;

procedure ClearBit(var Int: Integer;Bit: Integer);
var
  Mask: Integer;
begin
  Mask:=1;
  Mask:=not (Mask shl Bit);
  Int:=Int and Mask;
end;

function GetBit(Int: Integer;Bit: Integer):boolean;
var
  Mask: Integer;
begin
  Mask:=1;
  Mask:=Mask shl Bit;
  result:=(Int and Mask)<>0;
end;

function CalculateEntfern(A,B: TFloatPoint): Integer;
begin
  result:=round(Sqrt(Sqr(abs(b.x-a.x))+Sqr(abs(b.Y-a.Y))));
end;

function CalculateEntfern(A,B: TPoint): Integer;
begin
  result:=round(Sqrt(Sqr(abs(b.x-a.x))+Sqr(abs(b.Y-a.Y))));
end;

function FloatPoint(X,y: double):TFloatPoint;
begin
  result.X:=X;
  result.Y:=Y;
end;

function FloatToPoint(Point: TFloatPoint): TPoint;
begin
  result.X:=round(Point.X);
  result.Y:=round(Point.Y);
end;

function PointToFloat(Point: TPoint): TFloatPoint;
begin
  result.X:=Point.X;
  result.Y:=Point.Y;
end;

function CalculateWinkel(Pt1, Pt2: TFloatPoint): Integer;
var
  X,Y: double;
  A: Double;
  SinB: Double;
begin
  X:=Pt1.X-Pt2.X;
  Y:=Pt1.Y-Pt2.Y;
  if (X=0) and (Y=0) then
  begin
    result:=0;
    exit;
  end;
  A:=sqrt(sqr(X)+sqr(Y));
  SinB:=abs(Y)/a;
  result:=round(RadToDeg(ArcSin(SinB)));
       if (X<0) and (Y=0) then result:=270
  else if (X=0) and (Y<0) then result:=0
  else if (X=0) and (Y>0) then result:=180
  else if (X>0) and (Y=0) then result:=90
  else if (X<0) and (Y<0) then result:=270+result
  else if (X>0) and (Y<0) then result:=90-result
  else if (X<0) and (Y>0) then result:=270-result
  else if (X>0) and (Y>0) then result:=90+result;
end;

procedure SaveStringList(StringList: TStrings;Stream: TStream);
var
  Count : Integer;
  Dummy : Integer;
  Len   : Word;
  Str   : String;
  Point : TObject;
begin
  Count:=StringList.Count;
  Stream.Write(Count,SizeOf(Count));
  for Dummy:=0 to Count-1 do
  begin
    Len:=length(StringList[Dummy]);
    Stream.Write(Len,SizeOf(Len));
    Str:=StringList[Dummy];
    Stream.Write(Pointer(Str)^,len);
    Point:=StringList.Objects[Dummy];
    Stream.Write(Point,SizeOf(Point));
  end;                                       
end;

procedure LoadStringList(StringList: TStrings;Stream: TStream);
var
  Count : Integer;
  Dummy : Integer;
  Len   : Word;
  Str   : String;
  Point : TObject;
begin
  StringList.Clear;
  Stream.Read(Count,SizeOf(Count));
  for Dummy:=0 to Count-1 do
  begin
    Stream.Read(Len,SizeOf(Len));
    SetString(Str, nil, Len);
    Stream.Read(Pointer(Str)^,len);
    Stream.Read(Point,SizeOf(Point));
    StringList.AddObject(Str,Point);
  end;
end;

function CalculateWinkel(Pt1, Pt2: TPoint): Integer;overload;
var
  X,Y: double;
  A: Double;
  SinB: Double;
begin
  X:=Pt1.X-Pt2.X;
  Y:=Pt1.Y-Pt2.Y;
  if (X=0) and (Y=0) then
  begin
    result:=0;
    exit;
  end;
  A:=sqrt(sqr(X)+sqr(Y));
  SinB:=abs(Y)/a;
  result:=round(RadToDeg(ArcSin(SinB)));
       if (X<0) and (Y=0) then result:=270
  else if (X=0) and (Y<0) then result:=0
  else if (X=0) and (Y>0) then result:=180
  else if (X>0) and (Y=0) then result:=90
  else if (X<0) and (Y<0) then result:=270+result
  else if (X>0) and (Y<0) then result:=90-result
  else if (X<0) and (Y>0) then result:=270-result
  else if (X>0) and (Y>0) then result:=90+result;
end;

function CalculateAbstand(VonAngle, ZielAngle: Integer): Integer;
begin
  result:=min(min(abs(VonAngle-ZielAngle),abs((VonAngle+256)-ZielAngle)),abs(VonAngle-(ZielAngle+256)));
end;

function CorrectBottomOfRect(Rect: TRect): TRect;
begin
  result:=Rect;
  dec(result.Bottom);
end;

{ Schreibt einen String in einen Stream. Mit ReadString kann er wieder gelesen werden }
procedure WriteString(Stream: TStream; Str: String);
var
  Len : Integer;
begin
  Len:=Length(Str);
  Stream.Write(Len,SizeOf(Len));
  Stream.Write(Pointer(Str)^,Len);
end;

function ReadString(Stream: TStream): String;
var
  Len : Integer;
begin
  Stream.Read(Len,SizeOf(Len));
  SetString(result, nil, Len);
  Stream.Read(Pointer(result)^,len);
end;

function IntToTime(Time: Integer): String;
var
  Minutes : Integer;
  Hours   : Integer;
  Days    : Integer;
begin
  Minutes:=Time mod 60;
  days:=0;
  result:=IntToStr(Minutes)+' m';
  Hours:=Time div 60;
  if Hours>0 then
  begin
    if Hours>23 then
    begin
      days:=Hours div 24;
      Hours:=Hours mod 24;
    end;
    result:=IntToStr(Hours)+' h '+result;
    if Days>0 then result:=IntToStr(days)+' d '+result;
  end;
end;

function TypeIDToIndex(TypeID: TProjektType;WaffType: TWaffenType; Index: Integer): Integer;
begin
  result:=0;
  if Index=0 then
  begin
    case TypeID of
      ptGranate   : result:=0;
      ptMine      : result:=1;
      ptMunition  :
      begin
        case WaffType of
          wtProjektil : result:=2;
          wtRaketen   : result:=3;
          wtChemic    : result:=4;
        end;
      end;
      ptWaffe     :
      begin
        case WaffType of
          wtProjektil : result:=5;
          wtRaketen   : result:=6;
          wtLaser     : result:=7;
          wtChemic    : result:=8;
        end;
      end;
      ptPanzerung : result:=9;
      ptRWaffe    :
      begin
        case WaffType of
          wtProjektil : result:=10;
          wtRaketen   : result:=11;
          wtLaser     : result:=12;
        end;
      end;
      ptRMunition :
      begin
        case WaffType of
          wtProjektil : result:=2;
          wtRaketen   : result:=13;
        end;
      end;
      ptMotor     : result:=14;
      ptSensor    : result:=15;
      ptShield    : result:=16;
      ptExtension : result:=17;
      ptGuertel   : result:=18;
    end;
  end
  else
  begin
    case WaffType of
      wtProjektil : result:=0;
      wtLaser     : result:=1;
      wtRaketen   : result:=2;
    end;
    case TypeID of
      ptMotor     : result:=3;
      ptShield    : result:=4;
    end;
  end;
end;

function IsValidSaveGameVersion(Version: Integer): boolean;
begin
  result:=Version=SaveVersion;
{  case Version of
    $4AE3AEF1: result:=true;
    $223890DC: result:=true;
    $02EE5A49: result:=true;
    $3651ACC1: result:=true;
    $7712E2F7: result:=true;
    $022712BC: result:=true;
    $515C3A3B: result:=true;
    $0D3C8E02: result:=true;
  end;}
end;

function VersionHasInformation(Info: TSavedInformation;Version: Integer): boolean;
begin
  result:=Version=SaveVersion;
{  case Info of
    saGameSet           : result:=(Version=$7712E2F7) or (Version=$022712BC) or (Version=$515C3A3B) or (Version=$0D3C8E02) or (Version=$02EE5A49) or (Version=$223890DC) or (Version=$4AE3AEF1);
    saAlienList         : result:=(Version=$022712BC) or (Version=$515C3A3B) or (Version=$0D3C8E02) or (Version=$02EE5A49) or (Version=$223890DC) or (Version=$4AE3AEF1);
    saEinsatz           : result:=(Version=$515C3A3B) or (Version=$0D3C8E02) or (Version=$02EE5A49) or (Version=$223890DC) or (Version=$4AE3AEF1);
    saMission           : result:=(Version=$0D3C8E02) or (Version=$02EE5A49) or (Version=$223890DC) or (Version=$4AE3AEF1);
    sa32BitLongMessage  : result:=(Version=$02EE5A49) or (Version=$223890DC) or (Version=$4AE3AEF1);
    saArrayMonthStat    : result:=(Version=$223890DC) or (Version=$4AE3AEF1);
    saGameSetLanguage   : result:=(Version=$4AE3AEF1);
  end;}
end;

function SamePoint(Pos1,Pos2: TFloatPoint): boolean;
begin
  result:=(Pos1.x=Pos2.x) and (Pos1.y=Pos2.y);
end;

function SamePoint(Pos1: TPoint; Pos2: TPoint): boolean;
begin
  result:=(Pos1.x=Pos2.x) and (Pos1.y=Pos2.y);
end;

function RandomChance(Percent: double): boolean;
begin
  result:=false;
  if random*100<Percent then
  begin
    result:=true;
  end;
end;

function RemoveLineBreaks(Line: String): String;
var
  Dummy : Integer;
begin
  Dummy:=length(Line);
  while (Line[Dummy]=#10) do dec(Dummy);
  result:=Copy(Line,1,Dummy);
end;

function Remove0A(Text: String): String;
var
  Dummy: Integer;
begin
  result:='';
  for Dummy:=1 to length(Text) do
    if Text[Dummy]<>#13 then result:=result+Text[Dummy];
end;

procedure QuickSort(L,H: Integer;Compare: TSortEvent);
var
  I, J, P: Integer;
begin
  if L>=H then exit;
  repeat
    I := L;
    J := H;
    P := (L + H) shr 1;
    repeat
      while (I<>P) and (Compare(I, P,ftCompare) < 0) do Inc(I);
      while (J<>P) and (Compare(J, P,ftCompare) > 0) do Dec(J);
      if I <= J then
      begin
        Compare(I, J,ftExchange);
        if P = I then
          P := J
        else if P = J then
          P := I;
        Inc(I);
        Dec(J);
      end;
    until I > J;
    if L < J then QuickSort(L, J, Compare);
    L := I;
  until I >= H;
end;

procedure QuickSort(L,H: Integer;Compare: TObjSortEvent);
var
  I, J, P: Integer;
begin
  if L>=H then exit;
  repeat
    I := L;
    J := H;
    P := (L + H) shr 1;
    repeat
      while (I<>P) and (Compare(I, P,ftCompare) < 0) do Inc(I);
      while (J<>P) and (Compare(J, P,ftCompare) > 0) do Dec(J);
      if I <= J then
      begin
        Compare(I, J,ftExchange);
        if P = I then
          P := J
        else if P = J then
          P := I;
        Inc(I);
        Dec(J);
      end;
    until I > J;
    if L < J then QuickSort(L, J, Compare);
    L := I;
  until I >= H;
end;

function GetDefaultKeys: TAllHotKeys;
begin
  with Result.UFOKeys do
  begin
    RLeft:=Char(VK_LEFT);
    RRight:=Char(VK_RIGHT);
    GibSchub:=Char(VK_UP);
    MLeft:='Z';
    MRight:='X';
    WZelle1:='R';
    WZelle2:='T';
    WZelle3:='Y';
  end;
  with Result.BodenKeys do
  begin
    Pause:=' ';
    RundenEnde:=Char(VK_RETURN);
    Karte:='M';
    ObjektNamen:='O';
    WallsTrans:='P';
    NextSoldat:='W';
    PrevSoldat:='Q';
    ScrollLeft:=Char(VK_LEFT);
    ScrollRight:=Char(VK_RIGHT);
    ScrollUp:=Char(VK_UP);
    ScrollDown:=Char(VK_Down);
    Camera:='C';
    Ausruesten:='A';
    TakeAll:='T';
    BeamUp:='B';
    ShowGrid:='D';
    UseGrenade:='G';
    UseMine:='I';
    UseSensor:='S';
  end;
end;

procedure SaveLastSavedGame(Player,SaveGame: String);
var
  Registry: TRegistry;
begin
  Registry:=OpenXForceRegistry;
  Registry.WriteString('LastPlayer',Player);
  Registry.WriteString('LastGame',SaveGame);
  Registry.Free;
end;

function OpenXForceRegistry(SubFolder: String = ''; CanCreate: Boolean = true): TRegistry;
begin
  Result:=TRegistry.Create;
  if not Result.OpenKey('Software\Rich Entertainment\'+RegFolder+'\'+SubFolder,CanCreate) then
  begin
    result.Free;
    result:=nil;
  end;
end;

procedure ConvertForschProjekts(var Projekts: Array of TForschProject);
var
  Dummy: Integer;

  procedure AddParent(ID: Cardinal;Parent: Cardinal);
  var
    Dummy: Integer;
  begin
    for Dummy:=0 to High(Projekts) do
    begin
      if Projekts[Dummy].ID=ID then
      begin
        SetLength(Projekts[Dummy].Parents,Length(Projekts[Dummy].Parents)+1);
        Projekts[Dummy].Parents[High(Projekts[Dummy].Parents)]:=Parent;
      end;
    end;
  end;

begin
  for Dummy:=0 to High(Projekts) do
  begin
    AddParent(Projekts[Dummy].NextID1,Projekts[Dummy].ID);
    AddParent(Projekts[Dummy].NextID2,Projekts[Dummy].ID);
    AddParent(Projekts[Dummy].NextID3,Projekts[Dummy].ID);
  end;
end;

function PointsSideBySide(Point1: TPoint; Point2: TPoint): Boolean;
begin
  result:=(Abs(Point1.X-Point2.X)<=1) and (Abs(Point1.Y-Point2.Y)<=1);
end;

{$IFDEF SHOWMEMORY}
function GetProcessMemory: Integer;
var
  pmc: PPROCESS_MEMORY_COUNTERS;
  cb: Integer;
begin
  cb := SizeOf(_PROCESS_MEMORY_COUNTERS);
  GetMem(pmc, cb);
  pmc^.cb := cb;
  if GetProcessMemoryInfo(GetCurrentProcess(), pmc, cb) then
    result:=pmc^.WorkingSetSize
  else
    result:=-1;

  FreeMem(pmc);
end;
{$ENDIF}


const HashTable: Array[Byte] of Cardinal =
  ($646379E0,$1BD5A767,$30BDE508,$4A11DB2A,$58B373D6,$1DF86135,$61A9BE0D,$52C43048,
   $1102956D,$41856123,$36D175B4,$646C5E86,$031374A3,$58A19531,$45A5B1F9,$2FD5A3E4,
   $5ED64778,$5ED60B5F,$130BB8E0,$35925E62,$0F9EE3EF,$571DF1AD,$21A37066,$255A0C00,
   $10FAC005,$4C14961B,$2AAD5E8C,$12874ABE,$6D26F1BA,$204366A9,$6790A951,$7BB5D89C,
   $5B262F10,$7C8FF157,$720116B7,$4380939A,$1276CE07,$4AC9E425,$60C50CBD,$585F79B8,
   $0A68C49D,$4A590D13,$671B9163,$69A5A8F6,$7B13A8D2,$6D0B5A21,$5CC24AA9,$279F5F54,
   $15ECB0A9,$4A02D94F,$349B7E90,$2CCFFAD2,$0BFCB21F,$14C3B89D,$07F41316,$4E4FF970,
   $17B62335,$2662460B,$7C698E3B,$314AF92E,$606B19EA,$46D0EF99,$6A701601,$239DB80C,
   $2D634C41,$044E4347,$4A787067,$5594140A,$7E921036,$06F2EF15,$0EB6036E,$32C70B28,
   $40EC5BCC,$6E5FC103,$2F84D514,$381ABB66,$745EC502,$678BA711,$506F8B59,$44DC62C4,
   $396381D9,$48B1AF3F,$03D56C40,$05005F42,$5C38684E,$215F078D,$53305DC5,$32802EE0,
   $4CA0FDFB,$51FAE5EB,$0BD86F9E,$49C02A1A,$33530089,$21362AB2,$7DA6DF7C,$6366D170,
   $0C8C9D37,$698FF217,$1D685C7A,$1E913A67,$0A2F8205,$7228A21D,$3356E498,$3B595AFD,
   $7F957CF3,$19F940C4,$316795D6,$4700C932,$305E7C01,$19D9740A,$1568AE34,$7E86BB08,
   $155E8D2F,$152581F0,$543F8BB2,$5EDE067E,$05ABDE7D,$1F045076,$0C46AC50,$18C32195,
   $51CCBDEB,$7A4D659B,$5CCBAE0E,$3E32224B,$7F059979,$7A0EE761,$60AD4EEC,$1F7CBEA1,
   $08B39BC8,$6A196CEA,$6E004C96,$6E3B9CF5,$7BC8E8CD,$3D6B0608,$5A7BC22C,$11F640E3,
   $29283846,$0505B563,$51BFD8F1,$5C2C04B9,$792041A4,$34A25C39,$6C85731F,$06F7BFA0,
   $62A98022,$6C798CAE,$2C663D6D,$711BEB25,$27FF71C0,$14ACBCC5,$0AE185DB,$0E4D0D4C,
   $50C0B47E,$0CCD027B,$6524BA69,$0D264C12,$758D065C,$1DF113D1,$0E796917,$6B4F6D77,
   $3CC3455A,$626B46C6,$2ED33FE5,$0642D77E,$465F6F78,$591F915C,$7B7E0CD3,$6EB39023,
   $64F8A2B6,$7A798992,$77EBBDE1,$64933D69,$21DF1D14,$45026568,$0CA2610F,$01B82550,
   $2A5A3C92,$5796FADE,$624A245D,$39232DD6,$4F3DBFF4,$0EDB55CB,$70E5DCFB,$065382EE,
   $3E9CCAAB,$42EC6359,$1CA858C2,$372205CC,$350FD101,$451FDB07,$5ECF6727,$4881E5CA,
   $6B5E28F6,$29B26AD5,$56426E2D,$559020E8,$4C10C88C,$5428E0C3,$5ED173D3,$1C74D526,
   $2D6845C3,$711E2AD1,$2A3B1E1A,$13814084,$32F2D699,$363156FF,$37D2B300,$576DC102,
   $6CC2510E,$5613934D,$50C61885,$30B7D4A0,$6A422B24,$36B62DBB,$4F03D4AB,$0E20195E,
   $16AD7ADB,$17989449,$14C10D72,$72484D3C,$5524F630,$763654F7,$3A9F88D8,$72714E3A,
   $7264F326,$1E951DC5,$1A73ACDE,$04591A58,$221B67BD,$75F2BCB3,$7CAA7F83,$7938CF96,
   $2D931FC1,$4E4FA6C9,$23E2ABF4,$1BBFAFC9,$2BAE54EF,$65B368AF,$48000D72,$72878F3E,
   $04654D39,$0B9C6A22,$45DBDCAC,$087C8361,$0538D6E7,$602BD287,$51AD7EAA,$5B0BA556,
   $2F3758B5,$6B82938D,$7E165BC8,$240B6EED,$7CD7A0A3,$442AB333,$16E09206,$05E67623);

function HashString(Name: String): Cardinal;
var
  Dummy: Integer;
begin
  Name:=lowercase(Name);
  result:=0;
  for Dummy:=1 to length(Name) do
  begin
    result:=(HashTable[Byte(Name[Dummy])] xor result);
    asm
      ROL Result,2
    end;
  end;
end;

function ForschProjektToLagerItem(const Forschung: TForschProject): TLagerItem;
var
  ZaeExt: Integer;
  DummyDate: TKD4Date;
begin
  FillChar(result,sizeOf(result),#0);
  with result do
  begin
    Name:=Forschung.Name;
    Anzahl:=Forschung.Anzahl;
    ID:=Forschung.ID;
    LagerV:=Forschung.LagerV;
    WaffType:=Forschung.WaffType;
    Gewicht:=Forschung.Gewicht;
    KaufPreis:=Forschung.KaufPreis;
    VerKaufPreis:=Forschung.VerKaufPreis;
    Munition:=Forschung.Munition;
    TypeID:=Forschung.TypeID;
    Strength:=Forschung.Strength;
    Schuss:=Forschung.Schuss;
    Munfor:=Forschung.Munfor;
    Panzerung:=Forschung.Panzerung;
    PixPerSec:=Forschung.PixPerSec;

    Laden:=Forschung.Laden;
    Genauigkeit:=Forschung.Genauigkeit;

    Verfolgung:=Forschung.Verfolgung;
    Level:=1;
    ImageIndex:=Forschung.ImageIndex;
    Info:=Forschung.Info;
    Land:=Forschung.Land;
    HerstellBar:=Forschung.HerstellBar;
    Reichweite:=Forschung.Reichweite;
    Einhand:=Forschung.Einhand;
    Power:=Forschung.Power;
    TimeUnits:=Forschung.TimeUnits;
    UFOPaedieImage:=Forschung.UFOPaedieImage;
    Explosion:=Forschung.Explosion;
    ManuAlphatron:=Forschung.ManuAlphatron;
    ProdTime:=Forschung.ProdTime;
    if TypeID=ptExtension then
    begin
      ExtCount:=Forschung.ExtCount;
      FillChar(Extensions,SizeOf(Extensions),#0);
      for ZaeExt:=0 to ExtCount-1 do
        Extensions[ZaeExt]:=Forschung.Extensions[ZaeExt];

    end;
    AlienChance:=Forschung.AlienChance;

    Visible:=not Forschung.AlienItem;
    Useable:=not Forschung.AlienItem;
    AlienItem:=Forschung.AlienItem;
    NeedIQ:=Forschung.NeedIQ;

    //wann es zur Verf�gung gestellt worden ist.
    //wir k�nnen auch auf Forschung.Vor und Forschung.Start pr�ffen
    //und gleich Delta-Time abziehen
    DummyDate := savegame_api_GetDate;
    ResearchedDate:=EncodeDate(DummyDate.Year, DummyDate.Month, DummyDate.Day);
    if Forschung.Start then
      ResearchedDate:=IncMonth(ResearchedDate, -2);

    SoundShoot:=Forschung.ShootSound;
  end;
end;

procedure CleanStringList(List: TStringList);
var
  Dummy: Integer;
begin
  for Dummy:=List.Count-1 downto 0 do
  begin
    if length(List[Dummy])=0 then
    begin
      List.Delete(Dummy);
      Continue;
    end;
    if List[Dummy][1]='#' then
      List.Delete(Dummy);
  end;

end;

function randomMinMax(min,max: Integer): Integer;
begin
  result:=random(max-min)+min;
end;

function RandomPercent(Value: Integer; Percent: Integer): Integer;
begin
  result:=random(round(Value/100*Percent*2))+(Value-round(Value/100*Percent));
end;

function MinMax(Value,min,max: Integer): Integer;
begin
  if Value<min then
    result:=Min
  else if Value>max then
    result:=Max
  else
    result:=Value;
end;

function MinMax(Value,min,max: double): double;overload;
begin
  if Value<min then
    result:=Min
  else if Value>max then
    result:=Max
  else
    result:=Value;
end;

function Matchstrings(Source, pattern: string): Boolean;
var
  pSource: array [0..255] of Char;
  pPattern: array [0..255] of Char;

  function MatchPattern(element, pattern: PChar): Boolean;

    function IsPatternWild(pattern: PChar): Boolean;
    begin
      Result := StrScan(pattern, '*') <> nil;
      if not Result then Result := StrScan(pattern, '?') <> nil; 
    end;
  begin
    if 0 = StrComp(pattern, '*') then
      Result := True
    else if (element^ = Chr(0)) and (pattern^ <> Chr(0)) then
      Result := False
    else if element^ = Chr(0) then
      Result := True
    else
    begin
      case pattern^ of
        '*': if MatchPattern(element, @pattern[1]) then
            Result := True
          else
            Result := MatchPattern(@element[1], pattern);
          '?': Result := MatchPattern(@element[1], @pattern[1]);
        else
          if element^ = pattern^ then
            Result := MatchPattern(@element[1], @pattern[1])
          else
            Result := False;
      end;
    end;
  end;
begin
  StrPCopy(pSource, Source);
  StrPCopy(pPattern, pattern);
  Result := MatchPattern(pSource, pPattern);
end;

function GetMunitionGewicht(FullGewicht: double;MaxSchuesse: Integer;Schuesse: Integer): double;
begin
  if (Schuesse=-1) or (MaxSchuesse=0) then
    result:=FullGewicht
  else
    result:=(FullGewicht/MaxSchuesse)*Schuesse;
end;

function CheckSchussType(TypeID: TProjektType ;WaffenArt: TWaffenType;SchussArt: TSchussTypes): TSchussTypes;
begin
  result:=SchussArt;
  case TypeID of
    ptWaffe   :
    begin
      include(result,stNichtSchiessen);
      case WaffenArt of
        wtShortRange : result:=result*[stSchlag,stStossen,stSchwingen];
        else
          result:=result*[stNichtSchiessen,stGezielt,stSpontan,stAuto];
      end;
    end;
    ptSensor  : result:=[stWerfen,stBenutzen];
    ptGranate : result:=[stWerfen];
    ptMine    : result:=[stBenutzen];
  end;
end;

function NormalizeText(Text: String): String;
begin
  result:=Trim(Text);
  result:=StringReplace(result,#13#10#13#10#13#10,#13#10#13#10,[rfReplaceAll]);
end;

procedure CheckBorders(Value: Integer; Min, Max: Integer; AssertText: String);
begin
  Assert(Value>=Min,Format(AssertText+': zu klein %d',[Value]));
  Assert(Value<=Max,Format(AssertText+': zu gro� %d',[Value]));
end;

function ListViewConfHTML(Listview: TListview; outputFile: string; center: Boolean) : Boolean;
var
  i,f: Integer;
  tfile: TextFile;
begin
  try
    ForceDirectories(ExtractFilePath(outputFile));
    AssignFile(tfile,outputFile);
    ReWrite(tfile);
    WriteLn(tfile,'<html>');
    WriteLn(tfile,'<head>');
    WriteLn(tfile,'<title>HTML-Ansicht: '+listview.Name+'</title>');
    WriteLn(tfile,'</head>');
    WriteLn(tfile,'<table border="1" bordercolor="#000000">');
    WriteLn(tfile,'<tr>');
    for i := 0 to listview.Columns.Count - 1 do
    begin
      if center then
        WriteLn(tfile,'<td><b><center>'+listview.columns[i].caption+'</center></b></td>') else
        WriteLn(tfile,'<td><b>'+listview.columns[i].caption+'</b></td>');
    end;
    WriteLn(tfile,'</tr>');
    WriteLn(tfile,'<tr>');
    for i := 0 to listview.Items.Count-1 do
    begin
      WriteLn(tfile,'<td>'+listview.items.item[i].caption+'</td>');
      for f := 0 to listview.Columns.Count-2 do
      begin
        if listview.items.item[i].subitems[f]='' then Write(tfile,'<td>-</td>') else
          Write(tfile,'<td>'+listview.items.item[i].subitems[f]+'</td>');
      end;
      Write(tfile,'</tr>');
    end;
    WriteLn(tfile,'</table>');
    WriteLn(tfile,'</html>');
    CloseFile(tfile);
    Result := True;
  except
    Result := False;
  end;
end;

initialization
  ScreenshotDir:=IncludeTrailingBackslash(ExtractFilePath(Application.ExeName))+'screenshots\';

end.
