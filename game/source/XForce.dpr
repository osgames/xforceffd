{********************************************************************************
*										*
* Copyright (C) 2004  X-Force Team - jim_raynor@web.de				*
*										*
* This file is part of X-Force: Fight For Destiny (X-Force).			*
*										*
* X-Force is free software; you can redistribute it and/or modify		*
* it under the terms of the GNU General Public License as published by		*
* the Free Software Foundation; either version 2 of the License, or		*
* (at your option) any later version.						*
*										*
* X-Force is distributed in the hope that it will be useful,			*
* but WITHOUT ANY WARRANTY; without even the implied warranty of		*
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the			*
* GNU General Public License for more details.					*
*										*
* You should have received a copy of the GNU General Public License		*
* along with X-Force; if not, write to the Free Software			*
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA	*
*										*
*********************************************************************************
*										*
* basis_api bildet die Schnittstelle zu einer Basisliste, die bis zu 8 Basen	*
* verwaltet   									*
*										*
* WICHTIG!! Bevor die Funktionen genutzt werden k�nnen muss �ber 		*
*           basis_api_init die Basisliste gesetzt werden. Sollte durch 		*
*           savegame_api_init geschehen sein.					*
*										*
********************************************************************************}

program XForce;

{%File 'Versionsnummern.txt'}
{%File 'Settings.inc'}

uses
  MemCheck,
  Forms,
  Dialogs,
  classes,
  Graphics,
  Windows,
  Registry,
  Messages,
  SysUtils,
  frmMain in 'frmMain.pas' {MainForm},
  Defines in 'Defines.pas',
  KD4Utils in 'Units\KD4Utils.pas',
  NewGame in 'pages\NewGame.pas',
  MainPage in 'pages\MainPage.pas',
  StartPage in 'pages\StartPage.pas',
  Error in 'Error.pas' {ErrorDlg},
  XForce_Types in 'components\XForce_Types.pas',
  KD4SaveGame in 'components\KD4SaveGame.pas',
  CreditsPage in 'pages\CreditsPage.pas',
  MonthPage in 'pages\MonthPage.pas',
  VerSoldatPage in 'pages\VerSoldatPage.pas',
  KD4Page in 'pages\KD4Page.pas',
  InfoBasisPage in 'pages\InfoBasisPage.pas',
  InfoStatus in 'pages\InfoStatus.pas',
  VerWerkStatt in 'pages\VerWerkStatt.pas',
  VerNewProjekt in 'pages\VerNewProjekt.pas',
  InfoUfoPaedie in 'pages\InfoUfoPaedie.pas',
  VerLager in 'pages\VerLager.pas',
  VerArbeitsmarkt in 'pages\VerArbeitsmarkt.pas',
  VerBasis in 'pages\VerBasis.pas',
  VerOrgan in 'pages\VerOrgan.pas',
  Blending in 'Units\Blending.pas',
  OptionsPage in 'pages\OptionsPage.pas',
  VerRaumschiffe in 'pages\VerRaumschiffe.pas',
  VerRaumschiff in 'pages\VerRaumschiff.pas',
  UFOKampfIntro in 'pages\UFOKampfIntro.pas',
  UFOKampfPage in 'pages\UFOKampfPage.pas',
  InfoFinanzHilfe in 'pages\InfoFinanzHilfe.pas',
  KampfSimulation in 'pages\KampfSimulation.pas',
  DXContainer in 'components\DXContainer.pas',
  BasisListe in 'components\BasisListe.pas',
  DXBitmapButton in 'components\DXBitmapButton.pas',
  DXCheckBox in 'components\DXCheckBox.pas',
  DXCheckList in 'components\DXCheckList.pas',
  DXCreditsScroll in 'components\DXCreditsScroll.pas',
  DXEdit in 'components\DXEdit.pas',
  DXGroupCaption in 'components\DXGroupCaption.pas',
  DXInfoFrame in 'components\DXInfoFrame.pas',
  DXItemInfo in 'components\DXItemInfo.pas',
  DXLabel in 'components\DXLabel.pas',
  DXListBox in 'components\DXListBox.pas',
  DXMessage in 'components\DXMessage.pas',
  DXMonthStatus in 'components\DXMonthStatus.pas',
  DXPager in 'components\DXPager.pas',
  DXPointStatus in 'components\DXPointStatus.pas',
  DXProgressBar in 'components\DXProgressBar.pas',
  DXQueryBox in 'components\DXQueryBox.pas',
  DXSchiffViewer in 'components\DXSchiffViewer.pas',
  DXScrollBar in 'components\DXScrollBar.pas',
  DXSpinEdit in 'components\DXSpinEdit.pas',
  DXUFOKampf in 'components\DXUFOKampf.pas',
  ForschList in 'components\ForschList.pas',
  LagerListe in 'components\LagerListe.pas',
  OrganisationList in 'components\OrganisationList.pas',
  PersonList in 'components\PersonList.pas',
  RaumschiffList in 'components\RaumschiffList.pas',
  SoldatenList in 'components\SoldatenList.pas',
  ArchivFile in 'components\ArchivFile.pas',
  UFOList in 'components\UFOList.pas',
  WerkstattList in 'components\WerkstattList.pas',
  Koding in 'Units\Koding.pas',
  KD4HighScore in 'components\KD4HighScore.pas',
  DXCanvas in 'components\DXCanvas.pas',
  DXBasisInfo in 'components\DXBasisInfo.pas',
  VerUpgrades in 'pages\VerUpgrades.pas',
  DXTakticScreen in 'components\DXTakticScreen.pas',
  DXListBoxWindow in 'components\DXListBoxWindow.pas',
  KeyBoardOptions in 'pages\KeyBoardOptions.pas',
  Loader in 'Units\Loader.pas',
  DXTextViewer in 'components\DXTextViewer.pas',
  VerLaborPage in 'pages\VerLaborPage.pas',
  Sequenzer in 'components\Sequenzer.pas',
  AlienList in 'components\AlienList.pas',
  DXSchiffConfig in 'components\DXSchiffConfig.pas',
  EinsatzListe in 'components\EinsatzListe.pas',
  EinsatzIntro in 'pages\EinsatzIntro.pas',
  BodenEinsatz in 'pages\BodenEinsatz.pas',
  DXIsoEngine in 'components\DXIsoEngine.pas',
  DXBasisSelector in 'components\DXBasisSelector.pas',
  GameMenu in 'pages\GameMenu.pas',
  MissionList in 'components\MissionList.pas',
  MissionSkriptEngine in 'components\MissionSkriptEngine.pas',
  TraceFile in 'components\TraceFile.pas',
  DXClassicInfoFrame in 'components\DXClassicInfoFrame.pas',
  DirectFont in 'components\DirectFont.pas',
  ISOTools in 'Units\ISOTools.pas',
  DXUIBodenEinsatz in 'components\DXUIBodenEinsatz.pas',
  DXSoldatConfig in 'components\DXSoldatConfig.pas',
  GameFigureManager in 'components\GameFigureManager.pas',
  DXFrameBox in 'components\DXFrameBox.pas',
  IDHashMap in 'components\IDHashMap.pas',
  Compress in 'Units\Compress.pas',
  DXISOMiniMap in 'components\DXISOMiniMap.pas',
  PathFinder in 'components\PathFinder.pas',
  GameFigure in 'components\GameFigure.pas',
  ISOMessages in 'components\ISOMessages.pas',
  EinsatzAuswertung in 'pages\EinsatzAuswertung.pas',
  DXEinsatzPager in 'components\DXEinsatzPager.pas',
  ISOObjects in 'components\ISOObjects.pas',
  VerSoldatEinsatz in 'pages\VerSoldatEinsatz.pas',
  DXSoldatChooser in 'components\DXSoldatChooser.pas',
  ModalDialog in 'components\ModalDialog.pas',
  WatchMarktDialog in 'components\WatchMarktDialog.pas',
  EinsatzKI in 'components\EinsatzKI.pas',
  EinsatzOptionsDialog in 'components\EinsatzOptionsDialog.pas',
  AuftragWatch in 'components\AuftragWatch.pas',
  DXGameMessage in 'components\DXGameMessage.pas',
  SkriptUtils in 'Units\SkriptUtils.pas',
  EarthCalcs in 'Units\EarthCalcs.pas',
  GlobeRenderer in 'components\GlobeRenderer.pas',
  TakticPlugins in 'components\TakticPlugins.pas',
  D3DRender in 'Units\D3DRender.pas',
  NotifyList in 'components\NotifyList.pas',
  AGFImageList in 'components\AGFImageList.pas',
  ISOWaffenObjects in 'components\ISOWaffenObjects.pas',
  map_utils in 'Utils\map_utils.pas',
  ProjektRecords in 'Units\ProjektRecords.pas',
  ConvertRecords in 'Units\ConvertRecords.pas',
  MusikList in 'components\MusikList.pas',
  Musikverwalter in 'components\Musikverwalter.pas',
  stringConst in 'units\stringConst.pas',
  ReadLanguage in 'units\readlanguage.pas',
  Simulator in 'components\Simulator.pas',
  ColorBars in 'Units\ColorBars.pas',
  MapGenerator in 'components\MapGenerator.pas',
  DXHotKey in 'components\DXHotKey.pas',
  ExtRecord in 'components\ExtRecord.pas',
  ObjektRecords in 'Units\ObjektRecords.pas',
  savegame_api in 'api\savegame_api.pas',
  basis_api in 'api\basis_api.pas',
  forsch_api in 'api\forsch_api.pas',
  country_api in 'api\country_api.pas',
  lager_api in 'api\lager_api.pas',
  alien_api in 'api\alien_api.pas',
  person_api in 'api\person_api.pas',
  array_utils in 'Utils\array_utils.pas',
  raumschiff_api in 'api\raumschiff_api.pas',
  ufo_api in 'api\ufo_api.pas',
  town_api in 'api\town_api.pas',
  soldaten_api in 'api\soldaten_api.pas',
  einsatz_api in 'api\einsatz_api.pas',
  game_api in 'api\game_api.pas',
  ufopaedie_api in 'api\ufopaedie_api.pas',
  string_utils in 'utils\string_utils.pas',
  record_utils in 'utils\record_utils.pas',
  archiv_utils in 'utils\archiv_utils.pas',
  highscore_api in 'api\highscore_api.pas',
  werkstatt_api in 'api\werkstatt_api.pas',
  gameset_api in 'api\gameset_api.pas',
  script_api in 'api\script_api.pas',
  register_scripttypes in 'Units\register_scripttypes.pas',
  script_utils in 'utils\script_utils.pas',
  draw_utils in 'utils\draw_utils.pas',
  ui_utils in 'utils\ui_utils.pas',
  game_utils in 'utils\game_utils.pas',
  ErrorDescription in 'ErrorDescription.pas' {frmErrorDescription},
  DXISOTileGroup in 'components\DXISOTileGroup.pas',
  DXBaseBuilder in 'components\DXBaseBuilder.pas',
  earth_api in 'api\earth_api.pas',
  api_utils in 'utils\api_utils.pas',
  formular_utils in 'utils\formular_utils.pas',
  XANBitmap in 'components\XANBitmap.pas';

{$R *.RES}

type
  EInvSize      = class(Exception);
  EFileNotFound = class(Exception);
                
const
  NeededFiles : Array[0..4] of String = ('data\data.pak',
                                         'data\GameSets\default.pak',
                                         'data\[GameName].pak',
                                         'data\models.pak',
                                         'data\sounds.pak');

var
  Reg       : TRegistry;
  Dummy     : Integer;
  SH_Sem    : HWND;
  SH_Wind   : HWND;
  SH_Ins    : boolean;
  Result    : Integer;
  TestFile  : String;
  Dlg       : TErrorDlg;
  MForm     : TMainForm;
begin
//  MemChk;                               
  SH_Sem:=CreateSemaphore(nil,0,1,KD4Semaphore);
  SH_Ins:=(SH_Sem<>0) and (GetLastError=ERROR_ALREADY_EXISTS);
  If SH_Ins then
  begin
    CloseHandle(SH_Sem);
    SH_Wind:=FindWindow(nil,LGameName);
    If SH_Wind<>0 then
    begin
      PostMessage(SH_Wind,WM_SYSCOMMAND,SC_RESTORE,0);
      SetForegroundWindow(SH_Wind);
      exit;
    end;
  end;
  {$IFDEF TRACING}
  GlobalFile.Write('Applikation gestartet');
  GlobalFile.Write('Version',IVersion);
  GlobalFile.Write('*',60);
  {$ENDIF}

  InitDefines;
  Application.Initialize;
  { �berpr�fung der ben�tigten Datein }
  { Falls eine Datei nicht vorhanden  }
  { ist oder die Dateigr�sse ung�ltig }
  { ist, wird der Programmstart abge- }
  { brochen                           }
  Reg:=OpenXForceRegistry;
  CDPath:=IncludeTrailingBackslash(Reg.ReadString('CD-Path'))+'data';

  // Texteladen
  if Reg.ValueExists('Language') then
  begin
    if not ReadLanguageFile(Reg.ReadString('Language')) then
    begin
      MessageBeep(MB_ICONERROR);
      MessageDlg('Error reading language file. Please choose language with autostart programm',mtError,[mbOK],0);
      exit;
    end;
  end
  else
  begin
    if not ReadLanguageFile('deutsch') then
    begin
      MessageBeep(MB_ICONERROR);
      MessageDlg('Error reading language file. Please choose language with autostart programm',mtError,[mbOK],0);
      exit;
    end;
    Reg.WriteString('Language','german');
  end;

  Reg.Free;
  Dlg:=TErrorDlg.Create(Application);
  for Dummy:=0 to high(NeededFiles) do
  begin
    try
      TestFile:=NeededFiles[Dummy];
      TestFile:=StringReplace(TestFile,'[GameName]',LShortName,[rfReplaceAll]);
      TestFile:=StringReplace(TestFile,'[cddata]',CDPath,[rfReplaceAll]);
      if not FileExists(TestFile) then
        raise EFileNotFound.Create(Format(SFileNotFound,[TestFile]));
      {$IFDEF TRACING}
      GlobalFile.Write('Datei korrekt',TestFile);
      {$ENDIF}
    except
      on E: EFileNotFound do
      begin
        {$IFDEF TRACING}
        GlobalFile.Write('FEHLER: Datei nicht gefunden',TestFile);
        {$ENDIF}
        Dlg.MessageLabel.Caption:=E.Message;
//        Dlg.Label4.Caption:=MCDcorrekt;
        Result:=Dlg.ShowModal;
        if Result=ID_CANCEL then
        begin
          ReleaseSemaphore(SH_Sem,1,nil);
          Dlg.Free;
          exit;
        end;
      end;
    end;
  end;
  {$IFDEF TRACING}
  GlobalFile.Write('*',60);
  {$ENDIF}
  MForm:=nil;
  try
    Application.CreateForm(TMainForm, MainForm);
  MForm:=MainForm;
  //  MainForm.FormCreate(nil);
    MainForm.Archiv.OpenArchiv(FDataFile,true);
    MainForm.Archiv.OpenRessource(RNMouseCursor);
    MainForm.GameContainer.Mouse.Bitmap.LoadFromStream(MainForm.Archiv.Stream);
  except
    on E: EResNotFound do
    begin
      StartError(MForm,EMResNotFound,MKD4NewInstall);
      ReleaseSemaphore(SH_Sem,1,nil);
    end;
    on E: EInvalidStream do
    begin
      StartError(MForm,E.Message,MKD4NewInstall);
      ReleaseSemaphore(SH_Sem,1,nil);
    end;
    on E: Exception do
    begin
      StartError(MForm,E.Message,EUnknownError);
      ReleaseSemaphore(SH_Sem,1,nil);
    end;
  end;
  try
    if not Application.Terminated then
    begin
      MainForm.CDDirectory:=CDPath;
      while Mainform.GameContainer.IsLocked do Mainform.GameContainer.DecLock;
      while Mainform.GameContainer.IsLoadGame do Mainform.GameContainer.LoadGame(false);
      MainForm.StartTimer.Enabled:=true;
      Application.Run;
      ReleaseSemaphore(SH_Sem,1,nil);
//      MainForm.Free;
    end;
    Dlg.Free;
  except
  end;
  // Muss drin bleiben, damit diese Funktion nicht wegoptmiert wird
  // Ben�tigt zum Ermitteln der Zeile zu einer Adresse im Tracer
  TextStart;
end.
