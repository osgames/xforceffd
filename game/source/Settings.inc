{********************************************************************************
*										*
* Copyright (C) 2004  X-Force Team - jim_raynor@web.de				*
*										*
* This file is part of X-Force: Fight For Destiny (X-Force).			*
*										*
* X-Force is free software; you can redistribute it and/or modify		*
* it under the terms of the GNU General Public License as published by		*
* the Free Software Foundation; either version 2 of the License, or		*
* (at your option) any later version.						*
*										*
* X-Force is distributed in the hope that it will be useful,			*
* but WITHOUT ANY WARRANTY; without even the implied warranty of		*
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the			*
* GNU General Public License for more details.					*
*										*
* You should have received a copy of the GNU General Public License		*
* along with X-Force; if not, write to the Free Software			*
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA	*
*										*
*********************************************************************************
*										*
* Hier werden k�nnen Compilier-Schalter erstellt werden, um Optionen zum	*
* testen einzubauen. Beim Release muss DEBUGMODE ausgestellt werden.		*
*										*
********************************************************************************}

// Einstellung f�r X-Force

{.$DEFINE DEBUGMODE}

{.$DEFINE TESTVERSION}          // Ver�ffentlichung einer internen Testversion

{.$DEFINE ASSERT_QUERYPERFORMANCECOUNTER}  //QueryPerformanceCounter funktioniert angeblich fast �berall nicht

{$IFDEF DEBUGMODE}
  {.$DEFINE DEBUG}              // Fenstermodus
  {.$DEFINE NOPROGRESS}
  {.$DEFINE TRACING}
  {$DEFINE ERRORREPORT}        // Eintragungen in Error.txt bei Exception
  {$DEFINE MUSICTEST}          // M�glichkeit zur Auswahl der Musikdatei
  {$DEFINE SHOWMEMORY}         // Zeigt den Speicherverbrauch in der rechten unteren Ecke an
  {.$DEFINE TRACETOFILE}        // Alle Debugtexte aus dem GlobalFile werden als Datei abgespeichert}

  {$DEFINE COMPLETEABGLEICH}   // Abgleich aller Daten beim Laden (bisher nur f�r Aliens)

  {$DEFINE INIDATA}            // Holt die Optionen aus der Inidatei

  {.$DEFINE DEBUGMEMMANAGER}    // Aktiviert Memcheck zum Pr�fen des Speichers

  {$DEFINE CRASHTEST}          // Verursacht in der UFOP�die ein Absturz durch Freigabe von Savegame
  {$DEFINE STARTDAYMAX}        // Erm�glicht die Angabe des Angriffstages bei Aliens und UFOs weiter zu erh�hen/verringern

  {$DEFINE AUTOEQUIPSOLDIERS}  // R�stet Soldaten bei Spielbeginn mit Zuf�lligen Ausr�stungen aus

  {.$DEFINE NOLOOSE}

  {$DEFINE ENDLESSMONEY}       // Unendlich viel Geld steht zur Verf�gung

  //----------------------------------------------------------------------------
  // Soldaten ausr�sten
  {.$DEFINE NOSTACKING}         // Erzeugt f�r jede Ausr�stung einzelnen einen Slot
  {$DEFINE ALLMANAGERS}         // Zeigt alle Manager (also auch die Aliens) im Bodeneinsatz beim Ausr�sten an

  //----------------------------------------------------------------------------
  // Raumschiffliste
  {.$DEFINE NOHOMEBASE}         // Ist dieser Schalter gesetzt,wird beim Laden eines
                                // Spielstandes den Raumschiffen keine Homebase zugewiesen
  {$DEFINE INSTANTBUILD}        // Raumschiffe sind sofort nach dem Kauf einsatzbereit und kosten nichts

  //----------------------------------------------------------------------------
  // UFOliste
  {$DEFINE MAXONEUFO}          // Verhindert, dass mehr als ein UFO auftaucht
  {.$DEFINE MASSIVALIENS}       // Sechs mal mehr Aliens pro Bodeneinsatz

  //----------------------------------------------------------------------------
  // UFOKampf
  {.$DEFINE DRAWDEBUG}
  {.$DEFINE DRAWFRAMEBOX}
  {.$DEFINE DRAWFRAME}
  {.$DEFINE NOUFOMOVE}          // UFO bewegt sich nicht mehr
  {$DEFINE NOZOOM}             // Zoom wird abgestellt

  //----------------------------------------------------------------------------
  // L�nder
  {$DEFINE MAXCONFIDENT}       // Alle L�nder haben immer maximales vertrauen

  //----------------------------------------------------------------------------
  // Labor
  {.$DEFINE PROJEKTHISTORIE}
  // Forschliste
  {.$DEFINE WRITEENDED}         // Schreibt beendete Forschungsprojekte in GlobalFile
  {.$DEFINE FASTRESEARCH}       // Forschungszeit kann auf 1% gesetzt werden
  {.$DEFINE SHORTPATENT}        // Patentzeit wird in 10 Minuten gerechnet
  {.$DEFINE COMPLETERESEARCH}   // Aktivieren, damit beim Spielstart also Objekte Verf�gbar sind

  //----------------------------------------------------------------------------
  // Missionsliste
  {.$DEFINE LOTSOFMISSIONS}     // Es kommen bedeutend h�ufiger Missionen
  {.$DEFINE NOLOADOFMISSION}    // L�sst max. 50 Mission im Spielstand �brig
  {.$DEFINE SHOWMISSIONSKRIPT}  // Skript aus dem Spielstand tracen
  {.$DEFINE SAVEMISSIONTEST}    // Speichern w�rend Missionstest erm�glichen
  {.$DEFINE TRACEVARS}          // Zeigt die globalen Variablen nach dem Laden an

  //----------------------------------------------------------------------------
  // BasisListe
  {.$DEFINE CHEAPBASE}          // Eine neue Basis kostet nur 10 Credits
  {$DEFINE FASTBUILD}          // Neue Einrichtungen werden sofort fertig

  //----------------------------------------------------------------------------
  // Bodeneinsatz
  {.$DEFINE NOROUND}            // Bodeneinsatz mit Rundenbasierten Modus deaktivieren

  {.$DEFINE SHOWENEMYTU}        // Zeigt im Hint die Zeiteinheiten der Gegner an
  {.$DEFINE SHOWSTATE}          // fState von TGameFigure im Hint anzeigen

  {$DEFINE NOTREFFER}          // Einheiten werden nicht geschadet

  {$DEFINE ENDLESSMUNITION}    // Unbegrenzte Munition
  {.$DEFINE TRACEWAY}           // Weg einer Einheit in TraceFile ausgeben
  {$DEFINE DEADONRIGHTCLICK}   // Rechtsklick auf Einheit t�tet
  {.$DEFINE TRACETREFFER}       // Informationen zu einem Treffer im Tracefile ablegen
  {.$DEFINE SHOOTDEBUG}         // Verbessert die Anzeige eines Schusses zum testen

  {.$DEFINE ENDLESSUSE}         // Ausr�stung (Granaten, Minen ...) k�nnen beliebig oft benutzt werden
  {.$DEFINE DONTRESETTHROWMODE} // Nach einem Wurf einer Granate, bleibt der Wurf modus aktiviert
  {.$DEFINE NOTUCOST}           // Deaktiviert den Zeiteinheiten im Rundenmodus (achtung, dann nicht n�chste Runde machen)

  {$DEFINE FULLMAP}            // Karte ist komplett sichtbar
  {.$DEFINE SMALLMAP}           // Kreiert immer eine kleine Karte
  {$DEFINE SHOWCOORDINATION}   // Koordinaten der Tiles werden angezeigt
  {.$DEFINE NOSHOOT}            // Keine Sch�sse
  {.$DEFINE NOENEMYS}           // Keine Gegener
  {.$DEFINE CANNOTWIN}          // Kein Sieg m�glich
  {.$DEFINE MINEFROMALL}        // Minen werden von allen Figuren aktiviert
  {.$DEFINE NOCLIPPING}
  {.$DEFINE FASTMOVE}           // Einheiten bewegen sich schneller von einem Feld zum n�chsten

  {.$DEFINE SHOWUNITRECTS}      // Zeigt das DrawingRect der TGameFigure an

  {$DEFINE FASTMOVE}           // Einheiten bewegen sich schneller von einem Feld zum n�chsten

  {$DEFINE SHOWEP}             // Zeigt EPs von Soldaten und Aliens in DXItemInfo an

{$ELSE}
  {$DEFINE ERRORREPORT}        // Eintragungen in Error.txt bei Exception
  {$DEFINE NOZOOM}             // Zoom im UFOKampf deaktivieren
{$ENDIF}

{$IFDEF TESTVERSION}
  {$DEFINE EXTERNALFORMULARS}  // Formeln f�r spielentscheidene Aktionen werden aus der formel.ini geladen
  {$DEFINE DEBUGMEMMANAGER}    // Aktiviert Memcheck zum Pr�fen des Speichers

  {$DEFINE MUSICTEST}          // M�glichkeit zur Auswahl der Musikdatei

  {$DEFINE INIDATA}            // Holt die Optionen aus der Inidatei
{$ENDIF}

{.$DEFINE PUFFERBACKGROUND}     // Beim Start alle Hintergrundbilder laden

{$DEFINE LOWLIGHTDETAIL}       // Erm�glicht nur einfachen Schattenverlauf (notwendig da andere Modis Buggy)

{$DEFINE DISABLEDINVALIDOP}    // Verhindert das bei einer Invalid Op (Gleitkommafehler) eine Exception aus gel�st wird

{$DEFINE DISABLEBEAM}          // Deaktiviert im Bodeneinsatz das Beamfeature
{$DEFINE DISABLEFORMATION}     // Deaktiviert die Anzeige der Formationsbuttons im Bodeneinsatz

{.$DEFINE MULTISELECT}

{$DEFINE HIDEECHTZEITBUTTON}

{$DEFINE CLIPPINGWINDOW}

// Allgemeine Einstellungen im Bodeneinsatz
{$DEFINE NOCUT3D}              // Verhindert, dass Mauern in Ecken abgeschnitten werden

{.$DEFINE AUTOSAVEONWEEKEND}    // Speichert den Spielstand automatisch am Wochenende

{$IFDEF VER100}
  // Delphi 3
  {$DEFINE VER_Delphi3}
{$ENDIF}

{$IFDEF VER120}
  // Delphi 4
  {$DEFINE VER_Delphi4}
{$ENDIF}

{$IFDEF VER130}
  // Delphi 5
  {$DEFINE VER_Delphi5}
{$ENDIF}

{$IFDEF VER140}
  // Delphi 6
  {$DEFINE VER_Delphi6}
{$ENDIF}

{$IFDEF VER150}
  // Delphi 7
  {$DEFINE VER_Delphi7}
{$ENDIF}

{$IFDEF VER170}
  // Delphi 2005
  {$DEFINE VER_Delphi2005}
{$ENDIF}

{$IFDEF VER_Delphi3}
  {$DEFINE HAS_DELPHI3}
{$ENDIF}

{$IFDEF VER_Delphi4}
  {$DEFINE HAS_DELPHI3}
  {$DEFINE HAS_DELPHI4}
{$ENDIF}

{$IFDEF VER_Delphi5}
  {$DEFINE HAS_DELPHI3}
  {$DEFINE HAS_DELPHI4}
  {$DEFINE HAS_DELPHI5}
{$ENDIF}

{$IFDEF VER_Delphi6}
  {$DEFINE HAS_DELPHI3}
  {$DEFINE HAS_DELPHI4}
  {$DEFINE HAS_DELPHI5}
  {$DEFINE HAS_DELPHI6}
{$ENDIF}

{$IFDEF VER_Delphi7}
  {$DEFINE HAS_DELPHI3}
  {$DEFINE HAS_DELPHI4}
  {$DEFINE HAS_DELPHI5}
  {$DEFINE HAS_DELPHI6}
  {$DEFINE HAS_DELPHI7}
{$ENDIF}

{$IFDEF VER_Delphi2005}
  {$DEFINE HAS_DELPHI3}
  {$DEFINE HAS_DELPHI4}
  {$DEFINE HAS_DELPHI5}
  {$DEFINE HAS_DELPHI6}
  {$DEFINE HAS_DELPHI7}
  {$DEFINE HAS_DELPHI2005}
{$ENDIF}

