{********************************************************************************
*										*
* Copyright (C) 2004  X-Force Team - jim_raynor@web.de				*
*										*
* This file is part of X-Force: Fight For Destiny (X-Force).			*
*										*
* X-Force is free software; you can redistribute it and/or modify		*
* it under the terms of the GNU General Public License as published by		*
* the Free Software Foundation; either version 2 of the License, or		*
* (at your option) any later version.						*
*										*
* X-Force is distributed in the hope that it will be useful,			*
* but WITHOUT ANY WARRANTY; without even the implied warranty of		*
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the			*
* GNU General Public License for more details.					*
*										*
* You should have received a copy of the GNU General Public License		*
* along with X-Force; if not, write to the Free Software			*
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA	*
*										*
*********************************************************************************
*										*
* Hauptformular, das alle Oberfl�chen erstellt und wichtige Kommunikationen	*
* zwischen den einzelnen Teilen herstellt					*
*										*
********************************************************************************}

{$I Settings.inc}
unit frmMain;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  DXClass, DXDraws, DXContainer, MainPage, KD4SaveGame, Wave,
  NewGame, KD4Page, AppEvnts, StartPage, DXInput, ExtCtrls, mmSystem, jcl8087,
  KD4HighScore, GameMenu, VerLaborPage, InfoStatus, Registry, CreditsPage,
  MonthPage, XForce_types, VerSoldatPage, InfoBasisPage, VerWerkStatt, VerNewProjekt,
  InfoUfoPaedie, VerLager, VerArbeitsmarkt, VerBasis, ArchivFile, formular_utils,
  Error, VerOrgan, TraceFile, Blending, OptionsPage, VerRaumschiffe, ImgList,
  VerRaumschiff, DirectDraw, DXBitmapButton, RaumschiffList, UFOList,
  UFOKampfIntro, UFOKampfPage, InfoFinanzHilfe, VerSoldatEinsatz, DXISOEngine,
  KampfSimulation, VerUpgrades, KeyBoardOptions, DXUFOKampf, EinsatzIntro,
  BodenEinsatz,DXTakticScreen, DirectFont, EinsatzAuswertung, jclHookExcept,
  jclDebug, JclSysInfo, ComObj, MPlayer, savegame_api, MemCheck, StringConst;

const
  RsOSVersion = '%s %s, Version: %d.%d, Build: %x, "%s"';
  RsProcessor = '%s, %s, %d MHz %s%s';
  MMXText: array[Boolean] of PChar = ('', 'MMX');
  FDIVText: array[Boolean] of PChar = (' [FDIV Bug]', '');

type
  TErrorRecord = record
    ShowError : boolean;
    Message   : String;
    Correct   : String;
  end;

  TKD4PageClass = class of TKD4Page;

  TMainForm = class(TDXForm)
    StartTimer     : TTimer;
    SaveGame       : TKD4SaveGame;
    MessageHandler : TApplicationEvents;
    Timer1         : TTimer;
    DXInput        : TDXInput;
    Arrows         : TDXImageList;
    KD4HighScore   : TKD4HighScore;
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure FormKeyUp(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure FormCreate(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure StartGame(Sender: TObject);
    procedure GameContainerRestoreSurface(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
    procedure MessageHandlerException(Sender: TObject; E: Exception);
    procedure SaveGameUFOKampf(Sender: TObject);
    procedure MessageHandlerActivate(Sender: TObject);
    procedure MessageHandlerDeactivate(Sender: TObject);
    procedure MessageHandlerMessage(var Msg: tagMSG; var Handled: Boolean);
    procedure FormMouseWheelDown(Sender: TObject; Shift: TShiftState;
      MousePos: TPoint; var Handled: Boolean);
    procedure FormMouseWheelUp(Sender: TObject; Shift: TShiftState;
      MousePos: TPoint; var Handled: Boolean);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormDestroy(Sender: TObject);
   private
    fCDData       : String;
    fError        : boolean;
    fLoaded       : boolean;
    fMode         : String;
    procedure EraseBKND(var Msg: TWMEraseBkgnd);message WM_ERASEBKGND;
    function CreatePage(KD4PageClass: TKD4PageClass; Inc: boolean=true): TKD4Page;
    procedure LoadBitmaps;

    procedure LoadSettings;
    procedure SaveSettings;
    
    procedure ExitGame(Sender: TObject);
    procedure IncProgress;
    procedure WMGetDlgCode(var Msg: TWMGetDlgCode);message WM_GETDLGCODE;
    { Private-Deklarationen }
  protected
    procedure CreateWnd;override;
  public
    { Pages Definition }
    GameContainer         : TDXContainer;

    Archiv                : TArchivFile;                              // Archiv mit den Spieldaten
    ShowError             : TErrorRecord;

    StartPage             : TStartPage;                               // StartBildschirm
    MainPage              : TMainPage;                                // Hauptmen�
    UFOKampfIntro         : TUFOKampfIntro;                           // Seite vor einem UFO-Kampf
    UFOKampfPage          : TUFOKampfPage;                            // Seite zun UFO-Kampf
    EinsatzIntro          : TEinsatzIntro;                            // Seite vor einem Bodeneinsatz
    BodenEinsatz          : TBodenEinsatz;
    SoldatEinsatz         : TVerSoldatEinsatz;
    UFOSimulation         : TKampfSimulation;
    constructor Create(AOnwer: TComponent); override;
    property CDDirectory: String read fCDData write fCDData;

    { Public-Deklarationen }
  end;

var
  MainForm      : TMainForm;
  WasError      : Boolean = false;
  ErrorFileName : String;

  WindowMode    : Boolean = false;

procedure StartError(Form: TMainForm;Text, Beheben: String);

implementation

uses Defines, KD4Utils, DXInfoFrame, GlobeRenderer, game_api, highscore_api,
  JclFileUtils, JclDateTime, D3DRender;

var
  StackList : TStringList;

{$R *.DFM}

function GetDirectXVersion: String;
var
  Reg: TRegistry;
begin
  Reg:=TRegistry.Create;
  Reg.RootKey:=HKEY_LOCAL_MACHINE;
  result:='';
  if Reg.OpenKey('Software\Microsoft\DirectX',false) then
  begin
    if Reg.ValueExists('Version') then
    begin
      result:=Reg.ReadString('Version');
    end;
  end;
  Reg.Free;
end;

procedure StartError(Form: TMainForm;Text, Beheben: String);
var
  Dlg: TErrorDlg;
begin
  Dlg:=nil;
  if not WasError then
  begin
    WasError:=true;
    try
      if Form<>nil then
      begin
        Form.GameContainer.Finalize;
        Form.WindowState:=wsMinimized;
      end;
      ShowCursor(true);
      Dlg:=TErrorDlg.Create(Application);
      Dlg.MessageLabel.Caption:=Text;
//      Dlg.Label4.Caption:=Beheben;
      Dlg.BitBtn1.Visible:=false;
      Dlg.ShowModal;
//      while Dlg.Visible do
//        Application.HandleMessage;
    finally
      if Dlg<>nil then
        Dlg.Free;
    end;
    Halt;
  end;
end;

procedure TMainForm.FormKeyPress(Sender: TObject; var Key: Char);
begin
  GameContainer.Perform(WM_CHAR,Word(Key),0);
end;

procedure TMainForm.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
var
  Stream   : TMemoryStream;
begin
  // Startvorgang von X-Force l�uft noch
  if StartTimer.Enabled then
  begin
    CanClose:=false;
    exit;
  end;

  {$IFDEF TRACING}GlobalFile.Write('Schliessen abfrage');{$ENDIF}

  if not (game_api_Question(MExitQuestion,CGameEnd)) then
  begin
    CanClose:=false;
    exit;
  end;
  CanClose:=true;

  GameContainer.StopMusic;

  SaveSettings;

  // Highscore speichern
  {$IFDEF TRACING}GlobalFile.Write('HighScore speichern');GlobalFile.Write('*',60);{$ENDIF}
  Stream:=TMemoryStream.Create;
  try
    if FileExists(FUserDataFile) then
      Archiv.OpenArchiv(FUserDataFile)
    else
      Archiv.CreateArchiv(FUserDataFile);

    KD4HighScore.SaveToStream(Stream);
    Archiv.ReplaceRessource(Stream,RNHighScore);
  except
  end;
  Stream.Free;
  Archiv.Free;
end;

procedure TMainForm.FormKeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  GameContainer.Perform(WM_KEYUP,Key,0);
  if Key=VK_SNAPSHOT then
    MakeScreenShot(Self);
end;

function TMainForm.CreatePage(KD4PageClass: TKD4PageClass; Inc: boolean=true): TKD4Page;
begin
  {$IFDEF TRACING} GlobalFile.Write('Seite erstellen',KD4PageClass.ClassName); {$ENDIF}
  result:=KD4PageClass.Create(GameContainer);
  result.SaveGame:=SaveGame;
  if Inc then
    IncProgress;
end;

procedure TMainForm.LoadBitmaps;
var
  p: TPicture;
begin
  { Schon eingelesene Bilder �berspringen }
  try
    P:=TPicture.Create;

    { Bodeneinsatz }
    Archiv.OpenRessource(RNMouseMap);
    p.Bitmap.LoadFromStream(Archiv.Stream);
    BodenEinsatz.Engine.ISOMap.MouseMap:=p.Bitmap;

    { Soldatenhintergrund }
    IncProgress;

    { Gesichter Laden }
    Archiv.OpenRessource(RNGesichter);
    p.Bitmap.LoadFromStream(Archiv.Stream);
    SaveGame.PersonList.SetFaces(p.Bitmap);
    IncProgress;

    { Organisationssymbole }
    Archiv.OpenRessource(RNOrganBMP);
    p.Bitmap.LoadFromStream(Archiv.Stream);
    SaveGame.Organisations.SetBitmaps(p.Bitmap);
    IncProgress;

    { Erd-Maske f�r UFOListe }
    Archiv.OpenRessource(RNEarthMask);
    gEarthMask.LoadFromStream(Archiv.Stream);

    Archiv.OpenRessource(RNUFos);
    p.Bitmap.LoadFromStream(Archiv.Stream);
    SaveGame.UFOListe.SetBitmap(p.Bitmap);
//    UFOSimulation.UFOListe.SetBitmap(p.Bitmap);
    incProgress;

    { Raumschiffbilder }
    Archiv.OpenRessource(RNSchiffSmall);
    p.Bitmap.LoadFromStream(Archiv.Stream);
    UFOKampfPage.UFOKampf.SetBitmap(p.Bitmap,0);

    Archiv.OpenRessource(RNSymbols);
    p.Bitmap.LoadFromStream(Archiv.Stream);
    UFOKampfPage.UFOKampf.SetBitmap(p.Bitmap,1);
    IncProgress;

    { Ausr�stungssymbole }
    Archiv.OpenRessource(RNRWaffen);
    p.Bitmap.LoadFromStream(Archiv.Stream);
    SaveGame.LagerListe.SetLagerBitmap(p.Bitmap,1,true);

    Archiv.OpenRessource(RNEinrichtIcon);
    p.Bitmap.LoadFromStream(Archiv.Stream);
    SaveGame.BasisListe.SetBitmap(p.Bitmap);

    Archiv.OpenRessource(RNSchiffe);
    p.Bitmap.LoadFromStream(Archiv.Stream);
    SaveGame.Raumschiffe.SetBitmap(p.Bitmap,true);
//    UFOSimulation.Raumschiffe.SetBitmap(p.Bitmap,true);

    Archiv.OpenRessource(RNGSchiffe);
    p.Bitmap.LoadFromStream(Archiv.Stream);
    SaveGame.Raumschiffe.SetBitmap(p.Bitmap,false);
//    UFOSimulation.Raumschiffe.SetBitmap(p.Bitmap,false);

    Archiv.OpenRessource(RNAlienList);
    p.Bitmap.LoadFromStream(Archiv.Stream);
    SaveGame.AlienList.SetBitmap(p.Bitmap);
    Archiv.OpenArchiv(FGameDataFile,true);

    Archiv.OpenRessource(RNWaffenSymbols);
    p.Bitmap.LoadFromStream(Archiv.Stream);
    SaveGame.LagerListe.SetLagerBitmap(p.Bitmap,0,true);
    IncProgress;

    P.Free;
  except
    on E:EResNotFound do
    begin
      StartError(Self,EMResNotFound,MKD4NewInstall);
      exit;
    end;
    on E:Exception do
    begin
      raise;
    end;
  end;
end;

procedure TMainForm.ExitGame(Sender: TObject);
begin
  {$IFDEF TRACING} GlobalFile.Write('Spiel beenden Abfrage'); {$ENDIF}
  Close;
end;

procedure TMainForm.FormCreate(Sender: TObject);
var
  Reg        : TRegistry;
begin
  // Grafikeinstellungen
    Reg:=TRegistry.Create;
    if Reg.OpenKey('Software\Rich Entertainment\'+LShortName+'\Settings',false) then
    begin
      if Reg.ValueExists('TrueColor') then
        Mode32Bit:=Reg.ReadBool('TrueColor');

      {$IFNDEF DEBUG}
      if Reg.ValueExists('WindowedMode') then
        WindowMode:=Reg.ReadBool('WindowedMode');
      {$ELSE}
      WindowMode:=true;
      {$ENDIF}
    end;
    Reg.CloseKey;
    Reg.Free;

  GameContainer:=TDXContainer.Create(Self);
  with GameContainer do
  begin
    Parent:=Self;
    AutoInitialize:=false;
    SetBounds(0,0,ScreenWidth+17,ScreenHeight+17);
    Constraints.MinHeight:=ScreenWidth+17;
    Constraints.MinWidth:=ScreenHeight+17;
    AutoSize:=False;
    Options:=[doAllowReboot,doCenter,do3D,doDirectX7Mode,doHardware,doSelectDriver];
    OnRestoreSurface:=GameContainerRestoreSurface;
    ImageList:=Arrows;
//    MediaPlayer:=Self.MediaPlayer;
  end;

  game_api_SetDXContainer(GameContainer);

  Arrows.DXDraw:=GameContainer;
  fError:=false;
  fLoaded:=false;
  Application.Title:=LGameName;
  Caption:=LGameName;

  Archiv:=TArchivFile.Create;
  Archiv.OpenArchiv(FDataFile,true);

  if WindowMode then
  begin
    AlphaElements:=false;
    ShowCursor(true);
    GameContainer.Cursor:=crNone;
    Mode32Bit:=GetDeviceCaps(GetDC(GetActiveWindow),BITSPIXEL)=32;
    GameContainer.Display.BitCount:=GetDeviceCaps(GetDC(GetActiveWindow),BITSPIXEL);
//    Mode32Bit:=true;
  end
  else
  begin
    BorderStyle:=bsNone;
    GameContainer.Options:=GameContainer.options+[doFullScreen];
//    Mode32Bit:=false;
    if Mode32Bit then
      GameContainer.Display.BitCount:=32
    else
      GameContainer.Display.BitCount:=16;

    GameContainer.Display.Height:=ScreenHeight;
    GameContainer.Display.Width:=ScreenWidth;
  end;

  GameContainer.ShowCursor(false);
  GameContainer.SetCursor(CZeiger);
  try
    TKD4Page(StartPage):=CreatePage(TStartPage,false);
  except
    on E: EResNotFound do
    begin
      StartError(Self,EMResNotFound,MKD4NewInstall);
      exit;
    end;
    on E: Exception do
    begin
      raise;
    end;
  end;
  try
    GameContainer.SurfaceHeight:=ScreenHeight;
    GameContainer.SurfaceWidth:=ScreenWidth;
    GameContainer.Initialize;

    fMode:=IntToStr(GameContainer.Display.BitCount)+' Bit';
    Mode32Bit:=GameContainer.Display.BitCount=32;
  except
    on E:ECanNotDraw do
    begin
    end;
    on E:Exception do
    begin
      GlobalFile.Write('DirectX-Fehler',E.Message);
      raise;
      exit;
    end;
  end;
end;

procedure TMainForm.IncProgress;
begin
  {$IFNDEF NOPROGRESS}
  StartPage.IncLoadProgress;
  {$ENDIF}
end;

procedure TMainForm.EraseBKND(var Msg: TWMEraseBkgnd);
begin
  // Sorgt daf�r, dass kein Hintergrund f�r das Formular angezeigt wird
end;

procedure TMainForm.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  GameContainer.Perform(WM_KEYDOWN,Key,0);
end;

procedure TMainForm.WMGetDlgCode(var Msg: TWMGetDlgCode);
begin
  inherited;
  Msg.Result := DLGC_WANTTAB;
end;

procedure TMainForm.LoadSettings;
var
  Registry : TRegistry;
  Values   : TStringList;
  Dummy    : Integer;
  Setting  : Integer;
begin
  // Standardeinstellungen setzen
  GameContainer.PageAnimation:=true;
  GameContainer.MouseAnimation:=true;
  GameContainer.MouseShadow:=true;
  GameContainer.Volume:=100;
  LightDetail:=ldDoppelt;

  Values:=TStringList.Create;
  Registry:=OpenXForceRegistry('Settings');
  Registry.GetValueNames(Values);
  KeyConfiguration:=GetDefaultKeys;

  for Dummy:=0 to Values.Count-1 do
  begin
    if Values[Dummy]='Volume'             then
    begin
      Setting:=Registry.ReadInteger('Volume');
      if (Setting<0) or (Setting>100) then
        Setting:=100;
      GameContainer.Volume:=Setting;
    end;
    if Values[Dummy]='MusicVolume'        then
    begin
      Setting:=Registry.ReadInteger('MusicVolume');
      if (Setting<0) or (Setting>100) then
        Setting:=100;
      GameContainer.MusicVolume:=Setting;
    end;
    if Values[Dummy]='LightDetail'        then LightDetail:=TLightDetail(Registry.ReadInteger('LightDetail'));
    if Values[Dummy]='FreeLock'           then FreeLook:=Registry.ReadBool('FreeLock');
    if Values[Dummy]='AlwaysFast'         then GameMenu.AlwaysFastTime:=Registry.ReadBool('AlwaysFast');
    if Values[Dummy]='ScrollTempo'        then DXISOEngine.ScrollTempo:=Registry.ReadInteger('ScrollTempo')/100;
    if Values[Dummy]='AlphaBlendFaktor'   then Blending.AlphaBlendFaktor:=Registry.ReadInteger('AlphaBlendFaktor')/100;
    if Values[Dummy]='CompatibleMode'     then DXTakticScreen.SetCompatibleMode(Registry.ReadBool('CompatibleMode'));
    if Values[Dummy]='Gamma'              then GameContainer.Gamma:=Registry.ReadInteger('Gamma');
    if Values[Dummy]='D3DAlphaBlend'      then Enabled3DAlphaBlend:=Registry.ReadBool('D3DAlphaBlend');
    if Values[Dummy]='ShowSensorArea'     then ShowSensorMap:=Registry.ReadBool('ShowSensorArea');
    if Values[Dummy]='Settings'           then
    begin
      Setting:=Registry.ReadInteger('Settings');
      GameContainer.PageAnimation:=GetBit(Setting,1);
      GameContainer.MouseAnimation:=GetBit(Setting,3);
      GameContainer.Scrolling:=not GetBit(Setting,4);
      AlphaElements:=GetBit(Setting,0);
      AlphaControls:=GetBit(Setting,8);
      GameContainer.MouseShadow:=GetBit(Setting,5);
      TransparentEnergy:=GetBit(Setting,7);
      ShowStarField:=GetBit(Setting,6);
      GradNetz:=GetBit(Setting,9);
    end;
    if Values[Dummy]='HotKeys' then Registry.ReadBinaryData('HotKeys',KeyConfiguration,Registry.GetDataSize('HotKeys'));
  end;
  if not AlphaElements then AlphaControls:=false;
  {$IFDEF TRACING}GlobalFile.Write('Volume',GameContainer.Volume);{$ENDIF}

  {$IFDEF LOWLIGHTDETAIL}
  LightDetail:=ldEinfach;
  {$ENDIF}

  Registry.CloseKey;
  Registry.Free;
  Values.Free;
  IncProgress;
end;

procedure TMainForm.StartGame(Sender: TObject);
var
{$IFDEF TRACING}
  Time: Cardinal;
{$ENDIF}
  Dummy: Integer;
  Desc       : TDDPixelFormat;
begin
  // Verhindert einen Abbruch, wenn kurz nach dem Start von X-Force
  // der Task gewechselt wurde
  if not GameContainer.CanDraw then
  begin
    ShowWindow(Handle,SW_MINIMIZE);
    Application.Restore;
    exit;
  end;

  if WindowMode then
  begin
    ClientWidth:=ScreenWidth;
    ClientHeight:=ScreenHeight;
  end
  else
    SetBounds(0,0,ScreenWidth,ScreenHeight);
  
  StartTimer.Enabled:=false;

  GameContainer.MessageLock(true);

  if ShowError.ShowError then
  begin                 
    StartError(Self,ShowError.Message,ShowError.Correct);
    exit;
  end;

  FontEngine.DXDraw:=GameContainer;

  { Farben anpassen }
  if GameContainer.Surface.SurfaceDesc.ddpfPixelFormat.dwRBitMask=$0000F800 then
  begin
    fMode:='16 Bit - 565';
  end;
  bcBlue:=GameContainer.Surface.ColorMatch($00FF0000);
  bcLime:=GameContainer.Surface.ColorMatch($0000FF00);
  bcRed:=GameContainer.Surface.ColorMatch($000000FF);
  bcNavy:=GameContainer.Surface.ColorMatch($00800000);
  bcGreen:=GameContainer.Surface.ColorMatch($00008000);
  bcDarkGreen:=GameContainer.Surface.ColorMatch($00004000);
  bcMaroon:=GameContainer.Surface.ColorMatch($00000080);
  bcPurple:=GameContainer.Surface.ColorMatch($00800080);
  bcTeal:=GameContainer.Surface.ColorMatch($00808000);
  bcOlive:=GameContainer.Surface.ColorMatch($00008080);
  bcYellow:=GameContainer.Surface.ColorMatch($0000FFFF);
  bcFuchsia:=GameContainer.Surface.ColorMatch($00FF00FF);
  bcAqua:=GameContainer.Surface.ColorMatch($00FFFF00);
  bcWhite:=GameContainer.Surface.ColorMatch($00FFFFFF);
  bcDarkNavy:=GameContainer.Surface.ColorMatch($00380000);
  bcSkyBlue:=GameContainer.Surface.ColorMatch($00FF8484);
  bcGray:=GameContainer.Surface.ColorMatch($00404040);
  bcStdGray:=GameContainer.Surface.ColorMatch($00808080);
  bcLightRed:=GameContainer.Surface.ColorMatch($00A0A0F3);
  bcOrange:=GameContainer.Surface.ColorMatch($000080FF);
  bcSilver:=GameContainer.Surface.ColorMatch($00C0C0C0);
  bcDisabled:=GameContainer.Surface.ColorMatch($00303030);
  { ProjektViewer Farben einrichten }
  bcPVOver:=GameContainer.Surface.ColorMatch($00FF4040);
  bcPVHighLight:=GameContainer.Surface.ColorMatch($00000040);
  bcPVTechno:=GameContainer.Surface.ColorMatch($00FF2020);
  bcPVEinricht:=GameContainer.Surface.ColorMatch($00A00000);
  bcPVShip:=GameContainer.Surface.ColorMatch($00600000);
  pcPVNormal:=GameContainer.Surface.ColorMatch($00FF8080);
  coScrollBrush:=GameContainer.Surface.ColorMatch(clMaroon);

  Desc:=GameContainer.Surface.SurfaceDesc.ddpfPixelFormat;

  InitColorTable(Desc.dwRBitMask,Desc.dwBBitMask,Desc.dwGBitMask);

  GameContainer.ActivePage:=StartPage;
  GameContainer.IsDraw:=true;
  GameContainer.Lock;

  LoadSettings;
  IncProgress;

  { SaveGame Komponente Einrichten }
  { Figuren Namen laden }
  try
    Archiv.CloseArchiv;
    Archiv.OpenArchiv(FGameDataFile,true);

    // Personennamen laden
    Archiv.OpenRessource(RNNameDat);
    SaveGame.PersonList.LoadNames(Archiv.Stream);

    // Feste Personennamen laden
    Archiv.OpenRessource(RNFixedNameDat);
    SaveGame.PersonList.LoadFixedNames(Archiv.Stream);

    // L�nder laden
    Archiv.OpenRessource(RNOrganisations);
    SaveGame.Organisations.LoadLandInfo(Archiv.Stream);

    // St�dte laden
    Archiv.OpenRessource(RNTownData);
    SaveGame.Organisations.LoadTownInfo(Archiv.Stream);

    // Sounds laden
    GameContainer.ReadSounds(FSoundFile);

    // Bilder f�r die Arrows-ImageList laden
    Archiv.OpenArchiv(FDataFile,true);
    for Dummy:=0 to Arrows.Items.Count-1 do
    begin
      if Arrows.Items[Dummy].Name<>'' then
      begin
        Archiv.OpenRessource(Arrows.Items[Dummy].Name);
        Arrows.Items[Dummy].Picture.Bitmap.LoadFromStream(Archiv.Stream);
        Arrows.Items[Dummy].Restore;
      end;
    end;
  except
    on E:EResNotFound do
    begin
      StartError(Self,EMResNotFound,MKD4NewInstall);
    end;
    on E:EInvalidVersion do
    begin
      StartError(Self,E.Message,MKD4NewInstall);
    end;
    on E:Exception do
    begin
      raise;
    end;
  end;

  IncProgress;
  { Einrichten des Hauptmen�s }
  TKD4Page(MainPage):=CreatePage(TMainPage);

  {$IFDEF DEBUGMODE}
  MainPage.Version.Text:=Format(IVersion+' DEBUG',[KD4Version])+' - '+fMode;
  {$ELSE}
  MainPage.Version.Text:=Format(IVersion,[KD4Version])+' - '+fMode;
  {$ENDIF}

  MainPage.LoadHelpData;

  // Highscore laden
  if FileExists(FUserDataFile) then
  begin
    Archiv.OpenArchiv(FUserDataFile,true);
    if Archiv.ExistRessource(RNHighScore) then
    begin
      Archiv.OpenRessource(RNHighScore);
      KD4HighScore.LoadFromStream(Archiv.Stream);
    end;
  end;


  Archiv.OpenArchiv(FDataFile,true);

  { Einrichten der Tastatureinstellungen }
  CreatePage(TKeyBoardOptions);

  { Einrichten "Neues Spiel") }
  CreatePage(TNewGame);

  { Einrichten Spielmen� }
  CreatePage(TGameMenu);

  { Einrichten CreditsPage }
  CreatePage(TCreditsPage);

  { Einrichten der Basisverwaltung }
  CreatePage(TVerBasis);

  { Einrichten der Raumschiffverwaltung }
  CreatePage(TVerRaumschiffe);

  { Einrichten der Raumschiffausr�stung }
  CreatePage(TVerRaumschiff);

  { Einrichten der Soldatenverwaltung }
  CreatePage(TVerSoldatPage);

  { Einrichten der Laborverwaltung }
  CreatePage(TVerLaborPage);

  CreatePage(TVerUpgrades);

  { Einrichten der Werkstatt }
  CreatePage(TVerWerkStatt);

  { Einrichten Neues Projekt erstellen }
  CreatePage(TVerNewProjekt);

  { Einrichten der Lagerverwaltung }
  CreatePage(TVerLager);

  { Einrichten des Arbeitsmarktes }
  CreatePage(TVerArbeitsMarkt);

  { Einrichten der Basisinformationen }
  CreatePage(TInfoBasis);

  { Einrichten der Organisationsverwaltung }
  CreatePage(TVerOrgan);

  { Einrichten der Monatsbilanz }
  CreatePage(TInfoStatus);

  { Einrichten der UFOP�die }
  CreatePage(TInfoUfoPaedie);

  { Einrichten der Optionen }
  CreatePage(TOptionsPage);

  { Einrichten des UFO Kampf Seite }
  TKD4Page(UFOKampfPage):=CreatePage(TUFOKampfPage);

  { Einrichten des UFO Kampf Intros }
  TKD4Page(UFOKampfIntro):=CreatePage(TUFOKampfIntro);
  UFOKampfIntro.KampfPage:=UFOKampfPage;

  { Einrichten der Kampfsimulation }
  TKD4Page(UFOSimulation):=CreatePage(TKampfSimulation);
  UFOSimulation.KampfIntro:=UFOKampfIntro;

  { Einrichten des Bodeneinsatz-Briefings }
  TKD4Page(EinsatzIntro):=CreatePage(TEinsatzIntro);

  { Einrichten des Bodeneinsatzes }
  TKD4Page(BodenEinsatz):=CreatePage(TBodenEinsatz);
  EinsatzIntro.EinsatzPage:=BodenEinsatz;

  { Einrichten der Einsatzauswertung }
  CreatePage(TEinsatzAuswertung);

  { Einrichten der Soldatenverwaltung f�r den Bodeneinsatz }
  TKD4Page(SoldatEinsatz):=CreatePage(TVerSoldatEinsatz);

  { Bilder und Einstellungen laden }
  try
    LoadBitmaps;
  except
    on E:Exception do
    begin
      StartError(Self,EMResNotFound,MKD4NewInstall);
    end;
  end;
  if Archiv=nil then exit;
  SaveGame.MissionList.SearchForMissions;
  IncProgress;
  GameContainer.Unlock;
  GameContainer.ShowCursor(true);
  Archiv.CloseArchiv;
  GameContainer.ActivePage:=MainPage;
  MainPage.Animation:=paRightToLeft;

  GameContainer.MessageLock(false);

  {$IFDEF TRACING}
  Time:=GetTickCount-Time;
  GlobalFile.Write('Startzeit',Time);
  GlobalFile.Write('Prozessfortschritte',StartPage.StartBar.Value);
  GlobalFile.Write('*',60);
  {$ENDIF}
end;

procedure TMainForm.GameContainerRestoreSurface(Sender: TObject);
begin
  { Beim Restore Werden die Credits angehalten }
  GameContainer.Lock;
  Timer1.Enabled:=true;
end;

procedure TMainForm.Timer1Timer(Sender: TObject);
begin
  while GameContainer.IsLocked do GameContainer.UnLock;
  while GameContainer.IsLoadGame do GameContainer.LoadGame(false);
  GlobalFile.Write('Timer1.Restore');
  GameContainer.ReDraw(GameContainer.Surface);
  Timer1.Enabled:=false;
end;

procedure TMainForm.MessageHandlerException(Sender: TObject; E: Exception);
var
  ErrorFile: TTraceFile;
  CpuInfo  : TCpuInfo;
  Reg      : TRegistry;
  List     : TStringList;
  Test     : TDDDeviceIdentifier2;
  DD       : TDDCaps_DX7;
  Hel      : TDDCaps_DX7;
  {$IFDEF ERRORREPORT}
  Dummy    : Integer;
  {$ENDIF}
begin
  if (E is ENotEnoughMoney) or (E is ENotEnoughTimeUnits) or (E is ENotEnoughRoom) then
  begin
    GameContainer.UnLockAll;
    game_api_MessageBox(E.Message,CCancelAction);
    exit;
  end;
  // Pr�fen, ob X-Force verloren wurde
  if (E is ELooseGame) then
  begin
    {$IFNDEF NOLOOSE}
    GameContainer.ActivePage:=MainPage;
    MainPage.BackButtonActive:=false;
    game_api_MessageBox(E.Message,'Niederlage');
    {$ENDIF}
    exit;
  end;

  Set8087Precision(pcDouble);
  {$IFDEF ERRORREPORT}
  ErrorFileName:=SysUtils.FormatDateTime('"error-"dd-mm-yyyy-hh-nn".txt"',SysUtils.Date+SysUtils.Time);
  ErrorFile:=TTraceFile.Create(ErrorFileName);
  ErrorFile.New:=true;
  ErrorFile.Write('Nicht behandelte Exception in '+LShortName+' ausgel�st.');
  ErrorFile.WriteLine;

  // Systeminformationen zusammentragen
  ErrorFile.Write('Systemdaten:');
  ErrorFile.Write('  X-Force BuildDate',DateTimeToStr(FileTimeToLocalDateTime(GetFileLastWrite(Application.ExeName))));
  ErrorFile.Write('  X-Force Version',KD4Version);
  ErrorFile.Write('  X-Force Sprache',Language);
  if SaveGame.OpenGame then
  begin
    ErrorFile.WriteLine;
    ErrorFile.Write('Spielsatz:');
    ErrorFile.Write('  Datei',SaveGame.Gameset.FileName);
    ErrorFile.Write('  Name',SaveGame.Gameset.Name);
  end;

  ErrorFile.WriteLine;
  ErrorFile.Write('Installierte Updates');

  Reg:=OpenXForceRegistry('updates');
  List:=TStringList.Create;
  Reg.GetValueNames(List);
  for Dummy:=0 to List.Count-1 do
  begin
    ErrorFile.Write(List[Dummy],Reg.ReadString(List[Dummy]));
  end;
  Reg.Free;

  // Informationen zur Grafikkarte ermitteln
  ErrorFile.WriteLine;
  ErrorFile.Write('Direct X-Version',GetDirectXVersion);
  ErrorFile.Write('Grafik-Modus',fMode);

  if GameContainer.Initialized then
  begin
    GameContainer.DDraw.IDraw7.GetDeviceIdentifier(Test,0);
    ErrorFile.Write('Grafikkarte',Test.szDescription);
                                           
    DD.dwSize:=sizeOf(DD);
    HEL.dwSize:=sizeOf(Hel);
    GameContainer.DDraw.IDraw7.GetCaps(Addr(DD),Addr(Hel));
    ErrorFile.Write('Videospeicher',Format('%.0n frei / %.0n gesamt',[DD.dwVidMemFree/1,DD.dwVidMemTotal/1]));
  end;

  // BIOS-Informationen                          
  ErrorFile.WriteLine;
  ErrorFile.Write('BIOS-Name',GetBIOSName);
  ErrorFile.Write('BIOS-Copyright',GetBIOSCopyright);
  ErrorFile.Write('BIOS-Info',GetBIOSExtendedInfo);
  ErrorFile.Write('BIOS-Date',DateTimeToStr(GetBIOSDate));
  ErrorFile.WriteLine;

  // laufende Prozesse
{  ErrorFile.Write('Laufende Prozesse:');
  List.Clear;
  RunningProcessesList(List);
  ErrorFile.Write(List);
  ErrorFile.WriteLine;}

  ErrorFile.Write('Betriebssystem',Format(RsOSVersion, [GetWindowsVersionString, NtProductTypeString, Win32MajorVersion, Win32MinorVersion, Win32BuildNumber, Win32CSDVersion]));
  GetCpuInfo(CpuInfo);
  with CpuInfo do
    ErrorFile.Write('Prozessor',Format(RsProcessor, [Manufacturer, CpuName,RoundFrequency(FrequencyInfo.NormFreq),MMXText[MMX], FDIVText[IsFDIVOK]]));

  ErrorFile.Write('Arbeitsspeicher',Format('%.0n frei / %.0n gesamt',[GetFreePhysicalMemory/1,GetTotalPhysicalMemory/1]));
  ErrorFile.WriteLine;

  // Codestack f�r die Auswertung zusammenstellen
  ErrorFile.Write('Exceptionklasse',E.ClassName);
  if (GameContainer.ActivePage<>nil) then
    ErrorFile.Write('Seite',GameContainer.ActivePage.ClassName);
  ErrorFile.Write('Exception an Adresse',true,Integer(ExceptAddr));
  ErrorFile.Write('Exception Message',E.Message);
  ErrorFile.WriteLine;
  ErrorFile.Write(Format('Stackdump, Textstart $%.8x',[Integer(@Textstart)]));
  ErrorFile.Write(StackList);
  ErrorFile.Free;

  List.Free;
  {$ENDIF}
  StartError(Self,E.Message,ESendEMail);
end;

procedure TMainForm.SaveGameUFOKampf(Sender: TObject);
begin
  UFOKampfIntro.BerechneKampf;
  EinsatzIntro.ParentPage:=PageGameMenu;
  EinsatzIntro.BerechneEinsatze;
end;

procedure TMainForm.MessageHandlerActivate(Sender: TObject);
begin
  if not fError then
    GameContainer.Stop(false);
end;

procedure TMainForm.MessageHandlerDeactivate(Sender: TObject);
begin
  if not fError then
    GameContainer.Stop;
end;


procedure TMainForm.MessageHandlerMessage(var Msg: tagMSG;
  var Handled: Boolean);
begin
  case Msg.Message of
    WM_HSCROLL:
    begin
      case LOWORD(Msg.wParam) of
        SB_LINELEFT,SB_PAGELEFT,SB_LEFT: GameContainer.DoScrollMessage(ScreenToClient(Msg.pt),sdLeft);
        SB_LINERIGHT,SB_PAGERIGHT,SB_RIGHT: GameContainer.DoScrollMessage(ScreenToClient(Msg.pt),sdRight);
      end;
    end;
    WM_VSCROLL:
    begin
      case LOWORD(Msg.wParam) of
        SB_LINEUP,SB_PAGEUP,SB_TOP: GameContainer.DoScrollMessage(ScreenToClient(Msg.pt),sdUp);
        SB_LINEDOWN,SB_PAGEDOWN,SB_BOTTOM: GameContainer.DoScrollMessage(ScreenToClient(Msg.pt),sdDown);
      end;
    end;
  end;
end;

procedure TMainForm.CreateWnd;
var
  Region: HRGN;
begin
  inherited;
  if WindowMode then
  begin
    Region:=CreateRectRgn(0,0,ScreenWidth,ScreenHeight);
    SetWindowRgn(Handle,Region,true);
  end;
end;

procedure TMainForm.FormMouseWheelDown(Sender: TObject; Shift: TShiftState;
  MousePos: TPoint; var Handled: Boolean);
begin
  GameContainer.DoScrollMessage(MousePos,sdDown);
end;

procedure TMainForm.FormMouseWheelUp(Sender: TObject; Shift: TShiftState;
  MousePos: TPoint; var Handled: Boolean);
begin
  GameContainer.DoScrollMessage(MousePos,sdUp);
end;

constructor TMainForm.Create(AOnwer: TComponent);
begin
  inherited;

  {$IFDEF EXTERNALFORMULARS}
  formular_utils_readformulars(IncludeTrailingBackSlash(ExtractFilePath(Application.ExeName))+'formular.ini');
  {$ENDIF}
  ShowError.ShowError:=false;

  game_api_SetDirectInput(DXInput);
  game_api_SetQuitEvent(ExitGame);

  highscore_api_init(KD4HighScore);
end;

procedure TMainForm.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  Action:=caFree;           
end;

procedure TMainForm.FormDestroy(Sender: TObject);
begin
  GameContainer.Free;
end;

var
  IsTraceStack : Boolean = false;

{$IFDEF ERRORREPORT}
procedure HookException(ExceptObj: TObject; ExceptAddr: Pointer; OSException: Boolean);
var
  BasePtr  : Cardinal;
  StackTop : Cardinal;
  LastAdr  : Cardinal;
  Count    : Integer;
begin
  if IsTraceStack then
    exit;

  IsTraceStack:=true;
  asm
    mov BasePtr,ebp
    MOV eax, FS:[4]
    mov stacktop,eax
  end;
//  StackList.Clear;
  {$IFDEF DEBUGMODE}
  GlobalFile.StartStackDump;
  GlobalFile.WriteLine;
  GlobalFile.Write(Exception(ExceptObj).Message);
  GlobalFile.WriteLine;
  GlobalFile.Write('X-Force Version',KD4Version);
  GlobalFile.Write('Exception Adresse',true,Integer(ExceptAddr));
  GlobalFile.Write(Format('Stackdump, Textstart $%.8x',[Integer(@Textstart)]));
  GlobalFile.WriteLine;
  GlobalFile.Write('  $'+IntToHex(Integer(ExceptAddr),8));
  {$ENDIF}
  Count:=0;
  StackList.Add(Exception(ExceptObj).Message);
  StackList.Add('  $'+IntToHex(Integer(ExceptAddr),8));
  if not OSException then
  begin
    try
      while (BasePtr<stackTop) and (BasePtr<>0) do
      begin
        LastAdr:=PDWord(BasePtr+4)^-5;
        if LastAdr>$10000000 then break;
        if Count>2 then
        begin
          StackList.Add('  $'+IntToHex(LastAdr,8));
          {$IFDEF DEBUGMODE}
          GlobalFile.Write('  $'+IntToHex(LastAdr,8));
          {$ENDIF}
        end;
        BasePtr:=PDWord(BasePtr)^;
        inc(Count);
      end;
    except
      IsTraceStack:=false;
      StackList.Add('--------------------------');
      exit;
    end;
  end
  else
  begin
    try
      while (BasePtr<stackTop) and (BasePtr<>0) do
      begin
        LastAdr:=PDWord(BasePtr+4)^-5;
        if (LastAdr<$10000000) and (LastAdr>Cardinal(Addr(TextStart))) then
        begin
          if Count>2 then
          begin
            StackList.Add('  $'+IntToHex(LastAdr,8));
            {$IFDEF DEBUGMODE}
            GlobalFile.Write('  $'+IntToHex(LastAdr,8));
            {$ENDIF}
          end;
          inc(Count);
        end;
        BasePtr:=PDWord(BasePtr)^;
      end;
    except
      IsTraceStack:=false;
      StackList.Add('--------------------------------');
      exit;
    end;
  end;
  StackList.Add('--------------------------------');
  {$IFDEF DEBUGMODE}
  GlobalFile.EndStackDump;
  GlobalFile.WriteLine;
  {$ENDIF}
  IsTraceStack:=false;
end;
{$ENDIF}

var
  ControlWord: T8087Exceptions;

procedure TMainForm.SaveSettings;
var
  Registry: TRegistry;
  Setting : Integer;
begin
  // Einstellungen speichern
  {$IFDEF TRACING}GlobalFile.Write('*',60);GlobalFile.Write('Einstellungen speichern');{$ENDIF}
  Registry:=OpenXForceRegistry;
  Registry.WriteInteger('Version',KD4Version);
  Registry.OpenKey('Updates',true);
  Registry.WriteString('XFORCE-MAIN',MainVersion);
  Registry.Free;

  Registry:=OpenXForceRegistry('Settings');
  Registry.WriteInteger('Volume',GameContainer.Volume);
  Registry.WriteInteger('MusicVolume',GameContainer.MusicVolume);
  Registry.WriteBinaryData('HotKeys',KeyConfiguration,SizeOf(KeyConfiguration));

  Registry.WriteBool('AlwaysFast',AlwaysFastTime);
  Registry.WriteBool('FreeLock',FreeLook);
  Registry.WriteInteger('ScrollTempo',round(DXISOEngine.ScrollTempo*100));
  Registry.WriteInteger('AlphaBlendFaktor',round(Blending.AlphaBlendFaktor*100));
  Registry.WriteInteger('Gamma',GameContainer.Gamma);
  Registry.WriteBool('CompatibleMode',DXTakticScreen.GetCompatibleMode);
  Registry.WriteBool('D3DAlphaBlend',Enabled3DAlphaBlend);
  Registry.WriteBool('ShowSensorArea',ShowSensorMap);

  Setting:=0;
  if AlphaElements then SetBit(Setting,0);
  if GameContainer.PageAnimation then SetBit(Setting,1);
  if GameContainer.MouseAnimation then SetBit(Setting,3);
  if not GameContainer.Scrolling then SetBit(Setting,4);
  if GameContainer.MouseShadow then SetBit(Setting,5);
  if ShowStarField then SetBit(Setting,6);
  if TransparentEnergy then SetBit(Setting,7);
  if AlphaControls then SetBit(Setting,8);
  if GradNetz then SetBit(Setting,9);

  Registry.WriteInteger('Settings',Setting);
  Registry.WriteInteger('LightDetail',Ord(LightDetail));
  Registry.Free;
 end;

initialization
  StackList:=TStringList.Create;
  {$IFDEF ERRORREPORT}
  if not JclHookExceptions then
    GlobalFile.Write('JclHookExceptions hat nicht geklappt');
  JclAddExceptNotifier(HookException, npFirstChain);
  {$ENDIF}

  {$IFDEF DISABLEDINVALIDOP}
  ControlWord:=GetMasked8087Exceptions;
  Include(ControlWord,emInvalidOp);
  Exclude(ControlWord,emZeroDivide);
  SetMasked8087Exceptions(ControlWord);
  {$ENDIF}

finalization
  StackList.Free;
end.
