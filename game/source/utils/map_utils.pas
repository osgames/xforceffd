{********************************************************************************
*										*
* Copyright (C) 2004  X-Force Team - jim_raynor@web.de				*
*										*
* This file is part of X-Force: Fight For Destiny (X-Force).			*
*										*
* X-Force is free software; you can redistribute it and/or modify		*
* it under the terms of the GNU General Public License as published by		*
* the Free Software Foundation; either version 2 of the License, or		*
* (at your option) any later version.						*
*										*
* X-Force is distributed in the hope that it will be useful,			*
* but WITHOUT ANY WARRANTY; without even the implied warranty of		*
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the			*
* GNU General Public License for more details.					*
*										*
* You should have received a copy of the GNU General Public License		*
* along with X-Force; if not, write to the Free Software			*
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA	*
*										*
*********************************************************************************
*										*
* Utitlities um Maps-Dateien zu verwalten.					*
*										*
********************************************************************************}

{$I ../Settings.inc}

unit map_utils;

interface

uses XForce_Types, string_utils;

{ Karte }

type
  TArrayTileInfo = Array of TUsedTileInfo;

  TMapInfo = (miHotSpots);

function ValidMapVersion(Version: Cardinal): boolean;

function MapVersionHasInfo(Version: Cardinal; Info: TMapInfo): Boolean;

// Liest eine komplette m3d-Datei ein. Nach dem Laden stehen die verschiedenen
// map_utils Funktionen zum Zugriff auf die Daten zur Verf�gung
// Achtung! Bisher werden nur die Tilesets und die ben�tigten Tiles/Walls eingelesen
procedure map_utils_readMapInfos(FileName: String);

function map_utils_GetUsedTileSets: TStringArray;

function map_utils_GetUsedTiles: TArrayTileInfo;
function map_utils_GetUsedWalls: TArrayTileInfo;

implementation

uses Classes, SysUtils;

var
  g_IsMapLoaded  : Boolean = false;
  g_usedTileSets : TStringArray;
  g_usedTiles    : TArrayTileInfo;
  g_usedWalls    : TArrayTileInfo;

function ValidMapVersion(Version: Cardinal): boolean;
begin
  result:=false;
  case Version of
    $1512AB37: result:=true;
    $0D16A44F: result:=true;
  end;
end;

function MapVersionHasInfo(Version: Cardinal; Info: TMapInfo): Boolean;
begin
  case Info of
    miHotSpots : result:=Version=$1512AB37;
  end;
end;

procedure map_utils_readMapInfos(FileName: String);
var
  fs: TFileStream;

  ID       : Cardinal;
  TileSets : String;

  procedure ReadTileInfoArray(var Arr: TArrayTileInfo);
  var
    Count: Integer;
    Dummy: Integer;
  begin
    fs.Read(Count,SizeOf(Count));
    SetLength(Arr,Count);
    for Dummy:=0 to Count-1 do
    begin
      fs.Read(Arr[Dummy].ID,sizeOf(Cardinal));
      fs.Read(Arr[Dummy].TileSetIndex,SizeOf(Integer));
      fs.Read(Arr[Dummy].Index,SizeOf(Integer));
    end;
  end;

begin
  g_IsMapLoaded:=false;
  fs:=TFileStream.Create(FileName,fmOpenRead);

  // Karten ID pr�fen
  try
    fs.Read(ID,SizeOf(ID));
    if not ValidMapVersion(ID) then
    begin
      raise Exception.Create('Ung�ltiges Format der Karte');
    end;

    // Benutzte Tilesets einlesen
    TileSets:=string_utils_ReadString(fs);

    g_usedTileSets:=string_utils_explode(#13#10,TileSets);

    ReadTileInfoArray(g_usedTiles);
    ReadTileInfoArray(g_usedWalls);
    g_IsMapLoaded:=true;
  finally
    fs.Free;
  end;
end;

function map_utils_GetUsedTileSets: TStringArray;
begin
  assert(g_isMapLoaded,'Keine Karte ge�ffnet');
  result:=g_usedTileSets;
end;

function map_utils_GetUsedTiles: TArrayTileInfo;
begin
  assert(g_isMapLoaded,'Keine Karte ge�ffnet');
  result:=g_usedTiles;
end;

function map_utils_GetUsedWalls: TArrayTileInfo;
begin
  assert(g_isMapLoaded,'Keine Karte ge�ffnet');
  result:=g_usedWalls;
end;

end.
