unit api_utils;

interface

uses
  alien_api, basis_api, country_api, earth_api, einsatz_api, forsch_api,
  KD4SaveGame, lager_api, person_api, raumschiff_api, savegame_api,
  soldaten_api, town_api, ufo_api, werkstatt_api;

procedure api_reinit(SaveGame: TKD4SaveGame);

implementation

procedure api_reinit(SaveGame: TKD4SaveGame);
begin
  savegame_api_init(SaveGame);
  basis_api_init(SaveGame.BasisListe);
  forsch_api_init(Savegame.ForschListe);
  country_api_init(Savegame.Organisations);
  lager_api_init(SaveGame.LagerListe);
  alien_api_init(Savegame.AlienList);
  person_api_init(Savegame.PersonList);
  raumschiff_api_init(SaveGame.Raumschiffe);
  ufo_api_init(Savegame.UFOListe);
  town_api_init(Savegame.Organisations);
  einsatz_api_init(SaveGame.EinsatzList);
  werkstatt_api_init(Savegame.WerkStatt);
  soldaten_api_init(Savegame.SoldatenListe);
end;

end.
