{********************************************************************************
*										*
* Copyright (C) 2004  X-Force Team - jim_raynor@web.de				*
*										*
* This file is part of X-Force: Fight For Destiny (X-Force).			*
*										*
* X-Force is free software; you can redistribute it and/or modify		*
* it under the terms of the GNU General Public License as published by		*
* the Free Software Foundation; either version 2 of the License, or		*
* (at your option) any later version.						*
*										*
* X-Force is distributed in the hope that it will be useful,			*
* but WITHOUT ANY WARRANTY; without even the implied warranty of		*
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the			*
* GNU General Public License for more details.					*
*										*
* You should have received a copy of the GNU General Public License		*
* along with X-Force; if not, write to the Free Software			*
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA	*
*										*
*********************************************************************************
*										*
* Utitlities um Archiv-Dateien (*.pak) zu verwalten				*
*										*
********************************************************************************}

{$I ../Settings.inc}

unit archiv_utils;

interface

uses ArchivFile, XForce_Types;

// Liest aus einem Archiv die angegebene Ressource komplett in einen String
function archiv_utils_ReadToString(Archiv: String; Ressource: String): String;

function archiv_utils_OpenSpecialRessource(Ressource: String; GameSetFile: String): TArchivFile;

implementation

uses Defines;

function archiv_utils_ReadToString(Archiv: String; Ressource: String): String;
var
  ArchFile: TArchivFIle;
begin
  ArchFile:=TArchivFile.Create;
  ArchFile.OpenArchiv(Archiv,true);
  Assert(ArchFile.ExistRessource(Ressource));
  ArchFile.OpenRessource(Ressource);

  SetString(result, nil, ArchFile.Stream.Size);
  ArchFile.Stream.Read(Pointer(result)^, ArchFile.Stream.Size);

  ArchFile.Free;
end;

function archiv_utils_OpenSpecialRessource(Ressource: String; GameSetFile: String): TArchivFile;
var
  Ort   : String;
  Ress  : String;
begin
  result:=nil;
  if Ressource='' then
    exit;

  Ort:=Copy(Ressource,1,Pos('\',Ressource)-1);
  Ress:=Copy(Ressource,Pos('\',Ressource)+1,length(Ressource));

  result:=TArchivFile.Create;
  if Ort='default' then
  begin
    if Copy(Ress,1,pos(':',Ress)-1)='Sound' then
      result.OpenArchiv(FSoundFile,true)
    else
      result.OpenArchiv(FGameDataFile,true);
  end
  else if Ort='user' then
    result.OpenArchiv(GameSetFile,true)
  else
    Assert(false,'Ungültiger Ressourcen-String');

  if result.ExistRessource(Ress) then
    result.OpenRessource(Ress)
  else
  begin
    result.Free;
    result:=nil;
  end;
end;

end.
