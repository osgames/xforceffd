unit UnicodeHelper;

interface

uses
  Classes;

procedure FileToStringList(const FileName: string; const StringList: TStringList);

implementation

uses
  SysUtils, cUnicodeCodecs, ParserUtils;

procedure FileToStringList(const FileName: string; const StringList: TStringList);
var
  FileStream: TFileStream;
  MemoryStream: TMemoryStream;
  Result: TStream;
  Reader: TUtilsBOMReader;
  Transcoder: TUtilsStreamTranscoder;
  Buffer: array[0..1] of Byte;
  ByteOrderMarkType: TUnicodeCodecClass;
begin
  FileStream := TFileStream.Create(FileName, fmOpenRead or fmShareCompat);
  try
    MemoryStream := nil;
    Reader := TUtilsBOMReader.Create(FileStream, 8);
    try
      ByteOrderMarkType := Reader.ByteOrderMarkType;
      if ByteOrderMarkType = nil then
      begin
        Result := FileStream;
        //Check the first 2 bytes for optimistic check - some editors don't save BOM
        FileStream.Read(Buffer, 2);
        FileStream.Position := 0;
        if Buffer[1] = 0 then //We expect identifiers - defined in ASCII, so unicode characters have 2nd byte '0'
        begin
          ByteOrderMarkType := TUTF16LECodec;
        end
        else
        begin
          if Buffer[0] = 0 then  //same as above, just big endian
            ByteOrderMarkType := TUTF16BECodec;
        end;
      end;

      if ByteOrderMarkType <> nil then
      begin
        MemoryStream := TMemoryStream.Create;
        Transcoder := TUtilsStreamTranscoder.Create(FileStream, MemoryStream, 2048);
        try
          Transcoder.InputEncoding := GetEncodingName(ByteOrderMarkType);
          Transcoder.OutputEncoding := GetSystemEncodingName;
          Transcoder.Transcode;
          Result := MemoryStream;
          MemoryStream.Position := 0;
        finally
          Transcoder.Free;
        end;
      end;
    finally
      Reader.Free;
    end;
    StringList.LoadFromStream(Result);
    MemoryStream.Free;
  finally
    FileStream.Free;
  end;
end;

end.
 
