{********************************************************************************
*										*
* Copyright (C) 2004  X-Force Team - jim_raynor@web.de				*
*										*
* This file is part of X-Force: Fight For Destiny (X-Force).			*
*										*
* X-Force is free software; you can redistribute it and/or modify		*
* it under the terms of the GNU General Public License as published by		*
* the Free Software Foundation; either version 2 of the License, or		*
* (at your option) any later version.						*
*							    			*
* X-Force is distributed in the hope that it will be useful,			*
* but WITHOUT ANY WARRANTY; without even the implied warranty of		*
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the			*
* GNU General Public License for more details.					*
*										*
* You should have received a copy of the GNU General Public License		*
* along with X-Force; if not, write to the Free Software			*
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA	*
*										*
*********************************************************************************
*										*
* Utilities um mit ExtRecords zu arbeiten					*
*										*
********************************************************************************}

{$I ../Settings.inc}

unit record_utils;

interface

uses         
  ExtRecord;

// Sucht in einem Record-Array nach einem Objekt, das im String-Feld Field den
// Wert Value besitzt
function record_utils_FindString(const Records: TRecordArray; Field: String; Value: String): TExtRecord;

// Sucht in einem Record-Array nach einem Objekt, das im Cardinal-Feld Field den
// Wert Value besitzt
function record_utils_FindCardinal(const Records: TRecordArray; Field: String; Value: Cardinal): TExtRecord;

// Sucht in einem Record-Array nach einem Objekt mit der �bergebenen ID
// Vorraussetzung ist, dass alle Records im Array eine Cardinal-Eigenschaft ID
// besitzen
function record_utils_FindID(const Records: TRecordArray;ID: Cardinal): TExtRecord;

// Gibt alle Records in einem Array frei und setzt die gr��e auf 0
procedure record_utils_ClearArray(var Records: TRecordArray);

function record_utils_AddRecord(var Records: TRecordArray; Rec: TExtRecord): Integer;

implementation

function record_utils_FindString(const Records: TRecordArray; Field: String; Value: String): TExtRecord;
var
  Dummy: Integer;
begin
  result:=nil;
  for Dummy:=0 to high(Records) do
  begin
    if Records[Dummy].GetString(Field)=Value then
    begin
      result:=Records[Dummy];
      exit;
    end;
  end;
end;

function record_utils_FindCardinal(const Records: TRecordArray; Field: String; Value: Cardinal): TExtRecord;
var
  Dummy: Integer;
begin
  result:=nil;
  for Dummy:=0 to high(Records) do
  begin
    if Records[Dummy].GetCardinal(Field)=Value then
    begin
      result:=Records[Dummy];
      exit;
    end;
  end;
end;

function record_utils_FindID(const Records: TRecordArray;ID: Cardinal): TExtRecord;
begin
  result:=record_utils_FindCardinal(Records,'ID',ID);
end;

procedure record_utils_ClearArray(var Records: TRecordArray);
var
  Dummy: Integer;
begin
  for Dummy:=0 to high(Records) do
    Records[Dummy].Free;

  SetLength(Records,0);
end;

function record_utils_AddRecord(var Records: TRecordArray; Rec: TExtRecord): Integer;
begin
  SetLength(Records,length(Records)+1);
  result:=high(Records);
  Records[result]:=Rec;
end;

end.
