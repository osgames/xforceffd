{********************************************************************************
*										*
* Copyright (C) 2004  X-Force Team - jim_raynor@web.de				*
*										*
* This file is part of X-Force: Fight For Destiny (X-Force).			*
*										*
* X-Force is free software; you can redistribute it and/or modify		*
* it under the terms of the GNU General Public License as published by		*
* the Free Software Foundation; either version 2 of the License, or		*
* (at your option) any later version.						*
*										*
* X-Force is distributed in the hope that it will be useful,			*
* but WITHOUT ANY WARRANTY; without even the implied warranty of		*
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the			*
* GNU General Public License for more details.					*
*										*
* You should have received a copy of the GNU General Public License		*
* along with X-Force; if not, write to the Free Software			*
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA	*
*										*
*********************************************************************************
*										*
* alien_api bildet die Schnittstelle zu einer Alienliste, die die Aliens 	*
* verwaltet.
*										*
* WICHTIG!! Bevor die Funktionen genutzt werden k�nnen muss �ber 		*
*           alien_api_init die Alienliste gesetzt werden. Sollte durch 		*
*           savegame_api_init geschehen sein.					*
*										*
********************************************************************************}

{$I ../Settings.inc}

unit script_utils;

interface

uses uPSCompiler, uPSRuntime, uPSDebugger, uPSUtils, ExtRecord, Windows
{$IFDEF HAS_DELPHI6}, Variants{$ENDIF};

type
  TSkriptProcedure = record
    dummy: Integer;
    PSData: PPSVariantU32;
  end;

  TInfoType   = (itProcedure,itFunction,itType,itVar,itConstant,itField,itConstructor);

  TInfoTypes  = set of TInfoType;

  TParamInfoRecord = record
    Name           : Cardinal;
    OrgName        : String;
    Params         : String;
    OrgParams      : String;
    Father         : Cardinal;
    Nr             : Integer;
    ReturnTyp      : Cardinal;
    Typ            : TInfoType;
    HasFields      : Boolean;
    SubType        : Cardinal;
    Value          : String;
    BuildIn        : Boolean;
    DeclarePos     : record
      X            : Integer;
      Y            : Integer;
      UnitName     : String;
    end;
  end;

  TParamInfoArray = Array of TParamInfoRecord;

// Ermittelt zu einer Runtime-Engine den aktuellen String zum Laufzeitfehler
function script_utils_GetErrorString(Exec: TPSExec): String;

// Gibt den Namen einer Skript-CallBack Funktion zur�ck, die in Delphi Verarbeitet
// werden muss
function script_utils_GetFunctionName(fRunTime: TPSExec; const Proc: TSkriptProcedure): String;

// random kann nicht direkt importiert werden, deshalb umweg �ber script_utils
function script_utils_random(Range: Integer): Integer;

procedure script_utils_inc(var Int: Integer);
procedure script_utils_dec(var Int: Integer);

function script_utils_constArrayToPSList(RunTime: TPSExec;Proc: TPSInternalProcRec;Params: Array of const): TPSList;
procedure script_utils_ClearPSList(List: TPSList);

procedure script_utils_MessageBox(Mess: String);

procedure script_utils_AddTypeToExtRecord(Name: String; Typ: TPSTypeRec; Def: TExtRecordDefinition);
procedure script_utils_SetVarToExtRecord(Name: String; PSVar: PPSVariant; Rec: TExtRecord);
procedure script_utils_GetVarFromExtRecord(Name: String; Dta: Pointer; aType: TPSTypeRec; Rec: TExtRecord);

function script_utils_findClass(Name: String): TClass;

function script_utils_GetParams(Decl: TPSParametersDecl; Delim: String = ''): String;
function script_utils_GetTypeName(Typ: TPSType): String;

function script_utils_Preprocess(Skript: String; Defines: String): String;

function script_utils_IsMemberOfSet(Item: Integer; Src: PByteArray): Boolean;

function script_utils_BaseTypeCompatible(p1, p2: Integer): Boolean;

function script_utils_GetVarContents(Exec: TPSDebugExec; VarName: String): String;

function script_utils_GetStringValue(Constant: TPSConstant): String;

implementation

uses SysUtils, TypInfo, game_api, savegame_api, XForce_types, TraceFile,
  register_scripttypes, uPSPreProcessor, string_utils;

function ifcVariantToPPSVariant(const ifC: TPSVariantIFC): PPSVariant;
begin
  Assert(ifc.aType<>nil);
  result:=CreateHeapVariant(ifc.aType);
  result.FType:=ifc.aType;
  CopyMemory(Addr(PPSVariantData(result).Data[0]),ifc.Dta,ifc.aType.RealSize);
end;

function script_utils_GetErrorString(Exec: TPSExec): String;
begin
  result:=TIFErrorToString(Exec.ExceptionCode, Exec.ExceptionString)
end;

function script_utils_GetFunctionName(fRunTime: TPSExec; const Proc: TSkriptProcedure): String;
var
  PSProc: TPSProcRec;
begin
  if (Proc.PSData=nil) or (Proc.PSData.Data = 0) then
  begin
    result:='';
    exit;
  end;

  PSProc:=fRunTime.GetProcNo(Proc.PSData.Data);
  if (PSProc is TPSInternalProcRec) then
  begin
    result:=TPSInternalProcRec(PSProc).ExportName;
  end
  else
  begin
    fRunTime.CMD_Err(ErTypeMismatch);
  end;
end;

function script_utils_random(Range: Integer): Integer;
begin
  result:=random(Range);
end;

procedure script_utils_inc(var Int: Integer);
begin
  inc(Int);
end;

procedure script_utils_dec(var Int: Integer);
begin
  dec(Int);
end;

function script_utils_constArrayToPSList(RunTime: TPSExec;Proc: TPSInternalProcRec;Params: Array of const): TPSList;
var
  s    : String;
  res  : String;
  i    : Integer;
  ct   : PIFTypeRec;
  pvar : PPSVariant;
  Va   : Variant;
begin
  result:=TPSList.Create;

  s := Proc.ExportDecl;
  res := grfw(s);
  i := High(Params);
  while s <> '' do
  begin
    if i < 0 then
      raise Exception.Create('Not enough parameters');
    ct := RunTime.GetTypeNo(StrToInt(copy(GRLW(s), 2, MaxInt)));
    if ct = nil then
      raise Exception.Create('Invalid Parameter');
    pvar := CreateHeapVariant(ct);
    result.Add(pvar);

    with Params[I] do
    begin
      Va:=Unassigned;
      case VType of
        vtInteger:    Va := VInteger;
        vtBoolean:    Va := VBoolean;
        vtChar:       Va := VChar;
        vtExtended:   Va := VExtended^;
        vtString:     Va := VString^;
        vtAnsiString: Va := string(VAnsiString);
        vtCurrency:   Va := VCurrency^;
        vtVariant:    Va := VVariant^;
        vtObject:
        begin
          if ct.BaseType<>btClass then
            raise Exception.Create('Invalid Parameter');

          PPSVariantClass(pVar).Data:=VObject;
        end;
        else
          raise Exception.Create('Not supported Type');
      end;
    end;

    if not VarIsEmpty(Va) then
    begin
      if not VariantToPIFVariant(RunTime, Va, pvar) then
        raise Exception.Create('Invalid Parameter');
    end;

    Dec(i);
  end;
  if I > -1 then raise Exception.Create('Too many parameters');
  if res <> '-1' then
  begin
    pvar := CreateHeapVariant(RunTime.GetTypeNo(StrToInt(res)));
    result.Add(pvar);
  end;
end;

procedure script_utils_ClearPSList(List: TPSList);
var
  Dummy: Integer;
begin
  for Dummy:=0 to List.Count-1 do
  begin
    DestroyHeapVariant(List[Dummy]);
  end;
end;

procedure script_utils_MessageBox(Mess: String);
begin
  savegame_api_Message(Mess,lmMissionMessage);
end;

procedure script_utils_AddTypeToExtRecord(Name: String; Typ: TPSTypeRec; Def: TExtRecordDefinition);
var
  RecDef: TExtRecordDefinition;
  Dummy : Integer;
  RecTyp: TPSTypeRec_Record;
begin
  case Typ.BaseType of
    btU8,btU16,btU32,btChar:      Def.AddCardinal(Name,low(Cardinal),high(Cardinal));
    btS8,btS16,btS32:             Def.AddInteger(Name,low(Integer),high(Integer));
    btSingle,btDouble,btExtended: Def.AddDouble(Name,-5.0E-10324,1.7E10308,20);
    btString:                     Def.AddString(Name,high(Integer));
    btRecord:
    begin
      RecDef:=TExtRecordDefinition.Create('Record'+Name);
      RecDef.ParentDescription:=true;
      RecTyp:=(Typ as TPSTypeRec_Record);
      for Dummy:=0 to RecTyp.FieldTypes.Count-1 do
      begin
        script_utils_AddTypeToExtRecord(IntToStr(Dummy),RecTyp.FieldTypes[Dummy],RecDef);
      end;
      Def.AddRecordList(Name,RecDef);
    end;
    btArray,btStaticArray:
    begin
      RecDef:=TExtRecordDefinition.Create('Array'+Name);
      RecDef.ParentDescription:=true;
      if (Typ as TPSTypeRec_Array).ArrayType.BaseType=btRecord then
      begin
        RecTyp:=((Typ as TPSTypeRec_Array).ArrayType as TPSTypeRec_Record);
        for Dummy:=0 to RecTyp.FieldTypes.Count-1 do
        begin
          script_utils_AddTypeToExtRecord(IntToStr(Dummy),RecTyp.FieldTypes[Dummy],RecDef);
        end;
      end
      else
        script_utils_AddTypeToExtRecord(Name,(Typ as TPSTypeRec_Array).ArrayType,RecDef);

      Def.AddRecordList(Name,RecDef);
    end;
    btSet:
    begin
      Def.AddBinary(Name);
    end;
    btClass:
    begin
      // Klassen werden in einem String gespeichert. Dazu m�ssen die Objekte, die von
      // der Skriptsprache genutzt werden ReadFromScriptString und WriteToScriptString im
      // published-Teil ben�tigt. Beim Registrieren der Klassen in die Skriptsprache in
      // der Unit register_scripttypes wird sichergestellt, dass diese Methoden auch tats�chlich existieren
      Def.AddString(Name,10000);
    end;
    else
      Assert(false,'Ung�ltiger Typ: '+IntToStr(Typ.BaseType));
  end;
end;

procedure script_utils_SetVarToExtRecord(Name: String; PSVar: PPSVariant; Rec: TExtRecord);
type
  TWriteToScriptString = function(slf: TObject): String;

var
  RecTyp: TPSTypeRec_Record;
  ExtRec: TExtRecord;
  Dummy : Integer;
  Dummy2: Integer;
  ifc   : TPSVariantIFC;
  ifc2  : TPSVariantIFC;
  ifc3  : TPSVariantIFC;
  RecVar: PPSVariant;
  Entrys: Integer;
  Ptr   : Pointer;
  Offs  : Integer;

  ObjStr: String;
  Str   : String;
begin
  case PSVar.FType.BaseType of
    btU8,btU16,btU32,btChar:      Rec.SetCardinal(Name,PSGetUInt(@PPSVariantData(PSVar).Data, PSVar.FType));
    btS8,btS16,btS32:             Rec.SetInteger(Name,PSGetInt(@PPSVariantData(PSVar).Data, PSVar.FType));
    btSingle,btDouble,btExtended: Rec.SetDouble(Name,PSGetReal(@PPSVariantData(PSVar).Data, PSVar.FType));
    btString:
    begin
      Str:=PSGetString(@PPSVariantData(PSVar).Data, PSVar.FType);
      Str:=Copy(Str,1,length(Str));
      Rec.SetString(Name,Str);
    end;
    btRecord:
    begin
      ExtRec:=Rec.GetRecordList(Name).Add;
      RecTyp:=(PSVar.FType as TPSTypeRec_Record);

      ifc:=NewTPSVariantIFC(PSVar,False);

      for Dummy:=0 to RecTyp.FieldTypes.Count-1 do
      begin
        ifc2:=PSGetRecField(ifc,Dummy);

        RecVar:=ifcVariantToPPSVariant(ifc2);
        script_utils_SetVarToExtRecord(IntToStr(Dummy),RecVar,ExtRec);
        FreeMem(RecVar,sizeof(TPSVariant)+RecVar.FType.RealSize);
      end;
    end;
    btArray,btStaticArray:
    begin
      ifc:=NewTPSVariantIFC(PSVar,False);

      if PSVar.FType.BaseType=btStaticArray then
        Entrys:=(PSVar.FType as TPSTypeRec_StaticArray).Size
      else
        Entrys:=PSDynArrayGetLength(Pointer(ifc.Dta^),ifc.aType);

      for Dummy:=0 to Entrys-1 do
      begin
        ExtRec:=Rec.GetRecordList(Name).Add;

        ifc2:=PSGetArrayField(ifc,Dummy);

        if ifc2.aType.BaseType=btRecord then
        begin
          RecTyp:=(ifc2.aType as TPSTypeRec_Record);
          for Dummy2:=0 to RecTyp.FieldTypes.Count-1 do
          begin
            ifc3:=PSGetRecField(ifc2,Dummy2);

            RecVar:=ifcVariantToPPSVariant(ifc3);
            script_utils_SetVarToExtRecord(IntToStr(Dummy2),RecVar,ExtRec);
            FreeMem(RecVar,sizeof(TPSVariant)+RecVar.FType.RealSize);
          end;
        end
        else
        begin
          RecVar:=ifcVariantToPPSVariant(ifc2);
          script_utils_SetVarToExtRecord(Name,RecVar,ExtRec);
          FreeMem(RecVar,sizeof(TPSVariant)+RecVar.FType.RealSize);
        end;
      end;
    end;
    btSet:
    begin
      GetMem(Ptr,PSVar.FType.RealSize);
      CopyMemory(Ptr,Addr(PPSVariantData(PSVar).Data[0]),PSVar.FType.RealSize);

      Rec.SetBinary(Name,PSVar.FType.RealSize,Ptr);

      FreeMem(Ptr);
    end;
    btClass:
    begin
      ObjStr:='nil';
      if PPSVariantClass(PSVar).Data<>nil then
      begin
        ObjStr:=TWriteToScriptString(PPSVariantClass(PSVar).Data.MethodAddress('WriteToScriptString'))(PPSVariantClass(PSVar).Data);
      end;
      Rec.SetString(Name,ObjStr);
    end;
    else
      Assert(false,'Ung�ltiger Typ: '+IntToStr(PSVar.FType.BaseType));
  end;
end;

procedure script_utils_GetVarFromExtRecord(Name: String; Dta: Pointer; aType: TPSTypeRec; Rec: TExtRecord);
type
  TReadFromScriptString = function(slf: TClass; ObjStr: String): TObject;

var
  RecTyp: TPSTypeRec_Record;
  ExtRec: TExtRecord;
  Dummy : Integer;
  Dummy2: Integer;
  ifc   : TPSVariantIFC;
  ifc2  : TPSVariantIFC;
  ifc3  : TPSVariantIFC;
  Entrys: Integer;
  Ptr   : Pointer;
  Offs  : Integer;

  OK    : Boolean;

  ObjStr: String;
  aClass: TClass;
begin
  Assert(aType<>nil);
  case aType.BaseType of
    btU8,btU16,btU32,btChar:
    begin
      OK:=true;
      PSSetUInt(Dta,aType,OK,Rec.GetCardinal(Name));
      Assert(OK);
    end;
    btS8,btS16,btS32:
    begin
      OK:=true;
      PSSetInt(Dta,aType,OK,Rec.GetInteger(Name));
      Assert(OK);
    end;
    btSingle,btDouble,btExtended:
    begin
      OK:=true;
      PSSetReal(Dta,aType,OK,Rec.Getdouble(Name));
      Assert(OK);
    end;
    btString:
    begin
      OK:=true;
      PSSetString(Dta,aType,OK,Rec.GetString(Name));
      Assert(OK);
    end;
    btRecord:
    begin
      ExtRec:=Rec.GetRecordList(Name).Item[0];
      RecTyp:=(aType as TPSTypeRec_Record);

      ifc.Dta:=dta;
      ifc.aType:=aType;

      for Dummy:=0 to RecTyp.FieldTypes.Count-1 do
      begin
        ifc2:=PSGetRecField(ifc,Dummy);

        script_utils_GetVarFromExtRecord(IntToStr(Dummy),ifc2.Dta,ifc2.aType,ExtRec);
      end;
    end;
    btArray,btStaticArray:
    begin
      ifc.Dta:=dta;
      ifc.aType:=aType;

      if aType.BaseType=btStaticArray then
        Entrys:=(aType as TPSTypeRec_StaticArray).Size
      else
      begin
        Entrys:=Rec.GetRecordList(Name).Count;
        PSDynArraySetLength(Pointer(ifc.dta^),aType,Entrys);
      end;

      for Dummy:=0 to Entrys-1 do
      begin
        ExtRec:=Rec.GetRecordList(Name).Item[Dummy];
                              
        if aType.BaseType=btArray then
          ifc2:=PSGetArrayField(ifc,Dummy)
        else if aType.BaseType=btStaticArray then
        begin
          Offs := TPSTypeRec_StaticArray(aType).ArrayType.RealSize * Cardinal(Dummy);
          ifc2.aType := TPSTypeRec_StaticArray(aType).ArrayType;
          ifc2.Dta := Pointer(IPointer(ifc.Dta) + Offs);
        end;

        if ifc2.aType.BaseType=btRecord then
        begin
          RecTyp:=(ifc2.aType as TPSTypeRec_Record);
          for Dummy2:=0 to RecTyp.FieldTypes.Count-1 do
          begin
            ifc3:=PSGetRecField(ifc2,Dummy2);

            script_utils_GetVarFromExtRecord(IntToStr(Dummy2),ifc3.dta,ifc3.aType,ExtRec);
          end;
        end
        else
        begin
          script_utils_GetVarFromExtRecord(Name,ifc2.dta,ifc2.aType,ExtRec);
        end;
      end;
    end;
    btSet:
    begin
      CopyMemory(dta,Rec.GetBinary(Name),aType.RealSize);
    end;
    btClass:
    begin
      ObjStr:=Rec.GetString(Name);
      if ObjStr<>'nil' then
      begin
        aClass:=script_utils_findClass(TPSTypeRec_Class(aType).CN);
        Assert(aClass<>nil);
        TObject(dta^):=TReadFromScriptString(aClass.MethodAddress('ReadFromScriptString'))(aClass,ObjStr);
      end;
    end;
    else
      Assert(false,'Ung�ltiger Typ: '+IntToStr(aType.BaseType));
  end;
end;

function script_utils_findClass(Name: String): TClass;
var
  Dummy: Integer;
begin
  Name:=lowercase(Name);
  for Dummy:=0 to high(registeredClasses) do
  begin
    if lowercase(registeredClasses[Dummy].ClassName)=Name then
    begin
      result:=registeredClasses[Dummy];
      exit;
    end;
  end;
end;

function script_utils_GetParams(Decl: TPSParametersDecl; Delim: String = ''): String;
var
  Dummy: Integer;
begin
  Assert(Decl<>nil);
  result:='';
  if Decl.ParamCount>0 then
  begin
    result:=result+'(';
    for Dummy:=0 to Decl.ParamCount-1 do
    begin
      if Dummy<>0 then
        result:=result+'; ';

      result:=result+Delim;
        
      if (Decl.Params[Dummy].Mode=pmOut) then
        result:=result+'out ';
      if (Decl.Params[Dummy].Mode=pmInOut) then
        result:=result+'var ';

      Result:=result+Decl.Params[Dummy].OrgName;

      if Decl.Params[Dummy].aType<>nil then
        result:=result+': '+script_utils_GetTypeName(Decl.Params[Dummy].aType);

      result:=result+Delim;
    end;
    result:=result+')';
  end;

  if Decl.Result<>nil then
    result:=result+': '+script_utils_GetTypeName(Decl.Result);
end;

function script_utils_GetTypeName(Typ: TPSType): String;
begin
  if Typ.OriginalName<>'' then
    result:=Typ.OriginalName
  else
  begin
    if Typ.ClassType=TPSArrayType then
      result:='Array of '+script_utils_GetTypeName(TPSArrayType(Typ).ArrayTypeNo)
    else if Typ.ClassType=TPSRecordType then
      result:='record'
    else if Typ.ClassType=TPSEnumType then
      result:='enum';
  end;
end;

function script_utils_Preprocess(Skript: String; Defines: String): String;
var
  PreProcessor: TPSPreProcessor;
begin
  PreProcessor:=TPSPreProcessor.Create;

  string_utils_AddToTStrings(string_utils_explode(';',Defines),PreProcessor.Defines);
  PreProcessor.MainFile:=Skript;
  try
    PreProcessor.PreProcess('',Skript);
  except
  end;
  result:=Skript;
  PreProcessor.Free;
end;

function script_utils_IsMemberOfSet(Item: Integer; Src: uPSRuntime.PByteArray): Boolean;
begin
  result := (Src^[Item shr 3] and (1 shl (Item and 7))) <> 0;
end;

function script_utils_BaseTypeCompatible(p1, p2: Integer): Boolean;

  function IsIntType(b: TPSBaseType): Boolean;
  begin
    case b of
      btU8, btS8, btU16, btS16, btU32, btS32{$IFNDEF PS_NOINT64}, btS64{$ENDIF}: Result := True;
    else
      Result := False;
    end;
  end;

  function IsRealType(b: TPSBaseType): Boolean;
  begin
    case b of
      btSingle, btDouble, btCurrency, btExtended: Result := True;
    else
      Result := False;
    end;
  end;

  function IsIntRealType(b: TPSBaseType): Boolean;
  begin
    case b of
      btSingle, btDouble, btCurrency, btExtended, btU8, btS8, btU16, btS16, btU32, btS32{$IFNDEF PS_NOINT64}, btS64{$ENDIF}:
        Result := True;
    else
      Result := False;
    end;

  end;

begin
  if
    ((p1 = btProcPtr) and (p2 = p1)) or
    (p1 = btPointer) or
    (p2 = btPointer) or
    ((p1 = btNotificationVariant) or (p1 = btVariant)) or
    ((p2 = btNotificationVariant) or (p2 = btVariant))  or
    (IsIntType(p1) and IsIntType(p2)) or
    (IsRealType(p1) and IsIntRealType(p2)) or
    (((p1 = btPchar) or (p1 = btString)) and ((p2 = btString) or (p2 = btPchar))) or
    (((p1 = btPchar) or (p1 = btString)) and (p2 = btChar)) or
    ((p1 = btChar) and (p2 = btChar)) or
    ((p1 = btSet) and (p2 = btSet)) or
    {$IFNDEF PS_NOWIDESTRING}
    ((p1 = btWideChar) and (p2 = btChar)) or
    ((p1 = btWideChar) and (p2 = btWideChar)) or
    ((p1 = btWidestring) and (p2 = btChar)) or
    ((p1 = btWidestring) and (p2 = btWideChar)) or
    ((p1 = btWidestring) and ((p2 = btString) or (p2 = btPchar))) or
    ((p1 = btWidestring) and (p2 = btWidestring)) or
    (((p1 = btPchar) or (p1 = btString)) and (p2 = btWideString)) or
    (((p1 = btPchar) or (p1 = btString)) and (p2 = btWidechar)) or
    (((p1 = btPchar) or (p1 = btString)) and (p2 = btchar)) or
    {$ENDIF}
    ((p1 = btRecord) and (p2 = btrecord)) or
    ((p1 = btEnum) and (p2 = btEnum))
    then
    Result := True
  else
    Result := False;
end;

function script_utils_GetVarContents(Exec: TPSDebugExec; VarName: String): String;
var
  i: Longint;
  pv: PIFVariant;
  s1, s: string;
begin
  s := Uppercase(VarName);
  if pos('.', s) > 0 then
  begin
    s1 := copy(s,1,pos('.', s) -1);
    delete(s,1,pos('.', VarName));
  end
  else
  begin
    s1 := s;
    s := '';
  end;
  pv := nil;
  for i := 0 to Exec.CurrentProcVars.Count -1 do
  begin
    if FastUppercase(Exec.CurrentProcVars[i]) =  s1 then
    begin
      pv := Exec.GetProcVar(i);
      break;
    end;
  end;
  if pv = nil then
  begin
    for i := 0 to Exec.CurrentProcParams.Count -1 do
    begin
      if FastUppercase(Exec.CurrentProcParams[i]) =  s1 then
      begin
        pv := Exec.GetProcParam(i);
        break;
      end;
    end;
  end;
  if pv = nil then
  begin
    for i := 0 to Exec.GlobalVarNames.Count -1 do
    begin
      if FastUpperCase(Exec.GlobalVarNames[i]) =  s1 then
      begin
        pv := Exec.GetGlobalVar(i);
        break;
      end;
    end;
  end;
  if pv = nil then
    Result := ''
  else
    Result := PSVariantToString(NewTPSVariantIFC(pv, False), s);
end;

function script_utils_GetStringValue(Constant: TPSConstant): String;
begin

  case Constant.Value.FType.BaseType of
     1: result:=IntToStr(Constant.Value.tu8);
     2: result:=IntToStr(Constant.Value.tS8);
     3: result:=IntToStr(Constant.Value.tu16);
     4: result:=IntToStr(Constant.Value.ts16);
     5: result:=IntToStr(Constant.Value.tu32);
     6: result:=IntToStr(Constant.Value.ts32);
     7: result:=FloatToStr(Constant.Value.tsingle);
     8: result:=FloatToStr(Constant.Value.tdouble);
     9: result:=FloatToStr(Constant.Value.textended);
    11: result:=CurrToStr(Constant.Value.tcurrency);
    10: result:=''''+string(Constant.Value.tstring)+'''';
    18: result:='#'+IntToStr(Ord(Constant.Value.tchar));
    21: result:=Constant.Value.ttype.OriginalName;
    129: result:=Format('%s(%d)',[Constant.Value.FType.OriginalName,Constant.Value.ts32]);
    else
      result:='Unbekannter Typ :'+ IntToStr(Constant.Value.FType.BaseType)+' ('+Constant.Value.FType.OriginalName+')';
  end;
end;

end.
