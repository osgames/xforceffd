{********************************************************************************
*										*
* Copyright (C) 2004  X-Force Team - jim_raynor@web.de				*
*										*
* This file is part of X-Force: Fight For Destiny (X-Force).			*
*										*
* X-Force is free software; you can redistribute it and/or modify		*
* it under the terms of the GNU General Public License as published by		*
* the Free Software Foundation; either version 2 of the License, or		*
* (at your option) any later version.						*
*										*
* X-Force is distributed in the hope that it will be useful,			*
* but WITHOUT ANY WARRANTY; without even the implied warranty of		*
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the			*
* GNU General Public License for more details.					*
*										*
* You should have received a copy of the GNU General Public License		*
* along with X-Force; if not, write to the Free Software			*
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA	*
*										*
*********************************************************************************
*										*
* Stellt Funktionen zum Verwalten von Arrays zur Verf�gung			*
*										*
********************************************************************************}

{$I ../Settings.inc}

unit array_utils;

interface

uses TypInfo, Windows;

// L�scht einen Eintrag aus einem Array
// ArrayPtr gibt die Adresse der Variablen an, in der das Array gehalten wird (Addr(fArray))
// Als Typeinfo muss der Array type �bergeben werden (also TForscherArray und nicht nur TForscher)

// Folgender Aufbau hat TypeInfo bei einem Dynamischen Array
// Typ              ( 1 Byte ) = $11
// Name             ( erstes Byte l�nge + Zeichen )
// Elementgr�sse    ( 4 Byte ) Gr��e eines Elementes
// FinalizeTypInfo  ( Zeiger auf eine Typeinfo, falls f�r einen Eintrag ein Finalize notwendig ist)
// Beispiel
{
  type
    TTypedArray : Array of TTestRecord

  var
    TestArray: TTypedArray

  DeleteArray(Addr(TestArray),TypeInfo(TTypedArray),5)
}
procedure DeleteArray(const ArrayPtr: Pointer; TypInfo: PTypeInfo; Index: Integer);

// Bitte nicht Benutzen!! F�hrt zu sehr merkw�rdigen Fehlern
procedure AddArray(const ArrayPtr: Pointer; TypInfo: PTypeInfo; Elem: Pointer);

implementation

procedure DeleteArray(const ArrayPtr: Pointer; TypInfo: PTypeInfo; Index: Integer);
var
  NewElem: Integer;
  ElemSize: Integer;
  ElemAddr: Integer;
  ElemCount: Integer;
begin
  Assert(TypInfo.Kind=tkDynArray,'Es werden nur Dynamische Arrays unterst�tzt');
  // Elementgr��e ermitteln
  ElemSize:=PInteger(PByte(Integer(TypInfo)+1)^+Integer(TypInfo)+2)^;
  // Elementanzahl ermitteln
  ElemCount:=PInteger(Integer(ArrayPtr^)-4)^;

  ElemAddr:=Integer(ArrayPtr^)+(Index*ElemSize);

  // Eintrag freigeben

  // Pr�fen ob ein Finalize notwendig ist, dazu muss in der Type
  asm
    pushad
    mov esi,TypInfo
    xor eax,eax
    mov al,[esi+$01]
    add esi,eax
    add esi,$06

    mov eax,[esi]
    test eax,eax
    jz @end

    mov edx,[eax]
    mov eax,ElemAddr
    call system.@finalize

  @end:
    popad
  end;
  MoveMemory(Pointer(ElemAddr),Pointer(ElemAddr+ElemSize),ElemSize*((ElemCount-1)-index));
  NewElem:=ElemCount-1;
  // Letztes Element mit nullen f�llen
  ZeroMemory(Pointer(Integer(ArrayPtr^)+(NewElem*ElemSize)),ElemSize);

  // Array verkleinern
  asm
    mov ebx,NewElem
    push ebx
    mov ecx,$00000001

    mov edx,TypInfo

    mov eax,ArrayPtr
    call system.@DynArraySetLength
    add esp,$04
  end;
end;

procedure AddArray(const ArrayPtr: Pointer; TypInfo: PTypeInfo; Elem: Pointer);
var
  NewElem: Integer;
  ElemSize: Integer;
  ElemAddr: Integer;
  ElemCount: Integer;
begin
  Assert(TypInfo.Kind=tkDynArray,'Es werden nur Dynamische Arrays unterst�tzt');
  // Elementgr��e ermitteln
  ElemSize:=PInteger(PByte(Integer(TypInfo)+1)^+Integer(TypInfo)+2)^;
  // Elementanzahl ermitteln
  if Pointer(ArrayPtr^)<>nil then
    ElemCount:=PInteger(Integer(ArrayPtr^)-4)^
  else
    ElemCount:=0;

  NewElem:=ElemCount+1;
  // Array vergr��ern
  asm
    mov ebx,NewElem
    push ebx
    mov ecx,$00000001

    mov edx,TypInfo

    mov eax,ArrayPtr
    call system.@DynArraySetLength
    add esp,$04
  end;

  ElemAddr:=Integer(ArrayPtr^)+((NewElem-1)*ElemSize);

  // Pr�fen ob ein CopyRecord notwendig ist
  asm
    pushad
    mov esi,TypInfo
    xor eax,eax
    mov al,[esi+$01]
    add esi,eax
    add esi,$06

    mov ecx,[esi]
    test ecx,ecx
    jz @move

    // CopyRecord
    mov ecx,[ecx]
    mov edx,Elem
    mov eax,ElemAddr
    call system.@CopyRecord
    popad
    jmp @end
  @move:
    popad
    mov ecx,ElemSize
    mov edx,Elem
    mov eax,ElemAddr
    call MoveMemory
  @end:
  end;
end;

end.
