unit formular_utils;

interface

uses
  ExCQParser, ECQ_Def, IDHashMap, inifiles, sysutils, classes, string_utils,
  KD4Utils;

{$i ../settings.inc}

{$IFDEF EXTERNALFORMULARS}
procedure formular_utils_readformulars(FileName: String);

function formular_utils_SolveFormular(Name: String; VarNames: Array of String; Values: Array of double): double;
{$ENDIF}

implementation

{$IFDEF EXTERNALFORMULARS}
var
  g_hashmap : TIDHashMap;
  g_List    : TList;

procedure ClearList;
var
  Dummy: Integer;
begin
  for Dummy:=0 to g_List.Count-1 do
  begin
    TObject(g_List[Dummy]).Free;
  end;
end;

procedure formular_utils_readformulars(FileName: String);
var
  inifile  : TIniFile;
  sections : TStringList;
  formels  : TStringList;
  Dummy    : Integer;
  Dummy2   : Integer;
  Parser   : TExCQParser;
  expl     : TStringArray;
  ConstSec : Boolean;

  Vars     : TStringArray;

  procedure RegisterVarsAndConst(Parser: TExCQParser);
  var
    Dummy  : Integer;
    expl   : TStringArray;
  begin
    for Dummy:=0 to high(Vars) do
    begin
      if Pos('=',Vars[Dummy])=0 then
        Parser.RegisterVariable(Vars[Dummy])
      else
      begin
        expl:=string_utils_explode('=',Vars[Dummy]);
        Assert(length(expl)=2);

        Parser.RegisterVariable(expl[0],[StrToFloat(expl[1])],true);
      end;
    end;
  end;

begin
  ClearList;
  g_hashmap.ClearList;

  Assert(FileExists(FileName),'Formel-Datei '+FileName+' nicht gefunden');
  inifile:=TIniFile.Create(FileName);

  formels:=TStringList.Create;
  sections:=TStringList.Create;
  iniFile.ReadSections(sections);
  for Dummy:=0 to sections.Count-1 do
  begin
    formels.Clear;
    inifile.ReadSectionValues(sections[Dummy],formels);

    if (lowercase(sections[Dummy])='vars') or (lowercase(sections[Dummy])='const') then
    begin
      ConstSec:=lowercase(sections[Dummy])='const';
      for Dummy2:=0 to formels.count-1 do
      begin
        if ConstSec then                                   
          string_utils_Add(Vars,formels[Dummy2])
        else
          string_utils_Add(Vars,Copy(formels[Dummy2],1,Pos('=',formels[Dummy2])-1));
      end;
    end
    else if lowercase(sections[Dummy])='gameformulars' then
    begin
      for Dummy2:=0 to formels.Count-1 do
      begin
        Parser:=TExCQParser.Create;
        Parser.SolveMode:=smVM;
        Parser.PreSolve:=true;

        expl:=string_utils_explode('=',formels[Dummy2]);
        Assert(length(expl)=2);

        RegisterVarsAndConst(Parser);

        Parser.Parse(expl[1]);

//        Parser.Free;
        g_List.Add(Parser);
        g_hashmap.InsertID(HashString(expl[0]),Integer(Parser));
      end;
    end;
  end;

  sections.Free;
  formels.Free;
  inifile.Free;
end;

function formular_utils_SolveFormular(Name: String; VarNames: Array of String; Values: Array of double): double;
var
  Parser: TExCQParser;
  Dummy : Integer;
  Vari  : TVariable;
begin
  Assert(g_HashMap.FindKey(HashString(name),Integer(Parser)),'Formel '+Name+' nicht gefunden');

  for Dummy:=0 to high(VarNames) do
  begin
    Parser.SetVariable(VarNames[Dummy],[Values[Dummy]]);
  end;

  Vari:=Parser.Solve;

  result:=Vari.x;
end;

{$ENDIF}

initialization

{$IFDEF EXTERNALFORMULARS}
  g_hashmap:=TIDHashMap.Create;
  g_List:=TList.Create;
{$ENDIF}

finalization

{$IFDEF EXTERNALFORMULARS}
  ClearList;
  g_List.Free;
  g_hashmap.Free;
  g_hashmap:=nil;
{$ENDIF}

end.
