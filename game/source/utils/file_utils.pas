{********************************************************************************
*										*
* Copyright (C) 2004  X-Force Team - jim_raynor@web.de				*
*										*
* This file is part of X-Force: Fight For Destiny (X-Force).			*
*										*
* X-Force is free software; you can redistribute it and/or modify		*
* it under the terms of the GNU General Public License as published by		*
* the Free Software Foundation; either version 2 of the License, or		*
* (at your option) any later version.						*
*										*
* X-Force is distributed in the hope that it will be useful,			*
* but WITHOUT ANY WARRANTY; without even the implied warranty of		*
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the			*
* GNU General Public License for more details.					*
*										*
* You should have received a copy of the GNU General Public License		*
* along with X-Force; if not, write to the Free Software			*
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA	*
*										*
*********************************************************************************
*										*
* Stellt ein paar Funktionen zur Arbeit mit Dateien zur Verf�gung               *
*										*
********************************************************************************}

{$I ../Settings.inc}

unit file_utils;

interface

uses string_utils;

function file_utils_GetFileSize(FileName: String): Int64;

function file_utils_ReadDirectory(Dir: String; Filter: String; Recursiv: Boolean = false): TStringArray;

implementation

uses Windows, SysUtils;

function file_utils_GetFileSize(FileName: String): Int64;
var
  fhandle : THandle;
  t       : TOFStruct;
begin
  fhandle:=OpenFile(PChar(FileName),t,OF_READ);
  if fHandle=HFILE_ERROR then
  begin
    result:=-1;
    exit;
  end;
  result:=GetFileSize(fHandle,nil);
  CloseHandle(fHandle);
end;

function file_utils_ReadDirectory(Dir: String; Filter: String; Recursiv: Boolean = false): TStringArray;
var
  Data    : TSearchRec;
  DirList : TStringArray;
begin
  Dir:=IncludeTrailingBackslash(Dir);
  if FindFirst(Dir+Filter,faAnyFile,Data)=0 then
  begin
    repeat
      if copy(Data.Name,1,1)<>'.' then
      begin
        if ((Data.Attr and faDirectory)=faDirectory) and Recursiv then
        begin
          DirList:=file_utils_ReadDirectory(Dir+Data.Name,Filter,true);
          string_utils_Add(result,DirList);
        end
        else
          string_utils_Add(result,Dir+Data.Name);
      end;
    until FindNext(Data)<>0;
    FindClose(Data);
  end;
end;

end.
