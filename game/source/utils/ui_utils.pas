{********************************************************************************
*										*
* Copyright (C) 2004  X-Force Team - jim_raynor@web.de				*
*										*
* This file is part of X-Force: Fight For Destiny (X-Force).			*
*										*
* X-Force is free software; you can redistribute it and/or modify		*
* it under the terms of the GNU General Public License as published by		*
* the Free Software Foundation; either version 2 of the License, or		*
* (at your option) any later version.						*
*										*
* X-Force is distributed in the hope that it will be useful,			*
* but WITHOUT ANY WARRANTY; without even the implied warranty of		*
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the			*
* GNU General Public License for more details.					*
*										*
* You should have received a copy of the GNU General Public License		*
* along with X-Force; if not, write to the Free Software			*
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA	*
*										*
*********************************************************************************
*										*
* zus�tzliche Utilities	zur Unterst�tzung f�r das Userinterface		        *
*										*
********************************************************************************}

unit ui_utils;

interface

uses DXListBox, LagerListe, XForce_Types, DXInput;

function ui_utils_FillItems(ListBox: TDXListBox;LagerListe: TLagerListe;Filter: TItemFilter;Text: String = '';Anzahl: String = ''; CanUpgrade: boolean = false): Integer;

procedure ui_utils_ChangeKeyAssign(DXInput: TDXInput;State: TDXInputState;Key: Char);

function ui_utils_GetText(Key: Char): String;

implementation

uses
  sysutils, KD4Utils, math, lager_api, Windows,StringConst;


type
  TCountFilter = record
    Active      : Boolean;
    Count       : Integer;
    CountOption : (coGreater,coLower,coEqual);
  end;

var
  Lager     : TLagerListe;
  Box       : TDXListBox;
  IDIndex   : Array of TTwoInteger;

function CompareEntrys(I1,I2: Integer; Typ: TFunctionType): Integer;
var
  Str1,Str2      : String;
  Index1,Index2  : Integer;
  TempTwo        : TTwoInteger;
begin
  case Typ of
    ftCompare:
    begin
      Index1:=StrToInt(Box.Items[I1]);
      Index2:=StrToInt(Box.Items[I2]);
      result:=CompareType(Lager[Index1].TypeID,Lager[Index2].TypeID);
      if result=0 then
      begin
        Str1:=Lager[Index1].Name;
        Str2:=Lager[Index2].Name;
        result:=StrComp(PChar(Str1),PChar(Str2));
      end;
    end;
  ftExchange:
    begin
      Box.Items.Exchange(I1,I2);
      TempTwo:=IDIndex[I1];
      IDIndex[I1]:=IDIndex[I2];
      IDIndex[I2]:=TempTwo;
    end;
  end;
end;

function GenerateCountFilter(Text: String): TCountFilter;
var
  Err: Integer;
begin
  Result.Active:=false;
  Text:=StringReplace(Text,' ','',[rfReplaceAll]);
  if length(Text)=0 then
    exit;

  if (Text[1]='=') then
    result.CountOption:=coEqual
  else if Text[1]='>' then
    result.CountOption:=coGreater
  else if Text[1]='<' then
    result.CountOption:=coLower
  else
  begin
    Text:='='+Text;
    result.CountOption:=coEqual;
  end;

  Delete(Text,1,1);

  Val(Text,Result.Count,Err);

  if Err>0 then
    result.Active:=false
  else
    result.Active:=true;
end;

function ui_utils_FillItems(ListBox: TDXListBox;LagerListe: TLagerListe;Filter: TItemFilter;Text: String;Anzahl: String; CanUpgrade: boolean): Integer;
const
  Items: Array[TItemFilter] of TProjektTypes = {ifAll}      ([ptWaffe,ptRWaffe,ptPanzerung,ptMunition,ptMotor,ptRMunition,ptMine,ptSensor,ptGranate,ptShield,ptExtension,ptGuertel],
                                               {ifWaffen}    [ptWaffe,ptRWaffe,ptMunition,ptRMunition],
                                               {ifMine}      [ptMine],
                                               {ifSensoren}  [ptSensor],
                                               {ifMotoren}   [ptMotor,ptExtension],
                                               {ifPanzerung} [ptPanzerung,ptShield,ptGuertel],
                                               {ifRWaffen}   [ptRWaffe],
                                               {ifGranate}   [ptGranate],
                                               {ifSchiff}    [ptRWaffe,ptMotor,ptShield,ptExtension],
                                               {ifSoldat}    [ptWaffe,ptPanzerung,ptMunition,ptMine,ptSensor,ptGranate]);
var
  Dummy       : Integer;
  Count       : Integer;
  Add         : boolean;
  LastType    : TProjektType;
  Index       : Integer;
  Len         : Integer;
  CountFilter : TCountFilter;
begin
  result:=0;
  Index:=0;
  SetLength(IDIndex,LagerListe.Count);
  Box:=LIstBox;
  Lager:=LagerListe;
  ListBox.ChangeItem(true);
  ListBox.Items.Clear;

  CountFilter:=GenerateCountFilter(Anzahl);
  Text:=LowerCase(Text);

  for Dummy:=0 to LagerListe.Count-1 do
  begin
    Add:=true;
    with LagerListe[Dummy] do
    begin

      if not lager_api_ItemVisible(LagerListe[Dummy]) then
        Add:=false
      // Textfilter pr�fen
      else if (Text<>'') and (Pos(Text,LowerCase(Name))=0) then
        Add:=false
      // Typfilter pr�fen
      else if not (TypeID in Items[Filter]) then
        Add:=false
      // Upgradefilter pr�fen
      else if CanUpgrade and (Level>4) then
        Add:=false
      else if CanUpgrade and (AlienItem) then
        Add:=false
      // Anzahl Filter pr�fen
      else if CountFilter.Active then
      begin
        case CountFilter.CountOption of
          coGreater : Add:=Anzahl>CountFilter.Count;
          coLower   : Add:=Anzahl<CountFilter.Count;
          coEqual   : Add:=Anzahl=CountFilter.Count;
        end;
      end;
    end;
    if Add then
    begin
      ListBox.Items.Add(IntToStr(Dummy));
      IDIndex[Index].ID:=LagerListe[Dummy].ID;
      inc(Index);
      inc(result);
    end;
  end;
  SetLength(IDIndex,ListBox.Items.Count);
  QuickSort(0,ListBox.Items.Count-1,CompareEntrys);

  LastType:=ptNone;
  Count:=ListBox.Items.Count;
  Dummy:=0;
  while (Dummy<Count) do
  begin
    if LastType<>LagerListe[StrToInt(ListBox.Items[Dummy])].TypeID then
    begin
      LastType:=LagerListe[StrToInt(ListBox.Items[Dummy])].TypeID;
      ListBox.Items.Insert(Dummy,'C'+IntToStr(Integer(LastType)));
      inc(Count);
      inc(Dummy);
    end;
    inc(Dummy);
  end;
  ListBox.ChangeItem(false);
  ListBox.Items.OnChange(ListBox);
end;

procedure ui_utils_ChangeKeyAssign(DXInput: TDXInput;State: TDXInputState;Key: Char);
var
  Keys: TKeyAssignList;
begin
  Keys:=DXInput.Keyboard.KeyAssigns;
  Keys[State][0]:=Integer(Key);
  DXInput.Keyboard.KeyAssigns:=Keys;
end;

function ui_utils_GetText(Key: Char): String;
begin
  Result := '';
  case Integer(Key) of
    VK_RETURN  : Result := KLEnter;
    VK_BACK    : Result := KLBack;
    VK_END     : Result := KLEnd;
    VK_HOME    : Result := KLHome;
    VK_INSERT  : Result := KLInsert;
    VK_DELETE  : Result := KLDelete;
    VK_SPACE   : Result := KLSpace;
    VK_PRIOR   : Result := KLPrior;
    VK_NEXT    : Result := KLNext;
    VK_ESCAPE  : Result := KLEscape;
    VK_LEFT    : Result := KLLeft;
    VK_UP      : Result := KLUp;
    VK_RIGHT   : Result := KLRight;
    VK_DOWN    : Result := KLDown;
    $30..$39,
    $41..$58   : Result := Char(Key); {0..9, A..W}
    $59        : Result := KLAfterT;
    $5A        : Result := KLBeforeX;
    VK_NUMPAD0..VK_NUMPAD9:
                 Result := 'Num '+inttostr(Integer(Key)-VK_NUMPAD0);
    VK_MULTIPLY: Result := 'Num *';
    VK_ADD     : Result := 'Num +';
    VK_SUBTRACT: Result := 'Num -';
    VK_DECIMAL : Result := 'Num .';
    VK_DIVIDE  : Result := 'Num /';
  end;
end;

end.
