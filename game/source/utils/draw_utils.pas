{********************************************************************************
*										*
* Copyright (C) 2004  X-Force Team - jim_raynor@web.de				*
*										*
* This file is part of X-Force: Fight For Destiny (X-Force).			*
*										*
* X-Force is free software; you can redistribute it and/or modify		*
* it under the terms of the GNU General Public License as published by		*
* the Free Software Foundation; either version 2 of the License, or		*
* (at your option) any later version.						*
*										*
* X-Force is distributed in the hope that it will be useful,			*
* but WITHOUT ANY WARRANTY; without even the implied warranty of		*
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the			*
* GNU General Public License for more details.					*
*										*
* You should have received a copy of the GNU General Public License		*
* along with X-Force; if not, write to the Free Software			*
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA	*
*										*
*********************************************************************************
*										*
* Unterstützung mit Zeichenfunktionen                                           *
*										*
********************************************************************************}

unit draw_utils;

interface

uses Classes,DXDraws, graphics, DirectDraw, Blending;

procedure draw_utils_AccerlateTextOut(Surface: TDirectDrawSurface;Font: TFont;x,y: Integer;Text:String;Color: TColor);overload;
procedure draw_utils_AccerlateTextOut(Canvas: TCanvas;x,y: Integer;Text:String;Color: TColor);overload;

{ Farbfunktionen }
function draw_utils_GetAlphaColor(const Mem: TDDSurfaceDesc;Color: TBlendColor; Alpha: Integer): TBlendColor;

implementation

uses DirectFont, KD4Utils;

procedure draw_utils_AccerlateTextOut(Surface: TDirectDrawSurface;Font: TFont;x,y: Integer;Text:String;Color: TColor);
var
  FColor : TColor;
  AccPos : Integer;
  Dummy  : Integer;
  dx     : Integer;
  temp   : String;
  DFont  : TDirectFont;
begin
  AccPos:=0;
  dx:=x;
  FColor:=Font.Color;
  DFont:=FontEngine.FindDirectFont(Font,clBlack);
  for Dummy:=1 to length(Text) do
  begin
    if Text[Dummy]='&' then
    begin
      AccPos:=Dummy;
      break;
    end;
  end;
  if AccPos=0 then
  begin
    DFont.Draw(Surface,X,Y,Text);
    exit;
  end;
  if AccPos>1 then
  begin
    temp:=cut(text,1,AccPos-1);
    DFont.Draw(Surface,X,Y,temp);
    dx:=x+DFont.Textwidth(temp);
  end;
  if AccPos<length(text) then
  begin
    Font.Color:=Color;
    FontEngine.TextOut(Font,Surface,DX,Y,text[AccPos+1],clBlack);
    Font.Color:=FColor;
    dx:=dx+DFont.TextWidth(text[AccPos+1]);
    DFont.Draw(Surface,DX,Y,cut(text,Accpos+2,length(text)));
  end;
end;

procedure draw_utils_AccerlateTextOut(Canvas: TCanvas;x,y: Integer;Text:String;Color: TColor);overload;
var
  FColor : TColor;
  AccPos : Integer;
  Dummy  : Integer;
  dx     : Integer;
  temp   : String;
begin
  AccPos:=0;
  dx:=x;
  FColor:=Canvas.Font.Color;
  for Dummy:=1 to length(Text) do
    if Text[Dummy]='&' then AccPos:=Dummy;
  if AccPos=0 then
  begin
    Canvas.Font.Color:=clBlack;
    Canvas.TextOut(x+1,y,Text);
    Canvas.Font.Color:=FColor;
    Canvas.TextOut(x,y-1,Text);
    exit;
  end;
  if AccPos>1 then
  begin
    temp:=cut(text,1,AccPos-1);
    Canvas.Font.Color:=clBlack;
    Canvas.TextOut(x+1,y,temp);
    Canvas.Font.Color:=FColor;
    Canvas.TextOut(x,y-1,temp);
    dx:=x+Canvas.Textwidth(temp);
  end;
  if AccPos<length(text) then
  begin
    Canvas.Font.Color:=clBlack;
    Canvas.TextOut(dx+1,y,text[AccPos+1]);
    Canvas.Font.Color:=Color;
    Canvas.TextOut(dx,y-1,text[AccPos+1]);
    Canvas.Font.Color:=FColor;
    dx:=dx+Canvas.TextWidth(text[AccPos+1]);
    Canvas.Font.Color:=clBlack;
    Canvas.TextOut(dx+1,y,cut(text,Accpos+2,length(text)));
    Canvas.Font.Color:=FColor;
    Canvas.TextOut(dx,y-1,cut(text,Accpos+2,length(text)));
  end;
end;

function draw_utils_GetAlphaColor(const Mem: TDDSurfaceDesc;Color: TBlendColor; Alpha: Integer): TBlendColor;
var
  RValue: Cardinal;
  BValue: Cardinal;
  GValue: Cardinal;
begin
  with Mem.ddpfPixelFormat do
  begin
    RValue:=(dwRBitMask and (((Color and dwRBitMask)*Alpha) shr 8));
    GValue:=(dwGBitMask and (((Color and dwGBitMask)*Alpha) shr 8));
    BValue:=(dwBBitMask and (((Color and dwBBitMask)*Alpha) shr 8));
  end;
  result:=(RValue or GValue or BValue);
end;

end.
