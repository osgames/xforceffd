{********************************************************************************
*										*
* Copyright (C) 2004  X-Force Team - jim_raynor@web.de				*
*										*
* This file is part of X-Force: Fight For Destiny (X-Force).			*
*										*
* X-Force is free software; you can redistribute it and/or modify		*
* it under the terms of the GNU General Public License as published by		*
* the Free Software Foundation; either version 2 of the License, or		*
* (at your option) any later version.						*
*										*
* X-Force is distributed in the hope that it will be useful,			*
* but WITHOUT ANY WARRANTY; without even the implied warranty of		*
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the			*
* GNU General Public License for more details.					*
*										*
* You should have received a copy of the GNU General Public License		*
* along with X-Force; if not, write to the Free Software			*
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA	*
*										*
*********************************************************************************
*										*
* Stellt ein paar Funktionen f�r Strings zur Verf�gung				*
*										*
********************************************************************************}

{$I ../Settings.inc}

unit string_utils;

interface

uses
  SysUtils, Classes;

type
  TStringArray = Array of String;
  
  procedure string_utils_split(Trenner: String;Text: String;var Text1, Text2: String);
  function string_utils_explode(Trenner: String;Text: String): TStringArray;
  function string_utils_implode(Trenner: String;const Strings: TStringArray): String;
  function string_utils_inArray(Text: String;const Strings: TStringArray): Boolean;
  procedure string_utils_Add(var Strings: TStringArray; Text: String);overload;
  procedure string_utils_Add(var Strings: TStringArray; InputStrings: TStringArray);overload;

  function string_utils_LoadFromFile(FileName: String): String;

  procedure string_utils_AddToTStrings(Strings: TStringArray; List: TStrings);

  procedure string_utils_WriteString(Stream: TStream ;Str: String);
  function string_utils_ReadString(Stream: TStream): String;

  function string_utils_GetLanguageString(language: String; Text: String): String;
  function string_utils_SetLanguageString(language: String; Text: String; NewText: String): String;
  function string_utils_IsLanguageString(Text: String): Boolean;

implementation

procedure string_utils_split(Trenner: String;Text: String;var Text1, Text2: String);
var
  EndPos: Integer;
begin
  EndPos:=Pos(Trenner,Text);
  if EndPos=0 then
    EndPos:=Length(Text)+1;

  Text1:=Copy(Text,1,EndPos-1);
    
  Text2:=Copy(Text,EndPos+length(Trenner),length(Text));
end;

function string_utils_explode(Trenner: String;Text: String): TStringArray;
begin
  result:=nil;
  while Text<>'' do
  begin
    SetLength(result,length(result)+1);
    string_utils_split(Trenner,Text,result[high(result)],Text);
  end;
end;

function string_utils_implode(Trenner: String;const Strings: TStringArray): String;
var
  Dummy: Integer;
begin
  result:='';
  for Dummy:=0 to high(Strings) do
  begin
    if result<>'' then
      result:=Result+Trenner;
    result:=result+Strings[Dummy];
  end;
end;

function string_utils_inArray(Text: String;const Strings: TStringArray): Boolean;
var
  Dummy: Integer;
begin
  result:=false;
  for Dummy:=0 to high(Strings) do
  begin
    if Strings[Dummy]=Text then
    begin
      result:=true;
      exit;
    end;
  end;
end;

procedure string_utils_Add(var Strings: TStringArray; Text: String);
begin
  SetLength(Strings,Length(Strings)+1);
  Strings[high(Strings)]:=Text;
end;

procedure string_utils_Add(var Strings: TStringArray; InputStrings: TStringArray);overload;
var
  Dummy: Integer;
begin
  for Dummy:=0 to high(InputStrings) do
    string_utils_Add(Strings,InputStrings[Dummy]);
end;

function string_utils_LoadFromFile(FileName: String): String;
var
  f: TFileStream;
begin
  F := TFileStream.Create(FileName, fmOpenRead or fmShareDenyWrite);
  try
    SetLength(result, f.Size);
    f.Read(result[1], Length(result));
  finally
    f.Free;
  end;
end;

procedure string_utils_AddToTStrings(Strings: TStringArray; List: TStrings);
var
  Dummy: Integer;
begin
  for Dummy:=0 to high(Strings) do
  begin
    List.Add(Strings[Dummy]);
  end;
end;

function string_utils_GetLanguageString(language: String; Text: String): String;
var
  Strings  : TStringArray;
  Dummy    : Integer;
  lang     : String;
  langText : String;
begin
  result:='';
  if not string_utils_IsLanguageString(Text) then
  begin
    result:=Text;
    exit;
  end;

  Strings:=string_utils_explode(#01,Text);

  language:=lowerCase(language);

  for Dummy:=0 to high(Strings) do
  begin
    string_utils_split(':',Strings[Dummy],lang,langText);
    if lang=language then
    begin
      result:=langText;
      exit;
    end;
  end;
  if length(Strings)>0 then
    string_utils_split(':',Strings[0],lang,result);
end;

function string_utils_SetLanguageString(language: String; Text: String; NewText: String): String;
var
  Strings  : TStringArray;
  Dummy    : Integer;
  lang     : String;
  langText : String;
  ok       : Boolean;
begin
  ok:=false;
  Strings:=nil;

  if string_utils_IsLanguageString(Text) then
  begin
    Strings:=string_utils_explode(#01,Text);

    language:=lowerCase(language);


    for Dummy:=0 to high(Strings) do
    begin
      string_utils_split(':',Strings[Dummy],lang,langText);
      if lang=language then
      begin
        Strings[Dummy]:=language+':'+NewText;
        ok:=true;
        break;
      end;
    end;
  end;

  if not ok then
  begin
    setlength(Strings,length(Strings)+1);
    Strings[high(Strings)]:=language+':'+NewText;
  end;

  result:=string_utils_implode(#01,Strings)+#01;
end;

function string_utils_IsLanguageString(Text: String): Boolean;
begin
  result:=Pos(#01,Text)<>0;
end;

procedure string_utils_WriteString(Stream: TStream;Str: String);
var
  Len : Integer;
begin
  Len:=Length(Str);
  Stream.Write(Len,SizeOf(Len));
  Stream.Write(Pointer(Str)^,Len);
end;

function string_utils_ReadString(Stream: TStream): String;
var
  Len : Integer;
begin
  Stream.Read(Len,SizeOf(Len));
  SetString(result, nil, Len);
  Stream.Read(Pointer(result)^,len);
end;


end.
