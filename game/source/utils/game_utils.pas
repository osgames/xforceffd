{********************************************************************************
*										*
* Copyright (C) 2004  X-Force Team - jim_raynor@web.de				*
*										*
* This file is part of X-Force: Fight For Destiny (X-Force).			*
*										*
* X-Force is free software; you can redistribute it and/or modify		*
* it under the terms of the GNU General Public License as published by		*
* the Free Software Foundation; either version 2 of the License, or		*
* (at your option) any later version.						*
*										*
* X-Force is distributed in the hope that it will be useful,			*
* but WITHOUT ANY WARRANTY; without even the implied warranty of		*
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the			*
* GNU General Public License for more details.					*
*										*
* You should have received a copy of the GNU General Public License		*
* along with X-Force; if not, write to the Free Software			*
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA	*
*										*
*********************************************************************************
*										*
* enth�lt Utilities um bestimmte X-Force typen in Strings zu verwandeln	        *
*										*
********************************************************************************}

unit game_utils;

interface

uses XForce_Types,SysUtils,StringConst;

function game_utils_KD4DateToStr(Date: TKD4Date):String;
function game_utils_TypeToStr(TypeID: TProjektType): String;
function game_utils_IntToText(Zahl: Integer): String;
function game_utils_NameOfExtension(Extension: TExtensionProp): String;
function game_utils_WaffTypeToStr(WaffType: TWaffenType): String;
function game_utils_OrganStatusToStr(OrganStatus: TOrganisationStatus): String;
function game_utils_FormatedExtension(Extension: TExtension): String;
function game_utils_FormatedExtensionDiff(ExtensionOld, ExtensionNew: TExtension): String;
function game_utils_FormatOfExtension(Extension: TExtensionProp): String;
function game_utils_UFormatOfExtension(Extension: TExtensionProp): String;

implementation

function game_utils_KD4DateToStr(Date: TKD4Date):String;
begin
  result:=Format(FShortDate,[Date.Day,Date.Month,Date.Year,Date.Hour,Date.Minute]);
end;

function game_utils_TypeToStr(TypeID: TProjektType): String;
begin
  case TypeId of
    ptWaffe       : result:=IAWaffe;
    ptMunition    : result:=IAMunition;
    ptRWaffe      : result:=IARWaffe;
    ptRMunition   : result:=IARMunition;
    ptPanzerung   : result:=IAPanzerung;
    ptGranate     : result:=IAGranate;
    ptMine        : result:=IAMine;
    ptMotor       : result:=IAMotor;
    ptSensor      : result:=IASensor;
    ptShield      : result:=IAShield;
    ptEinrichtung : result:=LEinrichtung;
    ptRaumSchiff  : result:=LRaumschiff;
    ptExtension   : result:=IAExtension;
    ptNone        : result:=LTechnologie;
    ptGuertel     : result:=ST0309260009;
  end;
end;

function game_utils_IntToText(Zahl: Integer): String;
begin
  if (Zahl>0) and (Zahl<10) then
    result:=AZahl[Zahl]
  else
    result:=IntToStr(Zahl);
end;

function game_utils_NameOfExtension(Extension: TExtensionProp): String;
begin
  case Extension of
    epSensor: result:=LExtSensor;
    epShieldHitpoints: Result := IAShieldPoints;
    epShieldDefence: Result := IADefend;
    epShieldReloadTime: Result := IAAufLade;
    else
      Result := '';
  end;
end;

function game_utils_FormatOfExtension(Extension: TExtensionProp): String;
begin
  case Extension of
    epSensor: result := FFloat;
    epShieldHitpoints: Result := FInteger;
    epShieldDefence: Result := FPercent;
    epShieldReloadTime: Result := FMSeconds;
    else
      Result := '';
  end;
end;

function game_utils_UFormatOfExtension(Extension: TExtensionProp): String;
begin
  case Extension of
    epSensor: result := FUFloat;
    epShieldHitpoints: Result := FUInteger;
    epShieldDefence: Result := FUPercent;
    epShieldReloadTime: Result := FUMSeconds;
    else
      Result := '';
  end;
end;

function game_utils_FormatedExtension(Extension: TExtension): String;
begin
  if Extension.Prop in [epSensor] then
    Result := Format(game_utils_FormatOfExtension(Extension.Prop), [Extension.Value/1])
  else
    Result := Format(game_utils_FormatOfExtension(Extension.Prop), [Extension.Value]);
end;

function game_utils_FormatedExtensionDiff(ExtensionOld, ExtensionNew: TExtension): String;
begin
  if ExtensionOld.Prop in [epSensor] then
    Result := Format(game_utils_UFormatOfExtension(ExtensionOld.Prop), [ExtensionOld.Value/1, ExtensionNew.Value/1])
  else
    Result := Format(game_utils_UFormatOfExtension(ExtensionOld.Prop), [ExtensionOld.Value, ExtensionNew.Value]);
end;

function game_utils_WaffTypeToStr(WaffType: TWaffenType): String;
begin
  case WaffType of
    wtProjektil   : result:=IAProjektil;
    wtRaketen     : result:=IARaketen;
    wtLaser       : result:=IALaser;
    wtChemic      : result:=IAChemisch;
    wtShortRange  : result:=ST0401120001;
  end;
end;

function game_utils_OrganStatusToStr(OrganStatus: TOrganisationStatus): String;
begin
  result:='';
  case OrganStatus of
    osAllianz     : result:=IAAlliance;
    osFriendly    : result:=IAFriendly;
    osNeutral     : result:=IANeutral;
    osUnFriendly  : result:=IAUnFriendly;
    osEnemy       : result:=IAEnemy;
  end;
end;

end.
